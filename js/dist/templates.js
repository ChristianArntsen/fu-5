(function() {
window["JST"] = window["JST"] || {};

window["JST"]["authorize.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content payments-window fb-payments-window">\n	<div class="modal-header">\n		';
 if(closable){ ;
__p += '\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		';
 } ;
__p += '\n		<h4 class="modal-title" style="text-align: center">';
 if(action){ ;
__p +=
__e(action);
}else{;
__p += 'Enter PIN';
};
__p += '</h4>\n	</div>\n	<div class="modal-body">\n		<div class="container-fluid keypad">\n			<div class="row" style="margin-bottom: 10px;">\n				<div class="col-xs-12 col-sm-6 col-sm-offset-3">\n					<form>\n						<input type="password" name="pin" autocomplete="multi-user-pin" class="form-control input-lg pin" />\n					</form>\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-xs-12 col-sm-6 col-sm-offset-3">\n					<div class="btn-group btn-group-justified">\n						<div class="btn-group">\n							<button data-char="1" class="btn btn-default">1</button>\n						</div>\n						<div class="btn-group">\n							<button data-char="2" class="btn btn-default">2</button>\n						</div>\n						<div class="btn-group">\n							<button data-char="3" class="btn btn-default">3</button>\n						</div>\n					</div>\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-xs-12 col-sm-6 col-sm-offset-3">\n					<div class="btn-group btn-group-justified">\n						<div class="btn-group">\n							<button data-char="4" class="btn btn-default">4</button>\n						</div>\n						<div class="btn-group">\n							<button data-char="5" class="btn btn-default">5</button>\n						</div>\n						<div class="btn-group">\n							<button data-char="6" class="btn btn-default">6</button>\n						</div>\n					</div>\n				</div>			\n			</div>\n			<div class="row">\n				<div class="col-xs-12 col-sm-6 col-sm-offset-3">\n					<div class="btn-group btn-group-justified">\n						<div class="btn-group">\n							<button data-char="7" class="btn btn-default">7</button>\n						</div>\n						<div class="btn-group">\n							<button data-char="8" class="btn btn-default">8</button>\n						</div>\n						<div class="btn-group">\n							<button data-char="9" class="btn btn-default">9</button>\n						</div>\n					</div>\n				</div>			\n			</div>\n			<div class="row">\n				<div class="col-xs-12 col-sm-6 col-sm-offset-3">\n					<div class="btn-group btn-group-justified">\n						<div class="btn-group">\n							<button data-char="enter" class="btn btn-default">Enter</button>\n						</div>\n						<div class="btn-group">\n							<button data-char="0" class="btn btn-default">0</button>\n						</div>\n						<div class="btn-group">\n							<button data-char="del" class="btn btn-default">Del</button>\n						</div>\n					</div>\n				</div>			\n			</div>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["card_balance_check.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content card-balance-check">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Card Lookup</h4>\n	</div>\n	<div class="modal-body">\n		<form role="form" class="form-horizontal" automcomplete="off">\n			<div class="form-group">\n				<label class="control-label col-md-3">Type</label>\n				<div class="col-md-6">\n					<div class="btn-group" data-toggle="buttons">\n						<label class="btn btn-default active">\n							<input name="type" type="radio" autocomplete="off" value="giftcard" class="type" checked> Giftcard\n						</label>\n						<label class="btn btn-default">\n							<input name="type" type="radio" autocomplete="off" value="punch_card" class="type"> Punch Card\n						</label>\n					</div>	\n				</div>			\n			</div>\n			<div class="form-group">\n				<label class="control-label col-md-3">Card Number</label>\n				<div class="col-md-6">\n					<input type="text" name="number" class="form-control card-number" automcomplete="off" />\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-primary submit">Check Balance</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["card_balance_details.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content card-balance-details">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Card Lookup</h4>\n	</div>\n	<div class="modal-body">\n		<div class="container-fluid">\n			';
 if(type == 'ets-giftcard'){ ;
__p += '\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Value</strong>\n				</div>\n				<div class="col-md-6">\n					' +
__e(accounting.formatMoney(amount)) +
'\n				</div>							\n			</div>	\n				\n			';
 }else if(type == 'giftcard'){ ;
__p += '\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Number</strong>\n				</div>\n				<div class="col-md-6">\n					' +
__e(giftcard_number) +
'\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Value</strong>\n				</div>\n				<div class="col-md-6">\n					' +
__e(accounting.formatMoney(value)) +
'\n				</div>							\n			</div>\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Customer</strong>\n				</div>\n				<div class="col-md-6">\n					';
if(customer_first_name != null){ ;
__p += '\n					' +
__e(customer_first_name +' '+customer_last_name) +
'\n					';
 }else{ ;
__p += '\n					<span class="text-muted">No Customer</span>\n					';
 };
__p += '\n				</div>								\n			</div>\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Expiration</strong>\n				</div>\n				<div class="col-md-6">\n					';
 if(expiration_date){ ;
__p += '\n					' +
__e(moment(expiration_date).format('MM/DD/YYYY')) +
'\n					';
 }else{ ;
__p += '\n					<span class="text-muted">No Expiration</span>\n					';
 } ;
__p += '\n				</div>				\n			</div>\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Department</strong>\n				</div>	\n				<div class="col-md-6">\n					' +
__e(department) +
'\n				</div>								\n			</div>	\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Category</strong>\n				</div>\n				<div class="col-md-6">\n					' +
__e(category) +
'\n				</div>								\n			</div>\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Notes</strong>\n				</div>\n				<div class="col-md-6">\n					' +
__e(details) +
'\n				</div>				\n			</div>	\n			\n			';
 }else{ ;
__p += '\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Number</strong>\n				</div>\n				<div class="col-md-6">\n					' +
__e(punch_card_number) +
'\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Punches Left</strong>\n				</div>\n				<div class="col-md-6">\n					<ul class="punch-card-items">\n						';
_.each(items, function(item){ ;
__p += '\n							<li>(' +
__e(parseInt(item.punches - item.used)) +
') ' +
__e(item.name) +
'</li>\n						';
 }); ;
__p += '\n					</ul>\n				</div>							\n			</div>\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Customer</strong>\n				</div>\n				<div class="col-md-6">\n					';
if(customer_first_name != null){ ;
__p += '\n					' +
__e(customer_first_name +' '+customer_last_name) +
'\n					';
 }else{ ;
__p += '\n					<span class="text-muted">No Customer</span>\n					';
 };
__p += '\n				</div>								\n			</div>\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Expiration</strong>\n				</div>\n				<div class="col-md-6">\n					';
 if(expiration_date && expiration_date != '0000-00-00'){ ;
__p += '\n					' +
__e(moment(expiration_date).format('MM/DD/YYYY')) +
'\n					';
 }else{ ;
__p += '\n					<span class="text-muted">No Expiration</span>\n					';
 } ;
__p += '\n				</div>				\n			</div>\n			<div class="row">\n				<div class="col-md-3">\n					<strong>Notes</strong>\n				</div>\n				<div class="col-md-6">\n					' +
__e(details) +
'\n				</div>				\n			</div>					\n			';
 } ;
__p += '			\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-default" data-dismiss="modal">Close</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["ets_card_balance_check.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content ets-card-balance-check">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Giftcard Lookup</h4>\n	</div>\n	<div class="modal-body" style="min-height: 300px;"></div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["header.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="navbar-header">\n    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu-collapse">\n        <span class="sr-only">Toggle navigation</span>\n        <span class="icon-bar"></span>\n        <span class="icon-bar"></span>\n        <span class="icon-bar"></span>\n    </button>\n    <div class="navbar-brand app-header-logo" style="margin-right: 20px;">\n        <!--img src="/images/logo-white-small.png"-->\n        <img src="/images/foreUP-Holiday-2-small.png">\n    </div>\n</div>\n<div id="unread-messages" class="msg_notification"></div>\n<div class="collapse navbar-collapse" id="main-menu-collapse">\n    <ul class="nav navbar-nav" style="margin-left: -15px;" id="application-menu"></ul>\n    <div id="page-stats" class="header-stats hidden-xs hidden-sm "></div>\n    <ul class="nav navbar-nav navbar-right" id="user-menu"></ul>\n    <p id="page-header-content"></p>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["install_extension_prompt.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content extension-prompt">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n        <h4 class="modal-title">foreUP Printing Extension</h4>\n    </div>\n    <div class="modal-body container-fluid">\n        <div>\n            foreUP has created a new tool for your Star WebPRNT printers!\n            It is required for your printers to work and is just a click away.\n            Simply install the chrome extension for more security and better performance.\n        </div>\n    </div>\n    <div class="modal-footer">\n        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>\n        <button type="button" class="btn btn-primary add_to_chrome">Add to Chrome</button>\n    </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["item_search_add_new.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div style="padding: 5px 10px 10px 10px; width: 100%">\n	<div class="pull-left"><h4 class="tt-placeholder">No Results Found</h4></div>\n	<button class="btn btn-primary pull-right new-item">Add New Item</button>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["item_search_result.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 
var subText = false;
if(item_type == 'green_fee' || item_type == 'cart_fee'){ 
	subText = teesheet_name;

	if(season_id == 0){
		subText += ' - Special';
	}else{
		subText += ' - '+season_name;
	}
}
;
__p += '\n<div>\n	<div>' +
__e(name) +
'</div>\n	';
 if(subText){ ;
__p += '<div class="text-muted">' +
__e(subText) +
'</div>';
 } ;
__p += '\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["login.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content">\n	<div class="modal-header">\n		';
 if(closable){ ;
__p += '\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		';
 } ;
__p += '\n		<h4 class="modal-title">Login</h4>\n	</div>\n	<div class="modal-body">\n		<p class="alert alert-info">You have been logged out, please log back in to continue</p>\n		<div class="container-fluid">\n			<form class="form-horizontal">\n				<div class="form-group">\n					<label for="login-username" class="col-sm-2 control-label">Username</label>\n					<div class="col-sm-6">\n						<input type="text" class="form-control username" id="login-username" placeholder="Username">\n					</div>\n				</div>\n				<div class="form-group">\n					<label for="login-password" class="col-sm-2 control-label">Password</label>\n					<div class="col-sm-6">\n						<input type="password" class="form-control password" id="login-password" placeholder="Password">\n					</div>\n				</div>\n			</form>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button class=\'btn btn-primary login\'>Log In</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["logout_option_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content logout-option">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Logout</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		';
 if(register_log_open){ ;
__p += '\n		<div class="row">\n			<div class="col-xs-12" id="logout_close">\n				<h4>Close Cash Log</h4>\n				<span>Close cash log & sign out</span>\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-xs-12" id="logout_switch_user">\n				<h4>Switch User</h4>\n				<span>Keep cash log & switch user</span>					\n			</div>				\n		</div>\n		';
 if(can_bypass_register_log){ ;
__p += '<div class="row">\n			<div class="col-xs-12" id="logout_bypass">\n				<h4>Bypass Cash Log</h4>\n				<span>Bypass cash log & sign out</span>					\n			</div>				\n		</div>\n		';
 } ;
__p += '\n		';
 }else{ ;
__p += '\n		<div class="row">\n			<div class="col-xs-12" id="logout_close">\n				<h4>Logout</h4>\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-xs-12" id="logout_switch_user">\n				<h4>Switch User</h4>					\n			</div>				\n		</div>\n		';
 } ;
__p += '\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["logout_switch_user.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content logout-switch-user">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Switch User</h4>\n	</div>\n	<div class="modal-body">\n		<div class="container-fluid keypad">\n			<form class="form-horizontal">\n				<div class="form-group">\n					<label for="employee-id" class="col-sm-2 control-label">Employee</label>\n					<div class="col-sm-6">\n						' +
((__t = (_.dropdown(employees, {class:'form-control person_id', name:'employee_id', id:'employee-id'}, null))) == null ? '' : __t) +
'\n					</div>\n				</div>\n				<div class="form-group">\n					<label for="employee-password" class="col-sm-2 control-label">Password</label>\n					<div class="col-sm-6">\n						<input type="password" class="form-control password" id="employee-password" placeholder="Password">\n					</div>\n				</div>\n			</form>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button type="submit" class="btn btn-primary" id="sign-in-user">Sign In</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["manager_override_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content manager-override">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Manager Override</h4>\n	</div>\n	<div class="modal-body">\n		<div class="container-fluid keypad">\n			<form class="form-horizontal">\n				<div class="form-group">\n					<label for="manager-person-id" class="col-sm-2 control-label">Manager</label>\n					<div class="col-sm-6">\n						' +
((__t = (_.dropdown(employees, {class:'form-control person_id', name:'employee_id', id:'manager-person-id'}, null))) == null ? '' : __t) +
'\n					</div>\n				</div>\n				<div class="form-group">\n					<label for="manager-password" class="col-sm-2 control-label">Password</label>\n					<div class="col-sm-6">\n						<input type="password" class="form-control password" id="manager-password" placeholder="Password">\n					</div>\n				</div>\n			</form>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button type="submit" class="btn btn-primary submit">Sign In</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["no_permission.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="no_permission_background">\n	<div>\n		Sorry, you do not have permission.\n		<p>If you believe this is a mistake please reach out to us at our phone or chat support.</p>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["pass_search_result.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div>\n	<div>' +
__e(name) +
'</div>\n	<div class="text-muted">' +
__e(customer.first_name+' '+customer.last_name) +
'</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["search_customer.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div>\n	' +
__e(first_name) +
' ' +
__e(last_name) +
'\n	';
 if(phone_number && phone_number != ''){ ;
__p += '\n	<br><small class="details">H: ' +
__e(_.formatPhoneNumber(phone_number)) +
'</small>\n	';
 } ;
__p += '\n	';
 if(cell_phone_number && cell_phone_number != ''){ ;
__p += '\n	<br><small class="details">C: ' +
__e(_.formatPhoneNumber(cell_phone_number)) +
'</small>\n	';
 } ;
__p += '\n	';
 if(email && email != ''){ ;
__p += '\n	<br><small class="details">' +
__e(email) +
'</small>\n	';
 } ;
__p += '\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["terminal_select.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content fb-course-select">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Select Terminal</h4>\n	</div>\n	<div class="modal-body"></div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["terminal_select_button.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p +=
__e(label) +
'\n';
 if(active_register_log){ ;
__p += '\n';
 if(active_register_log_employee){ ;
__p += '\n<div style="font-size: 14px; padding-top: 5px">\n	<span class="label label-info" style="font-weight: normal">Drawer ' +
__e(active_register_log.get('drawer_number')) +
' IN USE</span> by ' +
__e(active_register_log_employee.get('first_name')) +
' ' +
__e(active_register_log_employee.get('last_name')) +
'\n	' +
__e(moment(active_register_log.get('shift_start')).format('M/D/YY [at] h:mma')) +
'\n</div>\n';
 } ;
__p += '\n';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["timeclock_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content timeclock">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Timeclock</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<label for="timeclock-person-id" class="col-sm-3 control-label">Employee</label>\n				<div class="col-sm-6">\n					' +
((__t = (_.dropdown(employees, {class:'form-control person_id', name:'employee_id', id:'timeclock-person-id'}, person_id))) == null ? '' : __t) +
'\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="timeclock-password" class="col-sm-3 control-label">Password</label>\n				<div class="col-sm-6">\n					<input type="password" class="form-control password" id="timeclock-password" placeholder="Password">\n				</div>\n			</div>\n			';
 if(show_terminals){ ;
__p += '\n			<div class="form-group">\n				<label for="timeclock-terminal-id" class="col-sm-3 control-label">Terminal</label>\n				<div class="col-sm-6">\n					' +
((__t = (_.dropdown(terminals, {class:'form-control terminal', name:'terminal_id', id:'timeclock-terminal-id'}, null))) == null ? '' : __t) +
'\n				</div>\n			</div>\n			';
 } ;
__p += '\n			<div class="form-group">\n				<div class="col-sm-12">\n					<h3 id="clock_in_status" style="text-align: center">\n						';
 if(clocked_in){ ;
__p += '\n						Clocked in ' +
__e(moment(current_entry.get('shift_start')).format('M/D/YYYY [at] h:mma')) +
'\n						';
 }else{ ;
__p += '\n						Clocked Out\n						';
 } ;
__p += '\n					</h3>\n				</div>\n			</div>\n			<div class="form-group">\n				<div class="col-sm-4 col-sm-offset-4">\n					';
 if(clocked_in){ ;
__p += '\n					<button class="btn btn-primary btn-block clock-out">Clock Out</button>\n					';
 }else{ ;
__p += '\n					<button class="btn btn-primary btn-block clock-in">Clock In</button>\n					';
 } ;
__p += '\n				</div>\n			</div>		\n		</form>\n		<div class="row">\n			<div class="col-sm-12">\n				<p style="text-align: center">\n					<a href="#" id="toggle_timeclock_entries">Show Timeclock History</a>\n				</p>\n			</div>\n		</div>\n		<div class="row" id="timeclock_entries" style="display: none">\n			<div class="table-responsive">\n				<table class="table table-condensed">\n					<thead>\n						<tr>\n							';
 if(show_terminals){ ;
__p += '<th>Terminal</th>';
 } ;
__p += '\n							<th>Shift Start</th>\n							<th>Shift End</th>\n							<th>Total Time</th>\n						</tr>\n					</thead>\n					<tbody>\n						';
 if(entries.length > 0){
						_.each(entries.models, function(entry){ ;
__p += '\n						<tr>\n							';
 if(show_terminals){ ;
__p += '\n							<td>\n								' +
__e(entry.get('terminal')) +
'\n							</td>\n							';
 } ;
__p += '\n							<td>\n								' +
__e(moment(entry.get('shift_start')).format('M/D/YYYY @ h:mma')) +
'\n							</td>\n							<td>\n								';
 if(entry.get('shift_end') == '0000-00-00 00:00:00'){ ;
__p += '\n								<span class="text-muted">In Progress</span>\n								';
 }else{ ;
__p += '\n								' +
__e(moment(entry.get('shift_end')).format('M/D/YYYY @ h:mma')) +
'\n								';
 } ;
__p += '\n							</td>\n							<td>\n								';
 if(entry.get('total') == '0'){ ;
__p += '\n								<span class="text-muted">n/a</span>\n								';
 }else{ ;
__p += '\n								' +
__e(entry.get('total')) +
'	\n								';
 } ;
__p += '\n							</td>\n						</tr>\n						';
 }) }else{ ;
__p += '\n						<tr>\n							<td colspan="4" style="text-align: center;">\n								<span class="text-muted">No Entries</span>\n							</td>\n						</tr>\n						';
 } ;
__p += '\n					</tbody>\n				</table>\n			</div>\n		</div>	\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["contact_forms/contact_form_replies.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content contact-form-details">\n  <div class="modal-header">\n    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n    <h4 class="modal-title">Contact Form Replies</h4>\n  </div>\n  <div class="modal-body container-fluid">\n    <table class="table table-striped">\n      <thead>\n        <tr>\n          <th>Date</th>\n          <th>First Name</th>\n          <th>Last Name</th>\n          <th>Email</th>\n          <th>Message</th>\n        </tr>\n      </thead>\n      <tbody>\n        ';
 _.each(replies, function (reply) { ;
__p += '\n          <tr>\n            <td>' +
__e(moment(reply.get('completed_at')).format('MM/DD/YYYY') ) +
'</td>\n            <td>' +
__e(reply.get('first_name') ) +
'</td>\n            <td>' +
__e(reply.get('last_name') ) +
'</td>\n            <td>' +
__e(reply.get('email') ) +
'</td>\n            <td>' +
__e(reply.get('message') ) +
'</td>\n          </tr>\n        ';
 }); ;
__p += '\n      </tbody>\n    </table>\n  </div>\n  <div class="modal-footer">\n    <button type="button" class="btn btn-default" data-dismiss="modal">Dismiss</button>\n  </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["contact_forms/contact_forms.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="contact-form-actions" class="col-md-3 col-lg-2">\n	<div class="row">\n		<div class="col-xs-12" style="padding-bottom: 15px">\n			<button class="btn btn-primary btn-block add">Add New Contact Form</button>\n			<button class="btn btn-danger btn-block delete">Delete Contact Forms</button>\n		</div>\n	</div>\n</div>\n<div class="col-md-9 col-lg-10">\n	<div class="row" id="contact-form-table"></div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["contact_forms/contact_forms_edit.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content contact-form-details">\n  <div class="modal-header">\n    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n    <h4 class="modal-title">Contact Form Details</h4>\n  </div>\n  <div class="modal-body container-fluid">\n    <form class="form-horizontal" role="form">\n      <div class="form-group">\n        <label class="col-md-3 control-label" for="contact-form-name">Contact Form Name</label>\n        <div class="col-md-5">\n          <input type="text" name="name" value="' +
__e(name) +
'" class="form-control" id="contact-form-name"\n            data-bv-notempty data-bv-notempty-message="Contact form name is required">\n        </div>\n      </div>\n      <div class="form-group">\n        <label class="col-md-3 control-label" for="contact-form-name">Preset</label>\n        <div class="col-md-5">\n          <select class="form-control" id="form_presets">\n            <option value="">--</option>\n            <option value="birthday">Birthday</option>\n            <option value="marketing">Marketing</option>\n          </select>\n        </div>\n      </div>\n\n      <div class="form-group">\n        <label class="col-md-3 control-label" for="customer-group-select">Customer Group</label>\n        <div class="col-md-5">\n          <select class="form-control" id="customer-group-select">\n            ';
 _.each(customer_groups, function (group) { ;
__p += '\n              <option ';
 if(customer_group_id == group.id) print("selected"); ;
__p += ' value="' +
((__t = (group.id)) == null ? '' : __t) +
'">' +
((__t = (group.get('label'))) == null ? '' : __t) +
'</option>\n            ';
 }); ;
__p += '\n          </select>\n        </div>\n      </div>\n\n      <table class="table table-striped">\n        <thead>\n          <tr>\n            <th>Field</th>\n            <th>Show</th>\n            <th>Required</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr>\n            <td>First Name</td>\n            <td><input type="checkbox" name="has_first_name" ' +
__e(has_first_name ? 'checked' : '') +
'></td>\n            <td><input type="checkbox" name="req_first_name" ' +
__e(req_first_name ? 'checked' : '') +
'></td>\n          </tr>\n          <tr>\n            <td>Last Name</td>\n            <td><input type="checkbox" name="has_last_name" ' +
__e(has_last_name ? 'checked' : '') +
'></td>\n            <td><input type="checkbox" name="req_last_name" ' +
__e(req_last_name ? 'checked' : '') +
'></td>\n          </tr>\n          <tr>\n            <td>Email</td>\n            <td><input type="checkbox" name="has_email" ' +
__e(has_email ? 'checked' : '') +
'></td>\n            <td><input type="checkbox" name="req_email" ' +
__e(req_email ? 'checked' : '') +
'></td>\n          </tr>\n          <tr>\n            <td>Phone Number</td>\n            <td><input type="checkbox" name="has_phone_number" ' +
__e(has_phone_number ? 'checked' : '') +
'></td>\n            <td><input type="checkbox" name="req_phone_number" ' +
__e(req_phone_number ? 'checked' : '') +
'></td>\n          </tr>\n          <tr>\n            <td>Birth Date</td>\n            <td><input type="checkbox" name="has_birthday" ' +
__e(has_birthday ? 'checked' : '') +
'></td>\n            <td><input type="checkbox" name="req_birthday" ' +
__e(req_birthday ? 'checked' : '') +
'></td>\n          </tr>\n          <tr>\n            <td>Address</td>\n            <td><input type="checkbox" name="has_address" ' +
__e(has_address ? 'checked' : '') +
'></td>\n            <td><input type="checkbox" name="req_address" ' +
__e(req_address ? 'checked' : '') +
'></td>\n          </tr>\n          <tr>\n            <td>City</td>\n            <td><input type="checkbox" name="has_city" ' +
__e(has_city ? 'checked' : '') +
'></td>\n            <td><input type="checkbox" name="req_city" ' +
__e(req_city ? 'checked' : '') +
'></td>\n          </tr>\n          <tr>\n            <td>State</td>\n            <td><input type="checkbox" name="has_state" ' +
__e(has_state ? 'checked' : '') +
'></td>\n            <td><input type="checkbox" name="req_state" ' +
__e(req_state ? 'checked' : '') +
'></td>\n          </tr>\n          <tr>\n            <td>Zip Code</td>\n            <td><input type="checkbox" name="has_zip" ' +
__e(has_zip ? 'checked' : '') +
'></td>\n            <td><input type="checkbox" name="req_zip" ' +
__e(req_zip ? 'checked' : '') +
'></td>\n          </tr>\n          <tr>\n            <td>Message</td>\n            <td><input type="checkbox" name="has_message" ' +
__e(has_message ? 'checked' : '') +
'></td>\n            <td><input type="checkbox" name="req_message" ' +
__e(req_message ? 'checked' : '') +
'></td>\n          </tr>\n        </tbody>\n      </table>\n    </form>\n  </div>\n  <div class="modal-footer">\n    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n    <button type="button" class="btn btn-primary save">Save</button>\n  </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["contact_forms/contact_forms_table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<table class="table table-bordered contact-forms col-xs-12">\n  <thead>\n    <th><input type="checkbox" class="check-all"></th>\n    <th>Form Name</th>\n    <th>Actions</th>\n  </thead>\n  <tbody>\n  </tbody>\n</table>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["contact_forms/contact_forms_table_row.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<td><input type="checkbox" data-contact-form-id="' +
((__t = (id)) == null ? '' : __t) +
'"></td>\n<td class="name">' +
((__t = ( name )) == null ? '' : __t) +
'</td>\n<td>\n  <a href="#" class="edit">Edit</a>&nbsp;|&nbsp;\n  <a href="#" class="code">Generate Code</a>&nbsp;|&nbsp;\n  <a href="#" class="replies">Replies</a>&nbsp;|&nbsp;\n  <a href="#" class="text-danger delete">Delete</a>\n</td>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["header/application_menu.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<a href="#" class="dropdown-toggle app-header-current app-header-btn" data-toggle="dropdown" role="button" aria-expanded="false">\n    <div class="app-icon app-icon-header app-icon-white app-icon-' +
__e(App.router.page) +
'"></div>\n    <span id="current-page">' +
__e(App.router.page) +
'</span>\n    <span class="caret"></span>\n</a>\n<ul class="dropdown-menu app-header-apps" role="menu">\n    ';

    var hide_food_bev = false;
    var hide_sales = false;
    
    if(App.data.modules.findWhere({'module_id': 'sales_v2'}) != undefined){
        hide_sales = true;
    }
    if(App.data.modules.findWhere({'module_id': 'food_and_beverage_v2'}) != undefined){
        hide_food_bev = true;
    }

    _.each(App.data.modules.models, function(module){
    if(
        (module.get('module_id') == 'sales' && hide_sales) ||
        (module.get('module_id') == 'food_and_beverage' && hide_food_bev)
    ){
    return false;
    }

    if(!App.data.user.has_module_permission(module.get('module_id'))){

        if(module.get("is_partner_app") && App.data.user.is_admin()){
            //Allow admins access to all partner apps
        } else {
            return false;
        }
    } ;
__p += '\n    <li>\n        <a href="' +
__e(module.getUrl()) +
'" ';
 if(module.get("is_partner_app")){print('target="_blank"')} ;
__p += '>\n            ';
 if(LANG[module.get('name_lang_key')]){ ;
__p += '\n            <div class="app-icon app-icon-' +
__e(LANG[module.get('name_lang_key')]) +
'"></div>\n            <div class="app-content">\n                <div class="app-name">\n                    ' +
__e(LANG[module.get('name_lang_key')]) +
'\n                </div>\n                <div class="app-description">\n                    ' +
__e(LANG[module.get('desc_lang_key')]) +
'\n                </div>\n            </div>\n            ';
 }else{ ;
__p += '\n            <div class="app-icon app-icon-' +
__e(module.get('name_lang_key')) +
'"></div>\n            <div class="app-content">\n                <div class="app-name">\n                    ' +
__e(module.get('name_lang_key')) +
'\n                </div>\n                <div class="app-description">\n                    ' +
__e(module.get('desc_lang_key')) +
'\n                </div>\n            </div>\n            ';
 } ;
__p += '\n        </a>\n    </li>\n    ';
 }) ;
__p += '\n</ul>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["header/user_menu.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<a href="#" class="dropdown-toggle app-header-btn" data-toggle="dropdown" role="button" aria-expanded="false">\n    ' +
__e(App.data.user.get('first_name') +' '+ App.data.user.get('last_name')) +
'\n    <span class="caret"></span>\n</a>\n<ul class="dropdown-menu app-header-list" role="menu">\n    <li>\n        <a href="#" id="menu_timeclock">\n            <div class="app-icon app-icon-time-clock"></div>\n            <div class="app-content">\n                <div class="app-name">\n                    Timeclock\n                </div>\n                <div class="app-description">\n                    Clock In/Clock Out\n                </div>\n            </div>\n        </a>\n    </li>\n    ';
 if(App.data.user.has_module_permission('employees')){ ;
__p += '\n    <li>\n        <a href="' +
__e(SITE_URL + '/employees') +
'">\n            <div class="app-icon app-icon-employees"></div>\n            <div class="app-content">\n                <div class="app-name">\n                    Employees\n                </div>\n                <div class="app-description">\n                    Add, Update & Search\n                </div>\n            </div>\n        </a>\n    </li>\n    ';
 } ;
__p += '\n    ';
 if(App.data.user.has_module_permission('config')){ ;
__p += '\n    <li>\n        <a href="' +
__e(SITE_URL + '/config') +
'">\n            <div class="app-icon app-icon-config"></div>\n            <div class="app-content">\n                <div class="app-name">\n                    Settings\n                </div>\n                <div class="app-description">\n                    & Preferences\n                </div>\n            </div>\n        </a>\n    </li>\n    ';
 } ;
__p += '\n    <li>\n        <a href="' +
__e(SITE_URL + '/support') +
'">\n            <div class="app-icon app-icon-support"></div>\n            <div class="app-content">\n                <div class="app-name">\n                    Support\n                </div>\n                <div class="app-description">\n                    ForeUp help\n                </div>\n            </div>\n        </a>\n    </li>\n    <li>\n        <a id="chat-support" onclick="olark(\'api.box.expand\')">\n            <div class="app-icon app-icon-support"></div>\n            <div class="app-content">\n                <div class="app-name">Chat</div>\n                <div class="app-description">Chat support</div>\n            </div>\n        </a>\n    </li>\n    <li>\n        <a id="uservoice-feedback" data-uv-trigger="smartvote">\n\n            <div class="app-icon app-icon-support"></div>\n            <div class="app-content">\n                <div class="app-name">Feedback</div>\n                <div class="app-description">Submit feedback</div>\n            </div>\n        </a>\n    </li>\n    <!-- START JULY REFERRAL PROGRAM LINK -->\n\n    <!--<li style="background-color:#d3ffbc;">\n        <a id="menu_referral_program" target=\'_blank\' href="http://foreupgolf.com/referrals/">\n            <div class="app-icon app-icon-Customers"></div>\n            <div class="app-content">\n                <div class="app-name">\n                    Referral Program\n                </div>\n                <div class="app-description">\n                    Make Some Extra Cash\n                </div>\n            </div>\n        </a>\n    </li>-->\n    <!-- END JULY REFERRAL PROGRAM LINK -->\n\n    <li>\n        <a id="menu_logout" href="' +
__e(SITE_URL + '/home/logout') +
'">\n            <div class="app-icon app-icon-logout"></div>\n            <div class="app-content">\n                <div class="app-name">\n                    Logout\n                </div>\n                <div class="app-description">\n                    Sign out of ForeUp\n                </div>\n            </div>\n        </a>\n    </li>\n</ul>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/customer_form.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if(birthday == '' || birthday == '0000-00-00'){
	birthday = '';
}else{
	birthday = moment(birthday).format('MM/DD/YYYY');
} ;
__p += '\n<div class="modal-content edit-customer-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		';
 if(typeof(person_id) != 'undefined'){
			var name = first_name + ' ' + last_name;
		}else{
			var name = 'New';
		} ;
__p += '\n		<h4 class="modal-title">\n			';
 if(person_id){ ;
__p += '\n			Customer - ' +
__e(name) +
'\n			';
 }else{ ;
__p += '\n			New Customer\n			';
 } ;
__p += '\n		</h4>\n	</div>\n	<div class="modal-body container-fluid" style="min-height: 500px;">\n		<div class="row">\n			<div class="col-md-12">\n				<ul class="nav nav-pills" role="tablist">\n					<li class="active"><a href="#customer-information" role="tab" data-toggle="tab">Information</a></li>\n					<li><a href="#customer-settings" role="tab" data-toggle="tab">Settings</a></li>\n					<li><a href="#customer-marketing" role="tab" data-toggle="tab">Marketing</a></li>\n				</ul>\n			</div>\n		</div>\n		<div class="tab-content" style="margin-top: 15px;">\n			<!-- Basic information -->\n			<div class="tab-pane active" id="customer-information">\n				<form class="form-horizontal" role="form" id="customer-information-form">\n					<div class="col-md-6">\n						<div class="form-group">\n							<label for="customer-first-name" class="col-md-4 control-label">First Name ' +
((__t = (get_required_text('first_name'))) == null ? '' : __t) +
'</label>\n							<div class="col-md-8">\n								<input class="form-control" type="text" value="' +
__e(first_name) +
'" name="first_name" id="customer-first-name" />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-last-name" class="col-md-4 control-label">Last name ' +
((__t = (get_required_text('last_name'))) == null ? '' : __t) +
'</label>\n							<div class="col-md-8">\n								<input class="form-control" type="text" value="' +
__e(last_name) +
'" name="last_name" id="customer-last-name" />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-email" class="col-md-4 control-label">Email ' +
((__t = (get_required_text('email'))) == null ? '' : __t) +
'</label>\n							<div class="col-md-8">\n								<input class="form-control" type="text" value="' +
__e(email) +
'" name="email" id="customer-email" />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-cell-phone" class="col-md-4 control-label" >Cell Phone ' +
((__t = (get_required_text('cell_phone_number'))) == null ? '' : __t) +
'</label>\n							<div class="col-md-8">\n								<input class="form-control" type=\'tel\'\n								value="' +
__e(cell_phone_number) +
'"\n								name="cell_phone_number"\n								id="customer-cell-phone" />\n\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-phone" class="col-md-4 control-label">Phone Number ' +
((__t = (get_required_text('phone_number'))) == null ? '' : __t) +
'</label>\n							<div class="col-md-8">\n								<input class="form-control"\n									   type=\'tel\'\n									   value="' +
__e(phone_number) +
'"\n									   name="phone_number"\n									   id="customer-phone" />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-birthday" class="col-md-4 control-label">Birthday</label>\n							<div class="col-md-5">\n								<input class="form-control" type="text" value="' +
((__t = ( (new Date(birthday)).toLocaleDateString())) == null ? '' : __t) +
'" name="birthday" id="customer-birthday" placeholder="MM/DD/YYYY" />\n							</div>\n							<span class="customer-age">' +
((__t = ( isNaN(new Date(birthday))? '' : '('+Math.floor((new Date - new Date(birthday))/(60000*60*24*365))+' yrs)' )) == null ? '' : __t) +
'</span>\n						</div>\n						<div class="form-group">\n							<label for="customer-date-created" class="col-md-4 control-label">Date Joined</label>\n							<div class="col-md-5">\n								';
 if(date_created == "" || _.isNull(date_created)){ ;
__p += '\n									<input class="form-control" type="text" value="" name="date_created" id="customer-date-created" placeholder="MM/DD/YYYY" />\n								';
 } else { ;
__p += '\n									<input class="form-control" type="text" value="' +
((__t = ( (new Date(date_created)).toLocaleDateString())) == null ? '' : __t) +
'" name="date_created" id="customer-date-created" placeholder="MM/DD/YYYY" />\n								';
 } ;
__p += '\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-photo" class="col-md-4 control-label">Photo</label>\n							<div class="col-md-8">\n								<div class="row">\n									<div class="col-xs-6" style="padding-right: 0px;">\n										<button class="btn btn-danger btn-sm btn-block delete-photo" style="';
if(!image_id){ ;
__p += 'display: none;';
 } ;
__p += '">&times;</button>\n										<img class="customer-photo" src="' +
((__t = (photo)) == null ? '' : __t) +
'">\n										<input type="hidden" value="' +
__e(image_id) +
'" id="image_id" name="image_id">\n									</div>\n									<div class="col-xs-6" style="padding-left: 0px;">\n										<button class="btn btn-sm btn-block btn-default edit-photo">Edit</button>\n										<button class="btn btn-sm btn-block btn-default take-photo">Take Photo</button>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>\n					<div class="col-md-6">\n						<div class="form-group">\n							<label for="customer-address1" class="col-md-4 control-label">Address ' +
((__t = (get_required_text('address_1'))) == null ? '' : __t) +
'</label>\n							<div class="col-md-8">\n								<input class="form-control" type="text" value="' +
__e(address_1) +
'" name="address_1" id="customer-address1" />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-addresss2" class="col-md-4 control-label"></label>\n							<div class="col-md-8">\n								<input class="form-control" type="text" value="' +
__e(address_2) +
'" name="address_2" id="customer-address2" />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-city" class="col-md-4 control-label">City ' +
((__t = (get_required_text('city'))) == null ? '' : __t) +
'</label>\n							<div class="col-md-8">\n								<input class="form-control" type="text" value="' +
__e(city) +
'" name="city" id="customer-city" />\n							</div>\n						</div>\n						';

						if(SETTINGS.country == "UK"){
						;
__p += '\n							<div class="form-group">\n								<label for="customer-state" class="col-md-4 control-label">County ' +
((__t = (get_required_text('state'))) == null ? '' : __t) +
'</label>\n								<div class="col-md-8">\n									<input class="form-control" type="text" value="' +
__e(state) +
'" name="state" id="customer-state" />\n								</div>\n							</div>\n\n						';
 }else{ ;
__p += '\n							<div class="form-group">\n								<label for="customer-state" class="col-md-4 control-label">State ' +
((__t = (get_required_text('state'))) == null ? '' : __t) +
'</label>\n								<div class="col-md-8">\n									' +
((__t = (_.dropdown(US_STATES, {"name":"state", "id":"customer-state", "class":"form-control"}, state))) == null ? '' : __t) +
'\n								</div>\n							</div>\n						';
 } ;
__p += '\n\n						<div class="form-group">\n							<label for="customer-zip" class="col-md-4 control-label">Post Code ' +
((__t = (get_required_text('zip'))) == null ? '' : __t) +
'</label>\n							<div class="col-md-4">\n								<input class="form-control" type="number" value="' +
__e(zip) +
'" name="zip" id="customer-zip" />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-status" class="col-md-4 control-label">Status</label>\n							<div class="col-md-4">\n								' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"status_flag", "id":"customer-status-flag", "class":"form-control"}, status_flag))) == null ? '' : __t) +
'\n							</div>\n						</div>\n\n					</div>\n					<div class="col-md-12">\n						<div class="form-group">\n							<label for="customer-comments" class="col-md-2 control-label">Comments</label>\n							<div class="col-md-10">\n								<textarea name="comments" id="customer-comments" class="form-control" style="height: 100px;">' +
__e(comments) +
'</textarea>\n							</div>\n						</div>\n					</div>\n				</form>\n			</div>\n\n			<!-- Settings -->\n			<div class="tab-pane" id="customer-settings">\n				<form class="form-horizontal" role="form" id="customer-settings-form">\n					<div class="col-md-6">\n						<div class="form-group">\n							<label for="customer-account-number" class="col-md-4 control-label">Account # ' +
((__t = (get_required_text('account_number'))) == null ? '' : __t) +
'</label>\n							<div class="col-md-8">\n								<input type="text" name="account_number" class="form-control" id="customer-account-number" value="' +
__e(account_number) +
'" />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-price-class" class="col-md-4 control-label">Price Class</label>\n							<div class="col-md-6">\n                                <select name="price_class" id="customer-price-class" class="form-control" ';
 if (!App.data.user.get('acl').can('update', 'Customer/PriceClass')){print('disabled="disabled"') } ;
__p += ' >\n                                    ';
 _.each(App.data.course.get('price_class_objects'), function(price_class_object){ ;
__p += '\n                                    <option value="' +
__e(price_class_object['class_id']) +
'" ';
 if(price_class_object['class_id'] == price_class) {print('selected')} ;
__p += '>' +
__e(price_class_object['name']) +
'</option>\n                                    ';
 }) ;
__p += '\n                                </select>\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-discount" class="col-md-4 control-label">Discount</label>\n							<div class="col-md-4">\n								<div class="input-group">\n									<input type="text" name="discount" class="form-control" id="customer-discount" value="' +
__e(discount) +
'" /><div class="input-group-addon">%</div>\n								</div>\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-member" class="col-md-4 control-label">Member</label>\n							<div class="col-md-4">\n								<input type="checkbox" name="member" id="customer-member" value="1" data-unchecked-value="0" style="margin-top: 10px;" ';
 if(member == "1"){ print('checked') };
__p += ' />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-member-balance" class="col-md-4 control-label">' +
__e(member_account_label) +
'</label>\n							<div class="col-md-8">\n								<div class="input-group">\n									<div class="input-group-addon">$</div><input type="text" name="member_account_balance" class="form-control" id="customer-member-balance" value="' +
__e(accounting.formatNumber(member_account_balance, 2)) +
'"\n									';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\n									disabled="disabled"\n									';
 } ;
__p += '/>\n								<div class="input-group-addon"><label style="margin: 0px; font-weight: normal;">\n									<input type="checkbox" value="1" data-unchecked-value="0" name="member_account_balance_allow_negative" style="margin-top: 5px;" ';
 if(member_account_balance_allow_negative == "1"){ print('checked') };
__p += '\n									';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\n									disabled="disabled"\n									';
 } ;
__p += '/> Allow negative</label></div>\n								</div>\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-member-account-limit" class="col-md-4 control-label">' +
__e(member_account_label) +
' Limit</label>\n							<div class="col-md-5">\n								<div class="input-group">\n									<div class="input-group-addon">(-)</div><input type="text" name="member_account_limit" class="form-control" id="customer-member-account-limit" value="' +
__e(accounting.formatNumber(Math.abs(member_account_limit), 2)) +
'"\n									';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\n									disabled="disabled"\n									';
 } ;
__p += '/>\n								</div>\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-customer-credit" class="col-md-4 control-label">' +
__e(customer_account_label) +
'</label>\n							<div class="col-md-8">\n								<div class="input-group">\n									<div class="input-group-addon">$</div><input type="text" name="account_balance" class="form-control" id="customer-customer-credit" value="' +
__e(accounting.formatNumber(account_balance, 2)) +
'"\n									';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\n									disabled="disabled"\n									';
 } ;
__p += '/><div class="input-group-addon"><label style="margin: 0px; font-weight: normal;">\n									<input type="checkbox" value="1" data-unchecked-value="0" name="account_balance_allow_negative" style="margin-top: 5px;" ';
 if(account_balance_allow_negative == "1"){ print('checked') };
__p += '\n									';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\n									disabled="disabled"\n									';
 } ;
__p += '\n									/> Allow negative</label></div>\n								</div>\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-account-limit" class="col-md-4 control-label">' +
__e(customer_account_label) +
' Limit</label>\n							<div class="col-md-5">\n								<div class="input-group">\n									<div class="input-group-addon">(-)</div><input type="text" name="account_limit" class="form-control" id="customer-account-limit" value="' +
__e(accounting.formatNumber(Math.abs(account_limit), 2)) +
'"\n									';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\n									disabled="disabled"\n									';
 } ;
__p += '\n									/>\n								</div>\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-invoice-balance" class="col-md-4 control-label">Open Invoices</label>\n							<div class="col-md-5">\n								<div class="input-group">\n									<div class="input-group-addon">$</div><input type="text" name="invoice_balance" class="form-control" id="customer-invoice-balance" value="' +
__e(accounting.formatNumber(invoice_balance, 2)) +
'"\n									';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\n									disabled="disabled"\n									';
 } ;
__p += '\n								/>\n								</div>\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-invoice-email" class="col-md-4 control-label">Invoice Email</label>\n							<div class="col-md-8">\n								<input class="form-control" type="text" value="' +
__e(invoice_email) +
'" name="invoice_email" id="customer-invoice-email" />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-non-taxable" class="col-md-4 control-label">Non-taxable</label>\n							<div class="col-md-4">\n								<input type="checkbox" name="taxable" id="customer-non-taxable" value="0" data-unchecked-value="1" style="margin-top: 10px;" ';
 if(taxable == "0"){ print('checked') };
__p += ' />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-food-minimum" class="col-md-4 control-label">F&B Minimum</label>\n							<div class="col-md-4">\n								<input type="checkbox" name="require_food_minimum" id="customer-food-minimum" value="1" data-unchecked-value="0" style="margin-top: 10px;" ';
 if(require_food_minimum == "1"){ print('checked') };
__p += ' />\n							</div>\n						</div>						\n						<div class="form-group">\n							<label for="customer-allow-loyalty" class="col-md-4 control-label">Allow Loyalty</label>\n							<div class="col-md-4">\n								<input type="checkbox" name="use_loyalty" id="customer-allow-loyalty" value="1" data-unchecked-value="0" style="margin-top: 10px;" ';
 if(use_loyalty == 1){ print('checked') };
__p += ' />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-loyalty-points" class="col-md-4 control-label">Loyalty Points</label>\n							<div class="col-md-5">\n								<input type="text" name="loyalty_points" class="form-control" id="customer-loyalty-points" value="' +
__e(accounting.formatNumber(loyalty_points)) +
'" />\n							</div>\n						</div>\n					</div>\n					<div class="col-md-6">\n						<!-- Customer Rainchecks -->\n						<div class="panel-group" id="customer-rainchecks">\n							<div class="panel panel-default">\n								<div class="panel-heading">\n									<h4 class="panel-title">\n										<a data-toggle="collapse" data-parent="#customer-rainchecks" href="#collapse-rainchecks">\n										Rainchecks\n										</a>\n										<span class="label label-info count">' +
__e(rainchecks.length) +
'</span>\n									</h4>\n								</div>\n								<div id="collapse-rainchecks" class="panel-collapse collapse">\n									<div class="panel-body">\n										<div class="panel-group" id="customer-rainchecks">\n											';
 if(rainchecks && rainchecks.length > 0){ ;
__p += '\n											';
_.each(rainchecks.models, function(raincheck){ ;
__p += '\n											<div class="row">\n												<div class="col-xs-2">#' +
__e(raincheck.get('raincheck_number')) +
'</div>\n												<div class="col-xs-8 text-muted">\n													Issued ' +
__e(moment(raincheck.get('date_issued'), 'YYYY-MM-DD HH:mm:ss').format('M/D/YY')) +
'\n													';
 if(raincheck.isExpired()){ ;
__p += '\n													<span class="label label-danger pull-right">\n														Expired ' +
__e(moment(raincheck.get('expiry_date')).format('M/D/YY')) +
'\n													</span>\n													';
 }else if(raincheck.hasExpiration()){ ;
__p += '\n													<span class="label label-success pull-right">\n														Expires ' +
__e(moment(raincheck.get('expiry_date')).format('M/D/YY')) +
'\n													</span>\n													';
 }else{ ;
__p += '\n													<span class="label label-success pull-right">No Expiration</span>\n													';
 } ;
__p += '\n												</div>\n												<div class="col-xs-2"><span class="pull-right">' +
__e(accounting.formatMoney(raincheck.get('total'))) +
'</span></div>\n											</div>\n											';
 }) ;
__p += '\n											';
 }else{ ;
__p += '\n											<div class="row">\n												<div class="col-md-12 text-muted">No Rainchecks</div>\n											</div>\n											';
 } ;
__p += '\n										</div>\n									</div>\n								</div>\n							</div>\n						</div>\n\n						<div class="panel-group" id="customer-groups"></div>\n						<div class="panel-group" id="customer-passes"></div>\n						<div class="panel-group" id="customer-minimum-charges"></div>\n						<div class="panel-group" id="customer-household"></div>\n\n						<!-- Login credentials -->\n						';
 if(App.data.user.get('acl').can('update', 'Customer/OnlineCredentials')){ ;
__p += '\n						<div class="panel-group" id="customer-login-credentials">\n							<div class="panel panel-default">\n								<div class="panel-heading">\n									<h4 class="panel-title">\n										<a data-toggle="collapse" data-parent="#customer-login-credentials" href="#collapse-login-credentials">\n										Login Credentials\n										</a>\n									</h4>\n								</div>\n								<div id="collapse-login-credentials" class="panel-collapse collapse in">\n									<div class="panel-body">\n										<div class="form-group">\n											<label for="customer-username" class="col-md-5 control-label">Username</label>\n											<div class="col-md-7">\n												<input type="text" name="username" id="customer-username" class="form-control" value="' +
__e(username) +
'" style="margin-top: 10px;" />\n											</div>\n										</div>\n										<div class="form-group">\n											<label for="customer-new-password" class="col-md-5 control-label">New Password</label>\n											<div class="col-md-7">\n												<input type="password" name="password" class="form-control" id="customer-new-password" value="" />\n											</div>\n										</div>\n										<div class="form-group">\n											<label for="customer-confirm-new-password" class="col-md-5 control-label">Confirm Password</label>\n											<div class="col-md-7">\n												<input type="password" name="confirm_password" class="form-control" id="customer-confirm-new-password" value="" />\n											</div>\n										</div>\n									</div>\n								</div>\n							</div>\n						</div>\n						';
 } ;
__p += '\n					</div>\n				</form>\n			</div>\n\n			<!-- Marketing -->\n			<div class="tab-pane" id="customer-marketing">\n				<form class="form-horizontal" role="form">\n					<div class="col-md-6">\n						<div class="form-group">\n							<label for="customer-email-unsubscribe" class="col-md-4 control-label">Email Unsubscribed</label>\n							<div class="col-md-8">\n								<input type="checkbox" name="opt_out_email" id="customer-email-unsubscribe" value="1" data-unchecked-value="0" style="margin-top: 10px;" ';
 if(opt_out_email == "1"){ print('checked') };
__p += ' />\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-text-unsubscribe" class="col-md-4 control-label">Text Subscribed</label>\n							<div class="col-md-8">\n								<input type="checkbox" name="opt_out_text" id="customer-text-unsubscribe" disabled="disabled" value="1" data-unchecked-value="0" style="margin-top: 10px;" ';
 if(texting_status == "subscribed"){ print('checked') };
__p += ' />\n								<button type="button" class="btn btn-primary" id="send_text_invite">Send Text Invite</button>\n							</div>\n						</div>\n						<div class="form-group">\n							<label for="customer-non-taxable" class="col-md-4 control-label">Email Suspended</label>\n							<div class="col-md-4">\n								<p class="form-control-static">\n									';
 if(unsubscribe_all == "0"){
										print('No');
									}else{
										print('Yes');
									} ;
__p += '\n								</p>\n							</div>\n						</div>\n					</div>\n				</form>\n			</div>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-primary save">Save</button>\n		<button type="button" data-dismiss="modal" class="btn btn-default pull-left">Cancel</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/customer_group.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<h5 class="name new_title">' +
__e(label) +
'</h5>\n<button class="btn btn-sm btn-default btn-destroy delete">\n	<i class="glyphicon glyphicon-trash"></i>\n</button>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/customer_groups.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="panel panel-default">\n	<div class="panel-heading">\n		<h4 class="panel-title">\n			<a data-toggle="collapse" data-parent="#customer-groups" href="#collapse-groups">\n			Groups\n			</a>\n			<span class="label label-info count">0</span>\n		</h4>\n	</div>\n	<div id="collapse-groups" class="panel-collapse collapse in">\n		<div class="panel-body">\n			<div class="form-group">\n				<label class="control-label">Add Customer to Existing Group</label>\n				<select name="new_group" id="customer-new-group" class="form-control">\n					<option value="0">- Add New Group -</option>\n					';
 _.each(App.data.course.get('groups').models, function(group){ ;
__p += '\n					<option value="' +
__e(group.get('group_id')) +
'">' +
__e(group.get('label')) +
'</option>\n					';
 }) ;
__p += '\n				</select>\n		</div>\n			<ul class="customer-data"></ul>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/customer_household.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="panel panel-default" style="overflow: visible">\n	<div class="panel-heading">\n		<h4 class="panel-title">\n			<a data-toggle="collapse" data-parent="#customer-household" href="#collapse-household">\n			Household Members\n			</a>\n			<span class="label label-info count">0</span>\n		</h4>\n	</div>\n	<div id="collapse-household" class="panel-collapse collapse in">\n		<div class="panel-body">\n				<div class="form-group">\n          <label class="control-label">Search and Attach Customers</label>\n          <input type="text" class="form-control" id="customer-household-member-search" value="">\n				</div>\n			<ul class="customer-data"></ul>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/customer_household_member.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<h5 class="name">' +
__e(first_name) +
' ' +
__e(last_name) +
'</h5>\n<button class="btn btn-default delete">\n	<i class="glyphicon glyphicon-trash"></i>\n</button>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/customer_minimum_charge.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if(date_added != ''){
	date_added = moment(date_added, 'YYYY-MM-DD').format('MM/DD/YYYY');
} ;
__p += '\n<td style="padding-left: 0px !important">\n	<strong>' +
__e(accounting.formatMoney(minimum_amount)) +
'</strong> - ' +
__e(name) +
'\n</td>\n<td style="padding-left: 0px !important; padding-right: 0px !important">\n	<input type="text" class="form-control start-date" style="padding: 4px 6px; height: 30px" placeholder="mm/dd/yyyy" value="' +
__e(date_added) +
'">\n</td>\n<td style="padding-right: 0px !important">\n	<button class="btn btn-default btn-sm delete pull-right" type="button">\n		<i class="glyphicon glyphicon-trash"></i>\n	</button>\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/customer_minimum_charges.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="panel panel-default" style="overflow: visible">\n	<div class="panel-heading">\n		<h4 class="panel-title">\n			<a data-toggle="collapse" data-parent="#customer-minimum-charges" href="#collapse-minimum-charges">\n				Minimum Charges\n			</a>\n			<span class="label label-info count">0</span>\n		</h4>\n	</div>\n	<div id="collapse-minimum-charges" class="panel-collapse collapse">\n		<div class="panel-body">\n			' +
((__t = (_.dropdown(App.data.minimum_charges.get_dropdown_data(), {'class': 'form-control', 'id': 'customer-minimum-charge-select'}))) == null ? '' : __t) +
'\n			<table class="table">\n				<thead>\n					<tr>\n						<th style="width: 60%">Charge</th>\n						<th style="width: 28%">Start Date</th>\n						<th style="width: 12%">&nbsp;</th>\n					</tr>\n				</thead>\n				<tbody></tbody>\n			</table>\n		</div>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/customer_pass.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<h5 class="name">\n	';
 if(multi_customer && customer){ ;
__p += '\n	' +
__e(customer.first_name+' '+customer.last_name) +
' - \n	';
 } ;
__p += '\n	' +
__e(name) +
'\n</h5>\n';
 if(start_date != ''){
	start_date = moment(start_date).format('M/D/YY');
}
if(end_date != ''){
	end_date = moment(end_date).format('M/D/YY');
} ;
__p += '\n<div style="color: #888">\n	' +
__e(start_date) +
' to ' +
__e(end_date) +
'\n	';
 if(!is_valid){ ;
__p += '\n	<label class="label label-danger">Not Valid</label>\n	';
 }else{ ;
__p += '\n	<label class="label label-success">Valid</label>\n	';
 } ;
__p += '\n	';
 if(customer_profile.person_id != customer.person_id){ ;
__p += '\n	<button style="margin-top: -10px" title="Disconnect pass from this customer" type="button" class="btn btn-default btn-sm pull-right disconnect">\n		<span class="fa fa-unlink"></span>\n	</button>\n	';
 } ;
__p += '\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/customer_passes.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="panel panel-default" style="overflow: visible">\n	<div class="panel-heading">\n		<h4 class="panel-title">\n			<a data-toggle="collapse" data-parent="#customer-passes" href="#collapse-passes">\n			Passes\n			</a>\n			<span class="label label-info count">0</span>\n		</h4>\n	</div>\n	<div id="collapse-passes" class="panel-collapse collapse in">\n		<div class="panel-body">\n			<div class="form-group">\n				<label class="control-label">Search and Attach Existing Group Passes</label>\n				<input type="text" class="form-control" id="customer-pass-search" value="">\n			</div>\n			<ul class="customer-data"></ul>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/photo_upload.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content customer-photo-upload">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">\n			Upload Photo\n		</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		';
 if(photo){ ;
__p += '\n		<div class="row">\n			<div class="col-md-8">\n				<div class="photo-container">\n					<img id="customer-photo" src="' +
((__t = (photo)) == null ? '' : __t) +
'" />\n				</div>\n			</div>\n			<div class="col-md-4">\n				<div class="row">\n					<div class="col-md-12 photo-edit">\n						<h4>Edit</h4>\n						<button class="btn btn-default crop"><i class="fa fa-crop"></i> Crop</button>\n					</div>\n					<div class="col-md-12 photo-crop" style="display: none">\n						<h4>Crop</h4>\n						<button class="btn btn-success save-crop"><i class="fa fa-check-circle"></i> Save</button>\n						<button class="btn btn-default cancel-crop"><i class="fa fa-times"></i> Cancel</button>\n					</div>\n				</div>\n				<div class="row photo-replace" style="margin-top: 15px">\n					<div class="col-md-12">\n						<h4>Replace</h4>\n						<input type="file" name="upload" id="file-input" style="display: none">\n						<button id="take-photo" class="btn btn-default"><i class="fa fa-camera"></i> Take Photo</button>\n						<button id="select-photo" class="btn btn-default"><i class="fa fa-cloud-upload"></i> Upload Photo</button>\n					</div>\n				</div>\n			</div>\n		</div>\n		';
 }else{ ;
__p += '\n		<div class="row">\n			<div class="col-md-12 upload-photo">\n				<h1>Add a photo</h1>\n				<div class="row">\n					<div class="col-md-6">\n						<button id="take-photo" class="btn btn-default btn-lg pull-right"><i class="fa fa-camera"></i> Take Photo</button>\n					</div>\n					<div class="col-md-6">\n						<input type="file" name="upload" id="file-input" style="display: none">\n						<button id="select-photo" class="btn btn-default btn-lg pull-left"><i class="fa fa-cloud-upload"></i> Upload Photo</button>\n					</div>\n				</div>\n			</div>\n		</div>\n		';
 } ;
__p += '\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-primary save">Save Photo</button>\n		<button type="button" data-dismiss="modal" class="btn btn-default pull-left">Cancel</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/photo_webcam.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-header">\n	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n	<h4 class="modal-title text-center">\n		Webcam Photo\n	</h4>\n</div>\n<div class="modal-body container-fluid">\n	<div class="row">\n		<div class="col-md-8 col-md-push-2">\n			<div class="webcam-instructions">\n				<i class="fa fa-exclamation-triangle text-warning text-center" style="font-size: 120px"></i>\n				<h2 class="text-center">Please "allow" access to<br />your webcam above</h2>\n			</div>\n			<div id="webcam-photo"></div>\n		</div>\n	</div>\n	<div class="row">\n		<div class="col-md-8 col-md-push-2">\n			<div class="row webcam-buttons">\n				<div class="col-md-12 webcam-take">\n					<button type="button" class="btn btn-lg btn-primary snap-photo btn-block" disabled>Take Photo</button>\n				</div>\n				<div class="col-md-6 webcam-approve">\n					<button type="button" class="btn btn-lg btn-default btn-block webcam-retry"><i class="fa fa-undo"></i> Retry</button>\n				</div>\n				<div class="col-md-6 webcam-approve">\n					<button type="button" class="btn btn-lg btn-success btn-block webcam-accept"><i class="fa fa-check-circle"></i> Accept</button>\n				</div>\n			</div>\n		</div>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/add_tip.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content fb-add-tip">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Add Tip</h4>\n	</div>\n	<div class="modal-body">\n		<form role="form" class="form-horizontal container-fluid">\n			<div class="row" style="margin-bottom: 15px;">\n				<div class="col-md-12">\n				<div class="btn-group">\n					<button class="btn btn-primary type ';
if(tip.type == 'cash'){ print('active') } ;
__p += '" data-type="cash" style="float: left;">Cash</button>\n					';
 if(customer_id){ ;
__p += '\n					<button class="btn btn-primary type ';
if(tip.type == 'member_balance'){ print('active') } ;
__p += '" data-type="member_balance" data-customer-id="' +
__e(customer_id) +
'">' +
__e(App.member_account_name +' Tip' ) +
'</button>\n					<button class="btn btn-primary type ';
if(tip.type == 'customer_balance'){ print('active') } ;
__p += '" data-type="customer_balance" data-customer-id="' +
__e(customer_id) +
'">' +
__e(App.customer_account_name +' Tip' ) +
'</button>\n					';
 } ;
__p += '\n\n					';
 if(payments.length > 0){
					_.each(payments, function(payment){
						var is_credit_card = ((payment.invoice_id != 0 && 
							payment.type.search("Gift Card") === -1 && 
							payment.type.search("Bank") === -1) || 
							payment.payment_type.search("Credit Card") !== -1
						);
						
						// If any payments are credit cards, display button to tip the card
						if(!is_credit_card){
							return true;
						} ;
__p += '\n						<button class="btn btn-primary type ';
if((tip.invoice_id != 0 && tip.invoice_id == payment.invoice_id) || tip.type == 'credit_card'){ print('active') } ;
__p += '" data-type="' +
__e(payment.type) +
'" data-invoice-id="' +
__e(payment.invoice_id) +
'">' +
__e(payment.payment_type) +
' Tip</button>\n					';
 }); } ;
__p += '	\n				\n					';
 if(payments.length > 0){
					_.each(payments, function(payment){
						var is_gift_card = ((payment.invoice_id == 0 &&
							payment.type.search("Credit Card") === -1 &&
							payment.type.search("Bank") === -1) && 
							payment.payment_type.search("Gift Card") !== -1
						);
						
						// If any payments are gift cards, display button to tip the card
						if(!is_gift_card){
							return true;
						} 
						;
__p += '\n						<button class="btn btn-primary type ';
if(tip.payment_type.substring(0, 9) == 'Gift Card'){ print('active') } ;
__p += '" data-type="' +
__e(payment.type) +
'" data-invoice-id="' +
__e(payment.invoice_id) +
'">' +
__e(payment.payment_type) +
' Tip</button>\n					';
 }); } ;
__p += '\n				</div>	\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-md-12">\n					<div class="form-group">\n						<label class="col-md-2">Amount</label>\n						<div class="col-md-3">\n							<input type="text" name="amount" id="tip_amount" class="form-control input-lg" value="' +
((__t = (accounting.formatMoney(tip.amount, ''))) == null ? '' : __t) +
'" />\n						</div>\n					</div>\n					<div class="form-group">\n						<input type="hidden" name="type" id="tip_type" value="' +
__e(tip.type) +
'" />\n						<input type="hidden" name="customer_id" id="tip_customer_id" value="' +
__e(tip.customer_id) +
'" />\n						<label class="col-md-2">Type</label>\n						<div class="col-md-3">\n							<p id="tip_payment_type" class="form-control-static" style="padding-top: 0px;">' +
__e(tip.payment_type) +
'</p>\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-md-2">Recipient</label>\n						<div class="col-md-6">\n							<input type="text" id="tip_employee_search" class="form-control input-lg" name="search" value="' +
__e(tip.tip_recipient_name) +
'" placeholder="Search employees..." autocomplete="off" />\n						</div>\n						<input type="hidden" name="tip_recipient" id="tip_recipient" value="' +
__e(tip.tip_recipient) +
'" />			\n					</div>\n				</div>\n				<input type="hidden" name="invoice_id" id="tip_invoice_id" value="' +
__e(tip.invoice_id) +
'" />\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-primary save">Save Tip</button>		\n	</div>\n</div>\n\n\n\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/cart_course_separator.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td class="course-separtor bg-warning" colspan="4">\n	<h4 style="margin: 0px; text-align: center">' +
__e(name) +
'</h4>\n</td>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/cart_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<td class="seat">' +
__e(seat) +
'</td>\n<td class="item">\n	<h4>\n		(' +
((__t = (accounting.toFixed(quantity,0))) == null ? '' : __t) +
')\n		' +
__e(name) +
'\n		';
 if(is_paid == 1){ ;
__p += '\n		<span style="float: right;">PAID</span>\n		';
 } ;
__p += '\n	</h4>\n	';
 if(!is_new){ var qty_buttons = 'display: none;'; }else{ var qty_buttons = ''; } ;
__p += '\n	<div class="quantity-buttons" style="overflow: hidden; ' +
((__t = (qty_buttons)) == null ? '' : __t) +
'">\n		<button class="btn btn-default sub">-</button>\n		<button class="btn btn-default add">+</button>\n	</div>\n	';
 if(modifiers.length > 0 || service_fees.length > 0){ ;
__p += '\n\n	<ul class="modifiers main">\n		<button class="btn btn-primary btn-sm modifier pull-left">Modifiers</button><br><br><br>\n	';
 if(service_fees.length > 0){ ;
__p += '\n		';
 _.each(service_fees.models, function(fee){ ;
__p += '\n		<li>\n			<span class="name">' +
__e(fee.get('name') ) +
': </span>\n			<span class="value">' +
__e(accounting.formatMoney(fee.get('subtotal')) ) +
'</span>\n		</li>\n		';
 }); ;
__p += '\n	';
 } ;
__p += '\n	';
 if(modifiers.length > 0){ ;
__p += '\n	';
 _.each(modifiers.nonDefault(), function(modifier){
	var selected_option = modifier.get('selected_option');
	
	if(!selected_option){
		selected_option = 'n/a';
	} ;
__p += '\n		<li>\n			<span class="name">' +
__e(modifier.get('name') ) +
': ' +
__e(_.capitalize(selected_option) ) +
'</span>\n			<span class="value">' +
__e(accounting.formatMoney(modifier.get('selected_price')) ) +
'</span>\n		</li>\n	';
 }); ;
__p += '\n	</ul>\n	';
 } ;
__p += '\n	';
 } ;
__p += '\n\n	<!-- SOUPS -->\n	';
 if(soups.length > 0){ ;
__p += '\n	<strong style="font-size: 12px; display: block; line-height: 12px; margin-top: 5px;">Soups</strong>\n	<ul class="modifiers">\n	';
 _.each(soups.models, function(side){ ;
__p += '\n		<li>\n			<span>' +
__e(side.get('name') ) +
'</span>\n			<span style="float: right;">' +
__e(accounting.formatMoney(side.get('price')) ) +
'</span>\n		</li>\n	';
 }); ;
__p += '\n	</ul>\n	';
 } ;
__p += '\n\n	<!-- SALADS -->\n	';
 if(salads.length > 0){ ;
__p += '\n	<strong style="font-size: 12px; display: block; line-height: 12px; margin-top: 5px;">Salads</strong>\n	<ul class="modifiers">\n	';
 _.each(salads.models, function(side){ ;
__p += '\n		<li>\n			<span>' +
__e(side.get('name') ) +
'</span>\n			<span style="float: right;">' +
__e(accounting.formatMoney(side.get('price')) ) +
'</span>\n		</li>\n	';
 }); ;
__p += '\n	</ul>\n	';
 } ;
__p += '\n\n	<!-- GENERAL SIDES -->\n	';
 if(sides.length > 0){ ;
__p += '\n	<strong style="font-size: 12px; display: block; line-height: 12px; margin-top: 5px;">Sides</strong>\n	<ul class="modifiers">\n	';
 _.each(sides.models, function(side){ ;
__p += '\n		<li>\n			<span>' +
__e(side.get('name') ) +
'</span>\n			<span style="float: right;">' +
__e(accounting.formatMoney(side.get('price')) ) +
'</span>\n		</li>\n	';
 }); ;
__p += '\n	</ul>\n	';
 } ;
__p += '\n	\n	';
 if(comp_total && comp_total > 0){ ;
__p += '\n	<span class="comp">Comp ' +
__e(accounting.formatMoney(-comp_total) ) +
'</span>\n	';
 } ;
__p += '\n</td>\n<td class="price">\n	' +
__e(accounting.formatMoney(total+sides_total - tax - sides_tax)) +
'\n</td>\n<td class="buttons">\n	<button class="btn btn-primary btn-sm edit pull-right">Edit</button>\n	<button class="btn btn-danger btn-sm void pull-right">Void</button>\n	<button class="btn btn-primary btn-sm message pull-right">Msg</button>\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/cart_items.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="table-container">\n	<table class="table">\n		<thead>\n			<tr>\n				<th class="seat">Seat</th>\n				<th class="item">Item</th>\n				<th class="price">Price</th>\n				<th class="total">&nbsp;</th>\n			</tr>\n		</thead>\n		<tbody class="fb-cart-items"></tbody>\n	</table>\n</div>\n<div class="scroll-container">\n	<div class="col-xs-6" style="padding: 0px;">\n		<button class="btn-block btn btn-default btn-lg up">&#x25B2;</button>\n	</div>\n	<div class="col-xs-6" style="padding: 0px;">\n		<button class="btn-block btn btn-default btn-lg down">&#x25BC;</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/cart_totals.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="col-xs-4">\n	' +
__e(num_items) +
' Items\n</div>\n<div class="col-xs-8">\n	<div class="row">\n		<div class="col-xs-6" style="text-align: right;">\n			Subtotal\n		</div>\n		<div class="col-xs-6" style="text-align: right;">\n			' +
__e(accounting.formatMoney(subtotal)) +
'\n		</div>\n	</div>\n	<div class="row">\n		<div class="col-xs-6" style="text-align: right;">\n			Tax\n		</div>\n		<div class="col-xs-6" style="text-align: right;">\n			' +
__e(accounting.formatMoney(tax)) +
'\n		</div>\n	</div>\n	<div class="row">\n		<div class="col-xs-6" style="text-align: right;">\n			<h4>Total</h4>\n		</div>\n		<div class="col-xs-6" style="text-align: right;">\n			<h4>' +
__e(accounting.formatMoney(total)) +
'</h4>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/change_issued.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<h2 style="font-size: 20px; margin: 15px; text-align: center; color: #444">Change Issued</h2>\n<h1 style="font-size: 28px; margin: 15px; text-align: center;">' +
__e(accounting.formatMoney(change_due)) +
'</h1>\n<a href="#" class="fnb_button close" style="display: block; float: none; margin: 60px auto 0; width: 100px;">Ok</a>\n\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/close_register.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content close-register-log">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">\n			Close Register\n			';
 if(multiCashDrawers){ ;
__p += '\n			(Drawer ' +
__e(drawer_number) +
')\n			';
 };
__p += '\n		</h4>\n	</div>\n	<div class="modal-body close-register-log">\n		<form class="form-horizontal" role="form">\n		<div class="container-fluid">\n			';
 if(blind_close == '0'){ ;
__p += '\n			<div class="row">\n				<div class="col-sm-12">\n					<p style="text-align: center; padding-bottom: 10px;">\n						You should have <strong>' +
__e(accounting.formatMoney(parseFloat(open_amount) + parseFloat(cash_sales_amount))) +
'</strong> \n						in the register\n					</p>\n				</div>\n			</div>\n			';
 } ;
__p += '\n			<div class="row">\n				<div class="col-md-5 col-md-offset-1">\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_pennies">Pennies</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_pennies" value="0" class="form-control input-lg change" data-amount="0.01" />\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_nickels">Nickels</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_nickels" value="0" class="form-control input-lg change" data-amount="0.05" />\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_dimes">Dimes</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_dimes" value="0" class="form-control input-lg change" data-amount="0.10" />\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_quarters">Quarters</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_quarters" value="0" class="form-control input-lg change" data-amount="0.25" />\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_ones">Ones</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_ones" value="0" class="form-control input-lg change" data-amount="1.00" />\n						</div>\n					</div>\n				</div>\n				<div class="col-md-5">\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_fives">Fives</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_fives" value="0" class="form-control input-lg change" data-amount="5.00" />\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_tens">Tens</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_tens" value="0" class="form-control input-lg change" data-amount="10.00" />\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_twenties">Twenties</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_twenties" value="0" class="form-control input-lg change" data-amount="20.00" />\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_fifties">Fifties</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_fifties" value="0" class="form-control input-lg change" data-amount="50.00" />\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_hundreds">Hundreds</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_hundreds" value="0" class="form-control input-lg change" data-amount="100.00" />\n						</div>\n					</div>\n				</div>\n			</div>\n			<hr style="border-color: #D0D0D0" />\n			<div class="row totals">\n				<div class="col-md-5 col-md-offset-1">\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_check_amount" style="padding-right: 5px">Total Checks</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_check_amount" value="' +
__e(accounting.formatMoney(close_check_amount, '')) +
'" class="form-control input-lg" style="padding-right: 5px;" />\n						</div>\n					</div>\n				</div>\n				<div class="col-md-5">\n					<div class="form-group">\n						<label class="col-sm-6 control-label" for="register_close_amount">Total Cash</label>\n						<div class="col-sm-6">\n							<input type="number" id="register_close_amount" value="' +
__e(accounting.formatMoney(close_amount, '')) +
'" class="form-control input-lg" style="padding-right: 5px;" />\n						</div>\n					</div>\n				</div>\n			</div>\n		</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-primary btn-lg close-register">Close Register</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/comp_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content comp-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Comp</h4>\n	</div>\n	<div class="modal-body">\n		<form role="form" class="form-horizontal">\n		<div class="container-fluid">\n			<div class="row">\n				<div class="col-md-12">			\n					<h1 class="comp-subtotal" style="text-align: center;">' +
__e(accounting.formatMoney(subtotal)) +
'</h1>\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-sm-2 control-label input-lg">Amount</label>\n				<div class="col-sm-3">\n					<div class="input-group">	\n						<input class="form-control input-lg amount" name="amount" value="' +
__e(accounting.formatMoney(comp.amount, '')) +
'" type="text" style="padding-right: 5px; padding-left: 10px;" />\n						<span class="input-group-addon">%</span>\n					</div>\n				</div>\n			</div>\n			<div class="row reasons">\n				<div class="col-sm-6">\n				';
 _.each(reasons, function(reason, index){ 
					if(index >= 5){ return true } ;
__p += '\n					<div class="row">\n						<div class="col-xs-12">\n							<button class="btn btn-default btn-lg btn-block reason';
 if(comp.description && reason == comp.description){ print(' active') } ;
__p += '">' +
__e(reason) +
'</button>\n						</div>\n					</div>\n				';
 }) ;
__p += '\n				</div>\n				<div class="col-sm-6">\n				';
 _.each(reasons, function(reason, index){ 
					if(index < 5){ return true } ;
__p += '\n					<div class="row">\n						<div class="col-xs-12">\n							<button class="btn btn-default btn-lg btn-block reason';
 if(comp.description && reason == comp.description){ print(' active') } ;
__p += '">' +
__e(reason) +
'</button>\n						</div>\n					</div>\n				';
 }) ;
__p += '\n				</div>				\n			</div>\n			<div class="row">\n				<div class="col-xs-12">\n					<hr style="border-top: 1px solid #E0E0E0;" />\n					<h1 class="comp-total" style="text-align: center">' +
__e(accounting.formatMoney(subtotal)) +
'</h1>\n				</div>\n			</div>\n		</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-danger btn-lg no-comp pull-left">No Comp</button>\n		<button class="btn btn-primary btn-lg save pull-right">Save</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/customer.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-md-10">\n	<div class="row">\n		<div class="col-xs-3">\n			<img class="photo" src="' +
__e(photo ) +
'" />		\n		</div>\n		<div class="col-xs-9">\n			<div class="row">\n				<div class="col-md-12">\n					<h3 class="name">' +
__e(first_name ) +
' ' +
__e(last_name ) +
'</h3>\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-md-12">' +
__e(phone_number) +
'</div>\n			</div>\n			<div class="row">\n				<div class="col-md-12">' +
__e(email) +
'</div>\n			</div>\n			<div class="row">\n				<div class="col-xs-12">\n					<strong>Acct #</strong> \n					<span class="balance">\n						';
 if(account_number){ 
						print(account_number);
					}else{ ;
__p += '\n						<span style="color: #AAA;">n/a</span>\n					';
 } ;
__p += '\n					</span>\n				</div>\n			</div>	\n			<div class="row">\n				<div class="col-xs-12">\n					<strong>' +
__e(App.data.course.get('member_balance_nickname')) +
'</strong> \n					<span class="balance pull-right ';
 if(member_account_balance < 0){ print('text-danger'); } ;
__p += '">' +
__e(accounting.formatMoney(member_account_balance) ) +
'</span>\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-xs-12">\n					<strong>' +
__e(App.data.course.get('customer_credit_nickname')) +
'</strong> \n					<span class="balance pull-right ';
 if(account_balance < 0){ print('text-danger'); } ;
__p += '">' +
__e(accounting.formatMoney(account_balance) ) +
'</span>\n				</div>\n			</div>\n			';

			if(!_.isNull(minimum_charges) && minimum_charges.length > 0){
				_.forEach(minimum_charges.models, function(minimum){ ;
__p += '\n					<div class="row">\n						<div class="col-xs-12">\n							<strong>' +
__e(minimum.get('name') ) +
'</strong>\n							<span class="balance pull-right ';
 if(minimum.get('totals') < minimum.get('minimum_amount')){ print('text-danger'); } ;
__p += '">\n								' +
__e(accounting.formatMoney(minimum.get('totals')) ) +
' / ' +
__e(accounting.formatMoney(minimum.get('minimum_amount')) ) +
'\n							</span>\n						</div>\n					</div>\n				';
 })
			}
			;
__p += '\n		</div>\n	</div>\n</div>\n<div class="col-md-2">\n	<div class="row">\n		<div class="col-xs-12">\n			<button class="btn btn-danger btn-lg pull-right remove">&times;</button>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/delete_receipt_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content delete-receipt-window">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n        <h4 class="modal-title">Delete Receipt</h4>\n    </div>\n    <div class="modal-body container-fluid">\n        <div class="row" id="delete-receipt-warning">\n            <h3>This Will Delete the Sale Belonging to the Receipt and Return All Payments</h3>\n            <p>Are you sure you want to continue? If yes, please provide a reason for deleting the sale:</p>\n            <input id="delete-receipt-reason" type="text" value="">\n        </div>\n    </div>\n    <div class="modal-footer">\n        <div class="row">\n            <div class="col-md-offset-3 col-md-3">\n                <button type="button" class="btn btn-danger btn-lg btn-block delete-sale" data-dismiss="modal">Yes</button>\n            </div>\n            <div class="col-md-3">\n                <button type="button" class="btn btn-primary btn-lg btn-block" data-dismiss="modal">No</button>\n            </div>\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/edit_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content edit-item">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">' +
__e(name) +
'</h4>\n		';
 if(use_course_firing == 1){ ;
__p += '\n			' +
((__t = (_.dropdown(meal_course_dropdown, {
				'name': 'meal_course_id', 
				'id':'meal_course_id', 
				'class':'form-control pull-left meal-course'
		}, " "+meal_course_id) )) == null ? '' : __t) +
'\n		';
 } ;
__p += '\n	</div>\n	<div class="modal-body" style="padding: 0px;">\n		<ul class="nav nav-tabs edit-item-tabs">\n			';
 
			if(typeof(default_section) == 'undefined'){
				default_section = 0;
			}

			_.each(sections, function(section, section_id){
			var incompleteClass = '';
			if(show_incomplete && section.incomplete){ incompleteClass = ' incomplete'; }
			;
__p += '\n			<li class="tab' +
__e(incompleteClass );
 if(section_id == default_section){ print(' active') }; ;
__p += '" style="width: ' +
__e(_.round(100 / _.size(sections), 2)) +
'%">\n				<a data-section-id="' +
__e(section_id) +
'" href="#item-edit-' +
__e(section_id) +
'">' +
__e(section.name) +
'</a>\n			</li>\n			';
 }); ;
__p += '\n		</ul>\n		<div class="tab-content">\n			';
 _.each(sections, function(section, section_id){ ;
__p += '\n			<div class="tab-pane ';
 if(section_id == default_section){ print('active') }; ;
__p += '" id="item-edit-' +
__e(section_id) +
'">\n				' +
__e(section.name) +
'\n			</div>\n			';
 }); ;
__p += '\n		</div>\n		<div class="container-fluid">\n			<div class="row">\n				<div class="col-md-12 form-group">\n					<label for="edit_item_comments">Comments</label>\n					<textarea style="height: 75px;" name="comments" id="edit_item_comments" class="form-control">' +
__e(comments) +
'</textarea>\n				</div>\n			</div>	\n		</div>\n	</div>\n	<div class="modal-footer container-fluid">\n		<div class="row">\n			<div class="col-md-2 col-md-push-2">\n				<label class="item-edit-field">Price</label>\n				<div class="form-group">\n					<input class="form-control input-lg" type="text" name="item[price]" id="edit_item_price" value="' +
((__t = (accounting.formatMoney(price,''))) == null ? '' : __t) +
'" />\n				</div>				\n			</div>\n			<div class="col-md-2 col-md-push-2 quantity">\n				<label class="item-edit-field">Qty</label>\n				<div class="input-group input-group-lg" style="margin-bottom: 15px;">\n					<span class="input-group-btn"><button class="btn btn-default sub" type="button">-</button></span>\n					<input class="form-control" type="text" name="item[quantity]" id="edit_item_quantity" style="padding: 12px 4px; text-align: center;" value="' +
((__t = (accounting.toFixed(quantity, 0))) == null ? '' : __t) +
'" />\n					<span class="input-group-btn"><button class="btn btn-default add" type="button">+</button></span>\n				</div>					\n			</div>\n			<div class="col-md-2 col-md-push-2">\n				<label class="item-edit-field">Disc (%)</label>\n				<div class="form-group">\n					<input class="form-control input-lg" type="text" name="item[discount]" id="edit_item_discount" value="' +
((__t = (accounting.formatNumber(discount))) == null ? '' : __t) +
'" />\n				</div>			\n			</div>\n			<div class="col-md-2 col-md-push-2 seat">\n				<label class="item-edit-field">Seat</label>\n				<div class="input-group input-group-lg" style="margin-bottom: 15px;">\n					<span class="input-group-btn"><button class="btn btn-default sub" type="button">-</button></span>\n					<input class="form-control" type="text" name="item[seat]" id="edit_item_seat" value="' +
((__t = (seat)) == null ? '' : __t) +
'" style="padding: 12px 4px; text-align: center;" />\n					<span class="input-group-btn"><button class="btn btn-default add" type="button">+</button></span>\n				</div>\n			</div>			\n			<div class="col-md-2 col-md-pull-8">\n				<button class="btn btn-danger btn-lg delete-item pull-left" style="margin-top: 30px;">Delete</button>\n			</div>			\n			<div class="col-md-2">\n				<button class="btn btn-primary btn-lg save-item pull-right" style="margin-top: 30px;">Save</button>\n			</div>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/employee_select_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content fb-change-employee">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Change Employee</h4>\n	</div>\n	<div class="modal-body container-fluid">\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/fire_course_button.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-md-6">\n	<button class="btn btn-default btn-lg btn-block fire-course">\n		<span class="label label-info pull-left">' +
__e(num_items) +
'</span>\n		' +
__e(name) +
'\n	</button>\n</div>\n<div class="col-md-6 fired-details">\n	';
 if(date_fired != null){ ;
__p += '\n	<span class="label label-warning fired"><span class="fa fa-fire"></span></span>\n	<span class="details">\n		By ' +
__e(fired_employee_name ) +
'\n		<br class="hidden-xs hidden-sm"> \n		' +
__e(moment(date_fired).format('M/D [at] h:mma')) +
'\n	</span>		\n	';
 } ;
__p += '\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/fire_course_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content fb-fire-course">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Fire Course</h4>\n	</div>\n	<div class="modal-body"></div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/foodbeverage.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="fb-cart" class="col-md-4">\n	<div class="inner-content" style="padding-top: 0px;">\n		<div class="row cart-buttons">\n			';
 
			var col_class = 'col-sm-4 col-md-4';
			if(use_course_firing == 1){ 
				col_class = 'col-sm-3 col-md-3';
			} ;
__p += '\n			<div class="' +
__e(col_class) +
'">\n				<button id="send_order" class="btn btn-primary fb-button btn-block">Send to Kitchen</button>\n			</div>\n			';
 if(use_course_firing == 1){ ;
__p += '\n			<div class="' +
__e(col_class) +
'">\n				<button id="fire_items" class="btn btn-primary fb-button btn-block">Fire</button>\n			</div>\n			';
 } ;
__p += '		\n			<div class="' +
__e(col_class) +
'">\n				<button id="pay_now" class="btn btn-primary fb-button btn-block">Pay Now</button>\n			</div>\n			<div class="' +
__e(col_class) +
'">\n				<button id="open_cash_drawer" class="btn btn-primary fb-button btn-block">Cash Drawer</button>\n			</div>\n		</div>\n		<div class="row">\n			<div class="panel panel-default">\n				<div class="panel-heading">\n					<button class="btn btn-xs btn-primary select-all">Select All</button>\n				</div>\n				<div class="panel-body cart-items" id="fb-cart-items"></div>\n				<div class="panel-body" id="fb-cart-totals"></div>\n				<div class="panel-heading" style="overflow: hidden; padding: 3px 5px;">\n					<h3 class="panel-title pull-left" style="line-height: 30px;"><button class="btn btn-sm btn-primary toggle-customers">Customers</button></h3>\n					<div id="guest_count" class="pull-right"></div>\n				</div>\n				<div class="panel-body customer-viewer">\n					<div class="row" style="margin-bottom: 15px;">\n						<div class="col-xs-7">\n							<input class="form-control input-lg" name="customer" id="table-customer-search" type="text" placeholder="Search customers..." />\n						</div>\n						<div class="col-xs-5">\n							<input class="form-control input-lg" name="table-name" id="table-name" type="text" placeholder="Table name" />\n						</div>\n					</div>\n					<div class="row" id="fb-customers"></div>\n				</div>								\n			</div>\n		</div>\n	</div>\n</div>\n\n<div id="items" class="col-md-8">\n	<div class="inner-content" style="padding-top: 0px;">\n		<div class="row">\n			<div class="col-md-2"><!-- Spacer --></div>\n			<div class="col-md-10">\n				<div class="row" id="fb-controls">\n					<div class="col-md-2">\n						<button id="select_table" class="btn btn-success fb-button btn-block">Select Table</button>\n					</div>\n					<div class="col-md-2">\n						<button id="merge_table" class="btn btn-success fb-button btn-block">Merge Table</button>\n					</div>\n					<div class="col-md-2">\n						<button id="close_register" class="btn btn-success fb-button btn-block">Close Register</button>\n					</div>\n					<div class="col-md-2">\n						<button id="logout" class="btn btn-success fb-button btn-block">Logout</button>	\n					</div>\n					<div class="col-md-2">\n						<button id="recent_transactions" class="btn btn-success fb-button btn-block">Recent Trans.</button>\n					</div>\n					<div class="col-md-2">\n						<button id="cancel_sale" class="btn btn-danger fb-button btn-block">Cancel Sale</button>\n					</div>\n				</div>\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-md-2" id="fb-categories"></div>\n			<div class="col-md-10" id="fb-items"></div>\n		</div>\n		<div class="row">\n			<div class="col-md-12">\n				<button class="btn btn-link pull-right edit-button-order" id="edit_button_order" style="color: white;">Edit Button Order</button>\n			</div>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/guest_count_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content fb-fire-course">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Guest Count</h4>\n	</div>\n	<div class="modal-body fluid-container">\n		<div class="row">\n			<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-sm-push-5" style="padding: 0px;">\n				<input type="text" name="guest_count" class="form-control guest-count input-lg" value="">\n			</div>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<div class="row">\n			<button class="btn btn-primary col-xs-12 col-sm-2 col-sm-push-5 save">Save</button>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/item_modifiers.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if(title){ ;
__p += '\n<h3>' +
((__t = (title )) == null ? '' : __t) +
'</h3>\n';
 } ;
__p += '\n\n<div class="container-fluid modifiers-pane">\n';
 if(modifiers){
var count = 0;
_.each(modifiers.models, function(modifier){
	
	if(category_id != null && modifier.get('category_id') != category_id){
		return true;
	}
	var options = modifier.get('options');
	var selected_option = modifier.get('selected_option')?modifier.get('selected_option'):modifier.get('default');
	var selected_price = modifier.get('selected_price');
	;
__p += '\n	<div class="row modifier">\n		<div class="col-md-3 col-xs-5">\n			<h4>' +
__e(modifier.get('name') ) +
'</h4>\n		</div>\n		<div class="col-md-2 col-xs-7 modifier-price">\n			<h4>$<span class="value">' +
__e(accounting.formatMoney(selected_price, '')) +
'</span></h4>\n		</div>\n		<div class="col-md-7 col-xs-12 options">\n			<div class="btn-group btn-group-lg">\n				';
 _.each(modifier.get('options'), function(option){
					if(
						selected_option && (
						(typeof(selected_option) == 'object' && _.indexOf(selected_option, option.label) > -1) ||
						(typeof(selected_option) != 'object' && selected_option.toLowerCase() == (option.label?option.label.toLowerCase():'something went wrong here obviously...')))
					){
					var btnClass = ' active';
				}else{
					var btnClass = '';
				} ;
__p += '\n				<button data-modifier-id="' +
((__t = ( modifier.get('modifier_id') )) == null ? '' : __t) +
'" data-price="' +
__e(accounting.toFixed(option.price, 2) ) +
'" class="btn btn-primary modifier-option' +
((__t = ( btnClass )) == null ? '' : __t) +
'" data-value="' +
((__t = ( option.label )) == null ? '' : __t) +
'">\n					' +
__e(_.capitalize(option.label)) +
'\n				</button>\n				';
 }); ;
__p += '\n			</div>\n		</div>\n	</div>\n';
 count++; }); ;
__p += '\n';
 }else{ ;
__p += '\n<div class="row">\n	<div class="col-md-12">No modifiers avaialable</div>\n</div>\n';
 } ;
__p += '\n</div>\n';
 if(count > 5){ ;
__p += '\n<div class="row scroll-buttons" style="margin-top: 5px;">\n	<div class="col-md-4">\n		<div class="btn-group btn-group-lg btn-group-justified">\n			<div class="btn-group">\n				<button class="btn btn-default btn-lg scroll-up">&#x25B2;</button>\n			</div>\n			<div class="btn-group">\n				<button class="btn btn-default btn-lg scroll-down">&#x25BC;</button>\n			</div>\n		</div>\n	</div>\n</div>\n';
 } ;
__p += '\n';
 if(title){ ;
__p += '\n<button class="btn btn-default btn-lg back">&lt; Back</button>\n';
 } ;
__p += '\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/item_sides.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


if(sides.length > number_sides){
	number_sides = sides.length;
}

for(var position = 1; position < number_sides + 1; position++){

var extraText = '';
if(position > number_sides){
	extraText = '(Extra)';
}

var selectedSide = false;
if(sides.length > 0 && sides.get(position)){
	var selectedSide = sides.get(position);
}

var showModifierButton = false;
if(selectedSide && selectedSide.get('modifiers').length > 0){
	var showModifierButton = true;
}
;
__p += '\n<div class="row side">\n	<div class="col-md-12">\n		<div class="row">\n			<div class="col-md-12">\n				<h3 class="pull-left">Side #' +
__e(position ) +
' ' +
__e(extraText) +
'</h3>\n				<button class="btn btn-default btn-lg side_modifiers pull-right" style="';
if(!showModifierButton){ print('display: none !important'); };
__p += '" data-position="' +
__e(position) +
'">Modifiers</a>\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-md-12">\n			';
 _.each(App.data.food_bev_sides.models, function(side){
				if(side.get('category') == 'Soups' || side.get('category') == 'Salads'){ return true; }
				if(hidden_sides && hidden_sides[side.get('item_id')] == 1){ return true; }

				var selected = '';
				if(selectedSide && selectedSide.get('item_id') == side.get('item_id')){
					selected = ' active';
				} ;
__p += '\n				<button class="btn btn-primary btn-lg side ' +
__e(selected) +
'" href="#" data-type="side" data-position="' +
__e(position) +
'" data-side-id="' +
__e(side.get('item_id')) +
'">\n					' +
__e(_.capitalize(side.get('name'))) +
'\n				</button>\n			';
 }); ;
__p += '	\n			</div>\n		</div>\n	</div>\n</div>\n';
 } ;
__p += '\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/item_soup_salad.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var position = 1;

if(soups.length > number_soups){
	number_soups = soups.length;
}
if(number_soups > 0){ ;
__p += '\n<div class="side">\n	<h1>Soup</h1>\n	<ul style="overflow: hidden; display: block;">\n	';
 _.each(App.sides.models, function(soup){
	if(soup.get('category') != 'Soups'){ return true; }
	var selected = '';
	if(soups.length > 0 && soups.get(position) && soups.get(position).get('item_id') == soup.get('item_id')){
		selected = ' selected';
	} ;
__p += '\n		<li>\n			<a class="fnb_button side' +
__e(selected) +
'" href="#" data-type="soup" data-position="' +
__e(position) +
'" data-side-id="' +
__e(soup.get('item_id')) +
'">\n				' +
__e(_.capitalize(soup.get('name'))) +
'\n			</a>\n		</li>\n	';
 }); ;
__p += '\n	</ul>\n	';
 var noSide = '';
	if(soups.length > 0 && soups.get(position) && soups.get(position).get('item_id') == 0){
		noSide = ' selected';
	} ;
__p += '\n	<a style="margin-top: 80px; clear: both;" data-type="soup" data-position="' +
__e(position) +
'" class="no_side fnb_button' +
__e(noSide) +
'" href="#">No Soup</a>\n</div>\n';
 } ;
__p += '\n\n';

if(salads.length > number_salads){
	number_salads = salads.length;
}
if(number_salads > 0){ ;
__p += '\n<div class="side">\n	<h1>Salad</h1>\n	<ul style="overflow: hidden; display: block;">\n	';
 _.each(App.sides.models, function(salad){
	if(salad.get('category') != 'Salads'){ return true; }
	var selected = '';
	if(salads.length > 0 && salads.get(position) && salads.get(position).get('item_id') == salad.get('item_id')){
		selected = ' selected';
	} ;
__p += '\n		<li>\n			<a class="fnb_button side' +
__e(selected) +
'" href="#" data-type="salad" data-position="' +
__e(position) +
'" data-side-id="' +
__e(salad.get('item_id')) +
'">\n				' +
__e(_.capitalize(salad.get('name'))) +
'\n			</a>\n		</li>\n	';
 }); ;
__p += '\n	</ul>\n	';
 var noSide = '';
	if(salads.length > 0 && salads.get(position) && salads.get(position).get('item_id') == 0){
		noSide = ' selected';
	} ;
__p += '\n	<a style="margin-top: 80px; clear: both;" data-type="salad" data-position="' +
__e(position) +
'" class="no_side fnb_button' +
__e(noSide) +
'" href="#">No Salad</a>\n</div>\n';
 } ;
__p += '\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/meal_course_select.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content fb-course-select">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Select Meal Course</h4>\n	</div>\n	<div class="modal-body"></div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/merge_table_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content table-layout">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Merge Table <strong>#' +
__e(table_num) +
'</strong></h4>\n	</div>\n	<div class="modal-body" style="padding: 0px; position: relative;">\n		<p class="text-center" style="padding-top: 10px;">\n			Select a table to merge into. <br><em>All items, receipts, and customers will be moved \n			from table <strong>' +
__e(table_num) +
'</strong> into the table selected.</em>\n		</p>\n		<div class="layout-tabs" style="border-top: 1px solid #E0E0E0"></div>\n		<div class="layouts merge-table"></div>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/open_register.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content open-register-log">\n	<div class="modal-header">\n		';
 if(closable){ ;
__p += '\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		';
 } ;
__p += '\n		<h4 class="modal-title" style="text-align: center">\n			Open Register\n			';
 if(multiCashDrawers == 1 && drawer_number != 0){ ;
__p += '\n			(Drawer ' +
__e(drawer_number) +
')\n			';
 };
__p += '\n		</h4>\n	</div>\n	<div class="modal-body">\n		<div class="container-fluid">\n			<input type="hidden" name="drawer_number" id="cash-drawer-number" value="' +
__e(drawer_number) +
'">\n			';
 if(shift_start == "0000-00-00 00:00:00"){ ;
__p += '\n			<div class="row">\n				<div class="col-xs-12">\n					<p>Enter the starting amount for your register</p>\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="register_open_amount">Amount</label>\n				<input type="number" id="register_open_amount" value="' +
__e(accounting.formatMoney(open_amount, '')) +
'" class="form-control input-lg" />\n			</div>\n			';
 }else{ ;
__p += '\n			<div class="row">\n				<div class="col-xs-12">\n					<p style=\'text-align: center\'>On <strong>' +
__e(moment(shift_start).format('MM/DD/YYYY [at] hh:mma')) +
'</strong> the register \n					was opened with <strong>' +
__e(accounting.formatMoney(open_amount)) +
'</strong></p>\n				</div>\n			</div>\n			';
 } ;
__p += '\n		</div>\n	</div>\n	<div class="modal-footer">\n	';
 if(shift_start == "0000-00-00 00:00:00"){ ;
__p += '\n		<button class="btn btn-default btn-lg pull-left skip">Skip</button>\n		<button class="btn btn-primary btn-lg pull-right save">Open</button>\n	';
 }else{ ;
__p += '\n		<button class="btn btn-primary pull-left" id="reopen_log">Close & Re-open</button>\n		<button class="btn btn-primary pull-right" id="continue_log">Continue Log</button>		\n	';
 } ;
__p += '\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/page_header_info.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<h3 style="margin: 8px 0px 0px 0px; padding: 0px; display: block; width: auto; text-align: center; color: white;">\n	';
 if(terminal){ ;
__p +=
__e(terminal.get('label'));
 };
__p += ' \n	';
 if(table_num != null){;
__p += '\n	#' +
__e(table_num) +
'\n	<span class="table-wait">- Seated ' +
__e(moment(sale_time, 'YYYY-MM-DD HH:mm:ss').fromNow()) +
'</span>\n	';
 } ;
__p += '\n</h3>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/payment.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<a href="" class="delete text-danger">&times;</a>\n<span class="desc">' +
__e(type) +
'</span>\n<span class="amount">\n    ';
 if(tran_type != 'PreAuth'){ ;
__p += '\n	' +
__e(accounting.formatMoney(amount)) +
'\n	';
 }else{ ;
__p += '\n	<i class="glyphicon glyphicon-check"></i>\n	Saved\n	';
 } ;
__p += '\n</span>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/payment_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content payments-window fb-payments-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Payments</h4>\n	</div>\n	<div class="modal-body">\n		<div class="container-fluid">\n			<div class="row">\n				<div class="col-md-6 col-md-push-6 payments">			\n					<div class="row">\n						<div class="form-group col-md-10 col-md-offset-2 payment-amount">\n							<label class="amount">Amount</label>\n							<div class="input-group amount">\n								<div class="input-group-addon">$</div>\n								<input type="text" name="amount" id="payment_amount" class="form-control amount" value="' +
__e(accounting.formatMoney(amount_due>0?amount_due:0, '')) +
'" autocomplete="off"/>\n							</div>\n						</div>\n					</div>\n					<div class="row">\n						<ul class="col-md-12 payments" id="window-payments"></ul>\n						<div class="form-group col-md-12 total-due">\n							<hr />\n							<label>Total Due</label>\n							<h3 class="total">' +
__e(accounting.formatMoney(amount_due)) +
'</h3>\n						</div>	\n					</div>				\n				</div>				\n				<div class="col-md-6 col-md-pull-6" id="payment-types">\n					<div class="row" id="saved-credit-cards"></div>\n					<div class="row">\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 pay" id="payment_cash">Cash</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12" id="payment_check" href="">Check</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12" id="payment_credit_card" href="">Credit Card</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12" id="payment_giftcard" href="">Gift Card</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 pay ';
 if(!enable_member_balance){ print('disabled') } ;
__p += '" id="payment_member_account" href="">' +
__e(member_account_label) +
'</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 pay ';
 if(!enable_customer_balance){ print('disabled') } ;
__p += '" id="payment_customer_account" href="">' +
__e(customer_account_label) +
'</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 ';
 if(!enable_loyalty){ print('disabled') } ;
__p += '" id="payment_loyalty" href="">Loyalty Points</button>\n	                    ';
 for (var i = 0; i < custom_payments.length; i++) { ;
__p += '\n	                    <button class="btn btn-default btn-lg col-sm-6 col-xs-12 custom_payment pay" id="' +
__e(custom_payments.models[i].get('customPaymentType')) +
'" href="">' +
__e(custom_payments.models[i].get('label')) +
'</button>\n	                    ';
 } ;
__p += '\n					</div>\n				</div>		\n			</div>\n		</div>\n		<div class="row" style="margin-top: 15px">\n			<div class="col-md-12">\n				<label for="refund-comment" class="control-label">Customer Note</label>\n				<textarea name="customer_note" class="form-control" id="customer-note">' +
__e(customer_note) +
'</textarea>\n				<span class="help-block">This note will be displayed on the sale receipt and in sale reports</span>\n			</div>\n		</div>\n	</div>\n    <div class="modal-footer">\n        <button type="button" class="btn btn-default hidden-xs pull-left" id="cancel-credit-card-payment" style="display:none; z-index:1000000; position:relative;">Cancel</button>\n    </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/receipt.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if(status == 'complete'){ var paidClass = ' paid'; var btnClass = ' disabled'; } else { var paidClass = ''; var btnClass = ''; } ;
__p += '\n<div class="row receipt' +
((__t = (paidClass)) == null ? '' : __t) +
'">\n	<div class="col-xs-12 title-header">\n		<button class="btn btn-danger btn-lg delete-receipt pull-left">&times;</button>\n		<h1 class="receipt-number">#' +
__e(receipt_id) +
'</h1>\n		';
 if(customer){ ;
__p += '\n			';
 if(status == 'complete'){ ;
__p += '\n			<span class="customer">' +
__e(customer.get('first_name') ) +
' ' +
__e(customer.get('last_name') ) +
'</span>\n			';
 }else{ ;
__p += '\n			<button class="btn btn-primary customer pull-right">' +
__e(customer.get('first_name') ) +
' ' +
__e(customer.get('last_name') ) +
'</button>\n			';
 } ;
__p += '\n		';
 }else{ ;
__p += '\n			';
 if(status != 'complete'){ ;
__p += '\n			<button class="btn btn-primary customer pull-right">- No Customer -</button>\n			';
 } ;
__p += '\n		';
 } ;
__p += '\n		<button class="btn btn-primary start-tab pull-right">Start Tab</button>\n	</div>\n	<div class="col-xs-12 title">\n		<form class="form-horizontal row">\n			<div class="col-xs-8">\n				<div class="form-group">\n					<label class="col-xs-4" style="padding-right: 5px; line-height: 35px;">Tip</label>\n					<div class="col-xs-8" style="padding-left: 0px; padding-right: 0px;">\n					';
 if(status == 'complete'){ ;
__p += '\n						<p class="form-control-static">' +
__e(accounting.formatMoney(auto_gratuity, '')) +
'%</p>\n					';
 }else{ ;
__p += '\n						<div class="input-group">\n							<input type="text" class="col-xs-10 form-control receipt-tip" value="' +
__e(accounting.formatMoney(auto_gratuity, '')) +
'"/>\n							<div class="input-group-btn tip-type">\n								<button type="button" class="btn btn-default ';
 if(auto_gratuity_type == 'percentage'){ print('active') } ;
__p += '" data-type="percentage">%</button>\n								<button type="button" class="btn btn-default ';
 if(auto_gratuity_type == 'dollar'){ print('active') } ;
__p += '" data-type="dollar">$</button>\n							</div>\n						</div>\n					';
 } ;
__p += '\n					</div>\n				</div>\n			</div>\n			<div class="col-xs-4">\n				<div class="row">\n					<div class="col-xs-12">\n						<span class="is-paid">PAID</span>\n                        ';
 if(!hide_taxable) { ;
__p += '\n                            ';
 if(taxable == 1){ ;
__p += '\n                            <button class="btn btn-default tax pull-right" data-tax="1">&#10004; Tax</button>\n                            ';
 }else{ ;
__p += '\n                            <button class="btn btn-default tax pull-right" data-tax="0">&#x25a2; Tax</button>\n                            ';
 } ;
__p += '\n                        ';
 } ;
__p += '\n					</div>\n				</div>\n			</div>		\n		</form>\n	</div>\n	<div class="col-xs-12 footer">\n		<div class="row">\n			<div class="col-xs-6">\n				<div class="btn-group-justified">\n					<div class="btn-group">\n						<button class="btn btn-default up">&#x25B2;</button>\n					</div>\n					<div class="btn-group">\n						<button class="btn btn-default down">&#x25BC;</button>\n					</div>\n				</div>\n			</div>\n			<div class="col-xs-6">\n				<h4 class="total">\n					<span class="text-muted">Total</span>\n					' +
__e(accounting.formatMoney(total)) +
'\n				</h4>\n				<h4 class="payments">\n					<span class="text-muted">Paid</span>\n					' +
__e(accounting.formatMoney(typeof total_paid !== 'undefined'?total_paid:0)) +
'\n				</h4>\n				<h4 class="due">\n					<span class="text-muted">Due</span>\n					' +
__e(accounting.formatMoney(typeof total_due !== 'undefined'?total_due:total)) +
'\n				</h4>\n			</div>\n		</div>\n	</div>	\n</div>\n<div class="row receipt-actions">\n	';
 if(items.length == 0){ var btnClass = ' disabled'; } ;
__p += '\n	<div class="col-xs-6" style="padding-right: 5px;">\n		<button class="btn btn-primary btn-lg btn-block print' +
((__t = (btnClass)) == null ? '' : __t) +
'">Print</button>\n	</div>\n	<div class="col-xs-6" style="padding-left: 5px;">\n		<button class="btn btn-primary btn-lg btn-block pay' +
((__t = (btnClass)) == null ? '' : __t) +
'">Pay</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/receipt_customer.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-md-8">\n	<div class="row">\n		<div class="col-xs-3">\n			<img class="photo" src="' +
__e(photo ) +
'" />		\n		</div>\n		<div class="col-xs-9">\n			<div class="row">\n				<div class="col-md-12">\n					<h3 class="name">' +
__e(first_name ) +
' ' +
__e(last_name ) +
'</h3>\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-md-12">' +
__e(phone_number) +
'</div>\n			</div>\n			<div class="row">\n				<div class="col-md-12">' +
__e(email) +
'</div>\n			</div>						\n		</div>\n	</div>\n</div>\n<div class="col-md-4">\n	<div class="row">\n		<strong>Acct #</strong> \n		<span class="balance">\n			';
 if(account_number){ 
			print(account_number);
		}else{ ;
__p += '\n			<span style="color: #AAA;">n/a</span>\n		';
 } ;
__p += '\n		</span>\n	</div>	\n	<div class="row">\n		<div class="col-md-12">\n			<strong>' +
__e(App.data.course.get('member_balance_nickname')) +
'</strong> \n			<span class="balance pull-right ';
 if(member_account_balance < 0){ print('text-danger'); } ;
__p += '">' +
__e(accounting.formatMoney(member_account_balance) ) +
'</span>\n		</div>\n	</div>\n	<div class="row">\n		<div class="col-md-12">\n			<strong>' +
__e(App.data.course.get('customer_credit_nickname')) +
'</strong> \n			<span class="balance pull-right ';
 if(account_balance < 0){ print('text-danger'); } ;
__p += '">' +
__e(accounting.formatMoney(account_balance) ) +
'</span>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/receipt_customer_search.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content receipt-customer-search">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Search Customers</h4>\n	</div>\n	<div class="modal-body">\n		<div class="container-fluid">\n			<div class="row">\n				<div class="col-md-6">\n					<input class="form-control input-lg search" type=\'text\' name="search" value="" placeholder="Search customers" />\n				</div>\n				<div class="col-md-2 col-md-offset-4">\n					<button class="btn btn-primary no-customer pull-right">- NO CUSTOMER -</button>\n				</div>\n			</div>\n			<div class="row customer-list"></div>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/receipt_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<span class="seat">SEAT<br />' +
__e(seat) +
'</span>\n';
 if(comp_total > 0){ ;
__p += '\n<span class="item-name">(' +
__e(accounting.formatNumber(quantity)) +
') ' +
__e(_.shorten(name, 9)) +
'</span>\n<span class="price"><strong>COMP</strong> ' +
__e(accounting.formatMoney(split_subtotal + split_sides_subtotal + split_service_fees_subtotal)) +
'</span>\n';
 }else{ ;
__p += '\n<span class="item-name">(' +
__e(accounting.formatNumber(quantity)) +
') ' +
__e(_.shorten(name, 16)) +
'</span>\n<span class="price">' +
__e(accounting.formatMoney(split_subtotal + (typeof split_sides_subtotal !== 'undefined'?split_sides_subtotal:0) + (typeof split_service_fees_subtotal !== 'undefined'?split_service_fees_subtotal:0))) +
'</span>\n';
 } ;
__p += '\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/receipts.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content item-sub-category">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Pay Now</h4>\n	</div>\n	<div class="modal-body" style="background-color: #F0F0F0; padding: 0px;">\n		<div class="container-fluid receipt-window-container">\n			<div class="row receipt-nav';
if(admin_auth){ print(' admin') };
__p += '">\n				<div class="col-md-2 hidden-sm hidden-xs">\n					<button class="btn btn-default btn-block left"><</button>\n				</div>\n				<div class="col-md-8">\n					<div class="row">\n						<div class="admin-buttons col-xs-12">\n							<div class="row">\n								<div class="col-sm-3">\n									<div class="btn-group btn-group-justified mode" data-mode="comp">\n										<div class="btn-group">\n											<button class="btn btn-danger comp active" data-value="comp">Comp</button>\n										</div>\n										<div class="btn-group">\n											<button class="btn btn-danger edit" data-value="edit">Edit</button>\n										</div>\n									</div>	\n								</div>\n								<div class="col-sm-3">\n									<button class="btn btn-danger btn-block admin-exit">Exit Admin</button>\n								</div>\n							</div>\n						</div>\n						<div class="standard-buttons col-xs-12">\n							<div class="row">\n								<div class="col-sm-3">\n									<button class="btn btn-primary btn-block new-receipt">+ New Receipt</button>\n								</div>\n								<div class="col-sm-3">\n									<div class="btn-group btn-group-justified mode" data-mode="move">\n										<div class="btn-group">\n											<button class="btn btn-primary btn-block move active" data-value="move">Move</button>\n										</div>\n										<div class="btn-group">\n											<button class="btn btn-primary btn-block split" data-value="split">Split</button>\n										</div>\n									</div>\n								</div>\n								<div class="col-sm-3">\n									<button class="btn btn-primary btn-block remove-items">Remove Items</button>		\n								</div>\n								<div class="col-sm-3">\n									<button class="btn btn-primary btn-block admin-override">Admin Override</button>\n								</div>\n							</div>\n						</div>\n					</div>\n				</div>\n				<div class="col-md-2 hidden-sm hidden-xs">\n					<button class="btn btn-default btn-block right">></button>\n				</div>\n			</div>\n			<div class="row receipts">\n				<!-- Receipts will be inserted here -->\n			</div>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/recent_transaction.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 var identifier = sale_id;
if(number != null){
	identifier = number;
} ;
__p += '\n<button class="btn btn-primary view_receipt">View</button>\n<button class="btn btn-primary print_receipt">Print</button>\n<button class="btn btn-primary email-receipt">Email</button>\n<div class="info" style="width: 250px; float: left;">\n	<h3>Sale #' +
__e(identifier) +
'</h3>\n	<h5>Total: ' +
__e(accounting.formatMoney(total)) +
'</h5>\n	';
 if(tips.length > 0){ ;
__p += '\n	<strong>Tips</strong>\n	<ul>\n	';
 _.each(tips.models, function(payment){ ;
__p += '\n		<li>' +
__e(payment.get('description')) +
': ' +
__e(accounting.formatMoney(payment.get('amount'))) +
'</li>\n	';
 }) ;
__p += '\n	</ul>\n	';
 } ;
__p += '\n</div>\n<div class="info" style="width: 250px; float: left;">\n	';
 if(employee){ ;
__p += '<div><span>Server:</span> <strong>' +
__e(employee.get('first_name')+' '+employee.get('last_name')) +
'</strong></div>';
 } ;
__p += '\n	';
 if(customer){ ;
__p += '<div><span>Customer:</span> <strong>' +
__e(customer.get('first_name')+' '+customer.get('last_name')) +
'</strong></div>';
 } ;
__p += '\n	<div><span>Table #</span> <strong>' +
__e(table_id) +
'</strong></div>\n</div>\n<button class="btn btn-success add_tip">Add Tip</button>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/recent_transaction_details.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content fb-receipt-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Sale #' +
__e(sale_id) +
'</h4>\n	</div>\n	<div id="sale-receipt" class="modal-body container-fluid">\n		<div class="row">\n			<div class="col-md-12 course-header">\n				<strong>' +
__e(App.data.course.get('name')) +
'</strong><br />\n				' +
__e(App.data.course.get('address')) +
'<br />\n				' +
__e(App.data.course.get('phone')) +
'<br />\n			</div>\n		</div>		\n		<div class="row">\n			<div class="col-md-12 sale-details">\n				<strong>Sale ID:</strong> POS ' +
__e(sale_id) +
'<br />\n				<strong>Date:</strong> ' +
__e(moment(sale_time).format('MM/DD/YYYY h:mma')) +
'<br />\n				<strong>Employee:</strong> ' +
__e(employee_name) +
'\n				';
 if(customer_name){ ;
__p += '\n				<br /><strong>Customer:</strong> ' +
__e(customer_name) +
'\n				';
 } ;
__p += '\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-md-12">\n				<div class="table-responsive items">\n					<table class="table table-condensed items">\n						<thead>\n							<tr>\n								<th>Item</th>\n								<th>Price</th>\n								<th>Qty</th>\n								<th>Discount</th>\n								<th class="total">Total</th>\n							</tr>\n						</thead>\n						<tbody>\n						';
 _.each(items, function(item){ ;
__p += '\n							<tr>\n								<td>' +
__e(item.name) +
'</td>\n								<td>' +
__e(accounting.formatMoney(item.price)) +
'</td>\n								<td>' +
__e(item.quantity) +
'</td>\n								<td>' +
__e(accounting.formatNumber(item.discount, 2)+'%') +
'</td>\n								<td class="total">' +
__e(accounting.formatMoney(item.subtotal)) +
'</td>\n							</tr>\n						';
 }); ;
__p += '	\n						</tbody>\n					</table>\n				</div>\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-sm-offset-5 col-sm-7">\n				<table class="table table-condensed totals">\n					<tr>\n						<th>Subtotal</th>\n						<td class="amount">' +
__e(accounting.formatMoney(subtotal)) +
'</td>\n					</tr>\n					<tr>\n						<th style="font-weight: normal;">Tax</th>\n						<td class="amount">' +
__e(accounting.formatMoney(tax)) +
'</td>\n					</tr>					\n					';
 
					if(taxes && taxes.length > 0){
					_.each(taxes, function(tax){ ;
__p += '\n					<tr>\n						<th style="font-weight: normal;">' +
__e(accounting.formatNumber(tax.percent, 3)+'% - '+tax.name) +
'</th>\n						<td class="amount">' +
__e(accounting.formatMoney(tax.amount)) +
'</td>\n					</tr>\n					';
 }); 
					} ;
__p += '\n					<tr>\n						<th>Total</th>\n						<td class="amount">' +
__e(accounting.formatMoney(total)) +
'</td>\n					</tr>					\n				</table>\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-sm-offset-5 col-sm-7">\n				<h5><strong>Payments</strong></h5>\n				<table class="table table-condensed totals">\n					';
 if(payments && payments.length > 0){
					_.each(payments, function(payment){ ;
__p += '\n					<tr>\n						<th style="font-weight: normal;">' +
__e(payment.payment_type) +
'</th>\n						<td class="amount">' +
__e(accounting.formatMoney(payment.amount)) +
'</td>\n					</tr>\n					';
 }); } ;
__p += '					\n				</table>\n			</div>\n		</div>		\n		<div class="row">\n			<div class="col-md-12 barcode-container">\n				<p>' +
__e(App.data.course.get('return_policy')) +
'</p>\n				<img class="barcode" src="/fu/index.php/barcode?barcode=POS ' +
__e(sale_id) +
'&text=POS ' +
__e(sale_id) +
'" />\n			</div>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button type="button" data-dismiss="modal" class="btn btn-default pull-left">Close</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/recent_transactions_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content recent-transactions">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Recent Transactions</h4>\n	</div>\n	<div class="modal-body" style="padding: 0px;">\n		<ul class="fb-recent-transactions"></ul>\n		<div class="row" style="padding: 10px 0px; margin: 0px;">\n			<div class="col-md-2 col-md-offset-5">\n				<button class="btn btn-default btn-block more">Load More</button><button class="btn btn-default btn-block share-tips">Share Tips</button>\n			</div>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/restaurant_reservation.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<td>\n	<button class="btn btn-default check-in">\n	';
 if(checked_in == 0){ ;
__p += '\n		<span class="fa fa-square-o"></span>\n	';
 }else{ ;
__p += '\n		<span class="fa fa-check-square-o"></span>\n	';
 } ;
__p += '\n	</button>\n</td>\n<td>' +
__e(moment(time, 'hh:mm:ss').format('h:mma')) +
'</td>\n<td>' +
__e(name) +
'</td>\n<td>' +
__e(guest_count) +
'</td>\n<td class="comments">' +
__e(comments) +
'</td>\n<td class="details">\n	<small class="text-muted">By ' +
__e(employee_name) +
'<br>' +
__e(moment(date_created).fromNow()) +
'</small>\n</td>\n<td>\n	<button class="btn btn-danger pull-right delete">X</button>\n	<button class="btn btn-default pull-right edit-reservation" style="margin-right: 5px;">Edit</button>	\n</td>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/restaurant_reservation_edit.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if(time){
	time = moment(time, 'hh:mm:ss').format('h:mma');
} ;
__p += '\n<td class="checkin">&nbsp;</td>\n<td class="time">\n	<input type="text" name="time" id="reservation-time" value="' +
__e(time) +
'" class="form-control time" style="width: 100px;" placeholder="6:30pm">\n</td>\n<td class="name">\n	<input type="text" name="name" id="reservation-name" value="' +
__e(name) +
'" class="form-control name" autocomplete="off">\n	<input type="hidden" name="customer_id" id="reservation-customer-id" value="' +
__e(customer_id) +
'">\n</td>\n<td class="guests">\n	<input type="text" name="guests" id="reservation-guests" value="' +
__e(guest_count) +
'" class="form-control guests" style="width: 40px; padding: 5px 6px;">\n</td>\n<td class="comments" colspan="2" style="padding-top: 8px;">\n	<textarea name="comments" id="reservation-comments" class="form-control" style="height: 35px;">' +
__e(comments) +
'</textarea>\n</td>\n<td class="edit">\n	<button class="btn btn-success pull-right save">Save</button>\n	<button class="btn btn-default pull-right cancel">X</button>\n</td>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/restaurant_reservation_list.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '\n<table class="table restaurant-reservations">\n	<thead style="background: #F5F5F5;">\n		<tr>\n			<th class="checkin">&nbsp;</th>\n			<th class="time">Time</th>\n			<th class="name">Name</th>\n			<th class="guests">Guests</th>\n			<th class="comments">Comments</th>\n			<th class="details">&nbsp;</th>\n			<th class="edit">&nbsp;</th>\n		</tr>\n	</thead>\n	<tbody></tbody>\n</table>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/restaurant_reservations_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content restaurant-reservations">\n	<div class="modal-header" style="overflow: hidden; padding: 10px; width:100%; display:flex;">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title pull-left" style="line-height: 35px;">Reservations</h4>\n		<input type="text" id="reservations-date" class="btn btn-lg btn-default pull-left" \n			value="' +
__e(moment(date, 'M/D/YYYY').format('M/D/YY')) +
'" style="margin-left: 15px">\n		<div class="pull-left" style="flex:1;">\n			<div class="pull-left" style="margin: 0px 15px">\n				Total <span class="label label-info" id="total-reservations">' +
__e(total) +
'</span>\n			</div>\n			<div class="pull-left" style="margin: 0px 15px">\n				Guests <span class="label label-info" id="total-guests">' +
__e(guests) +
'</span>\n			</div>\n			<div class="pull-right" style="margin: 0px 15px">\n				<button class="btn btn-primary pull-right new-reservation" style="margin-right: 35px;">Add New</button>\n			</div>\n		</div>\n	</div>\n	<div class="modal-body" style="padding: 0px; min-height: 400px;"></div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/send_message.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content item-sub-category">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Send Kitchen Message</h4>\n	</div>\n	<div class="modal-body container-fluid" style="padding-bottom: 0px;">	\n		<div class="row">\n			<div class="col-md-12 form-group">\n				<label for="item_message">Message</label>\n				<textarea style="height: 75px;" name="message" id="item_message" class="form-control"></textarea>\n			</div>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-primary btn-lg pull-right send">Send</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/sub_category_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content item-sub-category">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">' +
__e(name) +
'</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<div class="row" id="sub-category-items" style="margin: 0px;"></div>\n	</div>\n	<div class="modal-footer">\n		<div class="row">\n			<div class="col-md-offset-3 col-md-3">\n				<button class="btn-block btn btn-default btn-lg up">&#x25B2;</button>\n			</div>\n			<div class="col-md-3">\n				<button class="btn-block btn btn-default btn-lg down">&#x25BC;</button>\n			</div>\n			<div class="col-md-3">\n				<a href="#" class="pull-right edit-order">Edit Button Order</a>\n			</div>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/table_layout_edit_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content new-table-layout">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Table Layout</h4>\n	</div>\n	<div class="modal-body">\n		<form role="form" class="form-horizontal">\n			<div class="form-group">\n				<label class="col-md-3">Layout Name</label>\n				<div class="col-md-6">\n					<input name="name" class="form-control" type="text" value="' +
__e(name) +
'" />\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-primary btn-lg save">Save</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/table_layout_object.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="context-menu">\n	<ul class="dropdown-menu" role="menu">\n		<li><a tabindex="-1" href="" class="btn btn-link switch-employee';
 if(!employee_id){ print(' disabled') } ;
__p += '">Switch Employee</a></li>\n	</ul>\n</div>\n<button class="btn btn-sm btn-danger delete-table">&times;</button>\n';
 if(type == 'box'){ ;
__p += '\n<div class="box"></div>\n\n';
 }else if(type == 'diamond'){ ;
__p += '\n<svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n	 width="100%" height="100%" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">\n	<rect x="27" y="0" width="60" height="60" stroke="black" stroke-width="2" style="fill: white;" class=\'shape\' transform="rotate(45, 30, 30)"/>\n</svg>\n\n';
 }else if(type == 'triangle'){ ;
__p += '\n<svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n	 width="100%" height="100%" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">\n  	<polygon x="0" y="0" points="50,95 95,25 5,25" stroke="black" fill="white" stroke-width="2" class="shape" />\n</svg>\n\n';
 }else if(type == 'circle'){ ;
__p += '\n<svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n	 width="100%" height="100%" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">\n		<circle cx="50" cy="50" r="40" stroke="black" stroke-width="2" class=\'shape\' />\n</svg>\n\n';
 }else if(type == 'ellipse'){ ;
__p += '\n<svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n	 width="100%" height="100%" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">\n	<ellipse cx="50" cy="50" rx="45" ry="25" stroke="black" stroke-width="2" class=\'shape\' translate="scale(1,2)"/>\n</svg>\n';
 } ;
__p += '	\n<span class="name">' +
__e(table_name) +
'</span>\n<span class="label">' +
__e(label) +
'</span>\n<span class="employee">' +
__e(employee_last_name) +
'</span>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["foodbev/table_layout_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content table-layout">\n	<div class="modal-header">\n		';
 if(closable){ ;
__p += '\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		';
 } ;
__p += '\n		<div class="container-fluid">\n			<div class="row">	\n				<div class="col-md-offset-2 col-md-2">\n					<button id="tables_recent_transactions" class="btn btn-success fb-button btn-block">Recent Trans.</button>\n				</div>					\n				<div class="col-md-2">\n					<button id="tables_close_register" class="btn btn-success fb-button btn-block">Close Register</button>\n				</div>\n				<div class="col-md-2">\n					<button id="reservations" class="btn btn-info fb-button btn-block">Reservations</button>	\n				</div>				\n				<div class="col-md-2">\n					<button id="tables_logout" class="btn btn-danger fb-button btn-block">Logout</button>	\n				</div>\n			</div>\n		</div>\n	</div>\n	<div class="modal-body" style="padding: 0px; position: relative;">\n		<button id="edit_layouts" class="btn btn-link" style="top: 5px; right: 10px; position: absolute;">Edit Layouts</button>\n		\n		<div class="layout-tabs"></div>\n		<div class="layouts"></div>\n		<div class="zoom-buttons">\n			<button class="btn btn-md zoom-in">+</button>\n			<button class="btn btn-md zoom-out">-</button>\n		</div>\n		<div class="object-palette">\n			<div class="object drag" data-type="diamond">\n				<svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n					 width="100%" height="100%" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">\n					<rect x="27" y="0" width="60" height="60" stroke="black" fill="white" stroke-width="2" transform="rotate(45, 30, 30)"/>\n				</svg>\n			</div>\n			<div class="object drag" data-type="triangle">\n				<svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n					 width="100%" height="100%" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">\n  					<polygon x="0" y="0" points="50,95 95,25 5,25" stroke="black" fill="white" stroke-width="2" />\n				</svg>\n			</div>\n			<div class="object drag" data-type="circle">\n				<svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n					 width="100%" height="100%" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">\n  					<circle cx="50" cy="50" r="40" stroke="black" stroke-width="2" fill="white" style="background: white;"/>\n				</svg>\n			</div>			\n			<div class="object drag" data-type="ellipse">\n				<svg version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n					 width="100%" height="100%" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">\n  					<ellipse cx="50" cy="50" rx="45" ry="25" stroke="black" stroke-width="2" translate="scale(1,2)" fill="white" style="background: white;"/>\n				</svg>\n			</div>\n			<div class="object drag" data-type="box">\n				<div class="box"></div>\n			</div>			\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["item_kits/edit_item_kit.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content item-kit-details">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Item Kit Details</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="row">\n				\n				<!-- LEFT COLUMN -->\n				<div class="col-md-6">\n					<div class="form-group">\n						<label class="col-md-4 control-label" for="item-kit-upc">UPC #</label>\n						<div class="col-md-8">\n							<input type="text" name="item_number" value="' +
__e(item_number) +
'" class="form-control" id="item-kit-upc">\n						</div>\n					</div>					\n					<div class="form-group">\n						<label class="col-md-4 control-label" for="item-kit-name">Name <span class="text-danger">*</span></label>\n						<div class="col-md-8">\n							<input type="text" name="name" value="' +
__e(name) +
'" class="form-control" id="item-kit-name"\n								data-bv-notempty data-bv-notempty-message="Name is required">\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-md-4 control-label" for="item-kit-department">Department <span class="text-danger">*</span></label>\n						<div class="col-md-8">\n							<input type="text" name="department" value="' +
__e(department) +
'" class="form-control" id="item-kit-department"\n								data-bv-notempty data-bv-notempty-message="Department is required">\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-md-4 control-label" for="item-kit-category">Category</label>\n						<div class="col-md-8">\n							<input type="text" name="category" value="' +
__e(category) +
'" class="form-control" id="item-kit-category">\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-md-4 control-label" for="item-kit-subcategory">Subcategory</label>\n						<div class="col-md-8">\n							<input type="text" name="subcategory" value="' +
__e(subcategory) +
'" class="form-control" id="item-kit-subcategory">\n						</div>\n					</div>\n				</div>\n				\n				<!-- RIGHT COLUMN -->\n				<div class="col-md-6">\n					<div class="form-group">\n						<label class="col-md-4 control-label" for="item-kit-price">Unit Price</label>\n						<div class="col-md-4">\n							<input type="text" name="unit_price" value="' +
__e(accounting.formatMoney(unit_price, '')) +
'" class="form-control" id="item-kit-price" disabled>\n						</div>\n					</div>\n                    <div class="form-group">\n                        <label class="col-md-4 control-label" for="item-kit-cost">Cost</label>\n                        <div class="col-md-4">\n                            <input type="text" name="cost_price" value="' +
__e(accounting.formatMoney(cost_price, '')) +
'" class="form-control" id="item-kit-cost" disabled>\n                        </div>\n                    </div>\n                    <div class="form-group">\n                        <label class="col-md-4 control-label" for="item-kit-max-discount">Max Discount</label>\n                        <div class="col-md-4">\n                            <div class="input-group">\n                                <input type="text" name="max_discount" value="' +
__e(accounting.formatMoney(max_discount, '')) +
'" class="form-control" id="item-kit-max-discount">\n                                <span class="input-group-addon">%</span>\n                            </div>\n                        </div>\n                        <div class="col-md-4 col-xs-4" style="padding-left: 0px">\n                            <div class="form-control-static"><h5 class="item-kit-minimum-price" style="padding: 0px; margin: 0px;">= ' +
__e(accounting.formatMoney(5.00)) +
'</h5></div>\n                        </div>\n                    </div>\n                    <div class="form-group">\n						<label class="col-md-4 control-label" for="item-kit-punch-card">Is Punch Card</label>\n						<div class="col-md-4">\n							<input style="margin-top: 10px" type="checkbox" name="is_punch_card" value="1" id="item-kit-punch-card" ';
if(item_type == 'punch_card'){ print('checked') };
__p += '>\n						</div>\n					</div>\n					<div class="form-group">\n						<label class="col-md-4 control-label" for="item-kit-price">Description</label>\n						<div class="col-md-8">\n							<textarea name="description" class="form-control" style="height: 100px" id="item-kit-description">' +
__e(description) +
'</textarea>\n						</div>\n					</div>\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-md-12">\n					<div class="form-group">\n						<label class="col-md-2 control-label" for="item-kit-search">Add Items</label>\n						<div class="col-md-4">\n							<input type="text" name="search" value="" placeholder="Search items..." class="form-control" id="item-kit-search">\n						</div>\n					</div>\n					<div class="row">\n						<div class="col-md-12">\n						<table class="table item-kit-items">\n							<thead>\n								<th>&nbsp;</th>\n								<th style="width: 50%">Item</th>\n								<th>Orig. Price</th>\n								<th>Qty</th>\n								<th>Price</th>\n								<th>Total</th>\n							</thead>\n							<tbody>\n							';
 if(items && items.length > 0){ 
							_.each(items, function(item, index){ ;
__p += '\n							<tr>\n								<td><a class="text-danger delete-item" href="#" data-index="' +
__e(index) +
'">X</a></td>\n								<td>\n									' +
__e(item.name) +
'\n								</td>\n								<td>\n									' +
__e(accounting.formatMoney(item.base_price)) +
'\n								</td>\n								<td>\n									<input style="width: 100px" type="text" class="form-control item-quantity" data-index="' +
__e(index) +
'" name="items[' +
__e(item.item_id) +
'][quantity]" value="' +
__e(accounting.formatNumber(item.quantity, 2)) +
'">\n								</td>\n								<td>\n									<input style="width: 100px" type="text" data-index="' +
__e(index) +
'" name="items[' +
__e(item.item_id) +
'][unit_price]" value="' +
__e(accounting.formatMoney(item.unit_price, '')) +
'" placeholder="0.00" class="form-control item-price">\n								</td>\n								<td>\n									<strong>' +
__e(accounting.formatMoney(item.total)) +
'</strong>\n								</td>\n							</tr>					\n							';
 }) }else{ ;
__p += '\n							<tr>\n								<td colspan="6">\n									<h4 style=\'text-align: center\' class="text-muted">No Items Attached</h4>\n								</td>\n							</tr>\n							';
 } ;
__p += '\n							</tbody>\n						</table>\n						</div>\n					</div>			\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["item_kits/item_kit_table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="table-container layout-box">\n	<table class="table table-bordered item-kits"></table>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["item_kits/item_kit_table_row.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<td><input type="checkbox" value="1"></td>\n<td>' +
__e(name) +
'</td>\n<td>' +
__e(customer.first_name +' '+ customer.last_name) +
'</td>\n<td>' +
__e(moment(start_date).format('MM/DD/YYYY')) +
'</td>\n<td>' +
__e(moment(end_date).format('MM/DD/YYYY')) +
'</td>\n<td>' +
__e(moment(date_created).format('MM/DD/YYYY')) +
'</td>\n<td>\n	';
 if(price_class_name == ''){ ;
__p += '\n	<span class="text-muted">No Price Class</span>\n	';
 }else{ ;
__p += '\n	' +
__e(price_class_name) +
'\n	';
 } ;
__p += '\n</td>\n<td>\n	<a class="pull-right edit" href="#">Edit</a>\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["item_kits/item_kits.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="item-kit-actions" class="col-md-12">\n	<div class="row" style="margin-bottom: 15px;">\n		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-1 page-action">\n			<button class="btn btn-success table-action btn-block new">\n				<span class="fa fa-plus table-action-icon"></span>\n				New Item Kit\n			</button>\n		</div>\n		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-1 page-action">\n			<button class="btn btn-primary btn-block export">\n				<span class="fa fa-cloud-download table-action-icon"></span>\n				Export Item Kits\n			</button>\n		</div>\n	</div>\n	<div class="row">\n		<div class="col-md-12">\n			<div class="row" id="item-kit-table"></div>\n		</div>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["passes/edit_pass.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content pass-details">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Pass Details</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			';
 if(!pass_id){ ;
__p += '\n			<div class="form-group">\n				<label class="col-md-3 control-label" for="pass-start-date">Pass Type</label>\n				<div class="col-md-8">\n					' +
((__t = (_.dropdown(pass_item_menu, {
						'id': 'pass-item-id',
						'name': 'pass_item_id', 
						'class':'form-control',
						'data-bv-notempty': true,
						'data-bv-notempty-message': 'Pass type is required'
					}))) == null ? '' : __t) +
'\n				</div>\n			</div>\n			';
 } ;
__p += '\n			<div class="form-group">\n				<label class="col-md-3 control-label" for="pass-customer">Customer</label>\n				<div class="col-md-5">\n					';
 if(customer && customer.first_name){
						var customer_name = customer.first_name +' '+ customer.last_name;
					}else{
						var customer_name = '';
					} ;
__p += '\n					<input type="text" name="customer" value="' +
__e(customer_name) +
'" class="form-control" id="pass-owner"\n						data-bv-notempty data-bv-notempty-message="Customer is required">\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-3 control-label" for="pass-start-date">Start Date</label>\n				<div class="col-md-4">\n					<input placeholder="MM/DD/YYYY" type="text" name="start_date" value="' +
__e(moment(start_date).format('MM/DD/YYYY')) +
'" class="form-control" id="pass-start-date"\n						data-bv-notempty data-bv-notempty-message="Start date is required">\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-3 control-label" for="pass-end-date">End Date</label>\n				<div class="col-md-4">\n					<input placeholder="MM/DD/YYYY" type="text" name="end_date" value="' +
__e(moment(end_date).format('MM/DD/YYYY')) +
'" class="form-control" id="pass-end-date"\n						data-bv-notempty data-bv-notempty-message="End date is required">\n				</div>\n			</div>\n			';
 if(multi_customer == 1){ ;
__p += '\n			<div class="form-group">\n				<div class="col-md-12">\n					<h4>Customers</h4>\n				</div>\n				<div class="col-md-12">\n					<div class="row">\n						<div class="col-md-12">\n							<input type="text" class="form-control" id="pass-customer-search" placeholder="Search/add customers to pass..." style="margin-bottom: 5px">\n						</div>\n					</div>\n					<div class="row" id="pass-customers"></div>\n				</div>\n			</div>\n			';
 } ;
__p += '										\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["passes/pass_customer.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="col-md-12">\n	<h5 class="pull-left">' +
__e(first_name) +
' ' +
__e(last_name) +
'</h5>\n	<button title="Remove customer from pass" class="btn btn-default btn-sm remove pull-right">\n		<span class="fa fa-unlink"></span>\n	</button>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["passes/pass_table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="table-container layout-box">\n	<table class="table table-bordered passes"></table>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["passes/pass_table_row.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<td><input type="checkbox" value="1"></td>\n<td>' +
__e(name) +
'</td>\n<td>' +
__e(customer.first_name +' '+ customer.last_name) +
'</td>\n<td>' +
__e(moment(start_date).format('MM/DD/YYYY')) +
'</td>\n<td>' +
__e(moment(end_date).format('MM/DD/YYYY')) +
'</td>\n<td>' +
__e(moment(date_created).format('MM/DD/YYYY')) +
'</td>\n<td>\n	';
 if(price_class_name == ''){ ;
__p += '\n	<span class="text-muted">No Price Class</span>\n	';
 }else{ ;
__p += '\n	' +
__e(price_class_name) +
'\n	';
 } ;
__p += '\n</td>\n<td>\n	<a class="pull-right edit" href="#">Edit</a>\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["passes/passes.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="pass-actions" class="col-md-12">\n	<div class="row" style="margin-bottom: 15px;">\n		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-1 page-action">\n			<button class="btn btn-success table-action btn-block new-pass">\n				<span class="fa fa-plus table-action-icon"></span>\n				New Pass\n			</button>\n		</div>	\n	</div>\n	<div class="row">\n		<div class="col-md-12">\n			<div class="row" id="pass-table"></div>\n		</div>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/edit_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content item-details">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Item Details</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<div class="row">\n			<div class="col-md-12">\n				<ul class="nav nav-pills">\n					<li role="presentation" id="item-tab-general" class="active">\n						<a href="#item-general-settings" aria-controls="general" role="tab" data-toggle="pill">General Details</a>\n					</li>\n					';
 if(item_type == 'pass'){ ;
__p += '<li role="presentation" id="item-tab-pass">\n						<a href="#item-pass-settings" aria-controls="profile" role="tab" data-toggle="pill">Pass Settings</a>\n					</li>';
 } ;
__p += '\n				</ul>\n			</div>\n		</div>\n		<form class="form-horizontal tab-content" role="form" style="margin-top: 15px">\n			<div class="tab-pane active" id="item-general-settings">\n				<div class="row">\n					\n					<!-- LEFT COLUMN -->\n					<div class="col-md-6">\n						';
 if(item_type != 'service_fee'){ ;
__p += '\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-upc">UPC #</label>\n							<div class="col-md-8">\n								<input type="text" name="item_number" value="' +
__e(item_number) +
'" class="form-control" id="item-upc">\n							</div>\n						</div>\n						<div class="row">\n							<div class="col-md-12" id="item-upc-container"></div>\n						</div>\n						';
 } ;
__p += '\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-gl-code">GL Code</label>\n							<div class="col-md-8">\n								<input type="text" name="gl_code" value="' +
__e(gl_code) +
'" class="form-control" id="item-gl-code">\n							</div>\n						</div>						\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-name">Name <span class="text-danger">*</span></label>\n							<div class="col-md-8">\n								<input type="text" name="name" value="' +
__e(name) +
'" class="form-control" id="item-name"\n									data-bv-notempty data-bv-notempty-message="Name is required">\n							</div>\n						</div>\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-department">Department <span class="text-danger">*</span></label>\n							<div class="col-md-8">\n								<input type="text" name="department" value="' +
__e(department) +
'" class="form-control" id="item-department"\n									data-bv-notempty data-bv-notempty-message="Department is required">\n							</div>\n						</div>\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-category">Category <span class="text-danger">*</span></label>\n							<div class="col-md-8">\n								<input type="text" name="category" value="' +
__e(category) +
'" class="form-control" id="item-category"\n									data-bv-notempty data-bv-notempty-message="Category is required">\n							</div>\n						</div>\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-subcategory">Subcategory</label>\n							<div class="col-md-8">\n								<input type="text" name="subcategory" value="' +
__e(subcategory) +
'" class="form-control" id="item-subcategory">\n							</div>\n						</div>\n						';
 if(item_type != 'service_fee'){ ;
__p += '\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-supplier">Supplier</label>\n							<div class="col-md-8">\n								' +
((__t = (_.dropdown(App.data.suppliers.get_dropdown_data(), {'name': 'supplier_id', 'class': 'form-control'}, supplier_id))) == null ? '' : __t) +
'\n							</div>\n						</div>\n						';
 } ;
__p += '\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-receipt-agreement">Receipt Agreement</label>\n							<div class="col-md-8">\n								' +
((__t = (_.dropdown(App.data.item_receipt_content.get_dropdown_data(), {'name': 'receipt_content_id', 'class': 'form-control'}, receipt_content_id))) == null ? '' : __t) +
'\n							</div>\n						</div>\n						<div class="form-group">\n							<label class="col-md-4 control-label">Item Type</label>\n							';
if(false && typeof item_id!=='undefined'){console.log(name,item_type); print(item_type) } ;
__p += '\n							<div class="col-md-8';
if(false && typeof item_id!=='undefined'){ print(' hidden') } ;
__p += '">\n								<div class="radio">\n									<label>\n										<input class="item-type" type="radio" name="item_type" value="item"\n										';
if(typeof item_id!=='undefined'){ print(' disabled="true"') } ;
__p += '\n											';
if(item_type == 'item'){ print('checked') } ;
__p += '>\n										Item\n									</label>\n								</div>\n								<div class="radio">\n									<label>\n										<input class="item-type" type="radio" name="item_type" value="giftcard"\n										';
if(typeof item_id!=='undefined'){ print(' disabled="true"') } ;
__p += '\n											';
if(item_type == 'giftcard'){ print('checked') } ;
__p += '>\n										Gift Card\n									</label>\n								</div>\n								<div class="radio">\n									<label>\n										<input class="item-type" type="radio" name="item_type" value="service_fee"\n										';
if(typeof item_id!=='undefined'){ print(' disabled="true"') } ;
__p += '\n											';
if(item_type == 'service_fee'){ print('checked') } ;
__p += '>\n										Service Fee\n									</label>\n								</div>\n								<div class="radio">\n									<label>\n										<input class="item-type" type="radio" name="item_type" value="pass"\n										';
if(typeof item_id!=='undefined'){ print(' disabled="true"') } ;
__p += '\n											';
if(item_type == 'pass'){ print('checked') } ;
__p += '>\n										Member Pass\n									</label>\n								</div>\n							</div>\n						</div>\n					</div>\n					\n					<!-- RIGHT COLUMN -->\n					<div class="col-md-6">\n						';
 if(item_type != 'service_fee'){ ;
__p += '\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-inactive">Inactive</label>\n							<div class="col-md-8">\n								<input style="margin-top: 10px" type="checkbox" name="inactive" value="1" data-unchecked-value="0" id="item-inactive" ';
if(inactive == 1){ print('checked') };
__p += '>\n							</div>\n						</div>\n						';
 } ;
__p += '					\n						';
 if(item_type != 'service_fee'){ ;
__p += '\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-price">Unit Price <span class="text-danger">*</span></label>\n							<div class="col-md-4">\n								<input type="text" name="unit_price" value="' +
__e(accounting.formatMoney(unit_price, '')) +
'" class="form-control" id="item-price"\n									data-bv-notempty data-bv-notempty-message="Unit price is required">\n							</div>\n						</div>\n						';
 } ;
__p += '\n						';
 if(item_type == 'service_fee'){ ;
__p += '\n						<div class="form-group">\n							<label class="col-md-4 control-label">Fee Amount <span class="text-danger">*</span></label>\n							<div class="col-md-8">\n								<div class="row">\n									<div class="col-md-6">\n										<div class="input-group">\n											<span class="input-group-addon">$</span>\n											<input type="text" name="unit_price" value="' +
__e(accounting.formatMoney(unit_price, '')) +
'" \n												class="form-control" id="item-price" data-bv-notempty data-bv-notempty-message="Unit price is required">\n										</div>	\n									</div>\n									<div class="col-md-6">\n										<div class="input-group">\n											<input type="text" name="parent_item_percent" value="' +
__e(accounting.formatMoney(parent_item_percent, '')) +
'" \n												class="form-control" id="item-percent-price" data-bv-notempty data-bv-notempty-message="Percentage price is required">\n											<span class="input-group-addon">%</span>\n										</div>	\n									</div>	\n								</div>							\n							</div>\n						</div>\n						<div class="form-group">\n							<label class="col-md-3 col-md-offset-5 control-label" for="item-cost" style="font-weight: normal">Whichever is</label>\n							<div class="col-md-4">\n								' +
((__t = (_.dropdown({'more':'More', 'less':'Less'}, {'name': 'whichever_is', 'class': 'form-control'}, whichever_is))) == null ? '' : __t) +
'		\n							</div>\n						</div>\n						';
 } ;
__p += '\n						';
 if(item_type != 'service_fee'){ ;
__p += '\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-cost">Unit Cost <span class="text-danger">*</span></label>\n							<div class="col-md-4">\n								<input type="text" name="cost_price" value="' +
__e(accounting.formatMoney(cost_price, '')) +
'" class="form-control" id="item-cost"\n									data-bv-notempty data-bv-notempty-message="Unit cost is required"\n								';
 if (!App.data.user.get('acl').can('update', 'Inventory/Cost')){ ;
__p += ' disabled="disabled" ';
 } ;
__p += '\n								>\n							</div>\n						</div>\n						<div class="form-group">\n							<label class="col-xs-12 col-md-4 control-label" for="item-max-discount">Max Discount</label>\n							<div class="col-md-4 col-xs-6">\n								<div class="input-group">\n		  							<input type="text" name="max_discount" value="' +
__e(accounting.formatNumber(max_discount, 2)) +
'" class="form-control" id="item-max-discount" ';
 if (!App.data.user.get('acl').can('update', 'Inventory/MaxDiscount')){ ;
__p += ' disabled="disabled" ';
 } ;
__p += '>\n		  							<span class="input-group-addon">%</span>\n								</div>\n							</div>\n							<div class="col-md-4 col-xs-4" style="padding-left: 0px">\n								<div class="form-control-static"><h5 class="item-minimum-price" style="padding: 0px; margin: 0px;">= ' +
__e(accounting.formatMoney(5.00)) +
'</h5></div>\n							</div>\n						</div>\n						<div class="form-group">\n							<label class="col-md-5 control-label" for="item-include-tax">Unit Price Includes Tax</label>\n							<div class="col-md-7">\n								<input style="margin-top: 10px" type="checkbox" name="unit_price_includes_tax" value="1" data-unchecked-value="0" id="item-include-tax" ';
if(unit_price_includes_tax == 1 || (typeof item_id=='undefined' && parseInt(App.data.course.get('unit_price_includes_tax')))){ print('checked') };
__p += '>\n							</div>\n						</div>	\n						';
 } ;
__p += '\n						<div class="row">\n							<div class="col-md-12" id="item-customer-groups"></div>\n						</div>\n						';
 for(var i = 1; i <= 2; i++){ 
							var tax = {
								name: '',
								percent: ''
							};

							if(taxes[i-1] && taxes[i-1]['name']){
								tax = taxes[i-1];
							}
						;
__p += '\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-tax1-name">Tax ' +
__e(i) +
'</label>\n							<div class="col-md-8">	\n								<div class="row">\n									<div class="col-xs-7">\n										<input type="text" name="tax[' +
__e(i-1) +
'][name]" placeholder="Name" value="' +
__e(tax.name) +
'" class="form-control col-md-8" id="item-tax' +
__e(i) +
'-name">\n									</div>\n									<div class="col-xs-5" style="padding-left: 0px">\n										<div class="input-group">\n											<input type="text" name="tax[' +
__e(i-1) +
'][percent]" value="' +
__e(accounting.formatNumber(tax.percent, 3)) +
'" class="form-control" id="item-tax' +
__e(i) +
'-percent">\n											<span class="input-group-addon">%</span>\n										</div>\n									</div>\n								</div>\n								';
 if(i == 2){ ;
__p += '\n								<div class="row">\n									<div class="col-md-12">\n										<div class="checkbox">\n											<label><input type="checkbox" name="tax[' +
__e(i-1) +
'][cumulative]" value="1" data-unchecked-value="0" ';
if(taxes && taxes[1] && taxes[1]['cumulative'] == 1){ print('checked') } ;
__p += '> Cumulative</label>\n										</div>\n									</div>\n								</div>\n								';
 } ;
__p += '\n							</div>\n						</div>\n						';
 } ;
__p += '\n						<div class="form-group">\n							<div class="col-md-12">\n								<div class="panel panel-default" style="margin-bottom: 0px;">\n									<div class="panel-heading" role="tab" id="headingOne">\n										<h4 class="panel-title">\n											<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#additional-taxes" aria-expanded="true" aria-controls="collapseOne">\n												Additional Taxes\n											</a>\n										</h4>\n									</div>\n								</div>\n								<div id="additional-taxes" class="collapse" role="tabpanel" aria-labelledby="headingOne" style="padding-top: 10px;">\n									';
 for(var i = 3; i <= 6; i++){ 
										var tax = {
											name: '',
											percent: ''
										};

										if(taxes[i-1] && taxes[i-1]['name']){
											tax = taxes[i-1];
										}
									;
__p += '\n									<div class="form-group">\n										<label class="col-md-4 control-label" for="item-tax1-name">Tax ' +
__e(i) +
'</label>\n										<div class="col-md-8">	\n											<div class="row">\n												<div class="col-xs-7">\n													<input type="text" name="tax[' +
__e(i-1) +
'][name]" placeholder="Name" value="' +
__e(tax.name) +
'" class="form-control col-md-8" id="item-tax1-name">\n												</div>\n												<div class="col-xs-5" style="padding-left: 0px">\n													<div class="input-group">\n														<input type="text" name="tax[' +
__e(i-1) +
'][percent]" value="' +
__e(accounting.formatNumber(tax.percent, 3)) +
'" class="form-control" id="item-tax1-percent">\n														<span class="input-group-addon">%</span>\n													</div>\n												</div>\n											</div>\n										</div>\n									</div>\n									';
 } ;
__p += '\n								</div>\n							</div>\n						</div>\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-reorder-level">Taxable</label>\n							<div class="col-md-4">\n								';
 if(force_tax === null){ force_tax = 'default'; } ;
__p += '\n								' +
((__t = (_.dropdown({'default':'Default', '1':'Always Tax', '0':'Never Tax'}, {'name': 'force_tax', 'class': 'form-control'}, force_tax))) == null ? '' : __t) +
'\n							</div>\n						</div>\n						';
 if(item_type != 'service_fee'){ ;
__p += '\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-inventory-level">Quantity <span class="text-danger">*</span></label>\n							<div class="col-md-6">\n								<div class="input-group">\n									<input type="text" name="inventory_level" value="' +
__e(inventory_level) +
'" class="form-control" id="item-inventory-level" ';
 if (!App.data.user.get('acl').can('update', 'Inventory/Quantity')){ ;
__p += ' disabled="disabled" ';
 } ;
__p += '\n									>\n									<span class="input-group-addon">\n										<label><input type="checkbox" id="item-inventory-unlimited" name="inventory_unlimited" value="1" data-unchecked-value="0" ';
if(inventory_unlimited == 1){ print('checked') };
__p += ' ';
 if (!App.data.user.get('acl').can('update', 'Inventory/Quantity')){ ;
__p += ' disabled="disabled" ';
 } ;
__p += '\n											> Unlimited</label>\n									</span>\n								</div>\n							</div>\n						</div>\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-reorder-level">Reorder Level <span class="text-danger">*</span></label>\n							<div class="col-md-4">\n								<input type="text" name="reorder_level" value="' +
__e(reorder_level) +
'" class="form-control" id="item-reorder-level">\n							</div>\n						</div>\n						<div class="form-group">\n							<label class="col-md-4 control-label" for="item-reorder-level">E-Range Size</label>\n							<div class="col-md-4">\n								' +
((__t = (_.dropdown({0:0,1:1, 2:2, 3:3}, {'name': 'erange_size', 'class': 'form-control'}, erange_size))) == null ? '' : __t) +
'\n							</div>\n						</div>\n						';
 } ;
__p += '\n					</div>\n				</div>\n				<div class="row">\n					<div class="col-md-12">\n						<div class="form-group">\n							<label class="col-md-2 control-label" for="item-price">Description</label>\n							<div class="col-md-10">\n								<textarea name="description" class="form-control" style="height: 100px" id="item-description">' +
__e(description) +
'</textarea>\n							</div>\n						</div>\n					</div>\n				</div>\n				<div class="row">\n					<div class="col-md-12" id="item-service-fees"></div>\n				</div>\n			</div>\n\n			';
 if(item_type == 'pass'){ ;
__p += '<div class="tab-pane" id="item-pass-settings">\n			<!-- Pass Details Here -->\n			</div>';
 } ;
__p += '\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/edit_pass_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="form-group">\n	<label class="col-md-2 control-label" for="item-shared">Share with course group</label>\n	<div class="col-md-8">\n		<div class="checkbox">\n			<input type="checkbox" id="item-shared" name="is_shared" data-unchecked-value="0" value="1" ';
if(is_shared == 1){ ;
__p += 'checked';
 } ;
__p += '>\n		</div>\n	</div>\n</div>\n<div class="form-group edit-pass-item">\n	<label class="col-md-2 control-label" for="item-pass-expiration">Default Price</label>\n	<div class="col-md-4">\n		';

		if(pass_price_class_id === null){
			pass_price_class_id = 'none';
		}
		var price_classes = _.clone(App.data.course.get('price_classes'));
		_.extend(price_classes, {'none': '- No Default Price Class -'}); ;
__p += '\n		' +
((__t = (_.dropdown(price_classes, {"name":"pass_price_class_id", "class":"form-control"}, pass_price_class_id))) == null ? '' : __t) +
'\n	</div>\n</div>\n<div class="form-group">\n	<label class="col-md-2 control-label" for="item-pass-multi-customer">Multi Customer</label>\n	<div class="col-md-8">\n		<div class="checkbox">\n			<input type="checkbox" id="item-pass-multi-customer" name="pass_multi_customer" data-unchecked-value="0" value="1" ';
if(pass_multi_customer == 1){ ;
__p += 'checked';
 } ;
__p += '>\n		</div>\n	</div>\n</div>\n<div class="form-group">\n	<label class="col-md-2 control-label" for="item-pass-multi-customer">Enroll in Loyalty</label>\n	<div class="col-md-8">\n		<div class="checkbox">\n			<input type="checkbox" id="item-pass-enroll-loyalty" name="pass_enroll_loyalty" data-unchecked-value="0" value="1" ';
if(pass_enroll_loyalty == 1){ ;
__p += 'checked';
 } ;
__p += '>\n		</div>\n	</div>\n</div>\n<div class="form-group">\n	<label class="col-md-2 control-label" for="item-pass-expiration">Expires</label>\n	<div class="col-md-10">\n		<div class="form-group">\n			<div class="col-md-3">\n				<div class="input-group">\n					<span class="input-group-addon">\n						<input type="radio" name="pass_expiration_type" value="date" \n							class="pass-expiration" ';
 if(pass_expiration_date){ ;
__p += 'checked';
 } ;
__p += '>\n					</span>\n					<input type="text" name="pass_expiration_date" placeholder="MM/DD/YYYY" \n						value="';
 if(pass_expiration_date){ print(moment(pass_expiration_date).format('MM/DD/YYYY')) } ;
__p += '"class="form-control" id="pass-expiration-date">\n				</div>\n			</div>\n		</div>\n		<div class="form-group">\n			<div class="col-md-2">\n				<div class="input-group">\n					<span class="input-group-addon">\n						<input type="radio" name="pass_expiration_type" class="pass-expiration" value="days" ';
 if(pass_days_to_expiration){ ;
__p += 'checked';
 } ;
__p += '>\n					</span>\n					<input type="text" name="pass_days_to_expiration" value="';
 if(pass_days_to_expiration){ print(pass_days_to_expiration) } ;
__p += '" \n						class="form-control" id="pass-expiration-days">\n				</div>\n			</div>\n			<div class="col-xs-4" style="padding-left: 0px;">\n				<div class="form-control-static">days after purchase</div>\n			</div>\n		</div>		\n	</div>\n</div>\n<div class="row" style="margin-top: 15px; margin-bottom: 10px">\n	<div class="col-md-8">\n		<h3 style="margin: 0px;">Rules</h3>\n	</div>\n</div>\n<div class="row">\n	<div class="col-md-12" id="item-pass-rules"><!-- Pass rules list will be rendered here --></div>\n</div>\n<div class="row">\n	<div class="col-md-12">	\n		<button type="button" class="btn btn-success btn-block add-rule"><i class="fa fa-plus"></i> New Rule</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/item_customer_group.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<h5 class="name">\n	' +
__e(label) +
'\n	<em style="font-weight: normal">\n		-\n		';
 if(item_cost_plus_percent_discount === null){ ;
__p += '\n		No discount\n		';
 }else{ ;
__p += '\n		Cost + ' +
__e(accounting.formatNumber(item_cost_plus_percent_discount)) +
'%\n		';
 } ;
__p += '\n	</em>\n</h5>\n<button class="btn btn-default delete">\n	<i class="glyphicon glyphicon-trash"></i>\n</button>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/item_customer_groups.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="panel panel-default">\n	<div class="panel-heading">\n		<h4 class="panel-title">\n			<a data-toggle="collapse" data-parent="#item-customer-groups" href="#collapse-item-customer-groups">\n			Customer Groups\n			</a>\n			<span class="label label-info count">0</span>\n		</h4>\n	</div>\n	<div id="collapse-item-customer-groups" class="panel-collapse collapse">\n		<div class="panel-body">\n			<select name="new_group" id="item-new-group" class="form-control">\n				<option value="0">- Add New Group -</option>\n				';
 _.each(App.data.course.get('groups').models, function(group){ ;
__p += '\n				<option value="' +
__e(group.get('group_id')) +
'">' +
__e(group.get('label')) +
'</option>\n				';
 }) ;
__p += '\n			</select>\n			<ul class="customer-data"></ul>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/item_service_fee.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<button data-service-fee-id="' +
__e(service_fee_id) +
'" class="btn btn-default fa fa-sort" style="cursor: move; margin-right: 10px" type="button"></button>\n<strong>' +
__e(name) +
'</strong> - (' +
__e(accounting.formatMoney(unit_price)) +
' / ' +
__e(accounting.formatMoney(parent_item_percent, '')) +
'%)\n<button class="btn btn-default delete">\n	<i class="glyphicon glyphicon-trash"></i>\n</button>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/item_service_fees.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="panel panel-default" style="overflow: visible">\n	<div class="panel-heading">\n		<h4 class="panel-title">\n			<a data-toggle="collapse" data-parent="#customer-household" href="#collapse-item-service-fees">\n			Service Fees\n			</a>\n			<span class="label label-info count">0</span>\n		</h4>\n	</div>\n	<div id="collapse-item-service-fees" class="panel-collapse collapse">\n		<div class="panel-body">\n			<input class="form-control" id="service-fee-search" value="" placeholder="Search service fees">\n			<ul class="customer-data"></ul>\n		</div>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/item_table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="table-container layout-box">\n	<table class="table table-bordered items"></table>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/item_table_row.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<td><input type="checkbox" value="1"></td>\n<td>' +
__e(name) +
'</td>\n<td>' +
__e(customer.first_name +' '+ customer.last_name) +
'</td>\n<td>' +
__e(moment(start_date).format('MM/DD/YYYY')) +
'</td>\n<td>' +
__e(moment(end_date).format('MM/DD/YYYY')) +
'</td>\n<td>' +
__e(moment(date_created).format('MM/DD/YYYY')) +
'</td>\n<td>\n	';
 if(price_class_name == ''){ ;
__p += '\n	<span class="text-muted">No Price Class</span>\n	';
 }else{ ;
__p += '\n	' +
__e(price_class_name) +
'\n	';
 } ;
__p += '\n</td>\n<td>\n	<a class="pull-right edit" href="#">Edit</a>\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/item_upc.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<h5 class="name">' +
__e(upc) +
'</h5>\n<button class="btn btn-default delete">\n	<i class="glyphicon glyphicon-trash"></i>\n</button>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/item_upc_list.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="panel panel-default" style="overflow: visible">\n	<div class="panel-heading">\n		<h4 class="panel-title">\n			<a data-toggle="collapse" data-parent="#customer-household" href="#collapse-item-upcs">\n			Additional UPCs\n			</a>\n			<span class="label label-info count">0</span>\n		</h4>\n	</div>\n	<div id="collapse-item-upcs" class="panel-collapse collapse">\n		<div class="panel-body">\n			<div class="row">\n				<div class="col-xs-9"><input type="text" class="form-control" id="item-upc-name" value="" placeholder="Enter new UPC"></div>\n				<div class="col-xs-3"><button class="btn btn-primary add-upc">Add</button></div>\n			</div>\n			<ul class="customer-data"></ul>\n		</div>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/items.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="item-actions" class="col-md-12">\n	<div class="row" style="margin-bottom: 15px;">\n		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-1 page-action">\n			<button class="btn btn-success table-action btn-block new">\n				<span class="fa fa-plus table-action-icon"></span>\n				New Item\n			</button>\n		</div>\n		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-1 page-action">\n			<button class="btn btn-primary btn-block export">\n				<span class="fa fa-cloud-download table-action-icon"></span>\n				Export Items\n			</button>\n		</div>\n		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-1 page-action">\n			<button class="btn btn-primary btn-block receipt-agreements">\n				<span class="fa fa-file-text-o table-action-icon"></span>\n				Receipt Agreements\n			</button>\n		</div>\n		<!-- <div class="col-xs-12 col-sm-2 col-md-1">\n			<button class="btn btn-primary btn-block modifiers">\n				Modifiers\n			</button>\n		</div>\n		<div class="col-xs-12 col-sm-2 col-md-1">\n			<button class="btn btn-primary btn-block meal-courses">\n				Meal Courses\n			</button>\n		</div>\n		<div class="col-xs-12 col-sm-2 col-md-1">\n			<button class="btn btn-primary btn-block meal-courses">\n				<span class="fa fa-tasks table-action-icon"></span>\n				Inventory Audit\n			</button>\n		</div> -->\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/pass_item_rule.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="panel-heading">\n	<span class="fa fa-sort sort-rule"></span>\n	<strong>' +
__e(name) +
'</strong> - Price Class: ' +
__e(price_class_name) +
'\n	<button type="button" class="btn btn-sm btn-danger pull-right pass-rule-delete"><i class="fa fa-trash"></i> Delete</button>\n	<button type="button" class="btn btn-sm btn-default pull-right pass-rule-edit"><i class="fa fa-edit"></i> Edit</button>\n</div>\n<div class="panel-body">\n	<ul class="pass-rule-condition-summary">\n		';
 if(conditions && conditions.length > 0){ 
		_.each(conditions, function(condition){ ;
__p += '\n		<li>\n			If\n			<span class="pass-rule-field">' +
__e(fields[condition.field]) +
'</span> \n			';
 if(condition.filter && condition.filter.length > 0){ ;
__p += '\n			<span class="pass-rule-filter">(\n			';
 _.each(condition.filter, function(f, index){ ;
__p += '\n				' +
__e(f.label ) +
' ';
 if(condition.filter[index + 1]){ print('AND') } ;
__p += '\n			';
 }) ;
__p += '\n			)</span>\n			';
 } ;
__p += '\n			<span class="pass-rule-condition">' +
__e(condition.operator) +
'</span> \n			<span class="pass-rule-value">\n			';
 if((typeof(condition.value) == 'string' || typeof(condition.value) == 'number')){ ;
__p += '\n				' +
__e(condition.value) +
'\n			';
 }else if(condition.value && condition.value.length > 0){ ;
__p += '\n				';
 _.each(condition.value, function(cond, index){ ;
__p += '\n					' +
__e(cond.label ) +
' ';
 if(condition.value[index + 1]){ print('<strong>OR</strong>') } ;
__p += '\n				';
 }) ;
__p += '\n			';
 } ;
__p += '\n			</span>\n		</li>\n		';
 }) }else{ ;
__p += '\n		<span class="text-muted">No conditions</span>\n		';
 } ;
__p += '\n	</ul>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/pass_item_rule_condition_edit.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-md-12 pass-rule-condition">\n	<table class="table table-compact">\n		<thead>\n			<tr>\n				<th class="condition-header condition-table-field">Field</th>\n				';
 if(can_filter){ ;
__p += '\n				<th class="condition-header condition-table-filter">\n					Filter\n					<div class="dropdown dropdown-menu-right pull-right">\n						<button class="btn btn-xs btn-success dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="add-value">\n							+ Add Course\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							';
 _.each(teesheets, function(teesheet, index){ ;
__p += '\n							<li class="condition-value add-filter" data-field=\'teesheet\' data-value-id="' +
__e(index) +
'"><a href="#">' +
__e(teesheet.label) +
'</a></li>\n							';
 }); ;
__p += '\n						</ul>\n					</div>\n					<div class="dropdown dropdown-menu-right pull-right">\n						<button class="btn btn-xs btn-success dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="add-value">\n							+ Add Price Class\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							';
 _.each(price_classes, function(price_class, price_class_id){ ;
__p += '\n							<li class="condition-value add-filter" data-field=\'price_class\' data-value-id="' +
__e(price_class_id) +
'"><a href="#">' +
__e(price_class) +
'</a></li>\n							';
 }); ;
__p += '\n						</ul>\n					</div>					\n					<button class="btn btn-xs btn-success pull-right add-filter" type="button" data-field="timeframe">+ Add Date/Time</button>\n				</th>\n				';
 } ;
__p += '\n				<th class="condition-header condition-table-operator">&nbsp;</th>\n				<th class="condition-header condition-table-value ';
 if(scalar_value && can_filter){ ;
__p += 'scalar';
 } ;
__p += '">\n					Value\n					';
 if(field == 'teesheet'){ ;
__p += '\n					<div class="dropdown dropdown-menu-right pull-right">\n						<button class="btn btn-xs btn-success dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="add-value">\n							+ Add ' +
__e(fields[field]) +
'\n							<span class="caret"></span>\n						</button>\n						<ul class="dropdown-menu">\n							';
 _.each(teesheets, function(teesheet, index){ ;
__p += '\n							<li class="condition-value add-value" data-field=\'teesheet\' data-value-id="' +
__e(index) +
'"><a href="#">' +
__e(teesheet.label) +
'</a></li>\n							';
 }); ;
__p += '\n						</ul>\n					</div>\n					';
 }else if(field == 'timeframe'){ ;
__p += '\n					<button class="btn btn-xs btn-success pull-right add-value" type="button" data-field="timeframe">+ Add ' +
__e(fields[field]) +
'</button>\n					';
 } ;
__p += '\n				</th>\n				<th class="condition-header condition-table-actions">&nbsp;</th>\n			</tr>\n		</thead>\n		<tbody>\n			<tr>\n				<td class="condition-table-parameter condition-table-field">\n					' +
((__t = (_.dropdown(fields, {"name":"field", "class":"form-control condition-field"}, field))) == null ? '' : __t) +
'\n					<div class="arrow-notch"></div>\n				</td>\n				';
 if(can_filter){ ;
__p += '\n				<td class="condition-table-parameter condition-table-filter">\n					<div id="condition-filter"></div>\n					<div class="arrow-notch"></div>\n				</td>\n				';
 } ;
__p += '\n				<td class="condition-table-parameter condition-table-operator">\n					';
 if(field){ ;
__p += '\n					' +
((__t = (_.dropdown(valid_operators, {"name":"operator", "class":"form-control condition-operator"}, operator))) == null ? '' : __t) +
'\n					<div class="arrow-notch"></div>\n					';
 } ;
__p += '\n				</td>\n				<td class="condition-table-parameter condition-table-value ';
 if(scalar_value && can_filter){ ;
__p += 'scalar';
 } ;
__p += '">\n					<div id="condition-value"></div>\n				</td>\n				<td class="condition-table-parameter condition-table-actions">\n					<button type="button" class="btn btn-danger condition-delete"><i class="fa fa-times"></i></button>\n				</td>\n			</tr>\n		</tbody>\n	</table>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/pass_item_rule_condition_edit_parameter.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="btn btn-default btn-block rule-condition">\n	<a class="condition-parameter-delete text-danger">\n		<i class="fa fa-times"></i>\n	</a>\n';
 if(field == 'timeframe'){ 
	var anytime = true;
;
__p += '\n	';
 if(day_of_week && day_of_week.join('') != '1234567'){ 
	anytime = false;
	
	for(var x = 1; x <= 7; x++){ ;
__p += '\n	<span class="timeframe-day-of-week ';
 if(day_of_week.indexOf(x) >= 0){ ;
__p += 'active';
 } ;
__p += '">' +
__e(moment(x, 'e').format('dd')) +
'</span>\n	';
 } } ;
__p += '\n\n	';
 if(time && time[0] && time[1] && (time[0] != '0000' || time[1] != '2359')){ 
	anytime = false;
	;
__p += '\n	<span class="timeframe-time-of-day">\n		<span class="fa fa-clock-o"></span>\n		' +
__e(moment(time[0], 'HHmm').format('h:mma')) +
' to ' +
__e(moment(time[1], 'HHmm').format('h:mma')) +
'\n	</span>\n	';
 } ;
__p += '\n\n	';
 if(date && date[0] && date[1]){ 
	anytime = false;
	;
__p += '\n	<span class="timeframe-date">\n		<span class="fa fa-calendar-o"></span> \n		' +
__e(moment(date[0]).format('M/D/YY')) +
' to ' +
__e(moment(date[1]).format('M/D/YY')) +
'\n	</span>\n	';
 } ;
__p += '\n\n	';
 if(anytime){ ;
__p += '\n	<em>Anytime</em>\n	';
 } ;
__p += '\n\n';
 }else if(field == 'teesheet'){ ;
__p += '\n	' +
__e(label) +
'\n';
 }else if(field == 'price_class'){ ;
__p += '\n	<strong>Price class:</strong> ' +
__e(label) +
'\n';
 } ;
__p += '\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/pass_item_rule_condition_edit_timeframe.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 var days = {
	1: 'Mon',
	2: 'Tue',
	3: 'Wed',
	4: 'Thu',
	5: 'Fri',
	6: 'Sat',
	7: 'Sun'
}; ;
__p += '\n<div class="row">\n	<form class="form">\n		<div class="row">\n			<label class="col-xs-12" for="item-pass-expiration">Day of Week</label>\n		</div>\n		<div class="form-group">\n			<div class="col-xs-12">\n				';
 for(var day = 1; day <= 7; day++){ ;
__p += '\n				<label class="timeframe-day ';
 if(day_of_week.indexOf(day) >= 0){ ;
__p += 'active';
 } ;
__p += '">\n					<div class="day">' +
__e(days[day]) +
'</div>\n					<input type="checkbox" name="day[' +
__e(day) +
']" value="1" data-unchecked-value="0" class="day-of-week" ';
 if(day_of_week.indexOf(day) >= 0){ ;
__p += 'checked';
 } ;
__p += '>\n				</label>\n				';
 } ;
__p += '\n			</div>\n		</div>\n		<div class="row">\n			<label class="col-xs-6" for="item-pass-expiration">Time of Day</label>\n			<div class="col-xs-6" style="text-align: right">\n				<span class="label label-default time-start">' +
__e(moment(time[0], 'HHmm').format('h:mma') ) +
'</span> to\n				<span class="label label-default time-end">' +
__e(moment(time[1], 'HHmm').format('h:mma') ) +
'</span>\n			</div>\n		</div>\n		<div class="form-group">\n			<div class="col-xs-12">\n				<div id="time-of-day-slider"></div>\n			</div>\n		</div>\n		<div class="row">\n			<label class="col-xs-12" for="item-pass-expiration">Date Range</label>\n		</div>\n		<div class="form-group">\n			<div class="col-xs-4">\n				<input type="text" name="start_date" value="';
 if(date && date[0]){ print(moment(date[0], 'MM/DD/YYYY').format('MM/DD/YYYY')) } ;
__p += '" class="datepicker form-control timeframe-start-date" placeholder="MM/DD/YYYY">\n			</div>\n			<div class="col-xs-1" style="padding-right: 0px; padding-left: 0px">\n				<div class="form-control-static" style="text-align: center">to</div>\n			</div>\n			<div class="col-xs-4">\n				<input type="text" name="end_date" value="';
 if(date && date[1]){ print(moment(date[1], 'MM/DD/YYYY').format('MM/DD/YYYY')) } ;
__p += '" class="datepicker form-control timeframe-start-date" placeholder="MM/DD/YYYY">\n			</div>\n		</div>\n		<div class="form-group">\n			<div class="col-xs-12">\n				<button class="btn btn-primary save-timeframe">Save</button>\n			</div>			\n		</div>\n	</form>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/pass_item_rule_edit.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content pass-item-rule-details">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Pass Rule</h4>\n	</div>\n	<div class="modal-body container-fluid" style="padding-top: 10px">\n		<form class="form form-horizontal">\n			<div class="form-group">\n\n				<label class="col-md-2 control-label" for="pass-rule-type">Type</label>\n						<label class="radio-inline">\n							<input class="rule-type" type="radio" name="pass-rule-type" value="teetimes"\n							';
if(type == 'teetimes'){ print('checked') } ;
__p += '>\n							Tee Times\n						</label>\n						<label class="radio-inline">\n							<input class="rule-type" type="radio" name="pass-rule-type" value="items"\n							';
if(type == 'items'){ print('checked') } ;
__p += '>\n							Items\n						</label>\n			</div>\n			<div class="form-group">\n				<label class="col-md-2 control-label" for="pass-rule-name">Name</label>\n				<div class="col-md-5">\n					<input type="text" name="name" value="' +
__e(name) +
'" class="form-control" id="pass-rule-name">\n				</div>\n			</div>\n			';
 if(type == 'teetimes'){ ;
__p += '\n				<div class="form-group">\n					<label class="col-md-2 control-label" for="pass-rule-price">Set price to</label>\n					<div class="col-md-3">\n						' +
((__t = (_.dropdown(App.data.course.get('price_classes'), {"name":"price_class_id", "class":"form-control"}, price_class_id))) == null ? '' : __t) +
'\n					</div>\n					<div class="col-md-4" style="padding-left: 0px;">\n						<div class="form-control-static">if the conditions below are met</div>\n					</div>\n				</div>\n			';
 } ;
__p += '\n\n			';
 if(type == 'items'){ ;
__p += '\n				<div id="item-search"></div>\n			';
 } ;
__p += '\n\n			<div class="row" style="margin-top: 25px; margin-bottom: 10px">\n				<div class="col-md-8">\n					<h3 style="margin: 0px;">Conditions</h3>\n				</div>\n			</div>\n			<div class="row" id="rule-conditions">\n				<!-- Rule conditions here -->\n			</div>\n			<div class="row">\n				<div class="col-md-12 pass-rule-condition">\n					<button type="button" class="btn btn-success btn-block new-rule-condition">\n						<i class="fa fa-plus"></i> Add New Condition\n					</button>\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default pull-left cancel">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save Rule</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/print_barcodes.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content print-barcodes">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Print Barcodes</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<label class="col-md-4 control-label">Avery Label Size</label>\n				<div class="col-md-5">\n					<label class="radio-inline">\n						<input type="radio" name="label_size" id="label-size-5267" value="5267" checked> 5267 (4x20)\n					</label>\n					<label class="radio-inline">\n						<input type="radio" name="label_size" id="label-size-5160" value="5160"> 5160 (3x10)\n					</label>\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-4 control-label">Start Printing At</label>\n				<div class="col-md-5">\n					<input name="label_size" value="1" id="label-start-number" class="form-control">\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-4 control-label">Top Margin</label>\n				<div class="col-md-5">\n					<input name="top_margin" value="" id="label-top-margin" placeholder="Optional..." class="form-control">\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-4 control-label">Show Additional Price</label>\n				<div class="col-md-3" style="padding-right: 5px;">\n					<input name="additional_name" value="" id="label-additional-name" placeholder="eg. MSRP" class="form-control">\n				</div>\n				<div class="input-group col-md-2" style="padding-top: 0px;">\n					<input type="text" class="form-control" id="label-additional-percentage" placeholder="Diff">\n					<span class="input-group-addon">%</span>\n				</div>\n			</div>\n			<h4 style="margin-top: 20px">Barcode Quantities</h4>\n			';
 if(items && items.length > 0){
			_.each(items.models, function(item){ ;
__p += '\n			<div class="form-group">\n				<label class="col-md-4 control-label" style="font-weight: normal;">' +
__e(item.get('name')) +
'</label>\n				<div class="col-md-2">\n					<input data-item-id="' +
__e(item.get('item_id')) +
'" name="print_quantity[]" value="1" placeholder="Qty" class="form-control">\n				</div>\n			</div>\n			';
 }); ;
__p += '\n			';
 } ;
__p += '\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary print">Print</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/print_individual_barcodes.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content print-individual-barcodes">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\r\n        <h4 class="modal-title">Print Barcodes</h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <form class="form-horizontal" role="form">\r\n            <div class="form-group">\r\n                <label class="col-md-4 control-label">Show Additional Price</label>\r\n                <div class="col-md-3" style="padding-right: 5px;">\r\n                    <input name="additional_name" value="" id="label-additional-name" placeholder="eg. MSRP" class="form-control">\r\n                </div>\r\n                <div class="input-group col-md-2" style="padding-top: 0px;">\r\n                    <input type="text" class="form-control" id="label-additional-percentage" placeholder="Diff">\r\n                    <span class="input-group-addon">%</span>\r\n                </div>\r\n            </div>\r\n            <h4 style="margin-top: 20px">Barcode Quantities</h4>\r\n            ';
 if(items && items.length > 0){
            _.each(items.models, function(item){ ;
__p += '\r\n            <div class="form-group">\r\n                <label class="col-md-4 control-label" style="font-weight: normal;">' +
__e(item.get('name')) +
'</label>\r\n                <div class="col-md-2">\r\n                    <input data-item-id="' +
__e(item.get('item_id')) +
'" name="print_quantity[]" value="1" placeholder="Qty" class="form-control">\r\n                </div>\r\n            </div>\r\n            ';
 }); ;
__p += '\r\n            ';
 } ;
__p += '\r\n        </form>\r\n    </div>\r\n    <div class="modal-footer">\r\n        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\r\n        <button type="button" class="btn btn-primary print">Print</button>\r\n    </div>\r\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/search_for_items.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div>\n	<input class="form-control" id="item-search" value="" placeholder="Search Items">\n	<ul class="customer-data"></ul>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/searched_for_items_list.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="form-inline">\n	<div class="col-md-6">\n		<strong>' +
__e(name) +
'</strong> (<span data-bind="text:currencyFormatUnitPrice"></span> )\n	</div>\n	<div class="col-md-6">\n		<!--<label for="newprice">Discount to: </label>-->\n		<input type="text" class="form-control" id="newprice" data-bind="value:unit_price">\n		<button class="btn btn-default delete">\n			<i class="glyphicon glyphicon-trash"></i>\n		</button>\n	</div>\n\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/cart.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-lg-12">\n	<div class="';
 if(mode == 'sale'){ print('bg-primary ') }else{ print('btn-danger') } ;
__p += '" style="padding: 10px 0px;" id="cart-header">\n		<div class="col-xs-8 col-sm-8 col-md-4">\n			<input type="text" style="display: block;" class="form-control typeahead-item" id="item-search" value="" autocomplete="off" placeholder="Search items or scan bar code..." />\n		</div>\n		<div class="col-md-offset-4 col-sm-4 col-xs-4 col-md-4">\n			<div class="btn-group pull-right hidden-xs">\n				<button id="mode-sale" type="button" class="btn btn-default cart-mode ';
 if(mode == 'sale'){ print('active') } ;
__p += '" data-mode="sale">Sale</button>\n				<button id="mode-return" type="button" class="btn btn-default cart-mode ';
 if(mode == 'return'){ print('active') } ;
__p += '" data-mode="return">Return</button>\n			</div>\n			<div class="btn-group hidden-sm hidden-md hidden-lg pull-right">\n				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">\n					';
 if(mode == 'sale'){ ;
__p += 'Sale';
 }else{ ;
__p += 'Return';
 } ;
__p += '\n					<span class="caret"></span>\n				</button>\n				<ul class="dropdown-menu" role="menu">\n					<li>\n						<a href="#" class="cart-mode" data-mode="';
 if(mode == 'sale'){ print('return') }else{ print('sale') } ;
__p += '">\n						';
 if(mode == 'sale'){ print('Return') }else{ print('Sale') } ;
__p += '\n						</a>\n					</li>\n				</ul>\n			</div>			\n		</div>\n	</div>\n</div>\n<div class="col-lg-12" id="cart-items">\n	<div class="table-responsive" style="background-color: white;">\n		<table class="table">\n			<thead>\n				<tr>\n					<th class="delete"><a id="cancel-sale" class="delete-item">&times;</a></th>\n					<th class="select"><input type="checkbox" name="checkall" id="cart-checkall" value="1" ';
 if(check_all){ print('checked') } ;
__p += ' /></th>\n					<th class="name">Item</th>\n					<th class="price">Price</th>\n					<th class="qty">Qty</th>\n					<th class="discount">Disc (%)</th>\n					<th class="total">Total</th>\n				</tr>\n			</thead>\n			<tbody>\n			</tbody>\n		</table>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/cart_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<td class="delete"><a class="delete-item text-danger">&times;</a></td>\n<td class="select"><input type="checkbox" class="select-item" name="basket" value="1" ';
 if(typeof selected!=='undefined' && selected){ print('checked'); } ;
__p += ' /></td>\n<td class="name ';
 if(item_type == 'green_fee' || item_type == 'cart_fee'){ ;
__p += 'fee';
 } ;
__p += '">\n';
 if(item_type == 'green_fee' || item_type == 'cart_fee'){ 
	var season_dropdown_params = {"class": "seasons form-control pull-left", "style": "width: 50%;"};
	var fee_dropdown_params = {"class": "fees form-control pull-left", "style": "width: 50%"};

	if(!App.data.user.get('acl').can('update', 'Pos/FeeDropdown')){
		season_dropdown_params.disabled = 'disabled';
		fee_dropdown_params.disabled = 'disabled';
	}
;
__p += '\n	' +
((__t = (_.dropdown(season_dropdown, season_dropdown_params, selected_season))) == null ? '' : __t) +
'\n	' +
((__t = (_.dropdown(fee_dropdown, fee_dropdown_params, selected_fee))) == null ? '' : __t) +
'\n\n	';
if(customer_initials){ ;
__p += '\n	<span class="customer-initials" style="background-color: ' +
__e(customer_color) +
'">' +
__e(customer_initials) +
'</span>\n	';
 } ;
__p += '\n';
 }else if(item_type == 'giftcard' || item_type == 'punch_card' || item_type == 'invoice' || item_type == 'statement' || item_type == 'pass' || item_type == 'item_kit' || food_and_beverage == 1){ ;
__p += '\n    <a title="Edit details" href="" class="edit-details">\n    	' +
__e(name) +
'\n    </a>\n';
 }else if(item_type == 'customer_account' || item_type == 'member_account' || item_type == 'invoice_account'){ ;
__p += '\n    ' +
__e(name) +
' - ' +
__e(params.customer.first_name) +
' ' +
__e(params.customer.last_name) +
'\n';
 }else{ ;
__p += '\n	' +
__e(name) +
'\n	';
if(typeof customer_initials !== 'undefined' && customer_initials){ ;
__p += '\n	<span class="customer-initials" style="background-color: ' +
__e(customer_color) +
'">' +
__e(customer_initials) +
'</span>\n	';
 } ;
__p += '\n';
 } ;
__p += '\n	';
 if(typeof service_fees !== 'undefined' && service_fees && service_fees.length > 0){ ;
__p += '\n	';
 _.each(service_fees.models, function(fee){ ;
__p += '\n	<div>\n		<span class="name">(' +
__e(fee.get('name') ) +
'): </span>\n		<span class="value">' +
__e(accounting.formatMoney(fee.get('subtotal')) ) +
'</span>\n	</div>\n	';
 }); ;
__p += '\n	';
 } ;
__p += '\n	';
if(typeof modifiers !== 'undefined' && modifiers && modifiers.length > 0){ ;
__p += '\n	';
 _.each(modifiers.models, function(modifier){ ;
__p += '\n	<div>\n		<span class="name">' +
__e(modifier.get('name') ) +
' : ' +
__e(modifier.get('selected_option') ) +
' </span>\n		<span class="value">' +
__e(accounting.formatMoney(modifier.get('selected_price')) ) +
'</span>\n	</div>\n	';
 });;
__p += '\n	';
};
__p += '\n</td>\n\n';
 if(item_type == 'tournament' || item_type == 'giftcard' || item_type == 'item_kit'){ ;
__p += '\n<td class="price">\n	<p class="form-control-static">' +
__e(accounting.formatMoney(unit_price,'')) +
'</p>\n</td>	\n';
 }else{ ;
__p += '\n<td class="price">\n	<input name="price" type="number" novalidate step="any" class="item-price form-control auto-highlight" value="' +
__e(accounting.formatNumber(unit_price, 2, '')) +
'" />\n</td>	\n';
 } ;
__p += '\n\n';
 if(erange_size > 0 || item_type == 'giftcard' || item_type == 'statement'){ ;
__p += '\n<td class="qty">\n	<p class="form-control-static">' +
__e(quantity) +
'</p>\n</td>\n<td class="discount">\n	<input name="discount" type="number" class="item-discount form-control auto-highlight" value="' +
__e(accounting.formatNumber(discount_percent, 2)) +
'" />\n</td>\n';
 }else if(item_type == 'pass'){ ;
__p += '\n<td class="qty">\n	<p class="form-control-static">' +
__e(quantity) +
'</p>\n</td>\n<td class="discount">\n	<input name="discount" type="number" class="item-discount form-control auto-highlight" value="' +
__e(accounting.formatNumber(discount_percent, 2)) +
'" />\n</td>\n';
 }else if(item_type == 'punch_card' ||
	item_type == 'invoice' ||
	item_type == 'member_account' || 
	item_type == 'customer_account' || 
	item_type == 'invoice_account' || 
	item_type == 'tournament'){ ;
__p += '\n<td class="qty">\n	<p class="form-control-static">' +
__e(quantity) +
'</p>\n</td>\n<td class="discount">\n	<p class="form-control-static">' +
__e(accounting.formatNumber(discount_percent, 2)) +
'</p>\n</td>\n';
 }else{ ;
__p += '\n<td class="qty">\n	<input name="quantity" type="number" class="item-quantity form-control auto-highlight" value="' +
__e(quantity) +
'" />\n</td>\n<td class="discount">\n	<input name="discount" type="number" class="item-discount form-control auto-highlight" value="' +
__e(accounting.formatNumber(discount_percent, 2)) +
'" />\n</td>\n';
 } ;
__p += '\n\n';
 if(food_and_beverage == 1){ ;
__p += '\n<td class="total">' +
__e(accounting.formatMoney(subtotal + sides_subtotal)) +
'</td>\n';
 }else{ ;
__p += '\n<td class="total">' +
__e(accounting.formatMoney(subtotal)) +
'</td>	\n';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/cart_totals.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="item-count pull-left">\n	<h4>' +
__e(num_items ) +
' Items</h4>\n    ';
 if (!hide_taxable) { ;
__p += '\n	<div class="checkbox">\n		<label>\n			<input type="checkbox" name="taxable" id="taxable-checkbox" value="1" ';
 if(taxable){ ;
__p += 'checked';
 } ;
__p += ' \n			';
 if(!App.data.user.get('acl').can('update', 'Pos/Taxable')){ ;
__p += 'disabled';
 }  ;
__p += ' />\n			Taxable\n		</label>\n	</div>\n    ';
 } ;
__p += '\n</div>\n<div class="totals pull-right">\n	<ul class="charges">\n		<li>\n			<span class="amount">' +
__e(accounting.formatMoney(subtotal - service_fees_subtotal) ) +
'</span>\n			<span class="name">Subtotal</span>	\n		</li>\n		';
 if(service_fees_subtotal > 0){ ;
__p += '\n		<li>\n			<span class="amount">' +
__e(accounting.formatMoney(service_fees_subtotal) ) +
'</span>\n			<span class="name">Service Fees</span>\n		</li>\n		';
 }
		if(credit_card_fees > 0){ ;
__p += '\n		<li>\n			<span class="amount">' +
__e(accounting.formatMoney(credit_card_fees) ) +
'</span>\n			<span class="name">Credit Card Fee</span>\n		</li>\n		';
 } ;
__p += '\n		\n		';

		if(taxes.length > 0 && total > subtotal) {
		_.each(taxes, function(tax){
		  if(!tax.amount)return;
		;
__p += '\n		<li>\n			<span class="amount">' +
__e(accounting.formatMoney(tax.amount) ) +
'</span>\n			<span class="name">' +
__e(tax.name) +
' - ' +
__e(accounting.formatNumber(tax.percent, 3)) +
'%</span>	\n		</li>	\n		';
 }); } ;
__p += '\n	</ul>\n	<h4 class="total">\n		<span class="amount">' +
__e(accounting.formatMoney(total) ) +
'</span>\n		<label>Total</label> \n	</h4>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/cash_drawer_select.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content select-cash-drawer">\n	<div class="modal-header">\n		<h4 class="modal-title" style="text-align: center">Select Cash Drawer</h4>\n	</div>\n	<div class="modal-body">\n		<div class="container-fluid">\n			<div class="row">\n				<div class="col-xs-12">\n					<ul class="list-group cash-drawers">\n					  	<li class="list-group-item cash-drawer ';
 if((drawer_1 && !closing) || (!drawer_1 && closing)){ ;
__p += 'disabled';
 } ;
__p += '" data-number="1">\n					  		Drawer 1\n					  		';
 if(drawer_1){ ;
__p += '\n					  		<span class="label label-warning">IN USE</span>\n					  		<span class="text-align-right pull-right">' +
__e(drawer_1.employee.get('first_name')) +
' ' +
__e(drawer_1.employee.get('last_name')) +
'</span>\n					  		';
 } ;
__p += '\n					  	</li>\n					  	<li class="list-group-item cash-drawer ';
 if((drawer_2 && !closing) || (!drawer_2 && closing)){ ;
__p += 'disabled';
 } ;
__p += '" data-number="2">\n					  		Drawer 2\n					  		';
 if(drawer_2){ ;
__p += '\n					  		<span class="label label-warning">IN USE</span>\n					  		<span class="text-align-right pull-right">' +
__e(drawer_2.employee.get('first_name')) +
' ' +
__e(drawer_2.employee.get('last_name')) +
'</span>\n					  		';
 } ;
__p += '\n					  	</li>\n					</ul>\n				</div>\n			</div>\n		</div>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/change_due_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content change-due-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Change Due</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<div class="row">\n			<div class="col-lg-12">\n				<h3 style="text-align: center;">Change Due</h3>\n				<h1 style="text-align: center;">' +
__e(accounting.formatMoney( Math.abs(amount) )) +
'</h1>\n			</div>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default close-window">Close</button>		\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/confirm_sale_delete_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 var identifier = sale_id;
if(number != null){
identifier = number;
}
;
__p += '\r\n<div class="modal-content sale-delete-confirm-window">\r\n    <div class="modal-header">\r\n        <h4 class="modal-title">Delete Sale #' +
__e(identifier) +
'</h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <form class="form-horizontal" role="form">\r\n            <input type="hidden" value="';
sale_id;
__p += '" id="sale-id" />\r\n            <div class="form-group">\r\n                <label for="deletion-reason" class="col-md-3 control-label">Reason</label>\r\n                <div class="col-md-8">\r\n                    <select class="form-control" name="reason" id="deletion-reason">\r\n                        ';
 for (var i in refund_reasons.attributes) { ;
__p += '\r\n                        <option value="' +
__e(i) +
'">' +
__e(refund_reasons.attributes[i]) +
'</option>\r\n                        ';
 } ;
__p += '\r\n                    </select>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="deletion-comment" class="col-md-3 control-label">Comment</label>\r\n                <div class="col-md-8">\r\n                    <textarea class="form-control" value="" name="comment" id="deletion-comment" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="col-md-12 control-label">Deleting this sale will refund all associated payments. Are you sure?</div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <div class="modal-footer">\r\n        <button type="button" data-dismiss="modal" class="btn btn-danger pull-left confirm-delete" id="confirm-delete">Confirm Delete</button>\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/customer_details.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 var status = {
	"0": '',
	"3": 'ok',
	"1": 'danger',
	"2": 'warn'
} ;
__p += '\n<div class="row">\n	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-8">\n		<div class="row">\n			<div class="info col-xs-12">\n				<div class="photo-container col-xs-12 col-sm-5 col-md-4 col-lg-2">\n					<img src="' +
__e(photo) +
'" class="photo ' +
__e(status[status_flag]) +
'" />\n					';
 if(!image_id || image_id == 0){ ;
__p += '<button class="btn btn-default btn-xs add-photo" style="width:100px;">Add Photo</button>';
 } ;
__p += '\n				</div>\n				<div class="customer-data-container col-xs-12 col-sm-7 col-md-7 col-lg-6">\n					<h3>\n						' +
__e(first_name) +
' ' +
__e(last_name) +
'\n						<i id="edit-person" class="glyphicon glyphicon-pencil edit-customer" style="margin-left: 5px; font-size: 18px;"></i>\n					</h3>\n					';
 if(groups.length > 0){ ;
__p += '\n					<span class="contact">\n						';
 if(groups.length > 0){
						_.each(groups.models, function(group){ ;
__p += '\n						<span class="label label-default group">' +
__e(group.get('label')) +
'</span>\n						';
 }) } ;
__p += '\n					</span>\n					';
 } ;
__p += '\n					<span class="contact">\n						';
 if(_.isBlank(account_number)){ print('<a class="text-muted edit-customer">Add account number</a>') }else{ ;
__p +=
__e('#'+account_number);
 };
__p += '\n					</span>\n					<span class="contact">';
 if(typeof phone_number !== 'string' || phone_number.length < 7 || phone_number.length > 18){ print('<a class="text-muted edit-customer">Add phone number</a>') }else{ ;
__p +=
__e(phone_number);
 };
__p += '</span>\n					<span class="contact">';
 if(typeof email !== 'string' || email.length < 3){ print('<a class="text-muted edit-customer">Add email</a>') }else{ ;
__p +=
__e(email);
};
__p += '</span>\n					<span class="contact" style="display: inline-flex;">\n						';
 if(typeof birthday !== 'string' || birthday.length < 6 ){
							print('<a class="text-muted edit-customer">Add birthday</a>')
						}else{ ;
__p += '\n							<div class="customer-birthday">DOB: ' +
((__t = (
								(new Date(new Date(birthday).getTime() + (1000*60*60*12))).toLocaleDateString()
							)) == null ? '' : __t) +
'</div>\n							<div class="customer-age">' +
((__t = ( (function(){
								var age = Math.floor((new Date - new Date(birthday))/(60000*60*24*365));
								return isNaN(age)?'':'('+age+' yrs)';}()) )) == null ? '' : __t) +
'\n							</div>\n						';
};
__p += '\n					</span>\n				</div>\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-md-12">\n				<ul class="nav nav-pills customer-details" role="tablist">\n					<li role="presentation" class="active">\n						<a href="#customer-' +
__e(person_id) +
'-passes" aria-controls="passes" role="tab" data-toggle="pill">Passes <span class="badge">' +
__e(passes.length) +
'</span></a>\n					</li>\n					<li role="presentation">\n						<a href="#customer-' +
__e(person_id) +
'-rainchecks" aria-controls="rainchecks" role="tab" data-toggle="pill">Rainchecks <span class="badge">' +
__e(rainchecks.length) +
'</span></a>\n					</li>\n					<li role="presentation">\n						<a href="#customer-' +
__e(person_id) +
'-gift-cards" aria-controls="gift cards" role="tab" data-toggle="pill">Gift Cards <span class="badge">' +
__e(gift_cards.length) +
'</span></a>\n					</li>\n					<li role="presentation">\n						<a href="#customer-' +
__e(person_id) +
'-recent-transactions" aria-controls="recent transactions" role="tab" data-toggle="pill">Recent Transactions <span class="badge">' +
__e(recent_transactions.length) +
'</span></a>\n					</li>\n				</ul>\n				<div class="tab-content">\n					<div role="tabpanel" class="tab-pane customer-passes active" id="customer-' +
__e(person_id) +
'-passes"></div>\n					<div role="tabpanel" class="tab-pane" id="customer-' +
__e(person_id) +
'-rainchecks">\n						<ul class="customer-info">\n							';
 if(rainchecks.length > 0){
							_.each(rainchecks.models, function(raincheck){ ;
__p += '\n							<li class="customer-raincheck" data-raincheck-id="' +
__e(raincheck.get('raincheck_id')) +
'">					\n								<button class="btn btn-default btn-sm pay" style="';
 if(raincheck.isExpired()){ ;
__p += 'visibility: hidden;';
 } ;
__p += '">Pay</button>\n								<span class="number">#' +
__e(raincheck.get('raincheck_number')) +
'</span>\n								<span class="date">Issued ' +
__e(moment(raincheck.get('date_issued'), 'YYYY-MM-DD HH:mm:ss').format('M/D/YY')) +
'</span>\n								<strong class="customer-info-transaction-total pull-right">' +
__e(accounting.formatMoney(raincheck.get('total'))) +
'</strong>\n								\n								';
 if(raincheck.isExpired()){ ;
__p += '\n								<span class="label label-danger customer-info-label pull-right">Expired ' +
__e(moment(raincheck.get('expiry_date')).format('M/D/YY')) +
'</span>\n								';
 }else if(raincheck.hasExpiration()){ ;
__p += '\n								<span class="label label-success customer-info-label pull-right">Expires ' +
__e(moment(raincheck.get('expiry_date')).format('M/D/YY')) +
'</span>\n								';
 }else{ ;
__p += '\n								<span class="label label-success customer-info-label  pull-right">No Expiration</span>\n								';
 } ;
__p += '\n							</li>\n							';
 }) }else{ ;
__p += '\n							<li class="text-muted">No rainchecks</li>\n							';
 } ;
__p += '\n						</ul>\n					</div>\n					<div role="tabpanel" class="tab-pane" id="customer-' +
__e(person_id) +
'-gift-cards">\n						<ul class="customer-info">\n							';
 if(gift_cards.length > 0){
							_.each(gift_cards.models, function(gift_card){ ;
__p += '\n							<li class="customer-gift-card" data-gift-card-id="' +
__e(gift_card.get('giftcard_id')) +
'">					\n								<button class="btn btn-default btn-sm pay ';
if(gift_card.isExpired() || gift_card.get('value') <= 0){;
__p += 'disabled';
 } ;
__p += '">Pay</button>\n								<span class="number">#' +
__e(gift_card.get('giftcard_number')) +
'</span>\n								<strong class="customer-info-transaction-total pull-right">' +
__e(accounting.formatMoney(gift_card.get('value'))) +
'</strong>\n\n								';
 if(gift_card.isExpired()){ ;
__p += '\n								<span class="label label-danger customer-info-label pull-right">Expired ' +
__e(moment(gift_card.get('expiration_date')).format('M/D/YY')) +
'</span>\n								';
 }else if(gift_card.hasExpiration()){ ;
__p += '\n								<span class="label label-success customer-info-label pull-right">Expires ' +
__e(moment(gift_card.get('expiration_date')).format('M/D/YY')) +
'</span>\n								';
 }else{ ;
__p += '\n								<span class="label label-success customer-info-label pull-right">No Expiration</span>\n								';
 } ;
__p += '\n							</li>\n							';
 }) }else{ ;
__p += '\n							<li class="text-muted">No gift cards</li>\n							';
 } ;
__p += '\n						</ul>\n					</div>\n					<div role="tabpanel" class="tab-pane" id="customer-' +
__e(person_id) +
'-recent-transactions">\n						<ul class="customer-info">\n							';
 if(recent_transactions.length > 0){
							_.each(recent_transactions.models, function(recent_transaction){ ;
__p += '\n							<li class="customer-recent-transaction" data-sale-id="' +
__e(recent_transaction.get('sale_id')) +
'" title="View receipt">					\n								<span class="number">#' +
__e(recent_transaction.get('number')) +
'</span>\n								<span class="date" title="' +
__e(moment(recent_transaction.get('sale_time')).format('MM/DD/YYYY @ h:mma')) +
'">' +
__e(moment(recent_transaction.get('sale_time')).fromNow()) +
'</span>\n								<strong class="customer-info-transaction-total pull-right ';
if(recent_transaction.get('total') < 0){ ;
__p += 'text-danger';
 } ;
__p += '">' +
__e(accounting.formatMoney(recent_transaction.get('total'))) +
'</strong>\n								<span class="num-items pull-right">' +
__e(recent_transaction.get('items').length) +
' Items</span>\n							</li>\n							';
 }) }else{ ;
__p += '\n							<li class="text-muted">No recent transactions</li>\n							';
 } ;
__p += '\n						</ul>\n					</div>\n				</div>				\n			</div>\n		</div>\n	</div>\n	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">\n		<div class="balances">\n			<ul class="balances">\n				<li>\n					<span class="amount ';
 if(member_account_balance < 0){ ;
__p += 'text-danger';
 } ;
__p += '">' +
__e(accounting.formatMoney(member_account_balance)) +
'</span>\n					<a href="" id="pay-member-account" class="name ';
 if(member_account_balance < 0){ ;
__p += 'text-danger';
 } ;
__p += '">\n						' +
__e(member_account_label) +
'\n					</a>\n				</li>\n				<li>\n					<span class="amount ';
 if(account_balance < 0){ ;
__p += 'text-danger';
 } ;
__p += '">' +
__e(accounting.formatMoney(account_balance)) +
'</span>\n					<a href="" id="pay-customer-account" class="name ';
 if(account_balance < 0){ ;
__p += 'text-danger';
 } ;
__p += '">\n						' +
__e(customer_account_label) +
'\n					</a>\n				</li>\n				<li>\n					<span class="amount ';
 if(invoice_balance < 0){ ;
__p += 'text-danger';
 } ;
__p += '">' +
__e(accounting.formatMoney(invoice_balance)) +
'</span>\n					<a href="" id="pay-invoice-account" class="name ';
 if(invoice_balance < 0){ ;
__p += 'text-danger';
 } ;
__p += '">Open Invoices</a>\n				</li>\n				<li>\n					<span class="amount">' +
__e(accounting.formatNumber(loyalty_points)) +
'</span>\n					<span class="name">Loyalty Points</span>\n				</li>\n				';

				if(!_.isNull(minimum_charges) && minimum_charges.length > 0){
				_.forEach(minimum_charges.models, function(minimum){ ;
__p += '\n				<li>\n					<span class="amount ';
 if(minimum.get('totals') < minimum.get('minimum_amount')){ print('text-danger'); } ;
__p += '">\n						' +
__e(accounting.formatMoney(minimum.get('total')) ) +
' / ' +
__e(accounting.formatMoney(minimum.get('minimum_amount')) ) +
' </span>\n					<span class="name">' +
__e(minimum.get('name') ) +
'</span>\n				</li>\n				';
 }); } ;
__p += '\n			</ul>\n		</div>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/customer_pass.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 
var btn_class = 'btn-default';
var btn_text = 'Apply';

if(is_applied){
	btn_class = 'btn-primary applied';
	btn_text = 'Applied';
} ;
__p += '					\n<button class="btn btn-sm apply-pass ' +
__e(btn_class) +
'" ';
 if(!is_valid){ ;
__p += 'style="visibility: hidden"';
 } ;
__p += '>\n	' +
__e(btn_text) +
'\n</button>					\n<div class="pass-details">\n	<span class="restrictions-valid">\n	';
 if(is_valid){ ;
__p += '\n		<span class="fa fa-check-circle text-success"></span>\n	';
 }else{ ;
__p += '\n		<span class="fa fa-times-circle text-danger"></span>\n	';
 } ;
__p += '\n	</span>\n	<span class="pass-name">\n		<strong>\n			';
 if(multi_customer == 1 && customer){ ;
__p += '\n			' +
__e(customer.first_name+' '+customer.last_name) +
' - \n			';
 } ;
__p += '\n			' +
__e(name) +
'\n		</strong> \n		';
 if(rules && rules.length > 0){ ;
__p += '<a href="" class="show-pass-details">Show Details</a>';
 } ;
__p += '\n         - Used ' +
__e(times_used) +
' time';
 if(times_used != 1){;
__p += 's';
};
__p += '\n	</span>\n	';
 
	if(end_date && end_date != '0000-00-00 00:00:00'){ 
	var expiration = moment(end_date,'YYYY-MM-DD HH:mm:ss'); ;
__p += '\n	';
 if(expiration){ ;
__p += '\n	<span class="pull-right">\n		Expires \n		<span class="expiration ';
if(expiration.isBefore()){ print('text-danger expired') } ;
__p += '">\n			' +
__e(expiration.format('M/D/YY') ) +
'\n		</span>\n	</span>\n	';
 } } ;
__p += '\n</div>\n<div class="pass-restrictions-container" ';
 if(!show_rules){ ;
__p += 'style="display: none"';
 } ;
__p += '></div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/customer_pass_rule.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 
var btn_class = 'btn-default';
var btn_text = 'Apply';

if(is_applied){
	btn_class = 'btn-primary applied';
	btn_text = 'Applied';
} ;
__p += '						\n<button class="btn btn-sm apply-pass-rule ' +
__e(btn_class) +
'" ';
 if(!is_valid){ ;
__p += 'style="visibility: hidden"';
 } ;
__p += '>\n	' +
__e(btn_text) +
'\n</button>\n\n';
 if(is_valid){ ;
__p += '\n<span class="fa fa-check-circle text-success"></span>\n';
 }else{ ;
__p += '\n<span class="fa fa-times-circle text-danger"></span>\n';
 } ;
__p += '\n' +
__e(name) +
'\n';
 
if(App.data.course.get('price_classes') && App.data.course.get('price_classes')[price_class_id]){
	print(' - ' + App.data.course.get('price_classes')[price_class_id]);
} ;
__p += '\n';
 if(typeof times_used != 'undefined' ){ ;
__p += '\n- Used ' +
__e(times_used) +
' time';
 if(times_used != 1){;
__p += 's';
};
__p += '\n';
 } ;
__p += '\n\n<!--  -->\n';
 if(typeof accurate != 'undefined' && !accurate){ ;
__p += '\n<a href="#" class="tooltiplink" title="These are the uses since Jan 24, 2017"> &#42;</span></a>\n\n';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/customer_signature.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content customer-signature">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h1 class="payment-total text-center">' +
__e(accounting.formatMoney(amount)) +
'</h1>\n	</div>\n	<div class="modal-body" style="position: relative; height: 350px">\n		<canvas style="position: relative;"></canvas>\n		<br>\n		<h3 class="agreement">I agree to paying the above amount</h3>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default clear pull-left">Clear</button>	\n		<button type="button" class="btn btn-primary save">Save</button>	\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/customer_tab.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<a href="#customer_' +
__e(person_id) +
'" data-toggle="tab">' +
__e(last_name) +
', ' +
__e(first_name) +
'</a>\n<span class="delete text-danger">&times;</span>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/customers.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="col-lg-12">\n	<div class="bg-primary clearfix" style="min-height: 55px">\n		<div class="col-lg-4" style="padding: 10px 0px 0px 10px;">\n			<div class="input-group">\n				<input type="text" class="form-control typeahead-customer" id="customer-search" value="" placeholder="Search customers..." />\n				<span class="input-group-btn" style="padding-right: 15px;">\n					<button id="add-customer" style="top: -1px;" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span></button>\n				</span>\n			</div>\n		</div>\n		<div class="col-lg-8" style="padding: 0px;">\n			<ul id="customer-tabs" class="nav nav-tabs"></ul>\n		</div>\n	</div>\n</div>\n<div class="col-lg-12">\n	<div class="inner-content tab-content" id="customer-details"></div>\n</div>\n\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/edit_quick_button_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content edit-quick-button-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">';
 if(is_new){ print('New') }else{ print('Edit') };
__p += ' Quick Button</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<label for="quick-button-label" class="col-md-3 control-label">Label</label>\n				<div class="col-md-5">\n					<input class="form-control" type="text" value="' +
__e(display_name) +
'" name="label" id="quick-button-label" />\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="quick-button-color" class="col-md-3 control-label">Color</label>\n				<div class="col-md-9">\n					<label class="quickbutton-color">\n						<div class="color bg-primary"></div>\n						<input type="radio" name="color" value="primary" ';
 if(!color || color == 'primary'){ print('checked') } ;
__p += ' />\n					</label>\n					<label class="quickbutton-color">\n						<div class="color bg-success"></div>\n						<input type="radio" name="color" value="success" ';
 if(color == 'success'){ print('checked') } ;
__p += ' />\n					</label>\n					<label class="quickbutton-color">\n						<div class="color bg-warning"></div>\n						<input type="radio" name="color" value="warning" ';
 if(color == 'warning'){ print('checked') } ;
__p += ' />\n					</label>		\n					<label class="quickbutton-color">\n						<div class="color bg-danger"></div>\n						<input type="radio" name="color" value="danger" ';
 if(color == 'danger'){ print('checked') } ;
__p += ' />\n					</label>				\n				</div>\n			</div>			\n			<div class="form-group">\n				<label for="quick-button-item" class="col-md-3 control-label">Add Item</label>\n				<div class="col-md-6">\n					<input class="form-control" type="text" value="" name="item" id="quick-button-item" placeholder="Search items..." />\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="quick-button-item" class="col-md-3 control-label">Items</label>\n				<div class="col-md-6">\n					<ul class="quickbutton-items"></ul>\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		';
 if(!is_new){ ;
__p += '<button type="button" class="btn btn-danger pull-left delete">Delete</button>';
 } ;
__p += '\n		<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/edit_sale_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 var identifier = sale_id; 
if(number != null){
	identifier = number;
} ;
__p += '\n<div class="modal-content edit-sale-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Edit Sale #' +
__e(identifier) +
'</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<label class="col-md-3 control-label">Sales Receipt</label>\n				<div class="col-md-6">\n					<a class="view-receipt">View POS ' +
__e(identifier) +
' Receipt</a>\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="sale-date" class="col-md-3 control-label">Date</label>\n				<div class="col-md-4">\n					<input type="text" class="form-control" id="sale-date" data-date-format="YYYY-MM-DD HH:mm:SS" placeholder="Date" value="' +
__e(sale_time) +
'">\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="sale-customer" class="col-md-3 control-label">Customer</label>\n				<div class="col-md-7">\n					<input type="text" class="form-control" id="sale-customer" placeholder="Customer name" value="';
 if(customer) { ;
__p +=
__e(customer.get('first_name')) +
' ' +
__e(customer.get('last_name'));
 } ;
__p += '">\n					<input type="hidden" id="sale-customer-id" value="';
 if(customer) { ;
__p +=
__e(customer.get('person_id'));
 } ;
__p += '" />\n					<a href="" class="email-receipt">Email Receipt</a>\n				</div>\n			</div>\n			';
 if(customer_note){ ;
__p += '\n            <div class="form-group">\n                <label for="sale-customer-note" class="col-md-3 control-label">\n                	Customer Note<br>\n                	<small class="text-muted" style="font-weight: normal">* Visible to customer on receipts/invoices</small>\n                </label>\n                <div class="col-md-8">\n                    <textarea id="sale-customer-note" name="customer_note" class="form-control" rows="3">' +
__e(customer_note) +
'</textarea>\n                </div>\n            </div>\n			';
 } ;
__p += '\n			<div class="form-group">\n				<label for="sale-employee" class="col-md-3 control-label">Employee</label>\n				<div class="col-md-6">\n					<input type="text" class="form-control" id="sale-employee" placeholder="Employee name" value="';
 if(employee) { ;
__p +=
__e(employee.get('first_name')) +
' ' +
__e(employee.get('last_name'));
 };
__p += '">\n					<input type="hidden" id="sale-employee-id" value="';
 if(employee) { ;
__p +=
__e(employee.get('person_id'));
 } ;
__p += '" />\n				</div>\n			</div>\n            <div class="form-group">\n                <label for="sale-employee" class="col-md-3 control-label">Comment</label>\n                <div class="col-md-8">\n                    <textarea id="sale-comment" name="comment" class="form-control" rows="3">' +
__e(comment) +
'</textarea>\n                </div>\n            </div>\n            ';
 if (refund_reason != '') { ;
__p += '\n            <div class="form-group">\n                <label for="sale-refund-reason" class="col-md-3 control-label">Refund Reason</label>\n                <div class="col-md-8">\n                    <textarea id="sale-refund-reason" name="refund-reason" class="form-control" rows="3">' +
__e(refund_reason) +
'</textarea>\n                </div>\n            </div>\n            <div class="form-group">\n                <label for="sale-refund-comment" class="col-md-3 control-label">Refund Comment</label>\n                <div class="col-md-8">\n                    <textarea id="sale-refund-comment" name="refund-comment" class="form-control" rows="3">' +
__e(refund_comment) +
'</textarea>\n                </div>\n            </div>\n            ';
 } ;
__p += '\n			<div class="form-group">\n				<label class="col-md-3 control-label">&nbsp;</label>\n				<div class="col-md-8 edit-sale-actions">\n					<a class="add-tip">Add Tip</a>\n					<a class="full-return">Issue Full Return</a>\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n        ';
 if(App.data.user.is_admin()) { ;
__p += '\n		<button type="button" class="btn btn-danger delete pull-left">Delete Sale</button>\n        ';
 } ;
__p += '\n		<button type="button" class="btn btn-default save">Save</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/email_receipt_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content email-receipt-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Email Receipt - Sale #' +
__e(number) +
'</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<label for="customer-email" class="col-md-3 control-label">Email Address</label>\n				<div class="col-md-6">\n					<input type="text" class="form-control" id="customer-email" placeholder="Email Address" value="' +
__e(email) +
'">\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-primary send">Send</button>\n		<button type="button" data-dismiss="modal" class="btn btn-default pull-left">Cancel</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/ets_gift_card_refund.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content ets-card-balance-check">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">ETS Giftcard Refund</h4>\n	</div>\n	<div class="modal-body" style="min-height: 300px;"></div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/invoice_details_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content invoice">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Invoice Details</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<label class="col-md-2 control-label">\n					Total\n				</label>\n				<div class="col-md-10">\n					<p class="form-control-static">' +
__e(accounting.formatMoney(params.total)) +
'</p>\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-2 control-label">\n					Paid\n				</label>\n				<div class="col-md-10">\n					<p class="form-control-static">' +
__e(accounting.formatMoney(params.paid)) +
'</p>\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-2 control-label" for="invoice-price">Amount</label>\n				<div class="col-md-3">\n					<input type="text" name="unit_price" value="' +
__e(accounting.formatMoney(unit_price, '')) +
'" class="form-control price" id="invoice-price">\n				</div>\n			</div>\n			<div class="form-group">\n				<div class="col-md-2">\n					Total Receivable\n				</div>\n				<div class="col-md-3">\n					<p class="form-control-static">' +
__e(accounting.formatMoney(params.customer.invoice_balance)) +
'</p>\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/issue_giftcard_ets.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content ets-card-balance-check">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">ETS Giftcard</h4>\n	</div>\n	<div class="modal-body" style="min-height: 300px;"></div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/issue_giftcard_ets_value.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content ets-giftcard-value">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">ETS Giftcard Value</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<label class="col-md-3 control-label" for="giftcard-value">Value</label>\n				<div class="col-md-5">\n					<input type="text" name="value" value="' +
__e(unit_price) +
'" class="form-control" id="giftcard-value"\n						data-bv-notempty data-bv-notempty-message="Giftcard value required">\n				</div>\n			</div>								\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-primary continue">Continue</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/issue_giftcard_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content giftcard">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Giftcard Details</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<div class="col-md-5">\n					<div class="btn-group btn-group-justified action" data-toggle="buttons">\n						<label class="btn btn-default ';
if(params.action == 'new'){ print('active') };
__p += '">\n							<input type="radio" name="action" value="new" autocomplete="off" ';
if(params.action == 'new'){ print('checked') };
__p += '> New\n						</label>\n						<label class="btn btn-default ';
if(params.action == 'reload'){ print('active') };
__p += '">\n							<input type="radio" name="action" value="reload" autocomplete="off" ';
if(params.action == 'reload'){ print('checked') };
__p += '> Reload\n						</label>\n					</div>\n				</div>				\n			</div>\n			<div class="form-group">\n				<label class="col-md-3 control-label" for="giftcard-number">Number <span class="text-danger">*</span></label>\n				<div class="col-md-5">\n					<input type="text" name="giftcard_number" value="' +
__e(params.giftcard_number) +
'" class="form-control" id="giftcard-number"\n						data-bv-notempty data-bv-notempty-message="Giftcard number required">\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-3 control-label" for="giftcard-value">Value <span class="text-danger">*</span></label>\n				<div class="col-md-3">\n					<input type="text" name="value" value="' +
__e(accounting.formatMoney(unit_price, '')) +
'" class="form-control" id="giftcard-value"\n						data-bv-notempty data-bv-notempty-message="Value is required">\n				</div>\n			</div>\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="giftcard-customer">Customer</label>\n				<div class="col-md-5">\n					';
 if(params.customer && params.customer.first_name){
						var customer_name = params.customer.first_name +' '+ params.customer.last_name;
					}else{
						var customer_name = '';
					} ;
__p += '\n					<input type="text" name="customer" value="' +
__e(customer_name) +
'" class="form-control" id="giftcard-customer">\n				</div>\n			</div>\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="giftcard-expiration">Expiration</label>\n				<div class="col-md-4">\n					<input type="text" name="expiration_date" value="' +
__e(params.expiration_date) +
'" class="form-control" id="giftcard-expiration">\n				</div>\n			</div>\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="giftcard-department">Department</label>\n				<div class="col-md-5">\n					<div class="radio">\n						<label>\n							<input type="radio" name="limit_to" class="limit_to" value="department" id="giftcard-department-radio" style="margin-top: 10px;" checked>\n						</label>\n						<input type="text" name="department" value="' +
__e(params.department) +
'" class="form-control" id="giftcard-department">\n					</div>\n				</div>\n			</div>	\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="giftcard-category">Category</label>\n				<div class="col-md-5">\n					<div class="radio">\n						<label>\n							<input type="radio" name="limit_to" class="limit_to" value="category" id="giftcard-category-radio" style="margin-top: 10px;">\n						</label>\n						<input type="text" name="category" class="form-control" value="' +
__e(params.category) +
'" id="giftcard-category" disabled>\n					</div>\n				</div>\n			</div>\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="giftcard-notes">Notes</label>\n				<div class="col-md-9">\n					<textarea class="form-control" name="details" id="giftcard-notes" style="height: 75px;">' +
__e(params.details) +
'</textarea>\n				</div>\n			</div>													\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/issue_pass_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content issue-pass">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Pass Details</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="pass-customer">Customer</label>\n				<div class="col-md-5">\n					';
 if(params.customer && params.customer.first_name){
						var customer_name = params.customer.first_name +' '+ params.customer.last_name;
					}else{
						var customer_name = '';
					} ;
__p += '\n					<input type="text" name="customer" value="' +
__e(customer_name) +
'" class="form-control" id="pass-customer"\n					data-bv-notempty data-bv-notempty-message="Customer is required">\n				</div>\n			</div>\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="pass-start-date">Start Date</label>\n				<div class="col-md-4">\n					<input type="text" name="start_date" value="' +
__e(params.start_date) +
'" class="form-control" id="pass-start-date"\n						data-bv-notempty data-bv-notempty-message="Start date is required">\n				</div>\n			</div>\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="pass-end-date">Expiration</label>\n				<div class="col-md-4">\n					<input type="text" name="end_date" value="' +
__e(params.end_date) +
'" class="form-control" id="pass-end-date"\n						data-bv-notempty data-bv-notempty-message="Expiration date is required">\n				</div>\n			</div>										\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/issue_punch_card_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content punch-card">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Punch Card Details</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<label class="col-md-3 control-label" for="punch-card-number">Number <span class="text-danger">*</span></label>\n				<div class="col-md-5">\n					<input type="text" name="punch_card_number" value="' +
__e(params.punch_card_number) +
'" class="form-control" id="punch-card-number"\n						data-bv-notempty data-bv-notempty-message="Punch card number required">\n				</div>\n			</div>\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="punch-card-customer">Customer</label>\n				<div class="col-md-5">\n					';
 if(params.customer && params.customer.first_name){
						var customer_name = params.customer.first_name +' '+ params.customer.last_name;
					}else{
						var customer_name = '';
					} ;
__p += '\n					<input type="text" name="customer" value="' +
__e(customer_name) +
'" class="form-control" id="punch-card-customer">\n				</div>\n			</div>\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="punch-card-expiration">Expiration</label>\n				<div class="col-md-4">\n					<input type="text" name="expiration_date" value="' +
__e(params.expiration_date) +
'" class="form-control" id="punch-card-expiration">\n				</div>\n			</div>\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label">Items</label>\n				<div class="col-md-8">\n					<ul>\n						';
 _.each(items.models, function(item){ ;
__p += '\n						<li>\n							(' +
__e(item.get('quantity')) +
') ' +
__e(item.get('name')) +
'\n							';
 if(item.has('price_class_name') && item.get('price_class_name') != ''){ ;
__p += '\n							- ' +
__e(item.get('price_class_name') ) +
'\n							';
 } ;
__p += '\n						</li>\n						';
 }) ;
__p += '\n					</ul>\n				</div>\n			</div>			\n			<div class="form-group new-card">\n				<label class="col-md-3 control-label" for="punch-card-notes">Notes</label>\n				<div class="col-md-9">\n					<textarea class="form-control" name="details" id="punch-card-notes" style="height: 75px;">' +
__e(params.details) +
'</textarea>\n				</div>\n			</div>													\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/issue_raincheck_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content raincheck">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Issue Raincheck</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<label class="col-md-3 control-label" for="raincheck-teesheet">Course</label>\n				<div class="col-md-5">\n					' +
((__t = (_.dropdown(teesheet_menu, {"class":"form-control", "name":"teesheet_id", "id":"raincheck-teesheet"}, selected_teesheet))) == null ? '' : __t) +
'\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="raincheck-green-fee" class="col-md-3 control-label" for="raincheck-green-fee">Green Fee</label>\n				<div class="col-md-5">\n					' +
((__t = (_.dropdown(green_fees_menu, {"class":"form-control", "name":"green_fee", "id":"raincheck-green-fee"}, selected_green_fee))) == null ? '' : __t) +
'\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="raincheck-cart-fee" class="col-md-3 control-label" for="raincheck-cart-fee">Cart Fee</label>\n				<div class="col-md-5">\n					' +
((__t = (_.dropdown(cart_fees_menu, {"class":"form-control", "name":"cart_fee", "id":"raincheck-cart-fee"}, selected_cart_fee))) == null ? '' : __t) +
'\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="raincheck-customer" class="col-md-3 control-label">Customer</label>\n				<div class="col-md-5">\n					<input type="text" class="form-control" id="raincheck-customer" placeholder="Search customers..." value="';
 if(customer) { ;
__p +=
__e(customer.get('first_name')) +
' ' +
__e(customer.get('last_name'));
 } ;
__p += '">\n					<input type="hidden" id="raincheck-customer-id" value="';
 if(customer) { ;
__p +=
__e(customer.get('person_id'));
 } ;
__p += '" />\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="raincheck-expiration" class="col-md-3 control-label">Expiration</label>\n				<div class="col-md-4">\n					<input type="text" name="expiration_date" class="form-control" id="raincheck-expiration" data-date-format="YYYY-MM-DD HH:mm:SS" placeholder="" value="' +
__e(expiration_date) +
'">\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="raincheck-split" class="col-md-3 control-label">Split Raincheck</label>\n				<div class="col-md-4">\n					<input type="checkbox" name="split" id="raincheck-split" style="margin-top: 10px;" value="1" data-unchecked-value="0">\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-3 control-label">Players</label>\n				<div class="col-md-5">\n					<div class="btn-group players" data-toggle="buttons">\n						';
 players = parseInt(players) ;
__p += '\n						<label class="btn btn-default ';
if(players == 1){ print('active') } ;
__p += '">\n							<input type="radio" name="players" value="1" autocomplete="off" ';
if(players == 1){ print('checked') } ;
__p += '> 1\n						</label>\n						<label class="btn btn-default ';
if(players == 2){ print('active') } ;
__p += '">\n							<input type="radio" name="players" value="2" autocomplete="off" ';
if(players == 2){ print('checked') } ;
__p += '> 2\n						</label>\n						<label class="btn btn-default ';
if(players == 3){ print('active') } ;
__p += '">\n							<input type="radio" name="players" value="3" autocomplete="off" ';
if(players == 3){ print('checked') } ;
__p += '> 3\n						</label>\n						<label class="btn btn-default ';
if(players == 4){ print('active') } ;
__p += '">\n							<input type="radio" name="players" value="4" autocomplete="off" ';
if(players == 4){ print('checked') } ;
__p += '> 4\n						</label>\n						<label class="btn btn-default ';
if(players == 5){ print('active') } ;
__p += '">\n							<input type="radio" name="players" value="5" autocomplete="off" ';
if(players == 5){ print('checked') } ;
__p += '> 5\n						</label>\n					</div>\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="raincheck-holes" class="col-md-3 control-label">Holes Completed</label>\n				<div class="col-md-2">\n					';
 
					var holes = {};
					var number_holes = 18;

					for(x = 0; x < number_holes; x++){ 
						holes[x] = x;
					} ;
__p += '\n					' +
((__t = (_.dropdown(holes, {"class":"form-control holes", "name":"holes_completed", "id":"raincheck-holes"}, holes_completed))) == null ? '' : __t) +
'\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-md-12">\n					<table class="table">\n						<tbody>\n							<tr>\n								<th>Green Fee</th>\n								<td id="totals-green-fee">' +
__e(accounting.formatMoney(green_fee_price)) +
'</td>\n							</tr>\n							<tr>\n								<th>Cart Fee</th>\n								<td id="totals-cart-fee">' +
__e(accounting.formatMoney(cart_fee_price)) +
'</td>\n							</tr>\n							<tr>\n								<th>Players</th>\n								<td id="totals-players">x ' +
__e(accounting.formatNumber(players)) +
'</td>\n							</tr>\n							<tr>\n								<th>Subtotal</th>\n								<td id="totals-subtotal">' +
__e(accounting.formatMoney(subtotal)) +
'</td>\n							</tr>\n							<tr>\n								<th>Tax</th>\n								<td id="totals-tax">' +
__e(accounting.formatMoney(tax)) +
'</td>\n							</tr>\n							<tr>\n								<th>Total Credit</th>\n								<td id="totals-total">' +
__e(accounting.formatMoney(total)) +
'</td>\n							</tr>\n						</tbody>\n					</table>\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary save">Issue Raincheck</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/item_kit_details_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content item-kit-details">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Item Kit Pricing</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<div class="row">\n			<div class="col-md-12">\n				<table class="table item-kit-items">\n					<thead>\n						<th style="width: 50%">Item</th>\n						<th>Price</th>\n						<th>Qty</th>\n						<th>Total</th>\n					</thead>\n					<tbody>\n					';
 if(items && items.length > 0){ 
					_.each(items.models, function(item, index){ ;
__p += '\n					<tr>\n						<td>\n							' +
__e(item.get('name')) +
'\n						</td>\n						<td>\n							<input style="width: 100px" type="text" data-index="' +
__e(index) +
'" name="items[' +
__e(item.get('item_id')) +
'][unit_price]" value="' +
__e(accounting.formatMoney(item.get('unit_price'), '')) +
'" placeholder="0.00" class="form-control item-price">\n						</td>						\n						<td>\n							' +
__e(accounting.formatNumber(item.get('base_quantity'))) +
'\n						</td>\n						<td>\n							';

							var subtotal = new Decimal(item.get('non_discount_subtotal')).dividedBy(quantity).toDP(2).toNumber(2);
							;
__p += '\n							<strong>' +
__e(accounting.formatMoney(subtotal)) +
'</strong>\n						</td>\n					</tr>					\n					';
 }) }else{ ;
__p += '\n					<tr>\n						<td colspan="6">\n							<h4 style=\'text-align: center\' class="text-muted">No Items Attached</h4>\n						</td>\n					</tr>\n					';
 } ;
__p += '\n					</tbody>\n					<tfoot>\n						<tr>\n							<td colspan="3" style="text-align: right">\n							Item Kit Price\n							</td>\n							<th>\n								<strong>' +
__e(accounting.formatMoney(unit_price)) +
'</strong>\n							</th>\n						</tr>\n					</tfoot>\n				</table>\n			</div>\n		</div>												\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save</button>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/partial_return_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content partial-refund-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Partial Return - Sale #' +
__e(number) +
'</h4>\n	</div>\n    <div class="modal-body container-fluid">\n        <div class="row">\n            <label for="refund-reason" class="col-md-3 control-label">Reason</label>\n            <div class="col-md-8">\n                <select class="form-control" name="reason" id="refund-reason">\n                    ';
 for (var i in refund_reasons.attributes) { ;
__p += '\n                    <option value="' +
__e(i) +
'">' +
__e(refund_reasons.attributes[i]) +
'</option>\n                    ';
 } ;
__p += '\n                </select>\n            </div>\n        </div>\n        <div class="row">\n            <label for="refund-comment" class="col-md-3 control-label">Comment</label>\n            <div class="col-md-8">\n                <textarea class="form-control" value="" name="comment" id="refund-comment" />\n            </div>\n        </div>\n    </div>\n	<div class="well" style="display: block; border: none; border-bottom: 1px solid #E0E0E0; border-radius: 0px;">\n		<div class="row">\n			<div class="col-sm-6">\n				<div class="row">\n					<div class="col-xs-6"><strong>Sale Total</strong></div> \n					<div class="col-xs-6">' +
__e(accounting.formatMoney(total) ) +
'</div>\n				</div>\n				<div class="row">\n					<div class="col-xs-6"><strong>Refunded</strong></div>\n					<div class="col-xs-6">' +
__e(accounting.formatMoney(refunded) ) +
'</div>\n				</div>\n				<div class="row">\n					<div class="col-xs-6"><strong>Leftover</strong></div>\n					<div class="col-xs-6">' +
__e(accounting.formatMoney(leftover) ) +
'</div>\n				</div>\n			</div>\n			<div class="col-sm-6">\n				<div class="row">\n					<label for="customer-email" class="col-md-4 control-label">Amount</label>\n					<div class="col-md-6">\n						<input type="number" class="form-control amount" placeholder="0.00" value="' +
__e(accounting.formatMoney(leftover, '')) +
'">\n					</div>\n				</div>\n			</div>			\n		</div>\n	</div>	\n	<div class="modal-body container-fluid">\n		<div class="row refund-types">\n			<div class="col-sm-4">\n				<button class="btn btn-default cash">Cash</button>\n				<button class="btn btn-default credit-card">Credit Card</button>\n			</div>\n			<div class="col-sm-4">\n				';
 if(!customer){ var disabled = ' disabled'; }else{ var disabled = ''; } ;
__p += '\n				<button class="btn btn-default member-account' +
__e(disabled) +
'">Member Account</button>\n				<button class="btn btn-default customer-account' +
__e(disabled) +
'">Customer Credit</button>\n			</div>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button type="button" data-dismiss="modal" class="btn btn-default pull-left">Cancel</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/payment.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<a href="" class="delete text-danger">&times;</a>\n<span class="desc">' +
__e(description) +
'</span>\n<span class="amount">\n	';
 if(type != 'credit_card_save'){ ;
__p += '\n	' +
__e(accounting.formatMoney(amount)) +
'\n	';
 }else{ ;
__p += '\n	<i class="glyphicon glyphicon-check"></i>\n	Saved\n	';
 } ;
__p += '\n</span>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/payment_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var account_payments = 'disabled';
var member_account_payments = 'disabled';
var loyalty_payment = 'disabled';
var punch_card = '';
var coupon = '';
var raincheck = '';

if(mode == 'return'){
	punch_card = 'disabled';
	coupon = 'disabled';
	raincheck = 'disabled';
}
if(selected_customer){
	account_payments = '';
	if(selected_customer.get('member') == 1){
		member_account_payments = '';
	}
}
if(selected_customer && selected_customer.get('use_loyalty') == 1 && mode == 'sale'){
	loyalty_payment = '';
}
if(typeof refund_reasons == 'undefined'){
    refund_reasons = {};
}
;
__p += '\n<div class="modal-content payments-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Payments</h4>\n	</div>\n	<div class="modal-body">\n        <div class="container-fluid">\n	        ';
 if(mode == 'return') { ;
__p += '\n	        <div class="row">\n	            <label for="refund-reason" class="col-md-3 control-label">Return Reason</label>\n	            <div class="col-md-4">\n	                <select class="form-control" name="reason" id="refund-reason">\n	                    ';
 for (var i in refund_reasons.attributes) { ;
__p += '\n	                    <option value="' +
__e(i) +
'">' +
__e(refund_reasons.attributes[i]) +
'</option>\n	                    ';
 } ;
__p += '\n	                </select>\n	            </div>\n	        </div>\n	        <div class="row">\n	            <label for="refund-comment" class="col-md-3 control-label">Comment</label>\n	            <div class="col-md-8" style="padding-bottom: 20px">\n	                <textarea class="form-control" value="" name="comment" id="refund-comment" />\n	            </div>\n	        </div>\n	        ';
 } ;
__p += '\n			<div class="row">\n				<div class="col-md-6 col-md-push-6 payments">			\n					<div class="row">\n						<div class="form-group col-md-10 col-md-offset-2 payment-amount">\n							<label class="amount" for="payment_amount">Amount</label>\n							<div class="input-group amount">\n								<div class="input-group-addon">$</div>\n								<input type="number" name="amount" id="payment_amount" class="form-control amount input-lg" value="' +
__e(accounting.formatNumber(total_due, 2, '')) +
'" autocomplete="off"/>\n							</div>\n						</div>\n					</div>\n					<div class="row">\n						<ul class="col-md-12 payments" id="window-payments"></ul>\n						<div class="form-group col-md-12 total-due">\n							<hr />\n							<label>Total Due</label>\n							<h3 class="total">' +
__e(accounting.formatMoney(total_due)) +
'</h3>\n						</div>	\n					</div>				\n				</div>				\n				<div class="col-md-5 col-md-pull-6" id="payment-types">\n					<div class="row" id="saved-credit-cards"></div>\n					<div class="row">\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 pay" id="payment_cash">Cash</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12" id="payment_check" href="">Check</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12" id="payment_credit_card" href="">Credit Card</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12" id="payment_giftcard" href="">Gift Card</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 ' +
__e(raincheck) +
'" id="payment_raincheck" href="">Raincheck</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 ' +
__e(punch_card) +
'" id="payment_punch_card" href="">Punch Card</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 ' +
__e(coupon) +
'" id="payment_coupon" href="">Coupon</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 ' +
__e(loyalty_payment) +
'" id="payment_loyalty" href="">Loyalty Points</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 pay ' +
__e(member_account_payments) +
'" id="payment_member_account" href="">' +
__e(member_account_label) +
'</button>\n						<button class="btn btn-default btn-lg col-sm-6 col-xs-12 pay ' +
__e(account_payments) +
'" id="payment_customer_account" href="">' +
__e(customer_account_label) +
'</button>\n	                    ';
 for (var i = 0; i < custom_payments.length; i++) { ;
__p += '\n	                    <button class="btn btn-default btn-lg col-sm-6 col-xs-12 pay custom_payment" id="' +
__e(custom_payments.models[i].get('customPaymentType')) +
'" href="">' +
__e(custom_payments.models[i].get('label')) +
'</button>\n	                    ';
 } ;
__p += '\n					</div>\n				</div>		\n			</div>\n		</div>\n		<div class="row" style="margin-top: 15px">\n			<div class="col-md-12">\n				<label for="refund-comment" class="control-label">Customer Note</label>\n				<textarea name="customer_note" class="form-control" id="customer-note">' +
__e(customer_note) +
'</textarea>\n				<span class="help-block">This note will be displayed on the sale receipt and in sale reports</span>\n			</div>\n		</div>\n	</div>\n	<div class="modal-footer">\n        <button type="button" class="btn btn-default hidden-xs pull-left" data-dismiss="modal">Close</button>\n        <button type="button" class="btn btn-default hidden-xs pull-left" id="cancel-credit-card-payment" style="display:none; z-index:1000000; position:relative;">Cancel</button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/payment_window_billing_address.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<form role="form" id="billing_address_form">\n	<input type="hidden" name="amount" id="payment_amount" value="' +
__e(amount) +
'">\n	<div class="row">\n		<div class="form-group col-md-4">\n			<label for="credit_card_address" class="control-label hidden-xs">Billing Address</label>\n			<input type="text" class="form-control address" placeholder="Billing address" name="address" id="credit_card_address"\n				data-bv-notempty data-bv-notempty-message="Address is required" />											\n		</div>\n		<div class="form-group col-md-2">\n			<label for="credit_card_zipcode" class="control-label hidden-xs">Billing Zipcode</label>\n			<input type="text" class="form-control zipcode" placeholder="Billing zipcode"  name="zip" id="credit_card_zipcode" \n				data-bv-notempty data-bv-notempty-message="Zipcode is required" />											\n		</div>\n	</div>\n	<div class="row">\n		<div class="form-group col-md-12">\n			<button class="btn btn-primary save-billing-address">Next</button>\n		</div>\n	</div>\n</form>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/payment_window_credit_card_swipe.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row">\n	<div class="col-md-12">\n		<h3>Swipe Card...</h3>\n	</div>	\n</div>\n<div class="row">\n	<div class="col-md-4">\n		<input name="card_data" id="credit-card-data" type="password" autocomplete="off" value="" class="form-control">\n		<input name="amount" id="payment_amount" class="amount" type="hidden" autocomplete="off" value="' +
__e(amount) +
'">\n	</div>\n</div>\n<div class="row">\n	<div class="col-md-4">\n		';
 if(show_manual_entry){ ;
__p += '\n		<button class="btn btn-default manual-entry" style="margin-top: 10px;">Manual Entry</button>\n		';
 } ;
__p += '\n		<button class="btn btn-primary submit-payment" style="margin-top: 10px;">Submit Payment</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/payment_window_number.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="form-group">\n	<label for="payment-number">Number</label>\n	<input type="text" name="number" id="payment-number" class="form-control number" value="" autocomplete="off">\n</div>\n<div class="form-group">\n	<button class="btn btn-default back">Back</button>\n	<button class="btn btn-primary submit-payment pay">Submit</button>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/pos_header_stats.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="header-stat">\n    <div class="stat-value">' +
__e(accounting.formatMoney(total_sales) ) +
'</div>\n    <div class="stat-label">Total</div>\n</div>\n<div class="header-stat">\n    <div class="stat-value">' +
__e(accounting.formatMoney(average_sale) ) +
'</div>\n    <div class="stat-label">Average Sale</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/quick_button.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 var btn_class = 'btn-primary';
if(color && color != ''){ 
	btn_class = 'btn-'+color;
} ;
__p += '\n';
 if(items && items.models && items.models.length > 1){ ;
__p += '\n<div class="dropdown">\n	<button class="btn ' +
__e(btn_class) +
' btn-block quick-button" type="button" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">\n		' +
__e(display_name) +
'\n		<span class="caret"></span>\n	</button>\n	<ul class="dropdown-menu quick-button-sub-menu" role="menu">\n		';
 _.each(items.models, function(item){ ;
__p += '\n		';
 if(!item){ return true } ;
__p += '\n		<li role="presentation">\n			<button class="btn btn-default btn-block quick-button-item" role="menuitem" tabindex="-1" href="#" data-item-id="' +
__e(item.cid) +
'">' +
__e(item.get('name')) +
'</button>\n		</li>\n		';
 }) ;
__p += '\n	</ul>\n</div>\n';
 }else if(items.models.length == 1){ ;
__p += '\n<button class="btn btn-block ' +
__e(btn_class) +
' quick-button-item" data-item-id="' +
__e(items.at(0).cid) +
'">' +
__e(display_name) +
'</button>\n';
 } ;
__p += '\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/quick_button_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<span class="name">' +
__e(name) +
'</span>\n<a href="#" class="delete text-danger">&times;</a>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/quick_buttons.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row">\n	<div class="col-md-12">\n		<a id="issue-raincheck" class="btn btn-primary">Issue Raincheck</a>\n	</div>\n</div>\n<div class="row" style="margin-top: 15px;">\n	<div class="col-md-12">\n		<div class="btn-group btn-group-justified quick-button-tabs" role="tablist">\n			<div class="btn-group" role="group">\n				<a type="button" href="#quick-buttons-1" class="btn btn-default ';
 if(tab == '1'){ print('active') } ;
__p += '" data-tab-num="1" role="tab" data-toggle="tab">1</a>\n			</div>\n			<div class="btn-group" role="group">\n				<a type="button" href="#quick-buttons-2" class="btn btn-default ';
 if(tab == '2'){ print('active') } ;
__p += '" data-tab-num="2" role="tab" data-toggle="tab">2</a>\n			</div>\n			<div class="btn-group" role="group">\n				<a type="button" href="#quick-buttons-3" class="btn btn-default ';
 if(tab == '3'){ print('active') } ;
__p += '" data-tab-num="3" role="tab" data-toggle="tab">3</a>\n			</div>\n		</div>\n\n		<div class="tab-content ';
if(editing){ print('editing') } ;
__p += '">\n			<div class="tab-pane ';
 if(tab == '1'){ print('active') } ;
__p += '" role="tabpanel" id="quick-buttons-1">\n				<ul id="quick-buttons-list-1" class="quick-buttons"></ul>\n			</div>\n			<div class="tab-pane ';
 if(tab == '2'){ print('active') } ;
__p += '" role="tabpanel" id="quick-buttons-2">\n				<ul id="quick-buttons-list-2" class="quick-buttons"></ul>\n			</div>\n			<div class="tab-pane ';
 if(tab == '3'){ print('active') } ;
__p += '" role="tabpanel" id="quick-buttons-3">\n				<ul id="quick-buttons-list-3" class="quick-buttons"></ul>\n			</div>\n		</div>\n	</div>\n</div>\n<div class="row">\n	<div class="col-md-12">\n		<p id="quickbutton-settings">\n		';
 if(!editing){ ;
__p += '\n			<button class="btn btn-xs btn-default" style="padding: 3px 8px;" id="edit-quick-buttons">\n				<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Buttons\n			</button>\n		';
 }else{ ;
__p += '\n			<button class="btn btn-xs btn-default" style="padding: 3px 8px;" id="save-quick-buttons">Save Buttons</button>\n			<button class="btn btn-xs btn-default" style="padding: 3px 8px;" id="add-quick-button">Add New Button</button>\n		';
 } ;
__p += '\n		</p>\n	</div>\n</div>\n<div class="row">\n	<div class="col-md-12">\n		<button class="btn btn-default" id="sale-settings">\n			<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Sale Settings\n		</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/receipt.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="#main">\nMy stuff here\n</div>\n<h1>' +
((__t = (title)) == null ? '' : __t) +
'</h1>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/receipt_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content sale-receipt-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Sale #';
 if(number != null){ print(number) }else{ print(sale_id) };
__p += '</h4>\n	</div>\n	<div id="sale-receipt" class="modal-body container-fluid">\n		<div class="row">\n			<div class="col-md-12 course-header">\n				<strong>' +
__e(App.data.course.get('name')) +
'</strong><br />\n				' +
__e(App.data.course.get('address')) +
'<br />\n				' +
__e(App.data.course.get('phone')) +
'<br />\n			</div>\n		</div>		\n		<div class="row">\n			<div class="col-md-12 sale-details">\n				<strong>Sale ID:</strong> POS ';
 if(number != null){ print(number) }else{ print(sale_id) };
__p += '<br />\n				<strong>Date:</strong> ' +
__e(moment(sale_time).format('MM/DD/YYYY h:mma')) +
'<br />\n				<strong>Employee:</strong> ' +
__e(employee.get('first_name')+' '+employee.get('last_name')) +
'\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-md-12">\n				<div class="table-responsive items">\n					<table class="table table-condensed items">\n						<thead>\n							<tr>\n								<th>Item</th>\n								<th>Price</th>\n								<th>Qty</th>\n								<th>Discount</th>\n								<th class="total">Total</th>\n							</tr>\n						</thead>\n						<tbody>\n						';
 _.each(items.models, function(item){ ;
__p += '\n							<tr>\n								<td>\n									' +
__e(item.get('name')) +
'\n									';
 if(item.has('items') && item.get('items').length > 0){ ;
__p += '\n									<ul>\n										';
 _.each(item.get('items'), function(item){ ;
__p += '\n										<li>(' +
__e(item.quantity) +
') ' +
__e(item.name) +
'</li>\n										';
 }); ;
__p += '\n									</ul>\n									';
 } ;
__p += '\n								</td>\n								<td>' +
__e(accounting.formatMoney(item.get('unit_price'))) +
'</td>\n								<td>' +
__e(item.get('quantity')) +
'</td>\n								<td>' +
__e(accounting.formatNumber(item.get('discount_percent'), 2)+'%') +
'</td>\n								<td class="total">' +
__e(accounting.formatMoney(item.get('subtotal'))) +
'</td>\n							</tr>\n						    ';
 _.each(item.get('modifiers'), function(modifier){ ;
__p += '\n						    <tr>\n								<td>- ' +
__e(modifier.name ) +
': ' +
__e(modifier.selected_option ) +
'</td>\n								<td>' +
__e(accounting.formatMoney(modifier.selected_price/item.get('number_splits'))) +
'</td>\n								<td></td>\n								<td></td>\n								<td class="total"></td>\n							</tr>\n						';
  });
						}); ;
__p += '\n						</tbody>\n					</table>\n				</div>\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-sm-offset-5 col-sm-7">\n				<table class="table table-condensed totals">\n					<tr>\n						<th>Subtotal</th>\n						<td class="amount">' +
__e(accounting.formatMoney(subtotal)) +
'</td>\n					</tr>\n					';
 if(auto_gratuity_amount > 0){ ;
__p += '\n					<tr>\n						<th>' +
__e(accounting.formatNumber(auto_gratuity, 2)) +
'% Gratuity</th>\n						<td class="amount">' +
__e(accounting.formatMoney(auto_gratuity_amount)) +
'</td>\n					</tr>\n					';
 } ;
__p += '\n					';
 _.each(taxes, function(tax){ ;
__p += '\n					<tr>\n						<th style="font-weight: normal;">' +
__e(accounting.formatNumber(tax.percent, 3)+'% - '+tax.name) +
'</th>\n						<td class="amount">' +
__e(accounting.formatMoney(tax.amount)) +
'</td>\n					</tr>\n					';
 }); ;
__p += '\n					<tr>\n						<th>Total</th>\n						<td class="amount">' +
__e(accounting.formatMoney(total + auto_gratuity_amount)) +
'</td>\n					</tr>					\n				</table>\n			</div>\n		</div>\n		<div class="row">\n			<div class="col-sm-offset-5 col-sm-7">\n				<h5><strong>Payments</strong></h5>\n				<table class="table table-condensed totals">\n					';
 
					var signatures = [];
					if(payments && payments.length > 0){
					_.each(payments.models, function(payment, index){ 
					if(payment.get('signature')){
						signatures.push(payment.get('signature'));
					}

					if(payment.get('is_auto_gratuity') == 1){
						return false;
					} 
					var payment_amount = payment.get('amount');
					if(auto_gratuity_amount > 0 && index == 0){
						payment_amount += auto_gratuity_amount;
					} ;
__p += '\n					<tr>\n						<th style="font-weight: normal;">' +
__e(payment.get('description')) +
'</th>\n						<td class="amount">' +
__e(accounting.formatMoney(payment_amount)) +
'</td>\n					</tr>\n					';
 }); } ;
__p += '					\n				</table>\n			</div>\n		</div>\n		\n		';
 if(customer_note){ ;
__p += '\n		<div class="row">\n			<div class="col-md-12">\n				<strong>Customer Note</strong>\n				<p>' +
__e(customer_note) +
'</p>\n			</div>\n		</div>	\n		';
 } ;
__p += '	\n\n		';
 if(signatures.length > 0){ ;
__p += '\n		<div class="row">\n			<div class="col-md-12">\n				<h4>Signature</h4>\n			</div>\n			';
 _.each(signatures, function(sig_url){ ;
__p += '\n			<div class="col-md-12">\n				<img style="display: block; width: 100%;" src="' +
__e(sig_url) +
'">\n			</div>\n			';
 }) ;
__p += '\n		</div>\n		';
 } ;
__p += '\n		\n		<div class="row">\n			<div class="col-md-12 barcode-container">\n				<p>' +
__e(App.data.course.get('return_policy')) +
'</p>\n				';
 var barcode_text = "POS ";
				if(number != null){ 
					barcode_text += String(number) 
				}else{ 
					barcode_text += String(sale_id)
				} ;
__p += '\n				<img class="barcode" src="' +
__e(SITE_URL) +
'/barcode?barcode=' +
__e(barcode_text) +
'&text=' +
__e(barcode_text) +
'" />\n			</div>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button type="button" data-dismiss="modal" class="btn btn-default pull-left">Close</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/recent_transaction.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="context-' +
__e(sale_id) +
'" class="context-menu" style="position: absolute; z-index: 100;">\n	<ul class="dropdown-menu" role="menu">\n		<li><a tabindex="-1" href="" class="print">Print Receipt</a></li>\n		<li><a tabindex="-1" href="" class="email">Email Receipt</a></li>\n		<li><a tabindex="-1" href="" class="tip">Add Tip</a></li>\n	</ul>\n</div>\n<div class="container clearfix">\n	<div class="header clearfix">\n		<span class="time">' +
__e(moment(sale_time).format('h:mma')) +
'</span>\n		<span class="sale-num edit">\n			';
 if(total < 0){ var color = "text-danger"; }else{ var color = ''; } ;
__p += '\n			<a href="" class="' +
__e(color) +
'">\n				#';
 if(number != null){ ;
__p +=
__e(number);
 }else{ ;
__p +=
__e(sale_id);
 } ;
__p += '\n			</a>\n		</span>\n	</div>\n	<div class="details clearfix">\n		<span class="customer">\n			';
 if(customer && customer.person_id){ ;
__p += '\n				' +
__e(customer.last_name) +
'\n			';
 }else{ ;
__p += '\n				<span class="unknown">No Customer</span>\n			';
 } ;
__p += '\n		</span>\n		<span class="amount">' +
__e(accounting.formatMoney(total) ) +
'</span>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/recent_transactions.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="panel-heading">\n	<h3 class="panel-title">\n		Recent Transactions \n		<span class="glyphicon glyphicon-plus-sign pull-right hidden-lg"></span>\n		<span class="glyphicon glyphicon-minus-sign pull-right visible-lg hidden-xs hidden-sm hidden-md"></span>\n	</h3>\n</div>\n<div class="panel-body minimize-sm" style="overflow-y: scroll; max-height: 300px">\n	<ul class="transactions"></ul>\n	<button class="btn btn-default btn-block btn-sm more">Load More</button>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/refund_sale_payment_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content refund-sale-payment">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Refund Credit Card Payment</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n            <div class="modal-body container-fluid">\n                <div class="row">\n                    <label for="refund-reason" class="col-md-3 control-label">Return Reason</label>\n                    <div class="col-md-4">\n                        <select class="form-control" name="reason" id="refund-reason">\n                            ';
 for (var i in refund_reasons.attributes) { ;
__p += '\n                            <option value="' +
__e(i) +
'">' +
__e(refund_reasons.attributes[i]) +
'</option>\n                            ';
 } ;
__p += '\n                        </select>\n                    </div>\n                </div>\n                <div class="row">\n                    <label for="refund-comment" class="col-md-3 control-label">Comment</label>\n                    <div class="col-md-8">\n                        <textarea class="form-control" value="" name="comment" id="refund-comment" />\n                    </div>\n                </div>\n            </div>\n				';
 if(params.cardholder_name != ''){ ;
__p += '\n				<div class="form-group">\n					<label class="col-md-3 control-label">Name On Card</label>\n					<div class="col-md-4">\n						<p class="form-control-static">' +
__e(params.cardholder_name) +
'</p>\n					</div>\n				</div>\n				';
 } ;
__p += '\n			';
 if(params.card_type != ''){ ;
__p += '\n				<div class="form-group">\n					<label class="col-md-3 control-label">Type</label>\n					<div class="col-md-4">\n						<p class="form-control-static">' +
__e(params.card_type) +
'</p>\n					</div>\n				</div>\n			';
 } ;
__p += '\n			';
 if(params.masked_account != ''){ ;
__p += '\n				<div class="form-group">\n					<label class="col-md-3 control-label">Number</label>\n					<div class="col-md-4">\n						<p class="form-control-static">' +
__e(params.masked_account) +
'</p>\n					</div>\n				</div>\n			';
 } ;
__p += '\n\n\n				<div class="form-group">\n					<label class="col-md-3 control-label">Original Charge</label>\n					<div class="col-md-4">\n						<p class="form-control-static">' +
__e(accounting.formatMoney(amount)) +
'</p>\n					</div>\n				</div>\n			<div class="form-group">\n				<label class="col-md-3 control-label">Refund amount</label>\n				<div class="col-md-4">\n					';
 if(isNaN(parseFloat(params.amount_refunded)) || parseFloat(params.amount_refunded) == parseFloat(0)){ ;
__p += '\n					<input type="text" class="form-control refund" name="refund" id="payment-refund" value="' +
__e(accounting.formatMoney(amount, '', 2)) +
'">\n					';
 }else{ ;
__p += '\n					<p class="form-control-static">' +
__e(accounting.formatMoney(params.amount_refunded)) +
'</p>\n					';
 } ;
__p += '\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-md-12">\n				';
 if(isNaN(parseFloat(params.amount_refunded)) || parseFloat(params.amount_refunded) == parseFloat(0)){ ;
__p += '\n					<p>By selecting \'Refund\' below, you will return the stated amount to the card listed above.</p>\n				';
 }else{ ;
__p += '\n					<p>Funds can no longer be returned. A refund has already been issued to this card.</p>\n				';
 } ;
__p += '\n				</div>\n			</div>														\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>\n		';
 if(isNaN(parseFloat(params.amount_refunded)) || parseFloat(params.amount_refunded) == parseFloat(0)){ ;
__p += '<button type="button" class="btn btn-primary issue-refund">Refund</button>';
 } ;
__p += '\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/sale.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="sales-quick-buttons" class="col-md-3 col-lg-2"></div>\n<div id="sales-cart" class="col-md-9 col-lg-8">\n	<div class="row">\n		<div class="col-lg-12" id="cart-list"></div>	\n	</div>\n	<div class="row">\n		<div class="col-lg-12" id="cart-totals"></div>\n	</div>\n	<div class="row">\n		<div class="col-lg-12" id="sale-customers"></div>\n	</div>\n	<div class="row">\n		<div class="col-lg-12" id="sale-payments"></div>\n	</div>\n</div>\n<div id="sales-recent" class="col-md-12 col-lg-2">\n	<div class="row">\n		<div id="recent-transactions" class="col-md-6 col-lg-12"></div>\n		<div id="suspended-sales" class="col-md-6 col-lg-12"></div>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/sale_payment.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<span class="desc">' +
__e(description) +
' - ' +
__e(accounting.formatMoney(amount)) +
'</span>\n';
 if(params){ ;
__p += '<a href="" class="refund text-danger pull-right">Issue Refund</a>';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/sale_payments.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row">\n	<div class="col-lg-7" id="sale-actions">\n		<button class="btn btn-success pay">';
 if(mode == 'sale'){ ;
__p += 'Pay Now';
 }else{ ;
__p += 'Return Now';
 };
__p += '</button>\n        <button class="btn btn-success" id="quick_payment_credit_card" href="">Credit Card</button>\n        <div style="clear:both"></div>\n		';
 if(suspended_name && suspended_name != ''){ ;
__p += '\n		<a id="save-suspended-sale">Save Sale - ' +
__e(suspended_name) +
'</a>	\n		<a id="add-credit-card">Add Credit Card</a>\n		';
 }else{ ;
__p += '\n		<a id="suspend-sale">Suspend Sale/Start Tab</a>\n		';
 } ;
__p += '	\n		\n		<a id="open-cash-drawer">Cash Drawer</a>\n		';
 if(use_register_log == 1) { ;
__p += '\n			';
 if(active_register_log){ ;
__p += '\n			<a id="close-register">Close Register</a>\n			';
 }else{ ;
__p += '\n			<a id="open-register">Open Register</a>\n			';
 } ;
__p += '\n		';
 } ;
__p += '\n		<button class="btn btn-sm btn-default pos-manager-override" style="margin-left: 0px">Manager Override</button>\n	</div>\n	<div class="col-lg-5" id="sale-owing">\n		<ul id="payments"></ul>\n		<h3 id="total-due">Total Due: <strong>' +
__e(accounting.formatMoney(total_due)) +
'</strong></h3>\n		<div class="row" id="sale-payments" style="display: none;">\n			<div class="col-xs-12">\n				<h4 style="margin-bottom: 5px;">Original Sale Payments</h4>\n				<ul style="padding: 0px; list-style-type: none; margin: 0px;"></ul>\n			</div>\n		</div>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/sale_settings_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content sale-settings">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title" style="text-align: center">Sale Settings</h4>\n	</div>\n	<div class="modal-body">\n		<div class="container-fluid keypad">\n			<form class="form-horizontal" id="sale-settings">\n				<div class="form-group">\n					<label for="sales-automatic-receipt-printing" class="col-sm-5 control-label">Automatic receipt printing</label>\n					<div class="col-sm-7">\n						<div class="checkbox">							\n							<input id="sales-automatic-receipt-printing" name="print_after_sale" type="checkbox" data-unchecked-value="0" value="1" ';
 if(print_after_sale == 1){ ;
__p += 'checked';
};
__p += '>	\n						</div>\n					</div>\n				</div>\n				<div class="form-group">\n					<label for="sales-two-credit-card-receipts" class="col-sm-5 control-label">Print two credit card receipts</label>\n					<div class="col-sm-7">\n						<div class="checkbox">\n							<input id="sales-two-credit-card-receipts" name="print_two_receipts" type="checkbox" data-unchecked-value="0" value="1" ';
 if(print_two_receipts == 1){ ;
__p += 'checked';
};
__p += '>	\n						</div>\n					</div>\n				</div>\n				<div class="form-group">\n					<label for="sales-two-sales-receipts" class="col-sm-5 control-label">Print two sales receipts</label>\n					<div class="col-sm-7">\n						<div class="checkbox">\n							<input id="sales-two-sales-receipts" type="checkbox" name="print_two_receipts_other" data-unchecked-value="0" value="1" ';
 if(print_two_receipts_other == 1){ ;
__p += 'checked';
};
__p += '>\n						</div>\n					</div>\n				</div>\n				<div class="form-group">\n					<label class="col-sm-5 control-label">After sale load</label>\n					<div class="col-sm-7">\n						<div class="radio">\n							<label>\n								<input type="radio" name="after_sale_load" id="sale-load-teesheet" value="0" ';
 if(after_sale_load == 0){ ;
__p += 'checked';
};
__p += ' />\n								Tee sheet\n							</label>\n						</div>\n						<div class="radio">\n							<label>\n								<input type="radio" name="after_sale_load" id="sale-load-sales" value="1" ';
 if(after_sale_load == 1){ ;
__p += 'checked';
};
__p += ' />\n								Sales\n							</label>\n						</div>\n					</div>\n				</div>\n				<div class="form-group">\n					<label for="sales-track-cash" class="col-sm-5 control-label">Track cash in register</label>\n					<div class="col-sm-7">\n						<div class="checkbox">\n							<input id="sales-track-cash" type="checkbox" name="track_cash" value="1" data-unchecked-value="0" ';
 if(track_cash == 1){ ;
__p += 'checked';
};
__p += '>	\n						</div>\n					</div>\n				</div>\n				<div class="form-group">\n					<label for="sale-return-policy" class="col-sm-5 control-label">Return policy</label>\n					<div class="col-sm-7">\n						<textarea class="form-control" id="sale-return-policy" name="return_policy" style="height: 75px; margin-top: 10px;">' +
__e(return_policy) +
'</textarea>\n					</div>\n				</div>\n			</form>\n		</div>\n	</div>\n	<div class="modal-footer">\n		<button type="submit" class="btn btn-primary save">Save</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/sale_tip_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 var identifier = sale_id;
if(number != null){
	identifier = number;
} ;
__p += '\n<div class="modal-content sale-tip-window">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Add Tip - Sale #' +
__e(identifier) +
'</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<input type="hidden" value="" id="tip-record-id" />\n			<div class="form-group">\n				<label for="tip-type" class="col-md-3 control-label">Type</label>\n				<div class="col-md-6">\n					<select class="form-control" name="type" id="tip-type">\n						';
 var first = true;
						_.each(tips.models, function(tip){ ;
__p += '	\n						<option value="' +
__e(tip.get('payment_id')) +
'"\n							data-number="' +
__e(tip.get('number')) +
'"\n							data-type="' +
__e(tip.get('type') ) +
'"\n							data-payment-id="' +
__e(tip.get('payment_id')) +
'"\n							data-record-id="' +
__e(tip.get('record_id')) +
'" \n							data-invoice-id="' +
__e(tip.get('invoice_id')) +
'"\n							data-is-auto-gratuity="' +
__e(tip.get('is_auto_gratuity')) +
'"\n							';
 if(first){ print('selected') } ;
__p += '> \n							' +
__e(tip.get('description')) +
'\n						</option>	\n						';
 first = false; }) ;
__p += '					\n					</select>				\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="tip-amount" class="col-md-3 control-label">Amount</label>\n				<div class="col-md-3">\n					<input class="form-control" type="number" value="" name="amount" id="tip-amount" />				\n				</div>\n			</div>\n			<div class="form-group">\n				<label for="tip-recipient" class="col-md-3 control-label">Recipient</label>\n				<div class="col-md-5">\n					<input class="form-control" type="text" value="" name="amount" id="tip-recipient" />\n					<input type="hidden" value="" id="tip-recipient-id" />			\n				</div>\n			</div>						\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-primary apply-tip">Apply Tip</button>\n		<button type="button" data-dismiss="modal" class="btn btn-default pull-left">Cancel</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/statement_details_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content invoice">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Statement Details</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		<form class="form-horizontal" role="form">\n			<div class="form-group">\n				<label class="col-md-2 control-label">\n					Total\n				</label>\n				<div class="col-md-10">\n					<p class="form-control-static">' +
__e(accounting.formatMoney(params.total)) +
'</p>\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-2 control-label">\n					Paid\n				</label>\n				<div class="col-md-10">\n					<p class="form-control-static">' +
__e(accounting.formatMoney(params.total_paid)) +
'</p>\n				</div>\n			</div>\n			<div class="form-group">\n				<label class="col-md-2 control-label" for="statement-price">Amount</label>\n				<div class="col-md-3">\n					<input type="text" name="unit_price" value="' +
__e(accounting.formatMoney(unit_price, '')) +
'" class="form-control price" id="statement-price">\n				</div>\n			</div>\n			<div class="form-group">\n				<div class="col-md-2">\n					Total Receivable\n				</div>\n				<div class="col-md-3">\n					<p class="form-control-static">' +
__e(accounting.formatMoney(params.customer.invoice_balance)) +
'</p>\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n		<button type="button" class="btn btn-primary save">Save</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/suspend_selection_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content suspend-sale">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n		<h4 class="modal-title">Select a table/number</h4>\n	</div>\n	<div class="modal-body container-fluid">\n		';
 if(allow_credit_card_save){ ;
__p += '\n		<div class="row">\n			<div class="col-md-12 m-b-2">\n				<button class="btn btn-primary add-suspended-sale-card">Save Credit Card</button>\n			</div>\n		</div>\n		';
 } ;
__p += '\n		<div class="row">\n			<div class="col-md-12">\n			';
 for(var x = 1; x <= 24; x++){ ;
__p += '\n				<div class="number ';
 if(numbers_taken[String(x)]){ print('active') } ;
__p += '">' +
__e(x) +
'</div>\n			';
 } ;
__p += '\n			</div>\n		</div>\n		<form class="form-horizontal" role="form" style="margin-top: 15px;" autocomplete="off">\n			<div class="form-group">\n				<label for="suspended-number" class="col-md-2 control-label">Label</label>\n				<div class="col-md-6">\n					<input class="form-control" type="text" value="" name="label" id="suspended-label" autocomplete="off" />\n				</div>\n			</div>\n		</form>\n	</div>\n	<div class="modal-footer">\n		<button type="button" data-dismiss="modal" class="btn btn-default pull-left">Close</button>\n		<button type="button" class="btn btn-primary pull-right suspend">Suspend</button>\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/suspended_sale.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="context-menu">\n	<ul class="dropdown-menu" role="menu">\n		<li><a tabindex="-1" href="" class="kitchen">Send to Kitchen</a></li>\n		<li><a tabindex="-1" href="" class="print">Print Receipt</a></li>\n		<li><a tabindex="-1" href="" class="delete">Delete</a></li>\n	</ul>\n</div>\n';
 if(status == 'suspended'){ ;
__p += '\n<button class="label">' +
__e(suspended_name) +
'</button>\n';
 }else{ ;
__p += '\n<div class="cart-details">\n	<div>' +
__e(terminal_label) +
'</div>\n	<div>' +
__e(employee_name) +
'</div>\n</div>\n';
 } ;
__p += '\n<div class="info">\n	<h4 class="amount">' +
__e(accounting.formatMoney(total)) +
'</h4>\n	<div class="name';
 if(!customers.at(0)){ ;
__p += ' text-muted';
 } ;
__p += '">\n		';
 if(customers.at(0)){ ;
__p += '\n			' +
__e(customers.at(0).get('last_name')) +
'\n		';
 }else{ ;
__p += '\n			No Customer\n		';
 } ;
__p += '\n	</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/suspended_sales.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="panel-heading">\n	<h3 class="panel-title">\n		Suspended Sales\n		<span class="glyphicon glyphicon-plus-sign pull-right hidden-lg"></span>\n		<span class="glyphicon glyphicon-minus-sign pull-right visible-lg hidden-xs hidden-sm hidden-md"></span>\n	</h3>\n</div>\n<div class="panel-body minimize-sm" style="overflow-y: scroll; max-height: 300px;">\n	<ul class="carts suspended"></ul>\n	<div class="incomplete-container">\n		<h4 class="incomplete">Incomplete Sales</h4>\n		<ul class="carts incomplete"></ul>\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/teesheet_header_stats.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="header-stat">\n    <div class="stat-value">' +
__e(Bookings ) +
'</div>\n    <div class="stat-label">Bookings</div>\n</div>\n<div class="header-stat">\n    <div class="stat-value">' +
__e(Occupancy ) +
'</div>\n    <div class="stat-label">Occupancy</div>\n</div>\n<div class="header-stat">\n    <div class="stat-value">' +
__e(Player_No_Shows ) +
'</div>\n    <div class="stat-label">No Shows</div>\n</div>\n<div class="header-stat">\n    <div class="stat-value">' +
__e(Players_Checked_in ) +
'</div>\n    <div class="stat-label">Checked-in</div>\n</div>\n<div class="header-stat">\n    <div class="stat-value">' +
__e(Revenue ) +
'</div>\n    <div class="stat-label">Revenue</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["sales/terminal_header.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if(label && label != null && label != ''){ ;
__p += '\n<h3 style="margin: 8px 0px 0px 0px; padding: 0px; display: block; width: auto; text-align: center; color: white;">\n	' +
__e(label) +
'\n</h3>\n';
 } ;
__p += '\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["tee_sheet/recipient_table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content item-details">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n        <h4 class="modal-title">Tee Sheet Marketing</h4>\n    </div>\n    <div class="modal-body container-fluid">\n        <div>\n            Choose which of the days customers to message:\n            <label>Start Time <input id="start"></label>\n            <label>End Time <input id="end"></label>\n        </div>\n        <div id="region-recipients">\n\n        </div>\n        <table>\n            <thead>\n                <th><input type="checkbox"/></th>\n                <th>Customer Name</th>\n                <th>Email Address</th>\n                <th>Mobile Phone Number</th>\n                <th>&nbsp;</th>\n            </thead>\n            <tbody>\n\n            </tbody>\n        </table>\n    </div>\n    <div class="modal-footer">\n        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>\n        <button type="button" class="btn btn-primary save">Save</button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["tee_sheet/tee_sheet.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="tee_sheet_iframe_holder">\n\n</div>\n<style>\n    body {margin:0px;}\n    #main-nav {display:none;}\n    #page {height:100%;}\n    #tee_sheet_iframe {\n        width:100%;\n        height:100%;\n        border:none;\n        padding:0px;\n    }\n    .iframe_holder {\n        position:fixed;\n        right: 0;\n        bottom: 0;\n        left: 0;\n        top: 0;\n        -webkit-overflow-scrolling: touch;\n        overflow-y: scroll;\n    }\n    .iframe_holder::-webkit-scrollbar {\n        width: 0 !important;\n    }\n</style>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["tee_sheet/tee_sheet_to_marketing.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content item-details teesheet-modal">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n        <h4 class="modal-title">Tee Sheet Marketing</h4>\n    </div>\n    <div class="modal-body container-fluid">\n        <div class="recipient-header row">\n            Choose which of the days customers to message.\n            <form>\n                <div class="form-group col-xs-3">\n                    <label for="start" class="control-label">Start</label>\n                    <input type="datetime" value=\'\' class="form-control" id="start" >\n                </div>\n                <div class="form-group col-xs-3">\n                    <label for="end" class="control-label">End</label>\n                    <input type="datetime" value=\'\' class="form-control" id="end">\n                </div>\n            </form>\n        </div>\n\n        <table class="recipients">\n            <thead>\n                <th><input type="checkbox"/></th>\n                <th>Customer Name</th>\n                <th>Email Address</th>\n                <th>Mobile Phone Number</th>\n                <th>&nbsp;</th>\n            </thead>\n            <tbody>\n\n            </tbody>\n        </table>\n    </div>\n\n    ';
 if(HAS_MARKETING){ ;
__p += '\n        <div class="modal-footer">\n            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>\n            <button type="button" class="btn btn-primary" id="sendCampaign">Send Message</button>\n        </div>\n    ';
 } else { ;
__p += '\n        <div class="modal-footer warning">\n            Call us, 800-929-5737, to start messaging your customers today.\n            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>\n        </div>\n    ';
 } ;
__p += '\n\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit/templates/bulk_edit.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\n        <h4 class="modal-title">\n            Bulk Edit\n        </h4>\n    </div>\n    <div class="steps"></div>\n    <div class="modal-body wizard container-fluid bulk-edit-container"></div>\n    <div class="modal-footer"></div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit/templates/bulk_edit_confirm.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var labelMap = {};

labelMap.customers = {
   first_name: 'First Name',
   last_name: 'Last Name',
   email: 'Email',
   phone_number: 'Phone Number',
   cell_phone_number: 'Cell Phone Number',
   non_taxable: 'Non-taxable',
   address_1: 'Address Line 1',
   address_2: 'Address Line 2',
   city: 'City',
   state: 'State',
   zip: 'Zipcode',
   country: 'Country',
   date_created: 'Date Joined',
   birthday: 'Birthday',
   notes: 'Notes',
   status_flag: 'Customer Status',
   username: 'Username',
   password: 'Password',
   groups: 'Groups',
   require_food_minimum: 'Require F&B minimum',
   minimum_charges: 'Minimum Charges',
   recurring_charges: 'Billing Profiles',
   account_number: 'Account Number',
   price_class: 'Price Class',
   discount: 'Discount',
   invoice_email: 'Invoice Email',
   passes: 'Passes',
   household_members: 'Household Members',
   member: 'Is Member',
   member_account_balance_allow_negative: App.data.course.get('member_balance_nickname')+' Allow Negative',
   member_account_balance: App.data.course.get('member_balance_nickname')+' Balance',
   member_account_limit: App.data.course.get('member_balance_nickname')+' Limit',
   account_balance_allow_negative: App.data.course.get('customer_credit_nickname')+' Balance Allow Negative',
   account_balance: App.data.course.get('customer_credit_nickname')+' Balance',
   account_limit: App.data.course.get('customer_credit_nickname')+' Limit',
   use_loyalty: 'Allow Loyalty',
   loyalty_points: 'Loyalty Points'
};

var actions = {
   set: 'Set to',
   add: 'Add to existing',
   remove: 'Remove from existing',
   subtract: 'Remove from existing',
   increase: 'Add to existing amount',
   decrease: 'Subtract from existing amount',
   clear: 'Clear'
};

function getRecordName(type, field, record){

   if(type == 'customers'){
      if(field == 'recurring_charges' || field == 'minimum_charges' || field == 'passes'){
         return record.name;
      }else if(field == 'household_members'){
         return record.first_name +' '+ record.last_name;
      }else if(field == 'groups'){
         return record.label;
      }
   }
}

function getValue(field, value){
   switch(field){
      case 'state':
         return US_STATES[value];
      break;
      case 'status_flag':
         var values = {0: 'N/A', 1: 'Red', 2: 'Yellow', 3: 'Green'};
         return values[value];
      break;
      case 'price_class':
         return App.data.course.get('price_classes')[value];
      break;
      case 'non_taxable':
      case 'member':
      case 'member_account_balance_allow_negative':
      case 'account_balance_allow_negative':
      case 'require_food_minimum':
      case 'use_loyalty':
         if(value == 1){
            return 'Yes';
         }else{
            return 'No';
         }
      break;
   }
   return value;
}

var labels = labelMap[type];
;
__p += '\n<div class="row">\n   <div class="col-md-12">\n      <p class="alert alert-info" style="font-weight: normal">You have selected <strong>' +
__e(totalRecords) +
' ' +
__e(type) +
'</strong> to be edited</p>\n   </div>\n</div>\n\n<div class="row">\n   <div class="col-md-12">\n      <h3>Changes</h3>\n      <table class="table table-light change-summary">\n         <thead>\n            <tr>\n               <th style="width: 250px">Field</th>\n               <th style="width: 150px">Action</th>\n               <th>Value</th>\n            </tr>\n         </thead>\n         <tbody>\n            ';
 _.each(settings, function(change, fieldKey){
            if(fieldKey == 'member_balance_change_reason' || fieldKey == 'account_balance_change_reason' || fieldKey == 'loyalty_points_change_reason'){
               return true;
            }
            ;
__p += '\n            <tr>\n               <td><strong>' +
__e(labels[fieldKey]) +
'</strong></td>\n               <td>\n                  ';

                  var labelClass = 'label-primary';
                  if(change.action == 'remove' || change.action == 'decrease'){
                     labelClass = 'label-danger';
                  }else if(change.action == 'add' || change.action == 'increase'){
                     labelClass = 'label-success';
                  }else if(change.action == 'clear'){
                     labelClass = 'label-default';
                  } ;
__p += '\n                  <span class="label ' +
__e(labelClass) +
'">' +
__e(actions[change.action]) +
'</span>\n               </td>\n               <td>\n                  ';
 if(typeof(change.value) == 'object' && change.value !== null && change.value.length && change.value.length > 0){ ;
__p += '\n                  <ul>\n                     ';
 _.each(change.value, function(record){ ;
__p += '\n                     <li>' +
__e(getRecordName(type, fieldKey, record)) +
'</li>\n                     ';
 }) ;
__p += '\n                  </ul>\n                  ';
 }else{ ;
__p += '\n                     ';
 if(change.value == ''){ ;
__p += '\n                     <em class="text-muted">(Blank)</em>\n                     ';
 }else{ ;
__p += '\n                     ' +
__e(getValue(fieldKey, change.value)) +
'\n                     ';
 } ;
__p += '\n                  ';
 } ;
__p += '\n               </td>\n            </tr>\n            ';
 }) ;
__p += '\n         </tbody>\n      </table>\n   </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit/templates/bulk_edit_customer_changes.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var requiredFields = App.data.course.get('customer_field_settings');

var customerInfo = [
    {
        field: 'first_name',
        name: 'First Name',
        type: 'text'
    },
    {
        field: 'last_name',
        name: 'Last Name',
        type: 'text'
    },
    {
        field: 'email',
        name: 'Email',
        type: 'text'
    },
    {
        field: 'phone_number',
        name: 'Phone Number',
        type: 'text'
    },
    {
        field: 'cell_phone_number',
        name: 'Cell Phone Number',
        type: 'text'
    },
    {
        field: 'address_1',
        name: 'Address Line 1',
        type: 'text'
    },
    {
        field: 'address_2',
        name: 'Address Line 2',
        type: 'text'
    },
    {
        field: 'city',
        name: 'City',
        type: 'text'
    },
    {
        field: 'state',
        name: 'State',
        type: 'stateDropdown'
    },
    {
        field: 'zip',
        name: 'Zip',
        type: 'text'
    },
    {
        field: 'date_created',
        name: 'Date Joined',
        type: 'date'
    },
    {
        field: 'birthday',
        name: 'Birthday',
        type: 'date'
    },
    {
        field: 'comments',
        name: 'Notes',
        type: 'textarea'
    }
];

var getField = function(field){

    var required = '';
    if(requiredFields && requiredFields[field.field] && requiredFields[field.field].required == 1){
        required = ' <span class="text-danger">*</span>';
    }

    var value = '';
    if(field.value !== undefined){
        value = field.value;
    }

    var html = '<div class="row form-group">' +
        '<div class="col-md-4">' +
            '<input type="checkbox" class="checkbox enable-change"  id="editing-customer-' + field.field + '" name="' + field.field + ':edit" value="1">' +
            '<label for="editing-customer-' + field.field + '">' + field.name + required + '</label>' +
            '</div>' +
        '<div class="col-md-8">';

        if(field.type == 'text'){
            html += '<input type="text" class="form-control" name="' + field.field + '" value="" disabled>';

        }else if(field.type == 'textarea'){
            html += '<textarea name="' + field.field + '" class="form-control" style="height: 100px" disabled></textarea>';

        }else if(field.type == 'date'){
            html += '<input type="text" class="form-control calendar" name="' + field.field + '" value="" placeholder="MM/DD/YYYY" style="width: 120px" disabled>';

        }else if(field.type == 'stateDropdown'){
            html += _.dropdown(US_STATES, {"name":"state", "id":"editing-customer-state", "class":"form-control", "disabled":"disabled"}, value);
        }

        html += '</div></div>';
        return html;
} ;
__p += '\n\n<div class="row">\n    <div class="col-md-6">\n        <div class="row">\n            <div class="col-md-12">\n                <h4>Information</h4>\n                <hr>\n            </div>\n        </div>\n        ';
 _.each(customerInfo, function(field){ ;
__p += '\n        ' +
((__t = (getField(field))) == null ? '' : __t) +
'\n        ';
 }); ;
__p += '\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input type="checkbox" class="checkbox enable-change" id="editing-customer-status_flag" name="status_flag:edit" value="1">\n                <label for="editing-customer-status_flag">Customer Status</label>\n            </div>\n            <div class="col-md-8">\n                ' +
((__t = (_.dropdown({0: 'N/A', 1: 'Red', 2: 'Yellow', 3: 'Green'}, {name: 'status_flag', class: 'form-control', disabled: 'disabled'}))) == null ? '' : __t) +
'\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input type="checkbox" class="checkbox enable-change"  id="editing-customer-password" name="password:edit" value="1">\n                <label for="editing-customer-password">Password</label>\n            </div>\n            <div class="col-md-8">\n                <input type="text" class="form-control" name="password" value="" disabled>\n            </div>\n        </div>\n        ';

        var multiRecordOptions = {
            set: 'Replace all with',
            add: 'Add to existing',
            remove: 'Remove from existing',
            clear: 'Clear all'
        };
        ;
__p += '\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input type="checkbox" class="checkbox enable-change"  id="editing-customer-groups" name="groups:edit" value="1">\n                <label for="editing-customer-groups">Groups</label>\n            </div>\n            <div class="col-md-8">\n                <div class="row">\n                    <div class="col-md-12">\n                        ' +
((__t = (_.dropdown(multiRecordOptions, {name: 'groups:action', class: 'form-control form-group action-menu', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                        <select id="customer-add-group" class="form-control add-record" disabled>\n                            <option value="0">- Select Group -</option>\n                            ';
 _.each(App.data.course.get('groups').models, function(group){ ;
__p += '\n                            <option value="' +
__e(group.get('group_id')) +
'">' +
__e(group.get('label')) +
'</option>\n                            ';
 }) ;
__p += '\n                        </select>\n                    </div>\n                </div>\n                <div class="row" id="customer-groups"></div>\n            </div>\n        </div>\n\n        <div class="row">\n            <div class="col-md-12">\n                <h4>Billing</h4>\n                <hr>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-require_food_minimum" name="require_food_minimum:edit" value="1">\n                <label for="editing-customer-require_food_minimum">Require F&B Minimums</label>\n            </div>\n            <div class="col-md-8">\n                <input type="checkbox" id="customer-require_food_minimum" name="require_food_minimum" value="1" data-unchecked-value="0" class="checkbox" disabled>\n                <label for="customer-require_food_minimum">&nbsp;</label>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input type="checkbox" class="checkbox enable-change" id="editing-customer-minimum-charges" name="minimum_charges:edit" value="1">\n                <label for="editing-customer-minimum-charges">Minimum Charges</label>\n            </div>\n            <div class="col-md-8">\n                <div class="row">\n                    <div class="col-md-12">\n                        ' +
((__t = (_.dropdown(multiRecordOptions, {name: 'minimum_charges:action', class: 'form-control form-group action-menu', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                        ' +
((__t = (_.dropdown(App.data.minimum_charges.get_dropdown_data(), {'class': 'form-control form-group add-record', 'id': 'customer-add-minimum-charge', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                    </div>\n                </div>\n                <div class="row" id="customer-minimum-charges"></div>\n            </div>\n        </div>\n\n        <!-- Display this in mobile, but not production -->\n        <div class="row form-group" ';
 if(ENVIRONMENT == 'production' && window.location.href.indexOf('mobile.foreupsoftware.com') == -1){ ;
__p += 'style="display: none"';
 } ;
__p += '>\n            <div class="col-md-4">\n                <input type="checkbox" class="checkbox enable-change" id="editing-customer-recurring-charges" name="recurring_charges:edit" value="1">\n                <label for="editing-customer-recurring-charges">Billing Memberships</label>\n            </div>\n            <div class="col-md-8">\n                <div class="row">\n                    <div class="col-md-12">\n                        ' +
((__t = (_.dropdown(multiRecordOptions, {name: 'recurring_charges:action', class: 'form-control form-group action-menu', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                        ' +
((__t = (_.dropdown(App.data.recurring_charges.dropdownData(), {id: 'customer-add-recurring-charge', class: 'form-control form-group add-record', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                    </div>\n                </div>\n                <div class="row" id="customer-recurring-charges"></div>\n            </div>\n        </div>\n    </div>\n    <div class="col-md-6">\n        ';

        var balanceOptions = {
            set: 'Set amount to',
            increase: 'Add to existing amount',
            decrease: 'Subtract from existing amount'
        };
        ;
__p += '\n        <div class="row">\n            <div class="col-md-12">\n                <h4>Accounts</h4>\n                <hr>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input type="checkbox" class="checkbox enable-change"  id="editing-customer-price_class" name="price_class:edit" value="1">\n                <label for="editing-customer-price_class">Price Class</label>\n            </div>\n            <div class="col-md-8">\n                ' +
((__t = (_.dropdown(App.data.course.get('price_classes'), {name: 'price_class', class: 'form-control', disabled: 'disabled'}))) == null ? '' : __t) +
'\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-non_taxable" name="non_taxable:edit" value="1">\n                <label for="editing-customer-non_taxable">Non-taxable</label>\n            </div>\n            <div class="col-md-8">\n                <input type="checkbox" id="customer-non_taxable" name="non_taxable" value="1" data-unchecked-value="0" class="checkbox" disabled>\n                <label for="customer-non_taxable">&nbsp;</label>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input type="checkbox" class="checkbox enable-change"  id="editing-customer-discount" name="discount:edit" value="1">\n                <label for="editing-customer-discount">Discount</label>\n            </div>\n            <div class="col-md-8">\n                <div class="input-group" style="width: 100px">\n                    <input type="text" class="form-control" name="discount" value="" disabled>\n                    <div class="input-group-addon">%</div>\n                </div>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-invoice_email" name="invoice_email:edit" value="1">\n                <label for="editing-customer-invoice_email">Invoice Email</label>\n            </div>\n            <div class="col-md-8">\n                <input type="text" class="form-control" name="invoice_email" value="" disabled>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input type="checkbox" class="checkbox enable-change"  id="editing-customer-passes" name="passes:edit" value="1">\n                <label for="editing-customer-passes">Passes</label>\n            </div>\n            <div class="col-md-8">\n                <div class="row">\n                    <div class="col-md-12">\n                        ' +
((__t = (_.dropdown(multiRecordOptions, {name: 'passes:action', class: 'form-control form-group action-menu', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                        <input type="text" class="form-control form-group add-record" id="customer-pass-search" value="" placeholder="Search existing issued passes..." disabled>\n                    </div>\n                </div>\n                <div class="row" id="customer-passes"></div>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-member" name="member:edit" value="1">\n                <label for="editing-customer-member">Is Member</label>\n            </div>\n            <div class="col-md-8">\n                <input type="checkbox" id="customer-member" name="member" value="1" data-unchecked-value="0" class="checkbox" disabled>\n                <label for="customer-member">&nbsp;</label>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-member_account_balance_allow_negative" name="member_account_balance_allow_negative:edit" value="1">\n                <label for="editing-customer-member_account_balance_allow_negative">' +
__e(App.data.course.get('member_balance_nickname')) +
' Allow Negative</label>\n            </div>\n            <div class="col-md-8">\n                <input type="checkbox" id="customer-member_account_balance_allow_negative" name="member_account_balance_allow_negative" value="1" class="checkbox" data-unchecked-value="0" disabled>\n                <label for="customer-member_account_balance_allow_negative">&nbsp;</label>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-member_account_balance" name="member_account_balance:edit" value="1">\n                <label for="editing-customer-member_account_balance">' +
__e(App.data.course.get('member_balance_nickname')) +
' Balance</label>\n            </div>\n            <div class="col-md-8">\n                ' +
((__t = (_.dropdown(balanceOptions, {name: 'member_account_balance:action', class: 'form-control form-group', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                <div class="input-group form-group">\n                    <div class="input-group-addon">$</div><input type="text" class="form-control" name="member_account_balance" value="" style="width: 120px" disabled>\n                </div>\n                <input type="text" class="form-control" name="member_balance_change_reason" placeholder="Comment..." value="" disabled>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-member_account_limit" name="member_account_limit:edit" value="1">\n                <label for="editing-customer-member_account_limit">' +
__e(App.data.course.get('member_balance_nickname')) +
' Limit</label>\n            </div>\n            <div class="col-md-8">\n                ' +
((__t = (_.dropdown(balanceOptions, {name: 'member_account_limit:action', class: 'form-control form-group', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                <div class="input-group">\n                    <div class="input-group-addon">(-) $</div><input type="text" class="form-control" name="member_account_limit" value="" style="width: 120px" disabled>\n                </div>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-account_balance_allow_negative" name="account_balance_allow_negative:edit" value="1">\n                <label for="editing-customer-account_balance_allow_negative">' +
__e(App.data.course.get('customer_credit_nickname')) +
' Balance Allow Negative</label>\n            </div>\n            <div class="col-md-8">\n                <input type="checkbox" id="customer-account_balance_allow_negative" name="account_balance_allow_negative" value="1" class="checkbox" data-unchecked-value="0" disabled>\n                <label for="customer-account_balance_allow_negative">&nbsp;</label>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-account_balance" name="account_balance:edit" value="1">\n                <label for="editing-customer-account_balance">' +
__e(App.data.course.get('customer_credit_nickname')) +
' Balance</label>\n            </div>\n            <div class="col-md-8">\n                ' +
((__t = (_.dropdown(balanceOptions, {name: 'account_balance:action', class: 'form-control form-group', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                <div class="input-group form-group">\n                    <div class="input-group-addon">$</div><input type="text" class="form-control form-group" name="account_balance" value="" style="width: 120px" disabled>\n                </div>\n                <input type="text" class="form-control" name="account_balance_change_reason" placeholder="Comment..." value="" disabled>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-account_limit" name="account_limit:edit" value="1">\n                <label for="editing-customer-account_limit">' +
__e(App.data.course.get('customer_credit_nickname')) +
' Limit</label>\n            </div>\n            <div class="col-md-8">\n                ' +
((__t = (_.dropdown(balanceOptions, {name: 'account_limit:action', class: 'form-control form-group', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                <div class="input-group">\n                    <div class="input-group-addon">(-) $</div><input type="text" class="form-control" name="account_limit" value="" style="width: 120px" disabled>\n                </div>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-use_loyalty" name="use_loyalty:edit" value="1">\n                <label for="editing-customer-use_loyalty">Allow Loyalty</label>\n            </div>\n            <div class="col-md-8">\n                <input type="checkbox" id="customer-use_loyalty" name="use_loyalty" value="1" data-unchecked-value="0" class="checkbox" disabled>\n                <label for="customer-use_loyalty">&nbsp;</label>\n            </div>\n        </div>\n        <div class="row form-group">\n            <div class="col-md-4">\n                <input class="checkbox enable-change" type="checkbox" id="editing-customer-loyalty_points" name="loyalty_points:edit" value="1">\n                <label for="editing-customer-loyalty_points">Loyalty Points</label>\n            </div>\n            <div class="col-md-8">\n                ' +
((__t = (_.dropdown(balanceOptions, {name: 'loyalty_points:action', class: 'form-control form-group', disabled: 'disabled'}))) == null ? '' : __t) +
'\n                <input type="text" class="form-control" name="loyalty_points" value="" style="width: 120px" disabled>\n                <!-- <input type="text" class="form-control" name="loyalty_points_change_reason" placeholder="Comment..." value="" disabled style="margin-top: 10px"> -->\n            </div>\n        </div>\n    </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit/templates/bulk_edit_footer.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if(currentStep == 2){ ;
__p += '\n<!-- <button class="btn btn-default pull-left previous">\n    <span class="fa fa-chevron-left align-left"></span> Back\n</button> -->\n';
 } ;
__p += '\n\n';
 if(steps[currentStep + 1]){ ;
__p += '\n<button class="btn btn-primary pull-right wide next-step" style="margin-left: 15px">\n    ' +
__e(steps[currentStep + 1]) +
' <span class="fa fa-chevron-right align-right"></span>\n</button>\n';
 } ;
__p += '\n\n';
 if(!steps[currentStep + 1]){ ;
__p += '\n<button class="btn btn-primary pull-right" data-dismiss="modal">Close</button>\n';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit/templates/bulk_edit_status.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row">\n   <div class="col-md-offset-4 col-md-4">\n      <h1 style="text-align: center">\n         ';
 if(status == 'ready' || status == 'in-progress'){ ;
__p += '\n         Editing <strong>' +
__e(accounting.formatNumber(totalRecords)) +
'</strong> ' +
__e(type) +
'\n         ';
 }else if(status == 'cancelled'){ ;
__p += '\n         Bulk Edit Cancelled\n         ';
 }else{ ;
__p += '\n         Bulk Edit Complete!\n         ';
 } ;
__p += '\n      </h1>\n   </div>\n</div>\n<div class="row">\n   <div class="col-md-offset-4 col-md-4">\n      <div class="progress" style="margin-top: 20px; margin-bottom: 10px;">\n         <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' +
__e(percentComplete) +
'" aria-valuemin="0" aria-valuemax="100" style="width: ' +
__e(percentComplete) +
'%"></div>\n      </div>\n      ';
 if(status != 'cancelled'){ ;
__p += '\n      <p style="display: block; text-align: center">Editing... ' +
__e(percentComplete) +
'% complete</p>\n      ';
 }else{ ;
__p += '\n      <p style="display: block; text-align: center">Cancelled... ' +
__e(percentComplete) +
'% complete</p>\n      ';
 } ;
__p += '\n   </div>\n</div>\n\n';
 if(status != 'cancelled' && status != 'completed'){ ;
__p += '\n<div class="row" style="margin-top: 30px;">\n   <div class="col-md-offset-4 col-md-4">\n      <button class="btn btn-lg btn-danger cancel" style="display: block; width: 150px; margin: 0 auto;">Cancel Bulk Edit</button>\n   </div>\n</div>\n<div class="row" style="margin-top: 50px;">\n   <div class="col-md-offset-4 col-md-4" style="text-align: center;">\n      You may now close this window.<br />\n      Re-open this window to check on progress.\n   </div>\n</div>\n';
 } ;
__p += '\n\n';
 if(status == 'completed' || status == 'cancelled'){ ;
__p += '\n<div class="row" style="margin-top: 30px;">\n   <div class="col-xs-6 col-md-offset-3 col-md-3">\n      <div class="import-metric bg-primary">\n         <h1>' +
__e(accounting.formatNumber(recordsCompleted)) +
'</h1>\n         <h5>Records Edited</h5>\n      </div>\n   </div>\n   <div class="col-xs-6 col-md-3">\n      <div class="import-metric bg-primary" ';
 if(recordsFailed > 0){ ;
__p += 'style="background-color: #CE6161"';
 } ;
__p += '>\n         <h1>' +
__e(accounting.formatNumber(recordsFailed)) +
'</h1>\n         <h5>Records Failed</h5>\n      </div>\n   </div>\n</div>\n\n';
 if(response && response.errors && _.toArray(response.errors).length > 0){ ;
__p += '\n<div class="row" style="margin-top: 20px">\n   <div class="col-sm-12 col-md-offset-3 col-md-6">\n      <h2 class="text-danger">Errors</h2>\n      <table class="table table-light" style="width: 100%">\n         <thead>\n         <tr>\n            <th>Record #</th>\n            <th>Error</th>\n         </tr>\n         </thead>\n         <tbody>\n         ';
 $.each(response.errors, function(x, message){ ;
__p += '\n         <tr>\n            <td>' +
__e(x) +
'</td>\n            <td>' +
__e(message) +
'</td>\n         </tr>\n         ';
 }) ;
__p += '\n         </tbody>\n      </table>\n   </div>\n</div>\n';
 } ;
__p += '\n';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit/templates/customer_group.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="col-md-10">\n	' +
__e(label) +
'\n</div>\n<div class="col-md-2">\n	<button class="btn btn-sm btn-default btn-muted delete">\n		<i class="fa fa-trash"></i>\n	</button>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit/templates/customer_household_member.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="col-md-10">\n	' +
__e(first_name) +
' ' +
__e(last_name) +
'\n</div>\n<div class="col-md-2">\n	<button class="btn btn-sm btn-default btn-muted delete pull-right">\n		<i class="fa fa-trash"></i>\n	</button>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit/templates/customer_minimum_charge.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if(date_added != ''){
	date_added = moment(date_added, 'YYYY-MM-DD').format('MM/DD/YYYY');
} ;
__p += '\n<div class="col-md-6">\n	<strong>' +
__e(accounting.formatMoney(minimum_amount)) +
'</strong> - ' +
__e(name) +
'\n</div>\n<div class="col-md-4">\n	<input type="text" class="form-control start-date" style="padding: 4px 6px; height: 30px" placeholder="Date Added" value="' +
__e(date_added) +
'">\n</div>\n<div class="col-md-2">\n	<button class="btn btn-default btn-muted btn-sm delete pull-right" type="button">\n		<i class="fa fa-trash"></i>\n	</button>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit/templates/customer_pass.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-md-12">\n	<h5 class="name pull-left">\n		';
 if(multi_customer && customer){ ;
__p += '\n		' +
__e(customer.first_name+' '+customer.last_name) +
' -\n		';
 } ;
__p += '\n		' +
__e(name) +
'\n	</h5>\n	<button type="button" class="btn btn-default btn-sm pull-right disconnect">\n		<span class="fa fa-trash"></span>\n	</button>\n</div>\n<div class="col-md-12">\n	';
 if(start_date != ''){
	start_date = moment(start_date).format('M/D/YY');
	}
	if(end_date != ''){
	end_date = moment(end_date).format('M/D/YY');
	} ;
__p += '\n	<div style="color: #888">\n		' +
__e(start_date) +
' to ' +
__e(end_date) +
'\n		';
 if(!is_valid){ ;
__p += '\n		<span class="label label-danger">Not Valid</span>\n		';
 }else{ ;
__p += '\n		<span class="label label-success">Valid</span>\n		';
 } ;
__p += '\n	</div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit/templates/customer_recurring_charge.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="record-card">\n	<div class="record-card-title">\n		<h4 class="record-card-title" style="margin-top: 2px; float: left;">' +
__e(name) +
'</h4>\n		<button class="btn btn-sm btn-default delete pull-right">\n			<i class="fa fa-trash"></i>\n		</button>\n	</div>\n	';
 _.each(items, function(charge){ ;
__p += '\n	<div style="overflow: hidden; padding: 0px 10px 5px 10px; margin-bottom: 5px;">\n		<div class="pull-left" style="line-height: 30px;">\n			<strong>' +
__e(charge.item.name) +
'</strong>\n			- ' +
__e(accounting.formatMoney(charge.item.total)) +
'\n		</div>\n		<div class="pull-right" style="line-height: 30px;">\n			<label class="btn btn-sm btn-default">\n				Generate Prorated Charge\n				<input type="checkbox" class="prorate" name="prorate:' +
__e(charge.id) +
'" value="' +
__e(charge.id) +
'" style="position: relative; bottom: -2px;">\n			</label>\n		</div>\n	</div>\n	';
 });
__p += '\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/add_customer_credit_cards_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\r\n        <i class="fa fa-question-o pull-right header-icon"></i>\r\n        <h4 class="modal-title">\r\n            Add Credit Card\r\n        </h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <div id="payment_iframe_container">\r\n            <iframe id="payment_iframe" href=""></iframe>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class="modal-footer">\r\n    <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\r\n</div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customer_accounts.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<form>\r\n\r\n    ';
 if (true || !App.data.user.get('acl').can('update', 'Customer/AccountBalance')) { ;
__p += '\r\n    <div class="clearfix">\r\n      <div class="col-md-6">\r\n\r\n        <div class="form-group panel-group">\r\n          <div class="panel panel-default">\r\n            <div class="panel-heading row">\r\n              <h4 class="panel-title col-md-6">' +
__e(member_account_label) +
'</h4>\r\n              <div class="acct_bal col-md-6 text-right">' +
__e(accounting.formatMoney(member_account_balance)) +
'</div>\r\n            </div>\r\n            <div class="panel-body">\r\n                ';
 if (household_id != '') { ;
__p += '\r\n                <div class="form-group">\r\n                    All account transactions can be found under ' +
__e(household_head.first_name) +
' ' +
__e(household_head.last_name) +
'.\r\n                </div>\r\n\r\n                ';
 } else { ;
__p += '\r\n                <div class="form-group">\r\n                  <div class="row">\r\n                    <div class="col-md-6">\r\n                      <div class="btn-group three_btn" data-toggle="buttons">\r\n                        <label class="btn btn-secondary active">\r\n                          <input type="radio" name="member_account_option" value="increase" autocomplete="off" checked> +\r\n                        </label>\r\n                        <label class="btn btn-secondary ">\r\n                          <input type="radio" name="member_account_option" value="decrease" autocomplete="off"> -\r\n                        </label>\r\n                        <label class="btn btn-secondary ">\r\n                          <input type="radio" name="member_account_option" value="set" autocomplete="off"> Replace With\r\n                        </label>\r\n                      </div>\r\n                    </div>\r\n                    <div class="col-md-6">\r\n                      <div class="input-group">\r\n                      <div class="input-group-addon">' +
__e(SETTINGS.currency_symbol ) +
'</div>\r\n                      <input  name="member_account_balance" class="form-control" id="customer-member-balance" value=""\r\n                        ';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\r\n                        disabled="disabled"\r\n                        ';
 } ;
__p += '\r\n                      />\r\n                      </div>\r\n                    </div>\r\n                    <div class="col-md-6 pull-right">\r\n                      <div class="form-group pull-right marg-top-10">\r\n                        <label for="new_member_balance"> New Balance</label>\r\n                          <div class="new_balance">\r\n                            <input id="new_member_balance"  disabled />\r\n                          </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <input name="member_balance_change_reason" class="form-control" placeholder="Comment or Reason..."/>\r\n                </div>\r\n                <button class="btn btn-primary btn-sm pull-right" id="confirm_member_balance_btn">Confirm New Balance</button>\r\n\r\n                ';
 } ;
__p += '\r\n              </div><!--Panel Body Close-->\r\n            </div><!--End Panel-->\r\n          </div><!--End Panel Group-->\r\n\r\n          <div class="checkbox-control-container">\r\n            <input type="checkbox" value="1" data-unchecked-value="0" id="member_account_balance_allow_negative" name="member_account_balance_allow_negative" style="margin-top: 5px;" ';
 if(member_account_balance_allow_negative == "1"){ print('checked') };
__p += '\r\n              ';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\r\n                disabled="disabled"\r\n              ';
 } ;
__p += '\r\n            />\r\n            <label for="member_account_balance_allow_negative" class="control-label">Allow Negative Balance</label>\r\n          </div>\r\n\r\n          <div class="row">\r\n            <div class="col-md-6">\r\n              <label for="customer-member-account-limit" class="control-label" style="margin-top: 0;">' +
__e(member_account_label) +
' Limit</label>\r\n              <div class="input-group">\r\n                <div class="input-group-addon">(-)</div>\r\n                <input  name="member_account_limit" class="form-control" id="customer-member-account-limit" value="' +
__e(accounting.formatNumber(Math.abs(member_account_limit), 2)) +
'"\r\n                  ';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\r\n                    disabled="disabled"\r\n                  ';
 } ;
__p += '\r\n                />\r\n              </div>\r\n            </div>\r\n            <div class="col-md-6">\r\n              <div class="history" id="customer-member-account-transactions">\r\n                <a class="btn btn-secondary pull-right btn-sm marg-top-10" data-toggle="collapse" data-parent="#collapse-member-account-transactions" href="#collapse-member-account-transactions"> Toggle Transaction History</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n            <div class="col-xs-12">\r\n              <div id="collapse-member-account-transactions" class="panel-collapse collapse">\r\n                <div class="row" style="border-bottom: 1px solid #cccccc;">\r\n                  <div class="col-xs-2 column-header">Date</div>\r\n                  <div class="col-xs-6 column-header">Description</div>\r\n                  <div class="col-xs-2 column-header">Course</div>\r\n                  <div class="col-xs-2 pull-right column-header">Amount</div>\r\n                </div>\r\n\r\n                ';
 if(account_transactions && account_transactions['member'] && account_transactions['member'].length > 0){ ;
__p += '\r\n                ';
_.each(account_transactions['member'], function(transaction){ ;
__p += '\r\n\r\n                  <div id="customer-member-account-transaction-list">\r\n                    <div class="row">\r\n                      <div class="col-xs-2">' +
__e(moment(transaction.trans_date, 'YYYY-MM-DD HH:mm:ss').format('M/D/YY')) +
'</div>\r\n                      <div class="col-xs-6 text-muted">\r\n                        <div><small><i>' +
__e(transaction.trans_comment) +
'</i></small></div>\r\n                        ';

                        var regex = /<br\s*[\/]?>/gi;
                        ;
__p += '\r\n                        <div><small></i>' +
__e(transaction.trans_description.replace(regex, "\n")) +
'</i></small></div>\r\n                      </div>\r\n                      <div class="col-xs-2">\r\n                        ' +
__e(transaction.course ) +
'\r\n                      </div>\r\n                      <div class="col-xs-2"><span class="pull-right ' +
__e(transaction.trans_amount < 0 ? 'negative-balance' : 'positive-balance') +
'"><strong>' +
__e(accounting.formatMoney(transaction.trans_amount)) +
'</strong></span></div>\r\n                    </div>\r\n                  </div>\r\n\r\n                  ';
 }) ;
__p += '\r\n                  ';
 }else{ ;
__p += '\r\n                    <div class="row">\r\n                      <div class="col-md-12 text-muted">No Transactions</div>\r\n                    </div>\r\n                  ';
 } ;
__p += '\r\n                </div> <!-- End of Customer Transaction History -->\r\n              </div>\r\n            <!--<button class="btn btn-primary pull-right view-member-account-transaction-report">View Report</button>-->\r\n        </div> <!-- End of Member Account Section -->\r\n\r\n        <div class="col-md-6"><!-- Beginning Pro Shop Account Section-->\r\n            <div class="form-group panel-group">\r\n              <div class="panel panel-default">\r\n                <div class="panel-heading row">\r\n                  <h4 class="panel-title col-md-6">' +
__e(customer_account_label) +
'</h4>\r\n                  <div class="acct_bal col-md-6 text-right">' +
__e(accounting.formatMoney(account_balance)) +
'</div>\r\n                </div>\r\n                <div class="panel-body">\r\n                  ';
 if (household_id != '') { ;
__p += '\r\n                    <div class="form-group">\r\n                    All account transactions can be found under ' +
__e(household_head.first_name) +
' ' +
__e(household_head.last_name) +
'.\r\n                    </div>\r\n\r\n                  ';
 } else { ;
__p += '\r\n                    <div class="form-group">\r\n                      <div class="row">\r\n                        <div class="col-md-6">\r\n                          <div class="btn-group three_btn" data-toggle="buttons">\r\n                            <label class="btn btn-secondary active">\r\n                              <input type="radio" name="account_balance_option" value="increase" autocomplete="off" checked> +\r\n                            </label>\r\n                            <label class="btn btn-secondary ">\r\n                              <input type="radio" name="account_balance_option" value="decrease" autocomplete="off"> -\r\n                            </label>\r\n                            <label class="btn btn-secondary ">\r\n                              <input type="radio" name="account_balance_option" value="set" autocomplete="off"> Replace With\r\n                            </label>\r\n                          </div>\r\n                        </div>\r\n\r\n                        <div class="col-md-6">\r\n                          <div class="input-group">\r\n                            <div class="input-group-addon">' +
__e(SETTINGS.currency_symbol ) +
'</div>\r\n                            <input  name="account_balance" class="form-control" id="customer-customer-credit" value=""\r\n                              ';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\r\n                                disabled="disabled"\r\n                              ';
 } ;
__p += '\r\n                            />\r\n                          </div>\r\n                        </div>\r\n                        <div class="col-md-6 pull-right">\r\n                          <div class="form-group pull-right marg-top-10">\r\n                            <label for="new_account_balance" >New Balance</label>\r\n                            <div class="new_balance">\r\n                              <input id="new_account_balance" disabled />\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class="form-group">\r\n                      <input name="account_balance_change_reason" class="form-control" placeholder="Comment or Reason..."/>\r\n                    </div>\r\n                    <button class="btn btn-primary btn-sm pull-right" id="confirm_account_balance_btn">Confirm New Balance</button>\r\n                  ';
 } ;
__p += '\r\n                </div><!--Panel Body Close-->\r\n              </div><!--End Panel Body-->\r\n            </div><!-- End Panel Group-->\r\n\r\n            <div class="checkbox-control-container">\r\n              <input type="checkbox" value="1" data-unchecked-value="0" id="account_balance_allow_negative" name="account_balance_allow_negative" style="margin-top: 5px;" ';
 if(account_balance_allow_negative == "1"){ print('checked') };
__p += '\r\n                ';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\r\n                  disabled="disabled"\r\n                ';
 } ;
__p += '\r\n              />\r\n              <label for="account_balance_allow_negative" class="control-label">Allow Negative Balance</label>\r\n            </div>\r\n\r\n            <div class="row">\r\n              <div class="col-md-6">\r\n                <label for="customer-account-limit" class="control-label" style="margin-top: 0;">' +
__e(customer_account_label) +
' Limit</label>\r\n                <div class="input-group">\r\n                  <div class="input-group-addon">(-)</div>\r\n                  <input  name="account_limit" class="form-control" id="customer-account-limit" value="' +
__e(accounting.formatNumber(Math.abs(account_limit), 2)) +
'"\r\n                    ';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\r\n                      disabled="disabled"\r\n                    ';
 } ;
__p += '\r\n                  />\r\n                </div>\r\n              </div>\r\n\r\n              <div class="col-md-6">\r\n                <div class="history clearfix" id="customer-account-transactions">\r\n                  <a class="btn btn-secondary pull-right btn-sm marg-top-10" data-toggle="collapse" data-parent="#collapse-account-transactions" href="#collapse-account-transactions">Toggle Transaction History</a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class="col-md-12"> <!--Customer History Table -->\r\n              <div id="collapse-account-transactions" class="panel-collapse collapse">\r\n                <div class="row" style="border-bottom: 1px solid #cccccc;">\r\n                  <div class="col-xs-2 column-header">Date</div>\r\n                  <div class="col-xs-6 column-header">Description</div>\r\n                  <div class="col-xs-2 column-header">Course</div>\r\n                  <div class="col-xs-2 pull-right column-header">Amount</div>\r\n                </div>\r\n\r\n                ';
 if(account_transactions && account_transactions['customer'] && account_transactions['customer'].length > 0){ ;
__p += '\r\n                  ';
_.each(account_transactions['customer'], function(transaction){ ;
__p += '\r\n                    <div id="customer-account-transaction-list">\r\n                      <div class="row">\r\n                        <div class="col-xs-2">' +
__e(moment(transaction.trans_date, 'YYYY-MM-DD HH:mm:ss').format('M/D/YY')) +
'</div>\r\n                        <div class="col-xs-6 text-muted">\r\n                          <div><small><i>' +
__e(transaction.trans_comment) +
'</i></small></div>\r\n                          ';

                            var regex = /<br\s*[\/]?>/gi;
                          ;
__p += '\r\n                          <div><small></i>' +
__e(transaction.trans_description.replace(regex, "\n")) +
'</i></small></div>\r\n                        </div>\r\n                        <div class="col-xs-2">\r\n                          ' +
__e(transaction.course ) +
'\r\n                        </div>\r\n                        <div class="col-xs-2"><span class="pull-right ' +
__e(transaction.trans_amount < 0 ? 'negative-balance' : 'positive-balance') +
'"><strong>' +
__e(accounting.formatMoney(transaction.trans_amount)) +
'</strong></span></div>\r\n                      </div>\r\n                    </div>\r\n\r\n                ';
 }) ;
__p += '\r\n                ';
 }else{ ;
__p += '\r\n                  <div class="row">\r\n                    <div class="col-md-12 text-muted">No Transactions</div>\r\n                  </div>\r\n                ';
 } ;
__p += '\r\n                </div>\r\n              </div><!-- End of Pro Shop History Table-->\r\n\r\n              <!--<button class="btn btn-primary pull-right view-account-transaction-report">View Report</button>-->\r\n            </div>\r\n          </div>\r\n    <hr>\r\n      <div class="form-group col-md-12">\r\n        <div class="col-md-12 checkbox-control-container">\r\n            <input type="checkbox" value="1" data-unchecked-value="0" id="customer-member" name="member" style="margin-top: 0px;" ';
 if(member == "1"){ print('checked') };
__p += '\r\n            ';
 if (!App.data.user.get('acl').can('update', 'Customer/AccountBalance')){ ;
__p += '\r\n            disabled="disabled"\r\n            ';
 } ;
__p += '\r\n            />\r\n            <label for="customer-member" class="control-label">Customer is Member</label>\r\n        </div>\r\n    </div>\r\n  </div>\r\n    ';
 } ;
__p += '\r\n</div>\r\n<hr>\r\n    <div class="clearfix">\r\n      <div class="form-group col-md-12">\r\n        <div class="col-md-12 checkbox-control-container">\r\n          <input type="checkbox" name="taxable" id="customer-non-taxable" value="0" data-unchecked-value="1" style="margin-top: 10px;" ';
 if(taxable == "0"){ print('checked') };
__p += ' />\r\n          <label for="customer-non-taxable" class="control-label">Customer is Non-taxable</label>\r\n        </div>\r\n        <div class="col-md-12 checkbox-control-container">\r\n          <input type="checkbox" name="use_loyalty" id="customer-allow-loyalty" value="1" data-unchecked-value="0" style="margin-top: 10px;" ';
 if(use_loyalty == 1){ print('checked') };
__p += ' />\r\n          <label for="customer-allow-loyalty" class="control-label">Allow Loyalty</label>\r\n        </div>\r\n      </div>\r\n      <div class="form-group col-md-6">\r\n          <label for="customer-loyalty-points" class="col-md-12 control-label">Loyalty Points</label>\r\n          <div class="col-md-12">\r\n              <input  name="loyalty_points" class="form-control" id="customer-loyalty-points" value="' +
__e(accounting.formatNumber(loyalty_points)) +
'" />\r\n          </div>\r\n      </div>\r\n    </div>\r\n    <hr/>\r\n    <div class="clearfix">\r\n        <div class="col-md-6">\r\n            <div class="panel-group">\r\n                <div class="panel panel-default">\r\n                    <div class="panel-heading">\r\n                        <h4 class="panel-title">\r\n                            <a data-toggle="collapse" data-parent="#customer-credit-cards" href="#customer-credit-cards">\r\n                                Credit Cards\r\n                            </a>\r\n                        </h4>\r\n                    </div>\r\n                    <div id="customer-credit-cards" class="panel-collapse collapse in">\r\n                        <div class="panel-body">\r\n                            <div class="panel-group">\r\n                                <table id="credit-card-table">\r\n                                    ';
 if(typeof credit_cards != 'undefined' && credit_cards.length > 0){ ;
__p += '\r\n                                    ';
_.each(credit_cards, function(credit_card){ ;
__p += '\r\n                                    <thead>\r\n                                    <tr>\r\n                                        <th></th>\r\n                                        <th>Number</th>\r\n                                        <th>Status</th>\r\n                                        <th>Charges</th>\r\n                                        <th>Failed</th>\r\n                                        <th></th>\r\n                                    </tr>\r\n                                    </thead>\r\n                                    <tbody class="credit-card-list">\r\n                                    <tr data-credit-card-id="' +
__e(credit_card.credit_card_id) +
'">\r\n                                        <td>\r\n                                            <div class="col-md-12 credit-card ' +
__e(credit_card.card_type_simple) +
'">\r\n                                            </div>\r\n                                        </td>\r\n                                        <td class="card-number">' +
__e(credit_card.masked_account.replace(/x/g,'').replace(/\*/g,'')) +
'</td>\r\n                                        <td class="card-status"><div class="fa ' +
__e( credit_card.status != 'Declined' ? 'fa-check status-green' : 'fa-ban status-red' ) +
'"></div></td>\r\n                                        <td class="card-success">' +
__e(credit_card.successful_charges) +
'</td>\r\n                                        <td class="card-failure">' +
__e(credit_card.failed_charges) +
'</td>\r\n                                        <td class="card-remove"><div class="remove-credit-card"><i class="fa fa-trash-o"></i></div></td>\r\n                                    </tr>\r\n                                    ';
 }) ;
__p += '\r\n                                    ';
 }else{ ;
__p += '\r\n                                    <tr class="row">\r\n                                        <td colspan=6 class="text-muted">No Credit Cards</td>\r\n                                    </tr>\r\n                                    ';
 } ;
__p += '\r\n\r\n                                    </tbody>\r\n                                </table>\r\n                            </div>\r\n                            <a class="btn btn-muted btn-sm pull-right add-credit-card">Add Card</a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="panel-group" id="customer-passes"></div>\r\n\r\n        </div>\r\n        <div class="col-md-6">\r\n            <div class="panel-group" id="customer-rainchecks">\r\n                <div class="panel panel-default">\r\n                    <div class="panel-heading">\r\n                        <h4 class="panel-title">\r\n                            <a data-toggle="collapse" data-parent="#customer-rainchecks" href="#collapse-rainchecks">\r\n                                Rainchecks\r\n                            </a>\r\n                            <span class="label label-info count">' +
__e(rainchecks.length) +
'</span>\r\n                        </h4>\r\n                    </div>\r\n                    <div id="collapse-rainchecks" class="panel-collapse collapse in">\r\n                        <div class="panel-body">\r\n                            <div class="panel-group" id="customer-rainchecks">\r\n                                ';
 if(rainchecks && rainchecks.length > 0){ ;
__p += '\r\n                                ';
_.each(rainchecks, function(raincheck){ ;
__p += '\r\n                                <div class="row">\r\n                                    <div class="col-xs-2">#' +
__e(raincheck.raincheck_number) +
'</div>\r\n                                    <div class="col-xs-8 text-muted">\r\n                                        Issued ' +
__e(moment(raincheck.date_issued, 'YYYY-MM-DD HH:mm:ss').format('M/D/YY')) +
'\r\n                                        ';
 if(moment(raincheck.expiry_date).isValid() && moment(raincheck.expiry_date).isBefore(moment())){ ;
__p += '\r\n                                        <span class="label label-danger pull-right">\r\n														Expired ' +
__e(moment(raincheck.expiry_date).format('M/D/YY')) +
'\r\n													</span>\r\n                                        ';
 }else if(moment(raincheck.expiry_date).isValid()){ ;
__p += '\r\n                                        <span class="label label-success pull-right">\r\n														Expires ' +
__e(moment(raincheck.expiry_date).format('M/D/YY')) +
'\r\n													</span>\r\n                                        ';
 }else{ ;
__p += '\r\n                                        <span class="label label-success pull-right">No Expiration</span>\r\n                                        ';
 } ;
__p += '\r\n                                    </div>\r\n                                    <div class="col-xs-2"><span class="pull-right">' +
__e(accounting.formatMoney(raincheck.total)) +
'</span></div>\r\n                                </div>\r\n                                ';
 }) ;
__p += '\r\n                                ';
 }else{ ;
__p += '\r\n                                <div class="row">\r\n                                    <div class="col-md-12 text-muted">No Rainchecks</div>\r\n                                </div>\r\n                                ';
 } ;
__p += '\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="panel-group" id="customer-household" style="';
 print(household_id ? 'display:none' : '') ;
__p += '" ></div>\r\n        </div>\r\n    </div>\r\n\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customer_accounts_summary.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="hidden-sm hidden-xs">\r\n    <div class="col-md-6">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default summary-panel">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        ' +
__e(member_account_label) +
'\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body ' +
__e(member_account_balance < 0 ? 'negative-balance' : 'positive-balance') +
'">\r\n                    ' +
__e(accounting.formatMoney(member_account_balance)) +
'\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="col-md-6">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default summary-panel">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        ' +
__e(customer_account_label) +
'\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body ' +
__e(account_balance < 0 ? 'negative-balance' : 'positive-balance') +
'">\r\n                    ' +
__e(accounting.formatMoney(account_balance)) +
'\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class="hidden-sm hidden-xs">\r\n    <div class="col-md-6">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default summary-panel">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        Loyalty\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body">\r\n                    ' +
__e(accounting.formatNumber(loyalty_points)) +
'\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="col-md-6">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default summary-panel">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        Last Visit\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body">\r\n                    ';
 if(typeof last_visit != 'undefined'){
                    print(moment(last_visit, 'YYYY-MM-DD HH:mm:ss').format('M/D/YY'));
                    } else { ;
__p += '\r\n                    -\r\n                    ';
 } ;
__p += '\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customer_billing.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '\r\n<div class="clearfix">\r\n    <div class="col-md-12 billing-info-message">\r\n        The updated Billing and Invoicing software is under development. <br/>For now, to edit Billing and Invoicing settings,\r\n        go to the <a href="/index.php/invoices">Invoice</a> section or the <a href="/index.php/customers">Old Customers Page</a>. <br/>Thank you.\r\n    </div>\r\n</div>\r\n<div class="clearfix">\r\n    <div class="col-md-12">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        Billing Profiles\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body">\r\n                    <table id="billing-profiles-table">\r\n                        <thead>\r\n                            <tr>\r\n                                <td>BILLING PROFILE NAME</td>\r\n                                <td>CHARGES</td>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody></tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class="clearfix">\r\n    <div class="col-md-12">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        Minimums\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body">\r\n                    <table id="minimum-charges-table">\r\n                        <thead>\r\n                        <tr>\r\n                            <td>MIMIMUM NAME</td>\r\n                            <td>CHARGES</td>\r\n                            <td>TERM</td>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody></tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class="clearfix">\r\n    <div class="col-md-6">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        Unpaid Invoices\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body">\r\n                    <table id="unpaid-invoice-table">\r\n                        <thead>\r\n                        <tr>\r\n                            <td>INVOICE NUMBER</td>\r\n                            <td>CHARGES</td>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody></tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="col-md-6">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        Paid Invoices\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body">\r\n                    <table id="paid-invoice-table">\r\n                        <thead>\r\n                        <tr>\r\n                            <td>INVOICE NUMBER</td>\r\n                            <td>CHARGES</td>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody></tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customer_billing_summary.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="hidden-sm hidden-xs">\r\n    <div class="col-md-12">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default summary-panel">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        Balance Due\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body">\r\n                    ' +
__e(accounting.formatMoney(invoice_balance)) +
'\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="col-md-12">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default summary-panel">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        Last Invoice Date\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body">\r\n                    ';
 if(typeof invoices != 'undefined'){
                        print(moment(invoices[0].date, 'YYYY-MM-DD HH:mm:ss').format('M/D/YY'));
                    } else { ;
__p += '\r\n                        -\r\n                    ';
 } ;
__p += '\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customer_info.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if(birthday == '' || birthday == '0000-00-00'){
    birthday = '';
}else{
    birthday = moment(birthday).format('MM/DD/YYYY');
} ;
__p += '\r\n<form>\r\n    <div class="clearfix">\r\n        <div class="col-md-6 left-column">\r\n            <div class="form-group">\r\n                <label for="customer-first-name" class="col-md-12 control-label">First Name ' +
((__t = (get_required_text('first_name'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(first_name) +
'" name="first_name" id="customer-first-name" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="customer-last-name" class="col-md-12 control-label">Last name ' +
((__t = (get_required_text('last_name'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(last_name) +
'" name="last_name" id="customer-last-name" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="customer-email" class="col-md-12 control-label">Email ' +
((__t = (get_required_text('email'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(email) +
'" name="email" id="customer-email" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="customer-phone" class="col-md-12 control-label">Phone Number ' +
((__t = (get_required_text('phone_number'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control"\r\n                           type=\'tel\'\r\n                           value="' +
__e(phone_number) +
'"\r\n                           name="phone_number"\r\n                           id="customer-phone" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="customer-cell-phone" class="col-md-12 control-label" >Cell Phone Number ' +
((__t = (get_required_text('cell_phone_number'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type=\'tel\'\r\n                           value="' +
__e(cell_phone_number) +
'"\r\n                           name="cell_phone_number"\r\n                           id="customer-cell-phone" />\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-6">\r\n            <div class="form-group">\r\n                <label for="customer-address1" class="col-md-12 control-label">Address ' +
((__t = (get_required_text('address_1'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(address_1) +
'" name="address_1" id="customer-address1" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="customer-addresss2" class="col-md-12 control-label"></label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(address_2) +
'" name="address_2" id="customer-address2" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="customer-city" class="col-md-12 control-label">City ' +
((__t = (get_required_text('city'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(city) +
'" name="city" id="customer-city" />\r\n                </div>\r\n            </div>\r\n            ';

            if(SETTINGS.country == "UK"){
            ;
__p += '\r\n            <div class="form-group">\r\n                <label for="customer-state" class="col-md-12 control-label">County ' +
((__t = (get_required_text('state'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(state) +
'" name="state" id="customer-state" />\r\n                </div>\r\n            </div>\r\n\r\n            ';
 }else{ ;
__p += '\r\n            <div class="form-group">\r\n                <label for="customer-state" class="col-md-12 control-label">State ' +
((__t = (get_required_text('state'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown(US_STATES, {"name":"state", "id":"customer-state", "class":"form-control"}, state))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            ';
 } ;
__p += '\r\n\r\n            <div class="form-group">\r\n                <label for="customer-zip" class="col-md-12 control-label">Zip/Postal Code ' +
((__t = (get_required_text('zip'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="number" value="' +
__e(zip) +
'" name="zip" id="customer-zip" />\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n      <hr />\r\n    <div class="clearfix">\r\n        <div class="col-md-12">\r\n            <div class="form-group">\r\n                <label for="customer-account-number" class="col-md-12 control-label">Account Number ' +
((__t = (get_required_text('account_number'))) == null ? '' : __t) +
'</label>\r\n                <div class="col-md-12">\r\n                    <input type="text" name="account_number" class="form-control" id="customer-account-number" value="' +
__e(account_number) +
'" />\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-6">\r\n            <div class="form-group">\r\n                <label for="customer-price-class" class="col-md-12 control-label">Price Class</label>\r\n                <div class="col-md-12">\r\n                    <select name="price_class" id="customer-price-class" class="form-control" ';
 if (!App.data.user.get('acl').can('update', 'Customer/PriceClass')){print('disabled="disabled"') } ;
__p += ' >\r\n                    ';
 _.each(App.data.course.get('price_class_objects'), function(price_class_object){ ;
__p += '\r\n                    <option value="' +
__e(price_class_object['class_id']) +
'" ';
 if(price_class_object['class_id'] == price_class) {print('selected')} ;
__p += '>' +
__e(price_class_object['name']) +
'</option>\r\n                    ';
 }) ;
__p += '\r\n                    </select>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-6">\r\n            <div class="form-group">\r\n                <label for="customer-discount" class="col-md-12 control-label">Discount</label>\r\n                <div class="col-md-12">\r\n                    <div class="input-group">\r\n                        <input type="number" name="discount" class="form-control" id="customer-discount" value="' +
__e(discount) +
'" /><div class="input-group-addon">%</div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <hr/>\r\n    <div class="">\r\n        <div class="col-md-6">\r\n            <div class="form-group clearfix">\r\n                <label for="customer-birthday" class="col-md-12 control-label">Date of Birth <span class="customer-age">' +
((__t = ( isNaN(new Date(birthday))? '' : '('+Math.floor((new Date - new Date(birthday))/(60000*60*24*365))+' yrs)' )) == null ? '' : __t) +
'</span>\r\n                </label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
((__t = ( (new Date(birthday)).toLocaleDateString())) == null ? '' : __t) +
'" name="birthday" id="customer-birthday" placeholder="MM/DD/YYYY" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group clearfix">\r\n                <label for="customer-date-created" class="col-md-12 control-label">Date Joined</label>\r\n                <div class="col-md-12">\r\n                    ';
 if(date_created == "" || _.isNull(date_created)){ ;
__p += '\r\n                    <input class="form-control" type="text" value="" name="date_created" id="customer-date-created" placeholder="MM/DD/YYYY" />\r\n                    ';
 } else { ;
__p += '\r\n                    <input class="form-control" type="text" value="' +
((__t = ( (new Date(date_created)).toLocaleDateString())) == null ? '' : __t) +
'" name="date_created" id="customer-date-created" placeholder="MM/DD/YYYY" />\r\n                    ';
 } ;
__p += '\r\n                </div>\r\n            </div>\r\n\r\n            <div class="form-group clearfix">\r\n                <label for="customer-photo" class="col-md-12 control-label">Customer Photo</label>\r\n                <div class="col-md-12">\r\n                  <div class="col-xs-6" style="padding-right: 0px;">\r\n                      <button class="btn btn-destroy btn-sm btn-block delete-photo" style="';
if(!image_id){ ;
__p += 'display: none;';
 } ;
__p += 'display: block;position: absolute;top:0;left:15px;padding: 0;"><i class="fa fa-trash" aria-hidden="true"></i></button>\r\n                      <img class="customer-photo" src="' +
((__t = (photo)) == null ? '' : __t) +
'">\r\n                      <input type="hidden" value="' +
__e(image_id) +
'" id="image_id" name="image_id">\r\n                  </div>\r\n                  <div class="col-xs-6" style="padding-left: 0px;">\r\n                      <button class="btn btn-block btn-muted take-photo pull-right">Take Photo</button>\r\n                      <button class="btn btn-block btn-muted edit-photo pull-right">Upload Photo</button>\r\n                  </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class="form-group clearfix">\r\n                <label for="customer-handicap-account-number" class="col-md-12 control-label">Handicap Account Number</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control"  value="' +
__e(handicap_account_number) +
'" name="handicap_account_number" id="customer-handicap-account-number" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group clearfix">\r\n                <label for="customer-handicap-score" class="col-md-12 control-label">Handicap</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="number" value="' +
__e(handicap_score) +
'" name="handicap_score" id="customer-handicap-score" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group clearfix">\r\n                <label for="customer-status" class="col-md-12 control-label">Customer Status</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"status_flag", "id":"customer-status-flag", "class":"form-control"}, status_flag))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group clearfix">\r\n                <label for="customer-comments" class="col-md-12 control-label">Comments</label>\r\n                <div class="col-md-12">\r\n                    <textarea name="comments" id="customer-comments" class="form-control" style="height: 100px;">' +
__e(comments) +
'</textarea>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-6">\r\n            <div class="panel-group" id="customer-groups">\r\n            </div>\r\n\r\n            <!-- Login credentials -->\r\n            ';
 if(App.data.user.get('acl').can('update', 'Customer/OnlineCredentials')){ ;
__p += '\r\n            <div class="panel-group" id="customer-login-credentials">\r\n                <div class="panel panel-default">\r\n                    <div class="panel-heading">\r\n                        <h4 class="panel-title">\r\n                            <a data-toggle="collapse" data-parent="#customer-login-credentials" href="#collapse-login-credentials">\r\n                                Login Credentials\r\n                            </a>\r\n                        </h4>\r\n                    </div>\r\n                    <div id="collapse-login-credentials" class="panel-collapse collapse in">\r\n                        <div class="panel-body">\r\n                            <div class="form-group">\r\n                                <label for="customer-username" class="control-label">Email</label>\r\n                                    <input type="text" name="username" id="customer-username" class="form-control" value="' +
__e(username) +
'" />\r\n                            </div>\r\n                            <div class="form-group">\r\n                                <label for="customer-new-password" class="control-label">New Password</label>\r\n                                    <input type="password" name="password" class="form-control" id="customer-new-password" value="" />\r\n                            </div>\r\n                            <div class="form-group">\r\n                                <label for="customer-confirm-new-password" class="control-label">Confirm Password</label>\r\n                                    <input type="password" name="confirm_password" class="form-control" id="customer-confirm-new-password" value="" />\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            ';
 } ;
__p += '\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customer_info_summary.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="col-md-12 hidden-sm hidden-xs">\r\n    <div class="panel-group">\r\n        <div class="panel panel-default">\r\n            <div class="panel-heading customer-summary-comments-title">\r\n                <h4 class="panel-title">\r\n                    Customer Notes\r\n                    <div class="customer-summary-status-holder">Status: <span class="customer-summary-status"></span></div>\r\n                </h4>\r\n            </div>\r\n            <div class="panel-body customer-summary-comments">\r\n                ' +
__e(comments) +
'\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customer_marketing.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="clearfix">\r\n    <div class="col-md-4">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        Text Messenger\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body" id="messenger">\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="col-md-8">\r\n        <div class="panel-group">\r\n            <div class="panel panel-default">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title">\r\n                        Most Recent Marketing Campaigns\r\n                    </h4>\r\n                </div>\r\n                <div class="panel-body">\r\n                    ';
 if(recent_campaigns && recent_campaigns.length > 0){ console.dir(recent_campaigns); ;
__p += '\r\n                    ';
_.each(recent_campaigns, function(campaign){ ;
__p += '\r\n                        <div class="row campaign-row">\r\n                            <div class="col-md-1">\r\n                                <div class="campaign-icon"><i class="fa fa-envelope fa-lg" style="color:white;"></i></div>\r\n                            </div>\r\n                            <div class="col-md-11">\r\n                                <div>' +
__e(campaign.name) +
'</div>\r\n                                <div>Created ' +
__e(campaign.date_created) +
'</div>\r\n                            </div>\r\n                            <div class="campaign-row-bottom-border col-md-12"></div>\r\n                        </div>\r\n                    ';
 }); ;
__p += '\r\n                    ';
 }else{ ;
__p += '\r\n                    <div class="">\r\n                        <div class="col-md-12">\r\n                            <div class="col-md-12 text-muted">No Recent Campaigns</div>\r\n                        </div>\r\n                    </div>\r\n                    ';
 } ;
__p += '\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div> \r\n\r\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customer_marketing_summary.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="hidden-sm hidden-xs">\r\n    <div class="row">\r\n        <div class="col-md-6">\r\n            <div class="panel-group">\r\n                <div class="panel panel-default">\r\n                    <div class="panel-heading">\r\n                        <h4 class="panel-title">\r\n                            Quick Stats\r\n                        </h4>\r\n                    </div>\r\n                    <div class="panel-body">\r\n                        <table class="table-condensed customer-marketing-info">\r\n                            <tr>\r\n                                <td>Last Tee Time</td>\r\n                                <td>' +
__e(marketing_customer_info.prev_teetime ) +
'</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Next Tee Time</td>\r\n                                <td>' +
__e(marketing_customer_info.next_teetime ) +
'</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Last Purchased</td>\r\n                                <td>' +
__e(marketing_customer_info.last_purchased ) +
'</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Most Purchased</td>\r\n                                <td>' +
__e(marketing_customer_info.most_purchased ) +
'</td>\r\n                            </tr>\r\n                            ';
 if(marketing_customer_info.teetime_today){ ;
__p += '\r\n                                <tr>\r\n                                    <td>Todays Tee Time</td>\r\n                                    <td>' +
__e(marketing_customer_info.todays_teetime ) +
'</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Reround</td>\r\n                                    <td>' +
__e(marketing_customer_info.todays_reround ) +
'</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td>Carts</td>\r\n                                    <td>' +
__e(marketing_customer_info.cart_numbers ) +
'</td>\r\n                                </tr>\r\n                            ';
 } else { ;
__p += '\r\n                                <tr>\r\n                                    <td colspan="2">&nbsp;</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td colspan="2">No Tee Time Today</td>\r\n                                </tr>\r\n                            ';
 } ;
__p += '\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-6">\r\n            <div class="row"><div class="col-md-12">\r\n                <div class="panel-group ';
 if(opt_out_email){print('email-subscribed')} else {print('email-not-subscribed')};
__p += '">\r\n                    <div class="panel panel-default">\r\n                        <div class="panel-heading">\r\n                            <h4 class="panel-title">\r\n                                Email Subscribed\r\n                            </h4>\r\n                        </div>\r\n                        <div class="panel-body">\r\n                            <i class="subscription-icon"></i><span class="subscription-text"></span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div></div>\r\n            <div class="row">\r\n                <div class="col-md-6">\r\n                    <div class="panel-group ';
 if(texting_status && texting_status != 'unsubscribed'){print('text-subscribed')} else {print('text-not-subscribed')} ;
__p += '">\r\n                        <div class="panel panel-default">\r\n                            <div class="panel-heading">\r\n                                <h4 class="panel-title">\r\n                                    Text Marketing\r\n                                </h4>\r\n                            </div>\r\n                            <div class="panel-body">\r\n                                <i class="subscription-icon"></i><span class="send-invite-link">Send Invite</span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="col-md-6">\r\n                    <div class="panel-group text-subscribed">\r\n                        <div class="panel panel-default">\r\n                            <div class="panel-heading">\r\n                                <h4 class="panel-title">\r\n                                    2-Way Texting\r\n                                </h4>\r\n                            </div>\r\n                            <div class="panel-body text-subscribed">\r\n                                <i class="subscription-icon"></i>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customers.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="advanced-search-dropdown">\n	<div class="col-md-12">\n		<div class="advanced-search" id="advanced-search"></div>\n		<button id="advanced-search-button" class="pull-right btn btn-default">Search</button>\n	</div>\n</div>\n\n<div class="header clearfix hidden-xs">\n	<div class="col-md-2 hidden-sm hidden-xs">\n		<div class="pull-left"><h2>Customers</h2></div>\n		<div class="clearfix"></div>\n	</div>\n	<div class="col-md-10 ">\n		<div class="pull-right form-inline marg-top-5">\n			<div class="btn-group form-group">\n				<button type="button" class="btn btn-tertiary new" >New Customer<i class="fa fa-plus" aria-hidden="true"></i></button>\n				<button type="button" class="btn btn-tertiary groups" >Groups<i class="fa fa-users" aria-hidden="true"></i></button>\n				<button type="button" id="import-list" class="btn btn-tertiary" >Upload List<i class="fa fa-cloud-upload" aria-hidden="true"></i></button>\n				<button type="button" id="export-list" class="btn btn-tertiary" >Export List<i class="fa fa-share-square-o" aria-hidden="true"></i></button>\n			</div>\n			<div class="form-group">\n				<div id="quick-search"></div>\n			</div>\n            <div class="btn-group">\n                <button type="button" class="btn btn-tertiary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>\n                </button>\n                <ul class="dropdown-menu">\n                    <li id="page-dropdown-advanced-search"><a><i class="fa fa-sliders" aria-hidden="true"></i>Advanced Search</a></li>\n					<li id="page-dropdown-settings"><a><i class="fa fa-cogs" aria-hidden="true"></i>Page Settings</a></li>\n					<li id="page-dropdown-show-deleted">\n						<label class="checkbox-inline"><input type="checkbox">Show Deleted Customers</label>\n					</li>\n                </ul>\n            </div>\n		</div>\n	</div>\n</div>\n<div class="header mobile-header clearfix visible-xs-block visible-xs">\n	<div id="mobile-search"></div>\n	<button type="button" class="btn new_cust btn-primary new"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Customer</button>\n</div>\n<div id="mobile-list" class="visible-xs-block visible-xs"></div>\n<div id="main-table" class="hidden-xs customers-page">\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customers_menu.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<!-- Single button -->\n<div class="btn-group">\n    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n        <span class="caret"></span>\n    </button>\n    <ul class="dropdown-menu">\n        <li><a href="#" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a></li>\n        ';
 if(!this.model.get("deleted")){ ;
__p += '\n            <li><a href="#" class="delete"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a></li>\n        ';
 } else { ;
__p += '\n            <li><a href="#" class="undelete"><i class="fa fa-trash-o" aria-hidden="true"></i>Undelete</a></li>\n        ';
 } ;
__p += '\n    </ul>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/customers_table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="nd-table-container layout-box">\n	<table class="table items table-responsive"></table>\n\n</div>\n\n<div class="nd-footer clearfix">\n	<div class="row">\n		<div id="paginator" class="col-md-5 col-sm-6"></div>\n		<div id="state-display" class="col-md-3 hidden-sm hidden-xs"></div>\n		<div class="col-md-4 col-sm-6">\n		 	<div class="selectionButtons btn-group pull-right">\n				<button type="button" class="btn btn-danger controls" action="bulkDelete"><i class="fa fa-trash-o" aria-hidden="true"></i><span class="hidden-sm hidden-xs">Delete</span></button>\n				<button type="button" class="btn btn-primary controls merge" action="merge"><i class="fa fa-compress" aria-hidden="true"></i><span class="hidden-sm hidden-xs">Merge</span></button>\n				<button type="button" class="btn btn-primary controls" action="bulkEdit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="hidden-sm hidden-xs">Bulk Edit</span></button>\n				<!--<button type="button" class="btn btn-primary controls" action="email"><i class="fa fa-envelope-o" aria-hidden="true"></i>Email</button>-->\n				<!--<button type="button" class="btn btn-primary controls" action="text"><i class="fa fa-mobile" aria-hidden="true"></i>Text</button>-->\n			</div>\n		</div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/edit_customer_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\r\n        <i class="fa fa-question-o pull-right header-icon"></i>\r\n        <h4 class="modal-title">\r\n          ' +
__e(first_name) +
' ' +
__e(last_name) +
'<\r\n        </h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <div class="modal-well">\r\n\r\n            <div class="cust_summary" ';
 if (person_id == null) {print("style='display:none;'");} ;
__p += ' >\r\n                <div class="col-md-3">\r\n                  <div class="col-xs-3 col-md-4">\r\n                    <img class="customer-photo" src="' +
((__t = (photo)) == null ? '' : __t) +
'" height="120">\r\n                  </div>\r\n                  <div class="col-xs-9 col-md-8">\r\n                    <div class="customer-displayed-name">' +
__e(first_name) +
' ' +
__e(last_name) +
'</div>\r\n                  </div>\r\n                  <div class="col-xs-12 col-md-12">\r\n                    <span class="customer-displayed-info"><i class="fa fa-envelope fa-fw"></i> ' +
__e(email) +
'</span>\r\n                    <span class="customer-displayed-info"><i class="fa fa-phone fa-fw"></i> ' +
__e(phone_number) +
'</span>\r\n                    <span class="customer-displayed-info"><i class="fa fa-mobile-phone fa-fw"></i> ' +
__e(cell_phone_number) +
'</span>\r\n                  </div>\r\n                </div>\r\n                <div class="col-md-9 customer-summary">\r\n                    Summary\r\n                </div>\r\n            </div>\r\n            <div class="customer-tabs-wrapper tab-navigation-wrapper">\r\n              <div class="customer-tabs"></div>\r\n            </div>\r\n        </div>\r\n        <div class="customer-info tab_area-body clearfix"></div>\r\n    </div>\r\n    <div class="modal-footer">\r\n        <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\r\n        <button class="btn btn-primary pull-right" id="save-changes">Save Changes</button>\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/merge_customer_column_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '    <div class="customer-title">\r\n        <div class="customer-title-position">' +
__e(column) +
'</div>\r\n        <input class="form-control name-search" type="text" value="" id="name-search-' +
__e(column) +
'" data-customer-position="1"/>\r\n        <input type="hidden" value="' +
__e(person_id) +
'" name="person_id_' +
__e(column) +
'"/>\r\n    </div>\r\n    ';
 if (column == 1) { ;
__p += '\r\n    <div class="customer-merge-primary form-group selected">\r\n        <input type="checkbox" value="' +
__e(column) +
'" name="primary_customer_checkbox" id="primary-customer-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox" checked disabled/>\r\n        <label for="primary-customer-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Merge into this customer</label>\r\n        <div class="clear"></div>\r\n    </div>\r\n    ';
 } else { ;
__p += '\r\n    <div class="customer-merge-primary form-group not-selected">\r\n        <label for="primary-customer-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Duplicate Customer</label>\r\n        <div class="clear"></div>\r\n    </div>\r\n    ';
 } ;
__p += '\r\n    <div class="customer-merge-separator"></div>\r\n    ';
 if(person_id) { ;
__p += '\r\n    <div class="customer-merge-personal-' +
__e(column) +
'">\r\n        <div class="customer-merge-header">\r\n            Personal Information\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="name_checkbox" id="name-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="name-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Name</label>\r\n            <div class="col-md-12 form-control-container">\r\n                <input class="form-control" type="text" value="' +
__e(first_name) +
' ' +
__e(last_name) +
'" disabled/>\r\n            </div>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="email_checkbox" id="email-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="email-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Email Address</label>\r\n            <div class="col-md-12 form-control-container">\r\n                <input class="form-control" type="text" value="' +
__e(email) +
'"  disabled/>\r\n            </div>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="phone_number_checkbox" id="phone-number-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="phone-number-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Phone Number</label>\r\n            <div class="col-md-12 form-control-container">\r\n                <input class="form-control" type="text" value="' +
__e(phone_number) +
'"  disabled/>\r\n            </div>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="cell_phone_number_checkbox" id="cell-number-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="cell-number-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Mobile Number</label>\r\n            <div class="col-md-12 form-control-container">\r\n                <input class="form-control" type="text" value="' +
__e(cell_phone_number) +
'"  disabled/>\r\n            </div>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="comments_checkbox" id="comments-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="comments-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Comments</label>\r\n            <div class="col-md-12 form-control-container">\r\n                <input class="form-control" type="text" value="' +
__e(comments) +
'"  disabled/>\r\n            </div>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="1" id="select-all-personal-checkbox-' +
__e(column) +
'" data-section="personal" class="customer-merge-checkbox-radio select-all" />\r\n            <label for="select-all-personal-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Select All</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="customer-merge-separator"></div>\r\n    </div>\r\n    <div class="customer-merge-accounts-' +
__e(column) +
'">\r\n        <div class="customer-merge-header">\r\n            Account Info\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="account_number_checkbox" id="account-number-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="account-number-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Account Number</label>\r\n            <div class="col-md-12 form-control-container">\r\n                <input class="form-control" type="text" value="' +
__e(account_number) +
'"  disabled/>\r\n            </div>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="price_class_checkbox" id="price-class-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="price-class-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Price Class</label>\r\n            <div class="col-md-12 form-control-container">\r\n                <input class="form-control" type="text" value="' +
__e(price_class) +
'"  disabled/>\r\n            </div>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="discount_checkbox" id="discount-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="discount-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Discount</label>\r\n            <div class="col-md-12 form-control-container">\r\n                <input class="form-control" type="text" value="' +
__e(discount) +
'"  disabled/>\r\n            </div>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="taxable_checkbox" id="taxable-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="taxable-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">' +
__e(parseInt(taxable) ? 'Taxable' : 'Non-taxable') +
'</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="member_checkbox" id="member-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="member-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Member</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="minimums_checkbox" id="minimums-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox-radio" />\r\n            <label for="minimums-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Minimums</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="1" id="select-all-accounts-checkbox-' +
__e(column) +
'" data-section="accounts" class="customer-merge-checkbox-radio select-all" />\r\n            <label for="select-all-accounts-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Select All</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="customer-merge-separator"></div>\r\n    </div>\r\n    <div class="customer-merge-groups-and-passes-' +
__e(column) +
'">\r\n        <div class="customer-merge-header">\r\n            Groups and Passes\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="groups_checkbox" id="groups-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox" ';
 if (column == 1) {print('checked disabled')} ;
__p += '/>\r\n            <label for="groups-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Groups</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="passes_checkbox" id="passes-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox" ';
 if (column == 1) {print('checked disabled')} ;
__p += '/>\r\n            <label for="passes-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Passes</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group ';
 if (column != 1) {print('not-')} ;
__p += 'selected">\r\n            <input type="checkbox" value="1" id="select-all-groups-and-passes-checkbox-' +
__e(column) +
'" data-section="groups-and-passes" class="customer-merge-checkbox select-all" ';
 if (column == 1) {print('checked disabled')} ;
__p += '/>\r\n            <label for="select-all-groups-and-passes-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Select All</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="customer-merge-separator"></div>\r\n    </div>\r\n    <div class="customer-merge-miscellaneous-' +
__e(column) +
'">\r\n        <div class="customer-merge-header">\r\n            Miscellaneous\r\n        </div>\r\n        <div class="form-group selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="member_balance_checkbox" id="member-balance-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox" checked disabled/>\r\n            <label for="member-balance-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Member Balance</label>\r\n            <div class="col-md-12 form-control-container">\r\n                <input class="form-control" type="text" value="$' +
__e(member_account_balance) +
'"  disabled/>\r\n            </div>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group selected">\r\n            <input type="checkbox" value="' +
__e(column) +
'" name="customer_balance_checkbox" id="customer-balance-checkbox-' +
__e(column) +
'" class="customer-merge-checkbox" checked disabled/>\r\n            <label for="customer-balance-checkbox-' +
__e(column) +
'" class="col-md-12 control-label">Customer Balance</label>\r\n            <div class="col-md-12 form-control-container">\r\n                <input class="form-control" type="text" value="$' +
__e(account_balance) +
'"  disabled/>\r\n            </div>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group">\r\n            <input type="checkbox" value="1" class="customer-merge-checkbox" checked disabled/>\r\n            <label class="col-md-12 control-label">Update Old Sales</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group">\r\n            <input type="checkbox" value="1" class="customer-merge-checkbox"  checked disabled/>\r\n            <label class="col-md-12 control-label">Update Tee Times</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group">\r\n            <input type="checkbox" value="1" class="customer-merge-checkbox"  checked disabled/>\r\n            <label class="col-md-12 control-label">Transfer Gift Cards</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group">\r\n            <input type="checkbox" value="1" class="customer-merge-checkbox"  checked disabled/>\r\n            <label class="col-md-12 control-label">Transfer Punch Cards</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group">\r\n            <input type="checkbox" value="1" class="customer-merge-checkbox"  checked disabled/>\r\n            <label class="col-md-12 control-label">Transfer Credit Cards</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group">\r\n            <input type="checkbox" value="1" class="customer-merge-checkbox"  checked disabled/>\r\n            <label class="col-md-12 control-label">Transfer Invoices/Membership Packages</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group">\r\n            <input type="checkbox" value="1" class="customer-merge-checkbox"  checked disabled/>\r\n            <label class="col-md-12 control-label">Transfer Loyalty Points</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="form-group">\r\n            <input type="checkbox" value="1" class="customer-merge-checkbox"  checked disabled/>\r\n            <label class="col-md-12 control-label">Household Members</label>\r\n            <div class="clear"></div>\r\n        </div>\r\n        <div class="customer-merge-separator"></div>\r\n    </div>\r\n    ';
 } ;
__p += '\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/merge_customer_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\r\n        <i class="fa fa-question-o pull-right header-icon"></i>\r\n        <h4 class="modal-title">\r\n            Customer Merge\r\n        </h4>\r\n    </div>\r\n    <div class="modal-body">\r\n        <form id="merge_form" class="col-md-12">\r\n            <div class="customer-columns">\r\n                <div class="col-md-6 merging-customer">\r\n\r\n                </div>\r\n\r\n            <!-- Dividing customer merge view-->\r\n\r\n                <div class="col-md-6 merging-customer">\r\n\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <div class="modal-footer">\r\n        <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\r\n        <button class="btn btn-primary pull-right" id="save-changes">Save Changes</button>\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/tabs.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-md-2 col-sm-3 col-xs-3 selected-tab">\r\n    <div class="tab-selector tab-selector-info">\r\n        Information\r\n    </div>\r\n</div>\r\n<div class="col-md-2 col-sm-3 col-xs-3">\r\n    <div class="tab-selector tab-selector-accounts">\r\n        Accounts\r\n    </div>\r\n</div>\r\n';
 if(person_id != null){ ;
__p += '\r\n';
 if(typeof household_id != 'undefined' && household_id == ''){ ;
__p += '\r\n<div class="col-md-2 col-sm-3 col-xs-3">\r\n    <div class="tab-selector tab-selector-billing">\r\n        Billing\r\n    </div>\r\n</div>\r\n';
 } ;
__p += '\r\n<div class="col-md-2 col-sm-3 col-xs-3">\r\n    <div class="tab-selector tab-selector-marketing">\r\n        Communication\r\n    </div>\r\n</div>\r\n';
 } ;
__p += '\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit_support/templates/bulk_edit_support.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<!--button class="btn btn-primary btn-block new testing">Log a test event</button-->\n<nav id="bulk-edit-support-left-nav" class="col-sm-5 col-md-6 col-lg-4">\n\n</nav>\n<section id="bulk-edit-support-disp-area" class="col-sm-7 col-md-6 col-lg-8" style="background-color:white;"></section>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit_support/templates/course_selector.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<input type="search" id="golf_course" value="">\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit_support/templates/left_nav.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<table class="bulk-edit-support"></table>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["bulk_edit_support/templates/person_altered_table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<h1 class="table-title"></h1>\n<table class="person-altered"></table>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["data_import/templates/data_import.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content data-import">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n        <h4 class="modal-title">\n            Customer Import\n        </h4>\n    </div>\n    <div class="modal-body container-fluid" style="min-height: 500px;">\n        <div class="row" style="margin-bottom: 10px">\n            <div class="col-md-offset-2 col-md-2 data-import-step" id="data-import-step-1">\n                <h5 class="data-import-step-number">\n                    <span class="number">1</span>\n                    <span class="fa fa-check" style="display: none"></span>\n                </h5>\n                <p class="data-import-step-desc">Upload data</p>\n            </div>\n            <div class="col-md-2 data-import-step" id="data-import-step-2">\n                <h5 class="data-import-step-number">\n                    <span class="number">2</span>\n                    <span class="fa fa-check" style="display: none"></span>\n                </h5>\n                <p class="data-import-step-desc">Match columns</p>\n            </div>\n            <div class="col-md-2 data-import-step" id="data-import-step-3">\n                <h5 class="data-import-step-number">\n                    <span class="number">3</span>\n                    <span class="fa fa-check" style="display: none"></span>\n                </h5>\n                <p class="data-import-step-desc">Preview</p>\n            </div>\n            <div class="col-md-2 data-import-step" id="data-import-step-4">\n                <h5 class="data-import-step-number">\n                    <span class="number">4</span>\n                    <span class="fa fa-check" style="display: none"></span>\n                </h5>\n                <p class="data-import-step-desc">Import</p>\n            </div>\n        </div>\n        <div id="import-steps" class="row"></div>\n    </div>\n    <div class="modal-footer">\n        <button class="btn btn-default pull-left cancel-import" data-dismiss="modal">Close Window</button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["data_import/templates/data_import_finish.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row">\n    <div class="col-md-12">\n        <h2 style="font-weight: normal; text-align: center; margin-top: 25px">\n            ';
 if(status == 'pending'){ ;
__p += '\n            <strong>' +
__e(accounting.formatNumber(total_records)) +
'</strong> rows ready to import\n            ';
 }else if(status == 'ready' || status == 'in-progress'){ ;
__p += '\n            Importing <strong>' +
__e(accounting.formatNumber(total_records)) +
'</strong> rows\n            ';
 }else if(status == 'cancelled'){ ;
__p += '\n            Import Cancelled\n            ';
 }else{ ;
__p += '\n            Import Complete!\n            ';
 } ;
__p += '\n        </h2>\n        ';
 if(status == 'pending' && estimated_duration > 0){ ;
__p += '\n        <div style="text-align: center;">\n            Your import will take about <em>' +
__e(moment.duration(estimated_duration, 'seconds').humanize()) +
'</em>\n        </div>\n        ';
 } ;
__p += '\n    </div>\n</div>\n';
 if(status == 'pending'){ ;
__p += '\n<div class="row" style="margin-top: 15px;">\n    <div class="col-md-offset-3 col-md-6">\n        <label style="text-align: center">Notify by email when complete</label>\n        <input type="text" class="form-control" placeholder="Enter email address" id="data-import-notify-email">\n    </div>\n</div>\n';
 } ;
__p += '\n<div class="row">\n    <div class="col-sm-offset-4 col-sm-4 col-xs-12">\n        ';
 if(status == 'pending'){ ;
__p += '\n        <button class="btn btn-lg btn-block btn-success start-import" style="margin: 15px auto 0px auto">\n            <span class="fa fa-upload"></span>\n            Start Import\n        </button>\n        ';
 }else{ ;
__p += '\n        <div class="progress" style="margin-top: 15px; margin-bottom: 5px;">\n            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' +
__e(percent_complete) +
'" aria-valuemin="0" aria-valuemax="100" style="width: ' +
__e(percent_complete) +
'%"></div>\n        </div>\n        ';
 if(status != 'cancelled'){ ;
__p += '\n        <small style="display: block; text-align: center">Importing... ' +
__e(percent_complete) +
'% complete</small>\n        ';
 }else{ ;
__p += '\n        <small style="display: block; text-align: center">Cancelled... ' +
__e(percent_complete) +
'% complete</small>\n        ';
 } ;
__p += '\n        ';
 } ;
__p += '\n    </div>\n</div>\n<div class="row">\n    <div class="col-md-12" style="text-align: center; margin-top: 20px;">\n        <small>\n            ';
 if(status == 'pending'){ ;
__p += '\n            Once started, this import will run in the background.<br />\n            Re-open this window to check on progress.\n            ';
 }else if(status == 'in-progress'){ ;
__p += '\n            You may now close this window.<br />\n            Re-open this window to check on progress.\n            ';
 } ;
__p += '\n        </small>\n    </div>\n</div>\n';
 if(status == 'ready'){ ;
__p += '\n<div class="row">\n    <div class="col-md-12">\n        <button class="btn btn-block btn-danger close-job" style="width: 100px; margin: 50px auto 0px auto;">Cancel Import</button>\n    </div>\n</div>\n';
 } ;
__p += '\n';
 if(status == 'in-progress'){ ;
__p += '\n<div class="row">\n    <div class="col-md-12">\n        <button class="btn btn-block btn-danger cancel-in-progress" data-loading-text="Cancelling..." style="width: 100px; margin: 50px auto 0px auto;">Cancel Import</button>\n    </div>\n</div>\n';
 } ;
__p += '\n';
 if(status == 'completed' || status == 'cancelled'){ ;
__p += '\n    <div class="row">\n        <div class="col-md-offset-3 col-md-3">\n            <div class="import-metric bg-primary">\n                <h1>' +
__e(accounting.formatNumber(records_imported)) +
'</h1>\n                <h5>Records Imported</h5>\n            </div>\n        </div>\n        <div class="col-md-3">\n            <div class="import-metric bg-primary" ';
 if(records_failed > 0){ ;
__p += 'style="background-color: #CE6161"';
 } ;
__p += '>\n                <h1>' +
__e(accounting.formatNumber(records_failed)) +
'</h1>\n                <h5>Records Failed</h5>\n            </div>\n        </div>\n    </div>\n\n    ';
 if(response && response.failed_csv_url){ ;
__p += '\n    <div class="row">\n        <div class="col-md-12" style="text-align: center; padding-top: 15px">\n            <a href="' +
__e(response.failed_csv_url) +
'" style="font-size: 18px">Download failed records</a>\n        </div>\n    </div>\n    ';
 } ;
__p += '\n\n    ';
 if(response && response.errors && _.toArray(response.errors).length > 0){ ;
__p += '\n    <div class="row">\n        <div class="col-md-offset-2 col-md-8">\n            <h4 class="text-danger">Errors</h4>\n            <table class="table table-striped" style="width: 100%">\n                <thead>\n                    <tr>\n                        <th>Record #</th>\n                        <th>Error</th>\n                    </tr>\n                </thead>\n                <tbody>\n                ';
 $.each(response.errors, function(x, message){ ;
__p += '\n                <tr>\n                    <td>' +
__e(x) +
'</td>\n                    <td>' +
__e(message) +
'</td>\n                </tr>\n                ';
 }) ;
__p += '\n                </tbody>\n            </table>\n        </div>\n    </div>\n    ';
 } ;
__p += '\n\n    <div class="row">\n        <div class="col-md-12">\n            <button class="btn btn-block btn-default close-job" style="width: 100px; margin: 50px auto 0px auto;">Close</button>\n        </div>\n    </div>\n';
 } ;
__p += '\n';
 if(status == 'pending'){ ;
__p += '\n<div class="row">\n    <div class="col-md-12">\n        <button class="btn btn-block btn-default back" style="width: 100px; margin: 50px auto 0px auto;">\n            <span class="fa fa-arrow-left"></span>\n            Back\n        </button>\n    </div>\n</div>\n';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["data_import/templates/data_import_match.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="data-import-match">\n    <table class="table">\n        <thead>\n            <tr>\n                <th>Source (Your Columns)</th>\n                <th>&nbsp;</th>\n                <th>Destination (foreUP Columns)</th>\n            </tr>\n        </thead>\n        <tbody>\n        ';

        var foreup_fields = fields[type];

        _.each(foreup_fields, function(foreup_col, key){

            var match = null;
            if(typeof(settings.column_map[key]) != 'undefined'){
                match = 'col_' + settings.column_map[key];
            }

            var menu_options = {
                '': '- No Match (Ignore) -'
            };
            _.each(data.headers, function(col_name, index){
                menu_options['col_' + index] = '('+ index +') '+ col_name;
            });
            ;
__p += '\n            <tr>\n                <td>' +
((__t = (_.dropdown(menu_options, {class: 'form-control column-select', 'data-destination-col': key}, match))) == null ? '' : __t) +
'</td>\n                <td style="text-align: center"><span class="fa fa-arrow-right text-muted"></span></td>\n                <td style="text-align: right">';
 if(foreup_col.required){ ;
__p += '<span class="label label-danger">Required</span>';
 } ;
__p += '&nbsp;&nbsp;' +
__e(foreup_col.name) +
'</td>\n            </tr>\n        ';
 }) ;
__p += '\n        </tbody>\n    </table>\n    <div class="row">\n        <div class="col-md-12">\n            <button class="btn btn-lg btn-default back">\n                <span class="fa fa-arrow-left"></span>\n                Back\n            </button>\n            <button class="btn btn-lg btn-primary continue">\n                Next\n                <span class="fa fa-arrow-right"></span>\n            </button>\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["data_import/templates/data_import_preview.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row">\n    <div class="col-md-6" style="padding-top: 5px">\n        Showing <em>' +
__e((offset + 1) ) +
' - ' +
__e(Math.min(offset + limit, total_records)) +
'</em>\n        of <strong>' +
__e(accounting.formatNumber(total_records)) +
' records</strong>\n    </div>\n    <div class="col-md-6" style="padding-bottom: 5px">\n        <button class="btn btn-default btn-sm pull-right next-results">Next <span class="fa fa-arrow-right"></span></button>\n        <button class="btn btn-default btn-sm pull-right prev-results"><span class="fa fa-arrow-left"></span> Prev</button>\n    </div>\n</div>\n<div class="row">\n    <div class="col-md-12">\n        <div class="table-responsive" style="overflow-x: scroll">\n            <table class="table table-striped" style="padding-bottom: 0px">\n                <thead>\n                    <tr>\n                        <th>#</th>\n                        ';
 _.each(settings.column_map, function(col_index, foreup_key){ ;
__p += '\n                        <th>' +
__e(fields[type][foreup_key]['name']) +
'</th>\n                        ';
 }); ;
__p += '\n                    </tr>\n                </thead>\n                <tbody>\n                    ';
 for(var x = offset; x < (offset + limit); x++){
                    if(!data.data[x]){ continue; }
                    var row = data.data[x];
                    ;
__p += '\n                    <tr>\n                        <td>' +
__e(x) +
'</td>\n                        ';
 _.each(settings.column_map, function(col_index, foreup_key){
                        var value = row[col_index];
                        ;
__p += '\n                        <td>' +
__e(value) +
'</td>\n                        ';
 }) ;
__p += '\n                    </tr>\n                    ';
 } ;
__p += '\n                </tbody>\n            </table>\n        </div>\n    </div>\n</div>\n<div class="row">\n    <div class="col-md-12" style="padding-top: 20px;">\n        <button class="btn btn-lg btn-default back">\n            <span class="fa fa-arrow-left"></span>\n            Back\n        </button>\n        <button class="btn btn-lg btn-primary continue">\n            Next\n            <span class="fa fa-arrow-right"></span>\n        </button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["data_import/templates/data_import_upload.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="row">\n    <div class="col-md-12" style="text-align: center;">\n        <a href="https://s3-us-west-2.amazonaws.com/foreup.application.data-import/foreup_customer_import_template.csv">Download customers CSV template</a>\n    </div>\n</div>\n<div class="row">\n    <div class="col-md-12">\n        <div class="data-import-upload">\n            <div class="row">\n                <div class="col-md-12">\n                    <h4 class="text-muted">File Type Accepted</h4>\n                </div>\n            </div>\n            <div class="row">\n                <div class="col-md-offset-5 col-md-2">\n                    <span class="fa fa-file-text-o" style="display: block; font-size: 36px"></span>\n                    <h6>CSV<br />(.csv, .txt)</h6>\n                </div>\n            </div>\n            <div class="data-import-upload-file-area">\n                <h2>Drag File Here</h2>\n                <h6>Or</h6>\n                <button class="btn btn-primary select-file">Browse for File</button>\n            </div>\n        </div>\n    </div>\n</div>\n<div class="row">\n    <div class="col-md-12">\n        <div class="data-import-settings">\n            <h4>Settings</h4>\n            <div class="form-group">\n                <div class="checkbox">\n                    <label>\n                        <input type="checkbox" value="" id="first-row-headers" checked>\n                        First row contains column headers\n                    </label>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<input id="fileupload" class="form-control" type="file" name="file" style="display: none">';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["data_import/templates/data_import_upload_error.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div style="text-align: center">\n    <span class="fa fa-warning text-danger" style="font-size: 72px"></span>\n</div>\n<h2 class="text-danger" style="margin-bottom: 10px">Something went wrong</h2>\n';
 if(error){ ;
__p += '\n<div class="row">\n    <div class="col-md-12">\n        <h4 class="text-danger" style="text-align: center">' +
__e(error) +
'</h4>\n    </div>\n</div>\n';
 } ;
__p += '\n<div class="row">\n    <div class="col-md-offset-4 col-md-4">\n        <button class="btn btn-lg btn-default btn-block back" style="margin-top: 10px;">\n            Try Again\n            <span class="fa fa-refresh"></span>\n        </button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["data_import/templates/data_import_upload_loading.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div style="text-align: center">\n    <span class="fa fa-spinner fa-spin" style="font-size: 72px"></span>\n</div>\n<h4 style="margin-bottom: 20px">Uploading...</h4>\n<div class="row">\n    <div class="col-md-offset-4 col-md-4">\n        <span class="fa fa-file-text-o" style="display: block; font-size: 36px; text-align: center"></span>\n        <h6 style="text-align: center">' +
__e(file.name) +
' (' +
__e(accounting.formatNumber(parseInt(file.size / 1024))) +
'Kb)</h6>\n    </div>\n</div>\n<div class="row">\n    <div class="col-md-offset-4 col-md-4">\n        <div class="progress" style="margin-top: 15px; margin-bottom: 5px;">\n            <div class="progress-bar progress-bar-striped active" role="progressbar" id="upload-progress-bar" style="width: 1%"></div>\n        </div>\n        <small style="display: block; text-align: center"><span class="percentage">1</span>% complete</small>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["data_import/templates/data_import_upload_success.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div style="text-align: center">\n    <span class="fa fa-check text-success" style="font-size: 72px"></span>\n</div>\n<h2 class="text-success" style="margin-bottom: 20px">Everything OK!</h2>\n<div class="row">\n    <div class="col-md-offset-3 col-md-3">\n        <button class="btn btn-lg btn-block btn-default pull-left back">\n            Try Again\n            <span class="fa fa-refresh"></span>\n        </button>\n    </div>\n    <div class="col-md-3">\n        <button class="btn btn-lg btn-block btn-primary pull-right continue">\n            Continue\n            <span class="fa fa-arrow-right"></span>\n        </button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/templates/inventory_audit_data.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<table id="audit-inventory-list" style="width:100%;">\r\n    <thead>\r\n    <tr>\r\n        <th>UPC/ITEM #</th>\r\n        <th>ITEM NAME</th>\r\n        <th>SYSTEM COUNT</th>\r\n        <th>MANUAL COUNT</th>\r\n        <th>DIFFERENCE</th>\r\n        <th>REV. DIFF.</th>\r\n        <th>COST DIFF.</th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    </tbody>\r\n</table>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/templates/inventory_audit_default_screen.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="no-inventory-audit-selected-screen">\r\n    <div id="inventory-tag-image">\r\n\r\n    </div>\r\n    <div id="no-inventory-audit-text">\r\n        <div>No Inventory Audit Selected</div>\r\n        <div>(select new or past audit above)</div>\r\n    </div>\r\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/templates/inventory_audit_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td class="item">\r\n    ' +
__e(item_number) +
'' +
__e(item_id) +
'\r\n</td>\r\n<td class="price">\r\n    ' +
__e(name) +
'\r\n</td>\r\n<td class="audit-item-buttons">\r\n    <input type="hidden" value="' +
__e(item_id) +
'" name="item_id[]">\r\n    <div class="btn-group">\r\n        <button class="btn btn-primary btn-sm subtract-one audit-option" data-value="subtract-one">-</button>\r\n        <input class="quantity" value="0" name="inventory_level[]">\r\n        <button class="btn btn-primary btn-sm add-one audit-option" data-value="add-one">+</button>\r\n    </div>\r\n</td>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/templates/inventory_audit_items.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<table id="inventory-list" style="width:100%">\r\n    <thead>\r\n    <tr>\r\n        <th>UPC/ITEM#</th>\r\n        <th>ITEM NAME</th>\r\n        <th>UPDATE COUNT</th>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    </tbody>\r\n</table>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/templates/inventory_audit_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content item-details">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\r\n        <h4 class="modal-title">Inventory Audit</h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <div class="row">\r\n            <div class="col-md-12 centered">\r\n                <div class="btn-group">\r\n                    <button class="btn btn-primary audit-option" id="new_audit_button">New Audit</button>\r\n                    <button class="btn btn-primary audit-option" id="past_audits_button">Past Audits</button>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-12 centered audit-menu">\r\n            </div>\r\n        </div>\r\n        <form class="form-horizontal tab-content" role="form" style="margin-top: 15px" id="inventory-audit-form">\r\n            <div class="">\r\n\r\n            </div>\r\n            <div id="inventory-list-container" class="inventory-list-container">\r\n\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <div class="modal-footer inventory-audit-buttons">\r\n\r\n    </div>\r\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/templates/receipt_content_edit_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content receipt-content">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n        <h4 class="modal-title">\n            Receipt Agreement\n        </h4>\n    </div>\n    <div class="modal-body container-fluid" style="padding: 20px;">\n        <div class="row">\n            <div class="col-md-12">\n                <label for="receipt-content-name">Name</label>\n                <input type="text" name="name" class="form-control" id="receipt-content-name" value="' +
__e(name) +
'">\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-md-12">\n                <div class="checkbox">\n                    <label>\n                        <input type="checkbox" name="separate_receipt" value="1" data-unchecked-value="0" ';
 if(separate_receipt){ ;
__p += 'checked';
 } ;
__p += '>\n                        Use separate receipt\n                    </label>\n                </div>\n                <div class="checkbox">\n                    <label>\n                        <input type="checkbox" name="signature_line" value="1" data-unchecked-value="0" ';
 if(signature_line){ ;
__p += 'checked';
 } ;
__p += '>\n                        Add signature line\n                    </label>\n                </div>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-md-12">\n                <label>Receipt Content</label>\n                <textarea class="form-control" style="height: 250px" name="content">' +
__e(content) +
'</textarea>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-md-12">\n                <button class="btn btn-danger delete" style="margin-top: 15px">Delete</button>\n            </div>\n        </div>\n    </div>\n    <div class="modal-footer">\n        <button class="btn btn-default pull-left" type="button" data-dismiss="modal">Cancel</button>\n        <button class="btn btn-primary pull-right save" type="button">Save</button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/templates/receipt_content_list.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<thead>\n    <tr>\n        <th style="width: 60%;">Name</th>\n        <th style="width: 15%;">Separate Receipt</th>\n        <th style="width: 15%;">Signature Line</th>\n        <th></th>\n    </tr>\n</thead>\n<tbody></tbody>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/templates/receipt_content_list_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<td>\n    ' +
__e(name) +
'\n</td>\n<td>\n    ';
 if(!separate_receipt){ ;
__p += '\n    <span class="fa fa-circle-o"></span>\n    ';
 }else{ ;
__p += '\n    <span class="fa fa-check-circle"></span>\n    ';
 } ;
__p += '\n</td>\n<td>\n    ';
 if(!signature_line){ ;
__p += '\n    <span class="fa fa-circle-o"></span>\n    ';
 }else{ ;
__p += '\n    <span class="fa fa-check-circle"></span>\n    ';
 } ;
__p += '\n</td>\n<td>\n    <button class="btn btn-default btn-sm edit">Edit</button>\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["items/templates/receipt_content_window.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content receipt-content">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n        <h4 class="modal-title">\n            Manage Receipt Agreements\n        </h4>\n    </div>\n    <div class="modal-body container-fluid" style="min-height: 500px;">\n        <div class="row">\n            <div class="col-md-12">\n                Receipt Agreements\n                <button class="btn btn-primary new settings-corner-button">Add</button>\n            </div>\n        </div>\n        <div class="row" style="margin-top: 15px">\n            <div class="col-md-12" id="receipt-content-list"></div>\n        </div>\n    </div>\n    <div class="modal-footer">\n        <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["marketing_campaigns_support/templates/marketing_campaigns_controls.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<span class="course-filter col-xs-3">Course Filter: <input class="course-filter" type="text" name="course-filter"></span>\n<!--span class="course-filter col-xs-3">Show Deleted: <input class="show-deleted" type="checkbox" value="true" name="show-deleted"><span-->';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["marketing_campaigns_support/templates/marketing_campaigns_support.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<h1 class="table-title">Marketing Campaigns</h1>\n<form id="marketing-campaigns-support-controls"></form>\n<section id="marketing-campaigns-support-disp-area" class="col-xs-12" style="background-color:white;"></section>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["marketing_campaigns_support/templates/marketing_campaigns_table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<table class="marketing-campaigns"></table>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/billing.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="advanced-search-dropdown" style="display: none">\n    <div class="col-md-12">\n        <div class="advanced-search" id="advanced-search"></div>\n        <button id="advanced-search-button" class="pull-right btn btn-default">Search</button>\n    </div>\n</div>\n\n<div class="header clearfix hidden-xs">\n    <div class="col-md-2 hidden-sm hidden-xs">\n        <div class="pull-left"><h2>Billing</h2></div>\n        <div class="clearfix"></div>\n    </div>\n    <div class="col-md-10 ">\n        <div class="pull-right form-inline marg-top-10">\n\n            <div class="btn-group form-group">\n                <button type="button" class="btn btn-default new-template" >New Template<i class="fa fa-user-plus" aria-hidden="true"></i></button>\n                <!-- <button type="button" class="btn btn-default new-minimum" >New Minimum<i class="fa fa-plus" aria-hidden="true"></i></button>\n                <button type="button" class="btn btn-default new-invoice" >New Invoice<i class="fa fa-plus" aria-hidden="true"></i></button> -->\n            </div>\n\n            <div class="form-group">\n                <div id="quick-search"></div>\n            </div>\n\n            <div class="btn-group">\n                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>\n                </button>\n                <ul class="dropdown-menu">\n                    <li id="statement-settings"><a><i class="fa fa-file-text-o" aria-hidden="true"></i>Statement Settings</a></li>\n                    <li id="page-dropdown-advanced-search"><a><i class="fa fa-sliders" aria-hidden="true"></i>Advanced Search</a></li>\n                    <li id="page-dropdown-settings"><a><i class="fa fa-cogs" aria-hidden="true"></i>Page Settings</a></li>\n                </ul>\n            </div>\n        </div>\n    </div>\n</div>\n<div class="header mobile-header clearfix visible-xs-block visible-xs">\n    <div id="mobile-search"></div>\n</div>\n<div id="mobile-list" class="visible-xs-block visible-xs"></div>\n\n<div class="sub-header">\n    <div class="col-md-12 tabs nav-tabs">\n        <ul class="nav nav-tabs">\n            <li class="tab active" style="width: 150px; float: left;">\n                <div class="tab-selector" data-target="#statements-table" data-toggle="tab">\n                    Statements <span id="total-statements"></span>\n                </div>\n            </li>\n            <li class="tab" style="width: 150px; float: left;">\n                <div class="tab-selector" data-target="#recurring-charges-table" data-toggle="tab">\n                    Templates <span id="total-templates"></span>\n                </div>\n            </li>\n        </ul>\n    </div>\n</div>\n<div class="tab-content">\n    <div id="statements-table" class="tab-pane active hidden-xs"></div>\n    <div id="recurring-charges-table" class="tab-pane hidden-xs"></div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/default_recurring_statement_settings.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\n        <h4 class="modal-title">\n            Default Statement Settings\n        </h4>\n    </div>\n    <div class="modal-body container-fluid">\n        <div id="statement-settings" style="margin-top: 15px;"></div>\n    </div>\n    <div class="modal-footer">\n        <button class="btn btn-default pull-left cancel" data-dismiss="modal">Cancel</button>\n        <button id="save-settings" class="btn btn-primary pull-right">Save Settings</button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_account_settings.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row">\n    <div class="col-xs-12">\n        <h3 class="form-title" style="margin-top: 0px">Account Charge Settings</h3>\n        <hr style="border-top-color: #ccc; margin-top: 5px;">\n    </div>\n</div>\n<div class="row">\n    <div class="col-xs-12">\n        <input type="checkbox" name="payMemberBalance" id="statement-pay-member-balance" value="1" data-unchecked-value="0" ';
 if(payMemberBalance){ ;
__p += 'checked';
 } ;
__p += '>\n        <label for="statement-pay-member-balance">Charge <em>' +
__e(App.data.course.get('member_balance_nickname')) +
'</em> balance</label>\n    </div>\n</div>\n<div class="row">\n    <div class="col-xs-12">\n        <input type="checkbox" name="payCustomerBalance" id="statement-pay-customer-balance" value="1" data-unchecked-value="0" ';
 if(payCustomerBalance){ ;
__p += 'checked';
 } ;
__p += '>\n        <label for="statement-pay-customer-balance">Charge <em>' +
__e(App.data.course.get('customer_credit_nickname')) +
'</em> balance</label>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_confirm.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 var days = {
    0: 'Monday',
    1: 'Tuesday',
    2: 'Wednesday',
    3: 'Thursday',
    4: 'Friday',
    5: 'Saturday',
    6: 'Sunday'
}; ;
__p += '\n<div class="row confirm-charges well" style="padding: 10px 0px 0px 0px">\n    <div class="col-md-12">\n        <h4 style="margin-bottom: 15px; color: #888; font-weight: 500; text-transform: uppercase">Summary of Charges</h4>\n        <table class="table table-highlighted" style="border-bottom: 1px solid #E0E0E0; margin-bottom: 0px;">\n            <thead>\n                <tr>\n                    <th class="fee-name">Fee or Item</th>\n                    <th class="frequency">Frequency</th>\n                    <th class="bill-date">Bill Date</th>\n                    <th class="charge">Charge</th>\n                    <th class="tax-rate">Tax Rate</th>\n                    <th class="line-total">Line Total</th>\n                </tr>\n            </thead>\n            <tbody>\n                ';

                var total = 0;
                _.each(items, function(lineItem){ ;
__p += '\n                <tr>\n                    <td class="fee-name">' +
__e(lineItem.item.name) +
'</td>\n                    <td class="frequency">\n                        <span class="fa fa-calendar text-muted" style="margin-right: 5px;"></span>\n                        ' +
__e(_.capitalize(lineItem.repeated.period)) +
'\n                    </td>\n                    <td class="bill-date">\n                        ';

                        if(lineItem.repeated.bymonthday){ ;
__p += '\n\n                            ';
 if(lineItem.repeated.bymonthday <= 28 && lineItem.repeated.bymonthday >= 0){ ;
__p += '\n                                ';
 if(lineItem.repeated.period == 'yearly'){ ;
__p += '\n                                ' +
__e(moment().month(lineItem.repeated.bymonth[0]).date(lineItem.repeated.bymonthday).format('MMM Mo')) +
'\n                                ';
 }else{ ;
__p += '\n                                ' +
__e(moment().date(lineItem.repeated.bymonthday).format('Mo')) +
' of the month\n                                ';
 } ;
__p += '\n                            ';
 }else{ ;
__p += '\n                            Last day of\n                                ';
 if(lineItem.repeated.period == 'yearly'){ ;
__p += '\n                                ' +
__e(moment().month(lineItem.repeated.bymonth[0]).format('MMMM')) +
'\n                                ';
 }else{ ;
__p += '\n                                the month\n                                ';
 } ;
__p += '\n                            ';
 } ;
__p += '\n\n                        ';
 }else if(typeof(lineItem.repeated.bysetpos) != 'undefined' && lineItem.repeated.byweekday){ ;
__p += '\n                            ';
 if(lineItem.repeated.bysetpos === -1){ ;
__p += '\n                            Last\n                            ';
 }else if(lineItem.repeated.bysetpos === 1){ ;
__p += '\n                            First\n                            ';
 } ;
__p += '\n                            ';
 var day = lineItem.repeated.byweekday[0] ;
__p += '\n                            ' +
__e(days[day]) +
' of\n                            ';
 if(lineItem.repeated.period == 'yearly'){ ;
__p += '\n                            ' +
__e(moment().month(lineItem.repeated.bymonth[0]).format('MMMM')) +
'\n                            ';
 }else{ ;
__p += '\n                            the month\n                            ';
 } ;
__p += '\n                        ';
 } ;
__p += '\n                    </td>\n                    <td class="charge">' +
__e(accounting.formatMoney(lineItem.item.subtotal)) +
'</td>\n                    <td class="tax-rate">\n                        ';
 var total_tax_rate = _.reduce(lineItem.item.taxes, function(runningTotal, tax){
                        return runningTotal + parseFloat(tax.percent);
                        }, 0); ;
__p += '\n                        ' +
__e(accounting.formatNumber(total_tax_rate)) +
'%\n                    </td>\n                    <td class="line-total">' +
__e(accounting.formatMoney(lineItem.item.total)) +
'</td>\n                </tr>\n                ';
 }) ;
__p += '\n            </tbody>\n        </table>\n        <div class="row" id="statement-payment-settings">\n            <div class="col-md-9">\n                <div class="row">\n                    <div class="col-md-12" style="margin-top: 10px; margin-bottom: 5px">\n                        <input type="checkbox" name="autopayEnabled" id="statement-enable-autopay" value="1" data-unchecked-value="0" ';
 if(recurringStatement.autopayEnabled){ ;
__p += 'checked';
 } ;
__p += '>\n                        <label for="statement-enable-autopay">Enable Autopay</label>\n                    </div>\n                </div>\n                <div class="row autopay-settings">\n                    <div class="col-md-2 col-sm-12 form-group">\n                        <label>Charge after</label>\n                        <div class="input-group">\n                            <input type="text" name="autopayAfterDays" class="form-control" id="statement-charge-days" value="' +
__e(recurringStatement.autopayAfterDays) +
'">\n                            <span class="input-group-addon">days</span>\n                        </div>\n                    </div>\n                    <div class="col-md-2 col-sm-12 form-group">\n                        <label>Charge Attempts</label>\n                        ' +
((__t = (_.dropdown({1:'1', 2:'2', 3:'3', 4:'4', 5:'5'}, {class:'form-control autopay-settings', name: 'autopayAttempts', id: 'statement-charge-attempts', disabled: 'disabled'}, recurringStatement.autopayAttempts) )) == null ? '' : __t) +
'\n                    </div>\n                </div>\n                <div class="row autopay-settings">\n                    <div class="col-sm-12 form-group">\n                        <input type="checkbox" name="autopayOverdueBalance" id="statement-autopay-overdue" value="1" data-unchecked-value="0" ';
 if(recurringStatement.autopayOverdueBalance){ ;
__p += 'checked';
 } ;
__p += '>\n                        <label for="statement-autopay-overdue">Auto-pay overdue balance</label>\n                    </div>\n                </div>\n            </div>\n            <div class="col-md-3">\n                <table class="table table-highlighted" style="margin-top: -1px">\n                    <tbody>\n                    <tr style="border-top: none; border-right: none">\n                        <td colspan="2" style="padding-top: 0px !important; padding-bottom: 15px !important; border-top: none; border-right: 1px solid #E0E0E0">\n                            <hr style="margin-top: 0px; margin-bottom: 15px; width: 85%">\n                            <input type="checkbox" name="prorate_charges" id="prorate-charges" value="1" data-unchecked-value="0" ';
 if(prorate_charges){ ;
__p += 'checked';
 } ;
__p += '>\n                            <label for="prorate-charges">Prorate charges if added mid-cycle\n                        </td>\n                    </tr>\n                    <tr>\n                        <th style="color: black">Total</th>\n                        <th style="border-right: none; color: black; text-align: right">' +
__e(accounting.formatMoney(totalCharges)) +
'</th>\n                    </tr>\n                    </tbody>\n                </table>\n            </div>\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_edit.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\n        <!-- <i class="fa fa-question pull-right header-icon"></i> -->\n        <h4 class="modal-title">\n            Billing Template\n        </h4>\n    </div>\n    <div class="modal-body container-fluid">\n        <div class="row">\n            <div class="col-md-6">\n                <div class="form-group">\n                    <label>Template Name</label>\n                    <input type="text" value="' +
__e(name) +
'" class="form-control" name="name" id="charge-name" data-bv-notempty data-bv-notempty-message="Name is required">\n                </div>\n            </div>\n            <div class="col-md-6">\n                <div class="well schedule-legend">\n                    <div class="legend-item">\n                        <div class="pull-left"><div class="statement-period"></div></div>\n                        <div class="pull-right">Statement Period</div>\n                    </div>\n                    <div class="legend-item">\n                        <div class="pull-left"><div class="charge"></div></div>\n                        <div class="pull-right">Statement Charge</div>\n                    </div>\n                    <div class="legend-item">\n                        <div class="pull-left"><div class="active-charge"></div></div>\n                        <div class="pull-right">Currently Editing Charge</div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class="row" style="margin-bottom: 25px">\n            <div id="billing-visualization" class="col-md-12"></div>\n        </div>\n        <div class="row">\n            <div class="col-md-12 tabs" style="border-bottom: 2px solid #E0E0E0">\n                <ul class="nav nav-tabs">\n                    <li class="tab active" style="width: 150px; float: left;">\n                        <div class="tab-selector" data-target="#charge-items" data-toggle="tab">\n                            Items\n                        </div>\n                    </li>\n                    <li class="tab" style="width: 175px; float: left;">\n                        <div class="tab-selector" data-target="#charge-statement-settings" data-toggle="tab">\n                            Statement Settings\n                        </div>\n                    </li>\n                </ul>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-md-12">\n                <div class="tab-content" style="padding-top: 25px">\n                    <div role="tabpanel" id="charge-statement-settings" class="tab-pane"></div>\n                    <div role="tabpanel" id="charge-items" class="tab-pane active">\n                        <div class="row" style="margin-top: 10px">\n                            <div class="col-sm-2">\n                                <h5 style="margin: 0px">Schedule Fees and Items</h5>\n                            </div>\n                        </div>\n                        <div class="row line-items" id="line-items"></div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-md-12">\n                <button class="btn btn-default btn-lg pull-right" id="preview-statement">Preview Statement</button>\n            </div>\n        </div>\n    </div>\n    <div class="modal-footer">\n        <button class="btn btn-default pull-left cancel" data-dismiss="modal">Cancel</button>\n        <button id="save-recurring-charge" class="btn btn-primary pull-right">Save Template</button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var override = '';
if(override_price !== null){
    override = accounting.formatMoney(override_price, '');
}
;
__p += '\n<td>\n    ' +
__e(line_number) +
'\n</td>\n<td>\n    ' +
__e(item.name) +
'\n</td>\n<td class="charge-schedule">\n    <a class="set-charge-schedule">No Charge Schedule Set Up</a>\n</td>\n<td style="padding: 5px 10px !important;">\n    <input name="override_price" value="' +
__e(override) +
'" placeholder="' +
__e(item.basePrice) +
'" class="form-control item-price" style="width: 100px">\n</td>\n<td style="padding: 5px 10px !important;">\n    <input name="quantity" value="' +
__e(accounting.formatNumber(quantity)) +
'" class="form-control item-quantity" style="width: 75px">\n</td>\n<td>\n    ';
 var total_tax_rate = _.reduce(item.taxes, function(runningTotal, tax){
    return runningTotal + parseFloat(tax.percent);
    }, 0); ;
__p += '\n    ' +
__e(accounting.formatNumber(total_tax_rate, 2)) +
'%\n</td>\n<td>\n    ' +
__e(accounting.formatMoney(item.total)) +
'\n</td>\n<td>\n    <div class="btn-group pull-right">\n        <button class="btn btn-sm btn-default dropdown-toggle pull-right" style="padding: 0px 8px" data-toggle="dropdown">\n            <span class="fa fa-caret-down"></span>\n        </button>\n        <ul class="dropdown-menu pull-right">\n            <li><a href="#" class="edit"><span class="fa fa-pencil"></span> Edit Item</a></li>\n            <li><a href="#" class="remove"><span class="fa fa-trash"></span> Remove Item</a></li>\n        </ul>\n    </div>\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_item_list.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<table class="table table-highlighted">\n    <thead>\n    <tr>\n        <th style="width: 50px">&nbsp;</th>\n        <th>Fee/Item Name</th>\n        <th>Schedule of Charges</th>\n        <th style="width: 110px">Price</th>\n        <th style="width: 85px">Qty</th>\n        <th style="width: 90px">Tax Rate</th>\n        <th style="width: 110px">Total</th>\n        <th style="width: 50px; text-align: center">&nbsp;</th>\n    </tr>\n    </thead>\n    <tbody id="line-items"></tbody>\n    <tfoot>\n        <tr>\n            <td colspan="8" style="background-color: #F8F8F8">\n                <div class="search-field">\n                    <span class="fa fa-search input-icon" style="left: 32px;"></span>\n                    <input type="text" class="form-control item-search has-input-icon" placeholder="Search and add items...">\n                </div>\n            </td>\n        </tr>\n    </tfoot>\n</table>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_list.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<thead>\n<tr>\n    <th>Membership Name</th>\n    <th>Total Charges</th>\n    <th></th>\n</tr>\n</thead>\n<tbody>\n\n</tbody>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_list_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\n    ' +
__e(name) +
'\n</td>\n<td>\n    ' +
__e(accounting.formatMoney(totalCharges)) +
'\n</td>\n<td>\n    <div class="btn-group pull-right">\n        <button class="btn btn-muted btn-sm dropdown-toggle pull-right" data-toggle="dropdown">\n            <span class="fa fa-caret-down"></span>\n        </button>\n        <ul class="dropdown-menu pull-right">\n            <li><a href="#" class="edit"><span class="fa fa-pencil"></span> Edit</a></li>\n            <li><a href="#" class="delete"><span class="fa fa-trash"></span> Delete</a></li>\n        </ul>\n    </div>\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_row_menu.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="btn-group">\n    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n        <span class="caret"></span>\n    </button>\n    <ul class="dropdown-menu">\n        <li><a href="#" class="view"><i class="fa fa-file-text-o" aria-hidden="true"></i>Edit</a></li>\n        <li><a href="#" class="delete"><i class="fa fa-trash" aria-hidden="true"></i>Delete</a></li>\n    </ul>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_schedule.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="row recurring-schedule-container">\n    <div class="col-md-4">\n        <div class="recurring-schedule-tabs">\n\n        </div>\n        <div class="row">\n            <div class="col-md-12 account-settings-tab">\n                <span class="status-icon pull-left"></span>\n                Account Charges\n            </div>\n        </div>\n    </div>\n    <div class="col-md-8 recurring-schedule-details"></div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_schedule_item_details.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

console.debug('Item '+item.name+' repeatable data: ', repeated);
__p += '\n<div class="row">\n    <div class="col-md-12">\n        <label>Visual Diagram of Billing Schedule</label>\n        <div id="repeatable-visualization"></div>\n    </div>\n</div>\n<div class="row schedule-step" data-step="1">\n    <div class="col-xs-12">\n        <h3 class="form-title">\n            <span class="status-icon default">1</span>\n            <em>' +
__e(item.name) +
'</em> will be charged...\n        </h3>\n        <fieldset id="repeatable-period-selector"></fieldset>\n    </div>\n</div>\n<div class="row schedule-step" data-step="2">\n    <div class="col-md-12">\n        <hr class="separator">\n        <h3 class="form-title">\n            <span class="status-icon default">2</span>\n            <em>' +
__e(item.name) +
'</em> will be charged every...\n        </h3>\n        <fieldset id="repeatable-month-selector"></fieldset>\n        <hr class="separator">\n    </div>\n</div>\n<div class="row schedule-step" data-step="3">\n    <div class="col-md-12">\n        <hr class="separator">\n        <h3 class="form-title">\n            <span class="status-icon default">3</span>\n            <em>' +
__e(item.name) +
'</em> will be charged on...\n        </h3>\n        <fieldset id="repeatable-day-selector"></fieldset>\n    </div>\n</div>\n<div class="row schedule-step" data-step="4">\n    <div class="col-md-6 col-md-offset-3">\n        <em>' +
__e(item.name) +
'</em> will be charged at...\n        <div class="btn-group btn-group-justified" style="margin-top: 20px">\n            <div class="btn-group">\n                <button class="btn btn-default repeat-time-select ';
 if(typeof(repeated.byhour) != 'undefined' && repeated.byhour == 0){ ;
__p += 'active';
 } ;
__p += '" data-value="start">Beginning of Day</button>\n            </div>\n            <div class="btn-group">\n                <button class="btn btn-default repeat-time-select ';
 if(typeof(repeated.byhour) != 'undefined' && repeated.byhour == 23){ ;
__p += 'active';
 } ;
__p += '" data-value="end">End of Day</button>\n            </div>\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_schedule_list_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-md-12 recurring-schedule-tab">\n    ';
 if(_schedule_completion == 'not_started'){ ;
__p += '\n    <span class="status-icon pull-left"></span>\n    ';
 }else if(_schedule_completion == 'in_progress'){ ;
__p += '\n    <span class="status-icon warning pull-left">\n        <span class="fa fa-exclamation"></span>\n    </span>\n    ';
 }else if(_schedule_completion == 'complete'){ ;
__p += '\n    <span class="status-icon success pull-left">\n        <span class="fa fa-check"></span>\n    </span>\n    ';
 } ;
__p += '\n    ' +
__e(item.name) +
'\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charge_statement.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row">\n    <div class="col-md-4">\n        <p>\n        ';
 if(!isDefault){ ;
__p += '\n        These statements settings will only apply to <em>this</em> billing template\n        ';
 }else{ ;
__p += '\n        Any new billing templates created in the future will default to these settings. Existing billing statements will <em>not</em> be affected by changing these settings.\n        ';
 } ;
__p += '\n        </p>\n        <div id="statement-schedule"></div>\n    </div>\n    <div class="col-md-4">\n        <div class="row">\n            <div class="col-md-8 col-lg-6 form-group">\n                <label for="statement-due-days">Statement Due After</label>\n                <div class="input-group">\n                    <input type="text" name="dueAfterDays" class="form-control" id="statement-due-days" value="' +
__e(dueAfterDays) +
'">\n                    <span class="input-group-addon">days</span>\n                </div>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-md-12 form-group">\n                <input type="checkbox" name="financeChargeEnabled" class="checkbox" id="statement-finance-charge-enabled" data-unchecked-value="0" value="1" ';
 if(financeChargeEnabled){ ;
__p += 'checked';
 } ;
__p += '>\n                <label for="statement-finance-charge-enabled">Enable Finance Charge</label>\n            </div>\n        </div>\n        <div class="row finance-charge-settings">\n            <div class="col-md-12">\n                <label for="statement-finance-charge-amount">Finance Charge Amount</label>\n                <div class="form-group">\n                    <input type="text" name="financeChargeAmount" class="form-control" id="statement-finance-charge-amount" value="' +
__e(financeChargeAmount) +
'" placeholder="0.00" style="width: 75px; display: inline-block;">\n                    ' +
((__t = (_.dropdown({'percent': '(%) Percent of Total Due', 'fixed': '($) Fixed Amount'},{
                        name: 'financeChargeType',
                        id: 'statement-finance-charge-type',
                        class: 'form-control',
                        style: 'display: inline-block; width: 200px; padding-left: 5px;'
                    }, financeChargeType))) == null ? '' : __t) +
'\n                </div>\n            </div>\n        </div>\n        <div class="row finance-charge-settings">\n            <div class="col-md-10 col-lg-7 form-group">\n                <label for="statement-finance-charge-after-days">Apply Finance Charge</label>\n                <div class="input-group">\n                    <input type="text" name="financeChargeAfterDays" class="form-control" id="statement-finance-charge-after-days" value="' +
__e(financeChargeAfterDays) +
'">\n                    <span class="input-group-addon">days after due date</span>\n                </div>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-md-12" style="margin-top: 20px; margin-bottom: 5px">\n                <input type="checkbox" class="checkbox" name="autopayEnabled" id="statement-enable-autopay" value="1" data-unchecked-value="0" ';
 if(autopayEnabled){ ;
__p += 'checked';
 } ;
__p += '>\n                <label for="statement-enable-autopay">Enable Autopay</label>\n            </div>\n        </div>\n        <div class="row autopay-settings">\n            <div class="col-md-6 form-group">\n                <label>Autopay After</label>\n                <div class="input-group">\n                    <input type="text" name="autopayAfterDays" class="form-control" id="statement-charge-days" value="' +
__e(autopayAfterDays) +
'">\n                    <span class="input-group-addon">days</span>\n                </div>\n            </div>\n            <div class="col-md-6 form-group">\n                <label>Autopay Attempts</label>\n                ' +
((__t = (_.dropdown({1:'1', 2:'2', 3:'3', 4:'4', 5:'5'}, {class:'form-control autopay-settings', name: 'autopayAttempts', id: 'statement-charge-attempts', disabled: 'disabled'}, autopayAttempts) )) == null ? '' : __t) +
'\n            </div>\n        </div>\n        <div class="row autopay-settings">\n            <div class="col-sm-12 form-group">\n                <input type="checkbox" class="checkbox" name="autopayOverdueBalance" id="statement-autopay-overdue" value="1" data-unchecked-value="0" ';
 if(autopayOverdueBalance){ ;
__p += 'checked';
 } ;
__p += '>\n                <label for="statement-autopay-overdue">Autopay Past Due Amount</label>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-xs-12">\n                <input type="checkbox" class="checkbox" name="includePastDue" id="statement-include-past-due" value="1" data-unchecked-value="0" ';
 if(includePastDue){ ;
__p += 'checked';
 } ;
__p += '>\n                <label for="statement-include-past-due">Include Past Due Amount</label>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-xs-12">\n                <input type="checkbox" class="checkbox" name="includeMemberTransactions" id="statement-include-member-transactions" value="1" data-unchecked-value="0" ';
 if(includeMemberTransactions){ ;
__p += 'checked';
 } ;
__p += '>\n                <label for="statement-include-member-transactions">Include ' +
__e(App.data.course.get('member_balance_nickname')) +
' transactions</label>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-xs-12" style="padding-left: 45px">\n                <input type="checkbox" class="checkbox" name="payMemberBalance" id="statement-pay-member-balance" value="1" data-unchecked-value="0" ';
 if(payMemberBalance){ ;
__p += 'checked';
 } ;
__p += '>\n                <label for="statement-pay-member-balance">Pay off ' +
__e(App.data.course.get('member_balance_nickname')) +
'</label>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-xs-12">\n                <input type="checkbox" class="checkbox" name="includeCustomerTransactions" id="statement-include-customer-transactions" value="1" data-unchecked-value="0" ';
 if(includeCustomerTransactions){ ;
__p += 'checked';
 } ;
__p += '>\n                <label for="statement-include-customer-transactions">Include ' +
__e(App.data.course.get('customer_credit_nickname')) +
' transactions</label>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-xs-12" style="padding-left: 45px">\n                <input type="checkbox" class="checkbox" name="payCustomerBalance" id="statement-pay-customer-balance" value="1" data-unchecked-value="0" ';
 if(payCustomerBalance){ ;
__p += 'checked';
 } ;
__p += '>\n                <label for="statement-pay-customer-balance">Pay off ' +
__e(App.data.course.get('customer_credit_nickname')) +
'</label>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-xs-12">\n                <input type="checkbox" class="checkbox" name="sendEmail" id="statement-send-email" value="1" data-unchecked-value="0" ';
 if(sendEmail){ ;
__p += 'checked';
 } ;
__p += '>\n                <label for="statement-send-email">Email statement to customer</label>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-xs-12 form-group">\n                <input type="checkbox" class="checkbox" name="sendZeroChargeStatement" id="statement-send-zero-total" value="1" data-unchecked-value="0" ';
 if(sendZeroChargeStatement){ ;
__p += 'checked';
 } ;
__p += '>\n                <label for="statement-send-zero-total">Send Zero Charge Statements</label>\n            </div>\n        </div>\n    </div>\n    <div class="col-md-4">\n        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n             width="325px" height="240px" viewBox="0 0 325 240" enable-background="new 0 0 325 240" xml:space="preserve" style="margin-bottom: 20px;">\n            <g>\n                <path fill="#FFFFFF" d="M321,90c0,1.657-1.343,3-3,3H184c-1.657,0-3-1.343-3-3V73c0-1.657,1.343-3,3-3h134c1.657,0,3,1.343,3,3V90z"/>\n                <path fill="none" stroke="#000000" stroke-miterlimit="10" d="M321.5,89.5c0,1.65-1.35,3-3,3h-134c-1.65,0-3-1.35-3-3v-17c0-1.65,1.35-3,3-3h134c1.65,0,3,1.35,3,3V89.5z"/>\n            </g>\n            <path fill="#FFFFFF" stroke="#666666" stroke-width="2" stroke-miterlimit="10" d="M169,222.493c0,7.46-4.373,14.507-11,14.507H17c-6.627,0-13-7.047-13-14.507V16.507C4,9.047,10.373,4,17,4h141c6.627,0,11,5.047,11,12.507V222.493z"/>\n            <rect x="20" y="19" fill="#FFFFFF" stroke="#E6E6E6" stroke-width="2" stroke-miterlimit="10" width="21" height="21"/>\n            <rect x="20" y="101" fill="#E6E6E6" width="133" height="13"/>\n            <rect x="20" y="118" fill="#E6E6E6" width="133" height="13"/>\n            <rect x="20" y="135" fill="#E6E6E6" width="133" height="13"/>\n            <rect x="118" y="153" fill="#E6E6E6" width="35" height="13"/>\n            <line fill="none" stroke="#E6E6E6" stroke-width="2" stroke-miterlimit="10" x1="20" y1="53" x2="60" y2="53"/>\n            <line fill="none" stroke="#E6E6E6" stroke-width="2" stroke-miterlimit="10" x1="20" y1="59" x2="60" y2="59"/>\n            <line fill="none" stroke="#E6E6E6" stroke-width="2" stroke-miterlimit="10" x1="21" y1="66" x2="40" y2="66"/>\n            <line fill="none" stroke="#E6E6E6" stroke-width="2" stroke-miterlimit="10" x1="113" y1="19" x2="153" y2="19"/>\n            <line fill="none" stroke="#E6E6E6" stroke-width="2" stroke-miterlimit="10" x1="113" y1="25" x2="153" y2="25"/>\n            <line fill="none" stroke="#E6E6E6" stroke-width="2" stroke-miterlimit="10" x1="133" y1="31" x2="153" y2="31"/>\n            <rect x="87" y="49" fill="#E6E6E6" width="66" height="19"/>\n            <rect x="192" y="76" fill="none" width="177" height="25"/>\n            <text transform="matrix(1 0 0 1 192 84.5918)" font-family="Arial" font-size="12">Terms &amp; Conditions</text>\n            <rect x="87" y="72" fill="#FFFFFF" stroke="#C1272D" stroke-width="2" stroke-miterlimit="10" width="65" height="16"/>\n            <rect x="21" y="156" fill="#FFFFFF" stroke="#C1272D" stroke-width="2" stroke-miterlimit="10" width="84" height="27"/>\n            <rect x="30" y="214" fill="#FFFFFF" stroke="#C1272D" stroke-width="2" stroke-miterlimit="10" width="113" height="14"/>\n            <g>\n                <path fill="#FFFFFF" d="M320,177c0,1.657-1.343,3-3,3H183c-1.657,0-3-1.343-3-3v-17c0-1.657,1.343-3,3-3h134c1.657,0,3,1.343,3,3V177z"/>\n                <path fill="none" stroke="#000000" stroke-miterlimit="10" d="M320.5,176.5c0,1.65-1.35,3-3,3h-134c-1.65,0-3-1.35-3-3v-17c0-1.65,1.35-3,3-3h134c1.65,0,3,1.35,3,3V176.5z"/>\n            </g>\n            <rect x="191" y="163" fill="none" width="177" height="25"/>\n            <text transform="matrix(1 0 0 1 191 171.5918)" font-family="Arial" font-size="12">Message</text>\n            <g>\n                <path fill="#FFFFFF" d="M320,229c0,1.657-1.343,3-3,3H183c-1.657,0-3-1.343-3-3v-17c0-1.657,1.343-3,3-3h134c1.657,0,3,1.343,3,3V229z"/>\n                <path fill="none" stroke="#000000" stroke-miterlimit="10" d="M320.5,228.5c0,1.65-1.35,3-3,3h-134c-1.65,0-3-1.35-3-3v-17c0-1.65,1.35-3,3-3h134c1.65,0,3,1.35,3,3V228.5z"/>\n            </g>\n            <rect x="191" y="215" fill="none" width="177" height="25"/>\n            <text transform="matrix(1 0 0 1 191 223.5918)" font-family="Arial" font-size="12">Footer</text>\n            <line fill="none" stroke="#666666" stroke-miterlimit="10" x1="152" y1="80.5" x2="180" y2="80.5"/>\n            <line fill="none" stroke="#666666" stroke-miterlimit="10" x1="105" y1="167.5" x2="180" y2="167.5"/>\n            <line fill="none" stroke="#666666" stroke-miterlimit="10" x1="142" y1="219.5" x2="180" y2="219.5"/>\n        </svg>\n        <div class="row">\n            <div class="col-xs-12">\n                <label>Statement Terms and Conditions</label>\n                <div id="statement-terms-and-conditions" class="text-editor"></div>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-xs-12">\n                <label>Statement Message</label>\n                <div id="statement-message" class="text-editor"></div>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-xs-12">\n                <label>Statement Footer</label>\n                <div id="statement-footer" class="text-editor"></div>\n            </div>\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charges.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\n        <h4 class="modal-title">\n            Billing Memberships\n        </h4>\n    </div>\n    <div class="modal-body container-fluid">\n        <div class="modal-well">\n            <div class="row">\n                <div class="col-md-12">\n                    <div class="form-group">\n                        <label for="recurring-charge-name">Membership Name</label>\n                        <input type="text" name="name" class="form-control" id="recurring-charge-name">\n                    </div>\n                </div>\n                <div class="col-md-12">\n                    <button class="btn btn-primary pull-right" id="new-membership">New Membership ></button>\n                </div>\n            </div>\n        </div>\n        <div class="row recurring-charges"></div>\n    </div>\n    <div class="modal-footer">\n        <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/recurring_charges_table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="nd-table-container layout-box" id="grid">\n    <table class="table items"></table>\n</div>\n\n<div class="nd-footer clearfix">\n    <div class="row">\n        <div id="paginator" class="col-md-4 col-sm-6"></div>\n        <div id="state-display" class="col-md-3 hidden-sm hidden-xs"></div>\n        <div class="col-md-5 col-sm-6">\n            <div class="selectionButtons btn-group pull-right">\n                <button type="button" class="btn btn-primary btn-danger controls" action="bulkDelete">\n                    <i class="fa fa-trash-o" aria-hidden="true"></i><span class="hidden-sm hidden-xs">Delete</span>\n                </button>\n            </div>\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable_day_selector.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-md-5 col-sm-12 repeat-dynamic-day">\n    <label>On the</label>\n    <div class="row">\n        <div class="col-xs-5" style="padding-right: 0px;">\n            <select id="repeat-charge-set-position" class="form-control">\n                <option value="1" ';
 if(typeof(bysetpos) != 'undefined' && bysetpos != null && bysetpos === 1){ ;
__p += 'selected';
 } ;
__p += '>First</option>\n                <option value="-1" ';
 if(typeof(bysetpos) != 'undefined' && bysetpos != null && bysetpos === -1){ ;
__p += 'selected';
 } ;
__p += '>Last</option>\n            </select>\n        </div>\n        <div class="col-xs-7">\n            <select id="repeat-charge-weekday" class="form-control">\n                ';

                var weekdays = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
                for(var x = 0; x <= 6; x++){ ;
__p += '\n                <option value="' +
__e(x) +
'" ';
 if(typeof(byweekday) != 'undefined' && byweekday != null &&  byweekday.indexOf(x) !== -1){;
__p += 'selected';
};
__p += '>' +
__e(weekdays[x]) +
'</option>\n                ';
 } ;
__p += '\n            </select>\n        </div>\n    </div>\n</div>\n<div class="col-md-2 or-separator">\n    - or -\n</div>\n<div class="col-md-5 col-sm-12">\n    <div class="repeat-day-selector">\n        ';
 for(i = 1; i <= 28; i++){
        var checked = (typeof(bymonthday) != 'undefined' && bymonthday != null && _.indexOf(bymonthday, i) > -1);
        ;
__p += '\n        <button class="btn btn-default repeat-day-select ';
 if(checked){ print('active') } ;
__p += '" data-value="' +
__e(i) +
'">\n            ' +
__e(i) +
'\n        </button>\n        ';
 } ;
__p += '\n        <button class="btn btn-default repeat-day-select last ';
 if(typeof(bymonthday) != 'undefined' && bymonthday != null && _.indexOf(bymonthday, -1) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="-1">\n            Last day of month\n        </button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable_month_selector.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-md-5">\n    ';
 if(typeof(period) != 'undefined' && period && period != 'quarterly'){ ;
__p += '\n    <div class="row repeat-interval-container">\n        <div class="form-group col-sm-10 col-md-8 col-lg-6">\n            <label>Repeat every</label>\n            <div class="input-group">\n                <input type="text" class="form-control" id="repeat-interval" value="';
 if(typeof(interval) != 'undefined'){ ;
__p +=
__e(interval);
 } ;
__p += '">\n                <span class="input-group-addon">\n                    ';
 var label = 'months';
                    if(period == 'yearly'){ label = 'years'; }
                    print(label); ;
__p += '\n                </span>\n            </div>\n        </div>\n        <div class="form-group col-sm-10 col-md-8 col-lg-6">\n            <label for="repeat-start">Starting on</label>\n            <input type="text" class="form-control start-date date-picker" style="text-align: left" id="repeat-start" placeholder="Today"\n                   value="';
 if(typeof(dtstart) != 'undefined' && dtstart && dtstart.length != 0){ ;
__p +=
__e(moment(dtstart).format('MM/DD/YYYY'));
 } ;
__p += '">\n        </div>\n    </div>\n    ';
 } ;
__p += '\n</div>\n<div class="col-md-2 or-separator">\n    ';
 if(typeof(period) != 'undefined' && period != 'quarterly'){ ;
__p += '\n    - or -\n    ';
 } ;
__p += '\n</div>\n<div class="col-md-5 col-sm-6">\n    <div class="repeat-month-selector">\n        ';
 for(var x = 1; x <= 12; x++){
        var isActive = (typeof(bymonth) != 'undefined' && bymonth != null && bymonth.indexOf(x) !== -1);
        ;
__p += '\n        <button class="btn btn-default repeat-month ';
 if(isActive){ ;
__p += 'active';
 } ;
__p += '" data-value="' +
__e(x) +
'">' +
__e(moment().month(x - 1).format('MMM')) +
'</button>\n        ';
 } ;
__p += '\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable_period_selector.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-md-6 co-lg-4 col-xs-12">\n    <div class="btn-group btn-group-justified" role="group">\n        <div class="btn-group"><button type="button" class="btn btn-default period ';
 if(typeof(period) != 'undefined' && period == 'monthly'){ ;
__p += 'active';
 } ;
__p += '" data-value="monthly">Monthly</button></div>\n        <div class="btn-group"><button type="button" class="btn btn-default period ';
 if(typeof(period) != 'undefined' && period == 'quarterly'){ ;
__p += 'active';
 } ;
__p += '" data-value="quarterly">Quarterly</button></div>\n        <div class="btn-group"><button type="button" class="btn btn-default period ';
 if(typeof(period) != 'undefined' && period == 'yearly'){ ;
__p += 'active';
 } ;
__p += '" data-value="yearly">Yearly</button></div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/statement.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


function getPaymentType(payment){

    var value = '';

    if(payment.type == 'pos'){
        value += 'POS #' + payment.saleId;
    }else if(payment.type == 'ach'){
        value += 'ACH';
    }else if(payment.subType == 'visa'){
        value  +=  'VISA';
    }else if(payment.subType == 'mastercard'){
        value += 'MasterCard';
    }else if(payment.subType == 'discover'){
        value += 'Discover';
    }else if(payment.subType == 'jcb'){
        value = 'JCB';
    }else if(payment.subType == 'diners'){
        value = 'Diners';
    }else if(payment.subType == 'amex'){
        value = 'American Express';
    }

    value += ' ' + payment.accountNumber;
    return value;
}

var memberBalanceLabel = 'Member Account';
var customerBalanceLabel = 'Pro Shop Account';

if(App.data.course.get('member_balance_nickname')){
    memberBalanceLabel = App.data.course.get('member_balance_nickname');
}

if(App.data.course.get('customer_credit_nickname')){
    customerBalanceLabel = App.data.course.get('customer_credit_nickname');
}

;
__p += '\n<div class="modal-content">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n        <h4 class="modal-title">\n            Statement Preview\n        </h4>\n    </div>\n    <div class="modal-body">\n        <div class="statement-container">\n            <div class="row statement-header">\n                <div class="col-md-7">\n                    <div class="statement-course-info">\n                        <img src="http://development.foreupsoftware.com/app_files/view/2" class="statement-course-logo">\n                        <h3 class="statement-course-name">' +
__e(App.data.course.get('name')) +
'</h3>\n                        <address class="statement-course-address">\n                            ' +
__e(App.data.course.get('address')) +
'<br>\n                            ' +
__e(App.data.course.get('city')) +
' ' +
__e(App.data.course.get('state')) +
', ' +
__e(App.data.course.get('zip')) +
'\n                        </address>\n                        <div class="statement-course-contact">\n                            ' +
__e(App.data.course.get('website')) +
'<br>\n                            ' +
__e(App.data.course.get('email')) +
'<br>\n                            ' +
__e(App.data.course.get('phone')) +
'\n                        </div>\n                    </div>\n                    <div class="statement-bill-to">\n                        <table>\n                            <thead>\n                            <tr>\n                                <th>Bill To</th>\n                            </tr>\n                            </thead>\n                            <tbody>\n                            <tr>\n                                <td>\n                                    <h2 class="customer-name">' +
__e(customer.contact_info.first_name) +
' ' +
__e(customer.contact_info.last_name) +
'</h2>\n                                    <div class="account-number">\n                                        <strong>Acct: </strong>\n                                        ';
 if(!customer.account_number){ ;
__p += '\n                                        <span class="text-muted">N/A</span>\n                                        ';
 }else{ ;
__p += '\n                                        ' +
__e(customer.account_number) +
'\n                                        ';
 } ;
__p += '\n                                    </div>\n                                    <address class="customer-address">\n                                        ' +
__e(customer.contact_info.address_1) +
'<br>\n                                        ' +
__e(customer.contact_info.city) +
' ' +
__e(customer.contact_info.state) +
', ' +
__e(customer.contact_info.zip) +
'\n                                    </address>\n                                </td>\n                            </tr>\n                            </tbody>\n                        </table>\n                    </div>\n                </div>\n\n                <div class="col-md-5">\n                    <div>\n                        <h1 class="statement-label">STATEMENT</h1>\n                        <img class="barcode"/>\n                    </div>\n                    <table cellspacing="0" class="statement-summary">\n                        <tbody>\n                        <tr>\n                            <th>Bill Date</th>\n                            <td style="width: 125px">' +
__e(moment(dateCreated).format('M/D/YYYY')) +
'</td>\n                        </tr>\n                        <tr>\n                            <th>Statement #</th>\n                            <td>' +
__e(number) +
'</td>\n                        </tr>\n                        <tr>\n                            <td>Prev. Balance</td>\n                            <td>' +
__e(accounting.formatMoney(customer.invoice_balance)) +
'</td>\n                        </tr>\n                        <tr>\n                            <td>Payments</td>\n                            <td>' +
__e(accounting.formatMoney(totalPaid)) +
'</td>\n                        </tr>\n                        <tr>\n                            <td>New Charges</td>\n                            <td>' +
__e(accounting.formatMoney(total)) +
'</td>\n                        </tr>\n                        <tr>\n                            <th style="border-top: none;">\n                                <h2 class="statement-total">Total Due</h2>\n                            </th>\n                            <td style="border-top: none;">\n                                <h2 class="statement-total" style="float: right">' +
__e(accounting.formatMoney(totalOpen)) +
'</h2>\n                            </td>\n                        </tr>\n                        <tr>\n                            <th><em>Due On</em></th>\n                            <td>' +
__e(moment(dueDate).format('M/D/YYYY')) +
'</td>\n                        </tr>\n                        </tbody>\n                    </table>\n                    <div class="statement-terms">\n                        ';
 if(termsAndConditionsText){ ;
__p += '\n                        <div class="statement-message">\n                            <strong>Terms & Conditions</strong><br>\n                            ' +
((__t = (termsAndConditionsText)) == null ? '' : __t) +
'\n                        </div>\n                        ';
 } ;
__p += '\n                    </div>\n                </div>\n            </div>\n\n            <div class="row statement-items">\n                <div class="col-md-12">\n                    <h3 class="section-title">\n                        New Charges (' +
__e(moment(startDate).format('M/D/YYYY')) +
' to\n                        ' +
__e(moment(endDate).format('M/D/YYYY')) +
')\n                    </h3>\n                    <table class="items" cellspacing="0">\n                        <thead>\n                            <tr>\n                                <th style="width: 25px;">&nbsp;</th>\n                                <th class="date">Date</th>\n                                <th>Item</th>\n                                <th class="quantity">Qty</th>\n                                <th class="amount">Price</th>\n                                <th class="tax-rate">Tax Rate</th>\n                                <th class="amount">Total</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            ';
 if(charges && charges.length > 0){ ;
__p += '\n                            ';
 _.each(charges, function(line, index){ ;
__p += '\n                            <tr>\n                                <td>' +
__e(line.line) +
'</td>\n                                <td class="date">' +
__e(moment(line.dateCharged).format('M/D/YYYY')) +
'</td>\n                                <td>' +
__e(line.name) +
'</td>\n                                <td class="quantity">' +
__e(accounting.formatNumber(line.qty)) +
'</td>\n                                <td class="amount">' +
__e(accounting.formatMoney(line.unitPrice)) +
'</td>\n                                <td class="tax-rate">' +
__e(accounting.formatNumber(line.taxPercentage)) +
'%</td>\n                                <td class="amount">' +
__e(accounting.formatMoney(line.subtotal)) +
'</td>\n                            </tr>\n                            ';
 }) }else{ ;
__p += '\n                            <tr>\n                                <td colspan="7" class="empty">No New Charges</td>\n                            </tr>\n                            ';
 } ;
__p += '\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n\n            ';
 if(payments && payments.length > 0){ ;
__p += '\n            <div class="row statement-items">\n                <div class="col-md-12">\n                    <h3 class="section-title">\n                        Payments\n                    </h3>\n                    <table class="items" cellspacing="0">\n                        <thead>\n                            <tr>\n                                <th style="width: 25px;">&nbsp;</th>\n                                <th class="date">Date</th>\n                                <th>Payment Method</th>\n                                <th class="amount">Amount</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            ';
 _.each(payments, function(payment, index){ ;
__p += '\n                            <tr>\n                                <td>' +
__e((index + 1)) +
'</td>\n                                <td>' +
__e(moment(payment.dateCreated).format('M/D/YYYY')) +
'</td>\n                                <td>' +
__e(getPaymentType(payment)) +
'</td>\n                                <td class="amount">' +
__e(accounting.formatMoney(payment.amount)) +
'</td>\n                            </tr>\n                            ';
 }) ;
__p += '\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n            ';
 } ;
__p += '\n\n            <div class="row statement-items">\n                <div class="col-md-8">\n                    ';
 if(messageText){ ;
__p += '\n                    <div class="statement-message">\n                        ' +
((__t = (messageText)) == null ? '' : __t) +
'\n                    </div>\n                    ';
 } ;
__p += '\n                </div>\n                <div class="col-md-4">\n                    <table cellspacing="0" class="statement-totals">\n                        <tr>\n                            <td>Subtotal</td>\n                            <td style="width: 125px">' +
__e(accounting.formatMoney(subtotal)) +
'</td>\n                        </tr>\n                        <tr>\n                            <td>Taxes</td>\n                            <td>' +
__e(accounting.formatMoney(totalTax)) +
'</td>\n                        </tr>\n                        <tr>\n                            <td>Payments</td>\n                            <td>' +
__e(accounting.formatMoney(totalPaid)) +
'</td>\n                        </tr>\n                        <tr>\n                            <th><strong>Total</strong></th>\n                            <td><strong>' +
__e(accounting.formatMoney(totalOpen)) +
'</strong></td>\n                        </tr>\n                        <tr>\n                            <td><em>Due On</em></td>\n                            <td>' +
__e(moment(dueDate).format('M/D/YYYY')) +
'</td>\n                        </tr>\n                    </table>\n                </div>\n            </div>\n\n            ';
 if(includeMemberTransactions){
            var lastRunningBalance = memberBalanceForward;
            ;
__p += '\n            <div class="row statement-items">\n                <div class="col-md-12">\n                    <h3 class="section-title">\n                        ' +
__e(memberBalanceLabel) +
' (' +
__e(moment(startDate).format('M/D/YYYY')) +
' to\n                        ' +
__e(moment(endDate).format('M/D/YYYY')) +
')\n                    </h3>\n                    <table class="items">\n                        <thead>\n                            <tr>\n                                <th class="date">Date</th>\n                                <th>Transaction</th>\n                                <th>Details</th>\n                                <th class="amount">Debit</th>\n                                <th class="amount">Credit</th>\n                                <th class="amount">Balance</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                        ';
 if(memberTransactions && memberTransactions.length > 0){ ;
__p += '\n                        <tr>\n                            <td colspan="5" style="text-align: center"><em>Balance Forward</em></td>\n                            <td class="amount">' +
__e(accounting.formatMoney(memberBalanceForward)) +
'</td>\n                        </tr>\n                        ';
 _.each(memberTransactions, function(transaction){
                        lastRunningBalance = parseFloat(transaction.runningBalance);
                        ;
__p += '\n                        <tr>\n                            <td class="date">' +
__e(moment(transaction.transDate).format('M/D/YYYY')) +
'</td>\n                            <td>' +
__e(transaction.transComment) +
'</td>\n                            <td>\n                            ';
 if(transaction.sale && transaction.sale.items && transaction.sale.items.length > 0){ ;
__p += '\n                                ';
 _.each(transaction.sale.items, function(item){ ;
__p += '\n                                (' +
__e(accounting.formatNumber(item.quantity)) +
') ' +
__e(item.name) +
': ' +
__e(accounting.formatMoney(item.total)) +
'<br>\n                                ';
 }) ;
__p += '\n                            ';
 }else{ ;
__p += '\n                                ' +
((__t = (transaction.transDescription)) == null ? '' : __t) +
'\n                            ';
 } ;
__p += '\n                            </td>\n                            <td class="amount">';
 if(transaction.transAmount <= 0){ ;
__p +=
__e(accounting.formatMoney(Math.abs(transaction.transAmount)));
 } ;
__p += '</td>\n                            <td class="amount">';
 if(transaction.transAmount > 0){ ;
__p +=
__e(accounting.formatMoney(Math.abs(transaction.transAmount)));
 } ;
__p += '</td>\n                            <td class="amount">' +
__e(accounting.formatMoney(transaction.runningBalance)) +
'</td>\n                        </tr>\n                        ';
 }) ;
__p += '\n                        ';
 }else{ ;
__p += '\n                        <tr>\n                            <td colspan="6" class="empty">No transactions</td>\n                        </tr>\n                        ';
 } ;
__p += '\n                        <tr>\n                            <th colspan="5" style="text-align: right; font-weight: normal;"><em>Balance as of ' +
__e(moment(endDate).format('M/D/YYYY')) +
'</th>\n                            <th class="amount"><strong>' +
__e(accounting.formatMoney(lastRunningBalance)) +
'</strong></th>\n                        </tr>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n            ';
 } ;
__p += '\n\n            ';
 if(includeCustomerTransactions){
                var lastRunningBalance = customerBalanceForward;
            ;
__p += '\n            <div class="row statement-items">\n                <div class="col-md-12">\n                    <h3 class="section-title">\n                        ' +
__e(memberBalanceLabel) +
' (' +
__e(moment(startDate).format('M/D/YYYY')) +
' to\n                        ' +
__e(moment(endDate).format('M/D/YYYY')) +
')\n                    </h3>\n                    <table class="items">\n                        <thead>\n                        <tr>\n                            <th class="date">Date</th>\n                            <th>Transaction</th>\n                            <th>Details</th>\n                            <th class="amount">Debit</th>\n                            <th class="amount">Credit</th>\n                            <th class="amount">Balance</th>\n                        </tr>\n                        </thead>\n                        <tbody>\n                        ';
 if(customerTransactions && customerTransactions.length > 0){ ;
__p += '\n                        <tr>\n                            <td colspan="5" style="text-align: center"><em>Balance Forward</em></td>\n                            <td class="amount">' +
__e(accounting.formatMoney(customerBalanceForward)) +
'</td>\n                        </tr>\n                        ';
 _.each(customerTransactions, function(transaction){
                        lastRunningBalance = parseFloat(transaction.runningBalance);
                        ;
__p += '\n                        <tr>\n                            <td class="date">' +
__e(moment(transaction.transDate).format('M/D/YYYY')) +
'</td>\n                            <td>' +
__e(transaction.transComment) +
'</td>\n                            <td>\n                                ';
 if(transaction.sale && transaction.sale.items && transaction.sale.items.length > 0){ ;
__p += '\n                                ';
 _.each(transaction.sale.items, function(item){ ;
__p += '\n                                (' +
__e(accounting.formatNumber(item.quantity)) +
') ' +
__e(item.name) +
': ' +
__e(accounting.formatMoney(item.total)) +
'<br>\n                                ';
 }) ;
__p += '\n                                ';
 }else{ ;
__p += '\n                                ' +
((__t = (transaction.transDescription)) == null ? '' : __t) +
'\n                                ';
 } ;
__p += '\n                            </td>\n                            <td class="amount">';
 if(transaction.transAmount <= 0){ ;
__p +=
__e(accounting.formatMoney(Math.abs(transaction.transAmount)));
 } ;
__p += '</td>\n                            <td class="amount">';
 if(transaction.transAmount > 0){ ;
__p +=
__e(accounting.formatMoney(Math.abs(transaction.transAmount)));
 } ;
__p += '</td>\n                            <td class="amount">' +
__e(accounting.formatMoney(transaction.runningBalance)) +
'</td>\n                        </tr>\n                        ';
 }) ;
__p += '\n                        ';
 }else{ ;
__p += '\n                        <tr>\n                            <td colspan="6" class="empty">No transactions</td>\n                        </tr>\n                        ';
 } ;
__p += '\n                        <tr>\n                            <th colspan="5" style="text-align: right; font-weight: normal;"><em>Balance as of ' +
__e(moment(endDate).format('M/D/YYYY')) +
'</th>\n                            <th class="amount"><strong>' +
__e(accounting.formatMoney(lastRunningBalance)) +
'</strong></th>\n                        </tr>\n                        </tbody>\n                    </table>\n                </div>\n            </div>\n            ';
 } ;
__p += '\n            ';
 if(footerText){ ;
__p += '\n            <div class="row statement-footer">\n                <div class="col-md-12">\n                    <hr>\n                    ' +
((__t = (footerText)) == null ? '' : __t) +
'\n                </div>\n            </div>\n            ';
 } ;
__p += '\n        </div>\n    </div>\n    <div class="modal-footer">\n        <button class="btn btn-default pull-right" type="button" data-dismiss="modal">Close</button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/statement_row_menu.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="btn-group">\n    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n        <span class="caret"></span>\n    </button>\n    <ul class="dropdown-menu">\n        <li><a href="#" class="view"><i class="fa fa-file-text-o" aria-hidden="true"></i>View</a></li>\n        <li><a href="#" class="print"><i class="fa fa-print" aria-hidden="true"></i>Print</a></li>\n        <li><a href="#" class="email"><i class="fa fa-paper-plane" aria-hidden="true"></i>Email</a></li>\n    </ul>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/statements_table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="nd-table-container layout-box" id="grid">\n    <table class="table items"></table>\n</div>\n\n<div class="nd-footer clearfix">\n    <div class="row">\n        <div id="paginator" class="col-md-4 col-sm-6"></div>\n        <div id="state-display" class="col-md-3 hidden-sm hidden-xs"></div>\n        <div class="col-md-5 col-sm-6">\n            <div class="selectionButtons btn-group pull-right">\n                <button type="button" class="btn btn-primary btn-danger controls" action="bulkDelete">\n                    <i class="fa fa-trash-o" aria-hidden="true"></i><span class="hidden-sm hidden-xs">Delete</span>\n                </button>\n            </div>\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/settings.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="header clearfix hidden-xs">\n    <div class="header-contents">\n        <div class="col-md-2 hidden-sm hidden-xs">\n            <div class="pull-left"><h2>Settings</h2></div>\n            <div class="clearfix"></div>\n        </div>\n        <div class="col-md-10 ">\n            <div class="pull-right form-inline marg-top-10">\n                <div class="btn-group form-group">\n                    <button type="button" class="btn save-settings btn-primary new"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Save Settings</button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class="header mobile-header clearfix visible-xs-block visible-xs">\n	<div id="mobile-search"></div>\n	<button type="button" class="btn new_cust btn- new"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Customer</button>\n</div>\n<div id="mobile-list" class="visible-xs-block visible-xs"></div>\n<div id="main-container" class="hidden-xs">\n    <div id="sections-container" class="col-md-2 col-lg-2">\n        <ul class="nav nav-pills nav-stacked">\n\n        </ul>\n    </div>\n    <div id="sub-sections-container" class="col-md-2 col-lg-2">\n        <ul class="nav nav-pills nav-stacked">\n\n        </ul>\n    </div>\n    <div id="settings-container" class="col-md-8 col-lg-8">\n\n    </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["starter_teesheet/behaviors/substate_container.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="modalbox" class="modal right fade" role="dialog" >\n    <div class="modal-dialog ' +
((__t = ( size )) == null ? '' : __t) +
'">\n        <div class=\'modal-content edit-customer-window\'>\n            <div class=\'modal-body container-fluid\' >\n            </div>\n        </div>\n    </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["starter_teesheet/templates/people_message.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<span class="btn btn-primary btn-sm"><i class="fa fa-mobile" aria-hidden="true"></i></span>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["starter_teesheet/templates/starter_booking.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '\n<div class="booking ';
 if(teedOffTime){ ;
__p += 'teedoff';
 } ;
__p += '" >\n    <div class="row booking-item">\n        ';
 if(type != "teetime"){ ;
__p += '\n            ' +
__e(type.charAt(0).toUpperCase() + type.slice(1) ) +
'\n        ';
 } ;
__p += '\n        <div class="col-xs-12" id="players-region">\n\n        </div>\n        <div class="select-booking col-xs-4">\n            ';
 if(carts > 0){ ;
__p += '\n            ';

            if(cart1){ print (cart1); }
            if(cart1 && cart2){print(",")}
            if(cart2){ print(cart2);} ;
__p += '\n                <img src="/images/Cart_icon.png"  />\n            ';
 } ;
__p += '\n        </div>\n    </div>\n    <div class="row">\n\n    </div>\n    <div id="teetime-region">\n\n    </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["starter_teesheet/templates/starter_booking_edit.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row booking-header">\n    <div class="col-xs-3 back-navigation close-modal"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</div>\n    <div class="col-xs-8">\n        <h1>' +
__e(moment(start).format("h:mm a") ) +
'</h1>\n        <p>(Booked: ' +
__e(moment(dateBooked).format("MMM, D h:mm a") ) +
' ' +
__e(bookingSource ) +
')</p>\n    </div>\n</div>\n<h2>DETAILS</h2>\n<p>\n    ' +
__e(holes ) +
' holes /\n    ' +
__e(playerCount ) +
' players /\n    ' +
__e(carts ) +
' Cart Rentals /\n\n</p>\n<h2>PLAYERS (' +
__e(paidPlayerCount ) +
' Paid)</h2>\n<div id="edit-players-region" class="col-xs-12"></div>\n<div class="clearfix"></div>\n<h2>ASSIGNED CARTS</h2>\n<p>\n    ';
 if(carts == 0){ print("None") } else {
        if(cart1) print(cart1);
        if(cart1 && cart2) print("<br />");
        if(cart2) print(cart2)
    ;
__p += '\n</p>\n\n';
 } ;
__p += '\n\n<h2>NOTES</h2>\n<p>' +
__e(details ) +
'</p>\n';
 if(!teedOffTime || teedOffTime == "" ){ ;
__p += '\n\n    <h2>\n        <button id="teedoff">Mark Players Teed Off</button>\n    </h2>\n';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["starter_teesheet/templates/starter_booking_edit_player.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '\n<div class="row player">\n    <div class="text-center col-xs-2">\n        <div class="teed-off ';
 if(paid){ ;
__p += ' teed-off-check ';
 } ;
__p += '">\n            <i class="fa fa-check" aria-hidden="true"></i>\n        </div>\n    </div>\n    <div class="col-xs-6">\n        <h2>' +
__e(name );
 if(name==""){ ;
__p += 'No Name';
 } ;
__p += '</h2>\n        <p>' +
__e(priceClass ) +
'</p>\n    </div>\n    <div class="col-xs-4 text-right">\n        <div class="row player-paid-status">\n            <div class="';
 if(paid){ ;
__p += 'player-paid';
 } ;
__p += ' player-dot" ></div>\n\n            <div class="text-left" style="padding: 0px">\n                ';
 if(paid){ ;
__p += '\n                Paid\n                ';
 } else { ;
__p += '\n                Unpaid\n                ';
 } ;
__p += '\n            </div>\n        </div>\n    </div>\n</div>\n<div class="player-border"></div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["starter_teesheet/templates/starter_booking_layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="starter-booking">\n    <div class="booking-date row-xs-12">\n        <i class="fa fa-angle-left" id="prevDay" aria-hidden="true"></i>\n        <span>' +
__e(moment(date).format("dddd MMM. D") ) +
'</span>\n        <i class="fa fa-angle-right" id="nextDay" aria-hidden="true"></i>\n    </div>\n    <div class="row-xs-12 booking-front-back">\n        <div class="input-group">\n              <span class="input-group-btn">\n                <button class="teesheet-side btn btn-secondary ';
 if(side == 'front'){ ;
__p += 'active';
 } ;
__p += '" type="button" value="front">Front 9</button>\n                <button class="teesheet-side btn btn-secondary ';
 if(side == 'back'){ ;
__p += 'active';
 } ;
__p += '" type="button" value="back">Back 9</button>\n              </span>\n        </div>\n    </div>\n\n    <div id="booking-region" class="bookings-collection">\n\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["starter_teesheet/templates/starter_booking_player.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="col-xs-12">\n    <span id="message"></span>\n    <div class="';
 if(paid){ ;
__p += 'player-paid';
 } ;
__p += ' player-dot">\n\n    </div>\n    <p>\n        ' +
((__t = (name)) == null ? '' : __t) +
'\n    </p>\n    <span id="message"></span>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["starter_teesheet/templates/starter_timeslot.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="row">\n    <div class="col-xs-12 booking-time">\n        ' +
__e(moment(id).format("h:mm a") ) +
'\n    </div>\n</div>\n<div class="row" id="timeslot_container">\n\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/loadingview.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="loading" >\n    <img src="\\images\\loading.gif" />\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/navigation.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="page-navigation-container">\n    <div class="page-navigation col-xs-12">\n        <div class="row page-navigation-header" style="height: 50px">\n            <div class="col-md-offset-3 col-md-9">\n                <div id="r-table-search" class="pull-left input-container">\n                    <span class="fa fa-search input-icon"></span>\n                    <input class="search form-control" name="search" value="" id="report_search" placeholder="Search...">\n                </div>\n            </div>\n        </div>\n        <div class="row">\n            <ul class="col-md-3 nav nav-tabs-vertical">\n                ';
 _.each(tags,function(tag,key){ ;
__p += '\n                <li class="nav-tab-vertical" id="report-' +
__e(tag) +
'">\n                    <a href="#report-' +
__e(key) +
'" class="col-xs-12" data-toggle="tab" role="tab">\n                        <h4 class="tag">' +
__e(tag) +
'</h4>\n                        <span class="badge search-number"></span>\n                    </a>\n                </li>\n                ';
 }) ;
__p += '\n            </ul>\n            <div class="col-md-9">\n                <div class="row tab-content category-reports">    \n                    ';
 _.each(tags, function(tag, key){ ;
__p += '\n                    <div class="col-md-12 tab-pane" id="report-' +
__e(key) +
'">\n                        <div class="row">\n                            ';
 _.each(items, function(item){ ;
__p += '\n                            ';
 if(item.tags.indexOf(tag) > -1){ ;
__p += '\n                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 report-template" data-value="' +
((__t = ( item.id )) == null ? '' : __t) +
'" title="' +
__e(item.description) +
'">\n                                <div class="report-template-name">\n                                    <h5>\n                                        ';
 if(!item.title){ ;
__p += '<span class="text-muted">No Name</span>';
 }else{ ;
__p +=
__e(item.title );
 } ;
__p += '\n                                    </h5>\n                                </div>\n                                <div class="report-template-description">\n                                    <p>\n                                        ';
if(!item.description){;
__p += '<em>No description</em>';
 }else{ ;
__p +=
__e(item.description);
 } ;
__p += '\n                                    </p>\n                                </div>\n                             </div>\n                            ';
 } ;
__p += '\n                            ';
 }); ;
__p += '\n                        </div>\n                    </div>\n                    ';
 }) ;
__p += '\n                </div>\n            </div>\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/reports.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="r-tabs"></div>\n<div id="r-single-report" class="row background-color-gradient"></div>\n<div class="col-md-2" id="r-navigation"></div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/reports_edit.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="row">\n    <div id="loading" style="display:none">\n        <img src="\\images\\loading.gif" />\n    </div>\n    <div id="content">\n        <div id="r-reports"></div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/tab.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '\n<div class="nr-tabs ';
 if(active)print('isActive') ;
__p += ' " id="tab" >\n    ' +
((__t = ( title )) == null ? '' : __t) +
'\n    <button class="nr-tabs-close" id="closeTab">\n        <i class="hidden-menu fa fa-times" ></i>\n    </button>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/tabs.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="nr-tabs-container">\n\n    <div id="tabs" style="float: left;" class="nav nav-tabs"></div>\n\n    <button class="nr-tab-float" id="addtab">+</button>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["support_tools/templates/disp_area.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div>\n    Displaying some text\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["support_tools/templates/left_nav.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<table class="support-tools"></table>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["support_tools/templates/support_tools.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<nav id="support-tools-left-nav" class="col-sm-5 col-md-4 col-lg-3">\n\n</nav>\n<section id="support-tools-disp-area" class="col-sm-7 col-md-8 col-lg-9" style="background-color:white;">\n\n</section>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/groups/group.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="group-list-row row">\r\n    <div class="col-md-5">\r\n        <div class="form-group">\r\n            <label for="group-label" class="col-md-12 control-label">Group Name</label>\r\n            <div class="col-md-12">\r\n                <input class="form-control group-label" type="text" value="' +
__e(label) +
'" name="group-label" />\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="col-md-5">\r\n        <div class="form-group">\r\n            <label for="group-item-discount" class="col-md-12 control-label">Item Discount</label>\r\n            <div class="col-md-12">\r\n                <div class="input-group">\r\n                    <input class="form-control item-discount" type="text" value="' +
__e(itemDiscount ? itemDiscount : '0.00') +
'" name="item-discount" />\r\n                    <div class="input-group-addon">%</div>\r\n                </div>\r\n              </div>\r\n          </div>\r\n      </div>\r\n\r\n      <div class="col-md-1 pull-right">\r\n          <div class="btn btn-destroy pull-right marg-top-25">\r\n            <i class="fa fa-trash-o fa-1x group-delete"></i>\r\n          </div>\r\n      </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/groups/manage.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\r\n        <i class="fa fa-question-o pull-right header-icon"></i>\r\n        <h4 class="modal-title">\r\n            Groups\r\n        </h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <div class="modal-well">\r\n            <div class="col-md-5">\r\n                <div class="form-group">\r\n                    <label for="new-group-label" class="col-md-12 control-label">Group Name</label>\r\n                    <div class="col-md-12">\r\n                        <input class="form-control" type="text" value="" name="new-group-label" id="new-group-label" />\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-5">\r\n              <div class="form-group">\r\n                <label for="new-item-discount" class="col-md-12 control-label">Item Discount</label>\r\n                <div class="col-md-12">\r\n                  <div class="input-group">\r\n                    <input class="form-control" type="text" value="" name="new-item-discount" id="new-item-discount" />\r\n                    <div class="input-group-addon">%</div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class="col-md-2">\r\n              <button class="btn btn-primary pull-right marg-top-25" id="add-group">Add Group</button>\r\n            </div>\r\n          </div>\r\n          <div class="col-md-12 group-list">\r\n          </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class="modal-footer">\r\n    <button class="btn btn-secondary pull-left" data-dismiss="modal">Close</button>\r\n    <button class="btn btn-primary pull-right" id="save-changes">Save Changes</button>\r\n</div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/mobile/basic_search.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '\n    <input type="text" class="form-control typeahead-customer" placeholder="Search Customers" />\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/mobile/customer_list_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<img src="' +
__e(photo ) +
'" />\n<div class="name_group">\n  <h3>' +
__e(first_name ) +
'&nbsp;' +
__e(last_name ) +
'</h3>\n  <span class="group-list">\n      ';
 print(_.pluck(customerGroups,"label").join(",")); ;
__p += '\n  </span>\n</div>\n\n\n\n<i class="fa fa-angle-right" aria-hidden="true"></i>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/messages/message_history.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p +=
__e(message ) +
'\n<span class="timestamp">' +
__e(moment.utc(timestamp).fromNow() ) +
' - ' +
__e(employee_first_name ) +
' ' +
__e(employee_last_name ) +
'</span>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/messages/messages.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="customer_message_list">\n\n</div>\n';
 if(!App.data.user.has_module_permission("marketing_campaigns")){ ;
__p += '\nGive us a call to set up text messaging today.   (800)675-0422\n';
 } else { ;
__p += '\n<input id="customer_new_message" /><a id="customer_new_message_btn" class="btn btn-default">Send</a>\n';
 } ;
__p += '\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["customers/templates/page_settings/customer_settings_required_fields.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += 'Required Fields';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/advanced_search/advanced_search.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="row">\n  <div class="col-md-12" style="border-bottom: 1px solid #cccccc;margin-bottom: 10px; padding: 5px">\n    <span class="advanced-title" style="text-transform: uppercase; font-size: 14px;letter-spacing: 1px;">Advanced Search</span> <a class="pull-right" id="clear-filters" style="font-size: 11px; font-style:italic; text-decoration:underline; cursor: pointer;">Clear Filters</a>\n  </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/advanced_search/advanced_search_empty.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<form class="row" style="padding-top:5px;padding-bottom: 5px; background: #f5f5f5;" >\n  <div class="col-md-6">\n    <span style="font-size: 16px; font-weight: 200;"> &mdash; Filters Cleared &mdash;</span>\n  </div>\n  <div class="col-md-1 pull-right">\n    <button id="add" class="btn-search pull-right"><i class="fa fa-plus" aria-hidden="true"></i></button>\n  </div>\n</form>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/advanced_search/advanced_search_filter.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<form class="row" style="padding-top:5px;padding-bottom: 5px; border-bottom: 1px solid #cccccc;" >\n  <div class="col-md-2">\n    <select id="field" class="form-control">\n        ';
 _.forEach(allowed_fields,function(allowed_field){ ;
__p += '\n            <option\n                    value="' +
__e(allowed_field.name) +
'"\n                    ';
 if(allowed_field.name == field ){ print("selected") } ;
__p += '\n                >\n                ' +
__e(allowed_field.label ) +
'\n            </option>\n        ';
 }); ;
__p += '\n    </select>\n  </div>\n  <div class="col-md-2">\n    <select id="comp" class="form-control variable">\n        ';
 if(type == "text"){ ;
__p += '\n            <option value="eq" ';
 if(comparison=="eq") print("selected") ;
__p += '>Equals</option>\n            <option value="neq" ';
 if(comparison=="neq") print("selected") ;
__p += '>Not Equal</option>\n            <option value="like" ';
 if(comparison=="like") print("selected") ;
__p += '>Contains</option>\n            <option value="starts" ';
 if(comparison=="starts") print("selected") ;
__p += '>Starts With</option>\n            <option value="ends" ';
 if(comparison=="ends") print("selected") ;
__p += '>Ends With</option>\n        ';
 } else if(type == "number"){ ;
__p += '\n            <option value="eq" ';
 if(comparison=="eq") print("selected") ;
__p += '>Equals</option>\n            <option value="neq" ';
 if(comparison=="neq") print("selected") ;
__p += '>Not Equal</option>\n            <option value="lt" ';
 if(comparison=="lt") print("selected") ;
__p += '>Less Than</option>\n            <option value="lte" ';
 if(comparison=="lte") print("selected") ;
__p += '>Less Than or Equal</option>\n            <option value="gt" ';
 if(comparison=="gt") print("selected") ;
__p += '>Greater Than</option>\n            <option value="gte" ';
 if(comparison=="gte") print("selected") ;
__p += '>Greater Than Or Equal</option>\n        ';
 } else if(type == "date"){ ;
__p += '\n            <option value="eq" ';
 if(comparison=="eq") print("selected") ;
__p += '>Equal</option>\n            <option value="neq" ';
 if(comparison=="neq") print("selected") ;
__p += '>Not Equal</option>\n            <option value="lt" ';
 if(comparison=="lt") print("selected") ;
__p += '>Before</option>\n            <option value="lte" ';
 if(comparison=="lte") print("selected") ;
__p += '>On or Before</option>\n            <option value="gt" ';
 if(comparison=="gt") print("selected") ;
__p += '>After</option>\n            <option value="gte" ';
 if(comparison=="gte") print("selected") ;
__p += '>On or After</option>\n        ';
 } else if(type == "bool"){ ;
__p += '\n            <option value="eq" selected>Equals</option>\n        ';
 } else if(type == "dropdown"){ ;
__p += '\n            <option value="eq" selected>Equals</option>\n        ';
 } ;
__p += '\n\n    </select>\n  </div>\n  <div class="col-md-2">\n    ';
 if(type == "text"){ ;
__p += '\n        <input type="input" id="value" class="form-control" value="' +
__e(value ) +
'"/>\n    ';
 } else if(type == "number"){ ;
__p += '\n        <input type="number" id="value" class="form-control" value="' +
__e(value ) +
'"/>\n    ';
 } else if(type == "date"){ ;
__p += '\n        <input type="datetime" id="value" class="form-control" value="' +
__e(value ) +
'"/>\n    ';
 } else if(type == "bool"){ ;
__p += '\n        <select id="value" class="form-control">\n            <option value="1">Yes</option>\n            <option value="0">No</option>\n        </select>\n    ';
 } else if(type == "dropdown"){ ;
__p += '\n        <select id="value" class="form-control">\n            <option value=""></option> \n            ';
 _.forEach(possibleValues,function(possibleValue){ ;
__p += '\n            <option\n                    value="' +
__e(possibleValue) +
'">\n            ';
 if(value == possibleValue ){ print("selected") } ;
__p += '\n            ' +
__e(possibleValue) +
'\n            </option>\n            ';
 }); ;
__p += '\n        </select>\n    ';
 } ;
__p += '\n  </div>\n  <div class="col-md-2 pull-right">\n    <button id="delete" class="btn-search pull-right"><i class="fa fa-minus" aria-hidden="true"></i></button>\n    <button id="add" class="btn-search pull-right"><i class="fa fa-plus" aria-hidden="true"></i></button>\n  </div>\n</form>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/advanced_search/basic_search.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<input type="text" class="form-control search" placeholder="' +
__e(placeholder) +
'" />';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/confirm/confirm.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-header">\n    <a href="#" class="close">×</a>\n    <h3>Delete ?</h3>\n</div>\n<div class="modal-body">\n    <p>You are about to delete something..</p>\n    <p>Do you want to proceed?</p>\n</div>\n<div class="modal-footer">\n    <a class="btn yes danger">Yes</a>\n    <a class="btn no secondary">No</a>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/messages/emptyMessageThread.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<h4>No messages</h4>\n';
 if(!App.data.user.has_module_permission("marketing_campaigns")){ ;
__p += '\nGive us a call to set up text messaging today.   (800)675-0422\n';
 } else { ;
__p += '\n<a href="#new_customers">Start</a> a conversation with one of your customers today. Get notifications here as they respond.\n';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/messages/messageThread.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="message ';
 if(read == 1){print('read');} ;
__p += '">\n  <p>' +
__e(message ) +
'<br/>\n    <span class="name"> - ' +
__e(first_name ) +
' ' +
__e(last_name ) +
'</span>\n    <span class="time">' +
__e(timestamp ) +
'</span>\n  </p>\n  <a class="btn btn-primary btn-sm" id="respond"><i class="fa fa-reply" aria-hidden="true"></i></a>\n  <a id="ignore" style="position: absolute;bottom: 12px;right: 10px;cursor: pointer;">Ignore</a>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/messages/messageThreadLayout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="unread_msgs dropdown ';
 if(displayDropdown){ ;
__p += ' open ';
} ;
__p += '">\n  <div id="message_dropdown" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n    <i class="fa fa-mobile" aria-hidden="true"></i><span id="unread_count"></span>\n  </div>\n  <div class="dropdown-menu unread_msgs_thread-cont ">\n    <div class="message-options">\n      <a class="btn" id="notification-sound">\n        ';
 if(mute){ ;
__p += '\n          <i class="fa fa-volume-off" aria-hidden="true"></i>\n        ';
 } else { ;
__p += '\n          <i class="fa fa-volume-up" aria-hidden="true"></i>\n        ';
 } ;
__p += '\n      </a>\n    </div>\n    <div id="message_threads" ></div>\n  </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/wizard/wizard_footer.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if(currentStep > 1){ ;
__p += '\n<button class="btn btn-default pull-left previous">\n    <span class="fa fa-chevron-left align-left"></span> Back\n</button>\n';
 } ;
__p += '\n\n';
 if(steps[currentStep + 1]){ ;
__p += '\n<button class="btn btn-primary pull-right wide next-step" style="margin-left: 15px">\n    ' +
__e(steps[currentStep + 1]) +
' <span class="fa fa-chevron-right align-right"></span>\n</button>\n';
 } ;
__p += '\n\n';
 if(steps[currentStep + 1]){ ;
__p += '\n<button class="btn btn-muted pull-right save">Save For Later</button>\n';
 }else{ ;
__p += '\n<button class="btn btn-primary pull-right save">Save</button>\n';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/wizard/wizard_steps.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

_.each(steps, function(step, index){ ;
__p += '\n<div class="wizard-step" data-step="' +
__e(index) +
'">\n    <div class="wizard-step-circle ';
 if(currentStep == index){ ;
__p += 'active';
 }else if(currentStep > index){ ;
__p += 'complete';
 } ;
__p += '">\n        ' +
__e((index)) +
'\n    </div>\n    <div class="wizard-step-name">\n        ' +
__e(step) +
'\n    </div>\n</div>\n';
 }) ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/page_settings/column_settings.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="column-drag-handle handle"><i class="fa fa-bars" aria-hidden="true"></i></div>\n' +
__e(label ) +
'\n<div class="column-displayed">\n      <input id="col-toggle-' +
__e(name) +
'" type="checkbox" class="toggle-switch display-column-toggle" ';
 if(displayed) print("checked");;
__p += ' />\n      <label for="col-toggle-' +
__e(name) +
'"></label>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/page_settings/layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\n        <i class="fa fa-question-circle-o pull-right header-icon"></i>\n        <h4 class="modal-title">\n            Page Settings\n        </h4>\n    </div>\n    <div class="modal-body container-fluid">\n        <div class="col-md-6" style="padding: 15px;">\n            <div class="row">\n                <div class="col-md-12" id="page-size"></div>\n            </div>\n            <div class="row">\n                <div class="col-md-12" id="general-settings"></div>\n            </div>\n        </div>\n        <div class="col-md-6" style="border-left: 1px solid #cccccc">\n            <p class="table-set-help">\n                <small>\n                    Use the toggles below to customize the displayable data in your <em>' +
__e(module) +
'</em> portal.\n                    We recommend only displaying data you would want to see "at a glance", we recommend 10 or fewer.\n                </small>\n            </p>\n            <div id="columns" class="table-set"></div>\n        </div>\n    </div>\n</div>\n<div class="modal-footer">\n    <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\n    <button class="btn btn-primary pull-right" id="save-changes">Save Changes</button>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["_common/templates/page_settings/page_size.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="form-group">\n    <label class="control-label">Table Items to show</label>\n    <div class="btn-group page_size_holder">\n        <button data-value="25" type="button" class="page_size btn btn-default model-controls ';
 if(page_size == 25) print('active');
__p += '" >25</button>\n        <button data-value="50" type="button" class="page_size btn btn-default model-controls ';
 if(page_size == 50) print('active');
__p += '" >50</button>\n        <button data-value="100" type="button" class="page_size btn btn-default model-controls ';
 if(page_size == 100) print('active');
__p += '" >100</button>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/business_details/business_details_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="settings-header row">\r\n          <div class="col-md-12">\r\n            <div class="settings-title">Business Details</div>\r\n            <div class="settings-subtitle">These details are used to help us better serve you in generating invoices, receipts, and business communications.</div>\r\n          </div>\r\n        </div>\r\n    </div>\r\n    <div class="row">\r\n        <div class="col-md-4 col-lg-4 left-column">\r\n            <div class="form-group">\r\n                <label for="name" class="col-md-12 control-label">Business Name</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(name) +
'" name="name" id="name" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="address" class="col-md-12 control-label">Business Address</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(address) +
'" name="address" id="address" />\r\n                </div>\r\n            </div>\r\n            <!--div class="form-group">\r\n                <label for="business-address-2" class="col-md-12 control-label">Address Line 2</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="" name="address_2" id="business-address-2" />\r\n                </div>\r\n            </div-->\r\n            <div class="form-group">\r\n                <label for="country" class="col-md-12 control-label">Country</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.countries, {"name":"country", "id":"country", "class":"form-control"}, country))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="city" class="col-md-12 control-label">City</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(city) +
'" name="city" id="city" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="state" class="col-md-6 control-label">State</label>\r\n                <label for="postal" class="col-md-6 control-label">Postal/Zip Code</label>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="col-md-6">\r\n                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.states[country], {"name":"state", "id":"state", "class":"form-control"}, state))) == null ? '' : __t) +
'\r\n                </div>\r\n                <div class="col-md-6">\r\n                    <input class="form-control" type="text" value="' +
__e(postal) +
'" name="postal" id="postal" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="timezone" class="col-md-12 control-label">Timezone</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.timezones, {"name":"timezone", "id":"timezone", "class":"form-control"}, timezone))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="timeFormat" class="col-md-12 control-label">Time Format</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.timeFormats, {"name":"timeFormat", "id":"timeFormat", "class":"form-control"}, timeFormat))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="dateFormat" class="col-md-12 control-label">Date Format</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.dateFormats, {"name":"date_format", "id":"date-format", "class":"form-control"}, dateFormat))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n        <div class="col-md-4 col-lg-4">\r\n            <div class="form-group">\r\n                <label for="phone" class="col-md-12 control-label">Phone Number</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(phone) +
'" name="phone" id="phone" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="fax" class="col-md-12 control-label">Fax Number</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(fax) +
'" name="fax" id="fax" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="email" class="col-md-12 control-label">Business Email Address</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(email) +
'" name="email" id="email" />\r\n                </div>\r\n            </div>\r\n            <!--div class="checkbox-control-container">\r\n                <input type="checkbox" value="1" data-unchecked-value="0" id="invoiceEmailSame" name="invoiceEmailSame" />\r\n                <label for="invoiceEmailSame" class="control-label">Invoice Email is the same as Business Email</label>\r\n            </div-->\r\n            <div class="form-group">\r\n                <label for="billingEmail" class="col-md-12 control-label">Invoice Email Address</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(billingEmail) +
'" name="billingEmail" id="billingEmail" />\r\n                </div>\r\n            </div>\r\n            <hr/>\r\n            <div class="form-group">\r\n                <label for="website" class="col-md-12 control-label">Website URL</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(website) +
'" name="website" id="website" />\r\n                </div>\r\n            </div>\r\n            <!--div class="form-group">\r\n                <label for="business-address-1" class="col-md-12 control-label">Facebook</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="" name="address_1" id="business-address-1" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="business-address-1" class="col-md-12 control-label">Twitter</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="" name="address_1" id="business-address-1" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="business-address-1" class="col-md-12 control-label">LinkedIn</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="" name="address_1" id="business-address-1" />\r\n                </div>\r\n            </div-->\r\n        </div>\r\n          <div class="col-md-4 col-lg-4">\r\n            <span><i>business logo input should go here</i></span>\r\n          </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable/daily.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var startDate = '';
if(typeof(dtstart) != 'undefined' && dtstart){
startDate = moment(dtstart).format('MM/DD/YYYY');
}
;
__p += '\n<div class="row">\n    <div class="col-md-12">\n        <div class="row repeat-every-container">\n            <div class="col-md-5 form-group" style="margin-top: 10px;">\n                <label for="repeat-interval">Every</label>\n                <div class="input-group">\n                    <input type="text" name="interval" class="form-control" id="repeat-interval" value="';
 if(typeof(interval) != 'undefined' && interval){ print(interval) };
__p += '">\n                    <span class="input-group-addon">days</span>\n                </div>\n            </div>\n            <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; position: relative; height: 90px;">\n                <div style="text-align: center; background-color: white; position: absolute; top: 50%; width: 100%; padding: 5px; margin-top: -10px; z-index: 1">OR</div>\n                <div style="position: absolute; top: 5px; bottom: 5px; background-color: #E0E0E0; width: 1px; left: 50%;"></div>\n            </div>\n            <div class="col-md-6 form-group">\n                <label>On Specific Days</label>\n                <div class="repeat-day-selector">\n                    <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(0) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="0">Mo</button>\n                    <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(1) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="1">Tu</button>\n                    <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(2) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="2">We</button>\n                    <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(3) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="3">Th</button>\n                    <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(4) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="4">Fr</button>\n                    <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(5) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="5">Sa</button>\n                    <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(6) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="6">Su</button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class="row" id="effective-dates"></div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable/effective_dates.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var startDate = '';
if(typeof(dtstart) != 'undefined' && dtstart){
    startDate = moment(dtstart).format('MM/DD/YYYY');
}
var endDate = '';
if(typeof(until) != 'undefined' && until){
    endDate = moment(until).format('MM/DD/YYYY');
}
;
__p += '\n<div class="col-md-4 form-group">\n    <label for="repeat-start">Effective on</label>\n    <input type="text" name="dtstart" class="form-control" id="repeat-start" value="' +
__e(startDate) +
'" placeholder="MM/DD/YYYY">\n</div>\n<div class="col-md-8 form-group">\n    <label for="repeat-start" style="display: block">Effective until</label>\n    <div class="checkbox" style="padding: 0px; width: 80px; float: left; line-height: 35px; height: 35px; margin: 0px;">\n        <label>\n            <input type="radio" class="repeat-until-select" name="until_select" id="until-forever" value="forever" ';
 if(endDate == ''){ ;
__p += 'checked';
} ;
__p += '> Forever\n        </label>\n    </div>\n    <div class="input-group">\n        <span class="input-group-addon"><input type="radio" name="until_select" id="until-date" value="forever" ';
 if(endDate != ''){ ;
__p += 'checked';
} ;
__p += '></span>\n        <input type="text" name="until" class="form-control repeat-until-select" value="' +
__e(endDate) +
'" id="repeat-until" style="width: 110px;" placeholder="MM/DD/YYYY">\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable/monthly.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row">\n    <div class="col-md-12">\n        <div class="row repeat-every-container">\n            <div class="col-md-5 form-group" style="margin-top: 30px;">\n                <label for="repeat-interval">Every</label>\n                <div class="input-group">\n                    <input type="text" name="interval" class="form-control" id="repeat-interval" value="';
 if(typeof(interval) != 'undefined' && interval){ print(interval) };
__p += '">\n                    <span class="input-group-addon">months</span>\n                </div>\n            </div>\n            <div class="col-md-1" style="padding-left: 0px; padding-right: 0px; position: relative; height: 125px;">\n                <div style="text-align: center; background-color: white; position: absolute; top: 50%; width: 100%; padding: 5px; margin-top: -10px; z-index: 1; border: 1px solid #E0E0E0">OR</div>\n                <div style="position: absolute; top: 5px; bottom: 5px; background-color: #E0E0E0; width: 1px; left: 50%;"></div>\n            </div>\n            <div class="col-md-6 form-group">\n                <label>On Specific Months</label>\n                <div class="repeat-month-selector">\n                    ';
 for(var x = 1; x <= 12; x++){
                    var isActive = (typeof(bymonth) != 'undefined' && bymonth != null && bymonth.indexOf(x) !== -1);
                    ;
__p += '\n                    <button class="btn btn-default repeat-month ';
 if(isActive){ ;
__p += 'active';
 } ;
__p += '" data-value="' +
__e(x) +
'">' +
__e(moment().month(x - 1).format('MMM')) +
'</button>\n                    ';
 } ;
__p += '\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class="row" id="effective-dates"></div>\n<div class="row">\n    <div class="col-md-12 form-group">\n        <label>On Day of the Month</label>\n        <div class="repeat-day-selector">\n            ';
 for(i = 1; i <= 28; i++){
            var checked = (typeof(bymonthday) != 'undefined' && bymonthday != null && _.indexOf(bymonthday, i) > -1);
            ;
__p += '\n            <button class="btn btn-default repeat-day-select ';
 if(checked){ print('active') } ;
__p += '" data-value="' +
__e(i) +
'">\n                ' +
__e(i) +
'\n            </button>\n            ';
 } ;
__p += '\n            <button class="btn btn-default repeat-day-select last ';
 if(typeof(bymonthday) != 'undefined' && bymonthday != null && _.indexOf(bymonthday, -1) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="-1">\n                Last day of month\n            </button>\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable/quarterly.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var startDate = '';
if(typeof(dtstart) != 'undefined' && dtstart){
    startDate = moment(dtstart).format('MM/DD/YYYY');
}
;
__p += '\n<div class="row">\n    <div class="col-md-12 form-group">\n        <label>On Specific Months</label>\n        <div class="repeat-month-selector">\n            ';
 for(var x = 1; x <= 12; x++){
            var isActive = (typeof(bymonth) != 'undefined' && bymonth != null && bymonth.indexOf(x) !== -1);
            ;
__p += '\n            <button style="width: 16.6%" class="btn btn-default repeat-month ';
 if(isActive){ ;
__p += 'active';
 } ;
__p += '" data-value="' +
__e(x) +
'">' +
__e(moment().month(x - 1).format('MMM')) +
'</button>\n            ';
 } ;
__p += '\n        </div>\n    </div>\n</div>\n<div class="row">\n    <div class="col-md-12 form-group">\n        <label>On Day of the Month</label>\n        <div class="repeat-day-selector">\n            ';
 for(i = 1; i <= 28; i++){
            var checked = (typeof(bymonthday) != 'undefined' && bymonthday != null && _.indexOf(bymonthday, i) > -1);
            ;
__p += '\n            <button class="btn btn-default repeat-day-select ';
 if(checked){ print('active') } ;
__p += '" data-value="' +
__e(i) +
'">\n                ' +
__e(i) +
'\n            </button>\n            ';
 } ;
__p += '\n            <button class="btn btn-default repeat-day-select last ';
 if(typeof(bymonthday) != 'undefined' && bymonthday != null && _.indexOf(bymonthday, -1) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="-1">\n                Last day of month\n            </button>\n        </div>\n    </div>\n</div>\n<div class="row" id="effective-dates"></div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable/schedule.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var scheduleOptions = {
    '': '- Select Schedule -',
    'daily': 'Daily',
    'weekly': 'Weekly',
    'monthly': 'Monthly',
    'quarterly': 'Quarterly',
    'yearly': 'Yearly'
};
;
__p += '\n<div class="row">\n    <div class="col-md-12 form-group">\n        <label>Schedule</label>\n        ' +
((__t = (_.dropdown(scheduleOptions, {id: 'schedule-period', class: 'form-control'}, period) )) == null ? '' : __t) +
'\n    </div>\n</div>\n<div class="row" id="schedule-settings"></div>\n';
 if(period && showTimeOfDay){ ;
__p += '\n<div class="row">\n    <div class="col-md-12 form-group">\n        <div class="btn-group btn-group-justified">\n            <div class="btn-group">\n                <button class="btn btn-default repeat-time-select ';
 if(typeof(byhour) != 'undefined' && byhour == 0){ ;
__p += 'active';
 } ;
__p += '" data-value="start">Beginning of Day</button>\n            </div>\n            <div class="btn-group">\n                <button class="btn btn-default repeat-time-select ';
 if(typeof(byhour) != 'undefined' && byhour == 23){ ;
__p += 'active';
 } ;
__p += '" data-value="end">End of Day</button>\n            </div>\n        </div>\n    </div>\n</div>\n';
 } ;
__p += '\n';
 if(showSaveButton){ ;
__p += '\n<div class="row">\n    <div class="col-sm-6 col-sm-offset-6">\n        <button class="btn btn-primary save-schedule pull-right">Save</button>\n    </div>\n</div>\n';
 } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable/visualization.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var MONTH_WIDTH_MULTIPLIER = 4;
var period_repeatable = false;
var period_rules = false;
var period_period = false;
var num_periods = 60;

if(period){
    period_repeatable = period.rrule.options;
    period_rules = period.rules;
    period_period = period.period;
    num_periods = period.rules.length;
}

var end_date = moment().add(3, 'years');
var start_date = moment().startOf('month');
var continued = true;
var margin = 2;

var num_days = 0;
var day_rule_indexes = {};

var end_at_next_period = false;
var total_days = days.length;

;
__p += '\n\n<div class="scroll-left">\n    <span class="fa fa-chevron-left"></span>\n</div>\n\n<div class="repeatable-visualization">\n\n    <!-- Months -->\n    <div class="repeatable-visualization-row">\n    ';

    var date = start_date.clone();
    while(date.isBefore(end_date)){
        var month_width = (date.daysInMonth() * MONTH_WIDTH_MULTIPLIER); ;
__p += '\n        <div class="repeatable-period" style="width: ' +
__e(month_width) +
'px">' +
__e(date.format('MMM \'YY')) +
'</div>\n        ';
 date.add(1, 'month');
    }
    if(continued){ ;
__p += '\n        <div class="repeatable-period" style="width: 115px; margin-left: 15px; position: relative; padding-left: 15px; top: 10px; font-size: 16px; background: white; z-index: 2; box-shadow: 0px 0px 5px 10px white;">Continued...</div>\n    ';
 } ;
__p += '\n    </div>\n\n    <!-- Spanning period -->\n    <div class="repeatable-visualization-row">\n    ';

    if(period){

        var date = start_date.clone();
        var rule_index = 0;

        for(var x = 0; x < num_periods; x++){

            var rule_date = moment(period_rules[rule_index]).second(0).minute(0).hour(0);
            var next_rule_date = false;

            if(period_rules[rule_index + 1]){
                next_rule_date = moment(period_rules[rule_index + 1]).second(0).minute(0).hour(0);
            }else{
                continue;
            }
            var days_in_period = Math.ceil(next_rule_date.diff(rule_date, 'days', true));
            var period_width = (days_in_period * MONTH_WIDTH_MULTIPLIER) - margin;

            var start_offset = 0;
            if(rule_index == 0){
                var days_from_start = Math.ceil(rule_date.diff(start_date, 'days', true));
                start_offset = (days_from_start * MONTH_WIDTH_MULTIPLIER) + margin;
            }
            ;
__p += '\n            <div class="repeatable-period billing-period" style="width: ' +
__e(period_width) +
'px; ';
 if(rule_index == 0){ ;
__p += 'margin-left: ' +
__e(start_offset) +
'px; ';
 } ;
__p += '"></div>\n            ';
 rule_index++;
        }
    }
    ;
__p += '\n    </div>\n\n    <!-- Days -->\n    ';
 if(days && days.length > 0){

        for(var day_num = 0; day_num < total_days; day_num++){ ;
__p += '\n\n            <div class="repeatable-visualization-row" style="height: 17px;">\n                ';

                var rules = days[day_num].rules;
                var isActive = days[day_num]._active;
                var line_number = day_num + 1;

                for(var i = 0; i < rules.length; i++){
                    var rule_date = moment(rules[i]).second(0).minute(0).hour(0);
                    var days_from_start = Math.ceil( rule_date.diff(start_date, 'days', true) );
                    var position = (days_from_start * MONTH_WIDTH_MULTIPLIER); ;
__p += '\n\n                <div class="billing-day day-' +
__e(line_number) +
' ';
 if(isActive){ ;
__p += 'active';
 } ;
__p += '" style="left: ' +
__e(position) +
'px; z-index: ' +
__e(rules.length - i) +
'">' +
__e(line_number) +
'</div>\n                ';
 } ;
__p += '\n            </div>\n        ';
 } ;
__p += '\n    ';
 } ;
__p += '\n</div>\n\n<div class="scroll-right">\n    <span class="fa fa-chevron-right"></span>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable/weekly.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var startDate = '';
if(typeof(dtstart) != 'undefined' && dtstart){
startDate = moment(dtstart).format('MM/DD/YYYY');
}
;
__p += '\n<div class="row">\n    <div class="col-md-6 form-group">\n        <label for="repeat-interval">Every</label>\n        <div class="input-group">\n            <input type="text" name="interval" class="form-control" id="repeat-interval" value="';
 if(typeof(interval) != 'undefined' && interval){ print(interval) };
__p += '">\n            <span class="input-group-addon">weeks</span>\n        </div>\n    </div>\n</div>\n<div class="row">\n    <div class="col-md-12 form-group">\n        <label>On Day of the Week</label>\n        <div class="btn-group btn-group-justified">\n            <div class="btn-group">\n                <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(0) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="0">Mo</button>\n            </div>\n            <div class="btn-group">\n                <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(1) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="1">Tu</button>\n            </div>\n            <div class="btn-group">\n                <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(2) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="2">We</button>\n            </div>\n            <div class="btn-group">\n                <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(3) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="3">Th</button>\n            </div>\n            <div class="btn-group">\n                <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(4) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="4">Fr</button>\n            </div>\n            <div class="btn-group">\n                <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(5) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="5">Sa</button>\n            </div>\n            <div class="btn-group">\n                <button class="btn btn-default repeat-day ';
 if(byweekday && byweekday.indexOf(6) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="6">Su</button>\n            </div>\n        </div>\n    </div>\n</div>\n<div class="row" id="effective-dates"></div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["member_billing/templates/repeatable/yearly.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


var startDate = '';
if(typeof(dtstart) != 'undefined' && dtstart){
startDate = moment(dtstart).format('MM/DD/YYYY');
}
;
__p += '\n<div class="row">\n    <div class="col-md-6 form-group">\n        <label for="repeat-interval">Every</label>\n        <div class="input-group">\n            <input type="text" name="interval" class="form-control" id="repeat-interval" value="';
 if(typeof(interval) != 'undefined' && interval){ print(interval) };
__p += '">\n            <span class="input-group-addon">years</span>\n        </div>\n    </div>\n</div>\n<div class="row">\n    <div class="col-md-12 form-group">\n        <label>On Month</label>\n        <div class="repeat-month-selector">\n            ';
 for(var x = 1; x <= 12; x++){
            var isActive = (typeof(bymonth) != 'undefined' && bymonth != null && bymonth.indexOf(x) !== -1);
            ;
__p += '\n            <button style="width: 16.6%" class="btn btn-default repeat-month ';
 if(isActive){ ;
__p += 'active';
 } ;
__p += '" data-value="' +
__e(x) +
'">' +
__e(moment().month(x - 1).format('MMM')) +
'</button>\n            ';
 } ;
__p += '\n        </div>\n    </div>\n</div>\n<div class="row">\n    <div class="col-md-12 form-group">\n        <label>On Day of the Month</label>\n        <div class="repeat-day-selector">\n            ';
 for(i = 1; i <= 28; i++){
            var checked = (typeof(bymonthday) != 'undefined' && bymonthday != null && _.indexOf(bymonthday, i) > -1);
            ;
__p += '\n            <button class="btn btn-default repeat-day-select ';
 if(checked){ print('active') } ;
__p += '" data-value="' +
__e(i) +
'">\n                ' +
__e(i) +
'\n            </button>\n            ';
 } ;
__p += '\n            <button class="btn btn-default repeat-day-select last ';
 if(typeof(bymonthday) != 'undefined' && bymonthday != null && _.indexOf(bymonthday, -1) > -1){ ;
__p += 'active';
 } ;
__p += '" data-value="-1">\n                Last day of month\n            </button>\n        </div>\n    </div>\n</div>\n<div class="row" id="effective-dates"></div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/billing/billing_page_settings.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/billing/billing_settings_required_fields.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += 'Required Fields';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/billing/statement_settings.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="col-md-12 settings-header">\r\n            <div class="settings-title">Statement Settings</div>\r\n            <div class="settings-subtitle">These settings are used in determining what your customers see when they come to try and book online.</div>\r\n        </div>\r\n        <div class="col-md-4">\r\n            <div class="form-group">\r\n                <label for="quickbooks-terminal" class="col-md-12 control-label">Statements will be sent</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="col-md-12">\r\n                    on ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
' of the period\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">Advanced Options</label>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-4">\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">Charges on statements</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="" name="to_date" id="to-date" /> Day(s)\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Apply Finance Charges</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="col-md-12">\r\n                    <input placeholder="amount" class="form-control" type="text" value="" name="to_date" id="to-date" />\r\n                    ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">Apply Finance Charges after overdue</label>\r\n                <div class="col-md-12">\r\n                    <input placeholder="amount" class="form-control" type="text" value="" name="to_date" id="to-date" />\r\n                    ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-4">\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Enable Auto-pay</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-8 control-label">Charge after</label>\r\n                <label for="to-date" class="col-md-4 control-label">Charge after</label>\r\n                <div class="col-md-8">\r\n                    <input class="form-control" type="text" value="" name="to_date" id="to-date" /> Day(s)\r\n                </div>\r\n                <div class="col-md-4">\r\n                    ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Auto-pay overdue balance</label>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-12">\r\n        </div>\r\n        <div class="col-md-6">\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Include Past Due Amount</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Include Member Balance**</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Include Customer Credit**</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Include Transaction History</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">** All charges that fall between statement dates will appear on the next statement</label>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Send Zero Charge Statements</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Show Invoice Line Items</label>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-6">\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">Statement Notes</label>\r\n                <div class="col-md-12">\r\n                    <textarea class="form-control" type="text" value="" name="to_date" id="to-date"> </textarea>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">Statement Terms and Conditions</label>\r\n                <div class="col-md-12">\r\n                    <textarea class="form-control" type="text" value="" name="to_date" id="to-date"> </textarea>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">Statement Page Message</label>\r\n                <div class="col-md-12">\r\n                    <textarea class="form-control" type="text" value="" name="to_date" id="to-date"> </textarea>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/online_reservations/edit_online_reservation_group_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\r\n        <i class="fa fa-question-o pull-right header-icon"></i>\r\n        <h4 class="modal-title">\r\n            Online Reservation Group\r\n        </h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <div class="modal-content">\r\n            <div class="col-md-4">\r\n                <div class="form-group">\r\n                    <label for="businessName" class="col-md-12 control-label">Online Booking Group Name</label>\r\n                    <div class="col-md-12">\r\n                        <input class="form-control" type="text" value="" name="businessName" id="businessName" />\r\n                    </div>\r\n                </div>\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="invoiceEmailSame" name="invoiceEmailSame" />\r\n                    <label for="invoiceEmailSame" class="control-label">Password Protect Group</label>\r\n                </div>\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="invoiceEmailSame" name="invoiceEmailSame" />\r\n                    <label for="invoiceEmailSame" class="control-label">Display Booked Times and Names</label>\r\n                </div>\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="invoiceEmailSame" name="invoiceEmailSame" />\r\n                    <label for="invoiceEmailSame" class="control-label">Allow Player Name Entry</label>\r\n                </div>\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="invoiceEmailSame" name="invoiceEmailSame" />\r\n                    <label for="invoiceEmailSame" class="control-label">Use Customer Pricing</label>\r\n                </div>\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="invoiceEmailSame" name="invoiceEmailSame" />\r\n                    <label for="invoiceEmailSame" class="control-label">Hide Prices Online</label>\r\n                </div>\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="invoiceEmailSame" name="invoiceEmailSame" />\r\n                    <label for="invoiceEmailSame" class="control-label">Require Credit Card Online</label>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="dateFormat" class="col-md-12 control-label">Online Open Time</label>\r\n                    <div class="col-md-12">\r\n                        ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"date_format", "id":"date-format", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="dateFormat" class="col-md-12 control-label">Online Closing Time</label>\r\n                    <div class="col-md-12">\r\n                        ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"date_format", "id":"date-format", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="dateFormat" class="col-md-12 control-label">Days In Booking Window</label>\r\n                    <div class="col-md-12">\r\n                        ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.daysInBookingWindow(), {"name":"date_format", "id":"date-format", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="dateFormat" class="col-md-12 control-label">Minimum Players</label>\r\n                    <div class="col-md-12">\r\n                        ' +
((__t = (_.dropdown({"4":"4","3":"3","2":"2","1":"1"}, {"name":"date_format", "id":"date-format", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="dateFormat" class="col-md-12 control-label">Online Rates Allowed</label>\r\n                    <div class="col-md-12">\r\n                        ' +
((__t = (_.dropdown({"0":"Walking","2":"Riding","1":"Both"}, {"name":"date_format", "id":"date-format", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="dateFormat" class="col-md-12 control-label">Minimum Holes</label>\r\n                    <div class="col-md-12">\r\n                        ' +
((__t = (_.dropdown({"0":"No Limit","9":"9 Only","18":"18 Only"}, {"name":"date_format", "id":"date-format", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="dateFormat" class="col-md-12 control-label">Allow Online Payment</label>\r\n                    <div class="col-md-12">\r\n                        ' +
((__t = (_.dropdown({"0":"Not Allowed","1":"Optional","2":"Required"}, {"name":"date_format", "id":"date-format", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-8">\r\n                <div class="form-group">\r\n                    <label for="to-date" class="col-md-12 control-label">Available Customer Groups</label>\r\n                    <div class="col-md-12">\r\n                        <textarea class="form-control" type="text" value="" name="to_date" id="to-date"></textarea>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="to-date" class="col-md-12 control-label">Available Applicable Passes</label>\r\n                    <div class="col-md-12">\r\n                        <textarea class="form-control" type="text" value="" name="to_date" id="to-date"></textarea>\r\n                    </div>\r\n                </div>\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="invoiceEmailSame" name="invoiceEmailSame" />\r\n                    <label for="invoiceEmailSame" class="control-label">Require Booking Fee</label>\r\n                </div>\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="invoiceEmailSame" name="invoiceEmailSame" />\r\n                    <label for="invoiceEmailSame" class="control-label">Require Booking Fee Per Person</label>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="businessName" class="col-md-12 control-label">Booking Fee Item</label>\r\n                    <div class="col-md-12">\r\n                        <input class="form-control" type="text" value="" name="businessName" id="businessName" />\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="to-date" class="col-md-12 control-label">Booking Fee Terms</label>\r\n                    <div class="col-md-12">\r\n                        <textarea class="form-control" type="text" value="" name="to_date" id="to-date"></textarea>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="customer-info clearfix"></div>\r\n</div>\r\n<div class="modal-footer">\r\n    <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\r\n    <button class="btn btn-primary pull-right" id="save-changes">Save Changes</button>\r\n</div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/online_reservations/edit_online_special_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\r\n        <i class="fa fa-question-o pull-right header-icon"></i>\r\n        <h4 class="modal-title">\r\n            Online Reservation Group\r\n        </h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <div class="modal-content">\r\n            <div class="special-settings">\r\n                <div class="special-settings-header col-md-12">\r\n                    <div class="col-md-4">\r\n                        <div class="form-group">\r\n                            <label for="businessName" class="col-md-12 control-label">Timeframe Name</label>\r\n                            <div class="col-md-12">\r\n                                <input class="form-control" type="text" value="" name="businessName" id="businessName" />\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class="col-md-4">\r\n                        <div class="form-group">\r\n                            <label for="businessName" class="col-md-12 control-label">Date</label>\r\n                            <div class="col-md-12">\r\n                                <input class="form-control" type="text" value="" name="businessName" id="businessName" />\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="special-settings-body col-md-12">\r\n                    <table>\r\n                        <thead>\r\n                            <tr>\r\n                                <th>PRODUCT</th>\r\n                                <th>GREEN</th>\r\n                                <th>CARTS</th>\r\n                                <th>TIME RESTRICTIONS</th>\r\n                                <th></th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n                <div class="special-settings-footer col-md-12">\r\n                    <button class="btn btn-muted pull-left" data-dismiss="modal">+</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="customer-info clearfix"></div>\r\n</div>\r\n<div class="modal-footer">\r\n    <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\r\n    <button class="btn btn-primary pull-right" id="save-changes">Save Changes</button>\r\n</div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/online_reservations/general.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="settings-header row">\r\n            <div class="col-md-6 settings-title">General Online Reservations Settings</div>\r\n            <div class="col-md-6">\r\n              <div class="form-group marg-top-10">\r\n                  <label for="tee_sheet_id" class="col-md-12 control-label">Choose Tee Sheet</label>\r\n                  <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown({}, {"name":"tee_sheet_id", "id":"tee_sheet_id", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                  </div>\r\n              </div>\r\n            </div>\r\n            <div class="settings-subtitle">These settings are used in the Online Booking application.</div>\r\n        </div>\r\n    </div>\r\n    <div class="row">\r\n      <div class="col-md-4 tee-sheet-online-general-settings"></div>\r\n        <div class="col-md-8">\r\n          <div class="panel-group settings-panel online-reservation-groups-panel-group">\r\n              <div class="panel panel-default">\r\n                  <div class="panel-heading">\r\n                      <h4 class="panel-title pull-left">\r\n                        Online Reservations Groups\r\n                      </h4>\r\n                      <div class="pull-right form-inline marg-top-3">\r\n                        <div class="btn-group form-group">\r\n                            <button type="button" class="btn add_online_reservation_group btn-primary btn-sm panel-button">Add</button>\r\n                        </div>\r\n                    </div>\r\n                    <div class="clear"></div>\r\n                </div>\r\n                <div class="panel-body">\r\n                    <table class="online-reservation-group-table">\r\n                        <thead>\r\n                        <tr>\r\n                            <th>NAME</th>\r\n                            <th>ACTIVE</th>\r\n                            <th>REQUIRE LOGIN</th>\r\n                            <th>PLAYER TYPE</th>\r\n                            <th class="text-center">ACTIONS</th>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody class="online-reservation-group-list">\r\n\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="panel-group settings-panel online-specials-panel-group">\r\n            <div class="panel panel-default">\r\n                <div class="panel-heading">\r\n                    <h4 class="panel-title pull-left">\r\n                        Online Specials\r\n                    </h4>\r\n                    <div class="pull-right form-inline marg-top-3">\r\n                      <div class="btn-group form-group">\r\n                            <button type="button" class="btn add_online_special btn-primary btn-sm panel-button">Add</button>\r\n                        </div>\r\n                      </div>\r\n                      <div class="clear"></div>\r\n                    </div>\r\n                <div class="panel-body">\r\n                    <table class="online-specials-table">\r\n                        <thead>\r\n                        <tr>\r\n                            <th>NAME</th>\r\n                            <th>DAYS</th>\r\n                            <th>TIMES</th>\r\n                            <th class="text-center">ACTIONS</th>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody class="online-specials-list">\r\n\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n              </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/online_reservations/online_general_settings.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="form-group">\r\n    <label for="to-date" class="col-md-12 control-label">Online Reservations URL</label>\r\n    <div class="col-md-12">\r\n        <input class="form-control" type="text" value="https://foreupsoftware.com/index.php/booking/' +
__e(course_id) +
'/' +
__e(teesheet_id) +
'#teetimes" name="to_date" id="to-date" disabled/>\r\n    </div>\r\n</div>\r\n<div class="form-group">\r\n    <label for="online_open_time" class="col-md-12 control-label">Online Open Time</label>\r\n    <div class="col-md-12">\r\n        ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"online_open_time", "id":"online_open_time", "class":"form-control"}, online_open_time))) == null ? '' : __t) +
'\r\n    </div>\r\n</div>\r\n<div class="form-group">\r\n    <label for="online_close_time" class="col-md-12 control-label">Online Close Time</label>\r\n    <div class="col-md-12">\r\n        ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"online_close_time", "id":"online_close_time", "class":"form-control"}, online_close_time))) == null ? '' : __t) +
'\r\n    </div>\r\n</div>\r\n<div class="form-group">\r\n    <label for="days_in_booking_window" class="col-md-12 control-label">Days in Booking Window</label>\r\n    <div class="col-md-12">\r\n        ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.daysInBookingWindow(), {"name":"days_in_booking_window", "id":"days_in_booking_window", "class":"form-control"}, days_in_booking_window))) == null ? '' : __t) +
'\r\n    </div>\r\n</div>\r\n<div class="form-group">\r\n    <label for="minimum_players" class="col-md-12 control-label">Minimum Players</label>\r\n    <div class="col-md-12">\r\n        ' +
((__t = (_.dropdown({"4":"4","3":"3","2":"2","1":"1"}, {"name":"minimum_players", "id":"minimum_players", "class":"form-control"}, minimum_players))) == null ? '' : __t) +
'\r\n    </div>\r\n</div>\r\n<div class="form-group">\r\n    <label for="limit_holes" class="col-md-12 control-label">Limit Holes</label>\r\n    <div class="col-md-12">\r\n        ' +
((__t = (_.dropdown({"0":"No Limit","9":"9 Only","18":"18 Only"}, {"name":"limit_holes", "id":"limit_holes", "class":"form-control"}, limit_holes))) == null ? '' : __t) +
'\r\n    </div>\r\n</div>\r\n<div class="form-group">\r\n    <div class="checkbox-control-container">\r\n        <input type="checkbox" value="1" data-unchecked-value="0" id="require_credit_card" name="require_credit_card" style="margin-top: 5px;" ' +
__e( require_credit_card ? 'checked' : '' ) +
' />\r\n        <label for="require_credit_card" class="control-label">Require credit card online</label>\r\n    </div>\r\n</div>\r\n<div class="form-group">\r\n    <div class="checkbox-control-container">\r\n        <input type="checkbox" value="1" data-unchecked-value="0" id="notify_all_participants" name="notify_all_participants" style="margin-top: 5px;" ' +
__e( notify_all_participants ? 'checked' : '' ) +
' />\r\n        <label for="notify_all_participants" class="control-label">Send reservation confirmation</label>\r\n    </div>\r\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/online_reservations/online_messages_settings.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<form>\r\n  <div class="clearfix">\r\n      <div class="settings-header row">\r\n          <div class="col-md-12 settings-title">General Online Reservations Settings</div>\r\n          <div class="settings-subtitle">These settings are used in the Online Booking application.</div>\r\n      </div>\r\n      <div class="row">\r\n        <div class="form-group">\r\n            <label for="onlineBookingWelcomeMessage" class="col-md-12 control-label">Online Booking Welcome Message</label>\r\n            <div class="col-md-12">\r\n                <div class="form-control text-editor" type="text" id="onlineBookingWelcomeMessage"> </div>\r\n            </div>\r\n        </div>\r\n        <div class="form-group">\r\n            <label for="bookingRules" class="col-md-12 control-label">Booking Rules</label>\r\n            <div class="col-md-12">\r\n                <div class="form-control text-editor" type="text" id="bookingRules"> </div>\r\n            </div>\r\n        </div>\r\n        <div class="form-group">\r\n            <label for="noShowPolicy" class="col-md-12 control-label">No Show Policy</label>\r\n            <div class="col-md-12">\r\n                <div class="form-control text-editor" type="text" id="noShowPolicy"> </div>\r\n            </div>\r\n        </div>\r\n        <div class="form-group">\r\n            <label for="termsAndConditions" class="col-md-12 control-label">Terms and Conditions</label>\r\n            <div class="col-md-12">\r\n                <div class="form-control text-editor" type="text" id="termsAndConditions"> </div>\r\n            </div>\r\n        </div>\r\n        <div class="form-group">\r\n            <label for="reservationEmailText" class="col-md-12 control-label">Reservation Email Message</label>\r\n            <div class="col-md-12">\r\n                <div class="form-control text-editor" type="text" id="reservationEmailText"> </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/online_reservations/online_reservation_group_list_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            ' +
__e(name) +
'\r\n        </div>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            ' +
__e( isActive ? 'Yes' : 'No' ) +
'\r\n        </div>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            ' +
__e( onlineBookingProtected ? 'Yes' : 'No' ) +
'\r\n        </div>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            ' +
__e( priceClass ) +
'\r\n        </div>\r\n    </div>\r\n</td>\r\n<td class="text-center">\r\n    <div class="btn-group">\r\n        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\r\n            <i class="fa fa-sort-down" aria-hidden="true"></i>\r\n        </button>\r\n        <ul class="dropdown-menu">\r\n            <li class="edit-terminal"><a><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a></li>\r\n            <li class="delete-terminal"><a><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a></li>\r\n        </ul>\r\n    </div>\r\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/online_reservations/online_special_list_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            ' +
__e(name) +
'\r\n        </div>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n\r\n        </div>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n\r\n        </div>\r\n    </div>\r\n</td>\r\n<td class="text-center">\r\n    <div class="btn-group">\r\n        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\r\n            <i class="fa fa-sort-down" aria-hidden="true"></i>\r\n        </button>\r\n        <ul class="dropdown-menu">\r\n            <li class="edit-terminal"><a><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a></li>\r\n            <li class="delete-terminal"><a><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a></li>\r\n        </ul>\r\n    </div>\r\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/online_reservations/online_view_settings.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<form>\r\n  <div class="clearfix">\r\n      <div class="settings-header row">\r\n          <div class="col-md-6 settings-title">General Online Reservations Settings</div>\r\n          <div class="col-md-6">\r\n            <div class="form-group marg-top-10">\r\n                <label for="tee_sheet_id" class="col-md-12 control-label">Choose Tee Sheet</label>\r\n                <div class="col-md-12">\r\n                  ' +
((__t = (_.dropdown({}, {"name":"tee_sheet_id", "id":"tee_sheet_id", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n          </div>\r\n          <div class="settings-subtitle">These settings are used in the Online Booking application.</div>\r\n      </div>\r\n  </div>\r\n        <div id="online-view-settings-holder">\r\n\r\n        </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/online_reservations/online_view_tee_sheet_settings.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="col-md-4">\r\n    <div class="panel-group">\r\n        <div class="panel panel-default">\r\n            <div class="panel-heading">\r\n                <h4 class="panel-title">\r\n                    Online Booking Visible Hours\r\n                </h4>\r\n            </div>\r\n            <div class="panel-body">\r\n                <table>\r\n                    <thead>\r\n                        <tr>\r\n                            <th>DAY OF THE WEEK</th>\r\n                            <th>TIME RESTRICTIONS</th>\r\n                        </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                        <tr>\r\n                            <td>Monday</td>\r\n                            <td>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                                <div class="time-range-arrow col-md-2">-></div>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Tuesday</td>\r\n                            <td>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                                <div class="time-range-arrow col-md-2">-></div>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Wednesday</td>\r\n                            <td>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                                <div class="time-range-arrow col-md-2">-></div>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Thursday</td>\r\n                            <td>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                                <div class="time-range-arrow col-md-2">-></div>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Friday</td>\r\n                            <td>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                                <div class="time-range-arrow col-md-2">-></div>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Saturday</td>\r\n                            <td>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                                <div class="time-range-arrow col-md-2">-></div>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Sunday</td>\r\n                            <td>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                                <div class="time-range-arrow col-md-2">-></div>\r\n                                <div class="col-md-5">\r\n                                    ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                                </div>\r\n                            </td>\r\n                        </tr>\r\n                    </tbody>\r\n                </table>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class="col-md-8">\r\n    <div class="panel-group">\r\n        <div class="panel panel-default">\r\n            <div class="panel-heading">\r\n                <h4 class="panel-title">\r\n                    Aggregate Booking Settings\r\n                </h4>\r\n            </div>\r\n            <div class="panel-body">\r\n                <div class="col-md-6">\r\n                    <div class="form-group">\r\n                        <label for="quickbooks-terminal" class="col-md-12 control-label">Online Open Time</label>\r\n                        <div class="col-md-12">\r\n                            ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                        </div>\r\n                    </div>\r\n                    <div class="form-group">\r\n                        <label for="quickbooks-terminal" class="col-md-12 control-label">Online Close Time</label>\r\n                        <div class="col-md-12">\r\n                            ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.halfHours, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                        </div>\r\n                    </div>\r\n                    <div class="form-group">\r\n                        <label for="quickbooks-terminal" class="col-md-12 control-label">Days in Booking Window</label>\r\n                        <div class="col-md-12">\r\n                            ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.daysInBookingWindow(), {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                        </div>\r\n                    </div>\r\n                    <div class="form-group">\r\n                        <label for="quickbooks-terminal" class="col-md-12 control-label">Minimum Players</label>\r\n                        <div class="col-md-12">\r\n                            ' +
((__t = (_.dropdown({"4":"4","3":"3","2":"2","1":"1"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                        </div>\r\n                    </div>\r\n                    <div class="form-group">\r\n                        <label for="quickbooks-terminal" class="col-md-12 control-label">Limit Holes</label>\r\n                        <div class="col-md-12">\r\n                            ' +
((__t = (_.dropdown({"0":"No Limit","9":"9 only","18":"18 only"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="col-md-6">\r\n                    <div class="form-group">\r\n                        <label for="quickbooks-terminal" class="col-md-12 control-label">Online Rates Allowed</label>\r\n                        <div class="col-md-12">\r\n                            ' +
((__t = (_.dropdown({"0":"Walking","2":"Riding","1":"Both"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                        </div>\r\n                    </div>\r\n                    <div class="form-group">\r\n                        <label for="quickbooks-terminal" class="col-md-12 control-label">Allow Pre-Payment Online</label>\r\n                        <div class="col-md-12">\r\n                            ' +
((__t = (_.dropdown({"0":"Not Allowed","1":"Optional","2":"Required"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                        </div>\r\n                    </div>\r\n                    <!--div class="form-group">\r\n                        <label for="quickbooks-terminal" class="col-md-12 control-label">Reservations Per Day Per Player</label>\r\n                        <div class="col-md-12">\r\n                            ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                        </div>\r\n                    </div>\r\n                    <div class="form-group">\r\n                        <label for="quickbooks-terminal" class="col-md-12 control-label">Online Booking New Day Start Time</label>\r\n                        <div class="col-md-12">\r\n                            ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                        </div>\r\n                    </div-->\r\n                    <div class="form-group">\r\n                        <div class="checkbox-control-container">\r\n                            <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                            <label for="include_cash_logs" class="control-label">Require credit card online</label>\r\n                        </div>\r\n                    </div>\r\n                    <div class="form-group">\r\n                        <div class="checkbox-control-container">\r\n                            <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                            <label for="include_cash_logs" class="control-label">Require Booking Fee</label>\r\n                        </div>\r\n                    </div>\r\n                    <!--div class="form-group">\r\n                        <div class="checkbox-control-container">\r\n                            <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                            <label for="include_cash_logs" class="control-label">Hide Online Prices</label>\r\n                        </div>\r\n                    </div>\r\n                    <div class="form-group">\r\n                        <div class="checkbox-control-container">\r\n                            <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                            <label for="include_cash_logs" class="control-label">Include Tax in Online Booking Prices</label>\r\n                        </div>\r\n                    </div>\r\n                    <div class="form-group">\r\n                        <div class="checkbox-control-container">\r\n                            <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                            <label for="include_cash_logs" class="control-label">Apply These Settings to All Courses</label>\r\n                        </div>\r\n                    </div-->\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/loyalty/loyalty_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="col-md-12 settings-header">\r\n            <div class="settings-title">Customer Loyalty</div>\r\n            <div class="settings-subtitle">\r\n                Use these settings to create and adjust customer loyalty benefits.\r\n            </div>\r\n        </div>\r\n        <div class="col-md-12">\r\n            <div class="checkbox-control-container">\r\n                <input type="checkbox" value="1" data-unchecked-value="0" id="useLoyalty" name="useLoyalty" ' +
__e( useLoyalty ? 'checked' : '' ) +
' />\r\n                <label for="useLoyalty" class="control-label">Track Loyalty Points</label>\r\n            </div>\r\n            <div class="checkbox-control-container">\r\n                <input type="checkbox" value="1" data-unchecked-value="0" id="loyaltyAutoEnroll" name="loyaltyAutoEnroll" ' +
__e( loyaltyAutoEnroll ? 'checked' : '' ) +
' />\r\n                <label for="loyaltyAutoEnroll" class="control-label">Auto Enroll Loyalty</label>\r\n            </div>\r\n            <table class="loyalty-rate-table">\r\n                <thead>\r\n                    <tr>\r\n                        <th>\r\n                            <div class="col-md-12">Loyalty Name</div>\r\n                        </th>\r\n                        <th>\r\n                            <div class="col-md-12">Department or Item</div>\r\n                        </th>\r\n                        <th>\r\n                            <div class="col-md-12">Points Awarded per Dollar</div>\r\n                        </th>\r\n                        <th>\r\n                            <div class="col-md-12">Dollars Redeemable per 100 Points</div>\r\n                        </th>\r\n                        <th>\r\n                            <div class="col-md-12">Effective Rate</div>\r\n                        </th>\r\n                        <th></th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody class="loyalty-rate-list">\r\n\r\n                </tbody>\r\n            </table>\r\n            <hr />\r\n            <div class="form-group">\r\n                <div class="btn-group form-group col-md-12">\r\n                    <button type="button" class="btn add-loyalty-rate btn-primary form-control">Add Loyalty Rate</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/loyalty/manage_rate.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            <input class="form-control rateLabel" type="text" value="' +
__e(label) +
'" name="label"/>\r\n        </div>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            <input  class="form-control valueLabel" type="text" value="' +
__e(valueLabel) +
'" name="valueLabel"/>\r\n        </div>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            <input  class="form-control pointsPerDollar" type="text" value="' +
__e(pointsPerDollar) +
'" name="pointsPerDollar"/>\r\n        </div>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            <input  class="form-control dollarsPerPoint" type="text" value="' +
__e(dollarsPerPoint) +
'" name="dollarsPerPoint"/>\r\n        </div>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12 effectiveRate">\r\n            ' +
__e(dollarsPerPoint*pointsPerDollar) +
'%\r\n        </div>\r\n    </div>\r\n</td>\r\n<td class="text-center">\r\n    <div class="btn btn-destroy btn-sm">\r\n      <i class="fa fa-trash-o removeRate" aria-hidden="true"></i>\r\n    </div>\r\n</td>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/employees/employee_settings_required_fields.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += 'Required Fields';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/quickbooks/quickbooks_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="col-md-12 settings-header">\r\n            <div class="settings-title">Accounting Settings</div>\r\n            <div class="settings-subtitle">Here you can export your sales data and import the data into your quickbooks accounting software.</div>\r\n        </div>\r\n        <div class="col-md-12">\r\n            <div class="settings-section-header">\r\n                <div class="settings-section-title">\r\n                    QuickBooks Export\r\n                </div>\r\n            </div>\r\n            <div class="form-group left-column">\r\n                <label for="from-date" class="col-md-12 control-label">From Date</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="" name="from_date" id="from-date" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">To Date</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="" name="to_date" id="to-date" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="quickbooks-terminal" class="col-md-12 control-label">Country</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, 1))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="sales_revenue_payments" name="sales_revenue_payments" style="margin-top: 5px;" />\r\n                    <label for="sales_revenue_payments" class="control-label">Sales - Revenue / Payments</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="sales_cogs_inventory" name="sales_cogs_inventory" style="margin-top: 5px;" />\r\n                    <label for="sales_cogs_inventory" class="control-label">Sales - COGS / Inventory</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="manual_ar_adjustments" name="manual_ar_adjustments" style="margin-top: 5px;" />\r\n                    <label for="manual_ar_adjustments" class="control-label">Manual A/R Adjustments</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="receivings" name="receivings" style="margin-top: 5px;" />\r\n                    <label for="receivings" class="control-label">Receivings</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Include Cash Logs</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <button>Export Data</button>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-12">\r\n            <div class="panel-group">\r\n                <div class="panel panel-default">\r\n                    <div class="panel-heading">\r\n                        <h4 class="panel-title">\r\n                            Quickbooks Account Settings\r\n                        </h4>\r\n                    </div>\r\n                    <div class="panel-body">\r\n                        <table id="minimum-charges-table">\r\n                            <thead>\r\n                                <tr>\r\n                                    <th class="label">Misc</th>\r\n                                    <th>Account <span class="req">*</span></th>\r\n                                    <th>Class</th>\r\n                                    <th colspan="5"></th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                            <tr>\r\n                                <td>Cash Drawer Variance</td>\r\n                                <td><input class="account" name="cash_variance[account]" value="" /></td>\r\n                                <td><input class="class" name="cash_variance[class]" value="" /></td>\r\n                                <td colspan="5">\r\n                                    If the cash drawer is off, the difference will be applied to this account (Usually a Quickbooks \'Expense\' account).\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Accounts Receivable Payments </td>\r\n                                <td><input class="account" name="categories[' +
__e(md5('Account Payments')) +
'][income]" value="" /></td>\r\n                                <td><input class="class" name="categories[' +
__e(md5('Account Payments')) +
'][income_class]" value="" /></td>\r\n                                <td colspan="5">\r\n                                    Payments by members to pay off their accounts (Quickbooks \'Other Current Asset\' account).\r\n                                    Should be the same account as \'Member Account\',\'Customer Credit\' and \'Invoice Charge\' payments below.\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Accounts Receivable Adjustments</td>\r\n                                <td><input class="account" name="receivable_adjustments[account]" value="" /></td>\r\n                                <td><input class="class" name="receivable_adjustments[class]" value="" /></td>\r\n                                <td colspan="5">\r\n                                    Manual adjustments made to member accounts.\r\n                                    Funds will move from this account into \'Accounts Receivable Payments\' (above).\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Accounts Payable (Suppliers)</td>\r\n                                <td><input class="account" name="accounts_payable[account]" value="" /></td>\r\n                                <td><input class="class" name="accounts_payable[class]" value="" /></td>\r\n                                <td colspan="5">\r\n                                    Handles payments made to suppliers when receiving inventory (Quickbooks \'Accounts Payable\' account).\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Rain Checks Issued (Debit)</td>\r\n                                <td><input class="account" name="rain_checks_issued_debit[account]" value="" /></td>\r\n                                <td><input class="class" name="rain_checks_issued_debit[class]" value="" /></td>\r\n                                <td colspan="5">\r\n                                    Account to debit when a rain check is issued\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Rain Checks Issued (Credit)</td>\r\n                                <td><input class="account" name="rain_checks_issued_credit[account]" value="" /></td>\r\n                                <td><input class="class" name="rain_checks_issued_credit[class]" value="" /></td>\r\n                                <td colspan="5">\r\n                                    Account to credit when a rain check is issued\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Gift Card Adjustments (Debit)</td>\r\n                                <td><input class="account" name="gift_card_adjustments_debit[account]" value="" /></td>\r\n                                <td><input class="class" name="gift_card_adjustments_debit[class]" value="" /></td>\r\n                                <td colspan="5">\r\n                                    Account to debit when the value of a gift card is edited manually\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Gift Card Adjustments (Credit)</td>\r\n                                <td><input class="account" name="gift_card_adjustments_credit[account]" value="" /></td>\r\n                                <td><input class="class" name="gift_card_adjustments_credit[class]" value="" /></td>\r\n                                <td colspan="5">\r\n                                    Account to credit when the value of a gift card is edited manually, usually the same as the Gift Card payment account (Liability/Payable)\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Partial Refunds</td>\r\n                                <td><input class="account" name="refunds[account]" value="" /></td>\r\n                                <td><input class="class" name="refunds[class]" value="" /></td>\r\n                                <td colspan="5">\r\n                                    Usually an \'Expense\' account to offset sales income\r\n                                </td>\r\n                            </tr>\r\n                            </tbody>\r\n                            <thead>\r\n                            <tr>\r\n                                <th>Payments</th>\r\n                                <th>Account <span class="req">*</span></th>\r\n                                <th>Class</th>\r\n                                <th colspan="5"></th>\r\n                            </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                                ';
 var payment_types = [];
                                payment_types.push({'name' : 'Cash', 'key' : 'cash'});
                                payment_types.push({'name' : 'Check', 'key' : 'check'});
                                payment_types.push({'name' : 'Credit Card', 'key' : 'credit_card'});
                                payment_types.push({'name' : 'Credit Card - VISA', 'key' : 'credit_card_visa'});
                                payment_types.push({'name' : 'Credit Card - Discover', 'key' : 'credit_card_discover'});
                                payment_types.push({'name' : 'Credit Card - MasterCard', 'key' : 'credit_card_mastercard'});
                                payment_types.push({'name' : 'Credit Card - Amex', 'key' : 'credit_card_amex'});
                                payment_types.push({'name' : 'Credit Card - Diners', 'key' : 'credit_card_diners'});
                                payment_types.push({'name' : 'Credit Card - JCB', 'key' : 'credit_card_jcb'});
                                payment_types.push({'name' : 'Member Account (A/R)', 'key' : 'member_account'});
                                payment_types.push({'name' : 'Customer Credit (A/R)', 'key' : 'customer_credit'});
                                payment_types.push({'name' : 'Invoice Charge (A/R)', 'key' : 'invoice_charge'});
                                payment_types.push({'name' : 'Tournament Winnings (A/R)', 'key' : 'tournament_winnings'});
                                payment_types.push({'name' : 'Gift Cards', 'key' : 'gift_card'});
                                payment_types.push({'name' : 'Gift Card Discounts', 'key' : 'gift_card_discount'});
                                payment_types.push({'name' : 'Punch Cards', 'key' : 'punch_card'});
                                payment_types.push({'name' : 'Rain Checks', 'key' : 'rain_check'});
                                payment_types.push({'name' : 'Coupons', 'key' : 'coupons'});
                                payment_types.push({'name' : 'Loyalty Points', 'key' : 'loyalty_points'});
                                payment_types.push({'name' : 'Tips', 'key' : 'tips'});

                                for(var i in payment_types)
                                { ;
__p += '\r\n                                    <tr>\r\n                                        <td>\r\n                                            ' +
__e(payment_types[i].name) +
'\r\n                                        </td>\r\n                                        <td><input class="account" name="payments[' +
__e(payment_types[i].key) +
'][account]" value="" /></td>\r\n                                        <td><input class="class" name="payments[' +
__e(payment_types[i].key) +
'][class]" value="" /></td>\r\n                                        <td colspan="5">\r\n\r\n                                        </td>\r\n                                    </tr>\r\n                                ';
 } ;
__p += '\r\n                            </tbody>\r\n                            <thead>\r\n                            <tr>\r\n                                <th>Custom Payments</th>\r\n                                <th>Account <span class="req">*</span></th>\r\n                                <th>Class</th>\r\n                                <th colspan="5">&nbsp;</th>\r\n                            </tr>\r\n                            </thead>\r\n\r\n                            <thead>\r\n                            <tr>\r\n                                <th>Taxes</th>\r\n                                <th>Account <span class="req">*</span></th>\r\n                                <th>Class</th>\r\n                                <th>Vendor</th>\r\n                                <th colspan="4"></th>\r\n                            </tr>\r\n                            </thead>\r\n                        </table>\r\n                        <div class="form-group">\r\n                            <button>Save</button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/custom_payment.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <input class=\'form-control customPaymentType\' type="text" value="' +
__e(label) +
'" name="customPaymentType[]"/>\r\n</td>\r\n<td class="text-center">\r\n    <i class="fa fa-trash-o removeCustomPayment" aria-hidden="true"></i>\r\n</td>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/edit_terminal_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\r\n        <i class="fa fa-question-o pull-right header-icon"></i>\r\n        <h4 class="modal-title">\r\n            Edit Terminal\r\n        </h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <div class="modal-content">\r\n            <div class="col-md-5 terminal-settings">\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Terminal Name</label>\r\n                    <div class="col-md-12">\r\n                        <input class=\'form-control\' type="text" name="label" value="' +
__e(label) +
'" name="" />\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Quickbutton Tab</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(quickbuttonTab == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="quickbuttonTab" autocomplete="off" value="1" ' +
__e(quickbuttonTab == 1 ? 'checked' : '') +
'> 1\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(quickbuttonTab == 2 ? 'active' : '') +
'">\r\n                                <input type="radio" name="quickbuttonTab" autocomplete="off" value="2" ' +
__e(quickbuttonTab == 2 ? 'checked' : '') +
'> 2\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(quickbuttonTab == 3 ? 'active' : '') +
'">\r\n                                <input type="radio" name="quickbuttonTab" autocomplete="off" value="3" ' +
__e(quickbuttonTab == 3 ? 'checked' : '') +
'> 3\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Track Cash in Cash Drawer</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(UseRegisterLog == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="UseRegisterLog" autocomplete="off" value="1" ' +
__e(UseRegisterLog == 1 ? 'checked' : '') +
'> Yes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(UseRegisterLog == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="UseRegisterLog" autocomplete="off" value="0" ' +
__e(UseRegisterLog == 0 ? 'checked' : '') +
'> No\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(UseRegisterLog == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="UseRegisterLog" autocomplete="off" value=null ' +
__e(UseRegisterLog == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Multiple Cash Drawers</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(multiCashDrawers == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="multiCashDrawers" autocomplete="off" value="1" ' +
__e(multiCashDrawers == 1 ? 'checked' : '') +
'> Yes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(multiCashDrawers == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="multiCashDrawers" autocomplete="off" value="0" ' +
__e(multiCashDrawers == 0 ? 'checked' : '') +
'> No\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(multiCashDrawers == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="multiCashDrawers" autocomplete="off" value="null" ' +
__e(multiCashDrawers == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Automatically Print Receipts</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(autoPrintReceipts == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoPrintReceipts" autocomplete="off" value="1" ' +
__e(autoPrintReceipts == 1 ? 'checked' : '') +
'> Yes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(autoPrintReceipts == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoPrintReceipts" autocomplete="off" value="0" ' +
__e(autoPrintReceipts == 0 ? 'checked' : '') +
'> No\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(autoPrintReceipts == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoPrintReceipts" autocomplete="off" value="null" ' +
__e(autoPrintReceipts == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Use Web Print</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(webprnt == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="webprnt" autocomplete="off" value="1" ' +
__e(webprnt == 1 ? 'checked' : '') +
'> Yes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(webprnt == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="webprnt" autocomplete="off" value="0" ' +
__e(webprnt == 0 ? 'checked' : '') +
'> No\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(webprnt == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="webprnt" autocomplete="off" value="null" ' +
__e(webprnt == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Force HTTPS</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(Https == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="Https" autocomplete="off" value="1" ' +
__e(Https == 1 ? 'checked' : '') +
'> Yes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(Https == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="Https" autocomplete="off" value="0" ' +
__e(Https == 0 ? 'checked' : '') +
'> No\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(Https == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="Https" autocomplete="off" value="null" ' +
__e(Https == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Use Cash Register</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(cashRegister == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="cashRegister" autocomplete="off" value="1" ' +
__e(cashRegister == 1 ? 'checked' : '') +
'> Yes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(cashRegister == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="cashRegister" autocomplete="off" value="0" ' +
__e(cashRegister == 0 ? 'checked' : '') +
'> No\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(cashRegister == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="cashRegister" autocomplete="off" value="null" ' +
__e(cashRegister == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Print Tip Line</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(printTipLine == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="printTipLine" autocomplete="off" value="1" ' +
__e(printTipLine == 1 ? 'checked' : '') +
'> Yes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(printTipLine == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="printTipLine" autocomplete="off" value="0" ' +
__e(printTipLine == 0 ? 'checked' : '') +
'> No\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(printTipLine == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="printTipLine" autocomplete="off" value="null" ' +
__e(printTipLine == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Signature Slip Count</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group four-buttons" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(signatureSlipCount == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="signatureSlipCount" autocomplete="off" value="0" ' +
__e(signatureSlipCount == 0 ? 'checked' : '') +
'> 0\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(signatureSlipCount == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="signatureSlipCount" autocomplete="off" value="1" ' +
__e(signatureSlipCount == 1 ? 'checked' : '') +
'> 1\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(signatureSlipCount == 2 ? 'active' : '') +
'">\r\n                                <input type="radio" name="signatureSlipCount" autocomplete="off" value="2" ' +
__e(signatureSlipCount == 2 ? 'checked' : '') +
'> 2\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(signatureSlipCount == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="signatureSlipCount" autocomplete="off" value="null" ' +
__e(signatureSlipCount == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Credit Card Sales Receipt Print Count</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group four-buttons" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(creditCardReceiptCount == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="creditCardReceiptCount" autocomplete="off" value="0" ' +
__e(creditCardReceiptCount == 0 ? 'checked' : '') +
'> 0\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(creditCardReceiptCount == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="creditCardReceiptCount" autocomplete="off" value="1" ' +
__e(creditCardReceiptCount == 1 ? 'checked' : '') +
'> 1\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(creditCardReceiptCount == 2 ? 'active' : '') +
'">\r\n                                <input type="radio" name="creditCardReceiptCount" autocomplete="off" value="2" ' +
__e(creditCardReceiptCount == 2 ? 'checked' : '') +
'> 2\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(creditCardReceiptCount == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="creditCardReceiptCount" autocomplete="off" value="null" ' +
__e(creditCardReceiptCount == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Non Credit Card Sales Receipt Print Count</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group four-buttons" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(nonCreditCardReceiptCount == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="nonCreditCardReceiptCount" autocomplete="off" value="0" ' +
__e(nonCreditCardReceiptCount == 0 ? 'checked' : '') +
'> 0\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(nonCreditCardReceiptCount == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="nonCreditCardReceiptCount" autocomplete="off" value="1" ' +
__e(nonCreditCardReceiptCount == 1 ? 'checked' : '') +
'> 1\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(nonCreditCardReceiptCount == 2 ? 'active' : '') +
'">\r\n                                <input type="radio" name="nonCreditCardReceiptCount" autocomplete="off" value="2" ' +
__e(nonCreditCardReceiptCount == 2 ? 'checked' : '') +
'> 2\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(nonCreditCardReceiptCount == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="nonCreditCardReceiptCount" autocomplete="off" value="null" ' +
__e(nonCreditCardReceiptCount == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Auto Email Receipt To Customer</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(autoEmailReceipt == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoEmailReceipt" autocomplete="off" value="1" ' +
__e(autoEmailReceipt == 1 ? 'checked' : '') +
'> Yes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(autoEmailReceipt == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoEmailReceipt" autocomplete="off" value="0" ' +
__e(autoEmailReceipt == 0 ? 'checked' : '') +
'> No\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(autoEmailReceipt == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoEmailReceipt" autocomplete="off" value="null" ' +
__e(autoEmailReceipt == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Do Not Print Receipt if Emailed</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(autoEmailNoPrint == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoEmailNoPrint" autocomplete="off" value="1" ' +
__e(autoEmailNoPrint == 1 ? 'checked' : '') +
'> Yes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(autoEmailNoPrint == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoEmailNoPrint" autocomplete="off" value="0" ' +
__e(autoEmailNoPrint == 0 ? 'checked' : '') +
'> No\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(autoEmailNoPrint == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoEmailNoPrint" autocomplete="off" value="null" ' +
__e(autoEmailNoPrint == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Require On-Screen Signature for Member Payments</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-tertiary  ' +
__e(requireSignatureMemberPayments == 1 ? 'active' : '') +
'">\r\n                                <input type="radio" name="requireSignatureMemberPayments" autocomplete="off" value="1" ' +
__e(requireSignatureMemberPayments == 1 ? 'checked' : '') +
'> Yes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(requireSignatureMemberPayments == 0 ? 'active' : '') +
'">\r\n                                <input type="radio" name="requireSignatureMemberPayments" autocomplete="off" value="0" ' +
__e(requireSignatureMemberPayments == 0 ? 'checked' : '') +
'> No\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-tertiary ' +
__e(requireSignatureMemberPayments == null ? 'active' : '') +
'">\r\n                                <input type="radio" name="requireSignatureMemberPayments" autocomplete="off" value="null" ' +
__e(requireSignatureMemberPayments == null ? 'checked' : '') +
'> Default\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="afterSaleLoad" class="col-md-12 control-label">Online Sales Terminal</label>\r\n                    <div class="col-md-12">\r\n                        ' +
((__t = (_.dropdown({"null":"Default","0":"Tee Sheet","1":"Sales"}, {"name":"afterSaleLoad", "id":"afterSaleLoad", "class":"form-control"}, afterSaleLoad))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-7">\r\n                <div class="panel-group settings-panel terminal-printer-panel">\r\n                    <div class="panel panel-default">\r\n                        <div class="panel-heading">\r\n                            <h4 class="panel-title pull-left">Terminal Printers</h4>\r\n\r\n                            <div class="pull-right form-inline marg-top-3">\r\n                                <div class="btn-group form-group">\r\n                                    <button type="button" class="btn manage-printers btn-primary btn-sm new">Manage Printers</button>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class="clear"></div>\r\n\r\n                        </div>\r\n                        <div class="panel-body">\r\n                            <table class="terminal-printer-group-list">\r\n\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                ';
 if (App.data.course.get('use_mercury_emv')) { ;
__p += '\r\n                <div class="panel-group settings-panel">\r\n                    <div class="panel panel-default">\r\n                        <div class="panel-heading">\r\n                            <h4 class="panel-title pull-left">Vantiv EMV Credit Card Device</h4>\r\n\r\n                            <div class="pull-right form-inline marg-top-3">\r\n                                <div class="btn-group form-group">\r\n                                    <button type="button" class="btn add_custom_payment_type btn-primary new btn-sm">Add</button>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class="clear"></div>\r\n\r\n                        </div>\r\n                        <div class="panel-body">\r\n                            No Device Added\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                ';
 } ;
__p += '\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class="modal-footer">\r\n        <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\r\n        <button class="btn btn-primary pull-right" id="save-changes">Save Changes</button>\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/food_and_beverage.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="settings-header row">\r\n          <div class="col-md-12">\r\n            <div class="settings-title">Food & Beverage</div>\r\n            <div class="settings-subtitle">These settings are used in the Food and Beverage application, and are all the settings that pertain to the sales in the module.</div>\r\n          </div>\r\n        </div>\r\n    </div>\r\n    <div class="row">\r\n        <div class="col-md-4">\r\n            <div class="form-group">\r\n                <label for="defaultRegisterLogOpen" class="col-md-12 control-label">Default Register Open Amount</label>\r\n                <div class="col-md-12">\r\n                  <div class="input-group">\r\n                    <div class="input-group-addon">$</div>\r\n                    <input class="form-control" type="text" value="' +
__e(defaultRegisterLogOpen) +
'" name="defaultRegisterLogOpen" id="defaultRegisterLogOpen" />\r\n                  </div>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="autoGratuity" class="col-md-12 control-label">Auto Gratuity</label>\r\n                <div class="col-md-12">\r\n                  <div class="input-group">\r\n                    <input class="form-control" type="text" value="' +
__e(autoGratuity) +
'" name="autoGratuity" id="autoGratuity" />\r\n                    <div class="input-group-addon">%</div>\r\n                  </div>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="autoGratuityThreshold" class="col-md-12 control-label">Auto Gratuity Head Count Threshold</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(autoGratuityThreshold) +
'" name="autoGratuityThreshold" id="autoGratuityThreshold" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="serviceFeeActive" name="serviceFeeActive" style="margin-top: 5px;" ' +
__e(serviceFeeActive?'checked':'') +
'/>\r\n                    <label for="serviceFeeActive" class="control-label">Make Auto Gratuity a Service Fee</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="serviceFeeTax1Name" class="col-md-12 control-label">Service Fee Tax Rate 1 Name</label>\r\n                <div class="col-md-6">\r\n                    <input class="form-control" type="text" value="' +
__e(serviceFeeTax1Name) +
'" name="serviceFeeTax1Name" id="serviceFeeTax1Name" />\r\n                </div>\r\n                <div class="col-md-6">\r\n                  <div class="input-group">\r\n                    <input class="form-control" type="text" value="' +
__e(serviceFeeTax1Rate) +
'" name="serviceFeeTax1Rate" id="serviceFeeTax1Rate" />\r\n                    <div class="input-group-addon">%</div>\r\n                  </div>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="serviceFeeTax2Name" class="col-md-12 control-label">Service Fee Tax Rate 2 Name</label>\r\n                <div class="col-md-6">\r\n                    <input class="form-control" type="text" value="' +
__e(serviceFeeTax2Name) +
'" name="serviceFeeTax2Name" id="serviceFeeTax2Name" />\r\n                </div>\r\n                <div class="col-md-6">\r\n                  <div class="input-group">\r\n                    <input class="form-control" type="text" value="' +
__e(serviceFeeTax2Rate) +
'" name="serviceFeeTax2Rate" id="serviceFeeTax2Rate" />\r\n                    <div class="input-group-addon">%</div>\r\n                  </div>\r\n                </div>\r\n            </div>\r\n          </div>\r\n          <div class="col-md-4">\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="foodBevSortBySeat" name="foodBevSortBySeat" style="margin-top: 5px;" ' +
__e(foodBevSortBySeat?'checked':'') +
'/>\r\n                    <label for="foodBevSortBySeat" class="control-label">Sort Items by Seat</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="requireGuestCount" name="requireGuestCount" style="margin-top: 5px;" ' +
__e(requireGuestCount?'checked':'') +
'/>\r\n                    <label for="requireGuestCount" class="control-label">Require Guest Count</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="useCourseFiring" name="useCourseFiring" style="margin-top: 5px;" ' +
__e(useCourseFiring?'checked':'') +
'/>\r\n                    <label for="useCourseFiring" class="control-label">Use Course Firing</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="courseFiringIncludeItems" name="courseFiringIncludeItems" style="margin-top: 5px;" ' +
__e(courseFiringIncludeItems?'checked':'') +
'/>\r\n                    <label for="courseFiringIncludeItems" class="control-label">Include Items When the Course is Fired</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="trackCash" name="trackCash" style="margin-top: 5px;" ' +
__e(trackCash?'checked':'') +
'/>\r\n                    <label for="trackCash" class="control-label">Track Cash in Register</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="allowEmployeeRegisterLogBypass" name="allowEmployeeRegisterLogBypass" style="margin-top: 5px;" ' +
__e(allowEmployeeRegisterLogBypass?'checked':'') +
'/>\r\n                    <label for="allowEmployeeRegisterLogBypass" class="control-label">Allow Employees to Bypass Register Log Closing</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="blindClose" name="blindClose" style="margin-top: 5px;" ' +
__e(blindClose?'checked':'') +
'/>\r\n                    <label for="blindClose" class="control-label">Blind Register Close</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="requireEmployeePin" name="requireEmployeePin" style="margin-top: 5px;" ' +
__e(requireEmployeePin?'checked':'') +
'/>\r\n                    <label for="requireEmployeePin" class="control-label">Require Employee Login</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="separateCourses" name="separateCourses" style="margin-top: 5px;" ' +
__e(separateCourses?'checkout':'') +
'/>\r\n                    <label for="separateCourses" class="control-label">Separate Courses in Reports</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="deductTips" name="deductTips" style="margin-top: 5px;" ' +
__e(deductTips?'checked':'') +
'/>\r\n                    <label for="deductTips" class="control-label">Auto Deduct Tips from Cash in Z-Out Report</label>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/manage_printer.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <div class="form-group">\r\n        <label for="printer" class="control-label">Printer Name</label>\r\n        <input class="form-control printer" type="text" value="' +
__e(label) +
'" name="printer[]"/>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group col-md-12">\r\n        <label for="ip_address" class="control-label">IP Address</label>\r\n        <input  class="form-control ip_address" type="text" value="' +
__e(ipAddress) +
'" name="ip_address[]"/>\r\n    </div>\r\n</td>\r\n<td class="text-center">\r\n    <div class="btn btn-destroy marg-top-25">\r\n      <i class="fa fa-trash-o removePrinter" aria-hidden="true"></i>\r\n    </div>\r\n</td>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/manage_printer_group.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <div class="form-group">\r\n        <label class="control-label">Group Name</label>\r\n        <input class="form-control printerGroup" type="text" value="' +
__e(label) +
'" name="printerGroup[]" />\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group col-md-12">\r\n        <label class="control-label">Choose Default Printer</label>\r\n        ' +
((__t = (_.dropdown(App.data.course.get('receipt_printers').getDropdownOptions(), {"name":"default_printer_id", "class":"form-control receiptPrinterDropdown"}, defaultPrinterId))) == null ? '' : __t) +
'\r\n    </div>\r\n</td>\r\n<td class="text-center">\r\n    <div class="btn btn-destroy marg-top-25">\r\n      <i class="fa fa-trash-o removePrinterGroup" aria-hidden="true"></i>\r\n    </div>\r\n</td>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/manage_printers_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 var course_obj = App.data.course; ;
__p += '\r\n';
 var printerOptions = course_obj.get('receipt_printers').getDropdownOptions(); ;
__p += '\r\n<div class="modal-content bootstrap-fu">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\r\n        <i class="fa fa-question-o pull-right header-icon"></i>\r\n        <h4 class="modal-title">\r\n            Manage Printers\r\n        </h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <div class="modal-content">\r\n          <br/>\r\n          <div class="col-md-12">\r\n            <div class="col-md-6">\r\n                <h4 class="panel-title pull-left" style="margin-bottom: 10px;">Printers</h4>\r\n                <div class="form-inline marg-top-3 pull-right">\r\n                    <div class="btn-group form-group">\r\n                        <button type="button" class="btn add-printer btn-primary new btn-sm" style="margin-top: -10px">Add</button>\r\n                    </div>\r\n                </div>\r\n\r\n                <table class="printer-list">\r\n\r\n                </table>\r\n            </div>\r\n\r\n            <div class="col-md-6">\r\n                <div class="printer-row row">\r\n                  <div class="form-group col-md-6">\r\n                    <label class="control-label">Group Name</label>\r\n                    <input class="form-control" type="text" value="Barcode Label Printer" name="" disabled/>\r\n                  </div>\r\n                  <div class="form-group col-md-6">\r\n                    <label class="control-label">Choose Default Printer </label>\r\n                    ' +
((__t = (_.dropdown(printerOptions, {"name":"barcodeLabelPrinter", "class":"form-control receiptPrinterDropdown"}, course_obj.get('webprnt_label_ip')))) == null ? '' : __t) +
'\r\n                  </div>\r\n                </div>\r\n\r\n                <div class="printer-row row">\r\n                    <div class="form-group col-md-6">\r\n                        <label class="control-label">Group Name</label>\r\n                        <input class="form-control" type="text" value="Default Sales Printer" name="" disabled/>\r\n                    </div>\r\n                    <div class="form-group col-md-6">\r\n                        <label class="control-label">Choose Default Printer </label>\r\n                        <div>\r\n                            ' +
((__t = (_.dropdown(printerOptions, {"name":"defaultSalesPrinter", "class":"form-control receiptPrinterDropdown"}, course_obj.get('webprnt_ip')))) == null ? '' : __t) +
'\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class="printer-row row">\r\n                    <div class="form-group col-md-6">\r\n                        <label class="control-label">Group Name</label>\r\n                        <input class="form-control"s type="text" value="Default Kitchen Printer" name="" disabled/>\r\n                    </div>\r\n                    <div class="form-group col-md-6">\r\n                        <label class="control-label">Choose Default Printer </label>\r\n                        ' +
((__t = (_.dropdown(printerOptions, {"name":"defaultKitchenPrinter", "class":"form-control receiptPrinterDropdown"}, course_obj.get('default_kitchen_printer')))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n                <hr />\r\n                <h4 class="panel-title pull-left" style="margin-bottom:10px;">Printer Groups</h4>\r\n                <div class="form-inline marg-top-3">\r\n                    <div class="btn-group form-group pull-right">\r\n                        <button type="button" class="btn add-printer-group btn-primary btn-sm" style="margin-top: -10px">Add</button>\r\n                    </div>\r\n                </div>\r\n                <table class="printer-group-list">\r\n\r\n                </table>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="customer-info clearfix"></div>\r\n</div>\r\n<div class="modal-footer">\r\n    <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\r\n    <button class="btn btn-primary pull-right" id="save-changes">Save Changes</button>\r\n</div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/manage_terminal.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            ' +
__e(label) +
'\r\n        </div>\r\n    </div>\r\n</td>\r\n<td class="text-center">\r\n    <div class="btn-group">\r\n        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\r\n            <i class="fa fa-sort-down" aria-hidden="true"></i>\r\n        </button>\r\n        <ul class="dropdown-menu">\r\n            <li class="edit-terminal"><a><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a></li>\r\n            <li class="delete-terminal"><a><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a></li>\r\n        </ul>\r\n    </div>\r\n</td>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/point_of_sale.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="settings-header row">\r\n            <div class="col-md-12">\r\n              <div class="settings-title">Point of Sale</div>\r\n              <div class="settings-subtitle">These settings are used in the Point of Sale application, and are all the settings that pertain to sales with this module.</div>\r\n            </div>\r\n        </div>\r\n      </div>\r\n    <div class="row">\r\n        <div class="col-md-4">\r\n            <div class="form-group">\r\n                <label for="defaultRegisterLogOpen" class="col-md-12 control-label">Default Register Open Amount</label>\r\n                <div class="col-md-12">\r\n                  <div class="input-group">\r\n                    <div class="input-group-addon">$</div>\r\n                    <input class="form-control" type="text" value="' +
__e(defaultRegisterLogOpen) +
'" name="defaultRegisterLogOpen" id="defaultRegisterLogOpen" />\r\n                  </div>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="afterSaleLoad" class="col-md-12 control-label">After Sale Load</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown({"0":"Tee Sheet","1":"Sales"}, {"name":"afterSaleLoad", "id":"afterSaleLoad", "class":"form-control"}, afterSaleLoad))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="hideTaxable" name="hideTaxable" style="margin-top: 5px;" ' +
__e(hideTaxable?'checked':'') +
'/>\r\n                    <label for="hideTaxable" class="control-label">Hide Taxable Option</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="trackCash" name="trackCash" style="margin-top: 5px;" ' +
__e(trackCash?'checked':'') +
'/>\r\n                    <label for="trackCash" class="control-label">Track Cash In Register</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="allowEmployeeRegisterLogBypass" name="allowEmployeeRegisterLogBypass" style="margin-top: 5px;" ' +
__e(allowEmployeeRegisterLogBypass?'checked':'') +
'/>\r\n                    <label for="allowEmployeeRegisterLogBypass" class="control-label">Allow Employees to Bypass Register Log Closing</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="blindClose" name="blindClose" style="margin-top: 5px;" ' +
__e(blindClose?'checked':'') +
'/>\r\n                    <label for="blindClose" class="control-label">Blind Register Close</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="requireEmployeePin" name="requireEmployeePin" style="margin-top: 5px;" ' +
__e(requireEmployeePin?'checked':'') +
'/>\r\n                    <label for="requireEmployeePin" class="control-label">Require Employee Pin Every Sale</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="useTerminals" name="useTerminals" style="margin-top: 5px;" ' +
__e(useTerminals?'checked':'') +
'/>\r\n                    <label for="useTerminals" class="control-label">Use Terminals</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="requireCustomerOnSale" name="requireCustomerOnSale" style="margin-top: 5px;" ' +
__e(requireCustomerOnSale?'checked':'') +
'/>\r\n                    <label for="requireCustomerOnSale" class="control-label">Require Customer on All Sales</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="useEtsGiftcards" name="useEtsGiftcards" style="margin-top: 5px;" ' +
__e(useEtsGiftcards?'checked':'') +
'/>\r\n                    <label for="useEtsGiftcards" class="control-label">Use ETS Gift Cards</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="creditCardFee" class="col-md-12 control-label">Credit Card Fee</label>\r\n                <div class="col-md-12">\r\n                  <div class="input-group">\r\n                    <div class="input-group-addon">$</div>\r\n                    <input class="form-control" type="text" value="' +
__e(creditCardFee) +
'" name="creditCardFee" id="creditCardFee" />\r\n                  </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-4">\r\n            <div class="form-group">\r\n                <label for="memberBalanceNickname" class="col-md-12 control-label">Member Balance Label</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(memberBalanceNickname) +
'" name="memberBalanceNickname" id="memberBalanceNickname" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="customerCreditNickname" class="col-md-12 control-label">Customer Credit Label</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="' +
__e(customerCreditNickname) +
'" name="customerCreditNickname" id="customerCreditNickname" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="quickbooks-terminal" class="col-md-12 control-label">Credit Department</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, creditDepartmentName))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="quickbooks-terminal" class="col-md-12 control-label">Credit Category</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown({"0":"N/A","1":"Red","2":"Yellow","3":"Green"}, {"name":"terminal_id", "id":"quickbooks-terminal", "class":"form-control"}, creditDepartmentName))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="requireSignatureMemberPayments" name="requireSignatureMemberPayments" style="margin-top: 5px;" ' +
__e(requireSignatureMemberPayments?'checked':'') +
'/>\r\n                    <label for="requireSignatureMemberPayments" class="control-label">Require on-screen signature for member payments</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="creditLimitToSubtotal" name="creditLimitToSubtotal" style="margin-top: 5px;" ' +
__e(creditLimitToSubtotal?'checked':'') +
'/>\r\n                    <label for="creditLimitToSubtotal" class="control-label">Limit Customer Credit to Subtotal Only</label>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-4">\r\n            <div class="panel-group settings-panel custom-payments-panel-group">\r\n                <div class="panel panel-default">\r\n                    <div class="panel-heading">\r\n                        <h4 class="panel-title pull-left">\r\n                            Custom Payment Types\r\n                        </h4>\r\n                        <div class="pull-right form-inline marg-top-3">\r\n                            <div class="btn-group form-group">\r\n                                <button type="button" class="btn add_custom_payment_type btn-primary btn-sm panel-button">Add</button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class="clear"></div>\r\n\r\n                    </div>\r\n                    <div class="panel-body">\r\n                        <table class="custom-payment-type-table">\r\n                            <thead>\r\n                                <tr>\r\n                                    <th>PAYMENT TYPE NAME</th>\r\n                                    <th class="text-center"></th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody class="custom-payment-type-list">\r\n\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/printing_and_terminals.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="row settings-header">\r\n            <div class="col-md-12">\r\n              <div class="settings-title">Printing and Terminals</div>\r\n              <div class="settings-subtitle">These settings are used in the Point of Sale application, and are all the settings that pertain to printing receipts and hardware.</div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="row">\r\n        <div class="col-md-4">\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="printAfterSale" name="printAfterSale" style="margin-top: 5px;" ' +
__e(printAfterSale?'checked':'') +
'/>\r\n                    <label for="printAfterSale" class="control-label">Automatic Receipt Printing</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="printCreditCardReceipt" name="printCreditCardReceipt" style="margin-top: 5px;" ' +
__e(printCreditCardReceipt?'checked':'') +
'/>\r\n                    <label for="printCreditCardReceipt" class="control-label">Print Credit Card Receipts</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="printSalesReceipt" name="printSalesReceipt" style="margin-top: 5px;" ' +
__e(printSalesReceipt?'checked':'') +
'/>\r\n                    <label for="printSalesReceipt" class="control-label">Print Sales Receipts</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="printTipLine" name="printTipLine" style="margin-top: 5px;" ' +
__e(printTipLine?'checked':'') +
'/>\r\n                    <label for="printTipLine" class="control-label">Print Tip Line</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="receiptPrintAccountBalance" name="receiptPrintAccountBalance" style="margin-top: 5px;" ' +
__e(receiptPrintAccountBalance?'checked':'') +
'/>\r\n                    <label for="receiptPrintAccountBalance" class="control-label">Print Customer Account Balance</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="printMinimumsOnReceipt" name="printMinimumsOnReceipt" style="margin-top: 5px;" ' +
__e(printMinimumsOnReceipt?'checked':'') +
'/>\r\n                    <label for="printMinimumsOnReceipt" class="control-label">Print Current Minimums on Receipt</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="printTwoReceipts" name="printTwoReceipts" style="margin-top: 5px;" ' +
__e(printTwoReceipts?'checked':'') +
'/>\r\n                    <label for="printTwoReceipts" class="control-label">Print 2 Receipts on Credit Card Sales</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="printTwoSignatureSlips" name="printTwoSignatureSlips" style="margin-top: 5px;" ' +
__e(printTwoSignatureSlips?'checked':'') +
'/>\r\n                    <label for="printTwoSignatureSlips" class="control-label">Print 2 Signature Slips</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="printTwoReceiptsOther" name="printTwoReceiptsOther" style="margin-top: 5px;" ' +
__e(printTwoReceiptsOther?'checked':'') +
'/>\r\n                    <label for="printTwoReceiptsOther" class="control-label">Print 2 Receipts on all Sales</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="hideEmployeeLastNameReceipt" name="hideEmployeeLastNameReceipt" style="margin-top: 5px;" ' +
__e(hideEmployeeLastNameReceipt?'cheched':'') +
'/>\r\n                    <label for="hideEmployeeLastNameReceipt" class="control-label">Hide Employee Last Name on Receipt</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="cashDrawerOnCash" name="cashDrawerOnCash" style="margin-top: 5px;" ' +
__e(cashDrawerOnCash?'checked':'') +
'/>\r\n                    <label for="cashDrawerOnCash" class="control-label">Open Cash Drawer on Cash Payment</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="cashDrawerOnSale" name="cashDrawerOnSale" style="margin-top: 5px;" ' +
__e(cashDrawerOnSale?'checked':'') +
'/>\r\n                    <label for="cashDrawerOnSale" class="control-label">Open Cash Drawer on Every Sale</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="autoEmailReceipt" name="autoEmailReceipt" style="margin-top: 5px;" ' +
__e(autoEmailReceipt?'checked':'') +
'/>\r\n                    <label for="autoEmailReceipt" class="control-label">Auto-Email Receipt to Customer</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="autoEmailNoPrint" name="autoEmailNoPrint" style="margin-top: 5px;" ' +
__e(autoEmailNoPrint?'checked':'') +
'/>\r\n                    <label for="autoEmailNoPrint" class="control-label">Do Not Print Receipt if Receipt is Emailed</label>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n        <div class="col-md-4">\r\n            <div class="form-group">\r\n                <div class="btn-group form-group col-md-12">\r\n                    <button type="button" class="btn manage-printers btn-primary new settings-corner-button form-control">Manage Printers and Printer Groups</button>\r\n                </div>\r\n            </div>\r\n            <br />&nbsp;<br />\r\n            <div class="form-group">\r\n                <div class="btn-group form-group col-md-12">\r\n                    <button type="button" class="btn manage-receipt-agreements btn-primary new settings-corner-button form-control">Manage Receipt Agreements</button>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="onlineInvoiceTerminalId" class="col-md-12 control-label">Green Fee Receipt Agreement</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown(App.data.course.get('receipt_agreements').getDropdownOptions(), {"name":"onlineInvoiceTerminalId", "id":"onlineInvoiceTerminalId", "class":"form-control"}, onlineInvoiceTerminalId))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="onlineInvoiceTerminalId" class="col-md-12 control-label">Cart Fee Receipt Agreement</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown(App.data.course.get('receipt_agreements').getDropdownOptions(), {"name":"onlineInvoiceTerminalId", "id":"onlineInvoiceTerminalId", "class":"form-control"}, onlineInvoiceTerminalId))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="onlinePurchaseTerminalId" class="col-md-12 control-label">Online Sales Terminal</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown(App.data.course.get('terminals').getDropdownData(true), {"name":"onlinePurchaseTerminalId", "class":"form-control terminal-dropdown"}, onlinePurchaseTerminalId))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="onlineInvoiceTerminalId" class="col-md-12 control-label">Online Invoice Payment Terminal</label>\r\n                <div class="col-md-12">\r\n                    ' +
((__t = (_.dropdown(App.data.course.get('terminals').getDropdownData(true), {"name":"onlineInvoiceTerminalId", "class":"form-control terminal-dropdown"}, onlineInvoiceTerminalId))) == null ? '' : __t) +
'\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-4">\r\n            <div class="panel-group settings-panel terminal-panel-group">\r\n                <div class="panel panel-default">\r\n                    <div class="panel-heading">\r\n                        <h4 class="panel-title pull-left">\r\n                            Terminals\r\n                        </h4>\r\n                        <div class="pull-right form-inline marg-top-3">\r\n                            <div class="btn-group form-group">\r\n                                <button type="button" class="btn add-terminal btn-primary btn-sm new button">Add</button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class="clear"></div>\r\n\r\n                    </div>\r\n                    <div class="panel-body">\r\n                        <table class="terminal-list">\r\n\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/return_reason.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <input class=\'form-control returnReason\' type="text" value="' +
__e(label) +
'" name="returnReason[]"/>\r\n</td>\r\n<td class="text-center">\r\n    <i class="fa fa-trash-o removeReturnReason" aria-hidden="true"></i>\r\n</td>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/taxes_and_returns.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="row settings-header">\r\n            <div class="col-md-12">\r\n              <div class="settings-title">Taxes and Returns</div>\r\n              <div class="settings-subtitle">These settings are used in the Point of Sale application, and are all the settings that pertain to taxes and returns.</div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class="row">\r\n        <div class="col-md-4">\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">Tax Rate 1 Name</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="" name="to_date" id="to-date" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">Tax Rate 2 Name</label>\r\n                <div class="col-md-12">\r\n                    <input class="form-control" type="text" value="" name="to_date" id="to-date" />\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <div class="checkbox-control-container">\r\n                    <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                    <label for="include_cash_logs" class="control-label">Unit Price Includes Tax</label>\r\n                </div>\r\n            </div>\r\n            <div class="form-group">\r\n                <label for="to-date" class="col-md-12 control-label">Return Policy</label>\r\n                <div class="col-md-12">\r\n                    <textarea class="form-control" type="text" value="" name="to_date" id="to-date"></textarea>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-4">\r\n            <div class="panel-group col-md-12 return-reasons-panel-group">\r\n                <div class="panel settings-panel panel-default">\r\n                    <div class="panel-heading">\r\n                        <h4 class="panel-title pull-left">\r\n                            Return Reasons\r\n                        </h4>\r\n                        <div class="pull-right form-inline marg-top-3">\r\n                            <div class="btn-group form-group">\r\n                                <button type="button" class="btn add_return_reason btn-primary btn-sm">Add</button>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class="clear"></div>\r\n\r\n                    </div>\r\n                    <div class="panel-body">\r\n                        <table class="return-reason-table">\r\n                            <thead>\r\n                            <tr>\r\n                                <th>RETURN REASON</th>\r\n                                <th class="text-center"></th>\r\n                            </tr>\r\n                            </thead>\r\n                            <tbody class="return-reason-list">\r\n\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/sales/terminal_printer_group.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <div class="form-group">\r\n        <label class="col-md-12 control-label">Group Name</label>\r\n        <div class="col-md-12">\r\n            <div class="control-label">' +
__e(label) +
'</div>\r\n        </div>\r\n    </div>\r\n</td>\r\n<td>\r\n    <div class="form-group">\r\n        <label class="col-md-12 control-label">Choose Printer</label>\r\n        <div class="col-md-12">\r\n            ' +
((__t = (_.dropdown(App.data.course.get('receipt_printers').getDropdownOptions(), {"name":"default_printer_id", "class":"form-control receiptPrinterDropdown"}, defaultPrinterId))) == null ? '' : __t) +
'\r\n        </div>\r\n    </div>\r\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/basic_settings.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="col-md-4 tee-sheet-details">\r\n    <div class="panel-group terminal-printer-panel">\r\n        <div class="panel panel-default">\r\n            <div class="panel-heading">\r\n                <h4 class="panel-title">\r\n                    Tee Sheet Details\r\n                </h4>\r\n            </div>\r\n            <div class="panel-body">\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Tee Sheet Name</label>\r\n                    <div class="col-md-12">\r\n                        <input class=\'form-control\' type="text" name="label" value="' +
__e(title) +
'" name="" />\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <div class="checkbox-control-container">\r\n                        <input type="checkbox" value="1" data-unchecked-value="0" id="hideTaxable" name="hideTaxable" style="margin-top: 5px;" />\r\n                        <label for="hideTaxable" class="control-label">Default Tee Sheet</label>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Tee Sheet Color</label>\r\n                    <div class="col-md-6">\r\n                        <div class="colorPicker input-group colorpicker-component">\r\n                            <input class="form-control color" type="text" value="' +
__e(color) +
'" name="color"  />\r\n                            <span class="input-group-addon"><i></i></span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Number of Holes</label>\r\n                    <div class="col-md-12">\r\n                        <div class="btn-group" data-toggle="buttons">\r\n                            <label class="terminal-settings-btn btn btn-primary  ' +
__e(holes == 9 ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoEmailNoPrint" autocomplete="off" value="1" ' +
__e(holes == 9 ? 'checked' : '') +
'> 9 holes\r\n                            </label>\r\n                            <label class="terminal-settings-btn btn btn-primary ' +
__e(holes == 18 ? 'active' : '') +
'">\r\n                                <input type="radio" name="autoEmailNoPrint" autocomplete="off" value="0" ' +
__e(holes == 18 ? 'checked' : '') +
'> 18 holes\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class="form-group">\r\n                    <label for="afterSaleLoad" class="col-md-12 control-label">Increments</label>\r\n                    <div class="col-md-12">\r\n                        ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.increments, {"name":"afterSaleLoad", "id":"afterSaleLoad", "class":"form-control"}, increment))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="afterSaleLoad" class="col-md-12 control-label">Time to Finish 9 holes</label>\r\n                    <div class="col-md-12">\r\n                        ' +
((__t = (_.dropdown(SettingsRestModelDropdownOptions.times_to_finish, {"name":"afterSaleLoad", "id":"afterSaleLoad", "class":"form-control"}, frontnine))) == null ? '' : __t) +
'\r\n                    </div>\r\n                </div>\r\n\r\n                <div class="form-group">\r\n                    <div class="checkbox-control-container">\r\n                        <input type="checkbox" value="1" data-unchecked-value="0" id="hideTaxable" name="hideTaxable" style="margin-top: 5px;" ' +
__e(book_sunrise_sunset?'checked':'') +
'/>\r\n                        <label for="hideTaxable" class="control-label">Use Sunrise/Sunset for Open & Close</label>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Course Opens how many minutes after sunrise</label>\r\n                    <div class="col-md-12">\r\n                        <input placeholder="in minutes" class=\'form-control\' type="text" name="label" value="' +
__e(sunrise_offset) +
'" name="" />\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label class="col-md-12 control-label">Course Closes how many minutes before sunset</label>\r\n                    <div class="col-md-12">\r\n                        <input placeholder="in minutes" class=\'form-control\' type="text" name="label" value="' +
__e(sunset_offset) +
'" name="" />\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class="col-md-8 seasons">\r\n    <div class="seasons">\r\n\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/edit_price_class_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\r\n        <i class="fa fa-question-o pull-right header-icon"></i>\r\n        <h4 class="modal-title">\r\n            Edit Player Type\r\n        </h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <div class="modal-content">\r\n            <div class="col-md-6">\r\n                <div class="form-group">\r\n                    <label for="name" class="col-md-12 control-label">Player Type Name</label>\r\n                    <div class="col-md-12">\r\n                        <input class="form-control" type="text" value="' +
__e(name) +
'" name="name" id="name" />\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="color" class="col-md-12 control-label">Color Identifier</label>\r\n                    <div class="col-md-6">\r\n                        <div id="colorPicker" class="input-group colorpicker-component">\r\n                            <input class="form-control" type="text" value="' +
__e(color) +
'" name="color"  id="color" />\r\n                            <span class="input-group-addon"><i></i></span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <div class="checkbox-control-container col-md-12">\r\n                        <input type="checkbox" value="1" data-unchecked-value="0" id="cart" name="cart" style="margin-top: 5px;" ' +
__e(cart?'checked':'') +
'/>\r\n                        <label for="cart" class="control-label">Player Can Rent Carts</label>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group">\r\n                    <label for="greenGlCode" class="col-md-3 control-label">Green GL Code</label>\r\n                    <label for="cartGlCode" class="col-md-3 control-label">Cart GL Code</label>\r\n                    <label class="col-md-6 control-label">&nbsp</label>\r\n                </div>\r\n                <div class="form-group">\r\n                    <div class="col-md-3">\r\n                        <input class="form-control" type="text" value="' +
__e(greenGlCode) +
'" name="greenGlCode" id="greenGlCode" />\r\n                    </div>\r\n                    <div class="col-md-3">\r\n                        <input class="form-control" type="text" value="' +
__e(cartGlCode) +
'" name="cartGlCode" id="cartGlCode" />\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class="modal-footer">\r\n        <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\r\n        <button class="btn btn-primary pull-right" id="save-changes">Save Changes</button>\r\n    </div>\r\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/edit_tee_sheet_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="modal-content bootstrap-fu">\r\n    <div class="modal-header">\r\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>\r\n        <i class="fa fa-question-o pull-right header-icon"></i>\r\n        <h4 class="modal-title">\r\n            ' +
__e( title != '' ? title : "New Tee Sheet" ) +
'\r\n        </h4>\r\n    </div>\r\n    <div class="modal-body container-fluid">\r\n        <div class="tee-sheet-tabs-wrapper col-md-12">\r\n            <div class="tee-sheet-tabs"></div>\r\n        </div>\r\n        <div class="tee-sheet-info"></div>\r\n    </div>\r\n    <div class="clearfix"></div>\r\n</div>\r\n<div class="modal-footer">\r\n    <button class="btn btn-muted pull-left" data-dismiss="modal">Close</button>\r\n    <button class="btn btn-primary pull-right" id="save-changes">Save Changes</button>\r\n</div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/manage_seasons.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="panel-group terminal-printer-panel">\r\n    <div class="panel panel-default">\r\n        <div class="panel-heading">\r\n            <h4 class="panel-title">\r\n                Seasons\r\n            </h4>\r\n        </div>\r\n        <div class="panel-body">\r\n            <div class="col-md-4 season-list">(Default) Everyday Season</div>\r\n            <div class="col-md-8 season-settings">Select or add season to see the season details</div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class="panel-group terminal-printer-panel">\r\n    <div class="panel panel-default">\r\n        <div class="panel-heading">\r\n            <h4 class="panel-title">\r\n                Season Pricing by Player Type\r\n            </h4>\r\n        </div>\r\n        <div class="panel-body">\r\n            <div class="col-md-4 season-price-class-list">(Default) Regular</div>\r\n            <div class="col-md-8 season-timeframe-list">No Season Selected</div>\r\n        </div>\r\n    </div>\r\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/marketing.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += 'Hello';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/player_types.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="col-md-6 settings-header">\r\n            <div class="settings-title">Player Types</div>\r\n        </div>\r\n        <div class="col-md-6 settings-header">\r\n            <div class="pull-right form-inline marg-top-3">\r\n                <div class="btn-group form-group">\r\n                    <button type=\'button\' class="btn btn-primary new add-price-class settings-corner-button">Add New</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-12">\r\n            <div class="col-md-12">\r\n                <table class="player-type-table">\r\n                    <thead>\r\n                    <tr>\r\n                        <th>Player Type Name</th>\r\n                        <th>Actions</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody class="player-type-list">\r\n                        <tr>\r\n                            <td colspan="2" class="no-tee-sheets-message">\r\n                                The are currently no player types created.\r\n                            </td>\r\n                        </tr>\r\n                    </tbody>\r\n                </table>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/price_class_row.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            ' +
__e(name) +
'\r\n        </div>\r\n    </div>\r\n</td>\r\n<td class="text-center">\r\n    <div class="btn-group">\r\n        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\r\n            <i class="fa fa-sort-down" aria-hidden="true"></i>\r\n        </button>\r\n        <ul class="dropdown-menu">\r\n            <li class="edit-price-class"><a><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a></li>\r\n            <li class="delete-price-class"><a><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a></li>\r\n        </ul>\r\n    </div>\r\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/season_item_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="col-md-12 seasonListItem" data-season-id="' +
__e(id) +
'">\r\n    ' +
__e( isDefault ? '(Default)' : '' ) +
' ' +
__e( name ) +
'\r\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/season_settings_list_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += 'Hello there';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/tabs.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="col-md-2 col-sm-3 col-xs-3 selected-tab">\r\n    <div class="tab-selector tab-selector-basic-settings">\r\n        Basic Settings\r\n    </div>\r\n</div>\r\n<div class="col-md-2 col-sm-3 col-xs-3">\r\n    <div class="tab-selector tab-selector-marketing">\r\n        Marketing\r\n    </div>\r\n</div>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/taxes_and_receipts.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<form>\r\n    <div class="clearfix">\r\n        <div class="col-md-12 settings-header">\r\n            <div class="settings-title">Taxes and Receipts</div>\r\n        </div>\r\n        <div class="col-md-12">\r\n            <div class="panel-group col-md-6">\r\n                <div class="panel panel-default">\r\n                    <div class="panel-heading">\r\n                        <h4 class="panel-title">\r\n                            Taxes\r\n                        </h4>\r\n                    </div>\r\n                    <div class="panel-body">\r\n                        <div class="form-group col-md-6">\r\n                            <label for="to-date" class="control-label">Green Fee Department</label>\r\n                            <div>\r\n                                <input class="form-control" type="text" value="" name="to_date" id="to-date" />\r\n                            </div>\r\n                        </div>\r\n                        <div class="form-group col-md-6">\r\n                            <label for="to-date" class="control-label">Tax Rate</label>\r\n                            <div>\r\n                                <input class="form-control" type="text" value="" name="to_date" id="to-date" />\r\n                            </div>\r\n                        </div>\r\n                        <div class="form-group col-md-12">\r\n                            <div class="checkbox-control-container">\r\n                                <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                                <label for="include_cash_logs" class="control-label">Green Fee Tax Included</label>\r\n                            </div>\r\n                        </div>\r\n                        <div class="form-group col-md-6">\r\n                            <label for="to-date" class="control-label">Cart Department</label>\r\n                            <div>\r\n                                <input class="form-control" type="text" value="" name="to_date" id="to-date" />\r\n                            </div>\r\n                        </div>\r\n                        <div class="form-group col-md-6">\r\n                            <label for="to-date" class="control-label">Tax Rate</label>\r\n                            <div>\r\n                                <input class="form-control" type="text" value="" name="to_date" id="to-date" />\r\n                            </div>\r\n                        </div>\r\n                        <div class="form-group col-md-12">\r\n                            <div class="checkbox-control-container">\r\n                                <input type="checkbox" value="1" data-unchecked-value="0" id="include_cash_logs" name="include_cash_logs" style="margin-top: 5px;" />\r\n                                <label for="include_cash_logs" class="control-label">Cart Fee Tax Included</label>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="panel-group col-md-6">\r\n                <div class="panel panel-default">\r\n                    <div class="panel-heading">\r\n                        <h4 class="panel-title">\r\n                            Receipt Agreements\r\n                            <div class="pull-right form-inline marg-top-10">\r\n                                <div class="btn-group form-group">\r\n                                    <button type="button" class="btn manage_receipt_agreements btn-primary new">Manage</button>\r\n                                </div>\r\n                            </div>\r\n                        </h4>\r\n                    </div>\r\n                    <div class="panel-body">\r\n                        <div class="form-group">\r\n                            <label for="to-date" class="col-md-12 control-label">Green Fee Agreement</label>\r\n                            <div class="col-md-12">\r\n                                <div class="col-md-12">\r\n                                    ' +
((__t = (_.dropdown(App.data.item_receipt_content.get_dropdown_data(), {"name":"country", "id":"country", "class":"form-control"}, country))) == null ? '' : __t) +
'\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class="form-group">\r\n                            <label for="to-date" class="col-md-12 control-label">Green Fee Agreement</label>\r\n                            <div class="col-md-12">\r\n                                <div class="col-md-12">\r\n                                    ' +
((__t = (_.dropdown(App.data.item_receipt_content.get_dropdown_data(), {"name":"country", "id":"country", "class":"form-control"}, country))) == null ? '' : __t) +
'\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/tee_sheet_list_view.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<form>\r\n    <div class="clearfix settings-header">\r\n        <div class="col-md-6">\r\n            <div class="settings-title">Tee Sheets</div>\r\n        </div>\r\n        <div class="col-md-6">\r\n            <div class="pull-right">\r\n                <div class="btn-group form-group">\r\n                    <button type=\'button\' class="btn btn-primary add-tee-sheet marg-top-10">Add New</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class="col-md-12">\r\n            <div class="col-md-4">\r\n                <div class="panel-group terminal-printer-panel">\r\n                    <div class="panel panel-default">\r\n                        <div class="panel-heading">\r\n                            <h4 class="panel-title">\r\n                                General Settings\r\n                            </h4>\r\n                        </div>\r\n                        <div class="panel-body">\r\n                            <div class="form-group">\r\n                                <div class="checkbox-control-container">\r\n                                    <input type="checkbox" value="1" data-unchecked-value="0" id="hideTaxable" name="hideTaxable" style="margin-top: 5px;" ' +
__e(false?'checked':'') +
'/>\r\n                                    <label for="hideTaxable" class="control-label">Hide Back 9</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class="form-group">\r\n                                <div class="checkbox-control-container">\r\n                                    <input type="checkbox" value="1" data-unchecked-value="0" id="hideTaxable" name="hideTaxable" style="margin-top: 5px;" ' +
__e(false?'checked':'') +
'/>\r\n                                    <label for="hideTaxable" class="control-label">Default Fees With Customers to Player 1</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class="form-group">\r\n                                <div class="checkbox-control-container">\r\n                                    <input type="checkbox" value="1" data-unchecked-value="0" id="hideTaxable" name="hideTaxable" style="margin-top: 5px;" ' +
__e(false?'checked':'') +
'/>\r\n                                    <label for="hideTaxable" class="control-label">Limit gree/cart fee dropdown to fees customer is allowed to use</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class="form-group">\r\n                                <div class="checkbox-control-container">\r\n                                    <input type="checkbox" value="1" data-unchecked-value="0" id="hideTaxable" name="hideTaxable" style="margin-top: 5px;" ' +
__e(false?'checked':'') +
'/>\r\n                                    <label for="hideTaxable" class="control-label">Auto Check Split Tee Times</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class="form-group">\r\n                                <div class="checkbox-control-container">\r\n                                    <input type="checkbox" value="1" data-unchecked-value="0" id="hideTaxable" name="hideTaxable" style="margin-top: 5px;" ' +
__e(false?'checked':'') +
'/>\r\n                                    <label for="hideTaxable" class="control-label">Stack Tee Sheets</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class="form-group">\r\n                                <div class="checkbox-control-container">\r\n                                    <input type="checkbox" value="1" data-unchecked-value="0" id="hideTaxable" name="hideTaxable" style="margin-top: 5px;" ' +
__e(false?'checked':'') +
'/>\r\n                                    <label for="hideTaxable" class="control-label">Stack Horizontally</label>\r\n                                </div>\r\n                            </div>\r\n                            <div class="form-group">\r\n                                <label class="col-md-12 control-label">Tee\'d Off Color</label>\r\n                                <div class="col-md-6">\r\n                                    <div class="colorPicker input-group colorpicker-component">\r\n                                        <input class="form-control color" type="text" value="' +
__e(teedOffColor) +
'" name="teedOffColor"  />\r\n                                        <span class="input-group-addon"><i></i></span>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class="form-group">\r\n                                <label class="col-md-12 control-label">Completed Color</label>\r\n                                <div class="col-md-6">\r\n                                    <div class="colorPicker input-group colorpicker-component">\r\n                                        <input class="form-control color" type="text" value="' +
__e(teeTimeCompletedColor) +
'" name="teeTimeCompletedColor"  />\r\n                                        <span class="input-group-addon"><i></i></span>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-8">\r\n                <table class="tee-sheet-table">\r\n                    <thead>\r\n                        <tr>\r\n                            <th>Tee Sheet Name</th>\r\n                            <th>Actions</th>\r\n                        </tr>\r\n                    </thead>\r\n                    <tbody class="tee-sheet-list">\r\n                        ';
 if (false) { ;
__p += '\r\n\r\n                        ';
 } else { ;
__p += '\r\n                            <tr>\r\n                                <td colspan="2" class="no-tee-sheets-message">\r\n                                    The are currently no tee sheets created.\r\n                                </td>\r\n                            </tr>\r\n                        ';
 } ;
__p += '\r\n                    </tbody>\r\n                </table>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["settings/templates/tee_sheets/tee_sheet_row.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<td>\r\n    <div class="form-group">\r\n        <div class="col-md-12">\r\n            ' +
__e(title) +
'\r\n        </div>\r\n    </div>\r\n</td>\r\n<td class="text-center">\r\n    <div class="btn-group">\r\n        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\r\n            <i class="fa fa-sort-down" aria-hidden="true"></i>\r\n        </button>\r\n        <ul class="dropdown-menu">\r\n            <li class="edit-tee-sheet"><a><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a></li>\r\n            <li class="delete-tee-sheet"><a><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a></li>\r\n        </ul>\r\n    </div>\r\n</td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/behaviors/modal/modal_container.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="modalbox" class="modal fade" role="dialog" >\n    <div class="modal-dialog ' +
((__t = ( size )) == null ? '' : __t) +
'">\n        <div class=\'modal-content edit-customer-window\'>\n            <div class=\'modal-body container-fluid\' >\n            </div>\n        </div>\n    </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/edit/ArrayEdit.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 _.each(editableArray,function(value,key){{
;
__p += '\n        <div class="nr-filter-container form-inline">\n                <a class="btn btn-default nr-filter-delete" id="removeItem"><i class="fa fa-times"></i></a>\n                <input class="form-control nr-filter-value" data-index="';
 print(key) ;
__p += '" value="';
 print(value) ;
__p += '"/>\n        </div>\n';
 }}) ;
__p += '\n\n        <button id="addItem">Add</button>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/edit/ColumnCollection.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '\n<table id="column-table" class="table" style="background-color: white;">\n    <thead>\n        <tr>\n            <td>id</td>\n            <td>Report Id</td>\n            <td>Label</td>\n            <td>Column</td>\n            <td>Aggregate</td>\n            <td>Filterable</td>\n            <td>Visible</td>\n            <td>Selectable</td>\n            <td>Sortable</td>\n            <td>Locked</td>\n            <td>Delete</td>\n        </tr>\n    </thead>\n    <tbody>\n\n    </tbody>\n    <tfoot>\n        <td colspan="10"><button id="addColumn" class="btn">Create</button></td>\n    </tfoot>\n</table>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/edit/ColumnItem.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<td>' +
((__t = ( id )) == null ? '' : __t) +
'</td>\n<td>' +
((__t = ( report_id )) == null ? '' : __t) +
'</td>\n<td><input  data-bind="value:label" placeholder="Label" /></td>\n<td>\n    <select class="form-control" id="column_dropdown"  data-live-search="true" data-tags="true" style="min-width: 150px"></select>\n</td>\n<td><select data-bind="options:[null,\'sum\',\'count\',\'max\',\'day\',\'month\',\'year\'],value:aggregate"><span data-bind="text:aggregate"></span></select></td>\n<td><input type="checkbox" data-bind="checked:filterable" /></td>\n<td><input type="checkbox" data-bind="checked:visible" /></td>\n<td><input type="checkbox" data-bind="checked:selectable" /></td>\n<td><input type="checkbox" data-bind="checked:sortable" /></td>\n<td><input type="checkbox" data-bind="checked:locked" /></td>\n<td><button id="deleteColumn" data-bind="value:id" class="btn">Delete</button></td>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/edit/Report.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="form-group advanced_field" >\n    <label class="col-sm-2 control-label">Base Table</label>\n    <div class="col-sm-10">\n        <input class="form-control" placeholder="Base Table" data-bind="value:base" >\n    </div>\n</div>\n<div class="form-group advanced_field" >\n    <label class="col-sm-2 control-label">course_id</label>\n    <div class="col-sm-10">\n        <input class="form-control" placeholder="Course Id" data-bind="value:course_id" >\n    </div>\n</div>\n<div class="form-group">\n    <label class="col-sm-2 control-label">Description</label>\n    <div class="col-sm-10">\n        <input class="form-control" placeholder="Description" data-bind="value:description" >\n    </div>\n</div>\n<div class="form-group">\n    <label class="col-sm-2 control-label">Title</label>\n    <div class="col-sm-10">\n        <input class="form-control" placeholder="Title" data-bind="value:title" >\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/edit/ReportLayout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div style="background-color: white">\n    <div class="row text-center">\n        <button id="saveReport" class="button">SAVEME!</button>\n    </div>\n    <form class="form-horizontal">\n        <div id="r-report"></div>\n        <div class="form-group">\n            <label class="col-sm-2 control-label">Grouping</label>\n            <div class="col-sm-10">\n\n                <div id="r-grouping"></div>\n            </div>\n        </div>\n        <div class="form-group">\n            <label class="col-sm-2 control-label">Ordering</label>\n            <div class="col-sm-10">\n                <div id="r-ordering"></div>\n            </div>\n        </div>\n        <div class="form-group">\n            <label class="col-sm-2 control-label">Tags</label>\n            <div class="col-sm-10">\n                <div id="r-tags"></div>\n            </div>\n        </div>\n    </form>\n    <div class="row">\n        <label class="control-label col-sm-2 text-right">Dates</label>\n        <div class="col-sm-10" id="r-date-grouping"></div>\n    </div>\n    <div class="row">\n        <label class="col-sm-2 control-label text-right">Filters</label>\n        <div class="col-sm-10" id="r-filters"></div>\n    </div>\n</div>\n\n<div id="r-columns">\n\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/edit/ReportList.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<table class="table" style="background-color: white">\n    <thead>\n        <tr><td>Title</td><td>Description</td><td>&nbsp;</td></tr>\n    </thead>\n    <tbody id="reports">\n\n    </tbody>\n    <tr><td colspan="3"><button class="btn" id="createNewReport">Create</button></td></tr>\n</table>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/edit/ReportListItem.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<td>' +
((__t = ( title )) == null ? '' : __t) +
'</td>\n<td>' +
((__t = ( description )) == null ? '' : __t) +
'</td>\n<td>\n    <a href="#/report_edit/';
 print(id) ;
__p += '" class="btn btn-default" >Edit</a>\n    <a id="deleteReport" class="btn btn-default" >Delete</a>\n</td>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/edit/Tags.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<select multiple id="tags" class="form-control select input">\n        ';
 _.forEach(tags,function(tag){ ;
__p += '\n        <option selected>' +
((__t = ( tag )) == null ? '' : __t) +
'</option>\n        ';
 }) ;
__p += '\n</select>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/blank_report.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="r-navigation"></div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/chart.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="loading"> </div>\n<div id="chart-container" class="container-fluid" style="max-height: 400px">\n    <div class="row">\n        <div class="col-md-2" style="overflow:hidden; height:300px">\n            <div id="legend"></div>\n        </div>\n        <div class="col-md-6">\n            <canvas id="myChart" ></canvas>\n        </div>\n\n    </div>\n\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/chart_high.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="loading"> </div>\n<div id="chart-container" class="container-fluid" style="max-height: 400px">\n    <div class="row" style="position: relative">\n        <a id="chart-options" style="position:absolute;top:50px;left:80px;z-index:10">\n            <i class="fa fa-cog fa-3x"></i>\n        </a>\n        <div class="col-md-12 chart-wrapper">\n            <div class="chart-inner">\n                <div id="myChart" style="width:100%; height: 100%;"></div>\n            </div>\n        </div>\n    </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/modal_save.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content edit-customer-window">\n    <div class="modal-header">\n        Save Report\n    </div>\n    <div class="modal-body container-fluid" >\n        <span>';
 if(typeof(saving_for_report)!== "undefined") { ;
__p += ' The report needs to be saved first. ';
 };
__p += '</span>\n        <div id="r-details"></div>\n        <div class="form-group">\n            <label class="col-sm-2 control-label">Tags</label>\n            <div class="col-sm-10">\n                <div id="r-tags"></div>\n            </div>\n        </div>\n    </div>\n    <div class="modal-footer">\n        <button type="button" class="btn btn-primary save">Yes</button>\n        <button type="button" data-dismiss="modal" class="btn btn-default pull-left">No</button>\n    </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/report.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="row page-report-header background-color-gradient">\n    <div class="col-md-12">\n        <div class="row">\n            <div class="col-xs-12">\n                <h1 class="page-report-title">\n                    ' +
((__t = ( title )) == null ? '' : __t) +
'\n                    <span id="save">\n                        <i class="fa fa-star-o"></i> Save\n                    </span>\n                </h1>\n                <button id="toggle-chart" class="btn btn-default btn-sm" style="padding:10px; margin-left: 25px">\n                    Charts \n                    <span style="font-size:8px;vertical-align: super;">Beta</span>\n                </button>\n                <button id="send-to-marketing" class="btn btn-default btn-sm" style="padding:10px">\n                    Send to Marketing\n                </button>\n            </div>\n            <div class="date-group">\n                <div id="r-date-grouping"></div>\n            </div>\n        </div>\n        <div class="row">\n            <div class="col-xs-12">\n                <!-- <button class="btn btn-default" style="float:right; margin-right:100px;" id="chart-options"> Chart Options </button> -->\n                <div id="r-widget"></div>\n                <div id="r-chart-parameters"></div> \n            </div>      \n        </div>\n        <div class="row">\n            <div class="col-xs-12">\n                <div id="r-default-date-filter" class="page-filter"></div>\n                <div id="r-splitby" class="page-inline-input"></div>\n                <div id="r-filters" class="page-filter"></div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class="page-subnav">\n    <div id="r-columns" class="page-inline-input"></div>\n    <div class="btn-group">\n    </div>\n    <div class="dropdown" style="float:right;">\n\n        <select class="comparisons form-control">\n            <option selected>No Comparison</option>\n            <option id="last_week">Last Week</option>\n            <option id="last_month">Last Month</option>\n            <option id="last_year">Last Year</option>\n        </select>\n\n    </div>\n\n    <div class="dropdown" style="float:right;">\n        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">\n            Export\n            <span class="caret"></span>\n        </button>\n        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">\n            <li><a href="#" id="download-excel">Excel</a></li>\n            <li> <a href="#" id="download-pdf">Pdf</a></li>\n            <li> <a href="#" id="download-csv">CSV</a></li>\n        </ul>\n    </div>\n\n\n\n    <div id="r-table-search" class="pull-right input-container">\n        <span class="fa fa-search input-icon"></span>\n        <input class="search form-control" name="search" value="" placeholder="Search...">\n    </div>\n</div>\n\n<div class="page-data">\n    <div id="r-table"></div>\n    <div id="r-chart"></div>\n    <div id="r-summary"></div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/report.static.layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="page-report-header container">\n    <div class="row">\n        <div class="col-xs-12">\n            <h1 class="page-report-title">\n                ' +
((__t = ( title )) == null ? '' : __t) +
'\n            </h1>\n\n        </div>\n        <div class="date-group">\n            <div id="r-date-grouping"></div>\n        </div>\n    </div>\n    <div class="row">\n        <div class="col-xs-12">\n            <div id="r-chart-parameters"></div> \n        </div>      \n    </div>\n    <div class="row">\n        <div class="col-xs-12">\n            <div id="r-default-date-filter" class="page-filter"></div>\n            <div id="r-splitby" class="page-inline-input"></div>\n            <div id="r-filters" class="page-filter"></div>\n        </div>\n    </div>\n    <!--<div class="row">\n        <div class="col-xs-12">\n            <button id="data-refresh" class="btn btn-primary">Refresh Data</button>\n        </div>\n    </div>-->\n</div>\n\n<div class="page-subnav">\n    <div class="dropdown" style="float:right;">\n        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">\n            Export\n            <span class="caret"></span>\n        </button>\n        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">\n            <li><a href="#" id="download-excel">Excel</a></li>\n            <li> <a href="#" id="download-pdf">Pdf</a></li>\n            <li> <a href="#" id="download-csv">CSV</a></li>\n        </ul>\n    </div>\n</div>\n\n<div class="page-data">\n    <div id="r-table"></div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/splitby.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<select id="splitbyColumn"  data-bind="value:split_by" class="form-control">\n    <option value="">None</option>\n    ';
 _.each(visibleColumns,function(column,key){ ;
__p += '\n        <option value="' +
((__t = (column.get('completeName')  )) == null ? '' : __t) +
'" >\n            ';
 print(column.get("label") || column.get("column")); ;
__p += '\n        </option>\n    ';
 });
__p += '\n</select>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/static.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div>\n    Loading Data . . .\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/summary.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {

 if( summary_data != null && summary_data.length>0 && !loading){ ;
__p += '\n<table class="table">\n        <thead>\n            <tr>\n                <td>Column</td>\n                <td>Value</td>\n            </tr>\n        </thead>\n\n        ';
 _.each(summary_columns,function(column,key){ ;
__p += '\n            <tr>\n                <td>\n                    ';
 print(column.label || column.column) ;
__p += '\n                </td>\n                <td>\n                    ';
 print(summary_data[0][column.completeName]) ;
__p += '\n                </td>\n            </tr>\n\n        ';
 });
__p += '\n\n</table>\n';
  } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/table.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="controls"></div>\n<div style="min-height:200px;position:relative;" id="loading">\n    <table class="data_table display">\n        <thead>\n\n        </thead>\n        <tbody>\n        </tbody>\n    </table>\n\n</div>\n\n\n<div id="subview"></div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/table_compare.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="row">\n    <div class="col-lg-6 tableCompare">\n        <div class="dataTables_info" id="previous_daterange" style="\n            font-weight: bolder;\n            text-align: center;\n            width: 100%;\n        "></div>\n        <div  id="r-previous_table"></div>\n    </div>\n    <div class="col-lg-6 tableCompare">\n        <div class="dataTables_info" id="current_daterange"  style="\n            font-weight: bolder;\n            text-align: center;\n            width: 100%;\n        "></div>\n\n        <div  id="r-current_table"></div>\n    </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/table_mini.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div id="controls"></div>\n';
 if( data != null && visibleColumns != null){ ;
__p += '\n<table id="mini_table" class="display">\n    <thead>\n    <tr>\n        ';
 _.each(visibleColumns,function(column,key){ ;
__p += '\n        <th>';
 print(column.get("label") || column.get("column")); ;
__p += '</th>\n        ';
 });
__p += '\n\n        <th>';
 _.each(linkouts,function(linkout,key){ ;
__p += ' Links';
 });
__p += '</th>\n    </tr>\n    </thead>\n    <tbody>\n    ';
 _.each(data,function(row,row_key){ ;
__p += '\n    <tr>\n        ';
 _.each(visibleColumns,function(column,key){ ;
__p += '\n        <td class="clickable" data-column="' +
((__t = ( column.get('completeName') )) == null ? '' : __t) +
'" data-value="' +
((__t = ( row[column.get('completeName')] )) == null ? '' : __t) +
'">';
 print(row[column.get("completeName")] || "&nbsp;"); ;
__p += '</td>\n        ';
 });
__p += '\n        <td>\n            <div class="btn-group">\n            ';
 _.each(linkouts,function(link,key){ ;
__p += '\n                <button class="btn btn-default btn-sm linkout"  data-row="' +
((__t = ( row_key )) == null ? '' : __t) +
'" data-linkout="' +
((__t = ( key )) == null ? '' : __t) +
'">' +
((__t = ( link.label )) == null ? '' : __t) +
'</button>\n            ';
 });
__p += '\n            </div>\n        </td>\n    </tr>\n    ';
 });
__p += '\n    </tbody>\n    ';
 if(!_.isUndefined(arguments[0].totals)){ ;
__p += '\n        <tfoot>\n        <tr>\n            ';
 _.each(arguments[0].totals,function(value,key){ ;
__p += '\n            <td  >' +
((__t = ( value )) == null ? '' : __t) +
'</td>\n            ';
 });
__p += '\n            <td>&nbsp;</td>\n        </tr>\n        </tfoot>\n    ';
 } ;
__p += '\n</table>\n';
  } ;


}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/chart/edit_parameters.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="row">\n\n    <div class="col-md-4">\n        <div class=" selected_graph btn-group-vertical">\n\n            <button data-value="line" class="btn btn-default">Line</button>\n            <button data-value="bar" class="btn btn-default" >Bar</button>\n            <button data-value="pie" class="btn btn-default" >Pie Chart</button>\n            <!--<button data-value="map" class="btn btn-default" >Map</button>-->\n        </div>\n    </div>\n    <div class="col-md-8">\n        <div class="row">\n            <div class="col-md-6">\n                <h3 id="x-axis-label">X Axis</h3>\n                <select id="x-axis" data-bind="value:xAxis">\n                    ';
  _.each(possibleXColumns,function(value,key){ ;
__p += '\n                    <option value="' +
((__t = ( value.completeName )) == null ? '' : __t) +
'">' +
((__t = ( value.label || value.completeName )) == null ? '' : __t) +
'</option>\n                    ';
 }) ;
__p += '\n                </select>\n\n                <div class="date_modifiers">\n                    <label>\n                        Visual Width\n                    </label>\n                    <select data-bind="value:visualWidth" >\n                        <option value="day">Day</option>\n                        <option value="week">Week</option>\n                        <option value="month">Month</option>\n                        <option value="year">Year</option>\n                        <option value="range">Range</option>\n                    </select>\n                </div>\n\n                <div class="date_modifiers">\n                    <label>\n                        Visual Grouping\n                    </label>\n                    <select data-bind="value:visualGrouping,options:possibleGroupings" >\n                    </select>\n                </div>\n            </div>\n            <div class="col-md-6">\n\n                <div class="y_axis">\n\n                    <h3 id="y-axis-label">Y Axis</h3>\n                    <select id="y-axis" data-bind="value:yAxis">\n                        ';
 _.each(possibleYColumns,function(value,key){ ;
__p += '\n                        <option value="' +
((__t = ( value.completeName )) == null ? '' : __t) +
'">' +
((__t = ( value.label || value.completeName )) == null ? '' : __t) +
'</option>\n                        ';
 }) ;
__p += '\n                    </select>\n\n                    <div class="split_by">\n                        <label>\n                            Split By\n                        </label>\n                        <select id="y-axis" data-bind="value:splitBy">\n                            <option value="">None</option>\n                            ';
 _.each(possibleXColumns,function(value,key){ ;
__p += '\n                            <option value="' +
((__t = ( value.completeName )) == null ? '' : __t) +
'">' +
((__t = ( value.label || value.completeName )) == null ? '' : __t) +
'</option>\n                            ';
 }) ;
__p += '\n                        </select>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n\n    </div>\n\n\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/applied_filters/active_filter.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<a id="new-filter" data-type=\'' +
((__t = ( filter.type )) == null ? '' : __t) +
'\' data-value=\'' +
((__t = ( filter.value )) == null ? '' : __t) +
'\'>' +
((__t = ( filter.label )) == null ? '' : __t) +
'</a>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/applied_filters/item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '\n<button class="page-filter-button" data-toggle="dropdown">\n    <span class="filter-label" data-bind="text:filterLabel"></span>\n    <a class=\'delete-filter\' title="Remove filter">&times;</a>\n</button>\n<div class="dropdown-menu dropdown-filter" role="menu">\n    <div class="dropdown-filter-content">\n        <div id="r-filter" class="nr-filter-container form-inline"></div>\n    </div>\n    <div class="dropdown-filter-close">\n        <a class="nr-filter-delete" id="r-remove-filter">Remove Filter</i></a>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/applied_filters/layout.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div id="filters">\n\n</div>\n<div class="dropdown page-filter">\n    <button class="page-filter-add" id="add-filter"  data-toggle="dropdown">\n        + Add Filter\n        <span class="caret"></span>\n    </button>\n    <div class="dropdown-menu dropdown-filter" role="menu">\n        <div class="dropdown-filter-content" id="new-filter">\n\n        </div>\n    </div>\n</div>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/column_selection/collection.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<select class="selectpicker btn btn-link select-columns" multiple="multiple">\n\n</select>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/column_selection/item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p +=
((__t = ( label || column )) == null ? '' : __t);

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/filter/date_range.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '\n<div class="popover-content">\n    <div class="row tab tab-range">\n        <div class="col-sm-12">\n            <div class="input-daterange input-group">\n                ';
 if(typeof start_locked !== "undefined" && !start_locked){ ;
__p += '\n                <input type="text" class="input-small form-control" data-bind="value:start_value,events:[\'blur\']"/>\n                <span class="input-group-addon">to</span>\n                ';
 } ;
__p += '\n                <input type="text" class="input-small form-control" data-bind="value:end_value,events:[\'blur\']" />\n            </div>\n        </div>\n    </div>\n</div>\n\n<div id="datecontainer">\n\n</div>\n\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/filter/dropdown.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '\n\n<div class="input-group">\n        <div class="input-group-btn">\n                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-crosshairs" /> </button>\n                <ul class="dropdown-menu">\n                        <li data-value="custom"><a href="#"><i class="fa fa-crosshairs"></i> Custom Value</a></li>\n                        <li data-value="not_equal"><a href="#"><i class="fa fa-exclamation"></i>Not Equal</a></li>\n                        <li data-value="is_null"><a href="#"><i class="fa fa-star-o"></i>Does Not Exist</a></li>\n                        <li data-value="is_not_null"><a href="#"><i class="fa fa-star"></i>Exists</a></li>\n                        <li data-value="terminal"><a href="#"><i class="fa fa-desktop"></i>Terminal</a></li>\n                        <li data-value="user"><a href="#"><i class="fa fa-user"></i>Current User</a></li>\n                </ul>\n        </div>\n        <select\n                multiple="multiple"\n                class="form-control"\n                id="report-dropdown"\n                data-bind="value:value"\n                data-live-search="true"\n                data-tags="true"\n                style="min-width: 200px"\n        ></select>\n</div><!-- /input-group -->';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/filter/month.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<input class="form-control nr-filter-datepicker" data-provide="datepicker" data-bind="value:start_value" data-date-format=\'yyyy-mm-dd\' data-date-min-view-mode="months">';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/filter/number.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="input-group form-horizontal">\n    <div class="input-group-btn">\n        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-crosshairs" /> </button>\n        <ul class="dropdown-menu">\n            <li data-value="custom"><a href="#"><i class="fa fa-crosshairs"></i> Custom Value</a></li>\n            <li data-value="is_null"><a href="#"><i class="fa fa-star-o"></i>Does Not Exist</a></li>\n            <li data-value="is_not_null"><a href="#"><i class="fa fa-star"></i>Exists</a></li>\n            <li data-value="terminal"><a href="#"><i class="fa fa-desktop"></i>Terminal</a></li>\n            <li data-value="user"><a href="#"><i class="fa fa-user"></i>Current User</a></li>\n        </ul>\n    </div>\n    <select class="form-control nr-filter-operator" data-bind="value:operator,options:default_operators" id="operator_dropdown" style="height:34px; width:70px;"></select>\n    <span class="input-group-addon" style="border-left: 0; border-right: 0;"> </span>\n    <input class="form-control nr-filter-value" data-bind="value:value" type="number" style="height:34px;">\n\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/filter/text.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="input-group">\n    <div class="input-group-btn">\n        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-crosshairs" /> </button>\n        <ul class="dropdown-menu">\n            <li data-value="custom"><a href="#"><i class="fa fa-crosshairs"></i> Custom Value</a></li>\n            <li data-value="is_null"><a href="#"><i class="fa fa-star-o"></i>Does Not Exist</a></li>\n            <li data-value="is_not_null"><a href="#"><i class="fa fa-star"></i>Exists</a></li>\n            <li data-value="terminal"><a href="#"><i class="fa fa-desktop"></i>Terminal</a></li>\n            <li data-value="user"><a href="#"><i class="fa fa-user"></i>Current User</a></li>\n        </ul>\n    </div>\n    <input type="text" class="form-control" id="" style="height:34px" data-bind="value:value">\n</div><!-- /input-group -->';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/filter_selection/filterable_list.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<input class=\'form-control\' id="search_for_filter" placeholder="Find filter..." />\n<div class="filter_list_container">\n    <ul id="filter_list"></ul>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["reporting/templates/report/filter_selection/single_filter.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<a>' +
((__t = ( label || completeName )) == null ? '' : __t) +
'</a>';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["tip_transactions/TipSharing.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<div class="modal-content tip-transaction">\n    <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n        <h4 class="modal-title" style="text-align: center">Share Your Tips</h4>\n    </div>\n    <div class="modal-body">\n        <h3 class="tip-debited">Your Tips From Today: ';

            var entries = journal_entries.models;
            if(!entries)entries = [];
            var shared = new Decimal(0.00);
            for(var i=0;i < entries.length;i++){
            var entry = entries[i];
            if(entry.get('d_c') === 'credit')continue;
            ;
__p += '\n            $' +
__e(total) +
' ';
} ;
__p += ' </h3>\n        <ul class="journal-entries" style="list-style-type:none;">\n            ';

            for(var i=0;i < entries.length;i++){
            var entry = entries[i];
            if(entry.get('d_c') === 'debit')continue;
            if(!entry.get('employee_id') && entry.get('notes')==='Net Tips Payable'){
              entry.set('deleted',1);
              entry.set('amount',0);
              continue;
            }
            if(entry.get('deleted')*1)continue;
            ;
__p += '\n            <li id="tip-tr-emp-' +
__e(entry.get('employee_id')) +
'"\n                data-transaction-id="' +
__e(entry.get('transaction_id')) +
'"\n                data-employee-id="' +
__e(entry.get('employee_id')) +
'"\n                class="tipped-employee"\n                style="padding:5px;"><span class="input-group">\n                <span class="input-group-btn"><button class="btn btn-danger remove">×</button></span>\n                <label class="control-label form-control" style="margin-right:5px;">' +
__e(entry.get('notes').replace('Tips Payable: ','') ) +
'</label>\n                <span class="input-group-btn"><button type="button" class="btn btn-default " disabled>%</button></span><input style="width:75px" class="tip-percent form-control has-keypad" type="number" min="0" max="100" step="1" value="' +
__e(Math.round((entry.get('amount')/total)*100)) +
'">\n                <span class="input-group-btn"><button type="button" class="btn btn-default " disabled>$</button></span><input style="width:75px" class="tip-amount form-control has-keypad" type="number" min="0.00" step="0.01" value="' +
__e(entry.get('amount')) +
'"></span>\n                <input class="tip-comment form-control" type="text" name="comment" value="' +
__e(entry.get('comment')) +
'" placeholder="Add Comment&hellip;">\n            </li>\n            ';

            var amt = new Decimal(entry.get('amount'));
            shared = shared.plus(amt);
            }
            if(typeof total === 'undefined')total = 0.00;
            var remaining = (total?(new Decimal(total).minus(shared).toDP(2).toNumber()):0.00);
            ;
__p += '\n            <li class="add-emp">\n                <span class="input-group"><span class="input-group-btn"><button class="btn btn-success" disabled>+</button></span>\n                    <input class="employee-search form-control" type="text" name="employee" placeholder="Share With Employee&hellip;">\n                </span>\n            </li>\n\n        </ul>\n    </div>\n    <div class="modal-footer">\n        <button class="submit-tip-transaction pull-left">Submit</button>\n        <span class="tip-net-payable pull-right">Your Remaining Tips: $' +
__e(remaining) +
'</span>\n    </div>\n</div>';

}
return __p
}})();