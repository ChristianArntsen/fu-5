angular.module('appHeader', [])

    .directive('header', function() {
        
        return {
            restrict: 'E',
            replace: true,
            scope: {
                app: '@app',
                user: '@user'
            },
            controller: function($scope, $element,$http) {

                var promise = $http.get('/index.php/api/modules?api_key=no_limits');
                promise.then(function(response){
                    $scope.modules = response.data;
                    angular.forEach($scope.modules, function(module,key){
                        if(module.module_id.indexOf("_v2") != -1){
                            module.module_id = "v2/home#"+module.module_id.replace("_v2",'')
                        } else if(module.module_id == "marketing_campaigns"){
                            module.module_id = "marketing_campaigns/campaign_builder#/dashboard";
                        }
                        module.sort = parseFloat(module.sort);
                    })
                })
            },
            templateUrl: '/js/components/app-header/app-header.html'
        }
        
    });