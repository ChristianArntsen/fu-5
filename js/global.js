$(document).ready(function() {

    /* MODAL */
    //var globalModal = $('#globalModal');
    //
    //globalModal.on('hidden.bs.modal', function (e) {
    //    console.log('hey')
    //    $('.modal-content', $(this)).html('<div class="modal-load"></div>');
    //    $(this).removeData('bs.modal');
    //}).on('click', '[data-toggle="inner-modal"]:not(form)', function(e) {
    //    $(this).addClass('disabled');
    //    $.get($(this).attr('href'))
    //        .done(function(response) {
    //            globalModal.find('.modal-content').html(response);
    //        });
    //
    //    e.preventDefault();
    //    return false;
    //}).on('show.bs.modal', function(e) {
    //    var size = false;
    //    var modalClass = '';
    //    var dialogEl = $('.modal-dialog', this);
    //
    //    // TODO Add size as data attribute
    //
    //    if ($(this).data('bs.modal').options.size !== undefined) {
    //        size = $(this).data('bs.modal').options.size;
    //    }
    //
    //    dialogEl.removeClass('modal-sm modal-lg');
    //
    //    switch (size) {
    //        case 'small': modalClass = 'modal-sm'; break;
    //        case 'large': modalClass = 'modal-lg'; break;
    //    }
    //
    //    if (modalClass) {
    //        dialogEl.addClass(modalClass);
    //    }
    //}).on('submit', '[data-toggle="inner-modal"]', function(e) {
    //    var content = $(this).closest('.modal-content').html('<div class="modal-load"></div>');
    //    var action = $(this).attr('action');
    //
    //    if (action === undefined) {
    //        action = window.location;
    //    }
    //
    //    var method = $(this).attr('method');
    //
    //    if (method !== 'get') {
    //        method = 'post';
    //    }
    //
    //    var $ajax = method === 'post' ? $.post : $.get;
    //
    //    $ajax(action, $(this).serializeArray())
    //        .done(function(response) {
    //            content.html(response);
    //        });
    //
    //    e.preventDefault();
    //    return false;
    //});
    //
    //$.fn.modal.Constructor.DEFAULTS.backdrop = true;
    //
    //$( document ).ajaxError(function( event, jqxhr, settings, exception ) {
    //    // if ajax request was made for a modal:
    //    if (globalModal.data('bs.modal') && globalModal.data('bs.modal').isShown) {
    //        globalModal.one('loaded.bs.modal', function() {
    //            globalModal.find('.modal-content').html('' +
    //            '<div class="modal-body"><h3>Whoops...</h3>' +
    //            '<p>There was an error retrieving your request. Please check back later.</p>' +
    //            '</div>' +
    //            '<div class="modal-footer">' +
    //            '<button type="button" class="tutebtn grey-light" data-dismiss="modal">Close</button>' +
    //            '</div>');
    //        });
    //    }
    //});

    //$('#customer-new-customer').on('click', function(event) {
    //    event.preventDefault();
    //
    //    console.log('data:', $(this).attr('href'));
    //
    //    globalModal.modal();
    //
    //    $.post($(this).attr('href'))
    //        .success(function(data) {
    //            globalModal.find('.modal-content').html(data);
    //        })
    //        .fail(function(data) {
    //            console.log('failed', data)
    //        });
    //
    //})




    $('.bs-tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })
});

var stackedModalCount = 0;
var modalTemplate = '<div class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-load"></div></div></div></div>';



function defaultModal(url, data) {

    if (!data || data == undefined) {
        data = '';
    }

    var body = $('body');
    var newModal = $(modalTemplate).attr('id', 'bsModal'+stackedModalCount);

    body.append(newModal);
    newModal = body.find('#bsModal'+stackedModalCount);
    newModal.css('z-index', 1050 + (10 * stackedModalCount));

    addModalEvent();
    newModal.modal();

    setTimeout(function() {
        newModal.next('.modal-backdrop').css('z-index', 1040 + (10 * stackedModalCount));
    }, 100);

    $.post(url, data)
        .success(function(data) {
            newModal.find('.modal-content').html(data);
        })
        .fail(function(data) {
            console.log('failed', data)
        });


};

function bulkEdit(url) {
    if($("#sortable_table tbody :checkbox:checked").length >0) {
        defaultModal(url);
    } else {
        alert('You have not selected any customers to edit');
    }
};


function addModalEvent() {
    $('.modal').on('hidden.bs.modal', function(event) {

        event.stopImmediatePropagation();

        if (stackedModalCount > 0) {
            stackedModalCount--;
        }

        if (stackedModalCount > 0) {
            $(document.body).addClass('modal-open');
        } else {
            $(document.body).removeClass('modal-open');
        }

        $('.modal-content', $(this)).html('<div class="modal-load"></div>');
        $(this).removeData('bs.modal');

        $('#bsModal'+stackedModalCount).remove();

    })
        .on('shown.bs.modal', function(event) {
            console.log('modal showing?')
            stackedModalCount++;
        });
}







