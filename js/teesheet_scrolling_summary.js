var scrollingSummary = (function($) {
    var highlighter = $("<div style='background-color: blue; opacity: .3; position:absolute; top:0;left:0;' ></div>");
    var isDragging = false;
    var gradientColor = {};
    var gradientColor2 = {};
    var colors = [];
    var chartObject = [];

    var config = {
        chartContainer:"container",
        scrollContainer:".fc-agenda-slots",
        scrollWindow:".calScroller",
        backgroundColor:"white",
        columnColor:'rgb(124, 181, 236)',
        columnSecondaryColor:"red",
        highlighterColor:"blue",
        height:75
    };
    var ui = {
        chartContainer:function(){return $("#"+config['chartContainer'])},
        scrollContainer:function(){return $(config['scrollContainer'])},
        scrollWindow:function(){return $(config['scrollWindow'])}
    }


    var createPoint = function(value){
        var newPoint = {};
        if( value > 2){
            if(value>4)
                value = 4;
            newPoint = {
                y: value,
                color:  gradientColor
            }
        } else if(value == 2){

            newPoint = {
                y:value,
                color:gradientColor2
            }
        } else {
            newPoint = {
                y: value
            };
        }
        return newPoint;
    }
    var moveHighlighter = function(pos){
        highlighter.css({left:pos})
    }
    var animateHighlighter =_.throttle(function(pos){
            highlighter.animate({left:pos},300)
    },300)
    var scrollToPosition = _.throttle(function(pos){
        var height = ui.scrollContainer().height();
        ui.scrollWindow().animate({scrollTop:height*pos},100)
    },100)


    var scrollParent = function(currentX){
        var chartPadding = 0;
        var summaryWidth = ui.chartContainer().width() - chartPadding;
        var columnWidth = summaryWidth / colors.length;

        var parentOffset = ui.chartContainer().parent().offset();
        var relativeXPosition = currentX - parentOffset.left;
        var roundedXPos = Math.floor(relativeXPosition / columnWidth) * columnWidth;
        var scrollPercentage = roundedXPos/summaryWidth;
        if(scrollPercentage > 1)
            scrollPercentage = 1;
        if(scrollPercentage < 0)
            scrollPercentage =0;
        scrollToPosition(scrollPercentage);

        var summaryWindowPosition = summaryWidth*scrollPercentage;
        if(summaryWindowPosition + highlighter.width() > summaryWidth)
            summaryWindowPosition = summaryWidth-highlighter.width();
        if(isMouseFarToHighlighter(relativeXPosition,highlighter.position().left)){
            animateHighlighter(summaryWindowPosition);
        } else {
            moveHighlighter(summaryWindowPosition);
        }
    }
    var isMouseFarToHighlighter = function(relativeXPosition,left){
        return Math.abs(relativeXPosition - left) > 250
    }
    var calculateWindowWidth = function(){
        var chartPadding = 0;
        var summaryWidth = ui.chartContainer().width() - chartPadding;
        var teetimeWindow = ui.scrollWindow().height();
        var teetimeHeight = ui.scrollContainer().height();
        return teetimeWindow * summaryWidth / teetimeHeight
    }


    return {
        refreshData: function(teesheet_id, date){
            var self = this;
            var date = date.getFullYear() + "-"+(date.getMonth()+1)+"-"+date.getDate();
            var teesheetQuery = ""
            if(teesheet_id){
                if (stack_tee_sheets) {
                    teesheetQuery = "&course_id="+teesheet_id
                }
                else {
                    teesheetQuery = "&teesheet_id="+teesheet_id
                }
            }
            $.ajax({
                type: "GET",
                url: "index.php/teesheets/getTodaysSummary?date="+date+teesheetQuery,
                headers: {"read_only_session":1},
                dataType:'json',
                success: function(response){
                    // Clear out data before repopulating it
                    while(chartObject.series.length > 0) {
                        chartObject.series[0].remove(true);
                    }
                    for (var j in response) {
                        colors = [];
                        for (i = 0; i < response[j].length; i++) {
                            colors.push(createPoint(response[j][i].count));
                        }
                        chartObject.addSeries({
                            data: colors,
                            title: ""
                        });
                    }
                    ui.chartContainer().append(highlighter);
                    ui.scrollWindow().bind("scroll", self.handleParentScroll);
                }
            });
            //Figure out viewing width
            var summary_window = calculateWindowWidth();
            highlighter.css("background-color",config.highlighterColor);
            highlighter.width(summary_window);
            highlighter.height(config['height']);

        },
        handleParentScroll:function(e){
            if(isDragging)
                return;
            var newScrollPos = ui.scrollWindow().scrollTop();
            var height = ui.scrollContainer().height();
            var percentagePos = newScrollPos / height;
            var summaryWidth = ui.chartContainer().width();
            moveHighlighter(percentagePos * summaryWidth);
        },
        init: function(options){
            for(var prop in options) {
                if(options.hasOwnProperty(prop)){
                    config[prop] = options[prop];
                }
            }
            gradientColor = {
                linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                stops: [
                    [0, config.columnSecondaryColor],
                    [.5, config.columnColor],
                    [1, config.columnColor]
                ]
            };
            gradientColor2 = {
                linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                stops: [
                    [0, config.columnSecondaryColor],
                    [.1, config.columnColor],
                    [1, config.columnColor]
                ]
            };

            chartObject = new Highcharts.Chart({
                chart: {
                    renderTo: config['chartContainer'],
                    type: 'column',
                    height: config['height'],
                    margin: 0,
                    backgroundColor: config.backgroundColor
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    crosshair: false,
                    title: "",
                    labels: {enabled: false},
                    minorTickLength: 5,
                    tickLength: 0,
                    minPadding: 0,
                    maxPadding: 0,
                    gridLineWidth: 1,
                    gridZIndex: 900,
                    tickPositioner: function () {
                        var points = [];
                        for (i = 0; i < this.dataMax; i++) {
                            points.push(i + .5);
                        }
                        return points;
                    }
                },
                title: {
                    text: ''
                },
                subTitle: {
                    text: ''
                },
                yAxis: {
                    title: "",
                    min: 0,
                    max: 4,
                    labels: {enabled: false},
                    minPadding: 0,
                    maxPadding: 0
                },
                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    column: {
                        pointPadding: 0,
                        borderWidth: 0,
                        color: config.columnColor
                    },
                    series: {
                        pointPadding: 0,
                        groupPadding: 0,
                        borderWidth: 0,
                        shadow: false
                    }
                },
                legend: {
                    enabled: false
                },
                series: []
            });
            ui.chartContainer().bind('mousedown', function (e) {
                isDragging = true;
                scrollParent(e.pageX);
            });
            $(document).bind('mousemove', function (e) {
                if(isDragging){
                    scrollParent(e.pageX);
                }
            });
            $(document).bind('mouseup', function (e) {
                if(isDragging){
                    isDragging = false;
                }
            });



        }
    }
})(jQuery);

$(function () {
    if ($('#teesheets').length > 0) {
        scrollingSummary.init({
            chartContainer: "container",
            scrollContainer: ".fc-agenda-slots",
            scrollWindow: ".calScroller",
            backgroundColor: "white",
            columnColor: 'rgb(124, 181, 236)',
            columnSecondaryColor: "red",
            height: 25,
            highlighterColor: "blue"
        });
    }
    //scrollingSummary.refreshData();

});
