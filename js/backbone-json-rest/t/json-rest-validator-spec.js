if(typeof require !== 'undefined') {
    var validator = require('../json-rest-validator.js').validator;
    var examples = require('./examples');
}
else{
    var validator = new JsonRestValidator();
}
//*

describe("The JsonRestValidator",function()
{
    if(typeof require === 'undefined') {
        it("should be defined", function () {
            expect(JsonRestValidator).toBeDefined();
        });
    }

    it("should construct an instance",function(){
        expect(validator).toBeDefined();
    });
    
    if(typeof require === 'undefined') {
        it("should be an instance of JsonRestValidator", function () {
            expect(validator instanceof JsonRestValidator).toBe(true);
        });
    }

    describe("The reset method", function () {

        it("should have initialized the last_error and warnings attributes with reset", function () {
            expect(validator._last_error).toBe('');
            expect(JSON.stringify(validator._warnings)).toBe('[]');
        });

        it("should have had last_error and warnings set", function () {
            validator._last_error = 'cow';
            validator._warnings.push('cowtoo');
            expect(validator._last_error).toBe('cow');
            expect(JSON.stringify(validator._warnings)).not.toBe('[]');
            validator.reset();
        });
        it("should have re-initialized the last_error and warnings attributes with reset", function () {
            expect(validator._last_error).toBe('');
            expect(JSON.stringify(validator._warnings)).toBe('[]');
        });
    });

    describe("The get/set_last_error methods", function () {
        validator.reset();
        it("should be able to set the error", function () {
            validator.set_last_error('error 1');
            expect(validator._last_error).toBe('error 1');
        });
        it("should be able to get the error", function () {
            expect(validator.get_last_error()).toBe('error 1');
            validator.reset();
        });
        it("should get false after reset", function () {
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });
    });
    describe("The get/add_warning methods", function () {
        validator.reset();

        it("should be able to add a warning", function () {
            validator.add_warning('error 1');
            expect(validator._warnings[0]).toBe('error 1');
        });
        it("should be able to get the warnings array", function () {
            expect(JSON.stringify(validator.get_warnings())).toBe('["error 1"]');
        });
        it("should be able to get the last warning", function () {
            expect(validator.get_last_warning()).toBe("error 1");
        });
        it("should be able to add another warning", function () {
            validator.add_warning('error 2');
            expect(validator._warnings[1]).toBe('error 2');
            expect(JSON.stringify(validator.get_warnings())).toBe('["error 1","error 2"]');
            expect(validator.get_last_warning()).toBe("error 2");
            validator.reset();
        });
    });

    describe("The validate_member_name method", function () {
        validator.reset();
        it("should be fine with strings with spaces, -, or _", function () {
            expect(validator.validate_member_name('some-_ string', true)).toBe(true);
        });
        it("should have generated a warning because of the ' '", function () {
            expect(validator.get_last_warning()).toBe('Use of " " in member names allowed but not recommended: ' + 'some-_ string')
        });
        it("should hate strings with !@%&!@*@#$&% garbage with a burning hatred", function () {
            expect(validator.validate_member_name('some&(@#$!*()]ngstring', true)).toBe(false);
        });
        it("should have an appropriate last_error message after that.", function () {
            expect(validator.get_last_error()).toBe('illegal character "&" at position 4 of string: ' + 'some&(@#$!*()]ngstring');
            validator.reset();
        });


        it("should have cleared out the last_error and warnings attributes with reset", function () {
            expect(validator.get_last_error()).toBe(false);
            expect(validator.get_warnings().length).toBe(0);
        });

        it("should reject strings with space at the end", function () {
            expect(validator.validate_member_name('somestring ', true)).toBe(false);
        });
        it("should have an appropriate last_error message after that.", function () {
            expect(validator.get_last_error()).toBe('member names must not end with " ", "-", or "_"');
            validator.reset();
        });

        it("should reject strings with space at the beginning", function () {
            expect(validator.validate_member_name(' somestring', true)).toBe(false);
        });
        it("should have an appropriate last_error message after that.", function () {
            expect(validator.get_last_error()).toBe('member names must not begin with " ", "-", or "_"');
            validator.reset();
        });


        it("should reject strings with - at the end", function () {
            expect(validator.validate_member_name('somestring-', true)).toBe(false);
        });
        it("should have an appropriate last_error message after that.", function () {
            expect(validator.get_last_error()).toBe('member names must not end with " ", "-", or "_"');
        });
        validator.reset();
        it("should reject strings with - at the beginning", function () {
            expect(validator.validate_member_name('-somestring', true)).toBe(false);
        });
        it("should have an appropriate last_error message after that.", function () {
            expect(validator.get_last_error()).toBe('member names must not begin with " ", "-", or "_"');
            validator.reset();
        });


        it("should reject strings with _ at the end", function () {
            expect(validator.validate_member_name('somestring_', true)).toBe(false);
        });
        it("should have an appropriate last_error message after that.", function () {
            expect(validator.get_last_error()).toBe('member names must not end with " ", "-", or "_"');
        });
        it("should reject strings with _ at the beginning", function () {
            expect(validator.validate_member_name('_somestring', true)).toBe(false);
        });
        it("should have an appropriate last_error message after that.", function () {
            expect(validator.get_last_error()).toBe('member names must not begin with " ", "-", or "_"');
            validator.reset();
        });

        it("should tolerate strings with unicode", function () {
            expect(validator.validate_member_name('strangenesجسم جرم‌مند، جسم جرم‌مند دیگrtl', true)).toBe(true);
        });
        it("should have generated a warning because of the ' '", function () {
            expect(validator.get_warnings()[0]).toBe('Use of " " in member names allowed but not recommended: ' + 'strangenesجسم جرم‌مند، جسم جرم‌مند دیگrtl')
        });
        it("should have a second warning", function () {
            //console.log(validator.get_warnings())
            expect(validator.get_warnings().length).toBe(2)
        });
        it("should have generated a warning because of the unicode", function () {
            expect(validator.get_last_warning()).toBe('Use of Unicode characters U+0080 and above in member names allowed but not recommended: ' + 'strangenesجسم جرم‌مند، جسم جرم‌مند دیگrtl')
            validator.reset();
        });
    });

    describe("The validate_attributes method", function () {
        validator.reset();
        it("should accept a valid attributes object", function () {
            expect(validator.validate_attributes(examples.attributes)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it("should not accept an array", function () {
            expect(validator.validate_attributes([examples.attributes, examples.attributes])).toBe(false);
            expect(validator.get_last_error()).toBe('"attributes" must be an object');
            validator.reset();
        });

        it("should not accept a null", function () {
            expect(validator.validate_attributes(null)).toBe(false);
            expect(validator.get_last_error()).toBe('"attributes" must be an object');
            validator.reset();
        });

        it("should not accept a string", function () {
            expect(validator.validate_attributes('cow')).toBe(false);
            expect(validator.get_last_error()).toBe('"attributes" must be an object');
            validator.reset();
        });

        it("should not allow a 'links' member", function () {
            expect(validator.validate_attributes({'links': 'someval...'})).toBe(false);
            expect(validator.get_last_error()).toBe('Cannot use reserved "links" member in attributes');
            validator.reset();
        });

        it("should not allow a 'relationships' member", function () {
            expect(validator.validate_attributes({'relationships': 'someval...'})).toBe(false);
            expect(validator.get_last_error()).toBe('Cannot use reserved "relationships" member in attributes');
            validator.reset();
        });

    });

    describe("the validate_primary_meta method", function () {

        it("should accept a valid meta object", function () {
            expect(validator.validate_primary_meta({
                'key1': 'val 1',
                'key 2': [],
                'key3': {},
                'key4': null
            })).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it("should not accept an array", function () {
            expect(validator.validate_primary_meta([])).toBe(false);
            expect(validator.get_last_error()).toBe('"meta" must be an object');
            validator.reset();
        });

        it("should not accept a null", function () {
            expect(validator.validate_primary_meta(null)).toBe(false);
            expect(validator.get_last_error()).toBe('"meta" must be an object');
            validator.reset();
        });

        it("should not accept a string", function () {
            expect(validator.validate_primary_meta('cow')).toBe(false);
            expect(validator.get_last_error()).toBe('"meta" must be an object');
            validator.reset();
        });
    });

    describe("The validate_json_api method", function () {
        var ob = {
            "version": "1.0",
            "meta": {'key1': 'val 1', 'key 2': [], 'key3': {}, 'key4': null}
        };

        it("should accept a valid jsonapi object", function () {
            expect(validator.validate_json_api(ob)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it("should not accept an array", function () {
            expect(validator.validate_json_api([])).toBe(false);
            expect(validator.get_last_error()).toBe('"jsonapi" must be an object');
            validator.reset();
        });

        it("should not accept a null", function () {
            expect(validator.validate_json_api(null)).toBe(false);
            expect(validator.get_last_error()).toBe('"jsonapi" must be an object');
            validator.reset();
        });

        it("should not accept a string", function () {
            expect(validator.validate_json_api('cow')).toBe(false);
            expect(validator.get_last_error()).toBe('"jsonapi" must be an object');
            validator.reset();
        });

        it("should not accept a version that is not a string", function () {
            var ob2 = JSON.parse(JSON.stringify(ob));
            ob2.version = [];
            expect(validator.validate_json_api(ob2)).toBe(false);
            expect(validator.get_last_error()).toBe('"jsonapi" version must be a string: []');
            validator.reset();
        });

        it("should not accept a bad meta member", function () {
            var ob3 = JSON.parse(JSON.stringify(ob));
            ob3.meta = [];
            expect(validator.validate_json_api(ob3)).toBe(false);
            expect(validator.get_last_error()).toBe('"meta" must be an object');
            validator.reset();
        });
    });

    describe("The validate_link_member method", function () {
        validator.reset();

        it("should accept a valid link string", function () {
            expect(validator.validate_link_member('This is not really a URL...', true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });
        it("should accept a valid link object", function () {
            expect(validator.validate_link_member({
                'href': 'This is not really a URL...',
                'meta': {"tag": "a"}
            }, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it("should not accept a bad href", function () {
            expect(validator.validate_link_member({"href": []})).toBe(false);
            expect(validator.get_last_error()).toBe('Invalid link href found: []');
            validator.reset();
        });

        it("should not accept a bad meta member", function () {
            expect(validator.validate_link_member({"href": "cou", 'meta': []})).toBe(false);
            expect(validator.get_last_error()).toBe('"meta" must be an object');
            validator.reset();
        });
    });

    describe("The validate_links method", function () {
        validator.reset();
        // Accept a valid program
        it("should accept a valid links object", function () {
            expect(validator.validate_links(examples.links, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        // reject invalid program
        it("should not accept an array", function () {
            expect(validator.validate_links([])).toBe(false);
            expect(validator.get_last_error()).toBe('"links" must be an object');
            validator.reset();
        });

        it("should not accept a null", function () {
            expect(validator.validate_links(null)).toBe(false);
            expect(validator.get_last_error()).toBe('"links" must be an object');
            validator.reset();
        });

        it("should not accept a string", function () {
            expect(validator.validate_links('cow')).toBe(false);
            expect(validator.get_last_error()).toBe('"links" must be an object');
            validator.reset();
        });

        it("should reject an invalid link member", function () {
            expect(validator.validate_links({'self': {"href": "cou", 'meta': []}})).toBe(false);
            expect(validator.get_last_error()).toBe('"meta" must be an object at link index: self');
            validator.reset();
        });
    });

    describe("The validate_error_source method", function () {
        validator.reset();
        // Accept a valid program
        it("should accept a valid source", function () {
            expect(validator.validate_error_source({
                "pointer": "/data/attributes/name",
                "parameter": "name"
            }, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it("should reject an invalid pointer member", function () {
            expect(validator.validate_error_source({
                "pointer": [],
                "parameter": "name"
            }, true)).toBe(false);
            expect(validator.get_last_error()).toBe('Error pointers should be strings conforming to RFC6901');
            validator.reset();
        });

        it("should reject an invalid parameter member", function () {
            expect(validator.validate_error_source({
                "pointer": '/data/attributes/name',
                "parameter": []
            }, true)).toBe(false);
            expect(validator.get_last_error()).toBe('Error parameter should be a string indicating which URI query parameter caused the error');
            validator.reset();
        });
    });

    describe("The validate_errors_lazy method", function () {
    });

    describe("The validate_primary_errors method", function () {
    });

    describe("The validate_resource_linkage Method", function () {
        validator.reset();
        // Accept a valid program
        it('should accept a null value', function () {
            expect(validator.validate_resource_linkage(null)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it('should accept an empty array value', function () {
            expect(validator.validate_resource_linkage([])).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it('should accept a single linkage', function () {
            expect(validator.validate_resource_linkage(examples.resource_linkage)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it('should accept an array of linkages', function () {
            expect(validator.validate_resource_linkage(examples.resource_linkages)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        // Reject an invalid program
        it('should not allow a string', function () {
            expect(validator.validate_resource_linkage('cow')).toBe(false);
            expect(validator.get_last_error()).toBe('resource linkage must be null, an empty array, ' +
                'a single resource identifier object, ' +
                'or an array of resource identifier objects');
            validator.reset();
        });

        it('should not allow an undefined value', function () {
            expect(validator.validate_resource_linkage()).toBe(false);
            expect(validator.get_last_error()).toBe('resource linkage must be null, an empty array, ' +
                'a single resource identifier object, ' +
                'or an array of resource identifier objects');
            validator.reset();
        });
        it('should not allow a bad value in an array', function () {
            expect(validator.validate_resource_linkage([examples.resource_linkage, 'cow', examples.resource_linkage], true)).toBe(false);
            expect(validator.get_last_error()).toBe('"resource" must be an object: "cow" at resource linkage index 1');
            validator.reset();
        });
    });

    describe("The validate_resource_linkage Method", function () {
        validator.reset();
        // Accept a valid program
        it('should accept a null value', function () {
            expect(validator.validate_resource_linkage(null)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it('should accept an empty array value', function () {
            expect(validator.validate_resource_linkage([])).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it('should accept a single linkage', function () {
            expect(validator.validate_resource_linkage(examples.resource_linkage)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it('should accept an array of linkages', function () {
            expect(validator.validate_resource_linkage(examples.resource_linkages)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        // Reject an invalid program
        it('should not allow a string', function () {
            expect(validator.validate_resource_linkage('cow')).toBe(false);
            expect(validator.get_last_error()).toBe('resource linkage must be null, an empty array, ' +
                'a single resource identifier object, ' +
                'or an array of resource identifier objects');
            validator.reset();
        });

        it('should not allow an undefined value', function () {
            expect(validator.validate_resource_linkage()).toBe(false);
            expect(validator.get_last_error()).toBe('resource linkage must be null, an empty array, ' +
                'a single resource identifier object, ' +
                'or an array of resource identifier objects');
            validator.reset();
        });
        it('should not allow a bad value in an array', function () {
            expect(validator.validate_resource_linkage([examples.resource_linkage, 'cow', examples.resource_linkage], true)).toBe(false);
            expect(validator.get_last_error()).toBe('"resource" must be an object: "cow" at resource linkage index 1');
            validator.reset();
        });
    });

    describe("The validate_relationship method", function () {
        validator.reset();
        // accept valid programs
        it("should accept a valid relationship object", function () {
            expect(validator.validate_relationship(examples.relationship, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        // reject invalid programs
        it("should not accept a relationship object with no links, data, or meta members", function () {
            expect(validator.validate_relationship({'pizza': true}, true)).toBe(false);
            expect(validator.get_last_error()).toBe('One of the following members required on relationships: links | data | meta');
            validator.reset();
        });
        //Todo: Finish
    });

    describe("The validate_relationships method", function () {
        validator.reset();
        // accept valid programs
        it("should accept a valid relationships object", function () {
            expect(validator.validate_relationships({
                "arch_nemesis": examples.relationship,
                "cousin_bob": examples.relationship
            }, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        // reject invalid programs
        it("should not accept invalid relationship object", function () {
            expect(validator.validate_relationships({'arch_nemesis': {'pizza': true}}, true)).toBe(false);
            expect(validator.get_last_error()).toBe('One of the following members required on relationships: links | data | meta at relationships index: arch_nemesis');
            validator.reset();
        });
        //Todo: Finish
    });

    describe("The validate_resource_lazy method", function () {
        validator.reset();
        // Accept a valid program
        it("should accept a valid resource", function () {
            expect(validator.validate_resource_lazy(examples.resource, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });
        //Todo: Finish
    });

    describe("The validate_resource method", function () {
        validator.reset();
        // Accept a valid program
        it("should accept a valid resource", function () {
            expect(validator.validate_resource(examples.resource, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });
        //Todo: Finish

        it("should reject the incorrect example in examples js file", function () {
            expect(validator.validate_resource(examples.inv_resource, true)).toBe(false);
            expect(validator.get_last_error()).toBe('member names must not end with " ", "-", or "_" on resource with id: 1');
            validator.reset();
        });
    });

    describe("The validate_primary_included method", function () {

        it("should just work", function () {
            expect(validator.validate_primary_included(examples.long.included, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });
    });

    describe("The validate_primary_data method", function () {

        it("should just work", function () {
            expect(validator.validate_primary_data(examples.long.data, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it("should reject the incorrect example in examples js file", function () {
            expect(validator.validate_primary_data(examples.wrong.data, true)).toBe(false);
            expect(validator.get_last_error()).toBe('member names must not end with " ", "-", or "_" on resource with id: 1 at primary data index 0');
            validator.reset();
        });
    });

    describe("The validate_primary_lazy method", function () {

        it("should just work", function () {
            expect(validator.validate_primary_lazy(examples.long, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });
    });

    describe("The validate_result method", function () {

        it("should just work", function () {
            expect(validator.validate_result(examples.long, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it("should reject the incorrect example in examples js file", function () {
            expect(validator.validate_result(examples.wrong, true)).toBe(false);
            expect(validator.get_last_error()).toBe('member names must not end with " ", "-", or "_" on resource with id: 1 at primary data index 0');
            validator.reset();
        });
    });

    describe("The validate method", function () {

        it("should just work", function () {
            expect(validator.validate(examples.long, true)).toBe(true);
            expect(validator.get_last_error()).toBe(false);
            validator.reset();
        });

        it("should reject the incorrect example in examples js file", function () {
            expect(validator.validate(examples.wrong, true)).toBe(false);
            expect(validator.get_last_error()).toBe('member names must not end with " ", "-", or "_" on resource with id: 1 at primary data index 0');
            validator.reset();
        });
    });
});
//