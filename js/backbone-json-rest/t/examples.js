var examples = {};
examples.long = {
"data": [{
    "type": "articles",
    "id": "1",
    "attributes": {
        "title": "JSON API paints my bikeshed!",
        "twitter": "dgeb"
    },
    "links": {
        "self": "http://example.com/articles/1"
    },
    "relationships": {
        "author": {
            "links": {
                "self": "http://example.com/articles/1/relationships/author",
                "related": {
                    "href": "http://example.com/articles/1/comments",
                    "meta": {
                        "count": 10
                    }
                }
            },
            "data": { "type": "people", "id": "9" }
        },
        "comments": {
            "links": {
                "self": "http://example.com/articles/1/relationships/comments",
                "related": "http://example.com/articles/1/comments"
            },
            "data": [
                { "type": "comments", "id": "5" },
                { "type": "comments", "id": "12" }
            ]
        }
    }
}],
    "included": [{
    "type": "people",
    "id": "9",
    "attributes": {
        "first-name": "Dan",
        "last-name": "Gebhardt",
        "twitter": "dgeb"
    },
    "links": {
        "self": "http://example.com/people/9"
    }
}, {
    "type": "comments",
    "id": "5",
    "attributes": {
        "body": "First!"
    },
    "relationships": {
        "author": {
            "data": { "type": "people", "id": "2" }
        }
    },
    "links": {
        "self": "http://example.com/comments/5"
    }
}, {
    "type": "comments",
    "id": "12",
    "attributes": {
        "body": "I like XML better"
    },
    "relationships": {
        "author": {
            "data": { "type": "people", "id": "9" }
        }
    },
    "links": {
        "self": "http://example.com/comments/12"
    }
}]
};

examples.wrong = JSON.parse(JSON.stringify(examples.long));

examples.wrong.data[0].type += ' ';

examples.attributes = examples.long.data[0].attributes;

examples.links = examples.long.data[0].relationships.author.links;


examples.resource_linkage = { "type": "people", "id": "9" };
examples.resource_linkages = [{ "type": "people", "id": "9" },{ "type": "people", "id": "10" },{ "type": "people", "id": "11" }];

examples.relationship = examples.long.data[0].relationships.author;

examples.resource = examples.long.data[0];
examples.inv_resource = examples.wrong.data[0];
if(typeof exports !== 'undefined'){
    exports.long = examples.long;
    exports.wrong = examples.wrong;
    exports.attributes = examples.attributes;
    exports.links = examples.links;
    exports.links = examples.links;
    exports.resource_linkage = examples.resource_linkage;
    exports.resource_linkages = examples.resource_linkages;
    exports.relationship = examples.relationship;
    exports.resource = examples.resource;
    exports.inv_resource = examples.inv_resource;
}