/*
 * jQuery Keypad
 * version: 1.0
 * Author: foreUP (Jordan Bush)
 */
(function($){

	var template = '<div class="keypad-popup"><div class="container-fluid keypad">' +
			'<div class="row">' +
				'<div class="col-xs-12">' +
					'<div class="btn-group btn-group-justified">' +
						'<div class="btn-group">' +
							'<button data-char="1" class="btn btn-default keypad-number">1</button>' +
						'</div>' +
						'<div class="btn-group">' +
							'<button data-char="2" class="btn btn-default keypad-number">2</button>' +
						'</div>' +
						'<div class="btn-group">' +
							'<button data-char="3" class="btn btn-default keypad-number">3</button>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>' +
			'<div class="row">' +
				'<div class="col-xs-12">' +
					'<div class="btn-group btn-group-justified">' +
						'<div class="btn-group">' +
							'<button data-char="4" class="btn btn-default keypad-number">4</button>' +
						'</div>' +
						'<div class="btn-group">' +
							'<button data-char="5" class="btn btn-default keypad-number">5</button>' +
						'</div>' +
						'<div class="btn-group">' +
							'<button data-char="6" class="btn btn-default keypad-number">6</button>' +
						'</div>' +
					'</div>' +
				'</div>' +		
			'</div>' +
			'<div class="row">' +
				'<div class="col-xs-12">' +
					'<div class="btn-group btn-group-justified">' +
						'<div class="btn-group">' +
							'<button data-char="7" class="btn btn-default keypad-number">7</button>' +
						'</div>' +
						'<div class="btn-group">' +
							'<button data-char="8" class="btn btn-default keypad-number">8</button>' +
						'</div>' +
						'<div class="btn-group">' +
							'<button data-char="9" class="btn btn-default keypad-number">9</button>' +
						'</div>' +
					'</div>' +
				'</div>' +		
			'</div>' +
			'<div class="row">' +
				'<div class="col-xs-12">' +
					'<div class="btn-group btn-group-justified">' +
						'<div class="btn-group">' +
							'<button data-char="enter" class="btn btn-default">Enter</button>' +
						'</div>' +
						'<div class="btn-group">' +
							'<button data-char="0" class="btn btn-default">0</button>' +
						'</div>' +
						'<div class="btn-group">' +
							'<button data-char="del" class="btn btn-default">Del</button>' +
						'</div>' +
					'</div>' +
				'</div>' +		
			'</div>' +
		'</div></div>';
	
	function format_number(chars, formatMoney){
		var number = chars.replace(/[^0-9]+/g, '');

		if(formatMoney){
			number = number / 100;
			var number = accounting.formatMoney(number, '');
		}
		return number;
	}

	function add_character(input, character){
		var characters = input.val() + character;
		var formatted = format_number(characters, input.data('settings').formatMoney);

		input.val(formatted);
		input.focus();
		return true;
	}

	function remove_character(input){
		var characters = input.val();
		characters = characters.substring(0, characters.length - 1);
		var formatted = format_number(characters,  input.data('settings').formatMoney);

		input.val(formatted);
		input.focus();
		return true;
	}

	function close(keypad){
		if($.fn.keypad.currentField && $.fn.keypad.currentField.val() == ''){
			$.fn.keypad.currentField.val( $.fn.keypad.currentField.data('last-value') );
		}
		$.fn.keypad.currentField.trigger('keypad_close');
		keypad.hide();
	}

	function enter(keypad){
		$.fn.keypad.currentField.trigger('keypad_enter');
		close(keypad);		
	}

	function focus_on_end(input){
		// Scroll to the bottom, in case we're in a tall textarea
		// (Necessary for Firefox and Google Chrome)
		input.scrollTop = 999999;
	}

	$.fn.keypad = function( options ) {

        // Default options
        var settings = $.extend({
            position: 'left',
            formatMoney: true,
            closeOnClick: true
        }, options );

        $(document).off('click.keypad-close').on('click.keypad-close', function(e){
 			
 			if((!$.fn.keypad.currentField || $(e.target).get(0) !== $.fn.keypad.currentField.get(0))
				&& !$(e.target).hasClass('keypad')
				&& !$(e.target).hasClass('keypad-number')){
				
				if(keypad.is(':visible') && settings.closeOnClick){
					close($('div.keypad-popup'));
				}
			}
		});

		// Place hidden keypad in page body
		var keypad = $(template);
		keypad.attr('id', 'jquery_keypad');
		$('#jquery_keypad').remove();
		$('body').append(keypad);

		keypad.on('click', 'button', function(e){
			
			var character = $(this).attr('data-char');
			var currentField = $.fn.keypad.currentField;
			
			if(character == 'enter'){
				enter(keypad);
			}else if(character == 'del'){
				remove_character(currentField);
			}else{
				add_character(currentField, character);
			}

			return false;
		});

		$(document).off('keypress.keypad').on('keypress.keypad', function(e){
 			if(e.keyCode == 13 && $.fn.keypad.currentField){
				enter(keypad);
				return false;				
			}
		});

        // Attach keypad code to element on page
        return this.each(function(){

			$(this).addClass('has-keypad');
			$(this).data('settings', settings);

			$(this).click(function(e){
				
				if($.fn.keypad.currentField){
					close($('div.keypad-popup'));
				}
				
				$.fn.keypad.currentField = $(this);
				$(this).data('last-value', $(this).val()).val('');

				var height = parseInt($(this).height(), 10);
				var width = parseInt($(this).width(), 10);
				var padY = parseInt($(this).css('padding-top'), 10) + parseInt($(this).css('padding-bottom'), 10);
				var padX = parseInt($(this).css('padding-left'), 10) + parseInt($(this).css('padding-right'), 10);

				if(settings.position == 'right'){
					var top = $(this).offset().top + height + padY - 230;
					var left = $(this).offset().left + width + padX + 5;

				}else if(settings.position == 'bottom-right'){
					var top = $(this).offset().top;
					var left = $(this).offset().left + width + padX + 5;

				}else if(settings.position == 'left'){
					var top = $(this).offset().top + height + padY - 230;
					var left = $(this).offset().left - 200 - width + padX + 5;

				}else if(settings.position == 'bottom'){
					var top = $(this).offset().top + height + padY + 5;
					var left = $(this).offset().left + padX - 10;

				}else if(settings.position == 'top'){
					var top = $(this).offset().top + padY - 5;
					var left = $(this).offset().left + padX - 10;
				}

				$('div.keypad-popup').css({'top':top, 'left':left}).show();
				focus_on_end( $(this) );
			});

			$(this).on('focus', function(event){
				focus_on_end( $(this) );
			});
		});
    };
}( jQuery ));