cbApp.controller('emaillistCtrl', function ($scope, $uibModalInstance,contacts) {

    $scope.contacts = contacts;

    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});