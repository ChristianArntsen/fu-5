cbApp.directive('previewTemplate', function() {
   return {
       controller: function($scope, $element) {
           
           var body = angular.element($('body'));
           
           $scope.pt = {
               container: false,
               web: false,
               mobile: false
           };
           
           $scope.$on('loadImage', function(event, data) {
               console.log(data.category);
               $scope.pt.name = data.name;
               $scope.pt.container = true;
               $scope.pt.imageUrlWeb = (data.name.replace(' ', '-').toLowerCase()) + '-' + data.category.toLowerCase() + '-preview-web.jpg';
               $scope.pt.imageUrlMobile = (data.name.replace(' ', '-').toLowerCase()) + '-' +  data.category.toLowerCase() + '-preview-mobile.jpg';
               $scope.pt.loading = true;
               $scope.switchWeb();
               body.addClass('hideScroll');
           });
           
           $scope.closeContainer = function() {
               $scope.pt.container = false;
               body.removeClass('hideScroll');
           };
           
           $scope.switchWeb = function() {
               $scope.pt.web = true;
               $scope.pt.mobile = false;
           };
           
           $scope.switchMobile = function() {
               $scope.pt.web = false;
               $scope.pt.mobile = true;
           };
           
       },
       templateUrl: '/frontend/campaign-builder/app/template_select/template_select_preview.html'
   } 
});