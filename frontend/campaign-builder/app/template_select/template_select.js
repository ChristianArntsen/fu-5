cbApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('app.template_select', {
            url: '/template-select/:campaign_id',
            controller: 'templateSelectCtrl',
            templateUrl: '/frontend/campaign-builder/app/template_select/template_select.html'
        });
}]);
cbApp.directive('placeholder', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            // show an image-missing image
            element.error(function () {
                var w = element.width();
                var h = element.height();
                // using 20 here because it seems even a missing image will have ~18px width
                // after this error function has been called
                if (w <= 20) { w = 100; }
                if (h <= 20) { h = 100; }
                var url = 'http://placehold.it/' + w + 'x' + h + '/cccccc/ffffff&text=. . .';
                element.attr('orig', element.attr("src"));
                element.prop('src', url);
                element.css('border', 'double 3px #cccccc');
            });
        }
    }
});
cbApp.controller('templateSelectCtrl',
    function($scope,ComponentFactory,$document,$notification,CampaignFactory,$stateParams,TemplateFactory,dialogs) {
    

        $scope.click = function() {
            $notification.info('Input is Complete', 'You\'ve successfully completed the entire input box!');
        }


        $scope.campaign = {};
        $scope.chosenList = 'default';
        $scope.init = function(){
            $scope.chosenCategory = 'golf';
            if($stateParams.campaign_id != undefined){
                var promise = CampaignFactory.getCampaign($stateParams.campaign_id);
                promise.then(function(data){
                    $scope.campaign = data;
                    if($scope.campaign.status != "draft"){
                        $scope.disableInput = true;
                    }
                })
            }


            if($stateParams.campaign_id != undefined){
                $scope.loading = true;
                var promise = TemplateFactory.getAll();
                promise.then(function(data){
                    $scope.loading = false;
                    $scope.templates = data;
                })
            }

        }
        
        $scope.previewTemplate = function(templateName) {
            $scope.$broadcast('loadImage', {name: templateName, category: $scope.chosenCategory});
        };
        
        $scope.init();

        var showDialog = function(){

        }

        $scope.deleteTemplate = function(template){
            var dlg2 = dialogs.confirm("Careful there.","You're about to completely remove this template, are you sure you want to continue?",{size:"md"});
            dlg2.result.then(function(){
                CampaignFactory.deleteTemplate(template);
                var index = $scope.templates['saved'].indexOf(template);
                $scope.templates['saved'].splice(index,1);
            })
        }


        $scope.selectTemplate = function(template){
            if($scope.disableInput){
                dialogs.error("Can't do that.","This campaign isn't in draft mode.  Try starting a new campaign instead.",{size:"md"});
                return;
            }

            if($scope.campaign.marketing_template_id != null ){
                dlg = dialogs.create('/frontend/campaign-builder/app/template_select/overwrite_dialog.html','overwriteDialogController',{},{key: false,back: 'static',size:"md"});
                dlg.result.then(function(choice){
                    if(choice == 'delete'){
                        $scope.campaign.json_content = angular.fromJson(template.template);
                    }

                    $scope.campaign.marketing_template_id = template.id;
                    CampaignFactory.saveCampaign( $scope.campaign, $scope.campaign.campaign_id);
                    CampaignFactory.currentCampaign.template_details = {
                        "style":template.style,
                        "name":template.name
                    };
                    $scope.nextStep();
                });
            } else {
                $scope.campaign.json_content = angular.fromJson(template.template);
                $scope.campaign.marketing_template_id = template.id;

                CampaignFactory.saveCampaign( $scope.campaign, $scope.campaign.campaign_id);
                CampaignFactory.currentCampaign.template_details = {
                    "style":template.style,
                    "name":template.name
                };
                $scope.nextStep();
            }



        }
});