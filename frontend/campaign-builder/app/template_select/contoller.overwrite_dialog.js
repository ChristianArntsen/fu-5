cbApp.controller('overwriteDialogController',function($scope,$modalInstance,dialogs){

    $scope.choice = "keep";
    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    };

    $scope.save = function(){
        if($scope.choice == "delete"){
            var dlg2 = dialogs.confirm("Careful there.","You're about to erase all the content in this email.  Only continue if you're sure about it.",{size:"md"});
            dlg2.result.then(function(){
                $modalInstance.close($scope.choice);
            })
        } else {

            $modalInstance.close($scope.choice);
        }
    };

    $scope.hitEnter = function(evt) {
        if (angular.equals(evt.keyCode, 13) && !(angular.equals($scope.name, null) || angular.equals($scope.name, '')))
            console.log("enter");
    }
});