cbApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('test', {
            url: '/test',
            views: {
                'test': {
                    templateUrl: '/frontend/campaign-builder/app/test/test.html'
                }
            }
        });
}]);
