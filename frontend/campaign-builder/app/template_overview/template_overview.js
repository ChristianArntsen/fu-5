cbApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('app.template_overview', {
            url: '/template-overview/:campaign_id',
            controller: 'templateOverviewCtrl',
            templateUrl: '/frontend/campaign-builder/app/template_overview/template_overview.html'
        });
}]);

cbApp.controller('templateOverviewCtrl',
    function($scope,CampaignFactory,PhpDataFactory,$stateParams,$state,$timeout,dialogs,$http) {



        $scope.createEditCampaign = function(){
            $scope.loading = true;
            var promise = CampaignFactory.saveCampaign($scope.campaign,$stateParams.campaign_id);
            promise.then(function(data){
                $state.go('app.template_select',{"campaign_id":data.campaign_id})
            })
        }
        $scope.saveCampaign = function(){
            if($scope.tab.textSelected){
                $scope.campaign.type = "text";
            } else {
                $scope.campaign.type = "email";
            }
            $scope.convertPrettyToNormal();
            var promise = CampaignFactory.saveCampaign($scope.campaign,$stateParams.campaign_id);
        }



        var throttle;
        $scope.$watchGroup(['campaign.title','campaign.subject','campaign.from_name','campaign.from_email','campaign.content'],function(newVal,oldVal){
            if (throttle)
                $timeout.cancel(throttle);
            throttle = $timeout(function() {
                $scope.saveCampaign();
            }, 1000);
        },true)

        var selectAppropriateTab = function(campaign){
            $scope.tab.textSelected = campaign.type == "text";
            $scope.tab.emailSelected = campaign.type != "text";
        }

        $scope.campaign = {};
        $scope.init = function(){
            $scope.course_info = PhpDataFactory.course_info;
            $scope.tab = {};
            $scope.tab.textSelected = false;
            $scope.tab.emailSelected = true;
            $scope.selectedGroups = [];
            if($stateParams.campaign_id != undefined){
                var promise =
                    CampaignFactory.getCampaign($stateParams.campaign_id);
                promise.then(function(data){
                    $scope.campaign = data;
                    selectAppropriateTab(data);
                    if($scope.campaign.status != "draft"){
                        $scope.disableInput = true;
                    }
                    if($scope.campaign.recipients_pretty != undefined){
                        angular.forEach($scope.campaign.recipients_pretty,function(value,key){
                            $scope.selectedGroups.push({
                                "label":value.label,
                                "value":value.value,
                                "is_group":value.is_group,
                                "send":true
                            });
                        })

                    }

                    if($scope.campaign.from_name == undefined){
                        $scope.campaign.from_name = PhpDataFactory.course_info.name;
                    }
                    if($scope.campaign.from_email == undefined){
                        $scope.campaign.from_email = PhpDataFactory.course_info.email;
                    }
                    if($scope.campaign.subject == undefined || $scope.campaign.subject == '' ){
                        $scope.campaign.subject = $scope.campaign.name;
                    }
                    $scope.getTextStats();
                })
            }
        }




        $scope.selectedGroup = "";
        $scope.getEmailGroups = function(term){
            var promise = CampaignFactory.searchRecipients(term);
            promise.then(function(response){
                return response;
            })
            return promise;
        }
        $scope.personSelected = function($item, $model, $label){
            $item.send = true;
            if($scope.selectedGroups == undefined)
                $scope.selectedGroups = [];


            $scope.selectedGroups.push($item);

            if($scope.campaign.recipients_pretty === undefined){
                $scope.campaign.recipients_pretty = [];
            }
            if($scope.selectedGroups != $scope.campaign.recipients_pretty){
                $scope.campaign.recipients_pretty.push($item);
            }

            $scope.selectedGroup = "" ;

            $scope.getTextStats();
            $scope.saveCampaign();
        }
        $scope.removeRecipient = function(recipient){
            var index = $scope.selectedGroups.indexOf(recipient);
            if(index >= 0){
                $scope.selectedGroups.splice(index,1);
            }
            $scope.campaign.recipients_pretty = $scope.selectedGroups;

            $scope.getTextStats();
            $scope.saveCampaign();
        }
        $scope.convertPrettyToNormal = function() {
            var groups = [], individuals = [];
            angular.forEach($scope.selectedGroups, function (value, key) {
                if (value.is_group == 1) {
                    groups.push(value.value)
                } else if (!value.isGroup) {
                    individuals.push(value.value)
                }
            });
            $scope.campaign.recipients = {
                individuals: individuals,
                groups: groups
            }

            //$scope.saveCampaign();
        };
        $scope.showTextingInformation = function(){
            dialogs.notify("Texting","Legally, we can only market to those who have texted GOLF to 62687. Only those who have 'subscribed' will receive this marketing message.");
        }
        $scope.getTextStats  = function(){
            $scope.convertPrettyToNormal();
            $http.post('/index.php/marketing_campaigns/ajaxTextSubscriptionStats',$scope.campaign.recipients)
                .success(function(data) {
                    $scope.subscriptionStats = data.data;
                })
                .error(function(msg, code) {
                });
        }
        $scope.enableTextMarketing = function(){
            $scope.enablingTextMarketing = true;
            $http.post('/index.php/marketing_campaigns/ajaxEnableTextMarketing')
                .success(function(data) {
                    dialogs.notify("Texting","Texting has been enabled and all future campaigns will only be sent to those who subscribe to your text marketing.  The subscription texts will be sent out as soon as possible.");
                    $scope.course_info.marketing_texting = 1;
                    $scope.enablingTextMarketing = false;
                })
                .error(function(msg, code) {
                    dialogs.notify("Texting","There was an error enabling text marketing on your account.  Contact support if this keeps happening.");
                    $scope.enablingTextMarketing = false;
                });
        }
        $scope.viewRecipients = function(){
            dialogs.create("/frontend/campaign-builder/app/template_overview/recipients/recipients.html", "recipientsViewerCtrl")
        }
        $scope.init();
    });
