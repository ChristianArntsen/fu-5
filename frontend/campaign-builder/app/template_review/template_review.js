cbApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('app.template_review', {
            url: '/template-review/:campaign_id',
            controller: 'templateReviewCtrl',
            templateUrl: '/frontend/campaign-builder/app/template_review/template_review.html'
        });
}]);

cbApp.controller('templateReviewCtrl',
    function($scope,$stateParams,CampaignFactory,dialogs,$state) {
        function mysqlDate(date){
            date = date || new Date();
            return date.toISOString().split('T')[0];
        }
        function mysqlTime(date){
            date = date || new Date();
            return date.toISOString().split('T')[1].split('.')[0];
        }
        $scope.campaign = {};
        $scope.init = function(){
            $scope.sendInfo = {};
            $scope.minDate = $scope.minDate ? null : new Date();
            $scope.whenToSend = "now";
            if($stateParams.campaign_id != undefined){
                var promise = CampaignFactory.getCampaign($stateParams.campaign_id);
                promise.then(function(data){
                    $scope.campaign = data;
                 })
            }
        }
        $scope.$watch('whenToSend', function(value) {
            if(value == "reminder"){
                $scope.campaign.timebefore = 1;
                $scope.campaign.timebefore_unit = "day";
            }
        });

        $scope.rrule = "";
        $scope.rruleObject = "";

        $scope.$on('completeCampaign', function(e, arg) {
            if($scope.whenToSend=='reminder') {
                $scope.loading = true;
                $scope.campaign.status = "reminder";
                $scope.campaign.queued = true;
                var promise = CampaignFactory.saveCampaign($scope.campaign);
                promise.then(function (data) {
                    var confirmationText = "Your campaign has been scheduled.";
                    dialogs.notify("Congrats!", confirmationText, {size: "md"});
                    $state.go("app.dashboard");
                    $scope.$emit("emailsent");
                })
            } else if($scope.whenToSend=='recurring'){
                $scope.loading = true;
                $scope.campaign.status = "recurring";
                $scope.campaign.rrule = $scope.rrule;
                $scope.campaign.queued = true;
                var promise = CampaignFactory.saveCampaign($scope.campaign);
                promise.then(function(data){
                    var confirmationText  = "Your campaign has been scheduled.";
                    dialogs.notify("Congrats!",confirmationText,{size:"md"});
                    $state.go("app.dashboard");
                    $scope.$emit("emailsent");
                })
            } else {
                $scope.sendCampaign();
            }
        });
        
        $scope.sendCampaign = function(){
            if($scope.campaign.recipients_pretty == undefined){
                $scope.campaign.recipients_pretty = {
                    "groups":[0],
                    "individuals":[]
                }
            }
            if($scope.campaign.recipients_pretty[0] == undefined && CampaignFactory.currentCampaign.recipient_reports.length == 0){
                var promise = dialogs.error("Can't do that.","You haven't specified any recipients yet.  Go back to the first step.",{size:"md"});
                promise.result.then(function(){
                    $scope.$emit("emailfailed");
                })
                return;
            }


            if ($scope.whenToSend!='now' && $scope.whenToSend!='later' && $scope.whenToSend!='reminder') {
                var promise = dialogs.error("Whoops.","The recurring event's aren't working at the moment.  Try again tomorrow.",{size:"md"});

                promise.result.then(function(){
                    $scope.$emit("emailfailed");
                })
                return;
            }
            var promise2 = CampaignFactory.getStats();
            promise2.then(function(data) {
                if(CampaignFactory.currentCampaign.type == "email" && data.email.usage > data.email.limit){
                    dialogs.error("Oops","Seems you don't have enough credits to run another campaign.  Contact support get purchase some new credits or to be put on recurring billing.",{size:"md"});
                    $scope.$emit("emailfailed");
                } else if(CampaignFactory.currentCampaign.type == "text" && data.text.usage > data.text.limit){
                    dialogs.error("Oops","Seems you don't have enough credits to run another campaign.  Contact support get purchase some new credits or to be put on recurring billing.",{size:"md"});
                    $scope.$emit("emailfailed");
                } else {

                    $scope.campaign.queued = true;
                    $scope.campaign.send_date = $scope.sendInfo.date;
                    $scope.campaign.send_now = $scope.whenToSend == "now";
                    $scope.campaign.finishcampaign =  true;
                    $scope.loading = true;
                    var promise = CampaignFactory.saveCampaign($scope.campaign);
                    promise.then(function(data){
                        var confirmationText = "";
                        if($scope.whenToSend == "now"){
                            confirmationText  = "Your campaign has been sent off and will be delivered to your customers soon.";
                        } else {
                            confirmationText  = "Your campaign has been scheduled.";
                        }
                        dialogs.notify("Congrats!",confirmationText,{size:"md"});
                        $state.go("app.dashboard");
                        $scope.$emit("emailsent");
                    })
                }
            });

        }
        $scope.init();
    });
angular.module('rruleRecurringSelect', []).directive('rruleRecurringSelect', [function() {
    return {
        restrict: 'E',
        scope: {
            rule: "=",
            ruleObject: "=",
            okClick: "=",
            cancelClick: "=",
            showButtons: "="
        },
        templateUrl: '/frontend/campaign-builder/app/template_review/rrule_recurring_select.html',
        link: function(scope, elem, attrs) {
            scope.init = function() {
                scope.initFrequencies();
                scope.initWeekOrdinals();
                scope.selectedMonthFrequency = 'day_of_month';
                scope.resetData();
                scope.$watch(scope.currentRule, scope.ruleChanged);

                scope.$watch(function(scope){
                    return scope.dates.start.toUTCString();
                },function (newvalue, oldvalue) {
                    if(oldvalue != newvalue){
                        scope.calculateRRule();
                    }
                });
                scope.$watch(function(scope){
                    return scope.dates.end.toUTCString();
                },function (newvalue, oldvalue) {
                    if(oldvalue != newvalue){
                        scope.calculateRRule();
                    }
                });

                if(!_.isEmpty(scope.rule))
                    scope.parseRule(scope.rule);
                else
                    scope.calculateRRule();
            };

            scope.initFrequencies = function() {
                scope.frequencies = [
                    { name: 'Daily', rruleType: RRule.DAILY, type: 'day' },
                    { name: 'Weekly', rruleType: RRule.WEEKLY, type: 'week' },
                    { name: 'Monthly', rruleType: RRule.MONTHLY, type: 'month' },
                    { name: 'Yearly', rruleType: RRule.YEARLY, type: 'year' }
                ];
                scope.selectedFrequency = scope.frequencies[0];
            };

            scope.initMonthlyDays = function() {
                scope.monthDays = [];
                scope.yearMonthDays = [];

                _.times(31, function(index) {
                    var day = { day: index + 1, value: index + 1, selected: false }
                    scope.monthDays.push(day);
                    scope.yearMonthDays.push(day);
                });
                var lastDay = { day: 'Last Day', value: -1, selected: false };
                scope.monthDays.push(lastDay);
                scope.yearMonthDays.push(lastDay);
            };

            scope.initWeekOrdinals = function() {
                scope.weekOrdinals = ['st', 'nd', 'rd', 'th'];
            };

            scope.initMonthlyWeeklyDays = function() {
                scope.monthWeeklyDays = [];

                _.times(4, function(index) {
                    var days = _.map(scope.daysOfWeek(), function(dayOfWeek){
                        dayOfWeek.value = dayOfWeek.value.nth(index + 1);
                        return dayOfWeek;
                    });
                    scope.monthWeeklyDays.push(days);
                });
            };

            scope.resetData = function() {
                scope.weekDays = scope.daysOfWeek();
                scope.initMonthlyDays();
                scope.initMonthlyWeeklyDays();
                scope.initYearlyMonths();
                scope.selectedYearMonth = 1;
                scope.selectedYearMonthDay = 1;
                scope.interval = 1;
            };
            scope.dates = {
                    start : moment().add(35,'minutes').toDate(),
                    end : moment().add(7, 'days').toDate()
            }

            scope.daysOfWeek = function() {
                return [
                    { name: 'S', value: RRule.SU, selected: false },
                    { name: 'M', value: RRule.MO, selected: false },
                    { name: 'T', value: RRule.TU, selected: false },
                    { name: 'W', value: RRule.WE, selected: false },
                    { name: 'T', value: RRule.TH, selected: false },
                    { name: 'F', value: RRule.FR, selected: false },
                    { name: 'S', value: RRule.SA, selected: false },
                ];
            };

            scope.initYearlyMonths = function() {
                scope.yearMonths = [
                    { name: 'Jan', value: 1, selected: false },
                    { name: 'Feb', value: 2, selected: false },
                    { name: 'Mar', value: 3, selected: false },
                    { name: 'Apr', value: 4, selected: false },
                    { name: 'May', value: 5, selected: false },
                    { name: 'Jun', value: 6, selected: false },
                    { name: 'Jul', value: 7, selected: false },
                    { name: 'Aug', value: 8, selected: false },
                    { name: 'Sep', value: 9, selected: false },
                    { name: 'Oct', value: 10, selected: false },
                    { name: 'Nov', value: 11, selected: false },
                    { name: 'Dec', value: 12, selected: false }
                ];
            };

            scope.selectMonthFrequency = function(monthFrequency) {
                scope.selectedMonthFrequency = monthFrequency;
                scope.resetData();
                scope.calculateRRule();
            };

            scope.toggleSelected = function(day) {
                day.selected = !day.selected;
                scope.calculateRRule();
            };

            scope.calculateRRule = function() {
                switch(scope.selectedFrequency.type) {
                    case 'day':
                        scope.calculateDailyRRule();
                        break;
                    case 'week':
                        scope.calculateWeeklyRRule();
                        break;
                    case 'month':
                        scope.calculateMonthlyRRule();
                        break;
                    case 'year':
                        scope.calculateYearlyRRule();
                }

                if(!_.isUndefined(scope.rule)){
                    scope.rule = scope.recurrenceRule.toString();
                    scope.ruleObject = scope.recurrenceRule;

                }
            };

            scope.calculateInterval = function() {
                var interval = parseInt(scope.interval);
                if (!interval)
                    interval = 1;
                return interval;
            };

            getEndDateObject = function(){
                return moment(scope.dates.end).toDate()
            };
            getStartDateObject = function(){
                return moment(scope.dates.start).toDate()
            };

            scope.calculateDailyRRule = function() {
                console.log(getEndDateObject());
                scope.recurrenceRule = new RRule({
                    freq: RRule.DAILY,
                    interval: scope.calculateInterval(),
                    wkst: RRule.SU,
                    dtstart: getStartDateObject(),
                    until: getEndDateObject()
                });
            };

            scope.calculateWeeklyRRule = function() {
                var selectedDays = _(scope.weekDays).select(function(day) {
                    return day.selected;
                }).pluck('value').value();

                scope.recurrenceRule = new RRule({
                    freq: RRule.WEEKLY,
                    interval: scope.calculateInterval(),
                    wkst: RRule.SU,
                    byweekday: selectedDays,
                    dtstart: getStartDateObject(),
                    until: getEndDateObject()
                });
            };

            scope.calculateMonthlyRRule = function() {
                if(scope.selectedMonthFrequency == 'day_of_month')
                    scope.calculateDayOfMonthRRule();
                else
                    scope.calculateDayOfWeekRRule();
            };

            scope.calculateDayOfMonthRRule = function() {
                var selectedDays = _(scope.monthDays).select(function(day) {
                    return day.selected;
                }).pluck('value').value();

                scope.recurrenceRule = new RRule({
                    freq: RRule.MONTHLY,
                    interval: scope.calculateInterval(),
                    wkst: RRule.SU,
                    bymonthday: selectedDays,
                    dtstart: getStartDateObject(),
                    until: getEndDateObject()
                });
            };

            scope.calculateDayOfWeekRRule = function() {
                var selectedDays = _(scope.monthWeeklyDays).flatten().select(function(day) {
                    return day.selected;
                }).pluck('value').value();

                scope.recurrenceRule = new RRule({
                    freq: RRule.MONTHLY,
                    interval: scope.calculateInterval(),
                    wkst: RRule.SU,
                    byweekday: selectedDays,
                    dtstart: getStartDateObject(),
                    until: getEndDateObject()
                });
            };

            scope.calculateYearlyRRule = function() {
                var selectedMonths = _(scope.yearMonths).flatten().sortBy(function(month){
                    return month.value;
                }).select(function(month) {
                    return month.selected;
                }).pluck('value').value();

                var selectedDays = _(scope.yearMonthDays).flatten().sortBy(function(day){
                    return day.value;
                }).select(function(day) {
                    return day.selected;
                }).pluck('value').value();

                scope.recurrenceRule = new RRule({
                    freq: RRule.YEARLY,
                    interval: scope.calculateInterval(),
                    bymonth: selectedMonths,
                    bymonthday: selectedDays,
                    dtstart: getStartDateObject(),
                    until: getEndDateObject()
                });
            };

            scope.parseRule = function(rRuleString) {
                scope.recurrenceRule = RRule.fromString(rRuleString);

                scope.interval = scope.recurrenceRule.options.interval;

                scope.selectedFrequency = _.select(scope.frequencies, function(frequency) {
                    return frequency.rruleType == scope.recurrenceRule.options.freq;
                })[0];

                switch(scope.selectedFrequency.type) {
                    case 'week':
                        scope.initFromWeeklyRule();
                    case 'month':
                        scope.initFromMonthlyRule();
                }
            };

            scope.initFromWeeklyRule = function() {
                var ruleSelectedDays = scope.recurrenceRule.options.byweekday;

                _.each(scope.weekDays, function(weekDay) {
                    if (_.contains(ruleSelectedDays, weekDay.value.weekday))
                        weekDay.selected = true;
                });
            };

            scope.initFromMonthlyRule = function() {
                if(!_.isEmpty(scope.recurrenceRule.options.bymonthday) || !_.isEmpty(scope.recurrenceRule.options.bynmonthday))
                    scope.initFromMonthDays();
                else if(!_.isEmpty(scope.recurrenceRule.options.bynweekday))
                    scope.initFromMonthWeekDays();
            };

            scope.initFromMonthDays = function() {
                var ruleMonthDays = scope.recurrenceRule.options.bymonthday;
                scope.selectedMonthFrequency = 'day_of_month';

                _.each(scope.monthDays, function(weekDay) {
                    if(_.contains(ruleMonthDays, weekDay.value))
                        weekDay.selected = true;
                });

                if(scope.recurrenceRule.options.bynmonthday.length > 0 && scope.recurrenceRule.options.bynmonthday[0] == -1)
                    scope.monthDays[31].selected = true;
            };

            scope.initFromMonthWeekDays = function() {
                var ruleWeekMonthDays = scope.recurrenceRule.options.bynweekday;
                scope.selectedMonthFrequency = 'day_of_week';

                _.each(ruleWeekMonthDays, function(ruleArray) {
                    var dayIndex = ruleArray[0];
                    var weekIndex = ruleArray[1] - 1;

                    var week = scope.monthWeeklyDays[weekIndex];
                    _.each(week, function(day) {
                        if (day.value.weekday == dayIndex) {
                            day.selected = true;
                            return;
                        }
                    });
                });
            };

            scope.ruleChanged = function() {
                if (!_.isEmpty(scope.rule)) {
                    scope.parseRule(scope.rule);
                }
            };

            scope.currentRule = function() {
                return scope.rule;
            };

            scope.minDate = new Date();
            scope.init();
        }
    }
}]);