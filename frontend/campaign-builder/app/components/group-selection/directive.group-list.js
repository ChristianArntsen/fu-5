cbApp.directive('groupList', function(CampaignFactory) {
    return {
        require: '^groupSelection',
        link: function($scope, $element, $attrs,groupSelectionCtrl) {

        },
        templateUrl:"/frontend/campaign-builder/app/components/group-selection/view.group-list.html",
        controller: function($scope,$attrs,$element){

            $scope.removeRecipient = function(recipient){
                var index = $scope.selectedGroups.indexOf(recipient);
                if(index >= 0){
                    $scope.selectedGroups.splice(index,1);
                }
            }
        }
    };
});