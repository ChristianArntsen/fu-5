cbApp.directive('groupSelection', function(CampaignFactory) {
    return {
        link: function($scope, $element, $attrs) {

        },
        scope:{
            "placeholder":"@"
        },
        templateUrl:"/frontend/campaign-builder/app/components/group-selection/view.group-selection.html",
        controller: function($scope,$attrs,$element){
            if(!$scope.hasOwnProperty("placeholder")){
                $scope.placeholder = "Type to Filter";
            }

            $scope.selectedGroup = "";
            $scope.getEmailGroups = function(term){
                var promise = CampaignFactory.searchRecipients(term);
                promise.then(function(response){
                    return response;
                })
                return promise;
            }
            $scope.personSelected = function($item, $model, $label){
                $item.send = true;
                if($scope.selectedGroups == undefined)
                    $scope.selectedGroups = [];

                $scope.selectedGroups.push($item);
                $scope.selectedGroup = "" ;
            }
        }
    };
});