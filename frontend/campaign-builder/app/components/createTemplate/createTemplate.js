'use strict';

cbApp.controller('customDialogCtrl',function($scope,$modalInstance,data){
    //-- Variables --//

    $scope.template = {name : ''};

    //-- Methods --//

    $scope.cancel = function(){
        $modalInstance.dismiss('Canceled');
    };

    $scope.save = function(){
        $modalInstance.close($scope.template);
    };

    $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.template.name,null) || angular.equals($scope.template.name,'')))
            $scope.save();
    };
})