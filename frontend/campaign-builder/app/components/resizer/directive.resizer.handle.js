cbApp.directive('resizerHandle', function($document) {
    return {
        require: '^resizer',
        link: function(scope, element, attrs, resizerCtrl) {

            element.on('mousedown', function(event) {
                event.preventDefault();
                resizerCtrl.resize(event);
                $document.on('mousemove', resizerCtrl.resize);
                $document.on('mouseup', mouseup);
            });



            function mouseup() {
                $document.unbind('mousemove', resizerCtrl.resize);
                $document.unbind('mouseup', mouseup);
            }
        },
        controller: function($scope){
        }
    };
});