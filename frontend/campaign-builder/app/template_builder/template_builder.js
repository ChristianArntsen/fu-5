cbApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('app.template_builder', {
            url: '/template-builder/:campaign_id',
            controller: 'templateBuilderCtrl',
            templateUrl: '/frontend/campaign-builder/app/template_builder/template_builder.html'
        });
}]);

cbApp.controller('templateBuilderCtrl', function($scope,$state,ComponentFactory,CampaignFactory,$document,$stateParams,PhpDataFactory) {
    $scope.init = function(){
        var promise  = CampaignFactory.getCampaign($stateParams.campaign_id);
        promise.then(function(data){
            if(!data){
                $state.go('app.dashboard');
            }

            $scope.sections = data.json_content;
            $scope.campaign = data;
            $scope.template_details = data.template_details;
            $scope.logo.url = data.logo;
            $scope.logo.align = data.logo_align;
            $scope.course_info = PhpDataFactory.course_info;
        })
    }

    $scope.saveTemplate = function(){
        if($scope.sections ==undefined)
            return;
        $scope.campaign.json_content = $scope.sections;
        $scope.campaign.logo = $scope.logo.url ?  $scope.logo.url : "";
        $scope.campaign.logo_align = $scope.logo.align ?  $scope.logo.align : "";
        CampaignFactory.saveCampaign( $scope.campaign, $scope.campaign.campaign_id);
    }

    $scope.$watch('sections',function(newVal,oldVal){
        $scope.saveTemplate();
    },true)
    $scope.$watch('logo',function(newVal,oldVal){
        $scope.saveTemplate();
    },true)
    
    $scope.$on('watchEditLogo', function() {
        $scope.editLogo();
    });
    $scope.template_is_loading = true;
    $scope.layoutDone = function(){
        console.log("done");
        $scope.template_is_loading = false;
    }
    $scope.init();
    $scope.defaultComponents = [
        {
            type:"button"
        },
        {
            type:"spacer"
        },
        {
            type:"divider"
        },
        {
            type:"text"
        },
        {
            type:"image",
            padding:true
        }
    ];

    $scope.activeSection = {};
    $scope.$on('event:editSection',function(event,data){
        $scope.activeSection = data;
    });
    $scope.$watch(function(){
        return ComponentFactory.activeComponent;
    },function(newVal,oldVal){
        if(newVal != oldVal){
            $scope.activeSection = newVal;
        }
    });
    
    $document.on('click', function(){
        $scope.$apply(function() {
            ComponentFactory.activeComponent = {};
        });
    });
    $scope.endingoverride  = function($item, $part, $index,$target){
        return true;

    };

    $scope.editLogo = function(){
        ComponentFactory.activeComponent = $scope.logo;
    };

    $scope.logo = {
        "type":"image",
        "nopadding":true,
        "align":"left",
        "url":""
    };
    
    var layout1 = {
        "width":["100%"],
        "columns" : [[]]
    };
    
    var layout2 = {
        "width":["50%","50%"],
        "columns" : [[],[]]
    };
    
    var layout3 = {
        "width":["66%","34%"],
        "columns" : [[],[]]
    };

    var layout4 = {
        "width":["34%","66%"],
        "columns" : [[],[]]
    };
    
    $scope.addLayout =function($whichLayout){
        if($scope.sections == undefined)
            $scope.sections = [];
        if($whichLayout == 1){
            $scope.sections.push(angular.copy(layout1));
        } else if($whichLayout == 2){
            $scope.sections.push(angular.copy(layout2));
        } else if($whichLayout == 3){
            $scope.sections.push(angular.copy(layout3));
        } else if($whichLayout == 4){
            $scope.sections.push(angular.copy(layout4));
        }
    }

}).directive('addLayout', ['$document', function($document) {
    return {
        link: function(scope, element, attrs) {
            var button = element.find('.al-outer'),
                addLayout = element.find('.al-float');
            
            button.on('click', function(e) {
                e.stopPropagation();
                button.addClass('isActive');
                addLayout.css('display', 'block');

                setTimeout(function() {
                    addLayout.css({
                        'opacity': '1',
                        '-webkit-transform': 'translate(0,0)',
                        '-ms-transform': 'translate(0,0)',
                        'transform': 'translate(0,0)'
                    });
                }, 100);
            });
            
            angular.element($document).on('click', function() {
                button.removeClass('isActive');
                addLayout.css({
                    'opacity': '0',
                    '-webkit-transform': 'translate(0,40px)',
                    '-ms-transform': 'translate(0,40px)',
                    'transform': 'translate(0,40px)'
                });
                
                setTimeout(function() {
                    addLayout.css('display', 'none');
                }, 500);
            });
        }
    }
}]).directive('repeatDone', function() {
    return function(scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
});