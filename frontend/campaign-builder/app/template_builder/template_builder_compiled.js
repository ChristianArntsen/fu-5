cbApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('app.template_builder2', {
            url: '/template_builder_compiled',
            template: '<div compiledtemplate></div>'
        });
}]);

cbApp.directive('compiledtemplate',
    function(PhpDataFactory,$http,$compile) {
        var tpl = "/frontend/campaign-builder/app/template_builder/template_builder_compiled.html";
        return {
            scope: true,
            link: function(scope, element, attrs){
                scope.course_info = PhpDataFactory.course_info;
                scope.sections = angular.fromJson(PhpDataFactory.prefilledTemplate );
                scope.logo = {
                    "type":"image",
                    "nopadding":true,
                    "url":PhpDataFactory.logo,
                    "align":PhpDataFactory.logo_align
                };
                scope.email_hash = PhpDataFactory.email_hash;
                $http.get(tpl)
                    .then(function(response){
                        //console.log($compile(response.data)(scope).get(0).outerHTML);
                        element.html($compile(response.data)(scope));
                    });
            }
        };

    }
);