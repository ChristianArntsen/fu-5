cbApp.directive('component', function($compile,ComponentFactory) {
    var getTemplate = function(item) {
        if(item == undefined)
            return;
        if (item.type === 'image') {
            return '<component-image component-model="componentModel" ng-click="editItem(componentModel);$event.stopPropagation();"></component-image>';
        } else if (item.type === 'button') {
            return '<component-button component-model="componentModel" ng-click="editItem(componentModel);$event.stopPropagation();"></component-button>';
        } else if (item.type === 'divider') {
            return '<component-divider component-model="componentModel"></component-divider>';
        } else if (item.type === 'spacer') {
            return '<component-spacer component-model="componentModel"></component-spacer>';
        } else if (item.type === 'text') {
            return '<component-text component-model="componentModel"></component-text>';
        }
        return null;
    };
    return {
        restrict: 'E',
        scope: {
            componentModel: '='
        },
        link: function(scope, element) {
            var el = $compile(getTemplate(scope.componentModel))(scope);

            element.replaceWith(el);




            el.bind('click',function(){
                scope.componentModel.active = true;
                scope.$root.$broadcast('event:newactive',el);
            })


            scope.$on('event:newactive',function(event,element){
                if(el != element){
                    scope.componentModel.active = false;
                }
            })

        },
        controller: function($scope,$element,$rootScope){
            $scope.editItem = function(componentModel){
                ComponentFactory.activeComponent = componentModel;
            }
        }
    };
});