cbApp.directive('componentButtonEditor', function() {
    return {
        restrict: 'E',
        scope: {
            activeSection: '='
        },
        link: function(scope, element, attrs, tabsCtrl) {

        },
        templateUrl: '/frontend/campaign-builder/app/template_builder/components/directive.button.editor.html',
        controller: function($scope){
        }
    };
});