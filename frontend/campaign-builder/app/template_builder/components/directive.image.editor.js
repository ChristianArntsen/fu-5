cbApp.directive('componentImageEditor', function() {
    return {
        restrict: 'E',
        scope: {
            activeSection: '='
        },
        link: function(scope, element, attrs, tabsCtrl) {

        },
        templateUrl: '/frontend/campaign-builder/app/template_builder/components/directive.image.editor.html',
        controller: function($scope,ImageFactory){
            $scope.foreupImages = [];
            $scope.pastImages = [];

            $scope.selectImage = function(image){
                $scope.activeSection.url = image.url;
                $scope.activeSection.alt = image.alt;
            }

            $scope.dropzoneConfig = {
                'options': { // passed into the Dropzone constructor
                    'url': 'upload.php',
                    'maxFilesize':'2',//MB
                    'clickable':true,
                    'maxFiles':1,
                    'previewsContainer':'.previewWindow',
                    'createImageThumbnails':false
                },
                'eventHandlers': {
                    'sending': function (file, xhr, formData) {
                        $scope.componentModel.loading = true;
                        $scope.$apply();
                    },
                    'success': function (file, response) {
                    },
                    'error': function (file, errorMessage) {
                    },
                    'complete': function (file) {
                        $scope.componentModel.loading = false;
                        $scope.$apply();
                    },
                    'thumbnail': function (file, dataUrl) {
                    }
                }
            };


            $scope.$watch(ImageFactory.getSavedLists,function(newVal,oldVal){
                if(newVal!=undefined){
                    $scope.pastImages = newVal.myHistory;
                    $scope.foreupImages = newVal.defaults;
                }
            })
            $scope.init = function(){
                ImageFactory.refresh();
            }
            $scope.init()
        }
    };
});