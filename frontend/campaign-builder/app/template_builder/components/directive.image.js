var uploadController = function($scope,ImageFactory){
    $scope.alert;
    $scope.closeAlert = function(){
        delete($scope.alert);
    }
    $scope.dropzoneConfig = {
        'options': { // passed into the Dropzone constructor
            'url': '/index.php/marketing_campaigns/ajaxImageUpload',
            'maxFilesize':'2',//MB
            'clickable':true,
            'maxFiles':1,
            'previewsContainer':'.previewWindow',
            'createImageThumbnails':false,
            'acceptedFiles': "image/*"
        },
        'eventHandlers': {
            'sending': function (file, xhr, formData) {
                $scope.componentModel.loading = true;
                $scope.$apply();
            },
            'success': function (file, response) {
                response = angular.fromJson(response);

                $scope.componentModel.loading = false;
                $scope.componentModel.url = response.url;
                $scope.$apply();
                ImageFactory.refresh();
            },
            'error': function (file, errorMessage) {
                $scope.alert = {
                    "msg":errorMessage,
                    "type":"danger"
                };
                $scope.$apply();
            },
            'complete': function (file) {

            },
            'thumbnail': function (file, dataUrl) {
            },
            'uploadprogress': function(file,progress){
                $scope.progress = progress;
                $scope.$apply();
            },

        }
    };
    
    $scope.editLogo = function() {
        $scope.$emit('watchEditLogo', true);
    }
}

cbApp.directive('componentImage', function() {
    return {
        restrict: 'E',
        scope: {
            componentModel: '='
        },
        link: function(scope, element, attrs, tabsCtrl) {

        },
        templateUrl: '/frontend/campaign-builder/app/template_builder/components/directive.image.html',
        controller:uploadController
    };
});

cbApp.directive('componentLogo', function() {
    return {
        restrict: 'E',
        scope: {
            componentModel: '='
        },
        link: function(scope, element, attrs, tabsCtrl) {

        },
        templateUrl: '/frontend/campaign-builder/app/template_builder/components/directive.image.logo.html',
        controller:uploadController
    };
});