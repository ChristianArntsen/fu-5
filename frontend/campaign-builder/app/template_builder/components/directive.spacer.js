cbApp.directive('componentSpacer', function() {
    return {
        restrict: 'E',
        scope: {
            componentModel: '='
        },
        link: function(scope, element, attrs, tabsCtrl) {

        },
        templateUrl: '/frontend/campaign-builder/app/template_builder/components/directive.spacer.html',
        controller: function($scope){
            if($scope.componentModel.height != undefined)
                $scope.componentModel.height = $scope.componentModel.height;
            else
                $scope.componentModel.height = 25;
        }
    };
});