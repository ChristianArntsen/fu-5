angular.module('notify-toast', []).provider('$notification', function () {
    var settings = {
        info: {
            duration: 7000,
            enabled: true,
            class: 'info'
        },
        warning: {
            duration: 8000,
            enabled: true,
            class: 'warning'
        },
        error: {
            duration: 8000,
            enabled: true,
            class: 'danger'
        },
        success: {
            duration: 8000,
            enabled: true,
            class: 'success'
        },
        progress: {
            duration: 0,
            enabled: true,
            class: ''
        },
        custom: {
            duration: 35000,
            enabled: true,
            class: ''
        },
        details: true,
        localStorage: false,
        templateName: 'ng-notification-template'
    };
    this.setSettings = function (s) {
        angular.extend(settings, s);
    };

    function Notification($timeout, s) {
        var settings = s;
        var notifications = JSON.parse(localStorage.getItem('$notifications')) || [],
            queue = [];

        return {
            
            getSettings: function () {
                return settings;
            },

            /* ============ QUERYING RELATED METHODS ============*/

            getAll: function () {
                // Returns all notifications that are currently stored
                return notifications;
            },

            getQueue: function () {
                return queue;
            },

            /* ============== NOTIFICATION METHODS ==============*/

            info: function (title, content, userData, duration) {
                return this.notify('info', 'info', title, content, userData, duration);
            },

            error: function (title, content, userData, duration) {
                return this.notify('error', 'error', title, content, userData, duration);
            },

            success: function (title, content, userData, duration) {
                return this.notify('success', 'success', title, content, userData, duration);
            },

            warning: function (title, content, userData, duration) {
                return this.notify('warning', 'warning', title, content, userData, duration);
            },

            notify: function (type, icon, title, content, userData, duration) {
                return this.makeNotification(type, false, icon, title, content, userData, duration);
            },

            makeNotification: function (type, image, icon, title, content, userData, duration) {
                var notification = {
                    'type': type,
                    'image': image,
                    'icon': icon,
                    'title': title,
                    'content': content,
                    'timestamp': +new Date(),
                    'userData': userData,
                    'duration': duration,
                    'class': settings[type].class
                };
                notifications.push(notification);

                if (duration === undefined) {
                    duration = settings[type].duration;
                }

                queue.push(notification);
                if (duration) {
                    $timeout(function removeFromQueueTimeout() {
                        queue.splice(queue.indexOf(notification), 1);
                    }, duration);
                }

                this.save();
                return notification;
            },


            /* ============ PERSISTENCE METHODS ============ */

            save: function () {
                // Save all the notifications into localStorage
                if (settings.localStorage) {
                    localStorage.setItem('$notifications', JSON.stringify(notifications));
                }
            },

            restore: function () {
                // Load all notifications from localStorage
            },

            clear: function () {
                notifications = [];
                this.save();
            }
        }
    }

    this.$get = ['$timeout', '$templateCache',
        function ($timeout, $templateCache) {
            if (!$templateCache.get('ng-notification-template')) {
                $templateCache.put('ng-notification-template',
                    '<div class="toaster toaster-{{noti.class}} velocity-opposites-transition-flipBounceXIn" ' +
                    'ng-repeat="noti in queue">' +
                        '<div class="toaster-content">' +
                            '<button type="button" class="close" data-dismiss="modal" ng-click="removeNotification(noti)">'+
                            '<span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' +
                            '<div class="toaster-icon"></div>' +
                            '<div class="toaster-title" ng-bind="noti.title"></div>' +
                            '<div class="toaster-content" ng-bind="noti.content"></div>' +
                        '</div>' +
                    '</div>'
                );
            }
            return new Notification($timeout, settings);
        }
    ];
})

.directive('notifications', ['$notification', '$compile', '$templateCache',
    function ($notification, $compile, $templateCache) {
        /**
         *
         * It should also parse the arguments passed to it that specify
         * its position on the screen like "bottom right" and apply those
         * positions as a class to the container element
         *
         * Finally, the directive should have its own controller for
         * handling all of the notifications from the notification service
         */
        function link(scope, element, attrs) {
            var position = attrs.notifications;
            position = position.split(' ');
            element.addClass('ng-toaster-container');
            for (var i = 0; i < position.length; i++) {
                element.addClass(position[i]);
            }
        }


        return {
            restrict: 'A',
            scope: {},
            template: $templateCache.get($notification.getSettings().templateName),
            link: link,
            controller: ['$scope',
                function NotificationsCtrl($scope) {
                    $scope.queue = $notification.getQueue();

                    $scope.removeNotification = function (noti) {
                        $scope.queue.splice($scope.queue.indexOf(noti), 1);
                    };
                }
            ]

        };
    }
]);