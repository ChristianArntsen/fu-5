angular.module('autoFrame', [])

.directive('autoFrame', function($window) {
    return {
        link: function(scope, element, attrs) {

            scope.$on('iframe-loadeded',function(){
                resize();
            })

            var win = angular.element($window),
                parent = element.parent();

            win.on('resize', function() {
                resize();
            });
            
            function resize() {
                element.height(parent.height());
                //element.width(parent.width());
            }
            
            resize();
        }
    }
});