'use strict';

cbApp.
    factory('ImageFactory', function($http,$q,$log ) {
        var factory = {};

        factory.refresh = function(){
            factory.getAll();
        }
        factory.getSavedLists = function(){
            return factory.savedlists;
        }
        factory.getAll = function(){

            var deferred = $q.defer();
            $http.get('/index.php/marketing_campaigns/ajaxImageList')
                .success(function(data) {
                    deferred.resolve(data.data);
                    factory.savedlists = data.data;
                }).error(function(msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }

        factory.delete = function($id){
            var deferred = $q.defer();
            $http.get('/index.php/marketing_campaigns/ajaxImageDelete?image_id='+$id)
                .success(function(data) {
                    deferred.resolve(data.data);
                }).error(function(msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }

        return factory;
    });