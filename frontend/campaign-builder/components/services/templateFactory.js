'use strict';

cbApp.
    factory('TemplateFactory', function($http,$q,$log ) {
        var factory = {};

        factory.getAll = function(){

            var deferred = $q.defer();
            $http.get('/index.php/marketing_campaigns/ajaxGetTemplates')
                .success(function(data) {
                    deferred.resolve(data.data);
                }).error(function(msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }

        return factory;
    });