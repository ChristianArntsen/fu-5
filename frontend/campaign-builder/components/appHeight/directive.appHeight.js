cbApp.directive('appHeight', function($window) {
    return {
        link: function(scope, element, attrs) {
            
            var win = angular.element($window);
            var headerHeight = 160;
            
            console.log(element.height())
            
            element.height(win.height() - headerHeight);
            
            win.on('resize', function() {
                element.height(win.height() - headerHeight);
            });
            
        }
    }
});