cbApp.filter('capFirstLetter', function() {
    return function(input) {
        if (input != null || input !== undefined) {
            return input.substring(0,1).toUpperCase() + input.substring(1);
        }
    }
});

cbApp.filter('timeStamp', function() {
    return function(input) {
        if (input != null || input !== undefined) {
            return new Date(input);
        }
    }
});