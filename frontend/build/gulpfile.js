var gulp = require('gulp');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var template = require('gulp-template-compile');
var sass = require('gulp-sass');

const JS = '../../js/';
const LIB = 'lib/';
const BOWER = 'bower_components/';
const CSS = '../../css/';
const NPM = 'node_modules/';
const DIST = '../../js/dist/';
const CSS_DIST = '../../css/dist/';

const MAIN = '../main/';
const MAIN_CSS = '../main/css/';

const CAMPAIGN_BUILDER = '../campaign-builder/';
const CAMPAIGN_BUILDER_CSS = '../campaign-builder/resources/styles/';

const ONLINE_BOOKING = '../online_booking/';

/***********************************************************************
 * Main application build functions
 **********************************************************************/
gulp.task('main-compile-templates', function() {
   
	return gulp.src([
		MAIN + 'templates/**/*.html',
		MAIN + 'modules/**/*.html'
	])
	.pipe(template())
	.on('error', function(e){
		console.error(e);
	})
	.pipe(concat('templates.js'))
	.pipe(gulp.dest(DIST));
});

gulp.task('main-combine-js', ['main-compile-templates'], function() {

	return gulp.src([
		JS + 'jquery-1.10.2.min.js',
		JS + 'jquery-ui.1.10.3.min.js',
		JS + 'jquery.keypad.js',
		JS + 'moment.min.js',		
		JS + 'accounting.min.js',
		JS + 'StarBarcodeEncoder.js',
		JS + 'StarWebPrintBuilder.js',
		JS + 'StarWebPrintTrader.js',
		JS + 'taffy.js',
		JS + 'db.js',
		JS + 'webprnt.js',
		JS + 'bootstrap.js',
		NPM + 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
		JS + 'bootstrap-contextmenu.js',
		JS + '.modal.popover.js',
		JS + 'underscore.min.js',
		JS + 'backbone-1.1.2.js',
		BOWER + 'backbone-relational/backbone-relational.js',
		JS + 'backbone.marionette.2.2.0.js',
		JS + 'bootstrapValidator.0.5.3.js',
		JS + 'bootstrap-growl.js',
		JS + 'jquery.loadmask.js',
		JS + 'jquery.cardswipe.js',
		JS + 'fastclick.js',
		JS + 'typeahead.jquery.js',
		JS + 'typeahead.bloodhound.js',
		JS + 'select2.min.js',
		JS + 'ladda-bootstrap-master/dist/spin.js',
		JS + 'ladda-bootstrap-master/dist/ladda.js',
		JS + 'ladda-bootstrap-master/dist/ladda.jquery.min.js',
		BOWER + 'typeahead.js/dist/bloodhound.js',
		BOWER + 'typeahead.js/dist/typeahead.jquery.js',
		JS + 'accounting.min.js',
		JS + 'favico-0.3.10.min.js',
		JS + 'backbone.epoxy.min.js',
		JS + 'datatables/js/jquery.dataTables.js',
		JS + 'dataTables.tableTools.js',
		JS + 'datatables/js/dataTables.fixedHeader.js',
		JS + 'datatables/js/dataTables.scroller.min.js',
		JS + 'datatables/js/dataTables.colResize.js',
		JS + 'datatables/js/dataTables.colReorder.min.js',
		JS + 'bootstrap-daterangepicker/daterangepicker.js',
		JS + 'Chart.js',
		JS + 'highcharts/highcharts.js',
		JS + 'highcharts/theme.js',
		JS + 'ladd-bootstrap-master/dist/ladd.min.js',
		JS + 'bootstrap-multiselect.js',
		NPM + 'decimal.js/decimal.js',
        NPM + 'js-md5/src/md5.js',
		NPM + 'blueimp-load-image/js/load-image.js',
		NPM + 'blueimp-file-upload/js/jquery.fileupload.js',
        NPM + 'papaparse/papaparse.js',
        NPM + 'rrule/lib/rrule.js',
		MAIN + 'semicolon.js',
        NPM + 'backgrid/lib/backgrid.js',
		NPM + 'backgrid-paginator/backgrid-paginator.js',
		NPM + 'backgrid-select-all/backgrid-select-all.js',
		NPM + 'backgrid-moment-cell/backgrid-moment-cell.js',
		NPM + 'backbone.paginator/lib/backbone.paginator.js',
        MAIN + 'modules/_common/models/backbone-json-rest/json-rest-validator.js',
        MAIN + 'modules/_common/models/backbone-json-rest/backbone-json-rest.js',
		BOWER + 'cropper/dist/cropper.js',
		BOWER + 'webcamjs/webcam.js',
		BOWER + 'bootstrap-table/dist/bootstrap-table.js',
		BOWER + 'signature_pad/signature_pad.js',
		BOWER + 'nouislider/distribute/nouislider.js',
		BOWER + 'formatter/dist/jquery.formatter.js',
		LIB + 'please.js',
		NPM + 'summernote/dist/summernote.js',
        NPM + 'bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js',
		BOWER + 'jsbarcode/CODE128.js',
		BOWER + 'jsbarcode/JsBarcode.js',

		JS + 'dist/templates.js',
		MAIN + 'init.js',
        MAIN + 'models/*.js',
        MAIN + 'models/customers/*.js',
        MAIN + 'models/foodbev/*.js',
        MAIN + 'models/gift_cards/*.js',
        MAIN + 'models/rainchecks/*.js',
        MAIN + 'models/item_kits/*.js',
        MAIN + 'models/items/*.js',
        MAIN + 'models/marketing_campaigns/*.js',
        MAIN + 'models/sales/*.js',

		MAIN + 'models/suppliers/*.js',
        MAIN + 'behaviors/**/*.js',
        MAIN + 'collections/**/*.js',
        MAIN + 'views/*.js',
		MAIN + 'views/**/*.js',

		MAIN + 'modules/_common/behaviors/**/*.js',
        MAIN + 'modules/_common/models/*.js',
		MAIN + 'modules/_common/models/**/*.js',
        MAIN + 'modules/_common/views/**/*.js',
		MAIN + 'modules/reporting/behaviors/**/*.js',
		MAIN + 'modules/reporting/models/*.js',
		MAIN + 'modules/reporting/models/report/*.js',
		MAIN + 'modules/reporting/models/report/filter/*.js',
		MAIN + 'modules/reporting/views/*.js',
		MAIN + 'modules/reporting/views/edit/*.js',
		MAIN + 'modules/reporting/views/report/*.js',
		MAIN + 'modules/reporting/views/report/applied_filters/*.js',
		MAIN + 'modules/reporting/views/report/chart/*.js',
		MAIN + 'modules/reporting/views/report/column_selection/*.js',
		MAIN + 'modules/reporting/views/report/filter_selection/*.js',
		MAIN + 'modules/reporting/views/report/filters/*.js',
		MAIN + 'modules/support_tools/views/*.js',
		MAIN + 'modules/support_tools/models/*.js',
		MAIN + 'modules/bulk_edit_support/views/*.js',
		MAIN + 'modules/bulk_edit_support/models/*.js',
		MAIN + 'modules/marketing_campaigns_support/views/*.js',
		MAIN + 'modules/marketing_campaigns_support/models/*.js',
		MAIN + 'modules/service_fees/*.js',
        MAIN + 'modules/customers/**/*.js',
        MAIN + 'modules/settings/**/*.js',
		MAIN + 'modules/items/**/*.js',
		MAIN + 'modules/starter_teesheet/behaviors/**/*.js',
		MAIN + 'modules/starter_teesheet/models/**/*.js',
		MAIN + 'modules/starter_teesheet/views/**/*.js',
        MAIN + 'modules/data_import/**/*.js',
		MAIN + 'modules/member_billing/**/*.js',
		MAIN + 'modules/bulk_edit/**/*.js',
        MAIN + 'modules/tip_transactions/**/*.js',
		MAIN + 'main.js'
	])
	.pipe(concat('main.min.js'))
	.pipe(gulp.dest(DIST));
});

// Combine/minify CSS
gulp.task('main-combine-css', function() {
	
	return gulp.src([
		//MAIN_CSS + 'bootstrap/_variables.scss',
		CSS + 'bootstrap.css',
		//MAIN_CSS + 'bootstrap/*.scss',
		MAIN_CSS + '*.scss',
		JS + 'datatables/css/jquery.dataTables.css',
		JS + 'datatables/css/colReorder.dataTables.min.css',
		JS + 'ladda-bootstrap-master/dist/ladda-themeless.css',
		JS + 'bootstrap-daterangepicker/daterangepicker-bs3.css',
		CSS + 'ui-lightness/jquery-ui-1.8.14.custom.css',
		CSS + 'sass/header-bootstrap.css',
		BOWER + 'fontawesome/css/font-awesome.min.css',
		NPM + 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
		CSS + 'bootstrap-multiselect.css',
		CSS + 'select2.min.css',
		BOWER + 'cropper/dist/cropper.css',
		BOWER + 'bootstrap-table/dist/bootstrap-table.css',
		BOWER + 'nouislider/distribute/nouislider.min.css',
		CSS + 'bootstrap-datepicker.css',
		CSS + 'bootstrapValidator.0.5.3.css',
		CSS + 'loadmask.css',
		CSS + 'icons/*.css',
		NPM + 'summernote/dist/summernote.css',
        NPM + 'bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css',
		MAIN_CSS + '*.css',
		MAIN + '/modules/**/*.css',
		MAIN + '/modules/**/*.scss',
		CSS + 'modal.css'
	])
	.pipe(sass().on('error', sass.logError))
	.pipe(concat('main.min.css'))
	.pipe(minifyCSS())
	.pipe(gulp.dest(CSS_DIST));
});

gulp.task('main', ['main-combine-js', 'main-combine-css', 'main-compile-templates'], function(){
	
	// watch for JS changes
	gulp.watch([
		MAIN + 'models/*.js',
		MAIN + 'models/customers/*.js',
        MAIN + 'modules/customers/**/*.js',
        MAIN + 'modules/customers/**/*.html',
        MAIN + 'modules/settings/**/*.js',
        MAIN + 'modules/settings/**/*.html',
        MAIN + 'models/foodbev/*.js',
		MAIN + 'models/gift_cards/*.js',
		MAIN + 'models/item_kits/*.js',
        MAIN + 'models/items/*.js',
        MAIN + 'models/marketing_campaigns/*.js',
		MAIN + 'models/sales/*.js',
		MAIN + 'models/suppliers/*.js',
		MAIN + 'views/**/*.js',
		MAIN + 'models/**/*.js',
		MAIN + 'modules/_common/**/*.js',
		MAIN + 'modules/reporting/views/**/*.js',
		MAIN + 'modules/reporting/behaviors/**/*.js',
		MAIN + 'modules/reporting/models/**/*.js',
		MAIN + 'modules/reporting/templates/**/*.html',
		MAIN + 'modules/service_fees/*.js',
		MAIN + 'modules/data_import/**/*',
		MAIN + 'modules/bulk_edit/**/*',
		MAIN + 'modules/member_billing/**/*',
        MAIN + 'modules/customers/**/*',
        MAIN + 'modules/settings/**/*',
		MAIN + 'modules/starter_teesheet/**/*',
		MAIN + 'modules/items/**/*',
		MAIN + '*.js',
		MAIN + 'templates/**/*.html'
	],{ interval: 3500 }, function() {
		gulp.run('main-combine-js');
	});

	gulp.watch([
		MAIN + 'templates/**/*.html',
		MAIN + 'modules/**/*.html'
	],{ interval: 3500 }, function() {
		gulp.run('main-compile-templates');
	});

	// watch for CSS changes
	gulp.watch([
		CSS + '*.css',
		CSS + 'sass/reporting.css',
		CSS + 'bootstrap.css',
		CSS + 'bootstrap-datepicker.css',
		MAIN_CSS + '**/*.scss',
		MAIN_CSS + '*.css',
		MAIN_CSS + '**/*.css',
		MAIN + 'modules/**/*.css',
		MAIN + 'modules/**/*.scss',
	],{ interval: 3500 }, function() {
		gulp.run('main-combine-css');
	});
});

/***********************************************************************
 * Online booking build functions
 **********************************************************************/
// Combine/minify all the needed JS files, with main.js being last
gulp.task('online-booking-combine-js', function() {
    
    return gulp.src([
		JS + 'jquery-1.10.2.min.js',
		JS + 'jquery-migrate-1.2.1.min.js',
		JS + 'bootstrap.js',
		JS + 'bootstrap-datepicker.js',
		JS + 'underscore.min.js',
		JS + 'backbone-1.1.2-online-booking.js',
		JS + 'backbone.marionette.js',
		NPM + 'backbone.paginator/lib/backbone.paginator.js',
		JS + 'bootstrapValidator.0.4.5.js',
        JS + 'bootstrap-growl.js',
        JS + 'moment.min.js',
		JS + 'fastclick.js',
		NPM + 'decimal.js/decimal.js',
		JS + 'jquery.loadmask.js',
		BOWER + 'typeahead.js/dist/bloodhound.js',
		BOWER + 'typeahead.js/dist/typeahead.jquery.js',
		BOWER + 'jsbarcode/CODE128.js',
		BOWER + 'jsbarcode/JsBarcode.js',
		JS + 'accounting.min.js',
		ONLINE_BOOKING + 'models/*.js',
		ONLINE_BOOKING + 'views/*.js',
		ONLINE_BOOKING + 'main.js',
	])
	.pipe(concat('online-booking.min.js'))
	.pipe(gulp.dest(DIST));
});

// Combine/minify CSS
gulp.task('online-booking-combine-css', function() {
    
    return gulp.src([
		CSS + 'bootstrap.css',
		CSS + 'bootstrap-theme.css',
		CSS + 'bootstrap-datepicker.css',
		CSS + 'bootstrapValidator.0.4.5.css',
		CSS + 'loadmask.css',
		CSS + 'icons/*.css'
	])
	.pipe(concat('online-booking.min.css'))
	.pipe(minifyCSS())
	.pipe(gulp.dest(CSS_DIST));
});

gulp.task('online-booking', ['online-booking-combine-js', 'online-booking-combine-css'], function(){
	
	// watch for JS changes
	gulp.watch([
		ONLINE_BOOKING + 'views/*.js',
		ONLINE_BOOKING + 'models/*.js',
		ONLINE_BOOKING + '*.js'
	], function(){
		gulp.run('online-booking-combine-js');
	});

	// watch for CSS changes
	gulp.watch([
		CSS + 'bootstrap.css',
		CSS + 'bootstrap-theme.css',
		CSS + 'bootstrap-datepicker.css'
	], function(){
		gulp.run('online-booking-combine-css');
	});
});

gulp.task('marketing', [], function(){

	// watch for CSS changes
	gulp.watch([CAMPAIGN_BUILDER_CSS + 'main.scss'], function() {
		gulp.src(CAMPAIGN_BUILDER_CSS + 'main.scss')
			.pipe(sass().on('error', sass.logError))
			.pipe(gulp.dest(CAMPAIGN_BUILDER_CSS + 'main.css'));
	});
});