var RaincheckDetailsView = Backbone.View.extend({
	tagName: 'div',
	className: 'row',
	template: _.template( $('#template_raincheck_details').html() ),

	events: {
		'click button.print': 'print'
	},

	render: function(){
		
		var attr = this.model.toJSON();
		attr.course_name = App.data.course.get('name');

		this.$el.html(this.template(attr));
		this.$el.find('img.barcode').JsBarcode('RID ' + this.model.get('raincheck_number'), {
			format: 'CODE128',
			displayValue: true,
			fontSize: 18,
			width: 1,
			height: 50
		});
		return this;
	},

	print: function(){
		window.print();
	}
});

var RaincheckView = Backbone.View.extend({
	tagName: 'tr',
	className: '',
	template: _.template( $('#template_raincheck').html() ),

	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});

var RaincheckListView = Backbone.View.extend({
	tagName: 'table',
	className: 'table table-striped table-responsive rainchecks',
	
	initialize: function(){
		this.listenTo(this.collection, 'add remove reset', this.render);
	},
	
	render: function(){
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('<thead><tr> \
			<th class="sale">Raincheck #</th> \
			<th class="notes">Date Issued</th> \
			<th class="date">Expiration</th> \
			<th class="items">Status</th> \
			<th class="total">Total</th> \
			<th style="width: 110px">&nbsp;</th> \
		</tr></thead><tbody></tbody>');
		
		if(this.collection.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderItem, this);
		}

		return this;
	},
	
	renderEmpty: function(){
		this.$el.find('tbody').append('<tr class="purchase empty muted"><td colspan="5">No rainchecks available</td></tr>');
	},
	
	renderItem: function(item){
		this.$el.append( new RaincheckView({model: item}).render().el );
	}
});