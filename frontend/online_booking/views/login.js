var LoginView = Backbone.View.extend({
	
	id: 'login',
	className: 'modal-dialog',
	template: _.template( $('#template_log_in').html() ),
	signupTemplate: _.template( $('#template_register').html() ),
	forgotPasswordTemplate: _.template( $('#template_forgot_password').html() ),
	
	events: {
		'click .btn.login': 'login',
		'click .btn.register': 'showSignup',
		'click .btn.complete': 'register',
		'click .forgot-password': 'showForgotPassword',
		'click .reset-password': 'resetPassword',
		'keypress': 'submitForm'	
	},
	
	submitForm: function(e){

		if(e.keyCode != 13){ return true }
		
		if(this.$el.find('#login_form').length > 0){
			this.login();
		}else{
			this.register();
		}
		
		e.preventDefault();
		return false;
	},
	
	initialize: function(options){
		
		this.listenTo(App.vent, 'error:login', this.showLoginError);
		this.listenTo(App.vent, 'error:signup', this.showSignupError);
		this.listenTo(App.vent, 'error:password-reset', this.showPasswordResetError);
		this.listenTo(App.vent, 'password-reset', this.showPasswordResetSuccess);
		this.listenTo(App.vent, 'error', this.showError);
		
		var selected_schedule = App.data.schedules.findWhere({'selected': true});
		var all_protected = false;
		if(selected_schedule && selected_schedule.get('booking_classes')){
			all_protected = selected_schedule.get('booking_classes').allPasswordProtected();
		}
		
		this.hide_login = false;
		if((options && options.hide_signup) || all_protected){
			this.hide_signup = true;
		}
		
		this.redirect = false;
		if(options && options.redirect){
			this.redirect = options.redirect;
		}

		// If attempting to book a reservation, store the reservation in the view
		if(options && options.reservation){
			this.reservation = options.reservation;			
		
		// If just wanting to log in to app, attach listener to redirect user after login
		}else{
			this.listenTo(App.data.user, 'change:logged_in', this.redirectLogin);
		}
		var view = this;
		
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
	},
	
	resetPassword: function(e){
		e.preventDefault();
		var btn = $(e.currentTarget);
		var form = this.$el.find('#reset_password_form');
		form.bootstrapValidator('validate');
		
		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}			
		
		var username = this.$el.find('input.username').val();
		btn.button('loading');
        var course_id = App.data.course.id
		App.data.user.resetPassword(username,course_id);
		return false;
	},
	
	showError: function(model, response, options){
		this.showLoginError(model, response, options);
		this.showSignupError(model, response, options);
		this.$el.find('button').button('reset');
	},	
	
	showLoginError: function(model, response, options){
		if(this.$el.find('#login_form').data('bootstrapValidator')){
			this.$el.find('#login_form').data('bootstrapValidator').resetForm();
			this.$el.find('#login-error').text(response.responseJSON.msg).show();
		}
		this.$el.find('button').button('reset');
	},
	
	showPasswordResetError: function(model, response, options){
		if(this.$el.find('#reset_password_form').data('bootstrapValidator')){
			this.$el.find('#reset_password_form').data('bootstrapValidator').resetForm();
			this.$el.find('#password-reset-error').text(response.responseJSON.msg).show();
		}
		this.$el.find('button').button('reset');
	},	
	
	showPasswordResetSuccess: function(model, response, options){
		if(this.$el.find('#reset_password_form').data('bootstrapValidator')){
			this.$el.find('#reset_password_form').data('bootstrapValidator').resetForm();
			this.$el.find('#password-reset-success').text('Reset instructions sent').show();
		}
		this.$el.find('button').button('reset');
	},	
	
	showSignupError: function(model, response, options){
		if(this.$el.find('#register_form').data('bootstrapValidator')){
			this.$el.find('#register_form').data('bootstrapValidator').resetForm();
			this.$el.find('#signup-error').text(response.responseJSON.msg).show();
		}
		this.$el.find('button').button('reset');
	},	
	
    close: function() {
		this.remove();
    },

    render: function() {
		var attr = {};
		attr.hide_signup = this.hide_signup;
		
		var html = this.template(attr);
		this.$el.html(html);	

		$('#modal').html(this.el);
		$('#modal').find('input').first().focus();
		ga('send', 'event', 'Online Booking', 'View Login Form', App.data.course.getCurrentSchedule());
		return this;
    },
    
    redirectLogin: function(){
		var path = 'account';
		if(this.redirect){
			path = this.redirect;
		}
		App.router.navigate(path, {trigger: true});
	},
    
    renderSignup: function(event){
		
		var html = this.signupTemplate();
		this.$el.html(html);	

		if($('#modal:hidden').length > 0){
			$('#modal').html(this.el);	
		}
		ga('send', 'event', 'Online Booking', 'View Signup Form', App.data.course.getCurrentSchedule());
		return this;		
	},
	
    renderForgotPassword: function(event){
		
		var html = this.forgotPasswordTemplate();
		this.$el.html(html);	

		if($('#modal:hidden').length > 0){
			$('#modal').html(this.el);	
		}
		return this;		
	},	

    show: function(form){
		if(!form){
			form = 'login';
		}

		if(form == 'login'){
			this.render();
		
		}else if(form == 'signup'){
			this.renderSignup();
		}
		$('#modal').modal();
	},
	
	showSignup: function(){
		this.renderSignup();
		return false;
	},
	
	showForgotPassword: function(e){
		this.renderForgotPassword();
		e.preventDefault();
		return false;
	},	
	
	// Collect username and password and log user in
	login: function(event){	
		
		var form = this.$el.find('#login_form');
		form.bootstrapValidator('validate');
		
		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}		
		
		var reservation = this.reservation;
		var email = this.$el.find('#login_email').val();
		var password = this.$el.find('#login_password').val();
		var loginButton = this.$el.find('button.login');
		loginButton.button('loading');
		
		// Send request to server to log user in
		App.data.user.login(email, password, function(){

			// Once logged in, we have to refresh the tee times because
			// the price of the tee times may have changed according to the user's
			// price class (if set up to do so)
			App.data.times.refresh(false, function(){
				
				if(reservation){	
					
					// Find the new tee time retrieved on the refresh
					var updated_time = App.data.times.findWhere({
						course_id: reservation.get('course_id'),
						time: reservation.get('time')
					});

					if(updated_time){
						
						// Pull updated pricing from refreshed tee time
						var new_data = _.pick(updated_time.toJSON(), [
							'cart_fee', 
							'cart_fee_tax', 
							'cart_fee_tax_rate',
							'green_fee',
							'green_fee_tax_rate',
							'green_fee_tax',
							'booking_fee_price',
							'booking_fee_required'
						]);
						reservation.set(new_data);
						reservation.calculateTotal();
					}

					// If tee time is available for purchase, show payment
					// method selection window
					if(reservation.canPurchase() && reservation.get('total') > 0 || reservation.get('booking_fee_required')){
						var paymentMethod = new PaymentSelectionView({model: reservation});
						paymentMethod.show();
						return false;
					}

					// If a credit card is required to book the time, show credit card selection window
					if(reservation.get('require_credit_card') == 1 && !reservation.get('credit_card_id')){
						var creditCardView = new SelectCreditCardView({model: reservation});
						creditCardView.show();
						return false;
					}

					App.data.user.get('reservations').create(reservation.attributes, {wait: true, error: function(){
						loginButton.button('reset');
					}});								
				}				
			});
		});

		ga('send', 'event', 'Online Booking', 'Login', App.data.course.getCurrentSchedule());
		
		if(event){
			event.preventDefault();
		}
		return false;
	},
	
	// Register a new user account
	register: function(event){
		var view = this;
		var form = this.$el.find('#register_form');
		form.bootstrapValidator('validate');

		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}

		var data = {};
		var reservation = false;
		this.$el.find('button.complete').button('loading');
		
		if(this.reservation){
			reservation = this.reservation;
		}

		data.first_name = $('#register_first_name').val();
		data.last_name = $('#register_last_name').val();
		data.phone = $('#register_phone').val();
		data.email = $('#register_email').val();
		data.password = $('#register_password').val();
		
		// Save the new user to the server, upon success, book reservation
		App.data.user.save(data, {success: function(){
			
			ga('send', 'event', 'Online Booking', 'Signup', App.data.course.getCurrentSchedule());
			
			if(reservation && reservation.isValid()){
				
				view.$el.find('button.complete').button('loading');
				
				// If tee time is available for purchase, show payment
				// method selection window
				if(reservation.canPurchase() && reservation.get('total') > 0 || reservation.get('booking_fee_required')){
					var paymentMethod = new PaymentSelectionView({model: reservation});
					paymentMethod.show();
					return false;
				}

				// If a credit card is required to book the time, show credit card selection window
				if(reservation.get('require_credit_card') == 1 && !reservation.get('credit_card_id')){
					var creditCardView = new SelectCreditCardView({model: reservation});
					creditCardView.show();
					return false;
				}

				App.data.user.get('reservations').create(reservation.attributes, {wait: true, error: function(){
					view.$el.find('button.complete').button('reset');
				}});
			}		
		},
		'error': function(model, response){
			App.vent.trigger('error:signup', model, response);
		}});
		
		if(event){
			event.preventDefault();
		}		
		return false;
	}
});
