var CreditCardPaymentView = Backbone.View.extend({
	
	id: 'credit_card_payment',
	className: 'modal-dialog',
	template: _.template( $('#template_credit_card_payment').html() ),
	etsForm: _.template( $('#template_ets_credit_card_form').html() ),

	events: {
		'click button.ets-submit': 'etsSubmit'
	},
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
	},
	
    close: function() {
		this.remove();
    },
	
    render: function() {	
		
		var attr = this.model.toJSON();
		this.$el.html( this.template(attr) );

		if(!this.model.get('type')){
			this.model.set('type', 'purchase');
		}
		if(!this.model.get('recipient')){
			this.model.set('recipient', false);
		}

		var course_id = this.model.get('course_id');
		var url = SITE_URL + 'credit_card_window/'+ course_id +'/'+ this.model.get('type');
		var view = this;
		var footer = view.$el.find('div.modal-footer');
		var data = this.model.toJSON();

		if(this.model.get('recipient') != 'foreup' && 
			(this.model.get('merchant') == 'ets' || this.model.get('merchant') == 'element' || this.model.get('merchant') == 'apriva')
		){

			if(this.model.get('merchant') == 'ets'){	
				view.$el.find('div.modal-body').html( this.etsForm({amount: this.model.get('total')}) );
			}else{
				view.$el.loadMask();
				footer.hide();
			}

			$.post(url, data, function(response){
				if(view.model.get('merchant') == 'ets'){
					view.$el.find('div.modal-body').append(response);
				}else{
					view.$el.find('div.modal-body').html(response);
				}
			},'html');
		
		}else{
			footer.hide();
			var queryStr = $.param(data);
			var url = SITE_URL + 'credit_card_window/'+ course_id +'/'+ this.model.get('type') +'?'+queryStr;

			this.$el.find('#payment-warning').remove();

			view.$el.find('.modal-body').html('<div id="mercury_loader"><h2>Loading form...</h2>'+
				'<div class="progress progress-striped active">' +
				'<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>' +
					'</div></div>'+

                	'<p class="bg-info" style="padding:10px">Do not refresh this page or press the back button while processing a payment.</p>'+
					'<div id="foreign_currency_info"></div>'+
					'<iframe id="mercury_iframe" onLoad="$(\'#mercury_loader\').remove();" ' +
					'src="'+url+'" style="border: none; display: block; width: 100%; height: 540px; padding: 0px; margin: 0px;"></iframe><small>'+PAYMENT_WARNING+'</small>');
        }
		this.$el.find('#payment-warning a').attr('target', '_blank');
		
		$('#modal').html(this.el);

		return this;
    },

	etsSubmit: function(e){
		$(e.currentTarget).button('loading');
		ETSPayment.submitETSPayment();
		return false;
	},

    show: function(){
		this.render();
		$('#modal').modal();
	}
});