var ReservationConfirmationView = Backbone.View.extend({
	template: _.template( $('#template_confirmation').html() ),
	
	events: {
		'click .pay-now': 'purchaseReservation'
	},
	
	initialize: function(){
		this.listenTo(this.model, 'change:purchased', this.render);
	},
	
    render: function() {

		var attributes = this.model.attributes;

		attributes.user_name = App.data.user.get('first_name') + ' ' + App.data.user.get('last_name');
		attributes.user_email = App.data.user.get('email');
		attributes.foreup_discount_percent = App.data.course.get('foreup_discount_percent');
		attributes.hide_prices = App.settings.hide_online_prices;
		attributes.course_name = '';
		attributes.course_phone = '';

		if(this.model.get('purchased') && this.model.has('purchase_total')){
			attributes.total = attributes.purchase_total;
		}

		if(attributes.foreup_discount == undefined){
			attributes.foreup_discount = false;
		}
		
		if(IS_GROUP){
			var course = App.data.courses.get(attributes.course_id);
			if(course){
				attributes.course_name = course.get('name');
				attributes.course_phone = course.get('phone');
			}			
		}else{
			attributes.course_name = App.data.course.get('name');
			attributes.course_phone = App.data.course.get('phone');			
		}

		attributes.can_book_carts = false;
		if(App.settings.booking_carts == 1){
			attributes.can_book_carts = true;
		}

		this.$el.html(this.template(attributes));
		return this;
    },
    
    purchaseReservation: function(){
		
		var min_required_paying = 2;
		if(App.settings.minimum_players > min_required_paying){
			min_required_paying = App.settings.minimum_players;
		}

		if(this.model.get('pay_players') < min_required_paying){
			this.model.set('pay_players', min_required_paying);
		}		

		this.model.set({
			'discount_percent':App.data.course.get('foreup_discount_percent')
		});
		
		var purchaseView = new PurchaseReservationView({model: this.model});
		purchaseView.show();
	}
});
