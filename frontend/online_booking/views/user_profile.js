var UserProfileView = Backbone.View.extend({
	tagName: 'div',
	className: 'row',
	template: _.template( $('#template_user_profile').html() ),

	events: {
		'click .save-account': 'saveAccountInfo',
		'click .save-login': 'saveLoginInfo',
		'click .add-card': 'addCreditCard',
		'click #filter-transactions': 'filterAccountTransactions',
		'change #transaction-period': 'setAccountTransactionFilter',
		'change #transactions-start, #transactions-end': 'resetTransactionPeriod'
	},
	
	initialize: function(options){
		this.section = false;
		if(options && options.section){
			this.section = options.section;
		}
		this.children = new Backbone.ChildViewContainer();
		this.listenTo(App.vent, 'error', this.showError);
	},
	
	render: function(){
		
		var attr = this.model.attributes;
		attr.show_course_specific_info = true;
		if(IS_GROUP){
			attr.show_course_specific_info = false;
		}
		this.$el.html(this.template(attr));
		
		this.renderSettings();
		this.renderReservations();
		
		if(IS_GROUP){
			return this;
		}

		this.renderCreditCards();
		this.renderPurchases();
		this.renderGiftCards();
		this.renderRainchecks();
		this.renderInvoices();
		this.renderFoodBevSpending();

		this.$el.find('input.datepicker').datepicker();

		if(this.section){
			var tabLink = this.$el.find('#'+this.section+'-tab');
			tabLink.tab('show');
			this.$el.find(tabLink.attr('href')).addClass('active').siblings().removeClass('active');
		}
		
		return this;
	},
	
	renderSettings: function(){
		var settingsTemplate = _.template( $('#template_user_settings').html() );
		var attributes = this.model.attributes;

		this.$el.find('#account-settings').html( settingsTemplate(attributes) );
		this.$el.find('#account_birthday').datepicker({
			format: 'mm/dd/yyyy'
		});
	},
	
	renderReservations: function(){
		var reservations = this.model.get('reservations');
		var view = new ReservationListView({collection: reservations});
		this.children.add(view);
		this.$el.find('#account-reservations').html(  view.render().el );
		this.$el.find('#account-reservations').prepend('<h2>Reservations</h2>');
	},
	
	renderCreditCards: function(){
		var creditCards = this.model.get('credit_cards');
		var view = new CreditCardListView({collection: creditCards});
		this.children.add(view);
		this.$el.find('#account-creditcards').html( view.render().el );
		this.$el.find('#account-creditcards').prepend('<div style="overflow: hidden;"><h2 class="pull-left">Credit Cards</h2> <button class="btn btn-success add-card pull-left" style="margin-top: 20px; margin-left: 15px;">Add Card</button></div>');
	},
	
	renderPurchases: function(){
		var purchases = this.model.get('account_transactions');
		var view = new PurchaseListView({collection: purchases});
		this.children.add(view);
		this.$el.find('#account-transactions').html( view.render().el );
	},

	renderFoodBevSpending: function(){
		
		if(this.model.get('minimum_charge_transactions') === false){
			return false;
		}

		var transactions = this.model.get('minimum_charge_transactions');
		var spending_totals = this.model.get('minimum_charge_totals');
		
		var transactions_view = new MinimumChargeTransactionListView({collection: transactions});
		this.children.add(transactions_view);
		
		var totals_view = new MinimumChargeListView({collection: spending_totals});
		this.children.add(totals_view);

		this.$el.find('#fb-totals').html( totals_view.render().el );
		this.$el.find('#fb-transactions').html( transactions_view.render().el );
	},
	
	renderInvoices: function(){
		var invoices = this.model.get('invoices');
		var view = new InvoiceListView({collection: invoices});
		this.children.add(view);
		this.$el.find('#account-invoices').html( view.render().el );
		this.$el.find('#account-invoices').prepend('<h2 class="invoices">Invoices <span class="label due">$0.00</span></h2>');
	},	
	
	renderGiftCards: function(){
		var giftCards = this.model.get('gift_cards');
		var view = new GiftCardListView({collection: giftCards});
		this.children.add(view);
		this.$el.find('#giftcard-list').html( view.render().el );
	},

	renderRainchecks: function(){
		var rainchecks = this.model.get('rainchecks');
		var view = new RaincheckListView({collection: rainchecks});
		this.children.add(view);
		this.$el.find('#raincheck-list').html( view.render().el );
	},
	
	showError: function(model, response){
		if(model.changed.email){
			this.$el.find('#user-info-error').text(response.responseJSON.msg).show();
		}else{
			this.$el.find('#user-credentials-error').text(response.responseJSON.msg).show();
		}
	},
	
	saveAccountInfo: function(e){
		e.preventDefault();
		
		var form = this.$el.find('form.information');
		form.bootstrapValidator('validate');
		
		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}
		this.$el.find('button.save-account').button('loading');
		var view = this;	
				
		var params = {
			'first_name': this.$el.find('#account_first_name').val(),
			'last_name': this.$el.find('#account_last_name').val(),
			'email': this.$el.find('#account_email').val(),
			'phone': this.$el.find('#account_phone').val(),
			'address': this.$el.find('#account_address').val(),
			'city': this.$el.find('#account_city').val(),
			'state': this.$el.find('#account_state').val(),
			'zip': this.$el.find('#account_zip').val(),
			'birthday': this.$el.find('#account_birthday').val()
		};
		
		this.model.save(params, {wait: true, save_type: 'info', success: function(){
			var success_msg =  $('<span class="success-msg text-success"><span class="glyphicon glyphicon-ok"></span> Saved</span>');
			view.$el.find('button.disabled').after(success_msg);
			
			_.delay(function(msg){
				msg.fadeOut();
			}, 2000, success_msg);
			
			view.$el.find('button').button('reset');	
		
		}, error: function(){
			view.$el.find('button').button('reset');	
		}});
	},

	resetTransactionPeriod: function(e){
		this.$('#transaction-period').val('custom');
	},

	setAccountTransactionFilter: function(e){
		var setting = $(e.currentTarget).val();
		var start = this.$('#transactions-start');
		var end = this.$('#transactions-end');

		switch(setting){
			case 'today':
				start.val( moment().format('MM/DD/YYYY') );
				end.val( moment().format('MM/DD/YYYY') );
			break;
			case 'yesterday':
				start.val( moment().subtract(1, 'day').format('MM/DD/YYYY') );
				end.val( moment().subtract(1, 'day').format('MM/DD/YYYY') );	
			break;
			case 'last_week':
				start.val( moment().subtract(7, 'days').format('MM/DD/YYYY') );
				end.val( moment().format('MM/DD/YYYY') );	
			break;
			case 'last_2_weeks':
				start.val( moment().subtract(14, 'days').format('MM/DD/YYYY') );
				end.val( moment().format('MM/DD/YYYY') );	
			break;
			case 'last_month':
				start.val( moment().subtract(1, 'month').format('MM/DD/YYYY') );
				end.val( moment().format('MM/DD/YYYY') );	
			break;
			case 'last_6_months':
				start.val( moment().subtract(6, 'months').format('MM/DD/YYYY') );
				end.val( moment().format('MM/DD/YYYY') );	
			break;
			case 'last_year':
				start.val( moment().subtract(1, 'year').format('MM/DD/YYYY') );
				end.val( moment().format('MM/DD/YYYY') );	
			break;							
		}

		return false;
	},

	filterAccountTransactions: function(e){
		
		var button = $(e.currentTarget);
		
		var start = '';
		if(this.$('#transactions-start').val() != ''){
			var start = moment(this.$('#transactions-start').val()).format('YYYY-MM-DD');	
		}

		var end = '';
		if(this.$('#transactions-end').val() != ''){
			var end = moment(this.$('#transactions-end').val()).format('YYYY-MM-DD');	
		}
		
		button.button('loading');

		this.model.get('account_transactions').queryParams.start_date = start;
		this.model.get('account_transactions').queryParams.end_date = end;
		this.model.get('account_transactions').fetch({
			data: {
				start_date: start,
				end_date: end
			},

			success: function(){
				button.button('reset');
			},

			error: function(){
				button.button('reset')
			}
		});

		return false;
	},
	
	saveLoginInfo: function(e){
		e.preventDefault();
		
		var form = this.$el.find('form.account');
		form.bootstrapValidator('validate');
		
		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}
		this.$el.find('button.save-login').button('loading');	
		var view = this;
		
		var params = {
			'username': this.$el.find('#account_username').val(),
			'password': this.$el.find('#account_password').val(),
			'current_password': this.$el.find('#account_current_password').val()
		};
		
		this.model.save(params, {wait: true, save_type: 'credentials', success: function(){
			var success_msg =  $('<span class="success-msg text-success"><span class="glyphicon glyphicon-ok"></span> Saved</span>');
			view.$el.find('button.disabled').after(success_msg);
			
			_.delay(function(msg){
				msg.fadeOut();
			}, 2000, success_msg);
			
			view.$el.find('button').button('reset');
				
		}, error: function(){
			view.$el.find('button').button('reset');	
		}});		
	},
	
	addCreditCard: function(e){
		e.preventDefault();
		
		var model = new Backbone.Model({
			course_id: App.data.course.get('course_id'),
			schedule_id: ''
		});
		
		var view = new AddCreditCardView({model: model});
		view.show();
	},
	
	remove: function(){
		this.children.call('remove');
		Backbone.View.prototype.remove.call(this);
	}
});
