var SelectCreditCardView = Backbone.View.extend({
	id: 'select-credit-card',
	className: 'modal-dialog',
	template: _.template( $('#template_select_credit_card').html() ),
	
	events: {
		'click .btn.set-card': 'complete',
		'click .add-card': 'addCard'
	},
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
		this.creditCards =  App.data.user.get('credit_cards').models;
	},
	
    close: function() {
		this.remove();
    },

    render: function() {
		var html = this.template({'credit_cards': this.creditCards});
		this.$el.html(html);
		$('#modal').html(this.el);
		ga('send', 'event', 'Online Booking', 'Select Credit Card', App.data.course.getCurrentSchedule());
		return this;
    },
    
    show: function(){
		this.render();
		$('#modal').modal();
	},
	
	// Display credit card authorization iframe
	addCard: function(event){
		var addCardView = new AddCreditCardView({model: this.model});
		addCardView.show();
		return false;
	},

	showError: function(model, response, options){
		this.$el.find('#booking-error').text(response.responseJSON.msg).show();
		this.$el.find('button').button('reset');
	},	
	
	// Book teetime using selected credit card
	complete: function(event){
		
		var creditCardId = this.$el.find('#booking_credit_card').val();
		if(!creditCardId || creditCardId == 0){
			this.$el.find('#credit-card-error').html('Credit card required to continue. Please add a card.').show();
			return false;
		}
		this.$el.find('#credit-card-error').hide();
		
		var view = this;
		this.model.set('credit_card_id', creditCardId);
		
		// Complete reservation
		if(this.model.isValid()){
			
			var btn = this.$el.find('button.set-card');
			btn.button('loading');
			
			App.data.user.get('reservations').create(this.model.attributes, {wait: true, error: function(model, response){
				
				// If the request fails because of invalid promo code, 
				// revert back to the tee time details screen
				if(response.responseJSON.error_type == 'promo_code'){
					var bookTimeView = new BookTimeView({model: model});
					bookTimeView.show();					
				}else{
					view.showError(model, response);
				}
			}});
		}
		return false;
	}
});
