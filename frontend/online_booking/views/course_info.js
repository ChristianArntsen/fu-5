var CourseInformationView = Backbone.View.extend({
	tag: 'div',
	template: _.template( $('#template_course_info').html() ),

    render: function() {
		var html = this.template(this.model.attributes);
		this.$el.html(html);
		return this;
    }
});
