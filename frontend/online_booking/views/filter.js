var Filter = Backbone.Model.extend({
	
	defaults: {
		'schedule_id': false,
        'schedule_ids': [0],
		'time': '',
		'players': '',
		'holes': '',
		'date': '',
		'booking_class': false,
		'specials_only': 0
	},
	
	initialize: function(){
        this.listenTo(this, 'change:schedule_id', this.setSelectedSchedule);
		this.listenTo(this, 'change:booking_class', this.setBookingClass);
        var schedule_ids = this.get('schedule_ids');
        _.each(App.data.schedules.models, function(schedule){
            schedule_ids.push(schedule.get('teesheet_id'));
        });
        this.set('schedule_ids', schedule_ids);
	},
	
	// If schedule is changed, apply new settings
	setSelectedSchedule: function(){
		
		var id = this.get('schedule_id');
        var ids = this.get('schedule_ids');
		var selected_schedule;
		_.each(App.data.schedules.models, function(schedule){
            if (IS_GROUP) {
                if (ids.length > 1) {//$.inArray(''+schedule.get('teesheet_id'), ids)) {
                    schedule.unset('selected');
                    if (schedule.get('teesheet_id') == '0') {
                        schedule.set('selected', true);
                        selected_schedule = schedule;
                        selected_schedule.set('minimum_players', 1);
                    }
                }
                else if ($.inArray('' + schedule.get('teesheet_id'), ids) != -1) {
                    schedule.set('selected', true);
                    selected_schedule = schedule;
                }
            }
            else if (parseInt(schedule.get('teesheet_id')) == parseInt(id)) {
                schedule.set('selected', true);
                selected_schedule = schedule;
            }
            else {
                schedule.unset('selected');
            }
		});
        if (typeof selected_schedule != 'undefined') {
            App.settings.booking_carts = selected_schedule.get('booking_carts');
            App.settings.minimum_players = selected_schedule.get('minimum_players');
            App.settings.limit_holes = selected_schedule.get('limit_holes');
            App.settings.days_in_booking_window = parseInt(selected_schedule.get('days_in_booking_window'));
            App.settings.online_booking_protected = 0;
            App.settings.online_close_time = selected_schedule.get('online_close_time');
            App.settings.hide_online_prices = (selected_schedule.get('hide_online_prices') == 1);
        }

		this.updateFilterSettings();
	},
	
	// If booking class is changed, apply new settings
	setBookingClass: function(){
		
		var booking_class_id = this.get('booking_class');
		var selected_schedule = App.data.schedules.get( this.get('schedule_id') );
		
		if(booking_class_id && booking_class_id != 0){
			var booking_class = selected_schedule.get('booking_classes').get(booking_class_id);
			
			App.settings.booking_carts = booking_class.get('booking_carts');
			App.settings.minimum_players = booking_class.get('minimum_players');
			App.settings.limit_holes = booking_class.get('limit_holes');
			App.settings.days_in_booking_window = parseInt(booking_class.get('days_in_booking_window')) + parseInt(selected_schedule.get('days_out'));	
			App.settings.online_booking_protected = booking_class.get('online_booking_protected');
			App.settings.online_close_time = booking_class.get('online_close_time');
			App.settings.show_full_details = (booking_class.get('show_full_details') == 1);
			App.settings.hide_online_prices = (booking_class.get('hide_online_prices') == 1);
			App.settings.allow_name_entry = (booking_class.get('allow_name_entry') == 1);

			this.updateFilterSettings();
		
		// If booking class is just being reset. Clear out password protection
		// (in case last booking class was password protected)
		}else{
			App.settings.online_booking_protected = 0;
		}	
	},
	
	// Updates filter settings to match only those allowed
	// (called after change in schedule or booking class)
	updateFilterSettings: function(){
		
		var selected_schedule = App.data.schedules.get( this.get('schedule_id') );

		if(App.settings.show_full_details){
			this.set('players', 0);
		}else if(App.settings.minimum_players != 0){
			this.set('players', App.settings.minimum_players);
		}
		
		if(App.settings.limit_holes != 0){
			var holes = Math.min(parseInt(App.settings.limit_holes), parseInt(selected_schedule.get('holes')));
			this.set('holes', holes);
		}else{
			this.set('holes', parseInt(selected_schedule.get('holes')));
		}		

		var close_time = parseInt(App.settings.online_close_time);
		if(close_time > parseInt(App.data.course.get('close_time'))){
			close_time = parseInt(App.data.course.get('close_time'));
		}
		
		var cur_set_date = moment(this.get('date'), 'MM-DD-YYYY');
		var max_date = moment().add(App.settings.days_in_booking_window, 'days');

		this.set('date', cur_set_date.max(max_date).format('MM-DD-YYYY'));
	}
});

var FilterView = Backbone.View.extend({
	
	tagName: 'div',
	className: '',
	template: _.template( $('#template_filters').html() ),
	
	events: {
		'click .time .btn': 'setTime',
		'click .players .btn': 'setPlayers',
		'click .holes .btn': 'setHoles',
		'click .prevday': 'prevDay',
		'click .nextday': 'nextDay',
		'click .view-booking-rules': 'viewBookingRules',
		'change .schedules': 'setSchedule',
		'change .specials-only': 'setSpecials',
		'change #date-field': 'dateFieldChange',
        'click .dropdown-menu a': 'checkScheduleIDBox'
	},
	
	intitialize: function(){
		this.listenTo(this.model, 'change:schedule_id change:booking_class', this.renderCalendar);
	},
	
	getDateAdjustment: function(){
		var close_time = parseInt(App.settings.online_close_time);
		if(close_time > parseInt(App.data.course.get('close_time'))){
			close_time = parseInt(App.data.course.get('close_time'));
		}
		
		// Check if we need to skip a day in calendar (if we are past close time
		// of online booking)
		var date_adjustment = 0;
		if((close_time - 100) <= parseInt(moment().format('H00'))){
			date_adjustment = 1;
		}

		return date_adjustment;		
	},
	
	render: function(){
		var attributes = this.model.attributes;
		var schedule = App.data.schedules.findWhere({selected:true});

		attributes.show_any_player_button = App.settings.show_full_details;
		attributes.limit_holes = App.settings.limit_holes;
		attributes.minimum_players = parseInt(App.settings.minimum_players);
		attributes.date_adjustment = this.getDateAdjustment();

		attributes.show_booking_rules = true;
		if(IS_GROUP){
			attributes.show_booking_rules = false;
			attributes.show_any_player_button = true;
		}

		this.$el.html(this.template(attributes));
		this.renderCalendar();
		return this;
	},

	dateFieldChange: function(){
		var field = this.$el.find('#date-field');
		var value = field.val();
		var date = moment(value, 'MM-DD-YYYY');

		if(!date){
			field.val(this.model.get('date'));
			return false;
		}
		
		var min_date = moment({hour: 0, minute: 0, second: 0, millisecond: 0});
		var max_date = moment({hour: 0, minute: 0, second: 0, millisecond: 0}).add('days', App.settings.days_in_booking_window);

		if(!date.isAfter(min_date) || !date.isBefore(max_date)){
			field.val(this.model.get('date'));
			return false
		}

		this.$el.find('#date').datepicker('update', value);
		this.setDate(value);
		
		return true;
	},

	renderCalendar: function(){
		var view = this;
		var calendar = this.$el.find('#date');
		var schedule = App.data.schedules.findWhere({'selected': true});
		
		var calStart = moment({hour: 0, minute: 0, second: 0, millisecond: 0});
		var bookingDays = App.settings.days_in_booking_window;
		var calEnd = moment({hour: 0, minute: 0, second: 0, millisecond: 0}).add('days', bookingDays - 1);
		
		calendar.datepicker('remove');
		calendar.find('input').val( this.model.get('date') );

		// Initialize calendar picker
		calendar.datepicker({
			format: 'mm-dd-yyyy',
			startDate: calStart.toDate(),
			endDate: calEnd.toDate()
		});
		
		// When date is changed, set date on model
		calendar.on('changeDate', function(e){
			if(!e.dates[0]){
				return false;
			}
			view.$el.find('#date-menu').val(e.format('mm-dd-yyyy'));
			view.$el.find('#date-field').val(e.format('mm-dd-yyyy'));
			view.setDate(e.format('mm-dd-yyyy'));
		});
			
		var dateMenu = this.$el.find('#date-menu');
		var options = '';
		for(var i = 0; i < bookingDays; i++){
			var dateOption = moment({hour: 0, minute: 0, second: 0, millisecond: 0}).add('days', i);
			options += '<option value="' +dateOption.format('MM-DD-YYYY')+ '">' +dateOption.format('dddd, MMM Do')+ '</option>';
		}
		dateMenu.html(options).val(this.model.get('date'));
		
		dateMenu.off().on('change', function(e){
			var date = $(this).val();
			calendar.datepicker('setDate', date);
			view.setDate(date);
		});
		
		return this;		
	},

    checkScheduleIDBox: function(){
        var clickedRowID = $(event.target).data('value');
        var clickedBoxID = $(event.target).val();
        if (typeof clickedRowID != 'undefined') {
            $('#schedule_checkbox_'+clickedRowID).click();
        }
        if (clickedBoxID == '0' || clickedRowID == '0') {
            if ($('#schedule_checkbox_0').attr('checked')) {
                // Check all
                $('.schedule_checkbox').attr('checked', true);
            }
            else {
                // Un-Check all
                $('.schedule_checkbox').attr('checked', false);
            }
        }
        else if (!$('#schedule_checkbox_'+clickedBoxID).attr('checked') && !$('#schedule_checkbox_'+clickedRowID).attr('checked')) {
            $('#schedule_checkbox_0').attr('checked', false);
        }
    },

	refreshTimes: function(){
		this.collection.refresh();
	},
	
	prevDay: function(){
		var date = this.model.get('date');
		var min_date = moment({hour: 0, minute: 0, second: 0, millisecond: 0});
		var prevDay = moment(date).subtract('days', 1).min(min_date);
		
		this.$el.find('#date').datepicker('setDate', prevDay.toDate());
		this.$el.find('#date-menu').val(prevDay.format('MM-DD-YYYY'));
		this.setDate(prevDay.format('MM-DD-YYYY'));				
	},
	
	nextDay: function(){
		var date = this.model.get('date');
		var max_date = moment({hour: 0, minute: 0, second: 0, millisecond: 0}).add('days', App.settings.days_in_booking_window - 1);
		var nextDay = moment(date).add('days', 1).max(max_date);
		
		this.$el.find('#date').datepicker('setDate', nextDay.toDate());
		this.$el.find('#date-menu').val(nextDay.format('MM-DD-YYYY'));
		this.setDate(nextDay.format('MM-DD-YYYY'));	
	},
	
	setSchedule: function(event){
        if (IS_GROUP) {
            var schedule_checkboxes = $('.schedule_checkbox');
            var schedule_ids = [];
            for (var i = 0 ; i < schedule_checkboxes.length; i++) {
                var sched_box = $(schedule_checkboxes[i]);
                if (sched_box.attr('checked')) {
                    schedule_ids.push(sched_box.val());
                }
            }
            // If changing schedules, clear booking class
            this.model.set({schedule_ids: schedule_ids, booking_class: false});
            if (schedule_ids.length != 1) {
                this.model.set({schedule_id: 0});
            }
            else {
                this.model.set({schedule_id: schedule_ids[0]});
            }
        }
        else {
            var schedule_id = $(event.target).val();
            // If changing schedules, clear booking class
            this.model.set({schedule_id: schedule_id, booking_class: false});
            $(event.target).blur();
        }
		var maxDate = moment().add('days', App.settings.days_in_booking_window);
		var setDate = this.model.get('date');
		var date = moment.min(moment(setDate),maxDate).format('MM-DD-YYYY');
		this.model.set({'date': date});
				
		this.refreshTimes();
        this.renderCalendar();
		App.router.teetimes();
		return true;
	},
	
	setDate: function(value){
		this.model.set('date', value);
		this.refreshTimes();	
	},
	
	setTime: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('time', value);
		this.refreshTimes();
	},

	setSpecials: function(event){
		var checkbox = this.$el.find('input.specials-only');
		var value = 0;
		if(checkbox.prop('checked')){
			value = 1;
		}
		this.model.set('specials_only', value);
		this.refreshTimes();
	},	
	
	setHoles: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('holes', value);	
		this.refreshTimes();	
	},
	
	setPlayers: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('players', value);
		this.refreshTimes();		
	},
	
	viewBookingRules: function(event){
		event.preventDefault();
		var view = new BookingRulesView();
		view.show();
	}
});

var BookingRulesView = Backbone.View.extend({
	
	id: 'booking_rules',
	className: 'modal-dialog',
	template: _.template( $('#template_booking_rules').html() ),
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});			
	},
	
    close: function() {
		this.remove();
    },

    render: function() {
		var attributes = {
			booking_rules: App.data.course.get('booking_rules')
		};
		
		var html = this.template(attributes);
		this.$el.html(html);
		$('#modal').html(this.el);
		return this;
    },
    
    show: function(){
		this.render();
		$('#modal').modal();
	}
});
