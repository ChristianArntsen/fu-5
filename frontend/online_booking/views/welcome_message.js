var WelcomeMessageView = Backbone.View.extend({
	tag: 'div',
	template: _.template( $('#template_welcome_message').html() ),

	events: {
		'click .login': 'login'
	},

    render: function() {
		var attr = {};
		attr = this.model.attributes;
		attr.logged_in = App.data.user.get('logged_in');

		var html = this.template(this.model.attributes);
		this.$el.html(html);
		return this;
    },

	login: function(event){
		var loginView = new LoginView({model: App.data.user});
		loginView.show();
		return false;	
	}
});