var MinimumChargeListView = Backbone.View.extend({
	
	tagName: 'div',
	template: _.template( $('#template_minimum_charges').html() ),

	render: function(){
		
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		
		var data = {
			minimum_charges: false
		};
		data.minimum_charges = this.collection.toJSON();
		this.$el.html( this.template(data) );

		return this;
	}
});