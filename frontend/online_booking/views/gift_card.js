var GiftCardDetailsView = Backbone.View.extend({
	tagName: 'div',
	className: 'row',
	template: _.template( $('#template_gift_card_details').html() ),

	events: {
		'click button.print': 'print'
	},

	render: function(){
		
		var attr = this.model.toJSON();
		attr.course_name = App.data.course.get('name');

		this.$el.html(this.template(attr));
		this.$el.find('img.barcode').JsBarcode(this.model.get('giftcard_number'), {
			format: 'CODE128',
			displayValue: true,
			fontSize: 18,
			width: 1,
			height: 50
		});
		return this;
	},

	print: function(){
		window.print();
	}
});

var GiftCardView = Backbone.View.extend({
	tagName: 'tr',
	className: '',
	template: _.template( $('#template_gift_card').html() ),

	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});

var GiftCardListView = Backbone.View.extend({
	tagName: 'table',
	className: 'table table-striped table-responsive gift-cards',
	
	initialize: function(){
		this.listenTo(this.collection, 'add remove reset', this.render);
	},
	
	render: function(){
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('<thead><tr> \
			<th class="sale">Gift Card #</th> \
			<th class="notes">Notes</th> \
			<th class="date">Expiration</th> \
			<th class="items">Status</th> \
			<th class="total">Balance</th> \
			<th style="width: 100px;">&nbsp;</th> \
		</tr></thead><tbody></tbody>');
		
		if(this.collection.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderItem, this);
		}

		return this;
	},
	
	renderEmpty: function(){
		this.$el.find('tbody').append('<tr class="purchase empty muted"><td colspan="5">No gift cards available</td></tr>');
	},
	
	renderItem: function(item){
		this.$el.append( new GiftCardView({model: item}).render().el );
	}
});
