var Raincheck = Backbone.Model.extend({
	idAttribute: 'raincheck_id',
	defaults: {
		raincheck_number: '',
		expiry_date: false,
		total: 0.00
	}
});

var RaincheckCollection = Backbone.Collection.extend({
	model: Raincheck
});	
