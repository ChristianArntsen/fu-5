var User = Backbone.Model.extend({
	idAttribute: 'user_id',
	
	url: function(){
		return BASE_API_URL + 'users';
	},
	
	initialize: function(){
		this.on('error', this.triggerError);
	},
	
	defaults: {
		logged_in: false,
		first_name: '',
		last_name: '',
		email: '',
		username: '',
		phone: '',
		address_1: '',
		address_2: '',
		city: '',
		state: '',
		zip: '',
		credit_cards: null,
		purchases: null,
		reservations: null,
		rainchecks: null,
		use_loyalty: 0,
		member: 0,
		account_balance: 0,
		member_account_balance: 0,
		invoice_balance: 0,
		minimum_charge_transactions: false,
		minimum_charge_totals: false	
	},
	
	triggerError: function(model, response, options){
		App.vent.trigger('error', model, response);
	},
	
	set: function(attributes, options){
		if(attributes.credit_cards !== undefined && !(attributes.credit_cards instanceof CreditCardCollection)){
			attributes.credit_cards = new CreditCardCollection( attributes.credit_cards );
		}
		if(attributes.reservations !== undefined && !(attributes.reservations instanceof ReservationCollection)){
			attributes.reservations = new ReservationCollection( attributes.reservations );
		}
		if(attributes.account_transactions !== undefined && !(attributes.account_transactions instanceof PurchaseCollection)){
			attributes.account_transactions = new PurchaseCollection(
				attributes.account_transactions, 
				{state: {totalRecords:attributes.account_transactions_total}} 
			);
		}
		if(attributes.gift_cards !== undefined && !(attributes.gift_cards instanceof GiftCardCollection)){
			attributes.gift_cards = new GiftCardCollection( attributes.gift_cards );
		}
		if(attributes.rainchecks !== undefined && !(attributes.rainchecks instanceof RaincheckCollection)){
			attributes.rainchecks = new RaincheckCollection( attributes.rainchecks );
		}
		if(attributes.invoices !== undefined && !(attributes.invoices instanceof InvoiceCollection)){
			attributes.invoices = new InvoiceCollection( attributes.invoices );
		}						
		if(attributes.minimum_charge_totals !== undefined && !(attributes.minimum_charge_totals instanceof Backbone.Collection)){
			attributes.minimum_charge_totals = new Backbone.Collection( attributes.minimum_charge_totals );
		}
		if(attributes.minimum_charge_transactions !== undefined && !(attributes.minimum_charge_transactions instanceof Backbone.Collection)){
			attributes.minimum_charge_transactions = new Backbone.Collection( attributes.minimum_charge_transactions );
		}

		return Backbone.Model.prototype.set.call(this, attributes, options);
	},
	
    // Overwrite save function
    save: function(attrs, options) {
        options || (options = {});
		
		var save_type = '';
		if(options && options.save_type){
			save_type = options.save_type;
		}
		
        // Filter the data to send to the server
        if(save_type == 'info'){
			attrs = _.pick(attrs, 'first_name', 'last_name', 'phone', 'email', 'birthday', 'address', 'city', 'state', 'zip'); 
		
		}else if(save_type == 'credentials'){
			attrs = _.pick(attrs, 'username', 'current_password', 'password');
		}
		
        options.data = JSON.stringify(attrs);
        options.contentType = 'application/json';

        // Proxy the call to the original save function
        Backbone.Model.prototype.save.call(this, attrs, options);
    },
	
	isLoggedIn: function(){
		if(this.get('logged_in')){
			return true;
		}else{
			return false;
		}
	},
	
	resetPassword: function(username, course_id){
		var model = this;
		$.post(this.url() + '/send_password_reset', {username:username, api_key:API_KEY, course_id:course_id}, function(response){
			if(!response.success){
				App.vent.trigger('error:password-reset', model, response);
			}else{
				App.vent.trigger('password-reset', model, response);
			}
		
		}).fail(function(response){
			App.vent.trigger('error:password-reset', model, response);
		});
	},
	
	// Log the user in
	login: function(username, password, successCallback){
		var model = this;

		var booking_class_id = '';
		if(App.data.filters.has('booking_class') && App.data.filters.get('booking_class')){
			booking_class_id = App.data.filters.get('booking_class');
		}
		
		$.post(this.url() + '/login', {username:username, password:password, booking_class_id:booking_class_id, api_key:API_KEY}, function(response){	
			
			// Update user information returned from successful login
			model.set(response);
			
			if(successCallback && typeof(successCallback) == 'function'){
				successCallback();
			}
			
		},'json').fail(function(response){
			App.vent.trigger('error:login', model, response);
		});
	},
	
	logout: function(){
		var model = this;
		$.post(this.url() + '/logout', null, function(response){
			if(response.success){
				model.clear();
			}
		},'json');
	}
});
