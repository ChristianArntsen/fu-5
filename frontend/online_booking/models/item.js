var Item = Backbone.Model.extend({
	idAttribute: 'item_id',
	defaults: {
		unit_price: 0,
		name: ''
	}	
});