var Time = Backbone.Model.extend({

	defaults: {
		players: 4,
		holes: 18,
		carts: false,
		green_fee: 0.00,
		cart_fee: false,
		time: null,
		promo_code: '',
		promo_discount: 0.00,
		has_special: false,
		pay_online: 'no',
		foreup_discount: false,
		cart_fee_tax: 0,
		green_fee_tax: 0,
		player_list: false,
		booking_fee_required: false
	},
	
	initialize: function(){
		// If pricing has carts built in, force cart booking
		if(this.get('rate_type') == 'riding'){
			this.set({'carts': true});
		}
	},
	
	canPurchase: function(){
		if(
			(this.get('foreup_discount') && this.get('players') >= 2) ||
			this.get('pay_online') == 'optional' || 
			this.get('pay_online') == 'required' ||
			this.get('booking_fee_required')
		){
			return true;
		}
		return false;
	}	
});

var TimeCollection = Backbone.Collection.extend({
	url: function(){
		return BASE_API_URL + 'times';
	},
	model: Time,
	
	initialize: function(){
		this.listenTo(this, 'request', this.startLoading);
		this.listenTo(this, 'sync error', this.endLoading);
		this.listenTo(this, 'error', this.error);
		this.message = false;
	},
	
	parse: function(response, options){
        if(options && options.xhr && options.xhr.getResponseHeader('X-Message')){
			this.message = options.xhr.getResponseHeader('X-Message');
		}
        else{
			this.message = false;
		}
		return response;
	},

	error: function(errorVals){
        this.reset();
		return false;
	},
	
	startLoading: function(){
		App.data.status.set('loading', true);
	},
	
	endLoading: function(){
		App.data.status.set('loading', false);
	},
	
	refresh: function(data, callback){
        if(data){
			var params = data;
		}else{
			var params = _.pick(App.data.filters.attributes, 'time', 'date', 'holes', 'players', 'booking_class', 'group_id', 'schedule_id', 'schedule_ids', 'specials_only');
		}
		params.api_key = API_KEY;
		
		var bookingClasses = [];
		var selectedSchedule = App.data.schedules.findWhere({selected: true});
		if(selectedSchedule != undefined){
			if(selectedSchedule.has('booking_classes')){
				bookingClasses = selectedSchedule.get('booking_classes');	
			}
		}
		
		// If the booking class is not set, and the selected schedule has booking classes, do nothing
		if(bookingClasses.length > 0 && !params.booking_class && bookingClasses.length > 0){
			return true;
		}

        var that = this;
		var response = this.fetch({
			reset: true,
			data: params,
            error:function(collection, response) {
                if (typeof response.responseJSON != 'undefined') {
                    that.message = response.responseJSON.msg;
                }
            },
            success: function(){
            	if(typeof callback == 'function'){
            		callback();
            	}
            }
		});
	}
});
