var Reservation = Backbone.Model.extend({
	idAttribute: 'teetime_id',

    defaults: {
		players: 4,
		holes: 18,
		carts: false,
		green_fee: 0.00,
		cart_fee: false,
		total: 0.00,
		purchased: false,
		pay_players: 0,
		pay_carts: 0,
		pay_total: 0,
		pay_subtotal: 0,
		paid_player_count: 0,
		discount_percent: 0,
		discount: 0,
		has_special: false,
		special_discount_percentage: 0,
		promo_code: '',
		promo_discount: 0.00,
		group_id: 0,
		green_fee_tax_rate: false,
		cart_fee_tax_rate: false,
		player_list: false,
		booking_fee_required: true,
		details: '',
        booking_class_id: 0,
        pending_reservation_id: ''
	},

	initialize: function(){
	    this.set('pending_reservation_id', pending_reservation_obj.get_id());
	    this.on('change:players change:carts', this.calculateTotal);
        this.on('change:pay_players change:pay_carts', this.calculatePurchaseTotal);
		this.on('error', this.triggerError, this);
		this.calculateTotal();
	},
	
	calculateTotal: function(event){
		this.set({'pay_carts': this.get('carts'), 'pay_players': this.get('players')});

		var players = this.get('players');
		var carts = (this.has('carts') && this.has('cart_fee') && this.get('cart_fee') && this.get('carts'));

		var green_fee = this.calculateItemTotal(
			this.get('green_fee'),
			this.get('players'),
			0,
			this.get('green_fee_tax_rate')
		);

		var cart_fee = {total: 0, subtotal: 0, tax: 0};
		if(carts){
			var cart_fee = this.calculateItemTotal(
				this.get('cart_fee'),
				this.get('players'),
				0,
				this.get('cart_fee_tax_rate')
			);
		}

		var promo_discount = new Decimal(0);
		if(this.has('promo_discount') && this.get('promo_discount')){
			promo_discount = new Decimal(this.get('promo_discount'));
		}

		var subtotal = new Decimal(green_fee.total).plus(cart_fee.total);
		var total = subtotal.minus(promo_discount);
		
		this.set({
			'subtotal': subtotal.toDP(2).toNumber(),
			'total': total.toDP(2).toNumber()
		});
	},

	calculateItemTotal: function(price, quantity, discount_percent, tax_rate){

		if(!price || price == undefined){
			price = 0;
		}
		if(!discount_percent || discount_percent == undefined){
			discount_percent = 0;
		}
		var discount_percent = new Decimal(discount_percent);

		if(!tax_rate || tax_rate == undefined){
			tax_rate = 0;
		}		
		if(!discount_percent.isZero()){
			discount_percent = discount_percent.dividedBy(100);
		}

		var total = new Decimal(0);
		var discount_amount = new Decimal(price).times(quantity).times(discount_percent).toDP(2);
		var subtotal = new Decimal(price).times(quantity).toDP(2).minus(discount_amount).toDP(2);
		var tax = new Decimal(this.calculateTax(subtotal, tax_rate));
		var total = subtotal.plus(tax);
		return {
			subtotal: subtotal.toDP(2).toNumber(),
			tax: tax.toDP(2).toNumber(), 
			total: total.toDP(2).toNumber()
		}
	},

	calculateTax: function(subtotal, tax_rate){
		
		if(!tax_rate || !subtotal || subtotal == 0 || tax_rate == 0){
			return 0;
		}
		var rate = new Decimal(tax_rate);
		if(rate.isZero() || subtotal.isZero()){
			return 0;
		}
		
		rate = rate.dividedBy(100);
		return subtotal.times(rate).toDP(2).toNumber();
	},
	
	calculatePurchaseTotal: function(event){
		
		var players = this.get('pay_players');
		var carts = (this.has('pay_carts') && this.get('pay_carts') && this.has('cart_fee') && this.get('cart_fee'));
		var discount = App.data.course.get('foreup_discount_percent');
		
		var promo_discount = new Decimal(0);
		if(this.has('promo_discount') && this.get('promo_discount')){
			promo_discount = new Decimal(this.get('promo_discount'));
		}

		var discounted_green_fee = this.calculateItemTotal(
			this.get('green_fee'),
			this.get('pay_players'),
			discount,
			this.get('green_fee_tax_rate')
		);

		var discounted_cart_fee = {total: 0, subtotal: 0, tax: 0};
		if(carts){
			var discounted_cart_fee = this.calculateItemTotal(
				this.get('cart_fee'),
				this.get('pay_players'),
				discount,
				this.get('cart_fee_tax_rate')
			);
		}

		var green_fee = this.calculateItemTotal(
			this.get('green_fee'),
			this.get('pay_players'),
			0,
			this.get('green_fee_tax_rate')
		);

		var cart_fee = {total: 0, subtotal: 0, tax: 0};
		if(carts){
			var cart_fee = this.calculateItemTotal(
				this.get('cart_fee'),
				this.get('pay_players'),
				0,
				this.get('cart_fee_tax_rate')
			);
		}		
		
		var original_total = new Decimal(green_fee.total).plus(cart_fee.total);
		var discounted_total = new Decimal(discounted_green_fee.total).plus(discounted_cart_fee.total);
		var discount_amount = original_total.minus(discounted_total);

		// If tee time has special applied from course, or a promo code applied, 
		// do not offer extra ForeUp discount
		if (this.get('has_special')){
			discounted_total = original_total;
			discount_amount = new Decimal(0);			
		}
		
		if(promo_discount.greaterThan(0)){
			discounted_total = new Decimal(this.get('total'));
			discount_amount = new Decimal(0);				
		}
		
		this.set({
			'pay_subtotal': original_total.toDP(2).toNumber(),
			'discount': discount_amount.toDP(2).toNumber(),
			'pay_total': discounted_total.toDP(2).toNumber()
		});
	},	
	
	validate: function(){
	    var schedule = App.data.schedules.findWhere({'selected':true});
		var filters = App.data.filters;
		var booking_class = false;
		
		if(schedule && filters.get('booking_class')){
			booking_class = schedule.get('booking_classes').get(filters.get('booking_class'));
		}
		
		// Check if user is logged in
		if(!App.data.user.get('logged_in')){
			return 'log_in_required';
		}	
	},
	
	triggerError: function(model, response, options){
		App.vent.trigger('error', model, response);
	},
	
	canPurchase: function(){
		if(this.canGetForeUpDiscount() || this.canPayCourseOnline()){
			return true;
		}
		return false;
	},

	canPayCourseOnline: function(){
		return (this.get('pay_online') == 'optional' || this.get('pay_online') == 'required');
	},

	canGetForeUpDiscount: function(){
		return (this.get('foreup_discount') && this.get('players') >= 2)
	}
});

var ReservationCollection = Backbone.Collection.extend({
	
	url: function(){
		return BASE_API_URL + 'users/reservations';
	},
	model: Reservation,
	
	initialize: function(){
		this.on('sync', this.reservationMade, this);
		this.on('error', this.reservationError, this);
    },
	
	validate: function(reservation, callback){
		var data = _.extend({validate_only: true}, reservation);

		$.ajax({
			url: this.url(),
			method: 'POST',
			data: JSON.stringify(data),
			dataType: 'json',
			contentType: 'application/json'
		}).done(function(data){
		    if(typeof(callback) == 'function'){
				callback(true);
			}
		
		}).fail(function(data){
			if(typeof(callback) == 'function'){
				callback(false, data.responseJSON.msg);
			}
		});
	},

	reservationMade: function(reservation, response, options){
        if (reservation.get('update') == 1) {
            return true;
        }

	    // Remove/modify time on teetime page
		var selectedTime = App.data.times.findWhere({'time': reservation.get('time'), 'schedule_id': reservation.get('schedule_id')});
		
		if(selectedTime){
			var availableSpots = selectedTime.get('available_spots');
			var spotsLeft = availableSpots - reservation.get('players');
		
			if(spotsLeft <= 0){
				App.data.times.remove(selectedTime);
			}else{
				selectedTime.set('available_spots', spotsLeft);
			}
		}
        pending_reservation_obj.remove_count_down_timer();
		App.vent.trigger('reservation', reservation);
	}
});	
