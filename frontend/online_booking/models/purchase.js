var Purchase = Backbone.Model.extend({
	idAttribute: 'trans_id',
	
	defaults: {
		trans_amount: 0.00,
		customer_note: null
	}
});

var PurchaseCollection = Backbone.PageableCollection.extend({
	model: Purchase,
	mode: 'server',

	state: {
		pageSize: 25
	},

	parseState: function (resp, queryParams, state, options) {
		return {
			totalRecords: parseInt(options.xhr.getResponseHeader("X-Total")),
			currentPage: parseInt(options.xhr.getResponseHeader("X-Page")),
			pageSize: parseInt(options.xhr.getResponseHeader("X-Per-Page"))
		};
	},

	url: function(){
		var course_id;
		if(App.data.filters.get('course_id') === undefined){
			course_id = App.data.course.get('course_id')
		} else {
			course_id = App.data.filters.get('course_id');
		}
		return BASE_API_URL + 'courses/' + course_id + '/users/account_transactions';
	}
});	
