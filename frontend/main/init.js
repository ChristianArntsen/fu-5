// Override default backbone toJSON method to account for nested objects
Backbone.Model.prototype.toJSON = function() {
	if (this._isSerializing) {
		return this.id || this.cid;
	}
	this._isSerializing = true;
	var json = _.clone(this.attributes);
	_.each(json, function(value, name) {
		value && _.isFunction(value.toJSON) && (json[name] = value.toJSON());
	});
	this._isSerializing = false;
	return json;
}

var Base64 = {
	
	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	// public method for encoding
	encode : function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;

		input = Base64._utf8_encode(input);

		while (i < input.length) {

			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output +
			Base64._keyStr.charAt(enc1) + Base64._keyStr.charAt(enc2) +
			Base64._keyStr.charAt(enc3) + Base64._keyStr.charAt(enc4);

		}

		return output;
	},

	// public method for decoding
	decode : function (input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;

		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		while (i < input.length) {

			enc1 = Base64._keyStr.indexOf(input.charAt(i++));
			enc2 = Base64._keyStr.indexOf(input.charAt(i++));
			enc3 = Base64._keyStr.indexOf(input.charAt(i++));
			enc4 = Base64._keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

		}

		output = Base64._utf8_decode(output);

		return output;

	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}
		return string;
	}
}

// Underscore init, custom functions
_.mixin({

	round: function(value, places) {
		if(typeof(value) != 'number'){
			value = parseFloat(value);
		}
		if(isNaN(value)){
			value = 0;
		}
		if(!places){
			places = 2;
		}
		var neg = false;
		if(value < 0){
			neg = true;
			value = Math.abs(value);
		}

		value = +(Math.round(value + "e+" + places)  + "e-" + places);
		if(neg){
			return 0 - value;
		}
		return value;
	},

	getFormData: function(element){
		var parameters = {};
		element.find("input, textarea, select").each(function() {
			
			var inputType = this.tagName.toUpperCase() === "INPUT" && this.type.toUpperCase();
			if (inputType !== "BUTTON" && inputType !== "SUBMIT"){
				
				if($(this).attr('disabled')){
					parameters[$(this).attr('name')] = null;
				
				}else if($(this).attr('type') == 'checkbox' && $(this).prop('checked') == false){
					if($(this).data('unchecked-value') === undefined){
						var value = null;
					}else{
						var value = String($(this).data('unchecked-value'));
					}
					parameters[$(this).attr('name')] = value;
				
				}else if($(this).attr('type') == 'radio' && $(this).prop('checked') === true){
					parameters[$(this).attr('name')] = String($(this).val());
				
				}else if($(this).attr('type') != 'radio'){
					parameters[$(this).attr('name')] = String($(this).val());
				}
			}					
		});

		return parameters;
	},

	dropdown: function(data, attrs, current_value){
		var container = $('<div />');
		var menu = $('<select />').attr(attrs);
		container.append(menu);

		var options = '';
		_.each(data, function(value, key){
			var selected = '';
			if(key == current_value){
				selected = 'selected';
			}
			options += "<option value='"+key+"' "+selected+">"+value+"</option>";
		});

		menu.html(options);
		return container.html();
	},
	orderedDropdown: function(data, attrs, current_value){
		var container = $('<div />');
		var menu = $('<select />').attr(attrs);
		container.append(menu);

		var options = '';
		var orderedList = [];
		_.each(data,function(value,key){
			orderedList.push({
				"key":key,
				"value": value
			})
		});
		orderedList = _.sortBy(orderedList,"value");
		_.each(orderedList, function(value, key){
			var selected = '';
			if(value.key == current_value){
				selected = 'selected';
			}
			options += "<option value='"+value.key+"' "+selected+">"+value.value+"</option>";
		});

		menu.html(options);
		return container.html();
	},
	capitalize: function(str) {
		if(typeof(str) !== 'string'){ return str; }
		return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
	},
	
	// Filters parenthesis () and *. Handles characters not filtered
	// by browsers encodeURI() function, but still cause problems with Apache
	filterSpecialChars: function (str) {

		return str.replace(/['()]/g, escape). // i.e., %27 %28 %29
			replace(/\*/g, '%2A');
	},
	
	formatPhoneNumber: function(phone){
		if(!phone || phone == '' || phone == null){
			return '';
		}
		// First, strip everything but numbers
		phone = phone.replace(/[^0-9]/g, '');
		return '('+phone.substr(0, 3) + ') ' + phone.substr(3, 3) + '-' + phone.substr(6,4);	
	},
	
	shorten: function(str, maxLength){
		if(str.length > maxLength + 3){
			return str.substring(0, maxLength) + '...';
		}
		return str;
	},
	
	isBlank: function(str){
		if(typeof str === 'undefined' || !str || isNaN(str)){
			return true;
		}
		return false;
	},

	getNumber: function(number, fallback){

		if(!fallback || fallback == undefined){
			fallback = 0;
		}
		if(typeof(number) == 'undefined' || number === null || number === false){
			return fallback;
		}

		var num = number.split(/ /)[0].replace(/[^\d.]/g, '')

		if(num == ''){
			number = fallback;
		}
		return num;
	},

	getNumberWNegative: function(number, fallback){
		var number = number.split(/ /)[0].replace(/[^\d.\-]/g, '')
		if(!fallback || fallback == undefined){
			fallback = 0;
		}

		if(number == ''){
			number = fallback;
		}
		return number;
	},

    'sortKeysBy': function (obj, comparator) {
        var keys = _.sortBy(_.keys(obj), function (key) {
            return comparator ? comparator(obj[key], key) : key;
        });

        return _.object(keys, _.map(keys, function (key) {
            return obj[key];
        }));
    },

	similarity: function(s, t){

		var d = []; //2d matrix

		// Step 1
		var n = s.length;
		var m = t.length;

		if (n == 0) return m;
		if (m == 0) return n;

		//Create an array of arrays in javascript (a descending loop is quicker)
		for (var i = n; i >= 0; i--) d[i] = [];

		// Step 2
		for (var i = n; i >= 0; i--) d[i][0] = i;
		for (var j = m; j >= 0; j--) d[0][j] = j;

		// Step 3
		for (var i = 1; i <= n; i++) {
			var s_i = s.charAt(i - 1);

			// Step 4
			for (var j = 1; j <= m; j++) {

				//Check the jagged ld total so far
				if (i == j && d[i][j] > 4) return n;

				var t_j = t.charAt(j - 1);
				var cost = (s_i == t_j) ? 0 : 1; // Step 5

				//Calculate the minimum
				var mi = d[i - 1][j] + 1;
				var b = d[i][j - 1] + 1;
				var c = d[i - 1][j - 1] + cost;

				if (b < mi) mi = b;
				if (c < mi) mi = c;

				d[i][j] = mi; // Step 6

				//Damerau transposition
				if (i > 1 && j > 1 && s_i == t.charAt(j - 2) && s.charAt(i - 2) == t_j) {
					d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + cost);
				}
			}
		}

		// Step 7
		var lev_distance = d[n][m];
		return (1 - (lev_distance / (Math.max(n, m)))) * 100;
	}
});

function init_category_search(obj, callback){

	// Typeahead item suggestion engine
	var search = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote:  {
			url: API_URL + '/items/categories?q=%QUERY',
			wildcard: '%QUERY'
		},
		limit: 100
	});

	search.initialize();

	obj.typeahead({
		hint: false,
		highlight: true,
		minLength: 1
	},
	{
		limit: 101,
		name: 'categories',
		displayKey: 'name',
		source: search.ttAdapter(),
		templates: {
			suggestion: _.template('<div><%-name%></div>')
		}

	}).on('typeahead:selected', function(e, item, list){
		if(typeof(callback) == 'function'){
			return callback(e, item, list);	
		}
	});
}

function init_department_search(obj, callback){

	// Typeahead item suggestion engine
	var search = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote:  {
			url: API_URL + '/items/departments?q=%QUERY',
			wildcard: '%QUERY'
		},
		limit: 100
	});

	search.initialize();

	obj.typeahead({
		hint: false,
		highlight: true,
		minLength: 1
	},
	{
		limit: 101,
		name: 'departments',
		displayKey: 'name',
		source: search.ttAdapter(),
		templates: {
			suggestion: _.template('<div><%-name%></div>')
		}

	}).on('typeahead:selected', function(e, item, list){
		if(typeof(callback) == 'function'){
			return callback(e, item, list);	
		}
	});
}

function init_item_search(obj, callback, params){

	var queryString = '';
	if(params && params != undefined){
		queryString = '&' + $.param(params);
	} 

	// Typeahead item suggestion engine
	var itemSearch = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote:  {
			url: API_URL + '/items?q=%QUERY' + queryString,
			wildcard: '%QUERY',
			transform: function(response){
				return response.rows;
			}
		},
		limit: 100
	});

	itemSearch.initialize();

	var templates = {
		suggestion: JST['item_search_result.html']
	};

	if(params && params.show_add_button){
		templates.notFound = JST['item_search_add_new.html'];
	};

	obj.typeahead({
		hint: false,
		highlight: true,
		minLength: 1
	}, {
		limit: 101,
		name: 'items',
		displayKey: 'name',
		source: itemSearch.ttAdapter(),
		templates: templates

	}).on('typeahead:selected', function(e, item, list){
		return callback(e, item, list);
	});

	obj.on('typeahead:asyncrequest', function(e){
		obj.parent().prepend('<span class="fa fa-spinner fa-spin input-loading"></span>');
	});

	obj.on('typeahead:asynccancel typeahead:asyncreceive', function(e){
		$('span.input-loading').remove();
	});
}

function init_customer_search(obj, callback, params){
	var queryString = '';
	if(params && params != undefined){
		queryString = '&' + $.param(params);
	} 

	// Typeahead customer suggestion engine
	var customerSearch = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote:  {
			url: API_URL + '/customers?q=%QUERY' + queryString,
			wildcard: '%QUERY',
			cache: false
		},
		limit: 30
	});

	customerSearch.initialize();

	obj.typeahead('destroy').typeahead({
		hint: false,
		highlight: true,
		minLength: 1,
	},
	{
		limit: 101,
		name: 'customers',
		display: function(customer){
			return customer.first_name+' '+customer.last_name;
		},
		source: customerSearch.ttAdapter(),
		async: true,
		templates: {
			suggestion: JST['search_customer.html']
		}

	}).off('typeahead:selected').on('typeahead:selected', function(e, customer, list){
		customerSearch.clearRemoteCache();
		return callback(e, customer, list);
	});

	obj.data('Bloodhound', customerSearch);
}

function init_employee_search(obj, callback){

	// Typeahead employee suggestion engine
	var employeeSearch = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: API_URL + '/employees?q=%QUERY',
			wildcard: '%QUERY'
		},
		limit: 30
	});

	employeeSearch.initialize();

	obj.typeahead({
		hint: false,
		highlight: true,
		minLength: 1
	},
	{
		limit: 101,
		name: 'employees',
		displayKey: function(employee){
			return employee.first_name +' '+ employee.last_name;
		},
		source: employeeSearch.ttAdapter(),
		templates: {
			suggestion: _.template('<div><%-first_name%> <%-last_name%></div>')
		}

	}).on('typeahead:selected', function(e, employee, list){
		return callback(e, employee, list);
	});
}

if(SETTINGS.currency_symbol == 0 ) SETTINGS.currency_symbol = "$";
accounting.settings.currency.symbol  = SETTINGS.currency_symbol;
