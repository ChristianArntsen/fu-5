var LoginWindowView = ModalView.extend({

	tagName: "div",
	id: "login_window",
	template: JST['login.html'],

	initialize: function(options) {
		if(options && options.closable){
			this.closable = options.closable;
		}
		if(options && options.backdropOpacity){
			this.backdropOpacity = options.backdropOpacity;
		}
		ModalView.prototype.initialize.call(this, options);
	},

	events: {
		"click button.login": "submit",
		"keypress input": "submitForm"
	},

	render: function() {
		var attr = {};
		attr.closable = true;
		if(typeof(this.closable) != undefined){
			attr.closable = false;
		}

		this.$el.html(this.template(attr));
		return this;
	},

	onShow: function(){
		this.$el.find('#login-username').focus();
	},

	submitForm: function(e){
		if(e.keyCode == 13){
			e.preventDefault();
			this.submit();
		}
		return true;
	},

	submit: function(){
		var data = {};
		data.username = this.$el.find('input.username').val();
		data.password = this.$el.find('input.password').val();
		data.cart_id = App.data.cart.get('cart_id');

		if(App.data.course.get('terminals').getActiveTerminal()){
			var terminal = App.data.course.get('terminals').getActiveTerminal();
			if(terminal){
				data.terminal_id = terminal.get('terminal_id');
			}
		}
		var view = this;

		$.post(SITE_URL + '/api/authentication/login', data, function(response){
			view.hide();

		},'json').fail(function(){
			App.vent.trigger('notification', {msg: 'Invalid pin', type: 'error'});
		});
	}
});
