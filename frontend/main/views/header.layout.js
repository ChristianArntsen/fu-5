var HeaderApplicationMenu = Backbone.Marionette.ItemView.extend({
	tagName: 'li',
	className: 'dropdown',
	template: JST['header/application_menu.html'],
	ui:{
		"dropdownMenu":".dropdown-menu"
	},
	events:{
		//"click @ui.dropdownMenu":"handleMenuClick"
	},
	initialize: function(){
		this.listenTo(App.data.user, 'change:person_id', this.render);
		this.listenTo(App.router, 'route', this.setPage);
	},
	setPage: function(){
		this.$el.find('#current-page').html(App.router.page);
		return true;
	},
});

var HeaderUserMenu = Backbone.Marionette.ItemView.extend({
	tagName: 'li',
	className: 'dropdown',	
	template: JST['header/user_menu.html'],

	initialize: function(){
		this.listenTo(App.data.user, 'change:person_id', this.render)
	}
});

var HeaderLayout = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	className: 'container-fluid',
	template: JST['header.layout.html'],

	events: {
		"click #menu_timeclock": "showTimeclock",
		"click #menu_logout": "logout"
	},
	initialize:function(){
        this.listenTo(App.router, 'route', this.handleSelection);
	},
	handleSelection: function(){
		this.$el.find("#main-menu-collapse").removeClass('in');
	},
	regions: {
		header_content: '#page-header-content',
		stats: '#page-stats',
		application_menu: '#application-menu',
		user_menu: '#user-menu',
		unread_messages: '#unread-messages'
	},
	onRender:function(){


        if(SETTINGS.two_way_text == 1){
            var message_threads = new MessageThreadsLayout({
                collection: new MessageThreadCollection()
            });
            this.unread_messages.show(message_threads);
        }
	},
	showTimeclock: function(e){
		var timeclock_window = new TimeclockWindowView();
		timeclock_window.show();
		e.preventDefault();	
	},
	
	logout: function(e){
		
		var has_log = false;
		var register_log = App.data.register_logs.get_open_log(undefined, true);
		if(register_log){
			has_log = true;
		}

		// If user has skipped register log, or course doesn't use register logs
		if(App.data.course.get_setting('use_register_log') == 0){
			has_log = false;
		}

		if(has_log){
			var option_window = new LogoutOptionWindow({register_log_open: has_log});
			option_window.show();
			return false;		
		}

		window.location = SITE_URL + '/home/logout';
		return false;
	}
});