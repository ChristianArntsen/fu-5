var AuthorizeWindowView = ModalView.extend({

	tagName: "div",
	id: "authorize_window",
	template: JST['authorize.html'],

	initialize: function(options) {
		this.action = false;
		if(options && options.action){
			this.action = options.action;
		}
		if(options && options.closable){
			this.closable = options.closable;
		}
		if(options && options.backdropOpacity){
			this.backdropOpacity = options.backdropOpacity;
		}
		this.show_nav = true;
		if(options && options.show_nav != undefined){
			this.show_nav = options.show_nav;
		}
		ModalView.prototype.initialize.call(this, options);
	},

	events: {
		"click div.keypad button": "numberPress",
		"keypress input.pin": "submitForm",
		"onCardswipe": function(){ this.$el.find('div.modal-content').loadMask() },
		"cardswipe": "submit"
	},

	render: function() {
		var attr = {};
		var view = this;
		attr.action = this.action;
		
		attr.closable = true;
		if(typeof(this.closable) != undefined){
			attr.closable = false;
		}

		this.$el.html(this.template(attr));
		this.$el.find('input.pin').cardSwipe();

		setTimeout(function(){
			view.$el.find('input.pin').focus();
		}, 50);

		return this;
	},

	onShow: function(){
		if(this.show_nav){
			var z_index = parseInt($('#modal-authorize_window').css('z-index'));
			z_index += 2;
			_.defer(function(){ $('nav').css('z-index', z_index) });			
		}
	},

	submitForm: function(e){
		if(e.keyCode == 13){
			e.preventDefault();
			this.submit();
		}
		return true;
	},

	numberPress: function(e){
		var value = $(e.currentTarget).data('char');
		var field = this.$el.find('input.pin');

		if(value == 'del'){
			field.val( field.val().substr(0, field.val().length - 1) );
		}else if(value == 'enter'){
			this.submit();
		}else{
			field.val( field.val() + '' + value);
		}
	},

	submit: function(){
		var pin = this.$el.find('input.pin').val().replace('%','!');
		var view = this;

		if(this.action == 'Login'){
			$.post(FOOD_BEV_URL + '/authenticate', {'pin': pin}, function(response){
				if(!response.success){
					App.vent.trigger('notification', {msg: 'Invalid pin', type: 'error'});
					view.$el.find('input.pin').val('');
					return true;
				}
				view.completeAction(response);

			},'json').fail(function(){
				App.vent.trigger('notification', {msg: 'Invalid pin', type: 'error'});
			});

		}else{
			$.post(API_URL + '/authentication', {'pin': pin}, function(response){
				if(response.success && response.employee_id){
					view.completeAction(response);
				}

			}).fail(function(response){
				App.vent.trigger('notification', {'type': 'error', 'msg': response.responseJSON.msg});
			});
		}
	},

	completeAction: function(response){

		var auth_window = this;

		if(this.action == 'Cancel Sale'){
            if((!response.user_level || response.user_level < 2) && (App.data.food_bev_table.get("orders_made") > 0)){
                App.vent.trigger('notification', {msg: 'An order has already been sent to the kitchen.  Unable to cancel. Please find manager for Admin Override.', type: 'error'});

                return
            }
			if(!App.data.food_bev_table.close()){
				App.vent.trigger('notification', {msg: 'Please finish paying receipts or refund payments to cancel sale', type: 'error'});
			}
		}

		if(this.action == 'Admin Override'){
			console.log(response);
			if(!response.user_level || response.user_level < 2){
				App.vent.trigger('notification', {msg: 'Insufficient permissions or incorrect credentials. Please find manager for Admin Override.', type: 'error'});

				return
			}
			App.data.food_bev_table.set('admin_auth_id', response.employee_id);
			App.data.food_bev_table.get('receipts').trigger('admin');
		}

		if(this.action == 'Employee PIN'){
			App.data.cart.set({
				'employee_pin': response.pin,
				'employee_id': response.employee_id
			});
			App.data.cart.complete();
		}

		if(this.action == 'Login'){
			App.data.user.set(response.employee);
		}

		if(this.action === false){
			this.trigger('authorized', response);
		}
		
		$('nav').css('z-index', '');
		this.hide();
	}
});
