var TeeSheetIframeLayout = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	className: 'row',
    template: function(){ return ''; },
    
    onShow: function(){
        
        if($('#tee_sheet_iframe').length == 0) {
            // Create a new iframe and attach it to the body of the document
            $('<iframe class="page-iframe" src="' + SITE_URL + '/teesheets?in_iframe=1" id="tee_sheet_iframe"></iframe>').appendTo('#page-iframe-container');

        }else{
            // Update tee sheet
            $.ajax({
                type: "POST",
                url: SITE_URL + "/teesheets/get_newest_tee_times/",
                data: '',
                success: function(response){
                    var events = $.merge(response.caldata, response.bcaldata);
                    $('#tee_sheet_iframe')[0].contentWindow.Calendar_actions.update_teesheet(events, true, 'auto_update');
                },
                dataType:'json'
            });

            $('#tee_sheet_iframe').show();
        }

        //$('body').addClass('iframe');
    },

    onBeforeDestroy: function () {
        // Hide tee sheet iframe
        $('#tee_sheet_iframe').hide();
        $('body').removeClass('iframe');
    }
});

var TeesheetToMarketingModalView = ModalLayoutView.extend({
    template: JST['tee_sheet/tee_sheet_to_marketing.html'],
    initialize: function(){
        var options = {
            extraClass: 'modal-lg'
        };
        ModalView.prototype.initialize.call(this, options);
        this.model = new Backbone.Model({
            "start":this.collection.start,
            "end":this.collection.end,
        })
        this.listenTo(this.collection, 'sync', this.render);
        this.listenTo(this.model, 'change', this.refresh);
    },
    refresh:function(){
        $(".modal-content").loadMask();
        this.collection.fetch({
            success:function(){
                $.loadMask.hide();
            },
            "data":{
                "start":this.model.get("start"),
                "end":this.model.get("end")
            }
        })
    },
    events:{
        "blur #start":"startChanged",
        "blur #end":"endChanged",
        "click #sendCampaign":"sendCampaign"
    },
    sendCampaign:function(){
        $(".modal-content").loadMask();
        var selected = this.$el.find('table.recipients').bootstrapTable("getSelections");
        var selected_ids = _.pluck(selected,"person_id");
        this.createLinkedCampaign(selected_ids);
    },
    startChanged:function(){
        this.model.set("start",$("#start").data("DateTimePicker").date().format());
    },
    endChanged:function(){
        this.model.set("end",$("#end").data("DateTimePicker").date().format());
    },
    createLinkedCampaign:function(selected_ids){
        $.ajax({
            type: "POST",
            dataType:'json',
            url: "/index.php/marketing_campaigns/ajaxSaveCampaign",
            contentType:"application/json; charset=utf-8",
            data: JSON.stringify({data:{
                title:"Bookings Between: "+moment(this.model.get("start")).format("MMM DD, YYYY h:mm")+" - "+moment(this.model.get("end")).format("MMM DD, YYYY h:mm"),
                subject: "News about your booking",
                recipients:{
                    individuals:selected_ids,
                    groups: []
                }
            }}),
            success: function(response){
                window.location.href = "/marketing_campaigns/campaign_builder#/template-overview/"+response.data.campaign_id;
            }
        });
    },
    onRender: function(){

        var view = this;
        $('#start').datetimepicker({
            format: "MMM D h:mm A",
            defaultDate:this.model.get("start"),
            useCurrent: false
        });
        $('#end').datetimepicker({
            format: "MMM D h:mm A",
            defaultDate:this.model.get("end"),
            useCurrent: false
        });


        this.$el.find('table.recipients').bootstrapTable({
            classes: 'table-no-bordered',
            data: this.collection.toJSON(),
            uniqueId: 'person_id',
            clickToSelect:true,
            columns: [
                {
                    checkbox: true,
                    formatter: function(val, row, index){
                        if(row.opt_out_email == "1" && (row.opt_out_text == "1" || row.texting_status != "subscribed")){
                            return false;
                        }
                        return true
                    }
                },
                {
                    title: 'Customer Name',
                    field: 'first_name',
                    sortable: true,
                    formatter: function(val, row, index){
                        if(!val || !row.person_id){
                            return '-';
                        }
                        return row.first_name+' '+row.last_name;
                    }
                },
                {
                    title: 'Email Address',
                    sortable: true,
                    field: 'email'
                },
                {
                    title: 'Mobile Phone Number',
                    field: 'cell_phone_number',
                    sortable: true,
                    formatter: function(val, row, index){
                        if(row.msisdn)
                            return row.msisdn;
                        if(!val && !row.phone_number){
                            return '-';
                        }

                        if(!val && row.phone_number)
                            return "(H) "+row.phone_number;
                        return val;
                    }
                },
                {
                    title: 'Time',
                    field: 'start_datetime',
                    sortable: true,
                    formatter: function(val, row, index){

                        return moment(val).format("MMM D h:mm A");
                    }
                },
                {
                    title: ' ',
                    formatter: function(val, row, index){
                        var email_style="";
                        var phone_style="";
                        if(row.opt_out_email == "1"){
                            email_style = "color:red;"
                        } else {
                            email_style = "color:green;"
                        }
                        if(row.opt_out_text == "1" || row.texting_status != "subscribed"){
                            phone_style = "color:red;"
                        } else {
                            phone_style = "color:green;"
                        }
                        var formatted = '<i class="fa fa-mobile" style="margin: 0 5px;'+phone_style+'" aria-hidden="true"></i> <i class="fa fa-envelope" style="margin: 0 5px;'+email_style+'" aria-hidden="true"></i>';
                        //return row.opt_out_email+' '+row.opt_out_text;
                        return formatted;
                    }
                }
            ],
            pagination: false,
            search: true,
            onLoadSuccess: function(data){
                view.collection.reset(data.rows);
            }
        });

        this.$el.find('table.recipients').bootstrapTable("getData")

        this.$el.find('div.search').prepend('<span class="fa fa-search input-icon"></span>');
        //this.toggle_table_actions();
    }
});