var ItemTableView = Backbone.Marionette.ItemView.extend({
	
	tagName: 'div',
	className: 'col-xs-12',
	template: JST['items/item_table.html'],

	events: {
		'click .edit': 'edit',
		'click .delete': 'deleteItem',
		'check.bs.table': 'toggle_table_actions',
		'uncheck.bs.table': 'toggle_table_actions',
		'check-all.bs.table': 'toggle_table_actions',
		'uncheck-all.bs.table': 'toggle_table_actions'
	},

	initialize: function(){
		this.listenTo(this.collection, 'sync', this.updateRow);
		this.listenTo(this.collection, 'destroy', this.deleteRow);
	},

	toggle_table_actions: function(){
		
		var selections = this.$el.find('table').bootstrapTable('getSelections');
		if(!selections || selections.length == 0){
			this.$el.find('.table-toolbar button').prop('disabled', true);
		}else{
			this.$el.find('.table-toolbar button').prop('disabled', false);
		}
	},

	updateRow: function(model){
		var index = this.collection.indexOf(model);
		var rowData = model.toJSON();

		this.$el.find('table.items').bootstrapTable('updateRow', {
			index: index,
			row: rowData
		});
	},

	deleteRow: function(model){
		this.$el.find('table.items').bootstrapTable('removeByUniqueId', model.get('item_id'));
		this.toggle_table_actions();
	},

	edit: function(e){
		var link = $(e.currentTarget);
		var item = this.collection.at(link.data('index'));
		
		var itemKitWindow = new EditItemView({model: item});
		itemKitWindow.show();

		return false;
	},

	deleteItem: function(e){
		var link = $(e.currentTarget);
		var item = this.collection.at(link.data('index'));		

		if(confirm('Are you sure you want to delete this item?')){
			item.destroy({wait: true});
		}
		this.toggle_table_actions();
		return false;
	},

	onRender: function(){
		
		var url = this.collection.url + '?item_type=item&include_inactive=1';
		var view = this;

		this.$el.find('table.items').bootstrapTable({
			classes: 'table-no-bordered',
			url: url,
			data: this.collection.toJSON(),
			uniqueId: 'item_id',
			striped: true,
			sidePagination: 'server',
			columns: [
				{
					checkbox: true
				},
				{
					title: 'UPC',
					field: 'item_number',
					sortName: 'item_number',
					sortable: true
				},
				{
					title: 'Name',
					field: 'name',
					sortable: true,
					formatter: function(val, row, index){
						var name = val;
						if(row.inactive == 1){
							name += ' <span class="label label-danger">Inactive</span>';
						}
						if(row.is_shared == 1){
							name += ' <span class="label label-info">Shared</span>';
						}
						return name;
					}
				},
				{
					title: 'Department',
					field: 'department',
					sortable: true
				},
				{
					title: 'Category',
					field: 'category',
					sortable: true
				},
				{
					title: 'Sub-Category',
					field: 'subcategory',
					sortable: true
				},
				{
					title: 'Unit Cost',
					field: 'cost_price',
					sortable: true
				},			
				{
					title: 'Unit Price',
					field: 'unit_price',
					sortable: true
				},
				{
					title: 'Tax Rate(s)',
					field: 'taxes',
					formatter: function(val, row, index){
						if(!val || val.length == 0){
							return '-';
						}
						var text = '';
						_.each(val, function(tax){
							text += accounting.formatNumber(tax.percent, 3) + '% '
						});
						return text;
					},
					sortable: false
				},
				{
					title: 'Qty',
					field: 'inventory_level',
					sortable: true,
					formatter: function(val, row, index){
						if(row.inventory_unlimited == 1){
							return '-';
						}
						return val;
					}
				},
				{
					formatter: function(val, row, index){
						return '<div class="btn-group pull-right"><button type="button" data-index="'+index+'" class="btn btn-default btn-xs delete text-danger">Delete</button>' +
							'<button type="button" data-index="'+index+'" class="btn btn-default btn-xs edit">Edit</button></div>';
					}
				}
			],
			page: 1,
			pagination: true,
			pageSize: 50,
			pageNumber: 1,
			pageList: [10,25,50,100],
			search: true,
			toolbar: $('<div class="table-toolbar"><button class="btn btn-default text-danger delete">Delete</button><button class="btn btn-default print-barcode-sheet">Print Barcode Sheet</button><button class="btn btn-default print-barcodes">Print Barcodes</button></div>')[0],
			onLoadSuccess: function(data){
				view.collection.reset(data.rows);
			}
		});

		this.$el.find('div.search').prepend('<span class="fa fa-search input-icon"></span>');
		this.toggle_table_actions();
	}
});