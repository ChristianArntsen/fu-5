var PassItemRuleEditView = ModalLayoutView.extend({
	template: JST['items/pass_item_rule_edit.html'],
	
	regions: {
		conditions: '#rule-conditions',
		item_search: '#item-search'
	},

	events: {
		'click .new-rule-condition': 'new_condition',
		'click .save': 'save',
		'click .cancel': 'cancel',
		'change .rule-type':'type_change',
		'submit form': function(){ return false; }
	},

	type_change:function(){
		var rule_type = this.$el.find('input.rule-type:checked').val();
		this.temp_model.set('type', rule_type);
	},
	serializeData: function(){
		return this.temp_model.toJSON();
	},

	initialize: function(){
		var options = {
			extraClass: 'modal-lg',
			closable: false
		};
		ModalView.prototype.initialize.call(this, options);
		this.temp_model = new PassItemRule( this.model.toJSON() );


		this.listenTo(this.temp_model, 'change:type', this.render);
	},

	onRender: function(){
		var conditions = this.temp_model.get('conditions');
		this.conditions.show( new PassItemRuleConditionsView({collection: conditions}) );

		if(this.temp_model.get("type") == "items"){
			this.item_search.show( new SearchForItemsView({
					collection: this.temp_model.get('items')
				})
			);
		}

	},

	new_condition: function(){
		this.temp_model.get('conditions').add( new PassItemRuleCondition() );
		return false;
	},

	save: function(){
		var form_data = _.getFormData( this.$el.find('form') );
		var data = this.temp_model.toJSON();
		
		data.name = form_data.name;
		data.price_class_id = form_data.price_class_id;

		if(!data.name || data.name == ''){
			App.vent.trigger('notification', {msg: 'Rule name is required', type: 'error'});
			return false;
		}

		// Filter out any conditions that didn't have a field set
		if(data.conditions && data.conditions.length > 0){
			_.each(data.conditions, function(condition, index){
				if(!condition.field){
					data.conditions.splice(index);
				}
			});
		}else{
			data.conditions = [];
		}

		this.model.set(data);
		this.hide();
	},

	cancel: function(){

		if(!this.model.get('saved')){
			this.model.collection.remove( this.model );
		}
		this.hide();
		return false;
	}
});