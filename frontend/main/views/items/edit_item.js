var EditItemView = ModalLayoutView.extend({

	id: 'item_window',
	template: JST['items/edit_item.html'],

	events: {
		"click button.save": "save",
		"change #item-max-discount": "change_max_discount",
		"change #item-price": "change_price",
		"change #item-inventory-unlimited": "change_inventory_unlimited",
		"change input.item-type": "change_item_type",
		"click a.delete-item": "deleteItem"
	},

	initialize: function(){
		var options = {
			extraClass: 'modal-lg',
			closable: false
		};
		ModalView.prototype.initialize.call(this, options);
		this.customer = false;

		this.temp_model = new Item(this.model.toJSON());
		this.startListening();
	},

	regions: {
		'pass_settings': '#item-pass-settings',
		'upcs': '#item-upc-container',
		'customer_groups': '#item-customer-groups',
		'service_fees': '#item-service-fees'
	},

	startListening: function(){
		var self = this;

		this.listenTo(this.temp_model, 'change:unit_price change:max_discount', this.render_minimum_price);
		this.listenTo(this.temp_model, 'change:inventory_unlimited', this.render_inventory_unlimited);
		this.listenTo(this.temp_model, 'change:item_type', this.render);
		
		this.listenTo(this.model, 'sync', function(model){ 
			self.temp_model.set(model.toJSON());
		});

		this.listenTo(this.model, 'error', App.errorHandler);
	},

	serializeData: function(){
		return this.temp_model.toJSON();
	},

    onRender: function(){
		
		this.render_minimum_price();
		this.render_inventory_unlimited();

		if(this.temp_model.get('item_type') == 'pass'){
			
			var pass_settings_view = new PassItemEditView({
				model: this.temp_model, 
				collection: this.temp_model.get('pass_restrictions')
			});	
			this.pass_settings.show(pass_settings_view);
		}

		init_department_search(this.$el.find('#item-department'));
		init_category_search(this.$el.find('#item-category'));
		init_category_search(this.$el.find('#item-subcategory'));

		if(this.temp_model.get('item_type') != 'service_fee'){
			if(!this.temp_model.get('service_fees')){
				this.temp_model.set('service_fees',new ItemServiceFees());
			} 
			this.upcs.show( new ItemUPCListView({
					collection: this.temp_model.get('upcs')
				})
			);
			this.service_fees.show( new ItemServiceFeesView({
					collection: this.temp_model.get('service_fees')
				})
			);						
		}

		this.customer_groups.show( new ItemCustomerGroupsView({
				collection: this.temp_model.get('customer_groups')
			})
		);

		return this;
    },

	change_item_type: function(){
		var item_type = this.$el.find('input.item-type:checked').val();
		var data = this.getFormDataForModel(this.$el.find('form'));
		this.temp_model.set(data);
		this.temp_model.set('item_type', item_type);
	},

	change_price: function(){
		this.temp_model.set('unit_price', this.$el.find('#item-price').val());
	},

	change_max_discount: function(){
		this.temp_model.set('max_discount', this.$el.find('#item-max-discount').val());
	},

	change_inventory_unlimited: function(e){
		var checkbox = this.$el.find('#item-inventory-unlimited');
		if(checkbox.is(':checked')){
			this.temp_model.set('inventory_unlimited', 1);
		}else{
			this.temp_model.set('inventory_unlimited', 0);
		}
	},

    render_minimum_price: function(){
        var price = this.temp_model.get('unit_price');
        if (typeof price.replace == 'function'){
            price = price.replace(',','');
        }

        price = new Decimal(price);
		if(!_.isNull(this.temp_model.get('max_discount'))){
			var max_discount = new Decimal(this.temp_model.get('max_discount'));
		} else {
			var max_discount = new Decimal(100);
		}

    	var full_discount = new Decimal(100);

    	max_discount = full_discount.minus(max_discount).dividedBy(100).toDP(2);
    	price = price.times(max_discount).toDP(2).toNumber();

    	this.$el.find('.item-minimum-price').text( '= '+accounting.formatMoney(price) );
    	return false;
    },

    render_inventory_unlimited: function(){
    	if(this.temp_model.get('inventory_unlimited') == 1){
    		this.$el.find('#item-inventory-level, #item-reorder-level').attr('disabled', 'disabled');
    	}else{
			this.$el.find('#item-inventory-level, #item-reorder-level').attr('disabled', null);
    	}
    	if(!App.data.user.get('acl').can('update', 'Inventory/Quantity')){
			this.$el.find('#item-inventory-level, #item-reorder-level').attr('disabled', 'disabled');
		}
    },

	getFormDataForModel: function(form){
		var data = _.getFormData(form);
		data.taxes = {};

		if(!data.item_type){
			data.item_type = this.temp_model.get('item_type');
		}

		if(data.force_tax == 'default'){
			data.force_tax = null;
		}

		for(var tax_num = 0; tax_num <= 5; tax_num++){
			var name_key = 'tax['+tax_num+'][name]';
			var percent_key = 'tax['+tax_num+'][percent]';
			var cumulative_key = 'tax['+tax_num+'][cumulative]';

			if(data[name_key] != '' && data[percent_key]){
				var cumulative = 0;
				if(data[cumulative_key]){
					cumulative = data[cumulative_key];
				}

				data.taxes[tax_num] = {
					name: data[name_key],
					percent: data[percent_key],
					cumulative: cumulative
				}
			}
			delete data[name_key];
			delete data[percent_key];
			delete data[cumulative_key];
		}

		if(data.item_type == 'pass' && this.temp_model.get('pass_restrictions') != undefined){
			data.pass_restrictions = this.temp_model.get('pass_restrictions').toJSON();
		}
		
		delete data.service_fees;
		if(data.item_type != 'service_fees' && this.temp_model.get('service_fees') != undefined){
			data.service_fees = this.temp_model.get('service_fees').toJSON();
		}
		
		data.upcs = this.temp_model.get('upcs').toJSON();
		if (this.temp_model.get('customer_groups') != undefined){
			data.customer_groups = this.temp_model.get('customer_groups').toJSON();
		}

		return data;
	},

	save: function(e){
		e.preventDefault();
		var view = this;

		// Validate that required fields are filled in
		var form = this.$el.find('form');
		form.bootstrapValidator('validate');

		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}

		this.$el.loadMask();		

		var data = this.getFormDataForModel(form);
        data.cost_price = _.getNumber(data.cost_price?data.cost_price:'0.00');
        data.unit_price = _.getNumberWNegative(data.unit_price);
        this.model.set(data);
		if(this.model.isNew() && this.collection){
			this.collection.add(this.model);
		}

		this.model.save(null, {wait: true,
			success: function(){
				view.hide();
			},
			error: function(){
				$.loadMask.hide();
			}
		});			

		return false;
	}
});