var PassItemRuleView = Backbone.Marionette.LayoutView.extend({
	tagName: "div",
	className: "panel panel-default",
	template: JST['items/pass_item_rule.html'],

	fields: {
		false: '- Select Field -',
		'teesheet': 'Course',
		'timeframe': 'Date/Time',
		'total_used': 'Total Used',
		'total_used_per_day': 'Uses Per Day',
		'total_used_per_week': 'Uses Per Week',
		'total_used_per_month': 'Uses Per Month',
		'total_used_per_year': 'Uses Per Year',
		'days_since_purchase': 'Days Since Purchase'
 	},

 	regions: {

 	},

	modelEvents: {
		'change change:conditions': 'render'
	},

	initialize: function(options){
		if(options && options.parent_view){
			this.parent_view = options.parent_view;
		}
	},

	serializeData: function(){
		var data = this.model.toJSON();
		data.price_class_name = 'Default Price';
		data.fields = this.fields;

		if(data.price_class_id){
			if(App.data.course.get('price_classes') && App.data.course.get('price_classes')[data.price_class_id]){
				data.price_class_name = App.data.course.get('price_classes')[data.price_class_id];
			}
		}

		return data;
	},

	onRender: function(){
		this.$el.attr('id', 'rule-'+this.model.cid);
	},

	events: {
		'click .pass-rule-edit': 'edit',
		'click .pass-rule-delete': 'delete_rule'
	},

	edit: function(){
		this.model.set('saved', true, {silent: true});
		var rule_edit = new PassItemRuleEditView({model: this.model});
		rule_edit.show();
		return false;
	},

	delete_rule: function(e){
		if(!confirm('DELETE this rule?')){
			return false;
		}
		this.model.collection.remove(this.model);
		return false;
	}
}); 