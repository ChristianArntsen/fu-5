var PassItemRuleConditionParameterView = Backbone.Marionette.ItemView.extend({
	tagName: "div",
	className: "rule-condition-parameter",
	popover_shown: false,
	template: JST['items/pass_item_rule_condition_edit_parameter.html'],
	
	events: {
		'hide.bs.popover': 'on_popover_hide',
		'shown.bs.popover': 'shown',

		'click .condition-parameter-delete': 'delete_parameter',
		'click .btn.rule-condition': 'show_popover'
	},

	modelEvents: {
		'change': 'render'
	},

	onRender: function(){

		this.timeframe_view = false;
		if(this.model.get('field') == 'timeframe'){	
			var title = 'Date/Time Details';

			this.$el.find('.btn-default').popover({
				trigger: 'manual',
				placement: 'bottom',
				title: title,
				viewport: {
					selector: 'body',
					padding: '30px'
				}
			});
		}
	},

	edit: function(){
		if(this.model.get('field') == 'timeframe'){
			this.$el.find('.btn-default').popover('toggle');
		}
		return false;
	},

	delete_parameter: function(){
		this.model.collection.remove(this.model);
		return false;
	},

	show_popover: function(){
		
		$('div.rule-condition').not( this.$el.find('.btn-default') ).popover('hide');

		if(this.model.get('field') == 'timeframe' && !this.popover_shown){	
			this.timeframe_view = new PassItemRuleConditionTimeframeView({model: this.model});
			this.timeframe_view.render();
			this.listenTo(this.timeframe_view, 'close', this.hide_popover);

			this.$el.find('.btn-default').popover('show');
			this.$el.find('.btn-default').data('bs.popover').$tip.find('.popover-content').html( this.timeframe_view.el );
		
		}else{
			this.$el.find('.btn-default').popover('hide');
		}
	},

	shown: function(){
		this.popover_shown = true;
	},

	hide_popover: function(){
		if(this.$el.find('.btn-default').data('bs.popover')){
			this.$el.find('.btn-default').popover('hide');
		}
	},

	on_popover_hide: function(){
		this.popover_shown = false;
		if(this.timeframe_view){
			this.timeframe_view.destroy();	
		}
	}
});

var PassItemRuleConditionParametersView = Backbone.Marionette.CollectionView.extend({
	childView: PassItemRuleConditionParameterView,
	tagName: "div",

	emptyView: function(options){
		var view = new Backbone.Marionette.ItemView({
			template: function(){
				if(options && options.type == 'filter'){
					return '<em class="text-muted">No filters set</em>';
				}
				return '<em class="text-muted">No values added</em>';
			}
		});

		return view;
	},
});