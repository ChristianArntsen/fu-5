var ItemUPCView = Backbone.Marionette.ItemView.extend({
	template: JST['items/item_upc.html'],
	tagName: 'li',
	className: 'upc',

	events: {
		"click button.delete": "removeUpc"
	},

	removeUpc: function(){
		this.model.collection.remove(this.model);
	}
});

var ItemUPCListView = Backbone.Marionette.CompositeView.extend({
	template: JST['items/item_upc_list.html'],
	childViewContainer: 'ul',
	childView: ItemUPCView,

	onRender: function(){

	},

	events: {
		'click button.add-upc': 'addUPC'
	},

	addUPC: function(){

		var field = this.$el.find('#item-upc-name');
		var upc = field.val();
		var self = this;
		if(!upc){
			return false;
		}
		if(this.collection && this.collection.findWhere({upc: upc})){
			field.val('');
			return false;
		}

		var button = this.$el.find('button.add-upc');
		button.button('loading');

		$.get(API_URL + '/items/upcs', {upc: upc}, function(response){
			button.button('reset');
			if(response && response[0]){
				App.vent.trigger('notification', {msg: 'UPC already in use', type: 'error'});
			}else{
				self.collection.add( new ItemUPC({upc: upc}) );
				field.val('');
			}
		},'json');
		
		return false;
	},

	onAddChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	},

	onRemoveChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	}
});