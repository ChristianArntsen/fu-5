var PassItemRuleConditionTimeframeView = Backbone.Marionette.ItemView.extend({
	tagName: "div",
	className: "container-fluid",
	template: JST['items/pass_item_rule_condition_edit_timeframe.html'],

	events: {
		'change input.day-of-week': 'highlight_day',
		'click button.save-timeframe': 'save'
	},

	onRender: function(){
		
		var view = this;

		var slider = this.$el.find('#time-of-day-slider')[0];
		noUiSlider.create(slider, {
			start: [400,2000],
			margin: 50,
			connect: true,
			range: {
				'min': 0,
				'max': 2400
			},
			step: 50,
			format: {
				to: function(value){
					var number = new Decimal(value);
					var val = number.toDP(2).toNumber();
					val = view.parse_time(val, false);
					return String('0000' + val).slice(-4);
				},
				from: function(value){
					var number = new Decimal(value);
					var value = number.toDP(2).toNumber();
					return view.parse_time(value, true);
				}	
			},
			pips: {
				mode: 'values',
				density: 4,
				values: [0, 400, 800, 1200, 1600, 2000, 2400],
				format: {
					to: function(value){
						var time = String('0000' + value).slice(-4);
						return moment(time, 'HHmm').format('ha');
					},
					from: function(value){
						return value;
					}
				}
			}
		});

		var times = _.clone(this.model.get('time'));
		if(!times || !times[0] || !times[1]){
			times = [0, 2400];
		}
		slider.noUiSlider.set(times);
		slider.noUiSlider.on('update', _.bind(this.update_time_of_day, this));

		this.$el.find('.datepicker').datetimepicker({
			format: 'MM/DD/YYYY'
		});
	},

	update_time_of_day: function(){
		var time_slider = this.$el.find('#time-of-day-slider')[0];
		var time = time_slider.noUiSlider.get();
		this.$el.find('span.time-start').html( moment(time[0], 'HHmm').format('h:mma') );
		this.$el.find('span.time-end').html( moment(time[1], 'HHmm').format('h:mma') );
	},

	// Takes a decimal time (that plays nice with the slider) 
	// and converts it to 24 hour time
	// eg. 1650 is actually 2:30pm or 1630
	parse_time: function(time, fromTime){
		
		if(fromTime === undefined){
			fromTime = true;
		}
		
		var minutes_from = '30';
		var minutes_to = '50';
		if(!fromTime){
			minutes_from = '50';
			minutes_to = '30';
		}

		if(time == null || time == '' || time == undefined){
			return 0;
		}
		
		// Since 2400 doesn't actually exist in 24 hour time,
		// set it to the end of the day which is 11:59pm or 2359
		if(fromTime){
			if(time == 2359){
				return 2400;
			}
		}else{
			if(time == 2400){
				return 2359;
			}
		}
		time = String(time);

		if(time.substring(time.length - 2) == minutes_from){
			return time.replace(minutes_from, minutes_to);
		}

		return time;
	},

	highlight_day: function(e){
		var checkbox = $(e.currentTarget);

		if(checkbox.is(':checked')){
			checkbox.parents('label').addClass('active');
		}else{
			checkbox.parents('label').removeClass('active');
		}
	},

	save: function(){

		var data = _.getFormData(this.$el.find('form'));
		var time_slider = this.$el.find('#time-of-day-slider')[0]
		data.time = time_slider.noUiSlider.get();
		data.day_of_week = [];
		data.date = [data.start_date, data.end_date];
		delete data.start_date, data.end_date;

		for(var day = 1; day <= 7; day++){
			var key = 'day['+day+']';
			if(data[key] && data[key] == 1){
				data.day_of_week.push(day);
			}
			delete data[key];
		}

		this.model.set(data);
		this.model.generateLabel();
		this.trigger('close');

		return false;
	}
});