var PrintBarcodesView = ModalView.extend({

	id: 'barcodes_window',
	template: JST['items/print_barcodes.html'],

	events: {
		"click button.print": "print"
	},

	initialize: function(options){
		ModalView.prototype.initialize.call(this);
		this.customer = false;
	},

    render: function(){
		var attrs = {};
		attrs.items = this.collection;
		this.$el.html(this.template(attrs));
		return this;
    },
	
	print: function(){
		
		var item_type = 'item';
		if(this.collection.length > 0){
			item_type = this.collection.at(0).get('item_type');
		}

		if(item_type == 'punch_card'){
			item_type = 'item_kit';
		}
		
		var el = this.$el;
		var sheetSize = el.find('input[name="label_size"]:checked').val();
		var startIndex = el.find('#label-start-number').val();
		var topMargin = el.find('#label-top-margin').val();
		var additionalName = el.find('#label-additional-name').val();
		var additionalPercentage = el.find('#label-additional-percentage').val();

		var url = SITE_URL + '/barcode/pdf/';

		var itemQuantities = el.find('input[name="print_quantity[]"]');
		_.each(itemQuantities, function(input, index){
			var itemId = $(input).data('item-id');
			var qty = $(input).val();

			for(var x = 0; x < qty; x++){
				url += itemId + '~'
			}
		});

		if(!startIndex){
			startIndex = 1;
		}
		if(!additionalName){
			additionalName = 0;
		}
		if(!additionalPercentage){
			additionalPercentage = 0;
		}

		url = url.substring(0, url.length - 1);
		url += '/' + item_type + '/' + sheetSize + '/' + startIndex + '/' + additionalName + '/' + additionalPercentage + '/' + topMargin;

		window.open(url);
		return false;
	}
});

var PrintIndividualBarcodesView = ModalView.extend({

    id: 'individual_barcodes_window',
    template: JST['items/print_individual_barcodes.html'],

    events: {
        "click button.print": "print"
    },

    initialize: function(options){
        ModalView.prototype.initialize.call(this);
        this.customer = false;
    },

    render: function(){
        var attrs = {};
        attrs.items = this.collection;
        this.$el.html(this.template(attrs));
        return this;
    },

    print: function(){

        var item_type = 'item';
        if(this.collection.length > 0){
            item_type = this.collection.at(0).get('item_type');
        }

        if(item_type == 'punch_card'){
            item_type = 'item_kit';
        }

        var el = this.$el;

        var additionalPercentage = el.find('#label-additional-percentage').val();
        var itemIds = {};
        var itemIdList = [];
        var itemQuantities = el.find('input[name="print_quantity[]"]');

        _.each(itemQuantities, function(input, index){
            var itemId = $(input).data('item-id');
            var qty = $(input).val();
            itemIdList.push(itemId);
            itemIds[itemId] = qty;
        });
        var view = this;
        var controllerName = item_type+"s";
        $.ajax({
            type: "POST",
            url: "/index.php/"+controllerName+"/get_multiple_info",
            data: {'items[]':itemIdList},
            dataType:'json',
            success: function(response){
                if (response.success && response.items.length > 0) {
                    // Generate barcodes VIA webprnt
                    var label_data = '';
                    var items = response.items
                    for (var i in items) {
                        for (var c = 0; c < itemIds[items[i].item_id]; c++) {
                            var is_last = (items.length == parseInt(i) + 1 && itemIds[items[i].item_id] == parseInt(c) + 1);
                            var price_string = items[i].unit_price;
                            var additional_price = '';
                            if (additionalPercentage != '') {
                                additional_price = $('#additional_price').val()+' $'+(items[i].unit_price * (100 + parseFloat(additionalPercentage))/100).toFixed(2);
                            }
                            label_data = webprnt.build_barcode_label(label_data, items[i].name, items[i].long_item_id, price_string, is_last, additional_price, items[i].barcode_number);
                        }
                    }
                    if (App.data.course.get('multiple_printers')) {
                        var webprnt_label_id = App.data.course.get('webprnt_label_ip');
                        var webprnt_label_ip = App.data.course.get('receipt_printers').get(webprnt_label_id).get('ipAddress');
                    } else {
                        var webprnt_label_ip = App.data.course.get('webprnt_label_ip');
                    }

                    App.data.print_queue.add_receipt(webprnt_label_ip, label_data);
                    App.data.print_queue.print();

                    view.hide();
                }
                else {
                    alert('There was a problem printing out your barcodes. Please try again.');
                }
            }
        });

        return false;
    }
});