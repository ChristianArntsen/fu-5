var ItemCustomerGroupView = Backbone.Marionette.ItemView.extend({
	template: JST['items/item_customer_group.html'],
	tagName: 'li',
	className: 'upc',

	events: {
		"click button.delete": "removeGroup"
	},

	removeGroup: function(){
		this.model.collection.remove(this.model);
	}
});

var ItemCustomerGroupsView = Backbone.Marionette.CompositeView.extend({
	template: JST['items/item_customer_groups.html'],
	childViewContainer: 'ul',
	childView: ItemCustomerGroupView,

	events: {
		'change select': 'addGroup'
	},

	addGroup: function(event){
		
		var menu = $(event.currentTarget);
		var group_id = menu.val();
		if(group_id == "0"){
			return false;
		}
		var group = App.data.course.get('groups').get(group_id);
		this.collection.add(new ItemCustomerGroup(group.toJSON()) );
		menu.val(0);

		return false;
	},

	onAddChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	},

	onRemoveChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	}
});