var LogoutOptionWindow = ModalView.extend({
	
	tagName: "div",
	id: "logout_option_window",
	template: JST['logout_option_window.html'],

	initialize: function(options) {
		if(options && options.closable){
			this.closable = options.closable;
		}
		if(options && options.backdropOpacity){
			this.backdropOpacity = options.backdropOpacity;
		}
		this.register_log_open = false;
		if(options && _.has(options, 'register_log_open')){
			this.register_log_open = options.register_log_open;
		}
		ModalView.prototype.initialize.call(this, options);						
	},

	events: {
		"click #logout_close": "logout",
		"click #logout_bypass": "bypass_log",
		"click #logout_switch_user": "switch_user",
		"click #sign-in-user": "log_in",
		"change #employee-id": "highlight_password"
	},

	render: function(options) {	
		var attr = {};
		attr.closable = true;
		if(typeof(this.closable) != undefined){
			attr.closable = false;
		}
		attr.employees = App.data.course.get('employees').getDropdownData();
		attr.register_log_open = this.register_log_open;

		attr.can_bypass_register_log = false;
		if(App.data.user.is_admin() || App.data.course.get_setting('allow_employee_register_log_bypass') == 1){
			attr.can_bypass_register_log = true;
		}

		this.$el.html(this.template(attr));
		return this;
	},
	
	highlight_password: function(){
		this.$el.find('#employee-password').focus();
	},
	
	bypass_log: function(){
		window.location = SITE_URL + '/home/logout';
		return false;
	},
	
	logout: function(){

		var log = App.data.register_logs.get_open_log(undefined, true);
		
		if(log){
			var register_log_window = new RegisterLogView({
				model: log, 
				template: 'close',
				collection: App.data.register_logs,
				logout: true
			});
			register_log_window.show();
			this.hide();	
		
		}else{
			window.location = SITE_URL + '/home/logout';
		}

		return false;
	},
	
	switch_user: function(){
		this.template = JST['logout_switch_user.html'];
		this.render();
	},
	
	log_in: function(){
		var employee_id = this.$el.find('#employee-id').val();
		var password = this.$el.find('#employee-password').val();
		var view = this;
		var register_log = App.data.register_logs.get_open_log();
		
		App.data.user.switch_user(employee_id, password, function(){
			view.hide();
			if(register_log){
				if(App.data.course.get_setting('multi_cash_drawers') == 0){
					register_log.set('persist', 1).save();
				}
			}
		});
	}
});
