var ContactFormRepliesModalView = ModalView.extend({
  template: JST['contact_forms/contact_form_replies.html'],

  render: function () {
    this.$el.html(this.template({
      replies: this.collection.models
    }));
  }
});
