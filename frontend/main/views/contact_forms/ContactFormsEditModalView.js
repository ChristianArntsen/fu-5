var ContactFormsEditModalView = ModalView.extend({
  id: 'edit_contact_form_window',
  template: JST['contact_forms/contact_forms_edit.html'],

  events: {
    'click button.save': 'save',
    'change #form_presets': 'onChangeFormPresets'
  },

  presets: {
    birthday: {
      title: 'Birthday Form',
      attributes: {
        has_first_name: true,
        has_last_name: true,
        has_email: true,
        has_phone_number: true,
        has_birth_date: true,
        has_address: false,
        has_city: false,
        has_state: false,
        has_zip: false,
        has_message: false,

        req_first_name: true,
        req_last_name: true,
        req_email: true,
        req_phone_number: true,
        req_birth_date: true,
        req_address: false,
        req_city: false,
        req_state: false,
        req_zip: false,
        req_message: false
      }
    },
    marketing: {
      title: 'Marketing Form',
      attributes: {
        has_first_name: true,
        has_last_name: true,
        has_email: true,
        has_phone_number: true,
        has_birth_date: false,
        has_address: false,
        has_city: false,
        has_state: false,
        has_zip: false,
        has_message: false,

        req_first_name: true,
        req_last_name: true,
        req_email: true,
        req_phone_number: true,
        req_birth_date: false,
        req_address: false,
        req_city: false,
        req_state: false,
        req_zip: false,
        req_message: false
      }
    }
  },

  render: function () {
    this.$el.html(this.template(_.extend({customer_groups: App.data.customer_groups.models}, this.model.attributes)));
  },

  save: function (e) {
    var
      form,
      attrs,
      isNew = this.model.isNew();

    e.preventDefault();

    form = this.$el.find('form');
    form.bootstrapValidator('validate');

    if (!form.data('bootstrapValidator').isValid()) {
      return false;
    }

    attrs = _.reduce(form.find('input[type=checkbox]'), function (memo, input) {
        memo[input.getAttribute('name')] = input.checked;
        return memo;
      }, {
        name: this.$el.find('input[name=name]').val(),
        customer_group_id: this.$el.find('#customer-group-select').val()
      }
    );

    // DEBUG --v
    /*
    this.model.set(attrs);
    
    if (isNew) {
      App.data.contact_forms.add(this.model);
      App.vent.trigger('notification', {'msg': 'New contact form created.', 'type': 'success'});
    } else {
      App.vent.trigger('notification', {'msg': 'Contact form saved.', 'type': 'success'});
    }
    */
    // DEBUG --^

    this.model.save(attrs, {
      success: function (model, response, options) {
        if (isNew) {
          App.data.contact_forms.add(model);
          App.vent.trigger('notification', {'msg': 'New contact form created.', 'type': 'success'});
        } else {
          App.vent.trigger('notification', {'msg': 'Contact form saved.', 'type': 'success'});
        }
      },
      error: function (model, response, options) {
        App.vent.trigger('notification', {'msg': 'An error occurred while saving this contact form.', 'type': 'error'});
      }
    });

    this.hide();
    return false;
  },

  onChangeFormPresets: function (e) {
    var
      preset,
      key;

    if (e.target.value in this.presets) {
      preset = this.presets[e.target.value];
      for (key in preset.attributes) {
        this.$el.find('[name=' + key + ']').prop('checked', preset.attributes[key]);
      }
    }
  }
});
