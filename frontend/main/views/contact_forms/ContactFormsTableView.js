var ContactFormsTableView = Backbone.Marionette.CompositeView.extend({
  childView: ContactFormsTableRowView,
  childViewContainer: 'tbody',
  template: JST['contact_forms/contact_forms_table.html'],

  events: {
    'change .check-all': 'onChangeCheckAll'
  },

  initialize: function () {
    window.cftv = this;
  },

  onChangeCheckAll: function (event) {
    this.$el.find('input[type=checkbox]').prop('checked', event.target.checked);
  }
});
