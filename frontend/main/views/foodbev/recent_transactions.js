var FbRecentTransactionsView = ModalView.extend({

	id: "recent_transactions",
	class: "fb-recent-transactions",
	template: JST['foodbev/recent_transactions_window.html'],

	events: {
		"click button.more": "loadMore",
		"click button.share-tips": "shareTips"
	},

	initialize: function(attrs, options) {
		this.listenTo(this.collection, "add remove change", this.render);
		this.listenTo(this.collection, 'more', this.hideMoreButton);
		
		if(!options){
			options = {};
		}
		options.extraClass = 'modal-lg';
		
		ModalView.prototype.initialize.call(this, options);			
	},

	render: function() {
		var self = this;
		this.$el.html(this.template());
		this.$el.find('ul').html('');
		this.list = this.$el.find('ul');

		_.each(this.collection.models, function(transaction){
			self.renderTransaction(transaction);
		});
		return this;
	},

	renderTransaction: function(transaction){
		var ul = App.data.user.get('user_level');
		var pid = App.data.user.get('person_id');
		console.log(ul,pid,transaction.get('employee_id'));

		if((typeof (ul*1) !== 'number' || ul*1 < 3) && pid*1 !== transaction.get('employee_id')*1)return;

		this.list.append( new FbRecentTransactionView({'model': transaction}).render().el );
		return this;
	},

	loadMore: function(){
		this.$el.find('button.more').button('loading');
		this.collection.loadMore({
			food_and_beverage: 1
		});
		return false;
	},

	hideMoreButton: function(response){
		this.$el.find('button.more').button('reset');
		if(!response || response.length < this.collection.limit){
			this.$el.find('button.more').hide();
		}
	},

	shareTips: function(e){
		console.log(e);
		var employee_id = App.data.user.get('person_id');

        var url = this.collection.url;
        var transaction = new TipSharingTransaction();
        transaction.base_url = url + '/0/tip-share';
        var date = new Date();
        var tzo = date.getTimezoneOffset();
        transaction.url = transaction.base_url+'?employee_id='+employee_id
			+'&date='+encodeURI(date.toISOString())+'&tzo='+encodeURI(tzo/60);

        var tipTransactionWindow = new ShareTipsWindowView({model:transaction});
        console.log(transaction);

        tipTransactionWindow.show();

        transaction.fetch();
        return false;
	}
});
