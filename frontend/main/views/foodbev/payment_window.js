var FbPaymentWindowView = ModalView.extend({
    tagName: "div",
    id: "payment_window",
    template: JST['foodbev/payment_window.html'],
    numberTemplate: JST['sales/payment_window_number.html'],
    changeIssuedTemplate: JST['foodbev/change_issued.html'],
    creditCardTemplate: JST['sales/payment_window_credit_card_swipe.html'],
    billingAddressTemplate: JST['sales/payment_window_billing_address.html'],

    initialize: function() {
        ModalView.prototype.initialize.call(this, {extraClass: 'modal-lg'});
        this.listenTo(this.collection, "add remove change error", this.render);
        this.listenTo(this.collection, "invalid", this.displayError);
        this.listenTo(this.collection, "sync", this.closePayments);
        this.listenTo(App.vent, "credit-card gift-card", this.processCreditCard);

        this.address_entered = false;
        this.billing_address = false;
        this.billing_zip = false;
    },

    events: {
        'click button.pay': 'initPay',
        "click #payment_cash": "handleButton",//"payCash",
        "click #payment_check": "handleButton",//"showCheck",
        "click #payment_giftcard": "handleButton",//"showGiftCard",
        "click #payment_credit_card": "handleButton",//"showCreditCard",
        "click #payment_customer_account": "handleButton",//"payCustomerAccount",
        "click #payment_member_account": "handleButton",//"payMemberAccount",
        "click #payment_loyalty": "handleButton",//"payLoyalty",
        'click button.custom_payment': "payCustomPayment",
        'click button.back': 'showPayments',
        'click button.save-billing-address': 'saveBillingAddress',
        'keypress #payment-number': 'enterSubmit',
        'click #cancel-credit-card-payment': 'cancelCreditCardPayment',
        'click button.charge-stored-card': 'charge_stored_card'
    },

    handleButton: function(event){
        this.attemptingToClose = false;
        self = this;
        this.model.serverTotalPayments(function(res) {
            if(self.model.getTotalPayments() !== res * 1){
                self.model.get('payments').fetch({'success': function(res){
                    self.model.table.open(
                        self.model.table.get('table_num'),
                        function() {
                            $('#modal-payment_window').modal('hide');
                            if (!self.model.table.get('receipts') || !self.model.table.get('receipts').length) {
                                App.vent.trigger('notification', {
                                    'msg': 'Table service appears to be completed.',
                                    'type': 'warning'
                                });
                                $('.modal').modal('hide');
                            }
                        }
                    );

                }});
                App.vent.trigger('notification', {'msg': 'Please review payments and try again.', 'type': 'warning'});

                return;
            }
            switch (event.currentTarget.id) {
                case 'payment_cash':
                    self.payCash(event);
                    break;
                case 'payment_check':
                    self.showCheck(event);
                    break;
                case 'payment_giftcard':
                    self.showGiftCard(event);
                    break;
                case 'payment_credit_card':
                    self.showCreditCard(event);
                    break;
                case 'payment_customer_account':
                    self.payCustomerAccount(event);
                    break;
                case 'payment_member_account':
                    self.payMemberAccount(event);
                    break;
                case 'payment_loyalty':
                    self.payLoyalty(event);
                    break;
                default:
                    console.log('no action found for ', event);
            }
        },
        function(res){
            if(res.status === 404){
                self.model.table.open(
                    self.model.table.get('table_num'),
                    function() {
                        $('#modal-payment_window').modal('hide');
                        if (!self.model.table.get('receipts') || !self.model.table.get('receipts').length) {
                            App.vent.trigger('notification', {
                                'msg': 'Table service appears to be completed.',
                                'type': 'warning'
                            });
                            $('.modal').modal('hide');
                        }
                    }
                );
                App.vent.trigger('notification', {'msg': 'Receipt was not found.', 'type': 'warning'});
            }
        });
    },

    onShow:function(){
        window.onbeforeunload = function() {
            return "Close the modal before leaving this page or your payment may be lost.";
        };
    },
    onHide:function(){
        window.onbeforeunload = null;
    },

    initPay: function(e){
        this.paying = true;
        this.$el.loadMask({'message':'Adding payment...'});
        $(e.currentTarget).addClass('disabled').prop('disabled', true);
    },
    clearLoader: function(){
        this.paying = false;
        $.loadMask.hide();
        this.$el.find('button.pay').removeClass('disabled').prop('disabled', null);
    },
    render: function() {
        this.$el.html('');

        var data = {};
        data.customer_note = this.model.get('customer_note');
        data.amount_due = this.model.getTotalDue();
        data.member_account_label = App.data.course.get('member_balance_nickname');
        data.customer_account_label = App.data.course.get('customer_credit_nickname');
        data.enable_member_balance = false;
        data.enable_customer_balance = false;
        data.custom_payments = App.data.course.get('custom_payments');
        data.enable_loyalty = false;

        if(this.model.get('customer')){
            if(this.model.get('customer').get('member') == 1){
                data.enable_member_balance = true;
            }
            if(this.model.get('customer').get('use_loyalty') == 1){
                data.enable_loyalty = true;
            }
            data.enable_customer_balance = true;
        }

        this.$el.html(this.template(data));
        this.$el.find('#payment_amount').keypad({position: 'bottom-right'});
        this.renderPayments();

        return this;
    },

    enterSubmit: function(e){
        if(e.which == 13){
            this.$el.find('button.submit-payment').trigger('click');
            return false;
        }
        return true;
    },

    displayError: function(model, error){
        this.clearLoader();
        $.loadMask.hide();
        App.vent.trigger('notification', {'msg': error, 'type': 'error'});
    },

    showPayments: function(){
        this.render();
        return false;
    },

    hasTab: function () {
        return this.collection.hasTab();
    },

    renderPayments: function(){
        var saved_credit_cards = [];
        _.each(this.collection.models,function(payment){
            if(payment.get('tran_type') === 'PreAuth')saved_credit_cards.push(payment);
        });
        var saved_credit_card_container = this.$el.find('#saved-credit-cards');
        var payment_list = this.$el.find('#window-payments');

        payment_list.html('');
        saved_credit_card_container.html('');

        if(this.collection.length > 0){
            _.each(this.collection.models, function(payment){
                var payment_view = new PaymentView({model: payment})
                payment_list.append(payment_view.render().el );
            });
        }

        if(saved_credit_cards.length > 0){
            _.each(saved_credit_cards, function(payment){
                saved_credit_card_container.append('<button class="btn btn-success btn-lg col-lg-12 charge-stored-card pay" \
					data-payment-id="'+payment.get('type')+'" data-auth-code="'+payment.get('auth_code')+'">Charge <br>'+payment.get('type')+'</button>');
            });
        }

        return this;


        this.$el.find('ul.payments').html('');
        _.each(this.collection.models, this.addPayment, this);
        return this;
    },

    showChangeDue: function(change_due){
        var data = {};
        data.amount = change_due;
        var window = new ChangeDueWindow({model: new Backbone.Model(data), food_bev_table: App.data.food_bev_table });
        window.show();
        var receipt_printer = new Receipt();
        receipt_printer.open_cash_drawer();
    },

    addPayment: function(payment){
        payment.set({'receipt_id': this.model.get('receipt_id')}, {'silent':true});
        this.$el.find('ul.payments').append( new FbPaymentView({model: payment}).render().el );
    },

    getPaymentData: function(){
        var amount = _.getNumber(this.$el.find('#payment_amount').val());
        var customer_note = this.$el.find('#customer-note').val();
        if(customer_note == ''){
            customer_note = null;
        }
        this.model.set('customer_note', customer_note, {silent: true});
        return {"amount": amount, "customer_note": customer_note};
    },

    saveBillingAddress: function(e){

        var form = this.$el.find('#billing_address_form');
        form.bootstrapValidator('validate');

        if(!form.data('bootstrapValidator').isValid()){
            return false;
        }

        this.address_entered = true;
        this.billing_address = this.$el.find('#credit_card_address').val();
        this.billing_zip = this.$el.find('#credit_card_zipcode').val();
        this.showCreditCard(e);

        return false;
    },

    showBillingAddressForm: function(e){
        var attr = this.getPaymentData();

        this.$el.find('div.modal-body').html( this.billingAddressTemplate(attr) );
        this.$el.find('#credit_card_address').focus();
        return false;
    },

    showCreditCardSwipeForm: function(){

        var amount = this.$el.find('#payment_amount').val();
        var attrs = {
            amount: amount,
            show_manual_entry: true
        };
        var view = this;

        this.$el.find('div.modal-body').html(this.creditCardTemplate(attrs));
        var events = _.clone(this.events);

        // Temporarily bind the "submit" button to send the payment
        events['click button.submit-payment'] = 'payCreditCard';
        events['click button.manual-entry'] = 'payManualCreditCard';

        this.delegateEvents(events);
        this.$el.find('#credit-card-data').focus();

        this.$el.find('#credit-card-data').on('keypress', function(e){
            if(e.which == 13){
                view.payCreditCard(e);
                view.$el.loadMask();
            }
        });

        return false;
    },

    payManualCreditCard: function(e){
        this.manual_entry = true;
        return this.showCreditCard(e);
    },
    count : 0,
    showCreditCard: function(e){
        
        if(!App.data.course.get('merchant') || e.shiftKey){
            this.payCreditCard(e);
            return false;
        }

        if(App.data.course.get('merchant') == 'element' && !this.manual_entry){
            this.showCreditCardSwipeForm();
            return false;
        }

        if(App.data.course.get('merchant') == 'element' && this.manual_entry && !this.address_entered){
            this.showBillingAddressForm(e);
            return false;
        }

        var view = this;
        var data = this.getPaymentData();

        data.receipt_id	= this.model.get('receipt_id');
        data.ets_type = 'credit card';
        data.action = 'sale';
        data.table_number = App.data.food_bev_table.get('table_num');
        data.type = 'credit_card';
        data.source = 'f&b';
        data.record_id = 0;
        data.bypass_fee = 1;

        if(this.address_entered){
            data.address = this.billing_address;
            data.zip = this.billing_zip;
        }
        if(this.save_card && this.save_card === true){
            data.type = 'credit_card_save';
            data.amount = 0.00;
            this.save_card = false;
        }

        // Load merchant card iframe
        view.$el.loadMask();
        $.post(SITE_URL + '/v2/home/credit_card_window', data, function(response){
            view.$el.find('div.modal-footer').show();
            view.$el.find('div.modal-body').html(response);
            view.$el.find('div.modal-header button').hide();
            view.$el.find('div.modal-footer button').hide();
            view.$el.find('div.modal-footer #cancel-credit-card-payment').show();
            view.$el.find('h4.modal-title').text('Credit Card Payment');
            $('#modal-payment_window').data('bs.modal').options.keyboard = false;
            $('#modal-payment_window').data('bs.modal').options.backdrop = 'static';
        },'html');
    },

    payCreditCard: function(e,merchant_data){

        var payment = this.getPaymentData();
        payment.receipt_id = this.model.get('receipt_id');
        payment.type = 'Credit Card';

        payment.merchant_data = {};
        if(merchant_data != undefined){
            payment.merchant_data = merchant_data;
        }

        if(App.data.course.get('merchant') == 'element' && !e.shiftKey){
            payment.type = 'creditcard-element';

            payment.merchant_data.encrypted_track = this.$el.find('#credit-card-data').val();
        }
        this.collection.addPayment(payment, function(payment){
            if(
                payment.get('type') == 'Credit Card' &&
                payment.has('merchant_data') &&
                payment.get('merchant_data') &&
                App.data.course.get_setting('signature_slip_count') > 0
            ){
                var credit_card_receipt = new CreditCardReceipt(payment.attributes);
                credit_card_receipt.print();
            }
        });
        return false;
    },

    charge_stored_card: _.throttle(function(e){
        this.attemptingToClose = false;

        var payment_id =$(e.currentTarget).data('payment-id');
        var payment = this.collection.get(payment_id);
        var params = payment.attributes;
        this.payCreditCard(e, {
            credit_card_id: params.invoice_id?params.invoice_id:params.credit_card_invoice_id?params.credit_card_invoice_id:null,
            auth_code: params.auth_code,
            credit_card_id:params.credit_card_id});
        var amount = _.getNumber(this.$el.find('#payment_amount').val());
        payment.set('verified', false);
        this.$el.find('#payment_amount').val(amount);
        e.preventDefault();
        this.$el.find('.modal-content').loadMask();
        return false;
    },3000, {'leading': true, 'trailing': false}),

    processCreditCard: _.throttle(function(credit_card){
        var view = this;

        if(credit_card.merchant == 'ets'){
            credit_card.type = 'creditcard-ets';
        }

        if(credit_card.merchant == 'mercury'){
            credit_card.type = 'creditcard-mercury';
        }

        if(credit_card.merchant == 'element'){
            credit_card.type = 'creditcard-element';
        }
        if(credit_card.tran_type === 'PreAuth' || (credit_card.type === 'creditcard-mercury' && credit_card.merchant_data.TransType == 'auth') || (credit_card.type === 'creditcard-ets' && credit_card.merchant_data.type === 'credit_card_save')){
            credit_card.tran_type = 'PreAuth';
            credit_card.amount = 0;
            credit_card.merchant_data.Amount = 0;
        }
        this.collection.addPayment(credit_card, function(model, response){
            if(App.data.course.get_setting('signature_slip_count') > 0){
                var sale_data = model.attributes;
                sale_data.table_number = App.data.food_bev_table.get('table_num');
                sale_data.sale_number = sale_data.number;
                sale_data.check_number = view.model.get('id');
                if(credit_card.tran_type === 'PreAuth' || (credit_card.type === 'creditcard-ets' && credit_card.merchant_data.type === 'credit_card_save')){
                    return;
                }

                var receipt = new CreditCardReceipt(sale_data);
                receipt.print_network();
            }
            view.closePayments();
        });

    }, 1000, {'leading': true, 'trailing': false}),

    cancelCreditCardPayment: function(e) {
        //TODO: This does not ever appear to be called...

        var view = this;
        // Close window
        view.hide();

        var data = {};
        // Cancel Payment
        $.post(SITE_URL + '/v2/home/cancel_credit_card_payment', data, function(response){
        },'json');
    },

    payCash: function(event){
        var data = this.getPaymentData();
        data.type = "cash";
        data.description = 'Cash';
        var self = this;

        if(data.amount == ''){
            return false;
        }

        this.$el.find('div.modal-content').loadMask();
        data.amount = parseFloat(accounting.unformat(data.amount));

        this.collection.addPayment(data);
        event.preventDefault();
    },

    payCustomPayment: _.throttle(function(event){
        var data = this.getPaymentData();
        data.type = $(event.target).attr('id');
        data.description = $(event.target).html();
        var self = this;

        if(data.amount == ''){
            return false;
        }

        this.$el.find('div.modal-content').loadMask();
        data.amount = parseFloat(accounting.unformat(data.amount));

        this.collection.addPayment(data);
        event.preventDefault();
    }, 1000, {'leading': true, 'trailing': false}),

    payCheck: function(event){
        var data = this.getPaymentData();
        data.type = "check";
        data.details = 'Check';
        data.description = 'Check';
        var self = this;

        if(data.amount == ''){
            return false;
        }

        this.$el.find('div.modal-content').loadMask();
        data.amount = parseFloat(accounting.unformat(data.amount));
        data.card_number = this.$el.find('#payment-number').val();

        this.collection.addPayment(data);

        event.preventDefault();
    },

    showCheck: function(e){
        e.preventDefault();
        this.$el.find('#payment-types').html(this.numberTemplate());
        this.$el.find('#payment-number').focus();

        var events = _.clone(this.events);

        // Temporarily bind the "submit" button to send the payment
        events['click button.submit-payment'] = 'payCheck';
        this.delegateEvents(events);

        return false;
    },

    payMemberAccount: _.throttle(function(event){
        var customer_id = this.model.get('customer').id;
        var data = this.getPaymentData();
        data.type = "member_balance";
        data.description = 'Member Balance';
        data.customer_id = customer_id;

        if(data.amount == ''){
            return false;
        }

        var self = this;
        data.amount = parseFloat(accounting.unformat(data.amount));

        this.collection.addPayment(data);
        event.preventDefault();
    }, 1000, {'leading': true, 'trailing': false}),

    payCustomerAccount: _.throttle(function(event){
        var customer_id = this.model.get('customer').id;
        var data = this.getPaymentData();
        data.type = "account_balance";
        data.description = 'Customer Account';
        data.customer_id = customer_id;

        if(data.amount == ''){
            return false;
        }

        var self = this;
        data.amount = parseFloat(accounting.unformat(data.amount));

        this.collection.addPayment(data);
        event.preventDefault();
    }, 1000, {'leading': true, 'trailing': false}),

    payLoyalty: _.throttle(function(event){
        var customer_id = this.model.get('customer').id;
        var data = this.getPaymentData();
        data.type = "loyalty_points";
        data.description = 'Loyalty Points';
        data.customer_id = customer_id;

        if(data.amount == ''){
            return false;
        }

        var self = this;
        data.amount = parseFloat(accounting.unformat(data.amount));

        this.collection.addPayment(data);
        event.preventDefault();
    }, 1000, {'leading': true, 'trailing': false}),

    showGiftCard: function(e){
        e.preventDefault();
        var view = this;

        // If course uses ETS giftcards
        if(App.data.course.get('use_ets_giftcards') == 1){

            var data = {};
            data.amount = _.round(this.$el.find('#payment_amount').val().replace(',',''));
            data.type = 'gift_card';
            data.description = 'Gift Card';
            data.record_id = 0;

            // Load merchant card form
            view.$el.find('div.modal-content').loadMask();
            $.post(SITE_URL + '/v2/home/credit_card_window', data, function(response){
                view.$el.find('div.modal-body').html(response);
                view.$el.find('div.modal-footer').hide();
                view.$el.find('h4.modal-title').text('Gift Card Payment');
            },'html');

            return false;

        }else{
            this.$el.find('#payment-types').html(this.numberTemplate());
            this.$el.find('#payment-number').cardSwipe();
            this.$el.find('#payment-number').focus();

            var events = _.clone(this.events);

            // Temporarily bind the "submit" button to send the payment
            events['click button.submit-payment'] = 'payGiftCard';
            this.delegateEvents(events);
        }

        return false;
    },

    payGiftCard: function(){
        var data = this.getPaymentData();
        data.type = "gift card";
        data.description = 'Gift Card';
        data.card_number = this.$el.find('#payment-number').val();

        if(data.amount == ''){
            return false;
        }

        var self = this;
        this.$el.find('div.modal-content').loadMask();
        data.amount = parseFloat(accounting.unformat(data.amount));

        this.collection.addPayment(data);
        event.preventDefault();
    },

    closePayments: function(){
        if(this.attemptingToClose === true)return;
        this.attemptingToClose = true;
        var self = this;

        if(this.model.isPaid() || this.model.getTotalDue() < 0){
            var totalDue = this.model.getTotalDue();
            this.model.printReceipt('sale',function(){

                // If change is due, show change due window
                if(totalDue < 0){
                    self.showChangeDue(totalDue);
                    self.model.get('payments').fetch({'success': function(res){
                        self.model.table.open(
                            self.model.table.get('table_num'),
                            function() {
                                $('#modal-payment_window').modal('hide');
                                if (!self.model.table.get('receipts') || !self.model.table.get('receipts').length) {

                                    self.closeWindow();self.model.markPaid();
                                    self.attemptingToClose = false;
                                }
                            }
                        );

                    }});
                }else{

                    self.closeWindow();
                }

                self.model.markPaid();
            });
        }
    },

    closeWindow: function(event){
        if(event){
            event.preventDefault();
        }
        // strange behaviour after
        //$('#page-food-beverage').loadMask({'message':'Closing table...'});
        App.data.food_bev_table.close(function(){
            //$('#page-food-beverage').loadMask.hide();
            App.page.currentView.show_table_layouts();
        });

        this.hide();
    }
});

var FbPaymentView = Backbone.View.extend({

    tagName: 'li',
    className: 'payment',
    template: JST['foodbev/payment.html'],

    initialize: function(){
        this.listenTo(this.model, 'change', this.render);
        this.render();
    },

    events: {
        'click a.delete': 'deletePayment'
    },

    render: function(){
        this.$el.html(this.template(this.model.attributes));
        return this;
    },

    deletePayment: function(e){
        //TODO: This does not ever appear to be called...
        e.preventDefault();
        // If course is using ETS gift cards, ETS does not support
        // refunding a gift card payment without re-scanning the card
        if(this.model.has('card_type') && this.model.get('card_type') == 'Giftcard'){
            var refund_window = new EtsGiftCardRefundWindow({model: this.model});
            refund_window.show();

        }else{
            this.model.destroy({wait: true});
        }
        return false;
    }
});
