var FbPageHeaderView = Backbone.Marionette.ItemView.extend({
	tagName: "div",
	className: "",
	template: JST['foodbev/page_header_info.html'],

	initialize: function(){
		this.listenTo(this.collection, 'change:active', this.render);
	},

	modelEvents: {
		'change:suspended_sale_id': 'render',
		'change:table_num': 'render',
		'reset': 'render'
	},

	render: function(){
		var attr = this.model.attributes;
		var terminal = this.collection.getActiveTerminal();

		attr.terminal = false;
		if(terminal){
			attr.terminal = terminal
		}
		
		this.$el.html( this.template(attr) );
		return this;
	}
});
