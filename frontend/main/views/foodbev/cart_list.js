var FbCartListView = Backbone.View.extend({
	
	tagName: "div",
	className: "",
	template: JST['foodbev/cart_items.html'],
	
	initialize: function() {
		this.listenTo(this.collection, "add remove reset change:meal_course_id change:seat", this.render);
	},

	events: {
		'click button.up': 'scrollItemsUp',
		'click button.down': 'scrollItemsDown'
	},

	render: function() {
		var self = this;
		this.$el.html(this.template());
		this.$el.find('tbody').html('');
		this.table = this.$el.find('tbody');
		
		// If course uses "meal course firing", items should be grouped
		// by meal course within cart
		if(App.data.course.get('use_course_firing') == 1){
			
			var meal_courses = App.data.course.get('meal_courses').toJSON();
			meal_courses = new MealCourseCollection(meal_courses);
			meal_courses.add({'order': '0', 'name': '', 'meal_course_id': '0'});
			
			_.each(this.collection.models, function(item){
				item.set({'displayed': false}, {'silent': true});
			});			
			
			_.each(meal_courses.models, this.addCourseSeparator, this);
			
		}else{
			_.each(this.collection.models, this.addItem, this);
		}

		this.$el.find('div.table-container').scrollTop( this.$el.find('div.table-container').prop('scrollHeight') );
		return this;
	},

	addItem: function(item){
		item.set({'displayed': true}, {'silent': true});
		this.table.append( new CartListItemView({model:item}).render().el );
	},

	addCourseSeparator: function(meal_course){
		
		var items = [];
		if(meal_course.get('meal_course_id') == 0){
			items = this.collection.where({'displayed': false});		
		}else{
			items = this.collection.where({'meal_course_id': meal_course.get('meal_course_id')});		
		}
		
		var params = {};
		if(App.data.course.get('food_bev_sort_by_seat') == 1){
			params = {comparator: 'seat'};
		}
		items = new Backbone.Collection(items, params);	
		
		// If no items are found within that meal course, don't show the course separator
		if(!items || items.length == 0){
			return false;
		}		
		
		// Place course separator in cart list
		var course_separator = new CartListCourseSeparatorView({model:meal_course});
		this.table.append( course_separator.render().el );			
		
		// Show items underneath course separator
		_.each(items.models, this.addItem, this);
	},

	scrollItemsUp: function(event){
		var list = this.$el.find('.table-container');
		var curPos = list.scrollTop();
		
		list.scrollTop(curPos - 120);
		return false;
	},
	
	scrollItemsDown: function(event){
		var list = this.$el.find('.table-container');
		var curPos = list.scrollTop();
		
		list.scrollTop(curPos + 120);	
		return false;	
	}
});
