var FbRecentTransactionView = Backbone.View.extend({
	tagName: "li",
	className: "fb-recent-transaction",
	template: JST['foodbev/recent_transaction.html'],

	events: {
		"click button.add_tip": "addTip",
		"click button.print_receipt": "printReceipt",
		"click button.view_receipt": "viewReceipt",
		"click button.email-receipt": "email_receipt"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model.get('payments'), "add remove change", this.render);
	},

	render: function() {
		
		var attrs = this.model.attributes;
		attrs.tips = new PaymentCollection();
		
		// Seperate out tips to be displayed on their own
		_.each(attrs.payments.models, function(payment){
			if(payment.get('description').toLowerCase().indexOf('tip') > -1){
				attrs.tips.add(payment.attributes);
			}
		});
		
		this.$el.html(this.template(attrs));
		return this;
	},

	// Opens the receipt details in a new window
	viewReceipt: function(event){
		var receipt = new ReceiptWindowView({model: this.model});
		receipt.show();
		return false;
	},

	printReceipt: function(event){
		var sale = this.model.attributes;
		console.log('print the foodbev sale',sale)
		var receipt = new FBSalePrintReceipt(sale);
		receipt.print_network('sale', true);
		
		return false;
	},

	email_receipt: function(e){
		e.preventDefault();
		var emailReceipt = new EmailReceiptWindowView({model: this.model});
		emailReceipt.show();
		return false;
	},

	addTip: function(event){
		var addTipWindow = new TipWindowView({model: this.model});
		addTipWindow.show();
		
		return false;
	}
});
