var FbRecentTransactionDetailsView = ModalView.extend({
	tagName: "li",
	template: JST['foodbev/recent_transaction_details.html'],

	events: {
		"click button.close": "close"
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});
