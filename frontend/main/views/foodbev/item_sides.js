var ItemSidesView = Backbone.View.extend({
	tagName: "div",
	className: "sides inner-content",

	initialize: function(attributes, options){
		this.options = {};
		this.options.section_id = attributes.section_id;
		this.options.category_id = attributes.category_id;
		this.options.number_soups = attributes.number_soups;
		this.options.number_salads = attributes.number_salads;
		this.options.number_sides = attributes.number_sides;
		this.options.item_edit_view = attributes.item_edit_view;
		this.sides = App.data.food_bev_sides;
	},

	events: {
		"click .side": "selectSide",
		"click .no_side": "removeSide",
		"click .back": "back",
		"click .side_modifiers": "editModifiers"
	},

	render: function(){
		
		data = this.model.attributes;
		data.category_id = this.options.category_id;
		if(!data.hidden_sides){
			data.hidden_sides = [];
		}

		// If section is 'Soups/Salads'
		if(data.category_id == 4){
			this.template = JST['foodbev/item_soup_salad.html'];

		// Otherwise section is just 'Sides'
		}else{
			this.template = JST['foodbev/item_sides.html'];
		}
		
		this.$el.html(this.template(data));
		return this;
	},

	editModifiers: function(event){
		var sidePosition = $(event.currentTarget).data('position');

		// Get side
		var side = this.model.get('sides').get(sidePosition);
		var title = side.get('name') + ' > Modifiers';

		// Remove current sides window for now
		this.remove();

		// Show side modifiers window
		var view = new ItemModifiersView({
			model: side, 
			section_id: this.options.section_id, 
			title: title, 
			item_edit_view: this.options.item_edit_view
		});
		$('#item-edit-' + this.options.section_id).html( view.render().el );

		event.preventDefault();
		return false;
	},

	selectSide: function(event, sideType){
		
		var button = $(event.currentTarget);

		// Mark button as 'selected'
		button.addClass('active').siblings().removeClass('active');
		var side_id = button.data('side-id');
		var position = button.data('position');
		var type = button.data('type');

		// Retrieve the selected side and assign it the proper side 'position'
		var side = this.sides.get(side_id);
		var attributes = side.toJSON();

		var price = _.round(attributes.add_on_price);	
	
		if(this.model.get('side_prices')){
			var sidePrices = this.model.get('side_prices');

			if(sidePrices[parseInt(attributes.item_id)] !== undefined){
				price = sidePrices[parseInt(attributes.item_id)];
				if(isNaN(price)){
					price = 0;
				}
			}
		}
		attributes.position = position;
		attributes.price = price;
		attributes.unit_price = price;
		attributes.quantity = this.model.get('quantity');
		
		var options = {};
		if(this.model.cart instanceof Cart){
			options.cart = this.model.cart;
		}
		
		var itemSide = new FbItemSide(attributes, options);
		itemSide.calculatePrice();
		
		// Add the side to item
		if(type == 'side'){
			var sideCollection = this.model.get('sides');
		}else if(type == 'salad'){
			var sideCollection = this.model.get('salads');
		}else if(type == 'soup'){
			var sideCollection = this.model.get('soups');
		}

		if(sideCollection){
			sideCollection.add(itemSide, {merge: true});
		}
		
		// Show/hide side modifiers button
		var sideModifiersButton = this.$el.find('button.side_modifiers[data-position="' + position + '"]');
		if(itemSide.get('modifiers').length > 0){
			sideModifiersButton.show();
		}else{
			sideModifiersButton.hide();
		}

		return false;
	},

	removeSide: function(event){
		var button = $(event.currentTarget);

		// Mark button as 'selected'
		button.parents('div.side').find('a').removeClass('selected');
		button.addClass('selected');
		var position = button.data('position');
		var type = button.data('type');

		// Create a new side model that represents 'no side'
		var side = new ItemSide({'position':position, 'item_id':0, 'tax':0, 'total':0, 'name':'None', 'price':0});

		// Get appripriate collection to modify
		if(type == 'side'){
			var sideCollection = this.model.get('sides');
		}else if(type == 'salad'){
			var sideCollection = this.model.get('salads');
		}else if(type == 'soup'){
			var sideCollection = this.model.get('soups');
		}

		if(sideCollection){
			sideCollection.add(side, {'merge':true});
		}

		return false;
	}
});
