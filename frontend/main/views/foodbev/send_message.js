var FbSendMessageView = ModalView.extend({
	tagName: "div",
	template: JST['foodbev/send_message.html'],

	events: {
		"click .send": "sendMessage"
	},
	
	initialize: function(){
		ModalView.prototype.initialize.call(this);		
	},

	render: function() {
		this.$el.html(this.template());	
		return this;
	},
	
	onShow: function(){
		this.$el.find('textarea').focus();	
	},
	
	sendMessage: function(event){
		var table = App.data.food_bev_table;
		var message = $('#item_message').val();
		var url = table.url + 'orders';

		if(!message){
			App.vent.trigger('notification', {'msg': 'Message is required', 'type': 'error'});
			return false;
		}

		// Clone the cart item to place in the order object
		var itemCopy = new FbCartItem( this.model.toJSON() );

		// Create a new order with list of items and save it to database
		var order = new Order({
			'message':message, 
			'items': [itemCopy],
			'table_number': table.get('table_num'),
			'guest_count': table.get('guest_count'),
			'customers': table.get('customers').toJSON(),
			'table_name': table.get('name')
		}, {'url':url});
		
		order.save();
		
		this.hide();
		return false;
	}
});
