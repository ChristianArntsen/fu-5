var EmployeeButtonView = Backbone.Marionette.ItemView.extend({
	tagName: 'div',
	className: 'col-xs-12 col-sm-6 col-md-4 employee',
	template: function(attrs){
		return '<button class="btn btn-primary btn-lg btn-block employee">'+attrs.first_name +' '+ attrs.last_name+'</button>';
	},
	
	initialize: function(options){
		if(options && options.hasOwnProperty('table')){
			this.table = options.table;
		}
	},
	
	events: {
		"click button": "switchEmployee"
	},
	
	switchEmployee: function(){

		var employee_id = this.model.get('person_id');
		var first_name = this.model.get('first_name');
		var last_name = this.model.get('last_name');
		var table = this.table;
		
		this.table.changeEmployee(employee_id, function(){
			table.set({
				'employee_first_name': first_name,
				'employee_last_name': last_name,
				'employee_id': employee_id
			});
			
			$('#modal-employee-select-window').modal('hide');
		});
		
		return false;
	}
});

var EmployeeButtonListView = Backbone.Marionette.CollectionView.extend({
	tagName: 'div',
	className: 'row',
	childView: EmployeeButtonView,
	childViewOptions: function(){
		return {
			table: this.options.table
		};
	}
});

var FbEmployeeSelectWindow = ModalView.extend({
	
	id: 'employee-select-window',
	template: JST['foodbev/employee_select_window.html'],

	initialize: function(attrs, options){
		ModalView.prototype.initialize.call(this, options);
	},
	
	render: function() {
		this.$el.html(this.template());
		this.buttonsView = new EmployeeButtonListView({collection: this.collection, table: this.model});
		this.$el.find('.modal-body').html(this.buttonsView.render().el);
		return this;
	},
	
	onHide: function(){
		this.buttonsView.destroy();
	}
});
