var ItemModifiersView = Backbone.View.extend({
	tagName: "div",
	className: "modifiers inner-content",
	template: JST['foodbev/item_modifiers.html'],

	initialize: function(attributes, options){
		this.options = {};
		if(attributes.section_id){
			this.options.section_id = attributes.section_id;
		}
		if(attributes.title){
			this.options.title = attributes.title;
		}
		if(attributes.item_edit_view){
			this.item_edit_view = attributes.item_edit_view;
		}
	},

	events: {
		"click .modifier-option": "selectModifierOption",
		"click .back": "back",
		"click button.scroll-up": "scrollModifiersUp",
		"click button.scroll-down": "scrollModifiersDown"
	},

	render: function() {
		data = {};
		data.modifiers = this.model.get('modifiers');
		data.section_id = this.options.section_id;
		data.title = this.options.title;

		if(data.section_id == 3){
			data.category_id = 2;

		}else if(data.section_id == 2){
			data.category_id = 1;

		}else if(data.section_id == 1){
			data.category_id = 3;

		}else{
			data.category_id = null;
		}

		this.$el.html(this.template(data));
		return this;
	},
	
	scrollModifiersUp: function(event){
		event.preventDefault();
		var button = $(event.currentTarget);
		var list = this.$el.find('div.modifiers-pane');
		var curPos = list.scrollTop();
		var rowHeight = list.find('div.row.modifier').first().outerHeight();
		
		list.scrollTop(curPos - rowHeight);
		return false;
	},
	
	scrollModifiersDown: function(event){	
		event.preventDefault();
		var button = $(event.currentTarget);
		var list = this.$el.find('div.modifiers-pane');
		var curPos = list.scrollTop();
		var rowHeight = list.find('div.row.modifier').first().outerHeight();

		list.scrollTop(curPos + rowHeight);	
		return false;	
	},	

	selectModifierOption: function(event){
		
		var btn = $(event.currentTarget);
		var modifierId = btn.data('modifier-id');
		var modifier = this.model.get('modifiers').get(modifierId);
		var selected = btn.data('value');
		var price = btn.data('price');

		if(modifier.get('multi_select') == 1){
			btn.toggleClass('active');
			var options = this.getSelectedOptions(btn);		
			selected = options.selected;
			price = options.price;
			
		}else{
			btn.addClass('active');
			btn.siblings().removeClass('active');						
		}
		
		btn.parents('div').siblings('.modifier-price').find('span.value').text( accounting.formatMoney(price, '') );
		modifier.set({'selected_option': selected, 'selected_price': price});
		
		return false;
	},
	
	getSelectedOptions: function(btn){
		var buttons = btn.parent('div.btn-group').children('button.active');
		var selected = [];
		var price = 0.00;
		
		$.each(buttons, function(index, el){
			selected.push($(this).data('value'));
			price += _.round($(this).data('price'), 2);
		});
		
		return {'price': _.round(price, 2), 'selected': selected};
	},

	back: function(event){
		this.remove();
		this.item_edit_view.renderSection(this.options.section_id);
		return false;
	}
});
