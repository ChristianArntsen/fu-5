var RegisterLogView = ModalView.extend({
	
	tagName: "div",
	attributes: {'id':"register_log"},

	events: {
		"click .skip": "skip",
		"click .save": "save",
		"click .close-register": "close",
		"keypad_close": "calculateTotal",
		"blur input.change": "calculateTotal",
		"click #continue_log": "continueLog",
		"click #reopen_log": "reOpenLog"
	},

	initialize: function(attrs){
		
		this.reopen = false;
		if(attrs != undefined && attrs.template && attrs.template == 'close'){
			this.template = JST['foodbev/close_register.html'];
			this.register_close = true;
		}else{
			this.template = JST['foodbev/open_register.html'];
			this.register_close = false;
		}
		
		if(attrs && attrs.page){
			this.page = attrs.page;
		}		
		
		if(attrs && attrs.closable){
			this.closable = attrs.closable;
		}
		
		this.logout = false;
		if(attrs && attrs.logout){
			this.logout = true;
		}
		
		if(attrs && attrs.backdropOpacity){
			this.backdropOpacity = attrs.backdropOpacity;
		}
		
		ModalView.prototype.initialize.call(this, attrs);
		if(this.collection){
			this.listenTo(this.collection, 'error', this.displayError);	
		}
	},
	
	displayError: function(model, response){
		App.vent.trigger('notification', {'msg': response.responseJSON.msg, 'type': 'error'});
		$.loadMask.hide();
	},
	
	render: function(cash_sales_amount) {

		var data = this.model.attributes;
		data.closable = false;
		data.blind_close = App.data.course.get('blind_close');
		
		if(data.open_amount === false){
			data.open_amount = App.data.course.get('default_register_log_open');
		}

		if(cash_sales_amount != undefined){
			data.cash_sales_amount = cash_sales_amount;
		}
		data.multiCashDrawers = App.data.course.get_setting('multi_cash_drawers');
		data.drawer_number = App.data.course.get_setting('cash_drawer_number');
		
		this.$el.html(this.template(data));
		
		if(this.page == 'food_and_beverage'){
			this.$el.find('input[type="number"]').not('#register_close_amount').keypad({position: 'bottom-right', formatMoney: false});
			this.$el.find('#register_close_amount').keypad({position: 'right'});
		}
		
		if(this.register_close && data.blind_close == '0' && cash_sales_amount == undefined){
			this.$el.loadMask();
			this.get_cash_sales();	
		}	

		return this;
	},
	
	get_cash_sales: function(){
		var view = this;
		
		$.get(API_URL + '/sales/cash_sales_total', {
			'start': view.model.get('shift_start'),
			'end': moment().format(),
			'employee_id': view.model.get('employee_id'),
			'terminal_id': view.model.get('terminal_id'),
			'persist': view.model.get('persist')
		
		}, function(response){
			view.render(response.cash_sales_amount);
		});
	},
	
	save: function(e){
		var register_log_view = this;
		var amount = parseFloat( accounting.unformat($('#register_open_amount').val()) );
		this.$el.loadMask();
		
		if(isNaN(amount) || amount < 0){
			App.vent.trigger('notification', {msg: 'Please enter a valid dollar amount', type: 'error'});
			return false;
		}
		var terminal = App.data.course.get('terminals').findWhere({'active': true});
		
		var persist = 0;
		var terminal_id = 0;
		var cash_drawer_number = 0;
		if(terminal){
			persist = parseInt(terminal.get('persistent_logs'));
			terminal_id = terminal.get('terminal_id');
			cash_drawer_number = this.$el.find('#cash-drawer-number').val();
		}

		this.model.set({
			'open_amount': amount,
			'persist': persist,
			'terminal_id': terminal_id,
			'drawer_number': cash_drawer_number
		});

		if(!this.model.get('employee_id')){
			this.model.set('employee_id', App.data.user.get('person_id'))
		}

		this.model.unset('shift_start');

		// Create a new cash log
		this.collection.create(this.model, {wait: true, success: function(model){
			
			App.data.course.set('unfinished_register_log', 1);
			App.data.register_logs.set_active(model.register_log_id);

			if(register_log_view.page == 'food_and_beverage'){
				var layout_window = new TableLayoutWindowView({
					collection: App.data.food_bev_table_layouts
				}, {
					closable: false,
					backdropOpacity: '0.9'
				});
				$('div.modal').modal('hide');
				layout_window.show();
			
			}else{
				register_log_view.hide();
			}
						
		}, error: function(model, response){
			register_log_view.displayError(model, response);
		}});
		
		return false;
	},
	
	reOpenLog: function(){
		this.template = JST['foodbev/close_register.html'];
		this.register_close = true;
		this.reopen = true;
		this.closable = false;
		this.render();
		return false;
	},
	
	continueLog: function(){
		return this.skip();
	},
	
	calculateTotal: function(){
		
		var fields = this.$el.find('input.change');
		var total = 0.00;
		
		$.each(fields, function(index){	
			var qty = parseInt($(this).val());
			var amount = _.round($(this).data('amount'));
			total += _.round(qty * amount);
		});
		
		this.$el.find('#register_close_amount').val( String(accounting.formatNumber(total, 2, "")) );
		return true;
	},
	
	close: function(e){
		var data = {'counts': {}};
		var view = this;
		this.$el.find('div.modal-content').loadMask();
		
		data.close_check_amount = parseFloat($('#register_close_check_amount').val());
		data.close_amount = parseFloat($('#register_close_amount').val());
		data.counts.pennies = $('#register_close_pennies').val();
		data.counts.nickels = $('#register_close_nickels').val();
		data.counts.dimes = $('#register_close_dimes').val();
		data.counts.quarters = $('#register_close_quarters').val();
		data.counts.ones = $('#register_close_ones').val();
		data.counts.fives = $('#register_close_fives').val();
		data.counts.tens = $('#register_close_tens').val();
		data.counts.twenties = $('#register_close_twenties').val();
		data.counts.fifties = $('#register_close_fifties').val();
		data.counts.hundreds = $('#register_close_hundreds').val();
		data.closing_employee_id = App.data.user.get('person_id');
		data.status = 'close';

		if(isNaN(data.close_amount) || data.close_amount < 0){
			App.vent.trigger('notification', {msg: 'Close amount must be a valid dollar amount', type: 'error'});
			$.loadMask.hide();
			return false;
		}
		
		// Mark current log as "closed" (set shift end/close amount)
		this.model.set(data);
		this.model.save(null, {wait: true, success: function(model){

			if(view.reopen){
				view.model = new RegisterLog();
				view.template = JST['foodbev/open_register.html'];
				view.register_close = false;
				view.closable = false;
				view.reopen = false;
				view.render();
				
			}else if(view.page == 'food_and_beverage'){
				App.data.user.food_bev_logout();
				view.hide();
			
			}else{
				if(view.logout){
					window.location = SITE_URL + '/home/logout';
				}else{
					view.hide();
				}
			}	
		}});
		
		return false;
	},

	skip: function(e){
		
		if(!this.model.isNew()){
			$.post(API_URL + '/register_logs/continue', null);
			App.data.course.set('continued_register_log', 1);		
		
		}else{
			$.post(API_URL + '/register_logs/skip', null);
			App.data.course.set('skipped_register_log', 1);
		}
		
		if(this.page == 'food_and_beverage'){
			var layout_window = new TableLayoutWindowView({
				collection: App.data.food_bev_table_layouts
			}, {
				closable: false,
				backdropOpacity: '0.9'
			});		
			
			$('div.modal').modal('hide');
			layout_window.show();
		
		}else{
			this.hide();
		}
		
		return false;
	}
});
