var CartListItemView = Backbone.View.extend({
	tagName: "tr",
	template: JST['foodbev/cart_item.html'],

	events: {
		"click .edit": "openEditWindow",
		"click .message": "openMessageWindow",
		"click": "selectItem",
		"click button.add": "addQuantity",
		"click button.sub": "subtractQuantity",
		"click button.modifier": "toggleModifiers",
		"click button.void": "deleteItem"
	},

	initialize: function(){
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model, "change:incomplete", this.markIncomplete);
		this.listenTo(this.model, "change:is_new", this.renderQuantityButtons);
		this.listenTo(this.model.get('modifiers'), "change", this.render);
		this.listenTo(this.model.get('sides'), "add remove change", this.render);
		this.listenTo(this.model.get('soups'), "add remove change", this.render);
		this.listenTo(this.model.get('salads'), "add remove change", this.render);
		this.listenTo(App.data.food_bev_table, "change:admin_auth_id", this.toggleVoidButton);

		this.modifiersCollapsed = true;
	},

	toggleVoidButton: function(){
		if(this.model.get('is_ordered') && App.data.food_bev_table.get('admin_auth_id')){
			this.$el.find('button.void').show();
		}else{
			this.$el.find('button.void').hide();
		}
	},

	deleteItem: function(event){
		this.saved = true;
		if(this.receipt && this.receipt.getTotalPayments && this.receipt.getTotalPayments() > 0){
			App.vent.trigger('notification', {'type':'error', 'msg':'Please cancel partial payment(s) before deleting items from receipt.'});
			return false;
		}
		this.model.destroy();
		return false;
	},

	render: function(){
		console.log('rendering cart item...')

		this.$el.html(this.template(this.model.attributes));
		if(this.model.get('selected')){
			this.$el.addClass('selected');
		}else{
			this.$el.removeClass('selected');
		}

		if(this.model.get('is_ordered')){
			this.$el.addClass('ordered');
		}else{
			this.$el.removeClass('ordered');
		}
		if(this.model.get('is_paid') || (this.model.get('comp_total') && this.model.get('comp_total')>0) ){
			this.$el.addClass('paid');
		}else{
			this.$el.removeClass('paid');
		}

		// If the item has any required modifiers or sides, then automatically
		// pop up the edit window
		if(!this.model.get('showed_edit') && (
				this.model.get('modifiers').findWhere({'required': true}) ||
				this.model.get('number_soups') > 0 ||
				this.model.get('number_salads') > 0 ||
				this.model.get('number_sides') > 0
		)){
			// Set flag in item, so window doesn't keep popping up later
			this.model.set({'showed_edit': true});
			this.openEditWindow();
		}

		if(!this.modifiersCollapsed){
			this.expandModifiers();
		}

		this.toggleVoidButton();

		return this;
	},

	addQuantity: function(event){
		this.model.set({'quantity': parseInt(this.model.get('quantity')) + 1});
		this.deBouncedSave(this.model);
		return false;
	},
	deBouncedSave: _.debounce(function(model){
        model.save();
	},1000),

	subtractQuantity: function(event){
		if((this.model.get('quantity') - 1) <= 0){
			return false;
		}
		this.model.set({'quantity': this.model.get('quantity') - 1});
        this.deBouncedSave(this.model);
		return false;
	},

	renderQuantityButtons: function(){
		if(this.model.get('is_new')){
			this.$el.find('div.quantity_buttons').show();
		}else{
			this.$el.find('div.quantity_buttons').hide();
		}
	},
	
	openEditWindow: function(e){
		var item = this.model;
		var editItemWindow = new FbEditItemView({model:item,receipt:item.collection.receipt});

		var highlightNotCompleted = false;
		if(item.get('incomplete')){
			var highlightNotCompleted = true;
		}

		editItemWindow.show();
		if(e){
			e.preventDefault();
		}
		return false;
	},

	openMessageWindow: function(){
		var item = this.model;
		var sendMessageWindow = new FbSendMessageView({model: item});
		sendMessageWindow.show();

		return false;
	},

	selectItem: function(event){
		if(this.model.get('is_ordered')){
			return false;
		}

		if(this.model.get('selected')){
			this.model.set({"selected": false});
			this.$el.removeClass('selected');
		}else{
			this.model.set({"selected": true});
			this.$el.addClass('selected');
		}
		return false
	},

	markIncomplete: function(event){
		if(this.model.get('incomplete')){
			this.$el.addClass('incomplete');
		}else{
			this.$el.removeClass('incomplete');
		}
		return false
	},

	toggleModifiers: function(event){
		if(this.modifiersCollapsed){
			// expand the modifiers
			this.expandModifiers();
		}
		else{
			// collapse the modifiers
			this.collapseModifiers();
		}

		if(event){
			event.preventDefault();
			return false;
		}
	},

	expandModifiers: function(){
		this.modifiersCollapsed = false;
		this.$el.find('ul.modifiers').addClass('expanded');
	},

	collapseModifiers: function(){
		this.modifiersCollapsed = true;
		this.$el.find('ul.modifiers').removeClass('expanded');
	}
});
