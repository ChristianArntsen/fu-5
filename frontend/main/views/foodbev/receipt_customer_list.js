var ReceiptCustomerListView = Backbone.View.extend({
	
	tagName: 'div',
	className: 'col-md-12',
	
	initialize: function(options) {
		if(options && options.receipt){
			this.receipt = options.receipt;
		}

		this.listenTo(this.collection, "add", this.render);
		this.listenTo(this.collection, "remove", this.render);
		this.listenTo(this.collection, "reset", this.render);
	},

	render: function() {
		var self = this;
		this.$el.html('');
		
		if(this.collection.models.length > 0){
			_.each(this.collection.models, this.addCustomer, this);
		}else{
			this.$el.html('<h3 class="text-muted">No customers found</h3>');
		}
		return this;
	},

	addCustomer: function(customer){
		var view = this;
		this.$el.append( new ReceiptCustomerListItemView({model: customer, receipt: view.receipt}).render().el );
	}
});
