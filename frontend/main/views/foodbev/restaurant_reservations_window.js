var RestaurantReservationsWindow = ModalView.extend({
	
	id: 'restaurant-reservations',
	template: JST['foodbev/restaurant_reservations_window.html'],

	initialize: function(attrs, options){
		ModalView.prototype.initialize.call(this, {extraClass: 'modal-lg'});
		this.listenTo(this.collection, 'fetch reset add remove', this.renderCounts);
	},
	
	events: {
		'click button.new-reservation': 'newReservation',
		'dp.change': 'getReservations'
	},
	
	render: function() {
		this.reservationsView = new RestaurantReservationListView({collection: this.collection});
		var date = App.data.restaurant_reservations.date;
		var totals = this.collection.getTotals();

		var attributes = {
			date: date,
			total: totals.total,
			guests: totals.guests
		}

		this.$el.html(this.template(attributes));
		this.$el.find('div.modal-body').html( this.reservationsView.render().el );

		return this;
	},

	onShow: function(){
		this.$el.find('#reservations-date').datetimepicker({
			format: 'M/D/YY'
		});		
	},

	renderCounts: function(){
		var totals = this.collection.getTotals();
		this.$('#total-reservations').text(totals.total);
		this.$('#total-guests').text(totals.guests);
		return false;
	},
	
	newReservation: function(){
		this.reservationsView.addNew();
	},
	
	onHide: function(){
		this.reservationsView.destroy();
	},
	
	getReservations: function(){
		var input = this.$el.find('#reservations-date');

		if(!input || !input.data('DateTimePicker')){
			return false;
		}

		var date = input.data('DateTimePicker').date();
		input.val(date.format('M/D/YY'));

		this.collection.fetch({data: {
			date: date.format('M/D/YY')
		}});
		this.collection.date = date.format('M/D/YY');
		
		input.data('DateTimePicker').hide();
		return false;
	}
});
