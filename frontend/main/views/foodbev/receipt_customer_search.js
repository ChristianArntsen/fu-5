var ReceiptCustomerSearchView = ModalView.extend({
	
	id: 'customer-search-window',
	
	template: JST['foodbev/receipt_customer_search.html'],
	events: {
		"keyup input.search": "search",
		"click button.no-customer": "clearCustomer"		
	},
	
	initialize: function(options) {
		
		if(!options){
			options = {};
		}
		
		if(options && options.receipt){
			this.receipt = options.receipt;
		}
		
		ModalView.prototype.initialize.call(this, options);			
	},

	render: function() {
		var self = this;
		this.$el.html(this.template());
		return this;
	},
	
	search: function(event){
		var term = this.$el.find('input.search').val();
		var view = this;
		var receipt = this.receipt;
		
		if(term == ''){
			return false;
		}
		
		if(view.lastRequest){
			view.lastRequest.abort();
		}
		this.$el.find('input.search').addClass('loading');
		this.searchRequest(term, view);
	},
	
	searchRequest: _.debounce(function(term, view){
		var url = BASE_URL + 'index.php/api/customers';
	
		view.lastRequest = $.get(url, {q: term}, function(response){	
			var customers = new CustomerCollection(response);
			var customerSearchResults = new ReceiptCustomerListView({collection: customers, receipt: view.receipt});
			view.$el.find('div.customer-list').html( customerSearchResults.render().el );
			view.$el.find('input.search').removeClass('loading');
		}, 'json');
	
	}, 200),

	clearCustomer: function(event){
		event.preventDefault();
		this.receipt.removeCustomer();
		this.hide();		
	}	
});
