var PaymentView = Backbone.View.extend({
	tagName: 'li',
	className: 'payment',
	template: JST['foodbev/payment.html'],

	events: {
		"click a.delete": "deletePayment",
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
		if( this.model.attributes.merchant_data &&
			this.model.attributes.merchant_data.type ==='credit_card_save'){

			this.model.set('tran_type','PreAuth');
		}
	},

	render: function(){
		console.log('render payment',this.model.attributes);
		this.$el.html(this.template(this.model.attributes));
		return this;
	},

	deletePayment: function(e){
        if(e.preventDefault)e.preventDefault();
		if(App.use_ets_giftcards == 1 && this.model.get('type').search('Gift') > -1){
			var data = {};
			data.receipt_id = this.model.get('receipt_id');
			data.ets_type = 'giftcard';
			data.action = 'refund';
			data.payment_type = this.model.get('type');
			data.amount = parseFloat(this.model.get('amount'));
			
			var giftCardWindow = $('#payment_ets_gift_card');
			giftCardWindow.html('');
			giftCardWindow.show().siblings().hide();
			giftCardWindow.parent('div.left').addClass('max');
			$('div.payment-amount').hide();

			// Open credit card processing window
			$('#cbox2LoadedContent').mask('Please Wait');
			$.post(SITE_URL + '/food_and_beverage/credit_card_window', data, function(response){
				giftCardWindow.html(response);
			},'html');	
			
			$('#payment_ets_gift_card').show().siblings().hide();
			return false;
									
		}else{
			$('#payment_window .modal-content').loadMask();
			
			this.model.destroy({'wait':true, 'success':function(response){
				$.loadMask.hide();
				//$('#cbox2LoadedContent').unmask();
			}});
		}
		return false;
	}
});
