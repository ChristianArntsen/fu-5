var FoodBeverageLayout = Backbone.Marionette.LayoutView.extend({
	
	tagName: 'div',
	className: 'row page',
	attributes: {
		id: 'page-food-beverage'
	},
	template: JST['foodbev/foodbeverage.layout.html'],

	serializeData: function(){
		return {
			'use_course_firing': App.data.course.get('use_course_firing')
		}
	},

	events: {
		"click #send_order": "sendOrder",
		"click #pay_now": "payNow",
		"click #logout": "logout",
		"click #close_register": "closeRegister",
		"click #select_table": "selectTable",
		"click #cancel_sale": "cancelSale",
		"click #recent_transactions": "recentTransactions",
		"click #edit_button_order": "editButtonOrder",
		"click #save_button_order": "saveButtons",
		"click #open_cash_drawer": "open_cash_drawer",
		"click button.select-all": "select_all_cart_items",
		"click #fire_items": "fire_items",
		"click #merge_table": "mergeTable",
		"blur #table-name": "saveTableName",
		"cardswipe #table-customer-search": "submitCustomerSearch",
		"click .toggle-customers": "toggleCustomers"
	},
	
	regions: {
		cart_list: '#fb-cart-items',
		cart_totals: '#fb-cart-totals',
		item_buttons: '#fb-items',
		category_buttons: '#fb-categories',
		customers: '#fb-customers',
		guest_count: '#guest_count'
	},	
	
	initialize: function(attrs, options){
		
		this.listenTo(App.data.food_bev_table, 'change:name', this.refreshTableName);
		this.listenTo(App.data.user, 'change:fnb_logged_in', this.showTableLayout);		
		this.customersExpanded = false;
		// Refresh current table layout data
		App.data.food_bev_table_layouts.fetch();
		
		// If user is not logged in to F&B, show log in window
		if(!App.data.user.get('fnb_logged_in')){	
			App.data.food_bev_table_layouts.fetch();
			var auth_window = new AuthorizeWindowView({
				action: 'Login', 
				closable: false, 
				backdropOpacity: '0.9'
			});
			auth_window.show();
			return false;
		}

		this.showTableLayout(App.data.user);
	},
	
	mergeTable: function(){
		var mergeWindow = new MergeTableWindow({
			model: App.data.food_bev_table, 
			collection: App.data.food_bev_table_layouts
		});
		mergeWindow.show();
	},

	showRegisterOpen: function(){
		
		var user = App.data.user;
		if(!user.get('fnb_logged_in') || 
			App.data.course.get_setting('use_register_log') == 0 || 
			App.data.course.get_setting('skipped_register_log') == 1){
			return true;
		}

		var register_log = App.data.register_logs.get_open_log();
		
		// If there are no open register logs, show the window to open
		// the register log
		if(!register_log || register_log == undefined){
			var register_log_window = new RegisterLogView({
				model: new RegisterLog(),
				collection: App.data.register_logs,
				page: 'food_and_beverage',
				closable: false,
				backdropOpacity: '0.9'
			});
			
			register_log_window.show();

		// If there is an open register log, and it is persistent,
		// ask user if they would like to continue it or create a new one
		}else if(register_log && App.data.course.get('unfinished_register_log') == 0){
			var register_log_window = new RegisterLogView({
				model: register_log,
				collection: App.data.register_logs,
				page: 'food_and_beverage',
				closable: false,
				backdropOpacity: '0.9'
			});
			
			register_log_window.show();
		}			
	},
	
	showTableLayout: function(user){
		
		var register_log = App.data.register_logs.get_open_log();	
		var terminal = App.data.course.get('terminals').getActiveTerminal();

		// Skip the table layout window if a register log needs attention
		if(
			!user.get('fnb_logged_in') || 
			(
				terminal && terminal.get('terminal_id') != 0 &&
				App.data.course.get_setting('use_register_log') == 1 && 
                App.data.course.get('skipped_register_log') != 1 &&
                App.data.course.get('continued_register_log') != 1 &&
				(register_log == undefined || !register_log || App.data.course.get('unfinished_register_log') == 0)
			)
		){
			this.showRegisterOpen();
			return true;
		}
		
		// Show table layout window
		var table_layout = new TableLayoutWindowView({
			collection: App.data.food_bev_table_layouts
		}, {
			closable: false,
			backdropOpacity: '0.9'
		});
		table_layout.show();
		
		return false;
	},

	onRender: function(){
		
		this.$el.find('#table-customer-search').cardSwipe({
			setInput: false
		});

		init_customer_search(this.$el.find('#table-customer-search'), function(e, customer, list){	
			$(e.currentTarget).typeahead('val', '');
			App.data.food_bev_table.addCustomer(customer);
		});
	},

	// Search for a customer by account number (from card swipe)
	submitCustomerSearch: function(e, data){
		
		this.$el.find('#table-customer-search').data('Bloodhound').remote.cancelLastRequest();
		this.$el.find('#table-customer-search').typeahead('val', '');
		
		$.get(API_URL + '/customers', {q: data}, function(response){
			if(!response || !response[0]){
				App.vent.trigger('notification', {type: 'error', msg: 'Customer not found'});
				return false;
			}
			// Add customer to table automatically
			var customer = response[0];
			App.data.food_bev_table.addCustomer(customer);
		});

		return false;
	},	
	
	saveTableName: function(event){
		var name = $(event.currentTarget).val();
		App.data.food_bev_table.set('name', name).save();
	},
	
	refreshTableName: function(model){
		var table_name = model.get('name');
		this.$el.find('#table-name').val(table_name);
	},
	
	sendOrder: function(){
		App.data.food_bev_table.sendOrder();
		return false;
	},
	
	fire_items: function(){
		var meal_courses = App.data.food_bev_table.get('fired_meal_courses');
		var window = new FireMealCourseWindow({collection: meal_courses});
		window.show();
		return false;
	},
	
	payNow: function(){
		var receipts = App.data.food_bev_table.get('receipts');
		var receiptsView = new FbReceiptWindowView({collection: receipts});
		receiptsView.show();
		if(receipts.models.length === 1) {
			_.forEach(receipts.models, function (receipt) {
				if (receipt.get('customer')) {
					receipt.applyCustomerDiscount();
				}
			});
		}

		if(receipts.length && receipts.allReceiptsZeroed()){
			var table = App.data.food_bev_table;
			if(confirm('All receipts show a zero balance.\n\n'+
					'Press *OK* to auto-complete the Sales (with $0 cash payment) and close the table\n\n'+
				    'Press *Cancel* to continue editing receipts')){
				var all_done = true;
				_.forEach(receipts.models,function(receipt){
					if(receipt.get('payments').length == 0 && receipt.get('items').length > 0) {
						all_done = false;
						receipt.get('payments').addPayment({
							'amount': 0.00,
							'customer_note': 'Comp all items',
							'type': 'cash',
							'description': 'Cash'
						}, function () {

							App.data.food_bev_table.close(function () {
								App.page.currentView.show_table_layouts();
							});
						});
					}
				});
				if(all_done){
					App.data.food_bev_table.close(function () {
						App.page.currentView.show_table_layouts();
					});
				}
			}
		}

	},
	
	logout: function(){
		App.data.user.food_bev_logout();
		return false;
	},
	
	closeRegister: function(){
		
		var register_log = App.data.register_logs.get_open_log();		
		
		if(!register_log || register_log == undefined){
			App.vent.trigger('notification', {'type': 'info', 'msg': 'There are no open cash logs to close'});
			return false;
		}
		
		var register_log_window = new RegisterLogView({
			model: register_log, 
			template: 'close',
			page: 'food_and_beverage',
			collection: App.data.register_logs
		});
		register_log_window.show();
		return false;
	},
	
	open_cash_drawer: function(){
		var receipt_printer = new Receipt();
		receipt_printer.open_cash_drawer();					
	},	
	
	selectTable: function(){
		var table_layout = new TableLayoutWindowView({
			collection: App.data.food_bev_table_layouts
		}, {
			last_layout: App.data.food_bev_table_layouts.last_layout
		});
		table_layout.show();
		return false;
	},
	
	cancelSale: function(){
		var self = this;
		var user = App.data.user;
		if(App.data.food_bev_table.get('orders_made') > 0){
			var auth_window = new AuthorizeWindowView({'action': 'Cancel Sale'});
			auth_window.show();
		
		}else{	
			if(!confirm('Are you sure you want to cancel this sale?')){
				return false;
			}

			// Attempt to close table, if table fails to close, display error
			if(!App.data.food_bev_table.close(function(){self.show_table_layouts()})){
				App.vent.trigger('notification', {msg: 'Please finish paying receipts or refund payments before canceling sale', type: 'error'});
			}
		}
		
		return false;
	},
	
	recentTransactions: function(){
		var window = new FbRecentTransactionsView({collection: App.data.food_bev_recent_transactions});
		window.show();
		return false;
	},
	
	editButtonOrder: function(e){
		this.$el.addClass('editing');
		var items = this.$el.find('#fb-items > div');
		var categories = this.$el.find('#fb-categories > div');
		
		categories.sortable({
			cancel: '',
			helper: 'clone'
		});		
		
		items.sortable({
			cancel: '',
			helper: 'clone'
		});	
		
		this.$el.find('.edit-button-order').replaceWith('<button class="btn btn-link pull-right save-button-order" id="save_button_order">Save Button Order</button>');	
		
		e.preventDefault();
		return false;		
	},
	
	saveButtons: function(e){
		e.preventDefault();
		var buttons = $('#fb-items > div.row, #fb-categories > div.row');
		this.saveButtonOrder(buttons);
		
		this.$el.removeClass('editing');
		this.$el.find('.ui-sortable').sortable("destroy");
		this.$el.find('.save-button-order').replaceWith('<button class="btn btn-link pull-right edit-button-order" id="edit_button_order">Edit Button Order</button>');
		
		return false;
	},
	
	saveButtonOrder: function(el){
		
		var buttons = $.map(el.children(), function(el){
			
			var ui_button = $(el).find('button');
			var element = $(el);
			
			if(!ui_button || ui_button.length == 0){
				ui_button = element;
			}

			var menu_button;
			if(ui_button.data('item-id') != 0){
        		menu_button = App.data.menu_buttons.findWhere({
					item_id: String(ui_button.data('item-id')),
					subcategory_id: String(ui_button.data('subcategory-id')),
					category_id: String(ui_button.data('category-id'))
				});
				
			}else if(ui_button.data('category-id') != 0 && ui_button.data('subcategory-id') != 0){
                menu_button = App.data.menu_buttons.findWhere({
					subcategory_id: String(ui_button.data('subcategory-id')),
					category_id: String(ui_button.data('category-id'))
				});

			}else{
                menu_button = App.data.menu_buttons.findWhere({
					category_id: String(ui_button.data('category-id'))
				});
			}

		    if (typeof menu_button == 'undefined') {
                // Add button
                var new_button = {
                    category_id:ui_button.data('category-id'),
                    category_name:'',
                    item_id:ui_button.data('item-id'),
                    item_name:'',
                    name:ui_button.html(),
                    order:parseInt(element.index()),
                    subcategory_id:ui_button.data('subcategory-id'),
                    subcategory_name:''
                };
                App.data.menu_buttons.add(new_button);
            }
            else {
                menu_button.set({'order': parseInt(element.index())}, {silent: true});
            }
			

			return {
				'order': parseInt(element.index()), 
				'item_id': ui_button.data('item-id'), 
				'category_id': ui_button.data('category-id'), 
				'subcategory_id': ui_button.data('subcategory-id')
			}; 
		});

		$.post(API_URL + '/items/menu_buttons', {buttons: buttons}, function(response){
			
		},'json');

		return false;
	},
	
	select_all_cart_items: function(e){
		var table = App.data.food_bev_table;
		var button = $(e.currentTarget);
		
		if(button.data('selected')){
			table.get('cart').unSelectItems();
			button.data('selected', null);
		
		}else{
			table.get('cart').selectAllItems();
			button.data('selected', 1);
		}
		
		return false;
	},

	show_table_layouts: function(){
		// Close any open modal windows
		$('div.modal').modal('hide');
        $('#page-food-beverage').loadMask({'message':'Loading table layouts...'});
		// Refresh and show table layout window
		App.data.food_bev_table_layouts.fetch({success: function(){
			var layout_window = new TableLayoutWindowView({
				collection: App.data.food_bev_table_layouts
			}, {
				last_layout: App.data.food_bev_table_layouts.last_layout,
				closable: false,
				backdropOpacity: '0.9'
			});
			layout_window.show();
            $('#page-food-beverage').loadMask.hide();
		}});
	},

	toggleCustomers: function(){
		console.log('Toggle customers');
		if(this.customersExpanded){
			// collapse customers
			this.customersExpanded = false;
			this.$el.find('.customer-viewer').removeClass('expanded');
		}else{
			// expand customers
			this.customersExpanded = true;
			this.$el.find('.customer-viewer').addClass('expanded');
		}
	}
});
