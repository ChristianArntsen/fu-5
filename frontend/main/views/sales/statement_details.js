var StatementDetailsView = ModalView.extend({

	id: 'invoice_details',
	template: JST['sales/statement_details_window.html'],

	events: {
		"click button.save": "save",
		"change input.price": "changePrice"
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
	},

    render: function(){
		var attrs = this.model.attributes;
		this.$el.html(this.template(attrs));
		return this;
    },

	save: function(e){
		e.preventDefault();
		var view = this;
		var amount = this.$el.find('input.price').val();
		this.model.set('unit_price', amount).save();
		this.hide();
		return false;
	},

	// Limit the amount paid on statement to whatever is in their invoice account
	changePrice: function(e){
		var receivable = this.model.get('params').customer.invoice_balance;
		var max_amount = 0;
		if(receivable < 0){
			max_amount = Math.abs(receivable);
		}
		var amount = this.$el.find('input.price').val();

		if(amount > max_amount){
			this.$el.find('input.price').val(max_amount);
		}
	}
});
