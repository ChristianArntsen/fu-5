var IssuePunchCardView = ModalView.extend({

	id: 'issue_punch_card_window',
	template: JST['sales/issue_punch_card_window.html'],

	events: {
		"click button.save": "save",
		"onCardswipe": function(){ this.$el.find('div.modal-body').loadMask(); },
		"afterCardswipe": function(){ $.loadMask.hide(); },
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
	},

    render: function(){
		var attrs = this.model.attributes;
		this.$el.html(this.template(attrs));
		
		var view = this;
		var model = this.model;
		
		init_customer_search(this.$el.find('#punch-card-customer'), function(e, customer, list){
			view.$el.find('#punch-card-customer-id').val(customer.person_id);
			view.$el.find('#punch-card-customer').val(customer.first_name +' '+ customer.last_name).data('customer', customer);
			var params = model.get('params');
			params.customer = _.pick(customer, ['first_name', 'last_name', 'person_id']);
			model.set('params', params);
			return false;
		});		
		
		this.$el.find('#punch-card-number').cardSwipe();
				
		this.$el.find('#punch-card-expiration').datetimepicker({
			format: 'MM/DD/YYYY'
		});
		
		setTimeout(function(){
			view.$el.find('#punch-card-number').focus();
		}, 50);
		
		return this;
    },
	
	numberExists: function(number, callback){
		
		$.get(PUNCH_CARDS_URL, {punch_card_number: number}, function(response){
			if(response && response.length > 0){
				callback(parseInt(response[0].punch_card_id));
			}else{
				callback(false);
			}	
		},'json');
		
		return true;
	},
	
	newCard: function(e){
		
		var view = this;
		var form = this.$el.find('form');		
		
		var punch_card_data = {
			"punch_card_number": view.$el.find('input[name="punch_card_number"]').val(),
			"expiration_date": view.$el.find('input[name="expiration_date"]').val(),
			"details": view.$el.find('textarea[name="details"]').val(),
			"expiration_date": view.$el.find('input[name="expiration_date"]').val(),
			"customer": this.model.get('params').customer
		};
		var item_data = {
			"params": punch_card_data
		}
		
		// Check if punch card already exists
		this.numberExists(punch_card_data.punch_card_number, function(taken){		
			if(taken){
				form.bootstrapValidator('updateMessage', 'punch_card_number', 'notEmpty', 'Punch card number is already taken');
				form.bootstrapValidator('updateStatus', 'punch_card_number', 'INVALID');		
				view.$el.loadMask.hide();
			}else{
				form.bootstrapValidator('updateStatus', 'punch_card_number', 'VALID');
				view.model.set(item_data).save();
				view.hide();				
			}
		});			
	},
	
	save: function(e){
		
		e.preventDefault();
		var view = this;

		// Validate that required fields are filled in
		var form = this.$el.find('form');
		form.bootstrapValidator('validate');
		
		if(!form.bootstrapValidator('isValid')){
			return false;
		}
		view.$el.loadMask();
		
		var action = view.$el.find('input[name="action"]:checked').val();
		form.bootstrapValidator('updateStatus', 'punch_card_number', 'VALIDATING');
		
		this.newCard();
		return false;
	},
	
	success: function(raincheck){
		this.hide();
		App.vent.trigger('notification', {'msg': 'Raincheck issued', 'type': 'success'});
	},
	
	error: function(e){
		this.$el.loadMask('hide');
		App.vent.trigger('notification', {'msg': 'Error issuing raincheck', 'type': 'error'});		
	}
});
