var CustomerSignatureView = ModalView.extend({

	tagName: 'div',
	template: JST['sales/customer_signature.html'],
	id: 'customer-signature',

	events: {
		"click button.save": "save",
		"click button.clear": "clear"
	},

	initialize: function(properties){
        this.clearLoader = true;
        ModalView.prototype.initialize.call(this, {extraClass: 'modal-lg'});
        var resizeCanvas = _.bind(this.resizeCanvas, this);
        window.addEventListener("resize", resizeCanvas);
    },

    render: function(){
    	
    	var attr = {};
    	this.$el.html(this.template( this.model.attributes ));
		return this;
	},

	onShow: function(){

    	var canvas = this.$el.find('canvas')[0];
    	this.canvas = canvas;
    	this.signature_pad = new SignaturePad(canvas);
		
		this.resizeCanvas();
	},

	resizeCanvas: _.throttle(function(e){
		
		var width = this.$el.find('div.modal-body').width();
		var height = this.$el.find('div.modal-body').height();
		this.canvas.width = this.canvas.offsetWidth = width;
		this.canvas.height = this.canvas.offsetHeight = height;
		this.signature_pad.clear();
	}, 100),

	clear: function(){
		this.signature_pad.clear();
	},

	save: function(){

		if(this.signature_pad.isEmpty()){
			App.vent.trigger('notification', {msg: 'Signature is required', type: 'error'});
			return false;
		}

		var data = this.signature_pad.toDataURL();
		this.trigger('signature', {data: data});
		this.clearLoader = false;

		this.hide();

		return false;
	},

	onHide: function(){
		if(this.clearLoader){
			$('#payment_member_account').attr('disabled', null).removeClass('disabled');
			$.loadMask.hide();			
		}
	}
});