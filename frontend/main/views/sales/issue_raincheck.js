var IssueRaincheckView = ModalView.extend({

	id: 'issue_raincheck_window',
	template: JST['sales/issue_raincheck_window.html'],

	events: {
		"change .players input": "setPlayers",
		"change select.holes": "setHoles",
		"blur #raincheck-expiration": "setExpiration",
		"change #raincheck-cart-fee": "setCartFee",
		"change #raincheck-green-fee": "setGreenFee",
		"change #raincheck-teesheet": "setTeesheet",
		"click button.save": "save",
		"blur #raincheck-customer": "clearCustomer"
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
		this.listenTo(this.model, "change:teesheet", this.render);
		this.listenTo(this.model, "change:total", this.renderTotals);
		this.listenTo(App.data.rainchecks, "sync", this.success);
		this.listenTo(App.data.rainchecks, "error", this.error);
	},

    render: function(){

		var teesheet_id = null;
		if(this.model.get('teesheet')){
			teesheet_id = this.model.get('teesheet').get('teesheet_id');
		}
		var attrs = this.model.attributes;
		attrs.selected_teesheet = null;
		attrs.selected_green_fee = null;
		attrs.selected_cart_fee = null;

		if(this.model.get('teesheet')){
			attrs.selected_teesheet = this.model.get('teesheet').get('teesheet_id');
		}
		if(this.model.get('green_fee')){
			attrs.selected_green_fee = this.model.get('green_fee').cid;
		}
		if(this.model.get('cart_fee')){
			attrs.selected_cart_fee = this.model.get('cart_fee').cid;
		}

		// Get data for drop down menus
        attrs.teesheet_menu = {};
        var course_id = App.data.course.get('course_id');
        var teesheet_menu = App.data.course.get('teesheets').where({'can_book_teetimes': true, "course_id":course_id});
        _.extend(attrs.teesheet_menu, App.data.course.get('teesheets').getMenuData(teesheet_menu));

		attrs.green_fees_menu = {};
		attrs.cart_fees_menu = {};
		attrs.green_fees_menu[''] = '- Select Green Fee -';
		attrs.cart_fees_menu[''] = '- Select Cart Fee -';

		var green_fees = App.data.course.get('fees').where({teesheet_id: parseInt(teesheet_id), item_type: 'green_fee'});
		var cart_fees = App.data.course.get('fees').where({teesheet_id: parseInt(teesheet_id), item_type: 'cart_fee'});

		_.extend(attrs.green_fees_menu, App.data.course.get('fees').getMenuData(green_fees));
		_.extend(attrs.cart_fees_menu, App.data.course.get('fees').getMenuData(cart_fees));

		this.$el.html(this.template(attrs));
		var view = this;
		var model = this.model;

		init_customer_search(this.$el.find('#raincheck-customer'), function(e, customer, list){
			view.$el.find('#raincheck-customer-id').val(customer.person_id);
			view.$el.find('#raincheck-customer').val(customer.first_name +' '+ customer.last_name).data('customer', customer);
			model.set('customer', new Customer(customer));
			model.set('customer_id', customer.person_id);
            model.set('taxable', customer.taxable);
            model.calculateTotal();
            view.renderTotals();
			return false;
		});

		this.$el.find('#raincheck-expiration').datetimepicker();
		this.renderTotals();

		return this;
    },

    clearCustomer: function(){
    	var value = this.$el.find('#raincheck-customer').val();
    	if(!value || value == ''){
    		this.model.set({
    			'customer': null,
    			'customer_id': null
    		});
    	}
    },

    setTeesheet: function(e){
		var menu = $(e.currentTarget);
		var teesheet_id = menu.val();
		var teesheet = App.data.course.get('teesheets').get(teesheet_id);

		this.model.set({
			'teesheet_id': teesheet_id,
			'teesheet': teesheet,
			'green_fee': new Fee(),
			'cart_fee': new Fee()
		});
	},

    setGreenFee: function(e){
		e.preventDefault();
		var menu = $(e.currentTarget);
		var fee_id = menu.val();

		var data = {};
		if(fee_id != ''){
			data = App.data.course.get('fees').get(fee_id).attributes;
			this.model.set('green_fee', new Fee(data));
		}else{
			this.model.set('green_fee', null);
		}
	},

    setCartFee: function(e){
		e.preventDefault();
		var menu = $(e.currentTarget);
		var fee_id = menu.val();

		var data = {};
		if(fee_id != ''){
			data = App.data.course.get('fees').get(fee_id).attributes;
			this.model.set('cart_fee', new Fee(data));
		}else{
			this.model.set('cart_fee', null);
		}
	},

    setPlayers: function(e){
		e.preventDefault();
		var radio_btn = $(e.currentTarget);
		var players = parseInt(radio_btn.val());
		this.model.set('players', players);
	},

	setHoles: function(e){
		e.preventDefault();
		var menu = $(e.currentTarget);
		var holes = parseInt(menu.val());
		this.model.set('holes_completed', holes);
	},

	setExpiration: function(e){
		e.preventDefault();
		var field = $(e.currentTarget);
		var expiration = field.val();
		this.model.set('expiry_date', expiration);
	},

    renderTotals: function(){
		this.$el.find('#totals-green-fee').html( accounting.formatMoney(this.model.get('green_fee_price')) );
		this.$el.find('#totals-cart-fee').html( accounting.formatMoney(this.model.get('cart_fee_price')) );
		this.$el.find('#totals-players').html( 'x '+ accounting.formatNumber(this.model.get('players')) );
		this.$el.find('#totals-subtotal').html( accounting.formatMoney(this.model.get('subtotal')) );
		this.$el.find('#totals-tax').html( accounting.formatMoney(this.model.get('tax')) );
		this.$el.find('#totals-total').html( accounting.formatMoney(this.model.get('total')) );
	},

	save: function(e){
		e.preventDefault();
		var raincheck = this.model;
		raincheck.set('teesheet_id', parseInt(raincheck.get('teesheet').get('teesheet_id')));
		this.$el.loadMask();
		// If rainchecks should be split, create individual rainchecks for each player
		if(this.$el.find('#raincheck-split').prop('checked') == true){

			var players = parseInt(raincheck.get('players'));
			raincheck.set('players', 1);

			for(var x = 1; x <= players; x++){
				App.data.rainchecks.create(raincheck.toJSON(), {merge: true, wait: true, success: function(new_raincheck){
					new_raincheck.printReceipt();
                    if (x > players) {
                        App.data.cart.cancel();
                    }
                }});
			}

		}else{
			App.data.rainchecks.create(raincheck.toJSON(), {merge: true, wait: true, success: function(new_raincheck){
				new_raincheck.printReceipt();
                App.data.cart.cancel();
            }});
		}

    },

	success: function(raincheck){
		this.hide();
		App.vent.trigger('notification', {'msg': 'Raincheck issued', 'type': 'success'});
	},

	error: function(e){
		this.$el.loadMask('hide');
		App.vent.trigger('notification', {'msg': 'Error issuing raincheck', 'type': 'error'});
	}
});
