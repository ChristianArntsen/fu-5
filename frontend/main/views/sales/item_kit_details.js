var SaleItemKitDetailsView = ModalView.extend({

	id: 'item_kit_details',
	template: JST['sales/item_kit_details_window.html'],

	events: {
		"click button.save": "save",
		"change input.item-price": "changePrice"
	},

	initialize: function(){
		this.modelCopy = new CartItem(this.model.toJSON());
		this.listenTo(this.modelCopy, 'change:total change:subtotal', this.render);
		ModalView.prototype.initialize.call(this);
	},

    render: function(){
		var attrs = this.modelCopy.attributes;
		this.$el.html(this.template(attrs));
		return this;
    },

    changePrice: function(e){
    	var field = $(e.currentTarget);
    	var index = field.data('index');
    	var item = this.modelCopy.get('items').at(index);

    	try {
    		var price = new Decimal(field.val());	
    	}catch(e){
    		field.val( accounting.formatMoney(item.get('unit_price'), '') );
    		return false;
    	}
    	
    	item.set('unit_price', field.val());
    	this.modelCopy.calculatePrice();

    	return false;
    },

	save: function(e){
		
		var view = this;
		var items = this.modelCopy.get('items').toJSON();
		var prices = [];

		_.each(items, function(item){
			prices.push(item.unit_price);
		});

		var params = this.model.get('params');
		if(!params){
			params = {};
		}
		params.sub_item_overrides = prices;
		this.model.set('params', params);
		this.model.get('items').reset( this.modelCopy.get('items').toJSON() );
		
		this.model.save();
		this.hide();

		return false;
	},
	
	success: function(raincheck){
		this.hide();
		App.vent.trigger('notification', {'msg': 'Raincheck issued', 'type': 'success'});
	},
	
	error: function(e){
		this.$el.loadMask('hide');
		App.vent.trigger('notification', {'msg': 'Error issuing raincheck', 'type': 'error'});		
	}
});
