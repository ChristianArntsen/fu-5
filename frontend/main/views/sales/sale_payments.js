var SalePaymentsView = Backbone.View.extend({

	tagName: 'div',
	className: 'inner-content',
	template: JST['sales/sale_payments.html'],
	
	initialize: function(){
		this.listenTo(App.data.cart, 'change:suspended_name change:total change:total_due change:mode', this.render);
		this.listenTo(App.data.cart.get('payments'), 'add remove reset', this.renderPayments);
		this.listenTo(App.data.cart.get('payments'), 'add', this.credit_card_saved);
		this.listenTo(App.data.cart.get('sale_payments'), 'add remove reset', this.renderSalePayments);
		this.listenTo(App.data.register_logs, 'change:shift_end add', this.render);
		this.listenTo(App.data.course.get('terminals'), 'change', _.debounce(this.render, 100));
	},

	events: {
		'click button.pay': 'pay',
        'click button#quick_payment_credit_card': 'showCreditCard',
		'click #suspend-sale': 'suspend',
		'click #save-suspended-sale': 'save_suspended',
		'click #close-register': 'close_register',
		'click #open-register': 'open_register',
		'click #open-cash-drawer': 'open_cash_drawer',
		'click #new-register': 'new_multi_register',
		'click #add-credit-card': 'add_credit_card'
	},

	render: function(){
        //console.log('render')
		var attr = App.data.cart.attributes;
		attr.member_account_label = App.data.course.get('member_balance_nickname');
		attr.customer_account_label = App.data.course.get('customer_credit_nickname');
		attr.use_register_log = App.data.course.get_setting('use_register_log');
		attr.custom_payments = App.data.course.get('custom_payments');

		var terminal = App.data.course.get('terminals').getActiveTerminal();
		attr.multiCashDrawers = 0;
		
		if(terminal){
			attr.multiCashDrawers = terminal.get('multiCashDrawers');
		}
		attr.active_register_log = App.data.register_logs.get_open_log();

		this.$el.html(this.template(attr));
		this.renderPayments();
		this.renderSalePayments();
		return this;
	},

	renderPayments: function(){
		//console.log('render1')
		var payment_list = this.$el.find('#payments');
		payment_list.html('');

		if(App.data.cart.get('payments').length > 0){
			_.each(App.data.cart.get('payments').models, function(payment){
				var payment_view = new CartPaymentView({model: payment});
				payment_list.append( payment_view.render().el );
			});
		}

		return this;
	},

	renderSalePayments: function(){
		//console.log('render2')
		var payment_list = this.$el.find('#sale-payments').find('ul');		
		payment_list.html('');

		if(App.data.cart.get('sale_payments').length > 0){
			this.$el.find('#sale-payments').show();
			_.each(App.data.cart.get('sale_payments').models, function(payment){
				if(payment.get('amount')==0)return;
				var payment_view = new CartSalePaymentView({model: payment});
				payment_list.append( payment_view.render().el );
			});

		}else{
			this.$el.find('#sale-payments').hide();
		}

		return this;
	},	

	credit_card_saved: function(data){
		if(!data.get('type') || data.get('type') != 'credit_card_save'){
			return false;
		}
		App.vent.trigger('notification', {msg: 'Credit card saved', type: 'success'});
		$('#modal-payment_window').modal('hide');
		return false;
	},

    add_credit_card: function(e){
        var paymentWindow = new SalePaymentWindow({
        	collection: App.data.cart.get('payments')},
        	{
        		extraClass: 'modal-lg'
        	}
        );

        paymentWindow.save_card = true;
        paymentWindow.show();
        paymentWindow.showCreditCard(e);

        return false;
    },

	// Display payment window
	pay: function(){
		
		if(
			App.data.course.get_setting('require_customer_on_sale') == 1 &&
			App.data.cart.get('customers').length == 0
		){
			App.vent.trigger('notification', {msg: 'Customer is required to complete sale', type: 'error'});
			return false;
		}

		var window = new SalePaymentWindow({collection: App.data.cart.get('payments')}, {extraClass: 'modal-lg'});
		window.show();
	},
	
	// Show suspend sale window
	suspend: function(){
		var suspendSale = new SuspendSelectionView({model: new Backbone.Model()});
		suspendSale.show();
		return false;
	},
	
	// Save the current sale (that has already been suspended)
	save_suspended: function(){
		App.data.cart.suspend();	
		return false;
	},

    showCreditCard: function(e){
        
		if(
			App.data.course.get_setting('require_customer_on_sale') == 1 &&
			App.data.cart.get('customers').length == 0
		){
			App.vent.trigger('notification', {msg: 'Customer is required to complete sale', type: 'error'});
			return false;
		}

        var window = new SalePaymentWindow({collection: App.data.cart.get('payments')}, {extraClass: 'modal-lg'});
        window.show();
        window.showCreditCard(e);
    },

    // Sends 'open' command to physically open cash drawer
	open_cash_drawer: function(){

		var view = this;
		var receipt_printer = new Receipt();

		if(App.data.course.get_setting('multi_cash_drawers') == 1){
			receipt_printer.open_cash_drawer(App.data.user.get('person_id'));
		}else{
			receipt_printer.open_cash_drawer();
		}
	},

	close_register: function(){
		
		var log = App.data.register_logs.get_open_log();
		
		if(App.data.course.get_setting('multi_cash_drawers') == 1 &&
			log.get('employee_id') != App.data.user.get('person_id')
		){
			var employee = App.data.course.get('employees').get( log.get('employee_id') );
			App.vent.trigger('notification', {
				'msg': 'Register log can only be closed by ' + employee.get('first_name') +' '+employee.get('last_name'),
				'type': 'error'
			});
			return false;
		}

		var register_log_window = new RegisterLogView({
			model: log,
			template: 'close',
			collection: App.data.register_logs
		});
		register_log_window.show();
		
		return false;
	},
	
	open_register: function(){
		App.data.register_logs.prompt_to_open_log(true);
		return false;
	}
});

var SalePaymentWindow = ModalView.extend({

	id: 'payment_window',
	template: JST['sales/payment_window.html'],
	numberTemplate: JST['sales/payment_window_number.html'],
	creditCardTemplate: JST['sales/payment_window_credit_card_swipe.html'],
	billingAddressTemplate: JST['sales/payment_window_billing_address.html'],

	events: {
		'click button.pay': 'initPay',
		'click #payment_cash': 'payCash',
		'click #payment_check': 'showCheck',
		'click #payment_credit_card': 'showCreditCard',
		'click #payment_customer_account': 'payCustomerAccount',
		'click #payment_loyalty': 'payLoyalty',
		'click #payment_member_account': 'payMemberAccount',
		'click #payment_raincheck': 'showRaincheck',
		'click #payment_giftcard': 'showGiftcard',
		'click #payment_coupon': 'showCoupon',
		'click #payment_punch_card': 'showPunchCard',
        'click button.custom_payment': 'payCustomPayment',
		'click button.back': 'showPayments',
		'click input.amount': 'clearAmount',
		'click button.save-billing-address': 'saveBillingAddress',
		'click button.charge-stored-card': 'charge_stored_card',
		'blur input.amount': 'replaceAmount',
		'keypress #payment-number': 'enterSubmit',
        'click #cancel-credit-card-payment': 'cancelCreditCardPayment',
        'keypress #payment_amount': 'defaultPayCash',
        'blur #customer-note': 'set_customer_note'
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this, {extraClass: 'modal-lg', closable: false});
		this.listenTo(App.data.cart, 'change:total_due', this.render);
		this.listenTo(App.data.cart.get('payments'), 'add remove reset change', this.renderPayments);
		this.listenTo(App.data.cart.get('payments'), 'error', this.handlePaymentError);
		this.listenTo(App.vent, 'credit-card gift-card', this.processCreditCard);
		this.listenTo(App.data.cart, 'error', this.saleError);
		
		this.address_entered = false;
		this.billing_address = false;
		this.billing_zip = false;
		this.paying = false;
    },

    render: function(){

		var attr = App.data.cart.attributes;
		attr.selected_customer = App.data.cart.get('customers').findWhere({'selected':true});
        attr.refund_reasons = App.data.course.get('refund_reasons');

		var html = this.template(attr);
		this.$el.html(html);
		this.renderPayments();

		if(this.paying && (
			(App.data.cart.get('total_due') <= 0 && App.data.cart.get("mode") != "return") ||
			(App.data.cart.get('total_due') >= 0  && App.data.cart.get("mode") == "return")
			)&& App.data.cart.get('items').length > 0){
			$('div.payments-window').loadMask({'message': 'Completing sale...'});
		}

		this.$el.find('#payment_amount').select();
		return this;
    },

    defaultPayCash: function(e){
    	if(e.which == 13){
    		this.$('#payment_cash').trigger('click');
    		return false;
    	}
    },

	onShow: function(){
		this.$el.find('#payment_amount').select();
		window.onbeforeunload = function() {
			return "Close the modal before leaving this page or your payment may be lost.";
		};
	},
	onHide: function(e){
		window.onbeforeunload = null;
	},
    initPay: function(e){
		this.paying = true;
		this.$el.loadMask({'message':'Adding payment...'});
    	$(e.currentTarget).addClass('disabled').prop('disabled', true);
    },

    set_customer_note: function(){
		var customer_note = this.$el.find('#customer-note').val();
		if(customer_note == ''){
			customer_note = null;
		}
		App.data.cart.set('customer_note', customer_note, {'silent': true});
    },

	clearLoader: function(){
		this.paying = false;
		$.loadMask.hide();
		this.$el.find('button.pay').removeClass('disabled').prop('disabled', null);
	},

    handlePaymentError: function(model, response){
		this.clearLoader();
		if(response.status == 409){
			return false;
		}
		if(this.save_card && this.save_card === true){
			this.hide();
		}else{
			this.render();
		}
	},
    
    saleError: function(model, response){
		this.clearLoader();
		App.errorHandler(model, response);
		this.render();
	},
    
    enterSubmit: function(e){
		if(e.which == 13){
			this.$el.find('button.submit-payment').trigger('click');
			return false;
		}
		return true;
	},
    
    clearAmount: function(e){
		var field = $(e.currentTarget);
		var previousVal = field.val();
		field.data('previous-value', previousVal);
		field.val('');
		return true;
	},
	
	replaceAmount: function(e){
		var field = $(e.currentTarget);

		if(field.val() == '' || typeof field.val() === 'undefined' || field.val() == null){
			var previousVal = field.data('previous-value');
			field.val(previousVal);
		}
		return true;		
	},
    
    showPayments: function(e){
		this.render();
		return false;
	},
	
	getPaymentAmount: function(){

        var new_value = App.data.cart.get('total_due');
        var field = this.$el.find('#payment_amount');

        if (field.val() != undefined) {
            new_value = field.val();
        }
		var total_due = App.data.cart.get('total_due');

		if(new_value === false || new_value === '' || isNaN(new_value) || new_value === null || typeof new_value === 'undefined'){
			field.val(accounting.formatNumber(total_due, 2, ''));
			App.vent.trigger('notification', {'msg': 'Valid numbers only (no commas or symbols)', 'type':'error'});
			return false;
		}
		
		return _.round(new_value, 2);		
	},

	showCreditCardSwipeForm: function(){
		
		var amount = this.$el.find('#payment_amount').val();
		var attrs = {
			amount: amount,
			show_manual_entry: false
		};
		var view = this;
		this.paying = true;

		if(App.data.cart.get('mode') == 'sale'){
			attrs.show_manual_entry = true;
		}
		
		this.$el.find('div.modal-body').html(this.creditCardTemplate(attrs));	
		var events = _.clone(this.events);	
		
		// Temporarily bind the "submit" button to send the payment
		events['click button.submit-payment'] = 'payCreditCard';
		events['click button.manual-entry'] = 'payManualCreditCard';
		
		this.delegateEvents(events);
		this.$el.find('#credit-card-data').focus();
		
		this.$el.find('#credit-card-data').on('keypress', function(e){
			if(e.which == 13){
				view.payCreditCard(e);
				view.$el.loadMask();
			}
		});
	
		return false;
	},
	
	renderPayments: function(payment){
		
		var saved_credit_cards = App.data.cart.get('payments').where({type: 'credit_card_save'});
		var saved_credit_card_container = this.$el.find('#saved-credit-cards');
		var payment_list = this.$el.find('#window-payments');

		payment_list.html('');
		saved_credit_card_container.html('');

		if(App.data.cart.get('payments').length > 0){
			_.each(App.data.cart.get('payments').models, function(payment){
				var payment_view = new CartPaymentView({model: payment})
				payment_list.append(payment_view.render().el );
			});
		}

		if(saved_credit_cards.length > 0){
			_.each(saved_credit_cards, function(payment){
				saved_credit_card_container.append('<button class="btn btn-success btn-lg col-lg-12 charge-stored-card pay" \
					data-payment-id="'+payment.get('payment_id')+'">Charge '+payment.get('description')+'</button>');
			});
		}

		return this;
	},    

	processCreditCard: _.throttle(function(card_data){
		
		var view = this;
		App.data.cart.get('payments').addPayment(card_data, function(payment){
			if((card_data.type == 'credit_card' || card_data.type == 'credit_card_partial_refund') && App.data.course.get_setting('signature_slip_count') > 0){
				var credit_card_receipt = new CreditCardReceipt(payment.attributes);
				credit_card_receipt.print();
			}
		});

	}, 1000, {'leading': true, 'trailing': false}),

    cancelCreditCardPayment: function() {

        var view = this;
        // Close window
        view.hide();

        var data = {};
        // Cancel Payment
        $.post(SITE_URL + '/v2/home/cancel_credit_card_payment', data, function(response){
        },'json');
    },

	payCash: function(e){
		e.preventDefault();
		
		var amount = this.getPaymentAmount();
		if(amount === false) {
			return false;
		}
		
		var data = {};
		data.amount = amount;
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.type = 'cash';
		data.description = 'Cash';
		data.record_id = 0;

		this.collection.addPayment(data);
		return false;
	},

    payCustomPayment: function(e){
        e.preventDefault();

        var amount = this.getPaymentAmount();
        if(amount === false){
            return false;
        }

        var data = {};
        data.amount = amount;
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.type = $(e.target).attr('id');
        data.description = $(e.target).html();
        data.record_id = 0;

        this.collection.addPayment(data);
        return false;
    },

	showCheck: function(e){
		e.preventDefault();	
		this.$el.find('#payment-types').html(this.numberTemplate());
		this.$el.find('#payment-number').cardSwipe();	
		
		var events = _.clone(this.events);	
		
		// Temporarily bind the "submit" button to send the payment
		events['click button.submit-payment'] = 'payCheck';
		this.delegateEvents(events);		
		this.$el.find('#payment-number').focus();
		
		return false;
	},

	payCheck: function(e){
		e.preventDefault();

		var amount = this.getPaymentAmount();
		if(amount === false){
			return false;
		}

		var data = {};
		data.amount = amount;
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.number = this.$el.find('input.number').val();
		data.type = 'check';
		data.details = 'Check';
		data.record_id = 0;

		this.collection.addPayment(data);
		return false;
	},
	
	showGiftcard: function(e){
		e.preventDefault();	
		var view = this;

        if(e.shiftKey){
            this.payGiftcard(e);
            return false;
        }

		// If course uses ETS giftcards
		if(App.data.course.get('use_ets_giftcards') == 1){
			
			var data = {};
			data.amount = _.round(this.$el.find('#payment_amount').val());
			data.type = 'gift_card';
			data.description = 'Gift Card';
			data.record_id = 0;			
			data.action = 'sale';
			
			data.amount = Math.abs(data.amount);
			if(App.data.cart.get('mode') == 'return'){
				data.action = 'refund';
			}
			
			// Load merchant card form
			view.$el.loadMask();
			$.post(SITE_URL + '/v2/home/credit_card_window', data, function(response){
				view.$el.find('div.modal-body').html(response);
				view.$el.find('div.modal-footer').hide();
				view.$el.find('h4.modal-title').text('Gift Card Payment');
			},'html');			

			return false;
		}		
		
		this.$el.find('#payment-types').html(this.numberTemplate());	
		this.$el.find('#payment-number').cardSwipe({
            parseData:!SETTINGS.nonfiltered_giftcard_barcodes
        });
		
		var events = _.clone(this.events);	
		
		// Temporarily bind the "submit" button to send the payment
		events['click button.submit-payment'] = 'payGiftcard';
		this.delegateEvents(events);
		this.$el.find('#payment-number').focus();		
		
		return false;
	},
	
	payGiftcard: function(e){
		
		var view = this;
		var data = {};
		
		var amount = this.getPaymentAmount();
		if(amount === false){
			return false;
		}		
		
		data.amount = amount;
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.number = this.$el.find('input.number').val();
		data.type = 'gift_card';
		data.description = 'Gift Card';
		data.record_id = 0;
        if (e.shiftKey) {
            data.bypass = 1;
        }
		
		this.collection.addPayment(data, function(){
			// Reset view events back to normal
			view.delegateEvents();
			view.render();			
		});		
				
		return false;		
	},
	
	showPunchCard: function(e){
		e.preventDefault();	
		this.$el.find('#payment-types').html(this.numberTemplate());	
		this.$el.find('#payment-number').cardSwipe();	
		
		var events = _.clone(this.events);	
		
		// Temporarily bind the "submit" button to send the payment
		events['click button.submit-payment'] = 'payPunchCard';
		this.delegateEvents(events);
		this.$el.find('#payment-number').focus();		
		
		return false;
	},
	
	payPunchCard: function(e){
		
		var view = this;
		var data = {};
		
		var amount = this.getPaymentAmount();
		if(amount === false){
			return false;
		}
		
		data.amount = amount;
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.number = this.$el.find('input.number').val();
		data.type = 'punch_card';
		data.description = 'Punch Card';
		data.record_id = 0;
		
		this.collection.addPayment(data, function(){
			// Reset view events back to normal
			view.delegateEvents();
			view.render();			
		});
				
		return false;		
	},
	
	showCoupon: function(e){
		e.preventDefault();	
		this.$el.find('#payment-types').html(this.numberTemplate());	
		this.$el.find('#payment-number').cardSwipe();
		
		var events = _.clone(this.events);	
		
		// Temporarily bind the "submit" button to send the payment
		events['click button.submit-payment'] = 'payCoupon';
		this.delegateEvents(events);
		this.$el.find('#payment-number').focus();		
		
		return false;
	},
	
	payCoupon: function(e){
		
		var view = this;
		var data = {};
		
		var amount = this.getPaymentAmount();
		if(amount === false){
			return false;
		}
		
		data.amount = amount;
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.number = this.$el.find('input.number').val();
		data.type = 'coupon';
		data.description = 'Coupon';
		data.record_id = 0;
		
		this.collection.addPayment(data, function(){
			// Reset view events back to normal
			view.delegateEvents();
			view.render();			
		});
				
		return false;		
	},					
	
	showRaincheck: function(e){
		e.preventDefault();	
		this.$el.find('#payment-types').html(this.numberTemplate());	
		
		var events = _.clone(this.events);	
		
		// Temporarily bind the "submit" button to send the payment
		events['click button.submit-payment'] = 'payRaincheck';
		this.delegateEvents(events);
		this.$el.find('#payment-number').focus();		
		
		return false;
	},	
	
	payRaincheck: function(e){
		
		var view = this;
		var data = {};
		
		var amount = this.getPaymentAmount();
		if(amount === false){
			return false;
		}
				
		data.amount = amount;
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.number = this.$el.find('input.number').val();
		data.type = 'raincheck';
		data.description = 'Raincheck';
		data.record_id = 0;
		
		this.collection.addPayment(data, function(){
			// Reset view events back to normal
			view.delegateEvents();
			view.render();			
		});
				
		return false;		
	},

	payCustomerAccount: function(e){
		e.preventDefault();

		var amount = this.getPaymentAmount();
		if(amount === false){
			return false;
		}

		var data = {};
		data.amount = amount;
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.type = 'customer_account';
		data.description = 'Customer Account';
		data.record_id = 0;

		var selected_customer = App.data.cart.get('customers').findWhere({'selected':true});
		if(selected_customer){
			data.record_id = parseInt(selected_customer.get('person_id'));
			this.collection.addPayment(data);
		}

		return false;
	},

	payMemberAccount: function(e){
		e.preventDefault();

		var amount = this.getPaymentAmount();
		if(amount === false){
			return false;
		}

		var data = {};
		data.amount = amount;
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.type = 'member_account';
		data.description = 'Member Account';
		data.record_id = 0;

		var selected_customer = App.data.cart.get('customers').findWhere({'selected':true});
		if(selected_customer){
			data.record_id = parseInt(selected_customer.get('person_id'));
			this.collection.addPayment(data);
		}

		return false;
	},
	
	payLoyalty: function(e){
		e.preventDefault();

		var amount = this.getPaymentAmount();
		if(amount === false){
			return false;
		}

		var data = {};
		data.amount = amount;
        data.refund_comment = this.$el.find('#refund-comment').val();
        data.refund_reason = this.$el.find('#refund-reason option:selected').text();
        data.type = 'loyalty_points';
		data.description = 'Loyalty Points';
		data.record_id = 0;

		var selected_customer = App.data.cart.get('customers').findWhere({'selected':true});
		if(selected_customer){
			data.record_id = parseInt(selected_customer.get('person_id'));
			this.collection.addPayment(data);
		}

		return false;
	},	
	
	payManualCreditCard: function(e){
		this.manual_entry = true;
		return this.showCreditCard(e);
	},
	
	saveBillingAddress: function(e){
		
		this.address_entered = true;
		this.billing_address = this.$el.find('#credit_card_address').val();
		this.billing_zip = this.$el.find('#credit_card_zipcode').val();
		this.showCreditCard(e);
		
		return false;		
	},
	
	showBillingAddressForm: function(e){
		var amount = this.getPaymentAmount()
		var attr = {'amount': amount};
		
		this.$el.find('div.modal-body').html( this.billingAddressTemplate(attr) );
		this.$el.find('#credit_card_address').focus();
		return false;
	},
	
	showCreditCard: _.throttle(function(e){
		
		var amount = this.getPaymentAmount();
		if(amount === false){
			return false;
		}		
		
		if(!App.data.course.get('merchant') || e.shiftKey){
			this.payCreditCard(e);
			return false;
		}
		
		if(App.data.course.get('merchant') == 'element' && !this.manual_entry){
			this.showCreditCardSwipeForm();
			return false;
		}
		
		if(App.data.course.get('merchant') == 'element' && this.manual_entry && !this.address_entered){
			this.showBillingAddressForm(e);
			return false;
		}
		var view = this;
		var data = {};		
		
		data.amount = Math.abs(amount);
		data.type = 'credit_card';
		data.cart_id = App.data.cart.get('cart_id');

		if(this.save_card && this.save_card === true){
			data.type = 'credit_card_save';
			data.amount = 0;
		}else if(App.data.cart.get('mode') == 'return'){
			data.action = 'refund';
			if(amount > 0){
				data.action = 'sale';
			}
		}else{
			data.action = 'sale';
            if(amount < 0){
                data.action = 'refund';
            }
		}	

		if(this.address_entered){
			data.address = this.billing_address;
			data.zip = this.billing_zip;
		}	
		
		// Load merchant card iframe
		view.$el.loadMask();
		this.paying = true;
        $.post(SITE_URL + '/v2/home/credit_card_window', data, function (response) {
            view.$el.find('div.modal-body').html(response);
            view.$el.find('div.modal-header button').hide();
            view.$el.find('div.modal-footer button').hide();
			if (view.$el.find('#mercury-emv-iframe').length == 0 && App.data.course.get('merchant') != 'apriva') {
				view.$el.find('div.modal-footer #cancel-credit-card-payment').show();
			}
            view.$el.find('h4.modal-title').text('Credit Card Payment');
            $('#modal-payment_window').data('bs.modal').options.keyboard = false;
            $('#modal-payment_window').data('bs.modal').options.backdrop = 'static';
            $.loadMask.hide();
        }, 'html');
	},5000),
	
	payCreditCard: function(e, merchant_data){
		
		var payment = {};
		
		var amount = this.getPaymentAmount();
		if(amount === false){
			return false;
		}		
		
		payment.amount = amount;
        payment.refund_comment = this.$el.find('#refund-comment').val();
        payment.refund_reason = this.$el.find('#refund-reason option:selected').text();
        payment.type = 'credit_card';
		payment.record_id = 0;

		if(this.save_card && this.save_card === true){
			payment.type = 'credit_card_save';
			payment.amount = 0;
		}

		payment.merchant_data = {};
		if(merchant_data != undefined){
			payment.merchant_data = merchant_data;
		}

		if(App.data.course.get('merchant') == 'element' && !e.shiftKey){
			payment.merchant_data.encrypted_track = this.$el.find('#credit-card-data').val();
		}

		this.collection.addPayment(payment, function(payment){
			if(
				payment.get('type') == 'credit_card' &&
				payment.has('merchant_data') &&
				payment.get('merchant_data') &&
				App.data.course.get_setting('signature_slip_count') > 0
			){
				var credit_card_receipt = new CreditCardReceipt(payment.attributes);
				credit_card_receipt.print();
			}			
		});
		return false;
	},

	charge_stored_card: function(e){

		var payment_id =$(e.currentTarget).data('payment-id');
		var payment = App.data.cart.get('payments').get(payment_id);
		var params = payment.get('params');
		payment.set('verified', false);

		this.payCreditCard(e, {credit_card_id: params.credit_card_id});

		return false;
	}
});

var CartPaymentView = Backbone.View.extend({

	tagName: 'li',
	className: 'payment',
	template: JST['sales/payment.html'],

	initialize: function(){
		this.listenTo(this.model, 'change', this.render);
	},

	events: {
		'click a.delete': 'deletePayment'
	},

	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	},
	
	showLoading: function(){
		this.$el.find('a.delete').hide();
		this.$el.prepend("<i class='fa fa-spinner fa-spin'></i>");
	},
	
	hideLoading: function(){
		this.$el.find('a.delete').show();
		this.$el.find('i.fa').remove();
	},	

	deletePayment: function(e){
		
		var view = this;
		
		// If course is using ETS gift cards, ETS does not support
		// refunding a gift card payment without re-scanning the card
		if(this.model.get('type') == 'ets_gift_card'){
			var refund_window = new EtsGiftCardRefundWindow({model: this.model});
			refund_window.show();
		
		}else{
			this.showLoading();
			this.model.destroy({wait: true, error: function(){
				view.hideLoading();
			}});
		}
		
		return false;
	}
});

var CartSalePaymentView = Backbone.View.extend({

	tagName: 'li',
	className: 'payment',
	template: JST['sales/sale_payment.html'],

	events: {
		'click a.refund': 'refund'
	},

	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	},

	refund: function(e){
		// Update the old payment with amount refunded
		if(!this.model.attributes.params){
			this.model.attributes.params = {
				"cardholder_name":"",
				"card_type":"",
				"masked_account":"",
				"amount_refunded":0
			};
		}
		var view = new RefundSalePaymentView({model: this.model});
		view.show();
		return false;
	}
});

var ChangeDueWindow = ModalView.extend({

	id: 'change_due',
	template: JST['sales/change_due_window.html'],

	events: {
		'click button.close-window': 'hide',
	},

	initialize: function(options){
		ModalView.prototype.initialize.call(this);
		
		this.food_bev_table = false;
		if(options && options.food_bev_table){
			this.food_bev_table = options.food_bev_table;
		}
	},

    render: function() {
		var html = this.template(this.model.attributes);
		this.$el.html(html);
		
		return this;
    },
    
    onHide: function(){
		if(this.food_bev_table){
			$('#modal-payment_window').modal('hide');
			this.food_bev_table.close(function(){App.page.currentView.show_table_layouts();});
		}else{
			if(App.data.course.get_setting('after_sale_load') == 0){
				App.data.cart.redirect();
			}			
		}
	}
});
