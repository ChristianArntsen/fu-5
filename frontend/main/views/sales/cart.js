var CartItemView = Backbone.Marionette.ItemView.extend({

	tagName: 'tr',
	className: '',
	template: JST['sales/cart_item.html'],

	ui: {
		discountField:"input.item-discount"
	},
	events: {
		'click a.delete-item': 'deleteItem',
		'click input.select-item': 'toggleSelect',
		'blur input.item-price': 'changePrice',
		'blur input.item-quantity': 'changeQuantity',
		'blur @ui.discountField': 'changeDiscount',
		'change select.fees': 'changeFee',
		'change select.seasons': 'changeSeason',
		'click a.edit-details': 'showDetails',
		'click input.auto-highlight': 'highlightField',
		'keypress input.auto-highlight': 'enterBlur'
	},

	initialize: function(){
		this.listenTo(this.model, 'show-details', this.showDetails);
		this.listenTo(this.model, 'change:subtotal change:selected change:quantity change:discount_percent change:sides_subtotal change:params', this.render);
		this.listenTo(App.data.user.get('acl'), 'change', this.render);
	},

	render: function(){

		var attr = _.clone(this.model.attributes);
		var model = this.model;
		attr.fee_dropdown = {};
		if(attr.params && attr.params.customer_id){
			if(App.data.cart.get('customers').get(attr.params.customer_id)){
				var customer = App.data.cart.get('customers').get(attr.params.customer_id);
				attr.customer_initials = customer.get('first_name')[0] + customer.get('last_name')[0];
				attr.customer_color = customer.get_color();
			}
		}
		if(model.get('item_type') == 'cart_fee' || model.get('item_type') == 'green_fee'){
			
			var fee_params = {
				item_type: model.get('item_type')
			};
			attr.selected_season = false;

			var fees = [];
			// If course is set to lock down the fee dropdown to only fees accessible to the customer
			if(App.data.course.get_setting('limit_fee_dropdown_by_customer') == 1 && 
				attr.params && 
				_.has(attr.params, 'price_class_ids')
			){
				var include_specials = false;
				if(model.get('special_id') && model.get('special_id') != 0){
					include_specials = true;
				}

				// Filter fees by item type, price class and only show specials 
				// if the current item in the cart IS a special
				fees = App.data.course.get('fees').filterFees({
					item_type: model.get('item_type'),
					price_class_ids: attr.params.price_class_ids,
					include_specials: include_specials
				});

			}else{
				// Filter fees by item type (green fee or cart fee)
				fees = App.data.course.get('fees').filterFees(fee_params);
			}
			var season_ids = App.data.course.get('fees').get_valid_season_ids(fees);

			// Get the selected season ID
			if(attr.params && _.has(attr.params, 'season_id')){
				fee_params.season_id = attr.params.season_id;
				attr.selected_season = attr.params.season_id;
			}
			
			fees = new FeeCollection(fees);
			var fees_by_season = fees.filterFees(fee_params);

			// Generate dropdown menus from filtered fees
			attr.fee_dropdown = App.data.course.get('fees').getMenuData(fees_by_season);
			attr.season_dropdown = App.data.seasons.getDropdownData({'season_ids': season_ids});

			var params = {
				item_id: model.get('item_id')
			}
			if(model.get('timeframe_id') && model.get('timeframe_id') != 0){
				params.timeframe_id = parseInt(model.get('timeframe_id'));
			}else{
				params.special_id = parseInt(model.get('special_id'));
			}
			
			var selected_fee = App.data.course.get('fees').findWhere(params);
			if(selected_fee){
				attr.selected_fee = selected_fee.cid;
			}else{
				attr.selected_fee = false;
			}

			attr.customer_initials = false;
			attr.color = false;
			if(attr.params && attr.params.customer_id){
				if(App.data.cart.get('customers').get(attr.params.customer_id)){
					var customer = App.data.cart.get('customers').get(attr.params.customer_id);
					attr.customer_initials = customer.get('first_name')[0] + customer.get('last_name')[0];
					attr.customer_color = customer.get_color();
				}
			}
			
			attr.item_id = String(attr.item_id);
		}
		this.$el.html(this.template(attr));

		// if items loaded first, individual discounts are not refunded properly
		// if customers are loaded first, group discounts are not refunded properly
		// this triggers calculation after both are loaded
		this.changeDiscount(undefined,true);

		return this;
	},

	enterBlur: function(e){
		if(e && e.which == 13 && e.currentTarget){
			$(e.currentTarget).trigger('blur');
			return false;
		}
		return true;
	},

	highlightField: function(e){
		$(e.currentTarget).select();
	},
	
	validate_number: function(new_value, old_value, e){

		if(!new_value || new_value == '' || isNaN(new_value) || new_value == null){
			if(e){
                $(e.currentTarget).val(accounting.formatNumber(old_value, 2, ''));
            }
			App.vent.trigger('notification', {'msg': 'Valid numbers only (no commas or symbols)', 'type':'error'});
			return false;
		}
		return true;		
	},

	deleteItem: function(){
		this.model.destroy();
	},

	showDetails: function(e){
		
		if(this.model.has('item_type') && this.model.get('item_type') == 'giftcard'){
			this.giftcardDetails(e);
		}else if(this.model.has('item_type') && this.model.get('item_type') == 'punch_card'){
			this.punchCardDetails(e);
		}else if(this.model.has('item_type') && this.model.get('item_type') == 'invoice'){
			this.invoiceDetails(e);
		}else if(this.model.has('item_type') && this.model.get('item_type') == 'statement'){
			this.statementDetails(e);
		}else if(this.model.has('item_type') && this.model.get('item_type') == 'pass'){
			this.passDetails(e);
		}else if(this.model.get('food_and_beverage') == 1){
			this.foodBeverageDetails(e);
		}else if(this.model.has('item_type') && this.model.get('item_type') == 'item_kit'){
			this.itemKitDetails(e);
		}
		
		return false;
	},

	giftcardDetails: function(){
		var giftcardView = new IssueGiftcardView({model: this.model});
		giftcardView.show();
		return false;
	},

	punchCardDetails: function(){
		var punchCardView = new IssuePunchCardView({model: this.model});
		punchCardView.show();
		return false;
	},
	
	itemKitDetails: function(e){
		// Do not automatically display item kit details
		if(!e){
			return false;
		}		
		var itemKitDetails = new SaleItemKitDetailsView({model: this.model});
		itemKitDetails.show();
		return false;
	},

	passDetails: function(){
		var passView = new IssuePassView({model: this.model});
		passView.show();
		return false;
	},

	invoiceDetails: function(){
		var invoiceDetailsView = new InvoiceDetailsView({model: this.model});
		invoiceDetailsView.show();
		return false;
	},

	statementDetails: function(){
		var statementDetailsView = new StatementDetailsView({model: this.model});
		statementDetailsView.show();
		return false;
	},
	
	foodBeverageDetails: function(e){
		
		// Show F&B item details if there are details that need set,
		// or if the user clicks on the link
		if(	
			(e && e != undefined) || 
			(this.model.get('modifiers').findWhere({'required': true}) ||
				this.model.get('number_soups') > 0 ||
				this.model.get('number_salads') > 0 ||
				this.model.get('number_sides') > 0)
		){	
			var editItemView = new FbEditItemView({model: this.model});
			editItemView.show();
		}
		return false;			
	},

	changeFee: function(e){
		var fee_id = $(e.currentTarget).val();
		var fee = App.data.course.get('fees').get(fee_id);
		this.model.setFee(fee.attributes);

		return false;
	},

	changeSeason: function(e){
		var season_id = parseInt($(e.currentTarget).val().trim());
		var season = App.data.seasons.get(season_id);

		var params = this.model.get('params');
		params.season_id = season_id;
		
		if(season){
			params.teesheet_id = parseInt(season.get('teesheet_id'));
		}
		this.model.set('params', params);

		var fee = App.data.course.get('fees').findWhere({
			'season_id': params.season_id,
			'item_type': this.model.get('item_type')
		});
		if(fee){
			this.model.setFee(fee.attributes);
		}
		this.render();
		
		return false;
	},

	toggleSelect: function(e){
		if($(e.currentTarget).prop('checked')){
			this.model.set({'selected':true}).save();
		}else{
			this.model.set({'selected':false}).save();
		}
	},

	changePrice: function(e){
		var value = $(e.currentTarget).val();
		
		if(!this.validate_number(value, this.model.get('unit_price'), e)){
			return false;
		}
		value = accounting.formatNumber(value, 2, '');
		this.model.set({'unit_price':value, 'price':value});

		this.model.calculatePrice();
		
		if(this.model.isValid()){
			this.model.save(null, {validate: false});
		} else {
            this.model.set("discount_percent",0);
            this.model.set("unit_price",this.model.get("base_price"));
        }
	},

	changeQuantity: function(e){	
		var value = parseFloat($(e.currentTarget).val());
		if(!this.validate_number(value, this.model.get('quantity'), e)){
			return false;
		}	
		value = parseFloat(value);
		this.model.set({'quantity':value});

		this.model.calculatePrice();
		this.model.save();
	},

	changeDiscount: function(e,skipSave){
		var value = this.$el.find("input.item-discount").val();
		if(!value){
			return;
		}
		if(!this.validate_number(value, this.model.get('discount'))){
            this.$el.find("input.item-discount").val(this.model.get("discount"));
			return false;
		}
		this.model.set({'discount_percent':value, 'discount':value});
		value = accounting.formatNumber(value, 2, '');
		this.model.calculatePrice();
		if(this.model.isValid()){
			if(skipSave){
				return;
			}
			this.model.save(null, {validate: false});
		} else {
			this.model.set("discount_percent",0);
			this.model.set("unit_price",this.model.get("base_price"));
		}
	},

});

var CartView = Backbone.Marionette.CompositeView.extend({

	tagName: 'div',
	className: 'row',
	template: JST['sales/cart.html'],
	childView: CartItemView,
	childViewContainer: '#cart-items table tbody',
	
	initialize: function(){
		this.listenTo(this.collection, 'invalid', this.showError);
		this.listenTo(this.model, 'change:mode', this.render_cart_mode);
		this.listenTo(this.model, 'change:check_all', this.render_check_all);
		this.listenTo(this.model, 'change:taxable', this.render_taxable);
	},
	
	showError: function(model, error){
		
		var previousAttr = model.previousAttributes();

		model.set({
			'unit_price': previousAttr.unit_price,
			'price': previousAttr.unit_price,
			'discount_percent': previousAttr.discount_percent,
			'discount': previousAttr.discount_percent
		});
		
		App.vent.trigger('notification', {'type': 'error', 'msg':error});
	},

	events: {
		"click .cart-mode": "changeMode",
		"keypress #item-search": "submitSearch",
		"click #cancel-sale": "cancelSale",
		"click #cart-checkall": "check_all"
	},
	
	render_cart_mode: function(){
		
		if(this.model.get('mode') == 'sale'){
			this.$el.find('#cart-header').removeClass('btn-danger').addClass('bg-primary');
			this.$el.find('#mode-sale').addClass('active').siblings().removeClass('active');
			
			var dropdown_link = this.$el.find('ul.dropdown-menu a.cart-mode');
			dropdown_link.data('mode', 'return').text('Return');
			this.$el.find('button.dropdown-toggle').html('Sale <span class="caret"></span>');	
		
		}else{
			this.$el.find('#cart-header').removeClass('bg-primary').addClass('btn-danger');
			this.$el.find('#mode-return').addClass('active').siblings().removeClass('active');
			
			var dropdown_link = this.$el.find('ul.dropdown-menu a.cart-mode');
			dropdown_link.data('mode', 'sale').text('Sale');
			this.$el.find('button.dropdown-toggle').html('Return <span class="caret"></span>');
		}
		
		this.$el.find('#item-search').val('').focus();
	},
	
	render_check_all: function(){
		if(this.model.get('check_all')){
			this.$el.find('#cart-checkall').prop('checked', true);
		}else{
			this.$el.find('#cart-checkall').prop('checked', false);
		}
	},
	
	render_taxable: function(){
		if(this.model.get('taxable')){
			this.$el.find('#taxable-checkbox').prop('checked', true);
		}else{
			this.$el.find('#taxable-checkbox').prop('checked', false);
		}
	},		
	
	onRender: function(){
		
		// Typeahead item suggestion engine
		var itemSearch = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote: {
				url: API_URL + '/items?q=%QUERY',
				wildcard: '%QUERY',
				transform: function(response){
					return response.rows;
				}
			},
			limit: 100
		});

		itemSearch.initialize();

		this.$el.find('#item-search').typeahead({
			hint: false,
			highlight: true,
			minLength: 1
		},
		{
			limit: 101,
			name: 'items',
			displayKey: 'name',
			source: itemSearch.ttAdapter(),
			templates: {
				suggestion: JST['item_search_result.html']
			}

		}).on('typeahead:selected', function(e, item, list){
			$(e.currentTarget).typeahead('val', '');
			App.data.cart.addItem(item);
		});
	},
	
	onShow: function(){
		this.$el.find('input.typeahead-item').focus();
	},
	
	check_all: function(e){
		var checked = false;
		if($(e.currentTarget).prop('checked')){
			checked = true;
		}
		
		if(this.model.get('items').length == 0){
			return true;
		}
		
		this.model.set('check_all', checked, {'silent': true});
		
		_.each(this.model.get('items').models, function(item){
			item.set({'selected': checked}).save();
		});
		
		return true;
	},
	
	submitSearch: function(e){
		
		var view = this;
		
		// Submit search if Enter is pressed
		if(e.keyCode != 13){
			return true;
		}
		var query = $(e.currentTarget).val();

		$.get(API_URL + '/cart/search', {q: query}, function(response){
			
			// If no data was found, do nothing
			if(response.length == 0 || !response[0]){
				view.$el.find('input.typeahead-item').val('').focus();
				App.vent.trigger('notification', {'msg': 'Item not found', 'type': 'error'});
				$('#item-search').val('').typeahead('val', '').focus();
				return false;
			}
			
			// If data found is a raincheck, apply it as payment
			if(response[0].raincheck_id){
				var raincheck = response[0];
				var payment = {};
				
				payment.amount = raincheck.total;
				payment.number = raincheck.raincheck_number;
				payment.type = 'raincheck';
				payment.description = 'Raincheck ' + raincheck.raincheck_number;
				payment.record_id = raincheck.raincheck_id;
				
				App.data.cart.get('payments').addPayment(payment);					
			
			// If data found is a coupon, apply it as payment
			}else if(response[0].promo_id){
				var coupon = response[0];
				var payment = {};
				
				payment.amount = coupon.total;
				payment.number = coupon.promo_id;
				payment.type = 'coupon';
				payment.description = 'Coupon';
				payment.record_id = coupon.promo_id;
				
				App.data.cart.get('payments').addPayment(payment);	
			
			// If data found is a past sale, load cart for refund
			}else if(response[0].sale_id){
				var sale = new Sale(response[0]);
				App.data.cart.prepareFullReturn(sale);
				
			// If data is an item
			}else{			
				response[0]['selected'] = true;
				App.data.cart.addItem(response[0]);
			}

			$('#item-search').val('').typeahead('val', '').focus();				
		});
		
		return true;
	},
	
	changeMode: function(event){
		var mode = $(event.currentTarget).data('mode');
		App.data.cart.save({'mode': mode});
		event.preventDefault();
	},
	
	cancelSale: function(){
		
		// If cart has a suspended name, clicking the cancel button only re-suspends it
		// Warn user if they are about to refund all cart payments
		if(this.model.get('payments').length > 0 && (!this.model.get('suspended_name') || this.model.get('suspended_name') == '')){
			if(!confirm('This sale still contains payments, cancelling it will refund all payments.')){
				return false;
			}
		}

		this.model.cancel();
		return false;
	}
});

var CartTotalsView = Backbone.View.extend({

	tagName: 'div',
	className: 'inner-content',
	template: JST['sales/cart_totals.html'],

	events: {
		'click #taxable-checkbox': 'changeTaxable'
	},

	initialize: function(){
		this.listenTo(App.data.cart, 'change:selected change:total change:num_items change:taxable change:credit_card_fees change:service_fee_subtotal', this.render);
		this.listenTo(App.data.user.get('acl'), 'change', this.render);
	},

	render: function() {
		var attr = App.data.cart.attributes;
		this.$el.html(this.template(attr));
		return this;
	},

	changeTaxable: function(e){
		if(this.$el.find('#taxable-checkbox').prop('checked') === true){
			App.data.cart.save({'taxable': true});
		}else{
			App.data.cart.save({'taxable': false});
		}
	}
});
