var TerminalHeaderView = Backbone.Marionette.ItemView.extend({
	tagName: "div",
	className: "",
	template: JST['sales/terminal_header.html'],

	initialize: function(){
		this.listenTo(this.collection, 'change:active', this.render);
	},

	render: function(){
		var terminal = this.collection.getActiveTerminal();
		this.model = terminal;
		
		if(!terminal){
			return this;
		}
		var attr = this.model.attributes;
		this.$el.html(this.template(attr));
		
		return this;
	}
});
