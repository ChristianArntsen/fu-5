var SuspendSelectionView = ModalView.extend({

	id: 'suspend-sale',
	template: JST['sales/suspend_selection_window.html'],

	events: {
		'click button.add-suspended-sale-card': 'add_credit_card',
		'click button.close-window': 'hide',
		'click div.number': 'suspend',
		'click button.suspend': 'suspend'
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
		this.listenTo(App.data.cart.get('payments'), 'add', this.credit_card_saved);
	},

    render: function(){
		var numbers_taken = {};
		_.each(App.data.suspended_sales.models, function(suspended_sale){
			numbers_taken[suspended_sale.get('suspended_name')] = true;
		});
		var attributes = this.model.attributes
		attributes.numbers_taken = numbers_taken;
		attributes.allow_credit_card_save = false;

		if(App.data.course.get('merchant') && App.data.course.get('merchant') != ''){
			attributes.allow_credit_card_save = true;
		}

		var html = this.template(attributes);
		this.$el.html(html);

		return this;
    },

    suspend: function(e){

		// If a sale with that number already exists, do nothing
		if($(e.currentTarget).hasClass('active')){
			return false;
		}
		if($(e.currentTarget).hasClass('number')){
			var label = $(e.currentTarget).text();
		}else{
			var label = this.$el.find('#suspended-label').val();
		}

		if(!label || label == ''){
			return false;
		}
		var view = this;
		
		this.$el.loadMask();
		
		App.data.cart.suspend(label, function(){
			view.hide();
			
		}, function(){
			$.loadMask.hide();
		});

		return false;
	},

    add_credit_card: function(e){
        var paymentWindow = new SalePaymentWindow({
        	collection: App.data.cart.get('payments')}, 
        	{
        		extraClass: 'modal-lg'
        	}
        );

        paymentWindow.save_card = true;
        paymentWindow.show();
        paymentWindow.showCreditCard(e);
        
        return false;
    }
});
