var IssueGiftcardView = ModalView.extend({

	id: 'issue_giftcard_window',
	template: JST['sales/issue_giftcard_window.html'],
	etsValueTemplate: JST['sales/issue_giftcard_ets_value.html'],
	etsTemplate: JST['sales/issue_giftcard_ets.html'],
	
	events: {
		"click button.save": "save",
		"click input.limit_to": "changeLimitTo",
		"change .action input": "changeAction",
		"onCardswipe": function(){ this.$el.find('div.modal-body').loadMask(); },
		"afterCardswipe": function(){ $.loadMask.hide(); },
		"keypress #giftcard-number": function(e){ if(e.keyCode == 13){ e.preventDefault(); return false; } },
		"click button.continue": "etsContinue"
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
		this.listenTo(App.vent, 'gift-card', this.processEtsTransaction);	
	},

    render: function(){
		var attrs = this.model.attributes;
		var view = this;
		var model = this.model;		
		
		if(App.data.course.get('use_ets_giftcards') == 1){
			this.$el.html(this.etsValueTemplate(attrs));
		
		}else{
			this.$el.html(this.template(attrs));			
			
			init_category_search(this.$el.find('#giftcard-category'));
			init_department_search(this.$el.find('#giftcard-department'));

			init_customer_search(this.$el.find('#giftcard-customer'), function(e, customer, list){
				view.$el.find('#giftcard-customer-id').val(customer.person_id);
				view.$el.find('#giftcard-customer').val(customer.first_name +' '+ customer.last_name).data('customer', customer);
				var params = model.get('params');
				params.customer = _.pick(customer, ['first_name', 'last_name', 'person_id']);
				model.set('params', params);
				return false;
			});		
			
			this.$el.find('#giftcard-number').cardSwipe({
				parseData:!SETTINGS.nonfiltered_giftcard_barcodes
			});
			
			this.$el.find('#giftcard-expiration').datetimepicker({
				format: 'MM/DD/YYYY'
			});
			
			setTimeout(function(){
				view.$el.find('#giftcard-number').focus();
			}, 50);
			
			this.$el.find('form').bootstrapValidator();

			this.changeAction();
		}	
		return this;
    },
    
    etsContinue: function(){
		
		var view = this;
		var amount = this.$el.find('#giftcard-value').val();
		this.model.set({'unit_price': amount}).save();
		
		this.$el.html(this.etsTemplate());
		this.$el.loadMask();
		
		var data = {
			'action': 'refund',
			'amount': amount,
			'type': 'gift_card'
		};
		
		// Load iframe
		$.post(SITE_URL + '/v2/home/credit_card_window', data, function(response){
			view.$el.find('div.modal-body').html(response);
			$.loadMask.hide();
		},'html');		
	},
	
	processEtsTransaction: function(card){
		App.vent.trigger('notification', {'msg': 'Gift card credited', 'type': 'success'});
		this.hide();
		
		return false;
	},
    
	numberExists: function(number, callback, search_cart){
		
		if(search_cart === undefined){
			search_cart = false;
		}
		var view = this;

		// First check the giftcard isn't already in the cart
		if(search_cart){
			
			var giftcard_exists_in_cart = _.filter(App.data.cart.get('items').models, function(item){
				
				if(
					item && 
					item.cid != view.model.cid &&
					item.get('item_type') == 'giftcard' && 
					item.get('params') && 
					item.get('params').giftcard_number && 
					item.get('params').giftcard_number == number
				){
					return true;
				}
			});

			if(giftcard_exists_in_cart[0]){
				callback(true);
				return false;
			}			
		}

		$.get(GIFTCARDS_URL, {giftcard_number: number}, function(response){
			if(response && response.length > 0){
				callback(parseInt(response[0].giftcard_id));
			}else{
				callback(false);
			}	
		},'json');
		
		return true;
	},
	
	reloadCard: function(e){
		
		var view = this;
		var form = this.$el.find('form');
		
		var giftcard_data = {
			"giftcard_number": view.$el.find('input[name="giftcard_number"]').val(),			
			"giftcard_id": 0,
			"action": view.$el.find('input[name="action"]:checked').val()
		};	
		var item_data = {
			"unit_price": view.$el.find('input[name="value"]').val(),
			"params": giftcard_data
		}
		
		// Make sure giftcard exists
		this.numberExists(giftcard_data.giftcard_number, function(giftcard_id){		
			if(!giftcard_id){
				form.bootstrapValidator('updateMessage', 'giftcard_number', 'notEmpty', 'No giftcard found with this number');
				form.bootstrapValidator('updateStatus', 'giftcard_number', 'INVALID');		
				view.$el.loadMask.hide();
			}else{
				item_data.params.giftcard_id = giftcard_id;
				form.bootstrapValidator('updateStatus', 'giftcard_number', 'VALID');
				view.model.set(item_data).save();
				view.hide();				
			}
		});					
	},
	
	newCard: function(e){
		
		var view = this;
		var form = this.$el.find('form');		
		
		var giftcard_data = {
			"giftcard_number": view.$el.find('input[name="giftcard_number"]').val(),
			"expiration_date": view.$el.find('input[name="expiration_date"]').val(),
			"department": view.$el.find('input[name="department"]').val(),
			"category": view.$el.find('input[name="category"]').val(),
			"customer": this.model.get('params').customer,
			"details": view.$el.find('textarea[name="details"]').val(),
			"action": view.$el.find('input[name="action"]:checked').val()
		};
		
		var item_data = {
			"unit_price": view.$el.find('input[name="value"]').val(),
			"params": giftcard_data
		}
		
		// Check if giftcard already exists
		this.numberExists(giftcard_data.giftcard_number, function(taken){		
			if(taken){
				form.bootstrapValidator('updateMessage', 'giftcard_number', 'notEmpty', 'Giftcard number is already taken');
				form.bootstrapValidator('updateStatus', 'giftcard_number', 'INVALID');		
				view.$el.loadMask.hide();
			}else{
				form.bootstrapValidator('updateStatus', 'giftcard_number', 'VALID');
				view.model.set(item_data).save();
				view.hide();				
			}
		}, true);			
	},
	
	save: function(e){
		
		e.preventDefault();
		var view = this;

		// Validate that required fields are filled in
		var form = this.$el.find('form');
		var validation = form.data('bootstrapValidator').validate();

		if(!validation.isValid()){
			return false;
		}
		view.$el.loadMask();

		var action = view.$el.find('input[name="action"]:checked').val();
		form.bootstrapValidator('updateStatus', 'giftcard_number', 'VALIDATING');
		
		if(action == 'new'){
			this.newCard();
		}else{
			this.reloadCard();		
		}

		return false;
	},
	
	changeLimitTo: function(e){
		var radio = $(e.currentTarget);
		if(radio.val() == 'category'){
			this.$el.find('input[name="department"]').attr('disabled', true).val('');
			this.$el.find('input[name="category"]').attr('disabled', false);
		}else{
			this.$el.find('input[name="department"]').attr('disabled', false);
			this.$el.find('input[name="category"]').attr('disabled', true).val('');			
		}
	},
	
	changeAction: function(e){
		if(e){
			e.preventDefault();
		}
		var type = this.$el.find('input[name="action"]:checked').val();

		if(type == 'reload'){
			this.$el.find('div.new-card').hide().find('input').not('[name="department"],[name="category"]').attr('disabled', true);
		}else{
			this.$el.find('div.new-card').show().find('input').not('[name="department"],[name="category"]').attr('disabled', false);
		}
		
		return false;
	},
	
	success: function(raincheck){
		this.hide();
		App.vent.trigger('notification', {'msg': 'Raincheck issued', 'type': 'success'});
	},
	
	error: function(e){
		this.$el.loadMask('hide');
		App.vent.trigger('notification', {'msg': 'Error issuing raincheck', 'type': 'error'});		
	},

	onHide: function(){
		var params = this.model.get('params');
		if(App.data.course.get('use_ets_giftcards') == 0 && (!params || !params.giftcard_number)){
			this.model.destroy();
		}
	}
});