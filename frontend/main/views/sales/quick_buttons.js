var QuickButtonsView = Backbone.View.extend({

	tagName: 'div',
	className: 'inner-content',
	template: JST['sales/quick_buttons.html'],

	initialize: function(){
		this.listenTo(this.collection, 'add remove reset', this.render_buttons);
		
		if(App.data.course.get_setting('quickbutton_tab') != undefined){
			this.tab = App.data.course.get_setting('quickbutton_tab');
		}else{
			this.tab = '1';
		}
		this.editing = false;
		
		this.listenTo(App.data.user, 'change:user_level', this.toggle_sale_settings);
	},

	events: {
		"click .quick-button-tabs a": "highlightTab",
		"click #edit-quick-buttons": "edit",
		"click #save-quick-buttons": "save",
		"click #add-quick-button": "add_button",
		"click #sale-settings": "sale_settings"
	},

	highlightTab: function(e){
		this.$el.find('.quick-button-tabs a').removeClass('active');
		$(e.currentTarget).addClass('active');
		this.tab = String($(e.currentTarget).data('tab-num'));
	},

	render: function(){
		
		this.$el.html(this.template({
			tab: this.tab, 
			editing: this.editing
		}));
		
		this.render_buttons();
		this.render_settings();
		this.toggle_sale_settings();
		
		return this;
	},
	
	toggle_sale_settings: function(){
		
		var show_sale_settings = false;
		if(App.data.user.is_manager() || App.data.user.is_admin() || App.data.user.is_super_admin()){
			show_sale_settings = true;
		}
		
		if(!show_sale_settings){
			this.$el.find('#sale-settings').hide();
		}else{
			this.$el.find('#sale-settings').show();
		}
	},
	
	render_buttons: function(){
		var buttons_view = this;
		
		for(var x = 1; x <= 3; x++){
			var buttons = this.$el.find('#quick-buttons-list-'+x);
			buttons.html('');
			var models = this.collection.where({tab: String(x)});

			if(models.length > 0){
				_.each(models, function(quick_button){
					var view = new QuickButtonView({model: quick_button});
					buttons.append( view.render().el );
				});
			}
		}	
		
		return this;	
	},
	
	render_settings: function(){
		if(!this.editing){
			var html = '<button class="btn btn-xs btn-default" style="padding: 3px 8px;" id="edit-quick-buttons">' +
				'<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Buttons' +
			'</button>';
		}else{
			var html = '<button class="btn btn-xs btn-default" style="padding: 3px 8px;" id="add-quick-button">'+
				'<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add New Button</button>'+
				'<button class="btn btn-xs btn-default" style="padding: 3px 8px;" id="save-quick-buttons">'+
				'<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Save Buttons</button>';
		}
		this.$el.find('#quickbutton-settings').html(html);	
	},

	edit: function(e){
		
		this.editing = true;
		this.render_settings();
		
		this.$el.find('div.tab-content').addClass('editing');
		this.$el.find('ul.quick-buttons').sortable({
			cancel: '',
			distance: 5
		});
		
		return false;
	},
	
	add_button: function(){
		
		var models = this.collection.where({tab: String(this.tab)});
		var next_pos = 0;
		_.each(models, function(btn){
			if(parseInt(btn.get('position')) > next_pos){
				next_pos = parseInt(btn.get('position'));
			}
		});

		var button = new QuickButton({tab: String(this.tab), items: [], position: next_pos + 1});
		var view = new EditQuickButtonView({model: button, collection: this.collection});

		view.show();
		return false;
	},
	
	sale_settings: function(e){
		var sale_settings_window = new SaleSettingsWindow({model: App.data.course});
		sale_settings_window.show();
		return false;
	},

	save: function(e){
		
		this.editing = false;
		this.render_settings();
		var list_items = this.$el.find('ul.quick-buttons > li');
		
		var collection = this.collection;
		
		this.$el.find('div.tab-content').removeClass('editing');
		this.$el.find('ul.quick-buttons').sortable('destroy');
		
		list_items.each(function(){
			var position = $(this).index();
			var id = $(this).data('id');
			collection.get(id).set('position', position);	
		});	
		
		this.editing = false;	
		
		this.collection.saveOrder();
		return false;
	}
});

var QuickButtonView = Backbone.View.extend({

	tagName: 'li',
	className: '',

	template: JST['sales/quick_button.html'],

	initialize: function(options){
		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model.get('items'), 'add remove change', this.render);
	},

	events: {
		"click .quick-button-item, .quick-button": "addItem",
	},

	render: function(){
		var view = this;
		var attrs = this.model.attributes;

		this.$el.html(this.template(attrs));
		this.$el.attr('data-id', this.model.cid);
		return this;
	},

	addItem: function(e){
		var menu_item = $(e.currentTarget);

		if(menu_item.parents('div.tab-content').hasClass('editing')){
			this.edit();
			return false;
		}

		if(menu_item.parent().hasClass('dropdown')){
			return true;
		}

		var id = menu_item.data('item-id');
		var item = this.model.get('items').get(id);

		if(!item){
			return false;
		}
		
		var item_data = item.toJSON();
		App.data.cart.addItem(item_data);
	},

	edit: function(e){
		var view = new EditQuickButtonView({model: this.model});
		view.show();
		return false;
	}
});

var EditQuickButtonView = ModalView.extend({

	id: 'edit-quick-button',
	template: JST['sales/edit_quick_button_window.html'],

	events: {
		"click button.save": "save",
		"click button.delete": "deleteButton"
	},

	initialize: function(){
		ModalView.prototype.initialize.call(this);
		this.listenTo(this.model, 'invalid', this.showError);	
		this.listenTo(this.model, 'sync', this.hide);
		this.listenTo(this.model.get('items'), 'add remove reset', this.renderItems);		
	},

	render: function(){
		var attributes = this.model.attributes;
		attributes.is_new = this.model.isNew();
		var html = this.template(attributes);
		this.$el.html(html);
		var view = this;

		init_item_search(this.$el.find('#quick-button-item'), function(e, item, list){
			view.model.get('items').add(item);
			$(e.currentTarget).val('');
			return false;
		});
		
		this.renderItems();
		return this;
	},
	
	renderItems: function(){
		
		var items = this.model.get('items');
		var list = this.$el.find('ul');
		list.html('');
		
		_.each(items.models, function(item){
			list.append( new QuickButtonItemView({model: item}).render().el );
		});
		
		list.sortable({
			cancel: ''
		});
		
		return this;
	},

	save: function(){
		var list = this.$el.find('ul');
		var items = this.model.get('items');
		
		if(!this.model.set({
			display_name: this.$el.find('#quick-button-label').val(),
			color: this.$el.find('input[name="color"]:checked').val()
		}, {validate: true})){
			return false;
		}
		
		list.children().each(function(index, el){
			var position = $(el).index();
			var id = $(el).data('id');
			var item = items.get(id);
			item.set('order', position, {silent: true});
		});
		items.sort();			
		
		if(!this.model.collection){
			this.collection.create(this.model);
		}else{
			this.model.save();
		}
	},
	
	deleteButton: function(){
		this.model.destroy();
		this.hide();
		return false;
	},
	
	showError: function(model, error){
		App.vent.trigger('notification', {msg: error, type: 'danger'});
	}	
});

var QuickButtonItemView = Backbone.View.extend({
	
	tagName: 'li',
	template: JST['sales/quick_button_item.html'],

	events: {
		"click .delete": "delete_item"
	},
	
	render: function(){
		var html = this.template(this.model.attributes);
		this.$el.html(html);
		this.$el.attr('data-id', this.model.cid);
		return this;
	},
	
	delete_item: function(){
		this.model.destroy();
		return false;
	}
});
