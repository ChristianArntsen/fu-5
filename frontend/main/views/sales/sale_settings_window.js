var SaleSettingsWindow = ModalView.extend({
	
	tagName: "div",
	id: "sale_settings_window",
	template: JST['sales/sale_settings_window.html'],

	initialize: function(options) {
		if(options && options.closable){
			this.closable = options.closable;
		}
		if(options && options.backdropOpacity){
			this.backdropOpacity = options.backdropOpacity;
		}
		ModalView.prototype.initialize.call(this, options);
		
		this.listenTo(App.data.course, 'sync', this.saved);
		this.listenTo(App.data.course, 'error', this.show_error);						
	},

	events: {
		"click button.save": "save",
	},
	
	show_error: function(model, response){
		App.vent.trigger('notification', {'type': 'error', 'msg': response.responseJSON.msg});
		$.loadMask.hide();
	},
	
	saved: function(){
		App.vent.trigger('notification', {'type': 'success', 'msg': 'Settings saved'});
		this.hide();
	},

	render: function() {	
		var attr = this.model.attributes;
		this.$el.html(this.template(attr));
		return this;
	},
	
	save: function(){
		var data = {};
		data = _.getFormData(this.$el.find('#sale-settings'));
		App.data.course.save(data, {'patch': true});
		return false;
	}
});
