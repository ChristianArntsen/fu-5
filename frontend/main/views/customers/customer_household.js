var CustomerHouseholdMemberView = Backbone.Marionette.ItemView.extend({
	template: JST['customers/customer_household_member.html'],
	tagName: 'li',
	className: 'group',

	events: {
		"click button.delete": "removeMember"
	},

	removeMember: function(){
		this.model.collection.remove(this.model);
	}
});

var CustomerHouseholdView = Backbone.Marionette.CompositeView.extend({
	template: JST['customers/customer_household.html'],
	childViewContainer: 'ul',
	childView: CustomerHouseholdMemberView,

	onRender: function(){
		var collection = this.collection;
		var field = this.$el.find('#customer-household-member-search');

		init_customer_search(field, function(e, customer) {
            if (customer.household_members.length > 0) {
                alert(customer.first_name + ' ' + customer.last_name + ' is the head of a household. Remove all their household members before adding them to a new household.');
                return false;
            }

            if (customer.household_id > 0) {
                alert(customer.first_name + ' ' + customer.last_name + ' is a member of another household.');
                return false;
            }

            if (confirm('Account balances from newly added household members will be transferred to this account. Continue?')) {
                collection.add(_.pick(customer, ['person_id', 'first_name', 'last_name']));
                field.typeahead('val', '');
                return false;
            }
            field.typeahead('val', '');
			return false;
		});
	},

	onAddChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	},

	onRemoveChild: function(){
		this.$el.find('span.count').text( this.collection.length );
	}
});