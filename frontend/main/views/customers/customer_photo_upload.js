var CustomerPhotoUploadView = ModalView.extend({

	tagName: 'div',
	template: JST['customers/photo_upload.html'],
	webcamTemplate: JST['customers/photo_webcam.html'],
	id: 'customer-photo-upload',

	events: {
		"click #take-photo": "webcam_init",
		"click .snap-photo": "webcam_snap",
		"click .webcam-retry": "webcam_retry",
		"click .webcam-accept": "webcam_save",
		"click .crop": "init_crop",
		"click .save-crop": "save_crop",
		"click .cancel-crop": "close_crop_tool",
		"click .save": "save",
		"click #select-photo": "select_photo",
		"change #file-input": "load_selected_photo",
		"mouseup .modal-backdrop": "preventCropClose"
	},

	initialize: function(properties){
        ModalView.prototype.initialize.call(this, {extraClass: 'modal-lg'});

        if(properties && properties.take){
        	this.take = true;
        }
        var view = this;
        
        this.saveCustomer = false;
        if(properties && properties.saveCustomer){
        	this.saveCustomer = true;
        }

        // When user allows access to webcam
        Webcam.on('live', function(){
        	view.$el.find('.webcam-instructions').hide();
        	view.$el.find('.snap-photo').attr('disabled', null);
        });

        // If no webcam is installed or user denies access
        Webcam.on('error', function(){
        	App.vent.trigger('notification', {'msg': 'Error accessing webcam', 'type': 'error'});
        	view.hide();
        });
    },

    render: function(){
    	
    	var attr = this.model.toJSON();
    	var view = this;

    	if(this.take){
    		this.take = false;
    		this.$el.html(this.template({photo: false}));
			_.delay(function(){ view.webcam_init(); }, 10);
    		
    		return false;
    	}
    	
   		if(this.photo){
   			attr.photo = this.photo;
   		}

   		if(this.no_photo(attr.photo)){
   			attr.photo = false;
   		}else{
   			attr.photo = this.hq_photo(attr.photo);	
   		}

    	this.$el.html(this.template(attr));
		return this;
	},

	preventCropClose: function(){
		return false;
	},

	// Checks if a photo doesn't exist for the person
	no_photo: function(url){
		if(!url || url == '' || url.indexOf('profile_picture') > -1){
			return true;
		}
		return false;
	},

	// Gets the high quality version of an uploaded photo
	hq_photo: function(url){
		return url.replace('thumbnails', 'default');
	},

	save: function(){
		var imageData = this.$el.find('#customer-photo').prop('src');
		var width = this.$el.find('#customer-photo')[0].naturalWidth;
		var height = this.$el.find('#customer-photo')[0].naturalHeight;
		var view = this;

		var data = {
			data: imageData,
			width: width,
			height: height
		};

		this.$el.loadMask({message: 'Saving photo...'});
		this.model.upload_photo(data, function(response){
			
			if(response && response.url){
				App.vent.trigger('customer:photo', response);
			}

			if(view.saveCustomer){
				var customer_data = {};
				customer_data.photo = response.url;
				customer_data.image_id = response.image_id;
				
				view.model.set(customer_data).save(null, {
					error: function(model, response){

						// Photo most likely errored because a field was not filled out on customer
						// Display customer edit to resolve the issue
						var customer_edit_window = new CustomerFormView({
							model: view.model
						});
						customer_edit_window.show();

						App.vent.trigger('notification', {'msg': response.responseJSON.msg, 'type': 'danger'});
						$.loadMask.hide();
						view.hide();
					},
					success: function(){
						App.vent.trigger('notification', {'msg': 'Photo saved', 'type': 'success'});
						$.loadMask.hide();
						view.hide();						
					}
				});
			
			}else{
				$.loadMask.hide();
				view.hide();				
			}
		});

		return false;
	},

	select_photo: function(){
		this.$el.find('#file-input').click();
	},

	load_selected_photo: function(e){
	    var view = this;
	    
	    loadImage(
	        e.target.files[0],
	        function (img) {
	        	if(!img.toDataURL){
	        		App.vent.trigger('notification', {'type': 'error', 'msg': 'Error processing photo. Make sure file selected is a valid image.'});
	        	}else{
	        		if(view.$el.find('#customer-photo:visible').length > 0){
	        			view.$el.find('#customer-photo').prop('src', img.toDataURL('image/jpeg'));
	        		}else{
	        			view.photo = img.toDataURL('image/jpeg');
	        			view.render();
	        		}
	        	}
	        },{
	        	maxWidth: 2048,
	        	canvas: true
	       	}
	    );

	    return false;
	},

	init_crop: function(){

		var view = this;
		var width = parseInt(this.$el.find('div.photo-container').width());
		var height = parseInt(this.$el.find('div.photo-container').height());

		this.$el.find('div.photo-edit').hide();
		this.$el.find('div.photo-replace').hide();
		this.$el.find('div.modal-footer').hide();
		this.$el.find('div.photo-crop').show();

		$('#modal-customer-photo-upload').data('bs.modal').options.backdrop = 'static';

		this.$el.find('#customer-photo').cropper({
			aspectRatio: 1,
			autoCrop: true,
			rotatable: false,
			zoomable: false
		});

		return false;
	},

	// Replace image with new cropped version
	save_crop: function(){
		
		var image = this.$el.find('#customer-photo');
		var canvas = image.cropper('getCroppedCanvas');
		var croppedImageData = canvas.toDataURL('image/jpeg');
		
		this.close_crop_tool();

		image.prop('src', croppedImageData);
		return false;
	},

	close_crop_tool: function(){
		this.$el.find('div.photo-edit').show();
		this.$el.find('div.photo-replace').show();
		this.$el.find('div.modal-footer').show();
		this.$el.find('div.photo-crop').hide();
		$('#modal-customer-photo-upload').data('bs.modal').options.backdrop = true;

		this.$el.find('#customer-photo').cropper('destroy');
		return false;
	},

	webcam_init: function(){

		var html = this.webcamTemplate();
		this.$el.find('div.modal-content').html(html);

		Webcam.set({
			width: 480,
			height: 360,
			dest_width: 640,
			dest_height: 480,
			crop_width: 480,
			crop_height: 480,
			image_format: 'jpeg',
			jpeg_quality: 100
		});

		Webcam.attach( this.$el.find('#webcam-photo')[0] );
		return false;
	},

	webcam_snap: function(){
			
		Webcam.freeze();

		this.$el.find('#webcam-photo').addClass('snapped fadeOut');
		this.$el.find('.webcam-take').hide();
		this.$el.find('.webcam-approve').show();
		return false;
	},

	webcam_retry: function(){
		
		Webcam.unfreeze();

		this.$el.find('#webcam-photo').removeClass('snapped fadeOut');
		this.$el.find('.webcam-approve').hide();
		this.$el.find('.webcam-take').show();
		return false;		
	},

	webcam_save: function(){
		var model = this.model;
		var view = this;
		
		Webcam.snap(function(data_uri){
			Webcam.reset();
			view.photo = data_uri;
			view.render();
		});
	},

	onHide: function(){
		Webcam.off('live');
		Webcam.off('error');
	}
});