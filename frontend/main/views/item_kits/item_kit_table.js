var ItemKitTableView = Backbone.Marionette.ItemView.extend({
	
	tagName: 'div',
	className: 'col-xs-12',
	template: JST['item_kits/item_kit_table.html'],

	events: {
		'click .edit': 'edit',
		'click .delete': 'deleteItemKit',
		'check.bs.table': 'toggle_table_actions',
		'uncheck.bs.table': 'toggle_table_actions',
		'check-all.bs.table': 'toggle_table_actions',
		'uncheck-all.bs.table': 'toggle_table_actions'
	},

	initialize: function(){
		this.listenTo(this.collection, 'sync', this.updateRow);
		this.listenTo(this.collection, 'destroy', this.deleteRow);
	},

	updateRow: function(model){
		var index = this.collection.indexOf(model);
		var rowData = model.toJSON();

		var row = this.$el.find('table.item-kits').bootstrapTable('getRowByUniqueId', model.get('item_kit_id'));
		if(row){
			var action = 'updateRow';
		}else{
			var action = 'insertRow';		
		}

		this.$el.find('table.item-kits').bootstrapTable(action, {
			index: index,
			row: rowData
		});	
	},

	toggle_table_actions: function(){
		
		var selections = this.$el.find('table').bootstrapTable('getSelections');
		if(!selections || selections.length == 0){
			this.$el.find('.table-toolbar button').prop('disabled', true);
		}else{
			this.$el.find('.table-toolbar button').prop('disabled', false);
		}
	},

	deleteRow: function(model){
		this.$el.find('table.item-kits').bootstrapTable('removeByUniqueId', model.get('item_kit_id'));
	},

	edit: function(e){
		var link = $(e.currentTarget);
		var itemKit = this.collection.at(link.data('index'));
		
		var itemKitWindow = new EditItemKitView({model: itemKit});
		itemKitWindow.show();

		return false;
	},

	deleteItemKit: function(e){
		var link = $(e.currentTarget);
		var itemKit = this.collection.at(link.data('index'));		

		if(confirm('Are you sure you want to delete this item kit?')){
			itemKit.destroy({wait: true});
		}
		return false;
	},

	onRender: function(){
		
		var url = this.collection.url;
		var view = this;

		this.$el.find('table.item-kits').bootstrapTable({
			classes: 'table-no-bordered',
			url: url,
			data: this.collection.toJSON(),
			uniqueId: 'item_kit_id',
			sidePagination: 'server',
			columns: [
				{
					checkbox: true
				},
				{
					title: 'UPC',
					field: 'item_number',
					sortName: 'item_number',
					sortable: true
				},
				{
					title: 'Name',
					field: 'name',
					sortable: true
				},
				{
					title: 'Description',
					field: 'description',
					sortable: true
				},				
				{
					title: 'Price',
					field: 'unit_price',
					sortable: true
				},
				{
					formatter: function(val, row, index){
						return '<div class="btn-group pull-right"><button type="button" data-index="'+index+'" class="btn btn-default btn-xs delete text-danger">Delete</button>' +
							'<button type="button" data-index="'+index+'" class="btn btn-default btn-xs edit">Edit</button></div>';
					}
				}
			],
			page: 1,
			pagination: true,
			pageSize: 50,
			pageNumber: 1,
			pageList: [10,25,50,100],
			search: true,
			trimOnSearch:false,
			toolbar: $('<div class="table-toolbar"><button class="btn btn-default text-danger delete">Delete</button><button class="btn btn-default print-barcode-sheet">Print Barcode Sheet</button><button class="btn btn-default print-barcodes">Print Barcodes</button></div>')[0],
			onLoadSuccess: function(data){
				view.collection.reset(data.rows);
			}
		});

		this.$el.find('div.search').prepend('<span class="fa fa-search input-icon"></span>');
		this.toggle_table_actions();
	}
});