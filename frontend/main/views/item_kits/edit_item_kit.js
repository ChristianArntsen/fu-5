var EditItemKitView = ModalView.extend({

	id: 'item_kit_window',
	template: JST['item_kits/edit_item_kit.html'],

	events: {
		"click button.save": "save",
        "change #item-kit-max-discount": "change_max_discount",
        "change input.item-quantity": "changeItemQuantity",
		"change input.item-price": "changeItemPrice",
		"click a.delete-item": "deleteItem"
	},

	initialize: function(){
		var options = {
			extraClass: 'modal-lg'
		};
		ModalView.prototype.initialize.call(this, options);
		this.customer = false;

		// unset items if this is a newly instantiated kit without even a name or price
		if(this.model.get('items').length && !this.model.get('unit_price') && !this.model.get('name'))this.model.set('items',[]);

		this.listenTo(this.model, 'change:unit_price', this.render);
        this.temp_model = new ItemKit(this.model.toJSON());
        this.listenTo(this.temp_model, 'change:unit_price change:max_discount', this.render_minimum_price);
	},

    render: function(){
		var model = this.model;
		var attrs = model.attributes;
		var view = this;
		this.$el.html(this.template(attrs));

		// Typeahead item suggestion engine
		var itemSearch = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote: {
				url: API_URL + '/items?q=%QUERY&item_type[]=item&item_type[]=fee',
				wildcard: '%QUERY',
				transform: function(response){
					return response.rows;
				}
			},
			limit: 100
		});

		itemSearch.initialize();

		this.$el.find('#item-kit-search').typeahead({
			hint: false,
			highlight: true,
			minLength: 1
		},{
			limit: 101,
			name: 'items',
			displayKey: 'name',
			source: itemSearch.ttAdapter(),
			templates: {
				suggestion: JST['item_search_result.html']
			}

		}).on('typeahead:selected', function(e, item, list){
			$(e.currentTarget).typeahead('val', '');
			view.addItem(item);
		});
        this.render_minimum_price();
		init_department_search(this.$el.find('#item-kit-department'));
		init_category_search(this.$el.find('#item-kit-category'));
		init_category_search(this.$el.find('#item-kit-subcategory'));		

		return this;
    },

    addItem: function(item){
		
		var items = this.model.get('items');
		var item = _.pick(item, ['unit_price', 'name', 'item_id', 'cost_price', 'price_class_id', 'timeframe_id', 'item_type']);
		
		if(item.item_type == 'giftcard' || item.item_type == 'pass'){
			App.vent.trigger('notification', {msg: 'Gift cards and passes can\'t be added to item kits', type: 'error'});
			return false;
		}
        // Watch for duplicate items being added
        for (var i in items) {
            if (item.item_id == items[i].item_id && item.price_class_id == items[i].price_class_id) {
                App.vent.trigger('notification', {
                    msg: 'You have already added that item to the item kit. Please update the quantity.',
                    type: 'error'
                });
                return false;
            }
        }
		item.quantity = 1;
		item.total = this.calculateItemTotal(item.quantity, item.unit_price);
		item.total_cost = this.calculateItemTotal(item.quantity, item.cost_price);
		
		if(!item.timeframe_id){
			item.timeframe_id = null;
		}
		items.push(item);

		this.setModel();
		this.model.set('items', items);
		this.adjustTotal();
    },
	
	deleteItem: function(e){
		var index = $(e.currentTarget).data('index');
		var items = this.model.get('items');
		items.splice(index, 1);
		this.setModel();
		this.adjustTotal();
		return false;
	},

	changeItemPrice: function(field){
		var index = $(field.currentTarget).data('index');
		var item = this.model.get('items')[index];
		item.unit_price = _.getNumber($(field.currentTarget).val());
		item.total = this.calculateItemTotal(item.quantity, item.unit_price);
		this.setModel();
		this.adjustTotal();
	},

	changeItemQuantity: function(field){
		var index = $(field.currentTarget).data('index');
		var item = this.model.get('items')[index];
		item.quantity = _.getNumber($(field.currentTarget).val());
		item.total = this.calculateItemTotal(item.quantity, item.unit_price);
		item.total_cost = this.calculateItemTotal(item.quantity, item.cost_price);
		this.setModel();
		this.adjustTotal();
	},

    change_max_discount: function(){
        this.temp_model.set('max_discount', this.$el.find('#item-kit-max-discount').val());
    },

	calculateItemTotal: function(qty, price){
		var price = new Decimal(price);
		var qty = new Decimal(qty);
		var total = price.times(qty).toDP(2).toNumber();
		return total;		
	},

    render_minimum_price: function(){
        var price = this.temp_model.get('unit_price');
        if (typeof price.replace == 'function'){
            price = price.replace(',','');
        }

        price = new Decimal(price);
        var max_discount = new Decimal(this.temp_model.get('max_discount'));
        var full_discount = new Decimal(100);

        max_discount = full_discount.minus(max_discount).dividedBy(100).toDP(2);
        price = price.times(max_discount).toDP(2).toNumber();

        this.$el.find('.item-kit-minimum-price').text( '= '+accounting.formatMoney(price) );
        return false;
    },

	adjustTotal: function(){
		
		var total = new Decimal(0);
		var cost = new Decimal(0);
		var self = this;
		_.each(this.model.get('items'), function(item, index){
			item.total = self.calculateItemTotal(item.quantity, item.unit_price);
			item.total_cost = self.calculateItemTotal(item.quantity, item.cost_price);
			
			total = total.plus(item.total);
			cost = cost.plus(item.total_cost);
		});

		this.model.set({
			'unit_price': total.toDP(2).toNumber(),
			'cost_price': cost.toDP(2).toNumber()
		});
	},

	setModel: function(){
		var view = this;

		var data = {
			item_kit_number: view.$el.find('#item-kit-upc').val(),
			name: view.$el.find('#item-kit-name').val(),
			department: view.$el.find('#item-kit-department').val(),
			category: view.$el.find('#item-kit-category').val(),
			subcategory: view.$el.find('#item-kit-subcategory').val(),
			unit_price: view.$el.find('#item-kit-price').val(),
			cost_price: view.$el.find('#item-kit-cost').val(),
			is_punch_card: view.$el.find('#item-kit-punch-card').is(':checked'),
			description: view.$el.find('#item-kit-description').val(),
            max_discount: view.$el.find('#item-kit-max-discount').val(),
			items: this.model.get('items')
		};

		if(data.is_punch_card){
			data.item_type = 'punch_card';
 		}else{
 			data.item_type = 'item_kit';
 		}

		this.$el.loadMask();
		this.model.set(data);
	},

	save: function(e){
		
		e.preventDefault();
		var view = this;

		// Validate that required fields are filled in
		var form = this.$el.find('form');
		form.bootstrapValidator('validate');

		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}

		if(!this.model.get('items') || this.model.get('items').length == 0){
			App.vent.trigger('notification', {'msg': 'At least 1 item is required in item kit', 'type': 'error'});
			return false;
		}

		this.setModel();

		if(this.model.isNew()){
			this.collection.add(this.model);
		}

		this.model.save(null, {wait: true,
			success: function(){
				view.hide();
			},
			error: function(){
				$.loadMask.hide();
			}
		});			

		return false;
	}
});
