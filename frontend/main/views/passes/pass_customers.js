var Pass_customer_view = Backbone.Marionette.ItemView.extend({
	tagName: 'div',
	className: 'row',
	template: JST['passes/pass_customer.html'],

	events: {
		'click button.remove': 'remove_customer'
	},

	remove_customer: function(){
		if(this.model.collection){
			this.model.collection.remove(this.model);
		}
		return false;
	}
});

var Pass_customer_empty_view = Backbone.Marionette.ItemView.extend({
	tagName: 'div',
	className: 'row',
	template: function(){
		return '<div class="col-md-12"><h4 class="text-muted">No extra customers assigned</h4></div>';
	}
});

var Pass_customer_collection_view = Backbone.Marionette.CollectionView.extend({
	childView: Pass_customer_view,
	emptyView: Pass_customer_empty_view,
	tagName: "div",
	className: "col-md-12"
});