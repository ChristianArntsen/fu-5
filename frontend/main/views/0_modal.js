var ModalView  = Backbone.View.extend({
	
	id: 'modal',
	className: 'modal-dialog',

	initialize: function(params){

		var view = this;
		if(params && params.extraClass){
			this.extraClass = params.extraClass;
		}

		this.closable = true;
		if(params && params.closable !== undefined){
			this.closable = params.closable;
		}

		if(params && params.backdropOpacity){
			this.backdropOpacity = params.backdropOpacity;
		}	
		var modal_id = '#modal-'+ view.id;
		
		// When the modal is hidden, destroy the view
		$(document).on('hidden.bs.modal', modal_id, {view: view}, function(event){
			
			var view = event.data.view;
			var modal_id = '#modal-'+view.id;

			// If modal being hidden is the one attached to this view, destroy it
			if($(event.target).find(view.$el).length > 0){
				$(document).off('hidden.bs.modal', modal_id);
				$(modal_id).remove();
				
				view.trigger('hide');
				
				if(view.onHide && typeof(view.onHide) == 'function'){
					view.onHide();
				}
				
				view.remove();
			}
		});
	},

    show: function(){
		this.render();
		var modal_id = 'modal-' + this.id;

		// Check if modal container has been created already, if not, create it
		if($('#' + modal_id).length == 0){
			$('body').append('<div id="'+modal_id+'" class="modal" role="dialog" aria-hidden="true"></div>');
		}
		
		var params = {
			keyboard: true,
			backdrop: true
		};
		
		if(this.closable !== undefined && this.closable === false){
			params.keyboard = false;
			params.backdrop = 'static';
		}
		
		if(this.extraClass){
			this.$el.attr('class', this.className +' '+ this.extraClass);
		}

		$('#' + modal_id).modal(params).html(this.el);
		if(this.backdropOpacity){
			$('div.modal-backdrop').css('opacity', this.backdropOpacity);
		}
		
		if(typeof(this.onShow) == 'function'){
			this.onShow();
		}
		this.trigger('show');
	},

	hide: function(){

		if(this.onHide && typeof(this.onHide) == 'function'){
			this.onHide();
		}
		var modal_id = '#modal-' + this.id;
		$(modal_id).modal('hide');
	}
});
