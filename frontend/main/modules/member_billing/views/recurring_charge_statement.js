var RecurringChargeStatementView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    className: 'col-md-12',
    template: JST['member_billing/templates/recurring_charge_statement.html'],

    regions: {
        'scheduleSettings': '#statement-schedule'
    },

    initialize: function(){

    },

    events: {
        'click #preview-statement': 'preview',
        'blur #statement-charge-days': 'validateInt',
        'blur #statement-due-days': 'validateInt',
        'change #statement-enable-autopay': 'toggleAutopay',
        'change #statement-finance-charge-enabled': 'toggleFinanceCharge',
        'change select.statement-month-day': 'setMonthDay',
        'change select.statement-period': 'setPeriod',
        'change #statement-include-member-transactions': 'includeMemberTransactions',
        'change #statement-include-customer-transactions': 'includeCustomerTransactions'
    },

    validateInt: function(e){
        var input = $(e.currentTarget);
        var value = parseInt(input.val());

        if(value < 0 || value !== 0 && !value){
            input.val(0);
            return true;
        }
        return true;
    },

    includeMemberTransactions: function(){
        var includeMemberTransactions = this.$el.find('#statement-include-member-transactions').is(':checked');
        if(includeMemberTransactions){
            this.$el.find('#statement-pay-member-balance').attr('disabled', null);
        }else{
            this.$el.find('#statement-pay-member-balance').attr('checked', false).attr('disabled', 'disabled');
        }
    },

    includeCustomerTransactions: function(){
        var includeCustomerTransactions = this.$el.find('#statement-include-customer-transactions').is(':checked');
        if(includeCustomerTransactions){
            this.$el.find('#statement-pay-customer-balance').attr('disabled', null);
        }else{
            this.$el.find('#statement-pay-customer-balance').attr('checked', false).attr('disabled', 'disabled');
        }
    },

    onRender: function(){
        this.toggleAutopay();
        this.toggleFinanceCharge();

        var scheduleSettings = new RepeatableScheduleView({
            model: this.model.get('repeated'),
            showSaveButton: false,
            showTimeOfDay: false,
            className: ''
        });

        this.scheduleSettings.show(scheduleSettings);

        this.$el.find('.text-editor').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']]
            ],
            height: 100
        });

        this.$el.find('#statement-footer').summernote('code', this.model.get('footerText'));
        this.$el.find('#statement-terms-and-conditions').summernote('code', this.model.get('termsAndConditionsText'));
        this.$el.find('#statement-message').summernote('code', this.model.get('messageText'));

        this.includeMemberTransactions();
        this.includeCustomerTransactions();
    },

    toggleAutopay: function(){
        var autopay = this.$el.find('#statement-enable-autopay').is(':checked');
        if(autopay){
            this.$el.find('.autopay-settings input, .autopay-settings select').attr('disabled', null);
        }else{
            this.$el.find('.autopay-settings input, .autopay-settings select').attr('disabled', 'disabled');
            this.$el.find('#statement-charge-days').val(0);
            this.$el.find('#statement-charge-attempts').val(1);
        }
    },

    toggleFinanceCharge: function(){
        var financeCharge = this.$el.find('#statement-finance-charge-enabled').is(':checked');
        if(financeCharge){
            this.$el.find('.finance-charge-settings input, .finance-charge-settings select').attr('disabled', null);
        }else{
            this.$el.find('.finance-charge-settings input, .finance-charge-settings select').attr('disabled', 'disabled');
            this.$el.find('#statement-finance-charge-amount').val('');
            this.$el.find('#statement-finance-charge-after-days').val('');
        }
    },

    validate: function(){

        if(!this.model.get('repeated').isComplete()){
            App.vent.trigger('notification', {type: 'error', msg: 'Statement schedule must be completed to continue'});
            return false;
        }
        return true;
    },

    clearEmptyTextEditorData: function(content){
        if(content == '<p><br></p>'){
            return null;
        }
        return content;
    },

    getData: function(){

        var data = _.getFormData(this.$el);

        data.dueAfterDays = parseInt(_.getNumber(data.dueAfterDays, 0));
        data.includeCustomerTransactions = (data.includeCustomerTransactions == 1);
        data.includeMemberTransactions = (data.includeMemberTransactions == 1);
        data.includePastDue = (data.includePastDue == 1);
        data.sendEmail = (data.sendEmail == 1);
        data.sendZeroChargeStatement = (data.sendZeroChargeStatement == 1);
        //data.sendMail = (data.sendMail == 1);

        data.autopayAfterDays = parseInt(_.getNumber(data.autopayAfterDays, 0));
        data.autopayAttempts = parseInt(_.getNumber(data.autopayAttempts, 0));
        data.autopayEnabled = (data.autopayEnabled == 1);
        data.autopayOverdueBalance = (data.autopayOverdueBalance == 1);

        data.financeChargeEnabled = (data.financeChargeEnabled == 1);
        data.financeChargeAmount = _.getNumber(data.financeChargeAmount, 0);
        data.financeChargeAfterDays = parseInt(_.getNumber(data.financeChargeAfterDays, 0));

        data.footerText = this.clearEmptyTextEditorData(this.$el.find('#statement-footer').summernote('code'));
        data.termsAndConditionsText = this.clearEmptyTextEditorData(this.$el.find('#statement-terms-and-conditions').summernote('code'));
        data.messageText = this.clearEmptyTextEditorData(this.$el.find('#statement-message').summernote('code'));

        return data;
    }
});