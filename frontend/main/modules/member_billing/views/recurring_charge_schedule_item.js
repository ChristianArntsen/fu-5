var RecurringChargeScheduleItemDetailsView = Marionette.LayoutView.extend({

    tagName: 'div',
    className: 'recurring-schedule-item-details',
    template: JST['member_billing/templates/recurring_charge_schedule_item_details.html'],

    events: {
        'click .repeat-time-select': 'setTime'
    },

    regions: {
        'repeatable_visualization': '#repeatable-visualization',
        'repeatable_month_selector': '#repeatable-month-selector',
        'repeatable_period_selector': '#repeatable-period-selector',
        'repeatable_day_selector': '#repeatable-day-selector'
    },

    modelEvents: {
        'change:_schedule_visible': 'toggleVisibility'
    },

    initialize: function(){
        this.repeatable = this.model.get('repeated');
        this.listenTo(this.repeatable, 'change:period', this.render);
        this.listenTo(this.repeatable, 'change:_stepsCompleted', this.toggleSteps);
    },

    setTime: function(e){
        var button = $(e.currentTarget);
        this.$el.find('.repeat-time-select').removeClass('active');
        button.addClass('active');
        var value = button.data('value');

        if(value == 'start'){
            var data = {
                byhour: [0],
                byminute: [0],
                bysecond: [0]
            };
        }else{
            var data = {
                byhour: [23],
                byminute: [59],
                bysecond: [59]
            };
        }
        this.repeatable.set(data);
        return false;
    },

    toggleSteps: function(){
        var stepsCompleted = this.repeatable.get('_stepsCompleted');
        var steps = this.$el.find('div.schedule-step');

        $.each(steps, function(){
            if($(this).data('step') <= (stepsCompleted + 1)){
                $(this).css('display', 'block');
            }else{
                $(this).css('display', 'none');
            }
        });
    },

    onRender: function(){
        var repeatable = this.repeatable;
        var days = new Backbone.Collection();
        days.add(repeatable);

        this.repeatable_visualization.show( new RepeatableVisualizationView({collection: days}) );
        this.repeatable_month_selector.show( new RepeatableMonthSelectorView({model: repeatable}) );
        this.repeatable_period_selector.show( new RepeatablePeriodSelectorView({model: repeatable}) );
        this.repeatable_day_selector.show( new RepeatableDaySelectorView({model: repeatable}) );

        this.toggleVisibility();
        this.toggleSteps();
    },

    toggleVisibility: function(){
        if(!this.model.get('_schedule_visible')){
            this.$el.hide();
        }else{
            this.$el.show();
        }
    }
});

var RecurringChargeScheduleItemDetailsEmptyView = Marionette.ItemView.extend({
    template: function(){
        return '<div class="row line-item-empty text-muted"><div class="col-md-12">Use the search box above to add items or fees</div></div>';
    }
});

var RecurringChargeScheduleItemDetailsListView = Marionette.CollectionView.extend({

    tagName: 'div',
    events: {},
    childView: RecurringChargeScheduleItemDetailsView,
    emptyView: RecurringChargeScheduleItemDetailsEmptyView
});