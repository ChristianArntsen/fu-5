var StatementTableMenuCell = Backgrid.Cell.extend({

    template:JST['member_billing/templates/statement_row_menu.html'],

    initialize: function(){
        this.listenTo(this.model, 'view-statement', this.viewStatement);
    },

    events: {
        "click .view": "viewStatement",
        "click .delete": "deleteStatement",
        "click .print": "printStatement",
        "click .email": "emailStatement"
    },

    deleteStatement: function(e){
        if(e && e.preventDefault){
            e.preventDefault();
        }
        alert('TODO: Delete statement');
    },

    viewStatement: function(e){
        var self = this;
        if(e && e.preventDefault){
            e.preventDefault();
        }
        var statementId = this.model.get('id');

        var statement = new Statement({
            id: statementId
        });

        var statementModal = new StatementView({model: statement});
        statementModal.show();
        statementModal.$el.loadMask();

        statement.fetch({
            data: {
                include: ['customer', 'customerTransactions', 'memberTransactions', 'charges', 'payments']
            },
            success: function(model){

            }
        });
    },

    printStatement: function(e){
        alert('TODO: Print statement');
        return false;
    },

    emailStatement: function(e){
        alert('TODO: Email statement');
        return false;
    },

    render: function () {
        this.$el.html(this.template({}));
        return this;
    }
});

var StatementsTableView = Backbone.Marionette.ItemView.extend({

    template: JST['member_billing/templates/statements_table.html'],

    controls:{
        bulkDelete:function(){
            var result = confirm("Are you sure you want to delete of these statements?");
            if (result) {
                _.forEach(this.grid.getSelectedModels(),function(model){
                    model.destroy();
                })
            }
        }
    },

    behaviors: function(){
        return {
            TableBehavior: {
                behaviorClass: Marionette.TableBehavior,
                pageSettings:this.options.settings,
                controls:this.controls
            }
        }
    }
});