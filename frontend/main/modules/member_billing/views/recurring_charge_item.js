var RecurringChargeItemView = Marionette.ItemView.extend({

    tagName: 'tr',
    className: 'template-charge',
    template: JST['member_billing/templates/recurring_charge_item.html'],

    events: {
        'click .remove': 'remove_item',
        'click .edit': 'edit',
        'click': 'setActive',
        'click .set-charge-schedule': 'chargeSchedule',
        'hidden.bs.popover': 'onPopoverHide',
        'shown.bs.popover': 'onPopoverShown',
        'click .save-schedule': 'hideChargeSchedule',
        'blur input.item-quantity': 'setQuantity',
        'blur input.item-price': 'setPrice'
    },

    modelEvents: {
        'change:_active': 'highlight',
        'change:_error': 'highlightError'
    },

    setQuantity: function(){
        var quantity = _.getNumber(this.$el.find('input.item-quantity').val(), 1);
        this.model.set('quantity', quantity);
    },

    setPrice: function(){
        var price = _.getNumber(this.$el.find('input.item-price').val(), '');
        this.$el.find('input.item-price').val(price);
        if(price === ''){
            price = null;
        }
        this.model.set('override_price', price);
    },

    initialize: function(){
        this.popoverShown = false;
        this.listenTo(this.model.get('repeated'), 'change', this.setScheduleDescription);
        this.listenTo(this.model.get('item'), 'change:total', this.render);
    },

    setScheduleDescription: function(){
        this.$el.find('.set-charge-schedule').html(this.getScheduleDescription());
    },

    getScheduleDescription: function(){

        var repeatable = this.model.get('repeated');
        var periods = {
            'daily': 'day',
            'weekly': 'week',
            'monthly': 'month',
            'quarterly': 'quarter',
            'yearly': 'year'
        };

        if(typeof(repeatable.get('freq')) == 'undefined' || repeatable.get('freq') === null || repeatable.get('freq') === ''){
            return 'No schedule set';
        }

        var daysOfWeek = {
            0: 'Mon',
            1: 'Tue',
            2: 'Wed',
            3: 'Thu',
            4: 'Fri',
            5: 'Sat',
            6: 'Sun'
        };

        var text = 'Every ';
        var plural = false;

        if(repeatable.get('interval') > 1){
            text += repeatable.get('interval') + ' ';
            plural = true;
        }
        text += periods[repeatable.get('period')];

        if(plural) {
            text += 's';
        }

        switch(repeatable.get('period')){

            case 'daily':
                if(repeatable.get('byweekday') && repeatable.get('byweekday').length > 0){
                    text = 'Every ';

                    var dayList = [];
                    _.each(repeatable.get('byweekday'), function(weekday){
                        dayList.push( daysOfWeek[weekday] );
                    });

                    text += dayList.join(', ');
                }
            break;

            case 'weekly':
                if(repeatable.get('byweekday') && repeatable.get('byweekday').length > 0){
                    text += ' on ' + daysOfWeek[repeatable.get('byweekday')[0]];
                }
            break;

            case 'monthly':
                var months = repeatable.get('bymonth');

                if(!repeatable.get('bymonthday')){
                    return 'No schedule set';
                }
                var date = repeatable.get('bymonthday')[0];

                if(months && months.length > 0){
                    text = 'Every ';

                    var monthList = [];
                    _.each(months, function(month){
                        monthList.push(moment().month(month - 1).format('MMM'));
                    });
                    text += monthList.join(', ');
                }

                if(date == -1){
                    text += ' on the last day of the month';
                }else{
                    text += ' on the ' + moment().date(date).format('Do');
                }
            break;

            case 'quarterly':
                var date = repeatable.get('bymonthday')[0];
                var month = repeatable.get('bymonth')[0];
                text = 'Every quarter (';

                var monthList = [];
                _.each(repeatable.get('bymonth'), function(quarterMonth){
                    monthList.push(moment().month(quarterMonth - 1).format('MMM'));
                });
                text += monthList.join(', ');

                if(date == -1){
                    text += ') on the last day of the month';
                }else{
                    text += ') on the ' + moment().month(month - 1).date(date).format('Do');
                }
            break;

            case 'yearly':
                if(repeatable.get('bymonth') == null || repeatable.get('bymonthday') == null){
                    return 'No schedule set';
                }
                var month = repeatable.get('bymonth')[0];
                var date = repeatable.get('bymonthday')[0];
                text += ' on ' + moment().month(month - 1).date(date).format('MMM Do');
            break;
        }

        return text;
    },

    onRender: function(){

        this.$popover = this.$el.find('.set-charge-schedule');
        this.$popover.popover({
            trigger: 'manual',
            title: 'Charge Schedule',
            placement: 'bottom',
            viewport: {
                selector: 'body',
                padding: '30px'
            }
        });

        this.setScheduleDescription();
        if(this.model.get('_active')){
            this.$el.addClass('editing');
        }

        // Open the schedule edit window automatically when item is first added
        if(this.model.get('_new')){
            this.model.set('_new', false, {silent: true});
            _.delay(_.bind(this.chargeSchedule, this), 50);
        }
    },

    setActive: function(){
        this.model.set({
            '_active': true,
            '_error': false
        });
    },

    chargeSchedule: function(e){

        if(e){
            e.preventDefault();
        }
        this.model.set('_active', true);
        $('.set-charge-schedule').not(this.$popover).popover('hide');

        if(!this.popoverShown){
            this.repeatableView = new RepeatableScheduleView({model: this.model.get('repeated')});
            this.repeatableView.render();
            this.listenTo(this.repeatableView, 'close', this.hideChargeSchedule);

            this.$popover.popover('show');
            this.$popover.data('bs.popover').$tip.find('.popover-content').html( this.repeatableView.el );
        }else{
            this.$popover.popover('hide');
        }

        return false;
    },

    onPopoverShown: function(){
        this.popoverShown = true;
    },

    hideChargeSchedule: function(){
        if(this.$popover.data('bs.popover')){
            this.$popover.popover('hide');
        }
    },

    onPopoverHide: function(){
        this.popoverShown = false;
        if(this.repeatableView){
            this.repeatableView.destroy();
        }
    },

    highlight: function(){
        if(this.model.get('_active')){
            this.$el.addClass('editing');
        }else{
            this.$el.removeClass('editing');
        }
    },

    highlightError: function(){
        if(this.model.get('_error')){
            this.$el.addClass('error');
        }else{
            this.$el.removeClass('error');
        }
    },

    edit: function(){
        var view = this;
        var item = new Item({item_id: this.model.get('item_id'), item_type: false});
        var modal = new EditItemView({model: item, collection: App.data.items});
        modal.show();

        modal.$el.loadMask();
        item.fetch({
            success: function(){

                setTimeout(function(){
                    view.listenToOnce(item, 'sync', function(model){
                        var itemData = model.toJSON();
                        itemData = RecurringChargeItemListView.prototype.transformItem(itemData);
                        view.model.get('item').set(itemData);
                        view.model.get('item').calculateTotals();
                    });
                }, 100);
            }
        });

        return false;
    },

    remove_item: function(){
        this.model.collection.remove(this.model);
        return false;
    }
});

var RecurringChargeEmptyView = Marionette.ItemView.extend({
    tagName: 'tr',
    template: function(){
        return '<td colspan="8"><div class="line-item-empty text-muted">Use the search box above to add items or fees</div></td>';
    }
});

var RecurringChargeItemListView = Marionette.CompositeView.extend({

    template: JST['member_billing/templates/recurring_charge_item_list.html'],
    tagName: 'div',
    className: 'col-md-12',
    childViewContainer: 'tbody',
    childView: RecurringChargeItemView,
    emptyView: RecurringChargeEmptyView,

    events: {
        'click .new-item': 'newItem'
    },

    // We have to transform item attributes from underscore to camel case
    // because the item search API returns underscored properties, but the new REST API we work with
    // is camel case
    transformItem: function(itemObj){

        var transformed = {};
        _.each(itemObj, function(value, key){
            var camelCase = key.replace(/_([a-z])/g, function (g){ return g[1].toUpperCase(); });
            transformed[ camelCase ] = value;
        });

        return transformed;
    },

    onRender: function(){

        var collection = this.collection;
        var searchField = this.$el.find('input.item-search');
        var view = this;

        init_item_search(searchField, function(e, item){

            item.quantity = 1.00;

            var recurringChargeItem = {
                item_id: item.item_id,
                item_type: 'item',
                item: view.transformItem(item),
                _active: true,
                _new: true
            };

            collection.push(recurringChargeItem);
            searchField.typeahead('val', '');

            return false;

        }, {
            show_add_button: true,
            item_type: ['item']
        });
    },

    newItem: function(){

        var view = this;
        var collection = this.collection;
        var item = new Item();
        var modal = new EditItemView({model: item, collection: App.data.items});
        modal.show();

        view.listenToOnce(item, 'sync', function(model){
            var itemData = model.toJSON();
            itemData = view.transformItem(itemData);

            var recurringChargeItem = {
                item_id: itemData.itemId,
                item_type: 'item',
                item: itemData,
                _active: true
            };

            var item = collection.push(recurringChargeItem);
        });
    }
});