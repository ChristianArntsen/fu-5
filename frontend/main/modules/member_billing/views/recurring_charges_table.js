Backgrid.RecurringCharges = {};

Backgrid.RecurringCharges.Row = Backgrid.Row.extend({

    events: {
        click: function(e){
            if(e.target.nodeName != 'INPUT' && (e.target.nodeName != 'BUTTON' && e.target.parentNode.nodeName != 'BUTTON')){
                e.preventDefault();
                var modal = new RecurringChargeEditView({model: this.model});
                modal.show();
            }
        }
    }
});

var RecurringChargesTableMenuCell = Backgrid.Cell.extend({

    template:JST['member_billing/templates/recurring_charge_row_menu.html'],
    events: {
        "click .view": "viewTemplate",
        "click .delete": "deleteTemplate"
    },

    deleteTemplate: function(e){
        e.preventDefault();
        this.model.destroy();
        return false;
    },

    viewTemplate: function(e){
        e.preventDefault();
        var modal = new RecurringChargeEditView({model: this.model});
        modal.show();
    },

    render: function () {
        this.$el.html(this.template({}));
        return this;
    }
});

var RecurringChargesTableView = Backbone.Marionette.ItemView.extend({

    template: JST['member_billing/templates/recurring_charges_table.html'],

    controls: {
        bulkDelete:function(){
            var result = confirm("Are you sure you want to delete these templates?");
            if (result) {
                _.forEach(this.grid.getSelectedModels(),function(model){
                    model.destroy();
                })
            }
        }
    },

    behaviors: function(){
        return {
            TableBehavior: {
                behaviorClass: Marionette.TableBehavior,
                pageSettings:this.options.settings,
                controls:this.controls,
                row: Backgrid.RecurringCharges.Row
            }
        }
    }
});