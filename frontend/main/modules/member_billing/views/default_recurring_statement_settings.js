var DefaultRecurringStatementSettingsView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['member_billing/templates/default_recurring_statement_settings.html'],

    initialize: function(){
        this.listenTo(this.model, 'request', this.showLoading);
        this.listenTo(this.model, 'sync', this.hide);
        this.listenTo(this.model, 'error invalid sync', this.hideLoading);
        this.listenTo(App.vent, 'save-settings', this.saveSettings);

        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right', closable: false});
    },

    showLoading: function(){
        this.$el.loadMask();
    },

    hideLoading: function(){
        $.loadMask.hide();
    },

    events: {
        "click #save-settings": "saveSettings"
    },

    regions: {
        statementSettings: '#statement-settings'
    },

    onRender: function(){
        var recurringStatementSettings = new RecurringChargeStatementView({model: this.model});
        this.statementSettings.show(recurringStatementSettings);

        if (!this.options.modal) {
            this.$el.find('#modal').prevObject[0].className = '';
            this.$el.find('.modal-content').removeClass('modal-content');
            this.$el.find('.modal-footer').hide();
        }
    },

    saveSettings: function(){

        if(!this.statementSettings.currentView.validate()){
            return false;
        }
        var statementData = this.statementSettings.currentView.getData();
        statementData.isDefault = true;

        this.model.set(statementData);
        this.model.save();
    }
});