var BillingLayout = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    className: 'row new-page',
    template: JST['member_billing/templates/billing.layout.html'],

    ui: {
        "advancedSearchButton": "#advanced-search-button",
        "pageDropdownAdvancedSearch": "#page-dropdown-advanced-search",
        "advancedSearchDropdown": "#advanced-search-dropdown",
        "pageSettingsButton": "#page-dropdown-settings",
        "newCustomerButton": ".new",
        "newTemplate": "button.new-template",
        "statementSettings": "#statement-settings"
    },

    events: {
        "click  @ui.advancedSearchButton": "executeSearch",
        "click  @ui.statementSettings": "statementSettings",
        "click  @ui.pageDropdownAdvancedSearch": "toggleAdvancedSearch",
        "click  @ui.manageCustomerGroupsButton": "manageGroups",
        "click  @ui.pageSettingsButton": "pageSettings",
        "click  @ui.newTemplate": 'newBillingTemplate'
    },

    regions: {
        statementsTable: '#statements-table',
        recurringChargesTable: '#recurring-charges-table',
        advancedSearch: '#advanced-search',
        quickSearch:'#quick-search',
        mobileSearch: '#mobile-search',
        mobileList: '#mobile-list'
    },

    statements: [],
    recurringCharges: [],

    onDestroy:function(){
        $("#page-iframe-container").show();
    },

    initialize: function (options) {

        var self = this;
        if(options && options.statements){
            this.statements = options.statements;
        }
        if(options && options.recurringCharges){
            this.recurringCharges = options.recurringCharges;
        }
        if(options && options.recurringStatements){
            this.recurringStatements = options.recurringStatements;
        }

        this.listenTo(this.statements, 'reset', function(){
            var total = this.statements.state.totalRecords;
            if(!total){
                total = 0;
            }
            self.$el.find('#total-statements').html('(' + total + ')');
        });
        this.listenTo(this.recurringCharges, 'reset', function(){
            var total = this.recurringCharges.state.totalRecords;
            if(!total){
                total = 0;
            }
            self.$el.find('#total-templates').html('(' + total + ')');
        });

        this.settings = App.data.course.get('billing_page_settings');

        this.settingsPromise = this.settings.fetch();

        this.settings.possibleColumns = new PageSettingsPageColumnCollection([
            {
                headerCell: "select-all",
                name: "select",
                cell: "select-row"
            }, {
                name: "number",
                label: "Number",
                editable: false,
                sortable: true,
                cell: Backgrid.StringCell,
                sortType: "toggle"
            }, {
                name: "customer",
                label: "Customer",
                editable: false,
                sortable: false,
                cell: Backgrid.Cell.extend({
                    render: function() {
                        this.$el.empty();
                        var customer = this.model.get('customer');
                        if(customer && customer.get('contact_info')){
                            this.$el.html( customer.get('contact_info').first_name +' '+ customer.get('contact_info').last_name );
                        }else{
                            this.$el.html('<span class="text-muted">N/A</span>');
                        }
                        return this;
                    }
                }),
                sortType: "toggle"
            }, {
                name: "dateCreated",
                label: "Created On",
                editable: false,
                sortable: true,
                cell: Backgrid.Extension.MomentCell.extend({
                    displayFormat: "MM/DD/YY"
                }),
                sortType: "toggle"
            },{
                name: "repeats",
                label: "Repeats",
                editable: false,
                sortable: false,
                cell: Backgrid.Cell.extend({
                    render: function(){
                        var recurringStatement = this.model.get('accountRecurringStatement');
                        if(recurringStatement && recurringStatement.get('repeated')){
                            this.$el.html( _.capitalize(recurringStatement.get('repeated').get('period')) );
                        }else{
                            this.$el.html('<span class="text-muted">N/A</span>');
                        }
                        return this;
                    }
                }),
                sortType: "toggle"
            },{
                name: "nextOccurence",
                label: "Next Occurence",
                editable: false,
                sortable: true,
                cell: Backgrid.Cell.extend({
                    render: function(){
                        var recurringStatement = this.model.get('accountRecurringStatement');
                        if(recurringStatement && recurringStatement.get('repeated') && recurringStatement.get('repeated').get('next_occurence')){
                            this.$el.html( moment(recurringStatement.get('repeated').get('next_occurence')).format('MM/DD/YY') );
                        }else{
                            this.$el.html('<span class="text-muted">N/A</span>');
                        }
                        return this;
                    }
                }),
                sortType: "toggle"
            },{
                name: "dueDate",
                label: "Due On",
                editable: false,
                sortable: true,
                cell: Backgrid.Extension.MomentCell.extend({
                    displayFormat: "MM/DD/YY"
                }),
                sortType: "toggle"
            },{
                name: "total",
                label: "Total",
                editable: false,
                sortable: true,
                cell: Backgrid.NumberCell,
                sortType: "toggle"
            },{
                name: "totalPaid",
                label: "Total Paid",
                editable: false,
                sortable: true,
                cell: Backgrid.NumberCell,
                sortType: "toggle"
            },{
                name: "totalOpen",
                label: "Total Due",
                editable: false,
                sortable: true,
                cell: Backgrid.NumberCell,
                sortType: "toggle"
            },{
                name: "pay",
                label: "Pay",
                editable: false,
                sortable: false,
                cell: Backgrid.Cell.extend({
                    className: 'statement-pay-cell',
                    events: {
                        "click .view-statement": function(){
                            this.model.trigger('view-statement');
                            return false;
                        },
                        "click .pay-statement": function(){

                            var view = this;
                            var button = this.$el.find('button');
                            button.button('loading');
                            var searchStr = 'STA '+this.model.get('number');

                            // Clear out anything left in cart
                            App.data.cart.cancel(function(){

                                // Search retrieve statement details
                                $.get(API_URL + '/cart/search', {q: searchStr}, function(response){

                                    // Add statement to cart and open POS
                                    if(response && response[0]){
                                        App.data.cart.addItem(response[0]);
                                        window.location = SITE_URL + '/v2/home#sales';
                                    }
                                    button.button('reset');
                                });
                            });
                            return false;
                        }
                    },
                    render: function() {
                        if(this.model.get('totalOpen') > 0){
                            this.$el.html('<button class="btn btn-primary btn-sm pay-statement" data-loading-text="LOADING <span class=\'fa fa-spinner fa-spin\'></span>">PAY NOW</button>' );
                        }else{
                            this.$el.html('<button class="btn btn-primary btn-sm view-statement">VIEW</button>');
                        }
                        return this;
                    }
                })
            },{
                name: "actions",
                label: "Actions",
                editable: false,
                sortable: false,
                cell: StatementTableMenuCell
            }
        ]);

        self.statements.state.pageSize = self.settings.get("page_size");

        this.listenTo(this.statements.advancedSearch.get("allowed_fields"), "sync", function(){
            this.renderAdvancedSearch();
        }, this);
        this.advancedSearchShown = false;

        this.listenTo(this.statements.advancedSearch, "change:q", this.executeSearch);

        // Fetch the default recurring statement settings
        this.recurringStatements.fetch({
            data: {
                isDefault: 1
            }
        });
    },

    toggleAdvancedSearch:function(){
        if(this.advancedSearchShown){
            this.ui.advancedSearchDropdown.hide();
        } else {
            this.ui.advancedSearchDropdown.show();
        }
        this.advancedSearchShown = !this.advancedSearchShown;
    },

    pageSettings:function(e){
        var settingsModal = new BillingPageSettingsView({model: this.settings, modal:true});
        settingsModal.show();
    },

    renderAdvancedSearch: function(){

        var collection = this.statements;

        if(collection.advancedSearch.get("allowed_fields").length > 0){
            var advSearchView = new AdvancedSearchCollectionView({
                model: collection.advancedSearch
            });
            advSearchView.on("childview:execute:search",function(){
                this.executeSearch();
            },this);
            this.advancedSearch.show(advSearchView);
        }
    },

    getDefaultRecurringStatement: function(){
        var defaultRecurringStatement = this.recurringStatements.findWhere({isDefault: true});
        if(!defaultRecurringStatement){
            defaultRecurringStatement = new RecurringStatement({isDefault: true});
            this.recurringStatements.add(defaultRecurringStatement);
        }
        return defaultRecurringStatement;
    },

    newBillingTemplate: function(){
        var recurringCharge = new RecurringCharge();
        var defaultRecurringStatement = this.getDefaultRecurringStatement();
        var recurringChargeEdit = new RecurringChargeEditView({
            model: recurringCharge,
            defaultRecurringStatement: defaultRecurringStatement
        });
        recurringChargeEdit.show();
        return false;
    },

    executeSearch:function(e){
        var searchParams = this.statements.getFilterParams();
        this.statements.searchParams = searchParams;
        this.statements.fetch();
    },

    statementSettings: function(){

        var defaultRecurringStatement = this.getDefaultRecurringStatement();
        var modal = new DefaultRecurringStatementSettingsView({model: defaultRecurringStatement, modal:true});
        modal.show();
        return false;
    },

    renderSearch: function(){
        this.quickSearch.show(new BasicSearchView({
            model: this.statements.advancedSearch,
            placeholder: 'Search statements...'
        }));
    },

    onRender:function(){

        $("#page-iframe-container").hide();

        this.renderAdvancedSearch();
        this.renderSearch();

        this.renderStatementsTable();
        this.renderRecurringChargesTable();
    },

    renderStatementsTable: function(){
        var statementsTable = new StatementsTableView({
            collection: this.statements,
            settings: this.settings
        });
        this.statementsTable.show(statementsTable);
        this.statements.fetch({reset: true});
    },

    renderRecurringChargesTable: function(){

        var recurringCharges = this.recurringCharges;
        var settings = App.data.course.get('recurring_charges_page_settings');

        settings.possibleColumns = new PageSettingsPageColumnCollection([
            {
                headerCell: "select-all",
                name: "select",
                cell: "select-row"
            }, {
                name: "name",
                label: "Name",
                editable: false,
                sortable: false,
                cell: Backgrid.StringCell,
                sortType: "toggle"
            }, {
                name: "date_created",
                label: "Created On",
                editable: false,
                sortable: false,
                cell: Backgrid.Extension.MomentCell.extend({
                    displayFormat: "MM/DD/YY"
                }),
                sortType: "toggle"
            }, {
                name: "statementSchedule",
                label: "Statement Schedule",
                editable: false,
                sortable: false,
                cell: Backgrid.Cell.extend({
                    render: function() {
                        var frequency = this.model.get('recurringStatement').get('repeated').get('period');
                        this.$el.html(_.capitalize(frequency));
                        return this;
                    }
                }),
                sortType: "toggle"
            }, {
                name: "actions",
                label: "Actions",
                editable: false,
                sortable: false,
                cell: RecurringChargesTableMenuCell
            }
        ]);

        var recurringChargesTable = new RecurringChargesTableView({
            collection: recurringCharges,
            settings: settings
        });
        this.recurringChargesTable.show(recurringChargesTable);
        recurringCharges.fetch({
            data: {
                include: ['items', 'recurringStatement']
            },
            reset: true
        });
    }
});