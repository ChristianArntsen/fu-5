var RepeatableMonthlyView = Backbone.Marionette.ItemView.extend({

    tagName: "div",
    className: "col-md-12",
    template: JST['member_billing/templates/repeatable/monthly.html'],

    events: {
        'click .repeat-month': 'setMonth',
        'click .repeat-day-select': 'setDay'
    },

    modelEvents: {
        'change:interval': 'clearDaySelector',
        'change:bymonth': 'clearInterval'
    },

    onRender: function(){
        this.toggleEvery();
    },

    toggleEvery: function(){
        if(this.model.get('interval') == null){
            this.$el.find('#repeat-interval').addClass('subdued');
            this.$el.find('.repeat-month').removeClass('subdued');
        }else{
            this.$el.find('#repeat-interval').removeClass('subdued');
            this.$el.find('.repeat-month').addClass('subdued');
        }
    },

    clearInterval: function(){
        if(this.model.get('interval') == null){
            this.$el.find('#repeat-interval').val('');
        }
        this.toggleEvery();
    },

    clearDaySelector: function(){
        if(this.model.get('interval')){
            this.$el.find('button.repeat-month').removeClass('active');
        }
        this.toggleEvery();
    },

    setDay: function(e){
        var button = $(e.currentTarget);
        button.siblings().removeClass('active');
        button.addClass('active');
        var day = parseInt(button.data('value'));
        var data = {
            bymonthday: [day],
            byweekday: [],
            bysetpos: [1]
        };
        this.model.set(data);
    },

    setMonth: function(e){

        var button = $(e.currentTarget);
        var isActive = button.hasClass('active');
        var repeatable = this.model;
        var data = {};
        var month = parseInt(button.data('value'));
        var period = repeatable.get('period');

        var bymonth = [];
        if(repeatable.get('bymonth')){
            bymonth = _.clone(repeatable.get('bymonth'));
        }

        if(!isActive){
            bymonth.push(month);
            button.addClass('active');

        // Do not allow removing ALL months
        }else if(bymonth.length > 1){
            var month_index = bymonth.indexOf(month);
            bymonth.splice(month_index, 1);
            button.removeClass('active');
        }
        data.bymonth = bymonth;

        data.interval = null;
        repeatable.set(data);
    },

    behaviors: function() {
        return {
            RepeatableEffectiveDates: {
                behaviorClass: RepeatableEffectiveDatesBehavior
            },
            RepeatableInterval: {
                behaviorClass: RepeatableIntervalBehavior
            }
        }
    }
});