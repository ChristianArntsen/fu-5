var RepeatableDailyView = Backbone.Marionette.ItemView.extend({

    tagName: "div",
    className: "col-md-12",
    template: JST['member_billing/templates/repeatable/daily.html'],

    events: {
        'click .repeat-day': 'selectDay',
        'blur #repeat-interval': 'setInterval'
    },

    modelEvents: {
        'change:interval': 'clearDaySelector',
        'change:byweekday': 'clearInterval'
    },

    onRender: function(){
        this.toggleEvery();
    },

    clearInterval: function(){
        if(this.model.get('interval') == null){
            this.$el.find('#repeat-interval').val('');
        }
        this.toggleEvery();
    },

    clearDaySelector: function(){
        if(this.model.get('interval')){
            this.$el.find('button.repeat-day').removeClass('active');
        }
        this.toggleEvery();
    },

    toggleEvery: function(){
        if(this.model.get('interval') == null){
            this.$el.find('#repeat-interval').addClass('subdued');
            this.$el.find('.repeat-day').removeClass('subdued');
        }else{
            this.$el.find('#repeat-interval').removeClass('subdued');
            this.$el.find('.repeat-day').addClass('subdued');
        }
    },

    selectDay: function(e){

        var button = $(e.currentTarget);
        var weekday = parseInt(button.data('value'));
        var weekdays = _.clone(this.model.get('byweekday'));

        if(!weekdays || weekdays.length == 0){
            weekdays = [];
        }

        if(button.hasClass('active')){
            button.removeClass('active');
            var index = weekdays.indexOf(weekday);
            if(index > -1){
                weekdays.splice(index, 1);
            }

        }else{
            button.addClass('active');
            weekdays.push(weekday);
        }

        var data = {
            freq: RRule.DAILY,
            interval: null,
            bymonthday: null,
            bymonth: null,
            bysetpos: null,
            byweekday: weekdays
        };

        this.model.set(data);
    },

    behaviors: function() {
        return {
            RepeatableEffectiveDates: {
                behaviorClass: RepeatableEffectiveDatesBehavior
            },
            RepeatableInterval: {
                behaviorClass: RepeatableIntervalBehavior
            }
        }
    }
});