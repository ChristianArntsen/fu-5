var RepeatableScheduleView = Backbone.Marionette.LayoutView.extend({

    tagName: "div",
    className: "container-fluid",
    template: JST['member_billing/templates/repeatable/schedule.html'],

    regions: {
        'settings': '#schedule-settings'
    },

    events: {
        'click button.repeat-time-select': 'setTime',
        'change #schedule-period': 'setPeriod'
    },

    initialize: function(options){

        this.showSaveButton = true;
        this.showTimeOfDay = true;
        if(options && typeof(options.showSaveButton) != 'undefined'){
            this.showSaveButton = options.showSaveButton;
        }
        if(options && typeof(options.showTimeOfDay) != 'undefined'){
            this.showTimeOfDay = options.showTimeOfDay;
        }
        this.listenTo(this.model, 'change:period', this.render);
    },

    serializeData: function(){
        var data = this.model.toJSON();
        data.showSaveButton = this.showSaveButton;
        data.showTimeOfDay = this.showTimeOfDay;
        return data;
    },

    onRender: function(){

        var views = {
            'yearly': RepeatableYearlyView,
            'quarterly': RepeatableQuarterlyView,
            'monthly': RepeatableMonthlyView,
            'weekly': RepeatableWeeklyView,
            'daily': RepeatableDailyView
        };

        if(!this.model.get('period')){
            var settingsView = new Marionette.ItemView({
                template: function(){
                    return '<div class="col-md-12 text-muted" style="text-align: center; padding: 30px 15px;">Select a schedule using the above menu</div>';
                }
            });

        }else{
            var settingsView = new views[this.model.get('period')]({model: this.model});
        }

        this.settings.show(settingsView);
    },

    setTime: function(e){

        var button = $(e.currentTarget);
        this.$el.find('.repeat-time-select').removeClass('active');
        button.addClass('active');
        var value = button.data('value');

        if(value == 'start'){
            var data = {
                byhour: [0],
                byminute: [0],
                bysecond: [0]
            };

        }else{
            var data = {
                byhour: [23],
                byminute: [59],
                bysecond: [59]
            };
        }

        this.model.set(data);
        return false;
    },

    setPeriod: function (e) {

        var menu = $(e.currentTarget);
        var repeatable = this.model;
        var period = menu.val();
        var data = {};
        var interval = null;

        if (period == 'daily') {
            data.freq = RRule.DAILY;
            data.bymonth = null;
            data.bymonthday = null;
            data.byweekday = null;
            interval = 1;

        }else if (period == 'weekly') {
            data.freq = RRule.WEEKLY;
            data.bymonth = null;
            data.bymonthday = null;
            data.byweekday = [0];
            interval = 1;

        }else if (period == 'monthly'){
            data.freq = RRule.MONTHLY;
            data.bymonth = null;
            data.bymonthday = [1];
            data.byweekday = null;
            interval = 1;

        }else if(period == 'quarterly'){
            data.freq = RRule.MONTHLY;
            data.bymonth = [1,4,7,10];
            data.bymonthday = [1];
            data.byweekday = null;

        }else if(period == 'yearly'){
            data.freq = RRule.YEARLY;
            data.bymonth = [1];
            data.bymonthday = [1];
            data.byweekday = null;
            interval = 1;
        }

        data.bysetpos = null;
        data.period = period;
        data.interval = interval;

        if(!data.byhour || data.byhour.length == 0){
            data.byhour = [0];
            data.bysecond = [0];
            data.byminute = [0];
        }

        repeatable.set(data);

        return false;
    }
});