var RepeatableQuarterlyView = Backbone.Marionette.ItemView.extend({

    tagName: "div",
    className: "col-md-12",
    template: JST['member_billing/templates/repeatable/quarterly.html'],

    events: {
        'click .repeat-day-select': 'setDay',
        'click .repeat-month': 'setMonth'
    },

    setDay: function(e){
        var button = $(e.currentTarget);
        button.siblings().removeClass('active');
        button.addClass('active');
        var day = parseInt(button.data('value'));
        var data = {
            bymonthday: [day],
            byweekday: [],
            bysetpos: [1]
        };
        this.model.set(data);
    },

    setMonth: function(e){

        var view = this;
        var button = $(e.currentTarget);
        var repeatable = this.model;
        var data = {};
        var month = parseInt(button.data('value'));
        var period = repeatable.get('period');
        button.parents('.repeat-month-selector').find('button').removeClass('active');

        var jan = [1, 4, 7, 10];
        var feb = [2, 5, 8, 11];
        var mar = [3, 6, 9, 12];
        var months = [];

        if(jan.indexOf(month) !== -1){
            months = jan;
        }else if(feb.indexOf(month) !== -1){
            months = feb;
        }else if(mar.indexOf(month) !== -1){
            months = mar;
        }

        _.each(months, function(month){
            view.$el.find('button.repeat-month[data-value=' + month +']').addClass('active');
        });
        data.bymonth = months;

        data.interval = null;
        repeatable.set(data);
    },

    behaviors: function() {
        return {
            RepeatableEffectiveDates: {
                behaviorClass: RepeatableEffectiveDatesBehavior
            },
        }
    }
});