var RecurringChargeEditView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['member_billing/templates/recurring_charge_edit.html'],

    regions: {
        items: '#line-items',
        visualization: '#billing-visualization',
        statementSettings: '#charge-statement-settings'
    },

    initialize: function(options){
        this.listenTo(this.model, 'request', this.showLoading);
        this.listenTo(this.model, 'sync', this.hide);
        this.listenTo(this.model, 'error invalid', this.hideLoading);

        this.defaultRecurringStatement = false;
        if(options && options.defaultRecurringStatement){
            this.defaultRecurringStatement = options.defaultRecurringStatement;
        }

        if(!this.model.get('recurringStatement').get('id') && this.defaultRecurringStatement){
            var defaultData = this.defaultRecurringStatement.toJSON();
            delete defaultData.id;
            delete defaultData.repeated.id;
            defaultData.isDefault = false;
            defaultData.repeated.byhour = [0];
            defaultData.repeated.byminute = [0];
            defaultData.repeated.bysecond = [0];
            this.model.get('recurringStatement').set(defaultData);
        }

        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full', closable: false});
    },

    events: {
        'click #show-statement-settings': 'toggleStatementSettings',
        'click #close-statement-settings': 'toggleStatementSettings',
        'click #preview-statement': 'previewStatement',
        'click #save-recurring-charge': 'save'
    },

    showLoading: function(){
        this.$el.loadMask();
    },

    hideLoading: function(){
        $.loadMask.hide();
    },

    onRender: function(){
        this.statementSettings.show( new RecurringChargeStatementView({model: this.model.get('recurringStatement')}) );
        this.items.show( new RecurringChargeItemListView({collection: this.model.get('items')}) );
        this.visualization.show( new RepeatableVisualizationView({
            collection: this.model.get('items'),
            model: this.model.get('recurringStatement').get('repeated')
        }));
    },

    getData: function(){
        return {
            name: this.$el.find('#charge-name').val(),
            prorate_charges: this.$el.find('#prorate-charges').is(':checked')
        };
    },

    validate: function(){

        var fields = this.$el;
        fields.bootstrapValidator('validate');
        if(!fields.data('bootstrapValidator').isValid()){
            App.vent.trigger('notification', {msg: 'Template name is required', type: 'error'});
            return false;
        }

        if(this.model.get('items').length == 0){
            App.vent.trigger('notification', {type: 'error', msg: 'At least one item/charge is required'});
            return false;
        }

        var itemScheduleError = false;
        _.each(this.model.get('items').models, function(item){
            if(!item.get('repeated').isComplete()){
                itemScheduleError = true;
                item.set('_error', true);
            }
        });

        if(itemScheduleError){
            App.vent.trigger('notification', {type: 'error', msg: 'All item schedules must be completed'});
            return false;
        }

        return true;
    },

    save: function(){

        if(!this.validate()){
            return false;
        }
        if(!this.statementSettings.currentView.validate()){
            return false;
        }

        var recurringChargeData = this.getData();
        var statementData = this.statementSettings.currentView.getData();

        this.model.set(recurringChargeData);
        this.model.get('recurringStatement').set(statementData);
        this.model.get('recurringStatement').get('repeated').set({
            bysecond: [0],
            byminute: [0],
            byhour: [0]
        }, {silent: true});

        if(this.model.isNew()){
            App.data.recurring_charges.create(this.model);
        }else{
            this.model.save();
        }
    },

    previewStatement: function(){

        var settings = this.statementSettings.currentView.getData();
        var repeatable_view = new RepeatableVisualizationView({
            collection: this.model.get('items'),
            model: this.model.get('recurringStatement').get('repeated')
        });
        var recurring = repeatable_view.serializeData();

        if(!recurring.period.rrule || !recurring.period.rules){
            App.vent.trigger('notification', {msg: 'Statement settings must be completed before generating preview'});
            return false;
        }

        var statement_date = moment(recurring.period.rules[1]);
        var statement_start = moment(recurring.period.rules[0]);
        var statement_end = moment(recurring.period.rules[1]).subtract(1, 'day');
        var due_date = statement_end.clone().add( parseInt(settings.dueAfterDays) + 1, 'day');

        var customerContactInfo = {
            first_name: 'Tiger',
            last_name: 'Woods',
            address_1: '123 Fake St.',
            city: 'Orem',
            state: 'UT',
            zip: '12345'
        };
        var customer = {
            contact_info: customerContactInfo,
            account_number: '098765'
        };

        var charges = this.model.get('items').clone();

        var statementTotal = 0;
        var statementTotalTax = 0;
        var statementSubtotal = 0;

        _.each(charges.models, function(charge){

            var taxRate = 0;
            if(charge.get('item').get('taxes') && charge.get('item').get('taxes').length > 0){
                _.each(charge.get('item').get('taxes').models, function(tax) {
                    taxRate += tax.get('percent');
                });
            }

            charge.set({
                line: charge.get('line_number'),
                name: charge.get('item').get('name'),
                qty: charge.get('quantity'),
                unitPrice: charge.get('item').get('unitPrice'),
                subtotal: charge.get('item').get('subtotal'),
                total: charge.get('item').get('total'),
                tax: charge.get('item').get('tax'),
                taxPercentage: taxRate
            });

            statementTotal += charge.get('total');
            statementSubtotal += charge.get('subtotal');
            statementTotalTax += charge.get('tax');
        });

        var memberTransactions = [
            {
                trans_id: 1,
                sale_id: 1001,
                running_balance: -100,
                trans_amount: -100,
                trans_description: "(1) 18 Holes<br>(1) Soda<br>(1) Tees",
                trans_comment: "POS 1001",
                trans_date: moment(statement_start).add(1, 'days').toDate()
            },{
                trans_id: 2,
                sale_id: 1023,
                running_balance: -115.50,
                trans_amount: -15.50,
                trans_description: "(1) Lunch Special",
                trans_comment: "POS 1023",
                trans_date: moment(statement_start).add(4, 'days').toDate()
            },{
                trans_id: 3,
                sale_id: 1040,
                running_balance: -65.50,
                trans_amount: 50,
                trans_description: "Point of Sale 1040",
                trans_comment: "Balance Payment",
                trans_date: moment(statement_end).subtract(8, 'days').toDate()
            },{
                trans_id: 4,
                sale_id: null,
                running_balance: -25.00,
                trans_amount: 25,
                trans_description: "Accidently over charged for golf club",
                trans_comment: "Manual Adjustment",
                trans_date: moment(statement_end).subtract(3, 'days').toDate()
            },{
                trans_id: 5,
                sale_id: 1085,
                running_balance: -100,
                trans_amount: -75,
                trans_description: "(2) 9 Holes<br>(2) Sodas",
                trans_comment: "POS 1085",
                trans_date: moment(statement_end).subtract(1, 'days').toDate()
            }
        ];

        var customerTransactions = [
            {
                trans_id: 3,
                sale_id: 1040,
                running_balance: 100.00,
                trans_amount: 100.00,
                trans_description: "Tournament Winnnings",
                trans_comment: "Account adjustment",
                trans_date: moment(statement_end).subtract(12, 'days').toDate()
            },{
                trans_id: 4,
                sale_id: null,
                running_balance: 30.01,
                trans_amount: -69.99,
                trans_description: "(1) Nike Method Core X Putter",
                trans_comment: "POS 1102",
                trans_date: moment(statement_end).subtract(12, 'days').toDate()
            },{
                trans_id: 5,
                sale_id: 1085,
                running_balance: 15.01,
                trans_amount: -15,
                trans_description: "(1) Sleeve of Balls<br>(1) Tees",
                trans_comment: "POS 1085",
                trans_date: moment(statement_end).subtract(3, 'days').toDate()
            }
        ];

        var statement = new Statement({
            number: '12345',
            dateCreated: statement_date,
            startDate: statement_start,
            endDate: statement_end,
            dueDate: due_date,
            customer: customer,
            charges: charges,
            autopayEnabled: (settings.autopayEnabled == 1),
            includeMemberTransactions: (settings.includeMemberTransactions == 1),
            memberTransactions: memberTransactions,
            includeCustomerTransactions: (settings.includeCustomerTransactions == 1),
            customerTransactions: customerTransactions,
            total: statementTotal,
            totalTax: statementTotalTax,
            totalOpen: statementTotal,
            termsAndConditionsText: settings.termsAndConditionsText,
            footerText: settings.footerText,
            messageText: settings.messageText
        });

        var preview = new StatementView({model: statement});
        preview.show();

        return false;
    }
});