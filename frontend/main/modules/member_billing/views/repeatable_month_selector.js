var RepeatableMonthSelectorView = Marionette.ItemView.extend({

    tagName: 'div',
    className: 'row',
    template: JST['member_billing/templates/repeatable_month_selector.html'],

    events: {
        'click button.repeat-month': 'setMonth',
        'blur #repeat-interval': 'setRepeatInterval',
        'click .repeat-interval-container': 'toggleRepeatInterval',
        'click .repeat-month-selector': 'toggleRepeatMonth',
        'blur #repeat-start': 'setStartDate'
    },

    initialize: function(){
        this.listenTo(this.model, 'change:bymonth change:period', this.render);
    },

    onRender: function(){
        if(this.model.get('interval')){
            this.toggleRepeatInterval();
        }else{
            this.toggleRepeatMonth();
        }

        this.$el.find('#repeat-start').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    },

    toggleRepeatInterval: function(){
        this.$el.find('.repeat-interval-container').removeClass('disabled');
        this.$el.find('.repeat-month-selector').addClass('disabled');
        return true;
    },

    toggleRepeatMonth: function(){
        this.$el.find('.repeat-interval-container').addClass('disabled');
        this.$el.find('.repeat-month-selector').removeClass('disabled');
        return true;
    },

    setStartDate: function(){
        var start = this.$el.find('#repeat-start').val();
        if(!start){
            this.model.set('dtstart', null);
            this.$el.find('#repeat-start').val('');
            return true;
        }

        var startDate = moment(start, 'MM/DD/YYYY').set({'hour': 0, 'minute': 0, 'second': 0});
        if(!startDate.isValid()){
            this.$el.find('#repeat-start').val('');
            return false;
        }

        this.model.set('dtstart', startDate.toDate());
    },

    setRepeatInterval: function(){
        var interval = parseInt(this.$el.find('#repeat-interval').val());
        var model = this.model;

        if(!interval || interval < 0){
            this.$el.find('#repeat-interval').val( this.model.get('interval') );
            return false;
        }
        var data = {
            interval: interval,
            bymonth: []
        };
        model.set(data);
        this.$el.find('button.repeat-month').removeClass('active');
    },

    setMonth: function(e){

        var view = this;
        var button = $(e.currentTarget);
        var isActive = button.hasClass('active');
        var repeatable = this.model;
        var data = {};
        var month = parseInt(button.data('value'));
        var period = repeatable.get('period');

        if(period == 'yearly' || period == 'quarterly'){
            button.parents('.repeat-month-selector').find('button').removeClass('active');
        }

        if(period == 'yearly'){
            data.bymonth = [month];
            button.addClass('active');

        }else if(period == 'monthly'){
            bymonth = [];
            if(repeatable.get('bymonth')){
                bymonth = _.clone(repeatable.get('bymonth'));
            }

            if(!isActive){
                bymonth.push(month);
                button.addClass('active');

                // Do not allow removing ALL months
            }else if(bymonth.length > 1){
                var month_index = bymonth.indexOf(month);
                bymonth.splice(month_index, 1);
                button.removeClass('active');
            }
            data.bymonth = bymonth;

        }else if(period == 'quarterly'){
            var jan = [1, 4, 7, 10];
            var feb = [2, 5, 8, 11];
            var mar = [3, 6, 9, 12];
            var months = [];

            if(jan.indexOf(month) !== -1){
                months = jan;
            }else if(feb.indexOf(month) !== -1){
                months = feb;
            }else if(mar.indexOf(month) !== -1){
                months = mar;
            }

            _.each(months, function(month){
                view.$el.find('button.repeat-month[data-value=' + month +']').addClass('active');
            });
            data.bymonth = months;
        }
        data.interval = null;
        repeatable.set(data);
    }
});