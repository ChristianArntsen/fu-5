var RecurringChargeScheduleView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    template: JST['member_billing/templates/recurring_charge_schedule.html'],

    regions: {
        'recurring_charge_tabs': 'div.recurring-schedule-tabs',
        'recurring_charge_details': 'div.recurring-schedule-details'
    },

    events: {
        'click .account-settings-tab': 'showAccountChargeSettings'
    },

    initialize: function(){
        var view = this;
        this.listenTo(this.model.get('items'), 'change:_schedule_visible', this.initItemDetails);
        this.listenTo(this.model.get('recurringStatement'), 'change:_account_charge_settings_complete', this.markAccountChargeSettings);
    },

    onRender: function(){

        var items = this.model.get('items');
        items.toggleVisible( items.at(0).cid );

        this.recurring_charge_tabs.show( new RecurringChargeScheduleItemListView({collection: items}) );
        this.recurring_charge_details.show( new RecurringChargeScheduleItemDetailsListView({collection: items}) );

        this.markAccountChargeSettings();
    },

    initItemDetails: function(){

        if(this.recurring_charge_details.currentView instanceof RecurringChargeScheduleItemDetailsListView){
            return true;
        }
        this.$el.find('.account-settings-tab').removeClass('active');
        var items = this.model.get('items');
        this.recurring_charge_details.show( new RecurringChargeScheduleItemDetailsListView({collection: items}) );
        return true;
    },

    showAccountChargeSettings: function(){

        this.model.get('items').toggleVisible();
        var statement = this.model.get('recurringStatement');
        this.recurring_charge_details.show( new RecurringChargeAccountSettingsView({model: statement}) );
        this.$el.find('.account-settings-tab').addClass('active');
        return false;
    },

    markAccountChargeSettings: function(){
        if(this.model.get('recurringStatement').get('_account_charge_settings_complete')){
            this.$el.find('.account-settings-tab').find('span.status-icon').replaceWith('<span class="status-icon success pull-left"><span class="fa fa-check"></span></span>');
        }else{
            this.$el.find('.account-settings-tab').find('span.status-icon').replaceWith('<span class="status-icon pull-left"></span>');
        }
    },

    validate: function(){

        var schedulesIncomplete = _.find(this.model.get('items').models, function(item){
            return(!item.get('repeated').isComplete());
        });

        if(schedulesIncomplete){
            App.vent.trigger('notification', {type: 'error', msg: 'All item schedules must be finished to continue'});
            return false;
        }

        if(!this.model.get('recurringStatement').get('_account_charge_settings_complete')){
            App.vent.trigger('notification', {type: 'error', msg: '"Account Charges" must be reviewed before continuing'});
            return false;
        }

        return true;
    },

    save: function(){
        if(this.model.isNew()){
            App.data.recurring_charges.create(this.model);
        }else{
            this.model.save();
        }
    }
});