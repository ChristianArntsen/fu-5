var StatementView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['member_billing/templates/statement.html'],
    id: 'statement-preview-modal',

    initialize: function(options){
        options.extraClass = 'modal-lg';
        ModalLayoutView.prototype.initialize.call(this, options);
    },

    modelEvents: {
        'sync': 'render'
    },

    events: {

    },

    onRender: function(){

        this.$el.find('.barcode').JsBarcode('STA ' + this.model.get('number'), {
            format: 'CODE128',
            displayValue: true,
            fontSize: 12,
            width: 1,
            height: 20
        });
    }
});