var Statement = Backbone.JsonRestModel.extend({

    urlRoot: REST_API_COURSE_URL + '/accountStatements',
    type: 'accountStatements',

    relations: {
        accountRecurringStatement: {
            type: 'accountRecurringStatements',
            many: false
        },
        customer: {
            type: 'customers',
            many: false
        }
    },

    defaults: {
        startDate: null,
        endDate: null,
        total: 0,
        totalOpen: 0,
        totalTax: 0,
        subtotal: 0,
        dueDate: null,
        dateCreated: null,
        totalPaid: 0,
        datePaid: null,
        number: null,
        memo: null,
        recurringStatement: {},
        customer: {},
        memberTransactions: [],
        customerTransactions: [],
        autopayEnabled:  false,
        charges: [],
        payments: [],
        includeMemberTransactions: false,
        includeCustomerTransactions: false,
        termsAndConditionsText: null,
        footerText: null,
        messageText: null,
        memberBalanceForward: 0,
        customerBalanceForward: 0
    }
});

var StatementCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/accountStatements',
    model: Statement,
    showDeleted: false,

    initialize: function(){
        this.advancedSearch = new AdvancedSearchModel({module: 'accountStatements'});
    },

    getFilterParams: function(){
        var params = this.advancedSearch.getFilterParams();
        if(this.showDeleted){
            params["deleted"] = "1";
        }
        params.include = ['items','accountRecurringStatement','customer'];

        return params;
    },

    fetch: function(options) {
        if(!options)
            options = {};

        if(!options.data){
            options.data = {};
            options.data.include = ['items','accountRecurringStatement','customer'];
        }

        if(this.searchParams){
            options.data = this.searchParams;
        }
        return Backbone.JsonRestCollection.prototype.fetch.call(this, options);
    }
});

Backbone.modelFactory.registerModel(Statement, StatementCollection);