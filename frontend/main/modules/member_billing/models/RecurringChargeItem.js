var RecurringChargeItem = Backbone.JsonRestModel.extend({

    type: 'accountRecurringChargeItems',
    relations: {
        item: {
            type: 'item',
            many: false
        },
        repeated: {
            type: 'repeated',
            many: false
        }
    },

    defaults: {
        name: '',
        quantity: 1,
        line_number: false,
        override_price: null,
        repeated: {},
        item: {},
        _schedule_completion: 'not_started',
        _active: false,
        _error: false
    },

    initialize: function(){

        this.listenTo(this.get('repeated'), 'change:period change:bymonth change:bymonthday change:interval change:bysetpos change:byhour', this.checkScheduleCompletion);

        this.on('change:quantity', function(model){
            model.get('item').set('qty', model.get('quantity'));
        });

        this.on('change:override_price', function(model){
            var price = model.get('override_price');
            if(model.get('override_price') === null){
                price = model.get('item').get('basePrice');
            }
            model.get('item').set('unitPrice', price);
        });

        this.checkScheduleCompletion();

        if(this.get('item')){
            var data = {
                'qty': this.get('quantity')
            };
            if(this.get('override_price') !== null){
                data.unitPrice = this.get('override_price');
            }
            this.get('item').set(data);
            this.get('item').calculateTotals();
        }
    },

    checkScheduleCompletion: function(){

        var repeatable = this.get('repeated');
        if(!repeatable){
            return false;
        }

        if(!repeatable.get('period')){
            this.set('_schedule_completion', 'not_started');
        }else if(repeatable.isComplete()){
            this.set('_schedule_completion', 'complete');
        }else{
            this.set('_schedule_completion', 'in_progress');
        }
    }
});

var RecurringChargeItemCollection = Backbone.JsonRestCollection.extend({

    model: RecurringChargeItem,
    comparator: 'line_number',

    initialize: function(){
        this.on('change:_active', this.setActive);
        this.on('add remove', this.setLineNumbers);
    },

    setActive: function(activeModel){

        if(!activeModel.get('_active')){
            return false;
        }
        _.each(this.models, function(model){
            if(activeModel.cid != model.cid){
                model.set('_active', false);
            }
        });
    },

    toggleVisible: function(id){
        _.each(this.models, function(model){
            model.set('_schedule_visible', (model.cid == id));
        });
    },

    setLineNumbers: function(model){

        if(model && model.get('_active')){
            this.setActive(model);
        }

        _.each(this.models, function(model, index){
             model.set('line_number', index + 1);
        });
    }
});

Backbone.modelFactory.registerModel(RecurringChargeItem, RecurringChargeItemCollection);