var RepeatableEffectiveDatesBehavior = Marionette.Behavior.extend({

    events: {
        "change .repeat-until-select": "setEndDate",
        "blur #repeat-until": "setEndDate",
        "click #repeat-until": "selectForever",
        "blur #repeat-start": "setStartDate"
    },

    onRender: function(){

        var self = this;
        var html = JST['member_billing/templates/repeatable/effective_dates.html'](this.view.model.toJSON());
        this.$el.find('#effective-dates').html(html);

        this.startPicker = this.$el.find('#repeat-start');
        this.endPicker = this.$el.find('#repeat-until');

        this.startPicker.datetimepicker({
            format: 'MM/DD/YYYY',
            useCurrent: false,
            minDate: moment()
        });
        this.endPicker.datetimepicker({
            format: 'MM/DD/YYYY',
            useCurrent: false,
            minDate: moment()
        });

        this.startPicker.on("dp.change", function (e) {
            self.endPicker.data("DateTimePicker").minDate(e.date);
        });
        this.endPicker.on("dp.change", function (e) {
            self.startPicker.data("DateTimePicker").maxDate(e.date);
        });
    },

    selectForever: function(){
        this.$el.find('#until-date').prop('checked', 'checked');
    },

    setEndDate: function(){

        var until;
        if(this.$el.find('#until-forever').is(':checked')){
            until = null;
            this.$el.find('#repeat-until').val('');

        }else{
            until = this.$el.find('#repeat-until').val();
            until = moment(until, 'MM/DD/YYYY').set({'hour': 23, 'minute': 59, 'second': 59});

            if(!until.isValid()){
                until = null;
                this.startPicker.data("DateTimePicker").maxDate(null);
                this.$el.find('#repeat-until').val('');
            }
        }

        this.view.model.set({'until': until});
    },

    setStartDate: function(){
        var start = this.$el.find('#repeat-start').val();
        if(!start){
            this.view.model.set('dtstart', null);
            this.endPicker.data("DateTimePicker").minDate(moment());
            this.$el.find('#repeat-start').val('');
            return true;
        }

        var startDate = moment(start, 'MM/DD/YYYY').set({'hour': 0, 'minute': 0, 'second': 0});
        if(!startDate.isValid()){
            this.view.model.set('dtstart', null);
            this.endPicker.data("DateTimePicker").minDate(moment());
            this.$el.find('#repeat-start').val('');
            return false;
        }

        this.view.model.set('dtstart', startDate.toDate());
    }
});