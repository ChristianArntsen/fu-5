var EditTeeSheetView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['settings/templates/tee_sheets/edit_tee_sheet_view.html'],

    regions: {
        'tabs': '.tee-sheet-tabs',
        'info': '.tee-sheet-info'
    },
    events: {
        'click .tab-selector-basic-settings':'loadBasicSettings',
        'click .tab-selector-marketing':'loadMarketing'
    },

    initialize: function () {
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
    },

    save: function () {

    },

    loadBasicSettings: function(e){
        this.selectTab('.tab-selector-basic-settings');
        this.info.show( new TeeSheetBasicSettingsView({model: this.model}) );
    },

    loadMarketing: function(e){
        this.selectTab('.tab-selector-marketing');
        this.info.show( new TeeSheetMarketingView({model: this.model}));
    },

    selectTab: function(tab){
        $('.selected-tab').removeClass('selected-tab');
        $(tab).parent('div').addClass('selected-tab');
    },

    renderPrinters: function () {
        var view = this;
        this.$el.find('.printer-list').html('');
        if (this.printers.length > 0) {
            _.each(this.printers.models, function (printer) {
                view.addPrinter(printer);
            });
        } else {
            var html = '<tr id="no-payment-types"><td colspan="2">No Printers</td></tr>';
            this.$el.find('.printer-list').append(html);
        }
    },

    renderPrinterGroups: function () {
        var view = this;
        this.$el.find('.printer-group-list').html('');
        if (this.printer_groups.length > 0) {
            _.each(this.printer_groups.models, function (printer_group) {
                view.addPrinterGroup(printer_group);
            });
        } else {
            var html = '<tr id="no-payment-types"><td colspan="2">No Printer Groups</td></tr>';
            this.$el.find('.printer-group-list').append(html);
        }
    },

    onRender: function () {
        // this.renderPrinters();
        // this.renderPrinterGroups();
        // this.updatePrinterDropdowns();
        this.tabs.show( new TeeSheetDataTabsView({model: this.model}) );
        this.loadBasicSettings();
    }
});

var TeeSheetDataTabsView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['settings/templates/tee_sheets/tabs.html'],

});