var TeeSheetListView = Backbone.Marionette.ItemView.extend({
    tagName: 'div',
    template: JST['settings/templates/tee_sheets/tee_sheet_list_view.html'],
    events:{
        'click .add-tee-sheet':'newTeeSheet'
    },
    initialize:function(){
        this.tee_sheets = App.data.course.get('teesheets');
    },
    onRender:function(){
        this.renderTeeSheets();
        this.$el.find('.colorPicker').colorpicker();
    },
    newTeeSheet:function(){
        var tee_sheet = new Teesheet();
        this.editTeeSheet(tee_sheet);
    },
    addTeeSheet:function(tee_sheet){
        this.$el.find('.tee-sheet-list').append(new TeeSheetRowView({model: tee_sheet}).render().el );
    },
    editTeeSheet:function(tee_sheet){
        var editTeeSheetView = new EditTeeSheetView({model:tee_sheet});
        editTeeSheetView.show();
    },
    renderTeeSheets:function(){
        var view = this;
        this.$el.find('.tee-sheet-list').html('');
        if (this.tee_sheets.length > 0) {
            _.each(this.tee_sheets.models, function(tee_sheet){
                view.addTeeSheet(tee_sheet);
            });
        } else {
            var html = '<tr><td><div class="col-md-3">'+
                '<img src="/images/sales/terminals-icon.png"/>'+
                '</div>'+
                '<div class="col-md-7 no-printers-box">'+
                '<h4>You currently do not have any terminals</h4>'+
                '<div class="form-inline marg-top-3">'+
                '<div class="btn-group form-group">'+
                '<button type="button" class="btn add-terminal btn-primary new settings-corner-button">Add Terminal</button>'+
                '</div>'+
                '</div>'+
                '</div></td></tr>';
            this.$el.find('.terminal-list').append(html);
        }
    }
});

var TeeSheetRowView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/tee_sheets/tee_sheet_row.html'],

    initialize: function () {

    },

    events: {
        'click .edit-tee-sheet': 'edit',
        'click .delete-tee-sheet':'remove'
    },
    edit:function(e){
        e.preventDefault();
        var editTeeSheetView = new EditTeeSheetView({model:this.model});
        editTeeSheetView.show();
    },
    remove:function(){

    }
});