var EditTerminalView = ModalLayoutView.extend({

    tagName: 'div',
    id: "edit-terminal",
    template: JST['settings/templates/sales/edit_terminal_view.html'],

    regions: {

    },
    events: {
        //'click .add-printer' : 'newPrinter',
        'click .manage-printers' : 'managePrinters',
        'click #save-changes' : 'save'
    },

    initialize: function () {
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
        this.printers = App.data.course.get('receipt_printers');
        this.printer_groups = App.data.course.get('receipt_printer_groups');
        this.listenTo(App.data.course.get('receipt_printers'),'change add remove',this.updatePrinterDropdowns);
        this.listenTo(App.data.course.get('receipt_printer_groups'),'add remove',this.updatePrinterGroupDropdowns);

    },

    save: function(){
        var data = _.getFormData(this.$el);
        var terminalFields = ['label','quickbuttonTab','UseRegisterLog','multiCashDrawers','autoPrintReceipts','webprnt','Https',
            'cashRegister','printTipLine','signatureSlipCount','creditCardReceiptCount','nonCreditCardReceiptCount',
            'autoEmailReceipt','autoEmailNoPrint','requireSignatureMemberPayments','afterSaleLoad'];
        data = _.pick(data, terminalFields);
debugger;
        console.log('------------------------------------- saveTerminal --------------------------------------------');
        console.dir(data);

        this.model.set(data);
        this.model.save();

        this.hide();
    },

    renderPrinterGroups:function(){
        var view = this;
        this.$el.find('.terminal-printer-group-list').html('');
        this.addPrinterGroup(new ReceiptPrinterGroup({'label':'Barcode Label Printer', 'defaultPrinterId':'1'}));
        this.addPrinterGroup(new ReceiptPrinterGroup({'label':'Default Sales Printer', 'defaultPrinterId':'1'}));
        this.addPrinterGroup(new ReceiptPrinterGroup({'label':'Default Kitchen Printer', 'defaultPrinterId':'1'}));
        if (this.printer_groups.length > 0) {
            _.each(this.printer_groups.models, function(printer_group){
                view.addPrinterGroup(printer_group);
            });
        }
    },

    updatePrinterDropdowns: function(){
        var printerDropdowns = this.$el.find('.receiptPrinterDropdown');
        var view = this;
        _.each(printerDropdowns, function (dropdown){
            var currentlySelected = $(dropdown).val();
            var name = $(dropdown).attr('name');
            var printers = view.printers.getDropdownOptions();
            var new_dropdown = _.dropdown(printers, {"name":name, "class":"form-control receiptPrinterDropdown"}, currentlySelected);
            $(dropdown).replaceWith(new_dropdown);
        });
    },

    addPrinterGroupDropdowns: function(){
        return;
        var printerDropdowns = this.$el.find('.receiptPrinterDropdown');
        _.each(printerDropdowns, function (dropdown){
            var currentlySelected = $(dropdown).val();
            var name = $(dropdown).attr('name');
            var printers = {'null':'Default'};
            _.each(
                App.data.course.get('receipt_printers').models,
                function(printer){
                    if (printer.get('label') != '')
                        printers[printer.get('id')] = printer.get('label')
                }
            );
            var new_dropdown = _.dropdown(printers, {"name":name, "class":"form-control receiptPrinterDropdown"}, currentlySelected);
            $(dropdown).replaceWith(new_dropdown);
        });
    },

    addPrinterGroup: function (printer_group) {
       this.$el.find('.terminal-printer-group-list').append(new TerminalPrinterGroupView({model: printer_group}).render().el );
    },

    onRender:function(){
        this.renderPrinterGroups();
        this.updatePrinterDropdowns();
    },
    managePrinters:function(){
        var managePrintersView = new ManagePrintersView();
        managePrintersView.show();
    }
});

var TerminalPrinterGroupView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/sales/terminal_printer_group.html'],

    initialize: function () {

    },

    events: {

    },

    updateLabel: function(){
    },

    removePrinterGroup: function(){
    },

    save: function () {

    },

    onRender: function(){
    }

});