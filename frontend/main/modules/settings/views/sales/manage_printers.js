var ManagePrintersView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['settings/templates/sales/manage_printers_view.html'],

    regions: {

    },
    events: {
        'click .add-printer' : 'newPrinter',
        'click .add-printer-group' : 'newPrinterGroup',
        'click #save-changes' : 'save'
    },

    initialize: function () {
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
        this.printers = App.data.course.get('receipt_printers');
        this.printer_groups = App.data.course.get('receipt_printer_groups');
        this.listenTo(App.data.course.get('receipt_printers'),'change add remove',this.updatePrinterDropdowns);
        this.listenTo(App.data.course.get('receipt_printer_groups'),'change add remove',this.updatePrinterGroupDropdowns);
    },

    save: function(){
        _.each(this.printer_groups.models, function(printer_group){
            printer_group.save();
        });
        _.each(this.printers.models, function(printer){
            printer.save();
        });
    },

    updatePrinterDropdowns: function(){
        var printerDropdowns = this.$el.find('.receiptPrinterDropdown');
        var view = this;
        _.each(printerDropdowns, function (dropdown){
            var currentlySelected = $(dropdown).val();
            var name = $(dropdown).attr('name');
            var printers = view.printers.getDropdownOptions();
            var new_dropdown = _.dropdown(printers, {"name":name, "class":"form-control receiptPrinterDropdown"}, currentlySelected);
            $(dropdown).replaceWith(new_dropdown);
        });
    },

    updatePrinterGroupDropdowns: function(){
        return;
        var printerDropdowns = this.$el.find('.receiptPrinterDropdown');
        _.each(printerDropdowns, function (dropdown){
            var currentlySelected = $(dropdown).val();
            var name = $(dropdown).attr('name');
            var printers = {'null':'Default'};
            _.each(
                App.data.course.get('receipt_printers').models,
                function(printer){
                    if (printer.get('label') != '')
                        printers[printer.get('id')] = printer.get('label')
                }
            );
            var new_dropdown = _.dropdown(printers, {"name":name, "class":"form-control receiptPrinterDropdown"}, currentlySelected);
            $(dropdown).replaceWith(new_dropdown);
        });
    },

    renderPrinters:function(){
        var view = this;
        this.$el.find('.printer-list').html('');
        if (this.printers.length > 0) {
            _.each(this.printers.models, function(printer){
                view.addPrinter(printer);
            });
        } else {
            var html = '<tr id="no-payment-types"><td colspan="2"><span class="sub">No Printers</span></td></tr>';
            this.$el.find('.printer-list').append(html);
        }
    },

    renderPrinterGroups:function(){
        var view = this;
        this.$el.find('.printer-group-list').html('');
        if (this.printer_groups.length > 0) {
            _.each(this.printer_groups.models, function(printer_group){
                view.addPrinterGroup(printer_group);
            });
        } else {
            var html = '<tr id="no-payment-types"><td colspan="2"><span class="sub">No Printer Groups</span></td></tr>';
            this.$el.find('.printer-group-list').append(html);
        }
    },

    onRender:function(){
        this.renderPrinters();
        this.renderPrinterGroups();
        this.updatePrinterDropdowns();
    },

    newPrinter:function(){
        var view = this;
        var newPrinter = new ReceiptPrinter();
        this.printers.add(newPrinter);
        newPrinter.save({}, {success:function(){
            view.renderPrinters();
        }});

    },

    newPrinterGroup:function(){
        var view = this;
        var newPrinterGroup = new ReceiptPrinterGroup();
        this.printer_groups.add(newPrinterGroup);
        newPrinterGroup.save({}, {success:function(){
            view.renderPrinterGroups();
        }});

    },

    addPrinter: function (printer) {
        this.$el.find('.printer-list').append(new PrinterManageView({model: printer}).render().el );
    },

    addPrinterGroup: function (printer_group) {
        this.$el.find('.printer-group-list').append(new PrinterGroupManageView({model: printer_group}).render().el );
    }
});

var PrinterManageView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/sales/manage_printer.html'],

    initialize: function () {

    },

    events: {
        "blur .printer": "updateLabel",
        "blur .ip_address": "updateIpAddress",
        "click .removePrinter": "removePrinter"
    },

    updateLabel: function(){
        var label = this.$el.find('.printer').val();
        this.model.set('label', label);
    },

    updateIpAddress: function(){
        var ipAddresss = this.$el.find('.ip_address').val();
        this.model.set('ipAddress', ipAddresss);
    },

    removePrinter: function(){
        this.model.destroy();
        this.remove();
    },

    save: function () {

    },

    onRender: function(){
    }

});

var PrinterGroupManageView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/sales/manage_printer_group.html'],

    initialize: function () {

    },

    events: {
        "blur .printerGroup": "updateLabel",
        "change .receiptPrinterDropdown":"updateDefaultPrinter",
        "click .removePrinterGroup": "removePrinterGroup"
    },

    updateLabel: function(){
        var label = this.$el.find('.printerGroup').val();
        this.model.set('label', label);
    },

    updateDefaultPrinter: function(){
        var printerId = this.$el.find('.receiptPrinterDropdown').val();
        this.model.set('defaultPrinterId', printerId);
    },

    removePrinterGroup: function(){
        this.model.destroy();
        this.remove();
    },

    save: function () {

    },

    onRender: function(){
    }

});
