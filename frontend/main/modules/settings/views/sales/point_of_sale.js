var PointOfSaleView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/sales/point_of_sale.html'],
    events:{
        'click .custom-payment-name':'makeCustomPaymentEditable',
        'click .add_custom_payment_type':'newCustomPayment'
    },
    initialize:function(){
        this.listenTo(App.vent, 'save-settings', this.save);
    },
    save:function(){
        this.saveCustomPayments();
    },
    onRender:function(){
        this.renderCustomePaymentTypes();
    },
    renderCustomePaymentTypes:function(){
        var view = this;
        this.$el.find('.custom-payment-type-list').html('');
        var customPaymentTypes = App.data.course.get('custom_payments');
        if (customPaymentTypes.length > 0) {
            _.each(customPaymentTypes.models, function(customPaymentType){
                view.addCustomPaymentRow(customPaymentType);
            });
        } else {
            var html = '<tr id="no-payment-types"><td colspan="2"><span class="sub">No Custom Payment Types</span></td></tr>';
            this.$el.find('.custom-payment-type-list').append(html);
        }
    },
    addCustomPaymentRow:function(customPayment){
        this.$el.find('.custom-payment-type-list').append(new CustomPaymentView({model: customPayment}).render().el );
    },
    newCustomPayment:function(){
        var view = this;
        var customPaymentTypes = App.data.course.get('custom_payments');
        var newPaymentType = new CustomPaymentType();
        customPaymentTypes.add(newPaymentType);
        newPaymentType.save({}, {success:function(){
            view.renderCustomePaymentTypes();
        }});

    },
    saveCustomPayments:function(){
        var customPaymentTypes = App.data.course.get('custom_payments');
        _.each(customPaymentTypes.models, function(customPaymentType){
            customPaymentType.save();
        });
    },
    makeCustomPaymentEditable:function(){

    }
});

var CustomPaymentView = Backbone.Marionette.ItemView.extend({

    tagName: 'tr',
    template: JST['settings/templates/sales/custom_payment.html'],

    initialize: function () {

    },

    events: {
        "keyup .customPaymentType": "updateLabel",
        "click .removeCustomPayment": "removeCustomPayment"
    },

    updateLabel: function(){
        var customPaymentType = this.$el.find('.customPaymentType').val();
        this.model.set('label', customPaymentType);
    },

    removeCustomPayment: function(){
        this.model.destroy();
        this.remove();
    },

    save: function () {

    },

    onRender: function(){
    }

});
