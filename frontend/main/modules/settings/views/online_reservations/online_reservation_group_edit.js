var EditOnlineReservationGroupView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['settings/templates/online_reservations/edit_online_reservation_group_view.html'],

    regions: {

    },

    initialize: function () {
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
    }
});
