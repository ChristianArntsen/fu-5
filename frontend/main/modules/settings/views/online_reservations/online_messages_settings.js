var OnlineMessagesView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['settings/templates/online_reservations/online_messages_settings.html'],
    events: {
        'change #onlineBookingWelcomeMessage':'updateOnlineBookingWelcomeMessage',
        'change #bookingRules':'updateBookingRules',
        'change #noShowPolicy':'updateNoShowPolicy',
        'change #termsAndConditions':'updateTermsAndConditions',
        'change #reservationEmailText':'updateReservationEmailText'
    },
    initialize:function(){
        this.listenTo(App.vent, 'save-settings', this.save);
    },
    onRender: function(){
        var view = this;
        this.$el.find('.text-editor').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']]
            ],
            height: 100,
            callbacks:{
                onBlur:function(contents, $editable){
                    view.updateMessages();
                }
            }
        });

        this.$el.find('#onlineBookingWelcomeMessage').summernote('code', this.model.get('onlineBookingWelcomeMessage'));
        this.$el.find('#bookingRules').summernote('code', this.model.get('bookingRules'));
        this.$el.find('#noShowPolicy').summernote('code', this.model.get('noShowPolicy'));
        this.$el.find('#termsAndConditions').summernote('code', this.model.get('termsAndConditions'));
        this.$el.find('#reservationEmailText').summernote('code', this.model.get('reservationEmailText'));
    },
    clearEmptyTextEditorData: function(content){
        if(content == '<p><br></p>'){
            return null;
        }
        return content;
    },
    updateMessages:function(){
        this.model.set('onlineBookingWelcomeMessage', this.clearEmptyTextEditorData(this.$el.find('#onlineBookingWelcomeMessage').summernote('code')));
        this.model.set('bookingRules', this.clearEmptyTextEditorData(this.$el.find('#bookingRules').summernote('code')));
        this.model.set('noShowPolicy', this.clearEmptyTextEditorData(this.$el.find('#noShowPolicy').summernote('code')));
        this.model.set('termsAndConditions', this.clearEmptyTextEditorData(this.$el.find('#termsAndConditions').summernote('code')));
        this.model.set('reservationEmailText', this.clearEmptyTextEditorData(this.$el.find('#reservationEmailText').summernote('code')));
    },
    save:function(){
        this.model.save();
    }
});

