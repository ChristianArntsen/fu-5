var SettingsLayout = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    className: 'row new-page',
    template: JST['settings/templates/settings.layout.html'],
    currentFields:[],
    ui:{
    },
    events:{
        //"click li.settings-section-label a": "selectSection",
        //"click li.settings-sub-section-label a": "selectSubSection",
        "click button.save-settings": "saveSettings"
    },
    regions: {
        sections: '#sections-container',
        subSections: '#sub-sections-container',
        settings: '#settings-container'
    },
    onDestroy:function(){
        $("#page-iframe-container").show();
    },
    selectSection:function(e) {
        e.preventDefault();
        var button = $(e.target).closest('li');
        $('#sections-container .selected').removeClass('selected');
        button.addClass('selected');
        var section = button.data('section');
        this.showSettingsSubSections(section);
        this.showSettings(section);
    },
    selectSubSection:function(e) {
        e.preventDefault();
        var button = $(e.target).closest('li');
        $('#sub-sections-container .selected').removeClass('selected');
        button.addClass('selected');
        var section = $('#sections-container .selected').data('section');
        var subsection = button.data('section');
        this.showSettings(section, subsection);
    },
    saveSettings: function(e) {
        console.log('saving course settings');
        //debugger;

        if (typeof this.currentFields != 'undefined') {
            var data = _.getFormData(this.$el);
            // Filter down to the only options that are savable
            data = _.pick(data, this.currentFields);
            console.log('------------------------------------- saveSettings --------------------------------------------');
            console.dir(data);

            //        return;
            //data.groups = this.groups.toJSON();
            //data.photo = this.$el.find('img.customer-photo').prop('src');
            if (Object.keys(data).length > 0) {
                this.model.set(data);
                this.model.save();
            }
        }
        App.vent.trigger('save-settings');
    },
    initialize:function(options){
        this.options = options;
        // App.vent.trigger("notification",{
        //     "type":"info",
        //     "msg":"We are currently phasing out the old customers page, if you need to use the old Customers page please: <a href='/v2/home#customers'>click here</a>",
        //     "delay":10000
        // });
        // var self = this;
        // this.settingsPromise = this.settings.fetch();
        // this.settings.possibleColumns = new PageSettingsPageColumnCollection([
        //     {
        //         headerCell: Backgrid.SelectAllMenuHeaderCell,
        //         name: "select",
        //         cell: "select-row"
        //     },{
        //         name: "first_name",
        //         label: "First Name",
        //         editable: false,
        //         sortable: true,
        //         cell:Backgrid.StringCell,
        //         sortType: "toggle"
        //     },{
        //         name: "actions",
        //         label: "Actions",
        //         editable: false,
        //         cell:Backgrid.Customers.MenuCell
        //     }
        // ]);
        //
        // //If on mobile device, don't fetch the initial list
        // var isMobile = window.matchMedia("only screen and (max-width: 760px)");
        // if (!isMobile.matches) {
        //     this.settingsPromise.done(function(){
        //         self.collection.state.pageSize = self.settings.get("page_size");
        //         self.collection.fetch({data:{"include":"customerGroups"}});
        //         self.table.show( new CustomersTableView({collection: self.collection,settings:self.settings}) );
        //     })
        // }
        // this.listenTo(this.collection.advancedSearch.get("allowed_fields"),"sync",function(){
        //     this.renderAdvancedSearch()
        // },this)
        // this.advancedSearchShown = false;
        //
        // this.listenTo(this.collection.advancedSearch,"change:q",this.executeSearch);
    },
    onShow:function(){
        // this.ui.advancedSearchDropdown.hide();
        //
        //
        // this.mobileList.show( new MobileCustomerList({collection: this.collection}) );
    },

    handleExportListClick:function(e) {
    },
    onRender:function(){
        var view = this;
        var section = 'businessDetails';
        var subsection = null;
        if (this.options.section) {
            section = this.options.section;
        }
        if (this.options.subsection) {
            subsection = this.options.subsection;
        }
        if (this.model.id) {
            this.loadViews(section, subsection);
        }
        else {
            this.model.fetch({
                success: function (model) {
                    view.loadViews(section, subsection);
                }
            });
        }
        //this.showSettings();
    },
    loadViews: function(section, subsection){
        this.showSettingsSections(section);
        this.showSettingsSubSections(section, subsection);
        this.showSettings(section, subsection);
    },
    showSettingsSections:function(selected){
        //this.sections.show(new SettingsSectionsView(this.options));
        var sections = this.options.sectionDefinitions.sections;
        var sectionInfo = this.options.sectionDefinitions.sectionInfo;
        var view = this;
        _.each(sections, function(value, key){
            var options = sectionInfo[value];
            if(value == selected || (typeof selected == 'undefined' && options.selected)){
                var selectedClass = 'selected';
            } else {
                var selectedClass = '';
            }
            var subSections = '';
            if (options.subSections != null) {
                subSections = 'hasSubSections';
            }
            view.$el.find("#sections-container ul").append("<li class='settings-section-label "+selectedClass+" "+subSections+"' data-section='"+value+"'><a href='#settings/"+value+"'>"+options.label+"</a></li>");
        });
    },
    showSettingsSubSections:function(selectedSection, selected){
        this.$el.find("#sub-sections-container ul").html('');
        if (selectedSection == null) {
            this.$el.find("#sub-sections-container").hide();
            this.$el.find('#settings-container').addClass('col-md-10').removeClass('col-md-8');
        }
        else {
            var sectionInfo = this.options.sectionDefinitions.sectionInfo;
            var subSections = sectionInfo[selectedSection].subSections;
            if (subSections == null) {
                this.$el.find("#sub-sections-container").hide();
                this.$el.find('#settings-container').addClass('col-md-10').removeClass('col-md-8');
            }
            else {
                this.$el.find("#sub-sections-container").show();
                this.$el.find('#settings-container').removeClass('col-md-10').addClass('col-md-8');
                var view = this;
                _.each(subSections, function(options, key){
//                  var options = sectionInfo[value];
                    if(key == selected || (typeof selected == 'undefined' && options.selected)){
                        var selectedClass = 'selected';
                    } else {
                        var selectedClass = '';
                    }

                    view.$el.find("#sub-sections-container ul").append("<li class='settings-sub-section-label "+selectedClass+"' data-section='"+key+"'><a href='#settings/"+selectedSection+"/"+key+"'>"+options.label+"</a></li>");
                });
            }
        }
    },
    showSettings:function(selectedSection, selectedSubSection){
        //var settingsPage = new SettingsView(this.options);
        this.currentFields = [];
        if (typeof selectedSection != 'undefined') {
            var sectionInfo = this.options.sectionDefinitions.sectionInfo;

            if (typeof selectedSubSection != 'undefined' && selectedSubSection != null) {
                var options = sectionInfo[selectedSection].subSections[selectedSubSection];
                if (typeof options.layout == 'function') {
                    this.settings.show(options.layout(this.model));
                    this.currentFields = options.fields;
                }
                return;
            }
            var options = sectionInfo[selectedSection];
            if (typeof options.layout == 'function') {
                this.settings.show(options.layout(this.model));
                this.currentFields = options.fields;
            } else {
                // Load first subsection of settings
                var subsections = options.subSections;
                var options = subsections[Object.keys(subsections)[0]];
                this.settings.show(options.layout(this.model));
                this.currentFields = options.fields;
            }
        }
    }
});

var SettingsView = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: ''
});
