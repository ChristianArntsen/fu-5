var SettingsRestModel = Backbone.JsonRestModel.extend({
    idAttribute: "id",
    urlRoot: REST_API_COURSE_URL + '/settings',
    type: 'settings',
    saveFilter:[],
    defaults: {},// Loaded from course
    initialize: function() {
        this.fetch();


    }

});
var SettingsRestModelSections = {
    sections:[
        'details',
        'tee_sheets',
        'sales',
        'online_reservations',
        'customers',
        'employees',
        'billing',
        'loyalty',
        'quickbooks'
        //,'integrations'
    ],
    sectionInfo: {
        details: {
            label: 'Business Details',
            selected: true,
            subSections: null,
            layout: function (model) {
                return new BusinessDetailsView({model:model});
            },
            fields: [
                'businessName',
                'businessAddress',
                'country',
                'city',
                'state',
                'postalCode',
                'timezone',
                'dateFormat',
                'timeFormat',
                'phoneNumber',
                'faxNumber',
                'emailAddress',
                'invoiceEmailAddress',
                'websiteUrl'
            ]
        },
        tee_sheets: {
            label: 'Tee Sheets',
            subSections: {
                tee_sheets: {
                    label: 'Tee Sheets',
                    selected: true,
                    layout: function () {
                        return new TeeSheetListView({model:App.data.v2.settings});
                    }
                },
                player_types: {
                    label: 'Player Types',
                    layout: function () {
                        return new PlayerTypesView({collection:App.data.v2.price_classes});
                    }
                },
                taxes_and_receipts: {
                    label: 'Taxes and Receipts',
                    layout: function (model) {
                        return new TeeSheetTaxesAndReceiptsView({model:model});
                    }
                }
            }
        },
        sales: {
            label: 'Sales',
            subSections: {
                point_of_sale: {
                    label: 'Point of Sale',
                    selected: true,
                    layout: function (model) {
                        return new PointOfSaleView({model:model});
                    },
                    fields: [
                        'defaultRegisterLogOpen',
                        'afterSaleLoad',
                        'hideTaxable',
                        'trackCash',
                        'allowEmployeeRegisterLogBypass',
                        'blindClose',
                        'requireEmployeePin',
                        'useTerminals',
                        'requireCustomerOnSale',
                        'useEtsGiftcards',
                        'creditCardFee',
                        'memberBalanceNickname',
                        'customerCreditNickname',
                        'requireSignatureMemberPayments',
                        'creditLimitToSubtotal'
                    ]
                },
                food_and_beverage: {
                    label: 'Food & Beverage',
                    layout: function (model) {
                        return new FoodAndBeverageView({model:model});
                    }
                },
                printing_and_terminals: {
                    label: 'Printing and Terminals',
                    layout: function (model) {
                        return new PrintingAndTerminalsView({model:model});
                    },
                    fields: [
                        'printAfterSale',
                        'printCreditCardReceipt',
                        'printSalesReceipt',
                        'printTipLine',
                        'receiptPrintAccountBalance',
                        'printMinimumsOnReceipt',
                        'printTwoReceipts',
                        'printTwoSignatureSlips',
                        'printTwoReceiptsOther',
                        'hideEmployeeLastNameReceipt',
                        'cashDrawerOnCash',
                        'cashDrawerOnSale',
                        'autoEmailReceipt',
                        'autoEmailNoPrint',
                        'onlinePurchaseTerminalId',
                        'onlineInvoiceTerminalId'
                    ]
                },
                taxes_and_returns: {
                    label: 'Taxes and Returns',
                    layout: function (model) {
                        return new TaxesAndReturnsView({model:model});
                    }
                }
            }
        },
        online_reservations: {
            label: 'Online Reservations',
            subSections: {
                general: {
                    label: 'General',
                    selected: true,
                    layout: function () {
                        return new GeneralView();
                    }
                },
                online_view_settins: {
                    label: 'Online View Settings',
                    layout: function () {
                        return new OnlineViewSettingsView();
                    }
                },
                messages: {
                    label: 'Messages',
                    layout: function () {
                        return new OnlineMessagesView({model:App.data.v2.settings});
                    }
                }
            }
        },
        customers: {
            label: 'Customers',
            subSections: null,
            layout: function () {
                //return new BusinessDetailsView();
                var settings = App.data.course.get('customer_page_settings');
                if (settings.get('page_size') == 0) {
                    settings.possibleColumns = new PageSettingsPageColumnCollection([
                        {
                            headerCell: Backgrid.SelectAllMenuHeaderCell,
                            name: "select",
                            cell: "select-row"
                        },{
                            name: "first_name",
                            label: "First Name",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.StringCell,
                            sortType: "toggle"
                        },{
                            name: "last_name",
                            label: "Last Name",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.StringCell,
                            sortType: "toggle"
                        },{
                            name: "phone_number",
                            label: "Phone Number",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.StringCell,
                            sortType: "toggle"
                        },{
                            name: "email",
                            label: "Email",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.EmailCell,
                            sortType: "toggle"
                        },{
                            name: "date_created",
                            label: "Date Created",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.Extension.MomentCell.extend({
                                displayFormat: "MMM DD, YYYY"
                            }),

                            sortType: "toggle"
                        },{
                            name: "account_balance",
                            label: App.data.course.get('customer_credit_nickname'),
                            editable: false,
                            sortable: true,
                            cell:Backgrid.NumberCell,
                            sortType: "toggle"
                        },{
                            name: "member_account_balance",
                            label: App.data.course.get('member_balance_nickname'),
                            editable: false,
                            sortable: true,
                            cell:Backgrid.NumberCell,
                            sortType: "toggle"
                        },{
                            name: "email_subscribed",
                            label: "Email Subscribed",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.BooleanCell,
                            sortType: "toggle"
                        },{
                            name: "history",
                            label: "History",
                            editable: false,
                            cell:Backgrid.Customers.HistoryCell
                        },{
                            name: "customerGroups",
                            label: "Groups",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.Customers.GroupsCell,
                            sortType: "toggle"
                        },{
                            name: "actions",
                            label: "Actions",
                            editable: false,
                            cell:Backgrid.Customers.MenuCell
                        }
                    ]);
                    settings.fetch({'async': false});
                }
                return new CustomerPageSettingsView({model:settings, modal:false});
            }
        },
        employees: {
            label: 'Employees',
            subSections: null,
            futureSubSections: {
                page_settings: {
                    label: 'Page Settings',
                    selected: true,
                    layout: function () {
                        return new EmployeePageSettingsView();
                    }
                },
                roles: {
                    label: 'Employee Roles',
                    layout: function () {
                        return new EmployeeRolesView();
                    }
                }
            },
            layout: function () {
                var settings = App.data.course.get('employee_page_settings');
                if (settings.get('page_size') == 0) {
                    settings.possibleColumns = new PageSettingsPageColumnCollection([
                        {
                            headerCell: Backgrid.SelectAllMenuHeaderCell,
                            name: "select",
                            cell: "select-row"
                        },{
                            name: "first_name",
                            label: "First Name",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.StringCell,
                            sortType: "toggle"
                        },{
                            name: "last_name",
                            label: "Last Name",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.StringCell,
                            sortType: "toggle"
                        },{
                            name: "phone_number",
                            label: "Phone Number",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.StringCell,
                            sortType: "toggle"
                        },{
                            name: "email",
                            label: "Email",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.EmailCell,
                            sortType: "toggle"
                        },{
                            name: "date_created",
                            label: "Date Created",
                            editable: false,
                            sortable: true,
                            cell:Backgrid.Extension.MomentCell.extend({
                                displayFormat: "MMM DD, YYYY"
                            }),

                            sortType: "toggle"
                        },{
                            name: "actions",
                            label: "Actions",
                            editable: false,
                            cell:Backgrid.Customers.MenuCell
                        }
                    ]);
                    settings.fetch({'async': false});
                }
                return new EmployeePageSettingsView({model:settings, modal:false});
            }
        },
        billing: {
            label: 'Billing',
            subSections: {
                page_settings: {
                    label: 'Page Settings',
                    selected: true,
                    layout: function () {
                        var settings = App.data.course.get('billing_page_settings');
                        if (settings.get('page_size') == 0) {
                            settings.possibleColumns = new PageSettingsPageColumnCollection([
                                {
                                    headerCell: "select-all",
                                    name: "select",
                                    cell: "select-row"
                                }, {
                                    name: "number",
                                    label: "Number",
                                    editable: false,
                                    sortable: true,
                                    cell: Backgrid.StringCell,
                                    sortType: "toggle"
                                }, {
                                    name: "customer",
                                    label: "Customer",
                                    editable: false,
                                    sortable: false,
                                    cell: Backgrid.Cell.extend({
                                        render: function() {
                                            this.$el.empty();
                                            var customer = this.model.get('customer');
                                            if(customer && customer.contact_info){
                                                this.$el.html( customer.contact_info.first_name +' '+ customer.contact_info.last_name );
                                            }else{
                                                this.$el.html('<span class="text-muted">N/A</span>');
                                            }
                                            return this;
                                        }
                                    }),
                                    sortType: "toggle"
                                }, {
                                    name: "dateCreated",
                                    label: "Created On",
                                    editable: false,
                                    sortable: true,
                                    cell: Backgrid.Extension.MomentCell.extend({
                                        displayFormat: "MM/DD/YY"
                                    }),
                                    sortType: "toggle"
                                },{
                                    name: "repeats",
                                    label: "Repeats",
                                    editable: false,
                                    sortable: false,
                                    cell: Backgrid.Cell.extend({
                                        render: function(){
                                            var recurringStatement = this.model.get('accountRecurringStatement');
                                            if(recurringStatement && recurringStatement.get('repeated')){
                                                this.$el.html( recurringStatement.get('repeated').get('period').toUpperCase() );
                                            }else{
                                                this.$el.html('<span class="text-muted">N/A</span>');
                                            }
                                            return this;
                                        }
                                    }),
                                    sortType: "toggle"
                                },{
                                    name: "nextOccurence",
                                    label: "Next Occurence",
                                    editable: false,
                                    sortable: true,
                                    cell: Backgrid.Cell.extend({
                                        render: function(){
                                            var recurringStatement = this.model.get('accountRecurringStatement');
                                            if(recurringStatement && recurringStatement.get('repeated') && recurringStatement.get('repeated').get('next_occurence')){
                                                this.$el.html( moment(recurringStatement.get('repeated').get('next_occurence')).format('MM/DD/YY') );
                                            }else{
                                                this.$el.html('<span class="text-muted">N/A</span>');
                                            }
                                            return this;
                                        }
                                    }),
                                    sortType: "toggle"
                                },{
                                    name: "dueDate",
                                    label: "Due On",
                                    editable: false,
                                    sortable: true,
                                    cell: Backgrid.Extension.MomentCell.extend({
                                        displayFormat: "MM/DD/YY"
                                    }),
                                    sortType: "toggle"
                                },{
                                    name: "total",
                                    label: "Total",
                                    editable: false,
                                    sortable: true,
                                    cell: Backgrid.NumberCell,
                                    sortType: "toggle"
                                },{
                                    name: "totalPaid",
                                    label: "Total Paid",
                                    editable: false,
                                    sortable: true,
                                    cell: Backgrid.NumberCell,
                                    sortType: "toggle"
                                },{
                                    name: "totalOpen",
                                    label: "Total Due",
                                    editable: false,
                                    sortable: true,
                                    cell: Backgrid.NumberCell,
                                    sortType: "toggle"
                                },{
                                    name: "pay",
                                    label: "Pay",
                                    editable: false,
                                    sortable: false,
                                    cell: Backgrid.Cell.extend({
                                        className: 'statement-pay-cell',
                                        render: function() {
                                            if(this.model.get('totalOpen') > 0){
                                                this.$el.html('<button class="btn btn-primary btn-sm pay-statement">PAY NOW</button>' );
                                            }else{
                                                this.$el.html('<span class="text-muted">PAID</span>');
                                            }
                                            return this;
                                        }
                                    })
                                },{
                                    name: "actions",
                                    label: "Actions",
                                    editable: false,
                                    sortable: false,
                                    cell: StatementTableMenuCell
                                }
                            ]);
                            settings.fetch({'async': false});
                        }
                        return new BillingPageSettingsView({model:settings, modal:false});
                    }
                },
                roles: {
                    label: 'Statement Settings',
                    layout: function () {
                        var defaultRecurringStatement = this.getDefaultRecurringStatement();
                        return new DefaultRecurringStatementSettingsView({model: defaultRecurringStatement, modal:false});
                    },
                    getDefaultRecurringStatement: function(){
                        var recurringStatements = App.data.v2.recurringStatements;
                        var defaultRecurringStatement = false;
                        if (recurringStatements.length > 0) {
                            defaultRecurringStatement = recurringStatements.findWhere({isDefault: true});
                        }
                        if(!defaultRecurringStatement){
                            defaultRecurringStatement = new RecurringStatement({isDefault: true});
                            recurringStatements.add(defaultRecurringStatement);
                        }
                        return defaultRecurringStatement;
                    }
                }
            }
        },
        loyalty: {
            label: 'Loyalty',
            subSections: null,
            layout: function (model) {
                return new LoyaltyView({model: model});
            },
            fields: [
                'useLoyalty',
                'loyaltyAutoEnroll'
            ]
        },
        quickbooks: {
            label: 'QuickBooks',
            subSections: null,
            layout: function () {
                return new QuickbooksView();
            }
        }
        // , integrations:{
        //     label: 'Integrations',
        //     subSections:null
        // }
    }
};
var SettingsRestModelDropdownOptions = {
    countries: {
        "USA": "United States",
        "CA": "Canada"
    },
    states: {
        "USA": {
            "AL": "Alabama",
            "AK": "Alaska",
            "AZ": "Arizona",
            "AR": "Arkansas",
            "CA": "California",
            "CO": "Colorado",
            "CT": "Connecticut",
            "DC": "District of Columbia",
            "DE": "Delaware",
            "FL": "Florida",
            "GA": "Georgia",
            "HI": "Hawaii",
            "ID": "Idaho",
            "IL": "Illinois",
            "IN": "Indiana",
            "IA": "Iowa",
            "KS": "Kansas",
            "KY": "Kentucky",
            "LA": "Louisiana",
            "ME": "Maine",
            "MD": "Maryland",
            "MA": "Massachusetts",
            "MI": "Michigan",
            "MN": "Minnesota",
            "MS": "Mississippi",
            "MO": "Missouri",
            "MT": "Montana",
            "NE": "Nebraska",
            "NV": "Nevada",
            "NH": "New Hampshire",
            "NJ": "New Jersey",
            "NM": "New Mexico",
            "NY": "New York",
            "NC": "North Carolina",
            "ND": "North Dakota",
            "OH": "Ohio",
            "OK": "Oklahoma",
            "OR": "Oregon",
            "PA": "Pennsylvania",
            "RI": "Rhode Island",
            "SC": "South Carolina",
            "SD": "South Dakota",
            "TN": "Tennessee",
            "TX": "Texas",
            "UT": "Utah",
            "VT": "Vermont",
            "VA": "Virginia",
            "WA": "Washington",
            "WV": "West Virginia",
            "WI": "Wisconsin",
            "WY": "Wyoming",
            "AS": "American Samoa",
            "GU": "Guam",
            "MP": "Northern Mariana Islands",
            "PR": "Puerto Rico",
            "UM": "United States Minor Outlying Islands",
            "VI": "Virgin Islands"
        },
        "CA": {
            "AB": "Alberta",
            "BC": "British Columbia",
            "MB": "Manitoba",
            "NB": "New Brunswick",
            "NL": "Newfoundland and Labrador",
            "NS": "Nova Scotia",
            "NU": "Nunavut",
            "NT": "Northwest Territories",
            "ON": "Ontario",
            "PE": "Prince Edward Island",
            "QC": "Quebec",
            "SK": "Saskatchewan",
            "YT": "Yukon"
        }
    },
    timezones: {
        'Pacific/Midway': '(GMT-11:00) Midway Island, Samoa',
        'America/Adak': '(GMT-10:00) Hawaii-Aleutian',
        'Etc/GMT+10': '(GMT-10:00) Hawaii',
        'Pacific/Marquesas': '(GMT-09:30) Marquesas Islands',
        'Pacific/Gambier': '(GMT-09:00) Gambier Islands',
        'America/Anchorage': '(GMT-09:00) Alaska',
        'America/Ensenada': '(GMT-08:00) Tijuana, Baja California',
        'Etc/GMT+8': '(GMT-08:00) Pitcairn Islands',
        'America/Los_Angeles': '(GMT-08:00) Pacific Time (US & Canada)',
        'America/Denver': '(GMT-07:00) Mountain Time (US & Canada)',
        'America/Chihuahua': '(GMT-07:00) Chihuahua, La Paz, Mazatlan',
        'America/Dawson_Creek': '(GMT-07:00) Arizona',
        'America/Belize': '(GMT-06:00) Saskatchewan, Central America',
        'America/Cancun': '(GMT-06:00) Guadalajara, Mexico City, Monterrey',
        'Chile/EasterIsland': '(GMT-06:00) Easter Island',
        'America/Chicago': '(GMT-06:00) Central Time (US & Canada)',
        'America/New_York': '(GMT-05:00) Eastern Time (US & Canada)',
        'America/Havana': '(GMT-05:00) Cuba',
        'America/Bogota': '(GMT-05:00) Bogota, Lima, Quito, Rio Branco',
        'America/Caracas': '(GMT-04:30) Caracas',
        'America/Santiago': '(GMT-04:00) Santiago',
        'America/La_Paz': '(GMT-04:00) La Paz',
        'Atlantic/Stanley': '(GMT-04:00) Faukland Islands',
        'America/Campo_Grande': '(GMT-04:00) Brazil',
        'America/Goose_Bay': '(GMT-04:00) Atlantic Time (Goose Bay)',
        'America/Glace_Bay': '(GMT-04:00) Atlantic Time (Canada)',
        'America/St_Johns': '(GMT-03:30) Newfoundland',
        'America/Araguaina': '(GMT-03:00) UTC-3',
        'America/Montevideo': '(GMT-03:00) Montevideo',
        'America/Miquelon': '(GMT-03:00) Miquelon, St. Pierre',
        'America/Godthab': '(GMT-03:00) Greenland',
        'America/Argentina/Buenos_Aires': '(GMT-03:00) Buenos Aires',
        'America/Sao_Paulo': '(GMT-03:00) Brasilia',
        'America/Noronha': '(GMT-02:00) Mid-Atlantic',
        'Atlantic/Cape_Verde': '(GMT-01:00) Cape Verde Is.',
        'Atlantic/Azores': '(GMT-01:00) Azores',
        'Europe/Belfast': '(GMT) Greenwich Mean Time : Belfast',
        'Europe/Dublin': '(GMT) Greenwich Mean Time : Dublin',
        'Europe/Lisbon': '(GMT) Greenwich Mean Time : Lisbon',
        'Europe/London': '(GMT) Greenwich Mean Time : London',
        'Africa/Abidjan': '(GMT) Monrovia, Reykjavik',
        'Europe/Amsterdam': '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
        'Europe/Belgrade': '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',
        'Europe/Brussels': '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris',
        'Africa/Algiers': '(GMT+01:00) West Central Africa',
        'Africa/Windhoek': '(GMT+01:00) Windhoek',
        'Asia/Beirut': '(GMT+02:00) Beirut',
        'Africa/Cairo': '(GMT+02:00) Cairo',
        'Asia/Gaza': '(GMT+02:00) Gaza',
        'Africa/Blantyre': '(GMT+02:00) Harare, Pretoria',
        'Asia/Jerusalem': '(GMT+02:00) Jerusalem',
        'Europe/Minsk': '(GMT+02:00) Minsk',
        'Asia/Damascus': '(GMT+02:00) Syria',
        'Europe/Moscow': '(GMT+03:00) Moscow, St. Petersburg, Volgograd',
        'Africa/Addis_Ababa': '(GMT+03:00) Nairobi',
        'Asia/Tehran': '(GMT+03:30) Tehran',
        'Asia/Dubai': '(GMT+04:00) Abu Dhabi, Muscat',
        'Asia/Yerevan': '(GMT+04:00) Yerevan',
        'Asia/Kabul': '(GMT+04:30) Kabul',
        'Asia/Yekaterinburg': '(GMT+05:00) Ekaterinburg',
        'Asia/Tashkent': '(GMT+05:00) Tashkent',
        'Asia/Kolkata': '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',
        'Asia/Katmandu': '(GMT+05:45) Kathmandu',
        'Asia/Dhaka': '(GMT+06:00) Astana, Dhaka',
        'Asia/Novosibirsk': '(GMT+06:00) Novosibirsk',
        'Asia/Rangoon': '(GMT+06:30) Yangon (Rangoon)',
        'Asia/Bangkok': '(GMT+07:00) Bangkok, Hanoi, Jakarta',
        'Asia/Krasnoyarsk': '(GMT+07:00) Krasnoyarsk',
        'Asia/Hong_Kong': '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
        'Asia/Irkutsk': '(GMT+08:00) Irkutsk, Ulaan Bataar',
        'Australia/Perth': '(GMT+08:00) Perth',
        'Australia/Eucla': '(GMT+08:45) Eucla',
        'Asia/Tokyo': '(GMT+09:00) Osaka, Sapporo, Tokyo',
        'Asia/Seoul': '(GMT+09:00) Seoul',
        'Asia/Yakutsk': '(GMT+09:00) Yakutsk',
        'Australia/Adelaide': '(GMT+09:30) Adelaide',
        'Australia/Darwin': '(GMT+09:30) Darwin',
        'Australia/Brisbane': '(GMT+10:00) Brisbane',
        'Australia/Hobart': '(GMT+10:00) Hobart',
        'Asia/Vladivostok': '(GMT+10:00) Vladivostok',
        'Australia/Lord_Howe': '(GMT+10:30) Lord Howe Island',
        'Etc/GMT-11': '(GMT+11:00) Solomon Is., New Caledonia',
        'Asia/Magadan': '(GMT+11:00) Magadan',
        'Pacific/Norfolk': '(GMT+11:30) Norfolk Island',
        'Asia/Anadyr': '(GMT+12:00) Anadyr, Kamchatka',
        'Pacific/Auckland': '(GMT+12:00) Auckland, Wellington',
        'Etc/GMT-12': '(GMT+12:00) Fiji, Kamchatka, Marshall Is.',
        'Pacific/Chatham': '(GMT+12:45) Chatham Islands',
        'Pacific/Tongatapu': '(GMT+13:00) Nuku\'alofa',
        'Pacific/Kiritimati': '(GMT+14:00) Kiritimati'
    },
    timeFormats: {
        '12_hour': '1:00PM',
        '24_hour': '13:00'
    },
    dateFormats: {
        'middle_endian': '12/30/2000',
        'little_endian': '30-12-2000',
        'big_endian': '2000-12-30'
    },
    halfHours: {
        '0000':'12:00am',
        '0030':'12:30am',
        '0100':'1:00am',
        '0130':'1:30am',
        '0200':'2:00am',
        '0230':'2:30am',
        '0300':'3:00am',
        '0330':'3:30am',
        '0400':'4:00am',
        '0430':'4:30am',
        '0500':'5:00am',
        '0530':'5:30am',
        '0600':'6:00am',
        '0630':'6:30am',
        '0700':'7:00am',
        '0730':'7:30am',
        '0800':'8:00am',
        '0830':'8:30am',
        '0900':'9:00am',
        '0930':'9:30am',
        '1000':'10:00am',
        '1030':'10:30am',
        '1100':'11:00am',
        '1130':'11:30am',
        '1200':'12:00pm',
        '1230':'12:30pm',
        '1300':'1:00pm',
        '1330':'1:30pm',
        '1400':'2:00pm',
        '1430':'2:30pm',
        '1500':'3:00pm',
        '1530':'3:30pm',
        '1600':'4:00pm',
        '1630':'4:30pm',
        '1700':'5:00pm',
        '1730':'5:30pm',
        '1800':'6:00pm',
        '1830':'6:30pm',
        '1900':'7:00pm',
        '1930':'7:30pm',
        '2000':'8:00pm',
        '2030':'8:30pm',
        '2100':'9:00pm',
        '2130':'9:30pm',
        '2200':'10:00pm',
        '2230':'10:30pm',
        '2300':'11:00pm',
        '2330':'11:30pm',
        '2359':'11:59pm'
    },
    increments:{
        '5.0':'5 min',
        '6.0':'6 min',
        '7.0':'7 min',
        '7.5':'7.5 min',
        '8.0':'8 min',
        '9.0':'9 min',
        '10.0':'10 min',
        '11.0':'11 min',
        '12.0':'12 min',
        '13.0':'13 min',
        '14.0':'14 min',
        '15.0':'15 min',
        '20.0':'20 min',
        '30.0':'30 min',
        '45.0':'45 min',
        '60.0':'60 min'
    },
    times_to_finish:{
        '130':'1 hour 30 min',
        '135':'1 hour 35 min',
        '140':'1 hour 40 min',
        '145':'1 hour 45 min',
        '150':'1 hour 50 min',
        '155':'1 hour 55 min',
        '200':'2 hours',
        '205':'2 hours 5 min',
        '210':'2 hours 10 min',
        '215':'2 hours 15 min',
        '220':'2 hours 20 min',
        '225':'2 hours 25 min',
        '230':'2 hours 30 min'
    },
    daysInBookingWindow: function(){
        var daysInBookingWindow = {};
        for (var i = 0; i < 61; i++)
        {
            daysInBookingWindow[i] = i;
        }
        daysInBookingWindow[90] = 90;
        daysInBookingWindow[120] = 120;
        daysInBookingWindow[365] = 365;

        return daysInBookingWindow;
    }
}