var ServiceFee = Backbone.JsonRestModel.extend({
    defaults:{
        'item_id': null,
        'name' : null,
        'department' : null,
        'category' : null,
        'subcategory' : null,
        'item_number' : null,
        'description' : 'some fee!',
        'image_id' : null,
        'cost_price' : 0,
        'unit_price' : 0,
        'unit_price_includes_tax' : 0,
        'inventory_level' :0,
        'inventory_unlimited' : 1,
        'erange_size' : null, // what is this?
        'quickbooks_income' : null,
        'quickbooks_cogs' : null,
        'quickbooks_assets' : null,
        'max_discount' : 0,
        'quantity' : null,
        'is_unlimited' : 1,
        'reorder_level' : null,
        'food_and_beverage' : null,
        'number_of_sides' : null,
        'location' : null,
        'gl_code' : null,
        'allow_alt_description' : null,
        'is_serialized' : null,
        'invisible' : null,
        'deleted' : null,
        'is_side' : null,
        'soup_or_salad' : null,
        'add_on_price' : null,
        'print_priority' : 1,
        'kitchen_printer' : null,
        'do_not_print' : null,
        'inactive' : null,
        'meal_course_id' : null,
        'prompt_meal_course' : null,
        'do_not_print_customer_receipt' : null,
        'is_fee' : null, // not used
        'taxes' : null,
        'supplier_id' : null,
        'is_shared' : null, // not used?
        'force_tax' : null,
        'item_type' : 'service_fee',
        'parent_item_percent' :null,
        'whichever_is' : 'more',
        'is_service_fee' : 1,
        'parentItem':null,
        'total': 0,
        'subtotal': 0,
        'tax': 0,
        'taxable': 1,
        'base_price': 0
    },

    initialize: function() {

    },

    getTotal: function(taxable, divisor) {
        //
        result = this.calculatePrice(taxable, divisor);
        return result.get('split_total');
    },

    getSubtotal: function(taxable, divisor) {
        //
        result = this.calculatePrice(taxable, divisor);
        return result.get('split_subtotal');
    },

    getTax: function(taxable, divisor) {
        //
        result = this.calculatePrice(taxable, divisor);
        return result.get('split_tax');
    },

    calculatePrice: function(taxable, divisor, parent_data) {
        
        if(!divisor){
            divisor = 1;
        }

        if(!parent_data.comp){
            parent_data.comp = false;
        }

        if(taxable === true){
            taxable = 1;
        }
        if(taxable === false){
            taxable = 0;
        }
        
        var data = this.toJSON();

        var flat_fee = new FbCartItem(data);
        var percent_fee = new FbCartItem(data);

        // Calculate the percentage fee
        var parent_price = new Decimal(parent_data.subtotal).dividedBy(parent_data.quantity).toDP(2);
        var percentage = new Decimal(this.get('parent_item_percent')).dividedBy(100);
        var percent_price = parent_price.times(percentage).toDP(2).toNumber();

        percent_fee.set({
            'unit_price': percent_price,
            'price': percent_price,
            'quantity': parent_data.quantity,
            'comp': parent_data.comp
        });
        percent_fee.calculatePrice(taxable);

        var flat_fee_price = new Decimal(this.get('base_price'));
        flat_fee_price = flat_fee_price.dividedBy(divisor).toDP(2).toNumber();

        // Calculate flat fee
        flat_fee.set({
            'unit_price': flat_fee_price,
            'price': flat_fee_price,
            'quantity': parent_data.quantity,
            'comp': parent_data.comp       
        });
        flat_fee.calculatePrice(taxable);

        // Compare which fee is more/less and which one we will use
        var chosen_fee = false;
        if(this.get('whichever_is') === 'more'){   
            if(percent_fee.get('subtotal') >= flat_fee.get('subtotal')) {
                chosen_fee = percent_fee;
            }else {
                chosen_fee = flat_fee;
            }
        
        }else{
            if(percent_fee.get('subtotal') < flat_fee.get('subtotal')) {
                chosen_fee = percent_fee;
            }else {
                chosen_fee = flat_fee;
            }
        }
        
        if(!taxable){
            _.forEach(chosen_fee.get('taxes'),function(tax){
                tax.amount = 0;
            });
        }

        var data = {
            'quantity': chosen_fee.get('quantity'),
            'unit_price': chosen_fee.get('unit_price'),
            'price': chosen_fee.get('price'),
            'subtotal': chosen_fee.get('subtotal'),
            'total': chosen_fee.get('total'),
            'tax': chosen_fee.get('tax'),
            'taxes': chosen_fee.get('taxes'),
            'comp_total': chosen_fee.get('comp_total'),
            'non_discount_subtotal': chosen_fee.get('non_discount_subtotal'),
            'taxable': chosen_fee.get('taxable')
        };

        this.set(data);
    },

    setParentItem: function(parent_item) {
        //
    }
});