var PeopleModel = Backbone.JsonRestModel.extend({
    type:"people",
    idAttribute: "id",
    defaults: {
        "first_name": "",
        "last_name": "",
        "phone_number": "",
        "cell_phone_number": "",
        "email": "",
        "birthday": "",
        "address_1": "",
        "address_2": "",
        "city": "",
        "state": "",
        "zip": "",
        "country": "",
        "comments": "",
    },
    relations: {
        bookedPlayer: {
            type: 'bookedPlayer',
            many: false
        }
    }
});

var PeopleCollection = Backbone.JsonRestCollection.extend({
    model: PeopleModel,
});

Backbone.modelFactory.registerModel(PeopleModel,PeopleCollection);