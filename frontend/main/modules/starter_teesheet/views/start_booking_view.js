var StarterBookingItemView = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    className: function(){
        return 'col-xs-12';
    },

    template: JST['starter_teesheet/templates/starter_booking.html'],

    events: {
        'click': 'bookingClicked'
    },
    regions: {
        players: '#players-region'
    },
    modelEvents: {
        "change:playerCount": "modelChanged",
        "change:carts change:cart1 change:cart2": "modelChanged",
        "change:teedOffTime": "modelChanged",
        "change:paid":"modelChanged",
        "change:bookedPlayers":"playersUpdated"
    },
    playersUpdated:function(){
        this.listenTo( this.model.get("bookedPlayers"),"add remove change",function(){
            this.renderSubView();
        });
        this.renderSubView();
    },
    initialize:function(){
        this.listenTo( this.model.get("bookedPlayers"),"add remove change",function(){
            this.renderSubView();
        });
    },
    modelChanged:function(){
        this.$el.attr("class",this.className());
        this.render();
    },
    bookingClicked:function(){
        if(this.model.get("type")=="teetime"){
            App.vent.trigger('starter:bookingClicked', {'model': this.model});
        }
    },
    renderSubView :function(){
        if(this.model.get("type") != "teetime"){
            return;
        }
        this.players.show(new PlayersCollectionView(
            {
                collection:this.model.get("bookedPlayers"),
                playerCount : this.model.get("playerCount")
            }));
    },
    onRender: function(){
        this.renderSubView();
    }
});

var BookingsCollectionView = Backbone.Marionette.CompositeView.extend({
    tagName: 'div',
    reorderOnSort:true,
    template: JST['starter_teesheet/templates/starter_timeslot.html'],
    className: "row",
    childView: StarterBookingItemView,
    childViewContainer: "#timeslot_container",
    initialize: function(){
        this.collection = this.model.get('vc');
    }
});

var TimeslotsView = Backbone.Marionette.CompositeView.extend({
    reorderOnSort:true,
    template:function(){
        return "<div></div>";
    },
    childViewContainer: "div",
    childView: BookingsCollectionView,

});
