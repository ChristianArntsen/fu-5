var StarterBookingLayout = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    className: 'row page',
    attributes: {
        id: 'page-items'
    },
    template: JST['starter_teesheet/templates/starter_booking_layout.html'],

    regions: {
        bookings: '#booking-region',
        booking: '#teetime-region'
    },

    events: {
        'click button.teesheet-side': 'selectSide',
        'click #nextDay': 'nextDay',
        'click #prevDay': 'prevDay'
    },
    onRender: function(){
        var view = new TimeslotsView({collection:this.collection});
        this.bookings.show(view);
    },
    onDestroy:function(){
        if(this.poller){
            this.poller.destroy();
        }
        App.vent.off("starter:bookingClicked");
    },
    startPoller:function(){
        var self=this;
        self.poller = Backbone.Poller.get(this.options.originalCollection,
            {
                data:{
                    limit:100,
                    include: "bookedPlayers,bookedPlayers.people",
                    filter:{side:this.model.get("side")},
                    startDate:this.model.get("date").format("YYYY-MM-D"),
                    endDate:this.model.get("date").clone().add(1,'d').format("YYYY-MM-D")
                },
                delay: [
                    5000,
                    60000,
                    self.delayedFunction.bind(self)
                ]
            });
        self.poller.start();
    },
    initialize:function(){
        var self = this;
        this.listenTo(this.options.originalCollection, "add remove change:title change:playerCount change:paidPlayerCount change:start", function(e,t,r){
            self.somethingNoticableChanged = true;
        }, this);
        this.startPoller();

        this.listenTo(App.vent,"starter:bookingClicked",function(args){
            if(!self.booking)
                return;
            self.booking.show(new StarterBookingEditView({
                model:args.model
            }))
        });
    },
    delayedFunction : function(n){
        if(this.somethingNoticableChanged ){
            this.somethingNoticableChanged = false;
            return 0
        }
        return n*1.5;
    },
    nextDay:function(){
        this.model.get("date").add(1,'d');
        this.render();
        this.updateCollection();
    },
    prevDay:function(){
        this.model.get("date").subtract(1,'d');
        this.render();
        this.updateCollection();
    },
    selectSide:function(e){
        this.model.set("side",e.currentTarget.getAttribute('value'));
        this.render();
        this.updateCollection();

    },
    updateCollection:function(){
        this.startPoller();
        this.options.originalCollection.fetch({
            data:{
                include: "bookedPlayers",
                limit:100,
                filter:{side:this.model.get("side")},
                startDate:this.model.get("date").format("YYYY-MM-D"),
                endDate:this.model.get("date").clone().add(1,'d').format("YYYY-MM-D")
            },
            remove: false
        });
    }

});

