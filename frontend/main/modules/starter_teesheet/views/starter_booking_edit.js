

var StarterBookingEditView = Backbone.Marionette.LayoutView.extend({
    behaviors: {
        "ModalDialogBehavior":{
            behaviorClass: SideModalBehavior
        }
    },
    events: {
        "click #teedoff":"teeOff"
    },
    teeOff:function(){
        this.model.set("teedOffTime",moment().format());
        this.model.save();
        this.render();
    },
    tagName: 'div',
    className: 'edit-booking',
    template: JST['starter_teesheet/templates/starter_booking_edit.html'],

    regions: {
        players: '#edit-players-region'
    },
    onRender: function(){
        var teesheet_id = this.model.get("teesheet_id");
        this.players.show(new PlayersEditCollectionView(
            {
                collection : this.model.get("bookedPlayers"),
                playerCount : this.model.get("playerCount")
            }
        ))
    }
});
