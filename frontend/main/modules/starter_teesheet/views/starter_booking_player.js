var PlayersItem = Backbone.Marionette.LayoutView.extend({

    tagName: 'div',
    className: "row",
    template: JST['starter_teesheet/templates/starter_booking_player.html'],
    regions:{
        "message":"#message"
    },
    onRender:function(){
        if(this.model.get("people")){
            this.message.show(new MessagePersonView({model:this.model.get("people")}))
        }
    }
});



var PlayersCollectionView = Backbone.Marionette.CollectionView.extend({
    tagName: 'div',
    childView: PlayersItem,
    addChild: function(child, ChildView, index){
        if (child.get("position") <= this.options.playerCount) {
            Backbone.Marionette.CollectionView.prototype.addChild.apply(this, arguments);
        }
    },
    modelEvents: {
        "change": "modelChanged"
    },
    modelChanged:function(){
        this.render();
    },
});

var MessagePersonView = Backbone.Marionette.ItemView.extend({
    template: JST['starter_teesheet/templates/people_message.html'],
    events:{
        "click":"handleClick"
    },
    className: "person-message-icon",
    onRender:function(){
        if(this.model.get("phone_number") == "" && this.model.get("cell_phone_number") == ""){
            this.$el.hide();
        }
    },
    handleClick:function(e){
        e.stopPropagation();
        var customer = new Customer({
            person_id: this.model.get("id")
        });

        customer.fetch({
            silent: true,
            data: {
                include_rainchecks: true,
                include_credit_cards: true,
                include_marketing: true,
                include_account_transactions: true,
                include_billing: true,
                include_last_visit: true
            },
            success: function(model){
                customerEditModal.render();

                // Delay the listener because a sync event is triggered
                // AFTER this success callback (which we don't care about)
                setTimeout(function(){
                    customer.on('sync', function(){
                        self.model.fetch({
                            data:{
                                include:"customerGroups"
                            }
                        });
                    });
                }, 10);
            }
        });
        var customerEditModal = new EditCustomerView({model: customer});
        customerEditModal.defaultTab = "marketing";
        customerEditModal.show();
        $('.modal-content').loadMask();
    }
});