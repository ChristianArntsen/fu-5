var InventoryAudit = Backbone.JsonRestModel.extend({
    defaults: {
        inventory_audit_items:[]
    }

});

var InventoryAuditCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/inventory_audits',
    model: InventoryAudit
});

Backbone.modelFactory.registerModel(InventoryAudit, InventoryAuditCollection);