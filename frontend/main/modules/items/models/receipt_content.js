var ItemReceiptContent = Backbone.Model.extend({
	urlRoot: API_URL + '/receipt_content',
	idAttribute: "receipt_content_id",

	defaults: {
		"name": "",
		"content": "",
		"signature_line": false,
		"separate_receipt": false
	},

	validate: function(attrs){

		if(!attrs.name){
			return 'Name is required';
		}
		if(!attrs.content){
			return 'Receipt content is required';
		}
	}
});

var ItemReceiptContentCollection = Backbone.Collection.extend({
	model: ItemReceiptContent,
	url: API_URL + '/receipt_content',

	get_dropdown_data: function(){

		if(this.length == 0){
			return {0: '- No Receipt Agreements -'};
		}

		var options = {0: '- Select Receipt Agreement -'};
		_.each(this.models, function(model){
			options[model.get('receipt_content_id')] = model.get('name');
		});

		return options;
	}
});