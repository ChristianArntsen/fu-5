var ItemReceiptContentModalView = ModalLayoutView.extend({

    id: 'receipt_content_window',
    template: JST['items/templates/receipt_content_window.html'],

    events: {
        "click button.new": "new_content"
    },

    regions: {
        'ReceiptContentList': '#receipt-content-list'
    },

    initialize: function(options){
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});

    },

    onRender: function(){
        this.ReceiptContentList.show( new ReceiptContentListView({collection: this.collection}) );
    },

    new_content: function(){
        var model = new ItemReceiptContent();
        var details = new ReceiptContentEditView({model: model});
        details.show();
        return false;
    }
});