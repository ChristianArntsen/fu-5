var ReceiptContentItemView = Backbone.Marionette.ItemView.extend({
    className: '',
    tagName: 'tr',
    template: JST['items/templates/receipt_content_list_item.html'],
    modelEvents: {
        'change': 'render'
    },

    events: {
        "click .edit": "edit_receipt_content"
    },

    edit_receipt_content: function () {
        var details = new ReceiptContentEditView({model: this.model});
        details.show();
        return false;
    }
});

var ReceiptContentEmptyView = Backbone.Marionette.ItemView.extend({
    template: function(){
        return "<td class='text-muted' colspan='4'>No receipt printouts have been made</td>";
    }
});

var ReceiptContentListView = Backbone.Marionette.CompositeView.extend({
    className: 'table',
    tagName: 'table',
    childView: ReceiptContentItemView,
    emptyView: ReceiptContentEmptyView,
    template: JST['items/templates/receipt_content_list.html'],
    childViewContainer: 'tbody'
});