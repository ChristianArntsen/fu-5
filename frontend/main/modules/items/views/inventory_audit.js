var InventoryAuditView = ModalLayoutView.extend({

    template: JST['items/templates/inventory_audit_view.html'],

    regions: {
        audit_menu : '.audit-menu',
        inventory_audit_form : '.inventory-list-container'
    },

    events: {
        "change #category-or-department": "update_list",
        "change #inventory-audit-dropdown": "load_audit",
        "click #new_audit_button": "openNewAudit",
        "click #past_audits_button": "openPastAudits",
        "click button.save-and-close": "save_and_close",
        "click button.save-and-finish": "save_and_finish",
        "click .edit-numbers": "openEditView",
        "click .export-to-pdf": "export_to_pdf"
    },

    initialize:function(){
        ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
    },

    openPastAudits: function(){
        this.audit_menu.show(new InventoryAuditListMenu({collection:this.collection}));
//        this.inventory_audit_form.show(new InventoryAuditListView());
        this.inventory_audit_form.show(new InventoryAuditDefaultView());
        this.$el.find('.inventory-audit-buttons').html('');
    },

    openNewAudit: function(){
        var inventoryAudit = new InventoryAudit();
        this.openAudit(inventoryAudit);
    },

    openAudit: function(inventoryAudit){
        this.audit_menu.show(new InventoryAuditDepartmentMenu());
        this.inventory_audit_form.show(new InventoryAuditItemsView({model:inventoryAudit}));
        this.$el.find('.inventory-audit-buttons').html(
            //'<button type="button" class="btn btn-default save-and-close">Save and Close</button>'+
            '<button type="button" class="btn btn-primary save-and-finish">Save and Finish</button>'
        );
    },

    load_audit: function(e){
        var audit_id = this.$el.find('#inventory-audit-dropdown').val();
        this.inventory_audit_form.show(new InventoryAuditDataView({model:this.collection.get(audit_id)}));
        this.$el.find('.inventory-audit-buttons').html(
            '<button type="button" class="btn btn-default edit-numbers">Edit Numbers</button>'+
            '<button type="button" class="btn btn-primary export-to-pdf">Export to PDF</button>'
        );
    },

    export_to_pdf: function(e){
        var audit_id = this.$el.find('#inventory-audit-dropdown').val();
        var audit = this.collection.get(audit_id);
        window.location = '/index.php/items/pdf_inventory_audit/'+audit.get('id');
    },

    save_and_finish: function(e){

    },

    openEditView: function(e){
        var audit_id = this.$el.find('#inventory-audit-dropdown').val();
        this.openAudit(this.collection.get(audit_id));
    },

    update_list: function(){
        var value = this.$el.find('#category-or-department option:selected').text();
        var category_or_department = this.$el.find('#category-or-department option:selected').data('type');
        // Filter list
        if (category_or_department == 'department') {
            var item_list = App.data.items.where({department: value});
        } else {
            var item_list = App.data.items.where({category: value});
        }

        this.$el.find('#inventory-list tr').hide();
        if (value == 'All Departments and Categories') {
            this.$el.find('#inventory-list tr').show();
        }
        else {
            _.each(item_list, function(item){
                $('.item_'+item.get('item_id')).show();
            });
        }
        this.$el.find('#inventory-list tr').css('backgroundColor', '');
        this.$el.find('#inventory-list tr:visible').each(function (i) {
            if ((i+1) % 2 == 0) $(this).css('backgroundColor', '#f5f5f5');
        });
    },

    onRender: function(){
        this.openNewAudit();
    }

});

var InventoryAuditDataView = Backbone.Marionette.ItemView.extend({
    template: JST['items/templates/inventory_audit_data.html'],
    onRender:function(){
        //this.$el.loadMask();
        var that = this;
        this.model.fetch({
            "success": function (model) {
                that.populateTable(model.get('items'));
                //that.populateTable(App.data.items.models);
            },
            "error": function (collection, response, options) {
                App.vent.trigger('notification', {
                    'msg': "Unable to retrieve inventory list.  Contact support if this continues to happen.",
                    'type': 'danger'
                });
            }
        });
    },
    populateTable:function(items){
        var that = this;

        _.each(items, function (item) {

            var difference = item.manualCount - item.currentCount;
            that.$el.find('#audit-inventory-list tbody').append(
                '<tr class="">'+
                '<td class="">'+
                item.item_number+
                //item.get('item_id')+
                '</td>'+
                '<td class="">'+
                item.name+
                '</td>'+
                '<td class="">'+
                item.currentCount+
                '</td>'+
                '<td class="">'+
                item.manualCount+
                '</td>'+
                '<td class="">'+
                difference+
                '</td>'+
                '<td class="">$'+
                _.round(item.unit_price * difference)+
                '</td>'+
                '<td class="">$'+
                _.round(item.cost_price * difference)+
                '</td>'+
                '</tr>'
            );
            that.$el.find('#audit-inventory-list tr:visible').each(function (i) {
                if (i % 2 == 0) $(this).css('backgroundColor', '#f5f5f5');
            });
        });
    },
    modelEvents: {

    },

    events: {

    },

    edit_receipt_content: function () {

    }
});

var InventoryAuditDefaultView = Backbone.Marionette.ItemView.extend({
    template: JST['items/templates/inventory_audit_default_screen.html']
});

var InventoryAuditItemsView = Backbone.Marionette.ItemView.extend({
    template: JST['items/templates/inventory_audit_items.html'],
    events: {
        "click .add-one": "addOne",
        "click .subtract-one": "subtractOne",
        "focus .quantity": "selectAll"
    },
    onRender:function(){
        if (this.model.get('id')) {
            this.populateTable(this.model.get('items'));
        } else {
            // Fetch a fresh inventory list
            var that = this;
            App.data.items.fetch({
                "success": function (model) {
                    that.populateTable(App.data.items.models);
                },
                "error": function (collection, response, options) {
                    App.vent.trigger('notification', {
                        'msg': "Unable to retrieve inventory list.  Contact support if this continues to happen.",
                        'type': 'danger'
                    });
                }
            });
        }

        //this.model.fetch();

        // Fetch and render AuditItems
    },

    populateTable:function(items){
        var that = this;
        var depts = {};
        var categories = {};
        this.$el.find('#department_opt_group').html('');
        this.$el.find('#category_opt_group').html('');

        _.each(items, function (item) {
            var department = typeof item['department'] != 'undefined' ? item['department'] : item.get('department')
            depts[department.toLowerCase()] = department;
            var category = typeof item['category'] != 'undefined' ? item['category'] : item.get('category');
            categories[category.toLowerCase()] = category;

            that.$el.find('#inventory-list tbody').append(
                '<tr class="item_'+(typeof item['id'] != 'undefined' ? item['id'] : item.get('item_id'))+'">'+
                    '<td class="item">'+
                        (typeof item['item_number'] != 'undefined' ? item['item_number'] : item.get('item_number'))+
                    '</td>'+
                    '<td class="price">'+
                        (typeof item['name'] != 'undefined' ? item['name'] : item.get('name'))+
                    '</td>'+
                    '<td class="audit-item-buttons">'+
                        '<input type="hidden" value="'+
                            (typeof item['id'] != 'undefined' ? item['id'] : item.get('item_id'))+
                        '" name="item_id[]">'+
                        '<div class="btn-group">'+
                            '<button class="btn btn-primary btn-sm subtract-one audit-option" data-value="subtract-one">-</button>'+
                            '<input class="quantity" value="'+
                                (typeof item['manualCount'] != 'undefined' ? item['manualCount'] : 0)+
                            '" name="inventory_level[]">'+
                            '<button class="btn btn-primary btn-sm add-one audit-option" data-value="add-one">+</button>'+
                        '</div>'+
                    '</td>'+
                '</tr>'
            );
            that.$el.find('#inventory-list tr:visible').each(function (i) {
                if (i % 2 == 0) $(this).css('backgroundColor', '#f5f5f5');
            });
        });
        _.sortKeysBy(depts);
        _.each(depts, function(dept) {
            $('#department_opt_group').append('<option data-type="department" value="'+dept+'">'+dept+'</option>');
        });
        _.sortKeysBy(categories);
        _.each(categories, function(cat) {
            $('#category_opt_group').append('<option data-type="category" value="'+cat+'">'+cat+'</option>');
        });

    },
    addOne: function(event){
        var quantityBox = $(event.target).closest('tr').find('.quantity');
        var currentQuantity = quantityBox.val();
        quantityBox.val(parseInt(currentQuantity) + 1);

        return false;
    },

    subtractOne: function(event){
        var quantityBox = $(event.target).closest('tr').find('.quantity');
        var currentQuantity = quantityBox.val();
        quantityBox.val(parseInt(currentQuantity) - 1);

        return false;
    },
    selectAll: function(event){
        $(event.target).select();
    }
});

var InventoryAuditListMenu = Backbone.Marionette.ItemView.extend({
    template: function() {
        return "<div>CHOOSE PAST AUDIT</div><select name='inventory-audit-dropdown' id='inventory-audit-dropdown' class='form-control'><option value=''></option></select>";
    },
    onRender:function(){
        var that = this;
        this.collection.fetch({
            data:{limit:10000},
            "success": function (model) {
                _.each(that.collection.models, function (audit) {
                    that.$el.find('#inventory-audit-dropdown').append("<option value='" + audit.cid + "'>" + audit.get('date').date + "</option>");
                });
            },
            "error": function (collection, response, options) {
                App.vent.trigger('notification', {
                    'msg': "Unable to retrieve inventory audit list.  Contact support if this continues to happen.",
                    'type': 'danger'
                });
            }
        });
    }
});

var InventoryAuditDepartmentMenu = Backbone.Marionette.ItemView.extend({
    template: function(){
        return "<div>CATEGORY OR DEPARTMENT</div>"+
                    "<select name='category-or-department' id='category-or-department' class='form-control'>"+
                        "<option value=''>All Departments and Categories</option>"+
                        "<optgroup id='department_opt_group' label='Departments'>"+
                        "</optgroup>"+
                        "<optgroup id='category_opt_group' label='Categories'>"+
                        "</optgroup>"+
                    "</select>"
    }
});