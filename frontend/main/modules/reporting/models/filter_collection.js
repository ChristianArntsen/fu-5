var FilterCollection = Backbone.Collection.extend({
	model: function(attrs, options) {
		return new reporting_models_report_filter(attrs, options);
	},
	_generateFilterModel: function (filter) {
		var type = filter.type;
		var filterModel;
		if (type == "date_range" || type == "static" || type == "moving") {
			filterModel = new reporting_models_report_filter_dateRange(filter);
		} else if (type == "dropdown") {
			filterModel = new reporting_models_report_filter_dropdown(filter);
		} else if (type == "month") {
			filterModel = new reporting_models_report_filter_month(filter);
		} else if (type == "number") {
			filterModel = new reporting_models_report_filter_number(filter);
		} else if (type == "text") {
			filterModel = new reporting_models_report_filter_text(filter);
		} else {
			filterModel = new reporting_models_report_filter(filter);
		}
		if(filter.selectedColumn){
			filterModel.set("aggregate", filter.selectedColumn.get("aggregate", ""));
		}
		filterModel.collection = this;
		if(filterModel.get("operator") == undefined)
			filterModel.set("operator", "=");
		return filterModel;
	},
	"addFilter":function(newFilter){
		if(!newFilter){
			this.add(new reporting_models_report_filter({},{collection:this}));
		} else {
			var filter = this._generateFilterModel(newFilter);
			this.add(filter);
		}
	},
	"replaceFilter":function(oldFilter,newFilter){
		var index = this.indexOf(oldFilter);
		this.remove(this.at(index));
		this.add(newFilter,{at:index});
	},
	comparator: "label"

});