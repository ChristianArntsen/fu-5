var DateGrouping = Backbone.Model.extend({
	defaults: {
		grouping:"",
		type:"date_range",
		typeOfMovingRange:"",
		movingRange:"",
		column:"",
        start_value:"",
        end_value:"",
		intervals:["none","hour","day","month","quarter","year"],
        loading:"",
        start_locked:false
	},
	setToDynamicMode: function(){
        this.set("start_value",null);
        this.set("end_value",null);
        this.set("type","moving");
	},
    setToStaticMode: function(){
        this.set("typeOfMovingRange",null);
        this.set("movingRange",null);
        this.set("type","static");
    },
    loadDefaultRange:function(){
        this.setDateByLabel(this.get("movingRange"));
    },
    setDateByLabel:function(movingrange){
        if(movingrange == "lastyear"){
            this.set("start_value", this.getRanges("Last Year")[0].format('YYYY-MM-DD'));
            this.set("end_value", this.getRanges("Last Year")[1].format('YYYY-MM-DD'));
        } else if(movingrange == "yesterday"){
            this.set("start_value", this.getRanges("Yesterday")[0].format('YYYY-MM-DD'));
            this.set("end_value", this.getRanges("Yesterday")[1].format('YYYY-MM-DD'));
        } else if(movingrange == "7"){
            this.set("start_value", this.getRanges("Last 7 Days")[0].format('YYYY-MM-DD'));
            this.set("end_value", this.getRanges("Last 7 Days")[1].format('YYYY-MM-DD'));
        } else if(movingrange == "30"){
            this.set("start_value", this.getRanges("Last 30 Days")[0].format('YYYY-MM-DD'));
            this.set("end_value", this.getRanges("Last 30 Days")[1].format('YYYY-MM-DD'));
        } else if(movingrange == "thismonth"){
            //this.set("movingRange", "thismonth");
            //this.set("typeOfMovingRange", "label");
        } else if(movingrange == "thisweek"){
            this.set("movingRange", "thisweek");
            this.set("typeOfMovingRange", "label");
        } else if(movingrange == "lastmonth"){
            this.set("start_value", this.getRanges("Last Month")[0].format('YYYY-MM-DD'));
            this.set("end_value", this.getRanges("Last Month")[1].format('YYYY-MM-DD'));
        } else if(movingrange == "lastweek"){
            this.set("start_value", this.getRanges("Last Week")[0].format('YYYY-MM-DD'));
            this.set("end_value", this.getRanges("Last Week")[1].format('YYYY-MM-DD'));
        }
    },
    rangeArray:{
        'Today': [moment(), moment(),"test"],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
        'Last 30 Days': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
    },
    getRanges: function(label){
        return this.rangeArray[label];
    }
});