var reporting_models_report_filter_dateRange = DateGrouping.extend({
    defaults: {
        "start_value": moment().format("MMMM DD, YYYY"),
        "end_value": moment().format("MMMM DD, YYYY"),
        "start_locked":false
    }
});
_.extend(reporting_models_report_filter_dateRange.prototype.defaults, reporting_models_report_filter.prototype.defaults);
