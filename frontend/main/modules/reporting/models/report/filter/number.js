var reporting_models_report_filter_number = reporting_models_report_filter.extend({
    defaults: {
        "type": "number",
        "default_operators":[" ",">=",">","=","<","<="]
    }
});
_.defaults(reporting_models_report_filter_number.prototype.defaults, reporting_models_report_filter.prototype.defaults);