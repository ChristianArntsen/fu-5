var reporting_models_report_filter_dropdown = reporting_models_report_filter.extend({
    defaults: {
        "default_values": [],
        "dropdown_values": [],
        "type": "dropdown"
    },
    getPossibleValues:function(){
        return this.collection.report.getColumnsPossibleValues(this.get("column"));
    }
});
_.extend(reporting_models_report_filter_dropdown.prototype.defaults, reporting_models_report_filter.prototype.defaults);