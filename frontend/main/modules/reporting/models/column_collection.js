var ColumnCollection = Backbone.Collection.extend({
	model: function(attrs, options) {
		return new Column(attrs, options);
	},
	swapItems : function(index1, index2) {
		this.models[index1] = this.models.splice(index2, 1, this.models[index1])[0];
	},
    comparator:'order'

});