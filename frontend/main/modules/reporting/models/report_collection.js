var ReportCollection = Backbone.Collection.extend({

	model: function(attrs, options){
		return new reporting_models_report(attrs, options);
	},
	url: REPORT_TEMPLATES_URL
});