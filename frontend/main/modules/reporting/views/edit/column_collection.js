var reporting_views_edit_columnCollection = Backbone.Marionette.CompositeView.extend({
    getChildView: function(item){
        item.set("possibleColumns",this.reportColumns);
        return reporting_views_edit_columnItem;
    },
    initialize: function(){
        var self = this;
        var self = this;
        var reportColumns = new ReportColumns();
        reportColumns.fetch({
            data: $.param({ report_type: this.options.report_base}) ,
            success:function(){
                reportColumns.initialize();
                self.render();
            }
        });
        this.reportColumns = reportColumns;
    },
    template: JST['reporting/templates/edit/ColumnCollection.html'],
    childViewContainer: "#column-table tbody",
    events:{
        "click #addColumn":"addColumn"
    },
    addColumn :function(event){
        this.collection.add({report_id:this.options.report_id})
    }
});
