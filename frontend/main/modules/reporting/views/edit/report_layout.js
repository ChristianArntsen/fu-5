var reporting_views_edit_reportLayout = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['reporting/templates/edit/ReportLayout.html'],
    events: {
        "click #saveReport":"saveReport"
    },
    saveReport:function(){
        var self = this;
        this.report.save(null, {
            success: function (model, response) {
                //self.render();
            },
            error: function (model, response) {
            }
        });
    },
    onRender:function(){
        var reportView= new reporting_views_edit_report({model:this.report});
        this.getRegion("report").show(reportView);
    },
    initialize:function(){
        //Fetch report
        var self = this;
        self.report = new reporting_models_report({id:this.id});
        self.report.fetch({
            "success": function (model) {
                self.report.initialize();
                self.getRegion("columns").show(
                    new reporting_views_edit_columnCollection({
                            collection:self.report.get("columns"),
                            report_id:self.id,
                            report_base:self.report.get("base")
                        })
                )
                self.getRegion('filters').show(new reporting_views_appliedFilters_layout({collection:self.report.get('filters'),model:self.report}) );

                var groupingModel = new Backbone.Model({editableArray:self.report.get("grouping")})
                var tagsModel = new Backbone.Model({tags:self.report.get("tags")})
                var orderingModel = new Backbone.Model({editableArray:self.report.get("ordering")})

                var groupingView= new reporting_views_edit_arrayEdit({model:groupingModel});
                var tagsView = new reporting_views_edit_tags({model:tagsModel});
                var orderingView = new reporting_views_edit_arrayEdit({model:orderingModel});

                self.getRegion("grouping").show(groupingView);
                self.getRegion("tags").show(tagsView);
                self.getRegion("ordering").show(orderingView);


                self.report.get("columns").on("change",function(){
                    self.getRegion('filters').show(new reporting_views_appliedFilters_layout({collection:self.report.get('filters'),model:self.report}) );
                },self);

                self.listenTo(self.report,'change:base',self.initialize);
            }
        })
    },

    regions: {
        report: '#r-report',
        columns: '#r-columns',
        grouping: '#r-grouping',
        tags: '#r-tags',
        ordering: '#r-ordering',
        filters: '#r-filters'
    }
});