var reporting_views_edit_arrayEdit = EpoxyMarionetteView.extend({
    template: JST['reporting/templates/edit/ArrayEdit.html'],
    tagName:"div",
    onRender:function(){
    },
    events:{
        "click #addItem":"addItem",
        "click #removeItem":"removeItem",
        "input input":"updateItem"
    },
    updateItem:function(event){
        var obj = $(event.currentTarget);
        this.model.get("editableArray")[obj.attr("data-index")] = obj.val();
    },
    addItem:function(){
        this.model.get("editableArray").push("");
        this.render();
    },
    removeItem:function(event){
        var text = $(event.currentTarget).next().val();
        var index = this.model.get("editableArray").indexOf(text);
        this.model.get("editableArray").splice(index,1);
        this.render();
    }
});