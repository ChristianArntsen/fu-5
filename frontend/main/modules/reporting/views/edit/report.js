var reporting_views_edit_report = EpoxyMarionetteView.extend({
    template: JST['reporting/templates/edit/Report.html'],
    tagName:"div",
    onRender:function(){
        if(this.options.simpleView){
            this.$el.find(".advanced_field").hide();
        }
    }
});