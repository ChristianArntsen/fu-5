reporting_views_edit_reportListLayout = Backbone.Marionette.LayoutView.extend({
    tagName: 'div',
    template: JST['reporting/templates/reports_edit.layout.html'],
    events: {
    },
    onRender:function(){
        this.getRegion("reports").show(new reporting_views_edit_reportList({collection:this.reports}))
    },
    initialize:function(){
        var self = this;
        this.tabs = new TabsCollection();
        this.reports = new ReportCollection();
        this.reports.on('request', function() {
            self.$el.find("#loading").show();
            self.$el.find("#content").hide();
        });
        this.reports.on('sync', function() {
            self.$el.find("#loading").hide();
            self.$el.find("#content").show();
        });
    },

    regions: {
        reports: '#r-reports'
    }
});
