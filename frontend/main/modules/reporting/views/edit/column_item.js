var reporting_views_edit_columnItem = EpoxyMarionetteView.extend({
    template: JST['reporting/templates/edit/ColumnItem.html'],
    tagName:"tr",
    onInitialize:function(){
        this.listenTo(this.model, 'change', function(){this.model.set("dirty",true)});
    },
    onRender:function(){
        var possibleColumns = [];
        this.model.get("possibleColumns").each(function(column){
            possibleColumns.push({
                id:column.get("id"),
                text:column.get("label") || column.get("table") +"."+column.get("column")
            })
        });
        this.$el.find('#column_dropdown').select2({
            placeholder: "Select a Column",
            data: possibleColumns
        });
        this.$el.find('#column_dropdown').val(this.model.get("column_id")).trigger("change");
    },
    events:{
        "click #deleteColumn":"deleteColumn",
        "change #column_dropdown":"selectColumn"
    },
    selectColumn:function(){
        if(this.$el.find('#column_dropdown').val() != null){
            this.model.set("column_id",this.$el.find('#column_dropdown').val());
        }
    },
    deleteColumn :function(event){
        this.model.trigger('destroy', this.model, this.model.collection);
    }
});