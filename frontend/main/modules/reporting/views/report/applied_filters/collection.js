var reporting_views_report_appliedFilters_collection = Backbone.Marionette.CollectionView.extend({
    getChildView: function(item){
        this.saveDataToChildren(item);
        return reporting_views_appliedFilters_item;
    },
    saveDataToChildren: function(item){
        item.set("columns",this.model.get("columns"));
    },
    onRemoveChild:function(){
        //App.vent.trigger('refresh-report');
    }
});
