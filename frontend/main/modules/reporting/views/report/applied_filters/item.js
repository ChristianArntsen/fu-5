var reporting_views_appliedFilters_item = EpoxyMarionetteView.extend({
	tagName: 'div',
	className: 'dropdown page-filter',
	template: JST['reporting/templates/report/applied_filters/item.html'],
	modelEvents: {
		'change': 'updateLabel',
		'change:operator':"handleFilterChange",
		'change:column':"handleFilterChange",
		'change:value':"handleFilterChange"
	},
	dropdownStatus: "closed",
	onInitialize:function(){
	},
	onRender: function(){

		if(this.model.get("dropdownStatus") == "open"){
			this.$el.find(".dropdown").addClass("open")
		}
		this.updateLabel();
	},
	onShow:function(){
		if(this.model.get("type")){
			var filterView = this.generateFilterView(this.model);
			this.getRegion('filter').show(filterView);
		}
	},
	events:{
		"click a#r-remove-filter":"removeFilter",
		"click a.delete-filter":"removeFilter",
		"click .dropdown-menu":"dropDownHandler",
		"shown.bs.dropdown":"dropdownShown",
		"hidden.bs.dropdown":"dropdownHidden"
	},
	handleFilterChange:function(){
		if(_.isEmpty(this.model.get("value")))
			return false;
		if((this.model.get("value_type") == "custom" || this.model.get("value_type") == "") && this.model.get("type")=="number" && (_.isEmpty(this.model.get("operator")) || this.model.get("operator") == " ") )
			return false;

		App.vent.trigger('refresh-report');
	},
	updateLabel:function(){
		this.model.set("filterLabel",this._generateLabel());
	},
	dropdownShown:function(){
		this.model.set("dropdownStatus","open");
	},
	dropdownHidden:function(){
		this.model.set("dropdownStatus","closed");
	},
	_generateLabel: function(){
		if(!this.model.get("selectedColumn")){
			return "Select a Column";
		}
        var label = this.model.get("selectedColumn").generateLabel();
		if(this.model.get("type") == "date_range" || this.model.get("type") == "moving" || this.model.get("type") == "static"){
			label += ": ";
			if(!this.model.get("start_locked")){
				label += (moment(this.model.get("start_value")).format('MMMM DD, YYYY') || "-Select Date- ") +
				" - ";
			}
			return label+
				(moment(this.model.get("end_value")).format('MMMM DD, YYYY') || "-Select Date- ");
		} else {
			return label +
					" " +
					this.model.get("operator") +
					" " +
					this.model.get("value");
		}
	},
	dropDownHandler:function(e){
		e.stopPropagation();
	},
	generateFilterView: function (model) {
		var type = model.get("type");
		var filterView = null;
        if (type == "teetime") {
            filterView = new reporting_views_report_filters_dateRange({model: model})
        } else if (type == "date_range" || type == "moving" || type == "static") {
			filterView = new reporting_views_report_filters_dateRange({model: model})
		} else if (type == "dropdown") {
			filterView = new reporting_views_report_filters_dropdown({model: model})
		} else if (type == "month") {
			filterView = new reporting_views_report_filters_month({model: model})
		} else if (type == "number") {
			filterView = new reporting_views_report_filters_number({model: model})
		} else if (type == "text") {
			filterView = new reporting_views_report_filters_text({model: model})
		}
		return filterView;
	},

	removeFilter : function(){
		this.model.destroy();
		App.vent.trigger('refresh-report');
	},
	regions: {
		filter: '#r-filter'
	}
});
