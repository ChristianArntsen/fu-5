var reporting_views_report_static = Backbone.Marionette.LayoutView.extend({
		template: JST['reporting/templates/report/static.html'],
		className: "static-report-container",
		initialize: function () {
			this.listenTo(this.model, 'change:data', this.refreshTable);
			this.listenTo(this.model, 'change:loading', this.handleLoading);
		},
		onRender:function(){
		},
		onShow: function(){
			this.refreshTable();
		},
		refreshTable:function(){
			if(this.model.get("data")){
				this.$el.html(this.model.get("data").data);
			}
		},
		behaviors: {
			LoadingBehavior: {
				behaviorClass: reporting_behaviors_loading
			}
		}
	}
);
