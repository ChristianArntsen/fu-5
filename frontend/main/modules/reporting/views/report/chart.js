var reporting_views_report_chart = Backbone.Marionette.ItemView.extend({
	
	modelEvents:{
		"change:chart_data":"updateChart"
	},
	
	initialize:function(){
	},
	behaviors: {
		LoadingBehavior: {
			behaviorClass: reporting_behaviors_loading
		}
	},
	onShow: function () {
		this.updateChart();
	},

    updateChart:function(){
        this.categoryColumn = this.model.get("columns").findWhere({"completeName":this.model.chartParameters.get("xAxis")});
        this.valueColumn = this.model.get("columns").findWhere({"completeName":this.model.chartParameters.get("yAxis")});
        this.categoryLabel = (this.categoryColumn.get("label") || this.categoryColumn.get("completeName"));
		if(this.model.chartParameters.get("visualGrouping")){
			this.categoryLabel += " by "+this.model.chartParameters.get("visualGrouping")
		}
        this.valueLabel = this.valueColumn.get("label") || this.valueColumn.get("completeName");
        if(this.model.get("chart_data") != ""){
            this._renderChart();
        }
    },

	_renderChart:function(){
        if(!this.$el.find("#myChart")){
            return;
        }
        this.$el.find("#myChart").highcharts({
        });
	},
    
    template: JST['reporting/templates/report/chart_high.html'],

});

