var reporting_views_report_tableSplit = Backbone.Marionette.LayoutView.extend({
	template: function(){return "<div></div>"},

	initialize: function () {
		this.listenTo(this.model, 'change:data', this.refreshTable);
	},
	refreshTable: function(){
		var self = this;
		var newModel = "";
		var myModels = [];
		this.regionManager.removeRegions();
		_.each(this.model.get("data"),function(data,key){
			newModel = self.model.deepClone();
			newModel.set("data",data);
			myModels.push(newModel);
			var safeKey = String(key).replace(/[\s\&\"\']/g,"_");
			if(safeKey==""){
				safeKey = "Empty"
			}
			self.$el.append("<div><h2>"+key+"</h2><div id='"+safeKey+"'></div></div><hr/>")
			self.addRegion("region-".safeKey, "#"+safeKey);
			self.getRegion("region-".safeKey).show(new reporting_views_report_tableBasic({model:newModel}));
		})
	}
});
