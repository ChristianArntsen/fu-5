var reporting_views_report_chart_editParameters = EpoxyMarionetteView.extend({
    className:"container-fluid",
    template: JST['reporting/templates/report/chart/edit_parameters.html'],
    behaviors: {
        "ModalDialogBehavior":{
            behaviorClass: reporting_behaviors_modal_dialog
        }
    },
    ui:{
        date_modifiers:".date_modifiers",
        split_by:".split_by",
        y_axis:".y_axis",
        y_axis_label:"#y-axis-label",
        x_axis_label:"#x-axis-label",
        selected_graph:".selected_graph button"
    },
    onShow:function(){
        this.handleSelectedGraph();
        this.handleXAxisChange();
    },
    events:{
        'click @ui.selected_graph': 'graphSelected'
    },
    graphSelected:function(e){
        var value = $(e.target).attr("data-value");
        this.model.set("selectedGraph",value);
    },
    modelEvents:{
        "change:selectedGraph":"handleSelectedGraph",
        "change:xAxis":"handleXAxisChange"
    },
    handleSelectedGraph:function(){
        var type = this.model.get("selectedGraph")
        if(type == "pie"){
            this.ui.x_axis_label.html("Category");
            this.ui.y_axis_label.html("Value");
            this.ui.date_modifiers.hide();
            this.ui.split_by.hide();
            this.model.set("splitBy","");
        } else {
            this.ui.x_axis_label.html("X Axis");
            this.ui.y_axis_label.html("Y Axis");
            this.ui.y_axis.show();
            this.ui.split_by.show();
            this.handleXAxisChange();
        }
        this.ui.selected_graph.removeClass("active");
        this.ui.selected_graph.filter('[data-value='+type+']').addClass("active");
    },
    handleXAxisChange:function(){
        var selectedColumn = this.model.get("possibleXColumns").findWhere({"completeName":this.model.get("xAxis")});
        var type = selectedColumn.get("type");
        if(type == "date_range"){
            this.ui.date_modifiers.show();
        } else {
            this.ui.date_modifiers.hide();
        }
    }
});
