var reporting_views_report_bar = reporting_views_report_chart.extend({
    colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572',
        '#FF9655', '#FFF263', '#6AF9C4'],
    _renderChart:function(){
        if(!this.$el.find("#myChart")){
            return;
        }
        var information = this._convertCSVToDatasets(this.model.get("chart_data"));
        this.$el.find("#myChart").highcharts({
            title: {
                text: this.categoryLabel + " and "+this.valueLabel,
                style: {
                    color: 'black'
                }
            },
            chart: {
                type: 'column'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: information[0],
                title: {
                    text: this.categoryLabel
                }

            },
            yAxis: {
                min: 0,
                title: {
                    text: this.valueLabel
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0,
                itemStyle: {
                    color: 'black'
                }
            },
            series: information[1],
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            }
        });
    },
    _convertCSVToDatasets:function(data){
        var self = this;
        var labels = [];
        //For each array index after 0, it's a dataset
        var datasets = [];
        _.each(data,function(row,rowNumber){
            _.each(row,function(column,columnNumber){
                if(rowNumber > 0 && columnNumber==0){
                    labels.push(column);
                }
                if(rowNumber == 0 && columnNumber>0){
                    if(column === ""){
                        column = "Empty";
                    }
                    datasets.push({
                        name:column,
                        data:[]
                    })
                }
                if(rowNumber > 0 && columnNumber>0){
                    datasets[columnNumber-1]["data"].push(column*1);
                }
            })
        })

        return [labels,datasets];
    }
});

Highcharts.theme = {
    colors: ['#279BC1', '#0DC4BD', '#2AD898','#E8CA00','#E7A72C','#E76E2C','#C43850','#BD4CB4','#5E53C9'],

    title: {
        style: {
            color: '#000',
            font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
        }
    },
    subtitle: {
        style: {
            color: '#666666',
            font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
        }
    },
    yAxis: {
        color: '#95CBCE',
        lineColor: '#95CBCE',
        tickColor: '#95CBCE',
        gridLineColor: '#95CBCE',
        gridLineWidth: 1,
        title: {
            style: {
                color: 'black'
            }
        },
        labels: {
            style: {
                color: 'black'
            }
        }
    },
    xAxis: {
        color: '#95CBCE',
        gridLineColor: 'transparent',
        title: {
            style: {
                color: 'black'
            }
        },
        labels: {
            style: {
                color: 'black',
                fill: 'black'
            }
        }

    },
    legend: {
        itemStyle: {
            font: '9pt Trebuchet MS, Verdana, sans-serif',
            color: 'black'
        },
        itemHoverStyle:{
            color: 'gray'
        }
    }
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);