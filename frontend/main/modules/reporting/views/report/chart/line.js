var reporting_views_report_chart_line = reporting_views_report_bar.extend({
    _renderChart:function(){
        if(!this.$el.find("#myChart")){
            return;
        }
        var information = this._convertCSVToDatasets(this.model.get("chart_data"));
        this.$el.find("#myChart").highcharts({
            title: {
                text: this.categoryLabel + " and "+this.valueLabel
            },
            chart: {
                zoomType: 'x'
            },
            xAxis: {
                categories: information[0],
                title: {
                    text: this.categoryLabel
                }
            },
            yAxis: {
                title: {
                    text: this.valueLabel
                },
                min: 0
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: information[1]
        });
    }
});

