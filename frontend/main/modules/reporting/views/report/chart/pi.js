var reporting_views_report_chart_pi = reporting_views_report_chart.extend({
    _renderChart:function(){
        if(!this.$el.find("#myChart")){
            return;
        }
        var information = this._convertCSVToDatasets(this.model.get("chart_data"));


        this.$el.find("#myChart").highcharts({
            title: {
                text: this.categoryLabel + " and "+this.valueLabel,
                style: {
                    color: 'black'
                }
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    },
                    showInLegend: true
                }
            },
            series: [{
                type:'pie',
                name: this.valueLabel,
                data:information
            }]
        });
    },
    _convertCSVToDatasets:function(data){
        var datasets = [];
        _.each(data,function(row,rowNumber){
            if(rowNumber > 0){
                datasets.push([
                    row[0],
                    row[1]*1
                ])
            };
        })

        return datasets;
    }
});

