var reporting_views_report_filters_dropdown = reporting_views_report_filters_filter.extend({
    template: JST['reporting/templates/report/filter/dropdown.html'],
    className: 'nr-filter-container form-inline',

    onRender: function(){
        var data = [{id:this.model.get("value"),text:this.model.get("value")}];
        if(this.model.get("dropdown_values").length != 0){
            data = data.concat(this.model.get("dropdown_values"));
        }
        this.$el.find('#report-dropdown').select2({
            placeholder: "Type a value",
            data: data,
            closeOnSelect: false,
            width: '100%'
        });
    },
    onInitialize:function(){
        this.loadPossibleValues();

        $(document).on("click",".select2-container",function(e){
            e.stopPropagation();
        })
        $(document).on("click",".select2-selection__choice",function(e){
            e.stopPropagation();
        })
    },
    behaviors: {
        LoadingBehavior: {
            behaviorClass: reporting_behaviors_loading
        }
    },
    loadPossibleValues:function(){
        var self = this;
        self.model.set("loading",true);
        var promise = this.model.getPossibleValues();
        promise.done(function(response){
            self.model.set("default_values",
                _.pluck(response,self.model.get("column")));
            var data = [];
            _.each(self.model.get("default_values"),function(value){
                data.push({
                    id:value,
                    text:value
                });
            })
            self.model.set("dropdown_values",data);
            self.model.set("loading",false);
            self.render();
        })
        return promise;


    }
});
