var reporting_views_report_modalSave = ModalView.extend({

    tagName: 'div',
    template: JST['reporting/templates/report/modal_save.html'],

    events: {
        "click .save": "save"
    },

    initialize: function(){
        ModalView.prototype.initialize.call(this, {extraClass: 'modal-lg'});
        var tagsModel = new Backbone.Model({tags:this.model.get("tags")})
        this.tags = new reporting_views_edit_tags({model:tagsModel})
        this.details = new reporting_views_edit_report({model:this.model,simpleView:true})
    },

    render: function(){
        this.$el.html(this.template(this.model.attributes));
        this.$el.find("#r-tags").html(this.tags.render().$el);
        this.$el.find("#r-details").html(this.details.render().$el);
        return this;
    },
    save:function(){
        var self = this;
        var attributes = this.model.toJSON();
        if(attributes.base_report == null){
            attributes.base_report = attributes.id;
        }
        delete(attributes.id);
        delete(attributes.saving_for_report);
        delete(attributes.comparative);
        delete(attributes.comparativeType);
        delete(attributes.comparativeReports);
        var newReport = new reporting_models_report(attributes);
        var promise = newReport.save();
        var view = this;
        promise.success(function(response){
            view.hide();
            App.vent.trigger('notification', {msg: 'Report saved', type: 'success'});
            self.model.set("id",response.id);
            self.model.trigger('report-saved');
        })
        promise.error(function(response){
            view.hide();
            App.vent.trigger('notification', {msg: 'Problem saving the report', type: 'error'});
        })
    }

});
