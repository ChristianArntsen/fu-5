/**
 * Created by Brendon on 5/22/2015.
 */
var reporting_behaviors_table_rowsSummary = Backbone.Marionette.Behavior.extend({
    defaults: {
        table:".table_id"
    },
    ui: function(){
        return {
            table: this.options.table
        }
    },
    behaviors: {
        SelectingBehavior:{
            behaviorClass: reporting_behaviors_table_rowSelection
        }
    },
    events:{
        "selectablestop @ui.table":'handleSelectStop',
        "selectableselecting @ui.table":'handleSelecting'
    },
    handleSelectStop:function(){

        var totals = [];
        this.$el.find( ".ui-selected").each(function() {

            $(this).children('td').each (function(key) {
                var value = $(this).attr("data-value");
                if(!totals[key]){
                    if($.isNumeric(value)){
                        totals[key] = parseFloat(value)
                    } else {
                        totals[key] = "";
                    }
                } else {
                    if($.isNumeric(value) && $.isNumeric(totals[key])){
                        totals[key] += parseFloat(value)
                    }
                }
            });
        });
        if(this.summaryRow){
            this.summaryRow.remove();
        }
        this.summaryRow = this.buildSummaryRow(totals);
        this.$el.find( ".ui-selected").last().after(this.summaryRow );
    },
    handleSelecting:function(){
        if(this.summaryRow){
            this.summaryRow.remove();
        }
    },
    buildSummaryRow:function(totals){
        var row =  $("<tr>", {class: "summary"});
        _.each(totals,function(value,key){
            if($.isNumeric(value)){
                value = parseFloat(value).toFixed(2)
            }
            row.append("<td>"+ value +"</td>");
        })
        row.append("</tr>");
        return row;
    }
});