var reporting_behaviors_modal_dialog = Backbone.Marionette.Behavior.extend({
    onShow:function(){
        this.view.modalWindow.find(".modal-footer").html("<button type='button' data-dismiss='modal' class='btn btn-default pull-right'>Okay</button>");
    },
    onInitialize:function(){
    },
    behaviors: {
        ModalBehavior:{
            behaviorClass: reporting_behaviors_modal,
            title: "Information"
        }
    }
});