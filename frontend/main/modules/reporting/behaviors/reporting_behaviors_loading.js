/**
 * Created by Brendon on 5/22/2015.
 */
var reporting_behaviors_loading = Backbone.Marionette.Behavior.extend({
    defaults:{
        numberOfLoadingThreads:0
    },
    modelEvents:{
        "change:loading":"handleLoading"
    },
    handleLoading: function() {
        if(this.view.model == undefined){
            return;
        }

        if(this.options.numberOfLoadingThreads > 0 || this.view.model.get("loading") > 0){
            if(this.view.$el.find(".spinner").length){
                return;
            }
            var opts = {
                lines: 14 // The number of lines to draw
                , length: 22 // The length of each line
                , width: 3 // The line thickness
                , radius: 17 // The radius of the inner circle
                , scale: 3 // Scales overall size of the spinner
                , corners:.3 // Corner roundness (0..1)
                , color: '#000' // #rgb or #rrggbb or array of colors
                , opacity: 0.1 // Opacity of the lines
                , rotate: 81 // The rotation offset
                , direction: 1 // 1: clockwise, -1: counterclockwise
                , speed: 1 // Rounds per second
                , trail: 89 // Afterglow percentage
                , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
                , zIndex: 2e9 // The z-index (defaults to 2000000000)
                , className: 'spinner' // The CSS class to assign to the spinner
                , top: '1%' // Top position relative to parent
                , left: '50%' // Left position relative to parent
                , shadow: true // Whether to render a shadow
                , hwaccel: true // Whether to use hardware acceleration
                , position: 'absolute' // Element positioning
            }
            this.spinner = new Spinner(opts).spin();

            if(this.$el.find("#loading").length){
                this.$el.find("#loading").append(this.spinner.el);
            } else {
                this.$el.append(this.spinner.el);
            }

        } else {
            if(this.spinner){
                this.spinner.stop();
                this.spinner = undefined;

            }
        }
    },
    onLoadingStarted:function(){
        this.options.numberOfLoadingThreads++;
        this.handleLoading();
    },
    onLoadingFinished:function(){
        this.options.numberOfLoadingThreads--;
        this.handleLoading();
    },
    onRender:function(){
        this.handleLoading();
    }
});