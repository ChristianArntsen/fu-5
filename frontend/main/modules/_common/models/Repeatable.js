var Repeatable = Backbone.JsonRestModel.extend({

    type: 'repeated',
    defaults: {
        _stepsCompleted: 0,
        period: null,
        dtstart: null,
        until: null,
        count: null
    },

    initialize: function(options){
        this.on('change:period change:bymonth change:bymonthday change:interval change:byhour change:bysetpos change:byday', this.calculateStepsCompleted);
        this.calculateStepsCompleted();
        return Backbone.JsonRestModel.prototype.initialize.call(this, options);
    },

    set: function (key, val, options) {

        // Handle both `"key", value` and `{key: value}` -style arguments.
        var attrs;
        if(typeof key === 'object'){
            attrs = key;
            options = val;
        }else{
            (attrs = {})[key] = val;
        }

        // Make sure all array values care cast to integers to prevent
        // the JS RRule parser from hanging up
        attrs = _.mapObject(attrs, function(value, key){

            if(key == 'dtstart' || key == 'until'){
                if(!value){
                    return value;
                }
                return moment(value).toDate();
            }

            if(value !== null && typeof(value) == 'object' || typeof(value) == 'array'){
                if(value.length == 0){
                    return null;
                }
                value = _.map(value, function(val, index){
                    return parseInt(val);
                });
            }
            return value;
        });

        // Byday and byweekday are the same field. Our JS RRule library uses byweekday
        // but the server uses byday. Make sure we just set both variables any time we set one
        if (typeof(attrs.byweekday) != 'undefined') {
            attrs.byday = attrs.byweekday;
        }
        if (typeof(attrs.byday) != 'undefined') {
            attrs.byweekday = attrs.byday;
        }

        // Map the frequency STRING to INT, eg. 'YEARLY' to 0
        if(typeof(attrs.freq) == 'string'){
            var freqInt = RRule.FREQUENCIES.indexOf(attrs.freq);
            if(freqInt !== -1){
                attrs.freq = freqInt;
            }
        }

        if(typeof attrs.freq != 'undefined' && attrs.freq !== null && attrs.period == null){
            var jan = [1, 4, 7, 10];
            var feb = [2, 5, 8, 11];
            var mar = [3, 6, 9, 12];

            if(attrs.freq == RRule.YEARLY){
                attrs.period = 'yearly';

            }else if(attrs.freq == RRule.MONTHLY &&
                (
                    JSON.stringify(jan) == JSON.stringify(attrs.bymonth) ||
                    JSON.stringify(feb) == JSON.stringify(attrs.bymonth) ||
                    JSON.stringify(mar) == JSON.stringify(attrs.bymonth)
                )
            ){
                attrs.period = 'quarterly';

            }else if(attrs.freq == RRule.MONTHLY){
                attrs.period = 'monthly';
            }else if(attrs.freq == RRule.WEEKLY){
                attrs.period = 'weekly';
            }else{
                attrs.period = 'daily';
            }
        }

        return Backbone.Model.prototype.set.call(this, attrs, options);
    },

    calculateStepsCompleted: function(){
        var stepsCompleted = 0;
        if(this.get('period')){
            stepsCompleted++;
        }
        if(this.get('bymonth') || this.get('interval') || this.get('byday')){
            stepsCompleted++;
        }
        if(this.get('bymonthday') || typeof(this.get('bysetpos')) != 'undefined'){
            stepsCompleted++;
        }
        if(typeof(this.get('byhour')) !== 'undefined'){
            stepsCompleted++;
        }
        this.set('_stepsCompleted', stepsCompleted);
    },

    isComplete: function(){
        this.calculateStepsCompleted();
        return (this.get('_stepsCompleted') >= 4);
    }
});

var RepeatableCollection = Backbone.JsonRestCollection.extend({
    model: Repeatable
});

Backbone.modelFactory.registerModel(Repeatable, RepeatableCollection);