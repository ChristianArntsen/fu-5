var CurrencyCell = Backgrid.CurrencyCell = Backgrid.NumberCell.extend({

    /** @property */
    className: "currency-cell",
    formatter: Backgrid.NumberFormatter,
    render: function(){
        var value = this.model.get(this.column.get("name"));
        if(value < 0){
            this.$el.attr('style',"color:red");
        }
        var value = this.formatter.fromRaw(value, this.model);
        this.$el.text( SETTINGS.currency_symbol+value );
        return this;

    }
});