var LineItem = Backbone.JsonRestModel.extend({

    type: 'item',
    relations: {
        taxes: {
            type: 'itemTax',
            many: true
        }
    },

    idAttribute: 'itemId',
    defaults: {
        name: '',
        unitPrice: 0,
        qty: 1,
        tax: 0,
        subtotal: 0,
        total: 0,
        discount: 0,
        taxes: []
    },

    initialize: function(){
        if(!this.has('basePrice')){
            this.set('basePrice', this.get('unitPrice'));
        }
        this.calculateTotals();
        this.on('change:qty change:unitPrice', this.calculateTotals);
    },

    calculateTotals: function(){

        var subtotal = this.getSubtotal(this.get('unitPrice'), this.get('qty'), this.get('discount'));
        var tax = this.calculateTax(subtotal, this.get('taxes').models);
        var total = this.getTotal(subtotal, tax, true);

        this.set({
            'subtotal': subtotal,
            'tax': tax,
            'total': total
        });
    },

    calculateTax: function(subtotal, taxList, tax_included){

        var totalTax = new Decimal(0);
        var subtotal = new Decimal(subtotal);
        var item = this;
        var taxes = [];

        _.each(taxList, function(tax){
            taxes.push(_.clone(tax.toJSON()));
        });

        if(tax_included === undefined){
            tax_included = false;
        }
        if(!taxes || taxes.length == 0){
            return 0;
        }

        var total_percent = 0.00;
        _.each(taxes,function(tax){
            if(!tax.percent || tax.percent <= 0)return;
            total_percent = new Decimal(total_percent).plus(tax.percent);
        });
        total_percent = new Decimal(total_percent);

        _.each(taxes, function(tax, i){

            var percentage = new Decimal(tax.percent);
            if(!tax || percentage.isNaN() || percentage.isZero()){
                return true;
            }

            var taxAmount = new Decimal(0);
            if(tax_included){
                var actual_subtotal = subtotal.dividedBy( total_percent.dividedBy(100).plus(1) ).toDP(2);
                var percent = percentage.dividedBy(100);
                taxAmount = actual_subtotal.times(percent).toDP(2);

            }else if(tax.cumulative == "1"){
                var percent = percentage.dividedBy(100);
                taxAmount = subtotal.plus(totalTax).times(percent).toDP(2);

            }else{
                var percent = percentage.dividedBy(100);
                taxAmount = subtotal.times(percent).toDP(2);
            }

            tax.amount = taxAmount.toNumber();
            totalTax = totalTax.plus(taxAmount.toNumber());
        });

        this.get('taxes').reset(taxes, {silent: true});
        return totalTax.toDP(2).toNumber();
    },

    getSubtotal: function(price, qty, discount){
        var price = new Decimal(price);
        var discount = new Decimal(discount);
        var qty = new Decimal(qty);

        var percent = new Decimal(100).minus(discount).dividedBy(100);
        var discounted = price.times(percent).toDP(2).times(qty);

        return discounted.toDP(2).toNumber();
    },

    getTotal: function(subtotal, tax, taxable){
        var subtotal = new Decimal(subtotal);
        var tax = new Decimal(tax);

        if(taxable){
            var total = subtotal.plus(tax);
        }else{
            var total = subtotal;
        }

        return total.toDP(2).toNumber();
    }
});

var LineItemCollection = Backbone.JsonRestCollection.extend({
    model: LineItem
});

Backbone.modelFactory.registerModel(LineItem, LineItemCollection);