var ModelFactory = function() {
    this.registeredModels = {};
    this.registeredCollections = {};
};

var JsonRestDataStore = function(){
    this.storedIncludes = {};
};

JsonRestDataStore.prototype.isEmpty = function(){
    return this.storedIncludes.length == 0;
};

JsonRestDataStore.prototype.set = function(item){

    var type = "";
    var id = "";
    if(typeof item == "undefined" || typeof item.type == 'undefined'){
        throw "Unable to store data, missing 'type'.";
    }
    type = item.type;

    if(!this.storedIncludes[type])
        this.storedIncludes[type] = {};

    this.storedIncludes[type][String(item.id)] = item;
};

JsonRestDataStore.prototype.get = function(type, id){

    if(this.storedIncludes[type] && this.storedIncludes[type][String(id)]){
        return this.storedIncludes[type][String(id)];
    }
    return false;
};

ModelFactory.prototype.registerModel = function(model, collection) {

    this.registeredModels[model.prototype.type] = model;
    if(collection)
        this.registeredCollections[model.prototype.type] = collection;
};

ModelFactory.prototype.create = function(attributes, type, store) {

    if(this.registeredModels[type]){
        var model = this.registeredModels[type];
        return new model(attributes, {type: type, store: store});
    }

    console.warn("Model type '"+type+"' not found");
    return new Backbone.Model(attributes);
};

ModelFactory.prototype.createCollection = function(items, type, store){

    if(this.registeredCollections[type]){
        var collection = this.registeredCollections[type];
        return new collection(items, {store: store});
    }
    console.warn("Collection type '"+type+"' not found");
    return new Backbone.Collection(items);
};

Backbone.modelFactory = new ModelFactory();

Backbone.JsonRestCollection = Backbone.PageableCollection.extend({
    //
    store: null,

    initialize: function(attrs, options){
        if(typeof this.validator === 'undefined')
            this.validator = new JsonRestValidator();

        if(options && options.store && options.store !== null){
            this.store = options.store;
        }else{
            this.store = new JsonRestDataStore();
        }
    },

    parse: function(res,options){
        if(!this.store){
            this.store = new JsonRestDataStore();
        }
        var newState = this.parseState(res, _.clone(this.queryParams), _.clone(this.state), options);
        if (newState) this.state = this._checkState(_.extend({}, this.state, newState));

        if(typeof this.validator === 'undefined')
            this.validator = new JsonRestValidator();

        // validate the result received from the API endpoint
        this.valid = this.validator.validate(res,true);

        if(!this.valid) throw this.get_last_error();
        this.result = res;
        this.raw = res;

        if(res.included){
            _.each(res.included, function(item) {
                this.store.set(item);
            }, this);
        }
        // return data
        if(res.data && res.data.length){
            return res.data;
        }
        return [];
    },

    rawToJSON:function(){
        var tmp = {data:[]};
        for(var i=0;i<this.models.length;i++){
            tmp.data.push(this.models[i].rawToJSON());
        }
        this.raw = tmp;
        return this.raw;
    },
    get_last_error: function(){
        return this.validator.get_last_error();
    },
    get_warnings: function(){
        return this.validator.get_warnings();
    },
    toRestJSON: function(options) {
        return this.map(function(model) { return model.toRestJSON(options); });
    },
    _super: Backbone.Collection.prototype,


    // Any `state` or `queryParam` you override in a subclass will be merged with
    // the defaults in `Backbone.PageableCollection` 's prototype.
    state: {

        // You can use 0-based or 1-based indices, the default is 1-based.
        // You can set to 0-based by setting ``firstPage`` to 0.
        firstPage: 0,
        pageSize:50
    },

    // You can configure the mapping from a `Backbone.PageableCollection#state`
    // key to the query string parameters accepted by your server API.
    queryParams: {
        // Setting a parameter mapping value to null removes it from the query string
        currentPage: null,
        // Any extra query string parameters are sent as is, values can be functions,
        // which will be bound to the pageable collection instance temporarily
        // when called.
        start: function () { return this.state.currentPage * this.state.pageSize; },
        pageSize: "limit"
    },
    parseState: function (resp, queryParams, state, options) {
        if(resp.meta && resp.meta.total)
            return {totalRecords: resp.meta.total};
        return null;
    }
    
});

Backbone.JsonRestModel = Backbone.Model.extend({
    //
    _last_error:'',
    store: null,

    initialize: function(attrs, options){

        if(typeof this.validator === 'undefined')
            this.validator = new JsonRestValidator();

        if(options && options.type){
            this.type = options.type;
        }

        if(options && options.store && options.store !== null) {
            this.store = options.store;
        }else if(this.collection && this.collection.store && this.collection.store !== null){
            this.store = this.collection.store;
        }else{
            this.store = new JsonRestDataStore();
        }
    },

    set: function(key, val, options){

        // Handle both `"key", value` and `{key: value}` -style arguments.
        var attrs;
        if(typeof key === 'object'){
            attrs = key;
            options = val;
        }else{
            (attrs = {})[key] = val;
        }

        var store = this.store;
        var relations = this.relations;

        if(relations){

            // Loop through all attributes being set on the model
            // Turn related object keys into actual models/collections
            attrs = _.mapObject(attrs, function(value, key){

                // If this object property has no relationship, don't mess with it
                if(!relations[key] || typeof value != 'object'){
                    return value;
                }
                var relation = relations[key];

                if(relation.many){
                    var values = [];

                    // Make sure the list of objects is a proper array, objects cause problems
                    _.each(value, function(value){
                        values.push(value);
                    });
                    value = Backbone.modelFactory.createCollection(values, relation.type, store);
                }else{
                    value = Backbone.modelFactory.create(value, relation.type, store);
                }
                return value;
            });
        }

        return Backbone.Model.prototype.set.call(this, attrs, options);
    },

    parse: function(data){

        if(typeof this.validator === 'undefined')
            this.validator = new JsonRestValidator();
        if(!this.store && this.collection && this.collection.store){
            this.store = this.collection.store;
        }

        if(_.isNull(this.store)){
            console.error("The model store is null for some reason.");
            if(_.isUndefined(this.collection)){
                console.error("The collection on the model isn't set.");
            }
        }
        var included = false;
        if(data.included){
            included = data.included;
        }

        if(typeof data.data !== 'undefined'){
            data = data.data;
        }

        this.valid = this.validator.validate_resource(data);

        if(!this.valid)return {};
        this.raw = data;

        if(included){
            _.each(included, function(item) {
                this.store.set(item);
            }, this);
        }

        var attributes = {};
        if(data.attributes) {
            attributes = JSON.parse(JSON.stringify(data.attributes));
        }

        if(data.relationships){
            var related = this.gatherRelated(data.relationships);
            _.extend(attributes, related);
        }

        // merge id into attributes
        attributes.id = data.id;
        this.type = data.type;

        return attributes;
    },

    gatherRelated: function(related){

        var model = this;
        var store = this.store;
        var relatedData = {};

        _.each(related, function(value, key){

            var data = value.data;
            if(typeof data == 'undefined' || !data){
                return false;
            }
            var newData = {};

            if(data[0]){
                newData[key] = [];
                _.each(data, function(obj, index){
                    var storedObj = store.get(obj.type, obj.id);
                    storedObj.attributes.id = obj.id;

                    if(storedObj.relationships){
                        var subRelated = model.gatherRelated(storedObj.relationships);
                        if(!_.isEmpty(subRelated)) {
                            _.extend(storedObj.attributes, subRelated);
                        }
                    }
                    newData[key][index] = storedObj.attributes;
                });

            }else if(data.type){
                var storedObj = store.get(data.type, data.id);
                storedObj.attributes.id = data.id;

                if(storedObj.relationships){
                    var subRelated = model.gatherRelated(storedObj.relationships);

                    if(!_.isEmpty(subRelated)){
                        _.extend(storedObj.attributes, subRelated);
                    }
                }
                newData[key] = storedObj.attributes;
            }

            _.extend(relatedData, newData);
        });

        return relatedData;
    },

    toRestJSON: function(){
        if (this._isSerializing) {
            return this.id || this.cid;
        }
        this._isSerializing = true;
        var json = {
            type: this.rowtype,
            attributes: _.clone(this.attributes)
        };

        if(this.id){
            json.id = this.id;
        }

        _.each(json.attributes, function(value, name) {
            if(value && _.isFunction(value.toRestJSON)){
                json['attributes'][name] = value.toRestJSON();
            }
        });
        this._isSerializing = false;
        return json;
    },

    sync: function(method, model, options){

        var methodMap = {
            'create': 'POST',
            'update': 'PUT',
            'patch': 'PATCH',
            'delete': 'DELETE',
            'read': 'GET'
        };

        var type = methodMap[method];

        // Default options, unless specified.
        _.defaults(options || (options = {}), {
            emulateHTTP: Backbone.emulateHTTP,
            emulateJSON: Backbone.emulateJSON
        });

        // Default JSON-request options.
        var params = {type: type, dataType: 'json'};

        // Ensure that we have a URL.
        if (!options.url) {
            params.url = _.result(model, 'url') || urlError();
        }

        // Ensure that we have the appropriate request data.
        if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch')) {
            params.contentType = 'application/json';
            var payload = options.attrs || model.toRestJSON(options);
            params.data = JSON.stringify({
                data: payload
            });
        }

        // For older servers, emulate JSON by encoding the request into an HTML-form.
        if (options.emulateJSON) {
            params.contentType = 'application/x-www-form-urlencoded';
            params.data = params.data ? {model: params.data} : {};
        }

        // For older servers, emulate HTTP by mimicking the HTTP method with `_method`
        // And an `X-HTTP-Method-Override` header.
        if (options.emulateHTTP && (type === 'PUT' || type === 'DELETE' || type === 'PATCH')) {
            params.type = 'POST';
            if (options.emulateJSON) params.data._method = type;
            var beforeSend = options.beforeSend;
            options.beforeSend = function(xhr) {
                xhr.setRequestHeader('X-HTTP-Method-Override', type);
                if (beforeSend) return beforeSend.apply(this, arguments);
            };
        }

        // Don't process data on a non-GET request.
        if (params.type !== 'GET' && !options.emulateJSON) {
            params.processData = false;
        }

        // Pass along `textStatus` and `errorThrown` from jQuery.
        var error = options.error;
        options.error = function(xhr, textStatus, errorThrown) {
            options.textStatus = textStatus;
            options.errorThrown = errorThrown;
            if (error) error.call(options.context, xhr, textStatus, errorThrown);
        };

        // Make the request, allowing the user to override any Ajax options.
        var xhr = options.xhr = Backbone.ajax(_.extend(params, options));
        model.trigger('request', model, xhr, options);
        return xhr;
    },

    rawToJSON:function(){
        var tmp = JSON.parse(JSON.stringify(this.attributes));
        tmp ={id:tmp.id,type:this.rowtype,attributes:tmp};
        delete tmp.attributes.id;
        return this.raw;
    },

    get_last_error: function(){
        return this.validator.get_last_error();
    },

    get_warnings: function(){
        return this.validator.get_warnings();
    },
    _super: Backbone.Model.prototype
});