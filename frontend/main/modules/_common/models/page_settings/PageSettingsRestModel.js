var PageSettingsRestModel = Backbone.JsonRestModel.extend({

    urlRoot: REST_API_COURSE_URL + '/page_settings',
    type: 'page_settings',

    //This should be set when initiating the Settings elsewhere
    possibleColumns:[],
    defaults: {
        columns: [],
        page_size: 25,
        module: ''
    },

    getColumnsForGrid:function(){
        var self = this;
        //For every column, return back the grid column associated with it.
        var gridColumns  = [];
        if(this.possibleColumns){

            // If the columns are not set yet, just return all the default columns
            if(!this.get('columns') || this.get('columns').length == 0){
                return self.possibleColumns.clone();
            }

            _.forEach(this.get("columns"), function(column){
                var columnToAdd = self.possibleColumns.findWhere({name:column});
                if(columnToAdd)
                    gridColumns.push(columnToAdd);
            });
        }
        return gridColumns;
    },

    initialize: function() {
        // Listen to changes on itself.
        this.on('sync', this.handleColumnChange);
    },

    handleColumnChange:function(){

        var visibleColumns = this.get('columns');

        _.forEach(this.possibleColumns.models, function(column){
            var displayed = (visibleColumns.indexOf(column.get('name')) > -1)
            column.set("displayed", displayed);
        },this);
    }
});

Backbone.modelFactory.registerModel(PageSettingsRestModel);