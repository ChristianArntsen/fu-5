var LineItemTax = Backbone.JsonRestModel.extend({

    type: 'itemTax',

    defaults: {
        percent: 0,
        name: ''
    }
});

var LineItemTaxCollection = Backbone.JsonRestCollection.extend({
    model: LineItemTax
});

Backbone.modelFactory.registerModel(LineItemTax, LineItemTaxCollection);