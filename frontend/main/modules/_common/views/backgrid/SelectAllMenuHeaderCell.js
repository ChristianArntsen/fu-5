Backgrid.SelectAllMenuHeaderCell = Backgrid.Extension.SelectAllHeaderCell.extend({

    className: 'select-all-menu-header-cell',

    events: {
        "click .select-all-page": "selectAllPage",
        "click .select-all": "selectAll",
        "click .select-none": "selectNone"
    },

    initialize: function(options){
        this.listenTo(this.collection, 'sync reset', this.render);
        Backgrid.Extension.SelectAllHeaderCell.prototype.initialize.call(this, options);
    },

    render: function () {

        var total = this.collection.state.totalRecords;
        if(!total){
            total = 0;
        }

        this.$el.html('<div class=" btn-group select-all-menu">' +
            '<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                '<input type="checkbox" tabindex="-1">' +
                '<span class="caret"></span>' +
            '</button>' +
            '<ul class="dropdown-menu">' +
                '<li><a href="#" class="select-all-page">Select all on page</a></li>' +
                '<li><a href="#" class="select-all">Select all <span class="record-count">' + accounting.formatNumber(total) + '</span></a></li>' +
                '<li><a href="#" class="select-none">Select none</a></li>' +
            '</ul>' +
            '</div>');

        return this;
    },

    selectAllPage: function(){
        this.checkbox().prop('checked', true);
        this.collection.selectAll = false;

        this.$el.find('.btn-group').removeClass('open');
        this.onChange();

        return false;
    },

    selectAll: function(){
        this.checkbox().prop('checked',true);
        this.collection.selectAll = true;

        this.$el.find('.btn-group').removeClass('open');
        this.onChange();

        return false;
    },

    selectNone: function(){

        this.$el.find('.btn-group').removeClass('open');
        this.collection.selectAll = false;
        this.checkbox().prop('checked',false);
        this.collection.trigger('backgrid.selectnone',this.collection);
        return false;
    }
});
