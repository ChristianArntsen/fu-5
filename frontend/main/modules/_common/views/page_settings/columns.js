var PageSettingsColumnsItemView = Backbone.Marionette.ItemView.extend({

    tagName: 'li',
    template: JST['_common/templates/page_settings/column_settings.html'],

    ui:{
        "displayColumnToggle":".display-column-toggle"
    },

    events:{
        "click @ui.displayColumnToggle":"toggleDisplay",
    },

    onRender:function(){
        if(this.model.get("name") == "select" || this.model.get("name")=="actions"){
            this.$el.hide();
        }
    },

    toggleDisplay:function(){
        this.model.set("displayed", !this.model.get("displayed"));
        return true;
    }
});

var PageSettingsColumnsView = Backbone.Marionette.CollectionView.extend({

    tagName: 'div',
    behaviors: {
        Sortable: {
            behaviorClass: Marionette.SortableBehavior,
            handle:".handle"
        }
    },
    childView: PageSettingsColumnsItemView,

    setColumns: function(){

        var columns = [];

        this.model.possibleColumns.reset(this.collection.toJSON());
        this.model.possibleColumns.forEach(function(column,key){
            if(column.get("displayed")){
                columns.push(column.get("name"));
            }
        });
        this.model.set("columns", columns);
    }
});