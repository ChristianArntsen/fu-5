var PageSettingsView = ModalLayoutView.extend({

    tagName: 'div',
    template: JST['_common/templates/page_settings/layout.html'],

    ui:{
        "saveChanges":"#save-changes"
    },

    events:{
        'click @ui.saveChanges': 'save'
    },

    regions: {
        generalSettings: '#general-settings',
        pageSize: '#page-size',
        columns: '#columns'
    },
    onRender:function(){
    },
    renderPageSize: function(){
        this.pageSize.show(new PageSettingsPageSizeView({
            model: this.model
        }));
    },

    renderColumns: function(){
        this.columns.show(new PageSettingsColumnsView({
            collection: this.model.possibleColumns.clone(),
            model: this.model
        }));
    },

    initialize:function(){
        if (this.options.modal) {
            ModalLayoutView.prototype.initialize.call(this, {extraClass: 'modal-full right'});
        }
        this.listenTo(App.vent, 'save-settings', this.save);
    }
});