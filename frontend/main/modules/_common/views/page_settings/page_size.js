var PageSettingsPageSizeView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['_common/templates/page_settings/page_size.html'],

    ui:{
        pageSizeButton:".page_size"
    },

    events:{
        "click @ui.pageSizeButton":"handleButtonClick"
    },

    handleButtonClick: function(e){
        var button = $(e.currentTarget);
        button.addClass('active').siblings().removeClass('active');
    },

    setPageSize: function(){
        var size = this.$el.find('button.active').data('value');
        this.model.set('page_size', size);
    }
});