var AdvancedSearchFilterEmptyView = Backbone.Marionette.ItemView.extend({
    template: JST['_common/templates/advanced_search/advanced_search_empty.html'],
    ui:{
        "add":"#add"
    },
    events:{
        "click @ui.add":"add"
    },
    add:function(){
        this.collection.addNewFilter();
    }
});

var AdvancedSearchFilterView = Backbone.Marionette.ItemView.extend({

    template: JST['_common/templates/advanced_search/advanced_search_filter.html'],

	ui:{
		"field":"#field",
		"comp":"#comp",
		"value":"#value",
		"add":"#add",
		"delete":"#delete",
	},
	events:{
		"change @ui.field":"updateFilter",
		"change @ui.comp":"updateFilter",
		"blur @ui.value":"updateFilter",
		"click @ui.add":"add",
		"click @ui.delete":"delete"
	},
	modelEvents:{
    	"change:field":"fieldChangedHandler",
	},
	initialize:function(){
		_.bindAll(this, 'handleEnterPress');
		$(document).bind('keypress', this.handleEnterPress);
	},
	onBeforeDestroy:function(){
		$(document).unbind('keypress', this.handleEnterPress);
	},
	onRender:function(){
		this.updateFilter();
		if(this.ui.value.attr("type") == "datetime"){
			this.ui.value.datetimepicker({
                    format: 'MM/DD/YYYY'
			});
		}

	},
    handleEnterPress:function(event){
        if (event.which == 13 && this.$el.is(":visible")) {
            event.preventDefault();
            this.updateFilter();
            this.triggerMethod('execute:search');
        }
	},
	fieldChangedHandler:function(){
    	var newField = this.model.get("allowed_fields").findWhere({"name":this.model.get("field")});
		this.model.set("type",newField.get("type"));
		this.model.set("possibleValues",newField.get("possibleValues"));
		this.render();
	},
	updateFilter:function(){
		var field = this.ui.field.val();
		var comparison = this.ui.comp.val();
		var value = this.ui.value.val();
		this.model.set("value",value);
		this.model.set("comparison",comparison);
		this.model.set("field",field);
	},
	add:function(event){
		event.preventDefault();
		this.model.collection.addNewFilter();
	},
	delete:function(event){
		event.preventDefault();
		this.model.destroy();
	}
});
var AdvancedSearchCollectionView = Backbone.Marionette.CompositeView.extend({
	ui:{
		clearAllFiltersButton:"#clear-filters"
	},
	events:{
		"click @ui.clearAllFiltersButton":"clearFilters"
	},
	clearFilters:function(){
		this.collection.reset();
	},
	collection:this.collection,
    template: JST['_common/templates/advanced_search/advanced_search.html'],
    tagName: 'div',
    emptyView:AdvancedSearchFilterEmptyView,
	emptyViewOptions:function(){
    	return {
    		collection:this.collection
        }
	},
    childView: AdvancedSearchFilterView,
	initialize:function(){
    	this.collection = this.model.get("applied_filters");
	}
});