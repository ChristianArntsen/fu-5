var WizardStepsView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    className: 'wizard-steps-container',
    template: JST['_common/templates/wizard/wizard_steps.html'],

    modelEvents: {
        'change:currentStep': 'render'
    },

    events: {
        'click .wizard-step': 'goToStep'
    },

    goToStep: function(e){
        var element = $(e.currentTarget);
        var step = element.data('step');

        if(step <= this.model.get('maxStep')){
            this.model.set('currentStep', step);
        }
        return false;
    }
});