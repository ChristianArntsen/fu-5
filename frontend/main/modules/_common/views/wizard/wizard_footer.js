var WizardFooterView = Backbone.Marionette.ItemView.extend({

    tagName: 'div',
    template: JST['_common/templates/wizard/wizard_footer.html'],

    modelEvents: {
        'change:currentStep': 'render'
    },

    events: {
        'click button.previous': 'previousStep'
    },

    previousStep: function(){
        this.model.previous();
        return false;
    }
});