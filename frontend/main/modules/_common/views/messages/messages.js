
var MessageThreadsItem = Backbone.Marionette.ItemView.extend({
    tagName: 'li',
    className: function(){
        var classNames = "";
        if(this.model.get("incoming") == 1){
            classNames += "incoming";
        } else {
            classNames += "outgoing";
        }

        if(this.model.get("read") == 0){
            classNames += " unread";
        } else {
            classNames += " read";
        }
        return classNames;
    },
    onRender:function(){
        if(this.model.get("incoming") == "0"){
            this.$el.hide();
        } else {
            this.$el.show();
        }
        this.$el.removeClass("read unread");
        this.$el.addClass(this.className());
    },
    ui:{
        "respondBtn":"#respond",
        "ignoreBtn":"#ignore",
    },
    events:{
        "click @ui.respondBtn":"handleRespondBtn",
        "click @ui.ignoreBtn":"handleIgnoreBtn"
    },
    modelEvents:{
        "change":"handleModelChange"
    },
    handleModelChange:function(){
        this.render();
    },
    handleIgnoreBtn:function(){
        this.model.destroy();
    },
    handleRespondBtn:function(){
        var customer = new Customer({
            person_id: this.model.get("person_id")
        });
        var self = this;
        customer.fetch({
            silent: true,
            data: {
                include_rainchecks: true,
                include_credit_cards: true,
                include_marketing: true,
                include_account_transactions: true,
                include_billing: true,
                include_last_visit: true
            },
            success: function(model){
                customerEditModal.render();

                // Delay the listener because a sync event is triggered
                // AFTER this success callback (which we don't care about)
                setTimeout(function(){
                    customer.on('sync', function(){
                        self.model.fetch({
                            data:{
                                include:"customerGroups"
                            }
                        });
                    });
                }, 10);
            }
        });
        var customerEditModal = new EditCustomerView({model: customer});
        customerEditModal.defaultTab = "marketing";
        customerEditModal.show();
        $('.modal-content').loadMask();

    },
    template: JST['_common/templates/messages/messageThread.html']
});
var MessageEmptyThreadView = Backbone.Marionette.ItemView.extend({
    template: JST['_common/templates/messages/emptyMessageThread.html']
});
var MessageThreadsCollection = Backbone.Marionette.CollectionView.extend({
    tagName: 'ul',
    emptyView:MessageEmptyThreadView,
    childView: MessageThreadsItem

});

var MessageThreadsLayout = Backbone.Marionette.LayoutView.extend({
    regions: {
        'threads': '#message_threads'
    },
    tagName: 'div',
    template: JST['_common/templates/messages/messageThreadLayout.html'],
    ui:{
        "customerMessageList":"#customer_message_list",
        "unreadCount":"#unread_count",
        "notificationSoundButton":"#notification-sound"
    },
    events:{
        "click @ui.notificationSoundButton":"handleSoundButton",
        "show.bs.dropdown":"dropdownShown",
        "hide.bs.dropdown":"dropdownHidden"
    },
    dropdownShown:function(){
        this.model.set("displayDropdown",true)
    },
    dropdownHidden:function(){
        this.model.set("displayDropdown",false)
    },
    handleSoundButton:function(e){
        this.model.set("mute",!this.model.get("mute"));
        if(typeof(Storage) !== "undefined"){
            localStorage.muteNotifications = this.model.get("mute");
        }
        this.render();
        e.preventDefault();
        e.stopPropagation();
    },
    initialize: function (){
        var muteNotifications = false;
        if (typeof(Storage) !== "undefined" && localStorage.muteNotifications) {
            muteNotifications = localStorage.muteNotifications;
        }
        this.model = new Backbone.Model({
            "mute":muteNotifications,
            "audioVolume":.1,
            "displayDropdown":false
        });
        this.favicon=new Favico({
            animation:'slide'
        });
        this.audio = new Audio('https://s3-us-west-2.amazonaws.com/foreup.com/Unique+Notification0.mp3');
        this.audio.volume = .1;
        var self = this;
        this.collection.fetch();
        this.startPoller();

        self.collection.on("add change destroy",function(){
            var unreadCount = self.collection.getUnreadCount();
            self.updateNotifications(unreadCount);
            self.poller.destroy();
            self.startPoller();
            self.render();

        })

    },
    previousUnreadCount:0,
    updateNotifications:_.debounce(function(unreadCount){
        if(unreadCount > 0){
            this.favicon.badge(unreadCount);
        } else {
            this.favicon.badge(0)
        }
        if(unreadCount > this.previousUnreadCount && !this.model.get("mute")){
            this.audio.volume = this.model.get("audioVolume");
            this.audio.play();
        }
        this.previousUnreadCount = unreadCount;
    },2000),

    onRender: function(){
        var self = this;
        self.ui.unreadCount.html(self.collection.getUnreadCount());

        var filtered = this.collection.where({incoming: '1'});
        var filteredCollection = new MessageThreadCollection(filtered);

        this.threads.show(new MessageThreadsCollection({
            collection: filteredCollection
        }));

    },
    onDestroy:function(){
        if(this.poller){
            this.poller.destroy();
        }
    },
    startPoller:function(){
        var self=this;
        if(typeof VIRTUAL_NUMBER == "undefined" || VIRTUAL_NUMBER == "")
            return;
        self.poller = Backbone.Poller.get(this.collection,
            {
                remove:false,
                delay: 10000,
                data:{
                    timestamp:this.collection.getLastDate()
                }
            });
        self.poller.start();
    }
});
