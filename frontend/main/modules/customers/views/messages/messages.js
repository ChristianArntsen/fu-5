
var CustomerMessageHistoryItem = Backbone.Marionette.ItemView.extend({
    tagName: 'li',
    className: function(){
        if(this.model.get("incoming") == 1){
            return "incoming";
        } else {
            return "outgoing";
        }
    },
    template: JST['customers/templates/messages/message_history.html'],
    childView: MobileCustomerListItem
});
var CustomerMessageHistoryCollection = Backbone.Marionette.CollectionView.extend({
    tagName: 'ul',
    childView: CustomerMessageHistoryItem,
    initialize:function(){
        var self = this;
        this.startPoller();
        _.delay(function(){
            self.collection.on("add",function(){
                self.poller.destroy();
                self.startPoller();
            })
        },2000)

    },
    onAddChild:function(){
        this.options.container.scrollTop(this.options.container.prop("scrollHeight"));
    },
    onRender:function(){
        var view = this;
        setTimeout(function(){
            view.options.container.scrollTop(view.options.container.prop("scrollHeight"));
        },0);

    },
    onDestroy:function(){
        if(this.poller){
            this.poller.destroy();
        }
    },
    startPoller:function(){
        if(!this.collection){
            return false;
        }
        var self=this;
        self.poller = Backbone.Poller.get(this.collection,
            {
                remove:false,
                data:{
                    person: this.options.person,
                    timestamp:this.collection.getLastDate()
                }
            });
        self.poller.start();
    },
});

var CustomerMessages = Backbone.Marionette.LayoutView.extend({
    regions: {
        'messages': '#customer_message_list'
    },
    tagName: 'div',
    template: JST['customers/templates/messages/messages.html'],
    ui:{
        "newMessageBox":"#customer_new_message",
        "customerMessageList":"#customer_message_list",
        "newMessageBtn":"#customer_new_message_btn"
    },
    events:{
        "click @ui.newMessageBtn":"sendMessage",
        'keydown @ui.newMessageBox': 'handleEnterPress'
    },
    initialize: function (){

    },
    handleEnterPress:function(event){
        if (event.which == 13) {
            this.sendMessage();
        }
    },
    sendMessage: function(){
        var newMessage = this.ui.newMessageBox.val();
        this.ui.newMessageBox.val("");
        this.model.messages.sendNewMessage(this.model.get("person_id"),newMessage);
    },

    save: function(){
    },

    onRender: function(){
        var self = this;
        this.messages.show(new CustomerMessageHistoryCollection({
            collection: this.model.messages,
            person:this.model.get("person_id"),
            container:self.ui.customerMessageList
        }));

    },

});
