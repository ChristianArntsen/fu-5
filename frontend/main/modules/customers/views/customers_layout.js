
var CustomersLayout = Backbone.Marionette.LayoutView.extend({

	tagName: 'div',
	className: 'row new-page',
	template: JST['customers/templates/customers.layout.html'],
    ui:{
	    "advancedSearchButton":"#advanced-search-button",
        "pageDropdownAdvancedSearch":"#page-dropdown-advanced-search",
        "advancedSearchDropdown":"#advanced-search-dropdown",
        "pageSettingsButton":"#page-dropdown-settings",
        "toggleDeletedCheckbox":"#page-dropdown-show-deleted input",
        "exportListButton":"#export-list",
        "importListButton":"#import-list",
        "newCustomerButton":".new",
        "manageCustomerGroupsButton":".groups",
        "toggleDeleted":"#page-dropdown-show-deleted",
        "billingMembershipsButton": "button.billing-memberships"
    },
    events:{
        "click  @ui.advancedSearchButton":"executeSearch",
        "click  @ui.pageDropdownAdvancedSearch":"toggleAdvancedSearch",
        "click  @ui.manageCustomerGroupsButton":"manageGroups",
        "click  @ui.pageSettingsButton":"pageSettings",
        "click  @ui.newCustomerButton":"editCustomer",
        "change @ui.toggleDeletedCheckbox":"handleToggleDeletedCustomers",
        "click  @ui.toggleDeleted":"handleMenuClick",
        "click  @ui.exportListButton":"handleExportListClick",
        "click  @ui.importListButton":"handleImportListClick",
        "click @ui.billingMembershipsButton": 'billingMemberships'
    },
	regions: {
		table: '#main-table',
        advancedSearch:'#advanced-search',
        quickSearch:'#quick-search',
        mobileSearch:'#mobile-search',
        mobileList:'#mobile-list'
	},
    onDestroy:function(){
        $("#page-iframe-container").show();
    },
    initialize:function(){

        App.vent.trigger("notification",{
            "type":"info",
            "msg":"We are currently phasing out the old customers page, if you need to use the old Customers page please: <a href='/v2/home#customers'>click here</a>",
            "delay":10000
        });
        var self = this;
	    this.settings = App.data.course.get('customer_page_settings');
	    this.settingsPromise = this.settings.fetch();
        this.settings.possibleColumns = new PageSettingsPageColumnCollection([
            {
                headerCell: Backgrid.SelectAllMenuHeaderCell,
                name: "select",
                cell: "select-row"
            },{
                name: "first_name",
                label: "First Name",
                editable: false,
                sortable: true,
                cell:Backgrid.StringCell,
                sortType: "toggle"
            },{
                name: "last_name",
                label: "Last Name",
                editable: false,
                sortable: true,
                cell:Backgrid.StringCell,
                sortType: "toggle"
            },{
                name: "phone_number",
                label: "Phone Number",
                editable: false,
                sortable: true,
                cell:Backgrid.StringCell,
                sortType: "toggle"
            },{
                name: "email",
                label: "Email",
                editable: false,
                sortable: true,
                cell:Backgrid.EmailCell,
                sortType: "toggle"
            },{
                name: "date_created",
                label: "Date Created",
                editable: false,
                sortable: true,
                cell:Backgrid.Extension.MomentCell.extend({
                    displayFormat: "MMM DD, YYYY"
                }),

                sortType: "toggle"
            },{
                name: "account_balance",
                label: App.data.course.get('customer_credit_nickname'),
                editable: false,
                sortable: true,
                cell:Backgrid.CurrencyCell,
                sortType: "toggle"
            },{
                name: "member_account_balance",
                label: App.data.course.get('member_balance_nickname'),
                editable: false,
                sortable: true,
                cell:Backgrid.CurrencyCell,
                sortType: "toggle"
            },{
                name: "email_subscribed",
                label: "Email Subscribed",
                editable: false,
                sortable: true,
                cell:Backgrid.BooleanCell,
                sortType: "toggle"
            },{
                name: "course",
                label: "Course",
                editable: false,
                sortable: true,
                cell:Backgrid.StringCell,
                sortType: "toggle"
            },{
                name: "customerGroups",
                label: "Groups",
                editable: false,
                sortable: true,
                cell:Backgrid.Customers.GroupsCell,
                sortType: "toggle"
            },{
                name: "history",
                label: "History",
                editable: false,
                cell:Backgrid.Customers.HistoryCell
            },{
                name: "actions",
                label: "Actions",
                editable: false,
                cell:Backgrid.Customers.MenuCell
            }
        ]);

        //If on mobile device, don't fetch the initial list
        var isMobile = window.matchMedia("only screen and (max-width: 760px)");
        if (!isMobile.matches) {
            this.settingsPromise.done(function(){
                self.collection.state.pageSize = self.settings.get("page_size");
                self.collection.fetch({data:{"include":"customerGroups"}});
                self.table.show( new CustomersTableView({collection: self.collection,settings:self.settings}) );
            })

        }

	    this.listenTo(this.collection.advancedSearch.get("allowed_fields"),"sync",function(){
            this.renderAdvancedSearch()
        },this);
        this.advancedSearchShown = false;

	    this.listenTo(this.collection.advancedSearch,"change:q",this.executeSearch);
    },
    onShow:function(){
        this.ui.advancedSearchDropdown.hide();


        this.mobileList.show( new MobileCustomerList({collection: this.collection}) );
    },

    downloadURL : function downloadURL(url){
        var hiddenIFrameID = 'hiddenDownloader' + this.downloadCount++;
        var iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
        iframe.src = url;
    },
    handleImportListClick:function(e){
        var import_job = _.find(App.data.data_import_jobs.models, function(job){
            if(job.get('type') == 'customers' && job.get('status') != 'pending'){
                return job;
            }
        });

        if(!import_job){
            import_job = new DataImportJob({
                'type': 'customers'
            });
        }

        var modal = new DataImportView({model: import_job});
        modal.show();
    },
    handleExportListClick:function(e){

        var searchParams = this.collection.getFilterParams();
        searchParams.format = "csv";

        var currentText = e.target.innerHTML;
        e.target.html = "....";
        $(e.target).attr("disabled",true);
        e.target.innerHTML = "Downloading";
        this.collection.downloadFile({data:searchParams},function(){
            $(e.target).attr("disabled",false);
            e.target.innerHTML = currentText;
        });
    },
    toggleAdvancedSearch:function(){
        if(this.advancedSearchShown){
            this.ui.advancedSearchDropdown.hide();
        } else {
            this.ui.advancedSearchDropdown.show();
        }
        this.advancedSearchShown = !this.advancedSearchShown;
    },
    onRender:function(){
        this.renderAdvancedSearch();
        this.quickSearch.show(new BasicSearchView({
            model:this.collection.advancedSearch,
            placeholder: 'Search customers...'
        }));
        this.mobileSearch.show(new MobileCustomerSearch({
            model:this.collection.advancedSearch
        }));
    },
    renderAdvancedSearch:function(){
        if(this.collection.advancedSearch.get("allowed_fields").length > 0){
            var advSearchView = new AdvancedSearchCollectionView({
                model:this.collection.advancedSearch
            });
            advSearchView.on("childview:execute:search",function(){
                this.executeSearch();
            },this);
            this.advancedSearch.show(advSearchView);
        }
    },
    executeSearch:function(e){
        this.collection.state.currentPage = 0;
        var searchParams = this.collection.getFilterParams();
        this.collection.searchParams = searchParams;
        this.collection.fetch();
    },
    editCustomer:function(e){
        var customer = new Customer();
        customer.set("use_loyalty",App.data.course.get('loyalty_auto_enroll'));
        var customerEditModal = new EditCustomerView({model: customer});
        customerEditModal.show();

        customer.on('sync', function(model){
                set_feedback('Customer created', 'success_message', false);
        });
    },

    manageGroups:function(e) {
        var groups = App.data.customer_groups;
        var groupsManageModal = new GroupsManageView({collection: groups});
        groupsManageModal.show();
    },
    pageSettings:function(e){
        var settingsModal = new CustomerPageSettingsView({model: this.settings, modal:true});
        settingsModal.show();
    },
    handleToggleDeletedCustomers:function(e){
        e.stopPropagation();
        this.collection.showDeleted = this.ui.toggleDeletedCheckbox.is(':checked');
        this.executeSearch();
    },
    handleMenuClick:function(e){
        e.stopPropagation();
    }
});