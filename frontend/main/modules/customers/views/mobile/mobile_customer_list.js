
var MobileCustomerListItem = Backbone.Marionette.ItemView.extend({

    template: JST['customers/templates/mobile/customer_list_item.html'],
    tagName: 'li',
    className:"mobile-customer",

    initialize: function(){
    },
    events: {
        "click":"handleCustomerClick"
    },
    ui: {
    },
    handleCustomerClick:function(e){
        e.preventDefault();
        var person_id = this.model.get('id');

        var customer = new Customer({
            person_id: person_id
        });

        var customerEditModal = new EditCustomerView({model: customer});
        customerEditModal.show();
        $('.modal-content').loadMask();

        customer.fetch({
            silent: true,
            data: {
                include_rainchecks: true,
                include_credit_cards: true,
                include_marketing: true,
                include_account_transactions: true,
                include_billing: true,
                include_last_visit: true
            },
            success: function(model){
                customerEditModal.render();

                // Delay the listener because a sync event is triggered
                // AFTER this success callback (which we don't care about)
                setTimeout(function(){
                    customer.on('sync', function(){
                        //update_row(person_id, 'customers/get_row/true')
                    });
                }, 10);
            }
        });
    }
});
var MobileCustomerList = Backbone.Marionette.CollectionView.extend({
    tagName: 'ul',
    childView: MobileCustomerListItem
});