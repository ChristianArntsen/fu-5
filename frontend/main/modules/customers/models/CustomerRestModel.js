var CustomerRestModel = Backbone.JsonRestModel.extend({
    parse: function(data){
        if(!data.attributes && data.data){
            data = data.data;
        }
        var contactInfo = data.attributes.contact_info;
        delete data.attributes.contact_info;
        _.extend(data.attributes,contactInfo);
        return Backbone.JsonRestModel.prototype.parse.call(this, data, data);
    },
    idAttribute: "id",
    type: 'customers',
    urlRoot: REST_API_COURSE_URL + '/customers',
    defaults: {
        username: '',
        account_number: '',
        company_name: '',
        taxable: '',
        discount: '',
        opt_out_email: '',
        opt_out_text: '',
        date_created: '',
        contact_info: {
            first_name:"",
            last_name:"",
            phone_number:"",
            cell_phone_number:"",
            email:"",
            birthday:"",
            address_1:"",
            address_2:"",
            city:"",
            state:"",
            zip:"",
            country:""
        },
        handicapAccountNumber:"",
        handicapScore:"",
        comments:"",
        deleted:false,
        customerGroups:[]
    },

    relations: {
        customerGroups: {
            type: 'customerGroups',
            many: true
        }
    },

    save: function(attrs,options){
        options || (options = {});
        attrs || (attrs = _.clone(this.attributes));

        // Filter the data to send to the server
        attrs.contact_info = {
            first_name:attrs.first_name,
            last_name:attrs.last_name,
            phone_number:attrs.phone_number,
            cell_phone_number:attrs.cell_phone_number,
            email:attrs.email,
            birthday:attrs.birthday,
            address_1:attrs.address_1,
            address_2:attrs.address_2,
            city:attrs.city,
            state:attrs.state,
            zip:attrs.zip,
            country:attrs.country
        };

        // Proxy the call to the original save function
        return Backbone.Model.prototype.save.call(this, attrs, options);
    }
});

var CustomerRestModelCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/customers',
    showDeleted: false,
    model: CustomerRestModel,
    downloadCount: 0,

    initialize:function(){
        this.advancedSearch = new AdvancedSearchModel({module: 'customers'});
    },

    downloadFile: function(data,callback) {
        var req = new XMLHttpRequest();
        req.open("GET", this.url+"?"+$.param(data['data']), true);

        req.responseType = "blob";

        req.send(data);

        req.onreadystatechange = function() {//Call a function when the state changes.
            if(req.readyState == 4 && req.status == 200) {
                console.log("LOADED!!");
            }
            callback();
        }

        req.onload = function (event) {
            var blob = req.response;
            var link=document.createElement('a');
            link.href=window.URL.createObjectURL(blob);
            link.download="Customers_" + new Date() + ".csv";
            link.click();
        };
    },

    getFilterParams: function(){
        var params = this.advancedSearch.getFilterParams();
        if(this.showDeleted){
            params["deleted"] = "1";
        }
        params.include = 'customerGroups';

        return params;
    },

    fetch: function(options) {
        if(!options)
            options = {};

        if(!options.data){
            options.data = {};
            options.data.include = "customerGroups"
        } else {
            options.data.include = "customerGroups"
        }
        if(this.searchParams){
            options.data = this.searchParams;
        }
        return Backbone.JsonRestCollection.prototype.fetch.call(this, options);
    }
});

Backbone.modelFactory.registerModel(CustomerRestModel, CustomerRestModelCollection);