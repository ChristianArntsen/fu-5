var TipSharingTransaction = Backbone.Model.extend({

    idAttribute: "sale_id",
    defaults: {
        "transaction_id": null,
        "sale_id": null,
        "employee_id": null,
        "transaction_time": null,
        "notes": null,
        "comment": null,
        "journal_entries": []
    },

    get_url: function(){

    },

    initialize: function(attrs,options){
        var journal_entries = new TipJournalCollection(this.get('journal_entries'));
        console.log('entreez',this.get('journal_entries'),journal_entries,attrs);
        this.set('journal_entries',journal_entries);
    },

    parse: function(response) {
        var url = this.url;

        if (response.journal_entries) {
            var cart = this.get('cart');
            var journal_entries = new TipJournalCollection(response.journal_entries);
            response.journal_entries = journal_entries;
        }
        return response;
    },

    add_employee: function(employee){
        var entries = this.get('journal_entries');
        var entry = new TipJournalEntry({transaction_id:this.get('transaction_id'),employee_id:employee.emp_id,notes:'Tips Payable: '+employee.last_name+', '+employee.first_name});
        var found = false;
        _.each(entries.models,function(existing){
            if(existing.get('notes')===entry.get('notes')&&
                existing.get('transaction_id')==entry.get('transaction_id')&&
                existing.get('employee_id')==entry.get('employee_id')){
                existing.set({'deleted':0,'amount':0.00});
                found = true;
            }
        });
        if(!found)entries.add(entry);
    },

    remove_employee: function(employee_id){
        var entries = this.get('journal_entries');
        var self = this;
        _.each(entries.models,function(existing){
            if(existing.get('transaction_id')==self.get('transaction_id')&&
                existing.get('employee_id')==employee_id){
                existing.set('deleted',1);
                existing.set('amount',0.00);
            }
        });
    },

    balance_debit: function(change){
        var debit = this.get('journal_entries').get_debit();
        var total = this.get('total');
        var amount = new Decimal(debit.get('amount'));
        console.log('debit,total,amount,credits',debit,total,amount);
        var new_amount = amount.plus(change).toDP(2).toNumber();
        if(new_amount > total || new_amount < 0)return false;
        debit.set('amount',new_amount);
        return true;
    },

    set_employee_amount: function(employee_id,amount){
        var entries = this.get('journal_entries');
        var self = this;
        var changed = false;
        _.each(entries.models,function(existing){
            if(existing.get('transaction_id')==self.get('transaction_id')&&
                existing.get('employee_id')==employee_id){

                previous_amount = existing.get('amount');
                var dec_amt = new Decimal(amount);
                if(!self.balance_debit(dec_amt.minus(previous_amount)))return;
                existing.set('amount',amount);
                if(!self.is_valid()){
                    existing.set('amount',previous_amount);
                    changed = true;
                }
            }
        });
        return changed;
    },

    set_employee_comment: function(employee_id,comment){
        var entries = this.get('journal_entries');
        var self = this;
        _.each(entries.models,function(existing){
            if(existing.get('transaction_id')==self.get('transaction_id')&&
                existing.get('employee_id')==employee_id){
                existing.set('comment',comment);
            }
        });
    },

    get_balance: function(){
        var entries = this.get('journal_entries');
        var balance = new Decimal(0);
        _.each(entries.models,function(existing){
            if(existing.deleted)return;
            if(existing.get('d_c')==='debit'){
                balance = balance.plus(existing.get('amount'));
            }else{
                balance = balance.minus(existing.get('amount'));
            }
        });
        return balance.toDP(2).toNumber();
    },

    get_total: function(){

        var entries = this.get('journal_entries');
        var total = new Decimal(0);
        _.each(entries.models,function(existing){
            if(existing.deleted)return;
            if(existing.get('d_c')==='credit'){
                total = total.plus(existing.get('amount'));
            }
        });
        return total;
    },

    get_employee_count: function(){
        var entries = this.get('journal_entries');
        var count = 0;
        _.each(entries.models,function(existing){
            if(existing.deleted)return;
            if(existing.get('employee_id')){
                count ++;
            }
        });
        return count;
    },

    even_split: function(){
        var entries = this.get('journal_entries');
        var total = this.get_total();
        var split = new Decimal(total);
        split = split.dividedBy(this.get_employee_count()).toDP(2).toNumber();
        _.each(entries.models,function(existing){
            if(existing.deleted)return;
            if(existing.get('employee_id')){
                existing.set('amount',split);
            }
        });
    },

    scale_to_100: function(){
        // If we get into a state where we are at > 100% after deleting a tip
        // we want to scale it down so the tips total 100%

        var entries = this.get('journal_entries');
        var attrs = this.attributes;
        var total_available = new Decimal(attrs.total);
        var total_shared = new Decimal(attrs.total_debits);
        var self = this;

        var hash = {};
        // first get the percent for everyone
        var running_total = new Decimal(0);
        _.each(entries.models,function(existing){
            if(existing.deleted)return;
            if(existing.get('d_c')==='credit'){
                var pct = new Decimal(existing.get('amount')).dividedBy(total_shared);
                running_total = running_total.plus(pct.times(total_available).toDP(2));
                var over = new Decimal(0);
                if(total_available.minus(running_total).toDP(2).toNumber() < 0){
                    over = new Decimal(total_available.minus(running_total).toDP(2));
                }
                // then transform to total to 100%
                existing.set('amount',pct.times(total_available).plus(over).toDP(2).toNumber());
            }
            else{
                existing.set('amount',total_available.toDP(2).toNumber());
                self.set('total_debits',total_available.toDP(2).toNumber())
            }
        });
    },

    is_valid: function(){
        if(this.get_balance()<0)return false;
        return true;
    }

});