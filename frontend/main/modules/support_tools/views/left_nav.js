// left-side navigation for the support tools module
var support_tools_left_nav = Backbone.Marionette.ItemView.extend({
    template: JST['support_tools/templates/left_nav.html'],
    initialize: function() {
        this.handleTagUpdate();
    },
    handleTagUpdate: function(){
        this.render();
    },

    onRender: function(){

        this.table = this.$el.find('table.support-tools').bootstrapTable({
            classes: 'table-no-bordered table-no-background col-md-3 col-lg-2',
            data: [
                {id:0,name:'Marketing Campaign Support',tool:'marketing_campaigns_support_layout'},
                {id:1,name:'Bulk Edit Support',tool:'bulk_edit_support_layout'}
            ],
            uniqueId: 'id',
            sidePagination: 'client',
            columns: [
                    {
                    field: 'name',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                                   '<button class="btn btn-primary btn-block new" data-id="'+row.id+'">' +
                                       val +
                                   '</button>' +
                               '</div>'
                    },
                    sortable: true
                }
            ],
            page: 1,
            pagination: true,
            pageSize: 50,
            pageNumber: 1,
            pageList: [10,25,50,100],
            search: true
        });
    }
});