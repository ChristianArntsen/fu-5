var support_tools_display_area = Backbone.Marionette.ItemView.extend({
    template: JST['support_tools/templates/disp_area.html'],
    className: 'col-xs-12',
    initialize: function() {
        this.handleTagUpdate();
    },
    handleTagUpdate: function(){
        this.render();
    }
});