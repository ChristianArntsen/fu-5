var DataImportJob = Backbone.Model.extend({

    urlRoot: API_URL + '/data_import',

    defaults: {
        type: 'customers',
        error: false,
        fields: {
            customers: {
                first_name: {name: 'First Name'},
                last_name: {name: 'Last Name'},
                account_number: {name: 'Account Number'},
                email: {name: 'Email'},
                phone_number: {name: 'Phone Number'},
                cell_phone_number: {name: 'Cell Phone Number'},
                address_1: {name: 'Address Line 1'},
                address_2: {name: 'Address Line 2'},
                city: {name: 'City'},
                state: {name: 'State/province'},
                zip: {name: 'Zip/post code'},
                country: {name: 'Country'},
                account_balance_allow_negative: {name: 'Allow Negative Customer Credits'},
                account_balance: {name: 'Customer Credits'},
                account_limit: {name: 'Customer Credits Limit'},
                member_account_balance_allow_negative: {name: 'Allow Negative Member Account'},
                member_account_balance: {name: 'Member Account Balance'},
                member_account_limit: {name: 'Member Account Limit'},
                loyalty_points: {name: 'Loyalty Point Total'},
                use_loyalty: {name: 'Use Loyalty'},
                member: {name: 'Is Member'},
                birthday: {name: 'Birthday'},
                comments: {name: 'Comments'},
                groups: {name: 'Groups'},
                non_taxable: {name: 'Non-Taxable'},
                date_created: {name: 'Date Joined'},
                username: {name: 'Online Booking Username'},
                password: {name: 'Online Booking Password'}
            }
        },
        status: 'pending',
        percent_complete: 0,
        settings: {},
        total_records: 0,
        records_failed: 0,
        records_imported: 0,
        estimated_duration: 0
    },

    sync: function(method, model, options){

        if(!options.attrs && method !== 'read'){
            options.attrs = this.attributes;
        }

        if(options.attrs){
            var attributes = _.clone(options.attrs);
            attributes = _.omit(attributes, ['file', 'data']);

            options.contentType = 'application/json';
            options.data = JSON.stringify(attributes);
        }

        Backbone.Model.prototype.sync.call(this, method, model, options);
    },

    initialize: function(){

        this.set('settings', {
            column_map: {},
            first_row_headers: false,
            notify_email: false
        }, {
            silent: true
        });

        this.estimate_duration();

        var required_fields = App.data.course.get('customer_field_settings');
        var fields = this.get('fields');

        _.each(fields.customers, function(field, key){
            if(
                required_fields[key] &&
                required_fields[key]['required'] &&
                required_fields[key]['required'] == 1
            ){
                field.required = true;
            }
        });
    },

    get_fields: function(){
        var fields = this.get('fields');
        return fields[this.get('type')];
    },

    map_fields: function(){

        var csv_data = this.get('data');
        if(!csv_data){
            return false;
        }
        var foreup_fields = this.get_fields();
        var headers = csv_data.data[0];
        var mapped_fields = {};

        _.each(foreup_fields, function(col_data, foreup_key){
            var foreup_name = col_data.name;
            _.each(headers, function(column_name, column_key){

                if(_.similarity(column_name, foreup_name) >= 75){
                   mapped_fields[foreup_key] = column_key;
                }
            });
        });

        csv_data.headers = headers;
        var settings = this.get('settings');
        if(settings.first_row_headers){
            csv_data.data.splice(0, 1);
        }

        // Check if the last row is empty
        var last_row = csv_data.data[csv_data.data.length - 1];
        var last_row_empty = true;

         _.find(last_row, function(value){
             if(value){
                last_row_empty = false;
                return true;
            }
        });
        // If last row is empty, remove it
        if(last_row_empty){
            csv_data.data.splice(csv_data.data.length - 1, 1);
        }
        var total_records = csv_data.data.length;

        this.set('total_records', total_records);
        this.set('data', csv_data);
        this.set_setting('column_map', mapped_fields);
        this.estimate_duration();
    },

    estimate_duration: function(){

        // For now we will say 1 record takes on average 10ms
        var seconds = parseInt(this.get('total_records') * 0.010);
        this.set('estimated_duration', seconds);
    },

    parse_file: function(callback){

        var self = this;
        var file = this.get('file');
        if(!file){
            return false;
        }

        var params = {
            complete: function(results, file){

                self.set('data', results);
                self.map_fields();

                if(typeof callback == 'function'){
                    callback(results, file);
                }
            }
        };

        // Parse the CSV file to match fields
        Papa.parse(file, params);
        return true;
    },

    set_setting: function(key, value){
        var settings = this.get('settings');
        settings[key] = value;
        this.set('settings', settings);
    }
});

var DataImportJobCollection = Backbone.Collection.extend({
    url: API_URL + '/data_import',
    model: DataImportJob
});