if(typeof BASE_URL === 'undefined'){
    BASE_URL = '/';
}

var EmployeeAuditLog = Backbone.JsonRestModel.extend({
    defaults:{
        "id":null,
        "type":"employee_audit_log",
        "editor_id":null,
        "username":'',
        "person_id":null,
        "course_id":null,
        "gmt_logged":null,
        "action_type_id":null,
        "action_type_name":'',
        "action_type_desc":''
    },
    undo_action: function(callback){
        $.ajax(BASE_URL +'api_rest/employee_audit_log/' + this.id + '/undo', {
            method:"POST",
            data: JSON.stringify({"data":{"attributes": {"id":this.id}}}),
            contentType:'application/json',
            success: function (res) {
                if(typeof callback === 'function')callback(res);
            },
            dataType: 'json'
        });
    },
    redo_action: function(callback){
        $.ajax(BASE_URL +'api_rest/employee_audit_log/' + this.id + '/redo', {
            method:"POST",
            data: JSON.stringify({"data":{"attributes": {"id":this.id}}}),
            contentType:'application/json',
            success: function (res) {
                if(typeof callback === 'function')callback(res);
            },
            dataType: 'json'
        });
    },
    _super: Backbone.JsonRestModel.prototype
});

var EmployeeAuditLogCollection = Backbone.JsonRestCollection.extend({
    model:EmployeeAuditLog,

    url:BASE_URL +'api_rest/employee_audit_log',

    initialize: function(){
        this.fetch();
    },
    undo_action: function(id, callback){
        return this.get(id).undo_action(callback);
    },
    redo_action: function(id, callback){
        return this.get(id).redo_action(callback);
    },
    _super: Backbone.JsonRestCollection.prototype
});