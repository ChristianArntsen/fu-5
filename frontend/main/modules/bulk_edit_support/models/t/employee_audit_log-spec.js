if(typeof BASE_URL === 'undefined'){
    BASE_URL = '/';
}

describe('EmployeeAuditLog',function(){
    var model = new EmployeeAuditLog();

    it('should be defined',function(){
        expect(EmployeeAuditLog).toBeDefined();
    });

    it('should construct a new instance',function(){
        expect(model).toBeDefined();
    });

    it('should be an instance of JsonRestModel',function(){
        expect(model instanceof EmployeeAuditLog).toBe(true)
    });

    it('should have an instance of the validator',function(){
        expect(model.validator instanceof JsonRestValidator).toBe(true);
    });
});

describe('EmployeeAuditLogCollection',function(){
    var collection = new EmployeeAuditLogCollection();
    
    it('should be an instance of EmployeeAuditLogCollection',function(){
        expect(collection).toBeDefined();
        expect(collection instanceof EmployeeAuditLogCollection).toBe(true);
    });

    it('should have an instance of the validator',function(done){
        if(typeof App !== 'undefined' && App.data.user.get('user_level')*1<=3)
            pending('requires super-admin to test');
        expect(collection.validator instanceof JsonRestValidator).toBe(true);
        done();
    });

    it('should fetch with url already defined',function(done){
        if(typeof App !== 'undefined' && App.data.user.get('user_level')*1<=3)
            pending('requires super-admin to test');
        var ob = {};
        ob.method = 'get';
        ob.reset = true;
        ob.success = function(res){
            expect(res).toBeDefined();
            expect(collection.parse).toHaveBeenCalled();
            expect(collection.validator.validate).toHaveBeenCalled();
            expect(collection.valid).toBe(true);
            expect(collection.get(1) instanceof EmployeeAuditLog).toBe(true);
            expect(collection.get(1).attributes.id).toBe('1');
            expect(collection.get(1).id).toBe('1');
            done();
        };

        spyOn(collection,'parse').and.callThrough();
        spyOn(collection.validator,'validate').and.callThrough();
        collection.fetch(ob);
    },10000);
});