// left-side navigation for the support tools module
var BulkEditSupportLeftNav = Backbone.Marionette.ItemView.extend({
    template: JST['bulk_edit_support/templates/left_nav.html'],
    initialize: function() {
        this.handleTagUpdate();
    },
    handleTagUpdate: function(){
        this.render();
    },

    onRender: function(){

        this.table = this.$el.find('table.bulk-edit-support').bootstrapTable({
            classes: 'table-no-bordered table-no-background col-md-3 col-lg-2',
            //data: [{id:0,name:'Bulk Edit Support',tool:'bulk_edit_support_layout'},{id:1,name:'support tool 1',tool:'ItemKitLayout'},{id:2,name:'support tool 2',tool:'PassLayout'}],

            url:'/api_rest/index.php/courses/all',
            dataField:'data',

            uniqueId: 'id',
            sidePagination: 'client',
            columns: [
                {
                    field: 'attributes',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            val.title +
                            '</div>'
                    }
                },
                {
                    field: 'attributes',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            val.city + ', ' + val.state +
                            '</div>'
                    }
                },
                {
                    field: 'attributes',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            '<button class="btn btn-primary btn-block new course" data-name="' + val.title + '" data-id="' + row.id + '">logs</button>' +
                            '</div>'
                    }
                },
                {
                    field: 'id',
                    searchable: true,
                    visible:false
                }
            ],
            page: 1,
            pagination: true,
            pageSize: 50,
            pageNumber: 1,
            pageList: [10,25,50,100],
            search: true//,
            //clickToSelect:true,
           // singleSelect:true
        });
    }
});