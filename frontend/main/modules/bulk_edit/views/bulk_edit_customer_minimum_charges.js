var BulkEditCustomerMinimumChargeView = Backbone.Marionette.ItemView.extend({

    template: JST['bulk_edit/templates/customer_minimum_charge.html'],
    tagName: 'div',
    className: 'row bulk-edit-field-record',

    events: {
        "click button.delete": "removeCharge",
        "blur input.start-date": "saveStartDate"
    },

    removeCharge: function(){
        this.model.collection.remove(this.model);
    },

    saveStartDate: function(){

        var start = moment(this.$el.find('input.start-date').val(), 'MM/DD/YYYY');
        if(!start.isValid()){
            this.$el.find('input.start-date').val('');
            return false;
        }

        this.model.set({
            'date_added': start.format('YYYY-MM-DD')
        });
    },

    onRender: function(){
        this.$el.find('input.start-date').datetimepicker({
            format: 'MM/DD/YYYY',
            useCurrent: false
        });
    }
});

var BulkEditCustomerMinimumChargesView = Backbone.Marionette.CollectionView.extend({
    childView: BulkEditCustomerMinimumChargeView,
    tagName: 'div',
    className: 'col-md-12'
});