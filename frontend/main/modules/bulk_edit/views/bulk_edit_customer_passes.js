var BulkEditCustomerPassView = Backbone.Marionette.ItemView.extend({
    template: JST['bulk_edit/templates/customer_pass.html'],
    tagName: 'div',
    className: 'row bulk-edit-field-record',

    events: {
        "click button.disconnect": "disconnect",
        "blur input": "saveDates"
    },

    onRender: function(){
        this.$el.find('input').datetimepicker();
    },

    disconnect: function(e){
        this.model.collection.remove(this.model);
        return false;
    },

    saveDates: function(event){
        var data = {};
        data.start_date = this.$el.find('input[name="start_date"]').val();
        data.expiration = this.$el.find('input[name="expiration"]').val();
        this.model.set(data);
    }
});

var BulkEditCustomerPassesView = Backbone.Marionette.CollectionView.extend({
    childView: BulkEditCustomerPassView,
    className: 'col-md-12'
});