var BulkEditCustomerHouseholdMemberView = Backbone.Marionette.ItemView.extend({
    template: JST['bulk_edit/templates/customer_household_member.html'],
    tagName: 'div',
    className: 'row bulk-edit-field-record',

    events: {
        "click button.delete": "removeMember"
    },

    removeMember: function(){
        this.model.collection.remove(this.model);
    }
});

var BulkEditCustomerHouseholdMembersView = Backbone.Marionette.CollectionView.extend({
    childView: BulkEditCustomerHouseholdMemberView,
    tagName: 'div',
    className: 'col-md-12'
});