var BulkEditCustomerRecurringChargeView = Backbone.Marionette.ItemView.extend({
    template: JST['bulk_edit/templates/customer_recurring_charge.html'],
    tagName: 'div',
    className: 'row bulk-edit-field-record',

    events: {
        "click button.delete": "removeGroup",
        "change input.prorate": "setProrate"
    },

    setProrate: function(){

        var model = this.model;
        var items = model.get('items');

        this.$el.find('input.prorate').each(function(){
            var chargeItemId = $(this).val();
            var item = items.get(chargeItemId);

            if($(this).is(':checked')){
                item.set('prorate', true);
            }else{
                item.set('prorate', false);
            }
        });
    },

    removeGroup: function(){
        this.model.collection.remove(this.model);
    }
});

var BulkEditCustomerRecurringChargesView = Backbone.Marionette.CollectionView.extend({
    childView: BulkEditCustomerRecurringChargeView,
    tagName: 'div',
    className: 'col-md-12'
});