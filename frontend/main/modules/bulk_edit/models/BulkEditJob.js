var BulkEditJob = Backbone.JsonRestModel.extend({

    type: 'bulkEditJobs',
    urlRoot: REST_API_COURSE_URL + '/bulkEditJobs',
    defaults: {
        type: 'customers',
        status: 'pending',
        percentComplete: 0,
        settings: {},
        recordIds: [],
        totalRecords: 0,
        recordsFailed: 0,
        recordsCompleted: 0,
        estimatedDuration: 0,
        response: {}
    },

        /*
    sync: function(method, model, options){

        if(!options.attrs && method !== 'read'){
            options.attrs = this.attributes;
        }

        if(options.attrs){
            var attributes = _.clone(options.attrs);
            attributes = _.omit(attributes, ['file', 'data']);

            options.contentType = 'application/json';
            options.data = JSON.stringify(attributes);
        }

        Backbone.Model.prototype.sync.call(this, method, model, options);
    }, */

    initialize: function(){
        this.estimate_duration();
        var required_fields = App.data.course.get('customer_field_settings');
    },

    get_fields: function(){
        var fields = this.get('fields');
        return fields[this.get('type')];
    },

    estimate_duration: function(){

        // For now we will say 1 record takes on average 10ms
        var seconds = parseInt(this.get('total_records') * 0.010);
        this.set('estimated_duration', seconds);
    }
});

var BulkEditJobCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/bulkEditJobs',
    model: BulkEditJob
});

Backbone.modelFactory.registerModel(BulkEditJob, BulkEditJobCollection);