// Marketing campaign controls to filter rows
var MarketingCampaignsControls = Backbone.Marionette.ItemView.extend({
    template: JST['marketing_campaigns_support/templates/marketing_campaigns_controls.html'],
    initialize: function() {
        this.handleTagUpdate();
    },
    handleTagUpdate: function(){
        this.render();
    },
    events:{
        'keyup input.course-filter':function(e) {
            console.log(e);
            var tbl = $('table.marketing-campaigns');
            tbl.bootstrapTable('filterBy',{});
            tbl.bootstrapTable('filterBy',{course_name:e.target.value});
        },
        'paste input.course-filter':function(e) {
            console.log(e);
            var tbl = $('table.marketing-campaigns');
            tbl.bootstrapTable('filterBy',{});
            tbl.bootstrapTable('filterBy',{course_name:e.target.value});
        }/*,
        'change input.show-deleted':function(e) {
            console.log($(e.target.parentElement).find(':checked'));
            var tbl = $('table.marketing-campaigns');
            tbl.bootstrapTable('filterBy',{});
            tbl.bootstrapTable('filterBy',{deleted:($(e.target.parentElement).find(':checked').length?true:false)});
        }
        */
    }
});