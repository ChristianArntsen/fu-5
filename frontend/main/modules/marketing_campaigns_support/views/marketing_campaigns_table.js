// Display marketing campaigns table
var MarketingCampaignsTable = Backbone.Marionette.ItemView.extend({
    template: JST['marketing_campaigns_support/templates/marketing_campaigns_table.html'],

    initialize: function() {
        this.collection = App.data.marketing_campaigns;
        //this.listenTo(this.collection, 'sync', this.updateRow);
        this.handleTagUpdate();
    },

    handleTagUpdate: function(){
        this.render();
    },

    updateRow: function(id){
        this.$el.find('table.marketing-campaigns').bootstrapTable('refresh',{'silent':true});
        var model = this.collection.get(id);
        var index = this.collection.indexOf(model);
        this.$el.find('table.marketing-campaigns').bootstrapTable('expandRow',index);
    },

    events:{
        'click button.is-draft':function(e){
            var target = e.target.dataset.id;
            console.log(target)
            var col = this.collection;
            var view = this;
            col.demote_to_draft(target,function(res){
                console.log(res);
                var row;
                var disp = 'is_draft\n\nSuccess: ';
                row =res.data.meta;
                disp += row.success+'\n\n';
                col.get(target).fetch();
                col.get(target).fetch({parse:true,success:function(res){
                    view.updateRow(target);
                }});

                window.alert(disp);
            });
        },
        'click button.is-sent':function(e){
            var target = e.target.dataset.id;
            console.log(target)
            var col = this.collection;
            var view = this;
            this.collection.promote_to_sent(target,function(res){
                console.log(res);
                var row;
                var disp = 'is_sent\n\nSuccess: ';
                row =res.data.meta;
                disp += row.success+'\n\n';
                col.get(target).fetch({success:function(){
                    view.updateRow(target);
                }});

                window.alert(disp);
            });
        }
    },

    onRender: function(){
        var view = this;
        this.table = this.$el.find('table.marketing-campaigns').bootstrapTable({
            classes: 'table-no-bordered table-no-background col-xs-12',

            url:BASE_URL + 'api_rest/index.php/marketing_campaigns',
            dataField:'data',
            uniqueId: 'id',
            responseHandler: function(res){
                console.log('res',res);
                for(var i=0;i<res.data.length;i++){
                    res.data[i].attributes.id = res.data[i].id;
                    res.data[i] = res.data[i].attributes;
                }
                return res;
            },
            sidePagination: 'client',
            //sortName:'gmt_logged',
            //sortOrder:'desc',
            columns: [
                {
                    title: 'Name',
                    field: 'name',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12' + (row.deleted?' deleted':'') + '" style="padding-bottom: 15px;">' +
                            val +
                            '</div>'
                    },
                    sortable:true
                },
                {
                    title: 'Status',
                    field: 'status',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            val +
                            '</div>'
                    },
                    sortable:true
                },
                {
                    title: 'Course',
                    field: 'course_name',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            val +
                            '</div>'
                    },
                    sortable:true
                },
                {
                    title: 'City',
                    field: 'course_city',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            val +
                            '</div>'
                    }
                },
                {
                    title: 'State',
                    field: 'course_state',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            val +
                            '</div>'
                    }
                },
                {
                    title: 'Created',
                    field: 'date_created',
                    formatter: function(val, row, idx){
                        //return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                        return val && val.date?val.date.split('.')[0]:'---'// +
                        //'</div>'
                    },
                    search:true,
                    sortable:true,
                    halign:'center',
                    sorter:function(a,b){
                        if(!a&&!b) return 0;
                        if(!a) return -1;
                        else if(!b)return 1;

                        return new Date(a.date) - new Date(b.date);
                    }
                },
                {
                    title: 'Sent',
                    field: 'send_date',
                    formatter: function(val, row, idx){
                        //return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                        return val && val.date?val.date.split('.')[0]:'---'// +
                        //'</div>'
                    },
                    search:true,
                    sortable:true,
                    halign:'center',
                    sorter:function(a,b){
                        if(!a&&!b) return 0;
                        if(!a) return -1;
                        else if(!b)return 1;

                        return new Date(a.date) - new Date(b.date);
                    }
                },
                {
                    title: 'Preview',
                    field: 'preview',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-12" style="padding-bottom: 15px;">' +
                            '<a target="_blank" href="' + BASE_URL + 'index.php/email/hostedEmail?email=' + val + '">Preview</a>' +
                            '</div>'
                    }
                }/*,
                {
                    field: 'id',
                    formatter: function(val, row, idx){
                        return '<div class="col-xs-6" style="padding-bottom: 15px;">' +
                            '<button class="btn btn-primary btn-block new undo" data-id="' + val + '">Undo</button>' +
                            '</div>'+
                            '<div class="col-xs-6" style="padding-bottom: 15px;">' +
                            '<button class="btn btn-primary btn-block new redo" data-id="' + val + '">Redo</button>' +
                            '</div>'
                    }
                }*/
            ],
            detailView:true,
            detailFormatter: function(index, row, element){
                var wrap = '<div class="col-xs-4" style="padding-bottom: 15px;">'
                var ret= '';
                //ret += wrap + 'Deleted: ' + row.deleted + '</div>';
                //ret += wrap + 'Queued: ' + row.queued + '</div>';
                //ret += wrap + 'Is Sent: ' + (row.is_sent?true:false) + '</div>';
                //ret += wrap + 'Attempts: ' + row.attempts + '</div>';
                ret += '<div class="col-xs-12 stats">'
                ret += ''
                ret += '</div>'
                return ret;
            },
            onExpandRow:function(index,row,detail){
                console.log(row);
                $.ajax({
                    url: BASE_URL + 'index.php/marketing_campaigns/ajaxGetCampaignStats',
                    method:'GET',
                    data:{
                        campaign_id:row.id,
                        course:row.course_id
                    },
                    dataType:'json',
                    success:function(res){
                        var wrap = '<div class="col-xs-4" style="padding-bottom: 15px;">';
                        var wrap2 = '<div class="col-xs-6" style="padding-bottom: 15px;">';
                        var stats = $(detail.find('.stats'));
                        var email = res.data.usage.email;
                        var sms = res.data.usage.text;
                        var ret = '<h3>Usage Stats</h3>';
                        ret += wrap2 + 'Email: ' + email.usage + '/' + email.limit + '</div>';
                        ret += wrap2 + 'SMS: ' + sms.usage + '/' + sms.limit + '</div>';
                        ret += '<h3>Course Stats</h3>';
                        ret += wrap + 'Bounced: ' + res.data.course.bounced + '</div>';
                        ret += wrap + 'Clicked: ' + res.data.course.clicked + '</div>';
                        ret += wrap + 'Delivered: ' + res.data.course.delivered + '</div>';
                        ret += wrap + 'Opened: ' + res.data.course.opened + '</div>';
                        ret += wrap + 'Processed: ' + res.data.course.processed + '</div>';
                        ret += wrap + 'Unsubscribed: ' + res.data.course.unsubscribed + '</div>';
                        ret += '<h3>Campaign Stats</h3>';
                        ret += wrap + 'Bounced: ' + res.data.campaign.bounced + '</div>';
                        ret += wrap + 'Clicked: ' + res.data.campaign.clicked + '</div>';
                        ret += wrap + 'Delivered: ' + res.data.campaign.delivered + '</div>';
                        ret += wrap + 'Opened: ' + res.data.campaign.opened + '</div>';
                        ret += wrap + 'Processed: ' + res.data.campaign.processed + '</div>';
                        ret += wrap + 'Unsubscribed: ' + res.data.campaign.unsubscribed + '</div>';


                        ret += '<h3>Diagnostics</h3>';
                        ret += wrap + 'Deleted: ' + row.deleted + '</div>';
                        ret += wrap + 'Queued: ' + row.queued + '</div>';
                        ret += wrap + 'Is Sent: ' + (row.is_sent?true:false) + '</div>';
                        ret += wrap + 'Attempts: ' + row.attempts + '</div>';

                        ret += '<div class="col-xs-3" style="padding-bottom: 15px;">' +
                            '<button class="btn btn-primary btn-block new is-draft" data-id="' + row.id + '">Demote to Draft</button>' +
                            '</div>'+
                            '<div class="col-xs-3" style="padding-bottom: 15px;">' +
                            '<button class="btn btn-primary btn-block new is-sent" data-id="' + row.id + '">Promote to Sent</button>' +
                            '</div>'
                        stats.append(ret);
                    }
                });
            },
            page: 1,
            pagination: true,
            pageSize: 50,
            pageNumber: 1,
            pageList: [10,25,50,100],
            search:true,
            onLoadSuccess: function(data){
                return;
                // seems a bit backward to have the table
                // control the collection.
                // might make sense for this use-case?
                var dt = {data:[]};
                for(var i = 0;i<data.data.length;i++){
                    dt.data.push({id:data.data[i].id,type:'marketing_campaigns',attributes:data});
                }
                view.collection.reset(dt,{parse:true});
            }
        });
    }
});