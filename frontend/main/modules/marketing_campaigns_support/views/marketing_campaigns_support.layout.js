var marketing_campaigns_support_layout = Backbone.Marionette.LayoutView.extend({

    tagName: 'section',
    
    attributes: {
        id: 'marketing-campaigns-support'
    },

    template: JST['marketing_campaigns_support/templates/marketing_campaigns_support.layout.html'],

    regions: {
        controls: '#marketing-campaigns-support-controls',
        disp_area: '#marketing-campaigns-support-disp-area'
    },

    events: {
        //'click button.course': 'load_course_logs'
    },

    onRender: function(){

        console.log('show controls...');
        this.controls.show(new MarketingCampaignsControls());
        this.disp_area.show(new MarketingCampaignsTable());
    },

    load_course_logs: function(e){
        App.data.course_selected = [e.target.dataset.id,e.target.dataset.name];
        this.disp_area.show(new PersonAlteredTable());
    }
});