describe("marketingCampaigns",function(){
    var mc = new MarketingCampaigns();
    it("should be defined",function(){
        expect(MarketingCampaigns).toBeDefined();
    });
    it("should create an instance of marketingCampaigns",function(){
        expect(mc instanceof MarketingCampaigns).toBe(true);
    });
    describe("demote_to_draft",function () {
        it("should demote a marketing campaign to draft");
    });

    describe("promote_to_sent",function () {
        it("should promote a marketing campaign to sent");
    });
});

describe("marketingCampaignsCollection",function(){
    it("should be defined",function(){
        expect(MarketingCampaignsCollection).toBeDefined();
    });
    it("should create an instance of marketingCampaignsCollection",function(){
        expect(mcc instanceof MarketingCampaignsCollection).toBe(true);
    });
    describe("demote_to_draft",function () {
        it("should demote a marketing campaign to draft");
    });

    describe("promote_to_sent",function () {
        it("should promote a marketing campaign to sent");
    });
});