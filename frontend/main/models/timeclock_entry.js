var TimeclockEntry = Backbone.Model.extend({
	defaults: {
		'total': '0',
		'shift_end': '0000-00-00 00:00:00',
		'shift_start': '0000-00-00 00:00:00',
		'terminal': '',
		'terminal_id': null
	}
});

var TimeclockEntryCollection = Backbone.Collection.extend({
	model: TimeclockEntry,
	url: API_URL + '/timeclock',
	
	comparator: function(model){
		return 0 - moment(model.attributes.shift_start).unix();
	},
	
	initialize: function(){
		this.on('error', this.show_error);
	},
	
	show_error: function(model, response){
		$.loadMask.hide();
		App.vent.trigger('notification', {'type': 'error', 'msg': response.responseJSON.msg});
		return true;
	},
	
	clock_in: function(person_id, password, terminal_id, callback){
		
		var collection = this;

		this.create({
			'employee_id': person_id,
			'password': password,
			'terminal_id': terminal_id
		
		}, {wait: true, url: this.url + '/clock_in', success: function(model, response){
			collection.sort();
			if(typeof(callback) == 'function'){
				callback(model, response);
			}
		}, 'error': function(model, response){
			collection.show_error(null, response);
		}});
	},
	
	clock_out: function(person_id, password, callback){
		
		var collection = this;
		
		$.post(this.url + '/clock_out', {'employee_id': person_id, 'password': password}, function(response){
			if(!response.success){
				return false;
			}
			
			var entry = collection.findWhere({'total': '0'});
			entry.set(response);
			collection.sort();
			
			if(typeof(callback) == 'function'){
				callback(entry, response);
			}
		},'json').fail(function(response){
			collection.show_error(null, response);	
		});		
	}
});
