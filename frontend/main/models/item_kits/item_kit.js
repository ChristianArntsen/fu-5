var ItemKit = Backbone.Model.extend({
	idAttribute: "item_kit_id",
	defaults: {
		"name": "",
		"item_number": "",
		"department": "",
		"category": "",
		"subcategory": "",
		"description": "",
		"unit_price": 0.00,
		"cost_price": 0.00,
        "max_discount":100,
		"is_punch_card": false,
		"items": [],
		"item_type": "item_kit"
	}
});

var ItemKitCollection = Backbone.Collection.extend({
	url: API_URL + '/item_kits',
	model: ItemKit
});