var PrintQueue = Backbone.Model.extend({

	defaults: {
		"currently_printing": [],
		"errors":[],
		"error_limit":10,
		"last_error_notificatin":new Date(),
		"last_printer_extension_notification":new Date(),
		"is_printing": false,
		"extension_nag_count":0
	},

	extension_id: "ndcdpiikdnlagfjejimgpmaflngcemoo",

	get_web_print: function(printer_url, keys){
		
		// Initialize WebPrint object
		var queue = this;
		var web_print = new StarWebPrintTrader({url: printer_url, checkedblock:false});
		
		// Printer successfully received and printed receipt
        web_print.onReceive = function(response){
        	
	        var msg = '- onReceive -\n\n';
	        msg += 'TraderSuccess : [ ' + response.traderSuccess + ' ]\n';
	        msg += 'TraderStatus : [ ' + response.traderStatus + ',\n';
	
	        if (web_print.isCoverOpen            ({traderStatus:response.traderStatus})) {msg += '\tCoverOpen,\n';}
	        if (web_print.isOffLine              ({traderStatus:response.traderStatus})) {msg += '\tOffLine,\n';}
	        if (web_print.isCompulsionSwitchClose({traderStatus:response.traderStatus})) {msg += '\tCompulsionSwitchClose,\n';}
	        if (web_print.isEtbCommandExecute    ({traderStatus:response.traderStatus})) {msg += '\tEtbCommandExecute,\n';}
	        if (web_print.isHighTemperatureStop  ({traderStatus:response.traderStatus})) {msg += '\tHighTemperatureStop,\n';}
	        if (web_print.isNonRecoverableError  ({traderStatus:response.traderStatus})) {msg += '\tNonRecoverableError,\n';}
	        if (web_print.isAutoCutterError      ({traderStatus:response.traderStatus})) {msg += '\tAutoCutterError,\n';}
	        if (web_print.isBlackMarkError       ({traderStatus:response.traderStatus})) {msg += '\tBlackMarkError,\n';}
	        if (web_print.isPaperEnd             ({traderStatus:response.traderStatus})) {msg += '\tPaperEnd,\n';}
	        if (web_print.isPaperNearEnd         ({traderStatus:response.traderStatus})) {msg += '\tPaperNearEnd,\n';}
	
	        msg += '\tEtbCounter = ' + web_print.extractionEtbCounter({traderStatus:response.traderStatus}).toString() + ' ]\n';
	        console.log('webPrnt onReceive msg - '+msg);
       		
       		// Mark as done printing
			queue.set_currently_printing(printer_url, false);
			
			// Remove receipts from queue
			_db.receipt.remove(keys);
	    }
		
		// If the printer has an error
		web_print.onError = function(response){

	        var msg = '- onError -\n\n';
	        msg += '\tStatus:' + response.status + '\n';
	        msg += '\tResponseText:' + response.responseText;
	        console.log('webPrnt onError msg - '+msg);
       		
       		// Mark as done printing
			queue.set_currently_printing(printer_url, false);
			queue.increase_error(printer_url);
			if(new Date() - queue.get("last_error_notificatin") > 60000*10){
				queue.set("last_error_notification",new Date())
				App.vent.trigger('notification', {'type': 'error', 'msg': "Error connecting to the printer."});
			}
	    }
	    
	    return web_print;			
	},
	
	is_printing: function(path){
		var currently_printing = this.get('currently_printing');
		if(currently_printing[path] != undefined && currently_printing[path] != false){
			return true;
		}
		return false;
	},

	set_currently_printing: function(path, print_status){
		
		var all_status = this.get('currently_printing');
		all_status[path] = print_status;
		this.set('currently_printing', all_status);
		
		var any_printing = false;
		_.each(this.get('currently_printing'), function(is_printing){
			if(is_printing){
				any_printing = true;
			}
		});
		
		this.set('is_printing', any_printing);
	},

	increase_error: function(path){

		var all_errors = this.get("errors");
		if(all_errors[path] == undefined){
			all_errors[path] = 0;
		}
		all_errors[path]++;

	},

	can_print: function(path){
		var all_errors = this.get("errors");
		if(all_errors[path] == undefined){
			all_errors[path] = 0;
		}

		return all_errors[path] < this.get("error_limit");
	},

	old_add_receipt: function(ip_address, data){
		var url = window.location.protocol + "//" + ip_address +"/StarWebPRNT/SendMessage";
		_db.receipt.add(data, url);
	},

	add_receipt: function(ip_address, data){
		var self = this;
		if(App.data.v2.settings.get('usePrinterExtension') == 1) {
			chrome.runtime.sendMessage(this.extension_id, {
					message: "add",
					ip_address: ip_address,
					data: data
				},
				function (reply) {
					var hasExtension = false;

					if (reply) {
						if (reply.version) {
							if (reply.version >= 0) {
								hasExtension = true;
							}
						}
					}

					if (!hasExtension) {
						self.old_add_receipt(ip_address, data);
					}
				}
			);
		}else {
			this.old_add_receipt(ip_address, data);
		}
	},
	
	// Groups receipts by IP address, so all receipts aimed for one 
	// printer can go in one batch
	group_receipt_data: function(receipts){
		
		var data = {};
		if(!receipts || receipts.length == 0){
			return false;
		}
		
		_.each(receipts, function(receipt){
			
			if(typeof(data[receipt.url]) != 'object'){
				data[receipt.url] = {'keys':[], 'data': ''};
			}
			data[receipt.url]['keys'].push({'key': receipt.key});
			data[receipt.url]['data'] += receipt.receipt_data;
		});
		
		return data;
	},

	old_print: function(ip_address){
		var print_url = window.location.protocol + "//" + ip_address +'/StarWebPRNT/SendMessage';

		if(ip_address != undefined && this.is_printing(print_url)){
			return false;
		}
		var receipts = [];
		var receipts_by_printer = [];
		var queue = this;

		// If IP address was passed in, only retreive receipts for that printer
		if(ip_address != undefined){
			receipts = _db.receipt.get(print_url);
			grouped_data = this.group_receipt_data(receipts);

			// If no IP address, retrieve ALL receipts
		}else{
			receipts = _db.receipt.get();
			grouped_data = this.group_receipt_data(receipts);
		}

		if(!grouped_data){
			return false;
		}

		// Loop through each "batch" of data, and send it to the printer
		_.each(grouped_data, function(group, receipt_url){

			if(!queue.can_print(receipt_url)){
				console.log("error limit reached");
				return false;
			}
			if(queue.is_printing(receipt_url)){
				return false;
			}
			queue.set_currently_printing(receipt_url, true);

			var web_print = queue.get_web_print(receipt_url, group.keys);

			web_print.sendMessage({request: group.data});
		});
	},

	print: function(ip_address){
		var self = this;
		if(App.data.v2.settings.get('usePrinterExtension') == 1) {
			//send print command to extension
			chrome.runtime.sendMessage(this.extension_id, {
					message: "print",
					ip_address: ip_address
				},
				function (reply) {
					var hasExtension = false;

					//check if extension is installed
					if (reply) {
						if (reply.version) {
							if (reply.version >= 0) {
								hasExtension = true;

								//check if customer needs to be redirected to https
								//if(window.location.protocol=="http:"){
								//	var httpURL= window.location.hostname + window.location.pathname + window.location.search;
								//	window.location = "https://" + httpURL;
								//}

								if (/Mobi/.test(navigator.userAgent)) {
									console.log("Not")
								}
							}
						}
					}else {
						var nags = self.get("extension_nag_count");
						if((new Date() - self.get("last_printer_extension_notification") > 60000*2 && nags < 3) || nags == 0){

							var installExtensionPrompt = new InstallExtensionPromptView({model: new Backbone.Model()});
							installExtensionPrompt.show();

							self.set("last_printer_extension_notification", new Date());
							self.set("extension_nag_count", nags++);
						}
					}

					//print using the old method if extension is not installed
					if (!hasExtension) {
						self.old_print(ip_address);
					}
				}
			);
		}else {
			//print using the old method if extension setting is turned off
			this.old_print(ip_address);
		}
	}
});
