// Course Modules
var Module = Backbone.Model.extend({
	idAttribute: "module_id",
	backbone: ['passes', 'item_kits'],

	getUrl: function () {
		var module_id = this.get('module_id');
		var url = '';

		if (module_id == 'teesheets' && App.data.course.get('tee_sheet_speed_up') == 1 && App.data.course.get('sales_v2') == 1) {
			url = SITE_URL + '/v2/home#tee_sheet';
		} else if (module_id == 'customers') {		
			url = SITE_URL + '/v2/home#new_customers';
		} else if (module_id == 'items') {
			url = SITE_URL + '/v2/home#items_old';
		} else if (module_id.indexOf('_v2') > -1 || this.backbone.indexOf(module_id) > -1) {
			url = SITE_URL + '/v2/home#' + module_id.replace('_v2', '');
		} else if (this.get('is_partner_app')) {
            url = "/integration/load/"+this.get("module_id");

        } else {
			url = SITE_URL + '/' + module_id;
		}

		return url;
	}
});

var ModuleCollection = Backbone.Collection.extend({
	model: Module
});

// Employees
var Employee = Backbone.Model.extend({
	idAttribute: "person_id",

	defaults: {
		"first_name": '',
		"last_name": '',
		"timeclock_history": []
	},

	initialize: function () {
		this.set('timeclock_history', new TimeclockEntryCollection(this.get('timeclock_history')));
	},
});

var EmployeeCollection = Backbone.Collection.extend({
	model: Employee,
	comparator: 'first_name',

	getDropdownData: function () {
		var data = {};
		_.each(this.models, function (employee) {
			data[' ' + employee.get('person_id')] = employee.get('first_name') + ' ' + employee.get('last_name');
		});
		return data;
	}
});

// Customer groups
var Group = Backbone.Model.extend({
	idAttribute: "group_id",
	defaults: {
		"label": "",
		"item_cost_plus_percent_discount": null
	}
});

var GroupCollection = Backbone.Collection.extend({
	model: Group,

	getBestCostPlusPercent: function(matching_groups){
		
		if(!matching_groups || matching_groups.length == 0){
			return false;
		}
		var group_collection = this;
		var best_discount = false;

		_.each(matching_groups.models, function(group){
			var matched_group = group_collection.findWhere({group_id: group.get('group_id')});

			if(matched_group && (!best_discount || matched_group.get('item_cost_plus_percent_discount') < best_discount)){
				best_discount = matched_group.get('item_cost_plus_percent_discount');
			}
		});

		return best_discount;
	}
});

// Receipt printers
var ReceiptPrinter = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {
        "label": '',
        "ipAddress": false
    }
});

var ReceiptPrinterCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/printers',
    model: ReceiptPrinter,

    initialize: function () {
        this.fetch()
    },

    getDropdownOptions: function(){
        var printers = {'null':'Default'};
        _.each(
            this.models,
            function(printer){
                if (printer.get('label') != '')
                    printers[printer.get('id')] = printer.get('label')
            }
        );
        return printers;
    }
});

// Receipt printer groups
var ReceiptPrinterGroup = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {
        "label": '',
        'defaultPrinterId':null
    }
});

var ReceiptPrinterGroupCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/printerGroups',
    model: ReceiptPrinterGroup,

    initialize: function () {
        this.fetch()
    }
});

// Loyalty Rate
var LoyaltyRate = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {
        "label": '',
        'type':'',
        'value':'',
        'valueLabel':'',
        'pointsPerDollar':'',
        'dollarsPerPoint':'',
        'priceCategory':0,
        'teeTimeIndex':0
    }
});

var LoyaltyRatesCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/loyaltyRates',
    model: LoyaltyRate,

    initialize: function () {
        this.fetch()
    }
});

// Receipt agreements
var ReceiptAgreement = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {
        SeparateReceipt: false,
        SignatureLine:false,
        content:"",
        ipAddress:false,
        label:"",
        name:""
    }
});

var ReceiptAgreementCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/receiptAgreements',
    model: ReceiptAgreement,

    initialize: function () {
        this.fetch()
    },

    getDropdownOptions: function(){
        var receipt_agreements = {'0':''};
        _.each(
            this.models,
            function(receipt_agreement){
                if (receipt_agreement.get('name') != '')
                    receipt_agreements[receipt_agreement.get('id')] = receipt_agreement.get('name')
            }
        );
        return receipt_agreements;
    }
});

// Receipt printers
var MealCourse = Backbone.Model.extend({
	idAttribute: "meal_course_id",
	comparator: 'order',
	defaults: {
		"name": '',
		"order": 0
	}
});

var MealCourseCollection = Backbone.Collection.extend({
	model: MealCourse,
	initialize: function(attributes,options){
	}
});

// Course terminals
var Terminal = Backbone.Model.extend({
    idAttribute: "terminal_id",

    defaults: {
        "label": "",
        "multi_cash_drawers": 0
    }
});

var TerminalCollection = Backbone.Collection.extend({
    model: Terminal,

    getDropdownData: function () {
        var data = {};
        _.each(this.models, function (terminal) {
            data[String(terminal.get('terminal_id'))] = terminal.get('label');
        });

        return data;
    },

    getActiveTerminal: function () {
        if (!this.models || this.models.length == 0) {
            return false;
        }
        var terminal = this.findWhere({'active': true});
        if (!terminal || terminal == undefined) {
            return false;
        }
        return terminal;
    },

    setActive: function (terminal_id) {
        var terminal = this.get(terminal_id);
        if (!terminal || terminal == undefined) {
            return false;
        }

        _.each(this.models, function (term) {
            term.set('active', false);
        });
        terminal.set('active', true);

        $.post(SITE_URL + '/v2/home/set_terminal/' + terminal_id, null, function (response) {

            if (response.register_log && response.register_log != null) {
                var register_log_id = response.register_log.register_log_id;
                App.data.register_logs.add(response.register_log);
                App.data.register_logs.set_active(register_log_id);
            } else {
                App.data.register_logs.set_active(false);
                App.data.register_logs.prompt_to_open_log();
            }

        }, 'json');

        return true;
    }
});
var PassRule = Backbone.Model.extend({
	idAttribute: 'rule_number',
	defaults: {
		is_valid: false,
		is_applied: false
	},

	applyItemRule: function(pass){
		var self = this;


		var removePass = false;
		if(this.get('is_applied')){
			removePass = true;
		}




		//Remove all items attached to this rule
		var modelsToRemove = [];
		App.data.cart.get("items").forEach(function(item,key){
			if(
				item.get("params").customer_id == pass.get("customer_id") &&
				item.get("params").rule_number == self.get("rule_number")
				){
				modelsToRemove.push(item);
			}
		});
		App.data.cart.get('items').remove(modelsToRemove);


		if(removePass){
			pass.collection.toggleSingleRule(pass.get('pass_id'), this.get('rule_number'));
		}else{
			//If we're deselecting the pass, remove the item
			//If we're applying the pass, adjust the discount of existing item or add not item
			_.forEach(this.get("items"),function(item,key){
				item.params =  {
					"customer_id": pass.get("customer_id"),
					"rule_number":self.get("rule_number"),
					"pass_id":pass.get('pass_id'),

				};
				App.data.cart.addItem(item)
			})
			pass.collection.toggleSingleRule(pass.get('pass_id'), this.get('rule_number'));
		}
	},
	applyRule: function(pass){
		
		if(!pass){
			return false;
		}

		var passId = pass.get('pass_id');
		var rule = this;
		if(!rule.get('is_valid')){
			return false;
		}
		if(rule.get("type") == "items"){
			return this.applyItemRule(pass);
		}

		if(App.data.cart.get('items').length == 0){
			App.vent.trigger('notification', {
				'msg': 'No valid fees in the cart',
				'type': 'error'
			});
			return false;
		}

		var active_customer = App.data.cart.get('customers').findWhere({selected: true});

		// If "undoing" the pass, use the customer's default price class
		var removePass = false;
		if(rule.get('is_applied')){
			priceClassId = active_customer.get('price_class');
			removePass = true;
		}else{
			priceClassId = rule.get('price_class_id');
		}

		var curDate = moment().format('YYYY-MM-DD HH:mm:00');
		var self = this;

		// Loop through items in cart and determine which teesheets we need
		// to get pricing for
		var pricing = {};
		_.each(App.data.cart.get('items').models, function(cartItem){
			
			if(cartItem.get('item_type') != 'green_fee' && cartItem.get('item_type') != 'cart_fee'){
				return false;
			}

			if(!cartItem.has('params') || 
				!cartItem.get('params').customer_id || 
				cartItem.get('params').customer_id != active_customer.get('person_id')
			){
				return false;
			}

			// Build array of teesheet IDs with corresponding items that will be changed
			var teesheet_id = cartItem.get('params').teesheet_id;
			if(!pricing[teesheet_id] || pricing[teesheet_id] == undefined){
				pricing[teesheet_id] = [];
			}
			pricing[teesheet_id].push(cartItem);
		});

		// If no green fees/cart fees were found in the cart, display an error
		if(pricing.length == 0){
			App.vent.trigger('notification', {
				'msg': 'No valid fees in the cart belonging to '+
					active_customer.get('first_name')+' '+active_customer.get('last_name'),
				'type': 'error'
			});
			return false;
		}

		if(removePass){
			pass.collection.setApplied(false, false);
		}else{
			pass.collection.setApplied(passId, rule.get('rule_number'));
		}
		
		_.each(pricing, function(cartItems, teesheet_id){
			
			// Disconnect pass from items in the cart
			if(removePass){
				_.each(cartItems, function(cartItem){
					
					var params = cartItem.get('params');
					if(params.pass_id){
						delete params.pass_id;
					}
					if(params.pass_rule_number){
						delete params.pass_rule_number;
					}
					if(params.pass_rule_id){
						delete params.pass_rule_id;
					}
					cartItem.set('params', params);
				});
			}

			if(!priceClassId || priceClassId == 0){
				_.each(cartItems, function(cartItem){
					var params = cartItem.get('params');
					params.pass_id = passId;
					cartItem.set('params', params);
					cartItem.save();				
				});					
			
			}else{
				// Get current pricing for the fee from server
				App.data.course.get('fees').loadFees(
					teesheet_id, 
					priceClassId,
					curDate,
					
					// Apply this new pricing to each cart item 
					function(fee){
						_.each(cartItems, function(cartItem){
							App.data.course.get('fees').getFee(
								teesheet_id, 
								priceClassId, 
								cartItem.get('item_type'), 
								cartItem.get('params').holes,
								curDate,
								
								function(fee){
									var data = fee.toJSON();
									var params = cartItem.get('params');
									data.params = params;
									data.params.customer_id = active_customer.get('person_id');
									
									if(!removePass){
										data.params.pass_rule_number = rule.get('rule_number');
										data.params.pass_rule_id = rule.get('uniq_id');
										data.params.pass_id = passId;
									}

									cartItem.setFee(data);
									cartItem.trigger('change:subtotal');
								}
							);						
						});
					}
				);					
			}
		});		
	
		return false;
	}		
});

var PassRuleCollection = Backbone.Collection.extend({
	model: PassRule,
	
	set_applied_rule: function(rule_number){
		
		var rule = this.findWhere({rule_number: rule_number});

		_.each(this.models, function(model){
			//It's possible to have multiple item rules applied at once but only possible to have a single teetime rule applied
			if(model.get("type") != "items"){
				model.set({'is_applied': false});
			}
		});

		if(rule){
			rule.set({'is_applied': true});
		}
	}
});

// Customer passes
var Pass = Backbone.Model.extend({
	idAttribute: "pass_id",

	defaults: {
		"pass_id": null,
		"name": "",
		"start_date": "",
		"end_date": "",
		"expired": 0,
		"deleted": 0,
		"is_valid": true,
		"is_applied": false,
		"price_class_id": false,
		"customers": [],
		"customer": false,
		"multi_customer": 0,
        "times_used": 0
	},

	set: function(attributes, options){
		if(attributes.customers !== undefined && !(attributes.customers instanceof CustomerCollection)){
			attributes.customers = new CustomerCollection( attributes.customers );
		}
		if(attributes.rules !== undefined && !(attributes.rules instanceof PassRuleCollection)){
			attributes.rules = new PassRuleCollection( attributes.rules );
		}
		return Backbone.Model.prototype.set.call(this, attributes, options);
	},

	isValid: function () {
		var now = moment();

		if(!this.get('is_valid')){
			return false;
		}

		var start = moment(this.get('start_date'));
		if (this.get('start_date') && start.isValid() && now.isBefore(start)) {
			return false;
		}

		var end = moment(this.get('end_date'));
		if (this.get('end_date') && end.isValid() && now.isAfter(end)) {
			return false;
		}

		return true;
	}
});

var PassCollection = Backbone.Collection.extend({
	model: Pass,

	setApplied: function (passId, ruleNumber, applied) {
		if(typeof applied == "undefined"){
			applied = true;
		}
		_.each(this.models, function(pass){
			
			var rules = false;
			if(pass.get('rules')){
				rules = pass.get('rules').models;
			}

			if(!rules || rules.length == 0){
				return false;
			}

			var passIsBeingUsed = false;
			_.each(rules, function(rule){
				if(passId == pass.get('pass_id') && rule.get('rule_number') == ruleNumber){
					rule.set('is_applied', applied);
				}else if(rule.get("type") != "items"){
					rule.set('is_applied', false);
				}
				if(rule.get("is_applied")){
					passIsBeingUsed = true;
				}
			});

			if(passIsBeingUsed){
				pass.set('is_applied', true);
			}else{
				pass.set('is_applied', false);
			}
		});
	},

	toggleSingleRule: function(passId,ruleNumber){
		var self = this;
		_.each(this.models, function(pass){
			if(passId == pass.get('pass_id')){
				_.each(pass.get('rules').models, function(rule){
					if(rule.get('rule_number') == ruleNumber){
						rule.set('is_applied', !rule.get("is_applied"));
					}
				});
			}
			self.setPassBeingUsed(pass);
		});

	},
	setPassBeingUsed : function(pass){
		var passIsBeingUsed = false;
		_.each(pass.get('rules').models, function(rule){
			if(rule.get("is_applied")){
				passIsBeingUsed = true;
			}
		});

		if(passIsBeingUsed){
			pass.set('is_applied', true);
		}else{
			pass.set('is_applied', false);
		}
	}
});

// Green fees/cart fees
var Fee = Backbone.Model.extend({
	defaults: {
		"name": "",
		"type": "",
		"price": 0.00,
		"price_class_id": false,
		"timeframe_id": false,
		"special_id": false,
		"holes": 9,
		"taxes": []
	}
});

var FeeCollection = Backbone.Collection.extend({

	model: Fee,

	initialize: function () {
		this.pricing = {};
	},

	filterFees: function(params){
		
		var price_class_filter = false;
		if(params.price_class_ids){
			price_class_filter = _.clone(params.price_class_ids);
			delete params.price_class_ids;
		}

		var include_specials = true;
		if(_.has(params, 'include_specials')){
			if(!params.include_specials){
				include_specials = false;
			}
			delete params.include_specials;
		}
		var models = this.where(params);

		if(!models || models.length == 0){
			return models;
		}

		if(price_class_filter){
			models = _.filter(models, function(model){
				if(price_class_filter.indexOf(model.get('price_class_id')) >= 0 ||
					(model.get('special_id') != 0 && include_specials)){
					return true;
				}
				return false;
			});
		}

		return models;
	},

	// Takes a list of fees and returns all the season IDs that are used within those fees
	// for generating a valid dropdown of seasons
	get_valid_season_ids: function(fees){

		var season_ids = _.map(fees, function(fee){ return parseInt(fee.get('season_id')); });
		if(!season_ids){
			return false;
		}

		return _.unique(season_ids);
	},

	getMenuData: function (fees) {

		var menu = {};
		_.each(fees, function (fee) {
			menu[fee.cid] = fee.get('name');
		});

		return menu;
	},

	getCache: function (key) {
		if (!this.pricing[key] || this.pricing[key] == undefined) {
			return false;
		} else {
			return this.pricing[key];
		}
	},

	loadFees: function (teesheet_id, price_class_id, time, callback) {

		var self = this;
		var requestParams = {
			teesheet_id: teesheet_id,
			price_class_id: price_class_id,
			time: time
		};
		var key = teesheet_id + '_' + price_class_id + '_' + time;

		$.get(API_URL + '/fees', requestParams, function (response) {

			if (response && response.timeframe_id) {
				self.pricing[key] = response.timeframe_id;
			}

			if (callback && typeof(callback) == 'function') {
				callback(self.getCache(key));
			}
		}, 'json');
	},

	getFee: function (teesheet_id, price_class_id, type, holes, time, callback) {

		var self = this;
		var key = teesheet_id + '_' + price_class_id + '_' + time;

		if (this.getCache(key)) {

			var timeframe_id = this.getCache(key);
			var fee = self.findWhere({
				'timeframe_id': parseInt(timeframe_id),
				'holes': parseInt(holes),
				'item_type': type
			});

			if (callback && typeof(callback) == 'function') {
				callback(fee);
			}
			return true;
		}

		return false;
	}
});

// Course tee sheets
var Teesheet = Backbone.Model.extend({
    idAttribute: "teesheet_id",

    defaults: {

    }
});

var TeesheetCollection = Backbone.Collection.extend({
    model: Teesheet,
    getMenuData: function(teesheets){
        var menu = {};
        _.each(teesheets, function (teesheet) {
            menu[teesheet.id] = teesheet.get('title');
        });

        return menu;
    }
});

// Course tee sheets
var TeesheetV2 = Backbone.JsonRestModel.extend({
	idAttribute: "id",

	defaults: {

	}
});

var TeesheetCollectionV2 = Backbone.JsonRestCollection.extend({
    url: function(){ return REST_API_COURSE_URL + '/teesheets'},
    model: TeesheetV2,
	initialize:function(){
        this.fetch({
            data:{
                include: "onlineHours"
            }
        });
    },
	getMenuData: function(){
        var menu = {0:''};
        _.each(this.models, function (teesheet) {
            menu[teesheet.id] = teesheet.get('title');
        });

        return menu;
    }
});

// Reservation Groups
var ReservationGroup = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {
        allowNameEntry:0,
        bookingCarts:0,
        bookingFeeEnabled:0,
        bookingFeeItemId:0,
        bookingFeePerPerson:0,
        bookingFeeTerms:0,
        daysInBookingWindow:7,
        hideOnlinePrices:0,
        active:1,
        isAggregate:0,
        limitHoles:0,
        minimumPlayers:0,
        name:'',
        onlineBookingProtected:0,
        onlineCloseTime:2359,
        onlineOpenTime:0,
        onlineRateSetting:1,
        passItemIds:'',
        payOnline:0,
        priceClass:0,
        requireCreditCard:0,
        showFullDetails:0,
        useCustomerPricing:0
    }
});

var ReservationGroupCollection = Backbone.JsonRestCollection.extend({
    url: function(){ return REST_API_COURSE_URL + '/teesheets/'+ this.tee_sheet_id + '/reservationGroups'},
    model: ReservationGroup,
    tee_sheet_id: '',
    initialize: function () {

    }
});

// Specials
var Special = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {
        "isActive": '',
        'name':'',
        'priceClass':'',
        'onlineBookingProtected':'',
        'requireCreditCard':'',
        'onlineOpenTime':'',
        'onlineCloseTime':0,
        'daysInBookingWindow':0,
        'minimumPlayers':0,
        'limitHoles':0,
        'bookingCarts':0,
        'deleted':0,
        'useCustomerPricing':0,
        'isAggregate':0,
        'payOnline':0,
        'hideOnlinePricing':0,
        'onlineRateSetting':0,
        'showFullDetails':0,
        'passItemIds':0,
        'bookingFeeItemId':0,
        'bookingFeeTerms':0,
        'bookingFeePerPerson':0,
        'allowNameEntry':0
    }
});

var SpecialsCollection = Backbone.JsonRestCollection.extend({
    url: function(){ return REST_API_COURSE_URL + '/teesheets/'+ this.tee_sheet_id + '/specials'},
    model: Special,
    tee_sheet_id: '',
    initialize: function () {

    }
});

// Seasons
var SeasonV2 = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {
        "isActive": '',
        'name':'',
        'priceClass':'',
        'onlineBookingProtected':'',
        'requireCreditCard':'',
        'onlineOpenTime':'',
        'onlineCloseTime':0,
        'daysInBookingWindow':0,
        'minimumPlayers':0,
        'limitHoles':0,
        'bookingCarts':0,
        'deleted':0,
        'useCustomerPricing':0,
        'isAggregate':0,
        'payOnline':0,
        'hideOnlinePricing':0,
        'onlineRateSetting':0,
        'showFullDetails':0,
        'passItemIds':0,
        'bookingFeeItemId':0,
        'bookingFeeTerms':0,
        'bookingFeePerPerson':0,
        'allowNameEntry':0
    }
});

var SeasonCollectionV2 = Backbone.JsonRestCollection.extend({
    url: function(){ return REST_API_COURSE_URL + '/teesheets/'+ this.tee_sheet_id + '/seasons'},
    model: SeasonV2,
    tee_sheet_id: '',
    initialize: function () {

    }
});

// Seasons
var SeasonPriceClassV2 = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {

    }
});

var SeasonPriceClassCollectionV2 = Backbone.JsonRestCollection.extend({
    url: function(){ return REST_API_COURSE_URL + '/teesheets/'+ this.tee_sheet_id + '/seasons/' + this.season_id + '/priceClasses'},
    model: SeasonPriceClassV2,
    tee_sheet_id: '',
    season_id: '',
    initialize: function () {

    }
});

// Seasons
var SeasonTimeframeV2 = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {
        "isActive": '',
        'name':'',
        'priceClass':'',
        'onlineBookingProtected':'',
        'requireCreditCard':'',
        'onlineOpenTime':'',
        'onlineCloseTime':0,
        'daysInBookingWindow':0,
        'minimumPlayers':0,
        'limitHoles':0,
        'bookingCarts':0,
        'deleted':0,
        'useCustomerPricing':0,
        'isAggregate':0,
        'payOnline':0,
        'hideOnlinePricing':0,
        'onlineRateSetting':0,
        'showFullDetails':0,
        'passItemIds':0,
        'bookingFeeItemId':0,
        'bookingFeeTerms':0,
        'bookingFeePerPerson':0,
        'allowNameEntry':0
    }
});

var SeasonTimeframeCollectionV2 = Backbone.JsonRestCollection.extend({
    url: function(){ return REST_API_COURSE_URL + '/teesheets/'+ this.tee_sheet_id + '/seasons/' + this.season_id + '/timeframes'},
    model: SeasonTimeframeV2,
    tee_sheet_id: '',
    season_id: '',
    initialize: function () {

    }
});

var POSStats = Backbone.Model.extend({

    url: API_URL + '/cart/stats',
    defaults: {
        "total_sales": 0.00,
        "average_sale": 0.00
    },

    initialize: function () {
        this.listenTo(App.data.recent_transactions, 'add', this.refresh)
        this.listenTo(App.router, 'route:sales', this.refresh)
    },

    // Only refresh sales stats after 60 seconds of inactivity
    // so we don't bombard the server with extra requests
    refresh: _.debounce(function () {

        if (App.data.user.get('sales_stats') == 1) {
            return false;
        }
        this.fetch();

    }, 60000)
});

var RefundReasons = Backbone.Model.extend({

    url: API_URL + '/courses/refund_reasons',
    defaults: {},

    initialize: function () {
        this.fetch()
    }
});

// // Refund reasons
// var RefundReason = Backbone.JsonRestModel.extend({
//     idAttribute: "id",
//
//     defaults: {
//         'label':''
//     }
// });
//
// var RefundReasonCollection = Backbone.JsonRestCollection.extend({
//     url: REST_API_COURSE_URL + '/refundReasons',
//     model: RefundReason,
//
//     initialize: function () {
//         this.fetch()
//     }
// });

// Custom payments
var CustomPaymentType = Backbone.JsonRestModel.extend({
    idAttribute: "id",

    defaults: {
        label:'',
        customPaymentType:''
    }
});

var CustomPaymentTypeCollection = Backbone.JsonRestCollection.extend({
    url: REST_API_COURSE_URL + '/customPaymentTypes',
    model: CustomPaymentType,

    initialize: function () {
        this.fetch({async:false});
    }
});

var Course = Backbone.Model.extend({

	idAttribute: 'course_id',
	urlRoot: API_URL + '/courses',

	defaults: {
		name: '',
		address: '',
		groups: [],
		passes: [],
		fees: [],
		teesheets: [],
		terminals: [],
		receipt_printers: [],
		special_items: [],
		receipt_sections: [],
		custom_payments: [],
		pos_stats: null,
        refund_reasons: ['Defective item']
	},

	initialize: function () {
        this.set('multiCashDrawers', this.get('multi_cash_drawers'));
		this.set('groups', new GroupCollection(this.get('groups')));
		this.set('passes', new PassCollection(this.get('passes')));
		this.set('fees', new FeeCollection(this.get('fees')));
		this.set('teesheets', new TeesheetCollection(this.get('teesheets')));
		this.set('employees', new EmployeeCollection(this.get('employees')));
        this.set('terminals', new TerminalCollection(this.get('terminals')));
        this.set('receipt_printers', new ReceiptPrinterCollection());
        this.set('receipt_printer_groups', new ReceiptPrinterGroupCollection());
        this.set('receipt_agreements', new ReceiptAgreementCollection());
        this.set('meal_courses', new MealCourseCollection(this.get('meal_courses')));
		this.set('pos_stats', new POSStats(this.get('pos_stats')));
        //this.set('refund_reasons', new RefundReasonCollection());
        this.set('refund_reasons', new RefundReasons(this.get('refund_reasons')));
        this.set('custom_payments', new CustomPaymentTypeCollection());
        this.set('customer_page_settings', new PageSettingsRestModel({id: "customers", module:"customers"}));
        this.set('employee_page_settings', new PageSettingsRestModel({id: "employees",module: "employees"}));
        this.set('billing_page_settings', new PageSettingsRestModel({id: "billing",module: "billing"}));
        this.set('recurring_charges_page_settings', new PageSettingsRestModel({id: "recurring-charges",module: "billing"}));
        this.set('loyalty_rates', new LoyaltyRatesCollection());
        this.set('booking_classes', new ReservationGroupCollection());
        this.set('specials', new SpecialsCollection());
        this.set('seasons', new SeasonCollectionV2());
        this.set('season_price_classes', new SeasonPriceClassCollectionV2());
        this.set('season_timeframes', new SeasonTimeframeCollectionV2());

    },

	get_special_item: function (type) {

		var special_items = this.get('special_items');
		if (special_items && special_items[type] != undefined) {

			var item = _.clone(special_items[type]);

			if (item.taxes && item.taxes != undefined && item.taxes.length > 0) {
				var copiedTaxes = [];
				_.each(item.taxes, function (tax) {
					copiedTaxes.push(_.clone(tax));
				});
				item.taxes = copiedTaxes;
			}

			return item;
		}

		return false;
	},

	get_custom_payments: function () {

		var custom_payments = this.get('custom_payments');
		if (custom_payments) {
			return _.clone(custom_payments);
		}

		return false;
	},

	// Terminals can override certain course settings, like auto printing
	// receipts. First check if the terminal has something set, if not 
	// fall back to the course setting
	get_setting: function (setting) {
		var terminal = this.get('terminals').findWhere({'active': true});
		if (terminal && terminal != undefined) {
			if (terminal.has(setting) && terminal.get(setting) != null && terminal.get(setting) != '') {

				if (setting == 'receipt_ip' && this.get('multiple_printers') == 1) {
					var printer = this.get('receipt_printers').get(terminal.get(setting));

					if (!printer || printer == undefined) {
						return false;
					}
					return printer.get('ipAddress');
				}

				return terminal.get(setting);
			}
		}

		var key = this.get_key(setting);
		if (this.has(key)) {

			if (setting == 'receipt_ip' && this.get('multiple_printers') == 1) {
                var printer = this.get('receipt_printers').get(this.get(key));
				if (!printer || printer == undefined) {
					return false;
				}
				return printer.get('ipAddress');
			}

			if (key == 'print_credit_card_receipt') {
				if (this.get(key) == 1 && this.get('print_two_receipts') == 1) {
					return 2;
				} else if (this.get(key) == 1) {
					return 1;
				} else {
					return 0;
				}
			}

			if (key == 'print_sales_receipt') {
				if (this.get(key) == 1 && this.get('print_two_receipts_other') == 1) {
					return 2;
				} else if (this.get(key) == 1) {
					return 1;
				} else if (this.get(key) == 0) {
					return 0;
				}
			}

			if (key == 'print_two_signature_slips') {
				if (this.get(key) == 1) {
					return 2;
				} else {
					return 1;
				}
			}

			return this.get(key);
		}
	},

	get_key: function (key) {

		// Most of the terminal settings are named differently than
		// the same course settings, use object to map the keys
		var keys = {
			'auto_print_receipts': 'print_after_sale',
			'webprnt': 'webprnt',
			'receipt_ip': 'webprnt_ip',
			'hot_webprnt_ip': 'webprnt_hot_ip',
			'cold_webprnt_ip': 'webprnt_cold_ip',
			'use_register_log': 'track_cash',
			'cash_register': 'cash_drawer_on_cash',
			'print_tip_line': 'print_tip_line',
			'signature_slip_count': 'print_two_signature_slips',
			'credit_card_receipt_count': 'print_credit_card_receipt',
			'non_credit_card_receipt_count': 'print_sales_receipt',
			'after_sale_load': 'after_sale_load'
		};

		if (keys[key] != undefined) {
			return keys[key];
		}
		return key;
	}
});
