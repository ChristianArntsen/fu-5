var FbRecentTransaction = Backbone.Model.extend({
	idAttribute: "sale_id",
	defaults: {
		"total":0.00,
		"customer_name":"",
		"sale_time": "",
		"tips": [],
		"payments": []
	},

	initialize: function(attrs, options){
		var tips = new FbTipCollection( this.get('payments') );
		tips.url = this.collection.url + '/' + this.get('sale_id') + '/tips';

		this.set({'tips': tips});
	}
}); 

var FbTip = Backbone.Model.extend({
	defaults: {
		"amount": 0.00,
		"invoice_id": 0,
		"type": "",
		"payment_type":"",
		"tip_recipient": 0,
		"customer_id": 0,
		"tip_recipient_name": null
	},
	
	initialize: function(){
		if(this.get('invoice_id') && this.get('invoice_id') > 0){
			this.set('id', this.get('type') +':'+ this.get('invoice_id'));
		}else{
			this.set('id', this.get('type'));
		}
	}
});

var FbRecentTransactionCollection = Backbone.Collection.extend({
	url: SALES_URL,
	model: function(attrs, options){
		return new FbRecentTransaction(attrs, options);
	}
});

var FbTipCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		return new FbTip(attrs, options);
	}
});
