var FbItemSide = FbSide.extend({
	idAttribute: "position",
	default: {
		'item_id': 0,
		'price': 0,
		'quantity': 1,
		'tax': 0,
		'total': 0,
		'subtotal': 0,
		'discount': 0,
		"printer_ip": "",
		"print_priority": 0,
		"comp_total": 0,
		"subtotal_no_discount": 0,
		"taxes": []
	},

	initialize: function(attrs, options){
		
		this.cart = false;
		if(options && options.cart){
			this.cart = options.cart;
		}	
		var modifierCollection = new ModifierCollection(this.get('modifiers'));

		this.set({
			modifiers: modifierCollection
		});
		this.listenTo(this.get("modifiers"), "change", this.calculatePrice);

		if(this.cart instanceof Cart){
			this.listenTo(this.cart, 'change:taxable', this.calculatePrice);
			
			if(App.data.course.get('unit_price_includes_tax') == 1){
				this.set({'unit_price_includes_tax':true}, {silent: true});
			}else{
				this.set({'unit_price_includes_tax':false}, {silent: true});
			}
		}
	}
});

var FbItemSideCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		return new FbItemSide(attrs, options);
	},

	getTotalPrice: function(){
		var total = 0.00;

		if(this.models.length > 0){
			_.each(this.models, function(side){
				total += parseFloat(side.get('price'));
			});
		}
		return parseFloat(accounting.toFixed(total, 2));
	},

	getTotalTax: function(){
		var total = 0.00;

		if(this.models.length > 0){
			_.each(this.models, function(side){
				total += parseFloat(side.get('tax'));
			});
		}
		return parseFloat(accounting.toFixed(total, 2));
	}
});
