var Order = Backbone.Model.extend({
	
	url: function(){
		return this.instanceUrl;
	},
	idAttribute: "ticket_id",
	
	defaults: {
		"sale_id": null,
		"ticket_id": null,
		"items": null,
		"message": null,
		"printed": false,
		"deleted": false,
		"completed": false,
		"date_created": null,
		"date_ordered": null,
		"date_completed": null,
		"pos": false,
		"meal_course_id": 0,
		"customers": [],
		"guest_count": false,
		"table_number": false,
		"table_name": false
	},

	initialize: function(attributes, options){
		if(options && options.url){
			this.instanceUrl = options.url;
		}
		if(typeof options.table !== 'undefined'){
			this.table = options.table;
		}
		this.set('customers', new CustomerCollection(this.get('customers')));
		
		// When order is made, mark items in cart as "ordered"
		this.listenTo(this, 'sync', this.markItems, this);

		// When order is made, print order receipt in kitchen
		this.listenTo(this, 'sync', this.printOrder, this);
		this.listenTo(this, 'invalid error', this.displayError, this);
	},
	
	displayError: function(model, message){
		
		var msg = false;
		if(message.responseJSON && message.responseJSON.msg){
			msg = message.responseJSON.msg;
		}else if(message != undefined){
			msg = message;
		}
		App.vent.trigger('notification', {msg: msg, type: 'error'});
	},
	
	// Override the default backbone save function
	// We only need to send certain fields to the server when making an order
    save: function(attrs, options) {
		options || (options = {});

		// Data that will be sent to server
		var modifiedAttrs = {
			'items':[], 
			'message':this.get('message'), 
			'sale_id': this.get('sale_id'),
			'meal_course_id': this.get('meal_course_id'),
			'table_number': this.get('table_number'),
			'table_name': this.get('table_name')
		};
			
		_.each(this.get('items'), function(item){
			modifiedAttrs.items.push({
				'line': parseInt(item.get('line')),
				'item_id': parseInt(item.get('item_id'))
			});
		});

		options.data = JSON.stringify(modifiedAttrs);
		options.contentType = 'application/json';

        // Proxy the call to the original save function
        Backbone.Model.prototype.save.call(this, attrs, options);
    },

	markItems: function(order){
		if(!this.get('pos')){
			var cart = App.data.food_bev_table.get('cart');
			
			// Loop through each item in order, mark as ordered
			_.each(order.get('items'), function(orderItem){
				var cartItem = cart.get(orderItem.get('line'));
				cartItem.set('is_ordered', true);
			});

			cart.unSelectItems();
		}
		App.vent.trigger('notification', {'msg': 'Order sent', 'type': 'success'});
	},

	validate: function(){
		var complete = true;
		
		// Loop through each item in order and check if they are complete
		if(!this.get('pos')){
			
			_.each(this.get('items'), function(orderItem){
				// TODO: Remove calls to global App.*
				var item = App.data.food_bev_table.get('cart').get(orderItem.get('line'));
				if(!item.isComplete()){
					item.set({'incomplete':true});
					complete = false;
				}
			});

			if(!complete){
				App.data.food_bev_table.get('cart').unSelectItems();
				return 'Order failed. Please complete marked items';
			}
		}
	},

	// Print function called AFTER order is successfully sent to database
	// and has received a response
	printOrder: function(){
		
		var data = _.clone(this.attributes);
		var receipt = new FBOrderPrintReceipt(data);
		receipt.print_network();
		
		return true;
	}
});
