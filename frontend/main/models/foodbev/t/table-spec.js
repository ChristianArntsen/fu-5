if(typeof API_URL === 'undefined'){
    API_URL = '/api';
}

describe("The FbTable",function(){

    // requirements tests
    var tbl = new FbTable();//App.data.food_bev_table; // this for integration...
    it("should be defined",function(){
        expect(FbTable).toBeDefined();
    });
    it("should create an instance of itself",function(){
        expect(tbl).toBeDefined();
        expect(tbl instanceof  FbTable).toBe(true);
    });

    describe("initial state",function(){
        it("should have a null id",function(){
            expect(tbl.id).toBe(null);
        });

        it("should start with no receipts",function(){
            expect(tbl.get('receipts').length).toBe(0);
        });

        it("should start with no customers",function(){
            expect(tbl.get('customers').length).toBe(0);
        });
    });

    // observed: upon selecting table, open fires, 
    // then generate url, then updateObjectlayout,then updateAutoGratuity
    // the open method also has these side-effects

    describe("open method",function(){
        it("should be defined",function(){
            expect(tbl.open).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.open === 'function').toBe(true);
        });

        // TODO: figure out how to test if it works...
        
        it("should accept an open request",function(){
            tbl.open("101");
            expect(tbl.id).toBe("101");
        });

        it("should have a table number",function(){
            expect(tbl.get('table_num')).toBe('101');
        });

    });

    describe("generateUrl method",function(){
        it("should be defined",function(){
            expect(tbl.generateUrl).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.generateUrl === 'function').toBe(true);
        });

        // TODO: figure out how to test if it works...
        it("should generate and set the url when called",function(){
            tbl.url = 'cow';
            expect(tbl.url).toBe('cow');
            expect(tbl.generateUrl()).toBe('https://development.foreupsoftware.com/api/food_and_beverage/service/101/');
            expect(tbl.url).toBe('https://development.foreupsoftware.com/api/food_and_beverage/service/101/');
        });
    });

    describe("close method",function(){
        it("should be defined",function(){
            expect(tbl.close).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.close === 'function').toBe(true);
        });

        it("should return true on success",function() {
            expect(tbl.close()).toBe(true);
        });

        it("should call the destroy method",function(){
            
            tbl.open('101');
            spyOn(tbl,'destroy').and.callThrough();

            tbl.close();

            expect(tbl.destroy.calls.count()).toBe(1);
        });

        // if this is not called, the table still keeps old stuff.
        // Seems like it would be considered unexpected behaviour...
        it("should result in a call to the resetTable method",function(done){
            tbl.open('101');
            spyOn(tbl,'resetTable').and.callThrough();
            expect(tbl.get('table_num')).toBe('101');

            var callback = function(){
                expect(tbl.resetTable.calls.count()>=1).toBe(true);
                expect(tbl.get('table_num')).toBe(null);
                done();
            }

            tbl.close(callback);

        },15000);

        // not sure about the behaviour expected
        it("should call loadRecentTransactions",function(done){
            tbl.open('101');
            // wrap internally called methods with Jasmine spies
            spyOn(tbl,'loadRecentTransactions').and.callThrough();
            // call the function

            var callback = function(){
                expect(tbl.loadRecentTransactions.calls.count()>=1).toBe(true);
                done();
            }

            tbl.close(callback);
        },15000);
        
        // TODO: expand coverage to additional states of the closed Table
    });

    describe("updateAutoGratuity method",function(){
        tbl.open('101');
        it("should be defined",function(){
            expect(tbl.updateAutoGratuity).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.updateAutoGratuity === 'function').toBe(true);
        });

        // confirm state
        it('guest count should be 0',function(){
            expect(tbl.get('guest_count')).toBe(null);
        });
        it('auto gratuity threshold should be "0"',function(){
            expect(App.data.course.get('auto_gratuity_threshold')).toBe("0");
        });
        it('auto gratuity percent should be "18.00"',function(){
            expect(App.data.course.get('auto_gratuity')).toBe("18.00");
        });
        it('should not have any receipts',function(){
            expect(tbl.get('receipts').length).toBe(0);
        });

        it('should return null for lack of receipts',function(){
            expect(tbl.updateAutoGratuity()).toBe(null);

        });
        // TODO: figure out how to mock up additional states
    });

    describe("addCustomer method",function(){
        it("should be defined",function(){
            expect(tbl.addCustomer).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.addCustomer === 'function').toBe(true);
        });
        var customer = new Customer();
        customer.urlRoot = API_URL + '/customers?q=';
        customer.url = function(){
            return customer.urlRoot + 'bob hess';
        }
        console.log(customer.urlRoot);

        it("should start with no receipts",function(){
            expect(tbl.get('receipts').length).toBe(0);
        });

        it("should accept a customer",function(done) {
            customer.fetch({
                success: function (mod, res, opt) {
                    expect(customer.get('person_id')).toBe(4045);
                    expect(tbl.addCustomer(customer.attributes)).toBe(true);
                    expect(tbl.get('customers').length).toBe(1);
                    expect(tbl.get('receipts').length).toBe(1);
                    done();
                }
            });
        },15000);
    });

    describe("applyCustomerDiscount method",function(){
        it("should be defined",function(){
            expect(tbl.applyCustomerDiscount).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.applyCustomerDiscount === 'function').toBe(true);
        });

        // TODO: figure out how to test if it works...
    });

    describe("addCustomerToReceipt method",function(){
        it("should be defined",function(){
            expect(tbl.addCustomerToReceipt).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.addCustomerToReceipt === 'function').toBe(true);
        });

        // TODO: figure out how to test if it works...
    });

    describe("resetTable method",function(){
        it("should be defined",function(){
            expect(tbl.resetTable).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.resetTable === 'function').toBe(true);
        });

        // TODO: figure out how to test if it works...

        it('should have an initial state that is not null',function(){
            expect(tbl.get('suspended_sale_id')).not.toBe(null);
            expect(tbl.get('name')).not.toBe(null);
            expect(tbl.get('sale_time')).not.toBe(null);
            expect(tbl.get('table_num')).not.toBe(null);
        });

        it('should set a great many things null',function(){
            expect(tbl.get('suspended_sale_id')).toBe(null);
            expect(tbl.get('name')).toBe(null);
            expect(tbl.get('sale_time')).toBe(null);
            expect(tbl.get('table_num')).toBe(null);
            tbl.close();
        });
    });

    describe("loadRecentTransactions method",function(){
        it("should be defined",function(){
            expect(tbl.loadRecentTransactions).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.loadRecentTransactions === 'function').toBe(true);
        });

        // TODO: figure out how to test if it works...
    });

    describe("updateLayoutObject method",function(){
        it("should be defined",function(){
            expect(tbl.updateLayoutObject).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.updateLayoutObject === 'function').toBe(true);
        });

        // TODO: figure out how to test if it works...
    });

    describe("sendOrder method",function(){
        it("should be defined",function(){
            expect(tbl.sendOrder).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.sendOrder === 'function').toBe(true);
        });

        // TODO: figure out how to test if it works...
    });

    describe("merge method",function(){
        it("should be defined",function(){
            expect(tbl.merge).toBeDefined();
        });
        it("should be a function",function(){
            expect(typeof tbl.merge === 'function').toBe(true);
            tbl.close();
        });
        // TODO: figure out how to test if it works...
    });
    // end requirements tests

    // integration tests

    // end integration tests

    // regression tests

    // end regression tests
});

describe("The FbTableCollection",function(){
    it("should be defined",function(){
        expect(FbTableCollection).toBeDefined();
    })
});