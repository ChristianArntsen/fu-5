var FbReceipt = Backbone.Model.extend({
	
	idAttribute: "receipt_id",
	
	defaults: {
		"receipt_id": null,
		"items": [],
		"taxes": [],
		"date_paid": null,
		"status": "pending",
		"total": 0.00,
		"auto_gratuity": null,
		"auto_gratuity_amount": 0,
		"auto_gratuity_type": 'percentage',
		"subtotal": 0.00,
		"tax": 0.00,
		"amount_due": 0.00,
		"taxable": 1,
        "hide_taxable": 0,
		"customer": false,
		"service_fee": false,
		"customer_note": null
	},

	set: function(attributes, options){
		if(!attributes.receipt_id && !this.get('receipt_id')){
			attributes.receipt_id = this.collection.getNextNumber();
			attributes.id = attributes.receipt_id;
		}
		Backbone.Model.prototype.set.call(this, attributes, options);
	},

	unstick: function(){
		// if you split between three receipts, pay one, and delete one, the table "sticks".
		// the paid receipt gets the deleted share back, but it already is marked completed
		// so no more editing allowed...
		// this little method can be called to "unstick" the table
		if((!this.isPaid() || this.getTotalDue() < 0) && this.get('status')==='complete') {
			this.set('status', 'pending');
			this.save();
		}
	},

	initialize: function(attrs, options){
		var self = this;
		var receipt_id = this.get('receipt_id');
		if(this.collection && this.collection.table){
			this.table = this.collection.table;
		}else if(typeof options !== 'undefined' && typeof options.table !== 'undefined'){
			this.table = options.table;
		}else{
			this.table = App.data.food_bev_table;
		}
		
		// Initialize item collection
		var itemCollection = new FbReceiptCart(this.get('items'),{table:this.table});
		itemCollection.url = this.url() + '/items/';
		itemCollection.receipt = this;
		
		// Initialize payment collection
		var paymentCollection = new FbPaymentCollection(this.get('payments'));
		paymentCollection.url = this.url() + '/payments/';
		paymentCollection.receipt = this;
		if(this.get('customer')){
			this.set('customer', new Customer(this.get('customer')));

			this.get('customer').url = CUSTOMERS_URL + '/' + this.get('customer').id;
			this.get('customer').fetch({success:function(){
				self.setCustomer(self.get('customer'));
			}});
		}

		var serviceFee = false;
		if(this.get('service_fee')){
			serviceFee = new FbReceiptItem(this.get('service_fee'));
			this.set('service_fee_active',1);
		}

		var autoGratuity = 0;
		// TODO: Replace calls to global App.*
		if(this.get('auto_gratuity') === null &&
			App.data.food_bev_table.get('guest_count') >= App.data.course.get('auto_gratuity_threshold')){
			autoGratuity = App.data.course.get('auto_gratuity');
		}else if(this.get('auto_gratuity') != null){
			autoGratuity = this.get('auto_gratuity');
		}

		this.set({
			"service_fee": serviceFee,
			"items": itemCollection,
			"payments": paymentCollection,
			"auto_gratuity": autoGratuity,
			// TODO: Replace calls to global App.*
            "hide_taxable": parseInt(App.data.course.get('hide_taxable'))
		});
		this.listenTo(this.get('payments'), 'add remove reset', this.markPaid, this);
		this.listenTo(this.get('items'), 'add remove change:total change:sides_total change:comp change:comp_total', this.calculateTotals, this);
		this.on('change:taxable change:auto_gratuity change:auto_gratuity_type', this.calculateTotals);
		this.on('change:customer', this.applyCustomerDiscount);
		this.on('change:customer', this.render);
		this.save();
	},

	toCollections: function(){
		if(this.get('payments') && !this.get('payments').getTotal) {
			var paymentCollection = new FbPaymentCollection(this.get('payments'));
			paymentCollection.url = this.url() + '/payments/';
			self.model.set('payments', paymentCollection);
			this.listenTo(this.get('payments'), 'add remove reset', this.markPaid, this);
		}
		if(this.get('items') && !this.get('items').url) {
			var itemCollection = new FbReceiptCart(this.get('items'), {table: this.table});
			itemCollection.url = this.url() + '/items/';
			itemCollection.receipt = this;
			self.model.set('items', itemCollection);
			this.listenTo(this.get('items'), 'add remove change:total change:sides_total change:comp change:comp_total', this.calculateTotals, this);
		}
	},

	setCustomer: function(customer) {
		var receipt_data = {};
		receipt_data.customer = customer;
		this.table.get('customers').add(customer);

		if(customer.get('taxable') == '1'){
			receipt_data.taxable = true;
		}else{
			receipt_data.taxable = false;
		}

		this.set(receipt_data);
		this.save();
	},

	removeCustomer: function(){
		var id = null;
		if(this.get('customer')) {
			var customer_id = this.get('customer').id;
			var table = this.table;
			var customers = table.get('customers');

			_.forEach(customers.models,function(customer){
				if(typeof customer === 'undefined')return;
				if(customer.id === customer_id){
					customers.remove(customer);
				}
			});

			var discount = this.get('customer').get('discount');
			var groups = this.get('customer').get('groups');
			var url = this.table.generateUrl();
			this.get('customer').url = url + 'customers/' + customer_id;
			this.get('customer').destroy();
			this.set('customer',false);
			
			if((discount || (groups && groups.length > 0)) && this.get('items')){
				_.forEach(this.get('items').models,function(item){
					item.cart_item.set('discount',0);
				});
			}
			this.save();
		}
		return false;
	},

	clearSelectedItems: function(){
		if(this.get('items').length == 0){
			return false;
		}

		_.each(this.get('items').models, function(item){
			item.set('selected', false);
		});
	},
	
	hasCreditCardPayments: function(){
		if(this.get('payments').length == 0){
			return false;
		}
		
		var has_cc = false;
		_.each(this.get('payments').models, function(payment){
			if(payment.isCreditCard()){
				has_cc = true;
			}
		});
		
		return has_cc;		
	},

	getTotalPayments: function(){
		return this.get('payments').getTotal();
	},
	
	hasPayments: function(){
		if(this.get('payments').length == 0){
			return false;
		}

		return true;
	},
	
	serverTotalPayments: function(callback,error){
		$.ajax(
			this.url()+'/payments_total',
			{
				success:function(res){
					callback(res);
				},
				error:function(res){
					if(typeof error === 'function')
					    error(res);
				}
			}
		);
	},
	
	cancelAllPayments: function(){
		var payments = this.get('payments');
		_.each(payments.models, function(payment){
            payment.url = payments.url + payment.get('type');
			payment.destroy({wait:true});
		});
		this.markUnpaid();
		return false;
	},

	applyCustomerDiscount: function(model){
		
		var customer = this.get('customer');
		var items = this.get('items');
		// TODO: Replace calls to global App.*
		var cart = App.data.food_bev_table.get('cart');
		
		if(!customer){
			return false;
		}
		var customer_discount = new Decimal(customer.get('discount'));
		var customer_groups = customer.get('groups');
		
		if(!items || items.length == 0 || (customer_discount.isZero() && customer_groups.length == 0)){
			return false;
		}
		
		// Loop through items currently on receipt and apply discount
		_.each(items.models, function(receipt_item){
			
			var line = receipt_item.get('line');
			var cart_item = cart.get(line);
			
			// If customer has a discount, use it
			if(customer.get('discount') > 0){
				cart_item.set_discount(customer.get('discount'));
			
			// If customer does not have a discount, but belongs to some groups that might,
			// attempt to apply the group discount
			}else if(customer_groups && customer_groups.length > 0){
				cart_item.set_cost_plus_percent_discount(customer.get('groups'));
			}

			// items deadlock when adding customer to receipt
			// adding transaction to db calls to prevent that from happening
			cart_item.save()
		});
	},
	
	calculateTotals: function(a,b,c){
		if(!this.collection)return; // no collections means too new or too deleted for totals
		var splitItems = this.collection.getSplitItems();
		var receipt = this;

		// Init receipt variables
		var taxable = this.get('taxable');
		var sideTypes = ['sides','soups','salads'];
		var service_fee = false;
		
		var data = {};
		data.total = new Decimal(0.00);
		data.subtotal = new Decimal(0.00);
		data.tax = new Decimal(0.00);
		data.comp_total = new Decimal(0.00);
		data.total_paid = new Decimal(0.00);
		data.total_due = new Decimal(0.00);
		this.set('taxes', {});	

		// Add up all the items in receipt
		_.each(this.get('items').models, function(item){
			
			var line = item.get('line');
			var divisor = splitItems[line];

			item.calculatePrice(divisor, receipt.get('taxable'));
			
			if((item.get('taxable') == 1 && item.get('force_tax')===null) || item.get('force_tax')===1){
				receipt.mergeTaxes(item.get('taxes'));
			}

			_.each(sideTypes, function(sideType){
				if(item.get(sideType) && item.get(sideType).length > 0){
					_.each(item.get(sideType).models, function(side){
						if(side.get('taxable') == 0){
							return false;
						}
						receipt.mergeTaxes(side.get('taxes'));
					});
				}
			});

			_.each(item.get('service_fees').models, function(serviceFee){
				if(serviceFee.get('taxable') == 0){
					return false;
				}
				var serviceFeeTaxes =  _.clone(serviceFee.get('taxes'));
				receipt.mergeTaxes(serviceFeeTaxes);
			});
			
			data.total = data.total
				.plus(item.get('split_total'))
				.plus(item.get('split_service_fees_total'))
				.plus(item.get('split_sides_total')).toDP(2);
			data.subtotal = data.subtotal
				.plus(item.get('split_subtotal'))
				.plus(item.get('split_service_fees_subtotal'))
				.plus(item.get('split_sides_subtotal')).toDP(2);
			data.tax = data.tax
				.plus(item.get('split_tax'))
				.plus(item.get('split_service_fees_tax'))
				.plus(item.get('split_sides_tax')).toDP(2);
			data.comp_total = data.comp_total
				.plus(item.get('split_comp_total')).toDP(2)

		});

		// TODO: Replace calls to global App.*
		// Warning: Don't confuse the auto gratuity service fee with the new service fees that can now be attached to items
		if(App.data.course.get('service_fee_active') == 1 && (typeof this.noRecurse === 'undefined' || this.noRecurse === false)){
			this.noRecurse = true;
			var fee = this.getServiceFee(this.get('auto_gratuity'), this.get('auto_gratuity_type'), data.subtotal.toNumber());

			this.noRecurse = false;
			if(fee){
				this.set('service_fee', fee);
				if(typeof data.total.plus !== 'function' && !isNaN(data.total))
					data.total = new Decimal(data.total);
				data.total = data.total.plus(fee.get('total'));
				if(typeof data.tax.plus !== 'function' && !isNaN(data.tax))
					data.tax = new Decimal(data.tax);
				data.tax = data.tax.plus(fee.get('tax'));
				receipt.mergeTaxes(fee.get('taxes'));
			}

		}else{
			data.auto_gratuity_amount = this.getAutoGratuityAmount(this.get('auto_gratuity'), this.get('auto_gratuity_type'), data.subtotal.toNumber());
			data.total = data.subtotal.plus(data.auto_gratuity_amount).plus(data.tax);
		}

		if(typeof data.subtotal.plus !== 'function' && !isNaN(data.subtotal))
			data.subtotal = new Decimal(data.subtotal);
		
		data.subtotal = data.subtotal.toDP(2).toNumber();
		data.tax = data.tax.toDP(2).toNumber();
		
		if(typeof data.comp_total.plus !== 'function' && !isNaN(data.comp_total))
			data.comp_total = new Decimal(data.comp_total);
		
		data.comp_total = data.comp_total.toDP(2).toNumber();
		
		if(typeof data.total_paid.plus !== 'function' && !isNaN(data.total_paid))
			data.total_paid = new Decimal(data.total_paid);
		
		if(this.get('payments') && !this.get('payments').getTotal){
			this.toCollections();
		}
		
		data.total_paid = data.total_paid.plus(this.get('payments').getTotal());
		
		if(typeof data.total_due.plus !== 'function' && !isNaN(data.total_due))
			data.total_due = new Decimal(data.total_due);
		
		data.total_due = data.total.minus(data.total_paid).toDP(2).toNumber();
		data.total = data.total.toDP(2).toNumber();
		data.total_paid = data.total_paid.toDP(2).toNumber();

		// Update the receipt with totals
		this.set(data);
		return data;
	},

	getAutoGratuityAmount: function(gratuity_amount, type, total){
		if(type == 'dollar'){
			return gratuity_amount;
		}
		var percent = new Decimal(gratuity_amount).dividedBy(100);
		var amount = percent.times(total).toDP(2);
		return amount.toNumber();
	},

	getServiceFee: function(gratuity_amount, type, receipt_subtotal){

		if(!receipt_subtotal || receipt_subtotal == 0 || receipt_subtotal == ''){
			receipt_subtotal = 0;
		}
		if(!gratuity_amount || gratuity_amount == 0 || gratuity_amount == '' || receipt_subtotal == 0){
			return false;
		}

		if(type == 'percentage'){
			var percent = new Decimal(gratuity_amount).dividedBy(100).toDP(2);
			var subtotal = new Decimal(receipt_subtotal);
			var fee_amount = subtotal.times(percent).toDP(2).toNumber();			
		}else{
			var fee_amount = gratuity_amount;
		}

		// TODO: Replace calls to global App.*
		var fee_item = App.data.course.get_special_item('service_fee');

		fee_item.unit_price = fee_item.item_unit_price = fee_item.price = fee_amount;
		fee_item.discount = fee_item.discount_amount = fee_item.discount_percent = 0;
		fee_item.quantity = 1;
		fee_item.unit_price_includes_tax = 0;

		if(this.get('taxable') == 0){
			fee_item.tax = 0;
			fee_item.taxes = [];
		}
		fee_item = new FbCartItem(fee_item);
		return fee_item;
	},
	
	// Merges an item's taxes with the total receipt taxes (and amounts)
	mergeTaxes: function(itemTaxes){

		if(!itemTaxes || itemTaxes.length == 0){
			return false;
		}
		var receiptTaxes = this.get('taxes');

		_.each(itemTaxes, function(tax){

			var key = accounting.formatNumber(tax.percent, 2) + '' + tax.name;
			if(!tax.percent){
				return false;
			}
			var percent = new Decimal(tax.percent);
			
			if(percent.isZero() || percent.isNaN()){
				return false;
			}

			if(receiptTaxes[key]){
				if(!receiptTaxes[key].amount){
					receiptTaxes[key].amount = 0;
				}				
				var running_total = new Decimal(receiptTaxes[key].amount);
				receiptTaxes[key].amount = running_total.plus(tax.amount).toDP(2).toNumber();
			}else{
				receiptTaxes[key] = _.clone(tax);
				if(!receiptTaxes[key].amount){
					receiptTaxes[key].amount = 0;
				}
			}
		});

		this.set('taxes', receiptTaxes);
		return true;
	},

	getTotalDue: function(){
		// Figure out how much is due on the receipt
		this.calculateTotals();
		var totalPaid = this.get('payments').getTotal();
		totalDue = new Decimal(this.get('total')).minus(totalPaid);
		
		return totalDue.toDP(2).toNumber();
	},

	isPaid: function(){
		var totalDue = new Decimal(this.getTotalDue());
		var totalPayments = new Decimal(this.get('payments').getTotal());

		if(totalDue.greaterThan(0) || totalDue.lessThan(0)){
			return false;
		}

        var allPreAuth = this.get('payments').allPreAuth();
        var totalPayments = new Decimal(this.get('payments').getTotal());

        if(allPreAuth || this.get('payments').length===0 || (totalPayments.isZero() && this.get('items').length === 0)){
            return false;
        }

		return true;
	},

	markUnpaid: function(){

        this.set('status','pending');
        // Mark each item in cart as unpaid
        var isPaid = 0;
        _.each(this.get('items').models, function(item){
            item.cart_item.set({'is_paid': 0});
        });
	},

	markPaid: function(){
        var isPaid = 1;
		var totalPayments = new Decimal(this.get('payments').getTotal());
		var allPreAuth = this.get('payments').allPreAuth();

		if(allPreAuth || this.get('payments').length===0 || (totalPayments.isZero() && this.get('items').length === 0)){
			var isPaid = 0;
		}
		// TODO: Replace calls to global App.*
		var cart = App.data.food_bev_table.get('cart');

		// Mark each item in cart as paid or unpaid
		_.each(this.get('items').models, function(item){
			var line = item.get('line');
			cart.get(line).set({'is_paid': isPaid});
		});

		// Mark receipt as paid (if entire receipt has been paid)
		if(this.isPaid()){
			
			this.clearSelectedItems();

			// TODO: Replace calls to global App.*
			if(App.data.course.get_setting('cash_register') == 1){
				if(this.get('status')==='pending') {
					var receipt_printer = new Receipt();
					receipt_printer.open_cash_drawer();
				}
			}		
			
			this.set({'status':'complete'});
		}
	},

	// printReceipt would be nice to move elsewhere
	// call a print function, and feed this object into it?
	printReceipt: function(type,callback){
		if ($('div#jquery_keypad').is(':visible')){
			alert('Please save gratuity in order to proceed.');
			return false;
		}		

		var receipt = this;

		this.get('payments').fetch({success:function(){
			receipt.set({
				// TODO: Replace calls to global App.*
				'sale_id': App.data.food_bev_table.get('sale_id'),
				//'sale_number': App.data.food_bev_table.get('sale_number'),
				'loyalty': App.data.food_bev_table.get('loyalty')
			}, {'silent': true});

				receipt.calculateTotals();
			var data = _.clone(receipt.attributes);
			data.employee = new Employee(App.data.user.attributes);

			// Email receipt (if customer is attached)
			var receipt_emailed = false;
			if(App.data.course.get_setting('auto_email_receipt') == 1){
				var sale = new Sale(receipt.toJSON());
				receipt_emailed = sale.email_receipt_to_customer();
			}

			// Subtract any account payments from customer object,
			// so their balance is accurate if printed on the receipt
			if(data.customer && data.payments && data.payments.length > 0){
				var member_balance = data.customer.get('member_account_balance');
				var customer_balance = data.customer.get('account_balance');

				_.each(data.payments.models, function(payment){

					if(payment.get('type').indexOf(App.data.course.get('member_balance_nickname')) >= 0){
						member_balance -= payment.get('amount');
					}
					if(payment.get('type').indexOf(App.data.course.get('customer_credit_nickname')) >= 0){
						customer_balance -= payment.get('amount');
					}
				});

				data.customer.set({
					'member_account_balance': member_balance,
					'account_balance': customer_balance
				});
			}

			console.debug(data); //JBDEBUG

			var print_receipt = new FBSalePrintReceipt(data);
			print_receipt.auto_gratuity_included = true;

			if(type && type == 'sale'){
				if(
					(
						App.data.course.get_setting('auto_email_no_print') == 0 ||
						(App.data.course.get_setting('auto_email_no_print') == 1 && !receipt_emailed)
					) &&
					(
						(!receipt.hasCreditCardPayments() && App.data.course.get_setting('non_credit_card_receipt_count') > 0) ||
						(receipt.hasCreditCardPayments() && App.data.course.get_setting('credit_card_receipt_count') > 0)
					)
				){
					print_receipt.print_network(type);
				}

			}else{
				print_receipt.print_network(type);
			}
            if(typeof callback === 'function')callback();
        }});
		return true;
	},
	
	compAllItems: function(type, reason, amount, employee_id){
		var items = this.get('items');
		var receipt = this;
		
		if(items.length == 0){
			return false;
		}

		// TODO: Replace calls to global App.*
		$.post(App.data.food_bev_table.url + 'receipts/' + receipt.get('receipt_id') + '/comps', {
			'comp': {
				'type':type, 
				'description':reason, 
				'amount':amount, 
				'employee_id':employee_id
			}
		}, function(response){

		},'json');
		
		_.each(items.models, function(item){
			var line = item.get('line');
			var cart_item = App.data.food_bev_table.get('cart').get(line);
			if(item.getNumberSplits()>0 && item.splitPaid()===true){
				App.vent.trigger('notification', {'type':'error', 'msg':'Cannot comp a partially paid split item.'});
			}else
			    cart_item.set({'comp': {'amount': amount, 'type': type, 'description': reason, 'employee_id': App.data.food_bev_table.get('admin_auth_id')}}, {'validate': true});
		});
		return true;
	},
	
	clearComps: function(){
		var items = this.get('items');
		var receipt = this;
		
		if(items.length == 0){
			return false;
		}

		// TODO: Replace calls to global App.*
		$.post(App.data.food_bev_table.url + 'receipts/' + receipt.get('receipt_id') + '/comps', {'comp': false}, function(response){
			
		},'json');		
		
		_.each(items.models, function(item){
			var line = item.get('line');
			var cart_item = App.data.food_bev_table.get('cart').get(line);
			
			cart_item.set({'comp': false});
		});
		
		return true;			
	}
});

var FbReceiptCart = FbCart.extend({
	model: function(attrs, options){
		return new FbReceiptItem(attrs, options);
	},
	initialize:function(attrs,options){

		if(typeof options.table !== 'undefined'){
			this.table = options.table;
		}
	}
});

var FbReceiptCollection = Backbone.Collection.extend({

	model: FbReceipt,
	
	initialize: function(attrs,options){

		if(typeof options.table !== 'undefined'){
			this.table = options.table;
		}
	},

	render:function() {
		_.each(this.models, function(receipt){receipt.render();});
	},

	// Get all items selected within receipts
	getSelectedItems: function(){
		var selectedItems = [];

		// Loop through each receipt
		_.each(this.models, function(receipt){
			var items = receipt.get('items');
			if(items){
				selectedItems = selectedItems.concat(
					items.where({"selected":true})
				);
			}
		});

		return selectedItems;
	},

	unSelectItems: function(){
		// Loop through each receipt
		_.each(this.models, function(receipt){
			var items = receipt.get('items');
			if(items){
				var selectedItems = items.where({"selected":true});
				_.each(selectedItems, function(item){
					item.unset("selected");
				});
			}
		});

		return this;
	},

	getNextNumber: function(){
		var receiptNum = 0;
		_.each(this.models, function(receipt){
			var curNum = parseInt(receipt.get('receipt_id'));

			if(curNum && curNum > receiptNum){
				receiptNum = curNum;
			}
		});

		return receiptNum + 1;
	},

	getSplitItems: function(){
		var itemSplits = {};
		if(this.length == 0){
			return itemSplits;
		}

		// Loop through each receipt
		_.each(this.models, function(receipt){

			// Loop through each item in receipt
			var items = receipt.get('items');
			_.each(items.models, function(item){
				var line = item.get('line');

				// Store counts of split items in array
				if(itemSplits[line]){
					itemSplits[line]++;
				}else{
					itemSplits[line] = 1;
				}
			});
		});

		return itemSplits;
	},
	
	getCompTotals: function(line){
		var itemComps = {};
		if(this.length == 0){
			return itemComps;
		}

		// Loop through each receipt
		_.each(this.models, function(receipt){
			// Loop through each item in receipt
			if(line){
				var items = receipt.get('items').get(line);
			}else{
				var items = receipt.get('items');
			}
			
			_.each(items.models, function(item){
				
				var itemLine = item.get('line');
				var comp_total = new Decimal(item.get('comp_total'));
				if(comp_total.greaterThan(0)){
					
					if(!itemComps[itemLine]){
						itemComps[itemLine] = 0;
					}	
					var runningTotal = new Decimal(itemComps[itemLine]);
					itemComps[itemLine] = runningTotal.plus(item.get('comp_total')).toDP(2);
				}
			});
		});
		
		if(line){
			return new Decimal(itemComps[line]).toDP(2).toNumber();
		}
		
		return itemComps;
	},

	allReceiptsZeroed: function(){
		var complete = true;
		_.each(this.models, function(receipt){
			if(
			  receipt.getTotalDue() != 0
			){

				complete = false;
			}
		});
		return complete;
	},

	allReceiptsPaid: function(){
		var complete = true;
		_.each(this.models, function(receipt){
			if(!complete)return;
			if(
			    !(
			    	// we let them close on an empty receipt
				    receipt.get('payments').length == 0 &&
				    receipt.get('items').length == 0
				) &&
			    (
			    	// we don't let them close on unpaid receipts with items
					!receipt.isPaid() ||
					(receipt.get('payments').length == 0 && receipt.get('items').length > 0)
			    )
			){

				complete = false;
			}
		});
		return complete;
	},

	paymentsMade: function(){
		var payments = false;
		_.each(this.models, function(receipt){
			if(receipt.get('payments').length > 0){
				payments = true;
			}
		});

		return payments;
	},
	
	calculateAllTotals: function(){
		_.each(this.models, function(receipt){
			receipt.calculateTotals();
		});
	}
});
