var FbPayment = Backbone.Model.extend({
	
	idAttribute: "type",
	defaults: {
		"type": "",
		"amount": 0.00,
		"date": "",
		"card_number": "",
		"tran_type": null,
		"credit_card_invoice_id":null,
		"auth_code":null,
		"credit_card_id":null
	},

	url: function () {
		// for some reason, backbone cannot url encode the parens by itself
		return this.collection.url+this.get('type').replace('(','%28').replace(')','%29').replace(/\*/g,'x')
	},

	initialize: function(attrs, options){
		if(!attrs.type && attrs.payment_type)attrs.type=attrs.payment_type;
		this.set('type', attrs.type.replace('/','-').replace(/\*/g,'x'));
		this.listenTo(this, 'invalid', this.error, this);
	},

	error: function(){
		this.collection.trigger('invalid', this);
		this.destroy();
	},

	parse: function(response, options){
		if(this.collection.get(response.type)){
			this.collection.get(response.type).set(response);
		}

		if(response.type){
			response.type = response.type.replace('/','-').replace(/\*/g,'x');
		}

		if(response.sale_id){
			
			var sale_data = {
				'sale_id': response.sale_id,
				'sale_number': response.number,
				'loyalty': false
			};
			if(response.loyalty){
				sale_data.loyalty = response.loyalty;
			}

			// TODO: Replace calls to global App.*
			App.data.food_bev_table.set(sale_data);
		}

		if(response.payment_amount && !response.amount)response.amount = response.payment_amount;

		// If any messages were passed with response, display them
		if(response.msg){
			App.vent.trigger('notification', {msg: response.msg, type: 'warning'});
		}
		return response;
	},

	validate: function(attributes, options){
		if(typeof(attributes.amount) == 'string'){			
			var amount = parseFloat(accounting.unformat(attributes.amount));
		}else{
			var amount = attributes.amount;
		}

		if(amount < 0){
			return "Payment can not be negative";
		}
	},
	
	isCreditCard: function(){
		
		if(this.has('type') && (
			this.get('type').indexOf('Credit Card') > -1 ||
			this.get('type').indexOf('DCVR') > -1 ||
			this.get('type').indexOf('M/C') > -1 ||
			this.get('type').indexOf('M-C') > -1 ||
			this.get('type').indexOf('AMEX') > -1 ||
			this.get('type').indexOf('JCB') > -1 ||
			this.get('type').indexOf('DINERS') > -1 ||
			this.get('type').indexOf('VISA') > -1
		)){
			return true;
		}else{
			return false;
		}
	}
});

var FbPaymentCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		return new FbPayment(attrs, options);
	},

	getTotal: function(){
		var total = new Decimal(0);
		_.each(this.models, function(payment){
			total = payment.get('tran_type') === 'PreAuth' ? total : total.plus(payment.get('amount'));
		});

		return total.toDP(2).toNumber();
	},

	allPreAuth: function(){
		var ret = 1;
		if(!this.models.length)return null;
		_.each(this.models, function(payment){
			if(payment.get('tran_type')!=='PreAuth'){
				ret = 0;
			}
		});
		return ret;
	},

	hasTab: function () {
		var ret = false;
		_.each(this.models,function(payment){
			if(payment.get('tran_type') === 'PreAuth')ret = true;
		});
		if(ret)this.openedTab = true;
		else this.openedTab = false;
		return ret;
	},

	addPayment: function(payment, success_callback){
		var collection = this;
		if(payment.customers)
		payment.customers.cardNumber = payment.customers.cardNumber.replace(/\*/g,'x');
		
		// If a signature is required for member purchases, show the signature window instead of submitting payment;
		// TODO: Replace calls to global App.*
		if(App.data.course.get_setting('require_signature_member_payments') == 1 &&
			payment.type == 'member_balance' && (!payment.signature)){

			var signature_window = new CustomerSignatureView({model: new Backbone.Model(payment)});
			signature_window.show();
			
			// When the signature is finished, apply it to the payment and add the payment again
			this.listenTo(signature_window, 'signature', function(e){
				payment.signature = e.data;
				collection.addPayment(payment, success_callback);
			});

			return false;
		}
		if(payment && payment.params && payment.params.credit_card_id){
			payment.credit_card_id = payment.params.credit_card_id;
		}

		this.create(payment, {'wait': true, 'success': function(model, response){
			$.loadMask.hide();

			var sale_id = response.number;
			if(collection.receipt){
				collection.receipt.set('sale_number',sale_id);
			}

			model.url = collection.url + model.get('type');
			if(typeof(success_callback) == 'function'){
				success_callback(model, response);
			}
			collection.trigger('sync', model, response);
		}, 'error': function(model, response, options){
			$.loadMask.hide();
			if(response.status != 409){
				// TODO: Replace calls to global App.*
				App.vent.trigger('notification', {'type':'error', 'msg':response.responseJSON.msg});
			}
			collection.trigger('error', model, response, options);
		}});		
	}
});
