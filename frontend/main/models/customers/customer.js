var Customer = Backbone.Model.extend({

	urlRoot: CUSTOMERS_URL,
	idAttribute: "person_id",
	defaults: {
		"person_id": null,
		"first_name": "",
		"last_name": "",
		"phone_number": "",
		"cell_phone_number": "",
		"email": "",
		"birthday": "",
		'date_created': "",
		"address_1": "",
		"address_2": "",
		"city": "",
		"state": "",
		"zip": "",
		"country": "",
		"comments": "",
		"status_flag": 0,
		"account_number": "",
		"price_class": 0,
		"discount": 0,
		"taxable": 1,
		"require_food_minimum": 0,
		"marketing_customer_info": {},
		"member": 1,
		"member_account_limit": 0,
		"account_limit": 0,
		"member_account_balance": 0,
		"new_member_balance": 0,
		"member_account_balance_allow_negative": 0,
		"member_balance_change_reason": "",
		"member_account_option": "set",
		"account_balance_change_reason":"",
		"account_balance_option":"set",
		"account_balance": 0,
		"new_account_balance": 0,
		"account_balance_allow_negative": 0,
		"invoice_balance": 0,
        "account_transactions": {},
		"invoice_email": "",
		"use_loyalty":0,
		"loyalty_points": 0,
		"photo": BASE_URL +'images/profiles/Customer_photo_icon.png',
		"image_id": 0,
		"groups": [],
		"passes": [],
		"recent_transactions": [],
		"rainchecks": [],
		"gift_cards": [],
		"minimum_charges": [],
		"household_members": [],
		"household_id":"",
		"username": "",
		"opt_out_email": 0,
		"opt_out_text": 0,
		"unsubscribe_all": 0,
		"texting_status":false,
        "recent_campaigns": [],
        "last_open_campaign": [],
        "handicap_account_number":"",
        "handicap_score":"",
	},
	// Over ride backbone save method to only send certain fields to be
	// saved to server (instead of entire object)
	save: function (attrs, options) {
		attrs = attrs || this.toJSON();
		options = options || {};

		// Filter attributes that will be saved to server
		attrs = _.pick(attrs, [			
			'first_name',
			'last_name',
			'email',
			'invoice_email',
			'cell_phone_number',
			'phone_number',
			'birthday',
			'address_1',
			'address_2',
			'city',
			'date_created',
			'state',
			'zip',
			'status',
			'comments',
			'status_flag',
			'account_number',
			'price_class',
			'discount',
			'taxable',
			'require_food_minimum',
			'member',
			'account_balance',
			'account_limit',
			'account_balance_allow_negative',
			'member_account_balance',
			'member_account_limit',
			'member_account_balance_allow_negative',
			'invoice_balance',
			'use_loyalty',
			'loyalty_points',
			'username',
			'password',
			'confirm_password',
			'opt_out_email',
			'opt_out_text',
			'image_id',
			'photo',
			'passes',
            'handicap_account_number',
            'handicap_score',
            "member_balance_change_reason",
            "member_account_option",
            "account_balance_change_reason",
            "account_balance_option"
		]);

		// Call super with attrs moved to options
		Backbone.Model.prototype.save.call(this, attrs, options);
	},

	sync: function(method, model, options){

        this.messages = new PersonalMessageCollection();
		if(!options.attrs && method !== 'read'){
			options.attrs = this.attributes;
		}

		if(options.attrs){
			var attributes = _.clone(options.attrs);
			var passes = [];

			if(attributes.passes){
				attributes.passes = attributes.passes.toJSON();

				// Filter out item data that does not need sent to the server when the cart is saved
				_.each(attributes.passes, function(pass){
					passes.push(_.pick(pass, ['pass_id']));
				});
				attributes.passes = passes;
			}
			delete options.attrs;

			options.contentType = 'application/json';
      		options.data = JSON.stringify(attributes);			
		}

		this.messages.fetch({
			data: $.param({ person: this.get("person_id")})
		});

		Backbone.Model.prototype.sync.call(this, method, model, options);
	},

	toMinimumJSON: function(){
		return _.pick(this.toJSON(), [
			'person_id', 'course_id', 'first_name', 'account_number',
			'last_name', 'loyalty_points', 'customer_account', 
			'member_account', 'invoice_account', 'email', 'phone_number',
			'cell_phone_number', 'discount', 'taxable', 'course_id',
			'taxable'
		]);		
	},

	// Save a color to the customer, used for customer initials 
	// next to green fees/cart fees in POS cart
	get_color: function(customer_id){
		if(!this.get('color')){
			this.set('color', Please.make_color(), {'silent': true});
		}
		return this.get('color');
	},

	parse: function(res){
		if(res.push && res.length===1)return res[0];
		return res;
	},

	set: function(attributes, options){
		if(attributes.groups !== undefined && !(attributes.groups instanceof GroupCollection)){
			attributes.groups = new GroupCollection( attributes.groups );
		}
		if(attributes.passes !== undefined && !(attributes.passes instanceof PassCollection)){
			attributes.passes = new PassCollection( attributes.passes );
		}
		if(attributes.recent_transactions !== undefined && !(attributes.recent_transactions instanceof SaleCollection)){
			attributes.recent_transactions = new SaleCollection( attributes.recent_transactions );
		}
		if(attributes.gift_cards !== undefined && !(attributes.gift_cards instanceof Gift_card_collection)){
			attributes.gift_cards = new Gift_card_collection( attributes.gift_cards );
		}
        if(attributes.rainchecks !== undefined && !(attributes.rainchecks instanceof Raincheck_collection)){
            attributes.rainchecks = new Raincheck_collection( attributes.rainchecks );
        }
        if(attributes.recent_campaigns !== undefined && !(attributes.recent_campaigns instanceof Campaign_collection)){
            attributes.recent_campaigns = new Campaign_collection( attributes.recent_campaigns );
        }
		if(attributes.household_members !== undefined && !(attributes.household_members instanceof HouseholdMemberCollection)){
			attributes.household_members = new HouseholdMemberCollection( attributes.household_members );
		}
		if(attributes.minimum_charges !== undefined && !(attributes.minimum_charges instanceof MinimumChargeCollection)){
			attributes.minimum_charges = new MinimumChargeCollection( attributes.minimum_charges );
		}

		return Backbone.Model.prototype.set.call(this, attributes, options);
	},
	
	hasMemberBalance: function(){
		return this.get("member_account_balance") > 0 || this.get("member_account_balance_allow_negative") == '1';
	},
	
	hasCustomerBalance: function(){
		return this.get("account_balance") > 0 || this.get("account_balance_allow_negative") == '1';
	},

	sendTextInvite: function(){
		$.ajax({
			type: "POST",
			url: "/index.php/customers/send_text_invite?customer_id="+this.get("person_id")+"&course_id="+this.get("course_id"),
			success: function(response){
				App.vent.trigger('notification', {'msg':'An invite will be sent shortly.'});
			},
			dataType:'json'
		});
	},

	upload_photo: function(data, callback){
		var url = API_URL+'/customers/new/photo';
		if(this.get('person_id')){
			url = API_URL+'/customers/'+this.get('person_id')+'/photo';
		} 
		var model = this;
		data.course_id = this.get('course_id');

		$.post(url, data, function(response){
			if(callback && typeof(callback) == 'function'){
				callback(response);
			}
		});
	}
});

var CustomerCollection = Backbone.Collection.extend({
	url: CUSTOMERS_URL,
	model: Customer
});
