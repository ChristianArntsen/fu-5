// Customer groups
var HouseholdMember = Backbone.Model.extend({
	idAttribute: "person_id",
	defaults: {
		"first_name": "",
		"last_name": ""
	}
});

var HouseholdMemberCollection = Backbone.Collection.extend({
	model: HouseholdMember
});