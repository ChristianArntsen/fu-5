var ItemUPC = Backbone.Model.extend({
	idAttribute: "upc_id",
	defaults: {
		"upc": "",
	}
});

var ItemUPCCollection = Backbone.Collection.extend({
	model: ItemUPC
});