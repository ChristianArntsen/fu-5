var PassItemRule = Backbone.Model.extend({
	
	defaults: {
		"price_class_id": false,
		"items":[],
		"type":"teetimes",
		"name": '',
		"times_used": 0,
		"conditions": [],
		"saved": false
	},

	initialize: function(){
		if(this.collection && !this.get('rule_number')){
			this.set('rule_number', this.collection.getNextNumber());
			this.set('uniq_id',this.collection.generateUniqId());
		}

		this.set('conditions', new PassItemRuleConditions(this.get('conditions')));
		this.set('items',new ItemCollection(this.get("items")));
	}
});

var PassItemRules = Backbone.Collection.extend({
	
	model: PassItemRule,
	comparator: 'rule_number',
	
	getNextNumber: function(){
		
		var num = 1;
		if(this.models && this.models.length > 0){
			var last_num = this.at(this.models.length - 1).get('rule_number');
			num = last_num + 1;
		}

		return num;
	},
	generateUniqId : function(){
		var id = (+ new Date());
		id = id.toString().substr(id.toString().length - 5);
		if(this.findWhere({id:id})){
			return this.generateUniqId();
		}
		return id;
	}
});