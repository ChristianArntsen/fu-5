var Gift_card = Backbone.Model.extend({
	
	idAttribute: "giftcard_id",
	defaults: {
		'expiration_date': false
	},

	isExpired: function(){
		if(!this.hasExpiration()){
			return false;
		}
		var expiration = moment(this.get('expiration_date'));
		
		if(expiration.isBefore()){
			return true;
		}
		return false;
	},

	hasExpiration: function(){
		if(!this.get('expiration_date') || this.get('expiration_date') == '0000-00-00'){
			return false;
		}
		return true;	
	}
});

var Gift_card_collection = Backbone.Collection.extend({
	model: Gift_card
});