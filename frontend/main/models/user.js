var User = Backbone.Model.extend({
	idAttribute: 'person_id',

	defaults: {
		first_name: '',
		last_name: '',
		user_level: 1,
		fnb_logged_in: false,
		timeclock_history: [],
		skipped_register_log: 0,
		sales_stats: 0,
		acl: false
	},

	initialize: function(){
		this.set('timeclock_history', new TimeclockEntryCollection(this.get('timeclock_history')));
		this.set('acl', new ACL({
				person_id: this.get('person_id'),
				permissions: this.get('permissions')
			})
		);
	},

	food_bev_logout: function(){

		var model = this;

		$.ajax({
		   type: "POST",
		   url: API_URL + "/food_and_beverage/authenticate/logout",
		   data: "",
		   success: function(response){
				model.set('fnb_logged_in', false);
				var auth_window = new AuthorizeWindowView({action: 'Login',  closable: false, backdropOpacity: '0.9'});
				$('div.modal').modal('hide');
				auth_window.show();
		   },
		   dataType:'json'
		});

		return true;
	},

	switch_user: function(employee_id, password, callback){
		var model = this;
		$.post(API_URL + '/authentication/login', {'employee_id': employee_id, 'password': password}, function(response){
			
			if(response.success && response.employee){
				model.set(response.employee);
				model.get('acl').set({
					'person_id': response.employee.person_id,
					'permissions': response.employee.permissions,
					'override': false
				});
			}

			if(typeof(callback) == 'function'){
				callback(model, response);
			}
		}).fail(function(response){
			App.vent.trigger('notification', {'type': 'error', 'msg': response.responseJSON.msg});
		});
	},

	has_module_permission: function(module_id){
		if(_.findWhere(this.get('modules'), {'module_id': module_id}) === undefined){

			return false;
		}else{
			return true;
		}
	},

	is_clocked_in: function(){
		if(this.get('timeclock_history').at(0) && this.get('timeclock_history').at(0).get('total') == '0'){
			return true;
		}else{
			return false;
		}
	},

	is_super_admin: function(){
		return parseInt(this.get('user_level')) == 5;
	},

	is_admin: function(){
		return parseInt(this.get('user_level')) == 3;
	},

	is_manager: function(){
		return parseInt(this.get('user_level')) == 2;
	},

	is_employee: function(){
		return parseInt(this.get('user_level')) < 3;
	}
});
