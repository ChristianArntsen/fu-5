//var AprivaPax = {
//    GetOAuthToken: function()
//    {
//        // Create a current UNIX timestamp
//        DateTime date = DateTime.Now;
//        long unixTimestamp = date.Ticks - new DateTime(1970, 1, 1).Ticks;
//        unixTimestamp /= TimeSpan.TicksPerSecond;
//        String timestamp = unixTimestamp.ToString();
//
//        // Build the Authorization Header using Base64 encoding
//        byte[] EncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes("apriva:6c497276173742e4b6581bd1f5d644c9");
//        string base64String = System.Convert.ToBase64String(EncodeAsBytes);
//
//        // Build the POST Request header and parameters
//        HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://aibapp53.aprivaeng.com:9464/o/999/oauth2/token");
//        Request.Method = "POST";
//        Request.ContentType = "application/x-www-form-urlencoded";
//        Request.Headers.Add("Authorization: Basic " + base64String);
//
//        String PostParams = "grant_type=client_credentials&scope=";
//        PostParams += System.Web.HttpUtility.UrlEncode("https://ws.api.apriva.com/auth/user https://ws.api.apriva.com/auth/hosted.payment");
//        PostParams += "&timestamp=" + timestamp + "&access_uri=" + System.Web.HttpUtility.UrlEncode("https://aibapp53.aprivaeng.com:9467");
//
//        // Send the JSON request to the OAuth web service.
//        using (var streamWriter = new StreamWriter(Request.GetRequestStream()))
//        {
//            streamWriter.Write(PostParams);
//        }
//
//        // Receive the JSON Response from the OAuth web service, which contains an OAuth token.
//        var httpResponse = (HttpWebResponse)Request.GetResponse();
//        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
//        {
//            var OAuthToken = streamReader.ReadToEnd();
//        }
//    }
//}