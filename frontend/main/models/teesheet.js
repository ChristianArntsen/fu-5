// Base receipt model with receipt utility functions
var TeesheetStats = Backbone.Model.extend({
    url: function () {
        return API_URL + '/teesheet_stats';
    },
    idAttribute: "tee_sheet_id",
	defaults: {
    	tee_sheet_id:"",
		start:"",
        Bookings:"1",
        Occupancy: "0%",
        Player_No_Shows: "1",
        Players_Checked_in: 0,
        Revenue: "$0.00"
	}
});
