var CartCustomer = Customer.extend({
	
	urlRoot: CART_URL + '/customers',
	initialize: function(attributes, options){
		Customer.prototype.initialize.call(this);
		
		if(this.get('selected')){
			if(this.collection && this.collection.unselect_siblings){
				this.collection.unselect_siblings(this);
			}
		}
	},

	// Over ride backbone save method to only send certain fields to be
	// saved to server (instead of entire object)
	save: function (attrs, options) {
		attrs = attrs || this.toJSON();
		options = options || {};

		// Filter attributes that will be saved to server
		attrs = _.pick(attrs, ['person_id', 'selected', 'taxable', 'discount']);

		options.attrs = attrs;

		// Call super with attrs moved to options
		Backbone.Model.prototype.save.call(this, attrs, options);
	}
});

var CartCustomerCollection = Backbone.Collection.extend({
	
	url: CART_URL + '/customers',
	model: CartCustomer,

	initialize: function(attrs, options){
		
		var self = this;
		this.on('change:selected', this.unselect_siblings);
		this.on('add', this.warn);
		
		if(options && options.cart){
			this.cart = options.cart;
		}else{
			this.cart = App.data.cart;
		}	
	},
	
	// When a customer tab is selected, de-select all other customers
	// attached to cart
	unselect_siblings: function(selected_customer){

		if(this.models.length == 0){
			return false;
		}

		_.each(this.models, function(customer){
			if(customer != selected_customer){
				customer.set({'selected': false}, {'silent':true});
			}
		});
		
		// Change cart taxable status
		App.data.cart.set({'taxable': selected_customer.get('taxable')});
	},
	
	warn: function(customer){
		
		var status = parseInt(customer.get('status_flag'));
		var comments = customer.get('comments');
		
		if(status == 0 || status == 3 || !comments || comments == ''){
			return false;
		}
		
		if(status == 2){
			App.vent.trigger('notification', {msg: comments, type: 'warning', delay: 5000}); 
		}else if(status == 1){
			App.vent.trigger('notification', {msg: comments, type: 'danger', delay: 5000});  
		}

		return true;
	}
});