var Cart = Backbone.Model.extend({

	url: function(){
		if(this.get('cart_id')){
			return CART_URL + '/' + this.get('cart_id');
		}else{
			return CART_URL;
		}
	},
	idAttribute: 'cart_id',

	defaults: {
		'total': 0.00,
		'subtotal': 0.00,
		'tax': 0.00,
		'taxes': [],
		'total_due': 0.00,
		'num_items': new Decimal(0),
		'payments': [],
		'items': [],
		'customers': [],
		'payments': [],
		'sale_payments': [],
		'taxable': true,
		'status': 'active',
		'mode': 'sale',
		'suspended_name': null,
		'teetime_id': null,
		'override_authorization_id': null,
		'employee_pin': null,
		'check_all': true,
		'credit_card_fees': 0,
        'hide_taxable' : 0,
        'customer_note': null,
		"service_fees": [],
		'service_fees_subtotal': 0,
        'teetime': false
	},

	initialize: function(attributes, options){

		this.attributes = this.parseCollections(this.attributes);
		
		if(!this.has('unit_price_includes_tax')){
			this.set('unit_price_includes_tax', (App.data.course.get('unit_price_includes_tax') == 1));
		}
        this.set('hide_taxable', parseInt(App.data.course.get('hide_taxable')));
        this.calculateTotals();
		this.initialize_urls();

		this.on('error', this.checkTimeout);
		this.on('change:mode', this.changeMode);
		this.on('change:cart_id', this.initialize_urls);
		this.on('change:taxable', this.calculateTotals);
		this.listenTo(this.get('customers'),'add remove reset',this.calculateTotals);
	},

	parseCollections: function(data){

        if(data.customers !== undefined && !(data.customers instanceof CartCustomerCollection)){
            data.customers = new CartCustomerCollection(data.customers, {cart: this});
            this.listenTo(data.customers, 'add', this.applyCustomerSettings);
        }
		if(data.items !== undefined && !(data.items instanceof CartItemCollection)){
			data.items = new CartItemCollection(data.items, {cart: this});
			this.listenTo(data.items, 'change:selected change:total change:taxes change:sides_total add remove reset', this.calculateTotals);
		}
		if(data.payments !== undefined && !(data.payments instanceof CartPaymentCollection)){
			data.payments = new CartPaymentCollection(data.payments, {cart: this});
			this.listenTo(data.payments, 'change:amount add remove reset', this.calculateTotals);
		}
		if(data.sale_payments !== undefined && !(data.sale_payments instanceof CartPaymentCollection)){
			data.sale_payments = new CartPaymentCollection(data.sale_payments, {cart: this});
		}
		if(typeof data.num_items == "number"){
			data.num_items = new Decimal(data.num_items);
		}

		return data;
	},

	checkTimeout: function(model, response){
		if(response && response.status == 408){
			this.clear();
			App.data.incomplete_sales.fetch({
				data: {status: 'incomplete'},
				reset: true
			});
			$('div.modal').modal('hide');
		}
	},

	initialize_urls: function(){
		this.url = CART_URL + '/' + this.attributes.cart_id;
		this.attributes.customers.url = CART_URL +'/'+ this.attributes.cart_id +'/customers';
		this.attributes.items.url = CART_URL +'/'+ this.attributes.cart_id +'/items';
		this.attributes.payments.url = CART_URL +'/'+ this.attributes.cart_id +'/payments';
	},
	
	// Over ride backbone save method to only send certain fields to be
	// saved to server (instead of entire object)
	save: function (attrs, options) {
		attrs = attrs || this.toJSON();
		options = options || {};

		// Filter attributes that will be saved to server
		attrs = _.pick(attrs, ['status', 'taxable', 'mode', 'customers',
			'suspended_name', 'teetime_id', 'employee_pin', 'items',
			'payments', 'total', 'subtotal', 'tax', 'num_items', 'customer_note',
			'return_sale_id']);

		options.attrs = attrs;

		// Call super with attrs moved to options
		Backbone.Model.prototype.save.call(this, attrs, options);
	},

	sync: function(method, model, options){
		
		if(options.attrs){
			var attributes = _.clone(options.attrs);
			
			if(attributes.items){
				attributes.items = attributes.items.toJSON();

				// Filter out item data that does not need sent to the server when the cart is saved
				_.each(attributes.items, function(item){
					if(item.receipt_content){
						delete item.receipt_content;
					}
					if(item.fees){
						delete item.fees;
					}
				});
			}
			delete options.attrs;

			options.contentType = 'application/json';
      		options.data = JSON.stringify(attributes);			
		}

		Backbone.Model.prototype.sync.call(this, method, model, options);
	},

	getNextLine: function(){
		var line = 0;
		_.each(this.get('items').models, function(cartItem){
			var curLine = parseInt(cartItem.get('line'));

			if(curLine && curLine > line){
				line = curLine;
			}
		});

		return line + 1;
	},

	groupTaxes: function(taxes){
		
		var grouped = {};

		_.each(taxes, function(tax){
			
			if(!tax){
				return false;
			}

			var percent = new Decimal(tax.percent);
			var key = tax.name + ' ' + percent.toDP(3).toNumber();
			
			if(percent.isZero()){
				return false;
			}
			if(!tax.amount){
				tax.amount = 0;
			}
			
			if(grouped[key]){
				if(!grouped[key].amount){
					grouped[key].amount = 0;
				}
				var running_total = new Decimal(grouped[key].amount);
				grouped[key].amount = running_total.plus(tax.amount).toDP(2).toNumber();
			
			}else{
				grouped[key] = _.clone(tax);
			}
		});

		return _.toArray(grouped);
	},

	calculateTotals: function(){
		var totals = {
			total: new Decimal(0),
			subtotal: new Decimal(0),
			tax: new Decimal(0),
			taxes: [],
			num_items: new Decimal(0),
			service_fees_subtotal: new Decimal(0)
		};
		var cart = this;
		var side_types = ['sides','soups','salads'];

		// Loop through each item and add up the totals
		_.each(this.get('items').models, function(cartItem){

			if(!cartItem.get('selected')){
				return false;
			}
            if(cartItem.get('taxable') !== cart.get('taxable')) {
				cartItem.set('taxable', cart.get('taxable'));
				cartItem.calculatePrice();
			}

			var force_tax = null;
			if(cartItem.get('force_tax')==='0'||cartItem.get('force_tax')===0)force_tax=0;
			else if(cartItem.get('force_tax')==='1'||cartItem.get('force_tax')===1)force_tax=1;

			totals.num_items = totals.num_items.plus(cartItem.get('quantity'));
			totals.total = totals.total.plus(cartItem.get('total')).plus(cartItem.get('service_fees_total'));
			totals.subtotal = totals.subtotal.plus(cartItem.get('subtotal')).plus(cartItem.get('service_fees_subtotal'));
			totals.tax = totals.tax.plus(cartItem.get('tax')).plus(cartItem.get('service_fees_tax'));
			totals.service_fees_subtotal = totals.service_fees_subtotal.plus(cartItem.get('service_fees_subtotal'));
			
			if(cartItem.get('food_and_beverage') == 1){
				totals.subtotal = totals.subtotal.plus(cartItem.get('sides_subtotal'));
				totals.tax = totals.tax.plus(cartItem.get('sides_tax'));
				totals.total = totals.total.plus(cartItem.get('sides_total'));
			}


			// Add item taxes to cart taxes
			if( (( cart.get('taxable') && force_tax ===null ) || force_tax ===1)  && cartItem.get('taxes') && cartItem.get('taxes').length > 0){
				_.each(cartItem.get('taxes'), function(tax){
					var taxCopy = _.clone(tax);

					totals.taxes.push(taxCopy);
				});
			}
			
			// Add each side taxes to cart taxes
			_.each(side_types, function(side_type){
				if(!cartItem.has(side_type) || !cartItem.get('taxable')){
					return false;
				}
				_.each(cartItem.get(side_type).models, function(side){
					
					if(!cart.get('taxable') && !side.get('force_taxes')){
						return false;
					}
					if(side.get('taxes') && side.get('taxes').length > 0){
						_.each(side.get('taxes'), function(tax){
							var taxCopy = _.clone(tax);
							totals.taxes.push(taxCopy);
						});
					}							
				});	
			});

			// Add service fee taxes to cart taxes
			if(cartItem.get('service_fees') && cartItem.get('service_fees').length > 0){
				_.each(cartItem.get('service_fees').models, function(service_fee){
					service_fee.set('taxable',cart.get('taxable'));
					
					if(!cart.get('taxable') && !service_fee.get('force_tax')){
						return false;
					}

					_.each(service_fee.get('taxes'), function(tax){
						var taxCopy = _.clone(tax);
						totals.taxes.push(taxCopy);
					});
				});
			}
		});
		
		totals.total = totals.total.toDP(2).toNumber();
		totals.subtotal = totals.subtotal.toDP(2).toNumber();
		totals.tax = totals.tax.toDP(2).toNumber();
		totals.service_fees_subtotal = totals.service_fees_subtotal.toDP(2).toNumber();
		totals.taxes = this.groupTaxes(totals.taxes);
		totals.total_due = totals.total;
		totals.credit_card_fees = 0;
		// Total up all payments and subtract them from total (for total due)
		totals.total_due = new Decimal(totals.total_due);
		_.each(this.get('payments').models, function(payment){
			
			totals.total_due = totals.total_due.minus(payment.get('amount'));
			
			var params = payment.get('params');
			if(params && params.fee != undefined){
				totals.credit_card_fees = new Decimal(totals.credit_card_fees).plus(params.fee).toDP(2).toNumber();
				totals.total = new Decimal(totals.total).plus(params.fee).toDP(2).toNumber();
				totals.total_due = new Decimal(totals.total_due).plus(params.fee);
			}
		});
		totals.total_due = totals.total_due.toDP(2).toNumber();
		
		this.set(totals);
		return totals;
	},

    parse: function(response){
       	response = this.parseCollections(response);
        return response;
    },

    requirePin: function(){
		if(
			App.data.course.get_setting('require_employee_pin') == 1 &&
			(!this.has('employee_pin') || this.get('employee_pin') == null)
		){
			return true;
		}
		return false;	
    },

    complete: function(){
		
		var cart = this;
		var params = {'status': 'complete'};
		
		// For debugging, we will send a bunch of other cart info
		params.total = this.get('total');
		params.subtotal = this.get('subtotal');
		params.num_items = this.get('num_items');
		params.tax = this.get('tax');
		params.payments = this.get('payments');
		params.items = this.get('items');
		params.customer_note = this.get('customer_note');
		
		// If no items are selected in cart, stop
		if(this.get('items').where({'selected': true}).length == 0){
			$.loadMask.hide();
			return false;
		}

		// If employee PIN is required, show the employee pin window
		if(this.requirePin()){
			var employee_pin_window = new AuthorizeWindowView({action: 'Employee PIN'});
			employee_pin_window.show();
			return false;
		}
		
		if(App.data.course.get('require_employee_pin') == 1){
			params.employee_pin = this.get('employee_pin');
		}

		// Mark sale as complete (saves sale to database from cart)
		this.save(params, {'success': function(model, response){

			// If change is due, show change window
			var closeWindows = true;
			var total_due = new Decimal(cart.get('total_due'));
			
			// Hide payment window
			$('div.modal').modal('hide');
			if(!total_due.isZero() && cart.get('mode')!=='return'){
				var change = cart.get('payments').add({
					'amount': cart.get('total_due'),
					'type': 'change',
					'description': 'Change issued',
					'record_id': 0
				});
				var changeDueWindow = new ChangeDueWindow({model: change});
				changeDueWindow.show();

				closeWindows = false;
			}

			var sale = cart.addRecentTransaction(response);			
			
			// Open cash drawer if course uses it and there were cash payments
			if(App.data.course.get_setting('cash_drawer_on_sale') == 1){
				var receipt_printer = new Receipt();
				receipt_printer.open_cash_drawer(cart.get('employee_id'));
			
			}else if(App.data.course.get_setting('cash_register') == 1){
				for(var i = 0;i<cart.get('payments').length;i++ ){
					var has_cash = false;
					cart.get('payments').each(function(model){
						if(model.get("type") == "cash"){
							has_cash = true;
						}
					})
				}
				if(has_cash){
					var receipt_printer = new Receipt();
					receipt_printer.open_cash_drawer(cart.get('employee_id'));
				}
			}

			var receipt_emailed = false;
			if(App.data.course.get_setting('auto_email_receipt') == 1){
				receipt_emailed = sale.email_receipt_to_customer();
			}

			// If course is using a receipt printer
			if(	
				App.data.course.get_setting('auto_print_receipts') == 1 &&
				(
					App.data.course.get_setting('auto_email_no_print') == 0 || 
					(App.data.course.get_setting('auto_email_no_print') == 1 && !receipt_emailed)
				) &&
				(
					(!sale.hasCreditCardPayments() && App.data.course.get_setting('non_credit_card_receipt_count') > 0) ||
					(sale.hasCreditCardPayments() && App.data.course.get_setting('credit_card_receipt_count') > 0)
				)
			){
				sale.printReceipt();
			}				
			
			App.data.user.get('acl').clear_override();

			cart.clear();
			if(response.cart_id){
				cart.set('cart_id', response.cart_id);
			}

			if(closeWindows){
				$(document).find('.modal').modal('hide');
			}

			// If course prints receipts manually on PC printer
			if(App.data.course.get_setting('auto_print_receipts') == 0){
				var receipt_window = new ReceiptWindowView({model: sale, new_sale: true});
				receipt_window.show();
				
				// Delay so barcode image will load before printing
				setTimeout(function(){
					window.print();
				}, 200);
			
			// Redirect to tee sheet (if set to do so)
			}else{
				if(closeWindows && App.data.course.get_setting('after_sale_load') == 0){
					cart.redirect();
				}
			}	
		}});
	},
	
	redirect: function(){
		if(this.get('items').length > 0){
			return false;
		}

        if(App.data.course.get('tee_sheet_speed_up') == 1) {
            window.location = SITE_URL + '/v2/home#tee_sheet';
        }
        else {
            window.location = SITE_URL + '/teesheets';
        }
        return true;
	},

	sendFBOrder: function(e){
		var self = this;
		var sale_id = this.get('suspended_name')?this.get('suspended_name'):this.get('cart_id');
		var customer = this.get('customers').findWhere({'selected': true});
		var employee_id = this.get('employee_id');
		var employee = App.data.course.get('employees').get(employee_id);
		var item_models = this.get('items').where({'selected': true, 'food_and_beverage':"1"});
		var food_and_beverage = new CartItemCollection();
		var line = 1;
		var side_types = ['sides', 'soups', 'salads', 'items'];

		_.each(item_models, function(item){
			var item_data = item.toJSON();
			item_data.line = line;
			food_and_beverage.add(item_data);

			line++;
		});

		// If food & bev items were in cart, create a new F&B order
		if(food_and_beverage.length > 0 && this.get('mode') != 'return' && !this.get('order')){

			this._sendFBOrder(sale_id,food_and_beverage,employee,customer);
		}else if(this.get('order')){
			App.vent.trigger('notification', {'msg': 'Order already sent.', 'type':'warning'});
		}
	},

	_sendFBOrder: function(sale_id,food_and_beverage,employee,customer,callback){
		var customers = false;
		var self = this;
		if(customer != undefined){
			customers = [ customer.toJSON() ];
		}

		App.data.food_bev_table.open('POS_'+sale_id,function(){
			var table = App.data.food_bev_table;
			var order = new Order({
				'items': food_and_beverage.models,
				'sale_id': table.get('suspended_sale_id'),
				'pos': true,
				'employee': new Employee(employee.attributes),
				'customers': customers,
				'table_name': 'POS_'+sale_id,
				'table_number': 'POS_'+sale_id
			}, {'url': CART_URL + '/orders'});
			self.set('order',order);
			// Save and print order
			order.save({},{'success':function(response){
				if(typeof callback === 'function')callback(response);
			}});
		});
	},
	
	addRecentTransaction: function(response){
		
		var sale_id = response.sale_id;
		var customer = this.get('customers').findWhere({'selected': true});
		var payment_models = this.get('payments').models;
		var item_models = this.get('items').where({'selected': true});
		var items = [], payments = [], taxes = [], food_and_beverage = new CartItemCollection();
		var sale_number = response.number;
		var employee_id = this.get('employee_id');
		var employee = App.data.course.get('employees').get(employee_id);
		var credit_card_fee = this.get('credit_card_fees');

		var subtotal = this.get('subtotal');
		var tax = this.get('tax');
		var total = this.get('total');
		
		var line = 1;
		var side_types = ['sides', 'soups', 'salads', 'items'];

		var member_balance_change = 0;
		var account_balance_change = 0;

		// Loop through items and add them to an array for receipt printing
		_.each(item_models, function(item){
			
			var item_data = item.toJSON();
			item_data.line = line;

			if(item_data.item_type == 'member_account' && item_data.total){
				member_balance_change += item_data.total;
			}
			if(item_data.item_type == 'customer_account' && item_data.total){
				account_balance_change += item_data.total;
			}
			
			if(response.erange_codes != undefined && response.erange_codes[line] && response.erange_codes[line] != 0){
				item_data.erange_code = response.erange_codes[line];
			}
			
			if(item_data.item_type == 'tournament'){
				var tournament_price = new Decimal(item_data.pot_fee);
				item_data.unit_price = tournament_price.toDP(2).toNumber();
				item_data.subtotal = item_data.total = tournament_price.times(item_data.quantity).toDP(2).toNumber();
				item_data.tax = 0;
			}

			if(item_data.item_type == 'punch_card'){
				item_data.items = false;
			}

			items.push(item_data);
			
			// Break out service fees into their own lines for the receipt
			if(item_data.service_fees && item_data.service_fees.length > 0){
				_.each(item_data.service_fees, function(service_fee_data){
					line++;
					service_fee_data.line = line;
					items.push(service_fee_data);
				});
			}

			// If item is a F&B item, add it to a separate list
			if(item_data.food_and_beverage == 1){
				food_and_beverage.add(item_data);
			}

			// If item has sides, break apart those sides into their own lines
			if(item_data.food_and_beverage == 1 || item_data.item_type == 'tournament'){
				
				_.each(side_types, function(side_type){
					if(!item.has(side_type) || item.get(side_type).length == 0){
						return true;
					}	
					_.each(item.get(side_type).models, function(side){
						line++;
						var side_data = side.toJSON();
						side_data.line = line;

						items.push(side_data);						
					});
				});
			}
			
			line++;
		});
		
		// Add credit card fee to item list as its own line
		if(credit_card_fee > 0){
			line++;
			
			var fee_item = App.data.course.get_special_item('credit_card_fee');
			fee_item.unit_price = credit_card_fee;
			fee_item.subtotal = credit_card_fee;
			fee_item.total = credit_card_fee;
			fee_item.quantity = 1;
			fee_item.tax = 0;
			fee_item.line = line;
			
			subtotal = new Decimal(fee_item.unit_price).plus(subtotal).toDP(2).toNumber();
			items.push(fee_item);
		}
		
		var cart = this;

		// clean up if we already made the order
		if(this.get('order') && this.get('mode') != 'return'){
			App.data.food_bev_table.close();
		}
		else if(food_and_beverage.length > 0 && this.get('mode') != 'return'){
			// If food & bev items were in cart, create a new F&B order
			this._sendFBOrder(sale_id,food_and_beverage,employee,customer,function(){
				App.data.food_bev_table.close();
				cart.set('order', null);
			});
		}
		this.set('order',null);

		_.each(payment_models, function(payment){
			if(payment.get('type') == 'credit_card_save'){
				return true;
			}
			var attributes = payment.toJSON();
			if(attributes.params && attributes.params.signature_url){
				attributes.signature = attributes.params.signature_url;
			}
			attributes.sale_id = sale_id;
			payments.push(attributes);
		});

		_.each(this.get('taxes'), function(tax){
			taxes.push(_.clone(tax));
		});

		if(customer && customer.attributes){
			customer = _.clone(customer.attributes);
		}

		var member_account_payments = this.get('payments').where({'type': 'member_account'});
		if(member_account_payments.length > 0 && customer){
			_.each(member_account_payments, function(payment){
				member_balance_change -= payment.get('amount'); 
			});
		}

		var customer_account_payments = this.get('payments').where({'type': 'customer_account'});
		if(customer_account_payments.length > 0 && customer){
			_.each(customer_account_payments, function(payment){
				account_balance_change -= payment.get('amount');
			});
		}

		if(customer){
			customer.member_account_balance = new Decimal(customer.member_account_balance).plus(member_balance_change).toDP(2).toNumber();
			customer.account_balance = new Decimal(customer.account_balance).plus(account_balance_change).toDP(2).toNumber();
		}

		var sale_data = {
			'sale_id': sale_id,
			'number': sale_number,
			'teetime': this.get('teetime'),
			'items': items,
			'payments': payments,
			'sale_time': moment().format('YYYY-MM-DD HH:mm:ss'),
			'customer': customer,
			'employee': _.clone(employee.attributes),
			'comment': '',
			'taxes': _.clone(taxes),
			'total': total,
			'subtotal': subtotal,
			'tax': tax,
			'total_due': this.get('total_due'),
			'auto_gratuity_amount': 0,
			'customer_note': this.get('customer_note')
		};

		if(response.loyalty_points_earned && customer){
			sale_data.loyalty_points_earned = response.loyalty_points_earned
			customer.loyalty_points = parseInt(customer.loyalty_points) + parseInt(response.loyalty_points_earned);
		}

		if(response.loyalty_points_spent && customer){
			sale_data.loyalty_points_spent = response.loyalty_points_spent
			customer.loyalty_points = parseInt(customer.loyalty_points) - parseInt(response.loyalty_points_spent);
		}

		var sale = new Sale(sale_data);

		App.data.recent_transactions.unshift(sale);
		return sale;
	},

	isPaid: function(){
		var total_due = new Decimal(this.get('total_due'));
		var zero = new Decimal(0);
		if(
			(this.get('mode') == 'sale' && total_due.lessThanOrEqualTo(zero)) ||
			(this.get('mode') == 'return' && total_due.greaterThanOrEqualTo(zero))
		){
			return true;
		}
		return false;
	},

	changeMode: function(){
		var zero = new Decimal(0);
		if(this.get('items').length == 0){
			return;
		}
		_.each(this.get('items').models, function(model){
			var qty = new Decimal(model.get('quantity'));
			qty = zero.minus(qty).toDP(2).toNumber();
			model.set('quantity', qty);
		});
	},

	suspend: function(param, successCallback, errorCallback){
		
		var cart = this;
		var params = {'status':'suspended'};

		if(typeof(param) == 'string' && param != ''){
			params.suspended_name = param;
		}else{
			params.suspended_name = this.get('suspended_name');
		}
		var cart_data = cart.toJSON();
		cart_data.suspended_name = params.suspended_name;
		cart_data.status = 'suspended';

		this.save(params, {'wait':true, 'success': function(){

			App.data.suspended_sales.add(cart_data, {merge: true});

			cart.set({
				'suspended_name':null, 
				'status':'active', 
				'mode':'sale', 
				'taxable':true,
				'teetime_id': null,
				'override_authorization_id': null,
				'check_all': true,
				'return_sale_id': null
			});
			cart.get('items').reset();
			cart.get('sale_payments').reset();
			cart.get('payments').reset();
			cart.get('customers').reset();

			App.data.user.get('acl').clear_override();
			
			if(typeof(param) == 'function'){
				param();
			}
			
			if(typeof(successCallback) == 'function'){
				successCallback();
			}
		
		}, 'error': function(model, response){
			
			App.vent.trigger('notification', {'msg': response.responseJSON.msg, 'type':'error'});
			
			if(typeof(errorCallback) == 'function'){
				errorCallback();
			}
		}});
	},

	// When a customer is added to the cart, apply taxable status
	// and any discount to cart items
	applyCustomerSettings: function(customer, collection, options){
		
		if(!customer){
			return false;
		}

		// If customer has a discount, use it
		if(customer.get('discount') > 0){
			this.get('items').setDiscount(customer.get('discount'));
		
		// If customer does not have a discount, but belongs to some groups that might,
		// attempt to apply the group discount
		}else if(customer.get('groups').length > 0){
			this.get('items').setCostPlusPercentDiscount(customer.get('groups'));
		}

		var taxable = true;
		if(options.taxable != undefined){
			taxable = options.taxable;
		
		}else if(customer.get('taxable') == 0){
			taxable = false;
		}

		this.set('taxable', taxable);
		return true;
	},

	// Clears payments and selected items out of cart (called after sale completed)
	clear: function(){

		this.get('items').remove( this.get('items').where({'selected': true}) );
		this.get('sale_payments').reset();
		this.get('payments').reset();
		this.set({
			'suspended_name': null,
			'employee_pin': null,
			'employee_id': App.data.user.get('person_id'),
			'check_all': true,
			'credit_card_fees': 0,
			'return_sale_id': null,
			'status': 'active',
			'customer_note': null,
			'teetime': null
		});

		// Mark left over items as selected
		var items = this.get('items').models;
		if(items.length > 0){
			_.each(items, function(item){
				item.set({'selected': true});
			});
			
		}else{
			this.get('customers').reset();
			this.set({
				'taxable': true, 
				'mode': 'sale', 
				'teetime_id': null, 
				'override_authorization_id': null
			});
		}
	},

	addItem: function(data){

		if(data.item_type == 'tournament' && this.get('customers').findWhere({'selected': true}) == undefined){
			App.vent.trigger('notification', {'msg': 'A customer must be selected before adding a tournament', 'type': 'error'});
			return false;
		}
		
		var cart = this;
		if(this.get('mode') == 'return'){
			data.quantity = -1;
		}else{
			data.quantity = 1;
		}
		data.discount_percent = 0;
		data.selected = true;
		
		if(data.food_and_beverage == 1){
			data.price = data.unit_price;
		}

		if(!data.params){
			data.params = {};
		}
		data.params = _.clone(data.params);

		if(data.item_type == 'green_fee' || data.item_type == 'cart_fee'){
			data.params.teesheet_id = data.teesheet_id;
			data.params.holes = data.holes;

			if(_.has(data, 'season_id')){
				data.params.season_id = parseInt(data.season_id);
			}
		}

		var item = App.data.cart.get('items').create(data, {cart: App.data.cart, error: function(model, response, options){
			App.data.cart.get('items').remove(model);
			App.errorHandler(model, response, options);
		}});
		
		if((item.get('item_type') == 'invoice' || item.get('item_type') == 'statement') && item.has('params') && item.get('params').customer){
			var customer = item.get('params').customer;
			customer.selected = true;
			cart.get('customers').create(customer);
		}

		if(item.get('item_type') != 'invoice' && item.get('item_type') != 'statement'){
			item.trigger('show-details');
		}
	},
	
	prepareFullReturn: function(sale){
		
		// First clear out all items/customers/payments from cart
		var cart = this;
		this.cancel(function(){
			cart.issueFullReturn(sale);
		});
		
		return true;		
	},
	
	// Loads previous sale into cart to be returned
	issueFullReturn: function(sale){
			
		var cart = this;
		var cart_data = {};
		var taxable = true;
		cart_data.mode = 'return';
		cart_data.teetime_id = sale.get('teetime_id');
		cart_data.return_sale_id = sale.get('sale_id');
		cart_data.teetime = sale.get('teetime');

		if(sale.get('tax') == 0){
			taxable = false;
		}
		cart_data.taxable = taxable;
		var zero = new Decimal(0);

		// Set cart to 'return' mode, also set correct taxable status
		this.save(cart_data, {'success': function(model, response){
			
			// Loop through items in sale and add them to the cart
			_.each(sale.get('items').models, function(item){
				
				var attributes = item.toJSON();
				var qty = new Decimal(attributes.quantity);
				attributes.quantity = zero.minus(qty);
				attributes.selected = true;
				
				// If F&B item, and item has sides, clear out the sides 
				// because they are already broken out into their own lines
				if(attributes.food_and_beverage == 1){
					attributes.sides = attributes.soups = attributes.salads = [];
				}

				// Add item to the cart
				var cart_item = cart.get('items').create(attributes, {cart: cart});
			});

			cart.get('sale_payments').reset();

			if(sale.get('payments').length > 0){
				_.each(sale.get('payments').models, function(payment){
					if(payment.get('type') == 'credit_card' && payment.get('record_id') != 0){
						cart.get('sale_payments').add(payment);
					}
				});
			}

			// If customer was attached to the sale, add them to cart as well
			if(sale.get('customer') && sale.get('customer').get('person_id')){
				
				var customer = sale.get('customer').toJSON();
				var customer_taxable = customer.taxable;
				var customer_discount = customer.discount;
				customer.selected = true;
				
				// Override customer's taxable and discount settings so we
				// don't override what the amounts truly were from the sale
				customer.taxable = taxable;
				customer.discount = 0;
				
				var customer_model = cart.get('customers').create(customer);
				
				// Reset customers tax status and discount back to normal
				customer_model.set({
					'taxable': customer_taxable,
					'discount': customer_discount
				}, {'silent': true});
			}

			cart.trigger('loaded');

        }, 'error': function(model, response){
            if (typeof response.responseJSON.msg != 'undefined') {
                App.vent.trigger('notification', {'msg': response.responseJSON.msg, 'type': 'error'});
            }
            cart.trigger('loaded');

        }});
	},
	
	cancel: function(callback){	
		var cart = this;
		
		if(cart.get('suspended_name') != '' && cart.get('suspended_name') != null){
			this.suspend(callback);
		
		}else{
			cart.destroy({success: function(model, response){
				cart.off('change:taxable');
				cart.set({
					'suspended_name': null, 
					'cart_id': response.cart_id, 
					'mode': 'sale', 
					'taxable': true,
					'status': 'active'
				});	
				cart.get('items').reset();
				cart.get('customers').reset();
				cart.get('sale_payments').reset();
				cart.get('payments').reset();
				
				if(typeof(callback) == 'function'){
					callback();
				}	
			}});
		}		
	}
});

var CartItem = Backbone.Model.extend({

	idAttribute: "line",
	defaults: {
		"selected": true,
		"is_sub_item": false,
		"name": "",
		"item_type": "item",
		"item_id": "",
		"category": "",
		"sub_category": "",
		"department": "",
		"description": "",
		"max_discount": 0,
		"discount": 0,
		"discount_percent": 0,
		"unit_price": 0.00,
		"base_price": 0,
		"subtotal": 0.00,
		"total": 0.00,
		"tax": 0.00,
		"discount_amount": 0.00,
		"non_discount_subtotal": 0.00,
		"taxes": [],
		"taxable": true,
		"quantity": 1,
		"base_quantity": 1,
		"printer_ip": "",
		"print_priority": 0,
		"selected": true,
		"loyalty_points_per_dollar": false,
		"loyalty_dollars_per_point": false,
		"loyalty_cost": false,
		"loyalty_reward": false,
		"timeframe_id": null,
		"special_id": null,
		"giftcard_id": null,
		"punch_card_id": null,
		"teetime_id": null,
		"params": null,
		"modifier_total": 0.00,
		"food_and_beverage": 0,
        "erange_size": 0,
        "erange_code": null,
        "is_serialized": 0,
		"force_tax": null,
		"service_fees_total": 0,
		"service_fees_subtotal": 0,
		"service_fees_tax": 0
	},

	// Over ride backbone save method to only send certain fields to be
	// saved to server (instead of entire object)
	save: function (attrs, options) {
		attrs = attrs || this.toJSON();
		options = options || {};

		// Filter attributes that will be saved to server
		attrs = _.pick(attrs, [
			'item_id',
			'item_type',
			'quantity',
			'unit_price',
			'discount_percent',
			'tournament_id',
			'timeframe_id',
			'price_class_id',
			'special_id',
			'punch_card_id',
			'selected',
			'invoice_id',
			'params',
			'unit_price_includes_tax',
            'erange_size'
		]);

		options.attrs = attrs;

		// Call super with attrs moved to options
		Backbone.Model.prototype.save.call(this, attrs, options);
	},

	initialize: function(attrs, options){
		
		if(!this.get('is_sub_item') && !this.get('line') && App.data.cart){
			this.set('line', App.data.cart.getNextLine());
		}
		
		if(!this.has('unit_price_includes_tax')){
			this.set('unit_price_includes_tax', (App.data.course.get('unit_price_includes_tax') == 1));
		}
		this.set('unit_price_includes_tax', (this.get('unit_price_includes_tax') == 1));

		if(this.get('customer_groups')){
			this.set('customer_groups', new GroupCollection(this.get('customer_groups')));
		}
		if(this.get('service_fees')){
			this.set('service_fees', new ServiceFeeCollection(this.get('service_fees')));
		}
		
		this.cart = false;
		if(this.collection && this.collection.cart){
			this.cart = this.collection.cart;
		}
		
		if(!this.get('is_sub_item') && this.has('items') && this.get('items')){
			var subItems = new SubItemCollection();
			subItems.cart = this.cart;
			subItems.reset(this.get('items'));
			this.set('items', subItems);
		}

		// If a customer with a discount is attached to cart, apply discount to item
		var customer = false;
		var customer_discount = new Decimal(0);
		if(this.cart && this.cart.get('customers') instanceof CartCustomerCollection && this.cart.get('customers').length > 0){
			var customer = this.cart.get('customers').findWhere({selected: true});
			if(customer){
				customer_discount = new Decimal(customer.get('discount'));
			}
		}

		// If customer has discount
		if(customer && customer_discount.greaterThan(0)){
			this.set_discount(customer_discount.toDP(2).toNumber());

		// If customer belongs to groups, apply any discounts from them to the item
		}else if(customer && customer.get('groups').length > 0){
			this.set_cost_plus_percent_discount(customer.get('groups'));
		}
		
		// If any changes made to item, re-calcuate price (total, tax, etc)
		this.calculatePrice();
		this.on("change:quantity change:discount_percent change:unit_price", this.calculatePrice);
		if(this.get('items') && this.get('items') instanceof SubItemCollection){
			this.listenTo(this.get('items'), 'reset', this.calculatePrice);
		}
		this.on("change:selected", this.set_check_all);
		
		if(this.cart){
			this.listenTo(this.cart, 'change:taxable', this.calculatePrice);
		}
	},

	setFee: function(feeAttributes){
		var attr = _.omit(feeAttributes, ['quantity']);
		return this.set(attr).save();		
	},

	set_check_all: function(){
		if(this.cart && this.get('selected') == false){
			this.cart.set({'check_all': false});
		}
	},

	set_discount: function(discount_percent){

		if(!discount_percent){
			return false;
		}
		var discount_percent = new Decimal(discount_percent);

		var non_discount_items = ["green_fee", "cart_fee", "giftcard", "punch_card", 
			"member_account", "customer_account", "invoice_account", 
			"invoice", "statement", "tournament"];

		// If a discount is already set on the item, or it's an item that should
		// not have a discount at all, skip it
		var current_discount = new Decimal(this.get('discount'));
		if(current_discount.greaterThan(0) || non_discount_items.indexOf(this.get('item_type')) > -1){
			return true;
		}
		
		var max_discount = new Decimal(this.get('max_discount'));
		var allowed_discount = discount_percent;
		
		// Force discount to item's max discount
		if(max_discount.lessThan(discount_percent)){
			allowed_discount = max_discount;
		}
		
		this.set({
			'discount_percent': allowed_discount.toDP(2).toNumber(),
			'discount': allowed_discount.toDP(2).toNumber()
		});
	},

	set_cost_plus_percent_discount: function(customer_groups){
		console.log('set cost plus');
		// Get best discount rate (cost + lowest percentage)
		if(!this.get('customer_groups')){
			return false;
		}
		var cost_plus_percent = this.get('customer_groups').getBestCostPlusPercent(customer_groups);

		// If no discount was found in the given groups, do nothing
		if(cost_plus_percent === null || cost_plus_percent === false || cost_plus_percent === undefined || cost_plus_percent === ''){
			return false;
		}

		// Determine effective discount percentage on unit price
		cost_plus_percent = new Decimal(cost_plus_percent).dividedBy(100).toDP(2);
		var price = new Decimal(this.get('unit_price'));
		var cost = new Decimal(this.get('cost_price'));
		var discounted_price = cost.times(cost_plus_percent).plus(cost).toDP(2);
		
		//var effective_discount_percent = new Decimal(1).minus(discounted_price.dividedBy(price).toDP(2));
		var effective_discount_percent = price.minus(discounted_price).dividedBy(price);
		this.set_discount(effective_discount_percent.times(100).toDP(2).toNumber());
	},
	
	calculatePrice: function(taxable){
		var cartItem = this;
		if(typeof taxable === 'undefined' || typeof taxable === 'object'){
			if(this.cart){
				taxable = this.cart.get('taxable');
			}
		}
		var unit_price_includes_tax = this.get('unit_price_includes_tax');

		var force_tax = null;
		if(parseInt(this.get('force_tax')) === 0){
			force_tax = false;
		}else if(parseInt(this.get('force_tax')) === 1){
			force_tax = true;
		}

		var service_fees_total = new Decimal(0);
		var service_fees_subtotal = new Decimal(0);
		var service_fees_tax = new Decimal(0);

		if(this.get('item_type') == 'item_kit' || this.get('item_type') == 'punch_card'){
			
			var discounted_subtotal = new Decimal(0);
			var subtotal = new Decimal(0);
			var tax = new Decimal(0);
			var unit_price = new Decimal(0);

			if(this.get('items') && this.get('items').models){
				_.each(this.get('items').models, function(subItem){
					
					var subItemQty = new Decimal(subItem.get('base_quantity'));
					subItemQty = subItemQty.times(cartItem.get('quantity'));
					subItem.set({
						'discount_percent': cartItem.get('discount_percent'),
						'quantity': subItemQty
					});

					discounted_subtotal = discounted_subtotal.plus(subItem.get('subtotal'));
					subtotal = subtotal.plus(subItem.get('non_discount_subtotal'));
					unit_price = unit_price.plus(subItem.getSubtotal(subItem.get('unit_price'), subItem.get("base_quantity"), 0));
					service_fees_subtotal = service_fees_subtotal.plus(subItem.get('service_fees_subtotal')).toDP(2);
					service_fees_tax = service_fees_tax.plus(subItem.get('service_fees_tax')).toDP(2);
					service_fees_total = service_fees_total.plus(subItem.get('service_fees_total')).toDP(2);
				});				
			}

			discounted_subtotal = discounted_subtotal.toDP(2).toNumber();
			subtotal = subtotal.toDP(2).toNumber();
			unit_price = unit_price.toDP(2).toNumber();
		
		}else{
			var discounted_subtotal = this.getSubtotal(this.get('unit_price'), this.get("quantity"), this.get('discount_percent'));
			var subtotal = this.getSubtotal(this.get('unit_price'), this.get("quantity"), 0);
			var unit_price = this.get('unit_price');
		}

		var tax = this.getTax(discounted_subtotal, this.get('unit_price_includes_tax'));
		if(this.get('unit_price_includes_tax') && this.get('item_type') != 'item_kit' && this.get('item_type') != 'punch_card'){	
			discounted_subtotal = new Decimal(discounted_subtotal).minus(tax).toDP(2).toNumber();
			subtotal = new Decimal(subtotal).minus(tax);
		}


		if((taxable&&force_tax===null) || force_tax){
			// taxes normal
		}else{
			//tax = 0;
			_.forEach(this.get('taxes'),function(tax){
				tax.amount = 0;
			});
		}

		var total = this.getTotal(discounted_subtotal, tax, (taxable&&force_tax===null) || force_tax);
		
		var loyalty_cost = false;
		var loyalty_reward = false;
		if(this.get('loyalty_dollars_per_point') !== false || this.get('loyalty_points_per_dollar') !== false){
			discounted_subtotal = new Decimal(discounted_subtotal);
			if(this.get('loyalty_dollars_per_point') !== false){
				loyalty_cost = discounted_subtotal.dividedBy(this.get('loyalty_dollars_per_point')).toDP(2).toNumber();
			}
			if(this.get('loyalty_points_per_dollar') !== false){
				loyalty_reward = discounted_subtotal.times(this.get('loyalty_points_per_dollar')).toDP(2).toNumber();
			}
			discounted_subtotal = discounted_subtotal.toDP(2).toNumber();
		}

		var discount_amount = 0;
		var subtotal = new Decimal(subtotal);
		if(!subtotal.minus(discounted_subtotal).isZero()){
			discount_amount = subtotal.minus(discounted_subtotal).toDP(2).toNumber();
		}

		if(this.get('service_fees') && this.get('service_fees').length > 0){
			_.each(this.get('service_fees').models, function(fee){
				
				var parent_item_data = {
					"unit_price": unit_price,
					"non_discount_subtotal": subtotal.toDP(2).toNumber(),
					"subtotal": discounted_subtotal,
					"tax": tax,
					"total": total,
					"quantity": cartItem.get('quantity')
				};

				fee.calculatePrice(taxable, 1, parent_item_data);
				
				service_fees_subtotal = service_fees_subtotal.plus(fee.get('subtotal')).toDP(2);
				service_fees_tax = service_fees_tax.plus(fee.get('tax')).toDP(2);
				service_fees_total = service_fees_total.plus(fee.get('total')).toDP(2);
			});
		}
		
		this.set({
			"taxable": taxable,
			"unit_price": unit_price,
			"non_discount_subtotal": subtotal.toDP(2).toNumber(),
			"subtotal": discounted_subtotal,
			"tax": tax,
			"total": total,
			"discount_amount": discount_amount,
			"loyalty_cost": loyalty_cost,
			"loyalty_reward": loyalty_reward,
			"service_fees_subtotal": service_fees_subtotal.toNumber(),
			"service_fees_tax": service_fees_tax.toNumber(),
			"service_fees_total": service_fees_total.toNumber()
		});
	},

	getSubtotal: function(price, qty, discount){
		var price = new Decimal(price);
		var discount = new Decimal(discount);
		var qty = new Decimal(qty);
		
		var percent = new Decimal(100).minus(discount).dividedBy(100);
		var discounted = price.times(percent).toDP(2).times(qty);
		
		return discounted.toDP(2).toNumber();
	},
	
	setTax: function(){
		this.set('tax', this.getTax(this.get('subtotal')));
	},
	
	getTax: function(subtotal, tax_included){
		var model = this;
		if(tax_included === undefined){
			tax_included = false;
		}

		// If item has sub items, combine the taxes of the sub items
		if(this.get('item_type') == 'tournament' || this.get('item_type') == 'item_kit' || this.get('item_type') == 'punch_card'){	
			
			var combined_taxes = [];
			var item_tax = new Decimal(0);

			if(this.has('items') && this.get('items').models && this.get('items').length > 0){
				_.each(this.get('items').models, function(item){
					if(item.get('force_tax')!==0 && item.get('force_tax')!=='0')
					  combined_taxes = combined_taxes.concat(item.get('taxes'));
					item_tax = item_tax.plus(item.get('tax'));
					if(item.get('service_fees')){
						_.each(item.get('service_fees').models,function(fee){
							if(typeof fee === 'undefined')return;
							if(!fee.get('force_tax')===0 && !fee.get('force_tax')==='0')
							  combined_taxes = combined_taxes.concat(fee.get('taxes'));
						});
					}
				});
			}

			this.set('taxes', combined_taxes, {silent: true});
			return item_tax.toDP(2).toNumber();
		
		}else{
			var force_tax = this.get('force_tax');
			return this.calculateTax(subtotal, this.get('taxes'), tax_included);
		}
	},
	
	calculateTax: function(subtotal, taxList, tax_included, force_tax){

		var totalTax = new Decimal(0);
		var subtotal = new Decimal(subtotal);	
		var item = this;
		var taxes = [];

		if(this.get('type') == 'tournament'){
			tax_included = true;
		}

		_.each(taxList, function(tax){
			taxes.push(_.clone(tax));
		});
		
		if(tax_included === undefined){
			tax_included = false;
		}
		if(!taxes || taxes.length == 0){
			return 0;
		}

        var total_percent = 0.00;
        _.each(taxes,function(tax){
            if(!tax.percent || tax.percent <= 0)return;
            total_percent = new Decimal(total_percent).plus(tax.percent);
        });
        total_percent = new Decimal(total_percent);
		_.each(taxes, function(tax, i){

			var percentage = new Decimal(tax.percent);
			if(!tax || percentage.isNaN() || percentage.isZero()){
				return true;
			}
			
			var taxAmount = new Decimal(0);
			if(tax_included){
				var actual_subtotal = subtotal.dividedBy( total_percent.dividedBy(100).plus(1) ).toDP(2);
                var percent = percentage.dividedBy(100);
                taxAmount = actual_subtotal.times(percent).toDP(2);
			
			}else if(tax.cumulative == "1"){
				var percent = percentage.dividedBy(100);
				taxAmount = subtotal.plus(totalTax).times(percent).toDP(2);
			
			}else{
				var percent = percentage.dividedBy(100);
				taxAmount = subtotal.times(percent).toDP(2);
			}

			tax.amount = taxAmount.toNumber();
			totalTax = totalTax.plus(taxAmount.toNumber());
		});
		this.set('taxes', taxes, {silent: true});
		return totalTax.toDP(2).toNumber();		
	},

	getTotal: function(subtotal, tax, taxable){
		var subtotal = new Decimal(subtotal);
		var tax = new Decimal(tax);

		if(taxable){
			var total = subtotal.plus(tax);
		}else{
			var total = subtotal;
		}
		
		return total.toDP(2).toNumber();
	},

	validate: function(attrs, options){
		
		// Validate is also called on response from the server, we don't care
		// about validating server responses
		if(options && options.xhr != undefined){
			return;
		}
		
		if(attrs.quantity < 0){
			return false;
		}
		
		if(this.get('inventory_unlimited') == 0 && attrs.quantity > this.get('inventory_level')){
			App.vent.trigger('notification', {
				type: 'warning', 
				msg: "Warning: Inventory insufficient for <em>"+ attrs.name +"</em>"
			});			
		}

		if(attrs.unit_price !== undefined || attrs.discount_percent !== undefined){
			var minPrice = this.getSubtotal(attrs.base_price, attrs.quantity, attrs.max_discount);
			var setPrice = this.getSubtotal(attrs.unit_price, attrs.quantity, attrs.discount_percent);
			var hasPermission = (App.data.user.is_manager() || App.data.user.is_admin() || App.data.user.is_super_admin());
			var isAuthorized = false;
			
			if(App.data.cart.get('override_authorization_id') != null){
				isAuthorized = true;
				hasPermission = true;
			}

			if(!hasPermission && !isAuthorized && setPrice < minPrice){
				return "Maximum discount is " + accounting.formatMoney(this.get('max_discount'), '') + '% or ' + accounting.formatMoney(minPrice) +
					'<button class="btn btn-sm btn-default pos-manager-override">Manager Override</button>';
			
			}else if(hasPermission && !isAuthorized && setPrice < minPrice){
				App.vent.trigger('notification', {
					type: 'info', 
					msg: "Maximum discount is " + accounting.formatMoney(this.get('max_discount'), '') + '% or <strong>' + accounting.formatMoney(minPrice)+'</strong>'
				});
			}
		}
	},
	
	destroy: function(options){
		this.stopListening(this.cart);
		Backbone.Model.prototype.destroy.apply(this, options);
	}
});

var SubItemCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		attrs.is_sub_item = true;
		return new CartItem(attrs, options);
	}
});

var CartItemCollection = Backbone.Collection.extend({
	url: CART_URL + '/items',
	
	model: function(attrs, options){
		if(attrs && attrs.food_and_beverage == 1){
			attrs.price = attrs.unit_price;
			return new FbCartItem(attrs, options);
		}else{
			return new CartItem(attrs, options);
		}
	},

	initialize: function(attrs, options){

		if(options && options.cart){
			this.cart = options.cart;
		}else{
			this.cart = App.data.cart;
		}
		this.url = CART_URL +'/'+ this.cart.get('cart_id') +'/items';
	},

	getSelected: function(){
		return this.filter(function(model){
			if(model.get('selected') == true){
				return true;
			}
			return false;
		});
	},

	getUnselected: function(){
		return this.filter(function(model){
			if(model.get('selected') == false){
				return true;
			}
			return false;
		});
	},

	setDiscount: function(discount){
		_.each(this.models, function(item){
			item.set_discount(discount);
			item.save();
		});
	},

	setCostPlusPercentDiscount: function(customer_groups){
		
		if(!customer_groups || customer_groups.length == 0){
			return false;
		}

		_.each(this.models, function(item){
			item.set_cost_plus_percent_discount(customer_groups);
			item.save();
		});
	},
	
	getNextLine: function(){
		return this.cart.getNextLine();
	}
});

var SuspendedSaleCollection = Backbone.Collection.extend({
	url: CART_URL,
	model: Cart
});