var QuickButtonItem = Backbone.Model.extend({});
var QuickButtonItemCollection = Backbone.Collection.extend({
	model: QuickButtonItem,
	comparator: function(a, b){
		var order_a = parseInt(a.get('order'));
		var order_b = parseInt(b.get('order'));
		
		if(order_a > order_b){
			return 1;
		}
		if(order_a < order_b){
			return -1;
		}
		return 0;
	}
});

var QuickButton = Backbone.Model.extend({

	idAttribute: "quickbutton_id",

	defaults: {
		"display_name": '',
		"tab": 1,
		"position": 0,
		"items": [],
		"color": null
	},

	initialize: function(attrs, options){
		if(attrs && attrs.items){
			this.set('items', new QuickButtonItemCollection(attrs.items));
		}
	},
	
	validate: function(attrs){
		if(attrs.display_name !== undefined){
			if(attrs.display_name == ''){
				return 'Button name is required';
			}
		}
		if(attrs.items !== undefined){
			if(attrs.items.length == 0){
				return 'At least 1 item is required';
			}
		}		
	}
});

var QuickButtonCollection = Backbone.Collection.extend({

	model: QuickButton,
	url: QUICKBUTTONS_URL,
	
	comparator: function(a, b){
		var order_a = parseInt(a.get('position'));
		var order_b = parseInt(b.get('position'));

		if(order_a > order_b){
			return 1;
		}
		if(order_a < order_b){
			return -1;
		}
		return 0;
	},
	
	saveOrder: function(){
		
		var data = {};	
		_.each(this.models, function(model){
			data[model.get('quickbutton_id')] = parseInt(model.get('position'));
		});
		
		$.ajax({
			url: this.url + '/order',
			type: 'PUT',
			success: function(result) {
				console.debug(result);
			},
			data: JSON.stringify(data),
			dataType: 'json',
			contentType: 'application/json'
		});
	}
});
