var Raincheck = Backbone.Model.extend({
	idAttribute: "raincheck_id",
	
	defaults: {
		"raincheck_number": 0,
		"teesheet": null,
		"customer": null,
		"expiration_date": null,
		"green_fee": null,
		"cart_fee": null,
		"players": 1,
		"holes_completed": 0,
		"green_fee_price": 0,
		"cart_fee_price": 0,
		"tax": 0,
		"subtotal": 0,
		"total": 0,
		"expiry_date": null,
		"customer_id": null,
        "teetime_id": null,
        "taxable": true
    },
	
	initialize: function(){
		this.on('change:players change:green_fee change:cart_fee change:holes_completed change:teesheet', this.calculateTotal);
        this.calculateTotal();
	},
	
	save: function (attrs, options) {
		attrs = attrs || this.toJSON();
		options = options || {};

		// Filter attributes that will be saved to server
		attrs = _.pick(attrs, [
			'teesheet_id', 
			'teetime_id',
			'green_fee',
			'cart_fee',
			'players', 
			'holes_completed', 
			'customer_id',
			'expiry_date',
            'taxable'
		]);

		options.attrs = attrs;

		// Call super with attrs moved to options
		Backbone.Model.prototype.save.call(this, attrs, options);
	},	
	
	calculateTotal: function(){
        var gf = this.get('green_fee');
        gf = gf !== null && typeof gf.attributes != 'undefined' ? new Fee(this.get('green_fee').attributes) : new Fee(this.get('green_fee'));
        var cf = this.get('cart_fee');
        cf = cf !== null && typeof cf.attributes != 'undefined' ? new Fee(this.get('cart_fee').attributes) : new Fee(this.get('cart_fee'));

        var green_holes = gf != undefined ? parseInt(gf.get('holes')) : 18;
        var cart_holes = cf != undefined ? parseInt(cf.get('holes')) : 18;

		var green_fee = 0;
		if(gf && gf.get('unit_price')){
			green_fee = _.round(gf.get('unit_price'));
		}
		
		var cart_fee = 0;
		if(cf && cf.get('unit_price')){
			cart_fee = _.round(cf.get('unit_price'));
		}
		var adj_green_fee = green_fee;
		var adj_cart_fee = cart_fee;
		
		if(this.get('holes_completed') > 0){
            var green_ratio = _.round((green_holes - parseInt(this.get('holes_completed'))) / green_holes, 5);
            var cart_ratio = _.round((cart_holes - parseInt(this.get('holes_completed'))) / cart_holes, 5);
            adj_green_fee = _.round(green_fee * green_ratio);
			adj_cart_fee = _.round(cart_fee * cart_ratio);
		}


		var green_fee_tax = this.getTax(adj_green_fee, 'green_fee');
		if(gf && gf.get("unit_price_includes_tax") == 1){
			adj_green_fee -= green_fee_tax;
		}

		var cart_fee_tax = this.getTax(adj_cart_fee, 'cart_fee');
		if(cf && cf.get("unit_price_includes_tax") == 1){
			adj_cart_fee -= cart_fee_tax;
		}

		var total_fees = _.round(adj_green_fee + adj_cart_fee);
		var tax = _.round(green_fee_tax + cart_fee_tax);
		var tax =  _.round(tax * this.get('players'));
        if (!this.get('taxable')) {
            tax = 0;
        }
		var subtotal = _.round(total_fees * this.get('players'));
		var total = _.round(subtotal + tax);
		
		this.set({
			green_fee_price: adj_green_fee,
			cart_fee_price: adj_cart_fee,
			tax: tax,
			subtotal: subtotal,
			total: total
		});
	},
	
	getTax: function(subtotal, fee_type){
        if (!this.get('taxable')) {
            return 0;
        }
        else {
            var gf = this.get('green_fee');
            gf = gf !== null && typeof gf.attributes != 'undefined' ? new Fee(this.get('green_fee').attributes) : new Fee(this.get('green_fee'));
            var cf = this.get('cart_fee');
            cf = cf !== null && typeof cf.attributes != 'undefined' ? new Fee(this.get('cart_fee').attributes) : new Fee(this.get('cart_fee'));

            var totalTax = 0.00;
            var subtotal = parseFloat(subtotal);
            var taxes = [];

            if(fee_type == 'green_fee'){
                if(!gf){
                    return totalTax;
                }
                taxes = gf.get('taxes');

            }else{
                if(!cf){
                    return totalTax;
                }

                taxes = cf.get('taxes');
            }

            if(taxes && taxes.length > 0){
                _.each(taxes, function(tax){
                    var percentage = parseFloat(tax.percent);
                    if(isNaN(percentage) || percentage == 0){
                        return true;
                    }

                    var taxAmount = 0;
                    if(tax.cumulative == "1"){
                        taxAmount = _.round( _.round(percentage / 100, 4) * _.round(subtotal + totalTax, 2), 5);
                    }else{
                        taxAmount = _.round( _.round(percentage / 100, 4) * subtotal, 5);
                    }

                    taxAmount = _.round(taxAmount, 2);
                    tax.amount = taxAmount;
                    totalTax += taxAmount;
                });
            }

            return _.round(totalTax, 2);
        }
	},
	
	printReceipt: function(){
		var data = this.attributes;
		var receipt = new RaincheckReceipt(data);
		receipt.print();
	}
});

var RaincheckCollection = Backbone.Collection.extend({
	url: RAINCHECKS_URL,
	model: Raincheck
});
