var ACL = Backbone.Model.extend({

	defaults: {
		employee_id: false,
		permissions: [],
		override: false
	},

	can: function(action, permission){

		var permissions = this.get('permissions');
		if(this.get('override') && this.get('override').permissions){
			permissions = this.get('override').permissions;
		}

		permission_str = 'fu/acl/Permissions/' + permission + ':' + action;
		if(permissions && permissions[permission_str]){
			return true;
		}
		return false;
	},

	set_override: function(employee_id, permissions){
		this.set({
			override: {
				employee_id: employee_id,
				permissions: permissions
			}
		});
	},

	clear_override: function(){
		this.set({
			override: false
		});
	}
});