var Season = Backbone.Model.extend({
	idAttribute: "season_id",
	
	comparator: function(a, b){
		if(!a.get('season_name') || !b.get('season_name') || (a.get('season_name') == b.get('season_name'))){
			return 0;
		}
		if(a.get('season_name') > b.get('season_name')){
			return 1;
		}else{
			return -1;
		}
	}
});

var SeasonCollection = Backbone.Collection.extend({
	model: Season,

	getDropdownData: function(params){
		
		var seasons = this.toJSON();
		if(params && params.season_ids){
			seasons = _.filter(seasons, function(season){
				if(params.season_ids.indexOf(parseInt(season.season_id)) >= 0){
					return true;
				}
				return false;
			});
		}

		var data = {};
		_.each(seasons, function(season){
			var start = moment(season.start_date, 'YYYY-MM-DD');
			var end = moment(season.end_date, 'YYYY-MM-DD');
			var dateRange = ' [' + start.format('MMM D') + ' to ' + end.format('MMM D') + ']';

			data[' ' + season.season_id] = season.teesheet_title +' - '+ season.season_name + dateRange;
		});
		
		if(!params.season_ids || params.season_ids.indexOf(0) >= 0){
			data[' 0'] = 'Specials';
		}
		
		return data;
	}
});