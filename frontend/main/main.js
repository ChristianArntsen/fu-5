// Override ajax function to always pass API key
$(document).ajaxSend(function (e, xhr, options) {
	xhr.setRequestHeader("Api-key", API_KEY);
});

$(document).ajaxComplete(function(event, response, options){	
	
	var code = 0;
	var error = false;
	var message = '';
	
	if(response && response.code){

		code = response.code;
	} else if (response && response.status) {
		code = response.status;
	}

	if(response && response.responseJSON && response.responseJSON.error){
		error = response.responseJSON.error;
	}

	if(response && response.responseJSON && response.responseJSON.msg){
		message = response.responseJSON.msg;
	}

	// If user is logged out, show login window
	if (code == 401) {
		var login_window = new LoginWindowView({closable: false});
		login_window.show();
	}

	if(code == 500 && error == 'missing_cart'){
		setTimeout(function(){ location.reload() }, 2500);
	}
});

// Allow multiple nested bootstrap modal windows
$(document).on('show.bs.modal', '.modal', function (event) {
	var zIndex = 1040 + (10 * $('.modal:visible').length);
	$(this).css('z-index', zIndex);

	setTimeout(function () {
		$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
	}, 0);
});

// Configure number library
Decimal.config({precision: 10})

Backbone.Marionette.Renderer.render = function (template, data) {
	return template(data);
};

// Main router to handle page links
var Router = Backbone.Router.extend({
    last_route: '',
	page: 'Home',
	routes: {
		"": 					"index",
		"401": 					"no_permission",
		"sales":				"sales",
		"sales/logout": 		"sales_logout",
		"food_and_beverage": 	"food_and_beverage",
		"reports": 				"reports",
		"report_edit": 			"report_edit",
		"report_edit/:id": 		"report_edit",
        "tee_sheet":            "tee_sheet",
        "customers":            "customers",
        "new_customers":        "customers_new",
        "settings":             "settings",
        "settings/:section":    "settings",
        "settings/:section/:subsection":    "settings",
		"billing":        		"billing",
        "passes": 				"passes",
        "item_kits": 			"item_kits",
        "starter_teesheet/:id": 	"starter_teesheet",
		"contact_forms": "contact_forms",
        "support_tools": "support_tools",
        "bulk_edit_support": "bulk_edit_support",
        "items": 				"items",
        "items_old": 			"items_old"
	},

	initialize: function () {
        this.on('route', this.set_page_title);
        this.on('route', this.set_page_class_name);
        this.on('route', this.set_ping_handler);
	},

	index: function () {
		this.navigate('sales', {trigger: true});
	},

	set_page_title: function (route) {
		document.title = App.data.course.get('name') + ' - ' + this.page + ' (powered by ForeUP)';
	},
    set_page_class_name:function(route){
        $('#page').removeClass(this.last_route);
        $('#page').addClass(route);
        this.last_route = route;
    },
	check_access:function(route){
		var module = App.data.modules.findWhere({'module_id': route});
        if(_.isUndefined(module)){
            this.navigate("401", {trigger: true, replace: true});
            return false;
		}

        if(App.data.user.has_module_permission(module.get('module_id'))){
            return true;
		}

		this.navigate("401", {trigger: true, replace: true});
        return false;
	},
	set_ping_handler: function (route) {
		if (route == 'sales') {
			App.vent.on('ping', function () {
				var data = {
					module: route,
					cart_id: App.data.cart.get('cart_id')
				};

				$.ajax({
					url:SITE_URL + '/v2/home/ping',
					data: data,
					headers: {"read_only_session":1}
				})
			});
			return true;
		}
		App.vent.off('ping');
	},
    no_permission:function(){
        this.page = 'No Permission';
        var page = new NoPermissionView();
        App.page.show(page);

	},
	reports: function (id) {
		this.page = 'Reports';
		var page = new reporting_views_reports();
		App.page.show(page);
	},
	starter_teesheet:function(id){

        if(!this.check_access("teesheets")){
            return;
        }
		var teesheetId = id;
		var date = moment();
		var collection = new BookingCollection(null,{
			url:"/teesheets/"+teesheetId+"/bookings",
			teesheetId:teesheetId,
			date:date
		});
		var groupedByTime = Backbone.buildGroupedCollection({
			collection: collection,
			groupBy: function (booking) {
				return booking.get('start');
			},
			comparator:function(model){
				return model.get("id");
			}
		});

		var layoutModel = new Backbone.Model({
			date:date,
			side:"front"
		})
		App.page.show(new StarterBookingLayout({model:layoutModel,collection:groupedByTime,originalCollection:collection}));
	},
	report_edit: function (id) {
		if (id) {
			var page = new reporting_views_edit_reportLayout({id: id});
			App.page.show(page);
		} else {
			var page = new reporting_views_edit_reportListLayout();
			App.page.show(page);
		}
	},

	customers: function () {
        if(!this.check_access("customers")){
            return;
        }
		this.page = 'Customers';
		$('div.modal').modal('hide');
		var page = new CustomersIframeLayout();
		App.page.show(page);
	},
    integration:function(id){
        this.page = 'Integration';
        $('div.modal').modal('hide');
        var module = App.data.modules.findWhere({
        	module_id:"integration/"+id
		});

        var page = new IntegrationIframeLayout({
        	model:module
		});
        App.page.show(page);
	},
	items_old: function () {
		this.page = 'Items';
		$('div.modal').modal('hide'); 
		var page = new ItemsIframeLayout();
		App.page.show(page);
	},

	tee_sheet: function () {

        if(!this.check_access("teesheets")){
            return;
        }
		this.page = 'Tee Sheet';
		$('div.modal').modal('hide'); 
		var page = new TeeSheetIframeLayout();

		var stats = new TeesheetStats({
		});
		stats.fetch();

        App.nav.currentView.stats.empty();
        App.nav.currentView.stats.show(new TeesheetHeaderStatsView({model: stats}));
		App.page.show(page);
	},

    passes: function(){

        App.nav.currentView.stats.empty();
        if(!this.check_access("items")){
            return;
        }
        this.page = 'Passes';
        
        if(App.data.pass_items.length == 0){
			App.data.pass_items.fetch({
				data: {
					item_type: 'pass'
				}
			}); 	
        }

        var page = new PassLayout();
        App.page.show(page);
        page.table.show( new PassTableView({collection: App.data.passes}) );
    },

	item_kits: function(){
        App.nav.currentView.stats.empty();
        if(!this.check_access("items")){
            return;
        }
		this.page = 'Item Kits';
		var page = new ItemKitLayout();
		App.page.show(page);
		page.table.show( new ItemKitTableView({collection: App.data.item_kits}) );
	},


	items: function(){
        App.nav.currentView.stats.empty();
        if(!this.check_access("items")){
            return;
        }
		var page = new ItemLayout();
		App.page.show(page);
		page.table.show( new ItemTableView({collection: App.data.items}) );
	},

	billing: function(){
        App.nav.currentView.stats.empty();
		this.page = 'Billing';
		var page = new BillingLayout({
			statements: App.data.v2.statements,
			recurringCharges: App.data.recurring_charges,
			recurringStatements: App.data.v2.recurringStatements
		});
		App.page.show(page);
	},

    customers_new: function(){

        $('div.modal').modal('hide');
        App.nav.currentView.stats.empty();
        this.page = 'Customers';
        if(!this.check_access("customers")){
            return;
        }
        $("#page-iframe-container").hide();

        var page = new CustomersLayout({
            collection: App.data.v2.customers
        });

        App.page.show(page);
    },

    settings: function(section, subsection){
        App.nav.currentView.stats.empty();
        section = typeof section != 'undefined' ? section : 'businessDetails';
        subsection = typeof subsection != 'undefined' ? subsection : null;

        this.page = 'Settings';
        if(!this.check_access("config")){
            return;
        }
        $("#page-iframe-container").hide();

        var page = new SettingsLayout({
            model: App.data.v2.settings,
            sectionDefinitions: App.data.v2.settingsSections,
            section: section,
            subsection: subsection
        });

        App.page.show(page);
    },
    contact_forms: function () {
        App.nav.currentView.stats.empty();
		this.page = 'Contact Forms';
		var page = new ContactFormsLayoutView();
    	App.data.customer_groups.fetch();
		App.page.show(page);
    	App.data.contact_forms.fetch({
      		success: function () {
        		page.table.show(new ContactFormsTableView({collection: App.data.contact_forms}));
      		}
    	});
	},

    support_tools: function () {
        console.log('support tools')
        this.page = 'Support Tools';
        $('div.modal').modal('hide');
        var page = new support_tools_layout();
        App.page.show(page);

        page.left_nav.show(new support_tools_left_nav());
        page.disp_area.show(new support_tools_display_area());
    },

    bulk_edit_support: function () {
        console.log('bulk edit...')
        this.page = 'Bulk Edit Support Tool';
        $('div.modal').modal('hide');
        var page = new bulk_edit_support_layout();
        App.page.show(page);
        console.log('show left nav...');
        page.left_nav.show(new BulkEditSupportLeftNav());
    },

	sales: function() {
		if(!this.check_access("sales_v2")){
			return;
		}
		this.page = 'Sales';

		$('div.modal').modal('hide');
		var page = new SaleLayout();

		var terminal = App.data.course.get('terminals').getActiveTerminal();
		var terminal_name_view = new Backbone.View();
		if (terminal) {
			terminal_name_view = new TerminalHeaderView({collection: App.data.course.get('terminals')});
		}

		App.page.show(page);
		App.nav.currentView.header_content.show(terminal_name_view);
		App.nav.currentView.stats.show(new POSHeaderStatsView({model: App.data.course.get('pos_stats')}));
		
		page.cart_list.show(new CartView({model: App.data.cart, collection: App.data.cart.get('items')}));
		page.cart_totals.show(new CartTotalsView());
		page.sale_customers.show(new CartCustomersView());
		page.sale_payments.show(new SalePaymentsView());
		page.recent_transactions.show(new RecentTransactionListView({collection: App.data.recent_transactions}));
		page.suspended_sales.show(new SuspendedSalesListView({
			collection: App.data.suspended_sales,
			incomplete: App.data.incomplete_sales
		}));

		page.quick_buttons.show(new QuickButtonsView({collection: App.data.quick_buttons}));

		// Sides need to be loaded into page which are used by F&B items
		// sold through the POS
		if (App.data.food_bev_sides.length == 0) {
			App.data.food_bev_sides.fetch({
				reset: true, data: {
					food_and_beverage: 1,
					sides: 1
				}
			});
		}
	},

	sales_logout: function(){
		this.sales();
		$('#menu_logout').trigger('click');
	},

	food_and_beverage: function () {

        if(!this.check_access("food_and_beverage")){
            return;
        }
		this.page = 'Food & Beverage';
		$('div.modal').modal('hide');
		var page = new FoodBeverageLayout();
		var food_bev_header = new FbPageHeaderView({
			collection: App.data.course.get('terminals'),
			model: App.data.food_bev_table
		});
		
		App.page.show(page);
		App.nav.currentView.header_content.show(food_bev_header);
		App.nav.currentView.stats.empty();

		page.cart_totals.show(new FbCartTotalsView({collection: App.data.food_bev_table.get('cart')}));
		page.cart_list.show(new FbCartListView({collection: App.data.food_bev_table.get('cart')}));
		page.category_buttons.show(new CategoryButtonListView({collection: App.data.menu_buttons}));
		page.customers.show(new FbCustomerListView({collection: App.data.food_bev_table.get('receipts')}));
		page.guest_count.show(new GuestCountFieldView({model: App.data.food_bev_table}));

		// Lazy load all F&B items into page
		if (App.data.food_bev_items.length == 0) {
			App.data.food_bev_items.fetch({
				reset: true, 
				data: {
					food_and_beverage: 1,
				},
				success: function(){
					// Force buttons to re-render, using event listening for this causes an odd race condition
					// to occur where all items have not been added to the list when the event fires.
					// This causes the F&B category buttons to disappear
					page.category_buttons.currentView.render();
				}
			});
		}

		if (App.data.food_bev_sides.length == 0) {
			App.data.food_bev_sides.fetch({
				reset: true, data: {
					food_and_beverage: 1,
					sides: 1
				}
			});
		}

		// Load restaurant reservations
		if (App.data.restaurant_reservations.length == 0) {
			App.data.restaurant_reservations.fetch({
				reset: true, data: {
					date: moment().format('M/D/YYYY')
				}
			});
			App.data.restaurant_reservations.date = moment().format('M/D/YYYY');
		}

		// Load all F&B menu buttons
		if (App.data.menu_buttons.length == 0) {
			App.data.menu_buttons.fetch({
				reset: true
			});
		}

		App.data.food_bev_table.loadRecentTransactions();
	}
});

function displayError() {

} // JBDEBUG

var App = new Backbone.Marionette.Application();
App.options = {};
App.options.PING_INTERVAL = 60000;

App.addRegions({
	nav: '#main-nav',
	page: '#page'
});

App.addInitializer(function (options) {
	this.router = new Router();
	this.notify = new NotificationView();

	this.data = {};
	this.data.recent_transactions = new SaleCollection(RECENT_TRANSACTIONS);
	this.data.course = new Course(SETTINGS);
	this.data.user = new User(USER);
	this.data.cart = new Cart(CART);
	this.data.suspended_sales = new SuspendedSaleCollection(SUSPENDED_SALES);
	this.data.suspended_sales.status = 'suspended';
	this.data.incomplete_sales = new SuspendedSaleCollection(INCOMPLETE_SALES);
	this.data.incomplete_sales.status = 'incomplete';
	this.data.customers = new CustomerCollection();
	this.data.rainchecks = new RaincheckCollection();
	this.data.quick_buttons = new QuickButtonCollection(QUICK_BUTTONS);
	this.data.passes = new PassCollection();
	this.data.passes.url = API_URL + '/passes';
	this.data.minimum_charges = new MinimumChargeCollection(MINIMUM_CHARGES);
	this.data.data_import_jobs = new DataImportJobCollection(DATA_IMPORT_JOBS);
	this.data.item_receipt_content = new ItemReceiptContentCollection(ITEM_RECEIPT_CONTENT);
	this.data.recurring_charges = new RecurringChargeCollection();
	this.data.bulk_edit_jobs = new BulkEditJobCollection();

	this.data.v2 = {};
	this.data.v2.customers = new CustomerRestModelCollection();
	this.data.v2.customers.setSorting("last_name");
	this.data.v2.statements = new StatementCollection();
    this.data.v2.settings = new SettingsRestModel();
    this.data.v2.settingsSections = SettingsRestModelSections;
	this.data.v2.recurringStatements = new RecurringStatementCollection();
    this.data.v2.price_classes = new PriceClassCollection();
    this.data.v2.teesheets = new TeesheetCollectionV2();

	this.data.pass_items = new ItemCollection();
	this.data.pass_items.parse = function(data){
		return data.rows;
	}
	
	if(this.data.user.get('user_level')>=5){
		this.data.employee_audit_log = new EmployeeAuditLogCollection();
		this.data.employee_audit_log.model = EmployeeAuditLog;
		this.data.marketing_campaigns = new MarketingCampaignsCollection();
	}

	this.data.item_kits = new ItemKitCollection();
	this.data.items = new ItemCollection();
	this.data.suppliers = new SupplierCollection(SUPPLIERS);
	this.data.contact_forms = new ContactFormCollection();
  	this.data.customer_groups = new CustomerGroupCollection();
  	this.data.seasons = new SeasonCollection(SEASONS);
	this.data.food_bev_tables = new FbTableCollection();
	this.data.food_bev_table = new FbTable();
	this.data.food_bev_items = new ItemCollection(FOOD_BEV_ITEMS);
	this.data.food_bev_sides = new ItemCollection(FOOD_BEV_SIDES);
	this.data.menu_buttons = new MenuButtonCollection();

	this.data.food_bev_table_layouts = new TableLayoutCollection();
	this.data.food_bev_recent_transactions = new SaleCollection();
	this.data.restaurant_reservations = new RestaurantReservationCollection();
	this.data.modules = new ModuleCollection(SETTINGS.modules);
	this.data.partnerModules = new ModuleCollection(SETTINGS.partnerModules);
	this.data.register_logs = new RegisterLogCollection(REGISTER_LOGS);
	this.data.print_queue = new PrintQueue();

	var header_layout = new HeaderLayout();
	App.nav.show(header_layout);


	header_layout.application_menu.show(new HeaderApplicationMenu());
	header_layout.user_menu.show(new HeaderUserMenu());
});

App.on('start', function () {

	if (Backbone.history) {
		Backbone.history.start({pushState: false, root: BASE_URL + COURSE_ID});
	}

	var receipt_ip = App.data.course.get_setting('receipt_ip');
	var cold_receipt_ip = App.data.course.get_setting('cold_webprnt_ip');
	var hot_receipt_ip = App.data.course.get_setting('hot_webprnt_ip');
	var course = this.data.course;

	// Begin polling receipt printer queue
	// TODO: DEV-1347
	if (this.data.course.get('multiple_printers') == 1) {
		setInterval(function () {
			App.data.print_queue.print();
		}, 3000);

	} else {
		setInterval(function () {
			if (receipt_ip != '' && receipt_ip != 0) {
				webprnt.print_all(window.location.protocol + "//" + receipt_ip + "/StarWebPRNT/SendMessage");
			}
			if (cold_receipt_ip != '' && cold_receipt_ip != 0) {
				webprnt.print_all(window.location.protocol + "//" + '_cold_' + cold_receipt_ip + "/StarWebPRNT/SendMessage");
			}
			if (hot_receipt_ip != '' && hot_receipt_ip != 0) {
				webprnt.print_all(window.location.protocol + "//" + '_hot_' + hot_receipt_ip + "/StarWebPRNT/SendMessage");
			}
		}, 3000);
	}

	setInterval(function () {
		App.vent.trigger('ping');
	}, App.getOption('PING_INTERVAL'));
});

$(document).on('keydown', function (event) {

	// ALT + G to show giftcard lookup window
	if (event.altKey === true && (event.keyCode == 71 || event.keyCode == 103)) {
		var card_balance_window = new CardBalanceCheckWindow();
		card_balance_window.show();
	}

	// ALT + T to show terminal select window
	if (event.altKey === true && (event.keyCode == 84 || event.keyCode == 116)) {
		var terminal_select = new TerminalSelectWindow({collection: App.data.course.get('terminals')});
		terminal_select.show();
	}
});

App.errorHandler = function(object, response, options){

	var code = 0;
	if(response && response.code){
		code = response.code;
	}else if(response && response.status){
		code = response.status;
	}

	if(response && response.responseJSON && response.responseJSON.msg && code >= 400){
		App.vent.trigger('notification', {'type':'error', 'msg':response.responseJSON.msg});
	
	}else if(code >= 400){
		App.vent.trigger('notification', {'type':'error', 'msg':'An error has occurred, please try again'});

		if(typeof NREUM != 'undefined'){
			var extra = '';
			if(object && typeof(object) == 'object'){
				extra = JSON.stringify(object);
			}
			var newRelicMessage = 'Backbone API error (without message) [HTTP CODE:'+code+'] [URL:'+window.location.href+'] ['+extra+']';
			NREUM.noticeError(newRelicMessage);			
		}

	// If a model failed front end validation
	}else if(typeof(response) == 'string'){
		App.vent.trigger('notification', {'type':'error', 'msg':response});
	}
};

// Displays a notification to the user
App.vent.on('notification', function (data) {
	var delay = 4000;
	if (!data.type) {
		data.type = 'info';
	} else if (data.type == 'error') {
		data.type = 'danger';
	}

	if (!data.msg) {
		return false;
	}
	if (typeof(data.delay) != 'undefined') {
		delay = data.delay;
	}

	$.bootstrapGrowl(data.msg, {type: data.type, width: 'auto', align: 'center', delay: delay});
});

App.start();