<?php
// application/hooks/DisplayHook.php
class RedirectSSL {

    public function __construct()
    {
        $this->CI = & get_instance();
    }

    // NEW METHOD FOR SSL REDIRECTION.
    public function redirect_ssl() {

        //Browser will use https, regardless
        if($this->CI->session->userdata('terminal_https')==='1'){
            header('Strict-Transport-Security: max-age=31536000');
            return;
        }else
            header('Strict-Transport-Security: max-age=0');

        // Check if enabled
        if(!(isset($this->CI->config->config['ssl_redirect_enable']) && $this->CI->config->config['ssl_redirect_enable'] === true)) return;

        $mthd = $this->CI->router->fetch_method();
        $cls = $this->CI->router->fetch_class();
        $dir = $this->CI->router->fetch_directory();

        if($mthd==='logout' || $mthd===''){
            // v2 ssl exclusion is processed before home class inclusion
            // this is a dirty hack to get around the bad ordering.
            if (!$this->isConnectionSSL()) {
                $this->enforce_ssl();
                return;
            } else {
                return; // Already using SSL
            }
        }

        // Process Explicit Skip Lists First (For ajax pages that need to be more dynamic, Specifically timeclock AJAX pop-up)
        if($this->CI->config->config['ssl_skip_on_XMLHttpRequest'] === true &&
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'){
            // Do nothing.
            return;
        }
        if( in_array($cls, $this->CI->config->config['ssl_skip_class_list'])
            ||
            in_array($dir, $this->CI->config->config['ssl_skip_directory_list'])
            ||
            in_array($mthd, $this->CI->config->config['ssl_skip_method_list']) )
        {
            // Do nothing.
            return;
        }


        // Process Explicit Exclusion Lists Second
        if( in_array($cls, $this->CI->config->config['ssl_exclude_class_list'])
            ||
            in_array($dir, $this->CI->config->config['ssl_exclude_directory_list'])
            ||
            in_array($mthd, $this->CI->config->config['ssl_exclude_method_list']) )
        {
            header('Strict-Transport-Security: max-age=0');
            // Redirect to non-ssl portion of site.
            if ($this->isConnectionSSL())
            {
                $this->enforce_non_ssl();
                return;
            } else {
                return; // Already NOT using SSL
            }
        }

        // Process Inclusion Lists Third
        if( in_array($cls, $this->CI->config->config['ssl_include_class_list'])
            ||
            in_array($dir, $this->CI->config->config['ssl_include_directory_list'])
            ||
            in_array($mthd, $this->CI->config->config['ssl_include_method_list']) ) {
            // Redirect to ssl-enforced portion of site.
            if (!$this->isConnectionSSL()) {
                $this->enforce_ssl();
                return;
            } else {
                return; // Already using SSL
            }
        }

        // Redirect to non-ssl portion of site by default.
        if ($this->isConnectionSSL())
        {
            $this->enforce_non_ssl();
            return;
        } else {
            return; // Already NOT using SSL
        }

    }

    private function isConnectionSSL(){
        if (
            (
                isset($_SERVER['HTTPS']) &&
                ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1)
            )
            ||
            (
                isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
                $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
            )
        ) {
            return true;
        }

        return false;
    }

    private function enforce_ssl(){
        if(!headers_sent() && $this->CI->config->config['base_url_ssl_redirect'] != '')
        {
            header('Location: ' . $this->CI->config->config['base_url_ssl_redirect'] . $_SERVER['REQUEST_URI']);
            exit();
        }
    }

    private function enforce_non_ssl(){
        if(!headers_sent() && $this->CI->config->config['base_url_nonssl_redirect'] != '')
        {
            header('Location: ' . $this->CI->config->config['base_url_nonssl_redirect'] . $_SERVER['REQUEST_URI']);
            exit();
        }
    }

}
