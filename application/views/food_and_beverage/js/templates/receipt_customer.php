<script type="text/html" id="template_receipt_customer">
<img class="photo" src="<%-photo %>" />
<section>	
	<h3 class="name"><%-first_name %> <%-last_name %></h3>
	<span class="info"><%-phone%></span>
	<span class="info"><%-email%></span>
</section>
<section style="width: 225px;">
	<span class="info">
		<strong>Acct #</strong> 
		<span class="balance">
			<% if(account_number){ 
			print(account_number);
		}else{ %>
			<span style="color: #AAA;">n/a</span>
		<% } %>
		</span>
	</span>	
	<span class="info">
		<strong><%=App.member_account_name %></strong> 
		<span class="balance <% if(member_account_balance < 0){ print('negative'); } %>"><%-accounting.formatMoney(member_account_balance) %></span>
	</span>
	<span class="info">
		<strong><%=App.customer_account_name %></strong> 
		<span class="balance <% if(account_balance < 0){ print('negative'); } %>"><%-accounting.formatMoney(account_balance) %></span>
	</span>
</section>
</script>

<script type="text/html" id="template_receipt_customer_search">
<div style="display: block; overflow: hidden; height: 50px; padding: 15px;">
	<input style="float: left; margin: 0px;" type="text" name="search" class="search" value="" placeholder="Search customers..." />
	<img class="search-loading" src="<?php echo base_url('images/spinner_small.gif'); ?>" style="display: none;" />
	<a class="fnb_button no-customer" style="float: right; display: block; margin: 0px;">- NO CUSTOMER -</a>	
</div>			
<div class="customer-list" style="padding: 0px 0px 15px 0px;">
	<h5 class="empty">Search customers...</h5>
</div>	
</script>
