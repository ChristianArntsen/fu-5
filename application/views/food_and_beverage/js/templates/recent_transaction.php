<script type="text/html" id="template_recent_transaction">
<button class="fnb_button view_receipt" style="float: left; width: 90px; margin-right: 10px;">View Receipt</button>
<button class="fnb_button print_receipt" style="float: left; width: 90px; margin-right: 10px;">Print Receipt</button>

<div class="info" style="width: 250px; float: left;">
	<h3>Sale #<%-sale_id%></h3>
	<h5>Total: <%-accounting.formatMoney(total)%></h5>
	<% if(tips.length > 0){ %>
	<strong>Tips</strong>
	<ul>
	<% _.each(tips.models, function(tip){ %>
		<li><%-tip.get('payment_type')%>: <%-accounting.formatMoney(tip.get('amount'))%></li>
	<% }) %>
	</ul>
	<% } %>
</div>
<div class="info" style="width: 250px; float: left;">
	<div><span class="label">Server:</span> <strong><%-employee_name%></strong></div>
	<% if(customer_id){ %><div><span class="label">Customer:</span> <strong><%-customer_name%></strong></div><% } %>
	<div><span class="label">Table #</span> <strong><%-table_number%></strong></div>
</div>
<button class="fnb_button add_tip" style="float: right; width: 90px">Add/Edit Tip</button>
</script>
