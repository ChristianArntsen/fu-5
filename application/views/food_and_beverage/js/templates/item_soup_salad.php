<script type="text/html" id="template_item_soup_salad">
<%
var position = 1;

if(soups.length > number_soups){
	number_soups = soups.length;
}
if(number_soups > 0){ %>
<div class="side">
	<h1>Soup</h1>
	<ul style="overflow: hidden; display: block;">
	<% _.each(App.sides.models, function(soup){
	if(soup.get('category') != 'Soups'){ return true; }
	var selected = '';
	if(soups.length > 0 && soups.get(position) && soups.get(position).get('item_id') == soup.get('item_id')){
		selected = ' selected';
	} %>
		<li>
			<a class="fnb_button side<%-selected%>" href="#" data-type="soup" data-position="<%-position%>" data-side-id="<%-soup.get('item_id')%>">
				<%-_.capitalize(soup.get('name'))%>
			</a>
		</li>
	<% }); %>
	</ul>
	<% var noSide = '';
	if(soups.length > 0 && soups.get(position) && soups.get(position).get('item_id') == 0){
		noSide = ' selected';
	} %>
	<a style="margin-top: 80px; clear: both;" data-type="soup" data-position="<%-position%>" class="no_side fnb_button<%-noSide%>" href="#">No Soup</a>
</div>
<% } %>

<%
if(salads.length > number_salads){
	number_salads = salads.length;
}
if(number_salads > 0){ %>
<div class="side">
	<h1>Salad</h1>
	<ul style="overflow: hidden; display: block;">
	<% _.each(App.sides.models, function(salad){
	if(salad.get('category') != 'Salads'){ return true; }
	var selected = '';
	if(salads.length > 0 && salads.get(position) && salads.get(position).get('item_id') == salad.get('item_id')){
		selected = ' selected';
	} %>
		<li>
			<a class="fnb_button side<%-selected%>" href="#" data-type="salad" data-position="<%-position%>" data-side-id="<%-salad.get('item_id')%>">
				<%-_.capitalize(salad.get('name'))%>
			</a>
		</li>
	<% }); %>
	</ul>
	<% var noSide = '';
	if(salads.length > 0 && salads.get(position) && salads.get(position).get('item_id') == 0){
		noSide = ' selected';
	} %>
	<a style="margin-top: 80px; clear: both;" data-type="salad" data-position="<%-position%>" class="no_side fnb_button<%-noSide%>" href="#">No Salad</a>
</div>
<% } %>
</script>