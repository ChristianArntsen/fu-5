var PaymentWindowView = Backbone.View.extend({
	tagName: "div",
	id: "payment_window",
	template: _.template( $('#template_payment_window').html() ),

	initialize: function() {
		this.listenTo(this.collection, "add remove change", this.render);
		this.listenTo(this.collection, "invalid", this.displayError);
	},

	events: {
		"click #use_cash": "payCash",
		"click #use_check": "payCheck",
		"click a.show_payment_buttons": "showPaymentTypes",

		"click #use_gift_card": "showGiftCard",
		"click #use_punch_card": "showPunchCard",
		"click #use_credit_card": "showCreditCard",
		"click #use_customer_account": "payCustomerAccount",
		"click #use_member_account": "payMemberAccount",

		"click #pay_check_number": "payByCheck",
		"click #pay_gift_card": "payGiftCard",
		"click a.close": "closeWindow",

		"keydown #payment_check_number": "checkNumber"
	},

	render: function() {
		this.$el.html('');

		var data = {};
		data.amount_due = this.model.getTotalDue();
		data.enable_member_balance = false;
		data.enable_customer_balance = false;
		
		if(this.model.get('customer')){
			if(this.model.get('customer').hasMemberBalance()){
				data.enable_member_balance = true;
			}			
			if(this.model.get('customer').hasCustomerBalance()){
				data.enable_customer_balance = true;
			}		
		}

		this.$el.html(this.template(data));
		this.$el.find('#payment_amount_tendered').keypad({position: 'bottom-right'});
		this.renderPayments();

		return this;
	},

	checkNumber:function(e){
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    	}
	},

	displayError: function(model){
		$('#cbox2LoadedContent').unmask();
		set_feedback(model.validationError, 'error_message');
	},

	showPaymentTypes: function(){
		this.$el.find('#payment_buttons').show().siblings().hide();
		this.$el.find('div.left').removeClass('max');
		this.$el.find('div.payment-amount').show();
		return false;
	},

	renderPayments: function(){
		this.$el.find('ul.payments').html('');
		_.each(this.collection.models, this.addPayment, this);
		return this;
	},

	showChangeDue: function(change_due){
		var template = _.template( $('#template_change_issued').html() );
		var data = {};
		data.change_due = change_due;
		this.$el.html(template(data));
		var view = this;
		
		$(document).one('cbox2_closed', function(e){
			view.closeWindow();
		});
		
		return this;
	},

	addPayment: function(payment){
		payment.set({'receipt_id': this.model.get('receipt_id')}, {'silent':true});
		this.$el.find('ul.payments').append( new PaymentView({model:payment}).render().el );
	},

	getPaymentData: function(){
		var amount = this.$el.find('#payment_amount_tendered').val();
		return {"amount":amount};
	},

	unmarkCartItems: function(){

	},

	showPunchCard: function(event){
		event.preventDefault();
		return false;
	},

	showGiftCard: function(event){
		
		if(App.use_ets_giftcards == 1){
			var data = this.getPaymentData();
			data.receipt_id = this.model.get('receipt_id');
			data.ets_type = 'giftcard';
			data.amount = parseFloat(accounting.unformat(data.amount));

			var giftCardWindow = this.$el.find('#payment_ets_gift_card');
			giftCardWindow.html('');
			giftCardWindow.show().siblings().hide();
			giftCardWindow.parent('div.left').addClass('max');
			this.$el.find('div.payment-amount').hide();

			// Open credit card processing window
			$('#cbox2LoadedContent').mask('<?php echo lang('common_wait'); ?>');
			$.post(SITE_URL + '/food_and_beverage/credit_card_window', data, function(response){
				giftCardWindow.html(response);
			},'html');	
			
			this.$el.find('#payment_ets_gift_card').show().siblings().hide();
			event.preventDefault();
		
		}else{
			this.$el.find('#payment_gift_card').show().siblings().hide();
			event.preventDefault();			
		    $('#punch_card_number').swipeable({allow_enter:true, character_limit:20});
			$('#payment_giftcard_number').swipeable({allow_enter:true, character_limit:20});
		}
	},

	showCreditCard: function(event){
		
		// If shift key is pressed, perform basic Credit Card payment
		if((!App.ets_credit_cards && !App.mercury_credit_cards) || event.shiftKey){
			var data = this.getPaymentData();
			data.type = "Credit Card";
			data.amount = parseFloat(accounting.unformat(data.amount));
			var self = this;

			if(data.amount == ''){
				return false;
			}

			this.collection.create(data, {'success': function(){
				self.closePayments();
			}});
			return false;
		}

		var data = this.getPaymentData();
		data.receipt_id = this.model.get('receipt_id');
		data.ets_type = 'credit card';
		
		if(data.amount == ''){
			return false;
		}		
		
		data.amount = parseFloat(accounting.unformat(data.amount));

		var creditCardWindow = this.$el.find('#payment_credit_card');
		creditCardWindow.html('');
		creditCardWindow.show().siblings().hide();
		creditCardWindow.parent('div.left').addClass('max');
		this.$el.find('div.payment-amount').hide();

		// Open credit card processing window
		$('#cbox2LoadedContent').mask('<?php echo lang('common_wait'); ?>');
		$.post(SITE_URL + '/food_and_beverage/credit_card_window', data, function(response){
			creditCardWindow.html(response);
		},'html');

		event.preventDefault();
	},

	payCash: function(event){
		var data = this.getPaymentData();
		data.type = "cash";
		var self = this;
		
		if(data.amount == ''){
			return false;
		}
		
		$('#cbox2LoadedContent').mask('Please Wait...');
		data.amount = parseFloat(accounting.unformat(data.amount));
		
		this.collection.create(data, {'wait': true, 'success': function(){
			$('#cbox2LoadedContent').unmask();
			self.closePayments();
		}});
		event.preventDefault();
	},

	payCheck: function(event){
		this.$el.find('#payment_check').show().siblings().hide();
		event.preventDefault();	
	},

	payMemberAccount: function(event){
		var customer_id = this.model.get('customer').id;
		var data = this.getPaymentData();
		data.type = "member_balance";
		data.customer_id = customer_id;
		
		if(data.amount == ''){
			return false;
		}		
		
		var self = this;
		$('#cbox2LoadedContent').mask('Please Wait...');
		data.amount = parseFloat(accounting.unformat(data.amount));		

		this.collection.create(data, {'wait': true, 'success':function(){
			$('#cbox2LoadedContent').unmask();
			self.closePayments();
		}});
		event.preventDefault();
	},
	payCustomerAccount: function(event){
		var customer_id = this.model.get('customer').id;
		var data = this.getPaymentData();
		data.type = "account_balance";
		data.customer_id = customer_id;
		
		if(data.amount == ''){
			return false;
		}		
		
		var self = this;
		$('#cbox2LoadedContent').mask('Please Wait...');
		data.amount = parseFloat(accounting.unformat(data.amount));		

		this.collection.create(data, {'wait': true, 'success':function(){
			$('#cbox2LoadedContent').unmask();
			self.closePayments();
		}});
		event.preventDefault();
	},
	payGiftCard: function(){
		var data = this.getPaymentData();
		data.type = "gift card";
		data.card_number = this.$el.find('#payment_giftcard_number').val();
		
		if(data.amount == ''){
			return false;
		}		
		
		var self = this;
		$('#cbox2LoadedContent').mask('Please Wait...');
		data.amount = parseFloat(accounting.unformat(data.amount));		

		this.collection.create(data, {'wait': true, 'success':function(){
			$('#cbox2LoadedContent').unmask();
			self.closePayments();
		}});
		event.preventDefault();
	},

	payByCheck: function(event){
		var data = this.getPaymentData();
		data.type = "check";
		data.card_number = this.$el.find('#payment_check_number').val();
		if(data.amount == ''){
			return false;
		}		
		
		var self = this;
		$('#cbox2LoadedContent').mask('Please Wait...');
		data.amount = parseFloat(accounting.unformat(data.amount));		

		this.collection.create(data, {'wait': true, 'success':function(){
			$('#cbox2LoadedContent').unmask();
			self.closePayments();
		}});
		event.preventDefault();
	},

	closePayments: function(){
		if(this.model.isPaid()){
			var totalDue = this.model.getTotalDue();

			// If change is due, show change due window
			if(totalDue < 0){
				this.model.printReceipt('sale');
				this.showChangeDue(totalDue);
			}else{
				this.model.printReceipt('sale');
				this.closeWindow();
			}
		}
	},

	closeWindow: function(event){
		if(event){
			event.preventDefault();
		}	
			
		$(document).off('cbox2_closed', function(e){
			view.closeWindow();
		});		
		$.colorbox2.close();
		this.remove();
		App.closeTable();
	}
});
