var RecentTransactionDetailsView = Backbone.View.extend({
	tagName: "li",
	template: _.template( $('#template_recent_transaction_details').html() ),

	events: {
		"click button.close": "close"
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		return this;
	}
});
