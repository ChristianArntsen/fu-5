var AuthorizeWindowView = Backbone.View.extend({
	
	tagName: "div",
	id: "authorize_window",
	template: _.template( $('#template_authorize').html() ),

	initialize: function(options) {
		if(options && options.action){
			this.action = options.action;
		}
	},

	events: {
		"click a.close": "closeWindow",
		"click div.number_pad a": "numberPress",
		"keypress input.pin": "submitForm",
	},

	render: function() {	
		var attr = {};
		if(this.action){
			attr.action = this.action;
		}
		
		this.$el.html(this.template(attr));
		this.$el.find('#login_keypad .pin').swipeable({allow_enter:true, character_limit:20});
		return this;
	},
	
	submitForm: function(e){
		if(e.keyCode == 13){
			e.preventDefault();
			this.submit();
		}
		return true;
	},
	
	numberPress: function(e){
		var value = $(e.currentTarget).data('char');
		var field = this.$el.find('input.pin');
		
		if(value == 'del'){
			field.val( field.val().substr(0, field.val().length - 1) );
		}else if(value == 'enter'){
			this.submit();
		}else{
			field.val( field.val() + '' + value);
		}
	},
	
	closeWindow: function(e){
		$(document).off('cbox2_closed', function(e){
			view.closeWindow();
		});		
		$.colorbox2.close();
		this.remove();
		
		if(e){
			e.preventDefault();
		}
	},
	
	submit: function(){
		var pin = this.$el.find('input.pin').val().replace('%','!');
		var view = this;
		
		if(this.action == 'Login'){	
			$.post('index.php/food_and_beverage/login', {'pin_or_card': pin}, function(response){				
				if(!response.success){
					set_feedback('Invalid login', 'error_message', false, 2500);
					view.$el.find('input.pin').val('');
					return true;					
				}
				view.completeAction(response);
			
			},'json').fail(function(){
				set_feedback('Invalid login', 'error_message', false, 2500);
			});	
		
		}else{		
			$.post(App.api + 'authorize', {'pin': pin}, function(response){
				if(response.success){
					view.employee_id = response.employee_id;
					view.completeAction();
				}
			},'json').fail(function(){
				set_feedback('Invalid login', 'error_message', false, 2500);
			});
		}
	},
	
	completeAction: function(response){
		
		if(this.action == 'Cancel Sale'){
			if(!App.closeTable(false)){
				set_feedback('Please finish paying receipts or refund payments to cancel sale', 'error_message', 5000);
			}
		}
		
		if(this.action == 'Admin Override'){
			App.admin_auth_id = this.employee_id;
			App.receipts.trigger('admin');
		}
		
		if(this.action == 'Login'){
			var employee_name = response.employee.last_name +', '+  response.employee.first_name;
			if(App.receipt.hide_employee_last_name_receipt == "1"){
				var employee_name = response.employee.first_name;
			}
			
			$('#user_button .menu_title').html(response.employee.first_name + ' ' + response.employee.last_name);
			$('#user_button').show();

			App.receipt.header.employee_name = employee_name;
			App.employee_id = response.employee.person_id;
			App.employee_level =  response.employee.user_level;
			App.recent_transactions.reset(response.recent_transactions);
			App.register_log = new RegisterLog(response.register_log);
			App.admin_auth_id = false;

			// Show register log window, if no log has been set yet
			if(App.track_cash != 0 && App.register_log && !App.register_log.get('register_log_id')){
				
				this.closeWindow();
				
				var registerLog = new RegisterLogView({model: App.register_log});
				$.colorbox({
					title: 'Open Register',
					html: registerLog.render().el,
					width: 600,
					height: 400
				});

			// Show table select window
			}else{
				reload_tables(function(response){
					fnb.show_tables();
				});
			}
		}		
		this.closeWindow();
	}
});
