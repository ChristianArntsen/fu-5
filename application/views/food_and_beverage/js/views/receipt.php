var ReceiptView = Backbone.View.extend({
	
	tagName: "div",
	className: "receipt-container",
	template: _.template( $('#template_receipt').html() ),

	events: {
		"click a.pay": "payReceipt",
		"click a.print": "printReceipt",
		"click a.delete-receipt": "deleteReceipt",
		"click div.receipt": "receiptClick",
		"click a.tax": "changeTaxable",
		"click a.up": "scrollItemsUp",
		"click a.down": "scrollItemsDown",
		"click a.customer": "attachCustomer",
		"keypad_close input.fnb_gratuity": "updateGratuity"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model.get('items'), "add remove reset", this.render);
	},

	render: function() {
		this.$el.html( this.template(this.model.attributes) );
		this.$el.find('div.title').after( new ReceiptItemListView({collection:this.model.get('items')}).render().el );
		this.$el.find('.fnb_gratuity').keypad({position: 'right', closeOnClick: false});
		var receipt = this.$el;
		this.$el.find('.fnb_gratuity').on('focus', function(){
			receipt.find('a.pay, a.print').addClass('disabled');
		});
		this.renderStatus();
		return this;
	},

	renderStatus: function(){
		if(this.model.get('items').length == 0){
			this.$el.find('a.pay, a.print').addClass('disabled');

		}else if(this.model.get('status') != 'complete'){
			this.$el.find('a.pay, a.print').removeClass('disabled');

		}else{
			this.$el.find('a.print').removeClass('disabled');
		}
	},

	payReceipt: function(event){
		if ($('div#jquery_keypad').is(':visible')){
			alert('Please save gratuity in order to proceed.');
			return false;
		}

		if(this.model.get('items').length == 0 || this.model.get('status') == 'complete'){
			return false;
		}
		var receiptModel = this.model;
		this.model.calculateTotals();

		var payments = this.model.get('payments');
		var paymentWindow = new PaymentWindowView({collection: payments, model: receiptModel});

		$.colorbox2({
			title: 'Add Payment',
			html: paymentWindow.render().el,
			width: 700
		});

		if(event){
			event.preventDefault();
		}
	},
	
	attachCustomer: function(event){
		event.preventDefault();
		var receiptModel = this.model;
		var customerSearch = new ReceiptCustomerSearchView({receipt: receiptModel});
		
		$.colorbox2({
			title: 'Attach Customer',
			html: customerSearch.render().el,
			width: 665,
			height: 500
		});
		return false;
	},
	
	printReceipt: function(event){
		if ($('div#jquery_keypad').is(':visible')){
			alert('Please save gratuity in order to proceed.');
			return false;
		}
		this.model.printReceipt('pre-sale');

		if(event){
			event.preventDefault();
		}
	},
	
	changeTaxable: function(event){
		if(event){
			event.preventDefault();
		}
		
		var tax_btn = this.$el.find('a.tax');
		var new_status = 1 - tax_btn.data('tax');
		tax_btn.data('tax', new_status);
		
		if(new_status == 1){
			tax_btn.html('&#10004; Tax');
		}else{
			tax_btn.html('&#x25a2; Tax');
		}
		
		this.model.set('taxable', new_status);
		this.model.save();
		return false;
	},

	deleteReceipt: function(event){
		if(this.model.get('receipt_id') == 1){
			return false;
		}

		var splitItems = this.model.collection.getSplitItems();
		var items = this.model.get('items').models;
		var deleteReceipt = true;

		// Make sure items in receipt to be deleted exist on other receipts
		_.each(items, function(item){
			var line = item.get('line');
			if(splitItems[line] <= 1){
				deleteReceipt = false;
			}
		});

		if(deleteReceipt){
			this.model.destroy();
			App.receipts.calculateAllTotals();
		}
		return false;
	},
	
	scrollItemsUp: function(event){
		var list = this.$el.find('div.receipt-items');
		var curPos = list.scrollTop();
		
		this.$el.find('div.receipt-items').scrollTop(curPos - 47);
		return false;
	},
	
	scrollItemsDown: function(event){
		var list = this.$el.find('div.receipt-items');
		var curPos = list.scrollTop();
		
		this.$el.find('div.receipt-items').scrollTop(curPos + 47);	
		return false;	
	},
	
	updateGratuity: function(event){
		var auto_gratuity = this.$el.find('input.fnb_gratuity').val();
		this.model.set({'auto_gratuity': auto_gratuity});
		this.model.calculateTotals();
		this.model.save();
		// SHOW PRINT AND PAY BUTTONS
		this.$el.find('a.pay, a.print').removeClass('disabled');
	},
	
	receiptClick: function(event){
		
		// Ignore click when attempting to change gratuity
		if( $(event.srcElement).hasClass('fnb_gratuity') ){
			return true;
		}
		
		var mode = $('#split_payments').find('div.btn-group:visible').data('mode');		
		if(mode == 'move' || mode == 'split'){
			this.moveItems(event);
		}
		
		if(mode == 'comp'){
			this.compItems(event);
		}
	},
	
	compItems: function(event){
		var receipt = this.model;
		var compWindow = new CompWindowView({model: receipt});
		$.colorbox2({
			title: 'Comp Receipt',
			html: compWindow.render().el
		});	
	},
	
	moveItems: function(event){
		
		// If this receipt has already been paid, do nothing
		if(this.model.get('status') == 'complete'){
			App.receipts.unSelectItems();
			return false;
		}

		var selectedItems = App.receipts.getSelectedItems();
		var items = [];
		var mode = $('#split_payments').find('div.btn-group:visible').data('mode');

		if(selectedItems.length == 0){
			return false;
		}
		var targetReceiptItems = this.model.get('items');

		// Add selected items to target receipt.
		_.each(selectedItems, function(selectedItem){

			// Make sure items aren't being moved back onto same receipt
			if(selectedItem.collection != targetReceiptItems){
				
				// Copy the item including the nested collections
				var itemCopy = _.clone(selectedItem.attributes);
				itemCopy.sides = itemCopy.sides.toJSON();
				itemCopy.soups = itemCopy.soups.toJSON();
				itemCopy.salads = itemCopy.salads.toJSON();
				itemCopy.modifiers = itemCopy.modifiers.toJSON();
				
				targetReceiptItems.create(itemCopy, {complete: function(response){
					// If moving item, delete it from other receipt. Delete
					// call is made when move call has completed to prevent errors
					if(mode == 'move'){
						selectedItem.destroy();
					}
					
					_.each(App.receipts.models, function(receipt){
						receipt.calculateTotals();
					});
					
				}, isNew: true});

				// TODO: Hide the selected item so there is no delay
				// waiting for requests to complete
				if(mode == 'move'){

				}
			}
		});

		App.receipts.unSelectItems();
	}
});
