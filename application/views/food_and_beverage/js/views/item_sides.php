var ItemSidesView = Backbone.View.extend({
	tagName: "div",
	className: "sides",

	initialize: function(attributes, options){
		this.options = {};
		this.options.section_id = attributes.section_id;
		this.options.category_id = attributes.category_id;
		this.options.number_soups = attributes.number_soups;
		this.options.number_salads = attributes.number_salads;
		this.options.number_sides = attributes.number_sides;
	},

	events: {
		"click a.side": "selectSide",
		"click a.no_side": "removeSide",
		"click a.back": "back",
		"click a.side_modifiers": "editModifiers"
	},

	render: function() {
		data = this.model.attributes;
		data.category_id = this.options.category_id;
		if(!data.hidden_sides){
			data.hidden_sides = [];
		}

		// If section is 'Soups/Salads'
		if(data.category_id == 4){
			this.template = _.template( $('#template_item_soup_salad').html() );

		// Otherwise section is just 'Sides'
		}else{
			this.template = _.template( $('#template_item_sides').html() );
		}

		this.$el.html(this.template(data));
		return this;
	},

	editModifiers: function(event){
		var sidePosition = $(event.currentTarget).data('position');

		// Get side
		var side = this.model.get('sides').get(sidePosition);
		var title = side.get('name') + ' > Modifiers';

		// Remove current sides window for now
		this.remove();

		// Show side modifiers window
		var view = new ItemModifiersView({model: side, section_id: this.options.section_id, title: title});
		$('#item-edit-' + this.options.section_id).html( view.render().el );
		$.colorbox.resize();

		event.preventDefault();
	},

	selectSide: function(event, sideType){
		var button = $(event.currentTarget);

		// Mark button as 'selected'
		button.parents('div.side').find('a').removeClass('selected');
		button.addClass('selected');
		var side_id = button.data('side-id');
		var position = button.data('position');
		var type = button.data('type');

		// Retrieve the selected side and assign it the proper side 'position'
		var side = App.sides.get(side_id);
		var attributes = _.clone(side.attributes);
		attributes.modifiers = attributes.modifiers.toJSON();

		var price = attributes.add_on_price;	
		if(this.model.get('side_prices')){
			var sidePrices = this.model.get('side_prices');

			if(typeof(sidePrices[attributes.item_id]) !== undefined && sidePrices[attributes.item_id] !== ''){
				price = sidePrices[attributes.item_id];
				if(isNaN(price)){
					price = 0;
				}
			}
		}
		attributes.position = position;
		attributes.price = price;

		var itemSide = new ItemSide(attributes);
		itemSide.calculatePrice();

		// Add the side to item
		if(type == 'side'){
			var sideCollection = this.model.get('sides');
		}else if(type == 'salad'){
			var sideCollection = this.model.get('salads');
		}else if(type == 'soup'){
			var sideCollection = this.model.get('soups');
		}

		if(sideCollection){
			sideCollection.add(itemSide, {merge: true});
		}

		// Show/hide side modifiers button
		var sideModifiersButton = this.$el.find('a.side_modifiers[data-position="' + position + '"]');
		if(itemSide.get('modifiers').length > 0){
			sideModifiersButton.show();
		}else{
			sideModifiersButton.hide();
		}

		return false;
	},

	removeSide: function(event){
		var button = $(event.currentTarget);

		// Mark button as 'selected'
		button.parents('div.side').find('a').removeClass('selected');
		button.addClass('selected');
		var position = button.data('position');
		var type = button.data('type');

		// Create a new side model that represents 'no side'
		var side = new ItemSide({'position':position, 'item_id':0, 'tax':0, 'total':0, 'name':'None', 'price':0});

		// Get appripriate collection to modify
		if(type == 'side'){
			var sideCollection = this.model.get('sides');
		}else if(type == 'salad'){
			var sideCollection = this.model.get('salads');
		}else if(type == 'soup'){
			var sideCollection = this.model.get('soups');
		}

		if(sideCollection){
			sideCollection.add(side, {'merge':true});
		}

		return false;
	}
});
