var ReceiptItemView = Backbone.View.extend({
	tagName: "a",
	className: "fnb_button item",
	template: _.template( $('#template_receipt_item').html() ),

	events: {
		"click": "selectItem"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		
		if(this.model.attributes.comp && this.model.attributes.comp.amount > 0){
			this.$el.addClass('comped');	
		}
		
		if(this.model.get('selected')){
			this.$el.addClass('selected');
		}else{
			this.$el.removeClass('selected');
		}
		return this;
	},

	selectItem: function(event){
		
		event.preventDefault();
		if( $(event.currentTarget).parents('div.receipt').hasClass('paid') ){
			return false;
		}
		
		if(App.admin_auth_id){
			this.editItem();
			return false;
		}

		if(this.model.get('selected')){
			this.model.set({"selected":false});
			this.$el.removeClass('selected');
		}else{
			this.model.set({"selected":true});
			this.$el.addClass('selected');
		}

		return false;
	},
	
	editItem: function(){
		var mode = $('#split_payments').find('div.btn-group:visible').data('mode');
		var item = App.cart.get(this.model.get('line'));
		
		if(mode == 'comp'){
			var compWindow = new CompWindowView({model: item});
			$.colorbox2({
				title: 'Comp Item',
				html: compWindow.render().el
			});
			
		}else if(mode == 'edit'){		
			
			var editItemWindow = new EditItemView({model: item, source: 'receipts'});
			App.Page.itemEdit = editItemWindow;

			$.colorbox2({
				html: editItemWindow.render().el,
				width: 960,
				onClosed: function(){
					editItemWindow.remove();
				}
			});
			$('#cboxLoadedContent2').css({'margin-top':'0px'});
			$('#cboxTitle2').css({'display':'none'});
			return false;
		}
		
		return false;
	}
});
