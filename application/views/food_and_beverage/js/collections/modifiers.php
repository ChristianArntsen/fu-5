var ModifierCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		return new Modifier(attrs, options);
	},

	getTotalPrice: function(){
		var total = 0.00;

		if(this.models.length > 0){
			_.each(this.models, function(modifier){
				total += parseFloat(modifier.get('selected_price'));
			});
		}
		return total;
	},

	// Checks if all required modifiers are set
	isComplete: function(categoryId){
		var complete = true;

		_.each(this.models, function(modifier){
			if(categoryId && modifier.get('category_id') != categoryId){ return true; }
			if(modifier.get('required') && !modifier.get('selected_option')){
				complete = false;
			}
		});

		return complete;
	},
	
	// Returns modifiers that are set to something other than default
	nonDefault: function(){
		return this.filter(function(modifier){

			if(modifier.get('required') || modifier.get('default') != modifier.get('selected_option')){
				return true;
			}else{
				return false;
			}
		});
	}
});
