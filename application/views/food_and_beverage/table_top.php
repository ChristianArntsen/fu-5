<form id="add_item_form2">
	<?php echo form_input(array('name'=>'item','id'=>'item','size'=>'40', 'accesskey' => 'i', 'placeholder'=>lang('sales_start_typing_item_name')));?>
	<?php echo form_hidden('price_index','');?>
	<?php echo form_hidden('teetime_type','');?>
</form>
<script type="text/javascript">
$()
$( "#item" ).autocomplete({
	source: '<?php echo site_url("food_and_beverage/item_search"); ?>',
	delay: <?=$this->config->item("search_delay") ?>,
	autoFocus: false,
	minLength: 0,
	select: function(event, ui)
	{
		$("input[name=price_index]").val(ui.item.price_index);
		$("input[name=teetime_type]").val(ui.item.teetime_type);

		fnb.add_item(ui.item.value, false, ui.item.price_index, ui.item.teetime_type);
	}
});
$('#mode_button').click(function(){
	fnb.change_sales_mode('<?php echo $mode ?>');
});
$("#add_item_form2").submit(function(e) {
	e.preventDefault();
	$.ajax({
       type: "POST",
       url: "index.php/food_and_beverage/add/"+$("#item").val(),
       data: "",
       success: function(response) {
	   			fnb.update_page_sections(response);
	   },
       dataType:'json'
    });
});
</script>