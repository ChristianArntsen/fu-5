<ul id="error_message_box"></ul>

<?php
echo form_open('courses/send_push_notification/', array('id'=>'push_notification_form'));
?>
	<fieldset id="item_basic_info">
	<legend>New Push Notification</legend>
	<div id='push_notification_info'>
		<div class="field_row clearfix">
		<?php echo form_label(lang('courses_id').':', 'name',array('class'=>'wide')); ?>
			<div class='form_field'>
			<?php echo form_input(array(
				'autocomplete'=>'off',
				'name'=>'push_course_id',
				'id'=>'push_course_id')
			);?>
			</div>
			<input type='checkbox' id='push_all_courses' name='push_all_courses' /> Send to all courses
		</div>

		<div class="field_row clearfix">
			<?php echo form_label(lang('courses_push_notification_url').':', 'name',array('class'=>'wide')); ?>
			<div class='form_field'>
				<?php echo form_input(array(
								'name'=>'push_url',
								'id'=>'push_url')
				);?>
			</div>
		</div>

		<div class="field_row clearfix">
		<?php echo form_label(lang('courses_push_notification_message').':', 'name',array('class'=>'wide')); ?>
			<div class='form_field'>
			<?php echo form_textarea(array(
				'name'=>'push_message',
				'id'=>'push_message')
			);?>
			</div>
		</div>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit',
			'value'=>lang('common_send'),
			'class'=>'submit_button float_right')
		);
		?>
	</div>
	</fieldset>
</form>

<script>
$(document).ready(function(){
	$( "#push_course_id" ).autocomplete({
		source: "<?php echo site_url('courses/suggest');?>",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$('#push_course_id').val(ui.item.value);
		}
	});
	$('#push_notification_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
				success:function(response)
				{
					if (response.success == true) {
						alert('Push notification has been sent.');
						$.colorbox.close();
					} else {
						alert('Trouble sending push notification.');
					}
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			push_course_id:{
				number: true,
				required: {
					depends: function(element) { return !($('#push_all_courses').is(':checked')); }
				},
				minlength: 1
			},
			push_message:
			{
				required:true,
				minlength: 5
			},
			push_url:
			{
				required:true,
				url:true
			}
   		},
		messages:
		{

		}
	});
});
</script>