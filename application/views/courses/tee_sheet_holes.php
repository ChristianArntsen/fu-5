<ul id="error_message_box"></ul>
<?php
echo form_open('courses/save_tee_sheet_holes/',array('id'=>'tee_sheet_hole_form'));
?>
<fieldset id="tee_sheet_holes_info">
    <legend>Tee Sheet Holes for <?=$course_info->name?></legend>
    <div id='basic_tee_sheet_holes_info'>
        <div class="field_row clearfix">
            <?php echo form_label('Tee Sheet:', 'teesheet_id');  ?>
            <div class='form_field'>
                <?php
                echo form_dropdown('teesheet_id', $tee_sheet_list, $tee_sheets[0]['teesheet_id'] );
                ?>
            </div>
        </div>
        <div style="width:1000px">
            <table id="tee_sheet_holes_table">
                <tbody>
                    <tr>
                        <th>
                            Hole #
                        </th>
                        <th>
                            Par
                        </th>
                        <th>
                            Handicap
                        </th>
                        <th>
                            Pro Tip
                        </th>
                        <th>
                            Tee Box Long
                        </th>
                        <th>
                            Tee Box Lat
                        </th>
                        <th>
                            Mid Point Long
                        </th>
                        <th>
                            Mid Point Lat
                        </th>
                        <th>
                            Hole Long
                        </th>
                        <th>
                            Hole Lat
                        </th>
                        <th>
                            Green Front Long
                        </th>
                        <th>
                            Green Front Lat
                        </th>
                        <th>
                            Green Back Long
                        </th>
                        <th>
                            Green Back Lat
                        </th>
                    </tr>
                    <?php for ($i = 1; $i <= 18; $i++) {
                        $last_nine_class = $i > 9 ? 'last_nine' : '';
                        ?>
                        <tr class="<?=$last_nine_class?>">
                            <td class="hole_number" width="5%">
                                <?=$i?>
                            </td>
                            <td class="par" width="5%">
                                <input type="text" name="par[]" id="par_<?=$i?>" style="width:25px"/>
                            </td>
                            <td class="par" width="5%">
                                <input type="text" name="handicap[]" id="handicap_<?=$i?>" style="width:25px"/>
                            </td>
                            <td class="pro_tip" width="15%">
                                <input type="text" name="pro_tip[]" id="pro_tip_<?=$i?>" style="width:175px"/>
                            </td>
                            <td class="tee_box_long" width="7%">
                                <input type="text" name="tee_box_long[]" id="tee_box_long_<?=$i?>" style="width:50px"/>
                            </td>
                            <td class="tee_box_lat" width="7%">
                                <input type="text" name="tee_box_lat[]" id="tee_box_lat_<?=$i?>" style="width:50px"/>
                            </td>
                            <td class="mid_point_long" width="7%">
                                <input type="text" name="mid_point_long[]" id="mid_point_long_<?=$i?>" style="width:50px"/>
                            </td>
                            <td class="mid_point_lat" width="7%">
                                <input type="text" name="mid_point_lat[]" id="mid_point_lat_<?=$i?>" style="width:50px"/>
                            </td>
                            <td class="hole_long" width="7%">
                                <input type="text" name="hole_long[]" id="hole_long_<?=$i?>" style="width:50px"/>
                            </td>
                            <td class="hole_lat" width="7%">
                                <input type="text" name="hole_lat[]" id="hole_lat_<?=$i?>" style="width:50px"/>
                            </td>
                            <td class="green_front_long" width="7%">
                                <input type="text" name="green_front_long[]" id="green_front_long_<?=$i?>" style="width:50px"/>
                            </td>
                            <td class="green_front_lat" width="7%">
                                <input type="text" name="green_front_lat[]" id="green_front_lat_<?=$i?>" style="width:50px"/>
                            </td>
                            <td class="green_back_long" width="7%">
                                <input type="text" name="green_back_long[]" id="green_back_long_<?=$i?>" style="width:50px"/>
                            </td>
                            <td class="green_back_lat" width="7%">
                                <input type="text" name="green_back_lat[]" id="green_back_lat_<?=$i?>" style="width:50px"/>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php
        echo form_submit(array(
                'name'=>'submit',
                'id'=>'submit',
                'value'=>lang('common_save'),
                'class'=>'submit_button float_right')
        );
        ?>
    </div>
</fieldset>
<?php
echo form_close();
?>
<style>
    #tee_sheet_holes_table td {
        text-align: center;
        padding:1px 0px 2px 0px;
    }
    #tee_sheet_holes_table input {
        font-size:10px;
    }

</style>
<script type='text/javascript'>

    var tee_sheet_holes = {
        tee_sheet_id:'',
        data_changed:false,
        initialize:function(){
            var that = this;
            this.set_tee_sheet_id();
            // Change event on dropdown
            $('#teesheet_id').on('change', function(){
                that.switch_tee_sheet();
            });
            // Change to inputs
            $('#tee_sheet_holes_table input').on('keydown', function(){
                that.set_data_changed(true);
            });
            // Save button
            $('#submit').on('click', function(e){
                e.preventDefault();
                that.save_hole_data();
            });
            this.load_data();
        },
        set_tee_sheet_id:function(){
            this.tee_sheet_id = $('#teesheet_id').val();
        },
        set_data_changed:function(data_changed){
            this.data_changed = data_changed;
        },
        switch_tee_sheet:function(){
            if (this.data_changed) {
                alert('Please save your changes before switching to another tee sheet.');
                $('#teesheet_id').val(this.tee_sheet_id);
            } else {
                // Just change
                this.set_tee_sheet_id();
                this.load_data();
            }
        },
        load_data:function(){
            var that = this;
            $.ajax({
                type: "POST",
                url: "index.php/courses/get_tee_sheet_holes/" + this.tee_sheet_id,
                data: '',
                success: function (response) {
                    that.populate_table(response.tee_sheet_holes);
                },
                dataType: 'json'
            });
        },
        populate_table:function(data){
            $('#tee_sheet_hole_form td input').val('');

            for (var i in data) {
                var index = data[i].hole_number;
                $("#par_"+index).val(data[i].par);
                $("#handicap_"+index).val(data[i].handicap);
                $("#pro_tip_"+index).val(data[i].pro_tip);
                $("#tee_box_long_"+index).val(data[i].tee_box_long);
                $("#tee_box_lat_"+index).val(data[i].tee_box_lat);
                $("#mid_point_long_"+index).val(data[i].mid_point_long);
                $("#mid_point_lat_"+index).val(data[i].mid_point_lat);
                $("#hole_long_"+index).val(data[i].hole_long);
                $("#hole_lat_"+index).val(data[i].hole_lat);
                $("#green_front_long_"+index).val(data[i].green_front_long);
                $("#green_front_lat_"+index).val(data[i].green_front_lat);
                $("#green_back_long_"+index).val(data[i].green_back_long);
                $("#green_back_lat_"+index).val(data[i].green_back_lat);
            }
        },
        save_hole_data:function(callback){
            var that = this;
            $.ajax({
                type: "POST",
                url: "index.php/courses/save_tee_sheet_holes/"+this.tee_sheet_id,
                data: $('#tee_sheet_hole_form').serialize(),
                success: function (response) {
                    if (response.success) {
                        if (typeof callback == 'function') {
                            callback();
                        }
                        that.set_data_changed(false);
                    } else {

                    }
                },
                dataType: 'json'
            });
        }
    }
    //validation and submit handling
    $(document).ready(function()
    {
        tee_sheet_holes.initialize();
    });

</script>

