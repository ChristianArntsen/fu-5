<?php if (!$search) { ?>
<!-- Include jquery -->
<script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script   src="https://code.jquery.com/ui/1.12.0-rc.2/jquery-ui.min.js"   integrity="sha256-55Jz3pBCF8z9jBO1qQ7cIf0L+neuPTD1u7Ytzrp2dqo="   crossorigin="anonymous"></script>
<!-- Search box to find a course -->
<input id="course_search" placeholder="Search Courses" val=""/>
<?php } ?>
<?php
$payment_types = array();
$billing_info['credit_card'] ? $payment_types[] = 'Credit Card' : '';
$billing_info['teetimes'] ? $payment_types[] = $billing_info['teetimes'].' Daily Trades' : '';
?>
<?php if ($course_info->course_id) { ?>
<table cellspacing="10" id="course_info_table">
    <tbody>
    <tr>
        <td colspan="2" class="course_name"><?=$course_info->name?></td>
    </tr>
    <tr>
        <td colspan="2" class="course_location"><?=$course_info->city?>, <?=$course_info->state?> - <?=$course_information->course_type?> <?= $course_info->simulator ? 'Simulator' : $course_info->holes.' holes'?></td>
    </tr>
    <tr>
        <td colspan="2">Pays with: <?=implode(', ',$payment_types)?></td>
    </tr>
    <tr>
        <td><?=$course_info->active_course ? 'Active' : '<span class="bad">Inactive</span>'?></td>
        <td>Stage <?=$course_information->stage?></td>
    </tr>
    <tr>
        <td>Date Sold</td>
        <td> <?=$course_information->date_sold?></td>
    </tr>
    <tr>
        <td>CC Processor</td>
        <td><?=!empty($course_info->ets_key) ? 'ETS' : (!empty($course_info->mercury_id) ? 'Mercury' : (!empty($course_info->element_account_id) ? 'Element' : 'unknown'))?></td>
    </tr>
    <tr>
        <td>F&B</td>
        <td><?=$course_info->food_and_beverage ? 'Yes' : '<span class="bad">No</span>'?></td>
    </tr>
    <tr>
        <td>Marketing</td>
        <td><?=$course_info->marketing_campaigns ? 'Yes' : '<span class="bad">No</span>'?></td>
    </tr>
    <tr>
        <td>Invoicing</td>
        <td><?=$course_info->invoices ? 'Yes' : '<span class="bad">No</span>'?></td>
    </tr>
    <tr>
        <td>Websites</td>
        <td><?=$website_info->stage ? $website_info->stage : '<span class="bad">No</span>'?></td>
    </tr>
    <tr>
        <td>Mobile App</td>
        <td><?=$course_info->mobile_app_active ? 'Yes' : '<span class="bad">No</span>'?></td>
    </tr>
    </tbody>
</table>
<?php } else { ?>
    <table cellspacing="10" id="course_info_table">
        <tbody>
        <tr>
            <td colspan="2" class="course_name">Find course info.</td>
        </tr>
        </tbody>
    </table>
<?php } ?>

<?php if (!$search) {?>
<style>
    table {
        background-color: #2f5f9a;
        font-family: sans-serif;
        font-size: 14px;
        color: white;
        width:350px;
    }
    span.bad {
        color:#e48b00;
        font-weight:bold;
        font-size:20px;
    }
    .course_name {
        font-size: 24px;
        font-family: serif;
        width:328px;
        max-width:328px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .course_location {
        text-align:center;
    }
    #course_search {
        width:350px;
    }
</style>
<script>
    $(document).ready(function(){
        $( '#course_search' ).autocomplete({
            source: '<?php echo site_url('daemon/course_search'); ?>',
            delay: 200,
            autoFocus: true,
            minLength: 1,
            select: function(event, ui)	{
                event.preventDefault();
                var course_id = ui.item.value;
                $.ajax({
                    type: "POST",
                    url: "a7b2ee28-ad8e-49c4-927a-fd15d7c7a83d/"+course_id+'/1',
                    data: "",
                    success: function(response){
//                        Calendar_actions.update_teesheet(response.teetimes);
//                        $('#remove_teed_off_time, #teed_off_label, #teed_off_time').hide();
                        $('#course_info_table').replaceWith(response);
                    },
                    dataType:'html'
                });
                $(this).val('');
//                event.preventDefault();
//                var index = $(this).attr('id').replace('phone_', '').replace('zip_', '').replace('email_', '');
//                customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
//                $('#teetime_title_'+index).val(ui.item.name).removeClass('teetime_name');
//                $('#email_'+index).val(ui.item.email).removeClass('teetime_email');
//                $('#phone_'+index).val(ui.item.label);
//                $('#person_id_'+index).val(ui.item.value);
//
//                if(BackboneApp.data.course.get_setting('limit_fee_dropdown_by_customer') == 1 && ui.item.valid_price_classes){
//                    update_price_class_dropdown($('#price_class_'+index), ui.item.valid_price_classes, parseInt(ui.item.price_class));
//                }
            }

        });
    });
</script>
<?php } ?>