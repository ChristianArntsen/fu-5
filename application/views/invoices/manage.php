<?php $this->load->view("partial/header_new"); ?>
<style>
	#table_top input#month {
		float:right;
		width:100px;
		background:url("../images/pieces/search2.png") no-repeat scroll left center transparent;
		margin-right:10px;
		border-radius: 0px 5px 5px 0px;
		border-right: 1px solid rgb(85, 85, 85);
	}
	#paid_status {
		float:right;
		margin-right:10px;
		margin-top:10px;
	}
	.mtz-monthpicker .ui-state-default, .mtz-monthpicker .ui-widget-content .ui-state-default, .mtz-monthpicker .ui-widget-header .ui-state-default {
		border: 1px solid #d3d3d3/*{borderColorDefault}*/;
		background: #e6e6e6/*{bgColorDefault}*/ url(images/teesheet/ui-bg_glass_75_d0e5f5_1x400.png)/*{bgImgUrlDefault}*/ 50%/*{bgDefaultXPos}*/ 50%/*{bgDefaultYPos}*/ repeat-x/*{bgDefaultRepeat}*/;
		font-weight: normal/*{fwDefault}*/;
		color: #555555/*{fcDefault}*/;
	}
</style>
<script type='text/javascript' src="js/customer.js"></script>
<script type="text/javascript">
function reload_customer(html)
{	
	$.colorbox2.close();
	$('#cc_dropdown').replaceWith(""+html+"");	
}

var invoices = {
	switch_data : function(type) {
		$.ajax({
           type: "POST",
           url: "index.php/invoices/switch_data_type/"+type,
           data: '',
           success: function(response){
           		// RELOAD DATA
           		do_search();
		    },
            dataType:'json'
         });
	},
	adjust_table_headers : function(type) {
		if (type == 'billings') {
			var labels = ['&nbsp;','Title', 'Customer Name', 'Frequency', 'Bill', 'Email', 'Show Trans', 'Due After', 'Bill After', 'Expires', ''];
		}
		else if (type == 'invoices') {
			var labels = ['&nbsp;','Invoice Number', 'Customer Name', 'Date Billed', 'Total', 'Paid', 'Date', '', '', '', ''];
		}
		var header_labels = $('.header_cell');
		for (var i in labels) {
			var header_label = header_labels[i];
			$(header_label).html(labels[i]+"<span class='sortArrow'></span>");
		}
	}
};
$(document).ready(function()
{
	$('#recurring_billing_view').on('click', function(){
		$('#recurring_billing_radio').click();
		$('#billing_view_buttons .selected').removeClass('selected');
		$('#paid_status_msdd').hide();
		$('#month').hide();
		$(this).addClass('selected');
		invoices.adjust_table_headers('billings');
		invoices.switch_data('billings');
	});
	$('#invoice_view').on('click', function(){
		$('#invoice_radio').click();
		$('#billing_view_buttons .selected').removeClass('selected');
		$('#paid_status_msdd').show();
		$('#month').show();
		$(this).addClass('selected');
		invoices.adjust_table_headers('invoices');
		invoices.switch_data('invoices');
	});
	$('#projected_bills').click(function(e){
		e.preventDefault();
		$.colorbox({href:'index.php/invoices/view_projections', width:700, title:'Projections' });		
	});
	$('#charge_attempts').click(function(e){
		e.preventDefault();
		$.colorbox({href:'index.php/invoices/view_charge_attempts', width:1100, title:'Charge Attempts' });		
	});
	$('#month').monthpicker();
	$('#month').monthpicker().bind('monthpicker-click-month', function (e, month){
		do_search();
	});
	$('#paid_status').msDropDown();
	$('#paid_status').change(function(){do_search();});
	$('#invoices .dd').css('width', '100px');
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({'maxHeight':700, 'width':850});
	$('#new_invoice_button').colorbox({'maxHeight':700, 'width':850});
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
    enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
    enable_bulk_edit('<?php echo lang($controller_name."_none_selected")?>');
	init_email_links();
});
function init_email_links()
{
	$('.email_invoice').click(function(e){
		e.preventDefault();
		var invoice_id = $(this).attr('invoice-id');
		if(confirm('Email a copy of this Invoice?')){
			sales.email_invoice(invoice_id);
		}

	})
}
function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			headers:
			{
				0: { sorter: false},
				7: { sorter: false},
				8: { sorter: false},
				9: { sorter: false},
				10: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}


</script>

<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php echo 
					anchor("$controller_name/billing/-1/width~1200",
					lang($controller_name.'_new_invoice_or_billing'),
					array('class'=>'colbox none new', 
						'id'=>'new_invoice_button',
						'title'=>lang($controller_name.'_new')));
					// echo 
					// anchor("credit_cards/view/-1",
					// lang($controller_name.'_new_recurring_billing'),
					// array('class'=>'colbox none new', 
						// 'title'=>lang($controller_name.'_new_recurring_billing')));
				
					// echo 
					// anchor("$controller_name/view_batch_pdfs/",
					// lang($controller_name.'_batch_pdfs'),
					// array('class'=>'colbox none new', 
						// 'title'=>lang($controller_name.'_batch_pdfs')));
				
					/*echo 
					anchor("credit_cards/view/-1",
					lang($controller_name.'_templates'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_templates')));
				*/
					// echo 
					// anchor("invoices/view_settings/",
					// lang($controller_name.'_settings'),
					// array('class'=>'colbox none new', 
						// 'title'=>lang($controller_name.'_settings')));
				
					echo
					anchor("$controller_name/view_projections/width~400",
					lang("invoies_projected_bills"),
					array('id'=>'projected_bills',
						'class'=>'projecte_bills'));
					echo 
					anchor("$controller_name/view_charge_attempts/width~600",
					lang("invoices_charge_attempts"),
					array('id'=>'charge_attempts', 
						'class'=>'charge_attempts')); 
					echo 
					anchor("$controller_name/delete",
					lang("common_delete"),
					array('id'=>'delete', 
						'class'=>'delete_inactive')); 
				?>

			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
			<div id='table_top'>
				<div id="billing_view_buttons">
		       		<ul>
		       			<li id="invoice_view" class='selected'>Invoices</li>
		       			<li id="recurring_billing_view" class="last">Recurring Billings</li>
		       		</ul>
		       		<div style='display:none'>
				       <input type='radio' id='invoice_radio' name='billing_type' value='0' checked/><label for='invoices' id='invoices_label'>Invoices</label>  
		               <input type='radio' id='recurring_billing_radio' name='billing_type' value='1'/><label for='recurring_billings' id='recurring_billings_label'>Recurring Billings</label>
		        	</div>
		       </div>
		       <?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
					<input type="text" name ='search' id='search' placeholder="Search"/>
					<input type="hidden" name ='search_id' id='search_id' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form>
				<select name ='paid_status' id='paid_status'>
					<option value='all'>Status</option>
					<option value='paid'>Paid</option>
					<option value='unpaid'>Unpaid</option>
				</select>
				<input type="text" name ='month' id='month' placeholder="Month"/>
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $invoice_manage_table; ?>
				</div>
				<div id="billing_table_holder" style='display:none'>
				<?php echo $billing_manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>