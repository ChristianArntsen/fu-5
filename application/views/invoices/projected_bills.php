<style>
	#projected_bill_body {
		min-height:300px;
		margin:0px 10px 20px;
	}
	#projection_date, #run_projection {
		width:100px;
		float:left;
		margin:10px;
	}
	#projected_bill_header input#run_projection {
		background: #349ac5;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5',endColorstr='#4173b3');
		background: -webkit-linear-gradient(top,#349AC5,#4173B3);
		background: -moz-linear-gradient(top,#349AC5,#4173B3);
		color: white;
		display: block;
		font-size: 14px;
		font-weight: normal;
		height: 30px;
		text-align: center;
		text-shadow: 0px -1px 0px black;
		border-radius: 4px;
		margin-bottom: 10px;
		box-shadow: inset 0px 1px 1px 0px rgba(255,255,255,0.5),0px 3px 1px -2px rgba(255,255,255,.2);
		border: 1px solid #232323;
		width:140px;
	}
	#projected_bill_body table thead td {
		font-weight:bold;
		text-decoration:underline;
	}
	#projected_bill_body td.currency_alignment {
		text-align:right;
	}
	#projected_bill_body td.center_alignment {
		text-align:center;
	}
	.projection_disclaimer {
		margin:20px 0px 0px 10px;
		float:left;
	}
</style>
<div id='projected_bills'>
	<div id='projected_bill_header'>
		<!-- Date -->
		<input placeholder='Projection Date' type='text' value='' id='projection_date'/>
		<!-- Submit -->
		<input type='button' value='Run Projection' id='run_projection'/>
		<span class='projection_disclaimer'>
		* These are projected billing amounts, actual amount could vary.
		</span>
		<div class='clear'></div>
	</div>
	<div id='projected_bill_body'>
		<table>
			<thead>
				<tr>
					<td>Customer</td>
					<td class='currency_alignment'>Invoiced Items</td>
					<td class='currency_alignment'>New Account Charges</td>
					<td class='currency_alignment'>Total</td>
					<td class='center_alignment'>Card On File</td>
				</tr>
			</thead>
			<tbody id='projections'>
				
			</tbody>
		</table>
	</div>
	<!-- TODO: Add in disclaimer saying these are only projections -->
</div>
<script>
	$(document).ready(function(e){
		$('#projection_date').datepicker({dateFormat:'yy-mm-dd', minDate:0});
		$('#run_projection').click(function(e){
			e.preventDefault();
			var date = $('#projection_date').val(); 
			if (date == '')
			{
				alert('Please select a date');
			}
			else
			{
				$.ajax({
		           type: "POST",
		           url: "index.php/invoices/run_projection/"+date,
		           data: '',
		           success: function(response){
		           		console.dir(response);
		           		$('#projections').html(response.return_html);
						$.colorbox.resize();
				    },
		            dataType:'json'
		        });
		    }
		});
	});
</script>