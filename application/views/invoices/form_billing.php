<?php
$controller_name=strtolower(get_class($this));
echo form_open("customers/save_recurring_invoice/".$billing_info->billing_id, array('id'=>'invoice_form'));
$billing_base_url = site_url("customers/save_invoice");
?>
	<style>
	div.end-date {
		display: block;
		overflow: hidden;
	}
	div.end-date label {
		float: left;
		width: 75px;
		height: 30px;
		line-height: 30px;
		display: block;
	}

	div.end-date label > input {
		display: block;
		float: left;
		margin-right: 5px;
		margin-top: 8px;
	}

	label.disabled, input.disabled {
		color: #AAA;
		opacity: 0.2;
	}
	
	div.invoice_settings {
		border: 1px solid #BBB; 
		background-color: #E0E0E0; 
		float: none; 
		overflow: hidden; 
		width: 80%; 
		padding: 10px 29px; 
		margin: auto;
	}
	</style>
	<input type='hidden' id='billing_base_url' name='billing_base_url' value='<?=$billing_base_url;?>'/>
	<input type='hidden' id='billing_id' name='billing_id' value='<?=$billing_info->billing_id;?>'/>
	<table id='member_billing_table'>
		<tbody>
			<tr>
				<td style='width:750px;' id='invoice_creation_box'>
					<ul id="error_message_box"></ul>
					<fieldset id="billing_info" style='display:block'>
						<div id='invoice_time_options'>
							<div id="recurring_title" style="float: left; width: 270px;">
								<label style="color: white; padding-left: 15px; margin-right: 15px; display: block; float: left;">Title</label>
								<input id="billing_title" name="billing_title" style='margin-top:4px;' value='<?=$billing['title'];?>' placeholder="Billing Title" />
							</div>
							<div id='save_invoice' style="float: right; display: block;">Save</div>
						</div>
						<div id="invoice_settings" class="invoice_settings" style="display: none;">
							<?php if (!$person_info->person_id || $person_info->person_id == '') { ?>
								<div id='person_search_options' style='padding-bottom:5px;'>
									<input class="customer_group_search" name="customer_group_search" type="" placeholder="Add Customers" class="" style="width: 180px; margin-bottom:10px;" />
									<ul class='customer_list'></ul>
								</div>
							<?php } ?>
							<div style="width: 130px; float: right; overflow: hidden;">
								<label style="height: 30px; line-height: 30px; width: 50px" for="invoice_due_days">Due after </label>
								<span style="height: 30px; line-height: 30px; float: right; margin-left: 5px;">days</span>
								<input id="invoice_due_days" name="invoice_due_days" type="text" value="14" style="width: 30px; float: right;" />
							</div>							
						</div>
						<div id='recurring_settings' class="invoice_settings">
							<div>
								<?php if (!$person_info->person_id || $person_info->person_id == '') { ?>
								<div id='person_search_options' style='padding-bottom:5px;'>
									<input class="customer_group_search" name="customer_group_search" type="" placeholder="Add Customers" class="" style="width: 180px; margin-bottom:10px;" />
									<ul class='customer_list'></ul>
								</div>
								<?php } ?>
								<label for="frequency">Repeat Every</label> &nbsp;
								<input id="frequency" name="frequency" type="text" class="" style="width: 30px;" value="<?= (int) $billing['frequency']?>" />

								<?php echo form_dropdown('frequency_period', array(
									'day' => 'Day(s)',
									'week' => 'Week(s)',
									'month' => 'Month(s)',
									'year' => 'Year(s)',
								), $billing['frequency_period']); ?>

								<?php
								$week_menu_attr = "class='frequency_on weekday' style='display: none;' disabled";
								$month_menu_attr = "class='frequency_on month' style='display: none;' disabled";
								$year_menu_attr = "class='frequency_on year' style='display: none;' disabled";
								$hide_on_label = "";
								if($billing['frequency_period'] == 'day'){
									$hide_on_label = 'style="display: none;"';

								}else if($billing['frequency_period'] == 'week'){
									$week_menu_attr = "class='frequency_on weekday'";

								}else if($billing['frequency_period'] == 'month'){
									$month_menu_attr = "class='frequency_on month'";

								}else if($billing['frequency_period'] == 'year'){
									$year_menu_attr = "class='frequency_on year'";
								} ?>

								&nbsp;<label for="on" <?php echo $hide_on_label; ?> id="label_on">on</label>&nbsp;

								<?php echo form_dropdown('frequency_on', array(
									7 => 'Sunday',
									1 => 'Monday',
									2 => 'Tuesday',
									3 => 'Wednesday',
									4 => 'Thursday',
									5 => 'Friday',
									6 => 'Saturday'
								), $billing['frequency_on'], $week_menu_attr); ?>

								<?php echo form_dropdown('frequency_on', array(
									'begin' => '1st of the Month',
									'end' => 'End of the Month',
									'date' => 'Specific Date'
								), $billing['frequency_on'], $month_menu_attr); ?>

								<?php echo form_dropdown('frequency_on', array(
									'begin' => '1st of the Year',
									'end' => 'End of the Year',
									'date' => 'Specific Date'
								), $billing['frequency_on'], $year_menu_attr); ?>

								<input style="<?=($billing['frequency_on'] == 'date' ? '' : 'display: none;')?> width: 85px;" id="frequency_on_date" name="frequency_on_date" type="" placeholder="" class="date" value='<?=$billing['frequency_on_date']?>' <?= ($billing['frequency_on'] == 'date') ? '' : 'disabled'?> />
							</div>
							<div style="margin-top: 6px;">
								<input type='hidden' id='customer_id' name='customer_id' value='<?=$person_info->person_id;?>'/>
								<label style="width: 60px; padding-top: 7px; display: block; float: left; " for="start_date">Start On</label>
								<input id="start_date" name="start_date" type="" placeholder="Start Date" class="date" style="width: 90px;" value='<?=$start_date?>' />
							</div>
							<?php if(!empty($billing['end_date'])){
								$end_never = '';
								$end_date = 'checked';
								$end_date_status = '';
							}else{
								$end_never = 'checked';
								$end_date = '';
								$end_date_status = 'disabled';
							} ?>
							<div style="margin-top: 6px;" class="end-date">
								<label style="width: 60px;">End On</label>
								<label>
									<input type="radio" id="end_on_never" name="end_never" value="1" <?php echo $end_never; ?> />
									Never
								</label>
								<label style="width: 50px;">
									<input type="radio" id="end_on_date" name="end_never" value="0" <?php echo $end_date; ?>  />
									Date
								</label>
								<input id="end_date" name="end_date" type="" placeholder="End Date" style="width: 90px;" class="date <?php echo $end_date_status; ?>" <?php echo $end_date_status; ?> value='<?=$billing['end_date']?>'/>
								<div style="width: 145px; float: right; overflow: hidden; margin-left:15px;">
									<label style="height: 30px; line-height: 30px; width: 70px" for="invoice_due_days">Charge after </label>
									<span style="height: 30px; line-height: 30px; float: right; margin-left: 5px;">days</span>
									<input id="auto_bill_delay" name="auto_bill_delay" type="text" value="<?php echo $billing['auto_bill_delay']; ?>" style="width: 30px; float: right;" />
									<div>
										<?php echo form_dropdown('attempt_limit', array(
											'1' => '1',
											'2' => '2',
											'3' => '3'
										), $billing['attempt_limit']); ?>
										<span style="height: 30px; line-height: 30px; margin-left: 5px;">Charge attempts</span>
									</div>								
								</div>		
								<div style="width: 130px; float: right; overflow: hidden;">
									<label style="height: 30px; line-height: 30px; width: 50px" for="due_days">Due after </label>
									<span style="height: 30px; line-height: 30px; float: right; margin-left: 5px;">days</span>
									<input id="due_days" name="due_days" type="text" value="<?php echo $billing['due_days']; ?>" style="width: 30px; float: right;" />
									<div>
										<input type='hidden' name='auto_pay_overdue_hidden' id='auto_pay_overdue_hidden' value='<?=$billing['auto_pay_overdue']?>'/>
										<?php echo form_checkbox('auto_pay_overdue', 1, $billing['auto_pay_overdue']); ?>
										<span style="height: 30px; line-height: 30px; margin-left: 5px;">Auto Pay Overdue</span>
									</div>	
								</div>
							</div>
						</div>
						<?php 
							$data = array();
							$data['is_recurring_billing'] = $is_recurring_billing;
							$data['course_info'] = $course_info;
							$data['credit_cards'] = $credit_cards;
							$data['popup'] = $popup;
							$data['editing'] = $editing;
							$this->load->view('customers/invoice', $data);
						?>
					</fieldset>
				</td>
			</tr>
		</tbody>
	</table>
</form>

<script>
window.billing_config =
{
	customer_credit : '',
	member_balance : '',
	cp : '',
	bc : '',
	member_balance_nickname : '',
	customer_credit_nickname : '',
	base_url : ''
};

window.billing_config.customer_credit = '<?php echo ($person_info->account_balance); ?>';
window.billing_config.member_balance = '<?php echo ($person_info->member_account_balance); ?>';
window.billing_config.member_balance_nickname = '<?php echo (($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')));  ?>';
window.billing_config.customer_credit_nickname = '<?php echo (($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname')));  ?>';
window.billing_config.base_url = "/index.php/customers/load_invoice/0/0/0/0/";

$(document).ready(function()
{
	var cust = $('.customer_group_search');
	cust.autocomplete({
		source: '<?php echo site_url("invoices/recipient_search/ln_and_pn"); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)
		{
			event.preventDefault();
			//$("#customer").attr('value',"<?php echo lang('sales_start_typing_customer_name'); ?>");
                        cust.val('');
                        
                        //<input type="checkbox" name="campaign_group[]" value="0">
            if (ui.item.is_group)
			{
				$(this).next().append(
	       			'<li id="campaign_group-li-' + ui.item.value+'"><input id="campaign_group-' + ui.item.value+'" type="checkbox" checked="checked" value="'+ui.item.value+'" name="campaign_group[]">&nbsp;&nbsp;'+ui.item.label+'</li>');
				$('#campaign_group-'+ui.item.value).change(function(){
			        $('#campaign_group-li-'+ui.item.value).remove();
			        // marketing.change_email_count(-ui.item.email);
			        // marketing.change_text_count(-ui.item.text);
			    });
			    // marketing.change_email_count(ui.item.email);
			    // marketing.change_text_count(ui.item.text);
			}
			else
			{
				$(this).next().append(
			        '<li id="campaign_individuals-li-'+ui.item.value+'"><input id="campaign_individuals-'+ui.item.value+'" type="checkbox" checked="checked" value="'+ui.item.value+'" name="campaign_individuals[]">&nbsp;&nbsp;'+ui.item.label+'</li>');
			    $('#campaign_individuals-'+ui.item.value).change(function(){
			      	$('#campaign_individuals-li-'+ui.item.value).remove();
				    // marketing.change_email_count(-(ui.item.email == '' ? 0 : 1));
				    // marketing.change_text_count(-(ui.item.email == '' ? 0 : 1));
			    });
			    // marketing.change_email_count((ui.item.email == '' ? 0 : 1));
			    // marketing.change_text_count((ui.item.email == '' ? 0 : 1));
			}
			//marketing.populate_recipient_count();
	 	}
	});
	$('.pay_invoice').click(function(e){
		e.preventDefault();
		var invoice_id, redirect;

		invoice_id = "inv " + $(this).attr('data-id');

		redirect = function(){
			window.location = '<?php echo site_url('sales'); ?>';
		};

		sales.add_item(invoice_id, redirect);
	});

	$('.email_invoice').click(function(e){
		e.preventDefault();
		var invoice_id = $(this).attr('data-id');
		if(confirm('Email a copy of this Invoice?')){
			sales.email_invoice(invoice_id);
		}

	})
	$('.edit_invoice').click(function(e){
		e.preventDefault();
		var invoice_id = $(this).attr('data-id');
		//if(confirm('Email a copy of this Invoice?')){
			sales.edit_invoice(invoice_id);
		//}

	})

	customer.billing.initialize_controls();
	customer.billing.initialize_customer_search();
	$('.date').datepicker({
		'dateFormat': 'mm/dd/yy'
	});

	$('.invoice_box').click(function(e){
		var invoice_id, person_id;

		if ($(this).hasClass('no_results')) return;
		$('#temp_billing').remove();
		$('.advanced').removeClass('advanced');
		person_id = $('#customer_id').val();
		invoice_id = $(this).attr('id');
		$('.loaded_invoice').removeClass('loaded_invoice');
		$(this).addClass('loaded_invoice');
		$('#invoice_settings').hide();
		customer.invoice.load(invoice_id, person_id);
	});

	$('.recurring_billing_box').click(function(e){
		var billing_id, person_id;

		if ($(this).hasClass('no_results')) return;
		$('#temp_billing').remove();
		$('.advanced').removeClass('advanced');
		person_id = $('#customer_id').val();
		billing_id = $(this).attr('id');
		$('.loaded_invoice').removeClass('loaded_invoice');
		$(this).addClass('loaded_invoice');
		$('#invoice_settings').hide();

		$('#billing_base_url').val('<?php echo site_url('customers/save_recurring_invoice'); ?>/' + billing_id);
		customer.billing.load(billing_id, person_id);
	});

	$('#create_new_recurring_billing, #new_recurring').click(function(){
		var person_id = $('#customer_id').val();
		if ($('#customer_name').val() === ''){
			alert('Please select a customer before starting a recurring billing');
			return;
		}
		if ($(this).hasClass('disabled')) return;
		$('#temp_billing').remove();
		$('#invoice_settings').hide();
		$('#recurring_settings, #recurring_title').show();
		$('.advanced').removeClass('advanced');
		$('.loaded_invoice').removeClass('loaded_invoice');
		$('.no_results', '.recurring_billing_box_holder').remove();
		$('#billing_title').val('');
		$('.recurring_billing_box_holder').append('<div id="temp_billing" class="recurring_billing_box loaded_invoice"> New Recurring Billing</div>');

		customer.billing.create('recurring_billing', person_id);
	});

	$('#create_new_invoice, #new_invoice').click(function(){
		var person_id = $('#customer_id').val();
		if ($('#customer_name').val() === ''){
			alert('Please select a customer before starting an invoice');
			return;
		}
		if ($(this).hasClass('disabled')) return;
		$('#temp_billing').remove();
		$('.advanced').removeClass('advanced');
		$('.no_results','.invoice_box_holder').remove();
		$('.loaded_invoice').removeClass('loaded_invoice');
		$('#recurring_settings, #recurring_title').hide();
		$('#invoice_settings').show();
		$('.invoice_box_holder').append('<div id="temp_billing" class="invoice_box loaded_invoice"><div class="top_invoice_row"> New Invoice</div><div></div></div>');
		customer.billing.create('invoice', person_id);
	});

	$('.delete_billing').click(function(){
		var billing_id = $(this).parent().attr('id');
		if(confirm('Delete this recurring billing?')){
			$(this).parent().remove();
			customer.billing.remove('recurring_billing',billing_id);
		}
	});
	$('.delete_invoice').click(function(){
		var invoice_id = $(this).parent().parent().attr('id');
		console.log(invoice_id);
		if(confirm('Delete this invoice?')){
			$(this).parent().parent().remove();
			customer.invoice.remove('invoice',invoice_id);
		}
	});

	$('#create_multiple_invoices').click(function(){
		$('#multiple_invoice_options').toggleClass('hidden');
		$('#expand_indicator').toggleClass('right_ninety_degrees');
	});

	$('li','#multiple_invoice_options').click(function(){
		$('#multiple_invoice_options').toggleClass('hidden');
		$('#expand_indicator').toggleClass('right_ninety_degrees');
	});

	//make the whole button clickable
	// $('.invoice_type', '#multiple_invoice_options').click(function(){
		// var input = $('input',this);
		// $(input).prop('checked', !$(input).prop('checked'));
	// });

	//Generating bulk invoices
	$(':input','#multiple_invoice_options').change(function(){
		update_billings_link();
	});

	//validate fields when generate pdfs is clicked
	$('#generate_billings').click(function(e){
		var start, end,
			num_reports_selected = 0,
			recurring_billings_checked = $('#recurring_billings_checkbox','#multiple_invoice_options').attr('checked');

		start = $('#billing_start_date');
		end = $('#billing_end_date');

		$(':checked','#multiple_invoice_options').each(function(){
			num_reports_selected += 1;
		});

		if (num_reports_selected < 1){
			e.preventDefault();
			set_feedback('Please select the billing you would like to generate','error_message',false);
			return;
		}

		if (recurring_billings_checked && start.val() === '')
		{
			e.preventDefault();
			set_feedback('A start date must be selected','error_message',false);
			return;
		}
		else if (recurring_billings_checked && end.val() === '')
		{
			e.preventDefault();
			set_feedback('An end date must be selected','error_message',false);
			return;
		}
		$('#multiple_invoice_options').toggleClass('hidden');
		$('#expand_indicator').toggleClass('right_ninety_degrees');
		$('#pdf_download_message').removeClass('hidden');

		//clear out the checkboxes
		$(':checked','#multiple_invoice_options').each(function(){
			$(this).attr('checked',false);
		});

		//hide the date options
		$('#recurring_billings_dates').addClass('hidden');
	});

	//hide or show date options
	$('#recurring_billings_checkbox','#multiple_invoice_options').click(function(){
		$('#recurring_billings_dates').toggleClass('hidden');
	});

	function update_billings_link()
	{
		var negative_member_balance = $("#negative_member_balance_checkbox").attr('checked') ? 1 : 0,
			negative_credit_balance = $("#negative_credit_balance_checkbox").attr('checked') ? 1 : 0,
			recurring_billings = $("#recurring_billings_checkbox").attr('checked') ? 1 : 0,
			start_on = $('#billing_start_date').val() === '' ? 0 : $('#billing_start_date').val(),
			end_on = $('#billing_end_date').val() === '' ? 0 : $('#billing_end_date').val();

			start_on = encodeURIComponent(start_on);
			end_on = encodeURIComponent(end_on);
			link = window.billing_config.base_url;
			link += negative_member_balance + '/' + negative_credit_balance + '/' + recurring_billings + '/' + start_on + '/' + end_on;

			$('#billings_link').attr('href',"" + link + "");
	}

	// Recurring invoice options
	$('#frequency_period').live('change', function(e){
		var value = $(this).val();
		change_frequency_period(value);
	});

	$('select.frequency_on').live('change', function(e){
		var value = $(this).val();
		change_frequency_on(value);
		e.preventDefault();
	});

	$('#end_on_never').live('change', function(e){
		select_end_on('never');
		e.preventDefault();
	});

	$('#end_on_date').live('change', function(e){;
		select_end_on('date');
		e.preventDefault();
	});
});

function change_frequency_period(value){
	if(value == 'day'){
		$('select.frequency_on').hide().attr('disabled', 'disabled');
		$('#frequency_on_date').hide().attr('disabled', 'disabled');
		$('#label_on').hide();
		$('select.frequency_on.day').show().attr('disabled', null);

	}else if(value == 'week'){
		$('select.frequency_on').hide().attr('disabled', 'disabled');
		$('#frequency_on_date').hide().attr('disabled', 'disabled');
		$('#label_on').show();
		$('select.frequency_on.weekday').show().attr('disabled', null);

	}else if(value == 'month'){
		$('select.frequency_on').hide().attr('disabled', 'disabled');
		$('select.frequency_on.month').show().attr('disabled', null);
		$('#label_on').show();

	}else if(value == 'year'){
		$('select.frequency_on').hide().attr('disabled', 'disabled');
		$('select.frequency_on.year').show().attr('disabled', null);
		$('#label_on').show();
	}
}

function change_frequency_on(value){
	if(value == 'date'){
		$('#frequency_on_date').show().attr('disabled', null);
	}else{
		$('#frequency_on_date').hide().attr('disabled', 'disabled');
	}
}

function select_end_on(value){
	if(value == 'never'){
		$('#end_date').addClass('disabled').attr('disabled', 'disabled');
	}else{
		$('#end_date').removeClass('disabled').attr('disabled', null);
	}
}
</script>
