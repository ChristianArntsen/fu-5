<?php $this->load->view("partial/header_new"); ?>
<div>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fullcalendar.css?<?php echo APPLICATION_VERSION; ?>" />
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fullcalendar.print.css?<?php echo APPLICATION_VERSION; ?>" media="print" />
<style type="text/css">
  .calendar {
		width: 900px;
		margin: 0 auto;
		}
   .fc-mon div,.fc-tue div,.fc-wed div,.fc-thu div,.fc-fri div,.fc-sat div{min-height: 52px;}
   .event-action{text-decoration: underline;cursor: pointer;color: #0092ef;}
   .delete{color: #cd0a0a;}
   .hidden{visibility: hidden;}
</style>
<script src="<?php echo base_url();?>js/fullcalendar.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/jquery.dateformat.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#event_form').hide();
    var currentEvent = null;

    function submit_form()
    {
      var submitting = false;
      $('#myForm').validate({
        submitHandler:function(form)
        {
          if (submitting) return;
          submitting = true;
          $(form).mask("<?php echo lang('common_wait'); ?>");
          $(form).ajaxSubmit({
            success:function(response)
            {
              if(response.success)
              {
                if(response.cmd == 'create')
                {
                  calendar.fullCalendar('renderEvent',
                   {
                      id: response.id,
                      title: response.title,
                      start: $.datepicker.parseDate("yy-mm-dd",  response.start),
                      end: $.datepicker.parseDate("yy-mm-dd",  response.end),
                      details: response.details,
                      allDay: response.allDay
                    },
                    true // make the event "stick"
                  );
                }
                else
                {
                   currentEvent.id = response.id;
                   currentEvent.title = response.title;
                   currentEvent.start = $.datepicker.parseDate("yy-mm-dd",  response.start);
                   currentEvent.end = $.datepicker.parseDate("yy-mm-dd",  response.end);
                   currentEvent.details = response.details;
                  
                   calendar.fullCalendar('updateEvent', currentEvent);
                   currentEvent = null; // set to null every update
                }

                $.colorbox.close();

                set_feedback('Successfully created/updated event, thank you.','success_message',false);
              }
              else
              {
                alert(response.errorMessage);
              }
            },
            dataType:'json'
          });

        },
        errorLabelContainer: "#error_message_box",
        wrapper: "li",
        rules:
        {
          event_title:
          {
            required:true
          },
          event_start:
          {
            required:true
          },
          event_end:
          {
            required:true
          }
        },
        messages:
        {
          event_title:
          {
            required:"Title field is a required input."
          },
          event_start:
          {
            required:"Start field is a required input."
          },
          event_end:
          {
            required:"End field is a required input."
          }
        }
      });
    }


		var calendar = $('.calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'print month,basicWeek,basicDay'
			},
      selectable: true,
			selectHelper: true,
			select: function(start, end, allDay) {
        $.fn.colorbox({title: 'New Event', escKey: false, html: $('#event_form').html(), maxWidth: 600, height: 500,
        onComplete: function(){
          var def = ['title', 'end', 'details'];
          $('#event_title, #event_end, #event_details').click(function(){
            var val = $(this).val();
            if(def.indexOf(val) != -1){
              $(this).css('color', '#000');
              $(this).val('');
            }
          });

          $('#event_title, #event_end, #event_details').focus(function(){
            var val = $(this).val();
            if(def.indexOf(val) != -1){
              $(this).css('color', '#000');
              $(this).val('');
            }
          });

          $('#event_start, #event_end').datepicker({
            dateFormat:'yy-mm-dd',
            minDate: start
          });

          $('#event_start').val($.format.date(start, 'yy-MM-dd'));
        },
        onClosed: function(){
          $('#event_start').datepicker('destroy');
          $('#event_end').datepicker('destroy');
          $('#event_start').unbind();
          $('#event_end').unbind();
        }
        });
        $.fn.colorbox.resize();      

        submit_form();
			},
      eventClick: function(calEvent, jsEvent, view) {
         currentEvent = calEvent;
         var data = '';         
         if(!calEvent.end)
         {
           data  = '<br/><b>' + $.format.date(calEvent.start, 'yy-MM-dd') + '</b><br/><br/>';
         }
         else
         {
          data  = '<br/><b>' + $.format.date(calEvent.start, 'yy-MM-dd') + '</b> to <b>' + $.format.date(calEvent.end, 'yy-MM-dd') + '</b><br/><br/>';
         }
          data  = data + 'Details: ' + calEvent.details + '<br/><br/>';
          data  = data + '<a class="event-action" id="edit-event">edit</a>&nbsp;<a class="event-action delete" id="delete-event">delete</a>';
          $.fn.colorbox({title: calEvent.title, html: data, width: 400, maxWidth: 600, maxHeight: 500});

          $('#delete-event').click(function(e){
              e.preventDefault();

              $.ajax({
                url: "<?= site_url('events/delete'); ?>",
                type: "POST",
                data: {
                    event_id: calEvent.id
                },
                success: function( data ) {
                  calendar.fullCalendar( 'removeEvents', calEvent.id);
                  $.colorbox.close();
                  set_feedback('Event successfully deleted, thank you.','success_message',true);
                },
                dataType:'json'
              });
          });

          $('#edit-event').click(function(e){
              e.preventDefault();
              $('#cboxLoadedContent').empty();
              $('#cboxLoadedContent').append($('#event_form').html());
              $.fn.colorbox.resize({width: 475});
              
              setTimeout(function(){
                var f = $('#myForm');
                f.attr('action', f.attr('action') + '/' + calEvent.id);
                $('#event_title').val(calEvent.title);
                $('#event_start').val($.format.date(calEvent.start, 'yy-MM-dd'));
                var end = $.format.date(calEvent.end || calEvent.start, 'yy-MM-dd');
                $('#event_end').val(end);
                $('#event_details').val(calEvent.details);
                $('#event_id').val(calEvent.id);
                $('input, textarea').css('color', '#000');

                $('#event_start, #event_end').datepicker({
                  dateFormat:'yy-mm-dd',
                  minDate: calEvent.start
                });

                submit_form();
              }, 500);
          });
      },
			editable: true,
			events: "<?= site_url('events/get_all'); ?>"
		});

	});
</script>
<table id="title_bar">
  <tr>
  <td id="title_icon">
    <img src='<?php echo base_url()?>images/menubar/<?php echo strtolower($controller_name); ?>.png' alt='title icon' />
  </td>
  <td id="title">
    Calendar of Events
  </td>
  </tr>
</table>
<table id="contents">
  <td id="item_table">
    <div id="table_holder">
      <div id='calendar'></div>
    </div>
  </td>
</table>
<div id="event_form" class="hidden">
<form action="<?= site_url('events/save/'); ?>" id="myForm" method="POST">
  <input type="hidden" name="event_id" id="event_id" />
  <br/>
  <div class="field_row clearfix">
    <div class='form_field'>
    <?= form_input(array(
      'name'=>'event_title',
      'size'=>'25',
      'style' => 'width: 200px;height:25px;color:#ccc;',
      'id'=>'event_title',
      'value'=>'title')
    );?>
    </div>
  </div>
  <br/>
  <div class="field_row clearfix">
    <div class='form_field'>
    <?= form_input(array(
      'name'=>'event_start',
      'size'=>'25',
      'style' => 'width: 200px;height:25px;',
      'id'=>'event_start',
      'value'=>'start')
    ).' - '.form_input(array(
      'name'=>'event_end',
      'size'=>'25',
      'style' => 'width: 200px;height:25px;color:#ccc;',
      'id'=>'event_end',
      'value'=>'end')
    );?>
    </div>
  </div>
  <br/>
  <div class="field_row clearfix">
   <div class='form_field'>
    <?php echo form_textarea(array(
      'name'=>'event_details',
      'size'=>'25',
      'rows'  => 2,
      'cols'  => 25,
      'style' => 'width: 410px;color:#ccc;',
      'id'=>'event_details',
      'value'=> 'details')
    );?>
    </div>
  </div>
  <?= form_submit(array(
     'name'=>'submitButton',
    'id'=>'submitButton',
    'value'=> 'Save',
    'class'=>'submit_button float_left')
  );
  ?>
</form>
</div>
</div>

<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>