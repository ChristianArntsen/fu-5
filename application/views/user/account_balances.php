<h2>Account Details</h2>
<ul style="margin-bottom: 25px;">
<?php foreach($accounts as $account){ ?>
	<li style="border: none;">
		<h3 style="float: left; width: 150px;">Balance <span class="amount" style="margin-top: -4px;">$<?php echo $account['member_account_balance']; ?></span></h3>
		<h3 style="float: left; margin-left: 25px; width: 175px;">Loyalty Points <span class="amount" style="margin-top: -4px;"><?php echo $account['loyalty_points']; ?></span></h3>
	</li>
<?php } ?>
</ul>
<h2>Giftcards</h2>
<ul>
<?php if(!empty($giftcards)){ ?>
<?php foreach($giftcards as $giftcard){ ?>
	<li>
		<h3>
			#<?php echo $giftcard['giftcard_number']; ?> -
			<span style="font-weight: normal;">
			<?php if($giftcard['expiration_date'] != '0000-00-00'){ ?>
				Expires on: <?php echo date('M jS, Y', strtotime($giftcard['expiration_date'])); ?>
			<?php }else{ ?>
				<em>No expiration</em>
			<?php } ?>
			</span>
			<span class="amount">$<?php echo $giftcard['value']; ?></span>
		</h3>
		<?php if(!empty($giftcard['details'])){ ?>
		<span style="color: #999;"><?php echo $giftcard['details']; ?></span>
		<?php } ?>
	</li>
<?php } } else { ?>
	<li><h3 class="muted">No giftcards found</h3></li>
<?php } ?>
</ul>