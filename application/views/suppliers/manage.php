<?php $this->load->view("partial/header_new"); ?>
<script type="text/javascript">
$(document).ready(function() 
{ 
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({'maxHeight':700, 'width':<?php echo $form_width ?>});
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
    enable_email('<?php echo site_url("$controller_name/mailto")?>');
    enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
}); 

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{ 
			sortList: [[1,0]], 
			headers: 
			{ 
				0: { sorter: false}, 
				6: { sorter: false} 
			} 

		}); 
	}
}

function post_person_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);	
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}
</script>

<!--table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('common_list_of').' '.lang('module_'.$controller_name); ?>
		</td>
		<td id="title_search">
			<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
				<input type="text" name ='search' id='search'/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table-->
<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php echo anchor("$controller_name/view/-1/",
				lang($controller_name.'_new'),
				array('class'=>'colbox none new', 'title'=>lang($controller_name.'_new')));
				?>
				<?php echo anchor("$controller_name/excel_import/width~550",
				lang($controller_name.'_import'),
				array('class'=>'colbox none import','title'=>lang($controller_name.'_import')));
				?>
				<?php echo anchor("$controller_name/excel_export",
					lang($controller_name.'_export'),
					array('class'=>'none import'));
				?>
				<a class="email email_inactive" href="<?php echo current_url(). '#'; ?>" id="email"><?php echo lang("common_email");?></a>
				<?php echo anchor("$controller_name/delete",lang("common_delete"),array('id'=>'delete', 'class'=>'delete_inactive')); ?>
			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
			<div id='table_top'>
				<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
					<input type="text" name ='search' id='search' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form>
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>