<style>
#item_customer_groups {
	border-bottom: 1px solid #DDD;
	margin-bottom: 10px;
}

div.item_customer_group {
	display: block;
	font-size: 14px;
	padding-bottom: 6px;
	margin-left: 10px;
	margin-right: 10px;
}

#item_customer_groups a.delete {
	color: red;
	font-weight: bold;
	float: left;
	display: block;
	line-height: 16px;
	margin-right: 10px;
	cursor: pointer;
}
</style>
<ul id="error_message_box"></ul>
<?php
echo form_open('items/bulk_update/',array('id'=>'item_form'));
?>
<fieldset id="item_basic_info">
<legend><?php echo lang("items_basic_information"); ?></legend>
<div class='left_side'>
<div class="field_row clearfix">
<?php echo form_label(lang('items_name').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'id'=>'name')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('items_gl_code').':', 'gl_code',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'gl_code',
		'id'=>'gl_code',
		'value'=>'')
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_department').':', 'department',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'department',
		'id'=>'department',
		'value'=>'')
	);?>

	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('items_category').':', 'category',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'category',
		'id'=>'category')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('items_subcategory').':', 'subcategory',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'subcategory',
		'id'=>'subcategory')
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_supplier').':', 'supplier',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('supplier_id', $suppliers, '');?>
	</div>
</div>

<?php if((int) $this->config->item('quickbooks') > 0){ ?>
<div id='quickbooks_accounts' style='display:none; overflow: hidden; display: block; width: 450px;'>
	<div class="field_row clearfix">
	<?php echo form_label('Income Acct.', 'quickbooks_income', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown(
			'quickbooks_income',
			$quickbooks_accounts,
			'nochange'
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('COGS Acct.', 'quickbooks_cogs', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown(
			'quickbooks_cogs',
			$quickbooks_accounts,
			'nochange'
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Asset Acct.', 'quickbooks_assets', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown(
			'quickbooks_assets',
			$quickbooks_accounts,
			'nochange'
		);?>
		</div>
	</div>
</div>
<script>
$('#quickbooks_accounts').expandable({
	title : 'QuickBooks Account Maps'
});
</script>
<?php } ?>

<!--div class="field_row clearfix">
<?php echo form_label(lang('items_location').':', 'location',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'location',
		'id'=>'location')
	);?>
	</div>
</div-->

</div>
<div class='right_side'>

<div class="field_row clearfix">
<?php echo form_label('Status:', 'inactive', array('class'=>'wide')); ?>
	<div class='form_field' style="overflow: hidden">
		<label>
			<?php echo form_radio(array(
				'name'=>'inactive',
				'value'=> '',
				'checked' => true
			));?> 
			No Change
		</label>
		<label>
			<?php echo form_radio(array(
				'name'=>'inactive',
				'value'=> '0'
			));?> 
			Active
		</label>		
		<label>
			<?php echo form_radio(array(
				'name'=>'inactive',
				'value'=> '1'
			));?> 
			Inactive
		</label>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_cost_price').':', 'cost_price',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'cost_price',
		'size'=>'8',
		'id'=>'cost_price')
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_price').':', 'unit_price',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_price',
		'size'=>'8',
		'id'=>'unit_price')
	);?>
	</div>
</div>

<div class="field_row clearfix">
	<?php echo form_label('Max Discount:', 'max_discount',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo form_input(array(
				'name'=>'max_discount',
				'size'=>'8',
				'id'=>'max_discount')
		);?>%
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Customer Groups', 'customer_group_menu');  ?>
	<div class='form_field'>
	<?php echo form_dropdown('customer_group_menu', $customer_group_menu, ''); ?>
	</div>
</div>
<div id="item_customer_groups"></div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_tax_1').':', 'tax_percent_1',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'tax_names[]',
		'id'=>'tax_name_1',
		'size'=>'8',
		'value'=> isset($item_tax_info[0]['name']) ? $item_tax_info[0]['name'] : lang('items_sales_tax'))
	);?>

	<?php echo form_input(array(
		'name'=>'tax_percents[]',
		'id'=>'tax_percent_name_1',
		'size'=>'3',
		'value'=> isset($item_tax_info[0]['percent']) ? $item_tax_info[0]['percent'] : '')
	);?>
	%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_tax_2').':', 'tax_percent_2',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'tax_names[]',
		'id'=>'tax_name_2',
		'size'=>'8',
		'value'=> isset($item_tax_info[1]['name']) ? $item_tax_info[1]['name'] : '')
	);?>

	<?php echo form_input(array(
		'name'=>'tax_percents[]',
		'id'=>'tax_percent_name_2',
		'size'=>'3',
		'value'=> isset($item_tax_info[1]['percent']) ? $item_tax_info[1]['percent'] : '')
	);?>
	%
	<?php echo form_checkbox('tax_cumulatives[]', '1', isset($item_tax_info[1]['cumulative']) && $item_tax_info[1]['cumulative'] ? true : false); ?>
	<span class="cumulative_label">
	<?php echo lang('common_cumulative'); ?>
    </span>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('items_reorder_level').':', 'reorder_level',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'reorder_level',
		'size'=>8,
		'id'=>'reorder_level')
	);?>
	</div>
</div>

</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_description').':', 'description',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'description',
		'id'=>'description',
		'rows'=>'5',
		'cols'=>'17')
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Menu Category', 'menu_category', array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'menu_category',
		'id'=>'menu_category',
		'value'=>'')
	);?>
	</div>
</div>
	<div class="field_row clearfix">
		<?php echo form_label('Menu Subcategory', 'menu_subcategory', array('class'=>' wide')); ?>
		<div class='form_field'>
			<?php echo form_input(array(
					'name'=>'menu_subcategory',
					'id'=>'menu_subcategory',
					'value'=>'')
			);?>
		</div>
	</div>
    <!-- NEED TO HIDE IT UNTIL RELEASE DAY -->
    <div class="field_row clearfix">
        <?php echo form_label('Button Color', 'button_color', array('class'=>' wide')); ?>
        <div class='form_field'>
            <input class='button_color color-picker' name="button_color" value="<?=$item_info->button_color?>" type="text" />
        </div>
    </div>
	<div class="field_row clearfix">
		<?php echo form_label('Clear Existing Service Fees', 'clear_service_fees', array('class'=>' wide')); ?>
		<div class='form_field'>
			<?php echo form_checkbox('clear_service_fees', '1', false);?>
		</div>
	</div>
	<div id='service_fees_container' style='display:none; overflow: hidden; display: block; width: 900px;'>
		<label>Add New </label>
		<?php echo form_input(array('name' => "service_fee_search", 'value' => '', 'style' => 'width: 345px;', 'class'=>'service_fee_search', 'data-placeholder'=>'Search service fees...')); ?>
		<table id="service_fees">
			<thead>
			<tr>
				<th>&nbsp;</th>
				<th>Name</th>
				<th>Amount</th>
				<th>Percent</th>
				<th>Whichever Is</th>
				<th>&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>

<!--div class="field_row clearfix">

<?php echo form_label(lang('items_allow_alt_desciption').':', 'allow_alt_description',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('allow_alt_description', $allow_alt_desciption_choices);?>

	</div>

</div-->



<!--div class="field_row clearfix">

<?php echo form_label(lang('items_is_serialized').':', 'is_serialized',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('is_serialized', $serialization_choices);?>

	</div>

</div-->

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
    jQuery_1_7_2('input.button_color').spectrum({
        preferredFormat: 'hex6',
        allowEmpty: true,
        preferredFormat: "hex",
        showInput: true
    });

    $('#customer_group_menu').on('change', function(){
		if($(this).val() == 0){
			return false;
		}
		var value = $(this).val().split(':');
		var group_id = value[0];
		var cost_plus = value[1];
		var label = $(this).find('option:selected').text();
		
		var discount_html = ' - <em>No discount</em>';
		if(cost_plus != null && cost_plus != ''){
			discount_html = ' - <em>Cost + '+cost_plus+'% discount</em>';
		}
		
		if($('#item_customer_groups').children('div[data-group-id='+group_id+']').length <= 0){
			$('#item_customer_groups').append('<div class="item_customer_group" data-group-id="'+group_id+'">'+
			'<input type="hidden" name="customer_groups[]" value="'+group_id+'">'+
			'<strong>'+label+'</strong>'+discount_html+'<a class="delete">X</a></div>');
		}
		$(this).val(0);
		
		return false;
	});

	$('#item_customer_groups').on('click', 'a.delete', function(e){
		$(e.target).parent('div').remove();
		return false;
	});

	$("#menu_category").autocomplete({
		source: "<?php echo site_url('api/items/menu_categories');?>?api_key=no_limits",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui){
			$(this).val(ui.item.name);
			return false;
		}
		
	}).click(function(){$(this).autocomplete('search','')});

	$("#menu_subcategory").autocomplete({
		source: "<?php echo site_url('api/items/menu_subcategories');?>?api_key=no_limits",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui){
			$(this).val(ui.item.name);
			return false;
		}

	}).click(function(){$(this).autocomplete('search','')});

	$( "#department" ).autocomplete({
		source: "<?php echo site_url('items/suggest_department');?>",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0
	});
	$( "#category" ).autocomplete({
		source: "<?php echo site_url('items/suggest_category');?>",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0
	});

	$('#service_fees_container').expandable({
		title : 'Service Fees'
	});

	$("input.service_fee_search").autocomplete({
		source: '<?php echo site_url('service_fees/search'); ?>',
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 1,
		select: function( event, ui){
			$.post('<?php echo site_url('service_fees/getHTML'); ?>/' + ui.item.value, null, function(response){
				if(response){
					$('#service_fees').append(response);
				}
			});
			$("input.service_fee_search").val('');
			return false;
		}
	});

	$('a.delete_service_fee').die('click').live('click', function(e){
		var row = $(this).parents('tr');
		row.remove();
		return false;
	});

	var submitting = false;
	$('#item_form').validate({
		submitHandler:function(form)
		{
        if (submitting) return;
			if(confirm("<?php echo lang('items_confirm_bulk_edit') ?>"))
			{
				//Get the selected ids and create hidden fields to send with ajax submit.
				var selected_item_ids=get_selected_values();
				for(k=0;k<selected_item_ids.length;k++)
				{
					$(form).append("<input type='hidden' name='item_ids[]' value='"+selected_item_ids[k]+"' />");
				}


			submitting = true;
            $(form).mask("<?php echo lang('common_wait'); ?>");
            $(form).ajaxSubmit({
				success:function(response)
				{
                    $(form).unmask();
					$.colorbox.close();
					post_bulk_form_submit(response);
                    submitting = false;
                },
				dataType:'json'
				});
			}

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			unit_price:
			{
				number:true
			},
			tax_percent:
			{
				number:true
			},
			quantity:
			{
				number:true
			},
			reorder_level:
			{
				number:true
			}
   		},
		messages:
		{
			unit_price:
			{
				number:"<?php echo lang('items_unit_price_number'); ?>"
			},
			tax_percent:
			{
				number:"<?php echo lang('items_tax_percent_number'); ?>"
			},
			quantity:
			{
				number:"<?php echo lang('items_quantity_number'); ?>"
			},
			reorder_level:
			{
				number:"<?php echo lang('items_reorder_level_number'); ?>"
			}

		}
	});
});
</script>
