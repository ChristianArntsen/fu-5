<?php $this->load->view("partial/header_new"); ?>
<style>
a.edit-item {
	display: block;
	text-align: center;
}
</style>
<script type="text/javascript">
	function isJson(str) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	}
$(document).ready(function()
{
        $('#pro_shop_view').click(function(){
        	$('#pro_shop').click();
			$('#search_type').val($('input[name=item_type]:checked').val());
            do_type_switch(true, '', '550', $('input[name=item_type]:checked').val());
			enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>', '',  $('input[name=item_type]:checked').val());
            $('#item_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
			// CHANGE HEADER LABELS
			$('.restaurant_label').hide();
			$('.inventory_label').show();
			$('#new_item').prop('href', '<?php echo site_url("$controller_name/view_new_item/-1/width~1100/0"); ?>');
			$('#new_item').removeClass('colbox cboxElement').addClass('new');
		});

    	$('#food_and_beverage_view').click(function(){
			$('#restaurant').click();
			$('#search_type').val($('input[name=item_type]:checked').val());
            do_type_switch(true, '', '550', $('input[name=item_type]:checked').val());
			enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>', '',  $('input[name=item_type]:checked').val());
            $('#item_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
			// CHANGE HEADER LABELS
			$('.restaurant_label').show();
			$('.inventory_label').hide();
			$('#new_item').prop('href', '<?php echo site_url("$controller_name/view_new_item/-1/width~1100/1"); ?>');
			$('#new_item').removeClass('new').addClass('colbox');
		});
		
        $('input[name=item_type]').change(function(){
            
            $('#search_type').val($(this).val());
            console.log('Searching: ' + $(this).val());
            $('#new_item').attr('href', '<?php echo "index.php/$controller_name/view_new_item/-1/width~1100/"?>' + $(this).val());
            enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>', '', $(this).val());
            do_type_switch(true, '', '550', $(this).val());
            
        });
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	
    // Use backbone customer edit window
    <?php if(!empty($_GET['in_iframe'])){ ?>
    $('a.edit-item').live('click', function(e){
        e.preventDefault();

        var item_id = $(this).attr('data-item-id');
        var item = new window.parent.Item({
            item_id: item_id
        });
        var modal = new window.parent.EditItemView({model: item});
        modal.show();
        window.parent.$('.modal-content').loadMask();

        item.fetch({
            silent: true,
            success: function(model){
                    
                // Delay the listener because a sync event is triggered 
                // AFTER this success callback (which we don't care about)
                setTimeout(function(){
                    modal.render();
                    item.on('sync', function(){
                        update_row(item_id, 'items/get_row/true')
                    });
                }, 10);
            }
        });

        return false;
    });

    $('a.new').live('click', function(e){
        var item = new window.parent.Item();
        var modal = new window.parent.EditItemView({model: item});
        modal.show();

        item.on('sync', function(model){
            // Refresh customer table
            do_search(true, function(){
                highlight_row(model.get('item_id'));
                set_feedback('Item saved', 'success_message', false);
            });
        });

        return false;
    });

//    $('a#inventory_audit').removeClass('colbox').live('click', function(e){
//        e.preventDefault();
//        var modal = new window.parent.InventoryAuditView({collection: new window.parent.InventoryAuditCollection()});
//        modal.show();
//
//        return false;
//    });

	$('a.manage-receipt-content').live('click', function(e){
		var modal = new window.parent.ItemReceiptContentModalView({collection: window.parent.App.data.item_receipt_content});
		modal.show();
		return false;
	});
    <?php } ?>

	<?php if(!empty($_GET['in_iframe'])){ ?>
	//$('.edit-item').colorbox({'maxHeight':700, 'width':550});
	$('.colbox:not(a.edit-item)').colorbox({'maxHeight':700, 'width':550});
	<?php }else{ ?>
	$('.colbox,.new').colorbox({'maxHeight':700, 'width':550});
	<?php } ?>
    
    init_table_sorting();
    enable_select_all();
    
    enable_checkboxes();
    enable_row_selection();
    console.log('Search type: ' + $('#search_type').val());
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>', '100', $('#search_type').val());
    //enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
    $('#delete').click(function(e){
    	e.preventDefault();
    	var selected_values = get_selected_values();
    	//return;
    	if($("#sortable_table tbody :checkbox:checked").length >0)
		{

			$.ajax({
				type: "POST",
				url: 'index.php/items/delete',
				data: {
					'ids[]':selected_values,
					'confirm_message':'<?php echo lang($controller_name."_confirm_delete")?>'
				},
				success: function(data){
					if(isJson(data)){
						data - JSON.parse(data);
					}
					$.colorbox({
						'html':data,
						'width':500,
						onComplete:function(){
							$('input[name=item_ids]').click(function(){console.log('hey there!')});
							var checkboxes = $('input[name=item_ids]');
							console.log('checkboxes');
							console.dir(checkboxes);
							checkboxes.click(function(){
								console.log('asdfasd');
								console.log('clicking');
								var id = $(this).attr('id').replace('delete_item_', '');
								console.log(id);
								var checked = $(this).attr('checked');
								if (checked)
									$('#item_'+id).attr('checked','checked');
								else
									$('#item_'+id).removeAttr('checked');
							});
							console.log('after click assignment');
							var submitting = false;
							$('#delete_items_form').validate({
								submitHandler:function(form)
								{
									if (submitting) return;
									submitting = true;
									$(form).mask("<?php echo lang('common_wait'); ?>");
									$(form).ajaxSubmit({
										success:function(response)
										{
											if(response.success)
											{
												selected_rows = get_selected_rows();
												set_feedback(response.message,'success_message',false);

												$(selected_rows).each(function(index, dom)
												{
													$(this).find("td").animate({backgroundColor:"#FF0000"},1200,"linear")
														.end().animate({opacity:0},1200,"linear",function()
													{
														$(this).remove();
														//Re-init sortable table as we removed a row
														update_sortable_table();

													});
												});
											}
											else
											{
												set_feedback(response.message,'error_message',true);
											}
											$.colorbox.close();
											submitting = false;
										},
										dataType:'json'
									});

								},
								errorLabelContainer: "#error_message_box",
								wrapper: "li",
								rules:
								{
								},
								messages:
								{
								}
							});
						}
					});
				},
				error:function(data,msg){
					var response = JSON.parse(data.response);
					if(response.details){
						alert(response.details);
					} else {
						alert("Unknown error while deleting item");
					}
				}
			});
		}
		else
		{
			alert('<?php echo lang($controller_name."_none_selected")?>');
		}
    });
    enable_bulk_edit('<?php echo lang($controller_name."_none_selected")?>');
    enable_cleanup('<?php echo lang("items_confirm_cleanup")?>');

	$('#barcode_details').colorbox({width:550, label:'Barcode Details'});
    $('#barcode_details').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("items/barcode_details");?>/' + selected.join('~')+'/5267');
    });

	$('#generate_barcode_labels').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("items/generate_barcode_labels");?>/'+selected.join('~'));
    });
    //$('#pro_shop').button();
    //$('#restaurant').button();
});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$.tablesorter.addParser({ 
	        // set a unique id 
	        id: 'float', 
	        is: function(s) { 
	            // return false so this parser is not auto detected 
	            return false; 
	        }, 
	        format: function(s) { 
	            // format your data for normalization 
	            return s;//.toLowerCase().replace(/good/,2).replace(/medium/,1).replace(/bad/,0); 
	        }, 
	        // set type, either numeric or text 
	        type: 'numeric' 
	    });
	    $.tablesorter.addParser({ 
		    // set a unique id 
		    id: 'items', 
		    is: function(s) { 
		        // return false so this parser is not auto detected 
		        return false; 
		    }, 
		    format: function(s) { 
		        // format your data for normalization 
		        return s.toLowerCase().replace(/<[^>]+>/, ''); 
		    }, 
		    // set type, either numeric or text 
		    type: 'text' 
		}); 

		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				1: { sorter:'items'},
				8: { sorter: 'float'},
				9: { sorter: false},
				10: { sorter: false}
			}

		});
	}
}

function post_item_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.item_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.item_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

function post_bulk_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		set_feedback(response.message,'success_message',false);
		setTimeout(function(){window.location.reload();}, 2500);
	}
}
</script>

<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php echo 
					anchor("$controller_name/view_new_item/-1/width~1100/0",
					lang($controller_name.'_new'),
					array('class'=>'none new',
                                              'id'=>'new_item',
						'title'=>lang($controller_name.'_new')));

                                    
                                    echo 
					anchor("$controller_name/bulk_edit/width~1100",
					lang("items_bulk_edit"),
					array('id'=>'bulk_edit',
						'class' => 'bulk_edit_inactive',
						'title'=>lang('items_edit_multiple_items'))); 
                   if ($this->config->item('webprnt_label_ip') == '') {            
				 echo 
					anchor("$controller_name/generate_barcode_labels",
					lang("common_barcode_labels"),
					array('id'=>'generate_barcode_labels', 
						'class' => 'generate_barcodes_inactive',
						'target' =>'_blank',
						'title'=>lang('common_barcode_labels'))); 
				   }
				 echo 
					anchor("$controller_name/barcode_details",
					$this->config->item('webprnt_label_ip') != '' ? lang('common_print_barcodes') : lang('common_barcode_sheet'),
					array('id'=>'barcode_details', 
						'class' => 'barcode_details_inactive',
						'target' =>'_blank',
						'title'=>$this->config->item('webprnt_label_ip') != '' ? lang('common_print_barcodes') : lang('common_barcode_sheet'))); 
				echo anchor("$controller_name/excel_import/",
					lang('items_import'),
					array('class'=>'colbox none import',
						'title'=>lang('items_import')));
				echo anchor("$controller_name/excel_export",
					lang('items_export'),
					array('class'=>'none import'));
				echo anchor("$controller_name/excel_export/1",
					lang('items_export_food_and_beverage'),
					array('class'=>'none import'));
				 echo 
					anchor("modifiers/index/width~1000",
					"Manage Modifiers",
					array('id'=>'manage_modifiers', 
						'class' => 'colbox none',
						'target' =>'_blank',
						'title'=>'Manage Modifiers'));
				echo 
					anchor("meal_courses/index/width~450",
					"Manage Meal Courses",
					array('id'=>'meal_courses', 
						'class' => 'colbox none',
						'target' =>'_blank',
						'title'=>'Manage Meal Courses'));
				echo 
					anchor("items/view_inventory_audit/width~1100",
					"Inventory Audit",
					array('id'=>'inventory_audit', 
						'class' => 'colbox none',
						'target' =>'_blank',
						'title'=>'Inventory Audit'));
				?>

                <!--?php echo 
					anchor("$controller_name/cleanup",
					lang("items_cleanup_old_items"),
					array('id'=>'cleanup', 
						'class'=>'cleanup')); 
				?-->
				<?php if(!empty($_GET['in_iframe'])){ ?>
				<a href="" class="manage-receipt-content">Receipt Agreements</a>
				<?php } ?>

				<?php echo
				anchor("$controller_name/confirm_delete",
					lang("common_delete"),
					array('id'=>'delete',
						'class'=>'delete_inactive'));

				?>
			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
                    
                 <?php
                    if ($has_restaurant)
                    {
                        echo form_open("$controller_name/item_type_choices", array('id'=>'switch_items'));
                 ?>
		                <div id='item_view_buttons'>
				       		<ul>
				       			<!--li id='month_view'>Month</li-->
				       			<li id='pro_shop_view' class='selected'>Pro Shop</li>
				       			<li id='food_and_beverage_view' class='last'>Food And Beverage</li>
				       		</ul>
				       </div>  
				       <div style='display:none'>
				       <input type='radio' id='pro_shop' name='item_type' value='0' checked/><label for='pro_shop' id='pro_shop_label'>Pro Shop</label>  
                       <input type='radio' id='restaurant' name='item_type' value='1'/><label for='restaurant' id='restaurant_label'>Food And Beverage</label>
                	</div>
                    </form>
                <?php
                    }              
                ?>
            
                          <!--</div>-->
                         <div id='table_top'>
				<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
                                        <input type="hidden" name="search_type" id="search_type" value="0"/>
					<input type="text" name ='search' id='search' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form>
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>
