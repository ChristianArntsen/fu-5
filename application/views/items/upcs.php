<ul id="error_message_box"></ul>
<?php
echo form_open('items/close', array('id'=>'item_upcs_form'));
?>
<fieldset id="item_upcs_basic_info">
<legend><?php echo lang("items_additional_upcs"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label(lang('items_new_upc').':', 'new_upc',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'new_upc',
		'id'=>'new_upc',
		'value'=>'')
	);?>
	<?php
	echo form_button(array(
		'name'=>'add_upc',
		'id'=>'add_upc',
		'content'=>lang('common_add'),
		'class'=>'submit_button ')
	);
	?>
	</div>
</div>
<table id='upc_list'>
	<tbody>
		<?php
			foreach ($upcs as $upc) {
				echo "<tr id='upc_{$upc['upc_id']}'><td class='remove_upc'>x</td><td class='upc_code'>{$upc['upc']}</td></tr>";
			}
		?>
	</tbody>
</table>
<?php
echo form_submit(array(
	'name'=>'close_upc',
	'id'=>'close_upc',
	'value'=>lang('common_close'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<style>
	#new_upc {
		width:184px;
	}
	.remove_upc {
		font-size: 18px;
		color: red;
		padding: 0px 0px 0px 24px;
		cursor:pointer;
	}
	.upc_code {
		font-size: 18px;
	}
</style>
<script>
	$('#add_upc').click(function(e){
		e.preventDefault();
		upc.add();
	});
	$('#close_upc').click(function(e){
		e.preventDefault();
		$.colorbox2.close();
	});
	$('#new_upc').keydown(function(event)
	{
		if (event.keyCode == 13) {
			event.preventDefault();
			upc.add();
		}
	});
	function reinitialize_remove_upc_buttons(){
		$('.remove_upc').off().on('click', function (e){
			var tr_id = $(this).parent().attr('id');
			var upc_id = tr_id.replace('upc_', '');
			upc.remove(upc_id);
		});
	};
	reinitialize_remove_upc_buttons();
	var upc = {
		add:function(){
			var new_upc = $('#new_upc').val();
			var item_id = "<?=$item_id?>";
			// AJAX upc to server
			if (new_upc == '')
			{
				alert('Valid UPC code required');
				return;
			}
			$.ajax({
	        	type: "POST",
	        	url: "index.php/items/add_upc/"+new_upc,
	        	data: 'item_id='+item_id,
	        	success: function(response){
	        		if (response.success) {
		           		// Add new UPC to list
						$('#upc_list tbody').append("<tr id='upc_"+response.upc_id+"'><td class='remove_upc'>x</td><td class='upc_code'>"+new_upc+"</td></tr>");
						$('#new_upc').val('');
						$.colorbox2.resize();
						reinitialize_remove_upc_buttons();
					}
					else {
						alert('Error: Could not add UPC, most likely this is a duplicate UPC');
					}
			    },
	            dataType:'json'
	        });
		},
		remove:function(upc_id){
			// AJAX request to server
			$.ajax({
	        	type: "POST",
	        	url: "index.php/items/remove_upc/"+upc_id,
	        	data: '',
	        	success: function(response){
					if (response.success) {
						// Remove UPC from list
						$('#upc_'+upc_id).remove();
						$.colorbox2.resize();
					}
					else {
						alert('Error: Could not delete UPC');
					}
			    },
	            dataType:'json'
	        });
		}
	};
</script>