<?php
echo form_open("$controller_name/generate_barcodes",array('id'=>'barcodes_form', 'target'=>'_blank'));
?>
<?php if ($this->config->item('webprnt_label_ip') == '') { ?>
<fieldset id="barcode_basic_info">
<legend><?php echo lang("items_barcode_general_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label('Avery Label Size', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_radio('label_size','5267',TRUE);?> <img src='../images/pieces/label_5267.png'/>
	<?php echo form_radio('label_size','5160',TRUE);?>  <img src='../images/pieces/label_5160.png'/>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label("Start printing at label".':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'start_index',
		'id'=>'start_index',
		'value'=>1)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label("Top Margin".':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'top_margin',
		'id'=>'top_margin',
		'placeholder'=>'(optional)',
		'value'=>'')
	);?>
	</div>
</div>
</fieldset>
<?php } ?>
<fieldset id="barcode_basic_info">
    <div class="field_row clearfix">
        <?php echo form_label("Show Additional Price".':', 'additional_price',array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array(
                    'name'=>'additional_price',
                    'id'=>'additional_price',
                    'placeholder'=>'Label (MSRP)',
                    'value'=>'',
                    'style'=>'width:100px')
            );?>
            <?php echo form_input(array(
                    'name'=>'additional_price_percent',
                    'id'=>'additional_price_percent',
                    'placeholder'=>'%',
                    'value'=>'',
                    'style'=>'width:40px')
            );?>
        </div>
    </div>
</fieldset>
<fieldset id="barcode_basic_info">
<legend><?php echo lang("items_barcode_quantity_information"); ?></legend>
<?php foreach ($items as $item) {?>
<div class="field_row clearfix">
<?php echo form_label($item['name'].':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'barcode_quantity',
		'id'=>$item['id'],
		'value'=>1)
	);?>
	</div>
</div>
<?php } ?>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>$this->config->item('webprnt_label_ip') != '' ? lang('common_print') : lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<?php if ($this->config->item('webprnt_label_ip') == '') { ?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$.colorbox.resize();
	var submitting = false;
    $('#barcodes_form').submit(function(){
		var count = 0;
		var item_ids = new Array();
		$('input[name=barcode_quantity]').each(function(index){
			count = $(this).val();
			while (count > 0)
			{
				item_ids.push($(this).attr('id'));
				count--;
			}
		});
		var label_size = $('#barcodes_form input[name=label_size]:checked').val();
		var start_index = $('#start_index').val();
		var top_margin = $('#top_margin').val();
        var additional_price = $('#additional_price').val() != '' ? $('#additional_price').val() : 0;
        var additional_price_percent = $('#additional_price_percent').val() != '' ? $('#additional_price_percent').val() : 0;
        $('#barcodes_form').attr('action','<?php echo site_url("$controller_name/generate_barcodes");?>/'+item_ids.join('~')+'/'+label_size+'/'+start_index+'/'+additional_price+'/'+additional_price_percent+'/'+top_margin);
		return true;
	});
});
</script>
<?php } else { ?>
<style>
	#colorbox fieldset input#disable_blackmark, #colorbox fieldset input#enable_blackmark {
		margin: 20px 0px 0px 170px;
	}
</style>
<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	$.colorbox.resize();
	$('#submit').click(function(e){
		e.preventDefault();
		// FETCH barcode info VIA ajax
		var item_ids = {};
		var item_id_list = [];
		var inputs = $('input[name=barcode_quantity]');
		console.dir(inputs);
		for (var ind = 0; ind < inputs.length; ind++) {
			var count = parseInt($(inputs[ind]).val());
			var item_id = parseInt($(inputs[ind]).attr('id'));
			if (item_id > 0 && count > 0) {	
				item_id_list.push(item_id);
				item_ids[item_id] = count;
			}
		}
		console.dir(item_ids);
		//return;
		$.ajax({
	        type: "POST",
	        url: "index.php/<?=$controller_name?>/get_multiple_info",
	        data: {'items[]':item_id_list},
	        dataType:'json',
	        success: function(response){
	        	if (response.success && response.items.length > 0) {
	           		// Generate barcodes VIA webprnt
	           		var label_data = '';
	           		var items = response.items
	           		for (var i in items) {
	           			for (var c = 0; c < item_ids[items[i].item_id]; c++) {
	           				var is_last = (items.length == parseInt(i) + 1 && item_ids[items[i].item_id] == parseInt(c) + 1);
                            var price_string = items[i].unit_price;
                            var additional_price = '';
                            if ($('#additional_price_percent').val() != '') {
                                additional_price = $('#additional_price').val()+' $'+(items[i].unit_price * (100 + parseFloat($('#additional_price_percent').val()))/100).toFixed(2);
                            }
		           			label_data = webprnt.build_barcode_label(label_data, items[i].name, items[i].long_item_id, price_string, is_last, additional_price);
		           		}
	           		}
	           		webprnt.print(label_data,window.location.protocol + "//" + "<?=$this->config->item('webprnt_label_ip')?>/StarWebPRNT/SendMessage");
	           		webprnt.print_all(window.location.protocol + "//" + "<?=$this->config->item('webprnt_label_ip')?>/StarWebPRNT/SendMessage");
	           		//webprnt.print_barcodes(label_data);
	           		// Print barcodes VIA webprnt
					//alert('going to print out labels here');
					// Close window
					$.colorbox.close();
				}
				else {
					alert('There was a problem printing out your barcodes. Please try again.');
				}
	        }
	    });
	});
});
</script>	
<?php } ?>
