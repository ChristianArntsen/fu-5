<style>
ul.item-printers {
	display: block;
	overflow: hidden;
	padding: 0px 0px 0px 185px;
	margin: 5px 0px 15px 0px;
	min-width: 200px;
}

ul.item-printers li {
	display: block;
	margin: 6px 0px 0px 0px;
	overflow: hidden;
	height: 16px;
}

ul.item-printers li a.delete, 
#item_customer_groups a.delete {
	color: red;
	font-weight: bold;
	float: left;
	display: block;
	line-height: 16px;
	margin-right: 10px;
	cursor: pointer;
}

ul.item-printers li span.name {
	display: block;
	float: left;
	line-height: 16px;
}

ul.item-printers li a.delete:hover {
	cursor: pointer;
	text-decoration: underline;
}

tr.modifier-placeholder {
	border: 1px solid #ead979;
	background: #fcf2c2;
	height: 30px;
	width: 500px;
}

div.sort-button {
	border: 1px solid #222;
	background: #FFF;
	padding: 3px 6px;
	cursor: move;
}

label.item_type {
	margin-bottom: 75px;
}

#item_customer_groups {
	border-bottom: 1px solid #DDD;
	margin-bottom: 10px;
}

div.item_customer_group {
	display: block;
	font-size: 14px;
	padding-bottom: 6px;
	margin-left: 10px;
	margin-right: 10px;
}
</style>
<script>
$('#select-image').colorbox2({'maxHeight':750,'width':1100});
$(function(){
	$(document).unbind('changeImage').bind('changeImage', function(event){
		$.post('<?php echo site_url('items/save_image'); ?>/<?php echo $item_info->item_id; ?>', {image_id: event.image_id}, function(response){
			$('#image-info img').attr('src', response.thumb_url);
			$('#select-image').attr('href','<?php echo site_url('upload'); ?>/index/items?crop_ratio=1&image_id=' + event.image_id);
			$.colorbox2.close();
		},'json');
	});

	$('#remove-image').click(function(event){
		var url = $(this).attr('href');

		if(confirm('Remove this image?')){

			$.post(url, {image_id:0}, function(response){
				$('#image-info img').attr('src', response.thumb_url);
				$('#select-image').attr('href','<?php echo site_url('upload'); ?>/index/items?crop_ratio=1&image_id=' + response.image_id);
			},'json');
		}
		return false;
	});
});
</script>
<ul id="error_message_box"></ul>
<?php
echo form_open('items/save/'.$item_info->item_id, array('id'=>'item_form'));
?>
<fieldset id="item_basic_info">
<legend><?php echo lang("items_basic_information"); ?></legend>
<div class='left_side'>
<div class="field_row clearfix">
<?php echo form_label(lang('items_item_number').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'item_number',
		'id'=>'item_number',
		'value'=>$item_info->item_number)
	);?>
	</div>
</div>
<?php if ($item_info->item_id) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('items_additional_upcs').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php
	echo form_button(array(
		'name'=>'manage_upcs',
		'id'=>'manage_upcs',
		'content'=>lang('common_manage'),
		'class'=>'submit_button ')
	);
	?>
	</div>
</div>
<?php } ?>
<div class="field_row clearfix service-fee">
<?php echo form_label(lang('items_gl_code').':', 'gl_code',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'gl_code',
		'id'=>'gl_code',
		'value'=>$item_info->gl_code)
	);?>
	</div>
</div>

<div class="field_row clearfix service-fee">
<?php echo form_label(lang('items_name').':<span class="required">*</span>', 'name',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'id'=>'name',
		'value'=>$item_info->name)
	);?>
	</div>
</div>

<div class="field_row clearfix service-fee">
<?php echo form_label(lang('items_department').':<span class="required">*</span>', 'department',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'department',
		'id'=>'department',
		'value'=>$item_info->department)
	);?>

	</div>
</div>

<div class="field_row clearfix service-fee">
<?php echo form_label(lang('items_category').':<span class="required">*</span>', 'category',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'category',
		'id'=>'category',
		'value'=>$item_info->category)
	);?>
	</div>
</div>

<div class="field_row clearfix service-fee">
<?php echo form_label(lang('items_subcategory').':', 'subcategory',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'subcategory',
		'id'=>'subcategory',
		'value'=>$item_info->subcategory)
	);?>

	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_supplier').':<span class="required">*</span>', 'supplier',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('supplier_id', $suppliers, $selected_supplier);?>
	</div>
</div>

<?php if($item_info->food_and_beverage == 0){ ?>	
<div class="field_row clearfix service-fee">
	<?php echo form_label('Item Type', 'item_type', array('class'=>'wide item_type')); ?>
	<label class='form_field'>
		<?php echo form_radio(array(
			'name'=>'item_type',
			'id'=>'item_type',
			'value'=>'item',
			'checked'=>(!$item_info->is_giftcard && !$item_info->is_pass)? 1 : 0,
		));?> 
		Item
	</label>
	<label class='form_field'>
		<?php echo form_radio(array(
			'name'=>'item_type',
			'id'=>'item_type',
			'value'=>'giftcard',
			'checked'=>($item_info->is_giftcard)? 1 : 0)
		);?> 
		Giftcard
	</label>
	<label class='form_field'>
		<?php echo form_radio(array(
				'name'=>'item_type',
				'id'=>'item_type',
				'value'=>'pass',
				'checked'=>($item_info->is_pass)? 1 : 0)
		);?>
		Member Pass
	</label>
	<label class='form_field'>
		<?php echo form_radio(array(
				'name'=>'item_type',
				'id'=>'item_type',
				'value'=>'service_fee',
				'checked'=>(isset($item_info->is_service_fee) && $item_info->is_service_fee)? 1 : 0)
		);?>
		Service Fee
	</label>
</div>
<?php } ?>

<?php if((int) $this->config->item('quickbooks') > 0){ ?>
<div id='quickbooks_accounts' style='display:none; overflow: hidden; display: block; width: 450px;'>
	<div class="field_row clearfix">
	<?php echo form_label('Income Acct.', 'quickbooks_income', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown(
			'quickbooks_income',
			$quickbooks_accounts,
			$item_info->quickbooks_income
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('COGS Acct.', 'quickbooks_cogs', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown(
			'quickbooks_cogs',
			$quickbooks_accounts,
			$item_info->quickbooks_cogs
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Asset Acct.', 'quickbooks_assets', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown(
			'quickbooks_assets',
			$quickbooks_accounts,
			$item_info->quickbooks_assets
		);?>
		</div>
	</div>
</div>
<script>
$('#quickbooks_accounts').expandable({
	title : 'QuickBooks Account Maps'
});
</script>
<?php } ?>

</div>
<div class='right_side'>
<div class="field_row clearfix">
<?php echo form_label('Inactive:', 'inactive',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'inactive',
		'id'=>'inactive',
		'value'=>1,
		'checked'=>($item_info->inactive)? 1 : 0)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('items_cost_price').':<span class="required">*</span>', 'cost_price',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'cost_price',
		'size'=>'8',
		'id'=>'cost_price',
		'value'=>$item_info->cost_price)
	);?>
	</div>
</div>

	<div class="field_row clearfix service-fee">
		<?php echo form_label(lang('items_unit_price').':<span class="required">*</span>', 'unit_price',array('class'=>' wide')); ?>
		<div class='form_field'>
			<?php echo form_input(array(
					'name'=>'unit_price',
					'size'=>'8',
					'id'=>'unit_price',
					'value'=>$item_info->unit_price)
			);?>
		</div>
	</div>

	<div class="field_row clearfix service-fee-only">
		<?php echo form_label('Percent Price'.':<span class="required">*</span>', 'unit_price',array('class'=>' wide')); ?>
		<div class='form_field'>
			<?php echo form_input(array(
					'name'=>'parent_item_percent',
					'size'=>'8',
					'id'=>'parent_item_percent',
					'value'=>isset($item_info->parent_item_percent)?$item_info->parent_item_percent:0)
			);?>
		</div>
	</div>

	<div class="field_row clearfix service-fee-only">
		<?php echo form_label('Whichever is'.':', 'whichever_is',array('class'=>' wide')); ?>
		<div class='form_field'>
			<?php echo form_radio(array(
					'name'=>'whichever_is',
					'value'=>'more',
					'checked'=>(!isset($item_info->whichever_is) || ($item_info->whichever_is === 'more'))? 1 : 0)
			);?> More
			<?php echo form_radio(array(
					'name'=>'whichever_is',
					'value'=>'less',
					'checked'=>(isset($item_info->whichever_is) && $item_info->whichever_is == 'less')? 1 : 0)
			);?> Less
		</div>
	</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_max_discount').':', 'max_discount',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'max_discount',
		'size'=>'8',
		'id'=>'max_discount',
		'value'=>$item_info->max_discount?$item_info->max_discount:100)
	);?>
	% - Min Price $<span id='min_price'></span>
	</div>
</div>

<div class="field_row clearfix service-fee">
<?php echo form_label('Customer Groups', 'customer_group_menu');  ?>
	<div class='form_field'>
	<?php echo form_dropdown('customer_group_menu', $customer_group_menu, ''); ?>
	</div>
</div>
<div id="item_customer_groups">
<?php foreach($item_info->customer_groups as $group){ ?>
	<div class='item_customer_group' data-group-id="<?php echo $group['group_id']; ?>">
		<strong><?php echo $group['label']; ?></strong>
		<?php if($group['item_cost_plus_percent_discount'] === null){ ?>
		- <em>No discount</em>
		<?php }else{ ?>
		- <em>Cost +<?php echo round($group['item_cost_plus_percent_discount'], 2); ?>% discount</em>
		<?php } ?>
		<a class="delete">X</a>
		<input type="hidden" name="customer_groups[]" value="<?php echo $group['group_id']; ?>">
	</div>
<?php } ?>
</div>

<div class="field_row clearfix">
	<label for="item_tax_included">Unit Price Includes Tax</label>
	<div class='form_field'>
		<input type="hidden" value="0" name="unit_price_includes_tax">
		<input type="checkbox" value='1' name="unit_price_includes_tax" id="item_tax_included" <?php if(!empty($item_info->unit_price_includes_tax)){ echo 'checked'; } ?>>
	</div>
</div>

<div class="field_row clearfix service-fee">
<?php echo form_label(lang('items_tax_1').':', 'tax_percent_1',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'tax_names[]',
		'id'=>'tax_name_1',
		'size'=>'8',
		'value'=> isset($item_tax_info[0]['name']) ? $item_tax_info[0]['name'] : $this->config->item('default_tax_1_name'))
	);?>
	<?php echo form_input(array(
		'name'=>'tax_percents[]',
		'id'=>'tax_percent_name_1',
		'size'=>'3',
		'value'=> isset($item_tax_info[0]['percent']) ? $item_tax_info[0]['percent'] : $default_tax_1_rate)
	);?>
	%
	<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
	</div>
</div>

<div class="field_row clearfix service-fee">
<?php echo form_label(lang('items_tax_2').':', 'tax_percent_2',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'tax_names[]',
		'id'=>'tax_name_2',
		'size'=>'8',
		'value'=> isset($item_tax_info[1]['name']) ? $item_tax_info[1]['name'] : $this->config->item('default_tax_2_name'))
	);?>
	<?php echo form_input(array(
		'name'=>'tax_percents[]',
		'id'=>'tax_percent_name_2',
		'size'=>'3',
		'value'=> isset($item_tax_info[1]['percent']) ? $item_tax_info[1]['percent'] : $default_tax_2_rate)
	);?>
	%
	<?php echo form_checkbox('tax_cumulatives[]', '1', isset($item_tax_info[1]['cumulative']) && $item_tax_info[1]['cumulative'] ? (boolean)$item_tax_info[1]['cumulative'] : (boolean)$default_tax_2_cumulative); ?>
    <span class="cumulative_label">
	<?php echo lang('common_cumulative'); ?>
    </span>
	</div>
</div>
<div id='additional_taxes' style='display:none; overflow: hidden; display: block; width: 450px;'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('items_tax_3').':', 'tax_percent_3',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_3',
			'size'=>'8',
			'value'=> isset($item_tax_info[2]['name']) ? $item_tax_info[2]['name'] : $this->config->item('default_tax_3_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_3',
			'size'=>'3',
			'value'=> isset($item_tax_info[2]['percent']) ? $item_tax_info[2]['percent'] : $default_tax_3_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>
	<div class="field_row clearfix service-fee">
	<?php echo form_label(lang('items_tax_4').':', 'tax_percent_4',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_4',
			'size'=>'8',
			'value'=> isset($item_tax_info[3]['name']) ? $item_tax_info[3]['name'] : $this->config->item('default_tax_4_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_4',
			'size'=>'3',
			'value'=> isset($item_tax_info[3]['percent']) ? $item_tax_info[3]['percent'] : $default_tax_4_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>

	<div class="field_row clearfix service-fee">
	<?php echo form_label(lang('items_tax_5').':', 'tax_percent_5',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_5',
			'size'=>'8',
			'value'=> isset($item_tax_info[4]['name']) ? $item_tax_info[4]['name'] : $this->config->item('default_tax_5_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_5',
			'size'=>'3',
			'value'=> isset($item_tax_info[4]['percent']) ? $item_tax_info[4]['percent'] : $default_tax_5_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>

	<div class="field_row clearfix service-fee">
	<?php echo form_label(lang('items_tax_6').':', 'tax_percent_6',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_6',
			'size'=>'8',
			'value'=> isset($item_tax_info[5]['name']) ? $item_tax_info[5]['name'] : $this->config->item('default_tax_6_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_6',
			'size'=>'3',
			'value'=> isset($item_tax_info[5]['percent']) ? $item_tax_info[5]['percent'] : $default_tax_6_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>
</div>
<script>
	$('#additional_taxes').expandable({
		title : 'Additional Taxes'
	});
</script>
	<div class="field_row clearfix service-fee">
		<?php echo form_label(lang('items_force_tax').':', 'force_tax',array('class'=>' wide')); ?>
		<div class='form_field'>
			<?php echo form_radio(array(
					'name'=>'force_tax',
					'value'=>null,
					'checked'=>(!isset($item_info->force_tax) || ($item_info->force_tax !== true && $item_info->force_tax !== false))? 1 : 0)
			);?> Normal
			<?php echo form_radio(array(
					'name'=>'force_tax',
					'value'=>1,
					'checked'=>(isset($item_info->force_tax) && $item_info->force_tax == true)? 1 : 0)
			);?> Always Tax
			<?php echo form_radio(array(
					'name'=>'force_tax',
					'value'=>0,
					'checked'=>(isset($item_info->force_tax) && $item_info->force_tax == false)? 1 : 0)
			);?> Never Tax
		</div>
	</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_quantity').':<span class="required">*</span>', 'quantity',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php
		$disabled = ($item_info->is_unlimited)?'disabled':'';
	echo form_input(array(
		'name'=>'quantity',
		'id'=>'quantity',
		'size'=>'8',
		'value'=>($item_info->is_unlimited)?0:$item_info->quantity,
		$disabled=>$disabled
		)
	);?>
	<?php echo form_checkbox(array(
		'name'=>'is_unlimited',
		'id'=>'is_unlimited',
		'value'=>1,
		'checked'=>($item_info->is_unlimited)? 1 : 0)
	);?>
	<span class="cumulative_label">
	<?php echo lang('items_is_unlimited');?>
	</span>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_reorder_level').':<span class="required">*</span>', 'reorder_level',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'reorder_level',
		'id'=>'reorder_level',
		'size'=>'8',
		'value'=>($item_info->is_unlimited)?0:$item_info->reorder_level,
		$disabled=>$disabled
		)
	);?>
	</div>
</div>
<?php if ($this->config->item('erange_id')) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('items_erange_size').':', 'supplier',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('erange_size', array('0'=>'0','1'=>'1','2'=>'2','3'=>'3'), $item_info->erange_size);?>
	</div>
</div>
<?php } ?>
</div>
<div class='clear'></div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_description').':', 'description',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$item_info->description,
		'rows'=>'3',
		'cols'=>'17')
	);
        ?>
        <input type="hidden" name="food_and_beverage" id='food_and_beverage' value='<?php echo $item_info->food_and_beverage; ?>'/>
	</div>
</div>

<div id="pass-settings" style="<?php if($item_info->is_pass == 0){ ?>display: none;<?php } ?>">
    Pass settings have moved. To edit passes you must go to our new item page. <a href="<?php echo site_url('v2/home#items'); ?>">Click here to go to the new item page.</a>
</div>

<style>
#manage_upcs {
	margin:0;
	height:32px;
}
#modifiers_container .ui-widget-content, #service_fees_container .ui-widget-content {
	padding: 10px;
}

#modifiers_container th {
	text-align: left;
	padding: 5px 0px;
}

#modifiers_container td {
	padding: 2px 0px 2px 0px;
}

#modifiers_container .submit_button {
	margin: 5px 5px 0px 0px;
	height: 20px;
	line-height: 20px;
	color: white;
	font-size: 12px;
}

input.modifier_auto_select {
	display: block;
	margin: 0 auto;
}
#image-info img{
	border-radius:300px;
}
</style>
<div id='food_and_beverage_item_attributes' style='display:none;'>
	
	<div class="field_row clearfix">
	<?php echo form_label('Menu Category', 'menu_category', array('class'=>' wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'menu_category',
			'id'=>'menu_category',
			'value'=>$menu_category_name)
		);?>
		<a href="#" id="menu_category_copy">Use Above Category</a>
		</div>
		
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Menu Subcategory', 'menu_subcategory', array('class'=>' wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'menu_subcategory',
			'id'=>'menu_subcategory',
			'value'=>$menu_subcategory_name)
		);?>
		<a href="#" id="menu_subcategory_copy">Use Above Subcategory</a>
		</div>
	</div>
    <!-- NEED TO HIDE IT UNTIL RELEASE DAY -->
    <div class="field_row clearfix">
        <?php echo form_label('Button Color', 'button_color', array('class'=>' wide')); ?>
        <div class='form_field'>
            <input class='button_color color-picker' name="button_color" value="<?=$item_info->button_color?>" type="text" />
        </div>
    </div>
	<div class="field_row clearfix">
	<?php echo form_label('Do not print to kitchen', 'do_no_print', array('class'=>' wide')); ?>
		<div class='form_field'>
		<?php echo form_checkbox(array(
			'name'=>'do_not_print',
			'id'=>'do_not_print',
			'value'=>1,
			'checked'=>($item_info->do_not_print)? 1 : 0)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Do not print on customer receipt', 'do_not_print_customer_receipt', array('class'=>' wide')); ?>
		<div class='form_field'>
		<?php echo form_checkbox(array(
			'name'=>'do_not_print_customer_receipt',
			'id'=>'do_not_print_customer_receipt',
			'value'=>1,
			'checked'=>($item_info->do_not_print_customer_receipt)? 1 : 0)
		);?>
		</div>
	</div>
<?php if($this->session->userdata('multiple_printers') == 0){ ?>
	<div class="field_row clearfix">
	<?php echo form_label(lang('items_kitchen_printer').':', 'kitchen_printer',array('class'=>' wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('kitchen_printer', array("1"=>'Hot', '2'=>'Cold'), $item_info->kitchen_printer);?>
		</div>
	</div>
<?php }else{ ?>
	<div class="field_row clearfix">
	<?php echo form_label('Printer Groups', 'printer_menu', array('class'=>' wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('printer_menu', $printer_menu, null);?>
		</div>
		<ul class="item-printers">
		<input type="hidden" name="printer_groups[]" value="">
		<?php if(!empty($item_info->printer_groups)){ 
		foreach($item_info->printer_groups as $printer_group){ ?>
		<li data-printer-group-id="<?php echo $printer_group['printer_group_id']; ?>">
			<input type="hidden" name="printer_groups[]" value="<?php echo $printer_group['printer_group_id']; ?>">
			<a class="delete">X</a>
			<span class="name"><?php echo $printer_group['label']; ?></span>
		</li>
		<?php } } ?>
		</ul>
	</div>
<?php } ?>

<div class="field_row clearfix">
<?php echo form_label('Meal Course:', 'meal_course_id', array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('meal_course_id', $meal_courses, $item_info->meal_course_id);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Prompt Meal Course:', 'prompt_meal_course', array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'prompt_meal_course',
		'id'=>'prompt_meal_course',
		'value'=>1,
		'checked'=>($item_info->prompt_meal_course)? 1 : 0)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_print_priority').':', 'print_priorty',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('print_priority', array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'), $item_info->print_priority);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('items_is_side').':', 'is_side',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'is_side',
		'id'=>'is_side',
		'value'=>1,
		'checked'=>($item_info->is_side)? 1 : 0)
	);?>
	</div>
</div>
<div id='item_side_options'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('items_upgrade_price').':', 'add_on_price',array('class'=>' wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'add_on_price',
			'size'=>'8',
			'id'=>'add_on_price',
			'value'=>$item_info->add_on_price)
		);?>
		</div>
	</div>
</div>
<div id='item_non_side_options'>
	<div id='sides_container'>
		<div class="field_row clearfix">
		<?php echo form_label(lang('items_number_of_sides').':', 'number_of_sides',array('class'=>' wide')); ?>
			<div class='form_field'>
			<?php echo form_radio(array(
				'name'=>'number_of_sides',
				'value'=>0,
				'checked'=>($item_info->number_of_sides == 0)? 1 : 0)
			);?> 0
			<?php echo form_radio(array(
				'name'=>'number_of_sides',
				'value'=>1,
				'checked'=>($item_info->number_of_sides == 1)? 1 : 0)
			);?> 1
			<?php echo form_radio(array(
				'name'=>'number_of_sides',
				'value'=>2,
				'checked'=>($item_info->number_of_sides == 2)? 1 : 0)
			);?> 2
			<?php echo form_radio(array(
				'name'=>'number_of_sides',
				'value'=>3,
				'checked'=>($item_info->number_of_sides == 3)? 1 : 0)
			);?> 3
			<?php echo form_radio(array(
				'name'=>'number_of_sides',
				'value'=>4,
				'checked'=>($item_info->number_of_sides == 4)? 1 : 0)
			);?> 4
			<span id='edit_sides'>Edit Sides</span>
			</div>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('items_soup_or_salad').':', 'soup_or_salad',array('class'=>' wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('soup_or_salad', array('none'=>'None','soup'=>'Soup','salad'=>'Salad',/*'either'=>'One of Either',*/'both'=>'Both'), $item_info->soup_or_salad);?>
		</div>
	</div>

</div>
<script>
	$('#modifiers tbody').sortable({
		handle: '.sort-button',
		placeholder: 'modifier-placeholder',
		update: function(event, ui){
			console.debug(ui);
		}
	});
	$('#service_fees tbody').sortable({
		handle: '.sort-button',
		placeholder: 'service-fee-placeholder',
		update: function(event, ui){
			console.debug(ui);
		}
	});
</script>
	<div id='modifiers_container' style='display:none; overflow: hidden; display: block; width: 900px;'>
		<label>Add New </label>
		<?php echo form_input(array('name' => "modifier_search", 'value' => '', 'style' => 'width: 345px;', 'class'=>'modifier_search', 'data-placeholder'=>'Search modifiers...')); ?>
		<table id="modifiers">
			<thead>
			<tr>
				<th>&nbsp;</th>
				<th>Name</th>
				<th>Options (Price)</th>
				<th>Default</th>
				<th>&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			<?php if(!empty($modifiers)){ ?>
				<?php foreach($modifiers as $key => $modifier){

					$optionsArray = array();
					$optionsMenu = array('' => '- None -');
					foreach($modifier['options'] as $opt){
						$optionsArray[] = $opt['label'].' ('.to_currency((float) $opt['price']).')';
						$optionsMenu[$opt['label']] = $opt['label'];
					}
					$optionsString = implode(', ',$optionsArray);
					$default_options = $modifier['default'];
					?>
					<tr id="modifier-<?php echo $modifier['modifier_id']; ?>">
						<td class="sort-handle">
							<input type="hidden" class="order" name="modifiers[<?php echo $modifier['modifier_id']; ?>][order]" value="<?php echo $modifier['order']; ?>">
							<div class="sort-button">
								<span class="ui-button-icon-primary ui-icon ui-icon-carat-2-n-s"></span>
							</div>
						</td>
						<td>
							<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][modifier_id]" value="<?php echo $modifier['modifier_id']; ?>" class="modifier_id" />
							<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][multi_select]" value="<?php echo (int) $modifier['multi_select']; ?>" class="multi_select" />
							<div class='form_field'>
								<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][name]", 'value' => $modifier['name'], 'style' => 'width: 200px;', 'class'=>'modifier_name', 'disabled'=>false));?>
							</div>
						</td>
						<td>
							<div class='form_field'>
								<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][options]", 'value' => $optionsString, 'style' => 'width: 450px;', 'class'=>'modifier_options', 'disabled'=>false));?>
							</div>
						</td>
						<td>
							<div class='form_field'>
								<?php if($modifier['multi_select'] == 1){
									foreach($modifier['options'] as $option){

										$checked = false;
										if(in_array($option['label'], $default_options)){
											$checked = true;
										} ?>
										<label style="display: block; height: 20px; line-height: 20px;">
											<?php echo form_checkbox("modifiers[{$modifier['modifier_id']}][default][]", $option['label'], $checked); ?>
											<?php echo $option['label']; ?>
										</label>

									<?php } }else{ ?>
									<?php echo form_dropdown("modifiers[{$modifier['modifier_id']}][default]", $optionsMenu, $modifier['default'], "style='width: 150px;'"); ?>
								<?php } ?>
							</div>
						</td>
						<td>
							<a href="#" class="delete_modifier" style="color: white;">X</a>
						</td>
					</tr>
				<?php } }else{ ?>

			<?php } ?>
			</tbody>
		</table>
	</div>

</div>

	<div id='service_fees_container' style='display:none; overflow: hidden; display: block; width: 900px;'>
		<label>Add New </label>
		<?php echo form_input(array('name' => "service_fee_search", 'value' => '', 'style' => 'width: 345px;', 'class'=>'service_fee_search', 'data-placeholder'=>'Search service fees...')); ?>
		<table id="service_fees">
			<thead>
			<tr>
				<th>&nbsp;</th>
				<th>Name</th>
				<th>Amount</th>
				<th>Percent</th>
				<th>Whichever Is</th>
				<th>&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$ServiceFee = new \fu\service_fees\ServiceFee();
			$fees = $ServiceFee->getServiceFeesForItem($item_info->item_id);
			foreach($fees as $fee){
				echo $fee->getHTML();
			}
			?>
			</tbody>
		</table>
	</div>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
function toggle_pass_usage_restrictions(){
	if(!$('input[name="pass[limit_usage]"]').is(':checked')){
		$('#pass-usage-restriction').find('input,select').attr('disabled', true);
	}else{
		$('#pass-usage-restriction').find('input,select').attr('disabled', null);
	}
}
function update_min_price(){
	var min_price = 0;
	var unit_price = $("#unit_price").val();
	var max_discount = $('#max_discount').val();
	min_price = (unit_price * (100 - max_discount)/100).toFixed(2);
	min_price = min_price < 0 ? 0 : min_price;
	$('#min_price').html(min_price);
}
function display_side_options()
{
	if($('#is_side').is(':checked'))
	{
		$('#item_non_side_options').hide();
		$('#item_side_options').show();
	}
	else
	{
		$('#item_non_side_options').show();
		$('#item_side_options').hide();
	}
}
function add_modifier(modifierId, modifierName){
	var rowCount = $('#modifiers tr').length - 1;
	var newRow = $('#modifier-template').clone();

	newRow.find('input.modifier_id').attr('name', 'modifiers['+rowCount+'][modifier_id]').val('');
	newRow.find('input.modifier_name').attr('name', 'modifiers['+rowCount+'][name]').val(modifierName);
	newRow.find('input.modifier_default_price').attr('name', 'modifiers['+rowCount+'][default_price]').val('');
	newRow.find('input.modifier_options').attr('name', 'modifiers['+rowCount+'][options]').val('');
	newRow.show();
	newRow.attr('id', modifierId);

	$('#modifiers').append(newRow);
}
<?php if(!empty($item_info->item_id )){ ?>
var itemId = <?php echo $item_info->item_id; ?>;
<?php }else{ ?>
var itemId = 0;
<?php } ?>
//validation and submit handling
$(document).ready(function()
{
    jQuery_1_7_2('input.button_color').spectrum({
        preferredFormat: 'hex6',
        allowEmpty: true,
        preferredFormat: "hex",
        showInput: true
    });
	$('#menu_category_copy').on('click', function(){
		$('#menu_category').val( $('#category').val() );
		return false;
	});
	$('#menu_subcategory_copy').on('click', function(){
		$('#menu_subcategory').val( $('#subcategory').val() );
		return false;
	});

	$('#customer_group_menu').on('change', function(){
		if($(this).val() == 0){
			return false;
		}
		var value = $(this).val().split(':');
		var group_id = value[0];
		var cost_plus = value[1];
		var label = $(this).find('option:selected').text();
		
		var discount_html = ' - <em>No discount</em>';
		if(cost_plus != null && cost_plus != ''){
			discount_html = ' - <em>Cost + '+cost_plus+'% discount</em>';
		}
		
		if($('#item_customer_groups').children('div[data-group-id='+group_id+']').length <= 0){
			$('#item_customer_groups').append('<div class="item_customer_group" data-group-id="'+group_id+'">'+
			'<input type="hidden" name="customer_groups[]" value="'+group_id+'">'+
			'<strong>'+label+'</strong>'+discount_html+'<a class="delete">X</a></div>');
		}
		$(this).val(0);
		
		return false;
	});

	$('#item_customer_groups').on('click', 'a.delete', function(e){
		$(e.target).parent('div').remove();
		return false;
	});

	$('input[name="pass[limit_usage]"]').change(function(){
		toggle_pass_usage_restrictions();
	});
	<?php if($item_info->is_service_fee == 0){ ?>
		$('.field_row').show();
		$('.field_row.service-fee-only').hide();
	    $('#service_fees_container').show();
	<?php }else{ ?>
		$('.field_row').hide();
		$('.field_row.service-fee-only').show();
		$('.field_row.service-fee').show();
	    $('#service_fees_container').hide();

	    $('#cost_price').val('0');
		$('#is_unlimited').attr('checked',true);
		$('#quantity').val('0');
		$('#quantity').attr('disabled',true);
		$('#reorder_level').val('0');
		$('#reorder_level').attr('disabled',true);
<?php } ?>
	if($(this).val() == 'pass'){
		$('#pass-settings').show();
	}else{
		$('#pass-settings').hide();
	}
	$('input[name="item_type"]').on('change', function(){
		toggle_pass_usage_restrictions();
		if($(this).val() == 'service_fee'){
			$('.field_row').hide();
			$('.field_row.service-fee-only').show();
			$('.field_row.service-fee').show();
		}else{
			$('.field_row').show();
			$('.field_row.service-fee-only').hide();
		}
	});

	toggle_pass_usage_restrictions();

	$('#printer_menu').on('change', function(){
		if($(this).val() == ''){
			return false;
		}
		
		var printer_group_id = $(this).val();
		var label = $(this).find('option:selected').text();
		
		if($('ul.item-printers').children('li[data-printer-group-id='+printer_group_id+']').length <= 0){
			$('ul.item-printers').append('<li data-printer-group-id="'+printer_group_id+'">'+
			'<input type="hidden" name="printer_groups[]" value="'+printer_group_id+'">'+
			'<a class="delete">X</a><span class="name">'+label+'</span></li>');
		}
		$(this).val('');
		
		return false;
	});
	
	$('ul.item-printers').on('click', 'a.delete', function(e){
		$(e.target).parent('li').remove();
		return false;
	});
	
	$('#unit_price, #max_discount').on('keyup', function(){
		update_min_price();
	});
	update_min_price();
	if ($('#restaurant').attr('checked'))
		$('#food_and_beverage_item_attributes').show();
	display_side_options();
	$('#is_side').change(function(){
		display_side_options();
	});
	$('#edit_sides').click(function(){
		$.colorbox2({href:'index.php/items/view_side_options/<?=$item_info->item_id?>',width:600});
	});
	$('#modifiers_container').expandable({
		title : 'Modifiers'
	});
	$('#service_fees_container').expandable({
		title : 'Service Fees'
	});

	$('a.delete_modifier').die('click').live('click', function(e){
		var row = $(this).parents('tr');
		row.remove();
		return false;
	});

	$('a.delete_service_fee').die('click').live('click', function(e){
		var row = $(this).parents('tr');
		row.remove();
		return false;
	});

	$("input.modifier_search").autocomplete({
		source: '<?php echo site_url('modifiers/search'); ?>',
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 1,
		select: function( event, ui){
			$.post('<?php echo site_url('modifiers/get'); ?>/' + ui.item.value, null, function(response){
				if(response){
					$('#modifiers').append(response);
				}
			});
			$("input.modifier_search").val('');
			return false;
		}
	});

	$("input.service_fee_search").autocomplete({
		source: '<?php echo site_url('service_fees/search'); ?>',
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 1,
		select: function( event, ui){
			$.post('<?php echo site_url('service_fees/getHTML'); ?>/' + ui.item.value, null, function(response){
				if(response){
					$('#service_fees').append(response);
				}
			});
			$("input.service_fee_search").val('');
			return false;
		}
	});

	$('#is_unlimited').bind('change',function(){
		if($('#is_unlimited').attr('checked')) {
			$('#quantity').val('0');
			$('#quantity').attr('disabled',true);
			$('#reorder_level').val('0');
			$('#reorder_level').attr('disabled',true);
		}
		else {
			$('#quantity').val('<?php echo $item_info->quantity?>');
			$('#quantity').attr('disabled',false);
			$('#reorder_level').val('<?php echo $item_info->reorder_level?>');
			$('#reorder_level').attr('disabled',false);

		}
	});
	<?php if ($item_info->item_id) { ?>
	$('#manage_upcs').click(function(e){
		e.preventDefault();
		$.colorbox2({'maxHeight':750,'width':500, 'href':'index.php/items/view_upcs/'+<?=$item_info->item_id?>});
	});
	<?php } ?>
	
	$("#menu_category").autocomplete({
		source: "<?php echo site_url('api/items/menu_categories');?>?api_key=no_limits",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui){
			$(this).val(ui.item.name);
			return false;
		}
		
	}).click(function(){$(this).autocomplete('search','')});

	$("#menu_subcategory").autocomplete({
		source: "<?php echo site_url('api/items/menu_subcategories');?>?api_key=no_limits",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui){
			$(this).val(ui.item.name);
			return false;
		}

	}).click(function(){$(this).autocomplete('search','')});

	var dept = $( "#department" );
	dept.autocomplete({
		source: "<?php echo site_url('items/suggest_department');?>",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0
	});
	dept.click(function(){dept.autocomplete('search','')});

	var cat = $( "#category" );
	cat.autocomplete({
		source: "<?php echo site_url('items/suggest_category');?>",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0
	});
	cat.click(function(){cat.autocomplete('search','')});

	var subcat = $( "#subcategory" );
	subcat.autocomplete({
		source: "<?php echo site_url('items/suggest_subcategory');?>",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0
	});
	subcat.click(function(){subcat.autocomplete('search','')});

	var submitting = false;
    $('#item_form').validate({

		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			
			// IF $('#restautant') is checked, then we need to mark this item as food_and_beverage
			$('#food_and_beverage').val($('#restaurant').attr('checked') ? 1 : 0);
			$(form).mask("<?php echo lang('common_wait'); ?>");
			
			$.each($('#modifiers tbody tr'), function(index, row){
				$(row).find('input.order').val(index);
			});

			$(form).ajaxSubmit({
			success:function(response)
			{
				post_item_form_submit(response);
                submitting = false;
				if (response.success)
					$.colorbox.close();
				else
					$(form).unmask();
			},
			error: function(xhr, type, exception )
			{
				submitting = false;
				var res = JSON.parse(xhr.responseText);
				alert(res.details);
				$(form).unmask();
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			name:"required",
			department:"required",
			category:"required",
			cost_price:
			{
				required:true,
				isFloat:true
			},

			unit_price:
			{
				required:true,
				isFloat:true
			},
			tax_percent:
			{
				required:true,
				number:true
			},
			quantity:
			{
				required:true,
				number:true
			},
			reorder_level:
			{
				required:true,
				number:true
			}
			<?php if($item_info->food_and_beverage == 1){ ?>
			,menu_category:
			{
				required:true
			}
			<?php } ?>
   		},
		messages:
		{
			name:"<?php echo lang('items_name_required'); ?>",
			department:"<?php echo lang('items_department_required'); ?>",
			category:"<?php echo lang('items_category_required'); ?>",
			cost_price:
			{
				required:"<?php echo lang('items_cost_price_required'); ?>",
				number:"<?php echo lang('items_cost_price_number'); ?>"
			},
			unit_price:
			{
				required:"<?php echo lang('items_unit_price_required'); ?>",
				number:"<?php echo lang('items_unit_price_number'); ?>"
			},
			tax_percent:
			{
				required:"<?php echo lang('items_tax_percent_required'); ?>",
				number:"<?php echo lang('items_tax_percent_number'); ?>"
			},
			quantity:
			{
				required:"<?php echo lang('items_quantity_required'); ?>",
				number:"<?php echo lang('items_quantity_number'); ?>"
			},
			reorder_level:
			{
				required:"<?php echo lang('items_reorder_level_required'); ?>",
				number:"<?php echo lang('items_reorder_level_number'); ?>"
			}
			<?php if($item_info->food_and_beverage == 1){ ?>
			,menu_category:
			{
				required: "Menu category is required"
			}
			<?php } ?>
		}
	});
});
</script>
