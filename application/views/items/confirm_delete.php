<?php
echo form_open('items/delete/1',array('id'=>'delete_items_form'));
?>
<fieldset id="delete_items_basic_info">
<legend><?php echo lang("items_items_to_delete"); ?></legend>
<?php foreach ($items as $item) {?>
<div class="field_row clearfix">
	<?php echo form_checkbox(array(
		'name'=>'item_ids[]',
		'id'=>'delete_item_'.$item['id'],
		'value'=>$item['id'],
		'checked'=>'checked')
	);?>
	<div class='form_field'>
	<?php echo form_label($item['name'].':', 'delete_item_'.$item['id'],array('class'=>'wide')); ?>
	</div>
</div>
<?php } 
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_delete'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
/*
 *  THIS PAGE IS LOADED VIA HTML CODE TO COLORBOX... THAT MEANS THAT EVENT LISTENERS DON'T GET INTERPRETED BY JQUERY... NOT SURE WHY... THEY'RE IN THE 'ONCOMPLETE' COLORBOX HOOK
 * 
 */
//validation and submit handling
//$(document).ready(function()
//{

//});
</script>