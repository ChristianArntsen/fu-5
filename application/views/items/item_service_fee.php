<tr>
    <td class="sort-handle">
        <input type="hidden" class="order" name="service_fees[<?php echo $service_fee_id; ?>][order]" value="<?php echo isset($order)?$order:0; ?>">
        <div class="sort-button">
            <span class="ui-button-icon-primary ui-icon ui-icon-carat-2-n-s"></span>
        </div>
    </td>
    <td>
        <input type="hidden" name="service_fees[<?php echo $service_fee_id; ?>][service_fee_id]" value="<?php echo $service_fee_id; ?>" class="modifier_id" />
        <div class='form_field'>
            <?php echo form_input(array('name' => "service_fees[{$service_fee_id}][name]", 'value' => $name, 'style' => 'width: 200px;', 'class'=>'modifier_name', 'disabled'=>false));?>
        </div>
    </td>
    <td>
        <div class='form_field'>
            <?php echo form_input(
                array(
                    'name' => "service_fees[{$service_fee_id}][unit_price]",
                    'value' => $unit_price, 'style' => 'width: 100px;',
                    'class'=>'modifier_options',
                    'disabled'=>false));?>
        </div>
    </td>
    <td>
        <div class='form_field'>
            <?php echo form_input(
                array(
                    'name' => "service_fees[{$service_fee_id}][parent_item_percent]",
                    'value' => $parent_item_percent,
                    'style' => 'width: 100px;',
                    'class'=>'modifier_options',
                    'disabled'=>false));?>
        </div>
    </td>
    <td>
        <div class='form_field'>
            <?php echo form_input(
                array('name' => "service_fees[{$service_fee_id}][whichever_is]",
                    'value' => $whichever_is,
                    'style' => 'width: 100px;',
                    'class'=>'modifier_options',
                    'disabled'=>false));?>
        </div>
    </td>
    <td>
        <a href="#" class="delete_service_fee" style="color: white;">X</a>
    </td>
</tr>