<?php
$appBase = base_url()."frontend/campaign-builder";
if(isset($template) || isset($prefilledTemplate)){
?>
	<script>
		var phpFilledData = {};
		phpFilledData.template = <?php echo isset($template) ? json_encode($template) : '""';?>;
		phpFilledData.prefilledTemplate = <?php echo json_encode($prefilledTemplate);?>;
		phpFilledData.logo = "<?php echo $logo;?>";
		phpFilledData.logo_align = "<?php echo $logo_align;?>";
		phpFilledData.email_hash = "<?php echo $emailHash;?>";
		phpFilledData.campaigns = [];
		phpFilledData.campaigns['course_info'] = <?php echo json_encode($courseInfo);?>;
	</script>

<?php
} else {
?>
<script>
	phpFilledData = {};
	phpFilledData.campaigns = {};
	phpFilledData.campaigns['drafts'] = <?php echo json_encode($draftCampaigns);?>;
	phpFilledData.campaigns['current'] = <?php echo json_encode($currentCampaigns);?>;
	phpFilledData.campaigns['finished'] = <?php echo json_encode($finishedCampaigns);?>;
	phpFilledData.campaigns['course_info'] = <?php echo json_encode($courseInfo);?>;
</script>
<?php
}
?>
<html lang="en" ng-app="cbApp">
<head>
	<title></title>
	<link rel="stylesheet" href="<?php echo $appBase?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $appBase?>/resources/styles/main.css">
	<link rel="stylesheet" href="<?php echo $appBase?>/bower_components/angular-chart.js/dist/angular-chart.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300,100,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?php echo $appBase?>/bower_components/dropzone/downloads/css/dropzone.css">
	<link rel="stylesheet" href="<?php echo $appBase?>/bower_components/dropzone/downloads/css/basic.css">
    <link rel="stylesheet" href="<?php echo $appBase?>/bower_components/tooltipster/css/tooltipster.css">
    <link rel="stylesheet" href="<?php echo $appBase?>/bower_components/chosen/chosen.min.css">
    <link rel="stylesheet" href="<?php echo $appBase?>/bower_components/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/sass/header-bootstrap.css?<?php echo APPLICATION_VERSION; ?>">

	<link rel="stylesheet" href="css/medium-editor.css"> <!-- Core -->
	<link rel="stylesheet" href="css/themes/default.css"> <!-- or any other theme -->

	<link rel="stylesheet" href="<?php echo $appBase?>/bower_components/medium-editor/dist/css/medium-editor.css"> <!-- Core -->
	<link rel="stylesheet" href="<?php echo $appBase?>/bower_components/medium-editor/dist/css/themes/mani.css"> <!-- Core -->
	<link rel="stylesheet" href="<?php echo $appBase?>/campaign-builder/app/template_review/datetimepicker.css"> <!-- Core -->
	<style>
		input.medium-editor-toolbar-select{

		}
		input[type=range] {
			-webkit-appearance: none;
			width: 100%;
			margin: 0px 0;
		}
		input[type=range]:focus {
			outline: none;
		}
		input[type=range]::-webkit-slider-runnable-track {
			width: 100%;
			height: 32px;
			cursor: pointer;
			box-shadow: 0.1px 0.1px 0px #005900, 0px 0px 0.1px #007200;
			background: #0000ff;
			border-radius: 0px;
			border: 0px solid #010101;
		}
		input[type=range]::-webkit-slider-thumb {
			box-shadow: 0px 0px 0px rgba(0, 0, 62, 0.67), 0px 0px 0px rgba(0, 0, 88, 0.67);
			border: 0.4px solid rgba(0, 30, 0, 0.57);
			height: 32px;
			width: 17px;
			border-radius: 0px;
			background: #ffffff;
			cursor: pointer;
			-webkit-appearance: none;
			margin-top: 0px;
		}
		input[type=range]:focus::-webkit-slider-runnable-track {
			background: #8080ff;
		}
		input[type=range]::-moz-range-track {
			width: 100%;
			height: 32px;
			cursor: pointer;
			box-shadow: 0.1px 0.1px 0px #005900, 0px 0px 0.1px #007200;
			background: #0000ff;
			border-radius: 0px;
			border: 0px solid #010101;
		}
		input[type=range]::-moz-range-thumb {
			box-shadow: 0px 0px 0px rgba(0, 0, 62, 0.67), 0px 0px 0px rgba(0, 0, 88, 0.67);
			border: 0.4px solid rgba(0, 30, 0, 0.57);
			height: 32px;
			width: 17px;
			border-radius: 0px;
			background: #ffffff;
			cursor: pointer;
		}
		input[type=range]::-ms-track {
			width: 100%;
			height: 32px;
			cursor: pointer;
			background: transparent;
			border-color: transparent;
			color: transparent;
		}
		input[type=range]::-ms-fill-lower {
			background: #000080;
			border: 0px solid #010101;
			border-radius: 0px;
			box-shadow: 0.1px 0.1px 0px #005900, 0px 0px 0.1px #007200;
		}
		input[type=range]::-ms-fill-upper {
			background: #0000ff;
			border: 0px solid #010101;
			border-radius: 0px;
			box-shadow: 0.1px 0.1px 0px #005900, 0px 0px 0.1px #007200;
		}
		input[type=range]::-ms-thumb {
			box-shadow: 0px 0px 0px rgba(0, 0, 62, 0.67), 0px 0px 0px rgba(0, 0, 88, 0.67);
			border: 0.4px solid rgba(0, 30, 0, 0.57);
			width: 17px;
			border-radius: 0px;
			background: #ffffff;
			cursor: pointer;
			height: 32px;
		}
		input[type=range]:focus::-ms-fill-lower {
			background: #0000ff;
		}
		input[type=range]:focus::-ms-fill-upper {
			background: #8080ff;
		}


	</style>
</head>
<body>

<header app="Marketing"></header>

<div ui-view="header"></div>
<div ui-view="test"></div>

<script src="<?php echo $appBase?>/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular/angular.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
<script src="<?php echo $appBase?>/bower_components/tg-dynamic-directive/src/tg.dynamic.directive.js"></script>
<script src="<?php echo $appBase?>/bower_components/medium-editor/dist/js/medium-editor.js"></script>
<script src="<?php echo $appBase?>/components/mediumExtensions/variables.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular-medium-editor/dist/angular-medium-editor.js"></script>
<script src="<?php echo $appBase?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/dropzone/downloads/dropzone.js"></script>
<script src="<?php echo $appBase?>/bower_components/tooltipster/js/jquery.tooltipster.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular-animate/angular-animate.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/velocity/velocity.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/velocity/velocity.ui.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular-velocity/angular-velocity.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/chosen-build/chosen.jquery.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular-chosen-localytics/chosen.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular-dialog-service/dist/dialogs.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/chartjs/Chart.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular-chart.js/dist/angular-chart.js"></script>
<script src="<?php echo $appBase?>/bower_components/ladda/js/spin.js"></script>
<script src="<?php echo $appBase?>/bower_components/ladda/js/ladda.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular-ladda/dist/angular-ladda.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo $appBase?>/bower_components/lodash/dist/lodash.min.js"></script>


<script src="<?php echo $appBase?>/app/app.js"></script>
<script src="<?php echo $appBase?>/app/dashboard/dashboard.js"></script>
<script src="<?php echo $appBase?>/app/components/createTemplate/createTemplate.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/template_builder.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/template_builder_compiled.js"></script>
<script src="<?php echo $appBase?>/app/template_select/template_select.js"></script>
<script src="<?php echo $appBase?>/app/template_select/contoller.overwrite_dialog.js"></script>
<script src="<?php echo $appBase?>/app/template_review/datetimepicker.js"></script>
<script src="<?php echo $appBase?>/app/template_review/rrule.js"></script>
<script src="<?php echo $appBase?>/app/template_review/nlp.js"></script>
<script src="<?php echo $appBase?>/app/template_overview/template_overview.js"></script>
<script src="<?php echo $appBase?>/app/template_overview/recipients/recipients.js"></script>
<script src="<?php echo $appBase?>/app/template_review/template_review.js"></script>
<script src="<?php echo $appBase?>/app/template_preview/template_preview.js"></script>
<script src="<?php echo $appBase?>/app/view-campaign/view-campaign.js"></script>
<script src="<?php echo $appBase?>/app/view-campaign/directive.campaign-analytics.js"></script>
<script src="<?php echo $appBase?>/app/view-campaign/directive.emaillist.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/directive.template_builder_preview.js"></script>

<script src="<?php echo $appBase?>/components/filters/filters.js"></script>
<script src="<?php echo $appBase?>/components/tooltipster/directive.tooltipster.js"></script>
<script src="<?php echo $appBase?>/components/notifyToast/notifyToast.js"></script>
<script src="<?php echo $appBase?>/components/services/phpDataFactory.js"></script>
<script src="<?php echo $appBase?>/components/services/campaignFactory.js"></script>
<script src="<?php echo $appBase?>/components/services/templateFactory.js"></script>
<script src="<?php echo $appBase?>/components/services/imageFactory.js"></script>

<script src="<?php echo base_url() ?>/js/components/app-header/app-header.js"></script>


<script src="<?php echo $appBase?>/app/template_builder/component.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/components/directive.text.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/components/directive.button.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/components/directive.button.editor.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/components/directive.divider.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/components/directive.image.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/components/directive.image.editor.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/components/directive.spacer.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/directive.section.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/components.controls.js"></script>
<script src="<?php echo $appBase?>/app/template_builder/directive.section.controls.js"></script>
<script src="<?php echo $appBase?>/app/template_select/directive.template_select_preview.js"></script>

<script src="<?php echo $appBase?>/app/template_builder/services/service.editComponent.js"></script>

<script src="<?php echo $appBase?>/app/components/angular-sortable-view/src/angular-sortable-view.js"></script>
<script src="<?php echo $appBase?>/app/components/resizer/directive.resizer.js"></script>
<script src="<?php echo $appBase?>/app/components/resizer/directive.resizer.handle.js"></script>

<script src="<?php echo $appBase?>/app/components/group-selection/directive.group-list.js"></script>
<script src="<?php echo $appBase?>/app/components/group-selection/directive.group-selection.js"></script>

<script src="<?php echo $appBase?>/app/components/dropzone/directive.dropzone.js"></script>
<script src="<?php echo $appBase?>/components/appHeight/directive.appHeight.js"></script>
<script src="<?php echo $appBase?>/components/autoFrame/directive.autoFrame.js"></script>

<script src="<?php echo $appBase?>/bower_components/angular-bootstrap/ui-bootstrap.js"></script>
<script src="<?php echo $appBase?>/bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>

</body>
</html>