<div class="field_row clearfix">
	<?php echo form_label(lang('common_first_name') . ':'.($controller_name == 'suppliers' ? '' : get_required_text('first_name', !empty($field_settings) ? $field_settings : array())), 'first_name', array());?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'first_name', 'id' => 'first_name', 'value' => $person_info -> first_name));?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_last_name') . ':'.($controller_name == 'suppliers' ? '' : get_required_text('first_name',  !empty($field_settings) ? $field_settings : array())), 'last_name', array());?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'last_name', 'id' => 'last_name', 'value' => $person_info -> last_name));?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_email') . ':'.($controller_name == 'employees' ? '<span class="required">*</span>' : get_required_text('email',  !empty($field_settings) ? $field_settings : array())), 'email');?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'email', 'id' => 'email', 'size' => '40', 'value' => !empty($person_info -> email) ? $person_info -> email : ''));?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label(($controller_name != 'suppliers' ? lang('common_phone_number') : lang('common_phone_number')) . ':'.get_required_text('phone_number',  !empty($field_settings) ? $field_settings : array()), 'phone_number');?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'phone_number', 'id' => 'phone_number', 'value' => !empty($person_info -> phone_number) ? $person_info -> phone_number : '' ));?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label(($controller_name != 'suppliers' ? lang('common_cell_phone_number') : lang('common_phone_number_2')) . ':'.get_required_text('cell_phone_number',  !empty($field_settings) ? $field_settings : array()), 'cell_phone_number');?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'cell_phone_number', 'id' => 'cell_phone_number', 'value' => !empty($person_info -> cell_phone_number) ? $person_info -> cell_phone_number : '' ));?>
	</div>
</div>
<script>
	$(document).ready(function() {
		<?php if(!$this->config->item('currency_symbol') || $this->config->item('currency_symbol') === "$"): ?>
			$("#phone_number").mask2("(999) 999-9999");
			$("#cell_phone_number").mask2("(999) 999-9999");
		<?php endif; ?>
		$('#send_text_invite').live("click",function(){
			$.ajax({
				type: "POST",
				url: "index.php/customers/send_text_invite?customer_id=<?=!empty($person_info->person_id) ? $person_info->person_id : ''?>&course_id=<?=!empty($person_info->course_id) ? $person_info->course_id : ''?>",
				data: '',
				success: function(response){
					set_feedback("Invite will arrive shortly.",'success_message',false);
				},
				dataType:'json'
			});
		})
	})
</script>
<?php if ($controller_name == 'suppliers') {
?>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_fax_number') . ':', 'fax_number');?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'fax_number', 'id' => 'fax_number', 'value' => $person_info -> fax_number));?>
	</div>
</div>
<?php }?>
<?php if ($controller_name != 'suppliers') {
?>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_birthday') . ':'.get_required_text('birthday',  !empty($field_settings) ? $field_settings : array()), 'birthday');?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'birthday', 'id' => 'birthday', 'value' => !empty($person_info -> birthday) ? date('m/d/Y', strtotime($person_info -> birthday)) : '' ));?>
	</div>
</div>
<?php if (!isset($quick_add) || !$quick_add) { ?>
<div class="field_row clearfix">
	<label>Image</label>
	<div id="image-info">
		<img src="<?php echo $image_thumb_url; ?>" />
		<a id="select-image" title="Modify this image" href="<?php echo site_url('upload'); ?>/index/<?php echo $this->uri->segment(1); ?>?crop_ratio=1&image_id=<?php echo $person_info->image_id; ?>">Edit Image</a>
		<a id="remove-image" title="Disconnect image from person" href="<?php echo site_url($this->uri->segment(1)); ?>/save_image/<?php echo $person_info->person_id; ?>">Reset Image</a>
	</div>
</div>
<? }?>
<? }?>
<script>
	$('#birthday').datepicker({
		dateFormat : 'mm/dd/yy',
		changeMonth : true,
		changeYear : true,
		yearRange : "1920:2000",
		altFormat:'yy-mm-dd',
		defaultDate : "01/01/1979"
	});

</script>
<div id='address_info_box' style='display:none'>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_address_1') . ':'.get_required_text('address_1',  !empty($field_settings) ? $field_settings : array()), 'address_1');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'address_1', 'id' => 'address_1', 'value' => !empty($person_info -> address_1) ? $person_info -> address_1 : ''));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_address_2') . ':', 'address_2');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'address_2', 'id' => 'address_2', 'value' => !empty($person_info -> address_2) ? $person_info -> address_2 : '' ));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_city') . ':'.get_required_text('city',  !empty($field_settings) ? $field_settings : array()), 'city');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'city', 'id' => 'city', 'value' => !empty($person_info -> city) ? $person_info -> city : '' ));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_state') . ':'.get_required_text('state',  !empty($field_settings) ? $field_settings : array()), 'state');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'state', 'id' => 'state', 'value' => !empty($person_info -> state) ? $person_info -> state : '' ));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_zip') . ':'.get_required_text('zip',  !empty($field_settings) ? $field_settings : array()), 'zip');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'zip', 'id' => 'zip', 'value' => !empty($person_info -> zip) ? $person_info -> zip : '' ));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_country') . ':'.get_required_text('country',  !empty($field_settings) ? $field_settings : array()), 'country');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'country', 'id' => 'country', 'value' => !empty($person_info -> country) ? $person_info -> country : '' ));?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#address_info_box').expandable({
		title : 'Address Information:'
	});
</script>

<div id='marketing_info_box' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('customers_email_unsubscribed').':', 'email_unsubscribed'); ?>
		<div class='form_field'>
		<?php echo form_checkbox('email_unsubscribed', '1', (isset($person_info->opt_out_email) && $person_info->opt_out_email ? true : false));?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label("Text Subscribed"); ?>
		<div class='form_field'>
			<?php  if($can_edit_text_subscription):?>
				<?php echo form_checkbox('text_subscribed', '1', (isset($person_info->texting_status) && $person_info->texting_status == "subscribed" ? true : false));?>
			<?php else: ?>
				<?php echo form_checkbox('text_subscribed', '1', (isset($person_info->texting_status) && $person_info->texting_status == "subscribed" ? true : false),  'disabled="disabled"');?>
			<?php endif; ?>
			<?php if(isset($person_info->texting_status) && $person_info->texting_status != "subscribed"):		?>
				<a  id="send_text_invite">Send Invite</a>
			<?php endif; ?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('customers_email_suspended').':', ''); ?>
		<div class='form_field'>
		<?php if (!empty($bounced) && $bounced) { ?>
		<span id='blocked_email_box'>Yes <span class='unblock_email'>Unsuspend</span></span>
		<style>
			.unblock_email {
				cursor:pointer;
				color:rgb(92, 147, 168);
			}
		</style>
		<script>
			$(document).ready(function(){
				$('.unblock_email').click(function(){
					$.ajax({
			            type: "POST",
			            url: "index.php/customers/unblock_email?email=<?=$person_info->email?>",
			            data: '',
			            success: function(response){
			           		console.dir(response);
			           		if (response.success) {
			           			$('#blocked_email_box').replaceWith("No");
			           		}
					    },
			            dataType:'json'
			        });
				});

			});

		</script>
		<?php } else { ?>
		No
		<?php } ?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#marketing_info_box').expandable({
		title : 'Marketing:'
	});
</script>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_comments') . ':', 'comments');?>
	<div class='form_field'>
		<?php echo form_textarea(array('name' => 'comments', 'id' => 'comments', 'value' => !empty($person_info -> comments) ? $person_info -> comments : '' , 'rows' => '1', 'cols' => '12'));?>
	</div>
</div>
<?php
if ($this->config->item('mailchimp_api_key') && false)
{
?>
<div class="field_row clearfix">
	<div class="column">
		<?php echo form_label(lang('common_mailing_lists') . ':', 'mailchimp_mailing_lists');?>
	</div>
	<div class="column">
		<ul style="list-style: none;">
			<?php
			foreach (get_all_mailchimps_lists() as $list) {
				echo '<li>';
				echo form_checkbox(array('name' => 'mailing_lists[]', 'id' => $list['id'], 'value' => $list['id'], 'checked' => email_subscribed_to_list($person_info -> email, $list['id']), 'label' => $list['id']));
				echo form_label($list['name'], $list['id'], array('style' => 'float: none;'));
				echo '</li>';
			}
			?>
		</ul>
	</div>
	<div class="cleared"></div>
</div>
<?php

}
?>
<script>
    $(document).ready(function(){
        $('#zip').off('keyup').on('keyup', function(){
            var zip = $(this).val();
            if (zip.length == 5 && !isNaN(zip)) {
                // TODO: Add international capabilities
                var client = new XMLHttpRequest();
                client.open("GET", "https://api.zippopotam.us/us/"+zip, true);
                client.onreadystatechange = function() {
                    if(client.readyState == 4) {
                        var response = JSON.parse(client.responseText);
                        var city = '';
                        var state = '';
                        if (typeof response.places != 'undefined' && typeof response.places[0] != 'undefined' && typeof response.places[0]['place name'] != 'undefined') {
                            city = response.places[0]['place name'];
                        }
                        if (typeof response.places != 'undefined' && typeof response.places[0] != 'undefined' && typeof response.places[0]['state abbreviation'] != 'undefined') {
                            state = response.places[0]['state abbreviation'];
                        }
                        if ($('#city').val() == city && $('#state').val() == state) {
                            return;
                        }
                        if ($('#city').val() != '' && city != '') {
                            var confirmed = confirm("Would you like to use the following: "+city+", "+state);
                            if (confirmed) {
                                $('#city').val(city);
                                $('#state').val(state);
                            }
                        }
                        else {
                            $('#city').val(city);
                            $('#state').val(state);
                        }
                    };
                };

                client.send();
            }
        });
    });
</script>
