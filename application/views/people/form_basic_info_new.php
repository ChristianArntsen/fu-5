<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <?php echo form_label(lang('common_first_name') . ':'.($controller_name == 'suppliers' ? '' : '<span class="required">*</span>'), 'first_name', array());?>
            <?php echo form_input(array('name' => 'first_name', 'class'=>'form-control', 'id' => 'first_name', 'value' => $person_info -> first_name));?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <?php echo form_label(lang('common_last_name') . ':'.($controller_name == 'suppliers' ? '' : '<span class="required">*</span>'), 'last_name', array());?>
            <?php echo form_input(array('name' => 'last_name', 'id' => 'last_name', 'class'=>'form-control', 'value' => $person_info -> last_name));?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <?php echo form_label(lang('common_email') . ':'.($controller_name == 'employees' ? '<span class="required">*</span>' : ''), 'email');?>
            <?php echo form_input(array('class'=>'form-control', 'name' => 'email', 'id' => 'email', 'value' => $person_info -> email));?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <?php echo form_label(($controller_name != 'suppliers' ? lang('common_cell_phone_number') : lang('common_phone_number')) . ':', 'phone_number');?>
            <?php echo form_input(array('class'=>'form-control', 'name' => 'phone_number', 'id' => 'phone_number', 'value' => $person_info -> phone_number));?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <?php echo form_label(($controller_name != 'suppliers' ? lang('common_phone_number') : lang('common_phone_number_2')) . ':', 'cell_phone_number');?>
            <?php echo form_input(array('class'=>'form-control', 'name' => 'cell_phone_number', 'id' => 'cell_phone_number', 'value' => $person_info -> cell_phone_number));?>
        </div>
    </div>
    <?php if ($controller_name == 'suppliers') { ?>
       <div class="col-sm-6">
        <div class="form-group">
            <?php echo form_label(lang('common_fax_number') . ':', 'fax_number');?>
            <?php echo form_input(array('class'=>'form-control', 'name' => 'fax_number', 'id' => 'fax_number', 'value' => $person_info -> fax_number));?>
        </div>
    </div>
    <?php } else { ?>
        <div class="col-sm-6">
            <div class="form-group">
                <?php echo form_label(lang('common_birthday') . ':', 'birthday');?>
                <?php echo form_input(array('class'=>'form-control', 'name' => 'birthday', 'id' => 'birthday', 'value' => $person_info -> birthday));?>
            </div>
        </div>
    <?php } ?>
</div>

<?php if (!isset($quick_add) || !$quick_add) { ?>
    <div class="field_row clearfix" style="margin-bottom: 20px;">
        <label>Image</label>
        <div id="image-info">
            <img src="<?php echo $image_thumb_url; ?>" />
            <a id="select-image" title="Modify this image" href="<?php echo site_url('upload'); ?>/index/<?php echo $this->uri->segment(1); ?>?crop_ratio=1&image_id=<?php echo $person_info->image_id; ?>">Edit Image</a>
            <a id="remove-image" title="Disconnect image from person" href="<?php echo site_url($this->uri->segment(1)); ?>/save_image/<?php echo $person_info->person_id; ?>">Reset Image</a>
        </div>
    </div>
<? }?>

<script>
    $(document).ready(function() {
        <?php if($this->config->item('currency_symbol') && $this->config->item('currency_symbol') != "$"): ?>

            $("#phone_number").mask2("(999) 999-9999");
            $("#cell_phone_number").mask2("(999) 999-9999");
        <?php endif; ?>
    })
	$('#birthday').datepicker({
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		yearRange : "1920:2000",
		defaultDate : "1970-01-01"
	});

</script>
<div id='address_info_box' style="display:none; margin-bottom: 10px;">

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <?php echo form_label(lang('common_address_1'), 'address_1');?>
                <?php echo form_input(array('class'=>'form-control', 'name' => 'address_1', 'id' => 'address_1', 'value' => $person_info -> address_1));?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?php echo form_label(lang('common_address_2'), 'address_2');?>
                <?php echo form_input(array('class'=>'form-control', 'name' => 'address_2', 'id' => 'address_2', 'value' => $person_info -> address_2));?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <?php echo form_label(lang('common_city') . ':', 'city');?>
                <?php echo form_input(array('class'=>'form-control', 'name' => 'city', 'id' => 'city', 'value' => $person_info -> city));?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?php echo form_label(lang('common_state') . ':', 'state');?>
                <?php echo form_input(array('class'=>'form-control', 'name' => 'state', 'id' => 'state', 'value' => $person_info -> state));?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <?php echo form_label(lang('common_zip') . ':', 'zip');?>
                <?php echo form_input(array('class'=>'form-control', 'name' => 'zip', 'id' => 'zip', 'value' => $person_info -> zip));?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?php echo form_label(lang('common_country') . ':', 'country');?>
                <?php echo form_input(array('class'=>'form-control', 'name' => 'country', 'id' => 'country', 'value' => $person_info -> country));?>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript'>
	$('#address_info_box').expandable({
		title : 'Address Information'
	});
</script>

<div id='marketing_info_box' style="display:none; margin-bottom: 20px;">
    <div class="form-group">
        <label>
            <?php echo form_checkbox('email_unsubscribed', '1', ($person_info->opt_out_email ? true : false));?>
            &nbsp;
            <?php echo lang('customers_email_unsubscribed'); ?>
        </label>
    </div>
    <div class="form-group">
        <label>
            <?php echo form_checkbox('text_unsubscribed', '1', ($person_info->opt_out_text ? true : false));?>
            &nbsp;
            <?php echo lang('customers_text_unsubscribed'); ?>
        </label>
    </div>
    <div class="form-group">
        <?php echo form_label(lang('customers_email_suspended').':', ''); ?>
        <label>
            <?php if ($bounced) { ?>
                <span id='blocked_email_box'>Yes <span class='unblock_email'>Unsuspend</span></span>
                <style>
                    .unblock_email {
                        cursor:pointer;
                        color:rgb(92, 147, 168);
                    }
                </style>
                <script>
                    $(document).ready(function(){
                        $('.unblock_email').click(function(){
                            $.ajax({
                                type: "POST",
                                url: "index.php/customers/unblock_email?email=<?=$person_info->email?>",
                                data: '',
                                success: function(response){
                                    console.dir(response);
                                    if (response.success) {
                                        $('#blocked_email_box').replaceWith("No");
                                    }
                                },
                                dataType:'json'
                            });
                        });
                    });
                </script>
            <?php } else { ?>
                No
            <?php } ?>
        </label>
    </div>
</div>
<script type='text/javascript'>
	$('#marketing_info_box').expandable({
		title : 'Marketing'
	});
</script>
<div class="form-group">
    <?php echo form_label(lang('common_comments') . ':', 'comments');?>
    <?php echo form_textarea(array('class'=>'form-control', 'name' => 'comments', 'id' => 'comments', 'value' => $person_info -> comments, 'rows'=>'6'));?>
</div>
<?php
if ($this->config->item('mailchimp_api_key') && false) { ?>
<div class="field_row clearfix">
	<div class="column">
		<?php echo form_label(lang('common_mailing_lists') . ':', 'mailchimp_mailing_lists');?>
	</div>
	<div class="column">
		<ul style="list-style: none;">
			<?php
			foreach (get_all_mailchimps_lists() as $list) {
				echo '<li>';
				echo form_checkbox(array('name' => 'mailing_lists[]', 'id' => $list['id'], 'value' => $list['id'], 'checked' => email_subscribed_to_list($person_info -> email, $list['id']), 'label' => $list['id']));
				echo form_label($list['name'], $list['id'], array('style' => 'float: none;'));
				echo '</li>';
			}
			?>
		</ul>
	</div>
	<div class="cleared"></div>
</div>
<?php

}
?>