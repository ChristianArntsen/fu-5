<?php if (!$ajax) { ?>
<?php $this->load->view("partial/header_new"); ?>
<?php } ?>
<script>
$(document).ready(
    function(){
	$("#signup").click(function(){
    $("#pricing").toggleClass("show");
	});
});
</script>
<script src="https://use.typekit.net/crn3fud.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<div id = 'home_page_container'>
<style>
#home_module_list * {
	box-sizing: border-box;
}
#home_module_list {
	width: 1220px !important;
	height: 775px;
	padding: 0 !important;
	position: relative;
	overflow: hidden;
}
#teaser {
	width: 1220px;
	height: 775px;
	padding: 30px;
	background-image: url("../images/home/bg.png");
	background-position: top left;
	background-size: 100%;
	position: relative;
}

#home_module_list  h1 {
	 font-family: "bebas-neue", sans-serif;
	 position: absolute;
	 top: -125px;
	 left: 30px;
	 font-size: 71px;
	 letter-spacing: 5px;
	 color: #FFFFFF;
}
#home_module_list h2 span,
#home_module_list h1 span {
	color: #f86e00;
}

#home_module_list h2 {
	font-family: "Lato", sans-serif;
	font-size: 35.5px;
	line-height: 1.8;
	margin-top: 125px;
	display: block;
	color: #292929;
	margin-left: 30px;
}
img#social_graphic {
	position: absolute;
	top:0;right:0;
}
img#bottom {
	position: absolute;
	bottom:0;
	left:0;
	z-index: 10;
}
a.btn {
	border-radius: 5px;
	letter-spacing: 1px;
	font-size: 13px;
	display: inline-block;
	padding: 5px 12px;
	margin-bottom: 0;
	font-weight: 400;
	line-height: 1.42857143;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	cursor: pointer;
	user-select: none;
	border: 1px solid transparent;
	color: #ffffff;
	background-color: #f86e00;
	font-size: 16px;
}
.btn.signup {
	padding: 6px 25px;
	color: #ffffff;
	background-color: #f86e00;
	font-size: 30px;
	font-family: 'lato', sans-serif;
	font-weight: bold;
	position: absolute;
	left: 405px;
	bottom: -60px;
}
span.sub {
	display: block;
	color: #808080;
	margin-top: 20px;
	margin-left: 30px;
	font-size: 12px;
}
div.text {
	position: relative;
	z-index:50;
}
#pricing {
	width: 1220px;
	height: 775px;
	padding: 30px 60px;
	background: #ececec;
	position: absolute;
	bottom:0;
	left:0;
	z-index: 55;
	transform: translate(0, -100%);
	transition: .5s linear;
}
#pricing.show {
	transform: translate(0, 0);
}
#pricing .price {
	width: 47.5%;
	display:block;
	margin-right: 5%;
	float:left;
}
#pricing .price1,
#pricing .price2{
	border-radius: 5px;
	box-shadow: 0 5px 10px rgba(0,0,0,0.25);
	background: #ffffff;
}
.price_header {
	padding: 20px 20px;
	text-align: center;
	border-radius: 5px;
	border-bottom-left-radius: 0;
	border-bottom-right-radius: 0;
	background: #E0EFF1;
}
.price2 .price_header {
	background: #680148;
	color: #FFFFFF;
}
#pricing h3 {
	font-size: 24px;
	font-weight: 200;
}
#pricing span.pricing {
	font-size: 36px;
  display: block;
  text-align: center;
  font-weight: 100;
	position: relative;
}
#pricing span.pricing:before {
	content:'$';
	font-size: 12px;
}
#pricing span.pricing span.time {
	font-size: 12px;
}
.price_info {
	padding: 30px;
	text-align: center;
	font-size: 18px;
	color: #808080;
}
.price_info ul li {
	margin-bottom: 15px;
}
#home_module_list  h1.pricing_header {
	font-family: "bebas-neue", sans-serif;
	position: relative;
	display: block;
	font-size: 71px;
	letter-spacing: 5px;
	color: #292929;
	top: -29px;
	margin-bottom: -16px;
	left: 41px;
}

/* Modal style */
.modal_overlay {
	width: 100vw;
	height: 100vh;
	background: rgba(0,0,0,0.65);
	display: block;
	position: absolute;
	top:0;left:0;
	z-index: 150;
	display:none;
}
.modal {
	width: 500px;
	height: 245px;
	background: #f7f7f7;
	border: 1px solid #cccccc;
	border-radius: 5px;
	box-shadow: 0 5px 10px rgba(0,0,0,0.35);
	position: absolute;
	top: 0;
	left: 0;
	right:0;
	bottom:0;
	margin: auto;
	padding: 30px 10px;
	color: #808080;
}
.modal h3{
	display: block;
	text-align: center;
	font-size: 18px;
	font-family: 'Lato' , sans-serif;
	margin-bottom: 15px;
	border-bottom: 1px solid #cccccc;
	margin-top: -10px;
	padding-bottom: 7px;
}
.modal p {
	display: block;
	padding: 15px;
	margin-bottom: 15px;
	font-size: 16px;
	line-height: 1.8;
	text-align: center;
}

</style>

<?php if ($this->permissions->is_super_admin()) { ?>
<div id='add_article'>Add Article</div>
<script>
	$('#add_article').click(function(){
		$.colorbox({href:'index.php/home/add_article', width:600, 'title':'Make new article'});
	});
</script>




<?php } ?>
<div id="home_module_list">
	<div id="teaser">
	<img id="social_graphic" src="../images/home/social_net.png"/>
	<img id="bottom" src="../images/home/bottom_graphic.png"/>
	<div class="text">
		<h1>MORE REACH. <span>LESS WORK.</span></h1>
		<h2>foreUP Social Media Management<br/>
			    Increase your potential customers.</br/>
				Advertise deals & specials to your fans.<br/>
				Manage facebook social for your course.<br/>
				Let foreUP do all the work. <span>FREE!<sup>*</sup></span><br/>
		</h2>
        <?php if($user['course_info']->social_management == 0 && $user['user_level'] == 3): ?>
            <a class="btn signup" id="signup">Sign UP Now</a>
        <?php elseif($user['course_info']->social_management == 1 ): ?>
            <a class="btn signup">Registered</a>
        <?php else: ?>
<!--            <a class="btn signup" >Managers Start a Free Trial Today</a>-->
        <?php endif; ?>
		<span class="sub">*Free for the first 30 days</span>
	</div>
	<img id="bottom" src="../images/home/bottom_graphic.png"/>
	</div>

	<div id="pricing" style="text-align: center;">
		<h1 class="pricing_header">Free 30 Day Trial</h1>
		<div class="price price1">
			<div class="price_header">
				<h3>Basic Plan</h3>
					<span class="pricing">50<span class="time">/month</span></span>
			</div>
			<div class="price_info">
			<ul>
				<li>Facebook Social Monitoring</li>
				<li>Notification and Alerts</li>
			</ul>
			<a class="btn"  id="new-trial">Start Free Trial</a>
			<hr/ style="margin-top:10px;">
				<h3 style="margin-top:20px;display:block;">Add-Ons</h3>
				<div class="price3">
					<span class="pricing">+50<span class="time">/month</span></span>
					<div class="price_info" style="padding:10px">
						<ul><li>Generic Varying 2-3 posts per week</li></ul>
						<a class="btn"  id="new-trial"> Start Free Trial</a>
					</div>
					<span class="pricing">+100<span class="time">/month</span></span>
					<div class="price_info"style="padding:10px">
						<ul><li>Generic Varying 4-5 posts per week</li></ul>
						<a class="btn" id="new-trial"> Start Free Trial</a>
					</div>
				</div>
			</div>
		</div>
		<div class="price" style="margin-right: 0;">
			<div class="price2">
				<div class="price_header">
					<h3>Premium Plan</h3>
					<span class="pricing">319<span class="time">/month</span></span>
				</div>
				<div class="price_info">
                    <ul>
                        <li>Facebook Social Monitoring</li>
                        <li>Notification and response to your customers</li>
                        <li>Generic Varying 2-3 posts per week <small>(if desired, by request)</small></li>
                        <li>Includes 2 Custom branded posts per week</li>
                        <li>foreUP Notification response to your customers (within 24-48 hrs)</li>
                    </ul>
                    <a class="btn" id="new-trial">Start Free Trial</a>
                </div>
			</div>
		</div>


	</div>

</div>
<div class="modal_overlay">
	<div class="modal">
		<h3>Confirm Subscription</h3>
		<p>By clicking subscribe below, you are accepting that these subscription charges will be billed on your normal billing schedule. If you wish to cancel, you must call into support and cancel this charge before the 30 days is complete.</p>
		<a id="confirm" class="btn" style="display:block;margin:10px auto; width: 150px;">Confirm</a>
		<a id="cancelmodal" style="display:block;margin:10px auto;text-align:center;color:#808080;font-size: 12px;font-style:italic;">Cancel</a>
	</div>
</div>
    <script>

        $(document).ready(function() {
            $("#new-trial").live("click",function(){
                $(".modal_overlay").show();
            })
            $("#confirm").live("click",function(){
                $.ajax({url: "/home/activate_social", success: function(result){
                    location.reload();
                }});
                $(".modal_overlay").hide();
                alert("We'll contact you soon to get things setup.");
            })
            $("#cancelmodal").live("click",function(){
                $(".modal_overlay").hide();
            })
        });
    </script>
	<?php
	/*
	foreach($allowed_modules->result() as $module)
	{
        if ($this->permissions->is_super_admin() || $this->permissions->course_has_module($module->module_id))
        {

	?>
	<div class="module_item">
		<a href="<?php echo site_url("$module->module_id");?>">
			<img src="<?php echo base_url().'images/menubar/'.$module->module_id.'.png';?>" border="0" alt="Menubar Image" />
			<span><?php echo lang("module_".$module->module_id) ?></span>
		</a>
		- <span><?php echo lang('module_'.$module->module_id.'_desc');?></span>
	</div>
	<?php
	}
        }*/
	?>
</div>
</div>
<?php if (!$ajax) { ?>
<?php $this->load->view("partial/footer"); ?>
<?php } ?>
