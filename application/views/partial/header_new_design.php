<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">

    <base href="<?php echo base_url();?>" />
    <title><?php echo $this->config->item('name').' - '.lang('common_powered_by').' ForeUP' ?></title>

    <!-- favicon -->
    <link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>

    <!-- stylesheets -->
    <?php if ($controller_name == 'teesheets' || $controller_name == 'reservations') { ?>
        <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/all_tee_sheet_css.css?<?php echo APPLICATION_VERSION; ?>" />

        <link href="<?php echo base_url(); ?>css/weather/weather-icons.min.css?<?php echo APPLICATION_VERSION; ?>" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fullcalendar.print.css?<?php echo APPLICATION_VERSION; ?>" media="print"/>
    <?php } else { ?>
        <!--<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/all_css.css?<?php echo APPLICATION_VERSION; ?>" />-->
    <?php } ?>

    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/new_styles.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/phppos_print.css?<?php echo APPLICATION_VERSION; ?>"  media="print"/>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/redmond.css?<?php echo APPLICATION_VERSION; ?>">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/sass/main.css?<?php echo APPLICATION_VERSION; ?>" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

    <!-- Google Analytics -->

  	<script>
  	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  	  ga('create', 'UA-101056671-1', 'auto');
  	  ga('send', 'pageview');
  	</script>

    <!-- pagination init -->
    <script type="text/javascript">
        var lang_array = JSON.parse('<?php echo addslashes(json_encode($this->lang->all()));?>');
        function lang(key)
        {

            if (lang_array[key] != undefined)
            {
                return lang_array[key];
            }
        }
        var pagination = {
            override_links: function() {
                $('#pagination a').click(function(e) {
                    e.preventDefault();
                })
            }
        }
        var SITE_URL= "<?php echo site_url(); ?>";
    </script>


    <?php if ($controller_name == 'teesheets' || $controller_name == 'reservations') { ?>
        <!-- <script src="<?php echo base_url();?>js/jquery-1.5.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script> -->
        <script src="<?php echo base_url();?>js/all_tee_sheet_js.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>

    <?php } else if($controller_name == 'food_and_beverage'){ ?>

        <script src="<?php echo base_url();?>js/jquery-1.10.2.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery-migrate-1.2.1.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url('js/underscore.min.js?'.APPLICATION_VERSION); ?>"></script>
        <script src="<?php echo base_url('js/backbone.min.js?'.APPLICATION_VERSION); ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.colorbox.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.colorbox2.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery-ui.1.10.3.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/accounting.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/fastclick.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/bootstrap.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css?<?php echo APPLICATION_VERSION; ?>" /> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap-theme.min.css?<?php echo APPLICATION_VERSION; ?>" /> -->
        <script>
            window.addEventListener('load', function() {
                FastClick.attach(document.body);
            }, false);
        </script>

    <?php } else if($controller_name == 'config' || $controller_name == 'reports'){ ?>
        <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>js/jquery.cleditor.css?<?php echo APPLICATION_VERSION; ?>" />
        <script src="<?php echo base_url();?>js/jquery-1.10.2.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery-migrate-1.2.1.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery-ui.1.10.3.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url('js/spectrum.js'); ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.colorbox.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.colorbox2.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
        <script src="<?php echo base_url();?>js/ui.expandable.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <link href="<?php echo base_url('css/spectrum.css'); ?>" type="text/css" rel="stylesheet" />
        <script src="<?php echo base_url();?>js/jquery.tablesorter.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/common.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/manage_tables.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/jquery.cleditor.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/moment.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>

    <?php } else { ?>

        <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo base_url();?>js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
        <script src="<?php echo base_url();?>js/jquery.color.js"></script>
        <script src="<?php echo base_url();?>js/jquery.form.js"></script>
        <script src="<?php echo base_url();?>js/jquery.tablesorter.min.js"></script>
        <script src="<?php echo base_url();?>js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url();?>js/jquery.maskedinput.js"></script>
        <script src="<?php echo base_url();?>js/thickbox.js"></script>
        <script src="<?php echo base_url();?>js/jquery.colorbox.js"></script>
        <script src="<?php echo base_url();?>js/jquery.colorbox2.js"></script>
        <script src="<?php echo base_url();?>js/ui.expandable.js"></script>
        <script src="<?php echo base_url();?>js/common.js"></script>
        <script src="<?php echo base_url();?>js/manage_tables.js"></script>
        <script src="<?php echo base_url();?>js/date.js"></script>
        <script src="<?php echo base_url();?>js/hoverIntent.js"></script>
        <script src="<?php echo base_url();?>js/superfish.js"></script>
        <script src="<?php echo base_url();?>js/jquery.loadmask.min.js"></script>
        <script src="<?php echo base_url();?>js/jHtmlArea-0.7.0.js"></script>
        <script src="<?php echo base_url();?>js/jquery.checkbox.js"></script>
        <script src="<?php echo base_url();?>js/jquery.contextMenu.js"></script>
        <script src="<?php echo base_url();?>js/jquery.dd.js"></script>
        <script src="<?php echo base_url();?>js/jquery.timepicker.js"></script>
        <script src="<?php echo base_url();?>js/jquery.sth.js"></script>

    <?php } ?>


    <script src="<?php echo base_url();?>js/jquery.mask.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.contactable.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.customSelect.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.qtip.min2.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/StarWebPrintBuilder.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/StarWebPrintTrader.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/StarBarcodeEncoder.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/webprnt.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/taffy.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/db.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/swipeable.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/jquery.mtz.monthpicker.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>

    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/text_size_reduced.css?<?php echo APPLICATION_VERSION; ?>" />
    <script type="text/javascript">

        Date.format = '<?php echo get_js_date_format(); ?>';
        var key_map = {18:false};
        $(document).keydown(function(event)
        {
            if (event.keyCode in key_map) {
                key_map[event.keyCode] = true;
            }
            if (event.keyCode == 112)
            {
                <?php if ($this->session->userdata('reservations')) { ?>
                window.location = SITE_URL + "/reservations";
                <?php } else { ?>
                window.location = SITE_URL + "/teesheets";
                <?php }?>
            }
            else if (event.keyCode == 113)
            {
                window.location = SITE_URL + "/sales";
            }
            else if (event.keyCode == 117)
            {
                $.colorbox({
                    href: "index.php/home/view_giftcard_lookup",
                    title:'Giftcard/Punch Card Lookup',
                    width:500,
                    onComplete:function(e){
                        console.log('trying to select gcn');
                        //e.preventDefault();
                        console.dir($('#giftcard_number'));
                        $('#giftcard_number').focus();
                    }
                });
            }
            else if ((event.keyCode == 49 || event.keyCode == 97) && event.altKey)
            {
                event.preventDefault();
                <?php if ($this->session->userdata('reservations')) { ?>
                window.location = SITE_URL + "/reservations";
                <?php } else { ?>
                window.location = SITE_URL + "/teesheets";
                <?php }?>
            }
            else if ((event.keyCode == 50 || event.keyCode == 98) && event.altKey)
            {
                event.preventDefault();
                window.location = SITE_URL + "/sales";
            }
            else if ((event.keyCode == 48 || event.keyCode == 96) && event.altKey)
            {
                event.preventDefault();
                window.location = SITE_URL + "/config";
            }
            else if ((event.keyCode == 51 || event.keyCode == 99) && event.altKey)
            {
                event.preventDefault();
                window.location = SITE_URL + "/items";
            }
            else if ((event.keyCode == 52 || event.keyCode == 100) && event.altKey)
            {
                event.preventDefault();
                window.location = SITE_URL + "/employees";
            }
            else if ((event.keyCode == 53 || event.keyCode == 101) && event.altKey)
            {
                event.preventDefault();
                window.location = SITE_URL + "/customers";
            }
            else if ((event.keyCode == 54 || event.keyCode == 102) && event.altKey)
            {
                event.preventDefault();
                $.colorbox({
                    href:"index.php/home/view_print_queue",
                    title:'Print Queue',
                    width:500,
                    onComplete:function(e){
                        console.log('trying to select gcn');
                        //e.preventDefault();
                        console.dir($('#giftcard_number'));
                    }
                });
            }
            else if ((event.keyCode == 54 || event.keyCode == 102) && event.altKey)
            {
                event.preventDefault();
                window.location = SITE_URL + "/giftcards";
            }
            else if ((event.keyCode == 71 || event.keyCode == 103) && event.altKey)
            {
                event.preventDefault();
                $.colorbox({
                    href:"index.php/home/view_giftcard_lookup",
                    title:'Giftcard Lookup',
                    width:500,
                    onComplete:function(e){
                        console.log('trying to select gcn');
                        //e.preventDefault();
                        console.dir($('#giftcard_number'));
                        $('#giftcard_number').focus();
                    }
                });
            }
            else if (event.keyCode == 192 && event.altKey)
            {
                event.preventDefault();
                $.colorbox({
                    href:"index.php/home/view_key_guide",
                    title:'Quick Key Guide',
                    width:500,
                    onComplete:function(e){

                    }
                });
            }
            else if ((event.keyCode == 80 || event.keyCode == 112) && event.altKey)
            {
                event.preventDefault();
                if (window['mercury'] != undefined)
                    mercury.payments_window();
            }
            else if (event.keyCode == 84 && event.altKey) {
                event.preventDefault();
                $.colorbox({'href':'index.php/home/terminal_window','title':'Select Terminal', 'width':400, 'overlayClose':false,onComplete : function() {$(this).colorbox.resize();}});
            }
        }).keyup(function(event) {
            if (event.keyCode in key_map) {
                key_map[event.keyCode] = false;
            }
        });
        var old_messages = '';
        $(document).ready(function(){

            $(window).resize(function(){
                resize_table();
            });
            resize_table();
            setInterval(function(){webprnt.print_all(window.location.protocol + "//" + "<?=$this->config->item('webprnt_ip')?>/StarWebPRNT/SendMessage")}, 3000);

            $('#menu_button').mouseenter(function(){
                $('#software_menu').show();
                $('#software_menu').unbind('mouseleave').mouseleave(function(){
                    $('#software_menu').hide();
                });

                setTimeout("$('body').one('click', function(e){ $('#software_menu').hide(); });", 500);
            });

            $('#user_button').mouseenter(function(){
                $('#user_menu').show();
                $('#user_menu').unbind('mouseleave').mouseleave(function(){
                    $('#user_menu').hide();
                });

                setTimeout("$('body').one('click', function(){ $('#user_menu').hide(); });", 500);
            });

            $('#stats_button').click(function() {
                $('#shadow_row').toggle();
                $('#stats_row').toggle();
                var sb = $('#stats_button');
                sb.hasClass('selected')?sb.removeClass('selected'):sb.addClass('selected')
                if($(".note_dialog").css('top') == '125px'){
                    $(".note_dialog").stop().animate({top:'285px'},10);
                };
                if($(".note_dialog").css('top') == '285px'){
                    $(".note_dialog").stop().animate({top:'125px'},10);
                };
            })
            <?php
                if ($this->session->userdata('display_message')) {
            ?>
            set_feedback("<?=$this->session->userdata('display_message')?>",'error_message',true);
            <?php
                    $this->session->unset_userdata('display_message');
                }

                 $hidden_style = '';
                 if (!($this->permissions->is_employee() && $controller_name == 'sales') && ($controller_name == 'teesheets' || $controller_name == 'reservations' || $controller_name == 'sales' || $controller_name == 'customers' || $controller_name == 'marketing_campaigns')) { ?>

            <?php } else {
                     $hidden_style = 'display:none';
                 }

                if ($controller_name != 'food_and_beverage' && $this->session->userdata('use_terminals') && $this->session->userdata('terminal_id') === false)
                {
            ?>
            $.colorbox({
                'href':'index.php/home/terminal_window',
                'title':'Select Terminal',
                'width':400,
                'overlayClose':false,
                'onComplete' : function() {
                    $(this).colorbox.resize();
                }
            });
            <?php
                }
            ?>
            // Watch for any messages from the server
            if (!!window.EventSource && false) {
                //var old = '';
                var source = new EventSource('index.php/home/get_course_messages/');

                source.onmessage = function(e)
                {
                    var message_html = '';
                    var nd = new Date();
                    //console.log('running '+nd.getTime());
                    if(old_messages!=e.data){
                        //console.log(e.data);
                        var  messages = eval("("+e.data+")");
                        console.log('course messages');
                        console.dir(messages);
                        for (var i in messages)
                        {
                            console.log('message '+i);
                            message_html += "<div id='course_message_"+messages[i].message_id+"' class='course_message'>"+
                            "<span class='date_posted'>"+messages[i].date_posted+"</span>"+
                            messages[i].message+
                            "<span class='mark_as_read' onclick='mark_message_as_read("+messages[i].message_id+")'>x</span>"+
                            "</div>";
                        }
                        $('#course_messages').html(message_html);
                        old_messages = e.data;
                    }
                };
            }
        });
        var controller_name = '<?=$controller_name?>';
        function mark_message_as_read(message_id)
        {
            $('#course_message_'+message_id).remove();
            $.ajax({
                type: "POST",
                url: "index.php/home/mark_message_as_read/"+message_id,
                data: '',
                success: function(response){
                },
                dataType:'json'
            });
        }
        function resize_table() {
            {
                var sortable_table = $('#sortable_table');
                if (sortable_table.length > 0)
                {
                    var win_height = $(window).height();
                    if (controller_name == 'reports')
                    {}
                    else
                    {	var adj_win_height = (win_height - 200 > 250) ? win_height - 200 : 250;
                        var table_height = (sortable_table.height() > 250) ? sortable_table.height() : 250;
                        $('#table_holder').height((adj_win_height < table_height) ? adj_win_height : table_height);
                    }
                }
            }
        }
        function load_stats_header(controller, start_date, end_date) {

            <?php if (!($this->session->userdata('sales_stats') === '0' && $controller_name == 'sales')) { ?>
            console.log('initialize_stats_header'+controller+' sd: '+start_date+' ed: '+end_date);
            var date = new Date();
            var sd = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
            var ed = sd;
            $.ajax({
                type: "POST",
                url: "index.php/"+controller+"/generate_stats/",
                data: 'start='+(start_date==undefined?sd:start_date)+'&end='+(end_date==undefined?ed:end_date),
                success: function(response){
                    var header_stats_html = '';
                    var more_stats_html = '';
                    var left_border_class = '';
                    var header = response.header;
                    var more = response.more;
                    for (var i in header)
                    {
                        header_stats_html += "<div class='bar_stat "+left_border_class+"'><div class='stat_value'>"+header[i]+"</div><div class='stat_label'>"+i.replace(/_/g, " ")+"</div></div>";
                        left_border_class = 'left_border';
                    }
                    left_border_class = '';
                    for (var i in more)
                    {
                        more_stats_html += "<div class='bar_stat "+left_border_class+"'><div class='stat_value'>"+more[i]+"</div><div class='stat_label'>"+i.replace(/_/g, " ")+"</div></div>";
                        left_border_class = 'left_border';
                    }
                    $('#menubar_stats').html(header_stats_html);
                    $('#stats_section').html(more_stats_html);
                    if (controller == 'Sales')
                        $('#item').focus();
                    if (controller == 'teesheets')
                        teesheet_note();
                    console.log('stats');
                    console.dir(response);
                },
                dataType:'json'
            });
            <?php } ?>
        }

        <?php if(false && $this->config->item('ibeacon_enabled') == 1){ // DEACTIVATED UNTIL WE FINISH AND MAKE AVAILABLE ?>
        // Check for nearby ibeacons on this terminal for this course
        function get_beacons(){
            $.get('<?php echo site_url('ibeacon'); ?>', null, function(response){
                if(response){
                    $('body').prepend(response);
                }
            },'html');
        }

        $(function(){
            $('body div.ibeacon a.close').live('click', function(){
                $(this).parents('div.ibeacon').remove();
                return false;
            });

            // Poll for ibeacon messages every 5 seconds
            setInterval('get_beacons();', 5000);
        });
        <?php } ?>
    </script>
    <?php if ($controller_name == 'teesheets') { ?>
        <script src="<?php echo base_url();?>js/jquery.maskedinput.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/all_tee_sheet_2_js.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <?php } else if ($controller_name == 'reservations') { ?>
        <script src="<?php echo base_url();?>js/jquery.maskedinput.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
        <script src="<?php echo base_url();?>js/all_reservations_2_js.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <?php } ?>
    <style type="text/css">
        html {
            overflow: auto;
        }

        div.ibeacon {
            display: block;
            position: fixed;
            right: 10px;
            top: 10px;
            padding: 15px;
            background-color: #E9E9E9;
            border: 1px solid #AAA;
            border-radius: 5px;
            z-index: 100000;
            box-shadow: 2px 2px 10px rgba(0,0,0,0.2);
            overflow: hidden;
        }

        div.ibeacon span {
            display: block;
            padding: 2px 0px 2px 0px;
            font-size: 14px;
        }

        div.ibeacon span.balance span.amount {
            float: right;
            display: block;
            padding: 0px;
            font-weight: bold;
        }

        div.ibeacon span.balance {
            width: 225px;
        }

        div.ibeacon a.close {
            position: absolute;
            right: 10px;
            top: 0px;
            color: #666;
            font-weight: bold;
            font-size: 32px;
        }

        div.ibeacon a.close:hover {
            color: #398ABD;
            cursor: pointer;
        }

        div.ibeacon div.photo {
            width: 100px;
            float: left;
            min-height: 150px;
            margin-right: 10px;
            overflow: hidden;
        }

        div.ibeacon div.details {
            width: 300px;
            float: left;
            overflow: hidden;
        }

        div.ibeacon h3 {
            display: block;
            padding: 0px;
            margin: 0px 0px 5px 0px;
            font-size: 24px;
        }
    </style>

</head>

<body id='<?=$controller_name?>'>

<?php //echo $this->session->userdata('terminal_id'); ?>

<div id='body'>
    <div id='course_messages'>

    </div>

    <nav class="navbar app-header">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed btn-default" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand app-header-logo">
                    <!--img src="/images/logo-white-small.png"-->
                    <img src="/images/foreUP-Holiday-2-small.png">
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle app-header-current" data-toggle="dropdown" role="button" aria-expanded="false">
                            <?php echo lang('module_'.$controller_name);?> <span class="caret"></span>
                        </a>
                        <?php $this->load->view('partial/software_menu_new'); ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle app-header-btn" data-toggle="dropdown" role="button" aria-expanded="false">
                            <?php echo img(array('src' => $this->Appconfig->get_logo_image())); ?>
                            <?php echo $user_info->first_name.' '.$user_info->last_name ?>
                            <span class="caret"></span>
                        </a>
                        <?php $this->load->view('partial/user_menu_new'); ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
