<ul class="dropdown-menu app-header-apps" role="menu">
    <?php
    $allowed_modules = ($this->Module->get_allowed_modules($this->session->userdata('person_id')));

    $has_sales_v2 = false;
    $has_fnb_v2 = false;
    $backbone = ['passes', 'item_kits'];
    
    if($this->permissions->course_has_module('sales_v2')){
        $has_sales_v2 = true;
    }
    if($this->permissions->course_has_module('food_and_beverage_v2')){
        $has_fnb_v2 = true;
    }

    foreach($allowed_modules->result() as $module)
    {
        if($has_sales_v2 && $module->module_id == 'sales'){
            continue;
        }
        if($has_fnb_v2 && $module->module_id == 'food_and_beverage'){
            continue;
        }

        if ($this->permissions->is_super_admin() || $this->permissions->course_has_module($module->module_id))
        {

            if($module->module_id == 'customers' && $this->config->item('sales_v2') == 1){
                $url = 'v2/home#new_customers';
            }else if($module->module_id == 'customers' && $this->config->item('sales_v2') == 1){
                $url = 'v2/home#items_old';
            }else if($module->module_id == 'teesheets' && $this->config->item('tee_sheet_speed_up') == 1 && $this->config->item('sales_v2') == 1){
                $url = 'v2/home#tee_sheet';
            }else if(stripos($module->module_id, '_v2') !== false || (in_array($module->module_id, $backbone) && $has_sales_v2)){
                $url = 'v2/home#'.str_replace('_v2', '', $module->module_id);
            }else if($module->module_id == 'marketing_campaigns'){
                $url = "marketing_campaigns/campaign_builder#/dashboard";
            } else {
                $url = $module->module_id;
            }
            ?>
            <li>
                <a href="<?php echo site_url($url);?>" 
                    <?php if(!empty($_GET['in_iframe'])){ ?>onClick="window.parent.location = '<?php echo site_url($url); ?>'; return false;"<?php } ?>>
                    
                    <div class="app-icon app-icon-<?php echo str_replace('_v2', '', $module->module_id)?>"></div>
                    <div class="app-content">
                        <div class="app-name">
                            <?php echo lang($module->name_lang_key); ?>
                        </div>
                        <div class="app-description">
                            <?php echo lang($module->desc_lang_key);?>
                        </div>
                    </div>
                </a>
            </li>
        <?php
        }
    }
    ?>
    <li>
        <a href="<?php echo site_url("support");?>" target='_blank'>
            <div class="app-icon app-icon-support"></div>
            <div class="app-content">
                <div class="app-name">Support</div>
                <div class="app-description">Help section</div>
            </div>
        </a>
    </li>
</ul>