<?php
$cell_style = '';
if($export_excel == 1)
{
	//$excelXml = new Excel_XML();
	//$excelXml->setWorksheetTitle($title);
	$rows = array();
	#row = array();
	$row= array();
	foreach ($headers as $header) 
	{
		$row[] = strip_tags($header['data']);
	}
	$rows[] = $row;
	
	foreach($data as $datarow)
	{
		$row = array();
		foreach($datarow as $cell)
		{
			$row[] = strip_tags($cell['data']);
		}
		$rows[] = $row;
	}
	if ($data_2)
	{
		//$rows[] = '';
		foreach($data_2 as $datarow)
		{
			$row = array();
			foreach($datarow as $cell)
			{
				$row[] = strip_tags($cell['data']);
			}
			$rows[] = $row;
		}
	}
	foreach($summary_data as $key => $data) {
		$summary_details = array();
		if($key == 'revenue_payments'){
			$summary_details[] = 'Revenue Payments';
		}else if($key == 'non_revenue_payments'){
			$summary_details[] = 'Non Revenue Payments';
		}else if($key == 'total'){
			$summary_details[] = 'Total';
		}
		$summary_details[]= '$'.$data;
		$rows[] = $summary_details;
	}
	//$excelXml->addArray($rows);
	//$excelXml->generateXML($title);
	$content = array_to_csv($rows);
	
	force_download(strip_tags($title) . '.csv', $content);
	exit;
}

?>

<?php 
if ($export_excel == 2)
{
?>
<?php	
	$columns = count($headers);
	
	$width = 660;
	if($orientation && $orientation == 'landscape'){
		$width = 1000;
	}

    $cell_style = 'height="40"';
	$original_cell_style = $cell_style;
    $header_cell_style = 'height="40"';
}
else 
{
    $columns = count($headers);
?>
<style>
	#table_holder {
		width:100%;
	}

	div.header_cell {
		background: none !important;
		border-bottom: none !important;
		height: auto !important;
	}

	#items_table th {
		height: auto !important;
	}
</style>
<?php	
}
?>
<table id="report_contents" <? if($export_excel==2) echo "style='width:100%'"; ?>>
	<tr>
		<td id="item_table">
			<div id="table_holder">
				<table border='0' class="tablesorter report" id="sortable_table" style='width:100%'>
					<thead>
						<?php if ($export_excel == 2) {?>
						<tr>
							<th colspan=<?=$columns?> align='center' width='765'>
								<div style='text-align:center;font-size:20px;font-weight:bold;'>
									<?=$title?>
								</div>
							</th>
						</tr>
						<tr>
							<th colspan=<?=$columns?> align='center' width='765'>
								<div style='text-align:center; font-size:14px;'>
									<?=$subtitle?>
								</div>
							</th>
						</tr>
						<?php } ?>
						<tr>
							<?php foreach ($headers as $key => $header) {
                                if (!empty($header['width'])) {
                                    $header_cell_style .= ' width="'.$header['width'].'"';
                                } else if(isset($width) && isset($columns)) {
                                    $header_cell_style .= ' width="'.($width / $columns).'"';
                                } else {
	                                $header_cell_style = "";
                                }
							if(isset($type) && $type != 'summary_account_balance' && ($key == 3 || $key == 4)){ ?>
								<th <?=$header_cell_style?> width="140" align="<?php echo $header['align'];?>"><div class="header_cell header"><?php echo $header['data']; ?><?php if ($export_excel!=2) {?><span class="sortArrow">&nbsp;</span><?php } ?></div></th>
							<?php }else{ ?>
								<th <?=$header_cell_style?> align="<?php echo $header['align'];?>"><div class="header_cell header"><?php echo $header['data']; ?><?php if ($export_excel!=2) {?><span class="sortArrow">&nbsp;</span><?php } ?></div></th>
							<?php } } ?>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $index => $row) {
							$cell_style = $original_cell_style;
						    ?>
						<tr>
							<?php foreach ($row as $key => $cell) {
                                if (!empty($headers[$key]['width'])) {
                                    $cell_style .= ' width="'.$headers[$key]['width'].'"';
                                } else if(isset($width) && isset($columns)){
                                    $cell_style .= ' width="'.($width / $columns).'"';
                                }
							if(isset($type) && $type != 'summary_account_balance' && ($key == 3 || $key == 4)){ ?>
								<td <?=$cell_style?> width="140" class="<?php echo (isset($is_invoice_report) ? 'invoice_report colorbox' : ''); ?>" align="<?php echo $cell['align'];?>" colspan ="<?php echo isset($cell['colspan']) ? $cell['colspan'] : 1 ;?>"  style ="<?php echo isset($cell['top_border']) ? 'border-top:solid 2px #545454;' : '' ;?> <?php echo isset($cell['background']) ? 'background:'.$cell['background'].';' : '' ;?> <?php echo isset($cell['font-size']) ? 'font-size:'.$cell['font-size'].'px;' : '' ;?>" ><?php echo ($export_excel ==2 ? substr($cell['data'],0,25):$cell['data']); ?></td>
							<?php }else{ ?>
								<td <?=$cell_style?> class="<?php echo (isset($is_invoice_report) ? 'invoice_report colorbox' : ''); ?>" align="<?php echo isset($cell['align'])? $cell['align']:"";?>" colspan ="<?php echo isset($cell['colspan']) ? $cell['colspan'] : 1 ;?>"  style ="<?php echo isset($cell['top_border']) ? 'border-top:solid 2px #545454;' : '' ;?> <?php echo isset($cell['background']) ? 'background:'.$cell['background'].';' : '' ;?> <?php echo isset($cell['font-size']) ? 'font-size:'.$cell['font-size'].'px;' : '' ;?>" ><?php echo ($export_excel ==2 ? substr($cell['data'],0,25):$cell['data']); ?></td>
							<?php }  } ?>
						</tr>
						<?php } ?>
						<?php if (isset($data_2)) {
                            foreach ($data_2 as $row) { ?>
                                <tr>
                                    <?php foreach ($row as $cell) { ?>
                                        <td <?= $cell_style ?>
                                            align="<?php echo $cell['align']; ?>"><?php echo $cell['data']; ?></td>
                                    <?php } ?>
                                </tr>
                            <?php }
                        } ?>
					</tbody>
				</table>
			</div>

			<table id="report_summary" class="tablesorter report">
			<?php foreach($summary_data as $name=>$value) {
				if($name == "custom"){
					continue;
				}
				?>
				<tr class="summary_row"><td width='382' align='right'><?php echo "<strong>".lang('reports_'.$name). '</strong>: '.to_currency($value); ?></td></tr>
			<?php }?>
				<?php if(isset($summary_data["custom"])): ?>
					<?php foreach($summary_data["custom"] as $name=>$value) : ?>
						<tr class="summary_row"><td width='382' align='right'><?php echo "<strong>".$name. '</strong>: '.to_currency($value); ?></td></tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</table>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php if ($export_excel != 2) { ?>

<script type="text/javascript" language="javascript">
function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(); 
	}
}
$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	init_table_sorting();	
});
</script>
<?php } ?>
