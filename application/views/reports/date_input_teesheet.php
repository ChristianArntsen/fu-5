<?php $this->load->view("partial/header_new"); ?>
<div id="page_title" style="margin-bottom:8px;"><?php echo lang('reports_report_input'); ?></div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
?>
	<!--?php echo form_label(lang('reports_date_range'), 'report_date_range_label', array('class'=>'required')); ?>
	<div id='report_date_range_simple'>
		<input type="radio" name="report_type" id="simple_radio" value='simple' checked='checked'/>
		<?php echo form_dropdown('report_date_range_simple',$report_date_range_simple, '', 'id="report_date_range_simple"'); ?>
	</div>
	
	<div id='report_date_range_complex'>
		<input type="radio" name="report_type" id="complex_radio" value='complex' />
		<?php echo form_dropdown('start_month',$months, $selected_month, 'id="start_month"'); ?>
		<?php echo form_dropdown('start_day',$days, $selected_day, 'id="start_day"'); ?>
		<?php echo form_dropdown('start_year',$years, $selected_year, 'id="start_year"'); ?>
		-
		<?php echo form_dropdown('end_month',$months, $selected_month, 'id="end_month"'); ?>
		<?php echo form_dropdown('end_day',$days, $selected_day, 'id="end_day"'); ?>
		<?php echo form_dropdown('end_year',$years, $selected_year, 'id="end_year"'); ?>
	</div>
	<?php 
	if ($type == 'sales' || $type == 'payments' || $type == 'categories') {
	echo form_label(lang('reports_department'), 'reports_department_label', array('class'=>'required')); ?>
	<div id='report_sale_type'>
		<?php echo form_dropdown('sale_department', $departments, 'all', 'id="sale_department"'); ?>
	</div>
	<?php
	}
	echo form_label(lang('reports_sale_type'), 'reports_sale_type_label', array('class'=>'required')); ?>
	<div id='report_sale_type'>
		<?php echo form_dropdown('sale_type',array('all' => lang('reports_all'), 'sales' => lang('reports_sales'), 'returns' => lang('reports_returns')), 'all', 'id="sale_type"'); ?>
	</div>
	
	<div>
		<?php echo lang('reports_export'); ?>: 
		<input type="radio" name="export_excel" id="export_excel_no" value='0' checked='checked' /> <?php echo lang('common_no'); ?>
		<input type="radio" name="export_excel" id="export_excel_yes" value='1' /> <?php echo lang('common_excel_export'); ?>
		<?php if ($type != 'receivings' && $type != 'taxes' && $type != 'sales') { ?>
		<input type="radio" name="export_excel" id="export_pdf_yes" value='2' /> <?php echo lang('common_pdf_export'); 
		}
		?>
	</div-->
	<?php echo form_label(lang('reports_date'), 'report_date_label', array('class'=>'required')); ?>
	<div id='report_date_simple'>
		<?php echo form_input(array('name'=>'report_date', 'id'=>'report_date', 'value'=>date('m/d/Y'))); ?>
	</div>
	<?php echo form_label(lang('reports_teesheet'), 'report_teesheet_label', array('class'=>'required')); ?>
	<div id='report_teesheet_simple'>
		<?php echo $report_teesheets; ?>
	</div>
<?php
echo form_button(array(
	'name'=>'generate_report',
	'id'=>'generate_report',
	'content'=>lang('common_submit'),
	'class'=>'submit_button')
);
?>

<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$('#report_date').datepicker();
	$("#generate_report").click(function()
	{
		var report_date = new Date($("#report_date").val());
		var teesheet_id = $('#teesheetMenu').val();
		var year = report_date.getFullYear();
		var month = report_date.getMonth();
		var day = report_date.getDate();
		var m_z = (month < 10)?'0':'';
		var d_z = (day < 10)?'0':'';
		var final_date = year+m_z+month+d_z+day;
		
		window.location = window.location+'/'+final_date + '/'+ teesheet_id;
	});
});
</script>