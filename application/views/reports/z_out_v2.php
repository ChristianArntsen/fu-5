	<?php if ($export_excel == 1) {
	//$excelXml = new Excel_XML();
	//$excelXml->setWorksheetTitle($title);
	$rows = array();
	$row= array($this->session->userdata('course_name'),'Z Out Report','','','','');
	$rows[] = $row;
	$row= array('for '.date('m/d/Y', strtotime($start_date)).' - '.date('m/d/Y', strtotime($end_date)),'','','','','');
	$rows[] = $row;
	$row= array('','','','','','');
	$rows[] = $row;
	$row= array('Drawer Count','','','Sales','','');
	$rows[] = $row;
	$row= array('Description','System','Count','Description','Count','Amount');
	$rows[] = $row;
	$row = array();
	$total_revenue = 0;
	$total_payments = $payments['Total']['payment_amount'];
	$total_count = 0;
	$over_under = $cash_over = $cash_under = 0;
	unset($payments['Total']);
	$count = (count($revenue) > count($payments) ? count($revenue) : count($payments));
	$pk = $pay_keys = array_keys($payments);
	$tip_total = 0;
	for ($i = 0; $i < $count; $i++) {
		$row = array();
		$total_revenue += $revenue[$i]['subtotal'];
		$payment_amount = ($payments[$pk[$i]]['payment_type'] == '') ? '' : $payments[$pk[$i]]['payment_amount'];
		if ($payments[$pk[$i]]['payment_type'] == 'Tips')
			$tip_total = $payment_amount;
		$row[] = $payments[$pk[$i]]['payment_type'];
		$row[] = number_format($payment_amount,2);
		if ($pk[$i] == 'Cash' && $this->config->item('track_cash') && $show_cash_log)
		{
			$over_under = $payment_amount-$drawer_total;
			$cash_over = ($over_under > 0 ? '' : -$over_under);
			$cash_under = ($over_under < 0 ? '' : -$over_under);
			$total_count += $drawer_total;
			$row[] = number_format($drawer_total,2);
		}
		else {
			$total_count += $payment_amount;
			$row[] = number_format($payment_amount,2);
		}
		$row[] = $revenue[$i]['category'];
		$row[] = $revenue[$i]['count'];
		$row[] = number_format($revenue[$i]['subtotal'],2);
		$rows[] = $row;
	}
	$row= array('Total',number_format($total_payments,2),number_format($total_count,2),'Revenue','',number_format($total_revenue,2));
	$rows[] = $row;
	$row= array('Difference','',number_format($over_under,2),'','','');
	$rows[] = $row;
	$row= array('','','','','','');
	$rows[] = $row;
	$row= array('','','','Taxes/Partial Returns','','');
	$rows[] = $row;
	$row= array('','','','Description','','Amount');
	$rows[] = $row;

	$tax_total = $debit_total = 0;
	$tk = $tax_keys = array_keys($taxes);
	for ($i = 0; $i < count($taxes); $i++) {
		$tax_total += $taxes[$tk[$i]]['tax'];
		if ($taxes[$tk[$i]]['tax'] != 0)
		{
			$row= array('','','',$taxes[$tk[$i]]['percent'],'',number_format($taxes[$tk[$i]]['tax'],2));
			$rows[] = $row;
		}
	}
	if ($tip_total > 0)
    {
        $tax_total += $tip_total;
        $row= array('','','','Tips','',number_format($tip_total,2));
        $rows[] = $row;
    }
	$row= array('','','','Total','',number_format($tax_total,2));
	$rows[] = $row;
	$row= array('','','','','','');
	$rows[] = $row;
	$row= array('Drawer Count','',number_format($total_count,2),'Revenue','',number_format($total_count,2));
	$rows[] = $row;
	$row= array('','','','Taxes/Partial Returns','',number_format($tax_total,2));
	$rows[] = $row;
	$row= array('Cash Short','',number_format(-$cash_under,2),'Cash Over','',number_format($cash_over,2));
	$rows[] = $row;
	$row= array('Total ','',number_format($total_count+$debit_total-$cash_under,2),'Total ','',number_format($total_revenue+$tax_total+$cash_over,2));
	$rows[] = $row;

	//$excelXml->addArray($rows);
	//$excelXml->generateXML($title);
	$content = array_to_csv($rows);

	force_download(strip_tags('Z Out Report') . '.csv', $content);
	exit;
	}

// PDF OR HTML VERSIONS FOLLOW
$deduct_tips = $this->config->item('deduct_tips');
?>

<table id="report_contents" cellpadding="0" cellspacing="0" style="font-size: 11px;"><!-- id="item_table" -->
	<?php if ($export_excel == 2) { ?>
		<tr>
			<td colspan=8 style='text-align:center;'><?=$this->session->userdata('course_name')?></td>
		</tr>
		<tr>
			<td colspan=8 style='text-align:center;'>Z Out Report</td>
		</tr>
		<tr>
			<td colspan=8 style='text-align:center;'>for <?=date('m/d/Y', strtotime($start_date))?> - <?=date('m/d/Y', strtotime($end_date))?></td>
		</tr>
		<tr>
			<td colspan=8 style='text-align:center;'>Generated</td>
		</tr>
		<tr>
			<td colspan=8 style='text-align:center;'><?=date('m/d/Y h:ia')?></td>
		</tr>
		<tr>
			<td colspan=8><div style='width:750px'>&nbsp;</div></td>
		</tr>
	<?php } ?>
	<tr style='text-align:center; font-weight:bold;'>
		<td colspan=4 style='padding:8px'>

		</td>
		<td colspan=4 style='padding:8px'>

		</td>
	</tr>
	<tr style='text-align:center; font-weight:bold;'>
		<td colspan=4 style='border-right:1px solid black; border-top:1px solid black;  border-left:1px solid black; padding:8px'>
			Payments
		</td>
		<td colspan=4 style='border-right:1px solid black; border-top:1px solid black; padding:8px'>
			Sales
		</td>
	</tr>
	<!-- TOP LABELS -->
	<tr style='border-bottom:1px solid black;'>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Description
		</td>
		<td style=' padding:2px 5px; text-align:right;'>
			Amount
		</td>
		<td style=' padding:2px 5px; text-align:right;'>
			Tip
		</td>
		<td style='border-right:1px solid black; padding:2px 5px; text-align:right;'>
			<?php if ($show_cash_log) { ?>
			Drawer Count
			<?php } else { ?>
			Adjusted Total
			<?php } ?>
		</td>
		<td style='padding:2px 5px;'>
			Description
		</td>
		<td style=' padding:2px 5px; text-align:right;'>
			Item Count
		</td>
		<td>

		</td>
		<td style='border-right:1px solid black; padding:2px 5px; text-align:right;'>
			Amount
		</td>
	</tr>
	<tr><td colspan=8 style='border-top:1px solid black'></td></tr>
	<?php
	// DEDUCTING TIPS FROM TOTALS
	$payments2 = $payments;
	unset($payments['Tips']);
	$cash_tips = 0;
	$non_cash_rev_tips = 0;
	$non_rev_tips = 0;
	$cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
	$mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');

	if(isset($payments2['Tips']))
	foreach($payments2['Tips']['sub_payments'] AS $type => $tip)
	{
		if ($type == 'Cash')
			$cash_tips += $tip['payment_amount'];
		else if ($type == 'Check')
			$non_cash_rev_tips += $tip['payment_amount'];
		else if ($type == 'M/C' || $type == 'VISA' || $type == 'AMEX' || $type == 'DCVR' || $type == 'DINERS' || $type == 'JCB')
		{
			$payments2['Tips']['sub_payments']['Credit Card']['payment_amount'] += $tip['payment_amount'];
			unset($payments2['Tips']['sub_payments'][$type]);
			$non_cash_rev_tips += $tip['payment_amount'];
		}
		else if ($type == $cdn || $type == $mbn)
		{
			$payments2['Tips']['sub_payments'][lang('sales_account_balance_charges')]['payment_amount'] += $tip['payment_amount'];
			unset($payments2['Tips']['sub_payments'][$type]);
			$non_rev_tips += $tip ['payment_amount'];
		}
		else
			$non_rev_tips += $tip ['payment_amount'];
	}

	$account_revenue = array();
	foreach ($revenue as $index => $item_cat)
	{
		if ($item_cat['accounts_receivable'] > 0)
		{
			$account_revenue[] = $item_cat;
			unset($revenue[$index]);
		}
	}
		//print_r($payments);
	$total_revenue = 0;
	$total_payments = $payments['Total']['payment_amount'];

	if(isset($payments2['Tips']))
	    $total_payments -= $payments2['Tips']['payment_amount'];

	$total_count = 0;
	$over_under = $cash_over = $cash_under = 0;
	unset($payments['Total']);
	$count = (count($revenue) > count($payments) ? count($revenue) : count($payments));
	// SORTING PAYMENTS INTO REVENUE AND NON-REVENUE BEFORE DISPLAYING
	$revenue_payments = array();
	$non_revenue_payments = array();

	$tip_total = 0;
	if(isset($payments2['Tips']))
	    $tip_total = $payments2['Tips']['payment_amount'];

	$partial_returns = 0;
	foreach ($payments as $index => $payment)
	{
		if ($payment['payment_type'] == 'Cash' || $payment['payment_type'] == 'Check' || $payment['payment_type'] == 'Credit Card' || $payment['payment_type'] == 'CC Refund' || $payment['payment_type'] == 'Partial CC Refund')
		{
			$revenue_payments[$index] = $payment;
			if(isset($payments['payment_amount']))
			    $revenue_payment_total += $payment['payment_amount'];
		}
		else
			$non_revenue_payments[$index] = $payment;

		if (strpos($payment['payment_type'], 'Refund') !== false && $payment['payment_type'] != 'CC Refund')//($payment['payment_type'] == 'Partial CC Refund')
			$partial_returns += $payment['payment_amount'];
	}
	$revenue_payments = count($revenue_payments) > 0 ? $revenue_payments : array();
	$non_revenue_payments = count($non_revenue_payments) > 0 ? $non_revenue_payments : array();
	$payments = array_merge($revenue_payments,$non_revenue_payments);
	$pk = $pay_keys = array_keys($payments);
	$revenue_payment_total = 0;
	$non_revenue_payment_total = 0;
	$revenue_payment_count_total = 0;
	$non_revenue_payment_count_total = 0;
	$accounts_paid_html = array();
	$index = $ii = $item_incrementor = 0;
	$count = max(count($payments),count($revenue));
	for($i = 0; $i<$count;$i++) {
		$pmt = $payments[$pk[$i]];
		if(!isset($revenue[$i]))$revenue[$i] = array();
		$total_revenue += $revenue[$i]['subtotal'];
		$payment_amount = ($pmt['payment_type'] == '') ?
			'' :
			$pmt['payment_amount'];

		if ($pmt['payment_type'] == 'Cash' || $pmt['payment_type'] == 'Check' || $pmt['payment_type'] == 'Credit Card' || strpos($pmt['payment_type'], 'CC Refund') !== false)
		{
			$revenue_payment_total += $pmt['payment_amount'];
			$revenue_payment_count_total += ($pk[$i] == 'Cash' && $this->config->item('track_cash') && $show_cash_log) ? $drawer_total : $payment_amount;
			//print_r($payments2['Tips']['subpayments']);
			if ($deduct_tips && $pk[$i] != 'Cash')
				$revenue_payment_count_total +=  $payments2['Tips']['sub_payments'][$pk['i']]['payment_amount'];

		}
		else
		{
			$non_revenue_payment_total +=$pmt['payment_amount'];
			$non_revenue_payment_count_total += $payment_amount;
		}
	?>
	<tr>
		<td style='padding:2px 5px; <?=($export_excel == 2) ? 'width:140px;' : '' ?> border-left:1px solid black;'>
            <?php if($export_excel == 2){ ?>
	            <?=wordwrap($pmt['payment_type'],28,'<br />');?>
            <?php } else { ?>
	            <?=$pmt['payment_type'];?>
            <?php } ?>
		</td>
		<td style='text-align:right; padding:2px 5px;'>
			<?=number_format($payment_amount,2);?>
		</td>
		<td style=' padding:2px 5px; text-align:right;'>
			<?php
			if(isset($payments2['Tips'])) {
				if ($pmt['payment_type'] == 'Cash') {
					if ($deduct_tips)
						echo '(' . number_format($payments2['Tips']['payment_amount'] - $payments2['Tips']['sub_payments'][$pmt['payment_type']]['payment_amount'], 2) . ')';
					else
						echo number_format($payments2['Tips']['sub_payments']['Cash']['payment_amount'], 2);
				} else if (isset($payments2['Tips']['sub_payments'][$pmt['payment_type']]))
					echo number_format($payments2['Tips']['sub_payments'][$pmt['payment_type']]['payment_amount'], 2);
			}else echo number_format(0,2);
			?>
		</td>
		<td style='border-right:1px solid black; text-align: right; padding:2px 5px;'>
			<?php
				$tip = 0;
				if(isset($payments2['Tips'])) $tip = $payments2['Tips']['sub_payments'][$pmt['payment_type']]['payment_amount'];

				if ($pk[$i] == 'Cash' && $this->config->item('track_cash') && $show_cash_log)
				{
					if ($deduct_tips)
					{
						$non_cash_tip_adjustment = $payments2['Tips']['payment_amount'] - $tip;
						$revenue_payment_count_total += $non_cash_tip_adjustment;
						$over_under = $payment_amount-($drawer_total + $non_cash_tip_adjustment);
					}
					else
					{
						$over_under = $payment_amount + $cash_tips - $drawer_total;
						$non_revenue_payment_count_total += $cash_tips;
					}

					$cash_over = ($over_under > 0 ? '' : -$over_under);
					$cash_under = ($over_under < 0 ? '' : -$over_under);
					$total_count += $drawer_total;
					echo number_format($drawer_total,2);

				}
				else {

					$total_count += $payment_amount;


					if ($pk[$i] == 'Cash' && $deduct_tips)
					{
						$non_cash_tip_adjustment = 0;
						if(isset($payments2['Tips']))
						    $non_cash_tip_adjustment = $payments2['Tips']['payment_amount'] - $tip;
						echo number_format($payment_amount-$non_cash_tip_adjustment,2);
						//$revenue_payment_count_total += $non_cash_tip_adjustment;
						$total_count -= $non_cash_tip_adjustment;
					}
					else if ($pk[$i] == 'Cash')
					{
						$cash_tip_adjustment = $tip;
						echo number_format($payment_amount+$cash_tip_adjustment,2);
						$non_revenue_payment_count_total += $cash_tip_adjustment;
						$total_count += $cash_tip_adjustment;
					}
					else if ($pk[$i] != '')
					{
						$non_cash_tips = $tip;
						echo number_format($payment_amount+$non_cash_tips,2);
						$total_count +=$non_cash_tips;
						$non_revenue_payment_count_total += $non_cash_tips;
					}
				}
			?>
		</td>
	<?php
	$blank_cell_html = "		
			<td style='padding:2px 5px;'>
			</td>
			<td style='text-align:right; padding:2px 5px'>
			</td>
			<td></td>
			<td style='border-right:1px solid black; text-align:right; padding:2px 5px'>
			</td>
		";
	if ($revenue[$i]['category'] == 'Account Payments' || $revenue[$i]['category'] == 'Member Account Payments' || $revenue[$i]['category'] == 'Invoice Payments')
	{
		//echo $revenue[$i]['category'];
		$accounts_paid_html[] = "		
			<td style='padding:2px 5px;'>
				".$revenue[$i]['category']."
			</td>
			<td style='text-align:right; padding:2px 5px'>
				".number_format($revenue[$i]['count'],2)."
			</td>
			<td></td>
			<td style='border-right:1px solid black; text-align:right; padding:2px 5px'>
				".number_format($revenue[$i]['subtotal'],2)."
			</td>
		";
		echo $blank_cell_html;
	}
	else
	{
		echo "		
			<td style='padding:2px 5px;width:200px; '>
				".$revenue[$i]['category']."
			</td>
			<td style='text-align:right; padding:2px 5px'>
				".number_format($revenue[$i]['count'],2)."
			</td>
			<td></td>
			<td style='border-right:1px solid black; text-align:right; padding:2px 5px'>
				".number_format($revenue[$i]['subtotal'],2)."
			</td>
		";
	}
	?>
	</tr>
	<?php }
	//print_r($accounts_paid_html);
	?>
	<!-- HEADER ROW FOR REVENUE PAYMENTS -->
	<tr style='text-align:center; font-weight:bold; border-top:2px solid black;'>
		<td colspan=4 style='border-right:1px solid black; padding:8px; border-left:1px solid black; border-top:1px solid black;'>
			Revenue/Non-Revenue
		</td>
		<td colspan=4 style='border-right:1px solid black; padding:8px; border-top:1px solid black;'>
			Taxes/Partial Returns
		</td>
	</tr>
	<tr style='border-bottom:1px solid black;'>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Description
		</td>
		<td style=' padding:2px 5px; text-align:right;'>
			Totals
		</td>
		<td></td>
		<td style='border-right:1px solid black;  padding:2px 5px; text-align:right;'>
			Adjusted Totals
		</td>
		<td style='padding:2px 5px;'>
			Description
		</td>
		<td style=' text-align:right; padding:2px 5px;'>

		</td>
		<td></td>
		<td style='border-right:1px solid black; padding:2px 5px; text-align:right;'>
			Amount
		</td>
	</tr>
	<?php
	$tax_total = $debit_total = 0;
	$tk = $tax_keys = array_keys($taxes);
	$count = count($taxes) > 1 ? count($taxes) : 2;
	for ($i = 0; $i < $count; $i++) {
		$tax_total += $taxes[$tk[$i]]['tax'];
	?>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			<?php if ($i == 0) {
				echo 'Revenue Payments Total';
			} else if ($i == 1) {
				echo 'Non Revenue Payments Total';
			}
			?>
		</td>
		<td style='text-align:right; padding:2px 5px;'>
			<?php if ($i == 0) {
				echo number_format($revenue_payment_total,2);
			} else if ($i == 1) {
				echo number_format($non_revenue_payment_total,2);
			}
			?>
		</td>
		<td></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?php if ($i == 0) {
				echo number_format($revenue_payment_count_total,2);
			} else if ($i == 1) {
				echo number_format($non_revenue_payment_count_total,2);
			}
			?>
		</td>
		<td style='padding:2px 5px;'>
			<?php if (isset($taxes[$tk[$i]])) echo $taxes[$tk[$i]]['percent'];?>
		</td>
		<td>

		</td>
		<td></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?php if (isset($taxes[$tk[$i]])) echo number_format($taxes[$tk[$i]]['tax'],2);?>
		</td>
	</tr>
	<?php } ?>
	<?php
	if (!$deduct_tips && $payments2['Tips']['payment_amount'] > 0) {
		$tax_total += $payments2['Tips']['payment_amount'];
	?>
	<tr>
		<td style='border-left:1px solid black;'>

		</td>
		<td>

		</td>
		<td></td>
		<td style='border-right:1px solid black;'>

		</td>
		<td style='padding:2px 5px;'>
			Tips
		</td>
		<td>

		</td>
		<td></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?=number_format($tip_total,2)?>
		</td>
	</tr>
	<?php } ?>
	<?php
	if ($partial_returns != 0) {
		$tax_total += $partial_returns;
	?>
	<tr>
		<td style='border-left:1px solid black;'>

		</td>
		<td>

		</td>
		<td></td>
		<td style='border-right:1px solid black;'>

		</td>
		<td style='padding:2px 5px;'>
			Partial Returns
		</td>
		<td>

		</td>
		<td></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?=number_format($partial_returns,2)?>
		</td>
	</tr>
	<?php } ?>
	<!-- THIS IS THE THIRD ACCOUNTS PAID LINE -->
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
		</td>
		<td style='text-align:right;  padding:2px 5px;'>

		</td>
		<td></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>

		</td>
		<!-- NEED TO HAVE TAXES/PARTIAL RETURNS HERE -->

	</tr>
	<!-- THIS ROW IS THE PAYEMENTS TOTAL ROW -->
	<tr style='border-top:2px solid black;'>
		<td style='padding:2px 5px;  border-left:1px solid black; border-top:1px solid black;'>
			Total
		</td>
		<td style='text-align:right; padding:2px 5px; border-top:1px solid black;'>
			<?php //number_format($total_payments,2)?>
		</td>
		<td style='border-top:1px solid black;'></td>
		<td style='border-right:1px solid black; text-align:right; border-top:1px solid black; padding:2px 5px;'>
			<?=number_format($total_count,2)?>
		</td>
		<!-- NEED TO HAVE TAXES/PARTIAL RETURNS HERE -->
		<td style='padding:2px 5px;  border-top:1px solid black;'>
			Total
		</td>
		<td style='padding:2px 5px; border-top:1px solid black;'>

		</td>
		<td style='padding:2px 5px; border-top:1px solid black;'></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px; border-top:1px solid black;'>
			<?=number_format($tax_total,2)?>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Difference
		</td>
		<td style='text-align:right; padding:2px 5px;'>
		</td>
		<td></td>
		<td style='border-right:1px solid black;  text-align:right; padding:2px 5px;'>
			<?=number_format($over_under,2)?>
		</td>
		<td>

		</td>
		<td>

		</td>
		<td></td>
		<td style='border-right:1px solid black;'>

		</td>
	</tr>
	<tr><td colspan=8 style='border-top:1px solid black'></td></tr>
	<tr style='border-top:1px solid black;'>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Drawer Count
		</td>
		<td>

		</td>
		<td></td>
		<td style=' text-align:right; padding:2px 5px;'>
			<?= number_format($total_count,2)?>
		</td>
		<td style='padding:2px 20px;'>
			Sales
		</td>
		<td>

		</td>
		<td></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?= number_format($total_revenue,2); ?>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			+ Cash Short
		</td>
		<td>

		</td>
		<td></td>
		<td style=' text-align:right; padding:2px 5px;'>
			<?=number_format(-$cash_under,2)?>
		</td>
		<td style='padding:2px 20px;'>
			+ Cash Over
		</td>
		<td>
		</td>
		<td></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?=number_format($cash_over,2)?>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>

		</td>
		<td>

		</td>
		<td></td>
		<td style=' text-align:right; padding:2px 5px;'>

		</td>
		<td style='padding:2px 20px;'>
			+ Taxes/Partial Returns
		</td>
		<td>

		</td>
		<td></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?= number_format($tax_total,2);?>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			<!--- Cash Open-->
		</td>
		<td>

		</td>
		<td></td>
		<td style=' text-align:right; padding:2px 5px;'>
		</td>
		<td>
		</td>
		<td>

		</td>
		<td></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
		</td>
	</tr>
	<tr>
		<td style='padding:2px 5px;  border-left:1px solid black;'>
			Total
		</td>
		<td>

		</td>
		<td></td>
		<td style='text-align:right; padding:2px 5px;'>
			<?=number_format($total_count+$debit_total-$cash_under,2);?>
		</td>
		<td style='padding:2px 20px;'>
			Total
		</td>
		<td>

		</td>
		<td></td>
		<td style='border-right:1px solid black; text-align:right; padding:2px 5px;'>
			<?=number_format($total_revenue+$tax_total+$cash_over,2);?>
		</td>
	</tr>
	<tr><td colspan=8 style='border:1px solid black'><div style='text-align: center; padding:10px;'><?php echo lang('reports_z_out_note');?></div></td></tr>
	<tr><td colspan=8 style='border-top:1px solid black'></td></tr>

</table>
<div id="feedback_bar"></div>
