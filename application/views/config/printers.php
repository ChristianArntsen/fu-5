<style>
table.printers th, table.printers td {
	padding: 5px 20px;
	text-align: left;
}

table.printers th {
	background-color: #F0F0F0;
	border-bottom: 1px solid #E0E0E0;
}

table.printers {
	margin-bottom: 15px;
}

table.printers .centered {
	text-align:center;
}

.edit_printer, .delete_printer, #add_printer, .save_printer {
	color:blue;
	cursor:pointer;
}

div.printers {
	min-height: 400px; 
	padding: 15px;
}

div.printers h3 {
	margin: 10px 0px;
}
</style>
<div class="printers">
	<h3>Printer Groups</h3>
	<table class="printers" id='printer_groups'>
		<thead>
			<tr>
				<th valign="top">Label</th>
				<th valign="top" style="text-align: right;"><a id='add_printer_group' href="#">Add Printer Group</a></th>
			</tr>
		</thead>
		<tbody>	
			<?php 
			if(!empty($printer_groups)){
			foreach($printer_groups as $printer_group) { ?>
				<tr id='printer_<?=$printer_group['printer_group_id'];?>' data-printer-group-id="<?=$printer_group['printer_group_id'];?>">
					<td><input type='text' class="printer-label" value='<?php echo $printer_group['label']; ?>' disabled/></td>
					<td class='centered'><span class='edit_printer'>Edit</span> - <span class='delete_printer'>Delete</span></td>
				</tr>
			<?php } }else{ ?>
				<tr class="placeholder">
					<td colspan="2">No printer groups created</td>
				</tr>				
			<?php } ?>
		</tbody>
	</table>

	<h3>Printers</h3>
	<table class="printers" id='printers'>
		<thead>
			<tr>
				<th valign="top">Label</th>
				<th valign="top">IP Address</th>
				<th valign="top" style="text-align: right;"><a id='add_printer' href="#">Add Printer</a></th>
			</tr>
		</thead>
		<tbody>	
			<?php 
			if(!empty($printers)){
			foreach($printers as $printer) { ?>
				<tr id='printer_<?=$printer['printer_id'];?>' data-printer-id="<?=$printer['printer_id'];?>">
					<td><input type='text' class="printer-label" value='<?php echo $printer['label']; ?>' disabled/></td>
					<td><input type='text' class="printer-ip-address" value='<?php echo $printer['ip_address']; ?>' disabled/></td>
					<td class='centered'><span class='edit_printer'>Edit</span> - <span class='delete_printer'>Delete</span></td>
				</tr>
			<?php } }else{ ?>
				<tr class="placeholder">
					<td colspan="3">No printers created</td>
				</tr>				
			<?php } ?>
		</tbody>
	</table>
</div>
<script>
$(document).ready(function(){
	$('#add_printer').click(function(){
		printer.new();
		return false
	});

	$('#printers').on('click', '.edit_printer', function(e){
		e.preventDefault();
		printer.edit(this);
		return false
	});
	
	$('#printers').on('click', '.save_printer', function(e){
		e.preventDefault();
		printer.save(this);
		return false
	});	

	$('#printers').on('click', '.delete_printer', function(e){
		e.preventDefault();
		printer.remove(this);
		return false
	});
	
	$('#add_printer_group').click(function(){
		printer_group.new();
		return false
	});

	$('#printer_groups').on('click', '.edit_printer', function(e){
		e.preventDefault();
		printer_group.edit(this);
		return false
	});
	
	$('#printer_groups').on('click', '.save_printer', function(e){
		e.preventDefault();
		printer_group.save(this);
		return false
	});	

	$('#printer_groups').on('click', '.delete_printer', function(e){
		e.preventDefault();
		printer_group.remove(this);
		return false
	});	

});

var printer = {
	
	remove : function(link){
		var row = $(link).closest("tr");
		var printer_id = row.attr('data-printer-id');
		
		if(!confirm('Are you sure you want to delete this printer?')){
			return false;
		}		
		
		if(!printer_id){
			$(row).remove();
			return true;
		}
		
		$.post(SITE_URL+'/config/delete_printer', {'printer_id': printer_id}, function(response){
			$(row).remove();
		});
	},
	
	edit : function(link){
		// ENABLE ROW FOR EDITING
		var row = $(link).closest("tr");
		row.find('input').removeAttr('disabled');
		row.find('.edit_printer').replaceWith('<span class="save_printer">Save</span>');
	},
	
	save : function(link){
		var row =  $(link).closest("tr");
		var data = {
			'label': row.find('input.printer-label').val(),
			'ip_address': row.find('input.printer-ip-address').val(),
		};
		
		var url = SITE_URL + '/config/save_printer';
		var printer_id = row.attr('data-printer-id');
		
		if(printer_id != undefined){
			url+= '/' + printer_id;
		}
		
		$.post(url, data, function(response){
			row.find('input').attr('disabled', 'disabled');
			row.find('.save_printer').replaceWith('<span class="edit_printer">Edit</span>');
			row.attr('data-printer-id', response.printer_id);	
		}, 'json');			
	},

	new: function(){
		var new_row = $("<tr id='new_printer'>"+
			"<td><input type='text' class='printer-label' value=''/></td>"+
			"<td><input type='text' class='printer-ip-address' value=''/></td>"+
			"<td class='centered'><span class='save_printer'>Save</span> - <span class='delete_printer'>Delete</span></td>"+
		"</tr>");
		
		$('#printers tbody').find('tr.placeholder').remove();
		$('#printers tbody').append(new_row);    
		new_row.find('input:first').focus();      
	}
};

var printer_group = {
	
	remove : function(link){
		
		if(!confirm('Are you sure you want to delete this printer group? The group will be removed from all inventory items it is assigned to')){
			return false;
		}
		
		var row = $(link).closest("tr");
		var printer_group_id = row.attr('data-printer-group-id');
		
		if(!printer_group_id){
			$(row).remove();
			return true;
		}
		
		$.post(SITE_URL+'/config/delete_printer_group', {'printer_group_id': printer_group_id}, function(response){
			$(row).remove();
		});
	},
	
	edit : function(link){
		// ENABLE ROW FOR EDITING
		var row = $(link).closest("tr");
		row.find('input').removeAttr('disabled');
		row.find('.edit_printer').replaceWith('<a href="#" class="save_printer">Save</a>');
	},
	
	save : function(link){
		var row =  $(link).closest("tr");
		var data = {
			'label': row.find('input.printer-label').val(),
		};
		
		var url = SITE_URL + '/config/save_printer_group';
		var printer_group_id = row.attr('data-printer-group-id');
		
		if(printer_group_id != undefined){
			url+= '/' + printer_group_id;
		}
		
		$.post(url, data, function(response){
			row.find('input').attr('disabled', 'disabled');
			row.find('.save_printer').replaceWith('<a href="#" class="edit_printer">Edit</a>');
			row.attr('data-printer-group-id', response.printer_group_id);
		}, 'json');			
	},

	new: function(){
		var new_row = $("<tr id='new_printer_group'>"+
			"<td><input type='text' class='printer-label' value=''/></td>"+
			"<td class='centered'><a class='save_printer' href='#'>Save</a> - <a class='delete_printer' href='#'>Delete</a></td>"+
		"</tr>");
		
		$('#printer_groups tbody').find('tr.placeholder').remove();
		$('#printer_groups tbody').append(new_row);    
		new_row.find('input:first').focus();   
	}
};
</script>
