<style type="text/css">
#ui-datepicker-div{
	 width: 16.2em !important;
}
</style>

<h2 class="pricing_teesheet"><?= $teesheet_info->title; ?></h2>
<div class="sidebar">
	<div class="pricing_buttons">
	 	<div class="terminal_button" id="9_hole_pricing">9 Hole</div>
	    <div class="terminal_button" id="18_hole_pricing">18 Hole</div>
       	<input type="text" id="popupDatepicker">
        <input class="teesheet_id" id="teesheet_id" type="hidden" name="teesheet_id" value="<?= $teesheet_info->teesheet_id; ?>">
 	</div>
 </div>
	
<table border="1px" id="contents" class="special_table">
	<?php $incr =0; foreach ($grid_pricing as $key => $prclass) { ?>
		<tr class="noBorder">	
			<td><div class="teetime_price_classes"><?php echo $week_days[$incr].'-'.$key; ?></div></td>		
		</tr>
		<tr> 
			<td><div class="header_cell header">Price Class</div></td>
			<?php foreach ($teetimes as $k => $time) { ?>
				<td><div class="header_cell header pricing_teetimes"><?php echo $time; ?></div></td> 
			<?php } ?>	
		</tr>
		<?php foreach ($prclass as $key => $teetime) { ?>
			<tr>
				<td style='background-color:<?php echo $teetime['color']; ?>' class="teetime_price_classes cell_border"><?php echo $key; ?></td>
				<?php foreach ($teetime as $key => $value) { 
					if ($key != 'color') {
						if (($value['price1'] && $value['price3']) != '') {?>										 
					 		<td style='background-color:<?php echo $teetime['color']; ?>' class="pricing_hole hole_prices cell_border"><?php echo $value['price1']."/".$value['price3'] ;?></td> 
					 	<?php  } else{?>
					 			<td class="cell_border"></td>
					 		<?php }?>
					 	<?php if (($value['price2'] && $value['price4']) != '') { ?>
					 		<td style='background-color:<?php echo $teetime['color']; ?>' class="pricing_holes pricing_teetime hole_prices cell_border"><?php echo $value['price2']."/".$value['price4'] ;?></td>  
						<?php } ?>		
				<?php } } ?>	
			</tr>
		<?php } $incr++;?>	

	<?php } ?>	
</table>
<script type="text/javascript">
	$(document).ready(function () {
	   	var startDate,endDate,
	        selectCurrentWeek = function () {
	            window.setTimeout(function () {
	                $('#popupDatepicker').datepicker('widget').find('.ui-datepicker-current-day a').addClass('ui-state-active')
	            }, 1);
	        };
		$('#popupDatepicker').datepicker({
	        "showOtherMonths": false,
	        "selectOtherMonths": false,
	        "onSelect": function (dateText, inst) {
	            var teesheet_id = $('#teesheet_id').val();
	            var date = $(this).datepicker('getDate'),
	                dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
	            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay()+1);
	            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7);
	            $('#popupDatepicker').val($.datepicker.formatDate("yy-m-d", startDate, inst.settings) + ' - ' + $.datepicker.formatDate("yy-m-d", endDate, inst.settings));
	            var start_date =$.datepicker.formatDate("yy-m-d", startDate, inst.settings);
	            selectCurrentWeek();
	            $.ajax({
		            type: "POST",
		           	url: "index.php/seasons/pricing_graphics_view/"+teesheet_id,
		            data: {startDate:start_date,status:1},
		            success: function(response){
		            	$('#cboxLoadedContent').html(response);
		            	$('#popupDatepicker').val($.datepicker.formatDate("yy-m-d", startDate, inst.settings) + ' - ' + $.datepicker.formatDate("yy-m-d", endDate, inst.settings));
		            }
	        	});
	        },
	        "beforeShow": function () {
	            selectCurrentWeek();
	        }    
	    });
		$( "#9_hole_pricing" ).click(function() {
			$( ".pricing_hole" ).addClass( "pricing_holes" );
			$( ".pricing_teetime" ).removeClass( "pricing_holes" );
		});
		$( "#18_hole_pricing" ).click(function() {
			$( ".pricing_hole" ).removeClass( "pricing_holes" );
			$( ".pricing_teetime" ).addClass( "pricing_holes" );
		});
	});
</script>