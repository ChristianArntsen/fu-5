<?php
	echo form_open('seasons/save_special_options/'.$specials['special_id'], array('id'=>'special_form'));

$days_of_week = array(
	'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'
);

if($specials['date'] == '0000-00-00'){
	$is_daily = true;
}else{
	$is_daily = false;
}
?>
<fieldset id="dailyspecials_basic_info">
	<div class="field_row clearfix dailyspecials">
		<?php echo form_label('Active:', 'special_active', array('class'=>'special_labels'));?>
		<div class='form_field'>
			<?php echo form_checkbox(array('name' => 'active', 'id' => 'special_active', 'value' => 1, 'checked' => ($specials['active'] == 1)));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('Name:', 'specialname',array('class'=>'special_labels')); ?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'special_name', 'id' => 'special_name', 'value' => $specials['name']));?>
		</div>
	</div>
    <div class="field_row clearfix">
        <?php echo form_label('Booking Class:', 'booking_class_id',array('class'=>'special_labels')); ?>
        <div class='form_field'>
            <?php echo form_dropdown('booking_class_id', $booking_classes, $specials['booking_class_id'], 'id="booking_class_id"');?>
        </div>
    </div>
	<div class="field_row clearfix">
		<label style="margin-bottom: 50px; width: 164px">When:</label>
		<div class='form_field' style="float: left">
			<label style="width: 100px"><input type="radio" name="when" value="date" class="special-when" <?php if(!$is_daily){ echo 'checked';} ?>> Date</label> 
			<?php echo form_input(array('name' => 'special_date', 'id' => 'special_date', 'value' => $specials['date']));?>
		</div>
		<div class='form_field' style="float: left; width: 600px; overflow: hidden; height: 40px;">
			<label style="width: 100px"><input type="radio" name="when" value="daily" class="special-when" <?php if($is_daily){ echo 'checked';} ?>>  Daily</label>
			<?php foreach($days_of_week as $day){ ?>
			<label style="display: block; height: 14px; line-height: 14px; width: 60px; float: left;">
				<?php echo form_checkbox(array(
					'name' => $day, 
					'value' => 1,
					'class' => 'special_day',
					'checked' => ($specials[$day] == 1)
				)); ?>
				<?php echo ucfirst(substr($day, 0, 3)); ?>
			</label>
			<?php } ?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('Times:', 'times',array('class'=>'wide'));?>
	</div>		
	<div class="field_row clearfix">
		<?php echo form_label('AM:', 'am',array('class'=>'wide special_am')); ?>
		<div class='form_field'>	
			<?php
				$spec = explode(",", $specials['special_times']);
				foreach ($teetimes as $key=>$value) {
					if($key < 1200){
						$data = array(
		    				'name'        => 'specialtime[]',
		    				'id'          => 'specialtime',
		    				'value'       => $key,
		    				'checked'     => (in_array($key, $spec))? true : false,
		    				'style'       => 'margin:10px',
		    			);
			   			echo "<div class='special_teetimes'>";
			    		echo form_checkbox($data);
			    		echo $value;
			    		echo "</div>";
			    	}		
				}
			?>
		</div>		
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('PM:', 'pm',array('class'=>'wide special_am')); ?> 			
		<div class='form_field'>		
			<?php	
				foreach ($teetimes as $key=>$value) {
					if($key >= 1200){
						$data = array(
		    				'name'        => 'specialtime[]',
		    				'id'          => 'specialtime',
		    				'value'       => $key,
		    				'checked'     => (in_array($key, $spec))? true : false,
		    				'style'       => 'margin:10px',
		    			);
			   			echo "<div class='special_teetimes'>";
			    		echo form_checkbox($data);
			    		echo $value;
			    		echo "</div>";
			    	}		
				}
			?>
		</div>		
	</div>
	<div class="field_row clearfix special_prices">
		<?php echo form_label('18 Hole Green Fee:', '18_hole_green_fee',array('class'=>'special_labels')); ?>
		<?php echo form_label('9 Hole Green Fee:', '9_hole_green_fee',array('class'=>'special_labels')); ?>
		<?php echo form_label('18 Hole Cart:', '18_hole_cart',array('class'=>'special_labels')); ?>	
		<?php echo form_label('9 Hole Cart:', '9_hole_cart',array('class'=>'special_labels')); ?>

		
	</div>
	<div class="field_row clearfix special_prices">
		<?php echo form_input(array('name' => '18_hole_green_fee', 'id' => '18_hole_green_fee', 'value' => $specials['price_1']));?>
		<?php echo form_input(array('name' => '9_hole_green_fee', 'id' => '9_hole_green_fee', 'value' => $specials['price_2']));?>
		<?php echo form_input(array('name' => '18_hole_cart', 'id' => '18_hole_cart', 'value' => $specials['price_3']));?>
		<?php echo form_input(array('name' => '9_hole_cart', 'id' => '9_hole_cart', 'value' => $specials['price_4']));?>
	</div>	
	<?php echo form_input(array('name'=>'teesheet_id','id'=>'teesheet_id','value'=>$teesheet_id,  'type' => 'hidden'));

	 if($is_aggregate){
		echo form_input(array('name'=>'is_aggregate','value'=>1, 'type' => 'hidden'));
	}else{
		echo form_input(array('name'=>'is_aggregate','value'=>0, 'type' => 'hidden'));	
	}
	echo form_submit(array(
		'name'=>'submitButton',
		'id'=>'submitButton',
		'value'=> lang('common_save'),
		'class'=>'submit_button button_submit')
	); ?>		
</fieldset>
<?php
echo form_close();
?>
<script type="text/javascript">
function special_when_toggle(){

	var value = $('input.special-when:checked').val();
	var date_field = $('#special_date');
	var day_fields = $('input.special_day');
	
	if(value == 'date'){
		date_field.attr('disabled',null);
		day_fields.attr('disabled','disabled').prop('checked', null);
	}else{
		date_field.attr('disabled','disabled').val('');
		day_fields.attr('disabled',null);
	}	
}

$(document).ready(function(){
	$('#special_date').datepicker({
        defaultDate: new Date(),
        dateFormat: 'yy-mm-dd',
        timeFormat: 'HH:mm',
        defaultValue: '<?php echo date('Y-m-d'); ?>'
    });


	special_when_toggle();
	$('input.special-when').off().on('change', function(){
		special_when_toggle();
	});
	
	var submitting = false;
	$('#special_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{	
				$.colorbox.close();
				if(response.success == true){
					set_feedback('Successfully added Daily Specials','success_message',false);	
				}else{
					set_feedback('Successfully updated Daily Specials','success_message',false);	
				}
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
			wrapper: "li",
		rules:
		{
		},
		messages:
		{
		}
	});
});
</script>