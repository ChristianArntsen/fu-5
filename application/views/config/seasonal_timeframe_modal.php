<? $disabled = isset($price_info) && $price_info['default'] ? "disabled" : "";?>
<script>
function toggle_timeframe_status(timeframe){		
	var active_checkbox = timeframe.find('input.activate-timeframe');
	if(active_checkbox.length == 0){
		return true;
	}
	
	if(active_checkbox.attr('checked')){
		timeframe.find('input:not(.activate-timeframe), select').attr('disabled', null);
		timeframe.removeClass('disabled');
	}else{			
		timeframe.find('input:not(.activate-timeframe), select').attr('disabled', 'disabled');
		timeframe.addClass('disabled');
	}	
}		
</script>
<style type="text/css">
.pricing_table{
	position:relative;
	border:1px solid #666;
	margin:10px 0;
	padding: 10px;
	width: 520px;
}
.timeframe{
	position:relative;
	border:1px solid #ccc;
	margin: 5px 0px 5px 0px;
	padding:5px;
}
div.timeframe .close{
	position:absolute;
	top:0px;
	right:0px;
	background-color:#F00;
	color:#FFF;
	display:block;
	padding:5px;
	cursor:pointer;
	z-index: 10;
}
.inline_button{
	display:inline-block;
	line-height:20px;
	height:20px;
	width:120px;
	padding:4px 2px;
	margin:0;
}
.time_input{
	margin-right:5px;
}
.price_input{
	margin-right: 5px;
	margin-top: 5px;
	width: 50px !important;	
}
.delete_button{
	color: red;
	display: inline;
	float: right;
	margin-top: -30px;
	font-size: 12px;
	cursor:pointer;
}

div.timeframe label {
	width: 60px;
	display: block;
	height: 34px;
	line-height: 34px;
	float: left;
	padding: 0px;
	font-weight: bold;
	padding-left: 2px;
}

div.timeframe label.price {
	height: 14px;
	line-height: 14px;
	margin-top: 5px;
	margin-right: 4px;
}

div.timeframe div.row.days > label {
	width: 58px;
	float: left;
	font-weight: normal;
}

div.timeframe div.row {
	display: block;
	overflow: hidden;
	width: auto;
	padding: 0px;
	position: relative;
	z-index: 1;
}

div.timeframe div.label_span {
	margin-left: 58px;
}

div.timeframe div label.label_text {
	margin-left: 10px !important;
}

div.timeframe.disabled {
	color: #BBB;
}

div.timeframe.disabled label.active {
	color: #222;
}

div.row.days input {
	margin: 8px 5px 0px 0px;
	position: relative;
	top: 2px;
	left: 2px;
}

div.fee-info {
	width: 125px;
	float: left;
	display: block;
	margin-bottom: 10px;
}

div.fee-info > strong {
	display: block;
}
</style>
<ul id="error_message_box"></ul>
	<h2 style="margin-left: 15px; margin-top: 15px; margin-bottom: 5px;"><?php echo $price_info['name']; ?></h2>

	<?php echo form_open('seasons/save_timeframes/'.$season_info->season_id.'/'.$price_info['class_id'], array('id'=>'timeframes_form')); ?>
	<fieldset id="season_basic_info">
		<div id="pricing_<?=$price_info['class_id']?>" class="pricing_container" style="min-width:500px;min-height:380px;margin:10px;">
			<div id="timeframes_container">
				<? foreach ($timeframes as $timeframe){
					$this->load->view('pricing/timeframe', array('timeframe' => $timeframe, 'price_class' => $price_info));
				} ?>
				<a href="#" rel="add_timeframe" class="terminal_button inline_button" style="display: block; clear: both; width: 150px; margin-right: 5px;">+Add Timeframe</a>
			</div>
			
			<? echo form_submit(array(
				'name' => 'submit_details',
				'id' => 'submit_details',
				'value' => "Save",
				'class' => 'submit_button',
				'style' => 'display: block; width: 100px; margin: 0 auto;')
			); ?>
		<? if ($price_info['default'] == 0){?><a class="delete_button" rel="delete_pricing">Remove Price Class</a><? } ?>
	</div>
	</fieldset>
</form>
<script type="text/javascript">
$(document).ready(function(){
	$('input[name=price_category]').focus();
	
	$('#config_green_fees_form').load('index.php/config/view/season/<?=$season_info->season_id?>');
	function post_pricing_form_submit()
	{
		$('#config_green_fees_form').load('index.php/config/view/season/<?=$season_info->season_id?>');
	}
	
	$('input.activate-timeframe').on('change', function(e){
		var timeframe = $(this).parents('div.timeframe').first();
		toggle_timeframe_status(timeframe);
	});
	
	$('a[rel=delete_pricing]').on('click',function(){
		if (confirm("Are you sure you would like to delete this price class from this season?"))
		{
			$.post("<?php echo site_url(); ?>/seasons/delete_price/<?=$season_info->season_id?>/<?=$price_info['class_id']?>", function(response){		
				if(response.success){
					$.colorbox.close();
					post_pricing_form_submit();
					set_feedback(response.message, 'success_message', false, 1500);
					$('ul.pricing > li[data-class-id='+	<?=$price_info['class_id']?> +']').remove();
					$('select.price_class_menu').append("<option value='<?=$price_info['class_id']?>'><?=$price_info['name']?></option>");	
				}else{
					set_feedback(response.message, 'error_message', false, 2000);							
				}						
			},'json');
		}
		return false;
	});
	
	$('#timeframes_container').on('click', 'a.add_prices', function(){
		var timeframe = $(this).parents('.timeframe');

		var id = timeframe.attr('id').substr(10);
		var appendto = $('#timeframe_'+id).children('div.prices');
	
		$(this).remove();
		appendto.append('<input type="text" name="price5['+id+']" value="0.00" id="price5_'+id+'" class="price_input valid" placeholder="Price 5">');
		appendto.append('<input type="text" name="price6['+id+']" value="0.00" id="price6_'+id+'" class="price_input valid" placeholder="Price 6">');
		return false;
	});
	
	$('a[rel=add_timeframe]').on('click',function(){
		var name = $('#timeframe_name').val();
		
		$.post('<?php echo site_url(); ?>/seasons/add_timeframe/<?=$season_info->season_id?>/<?=$price_info['class_id']?>', {name: name}, function(response) {
			$("#timeframes_container a.terminal_button").before(response);
			$('#timeframe_name').val('');
		});
		return false;
	});
	
	$('#timeframes_container').on('click', 'div.timeframe .close', function(){
		if (confirm("Are you sure you want to delete this timeframe?"))
		{
			var timeframe_id = $(this).attr('id').substr(16);
			var timeframe_container = "timeframe_" + timeframe_id;
			$.ajax({
				type: "POST",
				url: "index.php/seasons/delete_timeframe/"+timeframe_id,
				complete: function( msg ) {
					$('#' + timeframe_container).hide(200, function(){ $('#'+timeframe_container).remove(); });
				}
			});
		}
		return false;
	});
	
	$('#timeframes_form').validate({
		submitHandler:function(form)
		{
			$('input').removeAttr('disabled');

			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success: function(response)
				{
					if(response.success){
						$('#tab3').load('index.php/config/view/season/<?php echo $season_info->season_id; ?>');
						set_feedback(response.message, 'success_message', false, 1500);
						$.colorbox.close();		
					}else{
						set_feedback(response.message, 'error_message', false, 2000);
					}
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
		wrapper: "li"
	});
});
</script>
