<?php
echo form_open('config/save_apriva_device_terminals',array('id'=>'config_device_terminals_form'));
?>
<table width="100%">
    <tbody>
    <tr>
        <th>Terminal</th>
        <th>Label</th>
        <th>IP Address</th>
        <th></th>
    </tr>
    <?php foreach($cc_devices as $device) { ?>
        <tr id="device_row_<?=$device['apriva_device_id']?>">
            <td>
                <input type='hidden' name='apriva_id[]' value='<?=$device['apriva_device_id']?>' />
                <?=form_dropdown('terminal_id[]', $terminals, $device['terminal_id'])?>
            </td>
            <td><?=form_input('device_label[]', $device['label'] != '' ? $device['label'] : 'Device '.$device['apriva_terminal_id'])?></td>
            <td><?=form_input('device_ip_address[]', $device['ip_address'])?></td>
            <td><a class="delete_device" href="#" data-device-id="<?=$device['apriva_device_id']?>">Delete</a></td>
        </tr>
    <?php } ?>
    <tr>
        <td><input type="submit" id="add_cc_device" value="Add Device" /></td>
        <td colspan="2"><input type="submit" id="save_cc_devices" value="Save Terminals" /></td>
    </tr>
    </tbody>
</table>
</form>
<script>
    function initialize_apriva_device_page() {
        $(document).off('click', '.delete_device').on('click', '.delete_device', function(e){
            e.preventDefault();
            var id = $(this).attr('data-device-id');
            if (confirm('Are you sure you want to delete this device?')) {
                $.ajax({
                    type: "POST",
                    url: "index.php/config/delete_apriva_device/",
                    data: 'device_id='+id,
                    success: function (response) {
                        // Update managed list
                        $('#device_row_'+id).remove();
                    },
                    dataType: 'json'
                });
            }
        });
        $(document).off('click', '#add_cc_device').on('click', '#add_cc_device', function(e){
            e.preventDefault();
            //$.colorbox({'href':'index.php/config/add_apriva_device', 'title':'Add Device', 'width':600, 'height':740});
            $.ajax({
                type: "POST",
                url: "index.php/config/save_apriva_device/",
                data: "",
                success: function(response){
                    // Update managed list
                    open_manage_apriva_devices();
                    initialize_apriva_device_page();
                    // Close window
                },
                dataType:'json'
            });
        });
        var submitting = false;
        $('#config_device_terminals_form').validate({
            submitHandler:function(form)
            {
                if (submitting) return;
                submitting = true;
                $('#food_and_beverage').val($('#restaurant').attr('checked') ? 1 : 0);// IF $('#restautant') is checked, then we need to mark this item as food_and_beverage
                $(form).mask("<?php echo lang('common_wait'); ?>");

                $(form).ajaxSubmit({
                    success:function(response) {
                        if(response.success) {
                            set_feedback(response.message,'success_message',false);
                        }
                        else {
                            set_feedback(response.message,'error_message',true);
                        }
                        submitting = false;
                        $.colorbox.close();
                        $(form).unmask();
                    },
                    dataType:'json'
                });

            },
            errorLabelContainer: "#error_message_box",
            wrapper: "li",
            rules: {},
            messages: {}
        });
    }
    $(document).ready(function(){
        initialize_apriva_device_page();
    });
</script>
<style>
    #config_device_terminals_form {
        margin:10px;
    }
    #add_cc_device, #save_cc_devices {
        cursor:pointer;
    }
    #save_cc_devices {
        float:right;
    }
    #config_device_terminals_form table td{
        text-align:center;
    }
</style>