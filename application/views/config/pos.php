<script>
var printer_list = <?php echo json_encode($printers); ?>;
</script>

<div class="field_row clearfix">	
    <?php echo form_label(lang('config_default_tax_rate_1').':', 'default_tax_1_rate',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
                'name'=>'default_tax_1_name',
                'id'=>'default_tax_1_name',
                'size'=>'10',
                'value'=>$this->config->item('default_tax_1_name')!==FALSE ? $this->config->item('default_tax_1_name') : lang('items_sales_tax_1')));?>

        <?php echo form_input(array(
                'name'=>'default_tax_1_rate',
                'id'=>'default_tax_1_rate',
                'size'=>'4',
                'value'=>$this->config->item('default_tax_1_rate')));?>%
        </div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('config_default_tax_rate_2').':', 'default_tax_1_rate',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
                'name'=>'default_tax_2_name',
                'id'=>'default_tax_2_name',
                'size'=>'10',
                'value'=>$this->config->item('default_tax_2_name')!==FALSE ? $this->config->item('default_tax_2_name') : lang('items_sales_tax_2')));?>

        <?php echo form_input(array(
                'name'=>'default_tax_2_rate',
                'id'=>'default_tax_2_rate',
                'size'=>'4',
                'value'=>$this->config->item('default_tax_2_rate')));?>%
                <?php echo form_checkbox('default_tax_2_cumulative', '1', $this->config->item('default_tax_2_cumulative') ? true : false);  ?>
            <span class="cumulative_label">
                <?php echo lang('common_cumulative'); ?>
            </span>
        </div>
</div>
<div class="field_row clearfix">	
<?php echo form_label('Credit card fee', '',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_input(array(
            'name'=>'credit_card_fee',
            'id'=>'credit_card_fee',
            'size'=>'4',
            'value'=>$this->config->item('credit_card_fee')
    )); ?>%
    </div>
</div>            
<div class="field_row clearfix">   
    <?php echo form_label(lang('config_unit_price_includes_tax').':', 'unit_price_includes_tax',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_checkbox(array(
        'name'=>'unit_price_includes_tax',
        'id'=>'unit_price_includes_tax',
        'value'=>'1',
        'checked'=>$this->config->item('unit_price_includes_tax')
    ));?>
    </div>
</div>

<div class="field_row clearfix">	
    <?php echo form_label(lang('config_customer_credit_nickname').':', 'customer_credit_nickname',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
                'name'=>'customer_credit_nickname',
                'id'=>'customer_credit_nickname',
                'size'=>'20',
                'value'=>$this->config->item('customer_credit_nickname')));?>

        </div>
</div>

<div class="field_row clearfix">	
    <?php echo form_label(lang('config_credit_department_category').':', 'customer_credit_department',array('class'=>'wide')); ?>
        <div class='form_field'>
        	<?php echo form_radio(array(
                'name'=>'credit_department_category',
                'id'=>'credit_department',
                'value'=>'0',
                'checked'=>$this->config->item('credit_category')==0));?>
        <?php echo form_dropdown('department', $departments, $this->config->item('credit_department_name'), 'id="department"');?>

        </div>
</div>
<div class="field_row clearfix">	
    <?php echo form_label('', 'customer_credit_department',array('class'=>'wide')); ?>
        <div class='form_field'>
        	<?php echo form_radio(array(
                'name'=>'credit_department_category',
                'id'=>'credit_category',
                'value'=>'1',
                'checked'=>$this->config->item('credit_category')==1));?>
        <?php echo form_dropdown('category', $categories, $this->config->item('credit_category_name'), 'id="category"');?>

        </div>
</div>

<div class="field_row clearfix">	
    <?php echo form_label(lang('config_limit_to_subtotal_only').':', 'customer_credit_subtotal_only',array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_checkbox(array(
                'name' => 'customer_credit_subtotal_only',
                'id' => 'credit_subtotal_only',
                'value' => '1',
                'checked' => $this->config->item('credit_subtotal_only')==1));?>
        </div>
</div>

<div class="field_row clearfix">
	<?php echo form_label(lang('config_nonfiltered_giftcard_barcodes').':', 'config_nonfiltered_giftcard_barcodes',array('class'=>'wide')); ?>
    <div class='form_field'>
		<?php echo form_checkbox(array(
			'name' => 'nonfiltered_giftcard_barcodes',
			'id' => 'nonfiltered_giftcard_barcodes',
			'value' => '1',
			'checked' => $this->config->item('nonfiltered_giftcard_barcodes')==1));?>
    </div>
</div>

<div class="field_row clearfix">	
    <?php echo form_label(lang('config_member_balance_nickname').':', 'member_balance_nickname',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
                'name'=>'member_balance_nickname',
                'id'=>'member_balance_nickname',
                'size'=>'20',
                'value'=>$this->config->item('member_balance_nickname')));?>

        </div>
</div>

<div class="field_row clearfix">
    <?php echo form_label('Limit green/cart fee dropdown to fees customer is allowed to use', 'limit_fee_dropdown_by_customer', array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_checkbox(array(
            'name' => 'limit_fee_dropdown_by_customer',
            'id' => 'limit_fee_dropdown_by_customer',
            'value' => '1',
            'checked' => $course_info->limit_fee_dropdown_by_customer));?>
    </div>
</div>

<div class="field_row clearfix">
    <?php echo form_label('Require on-screen signature for member payments', 'require_signature_member_payments', array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_checkbox(array(
            'name' => 'require_signature_member_payments',
            'id' => 'require_signature_member_payments',
            'value' => '1',
            'checked' => $course_info->require_signature_member_payments));?>
    </div>
</div>

<div class="field_row clearfix">
    <?php echo form_label('Default fees without customers to player 1', 'teetime_default_to_player1', array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_checkbox(array(
            'name' => 'teetime_default_to_player1',
            'id' => 'teetime_default_to_player1',
            'value' => '1',
            'checked' => $course_info->teetime_default_to_player1));?>
    </div>
</div>

<div class="field_row clearfix">
    <?php echo form_label('Require customer on all sales', 'require_customer_on_sale', array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_checkbox(array(
            'name' => 'require_customer_on_sale',
            'id' => 'require_customer_on_sale',
            'value' => '1',
            'checked' => $course_info->require_customer_on_sale));?>
    </div>
</div>

<div class="field_row clearfix">
    <?php echo form_label(lang('config_custom_payment_types').':', 'custom_payment_types',array('class'=>'wide')); ?>
    <div class='form_field'>
        <a class="manageg_custom_payments" href='index.php/config/manage_custom_payments' id='manage_custom_payments'>Manage</a>
        <script>
            $(document).ready(function(){
                $('#manage_custom_payments').colorbox2({'title':'Manage Custom Payments','width':450});
            });
        </script>
    </div>
</div>
<?php if ($this->config->item('use_mercury_emv') && !$this->permissions->is_employee()) { ?>
    <div class="field_row clearfix">
        <?php echo form_label(lang('config_manage_cc_devices').':', 'manage_cc_devices',array('class'=>'wide')); ?>
        <div class='form_field'>
            <a class="manage_cc_devices" href='#' id='manage_cc_devices'>Manage</a>
            <script>
                function open_manage_cc_devices() {
                    $.colorbox({'href':'index.php/config/manage_cc_devices','title':'Manage Credit Card Devices','width':600});
                }
                $(document).ready(function(){
                    $('#manage_cc_devices').on('click', function(e){
                        e.preventDefault();
                        open_manage_cc_devices();
                    });
                });
            </script>
        </div>
    </div>
<?php } ?>
<?php if ($this->config->item('apriva_username') && !$this->permissions->is_employee() && false) { ?>
    <div class="field_row clearfix">
        <?php echo form_label(lang('config_manage_cc_devices').':', 'manage_apriva_devices',array('class'=>'wide')); ?>
        <div class='form_field'>
            <a class="manage_apriva_devices" href='#' id='manage_apriva_devices'>Manage</a>
            <script>
                function open_manage_apriva_devices() {
                    $.colorbox({'href':'index.php/config/manage_apriva_devices','title':'Manage Credit Card Devices','width':800});
                }
                $(document).ready(function(){
                    $('#manage_apriva_devices').on('click', function(e){
                        e.preventDefault();
                        open_manage_apriva_devices();
                    });
                });
            </script>
        </div>
    </div>
<?php } ?>
<div class="field_row clearfix" style="margin-bottom: 10px; margin-top: 5px;">
	<h3>F&B Options</h3>
	<hr>
</div>
<div class="field_row clearfix">	
    <?php echo form_label(lang('config_auto_gratuity').':', 'auto_gratuity',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_input(array(
            'name'=>'auto_gratuity',
            'id'=>'auto_gratuity',
            'size'=>'4',
            'value'=>$this->config->item('auto_gratuity')));?>%
        <span class="cumulative_label">
            (<?php echo lang('config_food_and_beverage'); ?>)
        </span>
    </div>
</div>
<div class="field_row clearfix">    
    <?php echo form_label('Auto gratuity head count threshold:', 'auto_gratuity_threshold', array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_input(array(
            'name'=>'auto_gratuity_threshold',
            'id'=>'auto_gratuity_threshold',
            'size'=>'4',
            'value'=>$this->config->item('auto_gratuity_threshold')));?>
    </div>
</div>
<div class="field_row clearfix">
    <?php echo form_label('Make Auto Gratuity a Service Fee:', 'service_fee_active',array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_checkbox(array(
            'name' => 'service_fee_active',
            'id' => 'service_fee_active',
            'value' => '1',
            'checked' => !empty($course_info->service_fee_active) ? $course_info->service_fee_active : 0));?>
    </div>
</div>
<div id="service_fee_taxes" <?php echo (empty($course_info->service_fee_active) ? "style='display:none'" : "") ?>>
    <div class="field_row clearfix">
        <?php echo form_label(lang('config_service_fee_tax_rate_1').':', 'service_fee_tax_1_rate',array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array(
                'name'=>'service_fee_tax_1_name',
                'id'=>'service_fee_tax_1_name',
                'size'=>'10',
                'value'=>$this->config->item('service_fee_tax_1_name')!==FALSE ? $this->config->item('service_fee_tax_1_name') : lang('items_sales_tax_1')));?>

            <?php echo form_input(array(
                'name'=>'service_fee_tax_1_rate',
                'id'=>'service_fee_tax_1_rate',
                'size'=>'4',
                'value'=>$this->config->item('service_fee_tax_1_rate')));?>%
        </div>
    </div>

    <div class="field_row clearfix">
        <?php echo form_label(lang('config_service_fee_tax_rate_2').':', 'service_fee_tax_2_rate',array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_input(array(
                'name'=>'service_fee_tax_2_name',
                'id'=>'service_fee_tax_2_name',
                'size'=>'10',
                'value'=>$this->config->item('service_fee_tax_2_name')!==FALSE ? $this->config->item('service_fee_tax_2_name') : lang('items_sales_tax_2')));?>

            <?php echo form_input(array(
                'name'=>'service_fee_tax_2_rate',
                'id'=>'service_fee_tax_2_rate',
                'size'=>'4',
                'value'=>$this->config->item('service_fee_tax_2_rate')));?>%
            <!--?php echo form_checkbox('default_tax_2_cumulative', '1', $this->config->item('default_tax_2_cumulative') ? true : false);  ?>
            <span class="cumulative_label">
                <?php echo lang('common_cumulative'); ?>
            </span-->
        </div>
    </div>
</div>
<div class="field_row clearfix">
    <?php echo form_label('Sort items by seat', 'food_bev_sort_by_seat', array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_checkbox(array(
            'name' => 'food_bev_sort_by_seat',
            'id' => 'food_bev_sort_by_seat',
            'value' => '1',
            'checked' => $course_info->food_bev_sort_by_seat));?>
    </div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Require F&B Guest Count:', 'require_guest_count',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_checkbox(array(
				'name' => 'require_guest_count',
				'id' => 'require_guest_count',
				'value' => '1',
				'checked' => $course_info->require_guest_count));?>
		</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label('Use Course Firing:', 'use_course_firing',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_checkbox(array(
				'name' => 'use_course_firing',
				'id' => 'use_course_firing',
				'value' => '1',
				'checked' => $course_info->use_course_firing));?>
		</div>
</div>
<div class="field_row clearfix">
    <?php echo form_label('Include items when course is fired', 'course_firing_include_items',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
			'name' => 'course_firing_include_items',
			'id' => 'course_firing_include_items',
			'value' => '1',
			'checked' => $course_info->course_firing_include_items));?>
	</div>
</div>
<div class="field_row clearfix">
    <?php echo form_label('Print suggested tip amounts', 'print_suggested_tip',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_checkbox(array(
            'name' => 'print_suggested_tip',
            'id' => 'print_suggested_tip',
            'value' => '1',
            'checked' => $course_info->print_suggested_tip));?>
    </div>
</div>
<div class="field_row clearfix">    
<?php echo form_label('Hide modifier names on kitchen receipts', 'hide_modifier_names_kitchen_receipts',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name' => 'hide_modifier_names_kitchen_receipts',
                'id' => 'hide_modifier_names_kitchen_receipts',
                'value' => '1',
                'checked' => $course_info->hide_modifier_names_kitchen_receipts));?>
        </div>
</div>
<div class="field_row clearfix">    
<?php echo form_label('Print tip line on first receipt', 'food_bev_tip_line_first_receipt', array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name' => 'food_bev_tip_line_first_receipt',
                'id' => 'food_bev_tip_line_first_receipt',
                'value' => '1',
                'checked' => $course_info->food_bev_tip_line_first_receipt));?>
        </div>
</div>   					                         

<!--div class="field_row clearfix">	
<?php echo form_label(lang('config_currency_symbol').':', 'currency_symbol',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
                'name'=>'currency_symbol',
                'id'=>'currency_symbol',
                'value'=>$this->config->item('currency_symbol')));?>
        </div>
</div-->
<div id='printing_options'>
    
    <div id="webprint_options">
		<div class="field_row clearfix">	
		<?php echo form_label(lang('config_print_after_sale').':', 'print_after_sale',array('class'=>'wide')); ?>
				<div class='form_field'>
				<?php echo form_checkbox(array(
						'name'=>'print_after_sale',
						'id'=>'print_after_sale',
						'value'=>'1',
						'checked'=>$course_info->print_after_sale));?>
				</div>
		</div>

		<div class="field_row clearfix">	
		<?php echo form_label('Use Web Print:', 'webprnt',array('class'=>'wide')); ?>
				<div class='form_field'>
				<?php echo form_checkbox(array(
						'name'=>'webprnt',
						'id'=>'webprnt',
						'value'=>'1',
						'checked'=>$course_info->webprnt));?>
				</div>
		</div>
	   
		<?php if($this->session->userdata('multiple_printers') == 1){ ?>
		<div class="field_row clearfix">
			<h3 style="float: left; margin-left: 5px; width: 310px; height: 40px; line-height: 40px; border-bottom: 1px solid gray;">Printers</h3>	
			<a class="terminal_button" style="color: white; float: left;" href='index.php/config/view_printers' id='manage_printers'>Manage Printers</a>
			<script>
				$(document).ready(function(){
					$('#manage_printers').colorbox2({'title':'Manage Printers','width':650});
				});
			</script>
		</div>
		<?php }else{ ?>
		<a style="font-size: 14px; color: blue; float: right; display: block" onClick="return confirm('Are you sure? Multiple printing is a brand new feature that may cause errors in printing.');" href="<?php echo site_url('config/convert_to_multi_printers'); ?>" id="convert_to_multi_printers">Switch to multiple printers</a>
		<?php } ?>
		
		<div class="field_row clearfix">	
		<?php echo form_label('Sales Receipt Printer:', 'webprnt_ip',array('class'=>'wide')); ?>
			<div class='form_field'>
			<?php if($this->session->userdata('multiple_printers') == 0){ ?>	
			<?php echo form_input(array(
					'name'=>'webprnt_ip',
					'id'=>'webprnt_ip',
					'size'=>'15',
					'value'=>$course_info->webprnt_ip));
			?>
			<?php }else{ ?>
			<?php echo form_dropdown('webprnt_ip', $printer_menu, $course_info->webprnt_ip);?>
								
			<?php } ?>
			</div>
		</div>
	
	<?php if($this->session->userdata('multiple_printers') == 1){ ?>
	<div class="field_row clearfix">
	<?php echo form_label('Default Kitchen Printer:', 'default_kitchen_printer',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('default_kitchen_printer', $printer_menu, $course_info->default_kitchen_printer);?>	
		</div>
	</div>					
	
	<?php foreach($printer_groups as $printer_group){ ?>
	<div class="field_row clearfix">
		<?php echo form_label($printer_group['label'].':', null, array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php echo form_dropdown('printer_groups['.$printer_group['printer_group_id'].']', $printer_menu, $printer_group['default_printer_id']);?>	
		</div>
	</div>
	<?php } ?>
	<?php } ?>            
	
	 <?php if($this->session->userdata('multiple_printers') == 0){ ?>				
	<div class="field_row clearfix">	
		<?php echo form_label(lang('config_webprnt_hot_ip').':', 'webprnt_hot_ip',array('class'=>'wide')); ?>
			<div class='form_field'>
			<?php if($this->session->userdata('multiple_printers') == 0){ ?>	
			<?php echo form_input(array(
					'name'=>'webprnt_hot_ip',
					'id'=>'webprnt_hot_ip',
					'size'=>'15',
					'value'=>$course_info->webprnt_hot_ip));
			?>
			<?php }else{ ?>
			<?php echo form_dropdown('webprnt_hot_ip', $printer_menu, $course_info->webprnt_hot_ip);?>
			<?php } ?>
			</div>
	</div>
	  <div class="field_row clearfix">	
		<?php echo form_label(lang('config_webprnt_cold_ip').':', 'webprnt_cold_ip',array('class'=>'wide')); ?>
		<div class='form_field'> 
		<?php echo form_input(array(
				'name'=>'webprnt_cold_ip',
				'id'=>'webprnt_cold_ip',
				'size'=>'15',
				'value'=>$course_info->webprnt_cold_ip));
		?>           
	   </div>
	</div>
	<?php } ?>  
	<div class="field_row clearfix">	
	<?php echo form_label('Barcode Label Printer:', 'webprnt_label_ip',array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php if($this->session->userdata('multiple_printers') == 0){ ?>	
			<?php echo form_input(array(
					'name'=>'webprnt_label_ip',
					'id'=>'webprnt_label_ip',
					'size'=>'15',
					'value'=>$course_info->webprnt_label_ip));
			?>
			<?php }else{ ?>
			<?php echo form_dropdown('webprnt_label_ip', $printer_menu, $course_info->webprnt_label_ip);?>
			<?php } ?>                  
			<input id='enable_blackmark' type='button' class='loyalty_button label_mode_button' value="Turn on Label Mode" />
			<input id='disable_blackmark' type='button' class='loyalty_button label_mode_button' value="Turn off Label Mode" />
		</div>
	</div>
</div>
    <div class="field_row clearfix">
        <?php echo form_label(lang('config_use_kitchen_buzzers').':', 'use_kitchen_buzzers',array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_checkbox(array(
                'name'=>'use_kitchen_buzzers',
                'id'=>'use_kitchen_buzzers',
                'value'=>'1',
                'checked'=>$course_info->use_kitchen_buzzers));?>
        </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label(lang('config_print_credit_card_receipt').':', 'print_credit_card_receipt',array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_checkbox(array(
                'name'=>'print_credit_card_receipt',
                'id'=>'print_credit_card_receipt',
                'value'=>'1',
                'checked'=>$course_info->print_credit_card_receipt));?>
        </div>
    </div>
    <div class="field_row clearfix">
    <?php echo form_label(lang('config_print_sales_receipt').':', 'print_sales_receipt',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'print_sales_receipt',
                    'id'=>'print_sales_receipt',
                    'value'=>'1',
                    'checked'=>$course_info->print_sales_receipt));?>
            </div>
    </div>
    <div class="field_row clearfix">	
    <?php echo form_label(lang('config_print_tip_line').':', 'print_tip_line',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'print_tip_line',
                    'id'=>'print_tip_line',
                    'value'=>'1',
                    'checked'=>$course_info->print_tip_line));?>
            </div>
    </div>
    <div class="field_row clearfix">    
    <?php echo form_label('Print customer account balance:', 'receipt_print_account_balance',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'receipt_print_account_balance',
                    'id'=>'receipt_print_account_balance',
                    'value'=>'1',
                    'checked'=>$course_info->receipt_print_account_balance));?>
            </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Print current minimums on Receipt:', 'print_minimums_on_receipt',array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_checkbox(array(
                'name'=>'print_minimums_on_receipt',
                'id'=>'print_minimums_on_receipt',
                'value'=>'1',
                    'checked'=>$course_info->print_minimums_on_receipt));?>
        </div>
    </div>
    <div class="field_row clearfix">        
    <?php echo form_label(lang('config_print_two_receipts').':', 'print_two_receipts',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'print_two_receipts',
                    'id'=>'print_two_receipts',
                    'value'=>'1',
                    'checked'=>$course_info->print_two_receipts));?>
           </div>
    </div>
    <div class="field_row clearfix">        
    <?php echo form_label(lang('config_print_two_signature_slips').':', 'print_two_signature_slips',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'print_two_signature_slips',
                    'id'=>'print_two_signature_slips',
                    'value'=>'1',
                    'checked'=>$course_info->print_two_signature_slips));?>
           </div>
    </div>
    <div class="field_row clearfix">        
    <?php echo form_label(lang('config_print_two_receipts_other').':', 'print_two_receipts_other',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'print_two_receipts_other',
                    'id'=>'print_two_receipts_other',
                    'value'=>'1',
                    'checked'=>$course_info->print_two_receipts_other));?>
           </div>
    </div>
    <div class="field_row clearfix">        
    <?php echo form_label(lang('config_cash_drawer_on_cash').':', 'cash_drawer_on_cash',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'cash_drawer_on_cash',
                    'id'=>'cash_drawer_on_cash',
                    'value'=>'1',
                    'checked'=>$course_info->cash_drawer_on_cash));?>
           </div>
    </div>
    <div class="field_row clearfix">
        <?php echo form_label('Open Cash Drawer on Every Sale:', 'cash_drawer_on_sale',array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_checkbox(array(
                'name'=>'cash_drawer_on_sale',
                'id'=>'cash_drawer_on_sale',
                'value'=>'1',
                'checked'=>$course_info->cash_drawer_on_sale));?>
        </div>
    </div>
    <div class="field_row clearfix">        
    <?php echo form_label('Hide employee last name on receipt:', 'cash_drawer_on_cash',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'hide_employee_last_name_receipt',
                    'id'=>'hide_employee_last_name_receipt',
                    'value'=>'1',
                    'checked'=>$course_info->hide_employee_last_name_receipt));?>
           </div>
    </div>
    <div class="field_row clearfix">        
    <?php echo form_label('Auto email receipt to customer', 'auto_email_receipt', array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'auto_email_receipt',
                    'id'=>'auto_email_receipt',
                    'value'=>'1',
                    'checked'=>$course_info->auto_email_receipt));?>
           </div>
    </div>
    <div class="field_row clearfix">        
    <?php echo form_label('Do not print receipt if emailed', 'auto_email_no_print', array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'auto_email_no_print',
                    'id'=>'auto_email_no_print',
                    'value'=>'1',
                    'checked'=>$course_info->auto_email_no_print));?>
           </div>
    </div>
    <div class="field_row clearfix">        
    <?php echo form_label('Include teetime details on receipt', 'print_tee_time_details', array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'print_tee_time_details',
                    'id'=>'print_tee_time_details',
                    'value'=>'1',
                    'checked'=>$course_info->print_tee_time_details));?>
           </div>
    </div> 			    
</div>
<script type='text/javascript'>
	$('#printing_options').expandable({title:'Printing Options:'});
</script>
<div class="field_row clearfix">	
<?php echo form_label(lang('config_after_sale_load').':', 'after_sale_load',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_radio(array(
                'name'=>'after_sale_load',
                'id'=>'after_sale_load_0',
                'value'=>'0',
                'checked'=>$course_info->after_sale_load==0));?>
        <?php echo ($this->permissions->course_has_module('reservations') ? 'Reservations' : 'Tee Sheet');  ?>
        <?php echo form_radio(array(
                'name'=>'after_sale_load',
                'id'=>'after_sale_load_1',
                'value'=>'1',
                'checked'=>$course_info->after_sale_load==1));?>
        Sales
        </div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('config_automatic_update').':', 'teesheet_updates_automatically',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'teesheet_updates_automatically',
                'id'=>'teesheet_updates_automatically',
                'value'=>'1',
                'checked'=>$this->config->item('teesheet_updates_automatically')));?>
        </div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('config_send_reservation_confirmations').':', 'send_reservation_confirmations',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'send_reservation_confirmations',
                'id'=>'send_reservation_confirmations',
                'value'=>'1',
                'checked'=>$this->config->item('send_reservation_confirmations')));?>
        </div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('config_auto_split_teetimes').':', 'auto_split_teetimes',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'auto_split_teetimes',
                'id'=>'auto_split_teetimes',
                'value'=>'1',
                'checked'=>$this->config->item('auto_split_teetimes')));?>
        </div>
</div>

<div class="field_row clearfix">	
    <?php echo form_label(lang('config_track_cash').':', 'track_cash',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'track_cash',
                'id'=>'track_cash',
                'value'=>'1',
                'checked'=>$course_info->track_cash));?>
        </div>
</div>
<div class="field_row clearfix">    
    <?php echo form_label('Allow employees to bypass register log closing:', 'allow_employee_register_log_bypass',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'allow_employee_register_log_bypass',
                'id'=>'allow_employee_register_log_bypass',
                'value'=>'1',
                'checked'=>$course_info->allow_employee_register_log_bypass));?>
        </div>
</div>
<div class="field_row clearfix">    
    <?php echo form_label('Default register open amount:', 'default_register_log_open', array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_input(array(
                'name'=>'default_register_log_open',
                'id'=>'default_register_log_open',
                'size'=>'20',
                'value'=>$course_info->default_register_log_open));?>

        </div>
</div>
<div class="field_row clearfix">
    <?php echo form_label(lang('config_blind_close').':', 'blind_close',array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_checkbox(array(
            'name'=>'blind_close',
            'id'=>'blind_close',
            'value'=>'1',
            'checked'=>$this->config->item('blind_close')));?>
    </div>
</div>
<div class="field_row clearfix">
    <?php echo form_label(lang('config_hide_taxable').':', 'hide_taxable',array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_checkbox(array(
            'name'=>'hide_taxable',
            'id'=>'hide_taxable',
            'value'=>'1',
            'checked'=>$this->config->item('hide_taxable')));?>
    </div>
</div>
<div class="field_row clearfix">
    <?php echo form_label(lang('config_require_employee_pin').':', 'require_employee_pin',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'require_employee_pin',
                'id'=>'require_employee_pin',
                'value'=>'1',
                'checked'=>$this->config->item('require_employee_pin')));?>
        </div>
</div>
<?php if ($this->permissions->course_has_module('food_and_beverage')) { ?>
<div class="field_row clearfix">	
<?php echo form_label(lang('config_fnb_login_required').':', 'fnb_login',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'fnb_login',
                'id'=>'fnb_login',
                'value'=>'1',
                'checked'=>$this->config->item('fnb_login')));?>
        </div>
</div>
<?php } ?>
<div class="field_row clearfix">	
    <?php echo form_label(lang('config_separate_courses').':', 'separate_courses',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'separate_courses',
                'id'=>'separate_courses',
                'value'=>'1',
                'checked'=>$this->config->item('separate_courses')));?>
        </div>
</div>
<div class="field_row clearfix">
    <?php echo form_label(lang('config_minimum_food_spend').':', 'minimum_food_spend',array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_input(array(
            'name'=>'minimum_food_spend',
            'id'=>'minimum_food_spend',
            'size'=>'20',
            'value'=>$this->config->item('minimum_food_spend')));?>

    </div>
</div>
<div class="field_row clearfix">
    <?php echo form_label(lang('config_refund_reasons').':', 'return_reasons',array('class'=>'wide')); ?>
    <div class='form_field'>
        <a id="manage_refund_reasons" href="#">Manage Reasons</a>
    </div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('common_return_policy').':', 'return_policy',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_textarea(array(
                'name'=>'return_policy',
                'id'=>'return_policy',
                'rows'=>'4',
                'cols'=>'30',
                'value'=>$this->config->item('return_policy')));?>
        </div>
</div>
<script>
	$('#department').change(function(e){
		$('#credit_department').attr('checked', true);
	});
	$('#category').change(function(e){
		$('#credit_category').attr('checked', true);
	});
	$('#member_balance_nickname, #customer_credit_nickname').bind('keyup change', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		 	//Do something
			var box = $(this);
			box.val(box.val().replace(/'/g, '').replace(/"/g, ''));
	});
	$('#disable_blackmark').click(function(e){
		e.preventDefault();
        <?php if($this->session->userdata('multiple_printers') == 1){ ?>
        var printer_id = $('#webprnt_label_ip').val();
        var ip = printer_list[printer_id]['ip_address'];
        <?php } else { ?>
        var ip = $('#webprnt_label_ip').val();
        <?php } ?>

        if (ip == '') {
			alert('Printer IP address required');
		}
		else {
			var disable_blackmark = webprnt.blackmark_disable();
	   		webprnt.print(disable_blackmark,window.location.protocol + "//" + ip+"/StarWebPRNT/SendMessage");
	   		webprnt.print_all(window.location.protocol + "//" + ip+"/StarWebPRNT/SendMessage");
	   	}
	});
    $('#service_fee_active').click(function(){
        var box_checked = $(this).attr('checked');
        if (box_checked) {
            $('#service_fee_taxes').show();
        }
        else {
            $('#service_fee_taxes').hide();
        }
    });
	$('#enable_blackmark').click(function(e){
		e.preventDefault();
        <?php if($this->session->userdata('multiple_printers') == 1){ ?>
        var printer_id = $('#webprnt_label_ip').val();
        var ip = printer_list[printer_id]['ip_address'];
        <?php } else { ?>
        var ip = $('#webprnt_label_ip').val();
        <?php } ?>

		if (ip == '') {
			alert('Printer IP address required');
		}
		else {
			var enable_blackmark = webprnt.blackmark_enable();
   			webprnt.print(enable_blackmark,window.location.protocol + "//" + ip+"/StarWebPRNT/SendMessage");
   			webprnt.print_all(window.location.protocol + "//" + ip+"/StarWebPRNT/SendMessage");
   		}
	});
	$('#manage_refund_reasons').colorbox({'href':'index.php/config/manage_refund_reasons','width':600});
</script> 
<style>
	#printing_options input.label_mode_button {
		display:inline-block;
		color:white;
	}
</style>