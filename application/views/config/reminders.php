<script>
    function renderRow(data){
        var row = '<tr><td>'+data['type']+'</td><td>Your teetime is about 12 hours from now on Sat, Oct 10th, 2015 7:10 AM</td><td>Will be sent out the day before the teetime is scheduled.</td><td><a href="#" class="remove_reminder" id="'+data['id']+'">Delete</a></td></tr>';
        $(".rows").append(row);
        $.colorbox2.resize();
        init_row();
    }
    function init_row(){
        $('.remove_reminder').unbind("click");
        $('.remove_reminder').on('click', function (e){
            e.preventDefault();
            var id = $(this).attr('id');
            var row = $(this).parent().parent();
            if (id != undefined) {
                $.ajax({
                    type: "POST",
                    url: "index.php/config/delete_reminder/"+id,
                    data: '',
                    success: function(response){
                        row.remove();
                        $.colorbox2.resize();
                    },
                    dataType:'json'
                });
            }
            else {
                // else just remove and resize
                $(this).parent().parent().remove();
                $.colorbox2.resize();
            }
        });
    }

    $(document).ready(function() {
        init_row();
        $('#add_reminder').on('click', function (e){
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "index.php/config/add_reminder/",
                data: '',
                success: function(response){
                    renderRow(response);
                    init_row();
                    $.colorbox2.resize();
                },
                dataType:'json'
            });
        });
    });
</script>
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
<div style="margin:30px">
    <p>
        Additional reminders and configuration coming soon!  Let us know if you're interested.
    </p>
    <table class="pure-table">
        <thead>
        <tr>
            <td>Type</td>
            <td>Example Text</td>
            <td>Details</td>
            <td>Action</td>
        </tr>
        </thead>
        <tbody class="rows">
        <?php foreach($reminders as $reminder){ ?>
            <script>renderRow(<?=json_encode($reminder)?>);</script>

        <?php } ?>
        </tbody>
        <tfoot>
            <td colspan="4" style="text-align: right">
                <a id="add_reminder" href="#" >Add Reminder</a></td>
        </tfoot>
    </table>
</div>