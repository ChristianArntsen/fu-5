<div class="field_row clearfix">
    <?php echo form_label(lang('config_use_loyalty').':', 'use',array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_checkbox(array('name'=>'use_loyalty', 'value'=>'1', 'checked'=>$use_loyalty, 'id'=>'use_loyalty')); ?>
        <div id='add_loyalty_rate'><?=lang('config_add_loyalty_rate')?></div>
    </div>
</div>
<div class="field_row clearfix">
    <?php echo form_label(lang('config_loyalty_auto_enroll').':', 'loyalty_auto_enroll',array('class'=>'wide')); ?>
    <div class='form_field'>
        <?php echo form_checkbox(array('name'=>'loyalty_auto_enroll', 'value'=>'1', 'checked'=>$loyalty_auto_enroll, 'id'=>'loyalty_auto_enroll')); ?>
    </div>
</div>
<div class="field_row clearfix">
	<?php echo form_label('', '',array('class'=>'wide')); ?>
    <div class='form_field' id='loyalty_rate_list'>
    	<table id='loyalty_rates_table'>
    		<tbody>
    			<tr>
	    			<th></th>
	    			<th>Label</th>
	    			<th>Department or Item</th>
                    <th>Timeframe</th>
                    <th>Pts / $*</th>
                    <th>$ / 100 pts*</th>
                    <th>Effective Rate</th>
                </tr>
    	<?php foreach($loyalty_rates as $loyalty_rate) { ?>
    		<tr id='loyalty_rate_<?=$loyalty_rate->loyalty_rate_id;?>'>
    			<td><span id='delete_loyalty_rate_<?=$loyalty_rate->loyalty_rate_id;?>' class='delete_loyalty'></span>
    				<?php echo form_hidden('loyalty_rate_id[]',$loyalty_rate->loyalty_rate_id); ?>
    				<?php echo form_hidden('delete_loyalty_rate_id[]', 0); ?>
    			</td>
    			<td><?php echo form_input(array('name' => 'loyalty_label[]', 'value' => $loyalty_rate->label, 'size' => '25'));?></td>
    			<td>
    				<?php echo form_input(array('name' => 'loyalty_filter[]', 'value' => (!empty($loyalty_rate->value_label) ? $loyalty_rate->value_label : $loyalty_rate->value), 'size' => '35', 'placeholder'=>'Search...', 'id'=>'label_'.$loyalty_rate->loyalty_rate_id));?>
    				<?php echo form_hidden('type[]', $loyalty_rate->type); ?>
				    <?php echo form_hidden('value[]', $loyalty_rate->value); ?>
				    <?php echo form_hidden('tee_time_index[]', $loyalty_rate->tee_time_index); ?>
				    <?php echo form_hidden('timeframe_id[]', $loyalty_rate->timeframe_id); ?>
				    <?php echo form_hidden('price_category[]', $loyalty_rate->price_category); ?>
                </td>
                <td>
				    <?php echo form_checkbox(array('name' => 'limit_timeframe[]','checked'=>(!empty($loyalty_rate->limit_timeframe) ? $loyalty_rate->limit_timeframe : false), 'value' => 'true', "class"=>"limit_timeframe_checkbox",'id'=>'limit_timeframe_'.$loyalty_rate->loyalty_rate_id));?>
				    <input value="<?php echo $loyalty_rate->limit_timeframe ?>" type="hidden" id="limit_timeframe_<?=$loyalty_rate->loyalty_rate_id?>_hidden" name="limit_timeframe_hidden[]" id="limit_timeframe_hidden_"<?=$loyalty_rate->loyalty_rate_id ?> />
                </td>
    			<td><?php echo form_input(array('name' => 'points_per_dollar[]', 'value' => $loyalty_rate->points_per_dollar, 'size' => '6')); ?></td>
    			<td><?php echo form_input(array('name' => 'dollars_per_point[]', 'value' => $loyalty_rate->dollars_per_point, 'size' => '6')); ?></td>
    			<td><span class='effective_rate' id='er_<?=$loyalty_rate->loyalty_rate_id?>'><?php echo $loyalty_rate->points_per_dollar * $loyalty_rate->dollars_per_point ?>%</span></td>


            </tr>
    	<?php } ?>
    		<tr id='loyalty_notes'>
    			<td colspan=6>
    				* Note - Loyalty points are only given to customers when a purchase is paid for with cash, check, or credit card. Department/item search also <br/>allows Category, Subcategory, or Item Kit <br/>
    				- 'Pts / $' signifies how many points are rewarded per dollar of qualified purhcases.<br/>
    				- '$ / 100' pts signifies how many dollars can be redeemed for every 100 points accumulated by the customer.<br/>
    				- 'Effective Rate' represents the percentage value you're awarding customers.
    			</td>
    		</tr>
    		</tbody>
    	</table>
    </div>
</div>

<script>
function add_autocomplete()
{
	console.log('adding autocomplete');
	$( "input[name='loyalty_filter[]']" ).autocomplete({
		source: "<?php echo site_url('config/suggest_dept_cat_item');?>",
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			console.log('selecting on loyalty auto-complete', ui);
			
			event.preventDefault();
			var loyalty_filter = $(this);
			var id = loyalty_filter.attr('id').replace('label_', '');
			$("#loyalty_rate_"+id+" input[name='type[]']").val(ui.item.type);
			$("#loyalty_rate_"+id+" input[name='value[]']").val(ui.item.value);
			$("#loyalty_rate_"+id+" input[name='tee_time_index[]']").val(ui.item.teetime_type ? ui.item.teetime_type : 0);
			$("#loyalty_rate_"+id+" input[name='price_category[]']").val(ui.item.price_index ? ui.item.price_index : 0);
			$("#loyalty_rate_"+id+" input[name='timeframe_id[]']").val(ui.item.timeframe_id ? ui.item.timeframe_id : 0);
			loyalty_filter.val(ui.item.label);
		}
	});
}

function add_change_point_values()
{
	console.log('added change point vals');
	$("input[name='points_per_dollar[]'], input[name='dollars_per_point[]']").keyup(function(){
		console.log('trying to update vals');
		var id = $(this).closest('tr').attr('id').replace('loyalty_rate_', '');
		var ppd = $("#loyalty_rate_"+id+" input[name='points_per_dollar[]']").val();
		var dpp = $("#loyalty_rate_"+id+" input[name='dollars_per_point[]']").val();
		console.log('new val '+(ppd*dpp).toFixed(2));
		$('#er_'+id).html((ppd*dpp).toFixed(2)+'%');
	});
}
	
$(document).ready(function(){
	
	$('#loyalty_rates_table').on('click', '.delete_loyalty', function(e){
		var id = $(this).attr('id').replace('delete_loyalty_rate_','');
		$('#loyalty_rate_'+id+' input[name="delete_loyalty_rate_id[]"]').val(1);
		$('#loyalty_rate_'+id).hide();
		e.preventDefault();
	});

	$('.limit_timeframe_checkbox').on("change",function(){
	    if($(this).is(':checked')){
            $(this).next().val("1")
        } else {
            $(this).next().val("0")
        }
    })

	$('#add_loyalty_rate').on('click', function(e){
		var id=Math.floor(Math.random()*101);
		$('#loyalty_notes').before('<tr id="loyalty_rate_'+id+'">'+
			'<td>'+
				'<span id="delete_loyalty_rate_'+id+'" class="delete_loyalty"></span>'+
				'<input type="hidden" id="loyalty_rate_id[]" name="loyalty_rate_id[]" value="0">'+
			'</td><td>'+
				'<input type="text" name="loyalty_label[]" value="" size="25">'+
			'</td><td>'+
				'<input type="text" name="loyalty_filter[]" value="" size="35" placeholder="Search..." id="label_'+id+'" >'+
				'<input type="hidden" id="type[]" name="type[]" value="">'+
				'<input type="hidden" id="value[]" name="value[]" value="">'+
				'<input type="hidden" id="tee_time_type[]" name="tee_time_index[]" value="">'+
				'<input type="hidden" id="timeframe_id[]" name="timeframe_id[]" value="">'+
				'<input type="hidden" id="price_category[]" name="price_category[]" value="">'+
			'</td><td>'+

                '<input type="checkbox" name="limit_timeframe[]" value="true" id="limit_timeframe_'+id+'">'+
                '<input value="1" type="hidden" id="limit_timeframe_'+id+'_hidden" name="limit_timeframe_hidden[]" id="limit_timeframe_hidden_'+id+'" />' +
			'</td><td>'+

				'<input type="text" name="points_per_dollar[]" value="" size="6">'+
			'</td><td>'+
				'<input type="text" name="dollars_per_point[]" value="" size="6">'+
			'</td><td>'+
				'<span class="effective_rate" id="er_'+id+'"></span>'+
			'</td></tr>');

		add_change_point_values();
		add_autocomplete();
		e.preventDefault();
	});

	$('')

	add_change_point_values();
	add_autocomplete();
});
</script>
