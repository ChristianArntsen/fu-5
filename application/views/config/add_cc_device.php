<html>
<head>
    <script>
        function listener(event){
            //result processing logic goes here
            var jsonResponse = JSON.parse(event.data);

            // Save data to foreup
            $.ajax({
                type: "POST",
                url: "index.php/config/save_blackline_device/",
                data: "TerminalID="+jsonResponse.TerminalID+"&ApiKey="+jsonResponse.ApiKey+"&ApiPassword="+jsonResponse.ApiPassword,
                success: function(response){
                    // Update managed list
                    open_manage_cc_devices();
                    // Close window
                },
                dataType:'json'
            });
        }
        if (window.addedEventListener == undefined || window.addedEventListener != 1) {
            if (window.addEventListener) {
                addEventListener("message", listener, false)
            } else {
                attachEvent("onmessage", listener)
            }
            window.addedEventListener = 1;
        }
    </script>

</head>
<iframe style="overflow:hidden" width="550px" height="650px" src="<?=$register_url?>"></iframe>

</html>