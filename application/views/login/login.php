<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/login.css?<?php echo APPLICATION_VERSION; ?>" />
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
<title>foreUP <?php echo lang('login_login'); ?></title>
<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script type="text/javascript">
$(document).ready(function()
{
	$("#login_form input:first").focus();
});
</script>
</head>
<body>
	<!--div id="welcome_message" class="top_message">
		<?php echo lang($welcome_message); ?>
	</div-->

	<?php
	$this->load->helper('url');
	$this->load->library('user_agent');
	if ($this->agent->browser() != 'Chrome')
	{
	?>
	<div id='chrome_message'>
		<p>foreUP software is only fully supported on   <a href="http://google.com/intl/en/chrome/"><img align='middle' alt="Chrome" data-g-event="nav-logo" data-g-label="consumer-home" id="logo" src="../images/login/chrome.png" style="width:40px"> &nbsp;Chrome</a><br/>Please open or download Google Chrome <em>**free**</em> for the best experience <a href='http://google.com/chrome'>here</a></p>
	</div>
	<?php
	}
	?>
<?php echo form_open('login') ?>
<div id="container">
	<div class="golfer">
		<?php echo img(array('src' =>'../images/login/golfer.png'));?>
	</div>
	<div class="right">
		<div class="logo">
			<?php echo img(array('src' =>'../images/login/login_logo.png'));?>
		</div>
		<div class='login_header' <?php if(isset($prerelease) && $prerelease){ ?>
        <?php } ?>>
				<?php if (validation_errors()) {?>
					<div class="message_error">
						<?php echo validation_errors(); ?>
					</div>
				<?php } ?>
		</div>
			<div id="form_field_username">
				<?php echo form_input(array(
				'name'=>'username',
				'value'=>'',
				'placeholder'=>lang('login_username'),
				'size'=>'20')); ?>
			</div>
			<div id="form_field_password">
				<?php echo form_password(array(
				'name'=>'password',
				'value'=>'',
				'placeholder'=>lang('login_password'),
				'size'=>'20')); ?>
			</div>
			<div id="form_field_submit" >
				<div id="reset_button" style="width: 50%;float: left">
					 <?php echo anchor('login/reset_password', lang('login_forgot_password')); ?>
				</div>
                <div id="submit_button" style="width: 50%; float: right" >
                    <?php echo form_submit('login_button',lang('login_continue')); ?>
                </div>
                <div id="clear_cookie_button_holder" style="width: 100%; float: right; text-align:right;" >
                    Having trouble logging in?
                    <button onclick='clearallcookies()' id="clear_cookie_button" name="clear_cookie_button" type="button">Clear Cookies</button>

                </div>
                <style>
                    #clear_cookie_button_holder {
                        MARGIN-top:6px;
                    }
                    #clear_cookie_button_holder button {
                        background: #4885B1;
                        color: white;
                        height: 45px;
                        text-align: center;
                        border-radius: 5px;
                        cursor: pointer;
                        border: none;
                        width:50%;
                    }
                </style>
                <script>
                    function clearallcookies(){
                        document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
                        alert('Cookies cleared successfully. Now please try to log in again, and if you still have trouble, please contact foreUP support.');
                    };
                </script>
			</div>
		</div>
		  <?php if(isset($prerelease) && $prerelease){ ?>
		      <div style="text-align: center; ">
		          <p>Your course has been chosen to try our newest code.  If you run into any problems make sure to call support and let them know you're on the pre-released version.</p>
		      </div>
		  <?php } ?>
	</div>
</div>
<?php echo form_close(); ?>
</body>
</html>
