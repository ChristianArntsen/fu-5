<?php define("ECOM_ROOT_URL", "https://www.etsemoney.com/hp/v2"); ?>
<style>
.wrapper { 
	margin: 15px;
	width: 400px;
	height: 225px;
}

.etsFormGroup { 
	margin: 10px 0; 
}

#ets_img { 
	margin: 40px auto;
}

.etsButton {
	background: #3D80B9;
	color: white !important;
	border: 1px solid #232323 !important;
	display: block;
	font-size: 14px;
	font-weight: normal;
	height: 28px;
	width: 100px;
	padding: 8px 0 8px 0px;
	text-align: center;
	text-shadow: 0px -1px 0px black !important;
	border-radius: 4px;
	margin-bottom: 8px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
}
</style>
<script>
var interval_id = '';
var count = 0;
$(document).ready(function(){
	interval_id = setInterval(add_ets_handlers, 100);
});
function add_ets_handlers()
{
	if ($('#ETSIFrame').length > 0)
	{
		ETSPayment.addResponseHandler("success", function(e){
			console.log('ets success');
			console.dir(e);
			$.ajax({
			   type: "POST",
			   url: "<?php echo $url; ?>",
			   data: 'response='+JSON.stringify(e),
			   success: function(response){
					$('#ets_payment_window').html(response);
				},
				dataType:'html'
			 });
		});

		clearInterval(interval_id);
	}
	else if (count > 100)
	{
		clearInterval(interval_id);
	}
	count ++;
}
</script>
<div class="wrapper" id="ets_payment_window">
	<?php if (!$tee_time_card) { ?>
	<h1><?php if ($type == 'giftcard') {echo 'Giftcard ';}?>Total: $<?=$amount?></h1>
	<?php } ?>
	<!-- HTML5 Magic -->
	<div id='ets_session_id' data-ets-key="<?php echo trim($session->id); ?>">
		<img id='ets_img' src="http://www.loadinfo.net/main/download?spinner=3875&disposition=inline" alt="">
	</div>
</div>
<div id='ets_script_box'>
</div>
<script>
	if (typeof ETSPayment == 'undefined')
	{
		var e = document.createElement('script');
		e.src = "<?php echo ECOM_ROOT_URL ?>/init";
		$('head')[0].appendChild(e);
	}
	else
	{
		ETSPayment.createIFrame();
	}
</script>
