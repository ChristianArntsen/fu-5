<div>
	<fieldset>
		<?php echo form_open("sales/email_receipt/" . $sale_id, array('id' => 'email_receipt_form')); ?>
		<?php echo form_hidden('sale_id', $sale_id); ?>
		<ul id="error_message_box"></ul>
		<div class="field_row clearfix">
			<span class='tip_type_label'>
				<?php echo form_label(lang('sales_email') . ':', 'email_id'); ?>
			</span>
			<div class='form_field'>
				<?php echo form_input(array('name' => 'email_id', 'value' => $customer_email, 'id' => 'email_id')); ?>
			</div>
		</div>
		<div class="field_row clearfix">
		<?php
		echo form_submit(array(
			'name' => 'submit',
			'id' => 'submit',
			'value' => lang('sales_send_email'),
			'class' => 'submit_button float_left')
		);
		?>
		</div>
		<?php echo form_close(); ?>
	</fieldset>
</div>
<script>
	$(document).ready(function() {
		$('#email_receipt_form').validate({
			submitHandler:function(form) {
				$(form).mask('Please wait...');
				$(form).ajaxSubmit({
					success:function(response) {
						if(response.success) {
							set_feedback(response.message,'success_message',false);
							$.colorbox2.close();
						} else {
							set_feedback(response.message,'error_message',true);
							$.colorbox2.close();
						}
					},
					dataType:'json'
				});
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules: {
				email_id: {
					required: true
				}
	   		},
			messages: {
				email_id: {
					required: "<?php echo lang('sales_email_required'); ?>"
				}
			}
		});
	});
</script>