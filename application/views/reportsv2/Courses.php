
<style>
    <?= file_get_contents("css/dist/main.min.css") ?>
    @media print {
        /* styles go here */
    }
    table thead th{
        padding:  0 10px 0 10px;
        text-size:9px;
    }

</style>


<div class="row static-header">
    <div class="col-sm-6"><img src="<?=$logo?>"/></div>
    <div class="col-sm-6 text-right"><?=$date?></div>
</div>
<div class="row">
    <div class="col-sm-6">
        Foreup Course Report
    </div>
    <div class="col-sm-6">
        <ul class="list-group-item">
            <li class="list-group-item">-</li>
        </ul>


    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <h4>Customers</h4>
        <table>
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Start</th>
                    <th>New</th>
                    <th>Churn</th>
                    <th>Net New</th>
                    <th>Churn Rate</th>
                    <th>Growth Rate</th>
                    <th>Total Customers</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($table as $row):?>
                    <tr>
                        <td><?=$row->row_title?></td>
                        <td class="text-right"><?=$row->start?></td>
                        <td class="text-right"><?=$row->new?></td>
                        <td class="text-right"><?=-1*$row->cancelled?></td>
                        <td class="text-right"><?=$row->net_new?></td>
                        <td class="text-right"><?=number_format($row->churn_rate*100,2)?>%</td>
                        <td class="text-right"><?=number_format($row->growth_rate*100,2)?>%</td>
                        <td class="text-right"><?=$row->end?></td>
                    </tr>
                <?php endforeach;?>

            </tbody>
        </table>
    </div>

</div>