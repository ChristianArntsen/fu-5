
<style>
    <?= file_get_contents("css/dist/main.min.css") ?>
    @media print {
        /* styles go here */
    }
</style>


<div class="row static-header">
    <div class="col-sm-6"><img src="<?=$logo?>"/></div>
    <div class="col-sm-6 text-right"><?=$date?></div>
</div>
<div class="row">
    <div class="col-sm-6">
        <h4>Cash Terminal Balance Report - <?=$terminal_name?> </h4> <Br/>
        <b><?=$course_name?></b>
    </div>
    <div class="col-sm-6">
        <ul class="list-group-item">
            <li class="list-group-item">Opened: <?=$shift_start?> - <?=$opening_employee?> </li>
            <li class="list-group-item">Closed: <?=$shift_end?> - <?=$closing_employee?> </li>
        </ul>


    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <h4>Payments Collected</h4>
        <table>
            <thead>
            <tr >
                <th class="text-center">Payment Type</th>
                <th class="text-center">New</th>
                <th class="text-center">Refunds</th>
                <th class="text-center">Voids</th>
                <th class="text-center">Total</th>
                <th class="text-center">Collected</th>
                <th class="text-center">Short / Over</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($payments as $payment):?>
                <tr>
                    <td><?=$payment->type ?></td>
                    <td class="text-right"><?=money_format('%n', $payment->new) ?></td>
                    <td class="text-right"><?=money_format('%n', $payment->refunds) ?></td>
                    <td class="text-right"><?=money_format('%n', $payment->voids) ?></td>
                    <td class="text-right"><?=money_format('%n', $payment->total) ?></td>
                    <td class="text-right"><?=money_format('%n', $payment->collected) ?></td>
                    <td class="text-right"><?=money_format('%n', $payment->difference) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot class="report-totals">
            <tr>
                <td >&nbsp;</td>
                <td class="text-right"  ><?= money_format('%n', $totals['new']) ?></td>
                <td class="text-right" ><?=  money_format('%n', $totals['refunds']) ?></td>
                <td class="text-right"  ><?= money_format('%n', $totals['voids']) ?></td>
                <td class="text-right"><?=   money_format('%n', $totals['total']) ?></td>
                <td class="text-right"><?=   money_format('%n', $totals['collected']) ?></td>
                <td class="text-right"><?=   money_format('%n', $totals['difference']) ?></td>
            </tr>
            </tfoot>
        </table>
    </div>

</div>
<div class="spacer" style="height:25px;"></div>
<div class="row">
    <div class="col-sm-6 report-notes">
        <h4>Notes</h4>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-6 "><strong>Total Cash/Check:</strong></div>
            <div class="col-sm-4 text-right"><strong><?=money_format('%n', $total_cash_check)?></strong></div>
        </div>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-6"><strong>Total Credit Card: </strong></div>
            <div class="col-sm-4 text-right"><strong><?=money_format('%n', $total_credit_card)?></strong></div>
        </div>
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-sm-12">

        <h4>GL Summary</h4>
        <h5>Sales Revenue Break By Payment Accounts</h5>
        <sup>*These numbers can not be accurate if more than one payment type was accepted for a sale</sup>
        <table>
            <thead>
            <tr >
                <th class="text-left" colspan="2">Account</th>
                <th class="text-center">Disc</th>
                <th class="text-center">Total</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($gl_codes as $gl_code => $gl_total): ?>
                <tr class="report-table-grouping">
                    <td colspan="2"><?= $gl_code ?></td>
                    <td class="text-right"><?= money_format('%n', $gl_total['discount'])?></td>
                    <td class="text-right"><?= money_format('%n', $gl_total['total'])?></td>
                </tr>
                <?php foreach($gl[$gl_code] as $gl_payment): ?>
                    <tr class="report-table-indented">
                        <td colspan="2"><?=$gl_payment['payment_type']?></td>
                        <td class="text-right"><?=money_format('%n', $gl_payment['discount'])?></td>
                        <td class="text-right"><?=money_format('%n', $gl_payment['total'])?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
            </tbody>
            <tfoot class="report-totals">
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td class="text-right"><?=money_format('%n', $gl_totals['discount']) ?></td>
                    <td class="text-right"><?=money_format('%n', $gl_totals['total']) ?></td>
                </tr>
            </tfoot>
        </table>

    </div>

</div>