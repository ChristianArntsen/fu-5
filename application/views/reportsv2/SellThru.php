
<style>
    <?= file_get_contents("css/dist/main.min.css") ?>
    @media print {
        /* styles go here */
    }
</style>


<div class="row static-header">
    <div class="col-sm-6"><img src="<?=$logo?>"/></div>
    <div class="col-sm-6 text-right"><?=$date?></div>
</div>
<div class="row">
    <div class="col-sm-6">
        <h4>Payments and Sales</h4> <Br/>
        <b><?=$course_name?></b>
    </div>
    <div class="col-sm-6">
        <ul class="list-group-item">
            <li class="list-group-item">Start: <?=$start?> </li>
            <li class="list-group-item">End: <?=$end?> </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
            <table class="table table-bordered">
                <thead>
                <tr >
                    <th class="text-center">Item</th>
                    <th class="text-center">Department</th>
                    <th class="text-center">Category</th>
                    <th class="text-center">Sub Category</th>
                    <th class="text-center">Starting Inventory</th>
                    <th class="text-center">New Inventory</th>
                    <th class="text-center">Total Sold</th>
                    <th class="text-center">Pass Thru</th>
                </tr>
                </thead>
                <tbody>
		        <?php foreach($table as $row):?>
                    <tr>
                        <td><?=$row['name'] ?></td>
                        <td><?=$row['department'] ?></td>
                        <td><?=$row['category'] ?></td>
                        <td><?=$row['subcategory'] ?></td>
                        <td><?=$row['starting_inventory'] ?></td>
                        <td><?=$row['new_inventory'] ?></td>
                        <td><?=$row['total_sold'] ?></td>
                        <td><?=$row['sell_thru_rate'] ?></td>
                    </tr>
		        <?php endforeach; ?>
                </tbody>
            </table>
            

       
    </div>

</div>
<div class="spacer" style="height:25px;"></div>

