<script>
    var field_settings = <?=json_encode($field_settings);?>

</script>
<div class='background'>
    <?php
    echo form_open('/teesheets/save_teetime/'.(!empty($teetime_info->TTID) ? $teetime_info->TTID : ''),array('id'=>'teetime_form'));
    ?>
    <div id="existing_events">
        <div id="existing_events_title">
            Existing events scheduled at this time
        </div>
        <div id="existing_events_list">
        </div>
    </div>
    <div id='radioset' class='teebuttons'>
        <input type="hidden" id="saving_flag" value=""/>
        <input type="hidden" id="teetime_id" value="" />
        <input type="hidden" id="teetime_teesheet_id" name="teetime_teesheet_id" value="<?php echo (!empty($teetime_info->teesheet_id) ? $teetime_info->teesheet_id : '');?>"/>
        <input type="hidden" name="booking_source" value="<?php echo (!empty($teetime_info->TTID) ? $teetime_info->booking_source : ''); ?>" />
        <input type="hidden" name="promo_id" value="<?php echo isset($teetime_info->promo_id) ? $teetime_info->promo_id : ''; ?>" />
        <input type='radio' value='teetime' id='type_teetime' name='event_type'  <?php echo (empty($teetime_info) || ($teetime_info->type == 'teetime'))?'checked':''; ?>><label id='teetime_label' for='type_teetime' >Tee Time</label>
        <input type='radio' value='tournament' id='type_tournament' name='event_type' <?php echo (!empty($teetime_info) && ($teetime_info->type == 'tournament'))?'checked':''; ?>><label id='tournament_label' for='type_tournament'>Tournament</label>
        <input type='radio' value='league' id='type_league' name='event_type' <?php echo (!empty($teetime_info) && ($teetime_info->type == 'league'))?'checked':''; ?>><label id='league_label' for='type_league'>League</label>
        <input type='radio' value='event' id='type_event' name='event_type' <?php echo (!empty($teetime_info) && ($teetime_info->type == 'event'))?'checked':''; ?>><label id='event_label' for='type_event'>Event</label>
        <input type='radio' value='closed' id='type_closed' name='event_type' <?php echo (!empty($teetime_info) && ($teetime_info->type == 'closed'))?'checked':''; ?>><label id='closed_label' for='type_closed'>Block</label>
        <input type='radio' value='shotgun' id='type_shotgun' name='event_type' <?php echo (!empty($teetime_info) && ($teetime_info->type == 'shotgun'))?'checked':''; ?>><label id='shotgun_label' for='type_shotgun'>Shotgun</label>
        <div class='clear'></div>
    </div>
    <div id='closed_table' <?php echo (!empty($teetime_info) && ($teetime_info->type == 'closed'))?'':"style='display:none'"?>>
        <?php $this->load->view('teetimes/closed'); ?>
    </div>
    <div id='event_table'  <?php echo (!empty($teetime_info) && ($teetime_info->type == 'tournament' || $teetime_info->type == 'league' || $teetime_info->type == 'event'))?'':"style='display:none'"?>>
        <?php $this->load->view('teetimes/event'); ?>
    </div>
    <div id="shotgun_table" <?php echo (!empty($teetime_info) && ($teetime_info->type == 'shotgun'))?'':"style='display:none'"?>>
        <?php $this->load->view('teetimes/shotgun'); ?>
    </div>
    <div id='teetime_table' <?php echo (empty($teetime_info) || $teetime_info->type == 'teetime' || $teetime_info->type == '')?'':"style='display:none'"?>>
        <?php $this->load->view('teetimes/teetime'); ?>
    </div>
    <?php $this->load->view('teetimes/styles'); ?>
    <style>
        #existing_events {
            padding:15px 0px 5px 20px;
        }
        #existing_events_title {
            font-size:16px;
            font-weight:bold;
        }
        #existing_events_list div {
            font-size:14px;
            padding:3px 0px 0px 10px;
        }
        #existing_events_list div.existing_pending_reservation {
            color:#960000;
        }
    </style>
    <script>
        inline_form.initialize();
    </script>


