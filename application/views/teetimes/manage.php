<?php $this->load->view("partial/header_new"); ?>
<script type='text/javascript'>

	var old = ''; // used for event source auto_updates

	var calInfo = {};
    alldata = eval('(<?php echo $JSONData ?>)');
    all_tee_times = $.parseJSON('<?=$tee_times?>');
    all_tee_sheets = $.parseJSON('<?=$json_tee_sheets?>');
    stack_tee_sheets = <?=$this->config->item('stack_tee_sheets')?>;
    side_by_side_tee_sheets = <?=$this->config->item('side_by_side_tee_sheets')?>;
    calInfo.caldata = alldata.caldata;
    calInfo.bcaldata = alldata.bcaldata;
    calInfo.openhour = convertFormat('<?php echo $openhour?>');
    calInfo.closehour = convertFormat('<?php echo $closehour?>');
    calInfo.sunrise_offset = convertFormat('<?php echo $sunrise_offset?>');
    calInfo.sunset_offset = convertFormat('<?php echo $sunset_offset?>');
    calInfo.increment1 = '<?php echo $increment?>';
    calInfo.increment2 = '<?php echo $increment?>';
    calInfo.holes = '<?php echo $holes?>';
    calInfo.frontnine = '<?php echo $fntime?>';
    calInfo.purchase = '<?php echo $purchase?>';
    calInfo.hide_back_nine = '<?=$this->session->userdata('hide_back_nine');?>';
	calInfo.forecast = [];

    var BackboneApp = false;
    if(window.parent && window.parent.App){
        BackboneApp = window.parent.App;
    }

    function can_check_in_teetime(teesheet_id){
        
        var teesheet = BackboneApp.data.course.get('teesheets').get(teesheet_id);

        if(teesheet && teesheet.get('course_id') != BackboneApp.data.course.get('course_id')
        ){
            BackboneApp.vent.trigger('notification', {msg: 'Check-in is only available at '+ teesheet.get('course_name'), type: 'error'});
            return false;
        }
        return true;
    }

    function can_check_in_event(teesheet_id){
        var teesheet = BackboneApp.data.course.get('teesheets').get(teesheet_id);
        if(teesheet && teesheet.get('course_id') != BackboneApp.data.course.get('course_id')){
            BackboneApp.vent.trigger('notification', {msg: 'Check-in is only available at '+ teesheet.get('course_name'), type: 'error'});
            return false;
        }
        return true;
    }

    function convertFormat(hourString) {
        var h = parseInt(hourString.slice(0,2), 10);
        var m = parseInt(hourString.slice(2), 10);
        var mz = '';
        if (m < 10)
            mz = 0;
        return h+':'+mz+m
    }
    function add_new_teetimes() {
    	setTimeout('Calendar_actions.add_new_teetimes()', 1);
    }
	var teesheet_controls = {
		increments:{
			squeeze:{
				list:
					<?=$squeezes?>
				,
				add: function(data) {
					var list = teesheet_controls.increments.squeeze.list;
					if (typeof list[data['tee_sheet_id']] == 'undefined') {
						list[data['tee_sheet_id']] = {};
					}
					if (typeof list[data['tee_sheet_id']][data['date']] == 'undefined') {
						list[data['tee_sheet_id']][data['date']] = {};
					}
					list[data['tee_sheet_id']][data['date']][data['start']] = {'start':data['start'], 'end':data['end']};
				},
				remove: function(data) {
					var list = teesheet_controls.increments.squeeze.list;
					delete list[data['tee_sheet_id']][data['date']][data['start']];// = {'start':data['start'], 'end':data['end']};
				},
				get:function(tee_sheet_id, date, time){
					var list = teesheet_controls.increments.squeeze.list;
					var subset = {};

					if (tee_sheet_id == undefined) {
						return subset;
					}

					if (date == null && time == null) {
						subset = list[tee_sheet_id];
					}
					else {
						var selectedYear = date.getFullYear();
						var selectedMonth = date.getMonth() + 1;
						var selectedDay = date.getDate();
						var m_z = selectedMonth < 10 ? '0' : '';
						var d_z = selectedDay < 10 ? '0' : '';
						var date_string = selectedYear+'-'+m_z+selectedMonth+'-'+d_z+selectedDay;
						if (time == null) {
							if (typeof list[tee_sheet_id] != 'undefined') {
								subset = list[tee_sheet_id][date_string];
							}
						} else {
							if (typeof list[tee_sheet_id] != 'undefined') {
								for (var i in list[tee_sheet_id][date_string]) {
									if (parseInt(i) < parseInt(time)) {
										subset[i] = list[tee_sheet_id][date_string][i];
									}
								}
							}
						}
					}
					return subset;
				}
			},
			dailies: {
                list:
                <?=$dailies?>
                ,
                get:function(tee_sheet_id, date){
                    console.log('tee_sheet_id '+tee_sheet_id);
                    var list = teesheet_controls.increments.dailies.list;
                    var subset = [];

                    if (tee_sheet_id == undefined) {
                        return subset;
                    }

                    if (date == null && time == null) {
                        subset = list[tee_sheet_id];
                    }
                    else {
                        console.log('trying to get');
                        var weekday = [];
                        weekday[0]=  "sunday";
                        weekday[1] = "monday";
                        weekday[2] = "tuesday";
                        weekday[3] = "wednesday";
                        weekday[4] = "thursday";
                        weekday[5] = "friday";
                        weekday[6] = "saturday";
                        var selectedMonth = date.getMonth() + 1;
                        var selectedDay = date.getDate();
                        var selectedDayOfWeek = weekday[date.getDay()];
                        var m_z = selectedMonth < 10 ? '0' : '';
                        var d_z = selectedDay < 10 ? '0' : '';
                        var date_string = '0000-'+m_z+selectedMonth+'-'+d_z+selectedDay;
                        if (typeof list[tee_sheet_id] != 'undefined') {
                            for (var i in list[tee_sheet_id]) {
                                console.log(list[tee_sheet_id][i]['start_date']+' '+date_string+' '+list[tee_sheet_id][i]['end_date']+' == '+selectedDayOfWeek+' - '+list[tee_sheet_id][i][selectedDayOfWeek]);
                                if (list[tee_sheet_id][i]['start_date'] <= date_string && list[tee_sheet_id][i]['end_date'] >= date_string && list[tee_sheet_id][i][selectedDayOfWeek] == 1) {
                                    subset.push(list[tee_sheet_id][i]);
                                }
                            }
                        }
                    }
                    return subset;
                }
			}
		},
		setTodaysTemp:function(newForecast){
			$('#current_temp').html(newForecast['high_temp']+"&deg;");
			$('#high').html(newForecast['high_temp']+"&deg;");
			$('#low').html(newForecast['low_temp']+"&deg;");
			$('#weather_icon').html("<i class=\"wi wi-forecast-io-"+newForecast['image']+"\" ></i>");
		},
		getExistingDate:function(){
			var day = $('#teesheet_day').text();
			var month = $('#teesheet_month').text();
			var date_string = day+" "+ month;
			var existingDate = new Date(date_string);
			return moment(existingDate).startOf('day');
		},
		change_date:function(date){
			self = this;
			newDate = new moment(date).startOf('day');
			if(this.getExistingDate().diff(newDate) === 0 ){
				return true;
			}

			var dow = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
			var month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			$('#teesheet_day').html(date.getDate());
			$('#teesheet_dow').html(dow[date.getDay()]);
			$('#teesheet_month').html(month[date.getMonth()]+', '+date.getFullYear());

			var newForecast = _.findWhere(calInfo.forecast,{date:newDate.format("YYYY-MM-DD")});
			if(!_.isEmpty(newForecast)){
				this.setTodaysTemp(newForecast);
			} else {

				$.ajax({
					type: "GET",
					url: "index.php/teesheets/past_weather/"+newDate.format("YYYY-MM-DD"),
					headers: {"read_only_session":1},
					success: function(response){
						var newForecast = {
							"high_temp":response.data.max,
							"low_temp":response.data.min,
							"image":response.data.icon,
							"date":response.data.day
						};
						calInfo.forecast.push()
						if(!_.isEmpty(response.data.max)){
							self.setTodaysTemp(newForecast);
						}
					},
					dataType:'json'
				});


				var randomCoolIcons = ["wi-thermometer","wi-na"]
				$('#current_temp').html("--&deg;");
				$('#high').html("-");
				$('#low').html("-");
				var newIcon = randomCoolIcons[Math.floor(Math.random() * randomCoolIcons.length)];
				$('#weather_icon').html("<i class=\"wi "+newIcon+"\" ></i>");
			}
		},
		change_teesheet:function(){

		},
        highlight_tee_sheet: function(tee_sheet_id, date, start){
            $.colorbox({
                'href':'index.php/teesheets/view_increment_quick_settings/'+tee_sheet_id+'/'+date+'/'+start,
                'width':450,
                'title':'Highlight Tee Sheet Block',
                'onComplete':function(){}
            });
        }
	};
	function toggle_back_nine() {
        if (calInfo.hide_back_nine == 1) {
            calInfo.hide_back_nine = 0;
            render_back_nine();
            // UPDATE DATE AFTER BACK IS SHOWN
            $.ajax({
                type: "POST",
                url: "index.php/teesheets/hide_back_nine/0",
                data: '',
                success: function (response) {
                },
                dataType: 'json'
            });

        }
        else {
            calInfo.hide_back_nine = 1;
            render_back_nine();
            $.ajax({
                type: "POST",
                url: "index.php/teesheets/hide_back_nine/1",
                data: '',
                success: function (response) {
                },
                dataType: 'json'
            });
        }
    }
    function render_back_nine() {
        $('.calendar').each(function() {
            var tee_sheet_id = Calendar_actions.tee_sheet_id($(this));
            var front_nine = $('#frontnine-' + tee_sheet_id);
            var back_nine = $('#backnine-' + tee_sheet_id);
            if (side_by_side_tee_sheets) {
                front_nine.css('width', '100%');
                var fn_count = $('.frontnine').length;
                if (fn_count > 3) {
                    $('.tee_sheet_box').css('width', '25%');
                }
                else if (fn_count > 2) {
                    $('.tee_sheet_box').css('width', '33%');
                }
                else if (fn_count > 1) {
                    $('.tee_sheet_box').css('width', '50%');
                }
                else {
                    $('.tee_sheet_box').css('width', '100%');
                }
                $('.tee_sheet_box').css('float', 'left');
                back_nine.hide();
            }
            else if ($("#calendar-" + tee_sheet_id).fullCalendar('getView').name == 'agendaWeek' || (all_tee_sheets[tee_sheet_id].holes == 18 && calInfo.hide_back_nine == 1) || all_tee_sheets[tee_sheet_id].holes == 9) {
                front_nine.css('width', '100%');
                back_nine.hide();
            }
            else {
                front_nine.css('width', '50%');
                back_nine.show();
            }

            var d = $('#calendar-' + tee_sheet_id).fullCalendar('getDate');
            $('#calendarback-' + tee_sheet_id).fullCalendar('gotoDate', d.getFullYear(), d.getMonth(), d.getDate());
            $('#calendarback-' + tee_sheet_id).fullCalendar('rerenderEvents');
        });
    }

    function readjust_fron_nine()
    {
        $('.calendar').each(function() {
            var tee_sheet_id = Calendar_actions.tee_sheet_id($(this));
            var front_nine = $('#frontnine-'+tee_sheet_id);
            var back_nine = $('#backnine-'+tee_sheet_id);
            if (back_nine.css('display') == 'none' || all_tee_sheets[tee_sheet_id].holes == 9 || $("#calendar-"+tee_sheet_id).fullCalendar('getView').name == 'agendaWeek') {
                front_nine.css('width', '100%');
            }
            else {
                front_nine.css('width', '50%');
            }
        });
    }
    $(document).ready(function(){
        $('#back_nine_toggle').click(function(){
     		toggle_back_nine();
        });
        $('#highlight_prices_toggle').click(function(){
     		$('#main').toggleClass('highlight_prices');
        });
        $('#manage_templates').click(function(){
            $.colorbox({'href':'index.php/teesheets/view_template_manager', 'title':'Save/Apply Template','width':'800'});
        });
        $('.standby_entry').live("click", function(){
           $.ajax({
	           type: "POST",
	           url: "index.php/teesheets/get_standby",
	           data: 'id=' + $(this).attr('id'),
	           success: function(response){
	               console.log('response is: ' + response.urlData);
	          	   $.colorbox({'href':'index.php/teesheets/view_standby' + response.urlData,'width':1000,'title':'Standby Golfers'});
	           },
	           dataType:'json'
	       });
    	});
	    // Use backbone customer edit window
	    $('#teesheet_send_msg').live('click', function(e){
		    e.preventDefault();

//		    var modal = new window.parent.TeesheetToMarketingModal({model:new window.parent.Customer()});
//		    modal.show();
		    var cal_date = $(".calendar").fullCalendar('getDate');
		    console.log(cal_date);
		    var recipients = new window.parent.TeesheetRecipients([],{
			    "start":moment(cal_date).startOf('day').format(),
			    "end":moment(cal_date).endOf('day').format()
		    });


		    var modal = new window.parent.TeesheetToMarketingModalView ({collection:recipients});

		    modal.show();
		    modal.refresh();



		    return false;
	    });



        $('.new_standby').click(function(){
            $.colorbox({'href':'index.php/teesheets/view_standby', 'width':1000, 'title':'Standby Golfers'});
        });
    	$('.new_note').click(function(){
            $.colorbox({'href':'index.php/teesheets/view_note', 'width':525, 'title':'Add Note'});
        });
    	$('#settings_button').click(function(){
            <?php if ($this->config->item('seasonal_pricing')) { ?>
            $.colorbox({'href':'index.php/teesheets/seasonal_teesheet/<?php echo $this->session->userdata('teesheet_id'); ?>','width':675,'title':'Teesheet Settings'});
            <?php } else { ?>
            $.colorbox({'href':'index.php/teesheets/view_teesheet/<?php echo $this->session->userdata('teesheet_id'); ?>','width':675,'title':'Teesheet Settings'});
            <?php } ?>
    	});

    	var wb = $('#weatherBox ul');

    	wb.click(function(){
    		if (wb.hasClass('expanded'))
    		{
    			wb.removeClass('expanded');
        		$('#wbtabs-1').hide();
			}
    		else
    		{
	    		wb.addClass('expanded');
        		$('#wbtabs-1').show();
    		}
    	});
    	var ft = $('#facebook_title');
    	ft.click(function(){
    		if (ft.hasClass('expanded'))
    		{
    			ft.removeClass('expanded');
        		$('#facebook_content').hide();
			}
    		else
    		{
	    		ft.addClass('expanded');
        		$('#facebook_content').show();
    		}
    	})
    	var tst = $('#teetime_search_title');
    	tst.click(function(e){
    		if (tst.hasClass('expanded'))
    		{
    			tst.removeClass('expanded');
        		$('#teetime_search_content').hide();
			}
    		else
    		{
	    		tst.addClass('expanded');
        		$('#teetime_search_content').show();
    		}
    	})
        var tstandby = $('#teetime_standby_title');
    	tstandby.click(function(e){
    		if (tstandby.hasClass('expanded'))
    		{
    			tstandby.removeClass('expanded');
        		$('#teetime_standby_content').hide();
			}
    		else
    		{
	    		tstandby.addClass('expanded');
        		$('#teetime_standby_content').show();
    		}
    	})
        var note_collapse = $('#notes_title');
    	note_collapse.click(function(e){
    		if (note_collapse.hasClass('expanded'))
    		{
    			note_collapse.removeClass('expanded');
        		$('#notes_content').hide();
			}
    		else
    		{
	    		note_collapse.addClass('expanded');
        		$('#notes_content').show();
    		}
    	})
        // LOAD INITIAL NOTES
        $.ajax({
           type: "POST",
           url: "index.php/teesheets/get_notes/5",
	        headers: {"read_only_session":1},
           data: '',
           success: function(response){
          	 	$('#note_list').html(response.note_html);
          	 	if (response.note_count > 0)
          	 		$('#notes_title').click();
           },
           dataType:'json'
         });

    	$('#month_view').click(function(){

            calendar.fullCalendar( 'changeView', 'month', {
                droppable:true,
                editable:true,
                drop:function()
                {
                    var originalEventObject = $(this).data('eventObject');
                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);
                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    calendar.fullCalendar('renderEvent', eventObject, true);
                    //alert("DROPPED!");
                }
            });
            calendar.droppable();


                        calendar.fullCalendar( 'changeView', 'month', {

                                    droppable:true,
                                    editable:true,
                                    drop:function()
                                    {

                                        var originalEventObject = $(this).data('eventObject');

                                        // we need to copy it, so that multiple events don't have a reference to the same object
                                        var copiedEventObject = $.extend({}, originalEventObject);

                                        // assign it the date that was reported
                                        copiedEventObject.start = date;
                                        copiedEventObject.allDay = allDay;
                                        calendar.fullCalendar('renderEvent', eventObject, true);

                                        //alert("DROPPED!");
                                    }

                                });
                                calendar.droppable();

			$('#teesheet_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
		});

		$('#summary_toggle').click(function(){
			$("#container").toggle();
			$(this).toggleClass('selected');
		});
    	$('#week_view').click(function(){
			calendar.fullCalendar( 'changeView', 'agendaWeek' )
			$('#teesheet_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
		});
    	$('#day_view').click(function(){
			calendar.fullCalendar( 'changeView', 'agendaDay' )
			$('#teesheet_view_buttons .selected').removeClass('selected');
			$(this).addClass('selected');
            render_back_nine();
		});
		$('#previous_day').click(function(){
			calendar.fullCalendar('prev');
			calendarback.fullCalendar('prev');
			readjust_fron_nine();
		})
		$('#today').click(function(){
			calendar.fullCalendar('today');
			calendarback.fullCalendar('today');
			readjust_fron_nine();
		})
		$('#next_day').click(function(){
			calendar.fullCalendar('next');
			calendarback.fullCalendar('next');
			readjust_fron_nine();
		})
    	<?php if ($this->config->item('teesheet_updates_automatically')) { ?>
        	//Only if it's enabled in settings
    		if (!!window.EventSource) {
                //var old = '';
				var source = new EventSource('index.php/teesheets/teesheet_updates/<?php echo $this->session->userdata('course_id');?>');

				source.onmessage = function(e)
				{
					var nd = new Date();
					if(old!=e.data){
						console.log(e.data);
						var  response = eval("("+e.data+")");
				  		var events = $.merge(response.caldata, response.bcaldata);
			               Calendar_actions.update_teesheet(events, true, 'auto_update');

						old = e.data;
					}
				};
    		}
    		else {
    			add_new_teetimes();
    		}
    	<?php } ?>
    	$.ajax({
           type: "POST",
           url: "index.php/teesheets/make_5_day_weather",
		    headers: {"read_only_session":1},
           data: '',
           success: function(response){
          	 	$('#wbtabs-1').html(response.weather);
   			 	$('#teesheet_weather_box').html(response.todays);
	           if(response.forecast){
		           calInfo.forecast = response.forecast;
	           }
           },
           dataType:'json'
         });
        setTimeout(function(){render_back_nine()}, 100);
    });
    function post_teetime_form_submit(response)
	{
		set_feedback(response.message,'warning_message',false, 5000);
	}

</script>
<style>
    #teesheet_note_box {
        position: relative;
        z-index: 100;
        overflow: visible;
    }
    #teesheet_note_box textarea#teeheet_daily_note {
        position:absolute;
        z-index: 100;
        left:0;
    }
	.fc-event-bg table {
		margin-top:30px;
		margin-left:12px;
		font-size:10px;
		font-weight:normal;
	}
	/* Applying teesheet color */
	<?php if ($teesheet_color != '') { ?>
	#teesheet_controls, #table_top, div#reg_item_search {
		background: <?=$teesheet_color?>; 
	}
	<?php } ?>
	#teesheets .pending_reservation {
		background: #ffcaca; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffcaca', endColorstr='#7d524e'); /* for IE */
		background: -webkit-linear-gradient(top, #ffcaca, #7d524e);
		background: -moz-linear-gradient(top,  #ffcaca,  #7d524e); /* for firefox 3.6+ */
	}
    #teesheets .pending_reservation .fc-event-content .fc-event-title:after {
        content:' - PENDING RESERVATION';
    }
    #teesheets .fc-event-bg {
        background: #f8f8f8; /* for non-css3 browsers */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f8f8f8', endColorstr='#bebebe'); /* for IE */
        background: -webkit-linear-gradient(top, #f8f8f8, #bebebe);
        background: -moz-linear-gradient(top,  #f8f8f8,  #bebebe); /* for firefox 3.6+ */
    }
    #teesheets .teetime {
        background: #f8f8f8; /* for non-css3 browsers */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f8f8f8', endColorstr='#bebebe'); /* for IE */
        background: -webkit-linear-gradient(top, #f8f8f8, #bebebe);
        background: -moz-linear-gradient(top,  #f8f8f8,  #bebebe); /* for firefox 3.6+ */
    }
    #teesheets .tournament {
		background: #fff9e8; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff9e8', endColorstr='#fccc58'); /* for IE */
		background: -webkit-linear-gradient(top, #fff9e8, #fccc58);
		background: -moz-linear-gradient(top,  #fff9e8,  #fccc58); /* for firefox 3.6+ */
	}
	#teesheets .closed {
		background: #fdf2f2; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fdf2f2', endColorstr='#fac1c1'); /* for IE */
		background: -webkit-linear-gradient(top, #fdf2f2, #fac1c1);
		background: -moz-linear-gradient(top,  #fdf2f2,  #fac1c1); /* for firefox 3.6+ */
	}
	#teesheets .event {
		background: #FBEDFF; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#FBEDFF', endColorstr='#F2BDFF'); /* for IE */
		background: -webkit-linear-gradient(top, #FBEDFF, #F2BDFF);
		background: -moz-linear-gradient(top,  #FBEDFF,  #F2BDFF); /* for firefox 3.6+ */
	}
	#teesheets .shotgun {
		background: #f9f6f2;
		background: -moz-linear-gradient(top,  #f9f6f2 0%, #dbc3a2 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f9f6f2), color-stop(100%,#dbc3a2));
		background: -webkit-linear-gradient(top,  #f9f6f2 0%,#dbc3a2 100%);
		background: -o-linear-gradient(top,  #f9f6f2 0%,#dbc3a2 100%);
		background: -ms-linear-gradient(top,  #f9f6f2 0%,#dbc3a2 100%);
		background: linear-gradient(to bottom,  #f9f6f2 0%,#dbc3a2 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f6f2', endColorstr='#dbc3a2',GradientType=0 );
	}

	#teesheets .league {
		background: #bddeff; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ecfaf1', endColorstr='#b8ffd6'); /* for IE */
		background: -webkit-linear-gradient(top, #ecfaf1, #b8ffd6);
		background: -moz-linear-gradient(top,  #ecfaf1,  #b8ffd6); /* for firefox 3.6+ */
	}
	#teesheets .teetime div.fc-event-inner {
		border: 1px solid #929292;
	}
	#teesheets .tournament div.fc-event-inner {
		border: 1px solid #B18106;
	}
	#teesheets .closed div.fc-event-inner {
		border: 1px solid #B10606;
	}
	#teesheets .event div.fc-event-inner {
		border: 1px solid #993399;
	}
	#teesheets .shotgun div.fc-event-inner {
		border: 1px solid #BC935E;
	}
	#teesheets .league div.fc-event-inner {
		border: 1px solid #396;
/*		border: 1px solid #1AB106;*/
	}
	#teesheets .paid_1_1 .fc-event-bg, #teesheets .paid_2_2 .fc-event-bg, #teesheets .paid_3_3 .fc-event-bg, #teesheets .paid_4_4 .fc-event-bg, #teesheets .paid_5_5 .fc-event-bg {
		background: #edf8ff; /* for non-css3 browsers */
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#edf8ff', endColorstr='#bddeff'); /* for IE */
		background: -webkit-linear-gradient(top, #89C8F5, #4885B1);
		background: -moz-linear-gradient(top,  #89C8F5,  #4885B1); /* for firefox 3.6+ */
		/*background:-webkit-gradient(linear, left top, right top, color-stop(0%,#ecfaf1), color-stop(100%,#b8ffd6))*/
	}
	#teesheets .paid_4_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#89C8F5), color-stop(80%,#4885B1), color-stop(80%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_3_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#89C8F5), color-stop(60%,#4885B1), color-stop(60%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_2_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#89C8F5), color-stop(40%,#4885B1), color-stop(40%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_1_5 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#89C8F5), color-stop(20%,#4885B1), color-stop(20%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_3_4 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#89C8F5), color-stop(75%,#4885B1), color-stop(75%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_2_4 .fc-event-bg, #teesheets .paid_1_2 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#89C8F5), color-stop(50%,#4885B1), color-stop(50%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_1_4 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#89C8F5), color-stop(25%,#4885B1), color-stop(25%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_2_3 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#89C8F5), color-stop(67%,#4885B1), color-stop(67%,transparent), color-stop(100%,transparent))
	}
	#teesheets .paid_1_3 .fc-event-bg {
		background:-webkit-gradient(linear, left top, right top, color-stop(0%,#89C8F5), color-stop(34%,#4885B1), color-stop(34%,transparent), color-stop(100%,transparent))
	}
    <?php if ($this->config->item('teed_off_color')) { ?>
    #teesheets .teed_off .fc-event-bg {
        background: #edf8ff; /* for non-css3 browsers */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='<?=$this->config->item('teed_off_color')?>', endColorstr='<?=$this->config->item('teed_off_color')?>'); /* for IE */
        background: -webkit-linear-gradient(top, <?=$this->config->item('teed_off_color')?>, <?=$this->config->item('teed_off_color')?>);
        background: -moz-linear-gradient(top,  <?=$this->config->item('teed_off_color')?>,  <?=$this->config->item('teed_off_color')?>); /* for firefox 3.6+ */
        /*background:-webkit-gradient(linear, left top, right top, color-stop(0%,#ecfaf1), color-stop(100%,#b8ffd6))*/
    }
    <?php } ?>
    <?php if ($this->config->item('tee_time_completed_color')) { ?>
    #teesheets .mark_finished .fc-event-bg {
        background: #edf8ff; /* for non-css3 browsers */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='<?=$this->config->item('tee_time_completed_color')?>', endColorstr='<?=$this->config->item('tee_time_completed_color')?>'); /* for IE */
        background: -webkit-linear-gradient(top, <?=$this->config->item('tee_time_completed_color')?>, <?=$this->config->item('tee_time_completed_color')?>);
        background: -moz-linear-gradient(top,  <?=$this->config->item('tee_time_completed_color')?>,  <?=$this->config->item('tee_time_completed_color')?>); /* for firefox 3.6+ */
        /*background:-webkit-gradient(linear, left top, right top, color-stop(0%,#ecfaf1), color-stop(100%,#b8ffd6))*/
    }
    <?php } ?>
	<?php foreach($price_colors as $category_id => $color){
	if(empty($color)){ continue; } ?>
	#main.highlight_prices .price_color_<?php echo $category_id; ?> .fc-event-bg {
		background: <?php echo $color; ?> !important;
	}
	<?php } ?>

    #teesheets .frontnine, #teesheets .backnine {
        width:50%;
    }
	#back_nine_toggle, #highlight_prices_toggle, #manage_templates {
		float: right;
		margin: 12px 8px 0px 0px;
		border: 1px solid #b9b9b9;
		border-radius: 4px;
		color: #000;
		box-shadow: inset 0px 1px 52px -21px #000;
		padding:7px 12px;
		cursor:pointer;
	}

	#shotgun_grid tr td {
		padding: 4px 0px 4px 0px;
	}
	#shotgun_grid tr td.hole {
		color: #555;
		font-weight: bold;
		padding-right: 10px;
	}

	#shotgun_grid tr td input.shotgun-person {

	}

	#shotgun_grid tr td span.person {
		border: 1px solid #666;
		background: -moz-linear-gradient(center top , #9C9C9C, #797979) repeat scroll 0 0 rgba(0, 0, 0, 0);
		padding: 5px 10px;
		color: white;
	}
    #teesheet_note_box {
        width: 200px !important;
        margin: 12px 0px 0px 15px;
    }
    .tee_sheet_name {
        font-size: 26px;
        font-weight: bold;
        border-top: 5px solid #555;
        height: 30px;
        line-height: 30px;
    }
    tr.squeeze_time {
        background:#D2D2D2;
    }
    tr.not_available {
        background:#D2D2D2;
    }
    .alternate_increment {
        background:#F3F3F3;
    }
	.squeeze_label {
		padding: 12px 10px 0px;
		color: #818181;
		font-weight: bold;
		position:absolute;
	}
    #manage_templates:after {
        content: "Save/Apply Templates";
    }
    #highlight_prices_toggle:after {
        content: "Highlight Prices";
    }
    #back_nine_toggle:after {
        content: "Toggle Back 9";
    }
    @media all and (max-width:1450px) {
        #manage_templates:after {
            content: "Templates";
        }
        #highlight_prices_toggle:after {
            content: "Prices";
        }
        #back_nine_toggle:after {
            content: "Back 9";
        }
    }
    @media only screen  and (max-width : 1280px) {
	    .simple-teesheet-link a{
		    font: 28px Roboto;
		    width: 175px;
		    text-decoration: none;
		    background-color: #428bca;
		    color: white;
		    display: block;
		    padding: 2px 6px 2px 6px;
		    z-index: 99999;
		    border-radius: 4px;
		    visibility:visible!important;
		    display:block!important;
	    }
    }
    .simple-teesheet-link a{
	    visibility: none;
	    display: none;
    }

	#teesheet_send_msg{
		border: 1px solid black;
		border-image-source: initial;
		border-image-slice: initial;
		border-image-width: initial;
		border-image-outset: initial;
		border-image-repeat: initial;
		background: rgba(0, 0, 0, 0.2);
		width: 175px;
		margin-top: 10px;
		font-size: 16px;
		padding: 4px 0px 8px 12px;
		font-weight: lighter;
		color: white;
		text-align: left;
		cursor: pointer;
	}
</style>
<div class="wrapper">
    <div class="column">
	    <div class="tee_sheet_box" >
		    <div class="content simple-teesheet-link">
			    <a target="_parent"
			       href="/index.php/v2/home#starter_teesheet/<?=$this->session->userdata('teesheet_id');?>">Starter Sheet</a>
		    </div>
	    </div>

        <?php  if($this->permissions->employee_has_module('marketing_campaigns')): ?>
            <div id='teesheet_send_msg' class=''>Contact Tee Sheet</div>
        <?php endif; ?>
    	<div id='teetime_search_box'>
    		<div id='teetime_search_title'>Search<span class='icon minimize_icon'></span></div>
    		<div id='teetime_search_content' style='display:none'>
    			<input type='text' placeholder='Customer Name' value='' id='teetime_search_name'/>
    			<input type='hidden' value='' id='teetime_search_id'/>
    			<div id='teetime_search_results'>
    				<div class='teetime_result'>
	    				No results
	    			</div>
    			</div>
    		</div>
    	</div>
    	<div id="teetime_standby_box">
         	<div id='teetime_standby_title' class=''>Standby<span class='icon minimize_icon'></span></div>
        	<div id='teetime_standby_content' style='display:none;'>
	        	<ul class='expanded'>
                    <li>
		    			<input class="new_standby" type="button" value="Add New Standby">
                        <div id="teetime_populate_standby_list" style="overflow:auto; max-height: 300px">
                            <div class='teetime_result'>
                            No Standby Golfers
                            </div>
                        </div>
                    </li>
                </ul>
	        </div>
    	</div>
    	<div id="notes_box">
         	<div id='notes_title' class=''>Notes<span class='icon minimize_icon'></span></div>
        	<div id='notes_content' style='display:none;'>
	        	<ul class='expanded'>
                    <li>
		    			<input class="new_note" type="button" value="Add Note">
                        <div id="note_list" style="overflow:auto; max-height: 300px">
                        	<?php
                                //TODO: This logic shouldn't be here
                                if(!isset($notes)){
                                    $notes = [];
                                }
                                $this->load->view('employees/notes', array('notes',$notes));
                            ?>
                        </div>
                    </li>
                </ul>
	        </div>
    	</div>
        <div id="datepicker"></div>
        <?php if (!$this->config->item('simulator')) {?>
        <div id="weatherBox" class="weatherBox">
            <ul class='expanded'>
                <!--li class="stats"><a href="#tabs-1">Players</a></li-->
                <li class="weather"></i><a href="#wbtabs-1">Weather</a><span class='icon minimize_icon'></span></li>
            </ul>
            <!--div id='tabs-1' class="stats">

            </div-->
            <div id='wbtabs-1'>
                <?php //echo $fdweather; ?>
            </div>
        </div>
        <?php } ?>
        <!--div id="stats">
            <ul>

            </ul>

        </div-->
        <?php if (!$this->config->item('disable_facebook_widget')) { ?>
<!--         <div class="facebook_messages logged_in">-->
<!--         	<div id='facebook_title'>Facebook<span class='icon minimize_icon'></span></div>-->
<!--        	<div id='facebook_content' style='display:none'>-->
<!--				<div id="selected_page" class="truncated"></div>-->
<!--	        	<ol id="selectable">-->
<!---->
<!--				</ol>-->
<!--				<textarea id="message" rows="8" maxlength="150" placeholder="Enter message to post to page"></textarea>-->
<!--		    	<input class="facebook_login" type="button" value="Login">-->
<!--		    	<input class="post facebook_post" type="button" value="Post to Page">-->
<!--	        	<!--input class="post" type="button" value="Post" style='display:none'/>-->
<!--			  	<input class="login" type="button" value="Login"/>-->
<!--			  	<input class="logout" type="button" value="Logout" style='display:none'/-->-->
<!--			  	<span id='facebook_logout'>Logout</span>-->
<!--			</div>-->
<!--    	</div>-->
        <?php } ?>
        <?php if ($this->permissions->employee_has_module('config')) { ?>
        <div id='settings_button' class='ui-widget-content'>
        	<span id='settings_icon'></span> Teesheet Settings
        </div>
        <?php } ?>
    </div>
    <?php if ($current_teesheet == '') { ?>
    	<div style='padding-top:30px; font-size:18px;'>
    	No default teesheet available, please create one under settings.
    	</div>
    <?php } else { ?>
    <div id="main" class="container">
        <div id='teesheet_controls'>
    		<div id='course_selector'>
    		<?php if ($tsMenu != '') {?>
	            <form action="" method="post">
	                    <?php echo $tsMenu; ?>
	                    <input type="submit" value="changets" name="changets" id="changets" style="display:none"/>
	                    <input type="hidden" value="" name="teesheet_year" id="teesheet_selected_year"/>
	                    <input type="hidden" value="" name="teesheet_month" id="teesheet_selected_month"/>
	                    <input type="hidden" value="" name="teesheet_date" id="teesheet_selected_date"/>
	            </form>
	        <?php } ?>
	       </div>
	       <a id='print_button'>
	       		<div id='teesheet_download_button'>&nbsp;</div>
	       </a>
    	   <div id='teesheet_view_buttons'>
	       		<ul>
	       			<!--li id='month_view'>Month</li-->
	       			<li id='week_view'>Week</li>
	       			<li id='day_view' class='last selected'>Day</li>
	       		</ul>
	       </div>
			<div id='teesheet_view_buttons'>
				<ul>
					<li class="selected" id="summary_toggle">Toggle Summary</li>
				</ul>
			</div>
	    </div>
		<div id="teesheet_summary">
			<div id="container" style="position:relative;margin: 0 auto"></div>
		</div>
    	<div id='teesheet_info'>
    		<div id='teesheet_date_box'>
    			<div id='teesheet_day'>

    			</div>
    			<div class='date_info'>
    				<div id='teesheet_dow'>

    				</div>
    				<div id='teesheet_month'>

    				</div>
    			</div>
    			<div class='clear'></div>
    		</div>
    		<div class='line_spacer'>

    		</div>
    		<div id='teesheet_weather_box'>
    			<div id='weather_icon'>

    			</div>
    			<div id='current_temp'>

    			</div>
    			<div id='low_high'>
    				<div id='high'></div>
    				<div id='low'></div>
    			</div>
    		</div>
            <div id="teesheet_note_box">
                <textarea id="teeheet_daily_note" noteId="" name="teeheet_daily_note"></textarea>
            </div>

            <div id='teesheet_day_navigator'>
    			<ul>
	       			<li id='previous_day'>Previous</li>
	       			<li id='today'>Today</li>
	       			<li id='next_day' class='last'>Next</li>
	       		</ul>
		    </div>
		    <?php if ($holes == 18 && !$this->config->item('side_by_side_tee_sheets')) { ?>
		    <span id='back_nine_toggle'>

		    </span>
		    <?php } ?>
            <span id='highlight_prices_toggle'>

		    </span>
		    <span id='manage_templates'>

		    </span>
    	</div>
    	<div class="inner clearfix">
        <?
            if ($this->config->item('stack_tee_sheets')) {
                foreach($tee_sheets as $tee_sheet) { ?>
                    <div class="tee_sheet_box">
                        <div class="tee_sheet_name">Tee Sheet: <?=$tee_sheet['title']?></div>
                        <div class="content">
                            <div id="frontnine-<?=$tee_sheet['teesheet_id']?>" data-tee-sheet-id="<?=$tee_sheet['teesheet_id']?>" class="frontnine tee_sheet_holder">
                                <div id='calendar-<?=$tee_sheet['teesheet_id']?>' class="calendar"></div>
                            </div>
                            <div id="backnine-<?=$tee_sheet['teesheet_id']?>" data-tee-sheet-id="<?=$tee_sheet['teesheet_id']?>" class="backnine tee_sheet_holder">
                                <div id='calendarback-<?=$tee_sheet['teesheet_id']?>' class="calendarback" style='width:100%; display:none'></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                <?php
                }
            }
            else {
                if ($user_id) {?>
                <? } ?>
                <div class="tee_sheet_box">
                    <div class="content">
                        <div id="frontnine-<?=$current_teesheet?>" data-tee-sheet-id="<?=$this->session->userdata('teesheet_id')?>" class="frontnine tee_sheet_holder">
                            <div id='calendar-<?=$current_teesheet?>' class="calendar"></div>
                        </div>
                        <div id="backnine-<?=$current_teesheet?>" data-tee-sheet-id="<?=$this->session->userdata('teesheet_id')?>" class="backnine tee_sheet_holder">
                            <div id='calendarback-<?=$current_teesheet?>' class="calendarback" style='width:100%; display:none'></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            <?php } ?>
            <div class='clear'></div>
        </div>
    </div>
    <?php } ?>
</div>
<!-- Right Click Menu -->
<div class='contextMenu' id='myMenu' style='display:none'>
	<ul>
	    <li id="switch"><!--img src="../../images/pieces/switch.png" /-->Switch Sides</li>
        <li id="send_sms">Send SMS</li>
	    <li id="repeat">Repeat Event</li>
	    <li id="move">Change Day/Time</li>
	    <li id="split_by_time">Split by Time</li>
	    <li id="teed_off">Tee'd Off</li>
	    <li id="mark_turn">Mark the Turn</li>
	    <li id="mark_finished">Mark as Done</li>
    	<li id="check_in_1">Check In 1</li>
    	<li id="check_in_2">Check In 2</li>
    	<li id="check_in_3">Check In 3</li>
    	<li id="check_in_4">Check In 4</li>
    	<li id="check_in_5">Check In 5</li>
    	<li id="raincheck">Issue Rain Check</li>
    	<li id="resend_confirmation">Resend Confirmation</li>
        <li id="copy_event">Copy Event</li>
	</ul>
</div>
<div class='contextMenu' id='squeeze_menu' style='display:none'>
	<ul>
		<li id="add_squeeze">Add Squeeze</li>
        <li id="remove_squeeze">Remove Squeeze</li>
        <li id="highlight_tee_sheet">Highlight Tee Sheet</li>
        <li id="paste_event">Paste Event</li>
	</ul>
</div>
<div id="feedback_bar"></div>
<div id="teetime_info_bar"></div>
<div id="fb-root"></div>

<?php $this->load->view("partial/footer"); ?>
<script>

var teesheet = {
	teetime:{
		search:function(){
			$.ajax({
	           type: "POST",
	           url: "index.php/teesheets/teetime_search",
	           data: 'customer_id='+$('#teetime_search_id').val(),
	           success: function(response){
	           		$('#teetime_search_results').html(response.search_results);
	           },
	           dataType:'json'
	         });
		}
	}
}

var standby = {
	standby_time:{
		list_standby_golfers:function(){
			$.ajax({
	           type: "POST",
	           url: "index.php/teesheets/teetime_standby_get_list",
				headers: {"read_only_session":1},
	           data: '',
	           success: function(response){
	           		var standby_html = '';
                                if (response.length == 0)
                                {
                                    standby_html = '<div class=\'teetime_result\'>No Standby Golfers</div>';
                                }
                                else
                                {
                                    $('#teetime_standby_title').click();
                                }

                                //console.log('length: ' + response.length);

                                $.each(response, function(data){

                                            standby_html += "<div class='standby_entry' id='";
                                            standby_html += response[data].standby_id +
                                            "'><div class='teetime_result' id='" +
                                            response[data].standby_id + "'><div>" +
                                            response[data].name + ' </br>' +
                                            response[data].tee_sheet_title + ' </br>' +
                                            response[data].time + ' - ' +
                                            response[data].holes + ' Holes ' +

                                            response[data].players + ' Players' + "</div> </div>";

                                         standby_html += '</div>';

                                });

                                   //console.log(standby_html);


                                $('#teetime_populate_standby_list').html(standby_html);


                                //$( ".standby_entry" ).draggable({ containment: "#content_area_wrapper", scroll: false, revert:true, start: function(e) { $(e).css('z-index', 1000)} , stop: function(e) { $(e).css('z-index', 0)}  });
                                $('.standby_entry').draggable({zIndex:1000,
                                    helper:'clone', appendTo:'body',
                                    cursorAt: {left:-2, top:-2}
                              });

	           },
	           dataType:'json'
	         });
		}

	}

}
var standby_edit = {
    standby_golfer_edit:{
        edit_standby:function(){

        }
    }
}
var teetime_html = "<?php //echo (preg_replace('/\s+/',' ',str_replace(array(chr(10),chr(13),'\r\n','\t','\r','\n'), '', ($this->config->item('simulator')) ? $this->load->view("teetimes/form_simulator",$data,true) : $this->load->view("teetimes/form",$data,true))));?>";

	/*JOEL CREATE THE ARRAY HERE*/
	//FACEBOOK CODE
	window.my_config =
	{
		access_token : '',
		page_id : '',
		name : '',
		user_accounts : '',
		<?php if (base_url() == 'http://localhost:8888/') { ?>
		app_id : '317906091649413', // app_id for LOCALHOST testing only
		app_secret : '5a47cce8b870df193418cbdc1490913b', //app_secret for LOCALHOST testing only
		<?php } else if (base_url() == 'http://dev.foreupsoftware.com') { ?>
		app_id : '583489044999943', // App ID for foreUP Dev
		app_secret : '5f8b249b9473fd74b13814be106d45ad', //app_secret for foreUP Dev
		<?php } else if (base_url() == 'http://mobile.foreupsoftware.com') { ?>
		app_id : '398875546898011', // App ID for Mobile.foreUP Dev
		app_secret : '31d25e25007a63720f3f2e26fce3160a', //app_secret for Mobile.foreUP Dev
		<?php } else { ?>
		app_id : '411789792224848', // App ID for foreUP Master
		app_secret : 'f69cd64778c32cb7dda1c1ec6960e1b0', //app_secret for foreUP Master
		<?php } ?>
		extended_access_token : '',
		can_post_with_extended_access_token : false
	};

	var facebook_obj = {
		logout: function(){
			FB.logout(function(response){});
		}
	}

$(document).ready(function(){
	<?php if ($teesheet_year) { ?>
		$('.calendar').fullCalendar('gotoDate', <?=$teesheet_year?>, <?=$teesheet_month?>, <?=$teesheet_date?>)
	<?php } ?>
        standby.standby_time.list_standby_golfers();
		var teetime_search_name = $('#teetime_search_name');
		teetime_search_name.focus(function() { $(this).val('') });
		teetime_search_name.autocomplete({
			source: '<?php echo site_url("teesheets/customer_search/ln_and_pn"); ?>',
			delay: <?=$this->config->item("search_delay") ?>,
			autoFocus: false,
			minLength: 0,
			select: function(event, ui)
			{
				event.preventDefault();
				teetime_search_name.val(ui.item.label);
				$("#teetime_search_id").val(ui.item.value);
				teetime_search_name.blur();
				teesheet.teetime.search();
			}
		});

		$('#teesheetMenu').msDropDown(/*{blur:function(){changeTeeSheet()},click:alert('stuff')}*/)
		<?php if ($this->session->userdata('is_simulator')) { ?>
	    $('.calendar th.fc-col0').html('Sim 1');
	    $('.calendarback th.fc-col0').html('Sim 2');
		<?php } else { ?>
	    $('.calendar th.fc-col0').html('Front');
	    $('.calendarback th.fc-col0').html('Back');
		<?php } ?>

	/**********************************************************************************
	 **********************************FACEBOOK CODE***********************************
	 **********************************************************************************/
	function update_my_config()
	{

		window.my_config.page_id = '<?php echo $this->session->userdata('facebook_page_id'); ?>';
		window.my_config.name = '<?php echo $this->session->userdata('facebook_page_name'); ?>';
		window.my_config.extended_access_token = '<?php echo $this->session->userdata('facebook_extended_access_token'); ?>';
		if (window.my_config.extended_access_token !== '') {
			window.my_config.can_post_with_extended_access_token = true;
		}
		update_facebook_widget_appearance();
	}

	function update_facebook_widget_appearance()
	{
		if (window.my_config.can_post_with_extended_access_token)
		{
			$('.facebook_post').show();
			$('.facebook_login').hide();
			$('#facebook_logout').show();
			$('#message').show();
		} else {
			$('.facebook_post').hide();
			$('.facebook_login').show();
			$('#facebook_logout').hide();
			$('#message').hide();
		}
	}


	update_my_config();
    <?php if (!$this->config->item('disable_facebook_widget')) { ?>
	/*Load Facebook SDK*/
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : window.my_config.app_id,
	      channelUrl : '<?php echo site_url();?>', // Channel File
	      status     : true, // check login status
	      cookie     : true, // enable cookies to allow the server to access the session
	      xfbml      : true  // parse XFBML
	    });
	  };

	  (function(d){
	     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement('script'); js.id = id; js.async = true;
	     js.src = "https://connect.facebook.net/en_US/all.js";
	     ref.parentNode.insertBefore(js, ref);
	   }(document));


	$('.post').live('click', function(){
		if (window.my_config.can_post_with_extended_access_token)
		{
			post_facebook_message();
		}
		else
		{
			login();
		}
	});

	//EVERYTHING STARTS HERE
	 /********1********/
	$('.facebook_login').live('click', function(){
		login();
	});

	$('#facebook_logout').click(function(){
		//the user was already logged out of their official facebook session right after logging in. This method just clears their facebook settings from the database
		clear_facebook_settings();
	});
<?php } ?>
	/********2********/
	function login(){
		FB.login(function(response) {
	        if (response.authResponse) {
				//get an extended access token from facebook which will be stored in our database and used to make future posts
	            fetch_long_lived_access_token(response.authResponse.accessToken);
	        } else {
	            console.log("canceled");
	        }
		},{
            scope: 'manage_pages publish_actions', 
            return_scopes: true,
            auth_type: 'rerequest'
        });//http://stackoverflow.com/questions/8717388/the-user-hasnt-authorized-the-application-to-perform-this-action
	}

	function clear_facebook_settings()
	{
		var URL = '<?php echo site_url("teesheets/clear_facebook_settings");?>';
			$.post(URL, function(response) {
			    window.my_config.extended_access_token = '';
			    window.my_config.can_post_with_extended_access_token = false;
			    window.my_config.page_id = '';
				window.my_config.name = '';

				update_facebook_widget_appearance();
	    	});
	}

	/********3********/

	function fetch_long_lived_access_token(short_lived_access_token)
	{
		var URL;
		URL = "https://graph.facebook.com/oauth/access_token?client_id=" + window.my_config.app_id + "&client_secret=" + window.my_config.app_secret + "&grant_type=fb_exchange_token&fb_exchange_token=" + short_lived_access_token;

		$.post(URL, function(response) {

		    var token_elements = response.split('=');
		    extended_access_token = token_elements[1].split('&');
		    window.my_config.extended_access_token = extended_access_token[0];
		    window.my_config.can_post_with_extended_access_token = true;

			//post the facebook extended access token to server
			URL = '<?php echo site_url("teesheets/update_facebook_access_token");?>';
			data = {extended_access_token: window.my_config.extended_access_token};

			$.post(URL, data, function(response) {

		    });

		    //show a colorbox that will allow them to select which page is the golf course pages. NOTE: this pop up shows them all pages for which they are administrator
		    $.colorbox({'href':'index.php/teesheets/facebook_page_select/width~300', 'title':"Select the Course Page", 'overlayClose':false,
				onClosed: function(){
					update_facebook_widget_appearance();
				}
			});
	    });

	    return;
	}
	/********5********/
	function post_facebook_message(options)
	{
		var message, URL, success_message;
		message = $('#message').val();
        if(message == ''){
            alert('Enter Message');
        }else{
            URL = "https://graph.facebook.com/" + '' + "/feed?access_token=" + window.my_config.extended_access_token + "&message=" + message;

            $('.facebook_messages').mask("<?php echo lang('common_wait'); ?>");

            $.post(URL, function(response) {

            })
            .success(function(){
                $('.facebook_messages').unmask();
                $('#message').val('');

                success_message = "Successfully Posted to " + window.my_config.name + " Facebook Page";

                //functioned defined in other script
                set_feedback(success_message, 'success_message', false);
            })
            .error(function() {
                 $('.facebook_messages').unmask();
                 login();
            });
        }
		
	}
    
//    $('#teesheet_note_box').dialog({
//        dialogClass: 'note_dialog',
//        resizable: false
//    });
    $('#teeheet_daily_note').blur();
    $("#teeheet_daily_note").die();
    $('#teeheet_daily_note').live("keyup", function(event) {
        $('#teeheet_daily_note').css('height', 'auto');
        $('#teeheet_daily_note').css('height', $(this)[0].scrollHeight + 'px');
    });
    $('#teeheet_daily_note').live("focus", function(event) {
        var note_box = $('#teeheet_daily_note');
        if(!$('#teeheet_daily_note').hasClass('textExpand')){
            $('#teeheet_daily_note').addClass('textExpand');
            $('#teeheet_daily_note').css('height', 'auto');
            $(this).animate({ height: note_box[0].scrollHeight + 'px' }, 500);
        }
    });

    $('#teeheet_daily_note').live("blur", function(event) {
    	if($('#teeheet_daily_note').hasClass('textExpand')) {
            save_teesheet_note();       
            $('#teeheet_daily_note').removeClass('textExpand');
            $('#teeheet_daily_note').css('overflow-y;', 'hidden');
            $('#teeheet_daily_note').css('height', '21px');
            $(this).animate({ height: "1.2em" }, 500);
        }
    });
});
/* save and update teesheet note */
function save_teesheet_note(){
    var post_data = new Object;
    post_data.note_id = $('#teeheet_daily_note').attr('noteId');
    var day = $('#teesheet_day').text();
    var month = $('#teesheet_month').text();
    var date = day+" "+ month;
    post_data.date = $.datepicker.formatDate('yy-mm-dd', new Date(date));
    post_data.teesheet_id = '<?php echo $this->session->userdata('teesheet_id'); ?>';
    post_data.note = encodeURIComponent($('#teeheet_daily_note').val());
        $.ajax({
            type: "post",
            url: "index.php/teesheets/add_teesheet_note/",
            data: post_data,
            success: function(response){
                $('#teeheet_daily_note').attr('noteId',response.note_id);
            },
            dataType:'json'
        });
}
var previous_date = '';
function update_standbys(){
    var cal_date = $(".calendar").fullCalendar('getDate');
    var year = cal_date.getFullYear();
    var month = cal_date.getMonth()+1;
    var day = cal_date.getDate();
    if (previous_date == year+'-'+month+'-'+day) {
        return;
    }

    previous_date = year+'-'+month+'-'+day;

    $.ajax({
        type:"POST",
        url:"index.php/teesheets/teetime_standby_get_list/"+year+'-'+month+'-'+day,
        data: '',
        dataType:'json',
        success: function(response)
        {
            var standby_html = '';
            //Calendar_actions.update_teesheet(response.teesheets);
            $.each(response, function(data){
                standby_html += "<div class='standby_entry' id='";
                standby_html += response[data].standby_id +
                "'><div class='teetime_result' id='" +
                response[data].standby_id + "'><div>" +
                response[data].name + ' </br>' +
                response[data].tee_sheet_title + ' </br>' +
                response[data].time + ' - ' +
                response[data].holes + ' Holes ' +

                response[data].players + ' Players' + "</div> </div>";

                standby_html += '</div>';
            });
            //currently_editing = '';

            $('#teetime_populate_standby_list').html(standby_html);
            $('.standby_entry').draggable({zIndex:1000,
                helper:'clone', appendTo:'body',
                cursorAt: {left:-2, top:-2}
            });
        },
        dataType:'json'
    });
}
/* compare date to display teesheet note */
function teesheet_note(){
    var post_data = new Object;
    post_data.note_id = $('#teeheet_daily_note').attr('noteId');
    var day = $('#teesheet_day').text();
    var month = $('#teesheet_month').text();
    var date = day+" "+ month;
    post_data.date = $.datepicker.formatDate('yy-mm-dd', new Date(date));
    post_data.teesheet_id = '<?php echo $this->session->userdata('teesheet_id'); ?>';
    post_data.note = encodeURIComponent($('#teeheet_daily_note').val());
        $.ajax({
            type: "post",
            url: "index.php/teesheets/view_teesheet_note/",
            data: post_data,
            success: function(response){
               $('#teeheet_daily_note').attr('noteId',"");
                if(response[0] == undefined){
                    $('#teeheet_daily_note').val('');
                    $('#teeheet_daily_note').css({ boxShadow: 'inset 0px 6px 24px -12px #000' });
                }else{
                    $('#teeheet_daily_note').val(response[0].note);
                    $('#teeheet_daily_note').css({ boxShadow: 'inset -1px 3px 9px 0px rgb(34, 75, 138)' });
                    $('#teeheet_daily_note').attr('noteId',response[0].note_id);
                    
                }   
                
            },
            dataType:'json'
        });
}
</script>
<?php if ($this->config->item('print_after_sale') && !$this->config->item('webprnt') && false) { ?>
<div style='height:1px; width:1px; overflow: hidden'>
       <applet id='qz' name="qz" code="qz.PrintApplet.class" archive="<?php echo base_url()?>/qz-print.jar" width="100" height="100">
           <param name="printer" value="<?=$printer_name?>">
           <!-- <param name="sleep" value="200"> -->
       </applet>
</div>
<?php } ?>
<div  style="position:fixed; left:4000px;">
    <?php $this->load->view('teetimes/inline_form_js'); ?>
    <div id="hidden_tee_time_popup">
        <?php $this->load->view('teetimes/inline_form.php'); ?>
    </div>
</div>

