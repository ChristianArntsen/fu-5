<style>
    #event_people {
        display: block;
        overflow: hidden;
        width: auto;
        margin: 0px 10px 0px 0px;
        padding: 5px 2px 2px 2px;
    }

    #event_people > li {
        display: block;
        padding: 2px 0px 2px 4px;
        height: 30px;
        margin-bottom: 4px;
    }

    #event_people > li span {
        float: left;
        display: block;
        line-height: 30px;
        height: 30px;
        margin-right: 10px;
    }

    #event_people > li span.delete {
        height: 30px;
        line-height: 30px;
        margin-right: 10px;
        cursor: pointer;
    }

    #event_people > li span.name {
        width: 300px;
    }
    #event_people > li span.phone {
        width: 130px;
    }
    #event_people > li span.email {
        width: 250px;
    }
    #event_people > li span.status {
        width: 185px;
        float: right;
    }
    #event_save{
        /*margin-right: 610px;*/
    }
    #checkin_txtbox{
        width: 60px;
    }

    #event_people > li.paid {
        background: #f2faff;
        background: -moz-linear-gradient(top,  #f2faff 0%, #c0dcea 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f2faff), color-stop(100%,#c0dcea));
        background: -webkit-linear-gradient(top,  #f2faff 0%,#c0dcea 100%);
        background: -o-linear-gradient(top,  #f2faff 0%,#c0dcea 100%);
        background: -ms-linear-gradient(top,  #f2faff 0%,#c0dcea 100%);
        background: linear-gradient(to bottom,  #f2faff 0%,#c0dcea 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2faff', endColorstr='#c0dcea',GradientType=0 );
        border: 1px solid #B2D7E8;
        margin-left: -1px;
        margin-top: -1px;
    }

    a.pay, a.checkin {
        background: #9c9c9c;
        background: -moz-linear-gradient(top,  #9c9c9c 0%, #797979 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#9c9c9c), color-stop(100%,#797979));
        background: -webkit-linear-gradient(top,  #9c9c9c 0%,#797979 100%);
        background: -o-linear-gradient(top,  #9c9c9c 0%,#797979 100%);
        background: -ms-linear-gradient(top,  #9c9c9c 0%,#797979 100%);
        background: linear-gradient(to bottom,  #9c9c9c 0%,#797979 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9c9c9c', endColorstr='#797979',GradientType=0 );
        border: 1px solid #232323;
        color: #FFFFFF;
        cursor: pointer;
        display: block;
        font-size: 14px;
        font-weight: normal;
        height: 30px;
        line-height: 30px;
        margin: 0px;
        overflow: visible;
        padding: 0px 10px 0px 10px;
        position: relative;
        text-align: center;
        text-decoration: none !important;
        text-shadow: 0 -1px 0 #000000;
        float: left;
        border-radius: 5px;
    }

    span.count {
        display: inline-block;
        padding: 1px 4px;
        border-radius: 15px;
        background: #468BBA;
        color: white;
        width: 20px;
        text-align: center;
    }

    a.pay.done, a.checkin.done {
        background: #DDDDDD;
        border: 1px solid #BBBBBB;
        color: #666;
        text-shadow: none;
    }

    a.pay {
        margin-right: 10px;
        width: 40px;
    }

    a.checkin {
        width: 90px;
        text-align: left;
        margin-left: 75px;
        margin-top: -33px;
    }

    #event_people a.checkin {
        margin-left: 0px;
        margin-top: 0px;
    }
    .checkin_buttons{
        margin-top: -30px;
    }

    #event_checkin{
        border-radius:4px !important;
        background: #349ac5; /* for non-css3 browsers */
        background: -webkit-linear-gradient(top, #349AC5, #4173B3);
        background: -moz-linear-gradient(top,  #349AC5,  #4173B3); /* for firefox 3.6+ */
        color: white;
        display: block;
        font-size: 14px;
        font-weight: normal;
        text-align: center;
        text-shadow: 0px -1px 0px black;
        box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
        border: 1px solid #232323;
        margin-top:0px;
        margin-right: 20px;
    }

    .employee_details{
        background-color: none;
        color: #444;
        float: right;
        font-size: 12px;
        height: 22px;
        margin-right: 30px;
        margin-top: -10px;
        opacity: 0.8;
        padding-left: 18px;
        padding-top: 15px;
        width: 250px;
    }

    @media screen and (-webkit-min-device-pixel-ratio:0) {
        #event_checkin{

        }
    }
    #loaded_customer_name{
        padding-right: 16px;
    }
    #customer_form .error {
        background: none;
        border: none;
        float: none;
        font:20px;
        margin:auto;
        padding: auto;
    }
    .saveButtons, #checkin_txtbox {
        float:right;
    }
    .carts_buttonset label.ui-button {
        padding:0px 5px 2px;
    }
    .holes_buttonset label.ui-button {
        padding:0px 5px 6px;
    }
    #zip {
        margin-right:5px;
        float:none;
    }
    #add_teetime_credit_card {
        line-height:23px;
        font-size:12px;
        width:56px;
    }
    select#teetime_rate_1 {
        font-size: 14px;
        font-weight: lighter;
        font-family: "Quicksand",Helvetica,sans-serif;
        height: 32px;
        -webkit-appearance: menulist;
        box-sizing: border-box;
        align-items: center;
        border: 1px solid;
        border-image-source: initial;
        border-image-slice: initial;
        border-image-width: initial;
        border-image-outset: initial;
        border-image-repeat: initial;
        white-space: pre;
        -webkit-rtl-ordering: logical;
        color: black;
        background-color: white;
        cursor: default;
    }
    span.carts_buttonset {
        width:290px;
    }
    span.flag_icon {
        margin-left: 0px;
        width: 44px;
        background-position: 4px -162px;
    }
    #colorbox select {
        font-size:16px;
    }
    #colorbox input {
        margin-right:5px;
    }
    #credit_card_id {
        margin-top:5px;
    }
    #charge_for_tee_time {
        font-size:10px;
        cursor:pointer;
    }
    .search_email{
        font-size: 12px;
        display:block;
    }
    .search_phone_number{
        font-size: 12px;
        display:block;
    }
</style>