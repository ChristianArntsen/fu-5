<div id='terminal_selector'>
	<h2>Please select your current station:</h2>
	<?php foreach($terminals as $terminal){ ?>
	<div id='tid_<?php echo $terminal->terminal_id; ?>' class='terminal_button' style="min-height: 34px; width: auto; display: block; line-height: 34px; height: auto;">
		<div><?php echo $terminal->label; ?></div>
		<?php if($terminal->active_log){ ?>
		<div style="line-height: 16px">
			<span>DRAWER <?php echo $terminal->active_log['drawer_number']; ?> IN USE</span>
			by <?php echo $terminal->active_log_employee['first_name'].' '.$terminal->active_log_employee['last_name']; ?>
			<br /><?php echo date('m/d/Y h:ia', strtotime($terminal->active_log['shift_start'])); ?>
		</div>
		<?php } ?>
	</div>
	<?php } ?>
	<div id='tid_0' class='terminal_button' style="min-height: 34px; width: auto; display: block; line-height: 34px; height: auto;">Other</div>
</div>
<script>
	$(document).ready(function(){
		$.colorbox.resize();
		$('.terminal_button').click(function(){
			var terminal_id = $(this).attr('id').replace('tid_', '');
			$.ajax({
	           type: "POST",
	           url: "index.php/home/set_terminal/"+terminal_id,
	           data: '',
	           success: function(response){	
	           		$('#page-header-content').find('h3').html(response.terminal.label);
	           		$.colorbox.close();
			    },
	            dataType:'json'
	        });
		});
	})
</script>