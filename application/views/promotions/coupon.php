
<?php
function get_valid_days($Sunday, $Monday, $Tuesday, $Wednesday, $Thursday, $Friday, $Saturday)
{
    $length = $Sunday + $Monday + $Tuesday + $Wednesday + $Thursday + $Friday + $Saturday;
    $valid_array = array();
    
    if ($length > 0 && $length < 7)
    {
        $days['Sunday'] = $Sunday;
        $days['Monday'] = $Monday;
        $days['Tuesday'] = $Tuesday;
        $days['Wednesday'] = $Wednesday;
        $days['Thursday'] = $Thursday;
        $days['Friday'] = $Friday;
        $days['Saturday'] = $Saturday;
        
        foreach ($days as $day=>$value)
        {
            if ($value == 1)
            {
                //log_message('error', "VALID: $day:$value");
                $valid_array[] = $day;
            }
        }
    }
    return $valid_array;
}
    $footer_text = '';
    
    $days_promo = get_valid_days($Sunday, $Monday, $Tuesday, $Wednesday, $Thursday, $Friday, $Saturday);
    $days_promo_length = count($days_promo);
    //log_message('error', "PROMO: {$days_promo[0]}");
    if ($min_purchase > 0 && $bogo !== 'bogo')
    {
        $footer_text = 'With a minimum purchase of: $' . $min_purchase . '</br>';
    }
    if ($days_promo_length > 0)
    {
        $footer_text .= ' Valid on: ';
        if ($days_promo_length > 2)
        {
            for ($i = 0; $i < $days_promo_length; $i++)
            {
                if ($i < $days_promo_length - 1)
                {
                    $footer_text .= $days_promo[$i] . ', ';
                }
                else
                {
                    $footer_text .= 'And ' . $days_promo[$i];
                }
            }
        }
        else if ($days_promo_length == 2)
        {
            $footer_text .= $days_promo[0] . ' And ' . $days_promo[1];
        }
        else if ($days_promo_length == 1)
        {
            $footer_text .= $days_promo[0];
        }
        $footer_text .= '</br>';
    }
    if ($valid_from !== '' && $valid_to !== '')
    {
        $footer_text .= ' Valid Between: ' . $valid_from . ' and ' . $valid_to;
    }
?>

<div id="coupon-wrapper" >
    <table style="width: 600px;
    height: auto;
    margin: 15px auto 15px;
    min-height: 200px;
    border: 4px dashed #000;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    background:#ffffff url(<?= $this->Appconfig->get_logo_image('', $course_id); ?>) no-repeat top left;
    text-align:center;">
      <tr style="height:20px;">
        <td colspan="4">&nbsp;</td>
      </tr>
      <tr>
        <td class="spacer">&nbsp;</td>
        <td>&nbsp;</td>
        <td class="spacer">&nbsp;</td>
        <td><h4><?php echo $course; ?></h4></td>
      </tr>
      <tr style="height:20px;">
        <td colspan="4">&nbsp;</td>
      </tr>
      <tr>
        <td class="spacer">&nbsp;</td>
        <td class="expires">Expires: <span id="expiration-date"><?php echo ($expiration_date !== '4000-01-01'?date('m-d-Y', strtotime($expiration_date)):'N/A');?></span></td>
        <td class="spacer">&nbsp;</td>
        <td class="promotion-buy_and_get"><h3>&nbsp;</h3></td>
      </tr><?php
            if ($promotion_id == -1)
            {
                log_message('error', 'ERROR! there isn\'t a promotion id in coupon.php!');
            }
            $number_of_zeros = 10 - strlen($promotion_id);
            for($i = 0; $i < $number_of_zeros; $i++)
            {
                $promotion_id = '0' . $promotion_id;
            }
            log_message('error', 'UPC ID: ' . $promotion_id);
      ?>
      <tr>
        <td class="spacer">&nbsp;</td>
        <td class="expires"><div id="UPC"><?= "<img id='BC' src='".site_url('barcode')."?barcode=CP$discount_type{$promotion_id}&text=CP$discount_type{$promotion_id}&scale=1' />" ?>
        <?php echo form_hidden('send_BC',site_url('barcode')."?barcode=CP$discount_type{$promotion_id}&text=CP$discount_type{$promotion_id}&scale=1' />");?></div>
        </td>
        
        <td class="spacer">&nbsp;</td>
        <td class="promotion-text"><h1><? 
        if ($bogo === 'discount')
        {
            if ($amount_type === '%')
            {
                echo $amount;
                echo $amount_type . ' off';
            }
            else
                echo $amount_type . $amount . ' off';
        }
        else if ($amount_type === '%')
        {
            echo "Buy $buy_quantity Get $get_quantity $amount$amount_type off";
        }
        else
        {
            echo "Buy $buy_quantity Get $get_quantity Free";
        }
?> </h1><p><?php
            switch ($discount_type)
            {
                case 0:
                    echo 'of anything in the ' . $category_type . ' category';
                    break;
                case 1:
                    echo 'of anything in the ' . $subcategory_type . ' subcategory';
                    break;
                case 2:
                    if ($bogo === 'discount')
                    {
                        echo "of one $item_name item";
                    }
                    else
                    {
                        echo "Good for $item_name items";
                    }
                        
                    break;
                case 3:
                    echo 'of anything in the ' . $department_type . ' department';
                    break;
                default:
                    echo "unknown coupon";
                    break;
                  
            }
            ?></p></td>
        
      </tr>
      <tr style="height:40px;">
        <td colspan="4">&nbsp;</td>
      </tr>
      <tr style="height:20px;">
        <td class="promotion-footer" colspan="4">
          <span><?php echo $footer_text; ?></span>
        </td>
      </tr>
    </table>
  </div>
<?php

/*
 * echo "<img id='BC' src='".site_url('barcode')."?barcode=CP{$promotion_info->discount_type}{$promotion_id}41&text=CP{$promotion_info->discount_type}{$promotion_id}41&scale=1' /></td>";
        echo form_hidden('send_BC',site_url('barcode')."?barcode=CP{$promotion_info->discount_type}{$promotion_id}41&text=CP{$promotion_info->discount_type}{$promotion_id}41&scale=1' />");
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
