<style type="text/css">
  /* css overrides*/
  .label-override {
      width: 103px !important;
  }
  .label-override-long {
      width: 200px !important;
  }

  .label-override-dual {
      width: 52px !important;
      margin-left: 15px;
  }
  .radio-override{
    float:right;
    text-align: left;
    margin-right: 45px;
    margin-top: -5px;
  }
  ul.promotion-amount-type{
    list-style: none;    
  }
  ul.promotion-amount-type li{
    float:left;
    color:red;
    margin-left: 20px;
    margin-top: 4px;
  }
  #promotion-form-left{width:385px;float:left;margin-left: 20px}
  #promotion-form-right{width:555px;float:right;}
  .wysiwyg-title-iframe{height: 52px !important;}
  .wysiwyg-contents-iframe{min-height: 150px !important;height: 150px !important;}  
  #promotion_content{min-height:150px !important;}
  #text-counter{font-size:11px; color:#67696b;}
  .label-override-content {
      width: 290px !important;
  }

  #coupon-wrapper{
    width: 500px;
    height: auto;
    margin-top: 150px;
    min-height: 200px;
    border: 4px dashed #000;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    background: url(<?= $this->Appconfig->get_logo_image(); ?>) no-repeat top left;
    text-align:center;
  }
  
  td.spacer{width:20px;}
  td.expires{font-size:12px;}
  td.promotion-text h1{font-size: 44px;line-height: 33px;}
  td.promotion-text p{font-size: 12px;}
  td.promotion-footer{font-size: 11px;text-align:left;padding-left:10px;padding-bottom:10px;}
</style>
<?= form_open('promotions/save/'.$promotion_info->id,array('id'=>'promotion_form')); ?>

<fieldset id="promotion_basic_info">
<legend><?= lang("promotions_basic_information"); ?></legend>
<div id="promotion-form-left">  
<div class="field_row clearfix">
<?= form_label(lang('promotions_name').':<span class="required">*</span>', 'name',array('class'=>' wide label-override')); ?>
	<div class='form_field'>
	<?= form_input(array(
		'name'=>'promotion_name',
		'size'=>'25',
                'style' => 'width: 239px;',
		'id'=>'promotion_name',
		'value'=> $promotion_info->name)
	);?>
	</div>
</div>
    <div class='field_row clearfix'>
    <?= form_label('Type: ', 'name');?>

    <div class='form_field'>
	 	<div class='holes_buttonset'>
	        <input type='radio' class="coupon" id='teetime_holes_9' name='coupon' value='discount' <?php echo ($promotion_info->bogo !== 'bogo'? 'checked': '');?>/>
	        <label for='teetime_holes_9'  id='teetime_holes_9_label'>Discount</label>
	        <input type='radio' class="coupon" id='teetime_holes_18' name='coupon' value='bogo' <?php echo ($promotion_info->bogo === 'bogo'? 'checked': '');?>/>
	        <label for='teetime_holes_18' id='teetime_holes_18_label'>BOGO</label>		
                    </div>
    </div>
</div>
<div class="field_row clearfix">
	<label for="promo_online_code">Online Code</label>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'online_code',
        'style' => 'width: 150px;',
		'id'=>'promo_online_code',
		'value'=> $promotion_info->online_code)
	);?>
	</div>
</div>
   
    <div id="regular_discount">
    <div class='field_row clearfix'>
<?= form_label(lang('promotions_amount').':<span class="required">*</span>', 'name',array('class'=>' wide label-override')); ?>
	<div class='form_field'>
	<?= form_input(array(
		'name'=>'promotion_amount',
		'size'=>'25',
                'style'=>'width: 115px;float:left;margin-right: 10px;',
		'id'=>'promotion_amount',
		'value'=>$promotion_info->amount)
	);?>
  <ul class="promotion-amount-type">
    <li>$
        
      <?php 
            if ($promotion_info->amount_type === '%')
            {
                $dollar_amount_type = FALSE;
                $percentage_amount_type = TRUE;
            }
            else
            {
                $dollar_amount_type = TRUE;
                $percentage_amount_type = FALSE;
            }
            $data = array(
            'name'        => 'promotion_amount_type',
            'id'          => 'promotion_amount_type_dollar',
            'value'       => '$',
            'checked'     => $dollar_amount_type
            );

        echo form_radio($data);
      ?>            
    </li>
    <li>%
      <?php $data = array(
            'name'        => 'promotion_amount_type',
            'id'          => 'promotion_amount_type_pct',
            'value'       => '%',
            'checked'     => $percentage_amount_type
            );

        echo form_radio($data);
      ?>
    </li>
  </ul>
	</div>
    </div>
    <div class="field_row clearfix">
  <?= form_label('Minimum Purchase Amount:', 'name',array('class'=>'wide label-override-long')); ?>
    <div class='form_field'>
    <?= form_input(array(
      'name'=>'promotion_min_purchase',
      'size'=>'25',
      'style' => 'width:142px;',
      'id'=>'promotion_min_purchase',
      'value'=> $promotion_info->min_purchase)
    );?>
    </div>
  </div>
  </div>
    <div id="bogo_discount">
  <div class="field_row clearfix">
    <?= form_label(lang('promotions_buy').':', 'name',array('class'=>'wide label-override')); ?>
    <div class='form_field' style="float:left;">
    <?= form_input(array(
      'name'=>'promotion_buy',
      'size'=>'10',
      'style' => 'width: 10px;',
      'id'=>'promotion_buy',
      'value'=> $promotion_info->buy_quantity)
    );?>
    </div>
    <?= form_label(lang('promotions_get').':', 'name',array('class'=>'label-override-dual')); ?>
    
    <?= form_input(array(
      'name'=>'promotion_get',
      'size'=>'10',
      'style' => 'width: 10px;',
      'id'=>'promotion_get',
      'value'=> $promotion_info->get_quantity)
    );?>
    
      
     <?php 
            if ($promotion_info->amount_type === '%')
            {
                $dollar_bogo_amount_type = FALSE;
                $percentage_bogo_amount_type = TRUE;
            }
            else
            {
                $dollar_bogo_amount_type = TRUE;
                $percentage_bogo_amount_type = FALSE;
            }
            $data = array(
            'name'        => 'choose_free_or_percent',
            'id'          => 'buy_get_percent_off',
            'value'       => 'buy_get_percent_off',
            'checked'     => $percentage_bogo_amount_type
            );

        echo form_radio($data);
       
         ?>
    
    
    <?= form_input(array(
      'name'=>'promotion_bogo_discount',
      'size'=>'10',
      'style' => 'width: 25px;',
      'id'=>'percentage',
      'value'=> $promotion_info->amount)
    );?>% Off
        <?php $data = array(
            'name'        => 'choose_free_or_percent',
            'id'          => 'buy_get_free',
            'value'       => 'buy_get_free',
            'checked'     => $dollar_bogo_amount_type
            );

        echo form_radio($data);
      ?>Free
    
  </div>
    </div>
    <div id="date_and_times">
 <div class="field_row clearfix">
 <?= form_label(lang('promotions_expiration_date') . ':', 'name',array('class'=>' wide label-override')); ?>
	<div class='form_field'>
	<?= form_input(array(
		'name'=>'promotion_expiration_date',
		'size'=>'25',
                'style' => 'width: 239px;',
		'id'=>'promotion_expiration_date',
		'value'=> ($promotion_info->expiration_date === '4000-01-01'?'':$promotion_info->expiration_date))
	);?>
	</div>
</div>


<div class="field_row clearfix">
<?= form_label(lang('promotions_valid_on').':', 'name',array('class'=>'wide label-override')); ?>
	<div class='form_field'>
            
            <?php 
            echo form_checkbox(array(
                    'name'=>'valid_promotion_days[]',
                    'id'=>'valid_promotion_day',
                    'value'=>'Sunday',
                    'checked'=>($promotion_info->new?1:$promotion_info->Sunday)));
           echo ' Sunday ';
            echo form_checkbox(array(
                    'name'=>'valid_promotion_days[]',
                    'id'=>'valid_promotion_day',
                    'value'=>'Monday',
                    'checked'=>($promotion_info->new?1:$promotion_info->Monday)));
            echo ' Monday ';
            echo form_checkbox(array(
                    'name'=>'valid_promotion_days[]',
                    'id'=>'valid_promotion_day',
                    'value'=>'Tuesday',
                    'checked'=>($promotion_info->new?1:$promotion_info->Tuesday)));
            echo ' Tuesday ';
            echo form_checkbox(array(
                    'name'=>'valid_promotion_days[]',
                    'id'=>'valid_promotion_day',
                    'value'=>'Wednesday',
                    'checked'=>($promotion_info->new?1:$promotion_info->Wednesday)));
            echo ' Wednesday ';
            echo form_checkbox(array(
                    'name'=>'valid_promotion_days[]',
                    'id'=>'valid_promotion_day',
                    'value'=>'Thursday',
                    'checked'=>($promotion_info->new?1:$promotion_info->Thursday)));
            echo ' Thursday ';
            echo form_checkbox(array(
                    'name'=>'valid_promotion_days[]',
                    'id'=>'valid_promotion_day',
                    'value'=>'Friday',
                    'checked'=>($promotion_info->new?1:$promotion_info->Friday)));
            echo ' Friday ';
            echo form_checkbox(array(
                    'name'=>'valid_promotion_days[]',
                    'id'=>'valid_promotion_day',
                    'value'=>'Saturday',
                    'checked'=>($promotion_info->new?1:$promotion_info->Saturday)));
            echo ' Saturday ';
            ?>
            
		</div>
</div>

<div class="field_row clearfix">
<?= form_label(lang('promotions_valid_between').':', 'name',array('class'=>'wide label-override')); ?>
	<div class='form_field'>
		<?= form_dropdown('promotions_valid_between_start', $range, $promotion_info->valid_between_from, 'id="promotions_valid_between_start"  style="width:123px;"');?>
		<?= form_dropdown('promotions_valid_between_end', $range, $promotion_info->valid_between_to, 'id="promotions_valid_between_end"  style="width:123px;"');?>
	</div>
</div>

    </div>

  

  

  <div class="field_row clearfix">
  <?php echo form_label(lang('promotions_details').':', 'name',array('class'=>'wide label-override-long')); ?>
    <div class='form_field'>
    <?php echo form_textarea(array(
      'name'=>'promotion_additional_details',
      'size'=>'25',
      'rows'  => 2,
      'cols'  => 25,
      'style' => 'width: 352px;',
      'id'=>'promotion_additional_details',
      'value'=>$promotion_info->additional_details)
    );?>
    </div>
  </div>
 
<?= form_submit(array(
	 'name'=>'submitButton',
  'id'=>'submitButton',
	'value'=> lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
     </div>
</div>
<div id="promotion-form-right">
    <div class="field_row clearfix">
<?= form_label(lang('promotions_valid_for').':<span class="required">*</span>', 'name',array('class'=>' wide label-override')); ?>
	<div class='form_field'>
	<?= form_input(array(
		'name'=>'promotion_valid_for',
		'size'=>'25',
    'style' => 'width: 239px;',
		'id'=>'promotion_valid_for',
		'value'=> $promotion_info->valid_for)
	);?>
	</div>
</div>
    <div id='players' class='player_buttons'>
        	
	 		<!--<div class='players_buttonset'>
    			<input type='radio' id='players_1' name='limit' value='1' checked/>
    			<label for='players_1' id='players_1_label'>One Use Per Coupon</label>
    			<input type='radio' id='players_2' name='limit' value='2'/>
    			<label for='players_2' id='players_2_label'>Unlimited</label>   			
                        </div>  ADD THIS CODE WHEN LIMITED USE COUPONS ARE AVAILABLE--> 
                        <input type="hidden" name="limit" value="2"/>
   </div>
  <div id="coupon-wrapper">
    <table border="0">
      <tr style="height:20px;">
        <td colspan="4">&nbsp;</td>
      </tr>
      <tr>
        <td class="spacer">&nbsp;</td>
        <td>&nbsp;</td>
        <td class="spacer">&nbsp;</td>
        <td><h4><?= $course ?></h4></td>
      </tr>
      <tr style="height:20px;">
        <td colspan="4">&nbsp;</td>
      </tr>
      <tr>
        <td class="spacer">&nbsp;</td>
        <td class="expires">Expires: <span id="expiration-date">N/A</span></td>
        <td class="spacer">&nbsp;</td>
        <td class="promotion-buy_and_get"><h3>&nbsp;</h3></td>
      </tr><?php
            $promotion_id = -1;
            if ($promotion_info->id == -1)
            {
                $promotion_id = $promotion_info->approximate_id;
            }
            else
            {
                $promotion_id = $promotion_info->id;
            }
            $number_of_zeros = 10 - strlen($promotion_id);
            for($i = 0; $i < $number_of_zeros; $i++)
            {
                $promotion_id = '0' . $promotion_id;
            }
      ?>
      <tr>
        <td class="spacer">&nbsp;</td>
        <td class="expires"><?= "<img id='BC' src='".site_url('barcode')."?barcode=CP{$promotion_info->discount_type}{$promotion_id}&text=CP{$promotion_info->discount_type}{$promotion_id}&scale=2' />" ?></td>
        <?php echo form_hidden('send_BC',site_url('barcode')."?barcode=CP{$promotion_info->discount_type}{$promotion_id}&text=CP{$promotion_info->discount_type}{$promotion_id}&scale=2' />");?>
        <?php echo form_hidden('item_id', "$promotion_info->item_id");?>
        <?php echo form_hidden('discount_type', "$promotion_info->discount_type");?>
        <?php echo form_hidden('category_type', "$promotion_info->category_type");?>
        <?php echo form_hidden('subcategory_type', "$promotion_info->subcategory_type");?>
        <?php echo form_hidden('department_type', "$promotion_info->department_type");?>
        <td class="spacer">&nbsp;</td>
        <td class="promotion-text"><h1>50% off</h1><p>of anything in the Pro Shop</p></td>
      </tr>
      <tr style="height:40px;">
        <td colspan="4">&nbsp;</td>
      </tr>
      <tr style="height:20px;">
        <td class="promotion-footer" colspan="4">
          <span>&nbsp;</span>
        </td>
      </tr>
    </table>
  </div>
</div>
<div class="clearfix"></div>
</fieldset>
<?= form_close(); ?>

<script type="text/javascript">
    $('#date_and_times').expandable({title:'Date/Times:'});
    $('#date_and_times').expandable('open');
    
$(document).ready(function(){
  add_autocomplete();
  $("input[name=valid_promotion_days\\[\\]]").click(function() {
      update_coupon_footer();
  });
  $('#promotion_expiration_date').datepicker({dateFormat:'yy-mm-dd', minDate: new Date()});
  $('#promotion_expiration_date').change(function(){
    $('#expiration-date').html($(this).val());
    
  });
   $('.holes_buttonset').buttonset();
   $('.players_buttonset').buttonset();
   
   if ($('input[name=coupon]:checked').val() === 'discount')
   {
       $('#bogo_discount').hide();
       
   }
   else
   {
       $('#regular_discount').hide();
   }
   $('.coupon').click(function(){
         if ($(this).val() === 'discount')
         {
             $('#bogo_discount').hide();
             $('#regular_discount').show();
             
         }
         else
         {
             $('#regular_discount').hide();
             $('#bogo_discount').show();
         }
         console.log($(this).val());
    });
  function update_coupon_bogo_or_discount(){
      var selection = $('input[name=coupon]:checked').val();
      if (selection === 'bogo')
      {
          
          $('#promotion_get').val('1');
        $('#promotion_buy').val('1');
          var text = '';
          update_buy_n_get();
          var vos = $('select[name=promotions_valid_on_start] option:selected').val();
          var voe = $('select[name=promotions_valid_on_end] option:selected').val();
          var vbs = $('select[name=promotions_valid_between_start] option:selected').val();
          if(vbs=='anytime') vbs='close';
          var vbe = $('select[name=promotions_valid_between_end] option:selected').val();
          if(vbe=='anytime') vbe='close';
          var ad  = $('#promotion_additional_details').val();
          if(vos.length > 0 && voe.length > 0) text += ' Valid '+ ucwords(vos) + ' thru ' + ucwords(voe);
          if(vbs.length > 0 && vbe.length > 0) text += ' ' + vbs + ' thru ' + vbe + '. ';
          if(ad.length > 0) text = text + ad;

            text.trim();
            $('.promotion-footer span').html(text);
          
         
          updateBOGO();
          
      }
      else
      {
          
        
        $('#promotion_get').val('0');
        $('#promotion_buy').val('0');
                         
        var t = '';
        $('.promotion-buy_and_get h3').html(t);
        update_coupon_footer();
        updateH();
      }
  }
  function updateH(){
      
    var selection = $('input[name=coupon]:checked').val();
      if (selection === 'discount')
      {
          var sym = $("input[name=promotion_amount_type]:checked").val();
          console.log("SYMBOL: " + sym);
          var t    = (sym == '$') ? sym + $('#promotion_amount').val() : $('#promotion_amount').val() + sym;
          var text = t + ' off';
          if(t == '100%') text = 'FREE';
          $('.promotion-text h1').html(text);
      }
  }
  function updateBOGO(){
     var selection = $('input[name=coupon]:checked').val();
      if (selection === 'bogo')
      {
        var sym = $("input[name=choose_free_or_percent]:checked").val();
        var percent_value = $("#percentage").val();
        var text = (sym === 'buy_get_free') ? 'FREE': percent_value + '% OFF';
       
        $('.promotion-text h1').html(text);
      }
  }
  $("input:radio[name=choose_free_or_percent]").click(function() {
    updateBOGO();
  });
  
  $('#percentage').focus(function(){    
      $('input:radio[name=choose_free_or_percent]')[0].checked = true;
      updateBOGO();
  });
  $('#percentage').keyup(updateBOGO);
  $('#promotion_amount').keyup(updateH);
  $('#promotion_amount').blur(updateH);
  $("input[name=promotion_amount_type]").change(updateH);

  function pvf(){
    $('.promotion-text p').html($(this).val());
  }
  $('#promotion_valid_for').keyup(pvf);
  $('#promotion_valid_for').blur(pvf);

  function update_buy_n_get()
  {
    var b = $('#promotion_buy').val();
    var g = $('#promotion_get').val();
    var t = '';

    if(b.length > 0) t = t + 'Buy ' + b;
    if(g.length > 0) t = t + ' Get ' + g;

    $('.promotion-buy_and_get h3').html(t);
  }

  $('#promotion_buy').keyup(update_buy_n_get);
  $('#promotion_get').keyup(update_buy_n_get);
  $('#promotion_buy').blur(update_buy_n_get);
  $('#promotion_get').blur(update_buy_n_get);
  

  function ucwords (str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}

function add_autocomplete()
{
		console.log('adding autocomplete');
		$( "#promotion_valid_for" ).autocomplete({
			source: "<?php echo site_url('config/suggest_dept_cat_item');?>",
			delay: <?=$this->config->item("search_delay") ?>,
			autoFocus: false,
			minLength: 1,
			select: function(event, ui)
			{
                                event.preventDefault();
                                
				//event.preventDefault();
				var loyalty_filter = $(this);
				console.log('item type: ' + (ui.item.type));
				loyalty_filter.val(ui.item.label);
                                
                                var type = ui.item.type;
                                if (type === 'category')
                                {

                                    $('#category_type').val(ui.item.value);
                                    $('#subcategory_type, #department_type, #item_id').val('');
                                    type = '0';
                                }
                                else if (type === 'subcategory')
                                {
                                    $('#subcategory_type').val(ui.item.value);
                                    $('#category_type, #department_type, #item_id').val('');
                                    type = '1';    
                                }
                                else if (type === 'item')
                                {
                                    type = '2';
                                    $('#item_id').val(ui.item.value);
                                    $('#category_type, #department_type, #subcategory_type').val('');
                                }
                                else if (type === 'department')
                                {
                                    $('#department_type').val(ui.item.value);
                                    $('#category_type, #item_id, #subcategory_type').val('');
                                    type = '3';
                                }
                                else
                                {
                                    
                                    alert('Unknown type.  Please notify us of the choice you made.')
                                    type = '4';
                                }
                                $('#discount_type').val(type);
                                $('#BC').attr('src', '<?php echo site_url('barcode')."?barcode=CP";?>' + type + '<?php echo "$promotion_id&text=CP";?>' + '' + '<?php echo "$promotion_id&scale=1"; ?>');
                                $('#send_BC').val('<?php echo site_url('barcode')."?barcode=CP";?>' + type + '<?php echo "$promotion_id&text=CP";?>' + '' + '<?php echo "$promotion_id&scale=1"; ?>');
                                $('#promotion_valid_for').blur();
					//$("#customer").attr('value',"<?php echo lang('sales_start_typing_customer_name'); ?>");
//	      		cust.val('');
	      		//<input type="checkbox" name="campaign_group[]" value="0">
	//      		ui.item.is_group;
			}
		});
}
  function update_coupon_footer()
  {
      
    //console.log("I am here");
    var m = $('#promotion_min_purchase').val();    
    if(m=='') m = '';
    var days = $("input[name=valid_promotion_days\\[\\]]:checked");
    //var vos = $('select[name=promotions_valid_on_start] option:selected').val();
    //var voe = $('select[name=promotions_valid_on_end] option:selected').val();
    var vbs = $('select[name=promotions_valid_between_start] option:selected').val();
    if(vbs=='anytime') vbs='close';
    var vbe = $('select[name=promotions_valid_between_end] option:selected').val();
    if(vbe=='anytime') vbe='close';
    
    
    var ad  = $('#promotion_additional_details').val();
    var text = '';
    
    if (m.length > 0)
    {
        console.log('promotion mins: ' + m);   
        text = 'With a minimum purchase of: $' + m;
    }
    if (days.length > 0 && days.length < 7)
    {
        //console.dir(days);
        text += ' Offer Available on: ';
        
            for (var i = 0; i < days.length; i++)
            {
                if (i < days.length - 1 && days.length > 2)
                {
                    text +=  days[i].attributes.value.nodeValue + ', ';       
                }
                else if (days.length > 2)
                {
                    text += 'and ' + days[i].attributes.value.nodeValue;
                }
                else if (days.length == 2)
                {
                    text += days[i].attributes.value.nodeValue + ' and ' + days[++i].attributes.value.nodeValue;
                }   
                else
                {
                    text += days[i].attributes.value.nodeValue;
                }    
                    
            }
        
        text += ' ';
        
    }        
    //if(vos.length > 0 && voe.length > 0) text += ' Valid '+ ucwords(vos) + ' thru ' + ucwords(voe);
    if(vbs.length > 0 && vbe.length > 0) text += ' ' + vbs + ' thru ' + vbe + '. ';
    if(ad.length > 0) text = text + ad;
//     = m + ' Limit ' + l + '. Valid ' + ucwords(vos) + ' thru ' + ucwords(voe) + ' between ';
//    text = text + vbs + ' and ' + vbe + '. ' + ad;
    text.trim();
    $('.promotion-footer span').html(text);
  }

  $('.coupon').change(update_coupon_bogo_or_discount);
  $('#promotions_valid_on_start').change(update_coupon_footer);
  $('#promotions_valid_on_end').change(update_coupon_footer);
  $('#promotions_valid_between_start').change(update_coupon_footer);
  $('#promotions_valid_between_end').change(update_coupon_footer);
  $('#promotion_min_purchase').keyup(update_coupon_footer);
  $('#promotion_additional_details').keyup(update_coupon_footer);


  /***********************************************************************
     * repopulate coupon image template on the right during update
     ***********************************************************************/
    var promotion_id = '<?= $promotion_info->id; ?>';

    if(promotion_id)
    {
      $('#promotion_expiration_date').trigger('change');
      $('#promotion_amount').trigger('keyup');
      $("input[name=promotion_amount_type]").trigger('change');
      $('#promotion_valid_for').trigger('keyup');
      
      $('#promotion_buy').trigger('keyup');
      $('#promotion_get').trigger('keyup');
      $('#promotion_bogo_discount').trigger('keyup');
     
      $('#promotions_valid_on_start').trigger('change');
      $('#promotions_valid_on_end').trigger('change');
      $('#promotions_valid_between_start').trigger('change');
      $('#promotions_valid_between_end').trigger('change');
      $('#promotion_min_purchase').trigger('keyup');
      $('#promotion_additional_details').trigger('keyup');
      $('#percentage').trigger('keyup');
      $.colorbox.resize();
    }
  /***********************************************************************
     * eo populating
     ***********************************************************************/

  /***********************************************************************
     * submit handler
     ***********************************************************************/

    var submitting = false;
    $('#promotion_form').validate({
        
                
		submitHandler:function(form)
		{
                    if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
                    success:function(response)
                    {
                        console.log("I HAVE A SUCCESSFUL submission");
                        $.colorbox.close();
                        post_promotion_submit(response);
                        submitting = false;
                    },
                    dataType:'json'
                    });

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			promotion_name:
			{
				required:true
			},
			promotion_amount:
			{                     
				required:function (){
                                   
                                   var selection = $('input[name=coupon]:checked').val();
                                   if (selection === 'bogo')
                                   {
                                       return false
                                   }
                                   else
                                       return true
                               }
                                                       
			},
                        promotion_get:
			{                     
				required:function (){
                                   
                                   var selection = $('input[name=coupon]:checked').val();
                                   if (selection !== 'bogo')
                                   {
                                       return false
                                   }
                                   else
                                       return true
                               }
                                                       
			},
                        promotion_bogo_discount:
			{                     
				required:function (){
                                   var free = $('input[name=choose_free_or_percent]:checked').val();
                                   var selection = $('input[name=coupon]:checked').val();
                                   if (selection !== 'bogo' || free === 'buy_get_free')
                                   {
                                       return false
                                   }                                  
                                   else
                                       return true
                               }
                                                       
			},
                        promotion_buy:
			{                     
				required:function (){
                                   
                                   var selection = $('input[name=coupon]:checked').val();
                                   if (selection !== 'bogo')
                                   {
                                       return false
                                   }
                                   else
                                       return true
                               }
                                                       
			},
			promotion_valid_for:
			{
				required:true
			}
    },
		messages:
		{
			promotion_name:
			{
				required:"<?php echo lang('promotions_name_required'); ?>"
			},
			promotion_amount:
			{
				required:"<?php echo lang('promotions_amount_required'); ?>"
			},			
			promotion_valid_for:
			{
				required:"<?php echo lang('promotions_valid_for_required'); ?>"
			}
		}
	});
  
});
</script>
