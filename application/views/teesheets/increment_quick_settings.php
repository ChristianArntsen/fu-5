<?php
echo form_open('teesheets/save_quick_increment_adjustment/',array('id'=>'increment_adjustments_form'));
?>
    <fieldset id="incrememnt_adjustments_info">
        <div id="increment_adjustments">
            <input type="hidden" name="teesheet_id" value="<?=$teesheet_id?>"/>
            <input type="hidden" name="date" value="<?=$date?>"/>
            <table>
                <tbody>
                <tr d class="increment_row">
                    <td>
                        <div class="increment_settings_label">Increment Label</div>
                        <input class="increment_settings_input" name="increment_label" value=""/>
                    </td>
                    <td>
                        <div class="increment_settings_label">Start Time</div>
                        <input class="increment_settings_input timepicker" name="start_time" value="<?=$start?>"/>
                    </td>
                </tr>
                <tr  class="increment_row second_increment_row">
                    <td>
                        <div class="increment_settings_label">Highlight Color</div>
                        <input class='increment_settings_input increment_color color-picker' name="increment_color" value="" type="text" />
                    </td>
                    <td>
                        <div class="increment_settings_label">End Time</div>
                        <input class="increment_settings_input timepicker" name="end_time" value="<?=$end?>"/>
                    </td>
                </tr>
                </tbody>
            </table>
            <div id="increment_adjustment_button_holder">
                <?php
                echo form_submit(array(
                        'name'=>'save_increment_adjustments',
                        'id'=>'save_increment_adjustments',
                        'value'=>'Save',
                        'class'=>'submit_button')
                );
                ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </fieldset>
</form>
<script>
    var quick_increment_adjustment = {
        initialize : function () {
            jQuery_1_7_2('.timepicker').timepicker({});//{'timeFormat':'h:mm tt'});

            $('#save_increment_adjustments').off('click').on('click', function(e){
                e.preventDefault();
                quick_increment_adjustment.save();
            });
            jQuery_1_7_2('input.increment_color').spectrum({

                preferredFormat: 'hex6',
                allowEmpty: true,
                showInput: true,
                clickoutFiresChange:true,
                showPreviewCode:true,
                change: function(color) {
                    var selectedColor = color.toHexString();
                    var element = $(this).next('.sp-preview');
                    $(element).after('<span class="sp-preview-code">'+selectedColor+'</span>')
                }
            });
        },
        save : function (tee_sheet_id) {
            $.ajax({
                type: "POST",
                url: "index.php/teesheets/save_quick_increment_adjustment/",
                data: $('#increment_adjustments_form').serialize(),
                success: function (response) {
                    if (response.success) {
                        var tee_sheet_id = '<?=$teesheet_id?>';
                        $.colorbox.close();
                        var date = $('#calendar-'+tee_sheet_id).fullCalendar('getDate');
                        console.dir(date);
                        var year = date.getFullYear();
                        var month = date.getMonth();
                        var day = date.getDate();

                        var scrollPosition = $('.calScroller').scrollTop();
                        already_scrolling = true;
                        teesheet_controls.increments.dailies.list = response.dailies;
                        $('#calendar-'+tee_sheet_id).fullCalendar('gotoDate', year, month, day - 1, true);
                        $('#calendarback-'+tee_sheet_id).fullCalendar('gotoDate', year, month, day - 1, true);
                        render_back_nine();
                        setTimeout(function(){$('.calScroller').animate({scrollTop: scrollPosition}, 'fast')});
                        already_scrolling = false;
                    }
                },
                dataType: 'json'
            });
        }
    }
    $(document).ready(function(){
        quick_increment_adjustment.initialize();
    });
</script>
<style>
    #increment_adjustment_button_holder {
        padding:20px 0px 10px 20px;
        margin-bottom:30px;
    }
    #increment_adjustment_button_holder input[type=submit] {
        margin:10px;
        float:left;
    }
    #increment_adjustments table td {
        text-align:left;
        padding:0px 0px 0px 20px;
    }
    .timepicker {
        width:70px;
    }
    #increment_adjustments table a.remove_increment_adjustment {
        margin: 0px 0px 0px 0px;
        font-weight: bold;
        font-size: 20px;
        color: transparent;
        background: url(/images/teesheet/new/garbage-can.png) no-repeat transparent;
        width: 28px;
        height: 28px;
        display: inline-block;
    }
    .severe_warning {
        background: #c70d0d;
        color: white;
        margin: 10px;
        padding: 15px;
        font-size: 16px;
    }
    div#cboxTitle {
        height:60px;
        line-height:60px;
        font-weight:normal;
        font-size:20px;
        padding-left:20px;
        text-align:left;
        background:#369fcb;

    }
    #incrememnt_adjustments_info {
        padding-top:20px;
    }
    div#cboxContent {
        background:#ffffff;
    }
    div#cboxClose {
        background: url(/images/teesheet/new/white-x.png) no-repeat 0px 0;
        right:10px;
        top:22px;
    }
    div#cboxLoadedContent {
        margin-top:60px;
    }
    div.severe_warning {
        background: #f4d17e;
        color: #4c4c4b;
        margin: 0px 0px 30px;
        padding: 14px 26px;
        font-size: 12px;
        line-height: 30px;
    }
    .increment_settings_label {
        font-size:11px;
        color:gray;
        padding: 5px 0px;
    }
    #colorbox .increment_settings_input, #colorbox input, .sp-replacer, .increment_dropdown {
        height: 18px;
        width: 140px;
        border: 1px solid gainsboro;
        padding: 5px;
        color: #5a5a5a;
        border-radius:3px;
        box-shadow:none;
        background:white;
    }
    #increment_adjustments td.final_column {
        padding:0px 20px;
        text-align: center;
    }
    #increment_adjustments td.final_column .increment_settings_label {
        color: #d22b2b;
        font-size: 10px;
    }
    .sp-preview {
        height:16px;
        width:16px;
        border: 1px solid #dcdcdc;
    }
    .sp-dd {
        float: right;
    }
    .sp-preview-code {
        height: 20px;
        line-height: 20px;
    }
    #colorbox #increment_adjustment_button_holder #add_increment_adjustment, #colorbox #increment_adjustment_button_holder #save_increment_adjustments {
        float: right;
        margin: 0px 20px 0px 0px;
        text-shadow: none;
        border: none;
        width: auto;
        padding: 5px 20px;
        background:#369fcb;
        box-shadow:none;
        cursor:pointer;
    }
</style>