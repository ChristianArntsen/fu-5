<?php
echo form_open('teesheets/cart_sign_pdf/'.$teetime_info->TTID,array('id'=>'cart_sign_form', target=>'_blank'));
?>
<ul id="error_message_box"></ul>
<div id="cart_sign_settings">
    <fieldset id="cart_sign_basic_info">
        <legend><?php echo lang("teesheets_cart_sign_options"); ?></legend>
        <div class="field_row clearfix">
            <?php echo form_label(lang('teesheets_show_course_name').':', 'show_course_name'); ?>
            <div class='form_field'>
                <?php echo form_checkbox('show_course_name', '1', true);?>
            </div>
        </div>
        <div class="field_row clearfix">
            <?php echo form_label(lang('teesheets_show_event_name').':', 'show_event_name'); ?>
            <div class='form_field'>
                <?php echo form_checkbox('show_event_name', '1', true);?>
            </div>
        </div>
        <div class="field_row clearfix">
            <?php echo form_label(lang('teesheets_show_a_b').':', 'show_a_b'); ?>
            <div class='form_field'>
                <?php echo form_dropdown('show_a_b', array('a'=>'A','b'=>'B','both'=>'Both'), 'a', 'id="show_a_b"'); ?>
            </div>
        </div>
        <?php
        echo form_submit(array(
                'name'=>'submit',
                'id'=>'submit',
                'value'=>lang('common_generate'),
                'class'=>'submit_button float_right cart_sign_submit')
        );
        ?>
    </fieldset>
</div>
<?php
$this->load->view('teesheets/cart_sign_preview');
echo form_close();
?>
<script type='text/javascript'>

    //validation and submit handling
    $(document).ready(function()
    {
        $('.a_initial, .b_initial').hide();

        $('input[name=show_course_name]').on('change', function(){
            $('#cart_sign_download_link').remove();
            var checked = $(this).attr('checked');
            if (checked) {
                $('.course_name').show();
                $('.left_logo').show();
                $('.right_logo').show();
            }
            else {
                $('.course_name').hide();
                $('.left_logo').hide();
                $('.right_logo').hide();
            }
        });
        $('input[name=show_event_name]').on('change', function(){
            $('#cart_sign_download_link').remove();
            var checked = $(this).attr('checked');
            if (checked) {
                $('.event_name').show();
            }
            else {
                $('.event_name').hide();
            }
        });
        $('select#show_a_b').on('change', function(){
            $('#cart_sign_download_link').remove();
            $('.a_initial, .b_initial').hide();
            var a_b = $(this).val();
            if (a_b == 'a') {
                $('.a_sign').show();
                $('.b_sign').hide();
            }
            else if (a_b == 'b') {
                $('.b_sign').show();
                $('.a_sign').hide();
            }
            else if (a_b == 'both') {
                $('.a_sign').show();
                $('.b_sign').show();
                $('.a_initial, .b_initial').show();
            }
        });
});
</script>
<style>
    #cart_sign_form {
        width:1020px;
        height:600px;
    }
    #cart_sign_preview {
        width:740px;
        height:600px;
        float:left;
        overflow-y:scroll;
    }
    #cart_sign_settings {
        width:280px;
        height:600px;
        float:left;
    }
    #cart_sign_download_link {
        padding:30px 0px 0px 0px;
        text-align:center;
    }
    fieldset input.cart_sign_submit {
        margin:20px 0px 0px 100px;
    }
    .cart_sign_header {

     }
    div.course_name {

    }
    .event_name {

    }
    .starting_time {

    }
    .cart_sign_body {

    }
    .position_header {

    }
    .starting_position {

    }
    .player_header {

    }
    .player_1 {

    }
    .player_2 {

    }
    .cart_sign_footer {

    }
</style>
