<div id='charge_credit_card_form'>
	<?php if (count($charges) > 0) { ?>
	<fieldset id="previous_charge_basic_info">
	<legend><?php echo lang("teesheets_previous_charges"); ?></legend>
	<table style='width:98%; margin-left:5px;'>
	<tbody>
	<?php
		foreach($charges as $charge)
		{
			$date = date(get_date_format().' '.get_time_format(), strtotime($charge['sale_time']));
			$total = to_currency($charge['total']);
			echo "<tr><td>POS {$charge['sale_id']}</td><td>$date</td><td>$total</td></tr>";
		}
	?>
	</tbody>
	</table>
	</fieldset>
	<?php
		}
		echo form_open('/teesheets/charge_card/',array('id'=>'teetime_charge_form'));
	?>
	<fieldset id="credit_card_charge_basic_info">
	<legend><?php echo lang("teesheets_how_much_to_charge"); ?></legend>
	<div class="field_row clearfix">
		<?php echo form_label(lang('teesheets_charging_card').':', 'charging_card', array());?>
		<div class='form_field'>
			<span id='credit_card_label'><?=$credit_card->card_type?> <?=$credit_card->masked_account?></span>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('teesheets_amount').':', 'charge_amount', array());?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'charge_amount', 'id' => 'charge_amount', 'value' => ''));?>
		</div>
	</div>
	</fieldset>
	<?php
		echo form_hidden('card_to_charge', $credit_card->credit_card_id);
		echo form_hidden('charge_person_id', $credit_card->customer_id);
		echo form_hidden('tee_time_id', $teetime_info->TTID);
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit',
			'value'=>lang('teesheets_charge'),
			'class'=>'submit_button float_right')
		);
		
		echo form_close();
	?>
</div>
<style>
	.submit_button {
		margin: 4% 40%;
		cursor:pointer;
	}
</style>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
    $('#teetime_charge_form').validate({
		submitHandler:function(form)
		{
			var charge_amount = $('#charge_amount').val();
			if (submitting || !confirm('You are about to charge '+charge_amount+'. Do you want to proceed?')) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				$.colorbox2.close();
				//$.colorbox.close();
				if (response.success)
					set_feedback('You have successfully charged the credit card','success_message',false, 2000);
				else
				{
					var error_array = response.error_array;
					set_feedback(error_array[0],'warning_message',false, 5000);
				}	

				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			charge_amount: "required"   		
		},
		messages:
		{
     		charge_amount: "<?php echo lang('common_amount_required'); ?>"
		}
	});
});
</script>