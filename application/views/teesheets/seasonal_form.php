<?php
$time_dropdown = array(
	'0000'=>'12:00am',
	'0030'=>'12:30am',
	'0100'=>'1:00am',
	'0130'=>'1:30am',
	'0200'=>'2:00am',
	'0230'=>'2:30am',
	'0300'=>'3:00am',
	'0330'=>'3:30am',
	'0400'=>'4:00am',
	'0430'=>'4:30am',
	'0500'=>'5:00am',
	'0530'=>'5:30am',
	'0600'=>'6:00am',
	'0630'=>'6:30am',
	'0700'=>'7:00am',
	'0730'=>'7:30am',
	'0800'=>'8:00am',
	'0830'=>'8:30am',
	'0900'=>'9:00am',
	'0930'=>'9:30am',
	'1000'=>'10:00am',
	'1030'=>'10:30am',
	'1100'=>'11:00am',
	'1130'=>'11:30am',
	'1200'=>'12:00pm',
	'1230'=>'12:30pm',
	'1300'=>'1:00pm',
	'1330'=>'1:30pm',
	'1400'=>'2:00pm',
	'1430'=>'2:30pm',
	'1500'=>'3:00pm',
	'1530'=>'3:30pm',
	'1600'=>'4:00pm',
	'1630'=>'4:30pm',
	'1700'=>'5:00pm',
	'1730'=>'5:30pm',
	'1800'=>'6:00pm',
	'1830'=>'6:30pm',
	'1900'=>'7:00pm',
	'1930'=>'7:30pm',
	'2000'=>'8:00pm',
	'2030'=>'8:30pm',
	'2100'=>'9:00pm',
	'2130'=>'9:30pm',
	'2200'=>'10:00pm',
	'2230'=>'10:30pm',
	'2300'=>'11:00pm',
	'2330'=>'11:30pm',
	'2359'=>'11:59pm'
);

$block_time_options = array(
	0=>'12:00am',
	30=>'12:30am',
	100=>'1:00am',
	130=>'1:30am',
	200=>'2:00am',
	230=>'2:30am',
	300=>'3:00am',
	330=>'3:30am',
	400=>'4:00am',
	430=>'4:30am',
	500=>'5:00am',
	530=>'5:30am',
	600=>'6:00am',
	630=>'6:30am',
	700=>'7:00am',
	730=>'7:30am',
	800=>'8:00am',
	830=>'8:30am',
	900=>'9:00am',
	930=>'9:30am',
	1000=>'10:00am',
	1030=>'10:30am',
	1100=>'11:00am',
	1130=>'11:30am',
	1200=>'12:00pm',
	1230=>'12:30pm',
	1300=>'1:00pm',
	1330=>'1:30pm',
	1400=>'2:00pm',
	1430=>'2:30pm',
	1500=>'3:00pm',
	1530=>'3:30pm',
	1600=>'4:00pm',
	1630=>'4:30pm',
	1700=>'5:00pm',
	1730=>'5:30pm',
	1800=>'6:00pm',
	1830=>'6:30pm',
	1900=>'7:00pm',
	1930=>'7:30pm',
	2000=>'8:00pm',
	2030=>'8:30pm',
	2100=>'9:00pm',
	2130=>'9:30pm',
	2200=>'10:00pm',
	2230=>'10:30pm',
	2300=>'11:00pm',
	2330=>'11:30pm',
	2359=>'11:59pm'
);
?>
<style>
.delete_button{
	color: red;
	display: inline;
	float: right;
	margin-top: -32px;
	margin-right: 80px;
	font-size: 14px;
	cursor: pointer;
}
#teesheets div#cboxContent fieldset div.field_row label {
    width: 300px;
}
#teesheets select.frontnine {
    width: auto;
}
</style>
<script>
$(function(){
	// Price category color picker
    jQuery_1_7_2('input#teesheet_color').spectrum({
		preferredFormat: 'hex6',
		allowEmpty: true
	});
});
</script>
<ul id="error_message_box"></ul>
<?php echo form_open('teesheets/save/'.(isset($teesheet_info->teesheet_id) ? $teesheet_info->teesheet_id : '0'),array('id'=>'teesheet_form')); ?>
<fieldset id="teesheet_basic_info">
<legend><?php echo lang("teesheets_basic_information"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_teesheet_title').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'size'=>'20',
		'id'=>'name',
		'value'=>isset($teesheet_info->title) ? $teesheet_info->title : '')
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_default').':', 'default',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_radio("default",1,isset($teesheet_info->default) ? $teesheet_info->default : 0); ?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_color').':', 'color',array('class'=>'wide')); ?>
	<div class='form_field'>
	<input id='teesheet_color' name="teesheet_color" value="<?php echo isset($teesheet_info->color) ? $teesheet_info->color : ''; ?>" class="color-picker" type="text" />
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_holes').':', 'holes',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo form_dropdown('holes', array('9'=>'9','18'=>'18'), $teesheet_info->holes, 'id="holes"');?>
	</div>
</div>
<div class="field_row clearfix">
<?php 
	$attributes = ($teesheet_info->increment != '' && in_array(base_url(), $this->config->item('settings_block_base_urls'))) ? 'disabled="disabled"' : '';
	echo form_label(lang('config_increment').':', 'increment',array('class'=>'wide')); ?>
            <div class='form_field'>
        <?php echo form_dropdown('increment', array(
                '5.0'=>'5 min',
                '6.0'=>'6 min',
                '7.0'=>'7 min',
                '7.5'=>'7/8 min',
                '8.0'=>'8 min',
                '9.0'=>'9 min',
                '10.0'=>'10 min',
                '11.0'=>'11 min',
                '12.0'=>'12 min',
                '13.0'=>'13 min',
                '14.0'=>'14 min',
                '15.0'=>'15 min',
                '20.0'=>'20 min',
                '30.0'=>'30 min',
				'45.0'=>'45 min',
                '60.0'=>'60 min'),
                $teesheet_info->increment,
				$attributes);
        ?>
            <span class="settings_note">
                (How often each tee time starts)
            </span>
        </div>
</div>
<?php if ($is_admin) { ?>
<div class="field_row clearfix">
    <?php echo form_label('Increment Settings:', 'increment_settings',array('class'=>'wide')); ?>
    <div class='form_field'>
       <a href="#"	id="show-increment-settings">View</a>
    </div>
</div>
<?php } ?>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_frontnine').':', 'frontnine',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo form_dropdown('frontnine', array(
            '130'=>'1 hour 30 min',
            '140'=>'1 hour 40 min',
            '145'=>'1 hour 45 min',
            '150'=>'1 hour 50 min',
            '155'=>'1 hour 55 min',
            '200'=>'2 hours',
            '205'=>'2 hours 5 min',
            '210'=>'2 hours 10 min',
            '215'=>'2 hours 15 min',
            '220'=>'2 hours 20 min',
            '225'=>'2 hours 25 min',
            '230'=>'2 hours 30 min'),
            $teesheet_info->frontnine);
        ?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_book_sunrise_sunset').':', 'book_sunrise_sunset',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_radio("book_sunrise_sunset",0,$teesheet_info->book_sunrise_sunset==1?FALSE:TRUE); ?>
	Off
	<?php echo form_radio("book_sunrise_sunset",1,$teesheet_info->book_sunrise_sunset==1?TRUE:FALSE); ?>
	On
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_sunrise_offset').':', 'sunrise_offset',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'sunrise_offset',
		'size'=>'5',
		'id'=>'sunrise_offset',
		'value'=>(int)$teesheet_info->sunrise_offset)
	);?>
	<span class="settings_note">
		(in minutes)
	</span>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_sunset_offset').':', 'sunset_offset',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'sunset_offset',
		'size'=>'5',
		'id'=>'sunset_offset',
		'value'=>(int)$teesheet_info->sunset_offset)
	);?>
	<span class="settings_note">
		(in minutes)
	</span>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_online_booking').':', 'online_booking',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_radio("online_booking",0,$teesheet_info->online_booking==1?FALSE:TRUE); ?>
	Off
	<?php echo form_radio("online_booking",1,$teesheet_info->online_booking==1?TRUE:FALSE); ?>
	On
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Online Booking New Day Start Time', 'online_booking_new_day_start_time',array('class'=>'wide')); ?>
	<div class='form_field'>
	    <?php echo form_dropdown('online_booking_new_day_start_time', $time_dropdown,
	            isset($teesheet_info->online_booking_new_day_start_time) ? $teesheet_info->online_booking_new_day_start_time : '0000');
	    ?>
	</div>
</div>

<div id="online_times_blocked">
	<div class="field_row clearfix">
		<p>The online booking page will only be visible within the times listed below</p>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label('Block Online Booking:', 'block_online_booking', array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_radio("block_online_booking",0,$teesheet_info->block_online_booking==1?FALSE:TRUE); ?>
		No
		<?php echo form_radio("block_online_booking",1,$teesheet_info->block_online_booking==1?TRUE:FALSE); ?>
		Yes
		</div>
	</div>
	<?php
	$days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday' ,'Thursday', 'Friday', 'Saturday');
	foreach($days as $day){ 
		$open_key = strtolower(substr($day, 0, 3)).'_open'; 
		$close_key = strtolower(substr($day, 0, 3)).'_close'; 
		
		$open = 0;
		$close = 2359;

		if(isset($hours[$open_key])){
			$open = (int) $hours[$open_key];
		}
		if(isset($hours[$close_key])){
			$close = (int) $hours[$close_key];
		}
	?>
	<div class="field_row clearfix">
	<?php echo form_label($day, 'block_online_booking', array('style'=>'width: 150px;')); ?>
		<div class='form_field' style="float: left; margin-right: 25px;">
			Open <?php echo form_dropdown('hours['.$open_key.']', $block_time_options, $open); ?>
		</div>
		<div class='form_field' style="float: left;">
			Close <?php echo form_dropdown('hours['.$close_key.']', $block_time_options, $close); ?>
		</div>
	</div>	
	<?php } ?>
</div>
<script type='text/javascript'>
	$('#online_times_blocked').expandable({title:'Online Booking Visible Hours'});
</script>

<div class="field_row clearfix">
<?php echo form_label('Show On Aggregate Booking:', 'show_on_aggregate',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_radio("show_on_aggregate",0,$teesheet_info->show_on_aggregate==1?FALSE:TRUE); ?>
	No
	<?php echo form_radio("show_on_aggregate",1,$teesheet_info->show_on_aggregate==1?TRUE:FALSE); ?>
	Yes
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label('Aggregate Settings:', 'show_on_aggregate',array('class'=>'wide')); ?>
	<div class='form_field'>
		<a href="#"	id="show-aggregate-settings">View</a>
	</div>
</div>

<div class="field_row clearfix">
        <?php echo form_label(lang('config_online_open_time').':', 'online_open_time',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('online_open_time', $time_dropdown,
                        isset($teesheet_info->online_open_time) ? $teesheet_info->online_open_time : 0);
                ?>
                    <span class="settings_note">
                        (First available online tee time)
                    </span>
                </div>
        </div>
        <div class="field_row clearfix">
        <?php echo form_label(lang('config_online_close_time').':', 'online_close_time',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('online_close_time', $time_dropdown,
                        isset($teesheet_info->online_close_time) ? $teesheet_info->online_close_time : 0);
                ?>
                    <span class="settings_note">
                        (Last available online tee time)
                    </span>
                </div>
        </div>
<!--div class="field_row clearfix">
<?php echo form_label(lang('teesheets_days_out').':', 'days_out',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
			$two_week_array = array(
				'0'=>'0',
				'1'=>'1',
				'2'=>'2',
				'3'=>'3',
				'4'=>'4',
				'5'=>'5',
				'6'=>'6',
				'7'=>'7',
				'8'=>'8',
				'9'=>'9',
				'10'=>'10',
				'11'=>'11',
				'12'=>'12',
				'13'=>'13',
				'14'=>'14'
			);
			echo form_dropdown('days_out', $two_week_array, isset($teesheet_info->days_out) ? $teesheet_info->days_out : 0, 'id="days_out"');
		?>
	</div>
</div-->
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_days_in_booking_window').':', 'days_in_booking_window',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
        $sixty_day_array = array();
        for ($i = 0; $i < 61; $i++)
        {
            $sixty_day_array[$i] = $i;
        }
        $sixty_day_array[90] = 90;
        $sixty_day_array[120] = 120;
        $sixty_day_array[365] = 365;

        echo form_dropdown('days_in_booking_window', $sixty_day_array, isset($teesheet_info->days_in_booking_window) ? $teesheet_info->days_in_booking_window : 0, 'id="days_in_booking_window"');
		?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_minimum_players').':', 'minimum_players',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
			echo form_dropdown('minimum_players', array('4'=>'4', '3'=>'3', '2'=>'2', '1'=>'1'), isset($teesheet_info->minimum_players) ? $teesheet_info->minimum_players : 1, 'id="minimum_players"');
		?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('teesheets_limit_holes').':', 'limit_holes',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
			echo form_dropdown('limit_holes', array('0'=>'No Limit', '9'=>'9 only', '18'=>'18 only'), isset($teesheet_info->limit_holes) ? $teesheet_info->limit_holes : 0, 'id="limit_holes"');
		?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Reservations per day per player', 'reservation_limit_per_day', array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
			$reservation_limit_options = [0 => 'No Limit'];
			for($x = 1; $x < 10; $x++){
				$reservation_limit_options[] = $x;
			}
			echo form_dropdown('reservation_limit_per_day', $reservation_limit_options, isset($teesheet_info->reservation_limit_per_day) ? $teesheet_info->reservation_limit_per_day : 0, 'id="reservation_limit_per_day"');
		?>
	</div>
</div>

	<div class="field_row clearfix">
		<?php echo form_label('Notify all Players', 'notify_all_participants', array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php
			echo form_dropdown('notify_all_participants', array('0'=>'No', '1'=>'Yes'), isset($teesheet_info->notify_all_participants) ? $teesheet_info->notify_all_participants : 0, 'id="notify_all_participants"');
			?>
		</div>
	</div>

	<div class="field_row clearfix">
		<?php echo form_label('Online Cancelations Remove Self Only', 'self_cancel_only', array('class'=>'wide')); ?>
		<div class='form_field'>
			<?php
			echo form_dropdown('self_cancel_only', array('0'=>'No', '1'=>'Yes'), isset($teesheet_info->self_cancel_only) ? $teesheet_info->self_cancel_only : 0, 'id="self_cancel_only"');
			?>
		</div>
	</div>

<div class="field_row clearfix">
<?php echo form_label('Online Rates Allowed:', 'booking_carts',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
			echo form_dropdown('booking_carts', array('0'=>'Walking', '2'=>'Riding', '1'=>'Both'), isset($teesheet_info->booking_carts) ? $teesheet_info->booking_carts : 1, 'id="booking_carts"');
		?>
	</div>
</div>
<div class="field_row clearfix" id="hide_online_prices">
    <div class="field_row clearfix">
        <?php echo form_label('Hide Online prices', 'hide_online_prices',array('class'=>'wide')); ?>
        <div class='form_field'>
            <?php echo form_radio("hide_online_prices",0,isset($teesheet_info->hide_online_prices) && $teesheet_info->hide_online_prices ==1?FALSE:TRUE); ?>
            No
            <?php echo form_radio("hide_online_prices",1,isset($teesheet_info->hide_online_prices) && $teesheet_info->hide_online_prices ==1?TRUE:FALSE); ?>
            Yes
        </div>
    </div>
</div>
<!-- Hidden until we figure out new credit card ecommerce rules
<div class="field_row clearfix">
    <?php echo form_label('Allow pre-payment online', 'pay_online', array('class'=>'wide')); ?>
    <div class='form_field'>
		<?php echo form_dropdown('pay_online', array(
			'0' => 'Not Allowed', 
			'1' => 'Optional', 
			'2' => 'Required'
		), isset($teesheet_info->pay_online) ? $teesheet_info->pay_online : 1, 'id="pay_online"'); ?>
    </div>
</div> -->

<?php if (isset($teesheet_info->teesheet_id)) { // Turning off booking classes until after the tee sheet is created... they don't attach unless it's already created?>
    <div class="field_row clearfix">
<?php echo form_label(lang('teesheets_online_booking_classes').':', 'obc_name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<!-- <?php echo form_input(array(
		'name'=>'obc_name',
		'size'=>'20',
		'id'=>'obc_name',
		'placeholder'=>'Booking Class Name',
		'value'=>'')
	);
	?> -->
	<div id='add_online_booking_class'>Add</div>
</div>
<div id='online_booking_classes'>
	<?php if (count($obcs) > 0) {
		$this->load->view('teesheets/booking_classes');
	 } ?>
</div>
</div>
<?php } ?>
<div id='thank_you_options_box' style='display:none'>
	<div class="field_row clearfix">
		<?php echo form_label(lang('config_send_thank_you_email') . ':', 'send_thank_you');?>
		<div class='form_field'>
			<?php echo form_checkbox("send_thank_you",1,isset($teesheet_info->send_thank_you) ? $teesheet_info->send_thank_you : 0);?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('config_thank_you_campaign') . ':', 'thank_you_campaign');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'thank_you_campaign', 'id' => 'thank_you_campaign', 'value' => $thank_you_campaign));?>
			<?php echo form_hidden('thank_you_campaign_id', isset($teesheet_info->thank_you_campaign_id) ? $teesheet_info->thank_you_campaign_id : 0);?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#thank_you_options_box').expandable({
		title : 'Thank You Options:'
	});
</script>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
<a class="delete_button" rel="delete_teesheet">Delete Teesheet</a>
</fieldset>
<?php
echo form_close();
?>
<style>
	#add_online_booking_class {
		cursor:pointer;
	}
</style>
<script type='text/javascript'>
function view_increment_settings(){
    $.colorbox2({
        'href':'index.php/teesheets/view_increment_settings/<?=$teesheet_info->teesheet_id?>',
        'width':910,
        'title':'Increment Settings for <?=addslashes($teesheet_info->title)?>',
        'onComplete':function(){increment_adjustments.initialize();}});
}
//validation and submit handling
$(document).ready(function()
{
    $('input[name=book_sunrise_sunset]').click(function(){
        alert('Note: Turning the sunrise/sunset feature on or off will adjust your current reservations to match with the new times.');
    });
	$('#add_online_booking_class').click(function(){
		$.colorbox2({'href':'index.php/teesheets/view_booking_class/<?=isset($teesheet_info->teesheet_id) ? $teesheet_info->teesheet_id : -1?>', 'width':600,'title':'Create New Booking Class'})
	});
	$('#show-aggregate-settings').click(function(){
		$.colorbox2({'href':'index.php/teesheets/view_aggregate_settings/<?=$teesheet_info->teesheet_id?>', 'width':600,'title':'Aggregate Booking Settings'});
		return false;
	});
    $('#show-increment-settings').click(function(){
        view_increment_settings();
        return false;
    })
	$( "#thank_you_campaign" ).autocomplete({
		source: '<?php echo site_url("marketing_campaigns/suggest/"); ?>',
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$("#thank_you_campaign").val(ui.item.label);
			$("#thank_you_campaign_id").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});

	var submitting = false;
    $('#teesheet_form').validate({
		submitHandler:function(form)
		{
			if ($.trim($('#name').val()) != '')
			{
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				$(form).ajaxSubmit({
					success:function(response)
					{
						$.colorbox.close();
						$('#tab3').load('index.php/config/view/teesheet/' + response.teesheet_id);
					},
					dataType:'json'
				});
			}
			else
				alert('Please enter a teesheet/course name');

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
	
	$('a[rel=delete_teesheet]').on('click',function(){
		if (confirm("Are you sure you would like to delete this teesheet?"))
		{
			var teesheet_id = $('#teesheet_id').val();
			$.post("index.php/teesheets/delete_teesheet/<?php echo isset($teesheet_info->teesheet_id) ? $teesheet_info->teesheet_id : -1; ?>", null, function(response){
				if(response.success){
					set_feedback(response.message, 'success_message', false, 1500);
					$('#tab3').load('index.php/config/view/teesheets');
					$.colorbox.close();	
				}else{
					set_feedback(response.message, 'error_message', false, 2000);
				}
			},'json');
		}
		return false;
	});	
});
</script>
