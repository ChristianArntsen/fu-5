	<table id="receipt_items"  class='teetime_info_header'>
        <tr class='teetime_info_headers'>
            <th style="text-align:left;" colspan=3>Sale #:</th><th style="text-align: right"> <?= $sale_number?></th>
        </tr>
        <tr class='teetime_info_headers'>
            <th style="text-align:left;" colspan=3>Customer Name: </th><th style="text-align: right"><?= $customer_info->first_name?> <?= $customer_info->last_name?></th>
        </tr>
        <tr class='teetime_info_headers'>
            <th style="width:53%;text-align:left;"><?php echo lang('items_item'); ?></th>
            <th style="width:16%;text-align:center;"><?php echo lang('sales_quantity'); ?></th>
            <th style="width:15%;text-align:center;"><?php echo lang('sales_tax'); ?></th>
            <th style="width:16%;text-align:right;"><?php echo lang('sales_total'); ?></th>
        </tr>
	<?php
	foreach(array_reverse($cart, true) as $line=>$item)
	{
	?>
		<tr>
		<td><span class='long_name'><?php echo $item['price_category'].' '.$item['name']; ?></span><span class='short_name'><?php echo character_limiter($item['name'],10); ?></span></td>
		<td style='text-align:center;'><?php echo $item['quantity_purchased']; ?></td>
		<td style='text-align:center;'><?php echo $item['tax']; ?></td>
		<td style='text-align:right;'><?php echo to_currency($item['total']); ?></td>
		</tr>


	<?php
	}
	?>
	    
	<?php
		foreach($payments as $payment_id=>$payment)
	{ ?>
		<tr>
		<td colspan="1" style="text-align:right;"><?php echo lang('sales_payment'); ?></td>
		<td colspan="2" style="text-align:right;"><?php $splitpayment=explode(':',$payment['payment_type']); echo $splitpayment[0]; ?> </td>
		<td colspan="1" style="text-align:right"><?php echo to_currency( $payment['payment_amount'] ); ?>  </td>
	    </tr>
	<?php
	}
	?>	
    
	<?php foreach($payments as $payment) {?>
		<?php if (strpos($payment['payment_type'], lang('sales_giftcard'))!== FALSE) {?>
	<tr>
		<td colspan="1" style="text-align:right;"><?php echo lang('sales_giftcard_balance'); ?></td>
		<td colspan="2" style="text-align:right;"><?php echo $payment['payment_type'];?> </td>
		<td colspan="1" style="text-align:right"><?php echo to_currency($this->Giftcard->get_giftcard_value(end(explode(':', $payment['payment_type'])))); ?></td>
	</tr>
		<?php }?>
	<?php }?>
	</table>