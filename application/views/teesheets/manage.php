<?php $this->load->view("partial/teesheet_header"); ?>
<?php echo $this->sessioni->userdata('increment');?>
	
<script type='text/javascript'>
    var calInfo = {};
    alldata = eval('(<?php echo $JSONData ?>)');
    calInfo.caldata = alldata.caldata;
    calInfo.bcaldata = alldata.bcaldata;
    calInfo.openhour = convertFormat('<?php echo $openhour?>');
    calInfo.closehour = convertFormat('<?php echo $closehour?>');
    calInfo.increment1 = '<?php echo $increment?>';
    calInfo.increment2 = '<?php echo $increment?>';
    calInfo.holes = '<?php echo $holes?>';
    calInfo.frontnine = '<?php echo $fntime?>';
    calInfo.purchase = '<?php echo $purchase?>';
        
    function convertFormat(hourString) {
        var h = parseInt(hourString.slice(0,2), 10);
        var m = parseInt(hourString.slice(2), 10);
        var mz = '';
        if (m < 10)
            mz = 0;
        //console.log(hourString+' '+hourString.slice(0,2)+' '+h+' : '+mz+m);
        return h+':'+mz+m
    }
    $(document).ready(function(){
    	$.ajax({
           type: "POST",
           url: "index.php/teesheets/make_5_day_weather",
           data: '',
           success: function(response){
               if(response.forecast){
                   calInfo.forecast = response.forecast;
               }
   			 	$('#wbtabs-1').html(response.weather);
           },
           dataType:'json'
         }); 
    });
</script>
<div class="wrapper">
    <div class="column">
        <?php if ($user_level == 5 || $associated_courses != '[]') {?>
            <form action="" method="post">
                <div id="course_selector" class="ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display: block;">
                    <div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all">
                         Select Course
                    </div>
                    <?php echo $tsMenu; ?>
                    <input type="submit" value="changets" name="changets" id="changets" style="display:none"/>
                </div>
            </form>
        <?php } ?>
        <div id="datepicker"></div>
        <div id="weatherBox" class="weatherBox">
            <ul>
                <!--li class="stats"><a href="#tabs-1">Players</a></li-->
                <li class="weather"><a href="#wbtabs-1">Weather</a></li>
            </ul>
            <!--div id='tabs-1' class="stats">

            </div-->
            <div id='wbtabs-1'>
                <?php //echo $fdweather; ?>
            </div>
        </div>
        <div id="stats">
            <ul>

            </ul>

        </div>
    </div>
    <div id="main2" class="container">
    	<div id='teesheet_controls'>
    		
    	</div>
    	<div id='teesheet_info'>
    		
    	</div>
        <div class="inner clearfix">
        <?
            if ($user_id) {?>
            <? } ?>
            <div class="content">
                <div id="frontnine">
                    <div id='calendar' class="calendar"></div>
                </div>
                <div id="backnine">
                    <div id='calendarback' class='calendarback' style='width:348px; display:none'></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("partial/footer"); ?>