<?=!$preview && $sign_num % 2 != 0 ? '<div style="height:60px; width:800px; display:block;">&nbsp;</div>' : ''?>
<div class="cart_sign <?=$side?>_sign" style=" height:<?=$preview?'400':'470'?>px; width:<?=$preview?'680':'800'?>px; border:1px solid #ccc; font-family: 'Merriweather', serif; text-align:center; margin-bottom:<?=$preview || $sign_num % 2 == 0 ? '10' : '140'?>px; padding-top:25px; font-family:serif; margin-left:25px; <?=$side == 'b' && $preview ? 'display:none;' : ''?>">
    <div class="cart_sign_header" style="height:<?=$preview?'110':'130'?>px; padding-top:20px;width:100%;">
        <table width="100%">
            <tbody>
            <tr>
                <td width="20%">
                    <div class="left_logo" style="text-align: center;">
                        <?=$preview || $show_course_name ? img(array('src' => "index.php/app_files/view/".$logo_id, 'width' =>120)) : ''?>
                    </div>
                </td>
                <td width="60%">
                    <div class="course_name" style="margin:0 0 0 0; font-size:30px; width:100%; float:none;font-family: 'Merriweather', serif; text-align:center;"><?=$preview || $show_course_name ? $course_name : ''?></div>
                    <div class="event_name" style="width:100%; font-size:25px; margin-top:20px;font-family: 'Merriweather', serif; text-align:center;"><?=$preview || $show_event_name ? $event_name : ''?></div>
                </td>
                <td width="20%">
                    <div class="right_logo" style="text-align: center;">
                        <?=$preview || $show_course_name ? img(array('src' => "index.php/app_files/view/".$logo_id, 'width' =>120)) : ''?>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="cart_sign_body" style="height:<?=$preview?'150':'180'?>px; padding-top:10px;">
        <div class="player_1" style='font-family:"AxelBold", "Lucida Grande", "Verdana", sans-serif; font-size:<?=$preview?'40':'47'?>px; font-style:italic; font-weight:bold;'><?=empty($player_1)?'__________________':$player_1?></div>
        <div class="player_2" style='font-family:"AxelBold", "Lucida Grande", "Verdana", sans-serif; font-size:<?=$preview?'40':'47'?>px; font-style:italic; font-weight:bold; margin-top:20px;'><?=empty($player_2)?'__________________':$player_2?></div>
    </div>
    <div class="cart_sign_footer" style="padding-top:20px; font-family:serif;">
        <table width="100%">
            <tbody>
                <tr>
                    <td width="50%">
                        <div class="time_header" style="font-family: 'Merriweather', serif; text-align:center;">Starting Time:</div>
                    </td>
                    <td width="50%">
                        <div class="position_header" style="font-family: 'Merriweather', serif; text-align:center;">Starting Tee:</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="starting_time" style='font-size:30px;font-family:"AxelBold", "Lucida Grande", "Verdana", sans-serif; font-weight:bold; text-align:center;'><?=$starting_time?></div>
                    </td>
                    <td>
                        <div class="starting_position" style='font-family:"AxelBold", "Lucida Grande", "Verdana", sans-serif; font-size:40px; font-weight:bold; text-align:center;'><?=$starting_position?></div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?=!$preview && $sign_num % 2 == 0 ? '<div style="page-break-before:always;"></div>' : ''?>

