<div id="cart_sign_preview">

    <?php
    $i = 1;
    $sign_num = 1;
    while($i <= $teetime_info->holes) {
        // Up to 4 signs per hole
        $cart_sign['course_name'] = $this->config->item('name');
        $cart_sign['event_name'] = $teetime_info->title;
        $cart_sign['logo_id'] = $this->config->item('company_logo');
        $cart_sign['starting_time'] = date('g:i a', strtotime($teetime_info->start + 1000000));
        if ($a_b == 'a' || $a_b == 'both') {
            $cart_sign['sign_num'] = $sign_num;
            $cart_sign['side'] = 'a';
            $cart_sign['starting_position'] = $a_b == 'both' ? $i.'<span class="a_initial">A</span>' : $i;
            $cart_sign['player_1'] = $event_people[$i][1]['person_id'] === null ? $event_people[$i][1]['label'] : trim($event_people[$i][1]['first_name'].' '.$event_people[$i][1]['last_name']);
            $cart_sign['player_2'] = $event_people[$i][2]['person_id'] === null ? $event_people[$i][2]['label'] : trim($event_people[$i][2]['first_name'].' '.$event_people[$i][2]['last_name']);
            $this->load->view('teesheets/cart_sign', $cart_sign);
            $sign_num ++;
            $cart_sign['sign_num'] = $sign_num;
            $cart_sign['player_1'] = $event_people[$i][3]['person_id'] === null ? $event_people[$i][3]['label'] : trim($event_people[$i][3]['first_name'].' '.$event_people[$i][3]['last_name']);
            $cart_sign['player_2'] = $event_people[$i][4]['person_id'] === null ? $event_people[$i][4]['label'] : trim($event_people[$i][4]['first_name'].' '.$event_people[$i][4]['last_name']);
            $this->load->view('teesheets/cart_sign', $cart_sign);
            $sign_num ++;
        }
        if ($a_b == 'b' || $a_b == 'both') {
            $cart_sign['sign_num'] = $sign_num;
            $cart_sign['side'] = 'b';
            $cart_sign['starting_position'] = $a_b == 'both' ? $i.'<span class="a_initial">B</span>' : $i;
            $cart_sign['player_1'] = $event_people[$i][5]['person_id'] === null ? $event_people[$i][5]['label'] : trim($event_people[$i][5]['first_name'].' '.$event_people[$i][5]['last_name']);
            $cart_sign['player_2'] = $event_people[$i][6]['person_id'] === null ? $event_people[$i][6]['label'] : trim($event_people[$i][6]['first_name'].' '.$event_people[$i][6]['last_name']);
            $this->load->view('teesheets/cart_sign', $cart_sign);
            $sign_num ++;
            $cart_sign['sign_num'] = $sign_num;
            $cart_sign['player_1'] = $event_people[$i][7]['person_id'] === null ? $event_people[$i][7]['label'] : trim($event_people[$i][7]['first_name'].' '.$event_people[$i][7]['last_name']);
            $cart_sign['player_2'] = $event_people[$i][8]['person_id'] === null ? $event_people[$i][8]['label'] : trim($event_people[$i][8]['first_name'].' '.$event_people[$i][8]['last_name']);
            $this->load->view('teesheets/cart_sign', $cart_sign);
            $sign_num ++;
        }
        $i++;
    } ?>
</div>
<link href='https://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>
