<style>
#minimum_charge label {
	width: 150px;
}

#label_on {
	width: 50px;
	float: none;
}

div.category-container {
	display: block;
	width: 235px;
	overflow: hidden;
	float: left;
	margin: 15px 15px 0px 5px;
}

h3.type span.selected {
	background-color: #2575B5;
	color: white;
	padding: 4px 8px;
	font-size: 14px;
	border-radius: 3px;
}

h3.type {
	font-size: 20px;
	margin: 0px 0px 10px 0px;
}

ol.categories {
	display: block;
	height: 275px;
	overflow-y: scroll;
	overflow-x: hidden;
	background-color: white;
	border: 1px solid #E0E0E0;
	padding: 0px;
	text-indent: 0px;
}

ol.categories > li {
	display: block;
	list-style-type: none;
}

ol.categories > li input {
	margin-top: 5px;
	margin-right: 5px;
}

ol.categories > li label {
	height: 20px;
	line-height: 20px;
	padding: 5px 10px;
	display: block;
	width: auto !important;
	font-size: 14px;
}

ol.categories > li label.selected,
ol.categories > li label.selected:hover {
	background-color: #A1CEED;
}

ol.categories > li label:hover {
	background-color: #F9F9F9;
	cursor: pointer;
}
</style>

<form id="minimum_charge" style="padding: 15px">
	<?php if(!empty($charge['minimum_charge_id'])){ ?>
	<input id="charge_id" type="hidden" name="charge_id" value="<?php echo $charge['minimum_charge_id']; ?>">
	<?php } ?>
	<fieldset>
		<div class="field_row clearfix">
			<label for="name">Name</label>	
			<div class="form_field">
				<input type="text" name="name" id="name" value="<?php echo $charge['name']; ?>" style="width: 250px">
			</div>
		</div>
		<div class="field_row clearfix">
			<label for="active">Active</label>	
			<div class="form_field">
				<input type="hidden" name="is_active" value="0">
				<input type="checkbox" name="is_active" id="active" value="1" <?php if($charge['is_active'] == 1){ echo 'checked'; }?>>
			</div>
		</div>
		<div class="field_row clearfix">
			<label>Frequency</label>
			Bill every <input id="frequency" name="frequency" type="text" class="" style="width: 30px;" value="<?= (int) $charge['frequency']?>" />

			<?php echo form_dropdown('frequency_period', array(
				'day' => 'Day(s)',
				'week' => 'Week(s)',
				'month' => 'Month(s)',
				'year' => 'Year(s)',
			), $charge['frequency_period']); ?>

			<?php
			$week_menu_attr = "class='frequency_on weekday'";
			$month_menu_attr = "class='frequency_on month'";
			$year_menu_attr = "class='frequency_on year'";
			?>

			<label for="on" id="label_on" style="width: 20px">on</label>

			<?php echo form_dropdown('frequency_on', array(
				7 => 'Sunday',
				1 => 'Monday',
				2 => 'Tuesday',
				3 => 'Wednesday',
				4 => 'Thursday',
				5 => 'Friday',
				6 => 'Saturday'
			), $charge['frequency_on'], $week_menu_attr); ?>

			<?php echo form_dropdown('frequency_on', array(
				'begin' => '1st of the Month',
				'end' => 'End of the Month',
				'date' => 'Specific Date'
			), $charge['frequency_on'], $month_menu_attr); ?>

			<?php echo form_dropdown('frequency_on', array(
				'begin' => '1st of the Year',
				'end' => 'End of the Year',
				'date' => 'Specific Date'
			), $charge['frequency_on'], $year_menu_attr); ?>

			<span id="frequency_on_date" class="frequency_on_date" data-date="<?php echo $charge['frequency_on_date']; ?>"></span>
		</div>
		<div class="field_row clearfix">
			<label>Start Date</label>
			<div class="form_field">
				<span class="start-date" data-date="<?php echo $charge['start_date']; ?>"></span>
			</div>
		</div>
		<div class="field_row clearfix">
			<label>End Date</label>		
			<div class="form_field">
				<span class="end-date" data-date="<?php echo $charge['end_date']; ?>"></span>
			</div>
		</div>
		<div class="field_row clearfix">
			<label for="minimum_amount">Minimum Amount</label>	
			<div class="form_field">
				<input type="text" name="minimum_amount" id="minimum_amount" value="<?php echo $charge['minimum_amount']; ?>" style="width: 100px">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<div class="category-container">
			<h3 class="type">Departments 
				<span class="selected"><?php echo (isset($charge['item_filters']['departments'])) ? count($charge['item_filters']['departments']): 0; ?></span>
			</h3>
			<ol class="categories">
				<?php foreach($departments as $department){ 
				$selected = false;
				if(!empty($charge['item_filters']['departments']) && in_array($department['name'], $charge['item_filters']['departments'])){
					$selected = true;
				} ?>
				<li>
					<label class="<?php if($selected){ echo 'selected'; } ?>">
						<input class="category" type="checkbox" name="departments[]" value="<?php echo base64_encode($department['name']); ?>" <?php if($selected){ echo 'checked'; } ?>> 
						<?php echo $department['name']; ?>
					</label>
				</li>
				<?php } ?>
			</ol>
		</div>
		<div class="category-container">
			<h3 class="type">Categories 
				<span class="selected"><?php echo (isset($charge['item_filters']['categories'])) ? count($charge['item_filters']['categories']): 0; ?></span>
			</h3>
			<ol class="categories">
				<?php foreach($categories as $category){ 
				$selected = false;
				if(!empty($charge['item_filters']['categories']) && in_array($category['name'], $charge['item_filters']['categories'])){
					$selected = true;
				} ?>
				<li>
					<label class="<?php if($selected){ echo 'selected'; } ?>">
						<input class="category" type="checkbox" name="categories[]" value="<?php echo base64_encode($category['name']); ?>" <?php if($selected){ echo 'checked'; } ?>> 
						<?php echo $category['name']; ?>
					</label>
				</li>
				<?php } ?>
			</ol>
		</div>
		<div class="category-container">
			<h3 class="type">Sub Categories 
				<span class="selected"><?php echo (isset($charge['item_filters']['sub_categories'])) ? count($charge['item_filters']['sub_categories']): 0; ?></span>
			</h3>
			<ol class="categories">
				<?php foreach($sub_categories as $sub_category){
				$selected = false;
				if(!empty($charge['item_filters']['sub_categories']) && in_array($sub_category['name'], $charge['item_filters']['sub_categories'])){
					$selected = true;
				} ?>
				<li>
					<label class="<?php if($selected){ echo 'selected'; } ?>">
						<input class="category" type="checkbox" name="sub_categories[]" value="<?php echo base64_encode($sub_category['name']); ?>" <?php if($selected){ echo 'checked'; } ?>> 
						<?php echo $sub_category['name']; ?>
					</label>
				</li>
				<?php } ?>
			</ol>
		</div>
	</fieldset>
	<button id="save-charge" class="submit_button">Save</button>
</form>

<script>
$(document).ready(function(){

	make_date_picker('span.start-date', 'start_date');
	make_date_picker('span.end-date', 'end_date');
	make_date_picker('#frequency_on_date', 'frequency_on');

	$('#frequency_period').change(function(e){
		var period = $(this).val();
		var frequency_on = $('select.frequency_on:enabled').val();
		
		change_frequency_period(period);
		toggle_frequency_date(period, frequency_on);
	});

	$('select.frequency_on').change(function(e){
		var frequency_on = $(this).val();
		var period = $('#frequency_period').val();
		
		toggle_frequency_date(period, frequency_on);
	});

	$('input.category').click(function(e){

		var counter = $(this).parents('div.category-container').find('span.selected');
		var count = counter.text();

		if($(this).is(':checked')){
			$(this).parent('label').addClass('selected');
			count++;
		}else{
			$(this).parent('label').removeClass('selected');
			count--;
		}
		counter.text(count);
	});

	$('#minimum_charge').submit(function(){
		var data = $(this).serialize();
		var charge_id = '';

		if($('div.category-container').find('input:checked').length <= 0){
			alert('At least one Department, Category, or Sub Category must be selected');
			return false;
		}

		if($('#name').val() == ''){
			alert('Name is required');
			return false;
		}

		if($('#minimum_amount').val() <= 0){
			alert('Minimum amount must be greater than 0');
			return false;
		}

		if($('#charge_id').length > 0){
			charge_id = $('#charge_id').val();
		}
		var url = 'index.php/minimum_charges/save_charge/' + charge_id;
		
		$.post(url, data, function(response){
			$.colorbox({
				href: "index.php/minimum_charges/index", 
				width: 960,
				height: 700
			});
		}, 'json');
		
		return false;
	});

	change_frequency_period('<?php echo $charge['frequency_period']; ?>');
	toggle_frequency_date('<?php echo $charge['frequency_period']; ?>', '<?php echo $charge['frequency_on']; ?>');
});

function make_date_picker(selector, name){
	
	// Month picker
	var el = $(selector);
	var month_html = '<select class="'+el.prop('class')+' month" name="'+name+'_month">';
	var date = moment('2015-1-1');
	
	var selected_date = '2015-1-1';
	if(el.attr('data-date') && el.attr('data-date') != '0000-00-00'){
		selected_date = el.attr('data-date');
	}
	var selected = moment(selected_date, 'YYYY-MM-DD');
	var selected_month = 1;
	var selected_day = 1;

	if(selected.isValid()){
		var selected_month = selected.format('M');
		var selected_day = selected.format('D');
	}

	for(var x = 1; x <= 12; x++){
		selected = '';
		if(x == selected_month){
			selected = 'selected';
		}
		month_html += '<option value='+x+' '+selected+'>'+ date.format('MMM') + '</option>';
		date.add(1, 'month');
	}
	month_html += '</select>';
	var month_picker = $(month_html);

	var days_in_month = {
		1:31,
		2:28,
		3:31,
		4:30,
		5:31,
		6:30,
		7:31,
		8:31,
		9:30,
		10:31,
		11:30,
		12:31
	};
	var days = days_in_month[selected_month];

	var day_html = '<select class="'+el.prop('class')+' day" name="'+name+'_day">';

	day_html += get_day_options(days, selected_day);
	day_html += '</select>';
	var day_picker = $(day_html);
	
	el.replaceWith(month_picker);
	month_picker.after(day_picker);

	month_picker.change(function(){
		var days = days_in_month[ $(this).val() ];
		var selected_day = day_picker.val();
		day_picker.html( get_day_options(days, selected_day) );
	});
}

function get_day_options(days, day_selected){
	if(day_selected && day_selected > days){
		day_selected = days;
	}

	var day_html = '';
	var date = moment('2015-1-1');
	for(var x = 1; x <= days; x++){
		selected = '';
		if(x == day_selected){
			selected = 'selected = "selected"';
		}		
		day_html += '<option value="'+x+'" '+selected+'>'+ date.format('Do') + '</option>';
		date.add(1, 'day');
	}
	return day_html;
}

function change_frequency_period(value){
	if(value == 'day'){
		$('select.frequency_on').hide().attr('disabled', 'disabled');
		$('select.frequency_on_date').hide().attr('disabled', 'disabled');
		$('#label_on').hide();
		$('select.frequency_on.day').show().attr('disabled', null);

	}else if(value == 'week'){
		$('select.frequency_on').hide().attr('disabled', 'disabled');
		$('select.frequency_on_date').hide().attr('disabled', 'disabled');
		$('#label_on').show();
		$('select.frequency_on.weekday').show().attr('disabled', null);

	}else if(value == 'month'){
		$('select.frequency_on').hide().attr('disabled', 'disabled');
		$('select.frequency_on.month').show().attr('disabled', null);
		$('#label_on').show();

	}else if(value == 'year'){
		$('select.frequency_on').hide().attr('disabled', 'disabled');
		$('select.frequency_on.year').show().attr('disabled', null);
		$('#label_on').show();
	}
}

function toggle_frequency_date(period, value){
	if(value == 'date'){
		
		if(period == 'month'){
			make_date_picker('#frequency_on_date', 'frequency_on');
			$('select.frequency_on_date').hide().attr('disabled', 'disabled');
			//var day_options = get_day_options(28);
			//$('select.frequency_on_date.day').html(day_options);
			$('select.frequency_on_date.day').show().attr('disabled', null);
		
		}else if(period == 'year'){
			$('select.frequency_on_date').show().attr('disabled', null);
		}

	}else{
		$('select.frequency_on_date').hide().attr('disabled', 'disabled');
	}
}
</script>