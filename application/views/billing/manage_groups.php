<?php
echo form_open('courses/save_groups/',array('id'=>'course_groups_form'));
?>
<ul id="error_message_box"></ul>
<fieldset id="course_groups">
<legend><?php echo lang("customers_groups"); ?></legend>

<div class="field_row clearfix">	
<?php echo form_label(lang('customers_new_group').':', 'new_group'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'new_group',
		'id'=>'new_group',
		'value'=>'')
	);?>
	<?php echo form_dropdown('group_type', 
		array('linked'=>'Linked',
			'manage'=>'Managed'),
			'linked',
			"id='group_type'"			
	);?>
	<span class='green_plus' id='add_group_button'>Add</span>
	</div>
</div>

<?php 
$first_label = true;
foreach($groups as $group)
{
?>
	<div class="field_row clearfix" id='row_<?php echo $group['group_id']; ?>'>	
<?php echo form_label(($first_label)?lang('customers_groups').':':'', 'groups'); ?>
	<div class='form_field'>
		<?	echo '<span class="delete_item" onclick="groups.delete_group(\''.$group['group_id'].'\', \''.addslashes(str_replace('"', '', $group['label'])).'\')">Delete</span> '.$group['label'].' <span style="color:#ccc; font-size:smaller;">('.$group['type'].')('.$group['member_count'].' members)</span>';?>
	</div>
</div>
<? 
$first_label = false;
} ?>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_close'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>
var groups = {
	add_group:function() {
		var title = $('#new_group').val();
		var type = $('#group_type').val();
		$.ajax({
            type: "POST",
            url: "index.php/courses/add_group/",
            data: "group_label="+title+'&group_type='+type,
            success: function(response){
            	var groups_text = ($('#course_groups .field_row')[1] ==  undefined)?'Groups:':'';
            	$('#new_group').val('');
           		var html = '<div class="field_row clearfix" id="row_'+response.group_id+'">'+	
						'<label for="groups">'+
						groups_text+
						'</label>'+
						'<div class="form_field">'+
						'<span class="delete_item" onclick="groups.delete_group(\''+response.group_id+'\', \''+addslashes(title.replace('"', ''))+'\')">Delete</span>'+
						title+
						' <span style="color:#ccc; font-size:smaller;"> ('+
						type+
						')(0 members)</span></div></div>';
					
				$('#submit').before($(html));
				$.colorbox.resize();
            },
            dataType:'json'
        });
	},
	delete_group:function(group_id, title) {
		var answer = confirm('Are you sure you want to delete group: '+title+'?');
		if (answer) {
			$.ajax({
	            type: "POST",
	            url: "index.php/courses/delete_group/"+group_id,
	            data: "",
	            success: function(response){
					$('#row_'+group_id).remove();
					$.colorbox.resize();
	            },
	            dataType:'json'
	        });
	    }
	}
};

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#add_group_button').click(function(){
		groups.add_group();
	});
    $('#course_groups_form').validate({
		submitHandler:function(form)
		{
			$.colorbox.close();
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
});
</script>