<style>
#meal_courses, #meal_courses td {
	background-color: white;
}

#meal_courses td {
    background-color: white;
    border-bottom: 1px solid #B2B2B2;
    border-right: 1px solid #B2B2B2;
    color: #555555;
    font-size: 14px;
    height: 30px;
    padding: 0 6px;
    vertical-align: middle;
}

#meal_courses th {
    background: url("<?php echo base_url(); ?>images/backgrounds/paper_edge_gray.png") repeat-x scroll 0 -2px #CCCCCC;
    border-right: 1px solid #BBBBBB;
    color: #333333;
    font-size: 16px;
    height: 30px;
    padding: 0 6px;
    text-align: left;
}

#meal_courses tr.editing td {
	background-color: #F0F0F0 !important;
	padding: 5px !important;
}

#add_meal_course label {
	display: block;
	line-height: 11px;
	padding: 4px 0px;
	font-size: 12px;
}

#add_meal_course .form_field {
	float: left;
	margin-right: 10px;
}

#add_meal_course input {
	display: block;
}	

#add_meal_course {
	padding: 10px;
	overflow: hidden;
}

.form_field.name input {
	width: 200px;
}
</style>
<form method="post" class="meal_course" id="add_meal_course" action="<?php echo site_url('meal_courses/save'); ?>">
	<div class='form_field'>
		<label>Order</label>
		<input style="width: 40px;" type="text" name="order" value="0" />
	</div>	
	<div class='form_field'>
		<label>Name</label>
		<input style="width: 225px;" type="text" name="name" value="" />
	</div>
	<div class='form_field'>
		<input type="submit" class="submit_button" value="Add" />
	</div>
</form>
<div style="min-height: 400px;">
<table id="meal_courses">
	<thead class="fixedHeader">
		<tr>
			<th style="width: 50px;">Order</th>
			<th style="width: 375px;">Name</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>			
		<?php if(!empty($meal_courses)){ ?>
		<?php foreach($meal_courses as $key => $meal_course){ ?>
		<tr data-meal-course-id="<?php echo $meal_course['meal_course_id']; ?>">
			<td>
				<div class='form_field order'>
					<span class="order" data-name="order"><?php echo $meal_course['order']; ?></span>
				</div>
			</td>
			<td>
				<div class='form_field name'>
					<span class="name" data-name="name"><?php echo $meal_course['name']; ?></span>
				</div>
			</td>			
			<td class="edit">
				<a href="#" title="Edit this course" class="edit_course">Edit</a>
			</td>			
			<td class="delete">
				<a href="#" class="delete_course" title="Delete this course" style="color: red;">X</a>
			</td>
		</tr>
		<?php } }else{ ?>
		<tr class="empty">
			<td colspan="4">No meal courses created yet...</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
</div>
<script>
function get_meal_course_row(data){
	
	return '<tr data-meal-course-id="'+data.meal_course_id+'">'+
		'<td>' +
			'<div class="form_field order">'+
				'<span class="order" data-name="order">'+data.order+'</span>'+
			'</div>' +
		'</td>' +
		'<td>' +
			'<div class="form_field name">' +
				'<span class="name" data-name="name">'+data.name+'</span>' +
			'</div>' +
		'</td>'	+	
		'<td class="edit">' +
			'<a href="#" title="Edit this course" class="edit_course">Edit</a>' +
		'</td>' +		
		'<td class="delete">' +
			'<a href="#" class="delete_course" title="Delete this course" style="color: red;">X</a>' +
		'</td>' +
	'</tr>';
}

function get_meal_course_editing(data){
	
	return '<tr data-meal-course-id="'+data.meal_course_id+'">'+
		'<td>' +
			'<div class="form_field order">'+
				'<input style="width: 50px;" name="order" class="order" value="'+data.order+'">'+
			'</div>' +
		'</td>' +
		'<td>' +
			'<div class="form_field name">' +
				'<input style="width: 200px;" name="name" class="name" value="'+data.name+'">'+
			'</div>' +
		'</td>'	+	
		'<td class="edit">' +
			'<a href="#" title="Save this course" class="save_course">Save</a>' +
		'</td>' +		
		'<td class="delete">' +
			'<a href="#" class="cancel_editing" title="Cancel Editing" style="color: red;">Cancel</a>' +
		'</td>' +
	'</tr>';
}
	
$(function(){	
	
	$('#add_meal_course').submit(function(e){
		var url = $(this).attr('action');
		var data = $(this).serialize();
		var form = $(this);
		
		var order = $(this).find('input[name="order"]').val();
		var name = $(this).find('input[name="name"]').val();
		
		$.post(url, data, function(response){
			
			var template = {};
			template.name = name;
			template.order = order;
			template.meal_course_id = response.meal_course_id;
			
			$('#meal_courses tbody').find('tr.empty').hide();
			$('#meal_courses tbody').append( get_meal_course_row(template) );
			form[0].reset();
		}, 'json');
		
		e.preventDefault();
		return false;
	});
	
	$('a.delete_course').die('click').live('click', function(e){
		
		if(!confirm('Are you sure you want to delete this meal course?')){
			return false;
		}
		
		var row = $(this).parents('tr');
		var meal_course_id = row.attr('data-meal-course-id');
		var url = '<?php echo site_url('meal_courses/delete'); ?>/' + meal_course_id;
		
		$.post(url, null, function(response){
			if(response.success){
				row.remove();
			}
		},'json');
		
		return false;
	});

	$('a.edit_course').die('click').live('click', function(e){
		var row = $(this).parents('tr');
		
		var data = {};
		data.meal_course_id = row.attr('data-meal-course-id');
		data.name = row.find('span.name').text();
		data.order = row.find('span.order').text();
		
		var new_row = $(get_meal_course_editing(data));
		var original = row.replaceWith(new_row);
		new_row.data('original', original);

		return false;
	});
	
	$('a.cancel_editing').die('click').live('click', function(e){
		var row = $(this).parents('tr');
		row.replaceWith( row.data('original') );
		return false;
	});
	
	$('a.save_course').die('click').live('click', function(e){
		var row = $(this).parents('tr');
		var data = row.find('input, select').serialize();
		
		var order = row.find('input[name="order"]').val();
		var name = row.find('input[name="name"]').val();
		var meal_course_id = row.attr('data-meal-course-id')
		
		$.post('<?php echo site_url('meal_courses/save'); ?>/' + meal_course_id, data, function(response){
			
			var template = {};
			template.order = order;
			template.name = name;
			template.meal_course_id = meal_course_id;
			
			row.replaceWith( get_meal_course_row(template) );
		},'json');
		
		return false;
	});	
});
</script>
