<?php $this->load->view("partial/header_new"); ?>
<?php
if (isset($error_message))
{
	echo '<h1 style="text-align: center;">'.$error_message.'</h1>';
	exit;
}
?>
<div id="receipt_wrapper" class='receivings'>
	<div id="receipt_header">
		<div id='course_info'>
			<?php if($this->config->item('company_logo')) {?>
			<div id="company_logo"><?php echo img(array('src' => $this->Appconfig->get_logo_image(), 'width' =>144)); ?></div>
			<?php } ?>
			<div id='course_data'>
				<div id="company_name"><?php echo $this->config->item('name'); ?></div>
				<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
				<div id="company_city_state"><?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip'); ?></div>
				<div id="company_email"><?php echo $this->config->item('email'); ?></div>
				<div id="company_phone"><?php echo $this->config->item('phone'); echo ($this->config->item('fax') != '')?' Fax:'.$this->config->item('fax'):''; ?></div>
			</div>
			<div class='clear'></div>
		</div>
		<div id='invoice_info'>
			<div id="sale_receipt"><?php echo $receipt_title; ?></div>
			<div id='invoice_number'><?php echo $invoice_number_text.': '.$invoice_number ?></div>
			<div id="sale_time"><?php echo $transaction_time ?></div>
		</div>
		<div class='clear'></div>
	</div>
	<div id='supplier_info'>
		<div id='supplier_data'>
			<?php if(isset($supplier))
			{
			?>
				<div id="customer"><?php echo $supplier->company_name.' ('.$supplier->first_name.' '.$supplier->last_name.')'; ?></div>
				<div id="customer_address"><?php echo $supplier->address_1.' '.$supplier->address_2; ?></div>
				<?php if ($supplier->city != '' && $supplier->state != '' || $supplier->zip != '') {?>
				<div id="customer_city_state"><?php echo $supplier->city.', '.$supplier->state.' '.$supplier->zip; ?></div>
				<?php } ?>
				<!--div id="customer_phone"><?php echo $supplier->phone_number.' '.$supplier->email; ?></div-->
			<?php
			}
			?>
		</div>	
		<div class='clear'></div>
	</div>
	<div id="receipt_general_info">
		<div id='employee_general_info'>
			<div class='general_info_title'><?php echo lang('receivings_general_info'); ?></div>
			<div id="sale_id"><?php echo lang('receivings_id').": ".$receiving_id; ?></div>
			<div id="employee"><?php echo lang('employees_employee').": ".$employee; ?></div>
			<?php if(isset($reference))
			{
			?>
			<div id="customer"><?php echo lang('receivings_reference').": ".$reference; ?></div>
			<?php
			}
			?>
		</div>
		<div id='return_general_info'>
			<div class='general_info_title'><?php echo lang('common_return_policy'); ?></div>
			<div id="sale_return_policy">
			<?php echo nl2br($this->config->item('return_policy')); ?>
			</div>
		</div>
		<div class='clear'></div>
	</div>

	<table id="receipt_items">
	<tbody>
	<tr>
	<th style="width:10%;text-align:left;"><?php echo lang('sales_item_number'); ?></th>
	<th style="width:40%;text-align:left;"><?php echo lang('items_item'); ?></th>
	<th style="width:17%;text-align:left;"><?php echo lang('common_price'); ?></th>
	<th style="width:16%;text-align:center;"><?php echo lang('sales_quantity'); ?></th>
	<th style="width:16%;text-align:center;"><?php echo lang('sales_discount'); ?></th>
	<th style="width:17%;text-align:right;"><?php echo lang('sales_total'); ?></th>
	</tr>
	<tr>
		<td colspan="6"><hr/></td>
	</tr>
	<?php
	$pixels = 0;
	foreach(array_reverse($cart, true) as $line=>$item)
	{
		$pixels += 22;
	?>
		<tr>
		<td><?php echo $item['item_number']; ?></td>
		<td><span class='long_name'><?php echo $item['name']; ?></span><span class='short_name'><?php echo character_limiter($item['name'],40); ?></span></td>
		<td><?php echo to_currency($item['price']); ?></td>
		<td style='text-align:center;'><?php echo $item['quantity']; ?></td>
		<td style='text-align:center;'><?php echo $item['discount']; ?></td>
		<td style='text-align:right;'><?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100); ?></td>
		</tr>
		<?php if ($item['description'] != '' || $item['serialnumber'] != '') { 
			$pixels += 22;
			?>
	    <tr>
	    <td colspan="3" align="center"><?php echo $item['description']; ?></td>
		<td colspan="2" ><?php echo $item['serialnumber']; ?></td>
		<td colspan="1" style='text-align:right;'><?php echo '---'; ?></td>
	    </tr>
	<?php
	   	}
	}
	?>
		<tr style='height:<?php echo 50-$pixels ?>px;'></tr>
	</tbody>
	</table>
	<table>
		<tbody>
			<tr>
				<td colspan="6"><hr/></td>
			</tr>
		
			<tr>
				<td rowspan="4">
					<div id='barcode'>
					<?php echo "<img src='".site_url('barcode')."?barcode=$receiving_id&text=$receiving_id' />"; ?>
					</div>
				</td>
				<td colspan="3" style='text-align:right;'><?php echo lang('sales_sub_total'); ?></td>
				<td colspan="2" style='text-align:right'><?php echo to_currency($total); ?></td>
			</tr>
		
			<tr>
				<td colspan="3" style='text-align:right;'><?php echo lang('receivings_shipping_cost'); ?></td>
				<td colspan="2" style='text-align:right'><?php echo to_currency($shipping_cost); ?></td>
			</tr>
			<tr>
				<td colspan="3" style='text-align:right;'><?php echo lang('sales_total'); ?></td>
				<td colspan="2" style='text-align:right'><?php echo to_currency($total+$shipping_cost); ?></td>
			</tr>
		
			<tr>
				<td colspan="3" style='text-align:right;'><?php echo lang('sales_payment'); ?></td>
				<td colspan="2" style='text-align:right'><?php echo $payment_type; ?></td>
			</tr>
		
			<?php if(isset($amount_change))
			{
			?>
				<tr>
				<td colspan="4" style='text-align:right;'><?php echo lang('sales_amount_tendered'); ?></td>
				<td colspan="2" style='text-align:right'><?php echo to_currency($amount_tendered); ?></td>
				</tr>
		
				<tr>
				<td colspan="4" style='text-align:right;'><?php echo lang('sales_change_due'); ?></td>
				<td colspan="2" style='text-align:right'><?php echo $amount_change; ?></td>
				</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</div>
<?php $this->load->view("partial/footer"); ?>

<?php if ($this->Appconfig->get('print_after_sale'))
{
?>
<script type="text/javascript">
$(window).load(function()
{
	window.print();
});
</script>
<?php
}
?>