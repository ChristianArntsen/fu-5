<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Page title</title>
	<link href="<?php echo base_url(); ?>/css/dist/online-booking.min.css" type="text/css" rel="stylesheet" />
	<style>
	body {
		background: url('<?php echo base_url(); ?>/images/grid_noise.png');
	}
	
	#unavailable {
		background-color: white;
		border: 1px solid #E0E0E0;
		padding: 20px;
	}
	
	h1, h2 {
		text-align: center;
		line-height: 1em;
		margin: 10px;
	}
	
	h2 {
		font-size: 1.75em;
	}
	</style>	
</head>
<body>
	<nav class="navbar navbar-default" role="navigation" id="navigation">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"><?=!empty($golf_course_name) ? $golf_course_name : ''?></a>
			</div>
		</div>
	</nav>	
	<div class="container">
		<div class="col-md-12" id="unavailable">
			<?php if(!empty($closed) && $closed){ ?>
			<h1>Booking Closed</h1>
			<h2>Online booking available from <?php echo $open_time.' to '.$close_time ?></h2>			
			<?php }else{ ?>
			<h1>We're sorry,</h1>
			<h2>Online booking is currently unavailable</h2>
			<?php } ?>
		</div>
	</div>
</body>
</html>
