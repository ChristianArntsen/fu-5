<div id="credit_ets_giftcard" style="display: block; height: 250px; overflow: hidden; margin: 0px; font-size: 14px; padding: 15px;">
	<div class="field_row clearfix">
		<label style="display: block; margin-bottom: 5px;">Amount</label>
		<input type="text" name="amount" id="ets_giftcard_amount" style="display: block;" value="<?php echo $amount; ?>" />
	</div>
	<a class="terminal_button" style="margin: 25px 0px 5px 0px; float: left; display: block; width: 125px;" id="submit_ets_amount" href="">Add Credit</a>
</div>

<script>
$(function(){
	$('#submit_ets_amount').click(function(e){
		var amount = $('#ets_giftcard_amount').val();
		var line = <?php echo (int) $line; ?>;
		
		$('#credit_ets_giftcard').mask('Loading ETS..');
		$.post('<?php echo site_url('sales/credit_ets_giftcard_window') ?>/' + amount + '/' + line +'/1', null, function(response){
			sales.update_cart_ajax(line, 'price', amount);
			$('#credit_ets_giftcard').html(response);
		}, 'html');
		
		return false;
	});
});
</script>
