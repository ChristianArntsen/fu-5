<?php
$acl = new \fu\acl\Acl();
$price_class_disabled = $acl->can(new \fu\acl\Permissions\Customer\PriceClass(["update"])) ? "" : "disabled=disabled";
$membership_disabled = $acl->can(new \fu\acl\Permissions\Customer\AccountBalance(["update"]))?"" : "disabled=disabled";
$membership_disabled_bool = false;
if(!empty($membership_disabled)){
	$membership_disabled_bool = true;
}

if ($controller == 'teesheets')
	echo form_open('teesheets/save_customer/'.(!empty($person_info->person_id) ? $person_info->person_id : '-1') ,array('id'=>'customer_form'));
if ($controller == 'customers')
	echo form_open('customers/save/'.(!empty($person_info->person_id) ? $person_info->person_id : '-1'),array('id'=>'customer_form'));
if ($controller == 'sales' || $controller == 'teesheets')
	echo form_open('sales/save_customer/'.(!empty($person_info->person_id) ? $person_info->person_id : '-1'),array('id'=>'customer_form'));
	//print_r($price_classes);

function get_required_text($field, $field_settings){
	if(!empty($field_settings[$field]) && $field_settings[$field]['required'] == 1){
		return '<span class="required">*</span>';
	}
	return '';
}
?>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("customers_basic_information"); ?></legend>
<?php $this->load->view("people/form_basic_info"); ?>
<input type='hidden' value='1' name='from_customer_edit_page'/>
<div class="field_row clearfix">
<?php echo form_label(lang('common_status').':', 'status');  ?>
	<div class='form_field'>
	<?php
		echo form_dropdown('cust_status', array(0 => 'N/A',1 => 'Red', 2 => 'Yellow', 3 => 'Green'), !empty($person_info->status_flag) ? $person_info->status_flag : '' );
	?>
	</div>
</div>
</fieldset>
<?php if (isset($quick_add) && $quick_add) { ?>
<style>
	#customer_basic_info {
		width:100%;
	}
</style>
<?php } else { ?>
<fieldset id="customer_info">
<legend>
	<?php echo lang("customers_settings"); ?>
	<?php if ($person_info->person_id != '' && $person_info->person_id > 0){?>
		<?php if (!$household_head && $this->permissions->employee_has_module('invoices')) { ?>
		 - <a href='index.php/customers/billing/<?=$person_info->person_id;?>/width~1200' id='billing_link'>Billing Information</a>
		<?php } ?>
		 - <a href='index.php/customers/history/<?=$person_info->person_id;?>/width~650' id='history_link'>History</a>
		 - <a href='index.php/customers/manage_credit_cards/<?=$person_info->person_id;?>' id='manage_credit_cards' ><?=lang('customers_credit_cards')?></a>
	<?php } ?>
</legend>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_account_number').':'.get_required_text('account_number', $field_settings), 'account_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'account_number',
		'id'=>'account_number',
		'size'=>'40',
		'value'=>$person_info->account_number)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_price_class').':', 'price_class'); ?>
	<div class='form_field'>
	<?php
		echo form_dropdown('price_class', $price_classes, $person_info->price_class,$price_class_disabled);
	?>
	</div>
</div>
<div id='discount_rate' class="field_row clearfix" ?>
<?php echo form_label(lang('customers_discount').':', 'discount'); ?>
	<div class='form_field'>
	<?php
	$params = array(
		'name'=>'discount',
		'id'=>'discount',
		'class'=>'discount',
		'size'=>'10',
		'value'=>$person_info->discount);
	if($membership_disabled_bool){
		$params['disabled']="disabled";
	}
	echo form_input($params);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_non_taxable').':', 'taxable'); ?>
	<div class='form_field'>
		<?php echo form_checkbox('taxable', '1', $person_info->taxable == '' ? FALSE : (boolean)!$person_info->taxable);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('customers_member').':', 'member'); ?>
	<div class='form_field'>
	<?php echo form_checkbox('member', '1', empty($person_info->member) ? FALSE : (boolean)$person_info->member,$membership_disabled);?>
	</div>
</div>
<div id='customer_account_settings' <?php if ($household_head) {echo 'style="opacity:.2;"';}?>>
	<?php if ($household_head) {?>
		<div id='member_account_info' class="field_row clearfix">
			<?php echo form_label('Household Account'); ?>
		</div>
	<?php } ?>
<div id='member_account_info' class="field_row clearfix" <?php echo ($person_info->member)?"":"style='display:none'"?>>
<?php echo form_label(($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')).':', 'member_account_balance'); ?>
	<div class='form_field'>
	<?php
	$member_account_data = array(
		'name'=>'member_account_balance',
		'id'=>'member_account_balance',
		'class'=>'customer_balances',
		'size'=>'10',
		'value'=>$person_info->member_account_balance);
	if($membership_disabled_bool){
		$member_account_data['disabled']="disabled";
	}
	if ($household_head) {
		$member_account_data['disabled'] = true;
	}
	echo form_input($member_account_data);?>
	<?	echo form_checkbox('member_account_balance_allow_negative', 'member_account_balance_allow_negative', ($person_info->member_account_balance_allow_negative) ? true:FALSE,$membership_disabled).' Allow negative';?>
	</div>
</div>
<div id='member_account_limit_info' class="field_row clearfix" <?php echo ($person_info->member_account_balance_allow_negative)?"":"style='display:none'"?>>
<?php echo form_label(' - Account Limit:', 'member_account_limit'); ?>
	<div class='form_field'>
	<?php
	$member_account_data = array(
		'name'=>'member_account_limit',
		'id'=>'member_account_limit',
		'class'=>'account_limit',
		'size'=>'10',
		'value'=>($person_info->member_account_limit < 0 ? $person_info->member_account_limit : '-'));
	if($membership_disabled_bool){
		$member_account_data['disabled']="disabled";
	}
	if ($household_head) {
		$member_account_data['disabled'] = true;
	}
	echo form_input($member_account_data);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname')).':', 'account_balance'); ?>
	<div class='form_field'>
	<?php
	$account_data = array(
		'name'=>'account_balance',
		'id'=>'account_balance',
		'class'=>'customer_balances',
		'size'=>'10',
		'value'=>$person_info->account_balance);
	if($membership_disabled_bool){
		$account_data['disabled']="disabled";
	}
	if ($household_head) {
		$account_data['disabled'] = true;
	}
	echo form_input($account_data);?>
	<? echo form_checkbox('account_balance_allow_negative', 'account_balance_allow_negative', ($person_info->account_balance_allow_negative) ? true:FALSE,$membership_disabled).' Allow negative';?>
	</div>
</div>
<div id='account_limit_info' class="field_row clearfix" <?php echo ($person_info->account_balance_allow_negative)?"":"style='display:none'"?>>
<?php echo form_label(' - Account Limit:', 'account_limit'); ?>
	<div class='form_field'>
	<?php
	$hidden = $person_info->account_balance_allow_negative;
	$account_data = array(
		'name'=>'account_limit',
		'id'=>'account_limit',
		'class'=>'account_limit',
		'size'=>'10',
		'value'=>($person_info->account_limit < 0 ? $person_info->account_limit : '-'));
	if($membership_disabled_bool){
		$account_data['disabled']="disabled";
	}
	if ($household_head) {
		$account_data['disabled'] = true;
	}
	echo form_input($account_data);?>
	</div>
</div>
<div id='invoice_balance_info' class="field_row clearfix">
<?php echo form_label((lang('customers_invoice_balance')).':', 'invoice_balance'); ?>
	<div class='form_field'>
	<?php
	$invoice_data = array(
		'name'=>'invoice_balance',
		'id'=>'invoice_balance',
		'class'=>'invoice_balances',
		'size'=>'10',
		'style'=>'width:85px;',
		'value'=>$person_info->invoice_balance);
	if($membership_disabled_bool){
		$invoice_data['disabled']="disabled";
	}
	if ($household_head) {
		$invoice_data['disabled'] = true;
	}
	echo form_input($invoice_data);?>
	</div>
</div>
<div id='invoice_email_info' class="field_row clearfix">
<?php echo form_label((lang('customers_invoice_email')).':', 'invoice_email'); ?>
	<div class='form_field'>
	<?php
    $value = isset($person_info->invoice_email) ? $person_info->invoice_email:'';
	$invoice_data = array(
		'name'=>'invoice_email',
		'id'=>'invoice_email',
		'class'=>'invoice_balances',
		'placeholder'=>lang('customers_sends_duplicate_invoices'),
		'size'=>'40',
		'value'=>$value);
	if ($household_head) {
		$invoice_data['disabled'] = true;
	}
	echo form_input($invoice_data);?>
	</div>
</div>
<?php if ($this->config->item('use_loyalty')) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_loyalty_points').':', 'loyalty_points'); ?>
	<div class='form_field'>
	<?php
	$loyalty_data = array(
		'name'=>'loyalty_points',
		'id'=>'loyalty_points',
		'class'=>'customer_balances',
		'size'=>'10',
		'value'=>$person_info->loyalty_points);
	if($membership_disabled_bool){
		$loyalty_data['disabled']="disabled";
	}
	if ($household_head) {
		$invoice_data['disabled'] = true;
	}
	echo form_input($loyalty_data);?>
	<?php echo form_checkbox('use_loyalty', '1', $person_info->use_loyalty == '' ? (($this->config->item('loyalty_auto_enroll')) ? TRUE : FALSE) : (boolean)$person_info->use_loyalty).' '.lang('customers_use_loyalty');?>
	</div>
</div>
<?php } ?>
</div>
<div class="field_row clearfix">


<?php
$first_label = true;
$group_array = array('0'=>'Select Group');
$group_checkboxes = '';
foreach($groups as $group)
{
	$group_array[$group['group_id']] = $group['label'];

	$group_checkboxes .= "<div id='checkbox_holder_".$group['group_id']."'class='field_row clearfix ".(($group['is_member'] > 0)?'':'hidden')."'>".
			form_label('', 'groups').
			"<div class='form_field'>".
				form_checkbox('groups[]', $group['group_id'], ($group['is_member'] > 0) ? true:FALSE, "id='group_checkbox_{$group['group_id']}' group_id='{$group['group_id']}'").' '.$group['label'].
			"</div>
		</div>";

$first_label = false;
} ?>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_groups').':', 'groups');  ?>
	<div class='form_field'>
	<?php
		echo form_dropdown('groups', $group_array, '');
	?>
	</div>
</div>
<div class='group_checkboxes'>
<?= $group_checkboxes?>
</div>

<?php if($this->session->userdata('sales_v2') == 1){ ?>
<div class='pass_checkboxes' style="margin-bottom: 25px">
	<h2>Passes</h2>
	<?php
	if(!empty($passes)){
		foreach($passes as $pass){ ?>
			<div>
				<h4><?php echo $pass['name']; ?></h4>
				<?php echo date('m/d/Y', strtotime($pass['start_date'])); ?>
				to
				<?php echo date('m/d/Y', strtotime($pass['end_date'])); ?>
			</div>
		<?php } }else{ ?>
		<div>No passes</div>
	<?php } ?>
</div>

<?php }else{

$first_label = true;
$pass_array = array('0'=>'Select Pass');
$pass_checkboxes = '';
foreach($passes as $pass)
{
	$pass_array[$pass['pass_id']] = $pass['label'];

    $start_date = isset($pass['start_date'] ) && $pass['start_date']  == '' ? '' : date('Y-m-d H:i:s', strtotime($pass['start_date']));

	$pass_checkboxes .= "<div id='checkbox_holder_".$pass['pass_id']."'class='field_row clearfix ".(($pass['is_member'] > 0)?'':'hidden')."'>".
			form_label('', 'passes').
			"<div class='form_field'>".
				form_checkbox('passes[]', $pass['pass_id'], ($pass['is_member'] > 0) ? true:FALSE, "id='pass_checkbox_{$pass['pass_id']}' pass_id='{$pass['pass_id']}'").' '.$pass['label'].
				"<div class='dates'>".
				"<span class='date_label'>Start:</span>".
				form_input(array(
					'name'=>'start_date_'.$pass['pass_id'],
					'id'=>'start_date_'.$pass['pass_id'],
					'size'=>'10',
					'value'=>$start_date,
					'class'=>'start_date')).
				"<span class='date_label'>End:</span>".
				form_input(array(
					'name'=>'expiration_'.$pass['pass_id'],
					'id'=>'expiration_'.$pass['pass_id'],
					'size'=>'10',
					'value'=>$pass['expiration'] == '' ? '' : date('Y-m-d H:i:s', strtotime($pass['expiration'])),
					'class'=>'expiration')).
				"</div>".
			"</div>
		</div>";

$first_label = false;
} ?>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_passes').':', 'passes');  ?>
	<div class='form_field'>
	<?php
		echo form_dropdown('passes', $pass_array, '');
	?>
	</div>
</div>
<div class='pass_checkboxes'>
<?= $pass_checkboxes?>
</div>
<?php } ?>

<div id='minimum_charges' style='display:none;'>
	<div style="padding: 15px; font-size: 14px">
		<?php echo form_dropdown('minimum_charges_menu', $minimum_charges, '');?>
		<table id="customer_minimum_charges" style="min-height: 25px; margin-top: 15px">
			<thead>
			<tr>
				<td>Minimum Charge</td>
				<td>Start Date</td>
				<td>Options</td>
			</tr>
			</thead>
			<tbody>
			<?php
			if(!empty($customer_minimum_charges)){
				foreach($customer_minimum_charges as $key => $charge){ ?>
					<tr>
						<td>
							<input type="hidden" id="minimum_charge_<?php echo $charge['minimum_charge_id']; ?>" name="minimum_charges[<?=$key?>][minimum_charge_id]" value="<?php echo $charge['minimum_charge_id']; ?>">
							<?php echo $charge['name']; ?> <?php echo to_currency($charge['minimum_amount']); ?></td>
						<td>
							<input type="date" name="minimum_charges[<?=$key?>][date_added]" value="<?php echo $charge['date_added']; ?>" />
						</td>
						<td>
							<a class="delete" href="#" style="color: red; padding-left: 10px;">Delete</a>
						</td>

					</tr>
				<?php } } ?>
			</tbody>

		</table>
	</div>
</div>

<div id='household_box' style='display:none'>
	<?php if ($household_head) { ?>
		<div class="field_row clearfix">
		<?php echo form_label(lang('customers_household_head').':', 'household_head'); ?>
			<div class='form_field'>
			<?php echo $household_head['first_name'].' '.$household_head['last_name']; ?>
			</div>
		</div>
		<div class="field_row clearfix">
			<?php echo lang('customers_household_member_notice_paragraph'); ?>
		</div>
	<?php } else { ?>
		<div class="field_row clearfix">
		<?php echo form_label(lang('customers_add_member').':', 'add_member'); ?>
			<div class='form_field'>
			<?php echo form_input(array(
				'name'=>'add_member',
				'id'=>'add_member',
				'size'=>'40',
				'value'=>'')
			);?>
			</div>
			<input type='hidden' value='0' id='new_member'/>
		</div> 
		<div id='household_members'>
		<?php if ($household_members) { ?>
			<?php foreach($household_members as $member) { ?>
				<div class="field_row clearfix" id='member_row_<?=$member['person_id']?>'>
					<?php echo form_label("<span onclick='customer.delete_household_member({$member['person_id']})'>x</span>", 'household_member_notice'); ?>
					<div class='form_field'>
					<?php echo $member['first_name'].' '.$member['last_name']; ?>
					<?php echo form_hidden('household_member_id[]', $member['person_id']); ?>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
		</div>	  	
	<?php } ?>
</div>
<script type='text/javascript'>
	$('#household_box').expandable({
		title : 'Household Information:'
	});
	$('#minimum_charges').expandable({
		title : 'Minimum Charges'
	});
</script>
<?php

if (!$this->permissions->is_employee() || ($this->session->userdata('use_new_permissions') && $acl->can(new \fu\acl\Permissions\Customer\OnlineCredentials(["update"]))))
{
?>
<div id='customer_password_box' style='display:none'>
	<div class="field_row clearfix" style='display:none'>
	<?php echo form_label(lang('employees_username').':<span class="required">*</span>', 'username',array('class'=>'')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'username_fake',
			'size'=>'30',
			'id'=>'username_fake',
			'value'=>$person_info->username));?>
		</div>
	</div>
	<div class="field_row clearfix" style='display:none'>
	<?php echo form_label(lang('employees_password').':'.$password_label_attributes, 'password',array()); ?>
		<div class='form_field'>
		<?php echo form_password(array(
			'autocomplete'=>'off',
			'size'=>'30',
			'name'=>'password_fake',
			'id'=>'password_fake'
		));?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('employees_username').':<span class="required">*</span>', 'username',array('class'=>'')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'username',
			'size'=>'30',
			'id'=>'username',
			'value'=>$person_info->username));?>
		</div>
	</div>

	<?php
	$password_label_attributes = $person_info->person_id == "" ? '<span class="required">*</span>':'';
	?>

	<div class="field_row clearfix">
	<?php echo form_label(lang('employees_password').':'.$password_label_attributes, 'password',array()); ?>
		<div class='form_field'>
		<?php echo form_password(array(
			'autocomplete'=>'off',
			'size'=>'30',
			'name'=>'password',
			'id'=>'password'
		));?>
		</div>
	</div>

	<div class="field_row clearfix">
	<?php echo form_label(lang('employees_repeat_password').':'.$password_label_attributes, 'repeat_password',array()); ?>
		<div class='form_field'>
		<?php echo form_password(array(
			'autocomplete'=>'off',
			'size'=>'30',
			'name'=>'repeat_password',
			'id'=>'repeat_password'
		));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<span style='font-size:11px; padding:10px;'><?php echo lang('customers_password_change_notification');?></span><br/>
		<?php if ($person_info->username) { ?>
		<span style='font-size:11px; padding:10px 16px;'><?php echo lang('customers_or_send_password_change_email')." <span id='send_password_change_email'>".lang('customers_click_here').'</span>';?></span>
		<?php } ?>
	</div>
</div>
<script type='text/javascript'>
	$('#customer_password_box').expandable({
		title : 'Customer Online Credentials:'
	});
</script>
<?php
}
?>
</fieldset>
<?php } ?>
<div class='clear' style='text-align:center'>
<?php echo form_hidden('colorbox_flag', '0'); ?>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</div>
<?php
echo form_close();
?>
<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	$( "#submit" ).click(function() {
		$("#colorbox_flag").val(1);
	});
	$( "input[name=member_account_balance_allow_negative]" ).click(function() {
		var mali = $('#member_account_limit_info');
		if ($(this).attr('checked')){
			mali.show();
		}
		else {
			mali.hide();
		}
	});
	$( "input[name=account_balance_allow_negative]" ).click(function() {
		var ali = $('#account_limit_info');
		if ($(this).attr('checked')){
			ali.show();
		}
		else {
			ali.hide();
		}
	});
	$('#account_number').swipeable({allow_enter:false, character_limit:20});
	// HOUSEHOLD CODE
	$('#add_member' ).autocomplete({
		source: '<?php echo site_url('customers/customer_search/last_name'); ?>',
		delay: <?=$this->config->item("search_delay") ?>,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			console.log('about to add member');
			if (ui.item.value != '<?=!empty($person_info->person_id) ? $person_info->person_id : ''?>' && $('#member_row_'+ui.item.value).length == 0)
				customer.add_household_member(ui.item.value, ui.item.label);
			$('#new_member').val(1);
			$('#add_member').val('');
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title').val(ui.item.label);
		}
	});
	// Image library handling
	$('#select-image').colorbox2({'maxHeight':750,'width':1100});

	$(document).unbind('changeImage').bind('changeImage', function(event){
		$.post('<?php echo site_url('customers/save_image'); ?>/<?= !empty($person_info->person_id) ? $person_info->person_id : ''; ?>', {image_id: event.image_id}, function(response){
			$('#image-info img').attr('src', response.thumb_url);
			$('#select-image').attr('href','<?php echo site_url('upload'); ?>/index/customers?crop_ratio=1&image_id=' + event.image_id);
			$.colorbox2.close();
		},'json');
	});

	$('#remove-image').click(function(event){
		var url = $(this).attr('href');

		if(confirm('Remove this image?')){
			$.post(url, {image_id:0}, function(response){
				$('#image-info img').attr('src', response.thumb_url);
				$('#select-image').attr('href','<?php echo site_url('upload'); ?>/index/customers?crop_ratio=1&image_id=' + response.image_id);
			},'json');
		}
		return false;
	});

	$('#send_password_change_email').click(function(e){
		console.log('sending_reset_password_email');
		var username = $('#username').val();
		$.ajax({
           type: "POST",
           url: 'index.php/customer_login/do_reset_password_notify',
           data: {'username':username,'course_id':"<?=$this->session->userdata('course_id')?>"},
           success: function(response){
           		alert('Password Reset Email sent successfully.')
		   }
        });
	});
	$('#billing_link').colorbox();
	$('#history_link').colorbox2({'title':"Customer History"});
	$('#manage_credit_cards').colorbox2({'title':'Manage Credit Cards', width:600});
	$('.expiration, .start_date').datetimepicker({dateFormat:'yy-mm-dd', changeYear:true});
	var member_box = $('input[name=member]');
	var member_account_box = $('#member_account_info');
	member_box.change(function(e){
		(member_box.attr('checked')) ? member_account_box.show() : member_account_box.hide();
	})

	$('#minimum_charges_menu').change(function(){
		if($(this).val() == ''){
			return false;
		}
		var charge_id = $(this).val();

		if($('#minimum_charge_'+charge_id).length > 0){
			$(this).val('');
			return false;
		}

		var label = $(this).find('option:selected').text();
		var number_of_rows = $('#customer_minimum_charges > tbody tr').length;
		$('#customer_minimum_charges > tbody:last-child').append(
			'<tr>'+
				'<td><input type="hidden" id="minimum_charge_'+charge_id+'" name="minimum_charges['+number_of_rows+'][minimum_charge_id]" value="'+charge_id+'">'+label+'</td>'+
				'<td><input type="date" name="minimum_charges['+number_of_rows+'][date_added]" value="01/01/2016" /></td>'+
				'<td><a href="" class="delete" style="color: red; padding-left: 15px">Delete</a></td>'+
			'</tr>'

		);
//		$('#customer_minimum_charges').append('<li>'+
//			'<input type="hidden" id="minimum_charge_'+charge_id+'" name="minimum_charges[][minimum_charge_id]" value="'+charge_id+'">'+label+
//				'<input type="date" name="minimum_charges[][date_added]" value="01/01/2016" />'+
//				'<a href="" class="delete" style="color: red; padding-left: 15px">Delete</a>'+
//			'</li>');
		$(this).val('');
	});

	$('#customer_minimum_charges a.delete').die().live('click', function(){
		$(this).closest('tr').remove();
		return false;
	});

	$('#groups').change(function(){
		var group_select = $(this);
		var group_id = group_select.val();
		$('#checkbox_holder_'+group_id).show();
		$('#group_checkbox_'+group_id).attr('checked', 'checked');
		group_select.val(0);
		$.colorbox.resize();
	});
	$("input[name='groups[]']").click(function(){
		var group_checkbox = $(this);
		var group_id = group_checkbox.attr('group_id');
		var checked = group_checkbox.attr('checked');
		console.log('gid '+group_id+' checked '+checked);
		if (!checked)
			$('#checkbox_holder_'+group_id).hide();
		$.colorbox.resize();
	})
	$('#passes').change(function(){
		var pass_select = $(this);
		var pass_id = pass_select.val();
		$('#checkbox_holder_'+pass_id).show();
		$('#pass_checkbox_'+pass_id).attr('checked', 'checked');
		pass_select.val(0);
		$.colorbox.resize();
	});
	$("input[name='passes[]']").click(function(){
		var pass_checkbox = $(this);
		var pass_id = pass_checkbox.attr('group_id');
		var checked = pass_checkbox.attr('checked');
		if (!checked)
			$('#checkbox_holder_'+pass_id).hide();
		$.colorbox.resize();
	})
	var submitting = false;
    $('#customer_form').validate({
		submitHandler:function(form)
		{
			<?php if(!empty($field_settings)){
			foreach($field_settings as $key => $field){ 

			if(empty($field['required']) || $field['required'] == 0){
				continue;
			} ?>
				
				if($('#<?php echo $key?>').val() == ''){
					alert('<?php echo $field['name']; ?> is required');
					return;
				}
			<?php } } ?>

			//prevent member and account balances from going below if 0 if a neg balance is not checked
			var invalid_input, invalid_limit, href, is_available, customer_id, username, balance_lower_than_limit;
			$('.customer_balances').each(function(){
				var checkbox = $(this).siblings(),
					is_checked = $(checkbox).attr('checked'),
					amount = parseInt($(this).val());

				if (!is_checked && (amount < 0))
				{
					invalid_input = $(this);
				}
			});
			$('#member_account_limit').each(function(){
				var checkbox = $('input[name=member_account_balance_allow_negative]');
				is_checked = $(checkbox).attr('checked'),
				amount = parseInt($(this).val());
				var member_balance = $('#member_account_balance').val();
				if (is_checked && amount > 0)
				{
					invalid_limit = $(this);
				}
				if (amount < 0 && amount > member_balance)
				{
					balance_lower_than_limit = $(this);
				}
			});
			$('#account_limit').each(function(){
				var checkbox = $('input[name=account_balance_allow_negative]');
				is_checked = $(checkbox).attr('checked'),
				amount = parseInt($(this).val());
				var account_balance = $('#account_balance').val();

				if (is_checked && amount > 0)
				{
					invalid_limit = $(this);
				}
				if (amount < 0 && amount > account_balance)
				{
					balance_lower_than_limit = $(this);
				}
			});

			if (invalid_input){
				set_feedback("<?php echo lang('negative_balance_not_allowed'); ?>",'error_message',false);
				$(invalid_input).select();
				return;
			}
			if (invalid_limit){
				set_feedback("<?php echo lang('customers_account_limit_error'); ?>",'error_message',false);
				$(invalid_limit).select();
				return;
			}
			if (balance_lower_than_limit){
				set_feedback("<?php echo lang('customers_balance_lower_than_limit'); ?>",'error_message',false);
				$(balance_lower_than_limit).select();
				return;
			}

			if ($('#new_member').val() == '1' && !confirm("Account balances from newly added Household members will be transferred to this account. Continue?")) {
				return;
			}
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
                if (!response.success) {
                    set_feedback(response.message,'error_message',false);
                    $(form).unmask();
                    submitting = false;
                    return;
                }
				// If in the tee sheet, add customer to shot gun
				<?php if ($controller == 'teesheets') { ?>
                    var data = {};
                    data.name = $('#last_name').val()+', '+$('#first_name').val();
                    data.phone = $("#phone_number").val();
                    data.email = $('#email').val();
                    data.zip = $('#zip').val();
                    data.price_class = $('#price_class').val();
                    data.last_name = $('#last_name').val();
                    data.person_id = response.person_id;

                    if ($('#type_shotgun').attr('checked')) {
                        console.log('HERE the data we are trying to add---------------------------------');
                        console.dir(data);
                        shotgun.player.add($('#<?=$slot?>'), data);
                        $.colorbox2.close();
                    }
                    else {
                        var tee_time_slot = '<?=$tee_time_slot?>';
                        if (tee_time_slot == 1)
                        {
                            $('#teetime_title').val('');
                            $('#teetime_table input[name=phone]').val('');
                            $('#teetime_table input[name=email]').val('');
                            $('#teetime_table input[name=zip]').val('');
                            $('#teetime_table #price_class_1').val('');
                            $('#teetime_table input[name=person_id]').val('');
                            $('#teetime_title').val(data.name);
                            $('#teetime_table input[name=phone]').val(data.phone);
                            $('#teetime_table input[name=zip]').val(data.zip);
                            $('#teetime_table #price_class_1').val(data.price_class);
                            $('#teetime_table input[name=email]').val(data.email);
                            $('#teetime_table input[name=person_id]').val(response.person_id);
                        }
                        else
                        {
                            $('#teetime_title_'+tee_time_slot).val('');
                            $('#teetime_table input[name=phone_'+tee_time_slot+']').val('');
                            $('#teetime_table input[name=email_'+tee_time_slot+']').val('');
                            $('#teetime_table input[name=zip_'+tee_time_slot+']').val('');
                            $('#teetime_table #price_class_'+tee_time_slot).val('');
                            $('#teetime_table input[name=person_id_'+tee_time_slot+']').val('');
                            $('#teetime_title_'+tee_time_slot).val(data.name);
                            $('#teetime_table input[name=phone_'+tee_time_slot+']').val(data.phone);
                            $('#teetime_table input[name=zip_'+tee_time_slot+']').val(data.zip);
                            $('#teetime_table #price_class_'+tee_time_slot).val(data.price_class);
                            $('#teetime_table input[name=email_'+tee_time_slot+']').val(data.email);
                            $('#teetime_table input[name=person_id_'+tee_time_slot+']').val(response.person_id);
                        }
                        inline_form.set_available_rows();
                        $.colorbox2.close();
                    }
                <?php } else { ?>
                post_person_form_submit(response);
                if ($("#colorbox_flag").val() == '1') {
                    $.colorbox.close();
                };
                <?php } ?>
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			first_name: "required",
			last_name: "required",
			<?php
			if (!$this->permissions->is_employee() && $this->config->item('online_booking_protected'))
			{
			?>
    		username:
			{

				minlength: 5
			},
			password:
			{
				minlength: 8
			},
			repeat_password:
			{
 				equalTo: "#password"
			},
			<?php } ?>
    		email: {
				email: {
					'depends': function (element) {
						return $('#email').val($('#email').val().trim());
					}
				}
			}
   		},
		messages:
		{
     		first_name: "<?php echo lang('common_first_name_required'); ?>",
     		last_name: "<?php echo lang('common_last_name_required'); ?>",
     		<?php
			if (!$this->permissions->is_employee() && $this->config->item('online_booking_protected'))
			{
			?>
     		username:
     		{
     			minlength: "<?php echo lang('employees_username_minlength'); ?>"
     		},
     		password:
			{
				minlength: "<?php echo lang('employees_password_minlength'); ?>"
			},
			repeat_password:
			{
				equalTo: "<?php echo lang('employees_password_must_match'); ?>"
     		},
     		<?php } ?>
     		email: {
				email:"<?php echo lang('common_email_invalid_format'); ?>"
			}
		}
	});
});

</script>
<style>
	#account_number, #email {
		width:230px;	
	}
	#account_limit_info label, #member_account_limit_info label {
		font-size:14px;
	}
	.account_limit, .customer_balances, .discount {
		text-align:right;
	}
	#send_password_change_email {
		color:#ccc;
		cursor:pointer;
	}
	html[xmlns] div.hidden {
		display:none;
	}
	.dates {
		float:right;
	}
	.dates .date_label {
		padding:0px 5px 0px 10px;
	}
	.new_charge_data {
		float:left;
	}
	#new_charge_form {
		font-size: 12px;
		padding: 5px 30px 0px;
	}
	#amount {
		width:60px;
	}
	.first_row {
		margin-bottom:10px;
	}
	#description {

	}
	#subscription {

	}
	#start_date {
		width:85px;
	}
	#save_new_charge {
		float:right;
		margin-top:0px;
	}
	fieldset div.field_row label {
    float: left;
    padding: 5px;
    width: 175px !important;
}
</style>
