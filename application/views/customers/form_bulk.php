<?php
echo form_open('customers/bulk_update/',array('id'=>'customer_form'));
//print_r($price_classes);
?>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("customers_basic_information"); ?></legend>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>
                <?php echo form_checkbox('member', '1', !isset($person_info) || $person_info->member == '' ? FALSE : (boolean)$person_info->member);?>
                <?php echo form_label(lang('customers_member').':', 'member'); ?>
            </label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">

        </div>
    </div>
</div>

<div class="field_row clearfix">	
	<div class='form_field'>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('customers_price_class').':', 'price_class'); ?>
	<div class='form_field'>
	<?php 
		echo form_dropdown('price_class', $price_classes, isset($person_info->price_class)?$person_info->price_class:'');
	?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('customers_taxable').':', 'taxable'); ?>
	<div class='form_field'>
	<?php echo form_checkbox('taxable', '1', !isset($person_info) || $person_info->taxable == '' ? TRUE : (boolean)$person_info->taxable);?>
	</div>
</div>

<?php 
$first_label = true;
$group_array = array('0'=>'Select Group');
$group_checkboxes = '';
foreach($groups as $group)
{
	$group_array[$group['group_id']] = $group['label'];
	$group_checkboxes .= "<div id='checkbox_holder_".$group['group_id']."'class='float_left field_row clearfix ".((isset($group['person_id']) && $group['person_id'] != '' && $group['person_id'] == $person_info->person_id)?'':'hidden')."'>".
			form_label('', 'groups').
			"<div class='form_field'>".
				form_checkbox('groups[]', $group['group_id'], (isset($group['person_id'] ) && $group['person_id'] != '' && $group['person_id'] == $person_info->person_id) ? true:FALSE, "id='group_checkbox_{$group['group_id']}' group_id='{$group['group_id']}'").' '.$group['label'].
			"</div>
		</div>";

	$first_label = false;
} ?>


<div class="field_row clearfix">	
<?php echo form_label(lang('customers_add_to_groups').':', 'groups');  ?>

	<div class='form_field'>
	<?php 		
		echo form_dropdown('add_groups', $group_array, '');
	?>
	
	</div>
</div>

<div class='group_checkboxes  group_add'>
	<?= $group_checkboxes?>
	<div class="clear"></div>
</div>

<!-- CODE TO DO REMOVE FROM GROUPS -->
<?php 
$first_label = true;
$group_array = array('0'=>'Select Group');
$group_checkboxes = '';
foreach($groups as $group)
{
	$group_array[$group['group_id']] = $group['label'];
	$group_checkboxes .= "<div id='remove_checkbox_holder_".$group['group_id']."'class='float_left field_row clearfix ".((isset($group['person_id']) && $group['person_id'] != '' && $group['person_id'] == $person_info->person_id)?'':'hidden')."'>".
			form_label('', 'groups').
			"<div class='form_field'>".
				form_checkbox('remove_groups[]', $group['group_id'], (isset($group['person_id']) && $group['person_id'] != '' && $group['person_id'] == $person_info->person_id) ? true:FALSE, "id='remove_group_checkbox_{$group['group_id']}' group_id='{$group['group_id']}'").' '.$group['label'].
			"</div>
		</div>";

	$first_label = false;
} ?>


<div class="field_row clearfix">	
<?php echo form_label(lang('customers_remove_from_groups').':', 'groups');  ?>

	<div class='form_field'>
	<?php 		
		echo form_dropdown('remove_groups', $group_array, '');
	?>
	
	</div>
</div>

<div class='group_checkboxes  group_remove'>
	<?= $group_checkboxes?>
	<div class="clear"></div>
</div>
<!-- END OF CODE TO DO REMOVE FROM GROUPS -->
<hr />
	<legend>Online Booking</legend>
	<div class="field_row clearfix">
		<?php echo form_label("Activate"); ?>
		<div class='form_field'>
			<?php echo form_checkbox('activate_online_booking', '1');?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label("Password"); ?>
		<div class='form_field'>
			<?php echo form_input('online_booking_password');?>
		</div>
	</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	
	var submitting = false;
    $('#customer_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			if(confirm("<?php echo lang('customers_confirm_bulk_edit') ?>"))
			{
				//Get the selected ids and create hidden fields to send with ajax submit.
				var selected_customer_ids=get_selected_values();
				for(k=0;k<selected_customer_ids.length;k++)
				{
					$(form).append("<input type='hidden' name='customer_ids[]' value='"+selected_customer_ids[k]+"' />");
				}
								
				submitting = true;
	            $(form).mask("<?php echo lang('common_wait'); ?>");
	            $(form).ajaxSubmit({
					success:function(response)
					{
						$.colorbox.close();
						post_bulk_form_submit(response);
	                    submitting = false;
					},
					dataType:'json'
				});
			}			
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			first_name: "required",
			last_name: "required",
    		email: "email"
   		},
		messages: 
		{
     		first_name: "<?php echo lang('common_first_name_required'); ?>",
     		last_name: "<?php echo lang('common_last_name_required'); ?>",
     		email: "<?php echo lang('common_email_invalid_format'); ?>"
		}
	});

	$('#add_groups').change(function(){
		var select_box = $(this);
		var group_id = $(this).val();
		console.log(group_id);
		$('#checkbox_holder_'+group_id).removeClass('hidden');		
		$('#group_checkbox_'+group_id).attr('checked', 'checked');
		select_box.val(0);
		$.colorbox.resize();
	});
	$('#remove_groups').change(function(){
		var select_box = $(this);
		var group_id = $(this).val();
		console.log(group_id);
		$('#remove_checkbox_holder_'+group_id).removeClass('hidden');		
		$('#remove_group_checkbox_'+group_id).attr('checked', 'checked');
		select_box.val(0);
		$.colorbox.resize();
	});
	$('input[name=groups[]]').click(function(){
		var group_checkbox = $(this);
		var group_id = group_checkbox.attr('group_id');
		var checked = group_checkbox.attr('checked');
		console.log('gid '+group_id+' checked '+checked);
		if (!checked)
			$('#checkbox_holder_'+group_id).addClass('hidden');
		$.colorbox.resize();
	});
	$('input[name=remove_groups[]]').click(function(){
		var group_checkbox = $(this);
		var group_id = group_checkbox.attr('group_id');
		var checked = group_checkbox.attr('checked');
		console.log('gid '+group_id+' checked '+checked);
		if (!checked)
			$('#remove_checkbox_holder_'+group_id).addClass('hidden');
		$.colorbox.resize();
	});
});
</script>