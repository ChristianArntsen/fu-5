<form id="merge_form" action="index.php/customers/merge" method="POST">
    <table>
        <tbody>
            <tr>
                <td>

                </td>
                <td>
                    <div id="c1_header">
                        <input type="text" id="customer_1_search" class="customer_search" data-customer-position="1" placeholder="Primary Customer"/>
                        <input type="hidden" id="person_id_1" name="person_id_1"/>
                    </div>
                </td>
                <td>
                    <div id="c2_header">
                        <input type="text" id="customer_2_search" class="customer_search" data-customer-position="2" placeholder="Duplicate Customer"/>
                        <input type="hidden" id="person_id_2" name="person_id_2"/>
                    </div>
                </td>
            </tr>
            <tr class="section_start">
                <td class="section_title">PERSONAL INFO</td>
                <td class="centered"><input type="radio" name="personal_info_box" data-section="personal" class='section_checkbox' value="1"/></td>
                <td class="centered"><input type="radio" name="personal_info_box" data-section="personal" class='section_checkbox' value="2"/></td>
            </tr>
            <tr>
                <td>
                    Name
                </td>
                <td>
                    <input type="radio" name="name_box" id="name_box_1" class="merge_option" value="1"/> <input type="text" id="name_1" disabled>
                </td>
                <td>
                    <input type="radio" name="name_box" id="name_box_2" class="merge_option" value="2"/> <input type="text" id="name_2" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    Email
                </td>
                <td>
                    <input type="radio" name="email_box" id="email_box_1" class="merge_option" value="1"/> <input type="text" id="email_1" disabled>
                </td>
                <td>
                    <input type="radio" name="email_box" id="email_box_2" class="merge_option" value="2"/> <input type="text" id="email_2" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    Phone
                </td>
                <td>
                    <input type="radio" name="phone_box" id="phone_box_1" class="merge_option" value="1"/> <input type="text" id="phone_1" disabled>
                </td>
                <td>
                    <input type="radio" name="phone_box" id="phone_box_2" class="merge_option" value="2"/> <input type="text" id="phone_2" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    Address
                </td>
                <td>
                    <input type="radio" name="address_box" id="address_box_1" class="merge_option" value="1"/> <input type="text" id="address_1" disabled>
                </td>
                <td>
                    <input type="radio" name="address_box" id="address_box_2" class="merge_option" value="2"/> <input type="text" id="address_2" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    Comments
                </td>
                <td>
                    <input type="radio" name="comment_box" id="comment_box_1" class="merge_option" value="1"/> <input type="text" id="comment_1" disabled>
                </td>
                <td>
                    <input type="radio" name="comment_box" id="comment_box_2" class="merge_option" value="2"/> <input type="text" id="comment_2" disabled>
                </td>
            </tr>
            <!--tr>
                <td>
                    Comments
                </td>
                <td>
                    <input type="radio" name="comment_box" id="comment_box_1" class="merge_option" value="1"/> <input type="text" id="comment_1" disabled>
                </td>
                <td>
                    <input type="radio" name="comment_box" id="comment_box_2" class="merge_option" value="2"/> <input type="text" id="comment_2" disabled>
                </td>
            </tr-->

            <!--
            Currently Skipping
            Secondary Phone
            Birthday
            Image
            Marketing
            Status
            -->
            <tr class="section_start">
                <td class="section_title">ACCOUNT INFO</td>
                <td class="centered"><input type="radio" name="account_info_box" data-section="account" class='section_checkbox' value="1"/></td>
                <td class="centered"><input type="radio" name="account_info_box" data-section="account" class='section_checkbox' value="2"/></td>
            </tr>
            <tr>
                <td>
                    Account Number
                </td>
                <td>
                    <input type="radio" name="account_number_box" id="account_number_box_1" class="merge_option" value="1"/> <input type="text" id="account_number_1" disabled>
                </td>
                <td>
                    <input type="radio" name="account_number_box" id="account_number_box_2" class="merge_option" value="2"/> <input type="text" id="account_number_2" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    Price Class
                </td>
                <td>
                    <input type="radio" name="price_class_box" id="price_class_box_1" class="merge_option" value="1"/> <input type="text" id="price_class_1" disabled>
                </td>
                <td>
                    <input type="radio" name="price_class_box" id="price_class_box_2" class="merge_option" value="2"/> <input type="text" id="price_class_2" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    Discount
                </td>
                <td>
                    <input type="radio" name="discount_box" id="discount_box_1" class="merge_option" value="1"/> <input type="text" id="discount_1" disabled>
                </td>
                <td>
                    <input type="radio" name="discount_box" id="discount_box_2" class="merge_option" value="2"/> <input type="text" id="discount_2" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    Non-taxable
                </td>
                <td>
                    <input type="radio" name="non_taxable_box" id="non_taxable_box_1" class="merge_option" value="1"/> <span id="non_taxable_1" >-</span>
                </td>
                <td>
                    <input type="radio" name="non_taxable_box" id="non_taxable_box_2" class="merge_option" value="2"/> <span id="non_taxable_2" >-</span>
                </td>
            </tr>
            <tr>
                <td>
                    Member
                </td>
                <td>
                    <input type="radio" name="member_box" id="member_box_1" class="merge_option" value="1"/> <span id="member_1" >-</span>
                </td>
                <td>
                    <input type="radio" name="member_box" id="member_box_2" class="merge_option" value="2"/> <span id="member_2" >-</span>
                </td>
            </tr>
            <tr>
                <td>
                    Food Minimum
                </td>
                <td>
                    <input type="radio" name="food_minimum_box" id="food_minimum_box_1" class="merge_option" value="1"/> <span id="food_minimum_1" >-</span>
                </td>
                <td>
                    <input type="radio" name="food_minimum_box" id="food_minimum_box_2" class="merge_option" value="2"/> <span id="food_minimum_2" >-</span>
                </td>
            </tr>


            <!-- GROUPS AND PASSES -->
            <tr class="section_start">
                <td class="section_title">GROUPS and PASSES</td>
                <td class="centered"></td>
                <td class="centered"><input type="checkbox" name="groups_and_passes_box" data-section="groups_and_passes" class='section_checkbox' value="2"/></td>
            </tr>
            <tr>
                <td>
                    Passes
                </td>
                <td>
                    <!--input type="checkbox" name="passes_box_1" id="passes_box_1" class="merge_option" value="1"/--> <span class="checkbox_placeholder"></span><span id="passes_1" >-</span>
                </td>
                <td>
                    <input type="checkbox" name="passes_box_2" id="passes_box_2" class="merge_option" value="2"/> <span id="passes_2" >-</span>
                </td>
            </tr>
            <tr>
                <td>
                    Groups
                </td>
                <td>
                    <!--input type="checkbox" name="groups_box_1" id="groups_box_1" class="merge_option" value="1"/--> <span class="checkbox_placeholder"></span><span id="groups_1" >-</span>
                </td>
                <td>
                    <input type="checkbox" name="groups_box_2" id="groups_box_2" class="merge_option" value="2"/> <span id="groups_2" >-</span>
                </td>
            </tr>
            <!-- MISCELLANEOUS SECTION -->
            <tr class="section_start">
                <td class="section_title">MISCELLANEOUS</td>
                <td class="centered"></td>
                <td class="centered"><span class="red">* Required</span><!--input type="checkbox" name="miscellaneous_box" data-section="miscellaneous" class='section_checkbox' value="2"/--></td>
            </tr>
            <tr>
                <td>
                    Update Old Sales
                </td>
                <td>
                </td>
                <td class="centered">
                    <input type="checkbox" name="update_sales_box" id="update_sales_box_2" class="merge_option" value="2" disabled checked/><span class="red">*</span>
                </td>
            </tr>
            <tr>
                <td>
                    Update Tee Times
                </td>
                <td>
                </td>
                <td class="centered">
                    <input type="checkbox" name="update_tee_times_box" id="update_tee_times_box_2" class="merge_option" value="2" disabled checked/><span class="red">*</span>
                </td>
            </tr>
            <tr>
                <td>
                    Transfer Household Members
                </td>
                <td>
                </td>
                <td class="centered">
                    <input type="checkbox" name="transfer_household_members_box" id="transfer_household_members_box_2" class="merge_option" value="2" disabled checked/><span class="red">*</span>
                </td>
            </tr>
            <tr>
                <td>
                    Transfer Gift Cards
                </td>
                <td>
                </td>
                <td class="centered">
                    <input type="checkbox" name="transfer_gift_cards_box" id="transfer_gift_cards_box_2" class="merge_option" value="2" disabled checked/><span class="red">*</span>
                </td>
            </tr>
            <tr>
                <td>
                    Transfer Punch Cards
                </td>
                <td>
                </td>
                <td class="centered">
                    <input type="checkbox" name="transfer_punch_cards_box" id="transfer_punch_cards_box_2" class="merge_option" value="2" disabled checked/><span class="red">*</span>
                </td>
            </tr>
            <tr>
                <td>
                    Transfer Credit Cards
                </td>
                <td>
                </td>
                <td class="centered">
                    <input type="checkbox" name="transfer_credit_cards_box" id="transfer_credit_cards_box_2" class="merge_option" value="2" disabled checked/><span class="red">*</span>
                </td>
            </tr>
            <tr>
                <td>
                    Transfer Invoices/Recurring Billings
                </td>
                <td>
                </td>
                <td class="centered">
                    <input type="checkbox" name="transfer_invoices_box" id="transfer_invoices_box_2" class="merge_option" value="2" disabled checked/><span class="red">*</span>
                </td>
            </tr>
            <tr>
                <td>
                    Transfer Loyalty Points
                </td>
                <td>
                </td>
                <td class="centered">
                    <input type="checkbox" name="transfer_loyalty_box" id="transfer_loyalty_box_2" class="merge_option" value="2" disabled checked/><span class="red">*</span>
                </td>
            </tr>
            <tr>
                <td>
                    Transfer <?=$this->config->item('member_balance_nickname');?>
                </td>
                <td>
                    <!--input type="checkbox" name="member_account_balance_box_1" id="member_account_balance_box_1" class="merge_option" value="1"/--><span class="checkbox_placeholder"></span><span id="member_account_balance_1">-</span>
                </td>
                <td>
                    <input type="checkbox" name="member_account_balance_box_2" id="member_account_balance_box_2" class="merge_option" value="2" disabled checked/><span class="red">*</span> <input type="text" id="member_account_balance_2" disabled>
                </td>
            </tr>
            <tr>
                <td>
                    Transfer <?=$this->config->item('customer_credit_nickname');?>
                </td>
                <td>
                    <!--input type="checkbox" name="customer_account_balance_box_1" id="customer_account_balance_box_1" class="merge_option" value="1"/--> <span class="checkbox_placeholder"></span><span id="customer_account_balance_1">-</span>
                </td>
                <td>
                    <input type="checkbox" name="customer_account_balance_box_2" id="customer_account_balance_box_2" class="merge_option" value="2" disabled checked/><span class="red">*</span> <input type="text" id="customer_account_balance_2" disabled>
                </td>
            </tr>
            <!-- SAVE BUTTON -->
            <tr>
                <td colspan="3">
                    <input type="submit" name='run_merge' id='run_merge' value="Merge" class="submit_button"/>
                </td>
            </tr>

        </tbody>
    </table>
</form>
<script>
    var customer_merge = {
        options : {
            personal : {
                sections : [
                    'name',
                    'email',
                    'phone',
                    'address',
                    'comment'
                ]
            },
            account : {
                sections : [
                    'account_number',
                    'price_class',
                    'discount',
                    'non_taxable',
                    'member',
                    'food_minimum'
                ]
            },
            groups_and_passes : {
                sections : [
                    'groups',
                    'passes'
                ]
            },
            miscellaneous : {
                sections : [
                    'online_credentials',
                    'household',
                    'update_sales',
                    'update_tee_times',
                    'transfer_gift_cards',
                    'transfer_punch_cards',
                    'transfer_credit_cards',
                    'transfer_invoices',
                    'member_account_balance',
                    'customer_account_balance'
                ]
            }
        },
        check_section:function(target){
            var section = $(target).attr('data-section');
            var options = customer_merge.options[section].sections;
            var side = $(target).val();

            for (var i in options) {
                console.log('#'+options[i]+'_'+side);
                var checkbox = $('#'+options[i]+'_box_'+side);
                checkbox.click();//.attr('checked', true);
                customer_merge.highlight_cell(checkbox);
            }
        },
        highlight_cell:function (target){
            $(target).parent().parent().find('input').parent().css('backgroundColor','rgb(206, 154, 154)');
            $(target).parent().parent().find('input:checked').parent().css('backgroundColor','rgb(128, 218, 131)');
        },
        populate_customer:function(position, customer_info) {
            $('#person_id_'+position).val(customer_info.person_id);
            $('#name_'+position).val(customer_info.last_name+', '+customer_info.first_name);
            $('#email_'+position).val(customer_info.email);
            $('#phone_'+position).val(customer_info.phone_number);
            $('#address_'+position).val(customer_info.address_1+' '+customer_info.city+', '+customer_info.state+' '+customer_info.zip);
            $('#account_number_'+position).val(customer_info.account_number);
            $('#price_class_'+position).val(customer_info.price_class);
            $('#discount_'+position).val(customer_info.discount);
            $('#non_taxable_'+position).html(customer_info.taxable ? 'No' : 'Yes');
            $('#member_'+position).html(customer_info.member ? 'Yes' : 'No');
            $('#food_minimum_'+position).val(customer_info.phone_number);
            $('#member_account_balance_'+position).val(customer_info.member_account_balance);
            $('#customer_account_balance_'+position).val(customer_info.account_balance);
            $('#comment_'+position).val(customer_info.comments);
            // Concat Passes
            var passes = '';
            for (var i in customer_info.passes) {
                passes += customer_info.passes[i].label+', ';
            }
            $('#passes_'+position).html(passes);
            // Concat Groups
            var groups = '';
            for (var i in customer_info.groups) {
                groups += customer_info.groups[i].label+', ';
            }
            $('#groups_'+position).html(groups);
            $('#online_credentials_'+position).val(customer_info.username);
            $('#household_'+position).val(customer_info.phone_number);

            if (position == 1) {
                // Check all boxes
                var section_checkboxes = $('.section_checkbox');
                for (var i in section_checkboxes) {
                    if (Number.isInteger(parseInt(i)) && $(section_checkboxes[i]).val() == 1) {
                        customer_merge.check_section(section_checkboxes[i]);
                    }
                }
            }

        }
    };
    var submitting = false;
    $(document).ready(function(){
        // Initialize Section Checkboxes
        $('.section_checkbox').off('click').on('click', function(){
            customer_merge.check_section($(this));
        });
        $('.merge_option').off('click').on('click', function(){
            customer_merge.highlight_cell($(this));
        });
        // Initialize Searches
        $('.customer_search').autocomplete({
            source: '/index.php/customers/customer_search?term='+$(this).val(),
            delay: 200,
            autoFocus: false,
            minLength: 1,
            select: function( event, ui )
            {
                var cust_pos = $(this).attr('data-customer-position');
                //$('#merge_form').mask("<?php echo lang('common_wait'); ?>");
                // Populate customer 1 info
                $.ajax({
                    type: "POST",
                    url: 'index.php/customers/get_info/'+ui.item.value,
                    data: '',
                    success: function(response){
                        console.log('returned customer info');
                        console.dir(response);
                        customer_merge.populate_customer(cust_pos, response.customer_info);
                        $('#merge_form').unmask();
                    },
                    dataType:'json'
                });
            }
        });
        // Initialize Form
        $('#merge_form').validate({
            submitHandler:function(form)
            {
                var customer_name = $('#name_1').val();
                var person_1 = $('#person_id_1').val();
                var person_2 = $('#person_id_2').val();
                // Do we have 2 customers selected?
                if (!(person_1 != '' && person_2 != '' && person_1 != person_2)) {
                    alert("Please select two individuals you would like to merge.");
                    return;
                }

                if (confirm("You are about to update customer: "+customer_name+" and delete the duplicate account. You cannot undo this action. Are you sure you want to proceed?")) {

                    if (submitting) {
                        return;
                    }

                    submitting = true;
                    $(form).mask("<?php echo lang('common_wait'); ?>");
                    $(form).ajaxSubmit({
                        success: function (response) {
                            if (!response.success) {
                                $(form).unmask();
                                // Alert message that the accounts were not successfully merged
                                set_feedback(response.message, 'error_message', true);
                            }
                            else {
                                $.colorbox({'href':'index.php/customers/view/'+person_1,'title':'Update Customer','width':1100});
                                // Alert message that the accounts were successfully merged
                                set_feedback('The customers have been successfully merged', 'success_message', false);
                            }

                            submitting = false;
                        },
                        dataType: 'json'
                    });
                }
                else {
                    submitting = false;
                    $(form).unmask();

                }
            },
            errorLabelContainer: "#error_message_box",
            wrapper: "li",
            rules:{},
            messages:{}
        });
    });
</script>
<style>
    #merge_form {
        padding:15px;
    }
    #merge_form table {
        width:920px;
    }
    .centered {
        text-align:center;
    }
    .section_start {
        border-top:2px #797979 solid;
    }
    .section_title {
        font-size:20px;
    }
    #merge_form table tr td {
        height:34px;
        padding:0px 10px;
    }
    #merge_form table tr td input[type=text]{
        width:300px;
    }
    #merge_form table tr td span {
        text-align:center;
        width:300px;
        display:inline-block;
    }
    #merge_form table tr td span.red {
        text-align:center;
        width:auto;
        display:inline-block;
    }
    #merge_form table tr td span.checkbox_placeholder {
        width:17px;
    }
    #run_merge {
        cursor:pointer;
    }
</style>