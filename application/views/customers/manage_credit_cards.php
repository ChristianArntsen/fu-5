<form>
<ul id="error_message_box"></ul>
<fieldset id="customer_credit_card_info">
	<table>
		<tbody>
        <?php if (!empty($tee_sheet) && $tee_sheet == true) { ?>
            <tr>
                <td colspan=6 class='customer_name' style="font-size:20px;">Charge No Show Fees Only</td>
            </tr>
        <?php } ?>
            <tr>
                <td colspan=6 class='customer_name'>Customer: <?=$customer->first_name?> <?=$customer->last_name?></td>
            </tr>
			<tr class='credit_card_info credit_card_header'>
				<td>Card</td>
				<td>Status</td>
				<td class='centered'>Charges</td>
				<td class='centered'>Failures</td>
				<td></td>
                <td></td>
			</tr>
			<?php foreach($credit_cards as $credit_card) { ?>
			<tr class='credit_card_info' id='credit_card_row_<?=$credit_card['credit_card_id']?>'>
				<td><?=$credit_card['card_type']?> <?=$credit_card['masked_account']?></td>
				<td><?=$credit_card['status'] == '' ? 'Good' : $credit_card['status'] ?></td>
				<td class='centered'><?=$credit_card['successful_charges']?></td>
				<td class='centered'><?=$credit_card['failed_charges']?></td>
                <td><span class='remove_credit_card' data-credit-card-id='<?=$credit_card['credit_card_id']?>'>Delete</span></td>
                <?php if (!empty($tee_sheet) && $tee_sheet == true) { ?>
                <td><span class='charge_no_show' data-credit-card-id='<?=$credit_card['credit_card_id']?>'>Charge No Show</span></td>
                <?php } else { ?>
                <td></td>
                <?php } ?>
			</tr>
			<?php } ?>
			<tr class='credit_card_info'>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><span class='add_credit_card'>Add New Card</span></td>
                <td></td>
			</tr>
		</tbody>
	</table>
	<?php
	echo form_submit(array(
		'name'=>'close',
		'id'=>'close_history',
		'value'=>lang('common_close'),
		'class'=>'submit_button float_right')
	);
	?>
	</div>
</fieldset>
</form>
<script>
	$(document).ready(function(){
		$('#close_history').click(function(e){
			e.preventDefault();
			$.colorbox2.close();
		});
		$('.add_credit_card').click(function(){
			// OPEN CREDIT CARD WINDOW
			$.colorbox2({'href':'index.php/customers/open_add_credit_card_window/<?=$customer_id?>', 'width':650});
			// ONCOMPLETE RELOAD WINDOW
		});
		$('.remove_credit_card').click(function(){
			// WARN ABOUT BILLINGS WITH THIS CREDIT CARD ATTACHED
			if (confirm('You are about to remove this credit card from this customer account as well as from any associated billing accounts. Are you sure you want to continue?'))
			{
				var credit_card_id = $(this).attr('data-credit-card-id');
				// REMOVE FROM BILLINGS AND MARK AS DELETED
				$.ajax({
		           type: "POST",
		           url: "index.php/customers/delete_credit_card/<?=$customer_id?>/"+credit_card_id,
		           data: '',
		           success: function(response){
		           		console.dir(response);
						// REMOVE FROM HTML
						if (response.success)
							$('#credit_card_row_'+credit_card_id).remove();
				    },
		            dataType:'json'
		         });
			}
		});
        $('.charge_no_show').click(function(){
            var tee_time_id = $('#teetime_id').val();
            var credit_card_id = $(this).attr('data-credit-card-id');
            $.colorbox2({href:'index.php/teesheets/view_charge_card/'+tee_time_id+'/'+credit_card_id, width:500, onComplete:function(){
//                $('#credit_card_label').html($('#credit_card_id option:selected').text());
//                $('#card_to_charge').val(card_to_charge);
//                $('#charge_person_id').val($('#person_id').val());
//                $('#tee_time_id').val(teetime_info.TTID);
            }});
        });
	});
</script>
<style>
	fieldset#customer_credit_card_info div.field_row label {
		width: 305px;
	}
	#close_history {
		margin-bottom: 10px;
	}
	.add_credit_card, .remove_credit_card, .charge_no_show {
		color:blue;
		cursor:pointer;
		font-size:12px;
	}
	.credit_card_info td {
		padding:5px 10px;
	}
	.credit_card_header td {
		font-weight:bold;
	}
	.customer_name {
		padding: 5px 10px;
		font-size: 16px;
		font-weight: bold;
	}
	.centered {
		text-align:center;
	}
</style>