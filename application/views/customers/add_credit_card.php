<div class="colorbox" style="height: 200px; background-color: #EFEFEF">
	<form class="add_credit_card" style="padding: 20px;" action="<?php echo site_url('customers/card_captured'); ?>" autocomplete="off">
		<h1 style="margin-bottom: 5px;">Swipe Card...</h1>
		<input type="password" name="track_data" class="track_data" style="width: 200px; padding: 8px 4px" autocomplete="off" value="">
		<input type="hidden" name="person_id" class="person_id" value="<?php echo (int) $person_id; ?>">
	</form>
</div>
<script>
$(function(){
	setTimeout(function(){ 
		$('input.track_data').focus().val('');
	}, 50);
	
	$('form.add_credit_card').on('submit', function(e){
		
		var url = $(this).attr('action');
		var track_data = $('input.track_data').val();
		var person_id = $('input.person_id').val(); 
		url += '/' + person_id;
		$('#cbox2LoadedContent').mask('Please wait...');
		
		$.post(url, {'track_data': track_data}, function(response){
			$('#cbox2LoadedContent').unmask();
			
			if(response.succcess == false){
				alert(response.msg);
				$('input.track_data').val('');
				
			}else{
				reload_customer_credit_cards(0, <?=(int) $person_id?>);
			}
		});
		
		return false;
	});
});
</script>
