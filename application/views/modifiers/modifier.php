<tr data-modifier-id="<?php echo $modifier['modifier_id']; ?>">
	<td>
		<div class='form_field name'>
			<span class="data" data-name="name"><?php echo $modifier['name']; ?></span>
		</div>
	</td>
	<td>
		<div class='form_field options'>
			<span class="data" data-name="options" data-options="<?=json_encode($modifier['options']);?>">
			<?php if(!empty($modifier['options'])){ ?>
			<?php foreach ($modifier['options'] as $option){
				echo '<span class="option-label">'.$option['label'].'</span>'.($option['price'] != '<span class="option-price"></span>' ? ' ($<span class="option-price">'.$option['price'].'</span>)' : '').',  ';
			} } ?>
			</span>
		</div>
	</td>				
	<td>
		<div class='form_field multi-select'>
			<span class="data" data-name="multi_select" data-multi-select="<?=$modifier['multi_select']?>"><?php if($modifier['multi_select'] == 1){ ?>Yes<?php }else{ ?>No<?php } ?></span>
		</div>
	</td>		
	<td>
		<div class='form_field price'>
			<?php $category_array = array('1'=>'Customize', '2'=>'Condiments', '3'=>'Cook Temp');?>
			<span class="data" data-name="category_id" data-category-id="<?=$modifier['category_id']?>"><?php echo $category_array[$modifier['category_id']]; ?></span>
		</div>
	</td>
	<td>
		<div class='form_field price'>
			<span class="data" data-name="required" data-required='<?=$modifier['required']?>'><?php echo $modifier['required'] ? 'Yes' : 'No'; ?></span>
		</div>
	</td>
	<td>
		<a href="#" class="edit_modifier">Edit</a>
	</td>			
	<td>
		<a href="#" class="delete_modifier" style="color: red;">X</a>
	</td>
</tr>
