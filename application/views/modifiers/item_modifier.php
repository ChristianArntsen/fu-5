<?php
$optionsArray = '';
$optionsMenu = array('' => '- None -');
foreach($modifier['options'] as $opt){
	$optionsArray[] = $opt['label'].' ('.to_currency((float) $opt['price']).')';
	$optionsMenu[$opt['label']] = $opt['label'];
}
$optionsString = implode(', ',$optionsArray);
$default_options = $modifier['default']; 

if(empty($modifier['order'])){
	$modifier['order'] = 0;
}
?>
<tr>
	<td class="sort-handle">
		<input type="hidden" class="order" name="modifiers[<?php echo $modifier['modifier_id']; ?>][order]" value="<?php echo $modifier['order']; ?>">
		<div class="sort-button">
			<span class="ui-button-icon-primary ui-icon ui-icon-carat-2-n-s"></span>
		</div>
	</td>	
	<td>
		<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][modifier_id]" value="<?php echo $modifier['modifier_id']; ?>" class="modifier_id" />
		<div class='form_field'>
			<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][name]", 'value' => $modifier['name'], 'style' => 'width: 200px;', 'class'=>'modifier_name', 'disabled'=>false));?>
		</div>
	</td>
	<td>
		<div class='form_field'>
			<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][options]", 'value' => $optionsString, 'style' => 'width: 450px;', 'class'=>'modifier_options', 'disabled'=>false));?>
		</div>
	</td>
	<td>
		<div class='form_field'>
			<?php if($modifier['multi_select'] == 1){
			foreach($modifier['options'] as $option){
				
				$checked = false;
				if(!empty($default_options) && in_array($option['label'], $default_options)){
					$checked = true;
				} ?>
				<label style="display: block; height: 20px; line-height: 20px;">
					<?php echo form_checkbox("modifiers[{$modifier['modifier_id']}][default][]", $option['label'], $checked); ?>
					<?php echo $option['label']; ?>
				</label>
			
			<?php } }else{ ?>
			<?php echo form_dropdown("modifiers[{$modifier['modifier_id']}][default]", $optionsMenu, $modifier['default'], "style='width: 150px;'"); ?>
			<?php } ?>
		</div>
	</td>
	<td>
		<a href="#" class="delete_modifier" style="color: white;">X</a>
	</td>
</tr>