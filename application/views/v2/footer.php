<?php
if(defined('JASMINE') && JASMINE) {
?>
    <script src="/js/jasmine-standalone-2.4.1/lib/jasmine-2.4.1/jasmine.js"></script>
    <script src="/js/t/boot.js"></script>
<?php
}
?>

<script src="<?php echo base_url(); ?>js/dist/main.min.js?<?php echo APPLICATION_VERSION; ?>"></script>

<?php
// TODO: build a separate spec test file in gulp
    if(defined('JASMINE') && JASMINE === true) {
?>

        <script src="/js/backbone-json-rest/t/examples.js"></script>
        <script src="/js/backbone-json-rest/t/json-rest-validator-spec.js"></script>
        <script src="/js/backbone-json-rest/t/backbone-json-rest-spec.js"></script>
        <script src="/frontend/main/modules/bulk_edit_support/models/t/employee_audit_log-spec.js"></script>
        <script src="/frontend/main/models/customers/t/customer-spec.js"></script>
        <script src="/frontend/main/models/foodbev/t/table-spec.js"></script>
<?php
}
?>

<?php $this->load->view('partial/quickkeys'); ?>
</body>
</html>
