<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>ForeUp</title>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,500' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url(); ?>css/dist/main.min.css?<?php echo APPLICATION_VERSION; ?>" type="text/css" rel="stylesheet" />
	<link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/ndcdpiikdnlagfjejimgpmaflngcemoo">
    <link rel="shortcut icon" href="/favicon.ico">
	<!-- Google Analytics -->

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-101056671-1', 'auto');
	  ga('send', 'pageview');
	</script>

	<!-- Begin Inspectlet Embed Code -->

    <?php if ($this->config->item('load_3rd_party_tools')){ ?>
		<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){

				f[z]=function(){
					(a.s=a.s||[]).push(arguments)};var a=f[z]._={
				},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
					f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
					0:+new Date};a.P=function(u){
					a.p[u]=new Date-a.p[0]};function s(){
					a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
					hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
					return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
					b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
					b.contentWindow[g].open()}catch(w){
					c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
					var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
					b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
				loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
			/* custom configuration goes here (www.olark.com/documentation) */
			olark.identify('9651-903-10-1533');/*]]>*/</script>
		<script>
			olark('api.visitor.updateFullName', {
				fullName: "<?=$this->session->userdata('course_name') ?> - <?=$user_info->first_name?> <?=$user_info->last_name?>"
			});
			olark('api.visitor.updateEmailAddress', {
				emailAddress: '<?=$this->session->userdata('email') ?>'
			});
			olark('api.visitor.updatePhoneNumber', {
				phoneNumber: '<?=$user_info->phone_number ?>'
			});
		</script>
        <script type='text/javascript'>
            window.__lo_site_id = 59558;
            window.__wtw_custom_user_data = {};
            window.__wtw_custom_user_data.course = "<?=$this->session->userdata('course_id') ?>";
            window.__wtw_custom_user_data.terminal_id = "<?=$this->session->userdata('terminal_id') ?>";
            window.__wtw_custom_user_data.course_name = "<?=$this->session->userdata('course_name') ?>";
            window.__wtw_custom_user_data.email = "<?=$this->session->userdata('email') ?>";
            (function() {
                var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
                wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
            })();
        </script>
		<style>
			div.uv-icon {
				bottom:0px !important;
                right:-1000px !important;
			}
            .uv-icon svg {
                opacity:0;
                width:0px;
                height:0px;
                right:-1000px !important;
            }
		</style>
		<script>
			// Include the UserVoice JavaScript SDK (only needed once on a page)
			UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/uouQNcbVRYcnPbJP28TMAA.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();
			//
			// UserVoice Javascript SDK developer documentation:
			// https://www.uservoice.com/o/javascript-sdk
			//

			// Set colors
			UserVoice.push(['set', {
				accent_color: '#448dd6',
				trigger_color: 'white',
				trigger_background_color: '#448dd6'
			}]);
			// Identify the user and pass traits
			// To enable, replace sample data with actual user traits and uncomment the line
			UserVoice.push(['identify', {
				email:      '<?=$this->session->userdata('email') ?>', // User’s email address
				name:       "<?=$user_info->last_name !== null ? $user_info->first_name." ".$user_info->last_name: " " ?>", // User’s real name
				//created_at: 1364406966, // Unix timestamp for the date the user signed up
				id:         '<?=$this->session->userdata('emp_id') ?>', // Optional: Unique id of the user (if set, this should not change)
				type:       '<?=$this->session->userdata('user_level') ?>', // Optional: segment your users by type
				account: {
					id:           '<?=$this->session->userdata('course_id') ?>', // Optional: associate multiple users with a single account
					name:         '<?=addslashes($this->session->userdata('course_name')) ?>' // Account name
					//created_at:   1364406966, // Unix timestamp for the date the account was created
					//monthly_rate: 9.99, // Decimal; monthly rate of the account
					//ltv:          1495.00, // Decimal; lifetime value of the account
					//plan:         'Enhanced' // Plan name for the account
				}
			}]);

			// Add default trigger to the bottom-right corner of the window:
			UserVoice.push(['addTrigger', {
				trigger_position: 'bottom-right',
				mode:'smartvote'
			}]);
            //UserVoice.push(['addTrigger', '#uservoice-feedback', {mode:"smartvote"}]);


			// Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
			UserVoice.push(['autoprompt', {}]);
		</script>
    <?php } ?>
	<script>
	API_KEY = 'no_limits';
	BASE_URL = '<?php echo base_url(); ?>';
	SITE_URL = '<?php echo preg_replace('{/$}', '', site_url()); ?>';
	API_URL = '<?php echo site_url('api'); ?>';
	REST_API_COURSE_URL = BASE_URL + 'api_rest/index.php/courses/<?php echo $this->session->userdata('course_id'); ?>';

	CART_URL = '<?php echo site_url(); ?>/api/cart';
	SALES_URL = SITE_URL + '/api/sales';
	REPORTS_URL = SITE_URL + '/api/reports';
	REPORT_TEMPLATES_URL = SITE_URL + '/api/report_templates';
	REPORT_COLUMNS_URL = SITE_URL + '/api/report_columns';
	REPORT_TAGS_URL = SITE_URL + '/api/report_tags';
	QUICKBUTTONS_URL = SITE_URL + '/api/items/quickbuttons';
	CUSTOMERS_URL = SITE_URL + '/api/customers';
	RAINCHECKS_URL = SITE_URL + '/api/rainchecks';
	GIFTCARDS_URL = SITE_URL + '/api/giftcards';
	PUNCH_CARDS_URL = SITE_URL + '/api/punch_cards';
	FOOD_BEV_URL = SITE_URL + '/api/food_and_beverage';

	HAS_MARKETING = <?=$this->config->item("marketing_campaigns")?>;

	US_STATES = {
	    "":"",
		"AL": "Alabama",
		"AK": "Alaska",
		"AS": "American Samoa",
		"AZ": "Arizona",
		"AR": "Arkansas",
		"CA": "California",
		"CO": "Colorado",
		"CT": "Connecticut",
		"DE": "Delaware",
		"DC": "District Of Columbia",
		"FM": "Federated States Of Micronesia",
		"FL": "Florida",
		"GA": "Georgia",
		"GU": "Guam",
		"HI": "Hawaii",
		"ID": "Idaho",
		"IL": "Illinois",
		"IN": "Indiana",
		"IA": "Iowa",
		"KS": "Kansas",
		"KY": "Kentucky",
		"LA": "Louisiana",
		"ME": "Maine",
		"MH": "Marshall Islands",
		"MD": "Maryland",
		"MA": "Massachusetts",
		"MI": "Michigan",
		"MN": "Minnesota",
		"MS": "Mississippi",
		"MO": "Missouri",
		"MT": "Montana",
		"NE": "Nebraska",
		"NV": "Nevada",
		"NH": "New Hampshire",
		"NJ": "New Jersey",
		"NM": "New Mexico",
		"NY": "New York",
		"NC": "North Carolina",
		"ND": "North Dakota",
		"MP": "Northern Mariana Islands",
		"OH": "Ohio",
		"OK": "Oklahoma",
		"OR": "Oregon",
		"PW": "Palau",
		"PA": "Pennsylvania",
		"PR": "Puerto Rico",
		"RI": "Rhode Island",
		"SC": "South Carolina",
		"SD": "South Dakota",
		"TN": "Tennessee",
		"TX": "Texas",
		"UT": "Utah",
		"VT": "Vermont",
		"VI": "Virgin Islands",
		"VA": "Virginia",
		"WA": "Washington",
		"WV": "West Virginia",
		"WI": "Wisconsin",
		"WY": "Wyoming"
	};

	var LANG = <?php echo json_encode($this->lang->language); ?>;
	</script>
</head>
<body>
<nav id="main-nav" class="navbar navbar-default" style="border-radius: 0px; border: none;"></nav>
