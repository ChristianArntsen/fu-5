<script>
var App = window.parent.App;
var $ = window.parent.$;
$('#cancel-credit-card-payment').hide();

<?php if(!empty($action) && $action == 'refund'){
$amount = (float) 0 - $amount;
} ?>

<?php if(empty($type)){ 
	$type = 'credit_card'; 
} ?>

var payment = {};
payment.description = "<?php echo !empty($description) ? $description : 'Credit Card'; ?>";
payment.record_id = <?php echo !empty($invoice_id) ? (int) $invoice_id: 0; ?>;
payment.invoice_id = <?php echo !empty($invoice_id) ? (int) $invoice_id: 0; ?>;
payment.merchant_data = <?php echo !empty($merchant_data) ? json_encode($merchant_data): '{}'; ?>;
payment.merchant = 'mercury';
payment.approved = <?php if(!empty($approved)){ echo 'true'; }else{ echo 'false'; }?>;
payment.verified = <?php if(!empty($verified)){ echo '1'; }else{ echo '0'; }?>;
payment.message = "<?php echo !empty($merchant_data['ReturnMessage']) ? $merchant_data['ReturnMessage'] : ''; ?>";
payment.amount = <?php echo !empty($amount) ? (float) $amount : 0; ?>;
payment.payment_amount = <?php echo !empty($amount) ? (float) $amount : 0; ?>;
payment.auth_code = '<?php echo !empty($auth_code) ? $auth_code : ''; ?>';
payment.cardholder_name = '<?php echo !empty($cardholder_name) ? $cardholder_name : ''; ?>';
payment.card_number = '<?php echo !empty($card_number) ? $card_number : ''; ?>';
payment.card_type = '<?php echo !empty($card_type) ? $card_type : ''; ?>';
payment.tran_type = '<?php echo !empty($tran_type) ? $tran_type : ''; ?>';
payment.auth_amount = '<?php echo !empty($auth_amount) ? $auth_amount : ''; ?>';
payment.customer_note = <?php echo !empty($customer_note) ? json_encode($customer_note) : 'null'; ?>;

<?php if(!empty($params)){ ?>
payment.params = <?php echo json_encode($params); ?>;
payment.params.fee = <?php echo !empty($fee) ? (float) $fee : 0; ?>;
<?php }else{ ?>
payment.params = {};
<?php } ?>

<?php if(!empty($sale_id) && $action == 'refund'){ ?>
payment.tran_type = 'credit_card_partial_refund';
payment.type = 'credit_card_partial_refund';
payment.payment_type = 'Partial CC Refund';

<?php }else{ ?>
payment.type = '<?php echo $type; ?>';
<?php } ?>

App.vent.trigger('credit-card', payment);
</script>