<script>
$('#mercury_iframe').one('load', function(e){
	$.loadMask.hide();
});
</script>
<p class="bg-info" style="padding:10px">
    Do not refresh this page or press the back button while processing a payment.
</p>
<div style="text-align: center; height: 425px;">
	<iframe id='mercury_iframe' src="<?php echo $url?>" height="500px" width="100%" scrolling="auto" frameborder="0" runat="server" style="text-align: center;">
		Your browser does not support iFrames. To view this content, please download and use the latest version of one of the following browsers: Internet Explorer, Firefox, Google Chrome or Safari.
	</iframe>
</div>
