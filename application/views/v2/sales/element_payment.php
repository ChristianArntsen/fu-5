<script>
var App = window.parent.App;

var payment = {};
payment.record_id = <?php echo (int) $invoice_id; ?>;
payment.invoice_id = <?php echo (int) $invoice_id; ?>;
payment.merchant_data = <?php echo json_encode($merchant_data); ?>;
payment.merchant = 'element';
payment.approved = <?php if($approved){ echo 'true'; }else{ echo 'false'; } ?>;
payment.message = "<?php echo $merchant_data['ExpressResponseMessage']; ?>";
payment.amount = <?php echo (float) $amount; ?>;	
payment.payment_amount = <?php echo (float) $amount; ?>;

payment.params = {};
payment.params.fee = <?php echo (float) $fee; ?>;

<?php if($sale_id && $action == 'refund'){ ?>
payment.type = 'credit_card_refund';
payment.payment_type = 'Partial CC Refund';

<?php }else{ ?>
payment.type = '<?php echo $type; ?>';
payment.desription = 'Credit Card';
<?php } ?>

App.vent.trigger('credit-card', payment);
</script>
