<ul id="error_message_box"></ul>

<?php
	echo form_open('tournaments/save_awards/'.$tournament_info->tournament_id,array('id'=>'tournament_form'));
	// print_r($tournament_info);
?>

<fieldset id="tournament_basic_info">
<legend><?php echo lang("tournaments_award_information"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('tournaments_tournament_name').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo $tournament_info->name ?></div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('tournaments_tournament_accumulated_pot').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>$<?php echo $tournament_info->accumulated_pot ?></div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('tournaments_tournament_remaining_pot_balance').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field' id='remaining_pot_balance_label'>$<?php echo $tournament_info->remaining_pot_balance ?></div>
</div>

<?php echo form_input(array(
	'name'=>'remaining_pot_balance',
	'hidden'=>true,
	'id'=>'remaining_pot_balance',
	'value'=>$tournament_info->remaining_pot_balance)
	);
?>

<?php echo form_input(array(
	'name'=>'accumulated_pot',
	'hidden'=>true,
	'id'=>'accumulated_pot',
	'value'=>$tournament_info->accumulated_pot)
	);
?>

<div class="field_row clearfix">
	<?php echo form_open("sales/select_customer",array('id'=>'select_customer_form')); ?>
<!-- 	    <label id="customer_label" for="customer">
	        <?php echo lang('tournaments_customer_search'); ?>
	    </label> -->
	    <?php echo form_label(lang('tournaments_tournament_customer_search').':', 'name',array('class'=>'wide')); ?>
	    <?php echo form_input(array('name'=>'customer','id'=>'customer','size'=>'30','placeholder'=>lang('sales_start_typing_customer_name'),  'accesskey' => 'c'));?>
	    <!-- <?php
	        echo anchor("customers/view/-1/",
	        "<div class='small_button' style='margin:0 auto;'> <span>".lang('sales_new')."</span> </div>", array('class'=>'colbox none','id'=>'new_customer_button','title'=>lang('sales_new_customer')));
	?> -->
	</form>
</div>

<div class="field_row clearfix">
	<table id="tournament_winners">
		<tr class='header_row'>
			<th><?php echo lang('tournaments_tournament_winner_name');?></th>
			<th><?php echo lang('tournaments_tournament_winner_description');?></th>
			<th><?php echo lang('tournaments_tournament_winner_amount');?></th>
		</tr>


	<?php foreach ($this->Tournament_winners->get_info($tournament_info->tournament_id) as $tournament_winner) {?>
<tr>
				<?php
				$person_info = $this->Person->get_info($tournament_winner->person_id);
				?>

				<td>
					<a href='#' onclick='return deleteTournamentWinner(this, <?php echo $tournament_winner->tournament_winner_id ?>);'>X </a>
					<input hidden='true'  name='previous_award_amount[]' value='<?php echo $tournament_winner->amount ?>'/>
					<input hidden='true' class='delete_field' name='deleted[]' value='0'/>
					<input hidden='true' name='tournament_winner_id[]' value='<?php echo $tournament_winner->tournament_winner_id ?>' />
					<input hidden='true' name='person_id[]' value='<?php echo $tournament_winner->person_id ?>' />
					<?php echo $person_info->last_name.", ".$person_info->first_name; ?>
				</td>
				<td><input placeholder='Award description' class='winner_description' value='<?php echo $tournament_winner->description ?>' name='tournament_winner_description[]' /></td>
				<td><input placeholder='$0.00' class='winner_amount' name='tournament_winner_amount[]' id='tournament_winner_amount' type='text' size='3' value='<?php echo $tournament_winner->amount ?>'/></td>
			</tr>

		<?php } ?>
	</table>
</div>

<?
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>

</fieldset>
<?php
echo form_close();
?>


<script type='text/javascript'>

$( "#customer" ).autocomplete({
	source: '<?php echo site_url("sales/customer_search/ln_and_pn"); ?>',
	delay: 200,
	autoFocus: false,
	minLength: 1,
	select: function(event, ui)
	{
		$('#tournament_winners').append(
			"<tr>" +
				"<td>" +
					"<a href='#' onclick='return deleteTournamentWinner(this, null);'>X </a>" +
					"<input hidden='true'  name='previous_award_amount[]' value='0'/>" +
					"<input hidden='true' class='delete_field' name='deleted[]' value='0'/>" +
					"<input hidden='true' name='person_id[]' value='"+ui.item.value+"' />" +
					"<input hidden='true' name='tournament_winner_id[]' value='' />"+ui.item.label +
				"</td>" +
				"<td><input placeholder='Award description' class='winner_description' value='' name='tournament_winner_description[]' /></td>" +
				"<td><input placeholder='$0.00' class='winner_amount' name='tournament_winner_amount[]' type='text' size='5' value=''/></td>" +
			"</tr>"
		);
        initialize_award_amounts();
	},
	close: function(event, ui)
	{
		$('#customer').val('')
	}
});

function initialize_award_amounts (){
    $('.winner_amount').off('keypress').on('keypress', function(e) {
        if(!((e.keyCode > 95 && e.keyCode < 106)
            || (e.keyCode > 47 && e.keyCode < 58)
            || e.keyCode == 8 || e.keyCode ==46)) {
            return false;
        }
    });
}

//validation and submit handling
$(document).ready(function()
{
    initialize_award_amounts();
	var submitting = false;
    $('#tournament_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			var serial_data = $(form).serialize();
			$("<input type='hidden' name='full_data' value='"+serial_data+"' />").prependTo(form);
			$(form).ajaxSubmit({
			success:function(response)
			{
				$.colorbox.close();
                submitting = false;
				post_tournament_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			giftcard_number:
			{
				required:true
			},
			value:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{
			giftcard_number:
			{
				required:"<?php echo lang('giftcards_number_required'); ?>",
				number:"<?php echo lang('giftcards_number'); ?>"
			},
			value:
			{
				required:"<?php echo lang('giftcards_value_required'); ?>",
				number:"<?php echo lang('giftcards_value'); ?>"
			}
		}
	});
});

$('input:not(#customer)').live('keyup', function() {
  var input = $(this);
  updateRemainingBalance(input);
});

function deleteTournamentWinner(link, tournament_winner_id)
{
	var row = $(link).parent().parent();

	if (tournament_winner_id === null){
		$(link).parent().parent().remove();
	} else {

		$(link).parent().parent().hide();

		console.log(row);
		console.log($('.winner_amount', $(link).parent().parent()).val());
		console.log($('.delete_field', $(link).parent().parent()).val());

		$('.winner_amount', $(row)).val(0);
		$('.delete_field', $(row)).val(1);

		console.log($('.winner_amount', $(link).parent().parent()).val());
		console.log($('.delete_field', $(link).parent().parent()).val());
	}

	updateRemainingBalance();

	return false;
}


function updateRemainingBalance(input)
{
	var new_remaining_balance, accumulated_pot, current_input_amount,max_allowable_amount;

	current_input_amount = $(input).val();
	accumulated_pot = $('#accumulated_pot').val();
	new_remaining_balance = accumulated_pot - totalWinnerAmounts();

	if (new_remaining_balance < 0)
	{
		max_allowable_amount = parseFloat(current_input_amount) + parseFloat(new_remaining_balance);
		alert('Remaining balance is less than 0. Max award amount: $'+ max_allowable_amount);
		new_remaining_balance = 0;
		$(input).val(max_allowable_amount)
	}

	$('#remaining_pot_balance_label').html("$" + new_remaining_balance);//updates the form label
}

//when the form is submitted set the hidden input value equal to the remaining balance label
$('#submit').click(function(){
	$('#remaining_pot_balance').val($('#accumulated_pot').val() - totalWinnerAmounts());
});

//returns the sum of all winner amounts
function totalWinnerAmounts()
{
	var total_winner_amounts = 0,
		winner_amount;

	$('.winner_amount').each(function(index){
		winner_amount = parseFloat($(this).val());
		winner_amount = !isNaN(winner_amount) ? winner_amount: 0;//If the winner_amount is null set it to 0
		total_winner_amounts += winner_amount;
	});

	return total_winner_amounts;
}

</script>