<ul id="error_message_box"></ul>

<?php
	echo form_open('tournaments/save/'.$tournament_info->tournament_id,array('id'=>'tournament_form'));

?>

<fieldset id="tournament_basic_info">
<legend><?php echo lang("tournaments_basic_information"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('tournaments_tournament_name').':<span class="required">*</span>', 'name',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(
		array(
			'name'=>'tournament_name',
			'size'=>'36',
			'id'=>'tournament_name',
			'value'=>$tournament_info->name,
			'placeholder'=>''
		)
	);
	?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('tournaments_tournament_green_fee').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
		<table class='tax_table'>
			<tr>
				<th><?php echo lang('tournaments_inventory_item_price');?></th>
				<th><?php echo lang('tournaments_inventory_item_tax_rate');?></th>
				<th><?php echo lang('tournaments_inventory_item_total_price');?></th>
			</tr>
			<tr>
				<td>
					<?php echo form_label('', 'name',array('class'=>'tax_label price_without_tax')); ?>
				</td>
				<td>
					<?php echo form_label(number_format($green_fee_tax_item[0]['percent'],2), 'name',array('class'=>'tax_label tax_rate')); ?>
				</td>
				<td>
					<?php echo form_input(array(
						'name'=>'green_fee',
						'size'=>'8',
						'id'=>'green_fee',
						'value'=>$tournament_info->green_fee,
						'class'=>'tournament_fee_item price_with_tax_input')
						);
					?>
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('tournaments_tournament_carts_issued'), 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo form_checkbox(array(
		'name'=>'carts_issued',
		'class'=>'carts_issued',
		'value'=>1,
		'checked'=>$tournament_info->carts_issued)
	);?>
	</div>
</div>

<div class="field_row clearfix" id="cart_fee_container">
<?php echo form_label(lang('tournaments_tournament_cart_fee').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
		<table class='tax_table'>
			<tr>
				<td>
					<?php echo form_label('', 'name',array('class'=>'tax_label price_without_tax')); ?>
				</td>
				<td>
					<?php echo form_label(number_format($cart_fee_tax_item[0]['percent'],2), 'name',array('class'=>'tax_label tax_rate')); ?>
				</td>
				<td>
					<?php echo form_input(array(
						'name'=>'cart_fee',
						'size'=>'8',
						'id'=>'cart_fee',
						'value'=>$tournament_info->cart_fee,
						'class'=>'tournament_fee_item price_with_tax_input')
						);

					?>
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('tournaments_tournament_pot_fee').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
		<table class='tax_table'>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td>
					<?php echo form_input(array(
						'name'=>'pot_fee',
						'size'=>'8',
						'id'=>'pot_fee',
						'value'=>$tournament_info->pot_fee,
						'class'=>'tournament_fee_item')
						);
					?>
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('tournaments_tournament_customer_credit_fee').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
		<table class='tax_table'>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td>
					<?php echo form_input(array(
						'name'=>'customer_credit_fee',
						'size'=>'8',
						'id'=>'customer_credit_fee',
						'value'=>$tournament_info->customer_credit_fee,
						'class'=>'tournament_fee_item')
						);

					?>
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('tournaments_tournament_member_credit_fee').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
		<table class='tax_table'>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td>
					<?php echo form_input(array(
						'name'=>'member_credit_fee',
						'size'=>'8',
						'id'=>'member_credit_fee',
						'value'=>$tournament_info->member_credit_fee,
						'class'=>'tournament_fee_item')
						);

					?>
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('item_kits_add_item').':', 'item',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'item',
			'id'=>'item'
		));?>
	</div>
</div>

<div class="field_row clearfix">
	<table id="tournament_inventory_items" class="tournament_inventory_items">
		<tr class='header_row'>
			<th><?php echo lang('tournaments_inventory_item');?></th>
			<th><?php echo lang('tournaments_inventory_item_price');?></th>
			<th><?php echo lang('tournaments_inventory_item_tax_rate');?></th>
			<th><?php echo lang('tournaments_inventory_item_quantity');?></th>
			<th><?php echo lang('tournaments_inventory_item_total_price');?></th>
		</tr>


		<?php foreach ($this->Tournament_inventory_items->get_info($tournament_info->tournament_id) as $tournament_inventory_item) {?>
			<tr>
				<?php
				$item_info = $this->Item->get_info($tournament_inventory_item->item_id);
				$tax_info = $this->Item_taxes->get_info($tournament_inventory_item->item_id);
				$tax_rate_1 = $tax_info[0]['percent'];
				$tax_rate_2 = $tax_info[1]['percent'];
				$cumulative = $tax_info[1]['cumulative'];

				if ($cumulative == 1) {
					$tax_rate_2 = $tax_rate_2 + ($tax_rate_1 * $tax_rate_2 / 100);
				}

				$total_tax = $tax_rate_1 + $tax_rate_2;
				?>

				<td class='item_name_column'>
					<input hidden='true' name='tournament_inventory_item_id[]' value='<?php echo $tournament_inventory_item->item_id ?>' />
					<a href="#" onclick='return deleteTournamentInventoryRow(this);'>X</a>
					<?php echo $item_info->name; ?>
				</td>
				<td><label class='price_without_tax'></label></td>
				<td><label class='tax_rate'><?php echo number_format($total_tax,2)?></label></td>
				<td><input onkeyup='updateTotalTournamentPrice();' size="3" class='quantity' onchange='' name='tournament_inventory_item_quantity_<?php echo $tournament_inventory_item->item_id ?>' id='tournament_inventory_item_<?php echo $tournament_inventory_item->item_id ?>' type='text' size='3' value='<?php echo $tournament_inventory_item->quantity ?>'/></td>
				<td><input onkeyup='updateTotalTournamentPrice();' size="8" class='item_price price_with_tax_input' value='<?php echo $tournament_inventory_item->price ?>' name='tournament_inventory_item_price_<?php echo $tournament_inventory_item->item_id ?>' /></td>
			</tr>
		<?php } ?>

	</table>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('tournaments_tournament_total_price').':', 'name',array('class'=>'wide')); ?>
	<table class='tax_table'>
		<tr>
			<td>
			</td>
			<td>
			</td>
			<td>
				<div class='form_field total_tournament_price'></div>
			</td>
		</tr>
	</table>
</div>

<?php echo form_input(array(
	'name'=>'total_tournament_price',
	'class'=>'total_tournament_price',
	'hidden' => 'true'
));?>

<?
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>

</fieldset>
<?php
echo form_close();
?>


<script type='text/javascript'>

$( "#item" ).autocomplete({
	source: '<?php echo site_url("items/item_search"); ?>',
	delay: 200,
	autoFocus: false,
	minLength: 1,
	select: function( event, ui )
	{
		var combined_tax_rate = calculateCombinedTaxRate(ui.item);

		$( "#item" ).val("");
		if ($("#item_kit_item_"+ui.item.value).length ==1)
		{
			$("#item_kit_item_"+ui.item.value).val(parseFloat($("#item_kit_item_"+ui.item.value).val()) + 1);
		}
		else
		{
			$('#tournament_inventory_items').append(
				"<tr>" +
					"<td class='item_name_column'>" +
						"<a href='#' onclick='return deleteTournamentInventoryRow(this);'>X </a>" +
						"<input hidden='true' name='tournament_inventory_item_id[]' value='"+ui.item.value+"' />"+ui.item.label +
					"</td>" +
					"<td><label class='price_without_tax'></label></td>" +
					"<td><label class='tax_rate'>"+combined_tax_rate +"</label></td>" +
					"<td><input onkeyup='updateTotalTournamentPrice();' size='3' class='quantity' name='tournament_inventory_item_quantity_"+ui.item.value+"' id='tournament_inventory_item_"+ui.item.value+"' type='text' size='3' value='1'/></td>" +
					"<td><input onkeyup='updateTotalTournamentPrice();' size='8' class='item_price price_with_tax_input' value='"+ui.item.price+"' name='tournament_inventory_item_price_"+ui.item.value+"' /></td>" +
				"</tr>"
			);

		}

		$.colorbox.resize();
		updatePriceBeforeTax($('.price_with_tax_input:last'));
		updateTotalTournamentPrice();

		return false;
	}
});

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
    $('#tournament_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				$.colorbox.close();
                submitting = false;
				post_tournament_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			giftcard_number:
			{
				required:true
			},
			value:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{
			giftcard_number:
			{
				required:"<?php echo lang('giftcards_number_required'); ?>",
				number:"<?php echo lang('giftcards_number'); ?>"
			},
			value:
			{
				required:"<?php echo lang('giftcards_value_required'); ?>",
				number:"<?php echo lang('giftcards_value'); ?>"
			}
		}
	});

	updateTotalTournamentPrice();

	//hide the cart fee section if carts are not being issued
	if ($('.carts_issued').attr('checked') == false)
	{
		$('#cart_fee_container').hide();
	}

	//update prices
	$('.price_with_tax_input').each(function(){
		updatePriceBeforeTax($(this));
	});
});


//********FORM LISTNERS*******//

//when the form is submitted set the cart fee to 0 if carts_used is not checkked
$('#submit').click(function(){
	//set the cart fee to zero if carts are not being issued
	if ($('.carts_issued').attr('checked') == false)
	{
		$('#cart_fee').val(0);
		updateTotalTournamentPrice();
	}
});

//update the total tournament price as the input values are changed
$('input').live('keyup', function() {
	updateTotalTournamentPrice();
});

//set the value to zero if the user left the input blank
// $('input').blur(function() {
  	// if ($(this).val() === '')
	// {
		// $(this).val(0);
	// }
// });

//hide or show the cart fee row
$('.carts_issued').click(function(){
	$('#cart_fee_container').toggle();
	$.colorbox.resize();
	updateTotalTournamentPrice();
});

//Update price label when the total cost input value is changed
$('.price_with_tax_input').live('keyup', function() {
	 updatePriceBeforeTax($(this));
});

//********FORM FUNCTIONS*******//

function calculateCombinedTaxRate(item){
	var tax_rate_1 = parseFloat(item.tax_rate_1),
		tax_rate_2 = parseFloat(item.tax_rate_2);

	if (item.cumulative === '1')
	{
		tax_rate_2 = tax_rate_2 + (tax_rate_2 * tax_rate_1 / 100);
	}

	total_tax_rate = (tax_rate_1 + tax_rate_2);
	total_tax_rate = !isNaN(total_tax_rate) ? total_tax_rate: 0;

	return total_tax_rate.toFixed(2);
}

function deleteTournamentInventoryRow(link)
{
	$(link).parent().parent().remove();

	updateTotalTournamentPrice();
	return false;
}

function updateTotalTournamentPrice()
{
	var tournament_fee_items_total = 0,
		tournament_inventory_items_total = 0,
		total_price = 0,
		item_price, item_quantity;

	//get the costs of the tournament attributes
	$('.tournament_fee_item').each(function(index){
		// if ($(this).is(':visible'))
		// {
			item_price = parseFloat($(this).val());
			item_price = !isNaN(item_price) ? item_price: 0;
			tournament_fee_items_total += item_price;
		// }
	});

	//get the costs of the tournament inventory items
	$('tr:not(.header_row)', '#tournament_inventory_items').each(function(){
		item_price = parseFloat($('.item_price', $(this)).val());
		item_price = !isNaN(item_price) ? item_price: 0;
		item_quantity = parseFloat($('.quantity', $(this)).val());
		tournament_inventory_items_total += (item_price * item_quantity);
	});

	total_price = tournament_fee_items_total + tournament_inventory_items_total

	total_price = !isNaN(total_price) ? total_price: 0;//If the price is null set it to 0

	$('.total_tournament_price').html("$" + (total_price).toFixed(2));//updates the form label
	$('.total_tournament_price').val((total_price).toFixed(2));//updates the hidden form input
}

function updatePriceBeforeTax(options){
	var price_before_tax = 0,
		tax_rate = 0,
		total_price = 0,
		quantity = 1,
		tr = $(options).parent().parent();

	tax_rate = $('.tax_rate', $(tr)).html();
	tax_rate = tax_rate/100;
	total_price = $(options).val();
	price_before_tax = total_price / (1 + tax_rate);

	$('.price_without_tax', $(tr)).html("$" + (price_before_tax).toFixed(2));
}

function isTournamentInventoryItem(tr){
	return $(tr).parent().parent().hasClass('tournament_inventory_items');
}

</script>