<?php
//Takes a date from 0000-00-00 format to Jan 01 format
function convert_to_short_date($date)
{
	//Define an array with the short names for the months
	$monthsArray = array(
		'01' => "Jan",
		'02' => "Feb",
		'03' => "Mar",
		'04' => "Apr",
		'05' => "May",
		'06' => "Jun",
		'07' => "Jul",
		'08' => "Aug",
		'09' => "Sep",
		'10' => "Oct",
		'11' => "Nov",
		'12' => "Dec"
	);
	
	//Explode the date, and match the month number to its short name
	$pieces = explode("-",$date);
	$month = $monthsArray[$pieces[1]];
	$day = $pieces[2];
	
	//Return the date
	return $month . " " . $day;
	
	return 1;
}

function time_array(){
	return array(
                        '0000'=>'12:00am',
                        '0030'=>'12:30am',
                        '0100'=>'1:00am',
                        '0130'=>'1:30am',
                        '0200'=>'2:00am',
                        '0230'=>'2:30am',
                        '0300'=>'3:00am',
                        '0330'=>'3:30am',
                        '0400'=>'4:00am',
                        '0430'=>'4:30am',
                        '0500'=>'5:00am',
                        '0530'=>'5:30am',
                        '0600'=>'6:00am',
                        '0630'=>'6:30am',
                        '0700'=>'7:00am',
                        '0730'=>'7:30am',
                        '0800'=>'8:00am',
                        '0830'=>'8:30am',
                        '0900'=>'9:00am',
                        '0930'=>'9:30am',
                        '1000'=>'10:00am',
                        '1030'=>'10:30am',
						'1100'=>'11:00am',
						'1130'=>'11:30am',
						'1200'=>'12:00pm',
						'1230'=>'12:30pm',
						'1300'=>'1:00pm',
						'1330'=>'1:30pm',
						'1400'=>'2:00pm',
						'1430'=>'2:30pm',
						'1500'=>'3:00pm',
						'1530'=>'3:30pm',
						'1600'=>'4:00pm',
                        '1630'=>'4:30pm',
                        '1700'=>'5:00pm',
                        '1730'=>'5:30pm',
                        '1800'=>'6:00pm',
                        '1830'=>'6:30pm',
                        '1900'=>'7:00pm',
                        '1930'=>'7:30pm',
                        '2000'=>'8:00pm',
                        '2030'=>'8:30pm',
                        '2100'=>'9:00pm',
                        '2130'=>'9:30pm',
                        '2200'=>'10:00pm',
                        '2230'=>'10:30pm',
						'2300'=>'11:00pm',
						'2330'=>'11:30pm',
						'2400'=>'11:59pm'
	);
}
?>
