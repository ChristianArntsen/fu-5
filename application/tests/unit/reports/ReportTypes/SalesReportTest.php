<?php
namespace reports\ReportTypes;


use fu\reports\ReportParameters;
use fu\TestingHelpers\Actor;

class SalesReportTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected function _before()
    {

        $salesTables = new \fu\reports\TemporaryTables\SalesTable();
        $salesTables->cleanseAllOldTables();
	    $this->actor = new Actor();
    }

    protected function _after()
    {
        $salesTables = new \fu\reports\TemporaryTables\SalesTable();
        $salesTables->cleanseAllOldTables();
    }

    // tests
    public function testStaticRange()
    {
        $input = [
            'filters' => [],
            'group' =>[],
            'columns' => [
                0 => 'sale_id',
                1 => 'sale_date',
                2 => 'count(sale_id)'
            ],
            'order' => [
                0 => 'sale_id',
            ],
            "date_range"=>[
                "column"=>"sale_date",
                "type"=>"range",
                "start_value"=>"2015-05-08",
                "end_value"=>"2015-05-11",
                "editMode"=>false
           ],
        ];
        $reportParameters = $this->createDataReportParam($input);
        $report = new \fu\reports\ReportTypes\SalesReport($reportParameters);

        $data = $report->getData();
        verify(count($data))->equals(1);
    }

    public function testRelationalColumn()
    {
        $input = [
            'filters' => [],
            'group' =>[],
            'columns' => [
                0 => 'sales.teetime_id'
            ],
            'order' => [
                0 => 'sale_id',
            ],
            "date_range"=>[
                "column"=>"sale_date",
                "type"=>"range",
                "start_value"=>"2015-05-08",
                "end_value"=>"2015-05-11",
                "editMode"=>false
            ],
        ];
	    $this->actor->login();
        $reportParameters = $this->createDataReportParam($input);
        $report = new \fu\reports\ReportTypes\SalesReport($reportParameters);


        $data = $report->getData();
        verify(count($data))->equals(33);
    }

    public function testAggregateColumn()
    {
        $input = [
            'filters' => [],
            'group' =>[],
            'columns' => [
                0 => 'max(teetime.start)'
            ],
            'order' => [
                0 => 'sale_id',
            ],
            "date_range"=>[
                "column"=>"sale_date",
                "type"=>"range",
                "start_value"=>"2015-05-08",
                "end_value"=>"2015-05-11",
                "editMode"=>false
            ],
        ];
        $reportParameters = $this->createDataReportParam($input);
        $report = new \fu\reports\ReportTypes\SalesReport($reportParameters);

        $data = $report->getData();
        verify($data[0]["max(teetime.start)"])->equals(201504081534);
    }

    private function createDataReportParam($input)
    {
        $reportParameters = new \fu\reports\ReportParameters();
        isset($input['columns']) ? $reportParameters->setColumns($input['columns']) : "";
        isset($input['group']) ? $reportParameters->setGroupBy($input['group']) : "";
        isset($input['order']) ? $reportParameters->setOrderBy($input['order']) : "";
        isset($input['filters']) ? $reportParameters->setFilters($input['filters']) : "";
        isset($input['date_range']) ? $reportParameters->setDate($input['date_range']) : "";
        return $reportParameters;
    }
}