<?php
namespace reports;

use Carbon\Carbon;
use \Codeception\Verify;

class ParamDateTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }


    public function testLabel()
    {

        $this->specify("Check the last month label",function() {
            $param = new \fu\reports\ParamDate([
                "column"=>"sale_date",
                "type"=>"moving",
                "typeOfMovingRange"=>"label",
                "movingRange"=>"lastmonth",
            ]);
            $now = Carbon::now();
            $date = new \Carbon\Carbon($param->getStart());
            verify($date->month)->equals($now->month - 1);

            $date = new \Carbon\Carbon($param->getEnd());
            verify($date->month)->equals($now->month - 1);
        });

    }

    public function testBadLabel()
    {


        $this->specify("Check exception is thrown when a bad argument is passed in.",function(){
            $param = new \fu\reports\ParamDate([
                "column"=>"sale_date",
                "type"=>"moving",
                "typeOfMovingRange"=>"bad",
                "movingRange"=>"lastmonth",
            ]);
        }, ['throws' => 'fu\Exceptions\InvalidArgument']);
    }

    public function testDaysAgo()
    {

        $this->specify("Check the last days label",function() {
            $param = new \fu\reports\ParamDate([
                "column"=>"sale_date",
                "type"=>"moving",
                "typeOfMovingRange"=>"days",
                "movingRange"=>"7",
            ]);
            $now = Carbon::now();

            $date = new \Carbon\Carbon($param->getEnd());
            verify($date->day)->equals($now->day);


            $now->subDay(7);
            $date = new \Carbon\Carbon($param->getStart());
            verify($date->day)->equals($now->day);
        });
    }

    public function testInvalidType()
    {
        $this->specify("Check exception is thrown when a bad argument is passed in.",function(){
            $param = new \fu\reports\ParamDate([
                "column"=>"sale_date",
                "type"=>"moving",
                "typeOfMovingRange"=>"nada",
                "movingRange"=>"1",
            ]);
        }, ['throws' => 'fu\Exceptions\InvalidArgument']);


    }
}