<?php
namespace reports\ReportFilters;


use \Codeception\Verify;
class MonthTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testMonthFilter()
    {
        $filterObj = new \fu\reports\ReportFilters\Month([
            "column"=>"somecolumn",
            "operator"=>"=",
            "value"=>"2015-01-01",
            "startValue"=>"2015-01-01",
            "endValue"=>"2015-01-01",
        ]);
        verify(count($filterObj->getFilterObjects()))->equals(1);
    }

}