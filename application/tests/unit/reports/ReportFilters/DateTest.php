<?php
namespace reports\ReportFilters;


use \Codeception\Verify;
class DateTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testDate()
    {
        $filterObj = new \fu\reports\ReportFilters\Date([
            "column"=>"somecolumn",
            "operator"=>"=",
            "startValue"=>"2015-01-01",
            "endValue"=>"2015-01-01",
        ]);
        verify(count($filterObj->getFilterObjects()))->equals(2);
    }

}