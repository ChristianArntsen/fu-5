<?php
namespace libraries\fu\service_fees;


use fu\TestingHelpers\Actor;
use fu\TestingHelpers\TestingObjects\Item;
use fu\TestingHelpers\TestingObjects\Payment;

class TaxIncluded  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;


	protected function _before()
	{
		$this->actor = new Actor();
	}

	public function testTaxIncluded()
	{
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $itemObj->getArray();
		$item['unit_price_includes_tax'] = true;
		$payment = $paymentObj->getArray();
		$payment['amount'] =30.00;

		$CI = get_instance();
		$CI->db->trans_start(true);

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);

		$this->assertNotNull($response['sale_id']);

		$this->specify("Check that all sales tax was saved correctly.",function() use ($response,$CI){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(7.000);
			verify($results['amount'])->equals(1.96);

		});

		$this->specify("Check that sale was saved correctly.",function() use ($response,$CI){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])->get()->row_array();

			//verify($results)->isEmpty(); // If results is empty, following two tests will never pass...
			verify($results['tax'])->equals(1.96);
			verify($results['subtotal'])->equals(28.04);

			$results =
				$CI->db->from("sales_payments")
					->where("sale_id",$response['sale_id'])->get()->result_array();

			verify(count($results))->equals(1);
			verify($results[0]['payment_type'])->equals('Cash');
			verify($results[0]['payment_amount'])->equals(30.00);

		});
		$CI->db->trans_complete();
		//$this->actor->logout();//TODO: need to logout without killing the tests too...
	}

	public function testTaxIncludedNonTaxableCustomer()
	{
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $itemObj->getArray();
		$item['unit_price_includes_tax'] = true;
		$payment = $paymentObj->getArray();
		$payment['amount'] = 28.04;


		$CI = get_instance();
		$CI->db->trans_start(true);

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_customer(418,array('selected'=>1, 'taxable'=>0, 'discount'=>0));
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);

		$this->assertNotNull($response['sale_id']);

		$this->specify("Check that all sales tax was saved correctly.",function() use ($response,$CI){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])->get()->row_array();

			verify($results)->isEmpty();
		});

		$this->specify("Check that sale was saved correctly.",function() use ($response,$CI){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])->get()->row_array();

			//verify($results)->isEmpty(); // If results is empty, following two tests will never pass...
			verify($results['tax'])->equals(0.00);
			verify($results['subtotal'])->equals(28.04);

			$results =
				$CI->db->from("sales_payments")
					->where("sale_id",$response['sale_id'])->get()->result_array();

			verify(count($results))->equals(1);
			verify($results[0]['payment_type'])->equals('Cash');
			verify($results[0]['payment_amount'])->equals(28.04);

		});

		$CI->db->trans_complete();
		//$this->actor->logout();//TODO: need to logout without killing the tests too...
	}

	public function testTaxIncludedDiscountCustomer()
	{
		$paymentObj = new Payment();
		$itemObj = new Item();
		$item = $itemObj->getArray();
		$item['unit_price_includes_tax'] = true;
		$payment = $paymentObj->getArray();
		$payment['amount'] = 27.00;


		$CI = get_instance();
		$CI->db->trans_start(true);

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_customer(7943,array('selected'=>1, 'taxable'=>1, 'discount'=>10.00));
		$item['discount_percent']=10; // as though we get it pre-calculated from the frontend...
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);

		$this->assertNotNull($response['sale_id']);

		$this->specify("Check that all sales tax was saved correctly.",function() use ($response,$CI){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(7.000);
			verify($results['amount'])->equals(1.77);
		});

		$this->specify("Check that sale was saved correctly.",function() use ($response,$CI){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(1.77);
			verify($results['subtotal'])->equals(25.23);

			$results =
				$CI->db->from("sales_payments")
					->where("sale_id",$response['sale_id'])->get()->result_array();

			verify(count($results))->equals(1);
			verify($results[0]['payment_type'])->equals('Cash');
			verify($results[0]['payment_amount'])->equals(27.00);

		});
		$CI->db->trans_complete();

		//$this->actor->logout();//TODO: need to logout without killing the tests too...
	}
}
