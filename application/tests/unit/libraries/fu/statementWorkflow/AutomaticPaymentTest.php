<?php
namespace fu\statementWorkflow\classes;

use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupInvoices;
use fu\statementWorkflow\generateStatementJobs;

class AutomaticPaymentTest extends  \Codeception\TestCase\Test
{
	private $fakeQueue,$generator;
	/**
	 * @var AutomaticPayment
	 */
    private $auto_payment;

	protected function _before()
	{
		$this->fakeQueue = new FakeQueue();
		$this->generator = new generateStatementJobs($this->fakeQueue);
		$silex = &$this->generator->silex;

		$customer = $silex->db->getRepository('e:ForeupCustomers')->findOneBy(['person'=>3399,'course'=>6270]);

		$statement = $silex->db->getRepository('e:ForeupAccountStatements')->find(1);
		$this->auto_payment = new AutomaticPayment($silex,$customer,$statement);
	}

	protected function _after()
	{
		unset($this->generator);
	}

	public function testInterface()
	{
		$this->assertTrue(method_exists($this->auto_payment,'__construct'));
		$this->assertTrue(method_exists($this->auto_payment,'getSavedCreditCards'));
		$this->assertTrue(method_exists($this->auto_payment,'validateCard'));
		$this->assertTrue(method_exists($this->auto_payment,'getCreditCard'));
		$this->assertTrue(method_exists($this->auto_payment,'charge'));
		$this->assertTrue(method_exists($this->auto_payment,'validAutopay'));
	}

	public function testGetSavedCreditCards()
	{
		$cards = $this->auto_payment->getSavedCreditCards();
		$this->assertTrue(is_array($cards),'getSavedCreditCards should return an array');
		$this->assertGreaterThan(0,count($cards),'array returned by getSavedCreditcards for test customer should be > 0');
	}

	public function testGetCreditCard()
	{
		$card = $this->auto_payment->getCreditCard();
		$this->assertNotEmpty($card);
		$this->assertTrue(method_exists($card,'getCreditCardId'));
		$this->assertNotEmpty($card->getCreditCardId());
	}

	public function testCharge()
	{
		$this->auto_payment->charge(true);
	}

	public function testGetStatementSaleItem()
	{
		$item = $this->auto_payment->getStatementSaleItem(6270);
		$this->assertEquals('Statement',$item->getName());
		$this->assertEquals('statement_payment',$item->getItemNumber());
	}

	public function testSaveSale()
	{
		$result = $this->auto_payment->saveSale(6270,3399,123.45,12345,true);
		$this->assertTrue(is_int($result));
		$this->assertGreaterThan(1,$result);
	}

	public function testApplyPaymentToInvoices()
	{
		$invoice1 = new ForeupInvoices();
		$invoice2 = new ForeupInvoices();
		$invoice1->setTotal(5.25);
		$invoice2->setTotal(4.75);
		$invoice1->setPaid(0.00);
		$invoice2->setPaid(0.00);
		$invoices = [$invoice1,$invoice2];
		$remaining = $this->auto_payment->applyPaymentToInvoices($invoices,10.00);

		$this->assertEquals(0.00,$remaining);
		$this->assertEquals(5.25,$invoice1->getPaid());
		$this->assertEquals(4.75,$invoice2->getPaid());


		$invoice1->setPaid(0.00);
		$invoice2->setPaid(0.00);
		$invoices = [$invoice1,$invoice2];
		$remaining = $this->auto_payment->applyPaymentToInvoices($invoices,6.00);

		$this->assertEquals(0.00,$remaining);
		$this->assertEquals(5.25,$invoice1->getPaid());
		$this->assertEquals(0.75,$invoice2->getPaid());
	}
}

?>
