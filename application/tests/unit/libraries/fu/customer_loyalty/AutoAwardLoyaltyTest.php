<?php

namespace libraries\fu\customer_loyalty;

use fu\TestingHelpers\Actor;
use fu\TestingHelpers\Customer;
use fu\TestingHelpers\Package;

class AutoAwardLoyaltyTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $default_loyalty_rate;
    protected $package1;
    protected $package1_loyalty_rate;
    protected $package2;
    protected $package2_loyalty_rate;
    protected $package3;
    protected $package3_loyalty_rate;

    protected function _before()
    {
        $this->actor = new Actor();
        $this->actor->login();

        $CI = & get_instance();
        $CI->config->set_item('use_loyalty', true);
        $CI->load->model('Cart_model');
        $CI->load->model('Customer_loyalty');
        $CI->load->model('Course');

        $course = ['loyalty_auto_enroll' => 1];
        $CI->Course->save($course, $CI->session->userdata('course_id'));

        $this->default_loyalty_rate = [
            'label'				=> 'Test Default',
            'course_id'			=> $CI->session->userdata('course_id'),
            'type'				=> 'item',
            'value'				=> '5167',
            'value_label'		=> 'Jumbo Hot Dog',
            'tee_time_index'	=> 0,
            'price_category'	=> 0,
            'points_per_dollar'	=> 100,
            'dollars_per_point'	=> 100
        ];
        $CI->Customer_loyalty->save($this->default_loyalty_rate);

        $this->package1 = new Package();
        $this->package1_loyalty_rate = [
            'label'				=> 'Test Package 1',
            'course_id'			=> $CI->session->userdata('course_id'),
            'type'				=> 'item',
            'value'				=> '5167',
            'value_label'		=> 'Jumbo Hot Dog',
            'tee_time_index'	=> 0,
            'price_category'	=> 0,
            'points_per_dollar'	=> 50,
            'dollars_per_point'	=> 50,
            'loyalty_package_id' => $this->package1->get_id()
        ];
        $CI->Customer_loyalty->save($this->package1_loyalty_rate);

        $this->package2 = new Package();
        $this->package2_loyalty_rate = [
            'label'				=> 'Test Package 2',
            'course_id'			=> $CI->session->userdata('course_id'),
            'type'				=> 'item',
            'value'				=> '5167',
            'value_label'		=> 'Jumbo Hot Dog',
            'tee_time_index'	=> 0,
            'price_category'	=> 0,
            'points_per_dollar'	=> 20,
            'dollars_per_point'	=> 20,
            'loyalty_package_id' => $this->package2->get_id()
        ];
        $CI->Customer_loyalty->save($this->package2_loyalty_rate);

        $this->package3 = new Package();
        $this->package3_loyalty_rate = [
            'label'				=> 'Test Package 3',
            'course_id'			=> $CI->session->userdata('course_id'),
            'type'				=> 'item',
            'value'				=> '5167',
            'value_label'		=> 'Jumbo Hot Dog',
            'tee_time_index'	=> 0,
            'price_category'	=> 0,
            'points_per_dollar'	=> 10,
            'dollars_per_point'	=> 10,
            'loyalty_package_id' => $this->package3->get_id()
        ];
        $CI->Customer_loyalty->save($this->package3_loyalty_rate);
    }

    protected function _after()
    {
        $CI = & get_instance();

        $CI->db->where('loyalty_rate_id', $this->default_loyalty_rate['loyalty_rate_id']);
        $CI->db->delete('loyalty_rates');

        $CI->db->where('loyalty_rate_id', $this->package1_loyalty_rate['loyalty_rate_id']);
        $CI->db->delete('loyalty_rates');

        $CI->db->where('loyalty_rate_id', $this->package2_loyalty_rate['loyalty_rate_id']);
        $CI->db->delete('loyalty_rates');

        $CI->db->where('loyalty_rate_id', $this->package3_loyalty_rate['loyalty_rate_id']);
        $CI->db->delete('loyalty_rates');

        $this->package1->destroy();
        $this->package2->destroy();
        $this->package3->destroy();

    }

    protected function set_up_cart($customer_id) {
        $CI = & get_instance();

        $CI->Cart_model->cart_id = $CI->Cart_model->initialize_cart();
        $CI->Cart_model->save_customer(
            $customer_id,
            [
                'selected' => true
            ]
        );
        $CI->Cart_model->save_item(1, [
            'item_id' => '5167',
            'item_type' => 'item',
            'quantity' => 1,
            'unit_price' => '3',
            'discount_percent' => '0.00',
            'timeframe_id' => NULL,
            'special_id' => NULL,
            'punch_card_id' => NULL,
            'selected' => true,
            'params' =>
                array (
                ),
            'unit_price_includes_tax' => false,
            'erange_size' => '0'
        ]);
        $CI->Cart_model->save_payment(array (
            'approved' => 'false',
            'card_type' => '',
            'card_number' => '',
            'cardholder_name' => '',
            'auth_amount' => 0,
            'auth_code' => '',
            'description' => 'Cash',
            'tran_type' => '',
            'type' => 'cash',
            'record_id' => 0,
            'amount' => 3.21,
            'tip_recipient' => 0,
            'params' => NULL,
            'bypass' => 0,
            'is_auto_gratuity' => 0,
            'verified' => false,
            'refund_reason' => '',
            'refund_comment' => '',
            'merchant_data' => false,
        ));
    }

    public function testAutoAwardLoyalty()
    {

        $CI = & get_instance();

        $this->specify("Make sure the original stuff works", function() use ($CI) {
            $customer = new Customer();
            $this->set_up_cart($customer->get_id());
            $CI->Cart_model->save_sale();
            $customer_details = $CI->Customer->get_info($customer->get_id(), $CI->session->userdata('course_id'));
            $this->assertEquals(300, $customer_details->loyalty_points);
        });

        $this->specify("When making a sale and customer has multiple loyalties plus default, award all", function() use ($CI) {
            $customer = new Customer();

            $customer_loyalty_package1 = [
                'course_id' => $CI->session->userdata('course_id'),
                'customer_id' => $customer->get_id(),
                'loyalty_package_id' => $this->package1->get_id()
            ];
            $CI->Customers_loyalty_package->save($customer_loyalty_package1);

            $customer_loyalty_package2 = [
                'course_id' => $CI->session->userdata('course_id'),
                'customer_id' => $customer->get_id(),
                'loyalty_package_id' => $this->package2->get_id()
            ];
            $CI->Customers_loyalty_package->save($customer_loyalty_package2);

            $this->set_up_cart($customer->get_id());
            $CI->Cart_model->save_sale();

            $customer_details = $CI->Customer->get_info($customer->get_id(), $CI->session->userdata('course_id'));
            $customer_loyalty_package1 = $CI->Customers_loyalty_package->get($customer->get_id(), $this->package1->get_id())->result_object()[0];
            $customer_loyalty_package2 = $CI->Customers_loyalty_package->get($customer->get_id(), $this->package2->get_id())->result_object()[0];

            $CI->db->where('id', $customer_loyalty_package1->id);
            $CI->db->delete('customers_loyalty_packages');

            $CI->db->where('id', $customer_loyalty_package2->id);
            $CI->db->delete('customers_loyalty_packages');

            $this->assertEquals(300, $customer_details->loyalty_points);
            $this->assertEquals(150, $customer_loyalty_package1->loyalty_balance);
            $this->assertEquals(60, $customer_loyalty_package2->loyalty_balance);

        });

        $this->specify("When making a sale and customer has multiple loyalties but no default, award all but default", function() use ($CI) {
            $customer = new Customer();

            $CI->db->where('person_id', $customer->get_id());
            $CI->db->update('customers', ['use_loyalty' => false]);

            $customer_loyalty_package1 = [
                'course_id' => $CI->session->userdata('course_id'),
                'customer_id' => $customer->get_id(),
                'loyalty_package_id' => $this->package1->get_id()
            ];
            $CI->Customers_loyalty_package->save($customer_loyalty_package1);

            $customer_loyalty_package2 = [
                'course_id' => $CI->session->userdata('course_id'),
                'customer_id' => $customer->get_id(),
                'loyalty_package_id' => $this->package2->get_id()
            ];
            $CI->Customers_loyalty_package->save($customer_loyalty_package2);

            $this->set_up_cart($customer->get_id());
            $CI->Cart_model->save_sale();

            $customer_details = $CI->Customer->get_info($customer->get_id(), $CI->session->userdata('course_id'));
            $customer_loyalty_package1 = $CI->Customers_loyalty_package->get($customer->get_id(), $this->package1->get_id())->result_object()[0];
            $customer_loyalty_package2 = $CI->Customers_loyalty_package->get($customer->get_id(), $this->package2->get_id())->result_object()[0];

            $CI->db->where('id', $customer_loyalty_package1->id);
            $CI->db->delete('customers_loyalty_packages');

            $CI->db->where('id', $customer_loyalty_package2->id);
            $CI->db->delete('customers_loyalty_packages');

            $this->assertEquals(0, $customer_details->loyalty_points);
            $this->assertEquals(150, $customer_loyalty_package1->loyalty_balance);
            $this->assertEquals(60, $customer_loyalty_package2->loyalty_balance);
        });
    }
}