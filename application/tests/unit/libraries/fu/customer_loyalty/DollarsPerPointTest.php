<?php

namespace libraries\fu\customer_loyalty;

use fu\TestingHelpers\Actor;
use fu\TestingHelpers\Package;

class DollarsPerPointTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $default_loyalty_rate;
    protected $default_item;

    protected $package;
    protected $package_loyalty_rate;
    protected $package_loyalty_item;

    protected function _before()
    {
        $CI = & get_instance();
        $CI->load->model('Customer_loyalty');
        $CI->load->model('Loyalty_package');
        $CI->load->model('Item');

        $this->actor = new Actor();
        $this->actor->login();

        $this->default_loyalty_rate = [
            'label'				=> 'Item A',
            'course_id'			=> $CI->session->userdata('course_id'),
            'type'				=> 'department',
            'value'				=> 'Pro Shop',
            'value_label'		=> 'Pro Shop',
            'tee_time_index'	=> 0,
            'price_category'	=> 0,
            'points_per_dollar'	=> 12345.00,
            'dollars_per_point'	=> 23456.00
        ];
        $CI->Customer_loyalty->save($this->default_loyalty_rate);

        $this->default_item = [
            'name' => 'Default Test',
            'department' => 'Pro Shop',
            'category' => '',
            'subcategory' => '',
            'description' => '',
            'quantity' => '1',
            'course_id' => $CI->session->userdata('course_id')
        ];
        $CI->Item->save($this->default_item);

        $this->package = new Package();

        $this->package_loyalty_rate = [
            'label'				=> 'Item A',
            'course_id'			=> $CI->session->userdata('course_id'),
            'loyalty_package_id' => $this->package->get_id(),
            'type'				=> 'department',
            'value'				=> 'Pro Shop',
            'value_label'		=> 'Pro Shop',
            'tee_time_index'	=> 0,
            'price_category'	=> 0,
            'points_per_dollar'	=> 11111.00,
            'dollars_per_point'	=> 22222.00
        ];
        $CI->Customer_loyalty->save($this->package_loyalty_rate);

        $this->package_loyalty_item = [
            'name' => 'Package Test',
            'department' => 'Pro Shop',
            'category' => '',
            'subcategory' => '',
            'description' => '',
            'quantity' => '1',
            'course_id' => $CI->session->userdata('course_id')
        ];
        $CI->Item->save($this->package_loyalty_item);
    }

    protected function _after()
    {
        $CI = & get_instance();

        $CI->db->where('item_id', $this->package_loyalty_item['item_id']);
        $CI->db->delete('items');

        $CI->db->where('loyalty_rate_id', $this->package_loyalty_rate['loyalty_rate_id']);
        $CI->db->delete('loyalty_rates');

        $this->package->destroy();

        $CI->db->where('item_id', $this->default_item['item_id']);
        $CI->db->delete('items');

        $CI->db->where('loyalty_rate_id', $this->default_loyalty_rate['loyalty_rate_id']);
        $CI->db->delete('loyalty_rates');
    }

    public function testGetPointsPerDollar()
    {
        $CI = & get_instance();

        $this->specify("Get points per dollar on default NULL package", function() use ($CI) {
            $dollars = $CI->Customer_loyalty->get_dollars_per_point($this->default_item);
            $this->assertEquals(23456.00, $dollars['dollars_per_point']);
        });

        $this->specify("Get points per dollar on new loyalty packages", function() use ($CI) {
            $dollars = $CI->Customer_loyalty->get_dollars_per_point($this->default_item, $this->package->get_id());
            $this->assertEquals(22222.00, $dollars['dollars_per_point']);
        });
    }
}