<?php

namespace libraries\fu\customer_loyalty;

use fu\TestingHelpers\Actor;
use fu\TestingHelpers\Customer;
use fu\TestingHelpers\Package;

class AutoEnrollDefaultTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $package1;
    protected $package2;
    protected $package3;


    protected function _before()
    {
        $this->actor = new Actor();
        $this->actor->login();
        $this->package1 = new Package();
        $this->package2 = new Package(true);
        $this->package3 = new Package(true);
    }

    protected function _after()
    {
        $this->package1->destroy();
        $this->package2->destroy();
        $this->package3->destroy();
    }

    public function testAutoEnroll()
    {
        $CI = & get_instance();
        $CI->load->model('Customers_loyalty_package');

        $this->specify("When course is not loyalty_auto_enroll, don't enroll customer in default loyalty packages", function() use ($CI) {
            $course = ['loyalty_auto_enroll' => 0];
            $CI->Course->save($course, $CI->session->userdata('course_id'));

            $customer = new Customer();

            $packages = $CI->Customers_loyalty_package->get_all($customer->get_id())->result_object();
            $this->assertEmpty($packages);

            $customer->destroy();
        });

        $this->specify("When course is loyalty_auto_enroll and there are default loyalty packages, don't set customer to use_loyalty", function() use ($CI) {
            $course = ['loyalty_auto_enroll' => 1];
            $CI->Course->save($course, $CI->session->userdata('course_id'));

            $customer = new Customer();
            $this->assertEquals(false, $customer->get_use_loyalty());

            $customer->destroy();
        });

        $this->specify("When course is loyalty_auto_enroll and there are default loyalty packages, enroll customer in default loyalty packges", function() use ($CI) {
            $course = ['loyalty_auto_enroll' => 1];
            $CI->Course->save($course, $CI->session->userdata('course_id'));

            $customer = new Customer();
            $packages = $CI->Customers_loyalty_package->get_all($customer->get_id())->result_object();

            $this->assertEquals(2, count($packages));

            foreach($packages as $package) {
                $this->assertTrue($package->loyalty_package_id == $this->package2->get_id() || $package->loyalty_package_id == $this->package3->get_id());
            }

            $customer->destroy();
        });
    }
}