<?php

namespace libraries\fu\customer_loyalty;

use fu\TestingHelpers\Actor;
use fu\TestingHelpers\Customer;
use fu\TestingHelpers\Package;

class AutoEnrollTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $package1;
    protected $package2;
    protected $package3;


    protected function _before()
    {
        $this->actor = new Actor();
        $this->actor->login();
        $this->package1 = new Package();
        $this->package2 = new Package();
        $this->package3 = new Package();
    }

    protected function _after()
    {
        $this->package1->destroy();
        $this->package2->destroy();
        $this->package3->destroy();
    }

    public function testAutoEnroll()
    {
        $CI = & get_instance();
        $CI->load->model('Customers_loyalty_package');

        $this->specify("When course is not loyalty_auto_enroll, don't set customer to use_loyalty", function() use ($CI) {
            $course = ['loyalty_auto_enroll' => 0];
            $CI->Course->save($course, $CI->session->userdata('course_id'));

            $customer = new Customer();
            $this->assertEquals(false, $customer->get_use_loyalty());

            $customer->destroy();
        });

        $this->specify("When course is loyalty_auto_enroll and no default loyalty packages, set customer to use_loyalty", function() use ($CI) {
            $course = ['loyalty_auto_enroll' => 1];
            $CI->Course->save($course, $CI->session->userdata('course_id'));

            $customer = new Customer();
            $this->assertEquals(true, $customer->get_use_loyalty());

            $customer->destroy();
        });

        $this->specify("When course is loyalty_auto_enroll and no default loyalty packages, don't enroll customer in other packages", function() use ($CI) {
            $course = ['loyalty_auto_enroll' => 1];
            $CI->Course->save($course, $CI->session->userdata('course_id'));

            $customer = new Customer();

            $packages = $CI->Customers_loyalty_package->get_all($customer->get_id())->result_object();
            $this->assertEmpty($packages);

            $customer->destroy();
        });
    }
}