<?php
// TableOfAccountsTest.php

namespace libraries\fu\tips;

use \fu\tips\TipsTransactions;

class TipsTransactionsUnitTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	public function testNewTransactions(){
		$transaction = new TipsTransactions('tip_transactions');

		verify(method_exists($transaction,'get_last_error'))->true();
		verify(method_exists($transaction,'reset_last_error'))->true();
		verify(method_exists($transaction,'post'))->true();
		verify(method_exists($transaction,'put'))->true();
		verify(method_exists($transaction,'patch'))->true();
		verify(method_exists($transaction,'delete'))->true();
		verify(method_exists($transaction,'get'))->true();
	}

	public function testPost(){
		$transaction = new \fu\tips\TipsTransactions('tip_transactions');

		$this->specify('The post method should validate on valid inputs',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->post(0,0,null,null,null,true);
			verify($transaction_id)->true();
			verify($transaction->get_last_error())->isEmpty();
		});

		$this->specify('The post method should fail on bad sale id',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->post('not an int',12,null,null,null,true);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->post Error: sale_id is required, and must be an integer.');
		});

		$this->specify('The post method should fail on bad employee_id',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->post(12,'not an int',null,null,null,true);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->post Error: employee_id is required, and must be an integer.');
		});

		$this->specify('The post method should fail on bad transaction_time',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->post(12,12,'not a time',null,null,true);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->post Error: invalid transaction_time provided.');
		});

		$this->specify('The post method should fail on bad notes',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->post(12,12,'2016-12-12 05:00:00',1337,null,true);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->post Error: notes parameter must be a string.');
		});

		$this->specify('The post method should fail on bad comment',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->post(12,12,'2016-12-12 05:00:00','valid note',1337,true);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->post Error: comment parameter must be a string.');
		});

		// the above do not require database access
	}

	public function testDelete(){
		$transaction = new \fu\tips\TipsTransactions('tip_transactions');

		$this->specify('The delete method should fail on bad transaction_id',function() use ($transaction){
			$transaction_id = $transaction->delete('not an int');
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->delete Error: transaction_id is required, and must be an integer.');
		});

		// the above does not require database access
	}

	public function testGet(){
		$transaction = new \fu\tips\TipsTransactions('tip_transactions');
		$base_params = array('sale_id'=>12,'employee_id'=>12,'transaction_id'=>12,
			'transaction_time'=>'2016-12-12 05:00:00');

		$this->specify('The get method should fail if passed a non-int non-array',function() use ($transaction,$base_params){
			$transaction->reset_last_error();
			$rows = $transaction->get('invalid transaction_id',false,true);
			verify($rows)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->get Error: expecting transaction_id integer or parameters array.');
		});
		$this->specify('The get method should validate if passed an integer',function() use ($transaction,$base_params){
			$transaction->reset_last_error();
			$rows = $transaction->get(1,false,true);
			verify($rows)->true();
			verify($transaction->get_last_error())->isEmpty();
		});

		$this->specify('The get method should validate if passed the test array',function() use ($transaction,$base_params){
			$params = $base_params;
			$transaction->reset_last_error();
			$rows = $transaction->get($params,false,true);
			verify($rows)->true();
			verify($transaction->get_last_error())->isEmpty();
		});
		$this->specify('The get method should fail if passed an empty array',function() use ($transaction,$base_params){
			$transaction->reset_last_error();
			$rows = $transaction->get(array(),false,true);
			verify($rows)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->get Error: expecting transaction_id integer or parameters array.');
		});
		$this->specify('The get method should fail if passed an invalid transaction_id',function() use ($transaction,$base_params){
			$params = $base_params;
			$params['transaction_id']='bad_id';
			$transaction->reset_last_error();
			$rows = $transaction->get($params,false,true);
			verify($rows)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->get Error: transaction_id must be an integer.');
		});
		$this->specify('The get method should fail if passed an invalid sale_id',function() use ($transaction,$base_params){
			$params = $base_params;
			$params['sale_id']='bad_id';
			$transaction->reset_last_error();
			$rows = $transaction->get($params,false,true);
			verify($rows)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->get Error: sale_id must be an integer.');
		});
		$this->specify('The get method should fail if passed an invalid employee_id',function() use ($transaction,$base_params){
			$params = $base_params;
			$params['employee_id']='bad_id';
			$transaction->reset_last_error();
			$rows = $transaction->get($params,false,true);
			verify($rows)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->get Error: employee_id must be an integer.');
		});
		$this->specify('The get method should fail if passed an invalid transaction_time',function() use ($transaction,$base_params){
			$params = $base_params;
			$params['transaction_time']='bad_time';
			$transaction->reset_last_error();
			$rows = $transaction->get($params,false,true);
			verify($rows)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->get Error: invalid transaction_time provided.');
		});
	}

	public function testPatch(){

		$transaction = new \fu\tips\TipsTransactions('tip_transactions');
		$base_params = array('sale_id'=>12,'employee_id'=>12,'transaction_id'=>12,
			'transaction_time'=>'2016-12-12 05:00:00','note'=>'some note','comment'=>'some comment');

		$this->specify('The patch method should fail on empty sale id and transaction_id',function() use ($transaction,$base_params){
			$params = $base_params;
			unset($params['sale_id']);
			unset($params['transaction_id']);
			$transaction_id = $transaction->patch($params);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->patch Error: transaction_id or combination of sale_id, employee_id and transaction_time are required.');
		});

		$this->specify('The patch method should fail on empty employee id and transaction_id',function() use ($transaction,$base_params){
			$params = $base_params;
			unset($params['employee_id']);
			unset($params['transaction_id']);
			$transaction->reset_last_error();
			$transaction_id = $transaction->patch($params);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->patch Error: transaction_id or combination of sale_id, employee_id and transaction_time are required.');
		});

		$this->specify('The patch method should fail on empty transaction_time and transaction_id',function() use ($transaction,$base_params){
			$params = $base_params;
			unset($params['transaction_time']);
			unset($params['transaction_id']);
			$transaction->reset_last_error();
			$transaction_id = $transaction->patch($params);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->patch Error: transaction_id or combination of sale_id, employee_id and transaction_time are required.');
		});

		$this->specify('The patch method should fail on bad employee_id',function() use ($transaction,$base_params){
			$params = $base_params;
			$params['employee_id']='bad_id';
			$transaction->reset_last_error();
			$transaction_id = $transaction->patch($params);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->patch Error: employee_id is required, and must be an integer.');
		});

		$this->specify('The patch method should fail on bad transaction_id',function() use ($transaction,$base_params){
			$params = $base_params;
			$params['transaction_id']='bad_id';
			$transaction->reset_last_error();
			$transaction_id = $transaction->patch($params);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->patch Error: transaction_id must be an integer.');
		});

		$this->specify('The patch method should fail on bad transaction_time',function() use ($transaction,$base_params){
			$params = $base_params;
			$params['transaction_time']='bad time';
			$transaction->reset_last_error();
			$transaction_id = $transaction->patch($params);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->patch Error: invalid transaction_time provided.');
		});

		$this->specify('The patch method should fail on bad notes',function() use ($transaction,$base_params){
			$params = $base_params;
			$params['notes']=1337;
			$transaction->reset_last_error();
			$transaction_id = $transaction->patch($params);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->patch Error: notes parameter must be a string.');
		});

		$this->specify('The patch method should fail on bad comment',function() use ($transaction,$base_params){
			$params = $base_params;
			$params['comment']=1337;
			$transaction->reset_last_error();
			$transaction_id = $transaction->patch($params);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->patch Error: comment parameter must be a string.');
		});
	}

	public function testPut(){
		$transaction = new \fu\tips\TipsTransactions('tip_transactions');

		$this->specify('The put method should fail on bad sale id',function() use ($transaction){
			$transaction_id = $transaction->put('not an int',12);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->put Error: sale_id is required, and must be an integer.');
		});

		$this->specify('The put method should fail on bad employee_id',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->put(12,'not an int');
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->put Error: employee_id is required, and must be an integer.');
		});

		$this->specify('The put method should fail on bad transaction_id',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->put(12,12,'not a transaction_id');
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->put Error: transaction_id must be an integer.');
		});

		$this->specify('The put method should fail on bad transaction_time',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->put(12,12,12,'not a time');
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->put Error: invalid transaction_time provided.');
		});

		$this->specify('The put method should fail on bad notes',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->put(12,12,12,'2016-12-12 05:00:00',1337);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->put Error: notes parameter must be a string.');
		});

		$this->specify('The put method should fail on bad comment',function() use ($transaction){
			$transaction->reset_last_error();
			$transaction_id = $transaction->put(12,12,12,'2016-12-12 05:00:00','valid note',1337);
			verify($transaction_id)->false();
			verify($transaction->get_last_error())->notEmpty();
			verify($transaction->get_last_error())->equals('TipsTransactions->put Error: comment parameter must be a string.');
		});
	}
}

?>