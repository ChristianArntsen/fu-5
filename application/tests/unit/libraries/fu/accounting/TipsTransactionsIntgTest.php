<?php

namespace libraries\fu\tips;

use \fu\tips\TipsTransactions;
use \fu\accounting\TableOfAccounts;
use fu\TestingHelpers\Actor;
use fu\TestingHelpers\TestingObjects\Item;
use fu\TestingHelpers\TestingObjects\Payment;
use fu\TestingHelpers\TestingObjects\TipPayment;

class TipsTransactionsIntigTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	protected function _before()
	{
		$this->actor = new Actor();
	}

	public function testIntegration(){
		//*
		$TOA = new TableOfAccounts();
		$transactions = new TipsTransactions('tip_transactions');

		$paymentObj = new Payment();
		$tipPaymentObj = new TipPayment();
		$itemObj = new Item();
		$item = $itemObj->getArray();
		$item['unit_price_includes_tax'] = true;
		$payment = $paymentObj->getArray();
		$payment['amount'] = 30.00;
		$tipPayment = $tipPaymentObj->getArray();

		$CI = get_instance();

		$CI->db->trans_start(TRUE); // testing mode...

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);
		//$cart_model->save_payment($tipPayment);

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);
		$sale_id = $response['sale_id'];
		/*
		$CI->load->model('payment');
		$CI->payment->add(array(
			'sale_id' => $sale_id,
			'type' => 'cash',
			'payment_type' => 'Cash Tip',
			'payment_amount' => 5.55,
			'invoice_id' => 0,
			'tip_recipient' => 12,// this is person id
			'is_tip' => 1
		));
		*/

		$emp_id = $CI->session->userdata('emp_id');
		$person_id = $CI->session->userdata('person_id');

		$result = $TOA->get(array('course_id' => $CI->session->userdata('course_id'),'name'=>'tips_expense'));
		if(empty($result)){
			$account_id = $TOA->post($CI->session->userdata('course_id'),'tips_expense',
				'expense','An expense account used for tracking tips owed to waitstaff.');
		}else{
			$account_id = $result[0]['account_id'];
		}
		verify(is_numeric($account_id))->true();
		verify(is_int($account_id*1))->true();

/*// Tests appear to work, but bad stuff happens with output buffer...

		$this->specify('The post method should fail when we attempt to link to non-existent sale.',function() use ($transactions,$sale_id,$account_id) {
			$transactions->reset_last_error();
			$result = $transactions->post(2,12,null,'Cash Tip');
			verify($result)->false();
			verify($transactions->get_last_error())->contains('a foreign key constraint fails');
		});

		$this->specify('The post method should fail when we attempt to link to non-existent employee.',function() use ($transactions,$sale_id,$account_id) {
			$transactions->reset_last_error();
			$result = $transactions->post($sale_id,0,null,'Cash Tip');
			verify($result)->false();
			verify($transactions->get_last_error())->contains('a foreign key constraint fails');
		});
//*/
		$this->specify('The post method should succeed when we use valid inputs.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->post($sale_id,$emp_id,null,'Cash Tip');
			verify(is_numeric($result))->true();
			verify(is_int($result*1))->true();
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The get method should return an empty array for non-existent transaction.',function() use ($transactions,$sale_id,$account_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(-1);
			verify($result)->isEmpty();
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The get method should return our transaction for the sale_id.',function() use ($transactions,$sale_id,$account_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();
			verify($result[0]['sale_id'])->equals($sale_id);
			verify($result[0]['notes'])->equals('Cash Tip');
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The get method should return our transaction for the transaction_id.',function() use ($transactions,$sale_id,$account_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$result = $transactions->get(array('transaction_id'=>$result[0]['transaction_id']));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The get method should return one or more transactions for our employtee_id.',function() use ($transactions,$sale_id,$account_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$result = $transactions->get(array('employee_id'=>$result[0]['employee_id']));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The get method should return one or more transactions for our notes.',function() use ($transactions,$sale_id,$account_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$result = $transactions->get(array('notes'=>$result[0]['notes']));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The get method should return one or more transactions for our transaction_time.',function() use ($transactions,$sale_id,$account_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$result = $transactions->get(array('transaction_time'=>$result[0]['transaction_time']));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The put method should update notes.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$transaction_id = $result[0]['transaction_id'];
			$transaction_time = $result[0]['transaction_time'];

			$result = $transactions->put($sale_id,$emp_id,$transaction_id,null,'Check Tip');
			verify($result)->equals($transaction_id);

			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();

			verify($result[0]['notes'])->equals('Check Tip');
			verify($result[0]['transaction_time'])->equals($transaction_time);
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The put method should update comment.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$transaction_id = $result[0]['transaction_id'];

			$result = $transactions->put($sale_id,$emp_id,$transaction_id,null,'Check Tip','Some Comment');
			verify($result)->equals($transaction_id);

			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();

			verify($result[0]['comment'])->equals('Some Comment');
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The put method should unset notes and comment.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$transaction_id = $result[0]['transaction_id'];

			$result = $transactions->put($sale_id,$emp_id,$transaction_id);
			verify($result)->equals($transaction_id);

			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();
			verify(empty($result[0]['notes']))->true();
			verify(empty($result[0]['comment']))->true();
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The delete method should soft delete the transaction.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$transaction_id = $result[0]['transaction_id'];

			$result = $transactions->delete($transaction_id);
			verify($result)->equals(1);

			// check it is soft deleted.
			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->isEmpty();
			$result = $transactions->get(array('sale_id'=>$sale_id),true);
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();
			verify(empty($result[0]['notes']))->true();
			verify(empty($result[0]['comment']))->true();
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The put method should un soft delete.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id),true);
			$transaction_id = $result[0]['transaction_id'];

			$result = $transactions->put($sale_id,$emp_id,$transaction_id);
			verify($result)->equals($transaction_id);

			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();
			verify(empty($result[0]['notes']))->true();
			verify(empty($result[0]['comment']))->true();
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The patch method should update notes.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$transaction_id = $result[0]['transaction_id'];
			$transaction_time = $result[0]['transaction_time'];

			$result = $transactions->patch(array('transaction_id'=>$transaction_id,'notes'=>'Cash Tip'));
			verify($result)->equals($transaction_id);

			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();

			verify($result[0]['notes'])->equals('Cash Tip');
			verify($result[0]['transaction_time'])->equals($transaction_time);
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The patch method should update comment.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$transaction_id = $result[0]['transaction_id'];
			$transaction_time = $result[0]['transaction_time'];

			$result = $transactions->patch(array('transaction_id'=>$transaction_id,'comment'=>'Some Comment'));
			verify($result)->equals($transaction_id);

			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();

			verify($result[0]['comment'])->equals('Some Comment');
			verify($result[0]['transaction_time'])->equals($transaction_time);
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The patch method should not unset anything.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$transaction_id = $result[0]['transaction_id'];
			$transaction_time = $result[0]['transaction_time'];

			$result = $transactions->patch(array('transaction_id'=>$transaction_id));
			verify($result)->equals($transaction_id);

			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();

			verify($result[0]['comment'])->equals('Some Comment');
			verify($result[0]['notes'])->equals('Cash Tip');
			verify($result[0]['transaction_time'])->equals($transaction_time);
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The patch method should not un soft delete anything.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id));
			$transaction_id = $result[0]['transaction_id'];
			$transaction_time = $result[0]['transaction_time'];

			$result = $transactions->delete($transaction_id);
			verify($result)->equals(1);

			// check it is soft deleted.
			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->isEmpty();

			$result = $transactions->patch(array('transaction_id'=>$transaction_id));
			verify($result)->equals($transaction_id);

			// check it is soft deleted.
			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->isEmpty();

			$result = $transactions->get(array('sale_id'=>$sale_id),true);
			verify($result)->notEmpty();
			verify(is_array($result))->true();
			verify(count($result))->greaterThan(0);
			verify(is_numeric($result[0]['transaction_id']))->true();
			verify(is_int($result[0]['transaction_id']*1))->true();

			verify($result[0]['comment'])->equals('Some Comment');
			verify($result[0]['notes'])->equals('Cash Tip');
			verify($result[0]['transaction_time'])->equals($transaction_time);
			verify($transactions->get_last_error())->isEmpty();
		});

		$this->specify('The delete method should hard delete the transaction.',function() use ($transactions,$sale_id,$account_id,$emp_id) {
			$transactions->reset_last_error();
			$result = $transactions->get(array('sale_id'=>$sale_id),true);
			$transaction_id = $result[0]['transaction_id'];

			$result = $transactions->delete($transaction_id,true);
			verify($result)->equals(1);

			// check it is soft deleted.
			$result = $transactions->get(array('sale_id'=>$sale_id));
			verify($result)->isEmpty();
			$result = $transactions->get(array('sale_id'=>$sale_id),true);
			verify($result)->isEmpty();
		});
		//*/
		$CI->db->trans_complete(); // rolled back at this point
	}


}