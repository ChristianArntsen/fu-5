<?php
// TableOfAccountsTest.php

namespace libraries\fu\tips;

use fu\tips\TipsJournal;
use \fu\tips\TipsTransactions;
use \fu\accounting\TableOfAccounts;
use fu\TestingHelpers\Actor;
use fu\TestingHelpers\TestingObjects\Item;
use fu\TestingHelpers\TestingObjects\Payment;
use fu\TestingHelpers\TestingObjects\TipPayment;

class TipsJournalIntigTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	protected function _before()
	{
		$this->actor = new Actor();
	}

	public function testIntegration()
	{
		$TOA = new TableOfAccounts();
		$transactions = new TipsTransactions('tip_transactions');
		$journal = new TipsJournal('tip_journal');

		$paymentObj = new Payment();
		$tipPaymentObj = new TipPayment();
		$itemObj = new Item();
		$item = $itemObj->getArray();
		$item['unit_price_includes_tax'] = true;
		$payment = $paymentObj->getArray();
		$payment['amount'] = 30.00;
		$tipPayment = $tipPaymentObj->getArray();

		$CI = get_instance();

		$this->actor->login(); // need to log out, or breaks other tests
		$CI->load->model('v2/Cart_model');
		$cart_model = new \Cart_model();
		$cart_model->save_item(1, $item);
		$cart_model->save_payment($payment);

		$sale_msg = "";
		$response = $cart_model->save_sale($sale_msg);
		$sale_id = $response['sale_id'];
		/*
		$CI->load->model('payment');
		$CI->payment->add(array(
			'sale_id' => $sale_id,
			'type' => 'cash',
			'payment_type' => 'Cash Tip',
			'payment_amount' => 5.55,
			'invoice_id' => 0,
			'tip_recipient' => 12,// this is person id
			'is_tip' => 1
		));
		*/

		$emp_id = $CI->session->userdata('emp_id');
		$person_id = $CI->session->userdata('person_id');

		// make sure we have the Tip Expense account
		$result = $TOA->get(array('course_id' => $CI->session->userdata('course_id'),'name'=>'tips_expense'));
		if(empty($result)){
			$account_id = $TOA->post($CI->session->userdata('course_id'),'tips_expense',
				'expense','An expense account used for tracking tips owed to waitstaff.');
		}else{
			$account_id = $result[0]['account_id'];
		}
		verify(is_numeric($account_id))->true();
		verify(is_int($account_id*1))->true();

		// Create our transaction
		$transaction_id = $transactions->post($sale_id,$emp_id,null,'Cash Tip','This is a test transaction');
		verify(is_int($transaction_id*1))->true();

		$this->specify('The post method should succeed when we use valid account inputs.',function() use ($journal,$transaction_id,$account_id) {
			$journal->reset_last_error();
			$result = $journal->post($transaction_id,'debit',10.75,$account_id,null,'Tip Expense',null);
			verify($result)->true();
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The post method should succeed when we use valid employee inputs.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();
			$result = $journal->post($transaction_id,'credit',10.75,null,$emp_id,'Tip Payable: <Employee Name>',null);
			verify($result)->true();
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The get method should succeed when we use valid account inputs.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();
			$result = $journal->get(array('account_id'=>$account_id,'transaction_id'=>$transaction_id));
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify($result[0]['transaction_id'])->equals($transaction_id);
			verify($result[0]['account_id'])->equals($account_id);
			verify($result[0]['employee_id'])->isEmpty();
			verify($result[0]['d_c'])->equals('debit');
			verify($result[0]['notes'])->equals('Tip Expense');
			verify($result[0]['comment'])->isEmpty();
			verify($result[0]['deleted'])->equals(0);
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The get method should succeed when we use valid employee inputs.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();
			$result = $journal->get(array('employee_id'=>$emp_id,'transaction_id'=>$transaction_id));
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify($result[0]['employee_id'])->equals($emp_id);
			verify($result[0]['d_c'])->equals('credit');
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The get method should succeed when getting multiple rows.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();
			$result = $journal->get(array('transaction_id'=>$transaction_id));
			verify(is_array($result))->true();
			verify(count($result))->equals(2);
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The put method should succeed when updating comment.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();
			$result = $journal->put($transaction_id,'debit',10.75,$account_id,null,'Tip Expense','Some Comment');
			verify($result)->true();
			$result = $journal->get(array('account_id'=>$account_id,'transaction_id'=>$transaction_id));
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify($result[0]['comment'])->equals('Some Comment');
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The put method should succeed when updating employee comment.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();
			$result = $journal->put($transaction_id,'credit',10.75,null,$emp_id,'Tip Payable: <Employee Name>','Some Comment');
			verify($result)->true();
			$result = $journal->get(array('employee_id'=>$emp_id,'transaction_id'=>$transaction_id));
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify($result[0]['comment'])->equals('Some Comment');
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The delete method should succeed in soft deleting.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();

			$result = $journal->delete($transaction_id,$account_id);
			verify($result)->equals(1);

			$result = $journal->get(array('account_id'=>$account_id,'transaction_id'=>$transaction_id));
			verify($result)->isEmpty();
			verify($journal->get_last_error())->isEmpty();

			$result = $journal->get(array('account_id'=>$account_id,'transaction_id'=>$transaction_id),true);
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
		});

		$this->specify('The put method should un soft delete.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();

			$result = $journal->get(array('account_id'=>$account_id,'transaction_id'=>$transaction_id));
			verify($result)->isEmpty();
			verify($journal->get_last_error())->isEmpty();

			$result = $journal->put($transaction_id,'debit',10.75,$account_id,null,'Tip Expense','Some Comment');
			verify($result)->true();
			$result = $journal->get(array('account_id'=>$account_id,'transaction_id'=>$transaction_id));
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify($result[0]['comment'])->equals('Some Comment');
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The put method should succeed in un setting comment.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();
			$result = $journal->put($transaction_id,'debit',10.75,$account_id,null,'Tip Expense',null);
			verify($result)->true();
			$result = $journal->get(array('account_id'=>$account_id,'transaction_id'=>$transaction_id));
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify($result[0]['comment'])->isEmpty();
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The patch method should succeed when updating comment.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();
			$result = $journal->patch(array('account_id'=>$account_id,'transaction_id'=>$transaction_id,'comment'=>'Some Comment2'));
			verify($result)->true();
			$result = $journal->get(array('account_id'=>$account_id,'transaction_id'=>$transaction_id));
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify($result[0]['comment'])->equals('Some Comment2');
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The patch method should succeed when updating employee comment.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();
			$result = $journal->patch(array('employee_id'=>$emp_id,'transaction_id'=>$transaction_id,'comment'=>'Some Comment3'));
			verify($result)->true();
			$result = $journal->get(array('employee_id'=>$emp_id,'transaction_id'=>$transaction_id));
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify($result[0]['comment'])->equals('Some Comment3');
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The patch method should succeed when unsetting employee comment.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();
			$result = $journal->patch(array('employee_id'=>$emp_id,'transaction_id'=>$transaction_id,'comment'=>null));
			verify($result)->true();
			$result = $journal->get(array('employee_id'=>$emp_id,'transaction_id'=>$transaction_id));
			verify(is_array($result))->true();
			verify(count($result))->equals(1);
			verify($result[0]['comment'])->isEmpty();
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The delete method should succeed in hard deleting.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();

			$result = $journal->delete($transaction_id,$account_id,null,true);
			verify($result)->equals(1);

			$result = $journal->get(array('account_id'=>$account_id,'transaction_id'=>$transaction_id));
			verify($result)->isEmpty();
			verify($journal->get_last_error())->isEmpty();

			$result = $journal->get(array('account_id'=>$account_id,'transaction_id'=>$transaction_id),true);
			verify($result)->isEmpty();
			verify($journal->get_last_error())->isEmpty();
		});

		$this->specify('The delete method should succeed in hard deleting emp.',function() use ($journal,$transaction_id,$account_id,$emp_id) {
			$journal->reset_last_error();

			$result = $journal->delete($transaction_id,null,$emp_id,true);
			verify($result)->equals(1);

			$result = $journal->get(array('employee_id'=>$emp_id,'transaction_id'=>$transaction_id));
			verify($result)->isEmpty();
			verify($journal->get_last_error())->isEmpty();

			$result = $journal->get(array('employee_id'=>$emp_id,'transaction_id'=>$transaction_id),true);
			verify($result)->isEmpty();
			verify($journal->get_last_error())->isEmpty();
		});
	}

}