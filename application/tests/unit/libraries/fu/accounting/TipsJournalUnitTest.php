<?php
// TableOfAccountsTest.php

namespace libraries\fu\tips;

use fu\tips\TipsJournal;
use \fu\tips\TipsTransactions;

class TipsJournalUnitTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	public function testNewEntry(){
		$entry = new TipsJournal('tip_journal');
		verify(method_exists($entry,'get_last_error'))->true();
		verify(method_exists($entry,'reset_last_error'))->true();
		verify(method_exists($entry,'post'))->true();
		verify(method_exists($entry,'put'))->true();
		verify(method_exists($entry,'patch'))->true();
		verify(method_exists($entry,'delete'))->true();
		verify(method_exists($entry,'get'))->true();
	}

	public function testPost(){
		$entry = new TipsJournal('tip_journal');

		$this->specify('The post method should succeed with good test parameters',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'debit',10.25,12,12,'notes','comment',true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The post method should fail on bad transaction_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post('not an int','debit',10.25,12,12,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->post Error: transaction_id is required, and must be an integer.');
		});

		$this->specify('The post method should fail on bad d_c',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'not debit or credit',10.25,12,12,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->post Error: d_c must be a string with the value "debit" or "credit".');
		});

		$this->specify('The post method should fail on bad amount',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'credit','not numeric',12,12,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->post Error: amount must be a non-negative numeric value.');
		});

		$this->specify('The post method should fail on negative amount',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'credit',-10.25,12,12,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->post Error: amount must be a non-negative numeric value.');
		});

		$this->specify('The post method should fail on bad account_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'credit',10.25,'not int',12,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->post Error: account_id must be an integer.');
		});

		$this->specify('The post method should fail on unset account_id and employee_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'credit',10.25,null,null,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->post Error: either account_id or employee id must be set.');
		});

		$this->specify('The post method should succeed with account_id unset',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'debit',10.25,null,12,'notes','comment',true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The post method should succeed with employee_id unset',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'debit',10.25,12,null,'notes','comment',true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The post method should fail on bad employee_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'credit',10.25,12,'not int','notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->post Error: employee_id must be an integer.');
		});

		$this->specify('The post method should fail on bad note',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'credit',10.25,12,12,1337,'comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->post Error: notes must be a string.');
		});

		$this->specify('The post method should fail on bad comment',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->post(12,'credit',10.25,12,12,'note',1337,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->post Error: comment must be a string.');
		});
	}

	public function testDelete(){
		$entry = new TipsJournal('tip_journal');

		$this->specify('The delete method should succeed with good test parameters',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->delete(12,12,12,false,true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The delete method should fail on bad transaction_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->delete('bad id',12,12,false,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->delete Error: transaction_id is required, and must be an integer.');
		});

		$this->specify('The delete method should fail on unset account_id and employee_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->delete(12,null,null,false,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->delete Error: either account_id or employee_id must be set.');
		});

		$this->specify('The delete method should succeed with account_id unset',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->delete(12,null,12,false,true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The delete method should succeed with employee_id unset',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->delete(12,12,null,false,true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The delete method should fail on bad employee_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->delete(12,12,'bad id',false,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->delete Error: employee_id must be an integer.');
		});
	}

	public function testGet(){
		$entry = new TipsJournal('tip_journal');
		$base_params = array('transaction_id'=>12,'employee_id'=>12,'d_c'=>'debit','account_id'=>12,
			'amount'=>10.25,'notes'=>'some note','comment'=>'some comment');

		$this->specify('The get method should validate with good query parameters',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$result = $entry->get($params,false,true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The get method should fail on bad transaction_id',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['transaction_id'] = 'not int';
			$result = $entry->get($params,false,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->get Error: transaction_id must be an integer.');
		});

		$this->specify('The get method should if developer adds but forgets to validate a parameter',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['test_unvalidated'] = 'not int';
			$error = null;

			// TODO: working test for the exception
			// this seems to break the test harness' brain...
			//$this->setExpectedException('Exception','TipsJournal->get UserError: un-validated parameter: test_unvalidated. Please add validation.');
			//$entry->get($params, false, true);
	});

		$this->specify('The get method should fail on bad employee_id',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['employee_id'] = 'not int';
			$result = $entry->get($params,false,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->get Error: employee_id must be an integer.');
		});

		$this->specify('The get method should fail on bad account_id',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['account_id'] = 'not int';
			$result = $entry->get($params,false,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->get Error: account_id must be an integer.');
		});

		$this->specify('The get method should fail on bad d_c',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['d_c'] = 'not debit or credit';
			$result = $entry->get($params,false,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->get Error: d_c must be a string with the value "debit" or "credit".');
		});

		$this->specify('The get method should fail on bad amount',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['amount'] = 'not int';
			$result = $entry->get($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->get Error: amount must be a non-negative numeric value.');
		});

		$this->specify('The get method should fail on negative amount',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['amount'] = -10.25;
			$result = $entry->get($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->get Error: amount must be a non-negative numeric value.');
		});

		$this->specify('The get method should fail on bad notes',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['notes'] = 1337;
			$result = $entry->get($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->get Error: notes must be a string.');
		});

		$this->specify('The get method should fail on bad comment',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['comment'] = 1337;
			$result = $entry->get($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->get Error: comment must be a string.');
		});
	}

	public function testPatch(){

		$entry = new TipsJournal('tip_journal');
		$base_params = array('transaction_id'=>12,'employee_id'=>12,'d_c'=>'debit','account_id'=>12,
			'amount'=>10.25,'notes'=>'some note','comment'=>'some comment');

		$this->specify('The patch method should succeed with good test parameters',function() use ($entry,$base_params){

			$entry->reset_last_error();
			$params = $base_params;
			$result = $entry->patch($params,true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The patch method should fail on bad transaction_id',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['transaction_id'] = 'not int';
			$result = $entry->patch($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->patch Error: transaction_id is required, and must be an integer.');
		});

		$this->specify('The patch method should fail on bad d_c',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['d_c'] = 'not debit or credit';
			$result = $entry->patch($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->patch Error: d_c must be a string with the value "debit" or "credit".');
		});

		$this->specify('The patch method should fail on bad amount',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['amount'] = 'not int';
			$result = $entry->patch($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->patch Error: amount must be a non-negative numeric value.');
		});

		$this->specify('The patch method should fail on negative amount',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['amount'] = -10.25;
			$result = $entry->patch($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->patch Error: amount must be a non-negative numeric value.');
		});

		$this->specify('The patch method should fail on bad account_id',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['account_id'] = 'not int';
			$result = $entry->patch($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->patch Error: account_id must be an integer.');
		});

		$this->specify('The patch method should fail on unset account_id and employee_id',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			unset($params['account_id']);
			unset($params['employee_id']);
			$result = $entry->patch($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->patch Error: either account_id or employee id must be set.');
		});

		$this->specify('The patch method should succeed with account_id unset',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			unset($params['account_id']);
			$result = $entry->patch($params,true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The patch method should succeed with employee_id unset',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			unset($params['employee_id']);
			$result = $entry->patch($params,true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The patch method should fail on bad employee_id',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['employee_id'] = 'not int';
			$result = $entry->patch($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->patch Error: employee_id must be an integer.');
		});

		$this->specify('The patch method should fail on bad notes',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['notes'] = 1337;
			$result = $entry->patch($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->patch Error: notes must be a string.');
		});

		$this->specify('The patch method should fail on bad comment',function() use ($entry,$base_params){
			$entry->reset_last_error();
			$params = $base_params;
			$params['comment'] = 1337;
			$result = $entry->patch($params,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->patch Error: comment must be a string.');
		});
	}

	public function testPut(){
		$entry = new TipsJournal('tip_journal');

		$this->specify('The put method should succeed with good test parameters',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'debit',10.25,12,12,'notes','comment',true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The put method should fail on bad transaction_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put('not an int','debit',10.25,12,12,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->put Error: transaction_id is required, and must be an integer.');
		});

		$this->specify('The put method should fail on bad d_c',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'not debit or credit',10.25,12,12,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->put Error: d_c must be a string with the value "debit" or "credit".');
		});

		$this->specify('The put method should fail on bad amount',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'credit','not numeric',12,12,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->put Error: amount must be a non-negative numeric value.');
		});

		$this->specify('The put method should fail on negative amount',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'credit',-10.25,12,12,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->put Error: amount must be a non-negative numeric value.');
		});

		$this->specify('The put method should fail on bad account_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'credit',10.25,'not int',12,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->put Error: account_id must be an integer.');
		});

		$this->specify('The put method should fail on unset account_id and employee_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'credit',10.25,null,null,'notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->put Error: either account_id or employee id must be set.');
		});

		$this->specify('The put method should succeed with account_id unset',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'debit',10.25,null,12,'notes','comment',true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The put method should succeed with employee_id unset',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'debit',10.25,12,null,'notes','comment',true);
			verify($result)->true();
			verify($entry->get_last_error())->isEmpty();
		});

		$this->specify('The put method should fail on bad employee_id',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'credit',10.25,12,'not int','notes','comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->put Error: employee_id must be an integer.');
		});

		$this->specify('The put method should fail on bad note',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'credit',10.25,12,12,1337,'comment',true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->put Error: notes must be a string.');
		});

		$this->specify('The put method should fail on bad comment',function() use ($entry){
			$entry->reset_last_error();
			$result = $entry->put(12,'credit',10.25,12,12,'note',1337,true);
			verify($result)->false();
			verify($entry->get_last_error())->notEmpty();
			verify($entry->get_last_error())->equals('TipsJournal->put Error: comment must be a string.');
		});
	}
}

?>