<?php

namespace libraries\fu\tips;

use \fu\tips\TipTransaction;

class TipTransactionUnitTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	public function testNewTransaction(){
		$transaction = new TipTransaction('tip_transactions');

		verify(method_exists($transaction,'get_accounts'))->true();
		verify(method_exists($transaction,'get_tip_expense_account'))->true();
		verify(method_exists($transaction,'set_transaction_id'))->true();
		verify(method_exists($transaction,'save'))->true();
		verify(method_exists($transaction,'set'))->true();
		verify(method_exists($transaction,'is_balanced'))->true();
		verify(method_exists($transaction,'reload'))->true();
		verify(method_exists($transaction,'commit'))->true();
		verify(method_exists($transaction,'blank_journal_entry'))->true();
		verify(method_exists($transaction,'get_journal_entry'))->true();
		verify(method_exists($transaction,'put_journal_entry'))->true();
		verify(method_exists($transaction,'delete_journal_entry'))->true();
	}

	public function testGetAccounts(){
		$course_id = 6270;
		$transaction = new TipTransaction($course_id);

		verify($transaction->get_accounts('not int'))->false();
		verify($transaction->get_last_error())->notEmpty();
		$transaction->reset_last_error();
		verify($transaction->get_last_error())->isEmpty();
		verify($transaction->get_accounts($course_id,true))->true();
		verify($transaction->get_last_error())->isEmpty();
	}

	public function testGetTipExpenseAccount(){
		$course_id = 6270;
		$transaction = new TipTransaction($course_id);

		verify($transaction->get_tip_expense_account('not_int',true))->false();
		verify($transaction->get_last_error())->notEmpty();
		$transaction->reset_last_error();
		verify($transaction->get_last_error())->isEmpty();
		verify($transaction->get_tip_expense_account($course_id,true))->true();
		verify($transaction->get_last_error())->isEmpty();
	}

	public function testSetTransactionId(){
		$course_id = 6270;
		$transaction = new TipTransaction($course_id);

		verify($transaction->set_transaction_id('not_int',true))->false();
		verify($transaction->get_last_error())->notEmpty();
		$transaction->reset_last_error();
		verify($transaction->get_last_error())->isEmpty();
		verify($transaction->set_transaction_id(1,true))->true();
		verify($transaction->get_last_error())->isEmpty();
	}

	public function testSave(){
		$course_id = 6270;
		$transaction = new TipTransaction($course_id);

		verify($transaction->save('not_int','not_int',null,null,null,true))->equals(false);
		verify($transaction->get_last_error())->notEmpty();
		$transaction->reset_last_error();
		verify($transaction->get_last_error())->isEmpty();
		verify($transaction->save(1,1,null,null,null,true))->equals(1);
		verify($transaction->get_last_error())->isEmpty();
	}

	public function testGet(){
		$course_id = 6270;
		$transaction = new TipTransaction($course_id);

		verify($transaction->get('course_id'))->false();
		verify($transaction->get_last_error())->notEmpty();
		verify($transaction->get_last_error())->equals('TipTransaction->get Error: no transaction loaded. Please use set_transaction_id.');
		$transaction->reset_last_error();

		verify($transaction->save(1,1,null,null,null,true))->equals(1);
		verify($transaction->get_last_error())->isEmpty();

		verify($transaction->get('not key'))->false();
		verify($transaction->get_last_error())->notEmpty();
		verify($transaction->get_last_error())->equals('TipTransaction->get Error: cannot get that key.');
		$transaction->reset_last_error();

		verify($transaction->get('transaction_id'))->equals(1);
		verify(is_array($transaction->get(array('course_id'=>$course_id))))->true();
		verify(is_array($transaction->get('1')))->true();
	}

	public function testSet(){
		$course_id = 6270;
		$transaction = new TipTransaction($course_id);

		verify($transaction->set('notes',2))->false();
		verify($transaction->get_last_error())->notEmpty();
		verify($transaction->get_last_error())->equals('TipTransaction->set Error: no transaction loaded. Please use set_transaction_id.');
		$transaction->reset_last_error();

		verify($transaction->save(1,1,null,null,null,true))->equals(1);
		verify($transaction->get_last_error())->isEmpty();

		verify($transaction->set('not key',4))->false();
		verify($transaction->get_last_error())->notEmpty();
		verify($transaction->get_last_error())->equals('TipTransaction->set Error: cannot set that key.');
		$transaction->reset_last_error();

		verify($transaction->set('transaction_id',2))->false();
		verify($transaction->get_last_error())->notEmpty();
		verify($transaction->get_last_error())->equals('TipTransaction->set Error: cannot set that key.');
		$transaction->reset_last_error();

		verify($transaction->set('employee_id',2))->true();
		verify($transaction->get_last_error())->isEmpty();
	}

	public function testBlankJournalEntry(){
		$course_id = 6270;
		$transaction = new TipTransaction($course_id);

		verify($transaction->blank_journal_entry())->false();
		verify($transaction->get_last_error())->equals('TipTransaction->blank_journal_entry Error: no transaction loaded.');
		$transaction->reset_last_error();

		verify($transaction->save(1,1,null,null,null,true))->equals(1);
		verify($transaction->get_last_error())->isEmpty();

		$entry = $transaction->blank_journal_entry();
		verify(is_array($entry))->true();
		verify($entry['transaction_id'])->equals(1);
	}

	public function testGetPutDeleteJournalEntry(){
		$course_id = 6270;
		$transaction = new TipTransaction($course_id);
		$entry = $transaction->blank_journal_entry();

		$this->specify('Get and put setup errors',function() use ($transaction,$entry) {
			verify($transaction->put_journal_entry(array()))->false();
			verify($transaction->get_last_error())->equals('TipTransaction->put_journal_entry Error: no transaction loaded.');
			$transaction->reset_last_error();

			verify($transaction->delete_journal_entry(array()))->false();
			verify($transaction->get_last_error())->equals('TipTransaction->delete_journal_entry Error: no transaction loaded.');
			$transaction->reset_last_error();

			verify($transaction->get_journal_entry())->false();
			verify($transaction->get_last_error())->equals('TipTransaction->get_journal_entry Error: no transaction loaded.');
			$transaction->reset_last_error();
		});

		verify($transaction->save(1, 1, null, null, null, true))->equals(1);
		verify($transaction->get_last_error())->isEmpty();

		// required parameters
		$this->specify('put required parameters',function() use ($transaction,$entry) {
			verify($transaction->put_journal_entry($entry))->false();
			verify($transaction->get_last_error())->equals('TipTransaction->put_journal_entry Error: TipsJournal->post Error: either account_id or employee id must be set.');
			$transaction->reset_last_error();
			verify($transaction->put_journal_entry($entry))->false();
			verify($transaction->get_last_error())->contains('either account_id or employee');
			$entry['employee_id'] = 1;
			verify($transaction->put_journal_entry($entry))->false();
			verify($transaction->get_last_error())->contains('d_c');
			$entry['d_c'] = 'debit';
			verify($transaction->put_journal_entry($entry))->false();
			verify($transaction->get_last_error())->contains('amount');
			$entry['amount'] = 10.5;
			$transaction->reset_last_error();

			verify($transaction->put_journal_entry($entry))->true();
		});

		$entry['employee_id'] = 1;
		$entry['d_c'] = 'debit';
		$entry['amount'] = 10.5;
		$this->specify('Get and put retrieve and modify',function() use ($transaction,$entry) {
			verify($transaction->get_journal_entry())->false();
			verify($transaction->get_last_error())->equals('TipTransaction->get_journal_entry Warning: journal entry not found.');
			$transaction->reset_last_error();

			$result = $transaction->get_journal_entry(null, 1);
			verify(is_array($result))->true();
			verify($result['note'])->isEmpty();

			$entry['note'] = 'note';

			verify($transaction->put_journal_entry($entry))->true();

			$result = $transaction->get_journal_entry(null, 1);
			verify(is_array($result))->true();
			verify($result['note'])->equals('note');
		});

		$this->specify('Delete journal entry',function() use ($transaction,$entry) {
			verify($transaction->put_journal_entry($entry))->true();

			$result = $transaction->get_journal_entry(null, 1);
			verify(is_array($result))->true();
			verify($result['employee_id'])->equals(1);

			verify($transaction->delete_journal_entry(array()))->false();
			verify($transaction->get_last_error())->equals('TipTransaction->delete_journal_entry Error: TipsJournal->patch Error: either account_id or employee id must be set.');
			$transaction->reset_last_error();

			verify($transaction->delete_journal_entry(array('account_id'=>1)))->false();
			verify($transaction->get_last_error())->equals('TipTransaction->delete_journal_entry Warning: not found.');
			$transaction->reset_last_error();


			$result = $transaction->get_journal_entry(null, 1);
			verify(is_array($result))->true();
			verify($result['note'])->isEmpty();

			verify($transaction->delete_journal_entry($entry))->true();

			$result = $transaction->get_journal_entry(null, 1);
			verify($result)->isEmpty();
		});


	}

	public function testIsBalancedAndCommit(){
		$course_id = 6270;
		$transaction = new TipTransaction($course_id);

		$this->specify('is_balanced and commit setup errors',function() use ($transaction){
			verify($transaction->is_balanced())->true();
			verify($transaction->get_last_error())->equals('TipTransaction->is_balanced Warning: no journal_entries.');
			$transaction->reset_last_error();

			verify($transaction->commit())->false();
			verify($transaction->get_last_error())->equals('TipTransaction->commit Error: no transaction loaded.');
			$transaction->reset_last_error();
		});

		verify($transaction->save(1,1,null,null,null,true))->equals(1);
		verify($transaction->get_last_error())->isEmpty();

		$debit = $transaction->blank_journal_entry();
		$credit1 = $transaction->blank_journal_entry();
		$credit2 = $transaction->blank_journal_entry();

		$debit['amount'] = 10.00;
		$credit1['amount'] = 6.75;
		$credit2['amount'] = 3.25;

		$debit['account_id'] = 1;
		$credit1['employee_id'] = 1;
		$credit2['employee_id'] = 2;

		$debit['d_c'] = 'debit';
		$credit1['d_c'] = 'credit';
		$credit2['d_c'] = 'credit';
		$this->specify('is_balanced works',function() use ($transaction,$debit,$credit2,$credit1) {
			verify($transaction->put_journal_entry($debit))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->is_balanced())->false();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->put_journal_entry($credit1))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->is_balanced())->false();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->put_journal_entry($credit2))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->is_balanced())->true();
			verify($transaction->get_last_error())->isEmpty();

			$credit1['amount']-=0.1;

			verify($transaction->put_journal_entry($credit1))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->is_balanced())->false();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->delete_journal_entry($credit1))->true();
		});

		$debit['amount'] = 10.00;
		$credit1['amount'] = 6.75;
		$credit2['amount'] = 3.25;
		$this->specify('commit verifies correctly',function() use ($transaction,$debit,$credit2,$credit1) {
			verify($transaction->put_journal_entry($debit))->true();
			verify($transaction->get_last_error())->isEmpty();
			verify($transaction->put_journal_entry($credit2))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->commit(true))->false();
			verify($transaction->get_last_error())->equals('TipTransaction->commit Error: Transaction not balanced.');
			$transaction->reset_last_error();

			verify($transaction->put_journal_entry($credit1))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->commit(true))->true();
			verify($transaction->get_last_error())->isEmpty();
			$transaction->reset_last_error();

		});
	}

	public function testBalancingStrategies(){
		$course_id = 6270;
		$transaction = new TipTransaction($course_id);

		$transaction->reset_last_error();
		verify($transaction->save(1,1,null,null,null,true))->equals(1);
		verify($transaction->get_last_error())->isEmpty();

		$debit = $transaction->blank_journal_entry();
		$credit1 = $transaction->blank_journal_entry();
		$credit2 = $transaction->blank_journal_entry();

		$debit['amount'] = 10.00;
		$credit1['amount'] = 6.75;
		$credit2['amount'] = 3.25;

		$debit['account_id'] = 1;
		$credit1['employee_id'] = 1;
		$credit2['employee_id'] = 2;

		$debit['d_c'] = 'debit';
		$credit1['d_c'] = 'credit';
		$credit2['d_c'] = 'credit';

		$this->specify('Scorched Earth works properly',function() use ($transaction,$debit,$credit1,$credit2){
			verify($transaction->put_journal_entry($debit))->true();
			verify($transaction->get_last_error())->isEmpty();
			verify($transaction->put_journal_entry($credit1))->true();
			verify($transaction->get_last_error())->isEmpty();
			verify($transaction->put_journal_entry($credit2))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->is_balanced())->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->get_journal_entry(null,1))->notEmpty();
			verify($transaction->get_journal_entry(null,2))->notEmpty();

			$transaction->a_b_scorched_earth();

			verify($transaction->is_balanced())->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->get_journal_entry(null,1))->notEmpty();
			verify($transaction->get_journal_entry(null,2))->isEmpty();
			$entry1 = $transaction->get_journal_entry(null,1);
			verify($entry1['amount'])->equals((10.00));
		});

		$this->specify('Main Employee works properly',function() use ($transaction,$debit,$credit1,$credit2){
			$debit['amount']=2;
			verify($transaction->put_journal_entry($debit))->true();
			verify($transaction->get_last_error())->isEmpty();
			verify($transaction->put_journal_entry($credit1))->true();
			verify($transaction->get_last_error())->isEmpty();
			verify($transaction->put_journal_entry($credit2))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->is_balanced())->false();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->get_journal_entry(null,1))->notEmpty();
			verify($transaction->get_journal_entry(null,2))->notEmpty();

			$transaction->a_b_main_employee();

			verify($transaction->is_balanced())->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->get_journal_entry(null,1))->notEmpty();
			verify($transaction->get_journal_entry(null,2))->isEmpty();
			$entry1 = $transaction->get_journal_entry(null,1);
			verify($entry1['amount'])->equals((2.00));


			$debit['amount']=12;
			verify($transaction->put_journal_entry($debit))->true();
			verify($transaction->get_last_error())->isEmpty();
			verify($transaction->put_journal_entry($credit1))->true();
			verify($transaction->get_last_error())->isEmpty();
			verify($transaction->put_journal_entry($credit2))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->is_balanced())->false();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->get_journal_entry(null,1))->notEmpty();
			verify($transaction->get_journal_entry(null,2))->notEmpty();

			$transaction->a_b_main_employee();

			verify($transaction->is_balanced())->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->get_journal_entry(null,1))->notEmpty();
			verify($transaction->get_journal_entry(null,2))->notEmpty();
			$entry1 = $transaction->get_journal_entry(null,1);
			verify($entry1['amount'])->equals(8.75);
			$entry2 = $transaction->get_journal_entry(null,2);
			verify($entry2['amount'])->equals(3.25);
		});

		$this->specify('Keep Ratio works properly',function() use ($transaction,$debit,$credit1,$credit2) {
			$debit['amount']=12;
			verify($transaction->put_journal_entry($debit))->true();
			verify($transaction->get_last_error())->isEmpty();
			verify($transaction->put_journal_entry($credit1))->true();
			verify($transaction->get_last_error())->isEmpty();
			verify($transaction->put_journal_entry($credit2))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->is_balanced())->false();
			verify($transaction->get_last_error())->isEmpty();

			$transaction->a_b_keep_ratio();

			verify($transaction->is_balanced())->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->get_journal_entry(null,1))->notEmpty();
			verify($transaction->get_journal_entry(null,2))->notEmpty();
			$entry1 = $transaction->get_journal_entry(null,1);
			verify($entry1['amount'])->equals(8.10);
			$entry2 = $transaction->get_journal_entry(null,2);
			verify($entry2['amount'])->equals(3.90);

			$debit['amount']=7;
			verify($transaction->put_journal_entry($debit))->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->is_balanced())->false();
			verify($transaction->get_last_error())->isEmpty();

			$transaction->a_b_keep_ratio();

			verify($transaction->is_balanced())->true();
			verify($transaction->get_last_error())->isEmpty();

			verify($transaction->get_journal_entry(null,1))->notEmpty();
			verify($transaction->get_journal_entry(null,2))->notEmpty();
			$entry1 = $transaction->get_journal_entry(null,1);
			verify($entry1['amount'])->equals(4.73);
			$entry2 = $transaction->get_journal_entry(null,2);
			verify($entry2['amount'])->equals(2.27);
		});
	}
}
