<?php
namespace libraries\fu\service_fees;


use fu\TestingHelpers\Actor;
use fu\TestingHelpers\TestingObjects\Invoice;

use fu\TestingHelpers\TestingObjects\SFItem;
use fu\TestingHelpers\TestingObjects\Item;
use fu\TestingHelpers\TestingObjects\Payment;
use Respect\Validation\Rules\In;

class OldInvoicesTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;


	protected function _before()
	{
		$this->actor = new Actor();
	}

	public function testSaveInvoice()
	{
		$this->actor->login(); // TODO: need to log out, or breaks other tests
		$CI = get_instance();
		$CI->load->model('Invoice');

		$invoiceObj = new Invoice();

		$invoice = $invoiceObj->getArray();
		$invoice['due_date'] = date('Y-m-d', strtotime('+30 days'));
		$invoice['bill_start'] = date('Y-m-d H:i:s');
		$invoice['bill_end'] = $invoice['bill_start'];
		$invoice['course_id'] = $CI->session->userdata('course_id');
		$invoice['employee_id'] = $CI->session->userdata('person_id');
		$invoice['person_id'] = "2187";

		$item = array('description'=>'Nachos','item_id'=>'5231','item_type'=>'item','tax_included'=>'0','quantity'=>'1','price'=>'3.00');

		$invoice['items'] = array($item);

		$invoice_response = $CI->Invoice->save($invoice);
		$invoice_id = $invoice_response['invoice_id'];

		$this->specify("Check that a new invoice_id was generated", function() use($invoice_id) {
			verify(is_int($invoice_id))->true();
			verify($invoice_id)->greaterThan(0);
		});

		$this->specify("Check that we can retrieve items, and there is just 1", function() use($CI, $invoice_id) {
			$items = $CI->Invoice->get_items($invoice_id);
			verify($items)->notEmpty();
			verify($items)->hasKey(0);
			verify($items)->hasntKey(1);
			verify($items[0])->hasKey('name');
			verify($items[0]['name'])->equals('Nachos');
			verify($items[0])->hasKey('item_id');
			verify($items[0]['item_id'])->equals('5231');
		});
	}

	function testCalculateInvoice() {
		$this->actor->login(); // TODO: need to log out, or breaks other tests
		$CI = get_instance();
		$CI->load->model('Invoice');

		$invoiceObj = new Invoice();

		$invoice = $invoiceObj->getArray();
		$invoice['due_date'] = date('Y-m-d', strtotime('+30 days'));
		$invoice['bill_start'] = date('Y-m-d H:i:s');
		$invoice['bill_end'] = $invoice['bill_start'];
		$invoice['course_id'] = $CI->session->userdata('course_id');
		$invoice['employee_id'] = $CI->session->userdata('person_id');
		$invoice['person_id'] = "2187";
		$total = 0;
		$subtotal = 0;
		$tax = 0;

		$item = array('description'=>'Nachos','item_id'=>'5231','item_type'=>'item','tax_included'=>'0','quantity'=>'1','price'=>'3.00');

		$invoice['items'] = array($item);

		$invoice_responseA = $CI->Invoice->save($invoice);
		$invoice_id = $invoice_responseA['invoice_id'];

		$invoice_responseB = $CI->Invoice->process_invoice_items($invoice['items'], $invoice_id, $invoice['person_id'], $invoice['employee_id'], $invoice['course_id'],$total,$subtotal,$tax);

		$this->specify("Check that total, subtotal and tax were calculated right.",function() use ($total,$subtotal,$tax) {
			verify($subtotal)->equals(3.00);
			verify($tax)->equals(0.23);
			verify($total)->equals(3.23);
		});
	}

	function testCalculateInvoiceNonTaxable() {
		$this->actor->login(); // TODO: need to log out, or breaks other tests
		$CI = get_instance();
		$CI->load->model('Invoice');

		$invoiceObj = new Invoice();

		$invoice = $invoiceObj->getArray();
		$invoice['due_date'] = date('Y-m-d', strtotime('+30 days'));
		$invoice['bill_start'] = date('Y-m-d H:i:s');
		$invoice['bill_end'] = $invoice['bill_start'];
		$invoice['course_id'] = $CI->session->userdata('course_id');
		$invoice['employee_id'] = $CI->session->userdata('person_id');
		$invoice['person_id'] = "418";
		$total = 0;
		$subtotal = 0;
		$tax = 0;

		$item = array('description'=>'Nachos','item_id'=>'5231','item_type'=>'item','tax_included'=>'0','quantity'=>'1','price'=>'3.00');

		$invoice['items'] = array($item);

		$invoice_responseA = $CI->Invoice->save($invoice);
		$invoice_id = $invoice_responseA['invoice_id'];

		$sale_items = $CI->Invoice->process_invoice_items($invoice['items'], $invoice_id, $invoice['person_id'], $invoice['employee_id'], $invoice['course_id'],$total,$subtotal,$tax);

		$this->specify("Check that total, subtotal and tax were calculated right.",function() use ($total,$subtotal,$tax) {
			verify($subtotal)->equals(3.00);
			verify($tax)->equals(0.00);
			verify($total)->equals(3.00);
		});

		$this->specify("Check that the sale_items array is correct",function() use ($sale_items,$invoice_id) {
			verify($sale_items)->notEmpty();
			verify($sale_items)->hasKey(0);
			verify($sale_items)->hasntKey(1);
			verify($sale_items[0])->hasKey('name');
			verify($sale_items[0]['name'])->equals('Nachos');
			verify($sale_items[0])->hasKey('item_id');
			verify($sale_items[0]['invoice_id'])->equals($invoice_id);
			verify($sale_items[0]['item_id'])->equals('5231');
			verify($sale_items[0]['quantity'])->equals(1);
			verify($sale_items[0]['subtotal'])->equals(3.00);
			verify($sale_items[0]['tax'])->equals(0.00);
			verify($sale_items[0]['total'])->equals(3.00);
		});
	}

	function testCalculateInvoiceNonTaxableTaxIncluded() {
		$this->actor->login(); // TODO: need to log out, or breaks other tests
		$CI = get_instance();
		$CI->load->model('Invoice');

		// first, create a new tax included item by mildly abusing the service fee code...
		$sf_item = new SFItem();
		$sf_item = $sf_item->getArray();

		$ParentItem = new \fu\service_fees\ServiceFee();
		$sf_item['unit_price_includes_tax'] = 1;
		$sf_item['name'] = 'The parent';
		$sf_item['selected'] = true;
		$sf_item['item_type'] = 'item';
		$sf_item['is_service_fee'] = 0;
		$this->assertEquals(1,$ParentItem->save($sf_item));
		$parent_serial = $ParentItem->getAsArray();

		$this->assertTrue(is_numeric($parent_serial['item_id']));

		// make the parent not a service fee
		$CI = get_instance();
		$CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");


		// now the invoice stuff
		$invoiceObj = new Invoice();

		$invoice = $invoiceObj->getArray();
		$invoice['due_date'] = date('Y-m-d', strtotime('+30 days'));
		$invoice['bill_start'] = date('Y-m-d H:i:s');
		$invoice['bill_end'] = $invoice['bill_start'];
		$invoice['course_id'] = $CI->session->userdata('course_id');
		$invoice['employee_id'] = $CI->session->userdata('person_id');
		$invoice['person_id'] = "418";
		$total = 0;
		$subtotal = 0;
		$tax = 0;

		$item = array('description'=>'The parent','item_id'=>$parent_serial['item_id'],'item_type'=>'item','tax_included'=>'1','quantity'=>'1','price'=>'100.00');

		$invoice['items'] = array($item);

		$invoice_responseA = $CI->Invoice->save($invoice);
		$invoice_id = $invoice_responseA['invoice_id'];

		$sale_items = $CI->Invoice->process_invoice_items($invoice['items'], $invoice_id, $invoice['person_id'], $invoice['employee_id'], $invoice['course_id'],$total,$subtotal,$tax);

		$this->specify("Check that total, subtotal and tax were calculated right.",function() use ($total,$subtotal,$tax) {
			verify($subtotal)->equals(90.91);
			verify($tax)->equals(0.00);
			verify($total)->equals(90.91);
		});

		$this->specify("Check that the sale_items array is correct",function() use ($sale_items,$invoice_id,$parent_serial) {
			verify($sale_items)->notEmpty();
			verify($sale_items)->hasKey(0);
			verify($sale_items)->hasntKey(1);
			verify($sale_items[0])->hasKey('name');
			verify($sale_items[0]['name'])->equals('The parent');
			verify($sale_items[0])->hasKey('item_id');
			verify($sale_items[0]['invoice_id'])->equals($invoice_id);
			verify($sale_items[0]['item_id'])->equals($parent_serial['item_id']);
			verify($sale_items[0]['quantity'])->equals(1);
			verify($sale_items[0]['subtotal'])->equals(90.91);
			verify($sale_items[0]['tax'])->equals(0.00);
			verify($sale_items[0]['total'])->equals(90.91);
		});
	}

	function testCalculateInvoiceNonTaxableTaxIncludedSF() {
		$this->actor->login(); // TODO: need to log out, or breaks other tests
		$CI = get_instance();
		$CI->load->model('Invoice');

		// first, create a new tax included item by mildly abusing the service fee code...
		$sf_item = new SFItem();
		$sf_item = $sf_item->getArray();
		$sf_item3 = $sf_item;

		$ParentItem = new \fu\service_fees\ServiceFee();
		$ServiceFee = new \fu\service_fees\ServiceFee();
		$sf_item['unit_price_includes_tax'] = 1;
		$sf_item['unit_price'] = 100.50;
		$sf_item['name'] = 'The parent';
		$sf_item['selected'] = true;
		$sf_item['item_type'] = 'item';
		$sf_item['is_service_fee'] = 0;

		$sf_item3['unit_price'] = 1.50;
		$this->assertEquals(1,$ParentItem->save($sf_item));
		$this->assertEquals(1,$ServiceFee->save($sf_item3));
		$parent_serial = $ParentItem->getAsArray();
		$child_serial = $ServiceFee->getAsArray();

		$this->assertTrue(is_numeric($parent_serial['item_id']));

		// make the parent not a service fee
		$CI = get_instance();
		$CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");

		$this->assertEquals(1,$ServiceFee->applyToItems($parent_serial['item_id']));

		// now the invoice stuff
		$invoiceObj = new Invoice();

		$invoice = $invoiceObj->getArray();
		$invoice['due_date'] = date('Y-m-d', strtotime('+30 days'));
		$invoice['bill_start'] = date('Y-m-d H:i:s');
		$invoice['bill_end'] = $invoice['bill_start'];
		$invoice['course_id'] = $CI->session->userdata('course_id');
		$invoice['employee_id'] = $CI->session->userdata('person_id');
		$invoice['person_id'] = "418";
		$total = 0;
		$subtotal = 0;
		$tax = 0;

		$item = array('description'=>'The parent','item_id'=>$parent_serial['item_id'],'item_type'=>'item','tax_included'=>'1','quantity'=>'1','price'=>'100.50');

		$invoice['items'] = array($item);

		$invoice_responseA = $CI->Invoice->save($invoice);
		$invoice_id = $invoice_responseA['invoice_id'];

		$sale_items = $CI->Invoice->process_invoice_items($invoice['items'], $invoice_id, $invoice['person_id'], $invoice['employee_id'], $invoice['course_id'],$total,$subtotal,$tax);

		$this->specify("Check that total, subtotal and tax were calculated right.",function() use ($total,$subtotal,$tax) {
			verify($subtotal)->equals(137.04);
			verify($tax)->equals(0.00);
			verify($total)->equals(137.04);
		});

		$this->specify("Check that the sale_items array is correct",function() use ($sale_items,$invoice_id,$child_serial,$parent_serial) {
			verify($sale_items)->notEmpty();
			verify($sale_items)->hasKey(0);
			verify($sale_items)->hasKey(1);
			verify($sale_items)->hasntKey(2);
			verify($sale_items[0])->hasKey('name');
			verify($sale_items[0]['name'])->equals('The parent');
			verify($sale_items[0])->hasKey('item_id');
			verify($sale_items[0]['invoice_id'])->equals($invoice_id);
			verify($sale_items[0]['item_id'])->equals($parent_serial['item_id']);
			verify($sale_items[0]['quantity'])->equals(1);
			verify($sale_items[0]['subtotal'])->equals(91.36);
			verify($sale_items[0]['tax'])->equals(0.00);
			verify($sale_items[0]['total'])->equals(91.36);

			verify($sale_items[1])->hasKey('name');
			verify($sale_items[1]['name'])->equals('Test SF 1');
			verify($sale_items[1])->hasKey('item_id');
			verify($sale_items[1]['invoice_id'])->equals($invoice_id);
			verify($sale_items[1]['item_id'])->equals($child_serial['item_id']);
			verify($sale_items[1]['quantity'])->equals(1);
			verify($sale_items[1]['subtotal'])->equals(45.68);
			verify($sale_items[1]['tax'])->equals(0.00);
			verify($sale_items[1]['total'])->equals(45.68);
		});
	}

	function testCalculateInvoiceNonTaxableTaxIncludedFloat() {
		$this->actor->login(); // TODO: need to log out, or breaks other tests
		$CI = get_instance();
		$CI->load->model('Invoice');

		// first, create a new tax included item by mildly abusing the service fee code...
		$sf_item = new SFItem();
		$sf_item = $sf_item->getArray();

		$ParentItem = new \fu\service_fees\ServiceFee();
		$sf_item['unit_price_includes_tax'] = 1;
		$sf_item['unit_price'] = 100.50;
		$sf_item['name'] = 'The parent';
		$sf_item['selected'] = true;
		$sf_item['item_type'] = 'item';
		$sf_item['is_service_fee'] = 0;
		$this->assertEquals(1,$ParentItem->save($sf_item));
		$parent_serial = $ParentItem->getAsArray();

		$this->assertTrue(is_numeric($parent_serial['item_id']));

		// make the parent not a service fee
		$CI = get_instance();
		$CI->db->query("
          DELETE FROM foreup_service_fees
          WHERE item_id = ".$parent_serial['item_id'].";
        ");


		// now the invoice stuff
		$invoiceObj = new Invoice();

		$invoice = $invoiceObj->getArray();
		$invoice['due_date'] = date('Y-m-d', strtotime('+30 days'));
		$invoice['bill_start'] = date('Y-m-d H:i:s');
		$invoice['bill_end'] = $invoice['bill_start'];
		$invoice['course_id'] = $CI->session->userdata('course_id');
		$invoice['employee_id'] = $CI->session->userdata('person_id');
		$invoice['person_id'] = "418";
		$total = 0;
		$subtotal = 0;
		$tax = 0;

		$item = array('description'=>'The parent','item_id'=>$parent_serial['item_id'],'item_type'=>'item','tax_included'=>'1','quantity'=>'1','price'=>'100.50');

		$invoice['items'] = array($item);

		$invoice_responseA = $CI->Invoice->save($invoice);
		$invoice_id = $invoice_responseA['invoice_id'];

		$sale_items = $CI->Invoice->process_invoice_items($invoice['items'], $invoice_id, $invoice['person_id'], $invoice['employee_id'], $invoice['course_id'],$total,$subtotal,$tax);

		$this->specify("Check that total, subtotal and tax were calculated right.",function() use ($total,$subtotal,$tax) {
			verify($subtotal)->equals(91.36);
			verify($tax)->equals(0.00);
			verify($total)->equals(91.36);
		});

		$this->specify("Check that the sale_items array is correct",function() use ($sale_items,$invoice_id,$parent_serial) {
			verify($sale_items)->notEmpty();
			verify($sale_items)->hasKey(0);
			verify($sale_items)->hasntKey(1);
			verify($sale_items[0])->hasKey('name');
			verify($sale_items[0]['name'])->equals('The parent');
			verify($sale_items[0])->hasKey('item_id');
			verify($sale_items[0]['invoice_id'])->equals($invoice_id);
			verify($sale_items[0]['item_id'])->equals($parent_serial['item_id']);
			verify($sale_items[0]['quantity'])->equals(1);
			verify($sale_items[0]['subtotal'])->equals(91.36);
			verify($sale_items[0]['tax'])->equals(0.00);
			verify($sale_items[0]['total'])->equals(91.36);
		});
	}
}

?>