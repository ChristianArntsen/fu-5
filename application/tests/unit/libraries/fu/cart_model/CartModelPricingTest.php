<?php
namespace libraries\fu\service_fees;


use fu\TestingHelpers\Actor;
use fu\TestingHelpers\TestingObjects\Item;
use fu\TestingHelpers\TestingObjects\Payment;

class CartModelPricingTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	protected function _before()
	{
		if (!isset($this->actor)) {
			$CI = get_instance();
			$this->actor = new Actor();
			$this->actor->login();
			$CI->load->model('v2/Cart_model');
			$this->cart_model = new \Cart_model();
		}
	}

	public function testSaveItem()
	{
		$itemObj = new Item();
		$item = $itemObj->getArray();
		$item['selected']=true;
        $cart_model = $this->cart_model;
		$this->specify("Check that save_item exists.",function() use ($cart_model){
			verify(method_exists($cart_model,'save_item'))->true();
		});

		$this->specify("Check that save_item works.",function() use ($cart_model,$item) {
			$line = $cart_model->save_item(1,$item);
			verify($line)->equals(1);
		});
	}

	public function testGetItems()
	{
		$cart_model = $this->cart_model;
		$this->specify("Check that get_items exists.",function() use ($cart_model) {
			verify(method_exists($cart_model,'get_items'));
		});

		$this->specify("Check that get_items works.",function() use ($cart_model) {
			$items = $cart_model->get_items();
			verify(is_array($items))->true();
			verify($items)->hasKey(0);
			verify($items)->hasntKey(1);
			verify($items[0]['item_id'])->equals(5098);
		});
	}

	public function testGetItemTotals()
	{
		$cart_model = $this->cart_model;
		$this->specify("Check that get_item_totals exists.",function() use ($cart_model){
			verify(method_exists($cart_model,'get_item_totals'))->true();
		});

		$this->specify("Check that get_item_totals works.",function() use ($cart_model){
			$totals = $cart_model->get_item_totals(true,1);
			verify($totals['subtotal'])->equals(30);
			verify($totals['tax'])->equals(2.10);
			verify($totals['total'])->equals(32.10);
		});
	}

	public function testGetTotals()
	{
		$cart_model = $this->cart_model;
		$this->specify("Check that get_totals exists.",function() use ($cart_model){
			verify(method_exists($cart_model,'get_totals'))->true();
		});

		$this->specify("Check that get_totals works.",function() use ($cart_model){
			$totals = $cart_model->get_totals(true,1);
			verify($totals['subtotal'])->equals(30);
			verify($totals['tax'])->equals(2.10);
			verify($totals['total'])->equals(32.10);
			verify($totals['total_due'])->equals(32.10);
			verify($totals['total_paid'])->equals(0);
		});
	}

	public function testIsPaid()
	{
		$cart_model = $this->cart_model;
		$this->specify("Check that is_paid exists.",function() use ($cart_model){
			verify(method_exists($cart_model,'is_paid'))->true();
		});

		$this->specify("Check that is_paid works.",function() use ($cart_model){
			verify($cart_model->is_paid())->false();
		});
	}

	public function testCalculateItemTotals()
	{
		$cart_model = $this->cart_model;
		$items = $cart_model->get_items();
		$item = $items[0];

		$this->specify("Check that calculate_item_totals exists.",function() use ($cart_model){
			verify(method_exists($cart_model,'calculate_item_totals'))->true();
		});

		$this->specify("Check that calculate_item_totals taxable works.",function() use ($cart_model,$item){
			verify($cart_model->calculate_item_totals($item,1,0))->true();
			verify($item['subtotal'])->equals(30);
			verify($item['tax'])->equals(2.10);
			verify($item['taxes'][0]['amount'])->equals(2.10);
		});

		$this->specify("Check that calculate_item_totals non-taxable works.",function() use ($cart_model,$item){
			verify($cart_model->calculate_item_totals($item,0,0))->true();
			verify($item['subtotal'])->equals(30);
			verify($item['tax'])->equals(0);
			verify($item['taxes'][0]['amount'])->equals(2.1);
		});

		$this->specify("Check that calculate_item_totals tax included works.",function() use ($cart_model,$item){
			$item['unit_price_includes_tax'] = 1;
			verify($cart_model->calculate_item_totals($item,1,1))->true();
			verify($item['subtotal'])->equals(28.04);
			verify($item['tax'])->equals(1.96);
			verify($item['taxes'][0]['amount'])->equals(1.96);
		});

		$this->specify("Check that calculate_item_totals tax included not taxable works.",function() use ($cart_model,$item){
			$item['unit_price_includes_tax'] = 1;
			verify($cart_model->calculate_item_totals($item,0,1))->true();
			verify($item['subtotal'])->equals(28.04);
			verify($item['tax'])->equals(0);
			verify($item['taxes'][0]['amount'])->equals(1.96);
		});
	}

	public function testSavePayment()
	{
		$cart_model = $this->cart_model;
		$paymentObj = new Payment();
		$payment = $paymentObj->getArray();
		$payment['amount']=32.10;

		$this->specify("Check that save_payment exists.",function() use ($cart_model) {
			verify(method_exists($cart_model,'save_payment'))->true();
		});

		$this->specify("Check that save_payment works.",function() use ($cart_model,$payment) {
			$pmt = $cart_model->save_payment($payment);
			verify(is_array($pmt))->true();
			verify($pmt['type'])->equals('cash');
			verify(is_int($pmt['payment_id']));
			verify($pmt['amount'])->equals(32.10);
			verify($cart_model->is_paid())->true();
			$totals = $cart_model->get_totals(true,1);
			verify($totals['total_paid'])->equals(32.10);
			verify($totals['total_due'])->equals(0.00);
			verify($totals['total'])->equals(32.10);
		});

	}

	public function testSaveSale()
	{
		$cart_model = $this->cart_model;
		$CI = get_instance();
		$items = $cart_model->get_items();
		$item = $items[0];

		$this->specify("Check that save_sale exists.",function() use ($cart_model) {
			verify(method_exists($cart_model,'save_sale'))->true();
		});

		$response = $cart_model->save_sale('');

		$this->specify("Check that save_sale works.",function() use ($cart_model,$response) {
			verify(is_array($response))->true();
			verify($response['cart_id'])->equals($cart_model->cart_id);
			verify(is_int($response['sale_id']))->true();
		});

		$this->specify("Check that all sales tax was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items_taxes")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['percent'])->equals(7.000);
			verify($results['amount'])->equals(2.10);
		});

		$this->specify("Check that sale was saved correctly.",function() use ($response,$CI,$item){
			verify($response['sale_id'])->notNull();
			//Get taxes for the sale_id
			$results =
				$CI->db->from("sales_items")
					->where("sale_id",$response['sale_id'])
					->where("item_id",$item['item_id'])->get()->row_array();

			verify($results)->notEmpty();
			verify($results['tax'])->equals(2.10);
			verify($results['subtotal'])->equals(30.00);
			verify($results['total'])->equals(32.10);

		});
	}
}

?>
