<?php
namespace fu\PaymentGateway;

class MercuryPaymentGatewayTest extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	/**
	 * @var MercuryPaymentGateway
	 */
	private $gateway;

	protected function _before()
	{
		$this->gateway = new MercuryPaymentGateway(['id'=>6270,'mercury_id'=>'494691720','mercury_password'=>'KRD%8rw#+p9C13,T']);

	}

	public function testChargeInvalidArgument()
	{
		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Amount must be numeric');
		$this->gateway->charge('not numeric');

	}

	public function testReturnInvalidArgument()
	{
		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Amount must be numeric');
		$this->gateway->return('not numeric');

	}

	public function testSetStoredPaymentInvalidArgument()
	{
		$this->setExpectedException('TypeError',
			'Argument 1 passed to fu\PaymentGateway\MercuryPaymentGateway::setStoredPayment() '.
			'must be an instance of fu\PaymentGateway\StoredPaymentInfo, string given');
		$this->gateway->setStoredPayment('not numeric');

	}

	public function testCharge()
	{
		$config = ["credit_card_id"=>144,
			"token"=>'15aOvAG0xOUgTeGHQdHS+XmueLRssu2h5xXb5kAaMd0jEgUQFCIQBScD',
			"cardholder_name"=>'MPS TEST',
			"mercury_id" => 778825001,
			"mercury_password" => '$6a!a#aa.DHWgD9L',
			"invoice"=>123];

		$storedPayment = new StoredPaymentInfo('credit_card',$config['token'],
			$config['invoice'],$config['cardholder_name'],$config['credit_card_id'],'MasterCard');
		$credentials = new PaymentGatewayCredentials(0,'mercury',$config);

		$this->gateway->setStoredPayment($storedPayment);
		$this->gateway->setPaymentGatewayCredentials($credentials);

		$this->gateway->charge(9.45);
	}

	public function testAuthFail()
	{
		$config = ["credit_card_id"=>144,
			"token"=>'15aOvAG0xOUgTeGHQdHS+XmueLRssu2h5xXb5kAaMd0jEgUQFCIQBScD',
			"cardholder_name"=>'MPS TEST',
			"mercury_id" => 778825001,
			"mercury_password" => '$6a!a#aa.DHWgD9L wrong_password...',
			"invoice"=>123];

		$storedPayment = new StoredPaymentInfo('credit_card',$config['token'],
			$config['invoice'],$config['cardholder_name'],$config['credit_card_id'],'MasterCard');
		$credentials = new PaymentGatewayCredentials(0,'mercury',$config);

		$this->gateway->setStoredPayment($storedPayment);
		$this->gateway->setPaymentGatewayCredentials($credentials);

		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Authentication failed for MerchantID/Password');
		$this->gateway->charge(9.45);
	}

	public function testWrongToken()
	{
		$config = ["credit_card_id"=>144,
			"token"=>'15aOvAG0xOUgTeGHQdHS+XmueLRssu2h5xXb5kAaMd0jEgUQFCIQBSqq',
			"cardholder_name"=>'MPS TEST',
			"mercury_id" => 778825001,
			"mercury_password" => '$6a!a#aa.DHWgD9L',
			"invoice"=>123];

		$storedPayment = new StoredPaymentInfo('credit_card',$config['token'],
			$config['invoice'],$config['cardholder_name'],$config['credit_card_id'],'MasterCard');
		$credentials = new PaymentGatewayCredentials(0,'mercury',$config);

		$this->gateway->setStoredPayment($storedPayment);
		$this->gateway->setPaymentGatewayCredentials($credentials);

		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'A transaction processing error occurred: Token Service Unavailable. - 200 Response With Status:Failure Message:Token invalid');
		$this->gateway->charge(9.45);
	}

	public function testFailingCharge()
	{
		// this card fails for values > 10.99
		$config = ["credit_card_id"=>146,
			"token"=>'OnOEmY9/j8rSLx5r4jF2zEre2LHOuMOh9Cf9cUmOcIwjEgUQFCIQBSqk',
			"cardholder_name"=>'MPS TEST',
			"mercury_id" => 778825001,
			"mercury_password" => '$6a!a#aa.DHWgD9L',
			"invoice"=>123];

		$storedPayment = new StoredPaymentInfo('credit_card',$config['token'],
			$config['invoice'],$config['cardholder_name'],$config['credit_card_id'],'MasterCard');
		$credentials = new PaymentGatewayCredentials(0,'mercury',$config);

		$this->gateway->setStoredPayment($storedPayment);
		$this->gateway->setPaymentGatewayCredentials($credentials);
		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\PaymentDeclinedException',
			'The transaction was declined: DECLINE');
		$this->gateway->charge(30.45);
	}
}

?>