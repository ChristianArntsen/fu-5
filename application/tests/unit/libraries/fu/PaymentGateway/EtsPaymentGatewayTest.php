<?php
namespace fu\PaymentGateway;

class EtsPaymentGatewayTest extends \Codeception\TestCase\Test
{

	/**
	 * @var EtsPaymentGateway
	 */
	private $gateway;

	protected function _before()
	{
		$this->gateway = new EtsPaymentGateway(['id'=>6270,'ets_key'=>'1FE6A673-D75B-4E29-B636-2BFF6F1F5E68']);

	}

	public function testChargeInvalidArgument()
	{
		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Amount must be numeric');
		$this->gateway->charge('not numeric');

	}

	public function testReturnInvalidArgument()
	{
		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Amount must be numeric');
		$this->gateway->return('not numeric');

	}

	public function testSetStoredPaymentInvalidArgument()
	{
		$this->setExpectedException('TypeError',
			'Argument 1 passed to fu\PaymentGateway\EtsPaymentGateway::setStoredPayment() '.
			'must be an instance of fu\PaymentGateway\StoredPaymentInfo, string given');
		$this->gateway->setStoredPayment('not numeric');

	}

	public function testCharge()
	{
		$ets_key = '1FE6A673-D75B-4E29-B636-2BFF6F1F5E68';
		$invoice = $this->gateway->CI->Sale->add_credit_card_payment(
			array('ets_id'=>$ets_key,'tran_type'=>'Sale','frequency'=>'Recurring')
		);

		$config = [
			// TODO: automate creation of saved credit cards for id and replacement of exp. token
			"credit_card_id"=>147,
			"token"=>'b6ba65bb-bc5a-48a5-a5aa-a1b80aeebc4e',
			"cardholder_name"=>'ETS Test',
			'ets_key'=>$ets_key,
			"invoice"=>$invoice,"masked_account"=>'0000'];
		$storedPayment = new StoredPaymentInfo('credit_card',$config['token'],
			$config['invoice'],$config['cardholder_name'],$config['credit_card_id'],
			'MasterCard',$config['masked_account']);
		$credentials = new PaymentGatewayCredentials(0,'mercury',$config);

		$this->gateway->setStoredPayment($storedPayment);
		$this->gateway->setPaymentGatewayCredentials($credentials);
		// this may rarely fail because of an unlucky same amount triggering de-duplication
		$this->gateway->charge(round(9.00+rand(0,100)/100,2));
	}

	public function testAuthFail()
	{
		$config = ["credit_card_id"=>147,
			"token"=>'b6ba65bb-bc5a-48a5-a5aa-a1b80aeebc4e',
			"cardholder_name"=>'ETS Test',
			'ets_key'=>'1FE6A673-D75B-4E29-B636-2BFF6F1F5E68 wrong_key...',
			"invoice"=>124,"masked_account"=>'0000'];

		$storedPayment = new StoredPaymentInfo('credit_card',$config['token'],
			$config['invoice'],$config['cardholder_name'],$config['credit_card_id'],
			'MasterCard',$config['masked_account']);
		$credentials = new PaymentGatewayCredentials(0,'mercury',$config);

		$this->gateway->setStoredPayment($storedPayment);
		$this->gateway->setPaymentGatewayCredentials($credentials);

		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Authentication failed for MerchantID/Password');
		$this->gateway->charge(9.45);
	}

	public function testWrongToken()
	{
		$config = ["credit_card_id"=>147,
			"token"=>'b6ba65bb-bc5a-48a5-a5aa-a1b80aeebcaa',
			"cardholder_name"=>'ETS Test',
			'ets_key'=>'1FE6A673-D75B-4E29-B636-2BFF6F1F5E68',
			"invoice"=>124,"masked_account"=>'0000'];

		$storedPayment = new StoredPaymentInfo('credit_card',$config['token'],
			$config['invoice'],$config['cardholder_name'],$config['credit_card_id'],
			'MasterCard',$config['masked_account']);
		$credentials = new PaymentGatewayCredentials(0,'mercury',$config);

		$this->gateway->setStoredPayment($storedPayment);
		$this->gateway->setPaymentGatewayCredentials($credentials);

		$this->setExpectedException('fu\PaymentGateway\PaymentGatewayException\GeneralFailureException',
			'Charge failed: The account b6ba65bb-bc5a-48a5-a5aa-a1b80aeebcaa was not found.');
		$this->gateway->charge(9.45);
	}
}

?>
