<?php
namespace libraries\fu\data_import {

    use fu\TestingHelpers\CodeIgniterMocks\Customer;
    use fu\TestingHelpers\Actor;
    use League\Csv;

    class CustomerImportTest extends \Codeception\TestCase\Test
    {
        use \Codeception\Specify;

        protected function _before(){
            $this->actor = new Actor();
            \Codeception\Specify\Config::setDeepClone(false);
        }

        public function testCustomerImport(){

            $csv_data = '"First Name","Last Name","E-Mail","Cell Phone Number","Phone Number","Birthday","Address Line 1","Address Line 2","City","State/Province","Zip/Postal Code","Country","Comments","Account Number","Customer Credits","Allow Negative Customer Credits","Customer Credits Limit","Member Account Balance","Member Account Limit","Allow Negative Member Account","Non-Taxable","Member","Groups","Loyalty Points","Online Booking Username","Online Booking Password","Date Joined","Use Loyalty"
"John","Smith","john@mailinator.com",1231231234,"(098)765-4321",05/05/1955,"123 Test St.","Apt #456","Idaho Falls","ID",83401,"USA","Spends lots of money","ABCCD1234",-100,"Y",-500,-2500,-3000,"Y","Y","Y","Seniors, Members",,"johnsmith","johnsmith",06/01/06,y
,"Should Fail Because no first name",,,,,,,,,,,,,,,,,,,,,,,,,,
"Some junk","Should still import","garbageemail",1234,"(234)-123",05/06,,,,,,,,,,,,,,,,,,,,,,
"John","Smith","john@mailinator.com",1231231234,"(098)765-4321",05/05/1955,"123 Test St.","Apt #456","Idaho Falls","ID",83401,"USA","Spends lots of money","ABCCD1234",-100,"Y",-500,-2500,-3000,"Y","Y","Y","Seniors, Members",,"johnsmith","johnsmith",06/01/06,y
"Account Number","Fail","john@mailinator.com",1231231234,"(098)765-4321",05/05/1955,"123 Test St.","Apt #456","Idaho Falls","ID",83401,"USA","Spends lots of money","1.23E+015",-100,"Y",-500,-2500,-3000,"Y","Y","Y","Seniors, Members",,"johnsmith","johnsmith",06/01/06,y';

            // Create an in memory CSV file to read
            $csv_file = new \SplFileObject('php://memory', 'w+');
            $csv_file->fwrite($csv_data);
            $csv_file->rewind();

            $job_data = [
                'course_id' => 6270,
                'type' => 'customers',
                'source_path' => $csv_file,
                'source_type' => 'csv',
                'status' => 'pending',
                'settings' => [
                    'first_row_headers' => true,
                    'column_map' => [
                        'first_name' => 0,
                        'last_name' => 1,
                        'email' => 2,
                        'cell_phone_number' => 3,
                        'phone_number' => 4,
                        'birthday' => 5,
                        'address_1' => 6,
                        'address_2' => 7,
                        'city' => 8,
                        'state' => 9,
                        'zip' => 10,
                        'country' => 11,
                        'comments' => 12,
                        'account_number' => 13,
                        'account_balance' => 14,
                        'account_balance_allow_negative' => 15,
                        'account_balance_limit' => 16,
                        'member_account_balance' => 17,
                        'member_account_limit' => 18,
                        'member_account_balance_allow_negative' => 19,
                        'non_taxable' => 20,
                        'member' => 21,
                        'groups' => 22,
                        'loyalty_points' => 23,
                        'username' => 24,
                        'password' => 25,
                        'date_created' => 26,
                        'use_loyalty' => 27
                    ]
                ]
            ];

            $parser = new \fu\data_import\DataParsers\Csv($job_data['source_path'], $job_data['settings']);
            $inserter = new \fu\data_import\RecordInserters\Customer( new Customer() );

            $import_job = new \fu\data_import\DataImportJob($job_data);
            $import_job->set_inserter($inserter);
            $import_job->set_parser($parser);
            $import_job->import();

            $results = $import_job->to_array();

            $this->specify('Total records should be 5', function() use ($results){
                verify($results['total_records'])->equals(5);
            });

            $this->specify('Failed records should be 3', function() use ($results){
                verify($results['records_failed'])->equals(3);
            });

            $this->specify('Imported records should be 2', function() use ($results){
                verify($results['records_imported'])->equals(2);
            });

            $this->specify('Percent complete should be 100', function() use ($results){
                verify($results['percent_complete'])->equals(100);
            });

            $this->specify('Imported record keys should be an array with 2 keys', function() use ($results){
                verify(count($results['response']['imported_record_keys']))->equals(2);
            });

            $this->specify('Error list should contain 3 error messages', function() use ($results){
                verify(count($results['response']['errors']))->equals(3);
            });

            $this->specify('Last row should fail due to account number being scientific notation', function() use ($results){
                verify($results['response']['errors'][5])->contains('scientific notation');
            });

            // Get CSV of failed records
            $failed_records_csv = $import_job->build_failed_csv();

            $this->specify('Failed record should CSV should be created', function() use (&$failed_records_csv){
                verify($failed_records_csv)->notEmpty();
            });

            // Make readable CSV
            $failed_records_csv = Csv\Reader::createFromString($failed_records_csv->__toString());
            $failed_records = $failed_records_csv->fetchAll();

            $this->specify('Failed record CSV should have 4 lines (1 header, 3 errored rows)', function() use (&$failed_records){
                verify(count($failed_records))->equals(4);
            });

            $this->specify('Header row of failed record CSV should contain new "Original Row Number" column', function() use (&$failed_records){
                $header_row = $failed_records[0];
                verify($header_row[0])->equals('Original Row Number');
            });

            $this->specify('Column count of failed record CSV should match original column count (plus one)', function() use (&$failed_records){
                $header_row = $failed_records[0];
                verify(count($header_row))->equals(29);
            });
        }

        public function testCustomerInserter(){

            $invalid_data = [
                'email' => 'invalidemail.whatever',
                'cell_phone_number' => '(234)-123',
                'phone_number' => '123345',
                'birthday' => '',
                'groups' => "Whatever1, Whatever1",
                'date_created' => 'Somedate'
            ];

            $inserter = new \fu\data_import\RecordInserters\Customer( new Customer() );
            $transformed = $inserter->transform_data($invalid_data);

            $this->specify('Invalid email should be removed', function() use ($transformed){
                verify($transformed['email'])->isEmpty();
            });

            $this->specify('Invalid cell phone number should be removed', function() use ($transformed){
                verify($transformed['cell_phone_number'])->isEmpty();
            });

            $this->specify('Invalid phone number should be removed', function() use ($transformed){
                verify($transformed['phone_number'])->isEmpty();
            });

            $this->specify('Invalid birthday should be removed', function() use ($transformed){
                verify($transformed['birthday'])->isEmpty();
            });

            $this->specify('Invalid date created should be removed', function() use ($transformed){
                verify($transformed['date_created'])->isEmpty();
            });

            $this->specify('Groups field should contain 1 group', function() use ($transformed){
                verify(count($transformed['groups']))->equals(1);
            });

            $valid_data = [
                'email' => 'myemail@mailinator.com',
                'cell_phone_number' => '(234)-5678',
                'phone_number' => '1-(208)-123-0987',
                'birthday' => '01/11/1965',
                'groups' => "Whatever1, Whatever2",
                'date_created' => '6/2/2011',
                'member' => 'Y',
                'non_taxable' => 'y',
                'use_loyalty' => ''
            ];
            $transformed = $inserter->transform_data($valid_data);

            $this->specify('Valid email should be passed through', function() use ($transformed){
                verify($transformed['email'])->equals('myemail@mailinator.com');
            });

            $this->specify('Valid cell phone number should be passed through', function() use ($transformed){
                verify($transformed['cell_phone_number'])->equals('(234)-5678');
            });

            $this->specify('Valid phone number should be passed through', function() use ($transformed){
                verify($transformed['phone_number'])->equals('1-(208)-123-0987');
            });

            $this->specify('Valid birthday should be in Y-m-d format', function() use ($transformed){
                verify($transformed['birthday'])->equals('1965-01-11');
            });

            $this->specify('Valid date created should be in Y-m-d format', function() use ($transformed){
                verify($transformed['date_created'])->equals('2011-06-02');
            });

            $this->specify('Member field should be converted to TRUE if field contains Y', function() use ($transformed){
                verify($transformed['member'])->equals(TRUE);
            });

            $this->specify('Non taxable field should be converted to TRUE or FALSE and remapped to "taxable"', function() use ($transformed){
                verify($transformed['taxable'])->equals(FALSE);
            });

            $this->specify('Use loyalty field should be converted to TRUE or FALSE', function() use ($transformed){
                verify($transformed['taxable'])->equals(FALSE);
            });

            $fields_with_numbers = [
                'account_number',
                'phone_number',
                'cell_phone_number',
                'account_balance',
                'account_balance_limit',
                'member_account_balance',
                'member_account_limit',
                'loyalty_points'
            ];

            foreach($fields_with_numbers as $field){

                $msg = $field .' should fail if in scientific notation';
                $error = $inserter->has_errors([$field => '1.23E+015']);

                $this->specify($msg, function() use ($error){
                    verify(is_string($error))->true();
                    verify($error)->contains('scientific notation');
                });
            }
        }
    }
}