<?php
namespace libraries\fu\service_fees;


class SessionStub{
	private $userdata_hash;

	function __construct()
	{
		$userdata = array();
	}

	public function userdata($key){
		if($key === 'course_id'){
			return 6270;
		}
		return $this->userdata_hash[$key];
	}

	public function set_userdata($key,$val){
		$this->userdata_hash[$key]=$val;
	}
}

use fu\TestingHelpers\Actor;
use fu\TestingHelpers\TestingObjects\Item;
use fu\TestingHelpers\TestingObjects\Payment;

class SaleLibPricingTest  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	protected function _before()
	{
		if(!isset($this->actor)) {
			$CI = get_instance();
			$this->actor = new Actor();
			$this->actor->login();
			$CI->load->library('Sale_lib');
			$this->cart_model = new \Cart_model();
			//$CI->session = new SessionStub();
		}
	}

	public function testCalculateModifierTotal()
	{
		$this->specify("Check that calculate_modifier_total.",function(){
			$CI = get_instance();
			verify(method_exists($CI->sale_lib,'calculate_modifier_total'))->true();
		});
	}

	public function testIsUnlimited()
	{
		// is_unlimited($item_id) --> 1|0 or false

		$this->specify("Check that is_unlimited exists.",function(){
			$CI = get_instance();
			verify(method_exists($CI->sale_lib,'is_unlimited'))->true();
		});

		$this->specify("Check that it returns false with non-existing item",function() {
			$CI = get_instance();
			$tryit = $CI->sale_lib->is_unlimited(-208);
			verify($CI->sale_lib->is_unlimited(-208))->false();
		});

		$this->specify("Check that it returns 1 with unlimited item",function(){
			$CI = get_instance();
			verify($CI->sale_lib->is_unlimited(1425))->equals(1);
		});

		$this->specify("Check that it returns 0 with limited item",function(){
			$CI = get_instance();
			verify($CI->sale_lib->is_unlimited(1054))->equals(0);
		});
	}

	public function testCalculateItemSubtotal()
	{
		//calculate_item_subtotal($price, $quantity, $discount)--> round(float,2);
		$CI = get_instance();
		$this->specify("Check that calculate_item_subtotal exists.",function() use ($CI){
			verify(method_exists($CI->sale_lib,'calculate_item_subtotal'))->true();
		});

		$this->specify("Check that calculate_item_subtotal works.",function() use ($CI){
			verify($CI->sale_lib->calculate_item_subtotal(100,2,50))->equals(100.00);
		});
	}

	public function testCalculateItemTax()
	{
		//function calculate_item_tax($subtotal, $item_id, $item_type)-->round(float,2)
		$CI = get_instance();
		$this->specify("Check that calculate_item_tax exists.",function() use ($CI){
			verify(method_exists($CI->sale_lib,'calculate_item_tax'))->true();
		});

		$this->specify("Check that calculate_item_tax works.",function() use ($CI){
			verify($CI->sale_lib->calculate_item_tax(3.00,5228,'item'))->equals(0.23);
		});
	}

	public function testCalculateSubtotal()
	{
		//calculate_item_subtotal($price, $quantity, $discount)--> round(float,2);
		$CI = get_instance();
		$this->specify("Check that calculate_subtotal exists.",function() use ($CI){
			verify(method_exists($CI->sale_lib,'calculate_subtotal'))->true();
		});

		$this->specify("Check that calculate_subtotal works.",function() use ($CI){
			verify($CI->sale_lib->calculate_subtotal(100,2,50))->equals(100.00);
		});
	}

	public function testCalculateTax()
	{
		//calculate_tax($subtotal, &$taxes, $tax_included = false)--> round(float,2);
		$CI = get_instance();
		$this->specify("Check that calculate_tax exists.",function() use ($CI){
			verify(method_exists($CI->sale_lib,'calculate_tax'))->true();
		});

		$this->specify("Check that calculate_tax works.",function() use ($CI){
			$taxes = array(
				array('name' => 'Sales Tax 1', 'percent' => 6.75, 'cumulative' => 0),
				array('name' => 'Sales Tax 2', 'percent' => 3.25, 'cumulative' => 0)
			);
			verify($CI->sale_lib->calculate_tax(100.00,$taxes,false))->equals(10.00);
			verify($taxes[0]['amount'])->equals(6.75);
			verify($taxes[1]['amount'])->equals((3.25));
			verify($CI->sale_lib->calculate_tax(100.00,$taxes,true))->equals(9.09);
			verify($taxes[0]['amount'])->equals(6.14);
			verify($taxes[1]['amount'])->equals((2.95));
		});
	}

	// cart integrations:
	public function testGetBasketTotal(){
		$CI = get_instance();
		$itemObj = new Item();
		$item = $itemObj->getArray();
		$itemz = new \fu\items\ItemPrice();
		$taxz  = new \fu\items\ItemTax();
		$item['subtotal'] = $CI->sale_lib->calculate_item_subtotal($item['unit_price']);
		$itemz->calculatePrice($item);
		$CI->sale_lib->set_basket(array($item));
		$subtotal = $CI->sale_lib->get_basket_subtotal();
		verify($subtotal)->equals(30.00);
		$item['price']=$subtotal;
		$item['discount']=0;


		$item['taxes'] = $CI->sale_lib->get_basket_taxes();
		$CI->sale_lib->set_basket(array($item));
		$result = $taxz->calculate_item_tax($subtotal,$item,'item');
		$item['taxes'][0]['amount'] = $result;
		$item['tax'] = $result;

		$CI->sale_lib->set_basket(array($item));
		$sum_taxes = 0;
		foreach($item['taxes'] as $tax){
			$sum_taxes+=$tax['amount'];
		}

		$taxes = $CI->sale_lib->get_basket_taxes();

		verify($sum_taxes)->equals($result);

		$total = $CI->sale_lib->get_basket_total();
		verify($total)->greaterThan(0);
		verify($total)->equals($subtotal+$sum_taxes);
	}

	// cart integrations:
	public function testGetBasketTotalWithQuantity(){
		$CI = get_instance();
		$itemObj = new Item();
		$item = $itemObj->getArray();
		$itemz = new \fu\items\ItemPrice();
		$taxz  = new \fu\items\ItemTax();
		$item['quantity'] = 10;
		$item['subtotal'] = $CI->sale_lib->calculate_item_subtotal($item['unit_price']*$item['quantity']);
		$itemz->calculatePrice($item);
		$CI->sale_lib->set_basket(array($item));
		$subtotal = $CI->sale_lib->get_basket_subtotal();
		verify($subtotal)->equals(300.00);
		$item['price']=$subtotal/10;
		$item['discount']=0;


		$item['taxes'] = $CI->sale_lib->get_basket_taxes();
		$CI->sale_lib->set_basket(array($item));
		$result = $taxz->calculate_item_tax($subtotal,$item,'item');
		$item['taxes'][0]['amount'] = $result;
		$item['tax'] = $result;

		$CI->sale_lib->set_basket(array($item));
		$sum_taxes = 0;
		foreach($item['taxes'] as $tax){
			$sum_taxes+=$tax['amount'];
		}

		$taxes = $CI->sale_lib->get_basket_taxes();

		verify($sum_taxes)->equals($result);

		$total = $CI->sale_lib->get_basket_total();
		verify($total)->greaterThan(0);
		verify($total)->equals($subtotal+$sum_taxes);
	}

	public function testGetBasketTotalTaxInc(){
		$CI = get_instance();
		$itemObj = new Item();
		$item = $itemObj->getArray();
		$itemz = new \fu\items\ItemPrice();
		$taxz  = new \fu\items\ItemTax();
		$item['subtotal'] = $CI->sale_lib->calculate_item_subtotal($item['unit_price']);
		$itemz->calculatePrice($item);
		$CI->sale_lib->set_basket(array($item));
		$subtotal = $CI->sale_lib->get_basket_subtotal();
		verify($subtotal)->equals(30.00);
		$item['price']=$subtotal;
		$item['discount']=0;
		$item['unit_price_includes_tax']=1;


		$item['taxes'] = $CI->sale_lib->get_basket_taxes();
		$CI->sale_lib->set_basket(array($item));
		$result = $taxz->calculate_item_tax($subtotal,$item,'item');
		$item['taxes'][0]['amount'] = $result;
		$item['tax'] = $result;

		$CI->sale_lib->set_basket(array($item));
		$sum_taxes = 0;
		foreach($item['taxes'] as $tax){
			$sum_taxes+=$tax['amount'];
		}

		$taxes = $CI->sale_lib->get_basket_taxes();

		verify($sum_taxes)->equals($result);

		$total = $CI->sale_lib->get_basket_total();
		verify($total)->greaterThan(0);
		verify($total)->equals($subtotal);
	}
}

?>
