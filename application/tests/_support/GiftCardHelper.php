<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class GiftCardHelper extends \Codeception\Module
{
    private $course_id;
    private $id;
    private $payment_type = "gift_card";
    private $number = 500;
    private $value;
    private $original_value;
    private $payment_description = "Gift Card:";
    private $date_issued = '0000-00-00';
    private $expiration_date = '0000-00-00';
    private $table_name = 'foreup_giftcards';
    private $expectedPaymentMessage = 'Giftcard balance is ';
    private $customerID = NULL;

    function __construct($course_id)
    {
        parent::__construct();
        $this->course_id = $course_id;
        $this->payment_description .= '';
    }

    /**
     * Create a gift card by placing it in the database
     *
     * @param  double $value - (optional) The value of the gift card
     */
    public function _createGiftCard($value = 1000.00){
        //get connection to database
        $Db = $this->getModule('Db');
        $DbHelper = $this->getModule('DbHelper');

        //save info passed in
        $this->value = $value;
        $this->original_value = $value;

        //format the giftcards value
        setlocale(LC_MONETARY, 'en_US');
        $amount = money_format('%(#10n', $value);
        $this->expectedPaymentMessage .= str_replace(" ", "", $amount);

        $this->payment_description .= strval($this->number);

        $giftCard = [
            'CID' => 0,
            'course_id' => $this->course_id,
            'giftcard_number' => $this->number,
            'value' => $this->value,
            'customer_id' => $this->customerID,
            'date_issued' => $this->date_issued,
            'expiration_date' => $this->expiration_date
        ];
        $DbHelper->InsertAndUpdateOnDuplicate($this->table_name, $giftCard);

        $this->id = $Db->grabFromDatabase($this->table_name, 'giftcard_id', $giftCard);
    }

    public function _getGiftCardID(){
        return $this->id;
    }

    public function _getOriginalValue(){
        return $this->original_value;
    }

    public function _getGiftCardValue(){
        return $this->value;
    }

    public function _setGiftCardValue($newValue){
        $DbHelper = $this->getModule('DbHelper');
        $this->value = $newValue;
        $DbHelper->updateDatabase($this->table_name, array('giftcard_id' => $this->id), array('value' => $this->value));
    }

    public function _getGiftCardNumber(){
        return $this->number;
    }

    public function _getPaymentDescription(){
        return $this->payment_description;
    }

    public function _getPaymentType(){
        return $this->payment_type;
    }

    public function _getDateIssued(){
        return $this->date_issued;
    }

    public function _getExpirationDate(){
        return $this->expiration_date;
    }

    public function _getCustomerID(){
        return $this->customerID;
    }

    public function _getExpectedPaymentMSG(){
        return $this->expectedPaymentMessage;
    }
}
