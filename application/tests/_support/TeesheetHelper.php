<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class TeesheetHelper extends \Codeception\Module
{
    public function login($username, $password)
    {
        $browser = $this->getModule('PhpBrowser');
        $browser->amOnPage('/index.php/login');
        $browser->fillField('username', $username);
        $browser->fillField('password', $password);
        $browser->click('login_button');
    }
}
