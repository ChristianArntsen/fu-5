<?php
namespace Codeception\Module;
use Codeception\Lib\Driver\Db as Driver;
use Codeception\Exception\Module as ModuleException;
use Codeception\Exception\ModuleConfig as ModuleConfigException;
use Codeception\Configuration as Configuration;

/**
 * Additional methods for DB module
 *
 * Save this file as DbHelper.php in _helpers folder
 * Enable DbHelper in your suite.yml file
 * Execute codeception build to integrate this class in your codeception
 *
 */
class DbHelper extends \Codeception\Module
{
    protected $primaryColumns = [];
    /**
     * @api
     * @var
     */
    public $dbh;

    /**
     * @var array
     */
    protected $sql = [];

    /**
     * @var array
     */
    protected $config = [
        'populate' => true,
        'cleanup' => true,
        'dump' => null
    ];

    /**
     * @var bool
     */
    protected $populated = false;

    /**
     * @var \Codeception\Lib\Driver\Db
     */
    public $driver;

    /**
     * @var array
     */
    protected $insertedIds = [];

    /**
     * @var array
     */
    protected $requiredFields = ['dsn', 'user', 'password'];

    public function _initialize()
    {
        if ($this->config['dump'] && ($this->config['cleanup'] or ($this->config['populate']))) {

            if (!file_exists(Configuration::projectDir() . $this->config['dump'])) {
                throw new ModuleConfigException(
                    __CLASS__,
                    "\nFile with dump doesn't exist.
                    Please, check path for sql file: " . $this->config['dump']
                );
            }
            $sql = file_get_contents(Configuration::projectDir() . $this->config['dump']);
            $sql = preg_replace('%/\*(?!!\d+)(?:(?!\*/).)*\*/%s', "", $sql);
            if (!empty($sql)) {
                $this->sql = explode("\n", $sql);
            }
        }

        try {
            $this->driver = Driver::create($this->config['dsn'], $this->config['user'], $this->config['password']);
        } catch (\PDOException $e) {
            throw new ModuleException(__CLASS__, $e->getMessage() . ' while creating PDO connection');
        }

        $this->dbh = $this->driver->getDbh();

        // starting with loading dump
        if ($this->config['populate']) {
            $this->cleanup();
            $this->loadDump();
            $this->populated = true;
        }
    }

    public function _after(\Codeception\TestCase $test)
    {
        $this->removeInsertedRows();
    }

    /**
     * Delete entries from $table where $criteria conditions
     * Use: $I->deleteFromDatabase('users', array('id' => '111111', 'banned' => 'yes'));
     *
     * @param  string $table table name
     * @param  array $criteria conditions. See seeInDatabase() method.
     * @return boolean Returns TRUE on success or FALSE on failure.
     */
    public function deleteFromDatabase($table, $criteria)
    {
        $dbh = $this->getModule('Db')->dbh;
        $query = "delete from %s where %s";
        $params = array();
        foreach ($criteria as $k => $v) {
            $params[] = "$k = ?";
        }
        $params = implode(' AND ', $params);
        $query = sprintf($query, $table, $params);
        $this->debugSection('Query', $query, json_encode($criteria));
        $sth = $dbh->prepare($query);

        return $sth->execute(array_values($criteria));
    }

    /**
     * Delete entries from $table where $criteria conditions (equals sign replaced with >)
     * Use: $I->deleteGreaterThanFromDatabase('users', array('id' => '111111', 'banned' => 'yes'));
     *
     * @param  string $table table name
     * @param  array $criteria conditions. See seeInDatabase() method.
     * @return boolean Returns TRUE on success or FALSE on failure.
     */
    public function deleteGreaterThanFromDatabase($table, $criteria)
    {
        $dbh = $this->getModule('Db')->dbh;
        $query = "delete from %s where %s";
        $params = array();
        foreach ($criteria as $k => $v) {
            $params[] = "$k > ?";
        }
        $params = implode(' AND ', $params);
        $query = sprintf($query, $table, $params);
        $this->debugSection('Query', $query, json_encode($criteria));
        $sth = $dbh->prepare($query);

        return $sth->execute(array_values($criteria));
    }

    /**
     * Delete entries from $table where $criteria conditions (equals sign replaced with >=)
     * Use: $I->deleteGreaterThanFromDatabase('users', array('id' => '111111', 'banned' => 'yes'));
     *
     * @param  string $table table name
     * @param  array $criteria conditions. See seeInDatabase() method.
     * @return boolean Returns TRUE on success or FALSE on failure.
     */
    public function deleteGreaterThanOrEqualFromDatabase($table, $criteria)
    {
        $dbh = $this->getModule('Db')->dbh;
        $query = "delete from %s where %s";
        $params = array();
        foreach ($criteria as $k => $v) {
            $params[] = "$k >= $v";
        }
        $params = implode(' AND ', $params);
        $query = sprintf($query, $table, $params);
        $this->debugSection('Query', $query, json_encode($criteria));
        $sth = $dbh->prepare($query);

        return $sth->execute(array_values($criteria));
    }

    /**
     * Update entries in $table where $criteria conditions
     * Use: $I->updateDatabase('foreup_punch_card_items', array('punch_card_id' => '1225413'), array('used' => 0));
     *
     * @param  string $table table name
     * @param  array $criteria values to be updated
     * @param  array $key primary key of desired row.
     * @return boolean Returns TRUE on success or FALSE on failure.
     */
    public function updateDatabase($table, $key, $criteria)
    {
        $dbh = $this->getModule('Db')->dbh;
        $query = "UPDATE %s SET %s WHERE %s;";
        $condition = array();
        $update = array();
        foreach ($criteria as $k => $v) {
            $update[] = "$k = '$v'";
        }
        foreach ($key as $k => $v) {
            $condition[] = "$k = '$v'";
        }
        $condition = implode(' AND ', $condition);
        $update = implode(' , ', $update);
        $query = sprintf($query, $table, $update, $condition);
        $this->debugSection('Query', $query, json_encode($criteria));
        $sth = $dbh->prepare($query);

        return $sth->execute(array_values($criteria));

    }

    /**
     * Add $data to $table
     * Use: $I->placeInDatabase('foreup_punch_card_items', array('punch_card_id' => '1225413', 'item_id => 123, ....));
     *
     * @param  string $table table name
     * @param  array $data attribute values to be added to database
     * @return Returns an array of the last inserted ID's as key value pair.  Returns 0 for the value if there was an error.
     */
    public function placeInDatabase($table, array $data)
    {
        $query = $this->driver->insert($table, $data);
        $this->debugSection('Query', $query);

        $sth = $this->driver->getDbh()->prepare($query);
        if (!$sth) {
            $this->fail("Query '$query' can't be executed.");
        }
        $i = 1;
        foreach ($data as $val) {
            $sth->bindValue($i, $val);
            $i++;
        }
        $res = $sth->execute();
        if (!$res) {
            $this->fail(sprintf("Record with %s couldn't be inserted into %s", json_encode($data), $table));
        }

        //check to see if lastInsertedID was passed to this function in $data
        $primaryColumns = $this->getPrimaryColumn($table);
        $testForValidKey = true;
        foreach ($primaryColumns as $val) {
            if (!isset($data[$val])) {
                $testForValidKey = false;
            }
        }

        $where = array();
        //If lastInsertedID was passed to this function in $data use it
        if ($testForValidKey) {

            foreach ($primaryColumns as $val) {
                $where[$val] = $data[$val];
            }
        } else { //if lastInsertedID was not passed to this function in $data get it from the database
            foreach ($primaryColumns as $val) {
                $test = $val;
            }
            try {
                $where[$test] = (int)$this->driver->lastInsertId($table);
            } catch (\PDOException $e) {
                // ignore errors due to uncommon DB structure,
                // such as tables without _id_seq in PGSQL
                $where[$test] = 0;
            }

        }

        $this->insertedIds[] = [
            'table' => $table,
            'where' => $where
        ];
        $this->primaryColumns = [];
        return $where;
    }

    protected function deleteQuery($table, $primaryKey = [])
    {
        $dbh = $this->getModule('Db')->dbh;
        //$query = 'DELETE FROM ' . $table . ' WHERE ' . $primaryKey . ' = ' . $id;
        $query = 'DELETE FROM %s WHERE %s';
        $condition = array();
        foreach ($primaryKey as $k => $v) {
            $condition[] = "$k = '$v'";
        }
        $condition = implode(' AND ', $condition);
        $query = sprintf($query, $table, $condition);
        $dbh->exec($query);
    }


    protected function removeInsertedRows()
    {
        foreach ($this->insertedIds as $insertId) {

            try {
                $this->deleteQuery($insertId['table'], $insertId['where']);
            } catch (\Exception $e) {
                $tableParams = implode(' AND ', $insertId['where']);
                $this->debug("coudn\'t delete record {$tableParams} from {$insertId['table']}");
            }
        }
        $this->insertedIds = [];
    }

    protected function getPrimaryColumn($tableName)
    {

        $dbh = $this->getModule('Db')->dbh;
        if (false === isset($this->primaryColumns[$tableName])) {
            $stmt = $dbh->query('SHOW KEYS FROM ' . $this->driver->getQuotedName($tableName) . ' WHERE Key_name = "PRIMARY"');
            $columnInformation = $stmt->fetchALL(\PDO::FETCH_ASSOC);

            if (true === empty($columnInformation)) { // Need a primary key
                throw new \Exception('Table ' . $tableName . ' is not valid or doesn\'t have a primary key');
            }

            $i = 0;
            foreach ($columnInformation as $key => $val) {
                $this->primaryColumns[$tableName . '' . $i] = $columnInformation[$i]['Column_name'];
                $i++;
            }
        }

        return $this->primaryColumns;
    }


    /**
     * Add $data to $table update if row already exists
     * Use: $I->InsertAndUpdateOnDuplicate('foreup_punch_card_items', array('punch_card_id' => '1225413', 'item_id => 123, ....));
     *
     * @param  string $table table name
     * @param  array $data attribute values to be added to database
     * @return Returns an array of the last inserted ID's as key value pair.  Returns 0 for the value if there was an error.
     */


    public function InsertAndUpdateOnDuplicate($table, array $data)
    {
        $query = $this->insert($table, $data);
        $this->debugSection('Query', $query);

        $sth = $this->driver->getDbh()->prepare($query);
        if (!$sth) {
            $this->fail("Query '$query' can't be executed.");
        }

        //insert values into query
        $i = 1;
        foreach ($data as $val) {
            $sth->bindValue($i, $val);
            $sth->bindValue($i + count($data), $val);
            $i++;
        }

        $res = $sth->execute();
        if (!$res) {
            $this->fail(sprintf("Record with %s couldn't be inserted into %s", json_encode($data), $table));
        }

        //check to see if lastInsertedID was passed to this function in $data
        $primaryColumns = $this->getPrimaryColumn($table);
        $testForValidKey = true;
        foreach ($primaryColumns as $val) {
            if (!isset($data[$val])) {
                $testForValidKey = false;
            }
        }

        $where = array();
        //If lastInsertedID was passed to this function in $data use it
        if ($testForValidKey) {

            foreach ($primaryColumns as $val) {
                $where[$val] = $data[$val];
            }
        } else { //if lastInsertedID was not passed to this function in $data get it from the database
            foreach ($primaryColumns as $val) {
                $test = $val;
            }
            try {
                $where[$test] = (int)$this->driver->lastInsertId($table);
            } catch (\PDOException $e) {
                // ignore errors due to uncommon DB structure,
                // such as tables without _id_seq in PGSQL
                $where[$test] = 0;
            }

        }

        $this->insertedIds[] = [
            'table' => $table,
            'where' => $where
        ];
        $this->primaryColumns = [];
        return $where;
    }


    public function insert($tableName, array &$data)
    {
        $columns = array_map(
            [$this, 'getQuotedName'],
            array_keys($data)
        );

        foreach ($data as $k => $v) {
            $params[] = "$k = ?";
        }

        return sprintf(
            "INSERT INTO %s (%s) VALUES (%s) ON DUPLICATE KEY UPDATE %s;",
            $this->getQuotedName($tableName),
            implode(', ', $columns),
            implode(', ', array_fill(0, count($data), '?')),
            implode(', ', $params)
        );
    }

    public function getQuotedName($name)
    {
        return $name;
    }
}