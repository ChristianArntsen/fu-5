<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class PaymentTypesHelper extends \Codeception\Module
{
    private $course_id;

    /*
     * Codeception doesn't allow the constructor to call functions that make
     * references to outside modules like Db, DbHelper, or REST modules.
    */
    function __construct($course_id)
    {
        parent::__construct();
        $this->course_id = $course_id;
    }

    /**
     * Create the specified payment type by adding it to the database.  False is
     * returned if the payment type specified is not recognized
     *
     * @param  string $paymentType - type of payment to create
     * @param  array $paymentData - (optional in some cases) data required by database to create the payment type
     * @param  $Item - (optional in some cases) Item object
     * @return null - on failure to create the object function will return null.
     */
    public function _paymentFactory($paymentType, array $paymentData = null, $Item = null){
        switch($paymentType){
            case 'punch_card':
                $punchCard = new PunchCardHelper($this->course_id);

                //make sure the necessary fields are set
                if(isset($paymentData['punches']))
                    $punchCard->_createPunchCard($paymentData['item_id'], $paymentData['punches']);
                else
                    $punchCard->_createPunchCard($paymentData['item_id']);

                return $punchCard;
                break;
            case 'giftcard':
                $giftcard = new GiftCardHelper($this->course_id);

                //make sure the necessary fields are set
                if($paymentData == null)
                    $giftcard->_createGiftCard();
                else
                    $giftcard->_createGiftCard($paymentData['value']);

                return $giftcard;
                break;
            case 'raincheck':
                $raincheck = new RaincheckHelper($Item);
                $raincheck_num = $paymentData['raincheck_number'];
                unset($paymentData['raincheck_number']);

                if(empty($paymentData))
                    $raincheck->_createRaincheck($raincheck_num);
                else
                    $raincheck->_createRaincheck($raincheck_num, $paymentData);

                return $raincheck;
                break;
            case 'loyalty_points':
                $loyaltyPoints = new LoyaltyRatesHelper($Item);
                $loyaltyPoints->_createLoyaltyRate();
                return $loyaltyPoints;
                break;
            default:
                return false;
        }
        return true;
    }




}
