<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class SalesV2InterfaceHelper extends \Codeception\Module
{
    private $itemsUsed = [];
    private $courseID;
    private $cart_id;
    private $next_sale_id;
    private $next_number;

    //object storage
    private $itemsObj;
    private $payments;
    private $paymentTypes;
    private $cartClosure;

    function __construct($cart_id, $sale_id, $number, $courseID){
        parent::__construct();
        $this->cart_id = $cart_id;
        $this->next_sale_id = $sale_id;
        $this->next_number = $number;
        $this->courseID = $courseID;

        $this->itemsObj = new ItemsHelper($this->courseID);
        $this->itemsObj->_addItemsToDatabase();

        $this->payments = new PaymentsHelper($this->cart_id, $this->courseID, $this);

        $this->cartClosure = new CartClosureHelper($this->cart_id);

        $this->paymentTypes = new PaymentTypesHelper($this->courseID);
    }

/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
//                                                                            ItemsHelper/Green Fees Interface
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


    public function _addItemToCart_API($item, array $values = null){
        $this->itemsUsed[] = $item;
        $this->itemsObj->_addItemToCart_API($item, $this->cart_id, $values);
    }


    /**
     * Add an item to a cart using the database.
     * Uses array_merge() to overwrite default values for columns
     * in the foreup_pos_cart_items table. Values related to the item specified
     * by $item will automatically be pulled from the Items class.
     *
     * @param  string $item - item name
     * @param  array $values - (optional) desired values not tracked by ItemsHelper objects
     * @return null  no return value
     */
    public function _addItemToCart_DB($item, array $values = null){
        $this->itemsUsed[] = $item;
        $this->itemsObj->_addItemToCart_DB($item, $this->cart_id, $values);
    }


    /**
     * Calculates the tax for $item and returns the total
     * cost of $item
     *
     * @param  string $item - item name
     * @return float - total cost of $item
     */
    public function _totalCost($item){
        return $this->itemsObj->_totalCost($item);
    }

    /**
     * Calculates the tax for $item and returns the total
     * cost of $item
     *
     * @param  string $item - item name
     * @return float - tax applied to $item
     */
    public function _tax($item){
        return $this->itemsObj->_tax($item);
    }

    /**
     * Returns all information about the item specified in $item
     * that is stored in the private data member $Items.
     *
     * @param  string $item - item name
     * @return array
     */
    public function _getItem($item){
        return $this->itemsObj->_getItem($item);
    }

    /**
     * returns the item number for $item
     *
     * @param  string $item - item name
     * @return int - item number
     */
    public function _getItemNumber($item){
        return $this->itemsObj->_getItemNumber($item);
    }

    /**
     * Returns the inventory level
     *
     * @return int - inventory level
     */
    public function _getInventoryLevel(){
        return $this->itemsObj->_getInventoryLevel();
    }

    /**
     * Returns the id of $item
     *
     * @param  string $item - item name
     * @return int - id of $item
     */
    public function _getItemID($item){
        return $this->itemsObj->_getItemID($item);
    }

    /**
     * takes an items ID and returns its name
     *
     * @param  int $itemID - item id
     * @return string - name of the item with $itemID or null if no match is found
     */
    public function _getItemName($itemID){
        return $this->itemsObj->_getItemName($itemID);
    }

    /**
     * returns the Fee object used for creating green fees
     *
     * @return object - FeeItemsHelper Object
     */
    public function _getFeeObject(){
        return $this->itemsObj->_getFeeObject();
    }

    public function _subtotal(array $itemNames){
        return $this->itemsObj->_subtotal($itemNames);
    }


    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
//                                                                            PaymentsHelper Interface
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /**
     * makes a cash payment of $X.xx($amount)
     *
     * @param  int $amount - amount to be payed in cash
     * @param  bool $checkApi(optional) - check the API response for success
     * @return array - response from the api
     */
    public function _cashPayment($amount, $checkApi = false){
        return $this->payments->_cashPayment($amount, $checkApi);
    }

    /**
     * makes a check payment of $X.xx($amount)
     *
     * @param  int $amount - amount to be payed for with a check
     * @param  string $checkNum - check number
     * @param  bool $checkApi(optional) -  check the API response for success
     * @return array - response from the api
     */
    public function _checkPayment($amount, $checkNum, $checkApi = false){
        return $this->payments->_checkPayment($amount, $checkNum, $checkApi);
    }

    /**
     * makes a punch card payment of $X.xx($amount)
     *
     * @param  int $amount - amount to be payed for with the punch card
     * @param  object $punchCardObj - the punch card object
     * @param  bool $checkApi(optional) -  check the API response for success
     * @return array - response from the api
     */
    public function _punchCardPayment($amount, $punchCardObj, $checkApi = false){
        $itemID = $punchCardObj->_getItemID();
        $itemName = $this->itemsObj->_getItemName($itemID);
        $itemInfo = $this->itemsObj->_getItem($itemName);
        $item = [
            "item_id" => $itemInfo["item_id"],
            "name" => $itemInfo["db-requirements"]["name"]
        ];

        return $this->payments->_punchCardPayment($amount, $punchCardObj, $item, $checkApi);
    }

    /**
     * makes a gift card payment of $X.xx($amount)
     *
     * @param  int $amount - amount to be payed for with the gift card
     * @param  object $giftcardObj - the gift card object
     * @param  bool $checkApi(optional) -  check the API response for success
     * @return array - response from the api
     */
    public function _giftcardPayment($amount, $giftcardObj, $checkApi = false){
        return $this->payments->_giftcardPayment($amount, $giftcardObj, $checkApi);
    }

    /**
     * makes a raincheck payment of $X.xx($amount)
     *
     * @param  int $amount - amount to be payed for with the gift card
     * @param  object $raincheckObj - the Raincheck object
     * @param  bool $checkApi(optional) -  check the API response for success
     * @return array - response from the api
     */
    public function _raincheckPayment($amount, $raincheckObj, $checkApi = false){
        return $this->payments->_raincheckPayment($amount, $raincheckObj, $checkApi);
    }

    /**
     * makes a payment of $X.xx($amount) using Loyalty Points
     *
     * @param  int $amount - amount to be payed for with the gift card
     * @param  object $LoyaltyPointsObj - the Loyalty Points object
     * @param  int $customerID - customer id to be used in API call
     * @param  bool $checkApi(optional) -  check the API response for success
     * @return array - response from the api
     */
    public function _loyaltyPayment($amount, $LoyaltyPointsObj, $customerID, $LoyaltyItem, $checkApi = false){
        return $this->payments->_loyaltyPayment($amount, $LoyaltyPointsObj, $customerID, $LoyaltyItem, $checkApi);
    }

    /**
     * Returns the expected response from the api after making a payment
     *
     * @param  string $paymentType - type of payment made
     * @return array - response from the api
     */
    public function _getExpectedResponse($paymentType){
        return $this->payments->_getExpectedResponse($paymentType);
    }

/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
//                                                                            CartClosureHelper Interface
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    /**
     * Uses the API to close the cart.  Where possible data will be pulled from the ItemsHelper Object($this->itemsObj)
     *
     * @param  array $miscItemsData - array of items. items should be stored as a key value pair.  the key is the item name and the value is an array of data to be passed to the api
     * @param  array $customersData(optional) - array of customer data needed by the api
     * @param  object $paymentObj(optional) - any Payment objects used in the sale
     * @param  bool $checkSuccess(optional) - should this object check that the cart successfully closed
     * @return null  no return value
     */
    public function _closeCart(array $miscItemsData, array $customersData = null, $checkSuccess = false, $paymentObj = null){
        //prepare payment data to be sent to the CartClosureHelper
        $paymentData = $this->payments->_getPaymentData();

        foreach($paymentData as $paymentType => $paymentInfo){
            //set missing fields
            $paymentInfo['payment_id'] = $this->payments->_getPayment_ID($paymentType);
            $paymentInfo['merchant_data'] = $this->payments->_getMerchant_Data($paymentType);
            $paymentInfo['success'] = $this->payments->_getStatus();
            $paymentInfo['invoice_id'] = 0;

            //convert data to datatype API expects
            $paymentInfo['params'] = strval($paymentInfo['params']);
            $paymentInfo['bypass'] = boolval($paymentInfo['bypass']);
            $paymentInfo['verified'] = intval($paymentInfo['verified']);

            //save the updated array
            $paymentData[$paymentType] = $paymentInfo;
        }

        $paymentData['Payment_Object'] = $paymentObj;

        //Prepare item data to be sent to the CartClosureHelper
        $totalTax = 0;
        $items = [];
        foreach ($this->itemsUsed as $itemName) {
            //get items data
            $item = $this->itemsObj->_getItem($itemName);

            //convert data so it matches how the API stores it
            $item['db-requirements']['base_price'] = $item['db-requirements']['cost_price'];
            $item['db-requirements']['inventory_level'] = $item['db-requirements']['quantity'];
            unset($item['db-requirements']['quantity']);
            $item['db-requirements']['inventory_unlimited'] = $item['db-requirements']['is_unlimited'];
            unset($item['db-requirements']['is_unlimited']);
            $item['db-requirements']['number_sides'] = $item['db-requirements']['number_of_sides'];
            unset($item['db-requirements']['number_of_sides']);

            //get the api data fields
            $items[$itemName] = $this->cartClosure->_getCartClosureAPIItemInfo();
            $items[$itemName] = array_merge($items[$itemName], array_intersect_key($item['db-requirements'], $items[$itemName]));
            $items[$itemName]['item_id'] = $item['item_id'];
            //check if the item is a green fee and get its extra fields if it is
            if(isset($item['Green Fees'])){
                $items[$itemName] = array_merge($items[$itemName], $item['Green Fees']);
                $items[$itemName]['base_price'] = $item['db-requirements']['unit_price'];
                $items[$itemName]['item_type'] = $item['item_type'];
            }


            //merge the supplied item data
            if(!empty($miscItemsData[$itemName])) {
                $items[$itemName] = array_merge($items[$itemName], array_intersect_key($miscItemsData[$itemName], $items[$itemName]));
            }

            //calculate remaining values
            $subtotal = $items[$itemName]['unit_price'] * $items[$itemName]['quantity'];
            $items[$itemName]['subtotal'] = $subtotal * ((100 - intval($items[$itemName]['discount_percent'])) / 100);
            $items[$itemName]['non_discount_subtotal'] = $subtotal;
            $items[$itemName]['discount_amount'] = $subtotal * (intval($items[$itemName]['discount_percent'])/100);

            //calculate the dollar amount for each tax
            $taxes = null;
            foreach($item['Tax'] as $tax){
                $tax['amount'] = $this->itemsObj->tax($items[$itemName]['subtotal'], $tax['percent']);
                $totalTax += $tax['amount'];
                $taxes[] = $tax;
            }
            $items[$itemName]['Tax'] = $taxes;

            //calculate remaining values
            $items[$itemName]['tax'] = $totalTax;
            $items[$itemName]['total'] = $totalTax + $items[$itemName]['subtotal'];

            //convert data to datatype API expects
            $items[$itemName]['unit_price_includes_tax'] = boolval($items[$itemName]['unit_price_includes_tax']);
            $items[$itemName]['is_serialized'] = strval($items[$itemName]['is_serialized']);
            $items[$itemName]['is_giftcard'] = strval($items[$itemName]['is_giftcard']);
            $items[$itemName]['food_and_beverage'] = strval($items[$itemName]['food_and_beverage']);
            $items[$itemName]['is_side'] = strval($items[$itemName]['is_side']);
            $items[$itemName]['inactive'] = strval($items[$itemName]['inactive']);
            $items[$itemName]['erange_size'] = strval($items[$itemName]['erange_size']);
            $items[$itemName]['is_fee'] = strval($items[$itemName]['is_fee']);
            $items[$itemName]['meal_course_id'] = strval($items[$itemName]['meal_course_id']);
            $items[$itemName]['do_not_print_customer_receipt'] = strval($items[$itemName]['do_not_print_customer_receipt']);
        }


        //Prepare customer data to be sent to the CartClosureHelper


        //Have the CartClosure Object close the cart
        $saleData = [
            "sale_id" => $this->next_sale_id,
            "number" => $this->next_number
        ];

        if(isset($paymentData['Loyalty Points'])){
            $saleData['Loyalty Points'] = true;
        }

        $this->cartClosure->_closeCart($items, $paymentData, $saleData, null, $checkSuccess);
    }


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
//                                                                            PaymentTypesHelper Interface
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    /**
     * Create the specified payment type by adding it to the database.  False is
     * returned if the payment type specified is not recognized
     *
     * @param  string $paymentType - type of payment to create
     * @param  string $itemName - (optional) name of the item attached to the payment type
     * @param  array $otherData - (optional) other data needed to place the payment type into the database
     * @param  array $itemObj - - (optional in some cases) Item object
     * @return bool - true = success, false = failure
     */
    public function _paymentFactory($paymentType, $itemName = null, array $otherData = null, $itemObj = null){
        switch($paymentType){
            case 'punch_card':
                $itemID = null;
                //for punch cards make sure the item name was passed in
                if(isset($itemName))
                    $itemID = $this->itemsObj->_getItemID($itemName);
                else
                    return false;

                //check if # of punches was sent in
                if(isset($otherData['punches']))
                    return $this->paymentTypes->_paymentFactory($paymentType, array('item_id' => $itemID, 'punches' => $otherData['punches']));
                else
                    return $this->paymentTypes->_paymentFactory($paymentType, array('item_id' => $itemID));
                break;
            case 'giftcard':
                if($otherData == null)
                    return $this->paymentTypes->_paymentFactory($paymentType);
                else
                    return $this->paymentTypes->_paymentFactory($paymentType, $otherData);
                break;
            case 'raincheck':
                if(isset($otherData['raincheck_number']) && $itemObj !== null)
                    return $this->paymentTypes->_paymentFactory($paymentType, $otherData, $itemObj);
                else
                    return false;
                break;
            case 'loyalty_points':
                return $this->paymentTypes->_paymentFactory($paymentType, null, $this->itemsObj->_getItemID($itemName));
                break;
            default:
                return false;
        }
    }

}
