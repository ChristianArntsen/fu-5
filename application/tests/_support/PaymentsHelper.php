<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class PaymentsHelper extends \Codeception\Module
{
    private $payments = [];
    private $paymentPrototype = [
        "approved" => "false",
        "card_type" => "",
        "card_number" => "",
        "cardholder_name" => "",
        "auth_amount" => 0,
        "auth_code" => "",
        "description" => "",
        "tran_type" => "",
        "type" => "",
        "record_id" => 0,
        "amount" => "",
        "tip_recipient" => 0,
        "number" => '',
        "params" => null,
        "bypass" => 0,
        "is_auto_gratuity" => 0,
        "verified" => false,
        "refund_reason" => "",
        "refund_comment" => ""
    ];
    private $paymentResponse = [
        "success" => true,
        "payment_id" => "",
        "description" => "",
        "amount" => "",
        "type" => "",
        "record_id" => 0,
        "number" => "",
        "params" => "",
        "bypass" => false,
        "merchant_data" => false,
        "tran_type" => "",
        "approved" => "false",
        "verified" => 0
    ];

    private $cart_id = 0;
    private $course_id;
    private $payment_id = [];
    private $merchant_data;
    private $success;
    private $paymentObj = [];
    private $Interface;
    private $ItemNames = [];
    private $customer;

    /*
     * Codeception doesn't allow the constructor to call functions that make
     * references to outside modules like Db, DbHelper, or REST modules.
    */
    function __construct($cart_id, $course_id, $interface)
    {
        parent::__construct();

        $this->cart_id = $cart_id;
        $this->course_id = $course_id;
        $this->Interface = $interface;
    }

    /**
     * Sends a payment to the API and optionally also verifies that the payment
     * was successful by checking the expected response against the actual
     * response.
     *
     * @param  string $paymentType - type of payment being processed
     * @param  bool $checkApi - (optional) check the API response for success
     * @return array - response from the api
     */
    private function pay($paymentType, $checkApi = false){
        //get modules from codeception
        $Rest = $this->getModule('REST');

        //pay for cart
        $Rest->sendPOST('/index.php/api/cart/'.$this->cart_id.'/payments', json_encode($this->payments[$paymentType]));
        $response = $Rest->grabResponse();

        //save information from the response
        $decoded_response = json_decode($response, true);
        if(isset($decoded_response["payment_id"]))
            $this->payment_id[$paymentType] = $decoded_response["payment_id"];
        if(isset($decoded_response['merchant_data']))
            $this->merchant_data[$paymentType] = $decoded_response['merchant_data'];
        if(isset($decoded_response['success']))
            $this->success = $decoded_response['success'];

        //check api response for success
        if($checkApi){
            $this->CheckAPIResponse($response, $paymentType);
        }

        return $response;
    }

    /**
     * verifies that the payment was successful by checking the
     * expected response against the actual response.
     *
     * @param  string $response - response received from the API
     * @param  string $paymentType - the payment type that was used
     */
    private function CheckAPIResponse($response, $paymentType){
        codecept_debug("\n* Checking API response for success");

        //get codeception modules
        $Db = $this->getModule('Db');
        $SalesV2ApiHelper = $this->getModule('Salesv2apiHelper');

        //check that the payments endpoint succeeded
        $Db->seeInDatabase('foreup_pos_cart_payments', array('cart_id' => $this->cart_id, 'type' => $this->payments[$paymentType]["type"]));

        $expectedResponse = $this->buildExpectedResponse($paymentType);

        $SalesV2ApiHelper->checkAPIForSuccess($expectedResponse, $response);
        //add blank line to console
        codecept_debug("");
    }

    private function buildExpectedResponse($paymentType){
        $Db = $this->getModule('Db');
        $SalesV2ApiHelper = $this->getModule('Salesv2apiHelper');

        //set payment response info
        $expectedResponse = $this->paymentResponse;
        $expectedResponse["amount"] = $this->payments[$paymentType]["amount"];
        $expectedResponse["payment_id"] = intval($Db->grabFromDatabase('foreup_pos_cart_payments', 'payment_id', array('cart_id' => $this->cart_id)));
        $expectedResponse["description"] = $this->payments[$paymentType]["description"];
        $expectedResponse["type"] = $this->payments[$paymentType]["type"];
        if(!empty($this->payments[$paymentType]["number"])){
            $expectedResponse["number"] = $this->payments[$paymentType]["number"];
        }

        switch($paymentType){
            case "Punch Card":
                $itemID = $this->paymentObj[$paymentType]['Item_info']['item_id'];
                $used =  $this->paymentObj[$paymentType]['Object']->_getPunchesUsed();

                $expectedResponse["record_id"] = intval($this->paymentObj[$paymentType]['Object']->_getPunchCardID());

                $expectedResponse["params"] = [
                    "punches_used" => [
                        $itemID => [
                            'used' => $used,
                            'price_class_id' => 0
                        ]
                    ]
                ];

                $expectedResponse["punch_card"] = [
                    'punch_card_id' => strval($this->paymentObj[$paymentType]['Object']->_getPunchCardID()),
                    'course_id' => strval($this->course_id),
                    'customer_id' => NULL,
                    'punch_card_number' => strval($this->paymentObj[$paymentType]['Object']->_getPunchCardNumber()),
                    'details' => '',
                    'expiration_date' => strval($this->paymentObj[$paymentType]['Object']->_getExpirationDate()),
                    'issued_date' => strval($this->paymentObj[$paymentType]['Object']->_getIssueDate()),
                    'deleted' => '0',
                    'customer_first_name' => NULL,
                    'customer_last_name' => NULL,
                    'items' => [
                        [
                            'item_id' => intval($itemID),
                            'price_class_id' => 0,
                            'price_class_name' => 0,
                            'name' => $this->paymentObj[$paymentType]['Item_info']['name'],
                            'punches' => $this->paymentObj[$paymentType]['Object']->_getPunches(),
                            'used' => $used
                        ]
                    ]
                ];
                break;
            case "Gift Card":
                $expectedResponse["record_id"] = intval($this->paymentObj[$paymentType]['Object']->_getGiftCardID());
                $expectedResponse["gift_card"] = [
                    'CID' => '0',
                    'giftcard_id' => $this->paymentObj[$paymentType]['Object']->_getGiftCardID(),
                    'course_id' => strval($this->course_id),
                    'giftcard_number' => strval($this->paymentObj[$paymentType]['Object']->_getGiftCardNumber()),
                    'value' => number_format((double)$this->paymentObj[$paymentType]['Object']->_getGiftCardValue(), 2, ".", ""),
                    'customer_id' => $this->paymentObj[$paymentType]['Object']->_getCustomerID(),
                    'details' => '',
                    'date_issued' => $this->paymentObj[$paymentType]['Object']->_getDateIssued(),
                    'expiration_date' => $this->paymentObj[$paymentType]['Object']->_getExpirationDate(),
                    'department' => '',
                    'category' => '',
                    'deleted' => '0',
                ];

                //if the amount to be paid is greater than the giftcard total the response will send back the gift card total
                if($this->payments[$paymentType]["amount"] > $this->paymentObj[$paymentType]['Object']->_getGiftCardValue()){
                    $expectedResponse["amount"] = $this->paymentObj[$paymentType]['Object']->_getGiftCardValue();
                }else { //when the giftcards value can't cover the cost these fields are removed.
                    $expectedResponse['status'] = false;
                    $expectedResponse['msg'] = $this->paymentObj[$paymentType]['Object']->_getExpectedPaymentMSG();
                }
                break;
            case "Raincheck":
                $raincheck = $this->paymentObj[$paymentType]['Object'];
                $expectedResponse["record_id"] = intval($raincheck->_getRaincheckID());
                $expectedResponse["raincheck"] = $this->raincheckResponse($paymentType);
                break;
            case "Loyalty Points":
                $expectedResponse["description"] = "Loyalty";
                $amount = $this->paymentObj[$paymentType]['Object']->_getPointsUsed($expectedResponse["amount"]);
                if($SalesV2ApiHelper->isWholeNumber($amount)){
                    $amount = intval($amount);
                }
                $expectedResponse["number"] = $amount;
                $expectedResponse["record_id"] = intval($this->customer);
                break;
            default:
                break;
        }
        return $expectedResponse;
    }

    public function _getExpectedResponse($paymentType){
        return $this->buildExpectedResponse($paymentType);
    }

    private function raincheckResponse($paymentType){
        $raincheck = $this->paymentObj[$paymentType]['Object'];
        $Response = [
            "raincheck_id" => strval($raincheck->_getRaincheckID()),
            "raincheck_number" => strval($raincheck->_getRaincheckNumber()),
            "teesheet_id" => strval($raincheck->Item->_getTeeSheetID()),
            "teetime_id" => "",
            "teetime_date" => "0000-00-00",
            "teetime_time" => "0",
            "date_issued" => $raincheck->_getDateIssued(),
            "date_redeemed" => "0000-00-00 00:00:00",
            "players" => strval($raincheck->_getNumPlayers()),
            "holes_completed" => strval($raincheck->_getHolesCompleted()),
            "customer_id" => "0",
            "employee_id" => "12",
            "green_fee" => [
                "unit_price" => $raincheck->Item->_getUnitPrice(),
                "price_class_id" => intval($raincheck->Item->_getPriceClassID()),
                "timeframe_id" => intval($raincheck->Item->_getTimeframeID()),
                "item_number" => 1,
                "item_id" => $raincheck->Item->_getItemID()
            ],
            "cart_fee" => false,
            "tax" => $this->Interface->_tax($raincheck->Item->_getFeeItemLabel()),
            "total" => $this->Interface->_totalCost($raincheck->Item->_getFeeItemLabel()),
            "expiry_date" => $raincheck->_getExpirationDate(),
            "deleted" => "0",
            "subtotal" => $raincheck->Item->_getUnitPrice()
        ];
        return $Response;
    }

    /**
     * makes a cash payment of $X.xx($amount)
     *
     * @param  int $amount - amount to be payed in cash
     * @param  bool $checkApi(optional) - check the API response for success
     * @return array - response from the api
     */
    public function _cashPayment($amount, $checkApi = false){
        $paymentType = "Cash";
        codecept_debug("\n* Paying for items using ".$paymentType);
        $this->payments[$paymentType] = $this->paymentPrototype;

        //set up the payment payload
        $this->payments[$paymentType]["description"] = "Cash";
        $this->payments[$paymentType]["type"] = "cash";
        unset( $this->payments[$paymentType]["number"]);
        $this->payments[$paymentType]["amount"] = $amount;

        //pay for cart with cash
        return $this->pay($paymentType, $checkApi);
    }

    /**
     * makes a check payment of $X.xx($amount)
     *
     * @param  int $amount - amount to be payed for with a check
     * @param  string $checkNum - check number
     * @param  bool $checkApi(optional) -  check the API response for success
     * @return array - response from the api
     */
    public function _checkPayment($amount, $checkNum, $checkApi = false){
        $paymentType = "Check";
        codecept_debug("\n* Paying for items using a ".$paymentType);
        $this->payments[$paymentType] = $this->paymentPrototype;

        //set up the payment payload
        if($checkNum === ""){
            $this->payments[$paymentType]["description"] = "Check";
        }else {
            $this->payments[$paymentType]["description"] = "Check:" . $checkNum;
        }
        $this->payments[$paymentType]["type"] = "check";
        $this->payments[$paymentType]["number"] = $checkNum;
        $this->payments[$paymentType]["amount"] = $amount;

        //pay for cart with cash
        return $this->pay($paymentType, $checkApi);
    }

    /**
     * makes a punch card payment of $X.xx($amount)
     *
     * @param  int $amount - amount to be payed for with the punch card
     * @param  object $punchCardObj - the punch card object
     * @param  array $punchCardItem - the item contained in the punch card
     * @param  bool $checkApi(optional) -  check the API response for success
     * @return array - response from the api
     */
    public function _punchCardPayment($amount, $punchCardObj, array $punchCardItem, $checkApi = false){
        $paymentType = "Punch Card";
        codecept_debug("\n* Paying for items using a ".$paymentType);
        $this->paymentObj[$paymentType]['Object'] = $punchCardObj;
        $this->paymentObj[$paymentType]['Item_info'] = $punchCardItem;

        //get the payment prototype to fill out
        $this->payments[$paymentType] = $this->paymentPrototype;

        //set up the payment payload
        $this->payments[$paymentType]["description"] = $punchCardObj->_getPaymentDescription();
        $this->payments[$paymentType]["type"] = $punchCardObj->_getPaymentType();
        $this->payments[$paymentType]["number"] = $punchCardObj->_getPunchCardNumber();
        $this->payments[$paymentType]["amount"] = $amount;

        //pay for cart with a punch card
        return $this->pay($paymentType, $checkApi);
    }

    /**
     * makes a gift card payment of $X.xx($amount)
     *
     * @param  int $amount - amount to be payed for with the gift card
     * @param  object $giftcardObj - the gift card object
     * @param  bool $checkApi(optional) -  check the API response for success
     * @return array - response from the api
     */
    public function _giftcardPayment($amount, $giftcardObj, $checkApi = false){
        $paymentType = "Gift Card";
        codecept_debug("\n* Paying for items using a ".$paymentType);
        $this->paymentObj[$paymentType]['Object'] = $giftcardObj;

        //get the payment prototype to fill out
        $this->payments[$paymentType] = $this->paymentPrototype;

        //set up the payment payload
        $this->payments[$paymentType]["description"] = $giftcardObj->_getPaymentDescription();
        $this->payments[$paymentType]["type"] = $giftcardObj->_getPaymentType();
        $this->payments[$paymentType]["number"] = $giftcardObj->_getGiftCardNumber();
        $this->payments[$paymentType]["amount"] = $amount;

        //pay for cart with a gift card
        return $this->pay($paymentType, $checkApi);
    }

    /**
     * makes a raincheck payment of $X.xx($amount)
     *
     * @param  int $amount - amount to be payed for with the gift card
     * @param  object $raincheckObj - the Raincheck object
     * @param  bool $checkApi(optional) -  check the API response for success
     * @return array - response from the api
     */
    public function _raincheckPayment($amount, $raincheckObj, $checkApi = false){
        $paymentType = "Raincheck";
        codecept_debug("\n* Paying for items using a ".$paymentType);
        $this->paymentObj[$paymentType]['Object'] = $raincheckObj;

        //get the payment prototype to fill out
        $this->payments[$paymentType] = $this->paymentPrototype;

        //set up the payment payload
        $this->payments[$paymentType]["description"] = $raincheckObj->_getPaymentDescription();
        $this->payments[$paymentType]["type"] = $raincheckObj->_getPaymentType();
        $this->payments[$paymentType]["number"] = $raincheckObj->_getRaincheckNumber();
        $this->payments[$paymentType]["amount"] = $amount;
        $this->payments[$paymentType]['record_id'] = $raincheckObj->_getRaincheckID();

        $this->payments['Raincheck']['raincheck'] = $this->raincheckResponse($paymentType);

        //pay for cart with a gift card
        return $this->pay($paymentType, $checkApi);
    }

    /**
     * makes a payment of $X.xx($amount) using Loyalty Points
     *
     * @param  int $amount - amount to be payed for with the gift card
     * @param  object $loyaltyPointsObj - the Loyalty Points object
     * @param  int $customerID - customer id to be used in API call
     * @param  string $LoyaltyItem - customer id to be used in API call
     * @param  bool $checkApi(optional) -  check the API response for success
     * @return array - response from the api
     */
    public function _loyaltyPayment($amount, $loyaltyPointsObj, $customerID, $LoyaltyItem, $checkApi = false){
        $paymentType = "Loyalty Points";
        codecept_debug("\n* Paying for items using a ".$paymentType);
        $this->paymentObj[$paymentType]['Object'] = $loyaltyPointsObj;

        //get the payment prototype to fill out
        $this->payments[$paymentType] = $this->paymentPrototype;

        //set up the payment payload
        $this->payments[$paymentType]["description"] = $loyaltyPointsObj->_getPaymentDescription();
        $this->payments[$paymentType]["type"] = $loyaltyPointsObj->_getPaymentType();
        $this->payments[$paymentType]["amount"] = $amount;
        $this->payments[$paymentType]['record_id'] = $customerID;

        unset($this->payments[$paymentType]["number"]);

        $this->ItemNames[$LoyaltyItem] = $LoyaltyItem;
        $this->customer = $customerID;

        //pay for cart with a gift card
        return $this->pay($paymentType, $checkApi);
    }

    /**
     * Returns the payment data that was sent to the API
     *
     * @return array - All payment data.
     */
    public function _getPaymentData(){
        return $this->payments;
    }

    /**
     * Return the payment id for the specified payment type
     *
     * @param  string $paymentType - amount to be payed for with a check
     * @return int - payment id
     */
    public function _getPayment_ID($paymentType){
        return $this->payment_id[$paymentType];
    }

    /**
     * Return the merchant data for the specified payment type
     *
     * @param  string $paymentType - amount to be payed for with a check
     * @return bool - merchant data
     */
    public function _getMerchant_Data($paymentType){
        return $this->merchant_data[$paymentType];
    }

    /**
     * Return whether the payment was successful
     *
     * @return bool - was payment successful
     */
    public function _getStatus(){
        return $this->success;
    }
}
