<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class RaincheckHelper extends \Codeception\Module
{
    private $raincheck = [
        'raincheck_number' => 0,
        'teesheet_id' => 26,
        'date_issued' => "",
        'date_redeemed' => '0000-00-00 00:00:00',
        'players' => 4,
        'holes_completed' => 0,
        'employee_id' => 12,
        'green_fee' => 40.00,
        'cart_fee' => 0.00,
        'tax' => 5.40,
        'total' => 45.40,
        'green_fee_price_category' => "24_130_1",
        'cart_price_category' => "",
        'deleted' => 0
    ];
    public $Item;

    private $table_name = "foreup_rainchecks";

    function __construct($item = null)
    {
        parent::__construct();
        $this->raincheck['date_issued'] = date('Y-m-d h:m:s');
        if($item !== null){
            $this->Item = $item;
            $this->raincheck['teesheet_id'] = $item->_getTeeSheetID();
        }

    }

    /**
     * Calculate and set the total value of the raincheck
     */
    private function setTotal(){
        $this->raincheck['total'] = $this->raincheck['players'] * ($this->raincheck['green_fee'] + $this->raincheck['tax']);
    }

    /**
     * Create a raincheck by placing it in the database
     *
     * @param  int $raincheck_number - The raincheck number used to pull it up in the pos
     * @param  array $otherdata - (optional) change raincheck defaults
     */
    public function _createRaincheck($raincheck_number, array $otherdata = null){
        //get connection to database
        $Db = $this->getModule('Db');
        $DbHelper = $this->getModule('DbHelper');

        if(isset($this->Item)){
            $this->raincheck['green_fee'] = $this->Item->_getUnitPrice();
            $this->raincheck['tax'] = round(($this->Item->_getTaxInfo()[0]['percent'] * $this->raincheck['green_fee'])/100, 2, PHP_ROUND_HALF_UP);
            $this->raincheck['total'] = $this->raincheck['green_fee'] + $this->raincheck['tax'];
            $this->raincheck['green_fee_price_category'] = $this->Item->_getPriceClassID()."_".$this->Item->_getTimeframeID()."_1";
        }

        //save the values that were passed in
        $this->raincheck['raincheck_number'] = $raincheck_number;
        if(!empty($otherdata)) {
            $this->raincheck = array_merge($this->raincheck, array_intersect_key($otherdata, $this->raincheck));
        }

        //set the total value of the raincheck
        $this->setTotal();

        //place raincheck into database
        $this->raincheck['raincheck_id'] = $DbHelper->InsertAndUpdateOnDuplicate($this->table_name, $this->raincheck)['raincheck_id'];

        //grab raincheck ID from DB
        //$this->raincheck['raincheck_id'] = $Db->grabFromDatabase($this->table_name, 'raincheck_id', $this->raincheck);
    }

    public function _getNumPlayers(){
        return $this->raincheck['players'];
    }

    public function _setNumPlayers($players){
        $this->raincheck['players'] = $players;

        //reset the raincheck's value
        $this->setTotal();
    }

    public function _getHolesCompleted(){
        return $this->raincheck['holes_completed'];
    }

    public function _getPaymentDescription(){
        return "Raincheck ".$this->raincheck['raincheck_number'];
    }

    public function _getPaymentType(){
        return "raincheck";
    }

    public function _getRaincheckNumber(){
        return $this->raincheck['raincheck_number'];
    }

    public function _getRaincheckID(){
        return $this->raincheck['raincheck_id'];
    }

    public function _getDateIssued(){
        return $this->raincheck['date_issued'];
    }

    public function _getExpirationDate(){
        $Db = $this->getModule('Db');
        return $Db->grabFromDatabase('foreup_rainchecks', 'expiry_date', array('raincheck_id' => $this->raincheck['raincheck_id']));
    }

    public function _getTeeSheetID(){
        return $this->Item->_getTeeSheetID();
    }

    public function _setDateRedeemed($date = null){
        $DbHelper = $this->getModule('DbHelper');

        //if date is not set then set it to the current time
        if($date === null){
            $date = date('Y-m-d h:m:s');
        }

        $DbHelper->updateDatabase('foreup_rainchecks', array('raincheck_id' => $this->raincheck['raincheck_id']), array('date_redeemed' => $date));
    }

    /**
     * Used to test invalid rainchecks in the POS
     */
    public function _invalidateRaincheck(){
        //change the raincheck number to an invalid number
        $this->raincheck['raincheck_number'] = -1;
        $this->raincheck['raincheck_id'] = -1;
    }
}
