<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class CartClosureHelper extends \Codeception\Module
{
    private $closeCart = [
        "status" => "complete",
        "items" => [],
        "payments" => [],
        "total" => 0.00,
        "subtotal" => 0,
        "tax" => "",
        "num_items" => "1",
        "customer_note" => null
    ];
    private $closeCartItemInfo = [
        "order" => 0,
        "item_id" => "000000",
        "course_id" => "6270",
        "name" => "",
        "department" => "",
        "category" => "",
        "subcategory" => "",
        "supplier_id" => null,
        "item_number" => null,
        "description" => "",
        "unit_price" => "0.00",
        "base_price" => "0.00",
        "inactive" => "0",
        "do_not_print" => "0",
        "max_discount" => "100.00",
        "inventory_level" => 0,
        "inventory_unlimited" => 0,
        "cost_price" => "0.00",
        "is_giftcard" => "0",
        "is_side" => "0",
        "food_and_beverage" => "0",
        "number_sides" => "0",
        "number_salads" => "0",
        "number_soups" => "0",
        "print_priority" => "1",
        "is_fee" => "0",
        "erange_size" => "0",
        "meal_course_id" => "0",
        "taxes" => [],
        "is_serialized" => "0",
        "do_not_print_customer_receipt" => "0",
        "unit_price_includes_tax" => false,
        "allow_alt_description" => "0",
        "reorder_level" => "0",
        "receipt_content_id" => null,
        "receipt_content_signature_line" => null,
        "receipt_content_separate_receipt" => null,
        "is_pass" => "0",
        "pass_days_to_expiration" => null,
        "pass_restrictions" => null,
        "pass_price_class_id" => null,
        "pass_expiration_date" => null,
        "modifiers" => [],
        "sides" => "",
        "customer_groups" => [],
        "printer_groups" => "1",
        "item_type" => "item",
        "loyalty_points_per_dollar" => false,
        "loyalty_dollars_per_point" => 0,
        "timeframe_id" => null,
        "special_id" => null,
        "quantity" => 1,
        "selected" => true,
        "discount_percent" => 0,
        "params" => null,
        "is_sub_item" => false,
        "sub_category" => "",
        "discount" => 0,
        "subtotal" => 0,
        "total" => 0,
        "tax" => 0,
        "discount_amount" => 0,
        "non_discount_subtotal" => 0,
        "base_quantity" => 1,
        "printer_ip" => "",
        "loyalty_cost" => null,
        "loyalty_reward" => false,
        "giftcard_id" => null,
        "punch_card_id" => null,
        "teetime_id" => null,
        "modifier_total" => 0,
        "erange_code" => null,
        "line" => 1,
        "success" => 1
    ];
    private $taxes = [
        "name" => "",
        "percent" => 0,
        "cumulative" => "0",
        "amount" => 0.00
    ];
    private $closeCartPayment = [
        "amount" => 0.00,
        "refund_comment" => "",
        "refund_reason" => "",
        "number" => "",
        "type" => "",
        "description" => "",
        "record_id" => 0,
        "approved" => "false",
        "auth_amount" => 0,
        "auth_code" => "",
        "bypass" => false,
        "card_number" => "",
        "card_type" => "",
        "cardholder_name" => "",
        "invoice_id" => 0,
        "is_auto_gratuity" => 0,
        "params" => "",
        "tip_recipient" => 0,
        "tran_type" => "",
        "verified" => 0,
        "success" => true,
        "payment_id" => 0,
        "merchant_data" => false
    ];
    private $closeCartCustomers = [];

    private $closeCartResponse = [
        "sale_id" => 0,
        "number" => 0,
        "loyalty_points_spent" => 0,
        "cart_id" => 0,
        "success" => true
    ];
    private $cart_id;
    private $paymentObj;

    function __construct($cart_id)
    {
        parent::__construct();
        $this->cart_id = $cart_id;
    }


    /**
     * Closes the cart after payments have been made
     *
     * @param  array $itemsData - names of the items in the cart
     * @param  array $paymentData - payment types used to pay for the sale
     * @param  array $saleData - The sale_id and sale_number for the current sale(used to verify success)
     * @param  array $customersData(optional) - customers attached to cart
     * @param  bool $checkSuccess(optional) - should this object check that the cart successfully closed
     * @return array - response from the api
     */
    public function _closeCart(array $itemsData, array $paymentData, array $saleData, array $customersData = null, $checkSuccess = false){
        codecept_debug("\n* Closing the cart");

        //get modules from codeception
        $Rest = $this->getModule('REST');
        $Db = $this->getModule('Db');

        //set up the payload to close the cart
        $this->setItemInfo($itemsData);
        $this->setPaymentInfo($paymentData);

        if($customersData) {
            $this->setCustomerInfo($customersData);
        }

        //calculate the carts subtotal, tax, and final total
        $this->calculateFinalTotals();

        //finish the payment by closing the cart
        $Rest->sendPUT('/index.php/api/cart/'.$this->cart_id, json_encode($this->closeCart));
        $saleData['sale_id'] = $Db->grabFromDatabase('foreup_sales', 'max(sale_id)');
        $saleData['items'] = array_keys($itemsData);
        $response = $Rest->grabResponse();

        //make sure that the cart closed
        if($checkSuccess) {
            $this->CheckForSuccess($response, $saleData);
        }
    }

    /**
     * Returns the API format used to represent an Item while closing a cart
     *
     * @return array  API datamembers for a single item
     */
    public function _getCartClosureAPIItemInfo(){
        $itemInfo = $this->closeCartItemInfo;
        $itemInfo['taxes'] = $this->taxes;
        return $itemInfo;
    }

    /**
     * set item data so that the payload being sent to the api is properly formatted
     *
     * @param  array $items - array of items.  the item name should be the key and the item data should be a sub-array
     */
    private function setItemInfo(array $items){
        $itemInfo = null;

        //set data for each item
        foreach($items as $itemName => $item) {
            //get the items taxes
            $taxes = null;
            foreach ($item['Tax'] as $tax) {
                $taxes[] = array_merge($this->taxes, array_intersect_key($tax, $this->taxes));
            }
            if (!empty($taxes)) {
                $item['taxes'] = $taxes;
            }
            unset($item['Tax']);
            //get the items data
            $itemInfo[$itemName] = array_merge($this->closeCartItemInfo, $item);
            if($itemName === "Green Fee"){
                unset($itemInfo[$itemName]['order']);
            }
        }
        $this->closeCart["items"] = $itemInfo;
    }

    /**
     * set payment data so that the payload being sent to the api is properly formatted
     *
     * @param  array $payments - array of payment data.
     */
    private function setPaymentInfo(array $payments){
        $paymentInfo = null;

        if(isset($payments['Raincheck'])){
            $this->closeCartPayment['raincheck'] = [];
        }

        if(isset($payments['Payment_Object']) && isset($payments['Loyalty Points'])){
            $this->paymentObj = $payments['Payment_Object'];
        }
        unset($payments['Payment_Object']);

        //set data for each payment
        foreach($payments as $payment){
            $paymentInfo[] = array_merge($this->closeCartPayment, array_intersect_key($payment, $this->closeCartPayment));
        }

        $this->closeCart["payments"] = $paymentInfo;
    }

    /**
     * set customer data so that the payload being sent to the api is properly formatted
     *
     * @param  array $customers - array of Customers and their data.
     */
    private function setCustomerInfo(array $customers){
        $this->closeCart["customers"] = "";
    }

    /**
     * calculate the sales subtotal, total tax, and final total from the items in the cart
     */
    private function calculateFinalTotals(){
        //calculate totals
        $subtotal = 0;
        $totalTax = 0;
        $numItems = 0;
        foreach($this->closeCart["items"] as $item){
            $subtotal += $item["subtotal"];
            foreach($item["taxes"] as $tax){
                $totalTax += $tax["amount"];
            }
            ++$numItems;
        }
        $this->closeCart["subtotal"] = $subtotal;
        $this->closeCart["tax"] = $totalTax;
        $this->closeCart["total"] = $subtotal + $totalTax;
        $this->closeCart["num_items"] = strval($numItems);
    }

    /**
     * Check that closing the cart was successful
     *
     * @param  array $response - response received from API after closing the cart
     * @param  array $saleData - must contain the previous sale_id and the previous sale number
     */
    private function CheckForSuccess($response, $saleData){
        codecept_debug("\n* Checking API response for success");

        //get codeception modules
        $SalesV2ApiHelper = $this->getModule('Salesv2apiHelper');
        $Db = $this->getModule('Db');

        //get sale id and number fields to be used in $expectedCartResponse
        $new_sale_id = $Db->grabFromDatabase('foreup_sales', 'max(sale_id)');
        $new_number = $Db->grabFromDatabase('foreup_sales', 'max(number)');

        //Check that the cart successfully closed
        $expectedCartResponse = $this->closeCartResponse;
        $expectedCartResponse["sale_id"] = intval($new_sale_id);
        $expectedCartResponse["number"] = intval($new_number);
        $expectedCartResponse["cart_id"] = $this->cart_id + 1;

        if(isset($saleData['Loyalty Points']) && $saleData['Loyalty Points'] === true){
            $amount = $Db->grabFromDatabase('foreup_sales_payments', 'payment_amount', array('sale_id' => $new_sale_id, 'payment_type' => 'Loyalty'));
            $pointsUsed = $this->paymentObj->_getPointsUsed($amount);

            if($SalesV2ApiHelper->isWholeNumber($pointsUsed)){
                $pointsUsed = intval($pointsUsed);
            }
            $expectedCartResponse["loyalty_points_spent"] = $pointsUsed;
            $expectedCartResponse["loyalty_points_earned"] = 0;
        }

        $SalesV2ApiHelper->checkAPIForSuccess($expectedCartResponse, $response);

        //check that the inventory is updated
        $saleID = $saleData["sale_id"];

        foreach ($saleData['items'] as $item) {
            //check that the inventory is updated
            $quantity = $this->closeCart["items"][$item]["quantity"];
            $subtotal = $this->closeCart["items"][$item]["subtotal"];

            if($item !== 'Green Fee') {
                $db_items = [
                    'item_id' => $this->closeCart["items"][$item]["item_id"],
                    'quantity' => ($this->closeCart["items"][$item]["inventory_level"] - $quantity)
                ];
                $Db->seeInDatabase('foreup_items', $db_items);
            }
            $db_sales_items = [
                'sale_id' => $saleID,
                'item_id' => $this->closeCart["items"][$item]["item_id"],
                'quantity_purchased' => $quantity,
                'subtotal' => $subtotal,
                'tax' => $this->closeCart["items"][$item]["tax"],
                'total' => $this->closeCart["items"][$item]["total"]
            ];
            if(isset($saleData['Loyalty Points']) && $saleData['Loyalty Points'] === true){
                $db_sales_items['tax'] = 0;
                $db_sales_items['total'] = $subtotal;
            }
            $Db->seeInDatabase('foreup_sales_items', $db_sales_items);
        }

        //check that the sales table is updated
        $db_sales = [
            'sale_id' => $saleID,
            'number' => $saleData["number"],
            'payment_type' => $this->closeCart["payments"][0]["description"].': $'.$this->closeCart["payments"][0]["amount"].'<br />'
        ];
        if(isset($saleData['Loyalty Points']) && $saleData['Loyalty Points'] === true){
            $amount = $this->closeCart["payments"][0]["amount"];
            $db_sales['payment_type'] = 'Loyalty: $'.number_format($amount, 2).'<br />';
        }

        $Db->seeInDatabase('foreup_sales', $db_sales);


        codecept_debug("");
    }
}
