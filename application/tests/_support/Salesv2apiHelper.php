<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Salesv2apiHelper extends \Codeception\Module
{
    var $new_cart_id;
    var $course_id = 6270;
    var $items;
    var $DrPepper;
    var $DrPepper_item_number = null;
    var $DrPepper_item_id;
    var $DrPepper_item_type = 'item';
    var $DrPepper_inventory_level = 200000;
    var $Person_sup;
    var $Supplier;
    var $Supplier_id;
    var $TestItems;

    public function setItemsInfo(){
        $this->items = [
            'Dr Pepper' => [
              'course_id' => $this->course_id,
              'CID' => 0,
              'name' => 'Dr. Pepper',
              'department' => 'Food & Beverage',
              'category' => 'Beverage',
              'supplier_id' => $this->Supplier_id,
              'image_id' => 0,
              'cost_price' => 0.50,
              'unit_price' => 2.00,
              'unit_price_includes_tax' => 0,
              'quantity' => $this->DrPepper_inventory_level,
              'is_unlimited' => 1,
              'location' => 0,
              'allow_alt_description' => 0,
              'is_serialized' => 0,
              'is_giftcard' => 0,
              'invisible' => 0,
              'soup_or_salad' => 'none',
              'is_side' => 0,
              'add_on_price' => 0,
              'print_priority' => 1,
              'quickbooks_income' => 0,
              'quickbooks_cogs' => 0,
              'quickbooks_assets' => 0,
              'erange_size' => 0
          ],

        ];
    }

    public function login($username, $password)
    {
        $browser = $this->getModule('PhpBrowser');
        $browser->amOnPage('/index.php/login');
        $browser->fillField('username', $username);
        $browser->fillField('password', $password);
        $browser->click('login_button');
    }

    //expectedResponse must be ordered exactly the same as the API response or the test will fail
    public function CheckAPIResponseForChanges($actualResponse, $expectedResponse){
        $actualResponse = json_decode($actualResponse);
        $actualResponse = json_encode($actualResponse);
        $expectedResponse = json_encode($expectedResponse);
        if($actualResponse != $expectedResponse){
            codecept_debug("\nAPI Response Has Changed\n--Expected\n++Actual\n\t--$expectedResponse\n\t++$actualResponse\n");
        }
    }

    public function addItemToCart($itemName){
        $api = $this->getModule('REST');
        $api->sendPUT();
    }

    public function checkAPIForSuccess($expectedResponse, $actualResponse){
        $api = $this->getModule('REST');

        //check to see if the amount paid is an even dollar amount
        //if it is the API will return it as an int so $expectedResponse['amount']
        //needs to be cast to an int
        if(isset($expectedResponse['amount'])) {
            $wholeValue = intval($expectedResponse['amount']);
            if($this->isWholeNumber($expectedResponse['amount']) === true){
                $expectedResponse['amount'] = $wholeValue;
            }
        }

        //check if API response has changed since the last git push.
        //result is silent unless test is run in debug mode
        $this->CheckAPIResponseForChanges($actualResponse, $expectedResponse);

        //check that response matches what was expected
        $api->seeResponseContainsJson($expectedResponse);
        $api->seeResponseCodeIs(200);
    }

    public function isWholeNumber($value){
        $wholeValue = intval($value);
        $remainder = $value - $wholeValue;
        if($remainder == 0){
            return true;
        }
        return false;
    }
}