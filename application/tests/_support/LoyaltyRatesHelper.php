<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class LoyaltyRatesHelper extends \Codeception\Module
{
    private $loyaltyRate = [
        'course_id' => 6270,
        'label' => 'Loyalty Test',
        'type' => 'item',
        'value' => NULL,
        'value_label' => NULL,
        'points_per_dollar' => 50,
        'dollars_per_point' => 1,
        'tee_time_index' => 3,
        'price_category' => 1
        ];
    function __construct($itemID){
        parent::__construct();

        $this->loyaltyRate['value'] = strval($itemID);
        $this->loyaltyRate['value_label'] = strval($itemID);
    }

    public function _createLoyaltyRate(){
        $Db = $this->getModule('Db');
        $DbHelper = $this->getModule('DbHelper');

        codecept_debug("\n* Creating Loyalty Rate '".$this->loyaltyRate['label']."'");

        $DbHelper->InsertAndUpdateOnDuplicate('foreup_loyalty_rates', $this->loyaltyRate);
        $this->loyaltyRate['loyalty_rate_id'] = $Db->grabFromDatabase('foreup_loyalty_rates', 'loyalty_rate_id', $this->loyaltyRate);
    }

    public function _getPaymentDescription(){
        return "Loyalty Points";
    }

    public function _getPaymentType(){
        return "loyalty_points";
    }

    public function _getPointsUsed($amount){
        return ($amount/$this->loyaltyRate['dollars_per_point']) * 100;
    }
}
