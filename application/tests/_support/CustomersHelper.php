<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class CustomersHelper extends \Codeception\Module
{
    private $customer_data = [
        'CID' => 0,
        'person_id' => 0,
        'course_id' => 0,
        'api_id' => 0,
        'username' => '',
        'password' => '',
        'member' => 1,
        'price_class' => '1',
        'image_id' => 0,
        'account_number' => NULL,
        'account_balance' => 0.00,
        'account_balance_allow_negative' => 1,
        'account_limit' => -1000.00,
        'member_account_balance' => 0.00,
        'member_account_balance_allow_negative' => 1,
        'member_account_limit' => -1000.00,
        'invoice_balance' => 0,
        'invoice_balance_allow_negative' => 0,
        'invoice_email' => "",
        'use_loyalty' => 1,
        'loyalty_points' => 1000,
        'company_name' => "",
        'taxable' => 0,
        'discount' => "",
        'deleted' => 0,
        'opt_out_email' => 0,
        'opt_out_text' => 0,
        'unsubscribe_all' => 0,
        'course_news_announcements' => 0,
        'teetime_reminders' => 0,
        'status_flag' => 0,
        'require_food_minimum' => 0,
        'hide_from_search' => 0
    ];

    private $API_Info_AddCustomerToCart = [
        "person_id" => 0,
        "selected" => true,
        "taxable" => true,
        "discount" => "0.00"
    ];
    private $person;

    function __construct($course_id, array $info = null){
        parent::__construct();

        $this->person = new PersonHelper($info);
        $this->person->_createPerson();

        //set customer info
        $this->customer_data['course_id'] = $course_id;
        $this->customer_data['person_id'] = $this->person->_getPersonID();

        //set info for adding customer to a cart
        $this->API_Info_AddCustomerToCart['person_id'] = $this->customer_data['person_id'];
        $this->API_Info_AddCustomerToCart['taxable'] = boolval($this->customer_data['taxable']);

        //merge user supplied data
        if($info !== null) {
            $this->customer_data = array_merge($this->customer_data, array_intersect_key($info, $this->customer_data));
        }
    }

    public function _createCustomer(){
        $Db = $this->getModule('Db');
        $DbHelper = $this->getModule('DbHelper');
        codecept_debug("\n* Creating Customer ".$this->person->_getFullName());

        $DbHelper->InsertAndUpdateOnDuplicate('foreup_customers', $this->customer_data);
        $this->customer_data['customer_id'] = $Db->grabFromDatabase('foreup_customers', 'customer_id', $this->customer_data);
    }

    public function _addCustomerToCart_API($selected = true, $discount = "0.00"){
        $Rest = $this->getModule('REST');
        codecept_debug("\n* Adding ".$this->person->_getFullName(). " to the cart using the API");

        $this->API_Info_AddCustomerToCart['selected'] = $selected;
        $this->API_Info_AddCustomerToCart['discount'] = $discount;

        //add customer to the cart
        $Rest->sendPUT('/index.php/api/cart/customers/'.$this->person->_getPersonID(), json_encode($this->API_Info_AddCustomerToCart));
        return $Rest->grabResponse();
    }

    public function _getID(){
        return $this->customer_data['person_id'];
    }

    public function _getBeginningLoyaltyPoints(){
        return $this->customer_data['loyalty_points'];
    }

    public function _updateLoyaltyPoints($points){
        $DbHelper = $this->getModule('DbHelper');
        $DbHelper->updateDatabase('foreup_customers', array('person_id' => $this->customer_data['person_id']), array('loyalty_points' => $points));
    }
}
