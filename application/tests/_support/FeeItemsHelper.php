<?php
namespace Codeception\Module;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class FeeItemsHelper extends \Codeception\Module
{
    private $FeeItems = [];
    private $course_info = [
        'course_id' => 6270,
        'teesheet_id' => 26,
        'price_class' => [
            'course_id' => '',
            'name' => 'PC Fee Tester',
            'color' => '#6ea4e1'
        ],
        'season_info' => [
            'course_id' => '',
            'teesheet_id' => '',
            'season_name' => 'Green Fee Tester',
            'start_date' => '0000-01-01',
            'end_date' => '0000-12-31',
            'holiday' => 0,
            'increment' => 0.00,
            'online_booking' => 0,
            'online_open_time' => 0,
            'online_close_time' => 0,
            'deleted' => 0
        ],
        'season_timeframes' => [
            'class_id' => '',
            'season_id' => '',
            'timeframe_name' => 'Test Fees',
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'price1' => 199.99,
            'price2' => 99.99,
            'start_time' => 0,
            'end_time' => 2400
        ]
    ];
    private $fee_item_id = 5274;
    private $fee_item_name = 'Green Fee';
    private $holes;


    /*
     * Codeception doesn't allow the constructor to call functions that make
     * references to outside modules like Db, DbHelper, or REST modules.
     * When Creating an Items object your tests need to call _addItemsToDatabase();
     * to finish setting up items they will be using.
    */
    function __construct($course_id, $holes = 18)
    {
        parent::__construct();
        $this->course_info['course_id'] = $course_id;
        $this->holes = $holes;
    }


    /**
     * Add price class season and timeframes to the
     * database so fees can be added to the cart
     *
     * @return null  no return value
     */
    public function _createFees(){
        codecept_debug("\n* Adding price class \"" . $this->course_info['price_class']['name'] . "\" to the database");
        $this->course_info['price_class']['course_id'] = $this->course_info['course_id'];

        //add price class to database
        $this->course_info['price_class']["class_id"] = $this->addToDatabase('foreup_price_classes', 'class_id', $this->course_info['price_class']);


        //add course info to season
        codecept_debug("\n* Adding season \"" . $this->course_info['season_info']['season_name'] . "\" to the database");
        $this->course_info['season_info']['course_id'] = $this->course_info['course_id'];
        $this->course_info['season_info']['teesheet_id'] = $this->course_info['teesheet_id'];

        //add season to database
        $this->course_info['season_info']["season_id"] = $this->addToDatabase('foreup_seasons', 'season_id', $this->course_info['season_info']);


        //add season and price class info to timeframes
        codecept_debug("\n* Adding timeframe \"" . $this->course_info['season_timeframes']['timeframe_name'] . "\" to the database");
        $this->course_info['season_timeframes']['class_id'] = $this->course_info['price_class']["class_id"];
        $this->course_info['season_timeframes']['season_id'] = $this->course_info['season_info']["season_id"];

        //add season to database
        $this->course_info['season_timeframes']["timeframe_id"] = $this->addToDatabase('foreup_seasonal_timeframes', 'timeframe_id', $this->course_info['season_timeframes']);
    }


    /**
     * adds an array of values to a specified table
     *
     * @param  string $table - name of the table in the database
     * @param  string $primaryKey - name of the tables primary key
     * @param  array $data - data to be placed in the table
     * @return int - primary key
     */
    private function addToDatabase($table, $primaryKey, array $data){
        //get connection to database
        $DbHelper = $this->getModule('DbHelper');
        $Db = $this->getModule('Db');

        //add data to database
        $DbHelper->InsertAndUpdateOnDuplicate($table, $data);

        //get season id from database
        return $Db->grabFromDatabase($table, 'max('.$primaryKey.')', $data);
    }

    public function _getPriceClassID(){
        return $this->course_info['price_class']['class_id'];
    }

    public function _getPriceClassName(){
        return $this->course_info['price_class']['name'];
    }

    public function _getTimeframeID(){
        return $this->course_info['season_timeframes']['timeframe_id'];
    }

    public function _getParams(){
        return '{"teesheet_id":'.$this->course_info['teesheet_id'].',"holes":18,"season_id":'.$this->course_info['season_info']['season_id'].'}';
    }

    public function _getUnitPriceIncludesTax(){
        $Db = $this->getModule('Db');
        return $Db->grabFromDatabase('foreup_items', 'unit_price_includes_tax', array('item_id' => $this->fee_item_id));
    }

    public function _getInventoryLevel(){
        $Db = $this->getModule('Db');
        return $Db->grabFromDatabase('foreup_items', 'quantity', array('item_id' => $this->fee_item_id));
    }

    public function _getReorderLevel(){
        $Db = $this->getModule('Db');
        return $Db->grabFromDatabase('foreup_items', 'reorder_level', array('item_id' => $this->fee_item_id));
    }

    public function _getName(){
        return "18 - ".$this->course_info['price_class']['name']." ".$this->course_info['season_timeframes']['timeframe_name'];
    }

    public function _getDepartment(){
        $Db = $this->getModule('Db');
        return $Db->grabFromDatabase('foreup_items', 'department', array('item_id' => $this->fee_item_id));
    }

    public function _getCategory(){
        $Db = $this->getModule('Db');
        return $Db->grabFromDatabase('foreup_items', 'category', array('item_id' => $this->fee_item_id));
    }

    public function _getItemNumber(){
        $Db = $this->getModule('Db');
        return $Db->grabFromDatabase('foreup_items', 'item_number', array('item_id' => $this->fee_item_id));
    }

    public function _getCostPrice(){
        return 0.00;
    }

    public function _getUnitPrice(){
        if($this->holes === 18) {
            return $this->course_info['season_timeframes']['price1'];
        }else{
            return $this->course_info['season_timeframes']['price2'];
        }
    }

    public function _getItemType(){
        return 'green_fee';
    }

    public function _getTaxInfo(){
        $Db = $this->getModule('Db');
        $course_id = $this->course_info['course_id'];
        $CID = $Db->grabFromDatabase('foreup_items_taxes', 'CID', array('item_id' => $this->fee_item_id));
        $name = $Db->grabFromDatabase('foreup_items_taxes', 'name', array('item_id' => $this->fee_item_id));
        $percent = (double) $Db->grabFromDatabase('foreup_items_taxes', 'percent', array('item_id' => $this->fee_item_id));
        $cumulative = $Db->grabFromDatabase('foreup_items_taxes', 'cumulative', array('item_id' => $this->fee_item_id));
        return array(array('course_id' => $course_id, 'CID' => $CID, 'name' => $name, 'percent' => $percent, 'cumulative' => $cumulative));
    }

    public function _getItemID(){
        return $this->fee_item_id;
    }

    public function _getTeeSheetID(){
        return $this->course_info['teesheet_id'];
    }

    public function _getTeeSheetName(){
        $Db = $this->getModule('Db');
        return $Db->grabFromDatabase('foreup_teesheet', 'title', array('teesheet_id' => $this->course_info['teesheet_id']));
    }

    public function _getFeeItemLabel(){
        return $this->fee_item_name;
    }

    public function _getSeasonName(){
        return $this->course_info['season_info']['season_name'];
    }

    public function _getSeasonID(){
        return $this->course_info['season_info']['season_id'];
    }

    public function _getNumHoles(){
        return $this->holes;
    }
}
