<?php

/*
 * To check if the API responses have changed recently turn debug mode on by adding the -d command when running the test
 * format of notice that API response has changed:
 *
 *      Responses are NOT equal
 *      --Expected
 *      ++Actual
 *          ++<new response message>
 *          --<old response message>
 *
 */

class IssueRaincheckCest
{

    //var $orig_last_raincheck_id;

    var $rainCheck;
    var $rainCheck_id;
    var $rainCheck_cart_fee = 50.00;
    var $rainCheck_number = 10000000;
    var $rainCheck_issue_date = '2015-07-04';
    var $rainCheck_date_redeemed = '0000-00-00';

    var $Green_Fee;
    var $Green_Fee_taxes;
    var $Green_Fee_inventory = 10000;
    var $Green_Fee_type = 'green_fee';
    var $Green_Fee_item_id;
    var $Green_Fee_Season = 21;
    var $Green_Fee_amount = 53.38;
    var $Green_Fee_subtotal = 50.00;
    var $Green_Fee_tax;
    var $Green_Fee_subtotal_str = '50.00';

    var $course_id = 6270;

    var $Cart;
    var $Cart_taxes;
    var $Cart_item_id;
    var $Cart_inventory = 10000;
    var $Cart_total = 53.38;
    var $Cart_subtotal = 50.00;
    var $Cart_tax;
    var $Cart_subtotal_str = '50.00';
    var $Cart_Green_Fee_Season = 20;
    var $Cart_type = 'cart_fee';

    var $customer;
    var $customer_info;
    var $customer_loyalty_points = 20000;
    var $customer_id = 1000000;

    public function _before(Salesv2apiGuy $I)
    {
        $I->login('jackbarnes', 'jackbarnes');
        $I->setHeader('Content-Type', 'application/json');
        $I->setHeader('Api-key', 'no_limits');

        /*$this->rainCheck = [
            'raincheck_number' => $this->rainCheck_number,
            'teesheet_id' => 27,
            'date_issued' => $this->rainCheck_issue_date,
            'date_redeemed' => $this->rainCheck_date_redeemed,
            'players' => 1,
            'holes_completed' => 0,
            'customer_id' => 0,
            'employee_id' => 12,
            'green_fee' => 0.00,
            'cart_fee' => $this->rainCheck_cart_fee,
            'tax' => ($this->Cart_total - $this->rainCheck_cart_fee),
            'total' => $this->Cart_total,
            'cart_price_category' => '1_1_'.$this->Cart_Green_Fee_Season
        ];*/
        /*$I->InsertAndUpdateOnDuplicate('foreup_rainchecks', $this->rainCheck);*/
        $this->rainCheck_id = $I->grabFromDatabase('foreup_rainchecks', 'max(raincheck_id)') + 1;

        $this->rainCheck_number = $I->grabFromDatabase('foreup_rainchecks', 'max(raincheck_number)') + 1;
        $this->Cart_tax = $this->Cart_total - $this->Cart_subtotal;
        $this->Green_Fee_tax = $this->Green_Fee_amount - $this->Green_Fee_subtotal;

        $this->Green_Fee = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'name' => '18 Hole Green',
            'category' => 'Green Fees',
            'item_number' => $this->course_id.'_seasonal_'.$this->Green_Fee_Season,
            'image_id' => 0,
            'cost_price' => $this->Green_Fee_subtotal,
            'unit_price' => $this->Green_Fee_subtotal,
            'unit_price_includes_tax' => 0,
            'max_discount' => 10,
            'quantity' => $this->Green_Fee_inventory,
            'is_unlimited' => 1,
            'allow_alt_description' => 0,
            'is_serialized' => 0,
            'is_giftcard' => 0,
            'invisible' => 0,
            'soup_or_salad' => 'none',
            'is_side' => 0,
            'add_on_price' => 0,
            'print_priority' => 0,
            'erange_size' => 0
        ];

        $this->Cart = [
            'course_id' => $this->course_id,
            'CID' => 0,
            'name' => '18 Hole Cart',
            'category' => 'Carts',
            'item_number' => $this->course_id.'_seasonal_'.$this->Cart_Green_Fee_Season,
            'image_id' => 0,
            'cost_price' => $this->Cart_subtotal,
            'unit_price' => $this->Cart_subtotal,
            'unit_price_includes_tax' => 0,
            'max_discount' => 10,
            'quantity' => $this->Cart_inventory,
            'is_unlimited' => 1,
            'allow_alt_description' => 0,
            'is_serialized' => 0,
            'is_giftcard' => 0,
            'invisible' => 0,
            'soup_or_salad' => 'none',
            'is_side' => 0,
            'add_on_price' => 0,
            'print_priority' => 0,
            'erange_size' => 0
        ];

        $this->customer = [
            'CID' => 0,
            'person_id' => $this->customer_id,
            'course_id' => $this->course_id,
            'api_id' => 0,
            'username' => '',
            'password' => '',
            'member' => 0,
            'price_class' => 1,
            'image_id' => 0,
            'account_number' => '10231352102135',
            'account_balance' => 0.00,
            'account_balance_allow_negative' => 0,
            'account_limit' => 0.00,
            'member_account_balance' => 0.00,
            'member_account_limit' => 0.00,
            'invoice_balance' => 0.00,
            'invoice_email' => '',
            'use_loyalty' => 1,
            'loyalty_points' => $this->customer_loyalty_points,
            'company_name' => '',
            'discount' => 0.00,
            'deleted' => 0,
            'opt_out_email' => 1,
            'opt_out_text' => 1,
            'unsubscribe_all' => 0,
            'course_news_announcements' => 0,
            'teetime_reminders' => 0,
            'status_flag' => 3,
            'require_food_minimum' => 0
        ];

        $this->customer_info = [
            'first_name' => 'Tiger',
            'last_name' => 'Woods',
            'phone_number' => '',
            'cell_phone_number'=> 8014567123,
            'email' => 'woods2@gmail.com',
            'birthday' => '1980-07-24',
            'address_1' => '1546 E Flash Drive',
            'address_2' => '',
            'city' => 'Lehi',
            'state' => 'UT',
            'zip' => '84043',
            'country' => '',
            'comments' => '',
            'person_id' => $this->customer_id,
            'foreup_news_announcements_unsubscribe' => 0
        ];

        $I->InsertAndUpdateOnDuplicate('foreup_items', $this->Green_Fee);
        $this->Green_Fee_item_id = $I->grabFromDatabase('foreup_items', 'max(item_id)');
        $this->Green_Fee_taxes = array('course_id' => $this->course_id, 'CID' => 0, 'item_id' => $this->Green_Fee_item_id, 'name' => 'Sales Tax', 'percent' => 6.750, 'cumulative' => 0);
        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', $this->Green_Fee_taxes);

        $I->InsertAndUpdateOnDuplicate('foreup_items', $this->Cart);
        $this->Cart_item_id = $I->grabFromDatabase('foreup_items', 'max(item_id)');
        $this->Cart_taxes = array('course_id' => $this->course_id, 'CID' => 0, 'item_id' => $this->Cart_item_id, 'name' => 'Sales Tax', 'percent' => 6.750, 'cumulative' => 0);
        $I->InsertAndUpdateOnDuplicate('foreup_items_taxes', $this->Cart_taxes);

        $I->InsertAndUpdateOnDuplicate('foreup_people', $this->customer_info);
        $I->InsertAndUpdateOnDuplicate('foreup_customers', $this->customer);

    }

    public function _after(Salesv2apiGuy $I)
    {
    }

    //This should be the first function run after _before()
    /*public function cleanupSetup(Salesv2apiGuy $I){
        //This is used in the cleanup() function
        $this->orig_last_raincheck_id = $I->grabFromDatabase('foreup_rainchecks', 'LAST_INSERT_ID()');
    }*/

    // tests
    public function issueBasicGreenFeeRaincheck(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes = 18;
        $holes_completed = 0;
        $quantity = 1;

        $I->wantTo('issue a basic raincheck with a green fee on it');

        $payload = [
            "teesheet_id" => 27,
            "green_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => $this->Green_Fee_type,
                "holes" => $holes,
                "unit_price" => $this->Green_Fee['unit_price'],
                "base_price" => $this->Green_Fee['unit_price'],
                "name" => $this->Green_Fee['name'],
                "item_id" => $this->Green_Fee_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => ($this->Green_Fee_amount - $this->Green_Fee_subtotal)
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => $this->Green_Fee_amount
            ],
            "cart_fee" => null,
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => null,
            "expiry_date" => null
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => true,
            "raincheck_id" => $this->rainCheck_id,
            "raincheck_number" => $this->rainCheck_number
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $db_rainchecks = [
            'raincheck_id' => $this->rainCheck_id,
            'date_redeemed' => '0000-00-00 00:00:00',
            'players' => $players,
            'holes_completed' => $holes_completed,
            'expiry_date' => '0000-00-00 00:00:00',
            'green_fee' => number_format($this->Green_Fee_subtotal, 2)
        ];
        $I->seeInDatabase('foreup_rainchecks', $db_rainchecks);
    }

    public function badRequest(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes_completed = 0;

        $I->wantTo('confirm that an invalid request is recognized as such');

        $payload = [
            "teesheet_id" => null,
            "green_fee" => null,
            "cart_fee" => null,
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => null,
            "expiry_date" => null
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => false
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(400);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);
    }

    public function issueBasicCartFeeRaincheck(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes = 18;
        $holes_completed = 0;
        $quantity = 1;

        $I->wantTo('issue a basic raincheck with a cart fee on it');

        $payload = [
            "teesheet_id" => 27,
            "green_fee" => null,
            "cart_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => "cart_fee",
                "holes" => $holes,
                "unit_price" => $this->Cart['unit_price'],
                "base_price" => $this->Cart['unit_price'],
                "name" => $this->Cart['name'],
                "item_id" => $this->Cart_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $this->Cart_tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => $this->Cart_total
            ],
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => null,
            "expiry_date" => null
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => true,
            "raincheck_id" => $this->rainCheck_id,
            "raincheck_number" => $this->rainCheck_number
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $db_rainchecks = [
            'raincheck_id' => $this->rainCheck_id,
            'date_redeemed' => '0000-00-00 00:00:00',
            'players' => $players,
            'holes_completed' => $holes_completed,
            'cart_fee' => number_format($this->Cart_subtotal, 2),
            'expiry_date' => '0000-00-00 00:00:00'
        ];
        $I->seeInDatabase('foreup_rainchecks', $db_rainchecks);
    }

    public function issueGreenAndCartFeeRaincheck(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes = 18;
        $holes_completed = 0;
        $quantity = 1;

        $I->wantTo('issue a basic raincheck with a cart fee and green fee on it');

        $payload = [
            "teesheet_id" => 27,
            "green_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => $this->Green_Fee_type,
                "holes" => $holes,
                "unit_price" => $this->Green_Fee['unit_price'],
                "base_price" => $this->Green_Fee['unit_price'],
                "name" => $this->Green_Fee['name'],
                "item_id" => $this->Green_Fee_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $this->Green_Fee_tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => $this->Green_Fee_amount
            ],
            "cart_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => "cart_fee",
                "holes" => $holes,
                "unit_price" => $this->Cart['unit_price'],
                "base_price" => $this->Cart['unit_price'],
                "name" => $this->Cart['name'],
                "item_id" => $this->Cart_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $this->Cart_tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => $this->Cart_total
            ],
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => null,
            "expiry_date" => null
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => true,
            "raincheck_id" => $this->rainCheck_id,
            "raincheck_number" => $this->rainCheck_number
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $db_rainchecks = [
            'raincheck_id' => $this->rainCheck_id,
            'date_redeemed' => '0000-00-00 00:00:00',
            'players' => $players,
            'holes_completed' => $holes_completed,
            'expiry_date' => '0000-00-00 00:00:00',
            'green_fee' => number_format($this->Green_Fee_subtotal, 2),
            'cart_fee' => number_format($this->Cart_subtotal, 2)
        ];
        $I->seeInDatabase('foreup_rainchecks', $db_rainchecks);
    }

    public function issueGreenFeeRaincheckWithCustomer(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes = 18;
        $holes_completed = 0;
        $quantity = 1;

        $I->wantTo('issue a raincheck with a green fee and customer attached to it');

        $payload = [
            "teesheet_id" => 27,
            "green_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => $this->Green_Fee_type,
                "holes" => $holes,
                "unit_price" => $this->Green_Fee['unit_price'],
                "base_price" => $this->Green_Fee['unit_price'],
                "name" => $this->Green_Fee['name'],
                "item_id" => $this->Green_Fee_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $this->Green_Fee_tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => $this->Green_Fee_amount
            ],
            "cart_fee" => null,
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => $this->customer_id,
            "expiry_date" => null
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => true,
            "raincheck_id" => $this->rainCheck_id,
            "raincheck_number" => $this->rainCheck_number
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $db_rainchecks = [
            'raincheck_id' => $this->rainCheck_id,
            'date_redeemed' => '0000-00-00 00:00:00',
            'players' => $players,
            'holes_completed' => $holes_completed,
            'customer_id' => $this->customer_id,
            'expiry_date' => '0000-00-00 00:00:00',
            'green_fee' => number_format($this->Green_Fee_subtotal, 2)
        ];
        $I->seeInDatabase('foreup_rainchecks', $db_rainchecks);
    }

    public function issueCartFeeRaincheckWithCustomer(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes = 18;
        $holes_completed = 0;
        $quantity = 1;

        $I->wantTo('issue a raincheck with a cart fee and customer attached to it');

        $payload = [
            "teesheet_id" => 27,
            "green_fee" => null,
            "cart_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => $this->Cart_type,
                "holes" => $holes,
                "unit_price" => $this->Cart['unit_price'],
                "base_price" => $this->Cart['unit_price'],
                "name" => $this->Cart['name'],
                "item_id" => $this->Cart_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $this->Cart_tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => $this->Cart_total
            ],
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => $this->customer_id,
            "expiry_date" => null
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => true,
            "raincheck_id" => $this->rainCheck_id,
            "raincheck_number" => $this->rainCheck_number
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $db_rainchecks = [
            'raincheck_id' => $this->rainCheck_id,
            'date_redeemed' => '0000-00-00 00:00:00',
            'players' => $players,
            'holes_completed' => $holes_completed,
            'customer_id' => $this->customer_id,
            'cart_fee' => number_format($this->Cart_subtotal, 2),
            'expiry_date' => '0000-00-00 00:00:00'
        ];
        $I->seeInDatabase('foreup_rainchecks', $db_rainchecks);
    }

    public function issueGreenAndCartFeeRaincheckWithCustomer(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes = 18;
        $holes_completed = 0;
        $quantity = 1;

        $I->wantTo('issue a raincheck with a cart fee, green fee, and customer on it');

        $payload = [
            "teesheet_id" => 27,
            "green_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => $this->Green_Fee_type,
                "holes" => $holes,
                "unit_price" => $this->Green_Fee['unit_price'],
                "base_price" => $this->Green_Fee['unit_price'],
                "name" => $this->Green_Fee['name'],
                "item_id" => $this->Green_Fee_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $this->Green_Fee_tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => $this->Green_Fee_amount
            ],
            "cart_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => "cart_fee",
                "holes" => $holes,
                "unit_price" => $this->Cart['unit_price'],
                "base_price" => $this->Cart['unit_price'],
                "name" => $this->Cart['name'],
                "item_id" => $this->Cart_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $this->Cart_tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => $this->Cart_total
            ],
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => $this->customer_id,
            "expiry_date" => null
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => true,
            "raincheck_id" => $this->rainCheck_id,
            "raincheck_number" => $this->rainCheck_number
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $db_rainchecks = [
            'raincheck_id' => $this->rainCheck_id,
            'date_redeemed' => '0000-00-00 00:00:00',
            'players' => $players,
            'holes_completed' => $holes_completed,
            'customer_id' => $this->customer_id,
            'expiry_date' => '0000-00-00 00:00:00',
            'green_fee' => number_format($this->Green_Fee_subtotal, 2),
            'cart_fee' => number_format($this->Cart_subtotal, 2)
        ];
        $I->seeInDatabase('foreup_rainchecks', $db_rainchecks);
    }

    public function issueRaincheckWithExpiration(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes = 18;
        $holes_completed = 0;
        $quantity = 1;
        $expiration_date = date("Y-m-d H:i:s", strtotime("+1 month"));

        $I->wantTo('issue a raincheck with an expiration date');

        $payload = [
            "teesheet_id" => 27,
            "green_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => $this->Green_Fee_type,
                "holes" => $holes,
                "unit_price" => $this->Green_Fee['unit_price'],
                "base_price" => $this->Green_Fee['unit_price'],
                "name" => $this->Green_Fee['name'],
                "item_id" => $this->Green_Fee_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $this->Green_Fee_tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => 0
            ],
            "cart_fee" => null,
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => null,
            "expiry_date" => $expiration_date
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => true,
            "raincheck_id" => $this->rainCheck_id,
            "raincheck_number" => $this->rainCheck_number
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $db_rainchecks = [
            'raincheck_id' => $this->rainCheck_id,
            'date_redeemed' => '0000-00-00 00:00:00',
            'players' => $players,
            'holes_completed' => $holes_completed,
            'expiry_date' => $expiration_date,
            'green_fee' => number_format($this->Green_Fee_subtotal, 2)
        ];
        $I->seeInDatabase('foreup_rainchecks', $db_rainchecks);
    }

    public function splitRaincheck(Salesv2apiGuy $I)
    {
        //TO-DO
        /*
         * This function works through a script on the front end.
         * The Script breaks up the raincheck into rainchecks with
         * a single player on each and makes individual calls to the
         * API for each player.
        */
    }

    public function holesCompletedCartFeeRaincheck(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes = 18;
        $holes_completed = 9;
        $quantity = 1;
        $percentage_complete = $holes_completed/$holes;
        $subtotal = $this->Cart_subtotal * $percentage_complete;
        $tax = $subtotal * $this->Cart_taxes['percent'];
        $amount = $subtotal + $tax;

        $I->wantTo('issue a cart fee raincheck with holes completed');

        $payload = [
            "teesheet_id" => 27,
            "green_fee" => null,
            "cart_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => "cart_fee",
                "holes" => $holes,
                "unit_price" => $this->Cart['unit_price'],
                "base_price" => $this->Cart['unit_price'],
                "name" => $this->Cart['name'],
                "item_id" => $this->Cart_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => $amount
            ],
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => null,
            "expiry_date" => null
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => true,
            "raincheck_id" => $this->rainCheck_id,
            "raincheck_number" => $this->rainCheck_number
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $db_rainchecks = [
            'raincheck_id' => $this->rainCheck_id,
            'date_redeemed' => '0000-00-00 00:00:00',
            'players' => $players,
            'holes_completed' => $holes_completed,
            'cart_fee' => number_format($subtotal, 2),
            'expiry_date' => '0000-00-00 00:00:00'
        ];
        $I->seeInDatabase('foreup_rainchecks', $db_rainchecks);
    }

    public function holesCompletedGreenFeeRaincheck(Salesv2apiGuy $I)
    {
        $players = 1;
        $holes = 18;
        $holes_completed = 9;
        $quantity = 1;
        $percentage_complete = $holes_completed/$holes;
        $subtotal = $this->Cart_subtotal * $percentage_complete;
        $tax = $subtotal * $this->Cart_taxes['percent'];
        $amount = $subtotal + $tax;

        $I->wantTo('issue a cart fee raincheck with holes completed');

        $payload = [
            "teesheet_id" => 27,
            "green_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => $this->Green_Fee_type,
                "holes" => $holes,
                "unit_price" => $this->Green_Fee['unit_price'],
                "base_price" => $this->Green_Fee['unit_price'],
                "name" => $this->Green_Fee['name'],
                "item_id" => $this->Green_Fee_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => $amount
            ],
            "cart_fee" => null,
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => null,
            "expiry_date" => null
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => true,
            "raincheck_id" => $this->rainCheck_id,
            "raincheck_number" => $this->rainCheck_number
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $db_rainchecks = [
            'raincheck_id' => $this->rainCheck_id,
            'date_redeemed' => '0000-00-00 00:00:00',
            'players' => $players,
            'holes_completed' => $holes_completed,
            'expiry_date' => '0000-00-00 00:00:00',
            'green_fee' => number_format($subtotal, 2)
        ];
        $I->seeInDatabase('foreup_rainchecks', $db_rainchecks);
    }

    public function everythingSetRaincheck(Salesv2apiGuy $I)
    {
        //NOTE: this does not test the split raincheck option
        $players = 4;
        $holes = 18;
        $holes_completed = 9;
        $quantity = 1;
        $percentage_complete = $holes_completed/$holes;
        $Cart_fee = $this->Cart_subtotal * $percentage_complete;
        $Green_fee = $this->Green_Fee_subtotal * $percentage_complete;
        $Cart_tax = $Cart_fee * $this->Cart_taxes['percent'];
        $Green_tax = $Green_fee * $this->Green_Fee_taxes['percent'];
        $expiration_date = date("Y-m-d H:i:s", strtotime("+1 month"));

        $I->wantTo('issue a raincheck with all options selected');

        $payload = [
            "teesheet_id" => 27,
            "green_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => $this->Green_Fee_type,
                "holes" => $holes,
                "unit_price" => $this->Green_Fee['unit_price'],
                "base_price" => $this->Green_Fee['unit_price'],
                "name" => $this->Green_Fee['name'],
                "item_id" => $this->Green_Fee_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $Green_tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => 0
            ],
            "cart_fee" => [
                "price_class_id" => 1,
                "price_class_name" => "Regular",
                "timeframe_id" => 1,
                "teesheet_id" => 27,
                "teesheet_name" => "Main course 2",
                "season_id" => 7,
                "special_id" => false,
                "is_today" => true,
                "item_type" => "cart_fee",
                "holes" => $holes,
                "unit_price" => $this->Cart['unit_price'],
                "base_price" => $this->Cart['unit_price'],
                "name" => $this->Cart['name'],
                "item_id" => $this->Cart_item_id,
                "item_number" => 1,
                "taxes" => [
                    [
                        "name" => "Sales Tax",
                        "percent" => 6.75,
                        "cumulative" => 0,
                        "amount" => $Cart_tax
                    ]
                ],
                "quantity" => $quantity,
                "selected" => true,
                "unit_price_includes_tax" => false,
                "max_discount" => 0,
                "type" => "",
                "price" => 0
            ],
            "players" => $players,
            "holes_completed" => $holes_completed,
            "customer_id" => null,
            "expiry_date" => $expiration_date
        ];
        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
        $response = $I->grabResponse();

        $expectedResponse = [
            "success" => true,
            "raincheck_id" => $this->rainCheck_id,
            "raincheck_number" => $this->rainCheck_number
        ];
        $I->seeResponseContainsJson($expectedResponse);
        $I->seeResponseCodeIs(200);

        //check if API response has changed since the last git push
        $I->CheckAPIResponseForChanges($response, $expectedResponse);

        $db_rainchecks = [
            'raincheck_id' => $this->rainCheck_id,
            'date_redeemed' => '0000-00-00 00:00:00',
            'players' => $players,
            'holes_completed' => $holes_completed,
            'expiry_date' => $expiration_date,
            'green_fee' => number_format($Green_fee, 2),
            'cart_fee' => number_format($Cart_fee, 2)
        ];
        $I->seeInDatabase('foreup_rainchecks', $db_rainchecks);
    }

    // test that will fail until a fix is implemented, unless the fix
    // is implemented in the date picker front-end component, in which
    // case this test would still fail. so i guess i'll just comment it
    // out for now.
//    public function timeTravelIsImpossible(Salesv2apiGuy $I)
//    {
//        $I->wantTo('not be able to send expired expiry date (will fail until a fix is implemented)');
//        $payload = '{"teesheet_id":27,"green_fee":{"price_class_id":1,"price_class_name":"Regular","timeframe_id":1,"teesheet_id":27,"teesheet_name":"Main course 2","season_id":7,"special_id":false,"is_today":true,"item_type":"green_fee","holes":18,"unit_price":50,"base_price":50,"name":"18 - Regular ","item_id":"5274","item_number":1,"taxes":[{"name":"Sales Tax","percent":6.75,"cumulative":0,"amount":1.88}],"quantity":1,"selected":true,"unit_price_includes_tax":false,"max_discount":0,"type":"","price":0},"cart_fee":{"price_class_id":1,"price_class_name":"Regular","timeframe_id":14,"teesheet_id":27,"teesheet_name":"Main course 2","season_id":10,"special_id":false,"is_today":true,"item_type":"cart_fee","holes":18,"unit_price":1,"base_price":1,"name":"18 Cart - Regular ","item_id":"5276","item_number":3,"quantity":1,"selected":true,"unit_price_includes_tax":false,"max_discount":0,"type":"","price":0},"players":1,"holes_completed":8,"customer_id":939,"expiry_date":"' . date("Y-m-d H:i:s", strtotime("-1 month")) . '"}';
//        $I->sendPOST('/index.php/api/rainchecks', json_encode($payload));
//        // should be a bad request
//        $I->seeResponseCodeIs(400);
//    }

    /*public function cleanup(Salesv2apiGuy $I)
    {
        //$current_last_cart_id = $I->grabFromDatabase('foreup_pos_cart', 'max(cart_id)');
        $I->deleteGreaterThanFromDatabase('foreup_rainchecks', array('raincheck_id' => $this->orig_last_raincheck_id));
    }*/
}