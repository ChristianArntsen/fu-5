<?php
use \Salesv2apiGuy;

class LoginCest
{
    public function _before(Salesv2apiGuy $I)
    {
    }

    public function _after(Salesv2apiGuy $I)
    {
    }

    public function loginTest(Salesv2apiGuy $I)
    {
        $I->wantTo('test my login function');
        $I->login('jackbarnes', 'jackbarnes');
        $I->canSeeInCurrentUrl('home');
    }
}