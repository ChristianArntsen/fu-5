-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.5.42 - MySQL Community Server (GPL) by Remi
-- Server OS:                    Linux
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for testTable
DROP DATABASE IF EXISTS `testTable`;
CREATE DATABASE IF NOT EXISTS `testTable` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `testTable`;


-- Dumping structure for table testTable.foreup_account_transactions
DROP TABLE IF EXISTS `foreup_account_transactions`;
CREATE TABLE IF NOT EXISTS `foreup_account_transactions` (
  `account_type` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `invoice_id` int(10) unsigned NOT NULL,
  `trans_customer` int(11) NOT NULL DEFAULT '0',
  `trans_household` int(11) NOT NULL,
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_description` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `running_balance` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`trans_id`),
  KEY `foreup_account_transactions_ibfk_1` (`trans_customer`),
  KEY `foreup_account_transactions_ibfk_2` (`trans_user`),
  KEY `sale_id` (`sale_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `sale_id_2` (`sale_id`),
  KEY `invoice_id_2` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_account_transactions: ~0 rows (approximately)
DELETE FROM `foreup_account_transactions`;
/*!40000 ALTER TABLE `foreup_account_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_account_transactions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_actions
DROP TABLE IF EXISTS `foreup_actions`;
CREATE TABLE IF NOT EXISTS `foreup_actions` (
  `action_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `employee_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `action_object_type` varchar(255) NOT NULL,
  `action_object_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_actions: ~0 rows (approximately)
DELETE FROM `foreup_actions`;
/*!40000 ALTER TABLE `foreup_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_actions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_ads
DROP TABLE IF EXISTS `foreup_ads`;
CREATE TABLE IF NOT EXISTS `foreup_ads` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alt_text` text NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `destination_url` varchar(1024) NOT NULL,
  `click_limit` int(10) unsigned DEFAULT NULL,
  `click_total` int(10) unsigned NOT NULL DEFAULT '0',
  `view_limit` int(10) unsigned DEFAULT NULL,
  `view_total` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_ads: ~0 rows (approximately)
DELETE FROM `foreup_ads`;
/*!40000 ALTER TABLE `foreup_ads` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_ads` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_ad_clicks
DROP TABLE IF EXISTS `foreup_ad_clicks`;
CREATE TABLE IF NOT EXISTS `foreup_ad_clicks` (
  `ad_id` int(11) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `campaign_id` int(11) NOT NULL,
  `person_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_ad_clicks: ~0 rows (approximately)
DELETE FROM `foreup_ad_clicks`;
/*!40000 ALTER TABLE `foreup_ad_clicks` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_ad_clicks` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_ad_views
DROP TABLE IF EXISTS `foreup_ad_views`;
CREATE TABLE IF NOT EXISTS `foreup_ad_views` (
  `ad_id` int(11) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `campaign_id` int(11) NOT NULL,
  `person_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_ad_views: ~0 rows (approximately)
DELETE FROM `foreup_ad_views`;
/*!40000 ALTER TABLE `foreup_ad_views` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_ad_views` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_api_keys
DROP TABLE IF EXISTS `foreup_api_keys`;
CREATE TABLE IF NOT EXISTS `foreup_api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table testTable.foreup_api_keys: 0 rows
DELETE FROM `foreup_api_keys`;
/*!40000 ALTER TABLE `foreup_api_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_api_keys` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_api_key_permissions
DROP TABLE IF EXISTS `foreup_api_key_permissions`;
CREATE TABLE IF NOT EXISTS `foreup_api_key_permissions` (
  `api_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `sales` tinyint(4) NOT NULL,
  `extended_permissions` tinyint(4) NOT NULL,
  `tee_time_sales` tinyint(4) NOT NULL,
  UNIQUE KEY `course_id` (`api_id`,`course_id`),
  KEY `api_id` (`api_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_api_key_permissions: ~0 rows (approximately)
DELETE FROM `foreup_api_key_permissions`;
/*!40000 ALTER TABLE `foreup_api_key_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_api_key_permissions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_api_limits
DROP TABLE IF EXISTS `foreup_api_limits`;
CREATE TABLE IF NOT EXISTS `foreup_api_limits` (
  `uri` varchar(255) NOT NULL,
  `api_key` varchar(40) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table testTable.foreup_api_limits: 0 rows
DELETE FROM `foreup_api_limits`;
/*!40000 ALTER TABLE `foreup_api_limits` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_api_limits` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_api_logs
DROP TABLE IF EXISTS `foreup_api_logs`;
CREATE TABLE IF NOT EXISTS `foreup_api_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `params` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `authorized` varchar(255) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_api_logs: ~0 rows (approximately)
DELETE FROM `foreup_api_logs`;
/*!40000 ALTER TABLE `foreup_api_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_api_logs` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_app_files
DROP TABLE IF EXISTS `foreup_app_files`;
CREATE TABLE IF NOT EXISTS `foreup_app_files` (
  `CID` int(11) NOT NULL,
  `file_id` int(10) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_data` longblob NOT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_app_files: ~0 rows (approximately)
DELETE FROM `foreup_app_files`;
/*!40000 ALTER TABLE `foreup_app_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_app_files` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_auto_mailers
DROP TABLE IF EXISTS `foreup_auto_mailers`;
CREATE TABLE IF NOT EXISTS `foreup_auto_mailers` (
  `auto_mailer_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`auto_mailer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_auto_mailers: ~0 rows (approximately)
DELETE FROM `foreup_auto_mailers`;
/*!40000 ALTER TABLE `foreup_auto_mailers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_auto_mailers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_auto_mailer_campaigns
DROP TABLE IF EXISTS `foreup_auto_mailer_campaigns`;
CREATE TABLE IF NOT EXISTS `foreup_auto_mailer_campaigns` (
  `auto_mailer_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `trigger` tinyint(4) NOT NULL,
  `days_from_trigger` tinyint(4) NOT NULL,
  PRIMARY KEY (`auto_mailer_id`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_auto_mailer_campaigns: ~0 rows (approximately)
DELETE FROM `foreup_auto_mailer_campaigns`;
/*!40000 ALTER TABLE `foreup_auto_mailer_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_auto_mailer_campaigns` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_auto_mailer_recipients
DROP TABLE IF EXISTS `foreup_auto_mailer_recipients`;
CREATE TABLE IF NOT EXISTS `foreup_auto_mailer_recipients` (
  `person_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `auto_mailer_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`person_id`,`auto_mailer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_auto_mailer_recipients: ~0 rows (approximately)
DELETE FROM `foreup_auto_mailer_recipients`;
/*!40000 ALTER TABLE `foreup_auto_mailer_recipients` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_auto_mailer_recipients` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_billing
DROP TABLE IF EXISTS `foreup_billing`;
CREATE TABLE IF NOT EXISTS `foreup_billing` (
  `per_day` tinyint(4) NOT NULL,
  `billing_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` mediumint(9) NOT NULL,
  `contact_email` varchar(40) NOT NULL,
  `tax_name` varchar(255) NOT NULL,
  `tax_rate` float(15,3) NOT NULL,
  `product` tinyint(4) NOT NULL,
  `start_date` date NOT NULL,
  `period_start` tinyint(4) NOT NULL,
  `period_end` tinyint(4) NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `email_limit` mediumint(9) NOT NULL,
  `text_limit` mediumint(9) NOT NULL,
  `annual` tinyint(4) NOT NULL,
  `monthly` tinyint(4) NOT NULL,
  `teetimes` tinyint(4) NOT NULL,
  `free` tinyint(4) NOT NULL,
  `annual_amount` float(15,2) NOT NULL,
  `monthly_amount` float(15,2) NOT NULL,
  `teetimes_daily` tinyint(4) NOT NULL,
  `teetimes_weekly` tinyint(4) NOT NULL,
  `annual_month` tinyint(4) NOT NULL,
  `annual_day` tinyint(4) NOT NULL,
  `monthly_day` tinyint(4) NOT NULL,
  `teesheet_id` mediumint(9) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `last_billing_attempt` date NOT NULL,
  `started` tinyint(4) NOT NULL,
  `charged` tinyint(4) NOT NULL,
  `emailed` tinyint(4) NOT NULL,
  PRIMARY KEY (`billing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_billing: ~0 rows (approximately)
DELETE FROM `foreup_billing`;
/*!40000 ALTER TABLE `foreup_billing` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_billing` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_billing_charges
DROP TABLE IF EXISTS `foreup_billing_charges`;
CREATE TABLE IF NOT EXISTS `foreup_billing_charges` (
  `charge_id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_card_id` int(11) NOT NULL,
  `billing_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total` float(15,2) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `credit_card_type` varchar(255) NOT NULL,
  `items` text NOT NULL,
  `success` tinyint(4) NOT NULL,
  PRIMARY KEY (`charge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_billing_charges: ~0 rows (approximately)
DELETE FROM `foreup_billing_charges`;
/*!40000 ALTER TABLE `foreup_billing_charges` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_billing_charges` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_billing_charge_items
DROP TABLE IF EXISTS `foreup_billing_charge_items`;
CREATE TABLE IF NOT EXISTS `foreup_billing_charge_items` (
  `charge_id` int(11) NOT NULL,
  `line_number` tinyint(4) NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` float(15,2) NOT NULL,
  UNIQUE KEY `charge_id` (`charge_id`,`line_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_billing_charge_items: ~0 rows (approximately)
DELETE FROM `foreup_billing_charge_items`;
/*!40000 ALTER TABLE `foreup_billing_charge_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_billing_charge_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_billing_credit_cards
DROP TABLE IF EXISTS `foreup_billing_credit_cards`;
CREATE TABLE IF NOT EXISTS `foreup_billing_credit_cards` (
  `credit_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` mediumint(9) NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_expiration` date NOT NULL,
  `card_type` varchar(255) NOT NULL,
  `cardholder_name` varchar(255) NOT NULL,
  `expiration` date NOT NULL,
  `masked_account` varchar(255) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`credit_card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_billing_credit_cards: ~0 rows (approximately)
DELETE FROM `foreup_billing_credit_cards`;
/*!40000 ALTER TABLE `foreup_billing_credit_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_billing_credit_cards` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_booking_classes
DROP TABLE IF EXISTS `foreup_booking_classes`;
CREATE TABLE IF NOT EXISTS `foreup_booking_classes` (
  `booking_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `teesheet_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price_class` varchar(255) NOT NULL,
  `online_booking_protected` tinyint(4) NOT NULL,
  `require_credit_card` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `online_open_time` varchar(20) NOT NULL,
  `online_close_time` varchar(20) NOT NULL,
  `days_in_booking_window` smallint(6) NOT NULL,
  `minimum_players` tinyint(4) NOT NULL,
  `limit_holes` tinyint(4) NOT NULL,
  `booking_carts` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `show_full_details` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`booking_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_booking_classes: ~0 rows (approximately)
DELETE FROM `foreup_booking_classes`;
/*!40000 ALTER TABLE `foreup_booking_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_booking_classes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_booking_class_groups
DROP TABLE IF EXISTS `foreup_booking_class_groups`;
CREATE TABLE IF NOT EXISTS `foreup_booking_class_groups` (
  `booking_class_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`booking_class_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_booking_class_groups: ~0 rows (approximately)
DELETE FROM `foreup_booking_class_groups`;
/*!40000 ALTER TABLE `foreup_booking_class_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_booking_class_groups` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_communications
DROP TABLE IF EXISTS `foreup_communications`;
CREATE TABLE IF NOT EXISTS `foreup_communications` (
  `type` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recipient_count` mediumint(9) NOT NULL,
  `course_id` int(11) NOT NULL,
  `from` varchar(255) NOT NULL,
  `campaign_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_communications: ~0 rows (approximately)
DELETE FROM `foreup_communications`;
/*!40000 ALTER TABLE `foreup_communications` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_communications` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_courses
DROP TABLE IF EXISTS `foreup_courses`;
CREATE TABLE IF NOT EXISTS `foreup_courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `active_course` tinyint(4) NOT NULL DEFAULT '1',
  `demo` tinyint(4) NOT NULL,
  `clean_out_nightly` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL,
  `area_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `address` varchar(40) NOT NULL,
  `city` varchar(40) NOT NULL,
  `state` varchar(20) NOT NULL,
  `state_name` varchar(50) NOT NULL,
  `postal` varchar(10) NOT NULL,
  `woeid` varchar(25) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `country` varchar(40) NOT NULL,
  `area_code` varchar(10) NOT NULL,
  `FIPS` varchar(20) NOT NULL,
  `MSA` varchar(20) NOT NULL,
  `PMSA` varchar(20) NOT NULL,
  `timezone` varchar(20) NOT NULL,
  `DST` varchar(20) NOT NULL,
  `latitude_centroid` varchar(30) NOT NULL,
  `latitude_poly` varchar(30) NOT NULL,
  `longitude_centroid` varchar(30) NOT NULL,
  `longitude_poly` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `metal_spikes` varchar(10) NOT NULL,
  `play_five` varchar(10) NOT NULL,
  `holes` varchar(10) NOT NULL,
  `type` varchar(10) NOT NULL,
  `year_built` varchar(10) NOT NULL,
  `designer` varchar(30) NOT NULL,
  `season` varchar(30) NOT NULL,
  `GPolicy` varchar(40) NOT NULL,
  `dress_code` varchar(40) NOT NULL,
  `green_fees` varchar(40) NOT NULL,
  `weekend_rates` varchar(40) NOT NULL,
  `AdvTee` varchar(40) NOT NULL,
  `county` varchar(40) NOT NULL,
  `TSID` varchar(20) NOT NULL,
  `APIID` varchar(20) NOT NULL,
  `API` varchar(20) NOT NULL,
  `open_time` varchar(10) NOT NULL DEFAULT '0600',
  `close_time` varchar(10) NOT NULL DEFAULT '1900',
  `increment` varchar(10) NOT NULL DEFAULT '8',
  `frontnine` varchar(10) NOT NULL DEFAULT '200',
  `early_bird_hours_begin` varchar(10) NOT NULL,
  `early_bird_hours_end` varchar(10) NOT NULL,
  `morning_hours_begin` varchar(10) NOT NULL,
  `morning_hours_end` varchar(10) NOT NULL,
  `afternoon_hours_begin` varchar(10) NOT NULL,
  `afternoon_hours_end` varchar(10) NOT NULL,
  `twilight_hour` varchar(10) NOT NULL DEFAULT '1400',
  `super_twilight_hour` varchar(10) NOT NULL DEFAULT '2399',
  `holidays` tinyint(4) NOT NULL,
  `online_booking` tinyint(1) NOT NULL DEFAULT '0',
  `online_booking_protected` tinyint(4) NOT NULL,
  `min_required_players` tinyint(4) NOT NULL DEFAULT '2',
  `min_required_carts` tinyint(4) NOT NULL,
  `min_required_holes` tinyint(4) NOT NULL,
  `booking_rules` text NOT NULL,
  `auto_mailers` tinyint(4) NOT NULL,
  `config` int(1) NOT NULL DEFAULT '1',
  `courses` int(1) NOT NULL DEFAULT '0',
  `customers` int(1) NOT NULL DEFAULT '0',
  `employees` int(1) NOT NULL DEFAULT '0',
  `events` tinyint(4) NOT NULL,
  `food_and_beverage` tinyint(4) NOT NULL,
  `giftcards` int(1) NOT NULL DEFAULT '0',
  `invoices` tinyint(4) NOT NULL,
  `item_kits` int(1) NOT NULL DEFAULT '0',
  `items` int(1) NOT NULL DEFAULT '0',
  `marketing_campaigns` int(1) NOT NULL,
  `marketing_include_ads` tinyint(1) NOT NULL DEFAULT '0',
  `promotions` int(1) NOT NULL DEFAULT '0',
  `quickbooks` tinyint(4) NOT NULL,
  `receivings` int(1) NOT NULL DEFAULT '0',
  `reports` int(1) NOT NULL DEFAULT '0',
  `reservations` tinyint(4) NOT NULL,
  `sales` int(1) NOT NULL DEFAULT '0',
  `sales_v2` tinyint(1) NOT NULL DEFAULT '0',
  `food_and_beverage_v2` tinyint(1) NOT NULL DEFAULT '0',
  `schedules` tinyint(4) NOT NULL DEFAULT '0',
  `suppliers` int(1) NOT NULL DEFAULT '0',
  `teesheets` int(1) NOT NULL DEFAULT '1',
  `tournaments` tinyint(4) NOT NULL DEFAULT '0',
  `dashboards` int(1) NOT NULL,
  `teetimes` int(11) NOT NULL DEFAULT '1',
  `currency_symbol` varchar(3) NOT NULL DEFAULT '$',
  `currency_symbol_placement` varchar(20) NOT NULL DEFAULT 'Front',
  `currency_format` varchar(50) NOT NULL DEFAULT 'America',
  `company_logo` varchar(255) NOT NULL,
  `date_format` varchar(20) NOT NULL DEFAULT 'middle_endian',
  `default_tax_1_name` varchar(255) NOT NULL DEFAULT 'Sales Tax',
  `default_tax_1_rate` varchar(20) NOT NULL,
  `default_tax_2_cumulative` varchar(20) NOT NULL DEFAULT '0',
  `default_tax_2_name` varchar(255) NOT NULL DEFAULT 'Sales Tax 2',
  `default_tax_2_rate` varchar(20) NOT NULL,
  `unit_price_includes_tax` tinyint(4) NOT NULL,
  `customer_credit_nickname` varchar(255) NOT NULL,
  `credit_department` tinyint(4) NOT NULL,
  `credit_department_name` varchar(255) NOT NULL,
  `credit_category` tinyint(4) NOT NULL,
  `credit_category_name` varchar(255) NOT NULL,
  `member_balance_nickname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `billing_email` varchar(255) NOT NULL,
  `last_invoices_sent` date NOT NULL,
  `fax` varchar(40) NOT NULL,
  `language` varchar(20) NOT NULL DEFAULT 'english',
  `mailchimp_api_key` varchar(255) NOT NULL,
  `number_of_items_per_page` varchar(10) NOT NULL DEFAULT '20',
  `hide_back_nine` tinyint(4) NOT NULL,
  `print_after_sale` varchar(10) NOT NULL DEFAULT '0',
  `webprnt` tinyint(4) NOT NULL,
  `webprnt_ip` varchar(20) NOT NULL,
  `webprnt_hot_ip` varchar(30) NOT NULL,
  `webprnt_cold_ip` varchar(30) NOT NULL,
  `webprnt_label_ip` varchar(30) NOT NULL,
  `print_two_receipts` tinyint(4) NOT NULL DEFAULT '0',
  `print_two_signature_slips` tinyint(4) NOT NULL,
  `print_two_receipts_other` tinyint(4) NOT NULL DEFAULT '0',
  `updated_printing` tinyint(4) NOT NULL,
  `after_sale_load` tinyint(4) NOT NULL,
  `fnb_login` tinyint(4) NOT NULL,
  `teesheet_updates_automatically` tinyint(4) NOT NULL,
  `send_reservation_confirmations` tinyint(4) NOT NULL DEFAULT '1',
  `receipt_printer` varchar(255) NOT NULL,
  `print_credit_card_receipt` tinyint(4) NOT NULL DEFAULT '1',
  `print_sales_receipt` tinyint(4) NOT NULL DEFAULT '1',
  `print_tip_line` tinyint(4) NOT NULL DEFAULT '1',
  `cash_drawer_on_cash` tinyint(4) NOT NULL,
  `track_cash` varchar(10) NOT NULL,
  `blind_close` tinyint(4) NOT NULL,
  `separate_courses` tinyint(4) NOT NULL DEFAULT '0',
  `deduct_tips` tinyint(4) NOT NULL,
  `use_terminals` tinyint(4) NOT NULL,
  `use_loyalty` tinyint(4) NOT NULL DEFAULT '0',
  `return_policy` text NOT NULL,
  `time_format` varchar(20) NOT NULL DEFAULT '12_hour',
  `website` varchar(255) NOT NULL,
  `price_category_4` varchar(255) NOT NULL,
  `price_category_5` varchar(255) NOT NULL,
  `price_category_6` varchar(255) NOT NULL,
  `price_category_7` varchar(255) NOT NULL,
  `price_category_8` varchar(255) NOT NULL,
  `price_category_9` varchar(255) NOT NULL,
  `price_category_10` varchar(255) NOT NULL,
  `price_category_11` varchar(255) NOT NULL,
  `price_category_12` varchar(255) NOT NULL,
  `price_category_13` varchar(255) NOT NULL,
  `price_category_14` varchar(255) NOT NULL,
  `price_category_15` varchar(255) NOT NULL,
  `price_category_16` varchar(255) NOT NULL,
  `price_category_17` varchar(255) NOT NULL,
  `price_category_18` varchar(255) NOT NULL,
  `price_category_19` varchar(255) NOT NULL,
  `price_category_20` varchar(255) NOT NULL,
  `price_category_21` varchar(255) NOT NULL,
  `price_category_22` varchar(255) NOT NULL,
  `price_category_23` varchar(255) NOT NULL,
  `price_category_24` varchar(255) NOT NULL,
  `price_category_25` varchar(255) NOT NULL,
  `price_category_26` varchar(255) NOT NULL,
  `price_category_27` varchar(255) NOT NULL,
  `price_category_28` varchar(255) NOT NULL,
  `price_category_29` varchar(255) NOT NULL,
  `price_category_30` varchar(255) NOT NULL,
  `open_sun` tinyint(1) NOT NULL DEFAULT '1',
  `open_mon` tinyint(1) NOT NULL DEFAULT '1',
  `open_tue` tinyint(1) NOT NULL DEFAULT '1',
  `open_wed` tinyint(1) NOT NULL DEFAULT '1',
  `open_thu` tinyint(1) NOT NULL DEFAULT '1',
  `open_fri` tinyint(1) NOT NULL DEFAULT '1',
  `open_sat` tinyint(1) NOT NULL DEFAULT '1',
  `weekend_fri` tinyint(1) NOT NULL DEFAULT '1',
  `weekend_sat` tinyint(1) NOT NULL DEFAULT '1',
  `weekend_sun` tinyint(1) NOT NULL DEFAULT '1',
  `simulator` smallint(1) NOT NULL,
  `test_course` int(1) NOT NULL DEFAULT '0',
  `at_login` varchar(20) NOT NULL,
  `at_password` varchar(20) NOT NULL,
  `at_test` tinyint(1) NOT NULL DEFAULT '1',
  `mercury_id` varchar(255) NOT NULL,
  `mercury_password` varchar(255) NOT NULL,
  `ets_key` varchar(255) NOT NULL,
  `e2e_account_id` varchar(255) NOT NULL,
  `e2e_account_key` varchar(255) NOT NULL,
  `use_ets_giftcards` tinyint(4) NOT NULL,
  `facebook_page_id` varchar(255) NOT NULL,
  `facebook_page_name` varchar(255) NOT NULL,
  `facebook_extended_access_token` varchar(255) NOT NULL,
  `allow_friends_to_invite` tinyint(1) NOT NULL,
  `payment_required` tinyint(1) NOT NULL,
  `send_email_reminder` tinyint(1) NOT NULL,
  `send_text_reminder` tinyint(1) NOT NULL,
  `seasonal_pricing` tinyint(4) NOT NULL,
  `include_tax_online_booking` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `auto_split_teetimes` tinyint(4) NOT NULL,
  `teesheet_refresh_rate` tinyint(4) NOT NULL DEFAULT '90',
  `foreup_discount_percent` double(15,2) NOT NULL DEFAULT '25.00',
  `reservation_email_text` text NOT NULL,
  `reservation_email_photo` int(10) unsigned NOT NULL,
  `no_show_policy` varchar(2048) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ibeacon_major_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ibeacon_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `auto_gratuity` decimal(15,2) NOT NULL,
  `minimum_food_spend` int(11) NOT NULL,
  `hide_employee_last_name_receipt` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `quickbooks_export_settings` text NOT NULL,
  `require_employee_pin` tinyint(4) NOT NULL,
  `erange_id` varchar(255) NOT NULL,
  `erange_password` varchar(255) NOT NULL,
  `credit_card_fee` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `element_account_id` varchar(255) DEFAULT NULL,
  `element_account_token` varchar(255) DEFAULT NULL,
  `element_application_id` varchar(255) DEFAULT NULL,
  `element_acceptor_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_courses: ~3 rows (approximately)
DELETE FROM `foreup_courses`;
/*!40000 ALTER TABLE `foreup_courses` DISABLE KEYS */;
INSERT INTO `foreup_courses` (`course_id`, `active_course`, `demo`, `clean_out_nightly`, `active`, `area_id`, `CID`, `name`, `address`, `city`, `state`, `state_name`, `postal`, `woeid`, `zip`, `country`, `area_code`, `FIPS`, `MSA`, `PMSA`, `timezone`, `DST`, `latitude_centroid`, `latitude_poly`, `longitude_centroid`, `longitude_poly`, `phone`, `metal_spikes`, `play_five`, `holes`, `type`, `year_built`, `designer`, `season`, `GPolicy`, `dress_code`, `green_fees`, `weekend_rates`, `AdvTee`, `county`, `TSID`, `APIID`, `API`, `open_time`, `close_time`, `increment`, `frontnine`, `early_bird_hours_begin`, `early_bird_hours_end`, `morning_hours_begin`, `morning_hours_end`, `afternoon_hours_begin`, `afternoon_hours_end`, `twilight_hour`, `super_twilight_hour`, `holidays`, `online_booking`, `online_booking_protected`, `min_required_players`, `min_required_carts`, `min_required_holes`, `booking_rules`, `auto_mailers`, `config`, `courses`, `customers`, `employees`, `events`, `food_and_beverage`, `giftcards`, `invoices`, `item_kits`, `items`, `marketing_campaigns`, `marketing_include_ads`, `promotions`, `quickbooks`, `receivings`, `reports`, `reservations`, `sales`, `sales_v2`, `food_and_beverage_v2`, `schedules`, `suppliers`, `teesheets`, `tournaments`, `dashboards`, `teetimes`, `currency_symbol`, `currency_symbol_placement`, `currency_format`, `company_logo`, `date_format`, `default_tax_1_name`, `default_tax_1_rate`, `default_tax_2_cumulative`, `default_tax_2_name`, `default_tax_2_rate`, `unit_price_includes_tax`, `customer_credit_nickname`, `credit_department`, `credit_department_name`, `credit_category`, `credit_category_name`, `member_balance_nickname`, `email`, `billing_email`, `last_invoices_sent`, `fax`, `language`, `mailchimp_api_key`, `number_of_items_per_page`, `hide_back_nine`, `print_after_sale`, `webprnt`, `webprnt_ip`, `webprnt_hot_ip`, `webprnt_cold_ip`, `webprnt_label_ip`, `print_two_receipts`, `print_two_signature_slips`, `print_two_receipts_other`, `updated_printing`, `after_sale_load`, `fnb_login`, `teesheet_updates_automatically`, `send_reservation_confirmations`, `receipt_printer`, `print_credit_card_receipt`, `print_sales_receipt`, `print_tip_line`, `cash_drawer_on_cash`, `track_cash`, `blind_close`, `separate_courses`, `deduct_tips`, `use_terminals`, `use_loyalty`, `return_policy`, `time_format`, `website`, `price_category_4`, `price_category_5`, `price_category_6`, `price_category_7`, `price_category_8`, `price_category_9`, `price_category_10`, `price_category_11`, `price_category_12`, `price_category_13`, `price_category_14`, `price_category_15`, `price_category_16`, `price_category_17`, `price_category_18`, `price_category_19`, `price_category_20`, `price_category_21`, `price_category_22`, `price_category_23`, `price_category_24`, `price_category_25`, `price_category_26`, `price_category_27`, `price_category_28`, `price_category_29`, `price_category_30`, `open_sun`, `open_mon`, `open_tue`, `open_wed`, `open_thu`, `open_fri`, `open_sat`, `weekend_fri`, `weekend_sat`, `weekend_sun`, `simulator`, `test_course`, `at_login`, `at_password`, `at_test`, `mercury_id`, `mercury_password`, `ets_key`, `e2e_account_id`, `e2e_account_key`, `use_ets_giftcards`, `facebook_page_id`, `facebook_page_name`, `facebook_extended_access_token`, `allow_friends_to_invite`, `payment_required`, `send_email_reminder`, `send_text_reminder`, `seasonal_pricing`, `include_tax_online_booking`, `auto_split_teetimes`, `teesheet_refresh_rate`, `foreup_discount_percent`, `reservation_email_text`, `reservation_email_photo`, `no_show_policy`, `ibeacon_major_id`, `ibeacon_enabled`, `auto_gratuity`, `minimum_food_spend`, `hide_employee_last_name_receipt`, `quickbooks_export_settings`, `require_employee_pin`, `erange_id`, `erange_password`, `credit_card_fee`, `element_account_id`, `element_account_token`, `element_application_id`, `element_acceptor_id`) VALUES
	(1, 1, 0, 0, 0, 0, 0, 'Test Course', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0600', '1900', '8', '200', '', '', '', '', '', '', '1400', '2399', 0, 0, 0, 2, 0, 0, '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, '$', 'Front', 'America', '', 'middle_endian', 'Sales Tax', '', '0', 'Sales Tax 2', '', 0, '', 0, '', 0, '', '', '', '', '0000-00-00', '', 'english', '', '20', 0, '0', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, '', 1, 1, 1, 0, '', 0, 0, 0, 0, 0, '', '12_hour', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, '', '', 1, '', '', '', '', '', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 90, 25.00, '', 0, '', 0, 0, 0.00, 0, 0, '', 0, '', '', 0.00, NULL, NULL, NULL, NULL),
	(2, 1, 0, 0, 0, 0, 0, 'Course Sharing 1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0600', '1900', '8', '200', '', '', '', '', '', '', '1400', '2399', 0, 0, 0, 2, 0, 0, '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, '$', 'Front', 'America', '', 'middle_endian', 'Sales Tax', '', '0', 'Sales Tax 2', '', 0, '', 0, '', 0, '', '', '', '', '0000-00-00', '', 'english', '', '20', 0, '0', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, '', 1, 1, 1, 0, '', 0, 0, 0, 0, 0, '', '12_hour', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, '', '', 1, '', '', '', '', '', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 90, 25.00, '', 0, '', 0, 0, 0.00, 0, 0, '', 0, '', '', 0.00, NULL, NULL, NULL, NULL),
	(3, 1, 0, 0, 0, 0, 0, 'Course Sharing 2', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0600', '1900', '8', '200', '', '', '', '', '', '', '1400', '2399', 0, 0, 0, 2, 0, 0, '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, '$', 'Front', 'America', '', 'middle_endian', 'Sales Tax', '', '0', 'Sales Tax 2', '', 0, '', 0, '', 0, '', '', '', '', '0000-00-00', '', 'english', '', '20', 0, '0', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, '', 1, 1, 1, 0, '', 0, 0, 0, 0, 0, '', '12_hour', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, '', '', 1, '', '', '', '', '', 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 90, 25.00, '', 0, '', 0, 0, 0.00, 0, 0, '', 0, '', '', 0.00, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `foreup_courses` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_course_app_data
DROP TABLE IF EXISTS `foreup_course_app_data`;
CREATE TABLE IF NOT EXISTS `foreup_course_app_data` (
  `course_id` int(11) NOT NULL,
  `course_latitude` decimal(10,6) NOT NULL,
  `course_longitude` decimal(10,6) NOT NULL,
  `red` float NOT NULL,
  `green` float NOT NULL,
  `blue` float NOT NULL,
  `alpha` float NOT NULL,
  `home_view` int(11) NOT NULL,
  `gps_view` int(11) NOT NULL,
  `scorecard_view` int(11) NOT NULL,
  `teetime_view` int(11) NOT NULL,
  `food_and_beverage_view` int(11) NOT NULL,
  `last_updated` date NOT NULL,
  UNIQUE KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_course_app_data: ~0 rows (approximately)
DELETE FROM `foreup_course_app_data`;
/*!40000 ALTER TABLE `foreup_course_app_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_course_app_data` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_course_areas
DROP TABLE IF EXISTS `foreup_course_areas`;
CREATE TABLE IF NOT EXISTS `foreup_course_areas` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_course_areas: ~0 rows (approximately)
DELETE FROM `foreup_course_areas`;
/*!40000 ALTER TABLE `foreup_course_areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_course_areas` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_course_groups
DROP TABLE IF EXISTS `foreup_course_groups`;
CREATE TABLE IF NOT EXISTS `foreup_course_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `shared_tee_sheet` tinyint(4) NOT NULL DEFAULT '1',
  `shared_customers` tinyint(4) NOT NULL DEFAULT '1',
  `shared_giftcards` tinyint(4) NOT NULL DEFAULT '1',
  `shared_price_classes` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_course_groups: ~1 rows (approximately)
DELETE FROM `foreup_course_groups`;
/*!40000 ALTER TABLE `foreup_course_groups` DISABLE KEYS */;
INSERT INTO `foreup_course_groups` (`group_id`, `label`, `type`, `shared_tee_sheet`, `shared_customers`, `shared_giftcards`, `shared_price_classes`) VALUES
	(1, 'Sharing Customer', 'linked', 1, 1, 1, 0);
/*!40000 ALTER TABLE `foreup_course_groups` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_course_group_management
DROP TABLE IF EXISTS `foreup_course_group_management`;
CREATE TABLE IF NOT EXISTS `foreup_course_group_management` (
  `group_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_course_group_management: ~0 rows (approximately)
DELETE FROM `foreup_course_group_management`;
/*!40000 ALTER TABLE `foreup_course_group_management` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_course_group_management` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_course_group_members
DROP TABLE IF EXISTS `foreup_course_group_members`;
CREATE TABLE IF NOT EXISTS `foreup_course_group_members` (
  `group_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_course_group_members: ~2 rows (approximately)
DELETE FROM `foreup_course_group_members`;
/*!40000 ALTER TABLE `foreup_course_group_members` DISABLE KEYS */;
INSERT INTO `foreup_course_group_members` (`group_id`, `course_id`) VALUES
	(1, 2),
	(1, 3);
/*!40000 ALTER TABLE `foreup_course_group_members` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_course_messages
DROP TABLE IF EXISTS `foreup_course_messages`;
CREATE TABLE IF NOT EXISTS `foreup_course_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date_posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `read` tinyint(4) NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_course_messages: ~0 rows (approximately)
DELETE FROM `foreup_course_messages`;
/*!40000 ALTER TABLE `foreup_course_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_course_messages` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_customers
DROP TABLE IF EXISTS `foreup_customers`;
CREATE TABLE IF NOT EXISTS `foreup_customers` (
  `CID` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `course_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `member` smallint(6) NOT NULL,
  `price_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) unsigned NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_balance` decimal(15,2) NOT NULL,
  `account_balance_allow_negative` tinyint(4) NOT NULL,
  `account_limit` decimal(15,2) NOT NULL,
  `member_account_balance` decimal(15,2) NOT NULL,
  `member_account_balance_allow_negative` tinyint(4) NOT NULL,
  `member_account_limit` decimal(15,2) NOT NULL,
  `invoice_balance` decimal(15,2) NOT NULL,
  `invoice_balance_allow_negative` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `invoice_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `use_loyalty` tinyint(4) NOT NULL DEFAULT '1',
  `loyalty_points` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `discount` float(5,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `opt_out_email` tinyint(4) NOT NULL,
  `opt_out_text` tinyint(4) NOT NULL,
  `course_news_announcements` tinyint(1) NOT NULL,
  `unsubscribe_all` tinyint(1) NOT NULL,
  `teetime_reminders` tinyint(1) NOT NULL,
  `status_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Red, 2 = Yellow, 3 = Green',
  `require_food_minimum` smallint(6) NOT NULL,
  UNIQUE KEY `course_person` (`course_id`,`person_id`),
  UNIQUE KEY `account_number` (`account_number`,`course_id`),
  KEY `person_id` (`person_id`),
  KEY `deleted` (`deleted`),
  KEY `username` (`username`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `foreup_customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `foreup_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_customers: ~4 rows (approximately)
DELETE FROM `foreup_customers`;
/*!40000 ALTER TABLE `foreup_customers` DISABLE KEYS */;
INSERT INTO `foreup_customers` (`CID`, `person_id`, `course_id`, `api_id`, `username`, `password`, `member`, `price_class`, `image_id`, `account_number`, `account_balance`, `account_balance_allow_negative`, `account_limit`, `member_account_balance`, `member_account_balance_allow_negative`, `member_account_limit`, `invoice_balance`, `invoice_balance_allow_negative`, `invoice_email`, `use_loyalty`, `loyalty_points`, `company_name`, `taxable`, `discount`, `deleted`, `opt_out_email`, `opt_out_text`, `course_news_announcements`, `unsubscribe_all`, `teetime_reminders`, `status_flag`, `require_food_minimum`) VALUES
	(0, 1, 1, 0, 'Test1', '', 0, '', 0, NULL, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0, '', 1, 0, '', 1, 0.00, 0, 0, 0, 0, 0, 0, 0, 0),
	(0, 2, 1, 0, 'Test2', '', 0, '', 0, NULL, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0, '', 1, 0, '', 1, 0.00, 0, 0, 0, 0, 0, 0, 0, 0),
	(0, 1, 2, 0, 'Test1', '', 0, '', 0, NULL, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0, '', 1, 0, '', 1, 0.00, 0, 0, 0, 0, 0, 0, 0, 0),
	(0, 2, 3, 0, 'Test1', '', 0, '', 0, NULL, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0, '', 1, 0, '', 1, 0.00, 0, 0, 0, 0, 0, 0, 0, 0),
	(0, 3, 3, 0, 'Test3', '', 0, '', 0, NULL, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0, '', 1, 0, '', 1, 0.00, 0, 0, 0, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `foreup_customers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_customer_billing
DROP TABLE IF EXISTS `foreup_customer_billing`;
CREATE TABLE IF NOT EXISTS `foreup_customer_billing` (
  `billing_id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `email_invoice` tinyint(4) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `description` varchar(255) NOT NULL,
  `frequency` smallint(6) unsigned NOT NULL,
  `frequency_period` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `frequency_on` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `frequency_on_date` date NOT NULL,
  `due_days` smallint(5) unsigned NOT NULL,
  `stop_after` smallint(5) unsigned DEFAULT NULL,
  `last_invoice_generation_attempt` date NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `auto_bill_delay` tinyint(4) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL,
  `errors` tinyint(4) NOT NULL,
  `last_billing_attempt` date NOT NULL,
  `started` tinyint(4) NOT NULL,
  `charged` tinyint(4) NOT NULL,
  `emailed` tinyint(4) NOT NULL,
  `show_account_transactions` tinyint(1) NOT NULL DEFAULT '0',
  `attempt_limit` tinyint(1) NOT NULL DEFAULT '1',
  `auto_pay_overdue` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`billing_id`),
  KEY `course_id` (`course_id`),
  KEY `person_id` (`person_id`),
  KEY `start_date` (`start_date`,`end_date`),
  KEY `batch_id` (`batch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_customer_billing: ~0 rows (approximately)
DELETE FROM `foreup_customer_billing`;
/*!40000 ALTER TABLE `foreup_customer_billing` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_customer_billing` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_customer_billing_items
DROP TABLE IF EXISTS `foreup_customer_billing_items`;
CREATE TABLE IF NOT EXISTS `foreup_customer_billing_items` (
  `billing_id` int(11) NOT NULL,
  `line` tinyint(4) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `item_type` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `quantity` smallint(6) NOT NULL,
  `price` float(15,2) NOT NULL,
  `tax_percentage` float(15,2) NOT NULL,
  UNIQUE KEY `billing_id` (`billing_id`,`line`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_customer_billing_items: ~0 rows (approximately)
DELETE FROM `foreup_customer_billing_items`;
/*!40000 ALTER TABLE `foreup_customer_billing_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_customer_billing_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_customer_credit_cards
DROP TABLE IF EXISTS `foreup_customer_credit_cards`;
CREATE TABLE IF NOT EXISTS `foreup_customer_credit_cards` (
  `credit_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `ets_key` varchar(255) NOT NULL,
  `mercury_id` varchar(255) NOT NULL,
  `mercury_password` varchar(255) NOT NULL,
  `element_account_id` varchar(255) DEFAULT NULL,
  `element_account_token` varchar(255) DEFAULT NULL,
  `element_application_id` varchar(255) DEFAULT NULL,
  `element_acceptor_id` varchar(255) DEFAULT NULL,
  `recurring` tinyint(4) NOT NULL,
  `course_id` mediumint(9) NOT NULL,
  `customer_id` mediumint(9) NOT NULL,
  `tee_time_id` varchar(21) NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_expiration` date NOT NULL,
  `card_type` varchar(255) NOT NULL,
  `cardholder_name` varchar(255) NOT NULL,
  `expiration` date NOT NULL,
  `masked_account` varchar(255) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `successful_charges` smallint(6) NOT NULL,
  `failed_charges` smallint(6) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`credit_card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_customer_credit_cards: ~0 rows (approximately)
DELETE FROM `foreup_customer_credit_cards`;
/*!40000 ALTER TABLE `foreup_customer_credit_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_customer_credit_cards` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_customer_food_spending
DROP TABLE IF EXISTS `foreup_customer_food_spending`;
CREATE TABLE IF NOT EXISTS `foreup_customer_food_spending` (
  `course_id` int(11) unsigned NOT NULL,
  `person_id` int(11) unsigned NOT NULL,
  `minimum` decimal(15,2) NOT NULL DEFAULT '0.00',
  `spent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `amount` decimal(15,2) DEFAULT '0.00',
  `month` int(2) unsigned NOT NULL,
  `year` int(4) unsigned NOT NULL,
  `transaction_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_customer_food_spending: ~0 rows (approximately)
DELETE FROM `foreup_customer_food_spending`;
/*!40000 ALTER TABLE `foreup_customer_food_spending` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_customer_food_spending` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_customer_groups
DROP TABLE IF EXISTS `foreup_customer_groups`;
CREATE TABLE IF NOT EXISTS `foreup_customer_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_customer_groups: ~1 rows (approximately)
DELETE FROM `foreup_customer_groups`;
/*!40000 ALTER TABLE `foreup_customer_groups` DISABLE KEYS */;
INSERT INTO `foreup_customer_groups` (`group_id`, `label`, `CID`, `course_id`) VALUES
	(1, 'Test Group', 0, 1);
/*!40000 ALTER TABLE `foreup_customer_groups` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_customer_group_members
DROP TABLE IF EXISTS `foreup_customer_group_members`;
CREATE TABLE IF NOT EXISTS `foreup_customer_group_members` (
  `group_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`person_id`),
  KEY `group_id` (`group_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_customer_group_members: ~1 rows (approximately)
DELETE FROM `foreup_customer_group_members`;
/*!40000 ALTER TABLE `foreup_customer_group_members` DISABLE KEYS */;
INSERT INTO `foreup_customer_group_members` (`group_id`, `person_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `foreup_customer_group_members` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_customer_passes
DROP TABLE IF EXISTS `foreup_customer_passes`;
CREATE TABLE IF NOT EXISTS `foreup_customer_passes` (
  `pass_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`pass_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_customer_passes: ~0 rows (approximately)
DELETE FROM `foreup_customer_passes`;
/*!40000 ALTER TABLE `foreup_customer_passes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_customer_passes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_customer_pass_members
DROP TABLE IF EXISTS `foreup_customer_pass_members`;
CREATE TABLE IF NOT EXISTS `foreup_customer_pass_members` (
  `pass_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  PRIMARY KEY (`pass_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_customer_pass_members: ~0 rows (approximately)
DELETE FROM `foreup_customer_pass_members`;
/*!40000 ALTER TABLE `foreup_customer_pass_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_customer_pass_members` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_dashboards
DROP TABLE IF EXISTS `foreup_dashboards`;
CREATE TABLE IF NOT EXISTS `foreup_dashboards` (
  `dashboard_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `course_id` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`dashboard_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table testTable.foreup_dashboards: ~0 rows (approximately)
DELETE FROM `foreup_dashboards`;
/*!40000 ALTER TABLE `foreup_dashboards` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_dashboards` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_dashboard_widgets
DROP TABLE IF EXISTS `foreup_dashboard_widgets`;
CREATE TABLE IF NOT EXISTS `foreup_dashboard_widgets` (
  `widget_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `period` varchar(24) NOT NULL,
  `data_grouping` varchar(8) NOT NULL,
  `pos_x` int(10) unsigned NOT NULL DEFAULT '0',
  `pos_y` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `dashboard_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`widget_id`),
  KEY `metric_id` (`dashboard_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table testTable.foreup_dashboard_widgets: ~0 rows (approximately)
DELETE FROM `foreup_dashboard_widgets`;
/*!40000 ALTER TABLE `foreup_dashboard_widgets` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_dashboard_widgets` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_dashboard_widget_metrics
DROP TABLE IF EXISTS `foreup_dashboard_widget_metrics`;
CREATE TABLE IF NOT EXISTS `foreup_dashboard_widget_metrics` (
  `widget_id` int(10) unsigned NOT NULL,
  `metric_category` varchar(255) NOT NULL,
  `metric` varchar(255) NOT NULL,
  `relative_period_num` tinyint(2) NOT NULL,
  `position` int(10) unsigned NOT NULL,
  PRIMARY KEY (`widget_id`,`metric_category`,`metric`,`relative_period_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table testTable.foreup_dashboard_widget_metrics: ~0 rows (approximately)
DELETE FROM `foreup_dashboard_widget_metrics`;
/*!40000 ALTER TABLE `foreup_dashboard_widget_metrics` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_dashboard_widget_metrics` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_employees
DROP TABLE IF EXISTS `foreup_employees`;
CREATE TABLE IF NOT EXISTS `foreup_employees` (
  `course_id` int(11) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL DEFAULT '999999',
  `TSID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_level` tinyint(4) NOT NULL DEFAULT '1',
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pin` mediumint(9) DEFAULT NULL,
  `card` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_id` int(11) unsigned NOT NULL,
  `person_id` int(10) NOT NULL,
  `activated` int(1) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL,
  `zendesk` tinyint(4) NOT NULL,
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `pin` (`course_id`,`pin`),
  UNIQUE KEY `card` (`course_id`,`card`),
  KEY `person_id` (`person_id`),
  KEY `deleted` (`deleted`),
  CONSTRAINT `foreup_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `foreup_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_employees: ~0 rows (approximately)
DELETE FROM `foreup_employees`;
/*!40000 ALTER TABLE `foreup_employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_employees` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_employee_permissions
DROP TABLE IF EXISTS `foreup_employee_permissions`;
CREATE TABLE IF NOT EXISTS `foreup_employee_permissions` (
  `person_id` int(11) NOT NULL,
  `sales_stats` tinyint(4) NOT NULL,
  UNIQUE KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_employee_permissions: ~0 rows (approximately)
DELETE FROM `foreup_employee_permissions`;
/*!40000 ALTER TABLE `foreup_employee_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_employee_permissions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_event_people
DROP TABLE IF EXISTS `foreup_event_people`;
CREATE TABLE IF NOT EXISTS `foreup_event_people` (
  `teetime_id` varchar(21) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `checked_in` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `paid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cart` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_checked_in` datetime DEFAULT NULL,
  `date_paid` datetime DEFAULT NULL,
  `player_position` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hole` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `teetime_id` (`teetime_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_event_people: ~0 rows (approximately)
DELETE FROM `foreup_event_people`;
/*!40000 ALTER TABLE `foreup_event_people` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_event_people` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_fees
DROP TABLE IF EXISTS `foreup_fees`;
CREATE TABLE IF NOT EXISTS `foreup_fees` (
  `schedule_id` int(11) NOT NULL,
  `item_number` varchar(255) NOT NULL,
  `price_category_1` decimal(15,2) NOT NULL,
  `price_category_2` decimal(15,2) NOT NULL,
  `price_category_3` decimal(15,2) NOT NULL,
  `price_category_4` decimal(15,2) NOT NULL,
  `price_category_5` decimal(15,2) NOT NULL,
  `price_category_6` decimal(15,2) NOT NULL,
  `price_category_7` decimal(15,2) NOT NULL,
  `price_category_8` decimal(15,2) NOT NULL,
  `price_category_9` decimal(15,2) NOT NULL,
  `price_category_10` decimal(15,2) NOT NULL,
  `price_category_11` decimal(15,2) NOT NULL,
  `price_category_12` decimal(15,2) NOT NULL,
  `price_category_13` decimal(15,2) NOT NULL,
  `price_category_14` decimal(15,2) NOT NULL,
  `price_category_15` decimal(15,2) NOT NULL,
  `price_category_16` decimal(15,2) NOT NULL,
  `price_category_17` decimal(15,2) NOT NULL,
  `price_category_18` decimal(15,2) NOT NULL,
  `price_category_19` decimal(15,2) NOT NULL,
  `price_category_20` decimal(15,2) NOT NULL,
  `price_category_21` decimal(15,2) NOT NULL,
  `price_category_22` decimal(15,2) NOT NULL,
  `price_category_23` decimal(15,2) NOT NULL,
  `price_category_24` decimal(15,2) NOT NULL,
  `price_category_25` decimal(15,2) NOT NULL,
  `price_category_26` decimal(15,2) NOT NULL,
  `price_category_27` decimal(15,2) NOT NULL,
  `price_category_28` decimal(15,2) NOT NULL,
  `price_category_29` decimal(15,2) NOT NULL,
  `price_category_30` decimal(15,2) NOT NULL,
  `price_category_31` decimal(15,2) NOT NULL,
  `price_category_32` decimal(15,2) NOT NULL,
  `price_category_33` decimal(15,2) NOT NULL,
  `price_category_34` decimal(15,2) NOT NULL,
  `price_category_35` decimal(15,2) NOT NULL,
  `price_category_36` decimal(15,2) NOT NULL,
  `price_category_37` decimal(15,2) NOT NULL,
  `price_category_38` decimal(15,2) NOT NULL,
  `price_category_39` decimal(15,2) NOT NULL,
  `price_category_40` decimal(15,2) NOT NULL,
  `price_category_41` decimal(15,2) NOT NULL,
  `price_category_42` decimal(15,2) NOT NULL,
  `price_category_43` decimal(15,2) NOT NULL,
  `price_category_44` decimal(15,2) NOT NULL,
  `price_category_45` decimal(15,2) NOT NULL,
  `price_category_46` decimal(15,2) NOT NULL,
  `price_category_47` decimal(15,2) NOT NULL,
  `price_category_48` decimal(15,2) NOT NULL,
  `price_category_49` decimal(15,2) NOT NULL,
  `price_category_50` decimal(15,2) NOT NULL,
  PRIMARY KEY (`schedule_id`,`item_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_fees: ~0 rows (approximately)
DELETE FROM `foreup_fees`;
/*!40000 ALTER TABLE `foreup_fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_fees` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_fee_types
DROP TABLE IF EXISTS `foreup_fee_types`;
CREATE TABLE IF NOT EXISTS `foreup_fee_types` (
  `course_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `price_category_1` varchar(255) NOT NULL,
  `price_category_2` varchar(255) NOT NULL,
  `price_category_3` varchar(255) NOT NULL,
  `price_category_4` varchar(255) NOT NULL,
  `price_category_5` varchar(255) NOT NULL,
  `price_category_6` varchar(255) NOT NULL,
  `price_category_7` varchar(255) NOT NULL,
  `price_category_8` varchar(255) NOT NULL,
  `price_category_9` varchar(255) NOT NULL,
  `price_category_10` varchar(255) NOT NULL,
  `price_category_11` varchar(255) NOT NULL,
  `price_category_12` varchar(255) NOT NULL,
  `price_category_13` varchar(255) NOT NULL,
  `price_category_14` varchar(255) NOT NULL,
  `price_category_15` varchar(255) NOT NULL,
  `price_category_16` varchar(255) NOT NULL,
  `price_category_17` varchar(255) NOT NULL,
  `price_category_18` varchar(255) NOT NULL,
  `price_category_19` varchar(255) NOT NULL,
  `price_category_20` varchar(255) NOT NULL,
  `price_category_21` varchar(255) NOT NULL,
  `price_category_22` varchar(255) NOT NULL,
  `price_category_23` varchar(255) NOT NULL,
  `price_category_24` varchar(255) NOT NULL,
  `price_category_25` varchar(255) NOT NULL,
  `price_category_26` varchar(255) NOT NULL,
  `price_category_27` varchar(255) NOT NULL,
  `price_category_28` varchar(255) NOT NULL,
  `price_category_29` varchar(255) NOT NULL,
  `price_category_30` varchar(255) NOT NULL,
  `price_category_31` varchar(255) NOT NULL,
  `price_category_32` varchar(255) NOT NULL,
  `price_category_33` varchar(255) NOT NULL,
  `price_category_34` varchar(255) NOT NULL,
  `price_category_35` varchar(255) NOT NULL,
  `price_category_36` varchar(255) NOT NULL,
  `price_category_37` varchar(255) NOT NULL,
  `price_category_38` varchar(255) NOT NULL,
  `price_category_39` varchar(255) NOT NULL,
  `price_category_40` varchar(255) NOT NULL,
  `price_category_41` varchar(255) NOT NULL,
  `price_category_42` varchar(255) NOT NULL,
  `price_category_43` varchar(255) NOT NULL,
  `price_category_44` varchar(255) NOT NULL,
  `price_category_45` varchar(255) NOT NULL,
  `price_category_46` varchar(255) NOT NULL,
  `price_category_47` varchar(255) NOT NULL,
  `price_category_48` varchar(255) NOT NULL,
  `price_category_49` varchar(255) NOT NULL,
  `price_category_50` varchar(255) NOT NULL,
  PRIMARY KEY (`course_id`,`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_fee_types: ~0 rows (approximately)
DELETE FROM `foreup_fee_types`;
/*!40000 ALTER TABLE `foreup_fee_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_fee_types` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_giftcards
DROP TABLE IF EXISTS `foreup_giftcards`;
CREATE TABLE IF NOT EXISTS `foreup_giftcards` (
  `CID` int(11) NOT NULL,
  `giftcard_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `giftcard_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` decimal(15,2) NOT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `date_issued` date NOT NULL,
  `expiration_date` date NOT NULL,
  `department` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_number` (`giftcard_number`,`course_id`),
  KEY `deleted` (`deleted`),
  KEY `foreup_giftcards_ibfk_1` (`customer_id`),
  CONSTRAINT `foreup_giftcards_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `foreup_customers` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_giftcards: ~0 rows (approximately)
DELETE FROM `foreup_giftcards`;
/*!40000 ALTER TABLE `foreup_giftcards` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_giftcards` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_giftcard_transactions
DROP TABLE IF EXISTS `foreup_giftcard_transactions`;
CREATE TABLE IF NOT EXISTS `foreup_giftcard_transactions` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `trans_giftcard` int(11) NOT NULL,
  `trans_customer` int(11) NOT NULL,
  `trans_user` int(11) NOT NULL,
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_description` text NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_giftcard_transactions: ~0 rows (approximately)
DELETE FROM `foreup_giftcard_transactions`;
/*!40000 ALTER TABLE `foreup_giftcard_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_giftcard_transactions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_green_fees
DROP TABLE IF EXISTS `foreup_green_fees`;
CREATE TABLE IF NOT EXISTS `foreup_green_fees` (
  `teesheet_id` int(11) NOT NULL,
  `item_number` varchar(255) NOT NULL,
  `price_category_1` decimal(15,2) NOT NULL,
  `price_category_2` decimal(15,2) NOT NULL,
  `price_category_3` decimal(15,2) NOT NULL,
  `price_category_4` decimal(15,2) NOT NULL,
  `price_category_5` decimal(15,2) NOT NULL,
  `price_category_6` decimal(15,2) NOT NULL,
  `price_category_7` decimal(15,2) NOT NULL,
  `price_category_8` decimal(15,2) NOT NULL,
  `price_category_9` decimal(15,2) NOT NULL,
  `price_category_10` decimal(15,2) NOT NULL,
  `price_category_11` decimal(15,2) NOT NULL,
  `price_category_12` decimal(15,2) NOT NULL,
  `price_category_13` decimal(15,2) NOT NULL,
  `price_category_14` decimal(15,2) NOT NULL,
  `price_category_15` decimal(15,2) NOT NULL,
  `price_category_16` decimal(15,2) NOT NULL,
  `price_category_17` decimal(15,2) NOT NULL,
  `price_category_18` decimal(15,2) NOT NULL,
  `price_category_19` decimal(15,2) NOT NULL,
  `price_category_20` decimal(15,2) NOT NULL,
  `price_category_21` decimal(15,2) NOT NULL,
  `price_category_22` decimal(15,2) NOT NULL,
  `price_category_23` decimal(15,2) NOT NULL,
  `price_category_24` decimal(15,2) NOT NULL,
  `price_category_25` decimal(15,2) NOT NULL,
  `price_category_26` decimal(15,2) NOT NULL,
  `price_category_27` decimal(15,2) NOT NULL,
  `price_category_28` decimal(15,2) NOT NULL,
  `price_category_29` decimal(15,2) NOT NULL,
  `price_category_30` decimal(15,2) NOT NULL,
  `price_category_31` decimal(15,2) NOT NULL,
  `price_category_32` decimal(15,2) NOT NULL,
  `price_category_33` decimal(15,2) NOT NULL,
  `price_category_34` decimal(15,2) NOT NULL,
  `price_category_35` decimal(15,2) NOT NULL,
  `price_category_36` decimal(15,2) NOT NULL,
  `price_category_37` decimal(15,2) NOT NULL,
  `price_category_38` decimal(15,2) NOT NULL,
  `price_category_39` decimal(15,2) NOT NULL,
  `price_category_40` decimal(15,2) NOT NULL,
  `price_category_41` decimal(15,2) NOT NULL,
  `price_category_42` decimal(15,2) NOT NULL,
  `price_category_43` decimal(15,2) NOT NULL,
  `price_category_44` decimal(15,2) NOT NULL,
  `price_category_45` decimal(15,2) NOT NULL,
  `price_category_46` decimal(15,2) NOT NULL,
  `price_category_47` decimal(15,2) NOT NULL,
  `price_category_48` decimal(15,2) NOT NULL,
  `price_category_49` decimal(15,2) NOT NULL,
  `price_category_50` decimal(15,2) NOT NULL,
  PRIMARY KEY (`item_number`,`teesheet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_green_fees: ~0 rows (approximately)
DELETE FROM `foreup_green_fees`;
/*!40000 ALTER TABLE `foreup_green_fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_green_fees` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_green_fee_types
DROP TABLE IF EXISTS `foreup_green_fee_types`;
CREATE TABLE IF NOT EXISTS `foreup_green_fee_types` (
  `course_id` int(11) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `price_category_1` varchar(255) NOT NULL,
  `price_category_2` varchar(255) NOT NULL,
  `price_category_3` varchar(255) NOT NULL,
  `price_category_4` varchar(255) NOT NULL,
  `price_category_5` varchar(255) NOT NULL,
  `price_category_6` varchar(255) NOT NULL,
  `price_category_7` varchar(255) NOT NULL,
  `price_category_8` varchar(255) NOT NULL,
  `price_category_9` varchar(255) NOT NULL,
  `price_category_10` varchar(255) NOT NULL,
  `price_category_11` varchar(255) NOT NULL,
  `price_category_12` varchar(255) NOT NULL,
  `price_category_13` varchar(255) NOT NULL,
  `price_category_14` varchar(255) NOT NULL,
  `price_category_15` varchar(255) NOT NULL,
  `price_category_16` varchar(255) NOT NULL,
  `price_category_17` varchar(255) NOT NULL,
  `price_category_18` varchar(255) NOT NULL,
  `price_category_19` varchar(255) NOT NULL,
  `price_category_20` varchar(255) NOT NULL,
  `price_category_21` varchar(255) NOT NULL,
  `price_category_22` varchar(255) NOT NULL,
  `price_category_23` varchar(255) NOT NULL,
  `price_category_24` varchar(255) NOT NULL,
  `price_category_25` varchar(255) NOT NULL,
  `price_category_26` varchar(255) NOT NULL,
  `price_category_27` varchar(255) NOT NULL,
  `price_category_28` varchar(255) NOT NULL,
  `price_category_29` varchar(255) NOT NULL,
  `price_category_30` varchar(255) NOT NULL,
  `price_category_31` varchar(255) NOT NULL,
  `price_category_32` varchar(255) NOT NULL,
  `price_category_33` varchar(255) NOT NULL,
  `price_category_34` varchar(255) NOT NULL,
  `price_category_35` varchar(255) NOT NULL,
  `price_category_36` varchar(255) NOT NULL,
  `price_category_37` varchar(255) NOT NULL,
  `price_category_38` varchar(255) NOT NULL,
  `price_category_39` varchar(255) NOT NULL,
  `price_category_40` varchar(255) NOT NULL,
  `price_category_41` varchar(255) NOT NULL,
  `price_category_42` varchar(255) NOT NULL,
  `price_category_43` varchar(255) NOT NULL,
  `price_category_44` varchar(255) NOT NULL,
  `price_category_45` varchar(255) NOT NULL,
  `price_category_46` varchar(255) NOT NULL,
  `price_category_47` varchar(255) NOT NULL,
  `price_category_48` varchar(255) NOT NULL,
  `price_category_49` varchar(255) NOT NULL,
  `price_category_50` varchar(255) NOT NULL,
  PRIMARY KEY (`teesheet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_green_fee_types: ~0 rows (approximately)
DELETE FROM `foreup_green_fee_types`;
/*!40000 ALTER TABLE `foreup_green_fee_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_green_fee_types` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_groups
DROP TABLE IF EXISTS `foreup_groups`;
CREATE TABLE IF NOT EXISTS `foreup_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_groups: ~0 rows (approximately)
DELETE FROM `foreup_groups`;
/*!40000 ALTER TABLE `foreup_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_groups` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_group_members
DROP TABLE IF EXISTS `foreup_group_members`;
CREATE TABLE IF NOT EXISTS `foreup_group_members` (
  `group_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_group_members: ~0 rows (approximately)
DELETE FROM `foreup_group_members`;
/*!40000 ALTER TABLE `foreup_group_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_group_members` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_help_posts
DROP TABLE IF EXISTS `foreup_help_posts`;
CREATE TABLE IF NOT EXISTS `foreup_help_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `contents` text,
  `deleted` int(1) DEFAULT '0',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `views` int(11) DEFAULT '0',
  `keywords` varchar(255) DEFAULT NULL,
  `related_articles` text,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title` (`title`,`contents`),
  FULLTEXT KEY `title_2` (`title`,`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_help_posts: 0 rows
DELETE FROM `foreup_help_posts`;
/*!40000 ALTER TABLE `foreup_help_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_help_posts` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_help_post_ratings
DROP TABLE IF EXISTS `foreup_help_post_ratings`;
CREATE TABLE IF NOT EXISTS `foreup_help_post_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_help_post_ratings: ~0 rows (approximately)
DELETE FROM `foreup_help_post_ratings`;
/*!40000 ALTER TABLE `foreup_help_post_ratings` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_help_post_ratings` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_help_topics
DROP TABLE IF EXISTS `foreup_help_topics`;
CREATE TABLE IF NOT EXISTS `foreup_help_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_help_topics: ~0 rows (approximately)
DELETE FROM `foreup_help_topics`;
/*!40000 ALTER TABLE `foreup_help_topics` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_help_topics` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_holes
DROP TABLE IF EXISTS `foreup_holes`;
CREATE TABLE IF NOT EXISTS `foreup_holes` (
  `hole_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_id` int(11) NOT NULL,
  `par` int(3) NOT NULL,
  `hole_number` int(3) NOT NULL,
  `front_latitude` decimal(10,6) NOT NULL,
  `front_longitude` decimal(10,6) NOT NULL,
  `center_latitude` decimal(10,6) NOT NULL,
  `center_longitude` decimal(10,6) NOT NULL,
  `back_latitude` decimal(10,6) NOT NULL,
  `back_longitude` decimal(10,6) NOT NULL,
  `hole_latitude` decimal(10,6) NOT NULL,
  `hole_longitude` decimal(10,6) NOT NULL,
  `middle_latitude` decimal(10,6) NOT NULL,
  `middle_longitude` decimal(10,6) NOT NULL,
  `tee_1_latitude` decimal(10,6) NOT NULL,
  `tee_1_longitude` decimal(10,6) NOT NULL,
  `tee_2_latitude` decimal(10,6) NOT NULL,
  `tee_2_longitude` decimal(10,6) NOT NULL,
  `tee_3_latitude` decimal(10,6) NOT NULL,
  `tee_3_longitude` decimal(10,6) NOT NULL,
  `tee_4_latitude` decimal(10,6) NOT NULL,
  `tee_4_longitude` decimal(10,6) NOT NULL,
  `tee_5_latitude` decimal(10,6) NOT NULL,
  `tee_5_longitude` decimal(10,6) NOT NULL,
  `tee_6_latitude` decimal(10,6) NOT NULL,
  `tee_6_longitude` decimal(10,6) NOT NULL,
  `tee_7_latitude` decimal(10,6) NOT NULL,
  `tee_7_longitude` decimal(10,6) NOT NULL,
  `tee_8_latitude` decimal(10,6) NOT NULL,
  `tee_8_longitude` decimal(10,6) NOT NULL,
  PRIMARY KEY (`hole_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_holes: ~0 rows (approximately)
DELETE FROM `foreup_holes`;
/*!40000 ALTER TABLE `foreup_holes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_holes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_holidays
DROP TABLE IF EXISTS `foreup_holidays`;
CREATE TABLE IF NOT EXISTS `foreup_holidays` (
  `course_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`course_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_holidays: ~0 rows (approximately)
DELETE FROM `foreup_holidays`;
/*!40000 ALTER TABLE `foreup_holidays` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_holidays` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_home_articles
DROP TABLE IF EXISTS `foreup_home_articles`;
CREATE TABLE IF NOT EXISTS `foreup_home_articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `column` tinyint(4) NOT NULL,
  `img` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_home_articles: ~0 rows (approximately)
DELETE FROM `foreup_home_articles`;
/*!40000 ALTER TABLE `foreup_home_articles` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_home_articles` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_households
DROP TABLE IF EXISTS `foreup_households`;
CREATE TABLE IF NOT EXISTS `foreup_households` (
  `household_id` int(11) NOT NULL AUTO_INCREMENT,
  `household_head_id` int(11) NOT NULL,
  PRIMARY KEY (`household_id`),
  UNIQUE KEY `household_head_id` (`household_head_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_households: ~0 rows (approximately)
DELETE FROM `foreup_households`;
/*!40000 ALTER TABLE `foreup_households` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_households` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_household_members
DROP TABLE IF EXISTS `foreup_household_members`;
CREATE TABLE IF NOT EXISTS `foreup_household_members` (
  `household_id` int(11) NOT NULL,
  `household_member_id` int(11) NOT NULL,
  UNIQUE KEY `household_member_id` (`household_member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_household_members: ~0 rows (approximately)
DELETE FROM `foreup_household_members`;
/*!40000 ALTER TABLE `foreup_household_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_household_members` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_ibeacon
DROP TABLE IF EXISTS `foreup_ibeacon`;
CREATE TABLE IF NOT EXISTS `foreup_ibeacon` (
  `beacon_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `major_id` mediumint(8) unsigned NOT NULL,
  `minor_id` mediumint(8) unsigned NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `terminal_id` int(10) unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `received` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`beacon_id`),
  KEY `course_id` (`course_id`,`terminal_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_ibeacon: ~0 rows (approximately)
DELETE FROM `foreup_ibeacon`;
/*!40000 ALTER TABLE `foreup_ibeacon` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_ibeacon` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_images
DROP TABLE IF EXISTS `foreup_images`;
CREATE TABLE IF NOT EXISTS `foreup_images` (
  `image_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) unsigned NOT NULL,
  `module` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(5) unsigned NOT NULL,
  `height` int(5) unsigned NOT NULL,
  `filesize` decimal(7,2) unsigned NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `saved` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_images: ~0 rows (approximately)
DELETE FROM `foreup_images`;
/*!40000 ALTER TABLE `foreup_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_images` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_inventory
DROP TABLE IF EXISTS `foreup_inventory`;
CREATE TABLE IF NOT EXISTS `foreup_inventory` (
  `CID` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_inventory` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`trans_id`),
  KEY `foreup_inventory_ibfk_1` (`trans_items`),
  KEY `foreup_inventory_ibfk_2` (`trans_user`),
  CONSTRAINT `foreup_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `foreup_items` (`item_id`),
  CONSTRAINT `foreup_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `foreup_employees` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_inventory: ~0 rows (approximately)
DELETE FROM `foreup_inventory`;
/*!40000 ALTER TABLE `foreup_inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_inventory` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_inventory_audits
DROP TABLE IF EXISTS `foreup_inventory_audits`;
CREATE TABLE IF NOT EXISTS `foreup_inventory_audits` (
  `inventory_audit_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`inventory_audit_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_inventory_audits: ~0 rows (approximately)
DELETE FROM `foreup_inventory_audits`;
/*!40000 ALTER TABLE `foreup_inventory_audits` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_inventory_audits` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_inventory_audit_items
DROP TABLE IF EXISTS `foreup_inventory_audit_items`;
CREATE TABLE IF NOT EXISTS `foreup_inventory_audit_items` (
  `inventory_audit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `current_count` smallint(6) NOT NULL,
  `manual_count` smallint(6) NOT NULL,
  KEY `inventory_audit_id` (`inventory_audit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_inventory_audit_items: ~0 rows (approximately)
DELETE FROM `foreup_inventory_audit_items`;
/*!40000 ALTER TABLE `foreup_inventory_audit_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_inventory_audit_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_invoices
DROP TABLE IF EXISTS `foreup_invoices`;
CREATE TABLE IF NOT EXISTS `foreup_invoices` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_number` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL DEFAULT 'Invoices',
  `category` varchar(255) NOT NULL DEFAULT 'Invoices',
  `subcategory` varchar(255) NOT NULL,
  `day` tinyint(4) NOT NULL,
  `course_id` int(11) NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `billing_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bill_start` datetime NOT NULL,
  `bill_end` datetime NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `overdue_total` decimal(15,2) NOT NULL,
  `previous_payments` decimal(15,2) NOT NULL,
  `paid` decimal(15,2) NOT NULL,
  `overdue` decimal(15,2) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `credit_card_payment_id` int(11) NOT NULL,
  `email_invoice` tinyint(4) NOT NULL,
  `last_billing_attempt` date NOT NULL,
  `started` tinyint(4) NOT NULL,
  `charged_no_sale` tinyint(4) NOT NULL,
  `charged` tinyint(4) NOT NULL,
  `emailed` tinyint(4) NOT NULL,
  `send_date` date NOT NULL,
  `due_date` date NOT NULL,
  `auto_bill_date` date NOT NULL,
  `pay_customer_account` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pay_member_account` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show_account_transactions` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sale_id` int(10) unsigned NOT NULL,
  `notes` varchar(255) NOT NULL,
  `attempt_limit` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`invoice_id`),
  KEY `credit_card_payment_id` (`credit_card_payment_id`),
  KEY `course_id` (`course_id`),
  KEY `person_id` (`person_id`),
  KEY `billing_id` (`billing_id`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_invoices: ~0 rows (approximately)
DELETE FROM `foreup_invoices`;
/*!40000 ALTER TABLE `foreup_invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_invoices` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_invoice_changes
DROP TABLE IF EXISTS `foreup_invoice_changes`;
CREATE TABLE IF NOT EXISTS `foreup_invoice_changes` (
  `invoice_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `previous_paid` float(15,2) NOT NULL,
  `paid` float(15,2) NOT NULL,
  `previous_overdue` float(15,2) NOT NULL,
  `overdue` float(15,2) NOT NULL,
  KEY `invoice_id` (`invoice_id`,`course_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_invoice_changes: ~0 rows (approximately)
DELETE FROM `foreup_invoice_changes`;
/*!40000 ALTER TABLE `foreup_invoice_changes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_invoice_changes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_invoice_charge_attempts
DROP TABLE IF EXISTS `foreup_invoice_charge_attempts`;
CREATE TABLE IF NOT EXISTS `foreup_invoice_charge_attempts` (
  `charge_attempt_id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_card_charge_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `date` datetime NOT NULL,
  `success` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`charge_attempt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_invoice_charge_attempts: ~0 rows (approximately)
DELETE FROM `foreup_invoice_charge_attempts`;
/*!40000 ALTER TABLE `foreup_invoice_charge_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_invoice_charge_attempts` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_invoice_items
DROP TABLE IF EXISTS `foreup_invoice_items`;
CREATE TABLE IF NOT EXISTS `foreup_invoice_items` (
  `invoice_id` int(11) NOT NULL,
  `line_number` tinyint(4) NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` smallint(6) NOT NULL,
  `amount` float(15,2) NOT NULL,
  `paid_amount` float(15,2) NOT NULL,
  `paid_off` datetime NOT NULL,
  `tax` float(15,2) NOT NULL,
  `pay_account_balance` tinyint(4) NOT NULL,
  `pay_member_balance` tinyint(4) NOT NULL,
  UNIQUE KEY `invoice_id` (`invoice_id`,`line_number`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_invoice_items: ~0 rows (approximately)
DELETE FROM `foreup_invoice_items`;
/*!40000 ALTER TABLE `foreup_invoice_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_invoice_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_items
DROP TABLE IF EXISTS `foreup_items`;
CREATE TABLE IF NOT EXISTS `foreup_items` (
  `course_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) unsigned NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `unit_price_includes_tax` tinyint(4) NOT NULL,
  `max_discount` decimal(15,2) NOT NULL DEFAULT '100.00',
  `quantity` decimal(15,2) NOT NULL DEFAULT '0.00',
  `is_unlimited` tinyint(1) NOT NULL,
  `reorder_level` int(15) NOT NULL DEFAULT '0',
  `food_and_beverage` tinyint(1) NOT NULL DEFAULT '0',
  `soup_or_salad` enum('none','soup','salad','either','both') COLLATE utf8_unicode_ci NOT NULL,
  `number_of_sides` tinyint(4) NOT NULL DEFAULT '0',
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `gl_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `is_giftcard` tinyint(4) NOT NULL,
  `invisible` tinyint(4) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `is_side` tinyint(4) NOT NULL,
  `add_on_price` float(15,2) NOT NULL,
  `print_priority` tinyint(4) NOT NULL,
  `kitchen_printer` tinyint(4) NOT NULL DEFAULT '1',
  `quickbooks_income` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `quickbooks_cogs` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `quickbooks_assets` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `erange_size` tinyint(4) NOT NULL,
  `is_fee` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `item_number` (`item_number`,`course_id`),
  KEY `foreup_items_ibfk_1` (`supplier_id`),
  KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `deleted` (`deleted`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `foreup_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `foreup_suppliers` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_items: ~0 rows (approximately)
DELETE FROM `foreup_items`;
/*!40000 ALTER TABLE `foreup_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_items_taxes
DROP TABLE IF EXISTS `foreup_items_taxes`;
CREATE TABLE IF NOT EXISTS `foreup_items_taxes` (
  `course_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `item_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`,`name`,`percent`),
  CONSTRAINT `foreup_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_items_taxes: ~0 rows (approximately)
DELETE FROM `foreup_items_taxes`;
/*!40000 ALTER TABLE `foreup_items_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_items_taxes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_items_upcs
DROP TABLE IF EXISTS `foreup_items_upcs`;
CREATE TABLE IF NOT EXISTS `foreup_items_upcs` (
  `upc_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `upc` varchar(255) NOT NULL,
  PRIMARY KEY (`upc_id`),
  UNIQUE KEY `course_upc` (`course_id`,`upc`),
  KEY `item_id` (`item_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_items_upcs: ~0 rows (approximately)
DELETE FROM `foreup_items_upcs`;
/*!40000 ALTER TABLE `foreup_items_upcs` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_items_upcs` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_item_kits
DROP TABLE IF EXISTS `foreup_item_kits`;
CREATE TABLE IF NOT EXISTS `foreup_item_kits` (
  `CID` int(11) NOT NULL,
  `item_kit_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `item_kit_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_price` decimal(15,2) DEFAULT NULL,
  `cost_price` decimal(15,2) DEFAULT NULL,
  `quickbooks_income` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `quickbooks_cogs` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `quickbooks_assets` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `is_punch_card` tinyint(4) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_kit_id`),
  UNIQUE KEY `item_kit_number` (`item_kit_number`,`course_id`),
  KEY `name` (`name`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_item_kits: ~0 rows (approximately)
DELETE FROM `foreup_item_kits`;
/*!40000 ALTER TABLE `foreup_item_kits` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_item_kits` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_item_kits_taxes
DROP TABLE IF EXISTS `foreup_item_kits_taxes`;
CREATE TABLE IF NOT EXISTS `foreup_item_kits_taxes` (
  `CID` int(11) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `course_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_kit_id`,`name`,`percent`),
  CONSTRAINT `foreup_item_kits_taxes_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_item_kits_taxes: ~0 rows (approximately)
DELETE FROM `foreup_item_kits_taxes`;
/*!40000 ALTER TABLE `foreup_item_kits_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_item_kits_taxes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_item_kit_items
DROP TABLE IF EXISTS `foreup_item_kit_items`;
CREATE TABLE IF NOT EXISTS `foreup_item_kit_items` (
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(15,2) NOT NULL,
  PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  KEY `foreup_item_kit_items_ibfk_2` (`item_id`),
  CONSTRAINT `foreup_item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`) ON DELETE CASCADE,
  CONSTRAINT `foreup_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_item_kit_items: ~0 rows (approximately)
DELETE FROM `foreup_item_kit_items`;
/*!40000 ALTER TABLE `foreup_item_kit_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_item_kit_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_item_modifiers
DROP TABLE IF EXISTS `foreup_item_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_item_modifiers` (
  `item_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `override_price` decimal(10,2) DEFAULT NULL,
  `override_required` tinyint(4) NOT NULL,
  `default` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auto_select` int(11) NOT NULL,
  PRIMARY KEY (`item_id`,`modifier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_item_modifiers: ~0 rows (approximately)
DELETE FROM `foreup_item_modifiers`;
/*!40000 ALTER TABLE `foreup_item_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_item_modifiers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_item_sides
DROP TABLE IF EXISTS `foreup_item_sides`;
CREATE TABLE IF NOT EXISTS `foreup_item_sides` (
  `item_id` int(11) NOT NULL,
  `side_id` int(11) NOT NULL,
  `not_available` tinyint(4) NOT NULL DEFAULT '1',
  `default` tinyint(4) NOT NULL,
  `upgrade_price` float(15,2) NOT NULL,
  UNIQUE KEY `item_side` (`item_id`,`side_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_item_sides: ~0 rows (approximately)
DELETE FROM `foreup_item_sides`;
/*!40000 ALTER TABLE `foreup_item_sides` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_item_sides` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_links
DROP TABLE IF EXISTS `foreup_links`;
CREATE TABLE IF NOT EXISTS `foreup_links` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tee_1_name` varchar(255) NOT NULL,
  `tee_2_name` varchar(255) NOT NULL,
  `tee_3_name` varchar(255) NOT NULL,
  `tee_4_name` varchar(255) NOT NULL,
  `tee_5_name` varchar(255) NOT NULL,
  `tee_6_name` varchar(255) NOT NULL,
  `tee_7_name` varchar(255) NOT NULL,
  `tee_8_name` varchar(255) NOT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_links: ~0 rows (approximately)
DELETE FROM `foreup_links`;
/*!40000 ALTER TABLE `foreup_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_links` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_loyalty_rates
DROP TABLE IF EXISTS `foreup_loyalty_rates`;
CREATE TABLE IF NOT EXISTS `foreup_loyalty_rates` (
  `loyalty_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `type` enum('department','category','subcategory','item','item_kit') NOT NULL,
  `value` varchar(255) NOT NULL,
  `points_per_dollar` double(15,2) NOT NULL,
  `dollars_per_point` double(15,2) NOT NULL,
  `course_id` int(11) NOT NULL,
  `tee_time_index` tinyint(4) NOT NULL,
  `price_category` tinyint(4) NOT NULL,
  PRIMARY KEY (`loyalty_rate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_loyalty_rates: ~0 rows (approximately)
DELETE FROM `foreup_loyalty_rates`;
/*!40000 ALTER TABLE `foreup_loyalty_rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_loyalty_rates` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_loyalty_transactions
DROP TABLE IF EXISTS `foreup_loyalty_transactions`;
CREATE TABLE IF NOT EXISTS `foreup_loyalty_transactions` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `trans_customer` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) DEFAULT NULL,
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_description` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`trans_id`),
  KEY `foreup_loyalty_transactions_ibfk_1` (`trans_customer`),
  KEY `foreup_loyalty_transactions_ibfk_2` (`trans_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_loyalty_transactions: ~0 rows (approximately)
DELETE FROM `foreup_loyalty_transactions`;
/*!40000 ALTER TABLE `foreup_loyalty_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_loyalty_transactions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_marketing_campaigns
DROP TABLE IF EXISTS `foreup_marketing_campaigns`;
CREATE TABLE IF NOT EXISTS `foreup_marketing_campaigns` (
  `campaign_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `template` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `content_2` text NOT NULL,
  `content_3` text NOT NULL,
  `content_4` text NOT NULL,
  `content_5` text NOT NULL,
  `content_6` text NOT NULL,
  `images` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_2` varchar(255) NOT NULL,
  `image_3` varchar(255) NOT NULL,
  `image_4` varchar(255) NOT NULL,
  `d` int(11) NOT NULL,
  `send_date` datetime NOT NULL,
  `send_date_cst` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `recipients` mediumtext,
  `recipient_count` smallint(6) NOT NULL,
  `person_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `title` text NOT NULL,
  `subject` varchar(255) NOT NULL,
  `template_path` varchar(1000) NOT NULL,
  `logo_path` varchar(1000) DEFAULT NULL,
  `header` varchar(1000) DEFAULT NULL,
  `queued` tinyint(4) NOT NULL,
  `is_sent` int(1) NOT NULL DEFAULT '0',
  `attempts` tinyint(4) NOT NULL,
  `reported` tinyint(4) NOT NULL,
  `run_time` datetime NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `promotion_id` int(11) NOT NULL,
  `marketing_template_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `from_name` varchar(255) DEFAULT NULL,
  `from_email` varchar(255) DEFAULT NULL,
  `reply_to` varchar(255) DEFAULT NULL,
  `rendered_html` text,
  `json_content` text,
  `logo` varchar(255) DEFAULT NULL,
  `logo_align` varchar(255) DEFAULT NULL,
  `version` tinyint(4) NOT NULL DEFAULT '1',
  `remote_hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`campaign_id`),
  UNIQUE KEY `remote_hash` (`remote_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_marketing_campaigns: ~0 rows (approximately)
DELETE FROM `foreup_marketing_campaigns`;
/*!40000 ALTER TABLE `foreup_marketing_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_marketing_campaigns` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_marketing_images
DROP TABLE IF EXISTS `foreup_marketing_images`;
CREATE TABLE IF NOT EXISTS `foreup_marketing_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) DEFAULT NULL,
  `path` varchar(500) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table testTable.foreup_marketing_images: ~0 rows (approximately)
DELETE FROM `foreup_marketing_images`;
/*!40000 ALTER TABLE `foreup_marketing_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_marketing_images` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_marketing_templates
DROP TABLE IF EXISTS `foreup_marketing_templates`;
CREATE TABLE IF NOT EXISTS `foreup_marketing_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `template` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `style` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table testTable.foreup_marketing_templates: ~0 rows (approximately)
DELETE FROM `foreup_marketing_templates`;
/*!40000 ALTER TABLE `foreup_marketing_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_marketing_templates` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_member_account_transactions
DROP TABLE IF EXISTS `foreup_member_account_transactions`;
CREATE TABLE IF NOT EXISTS `foreup_member_account_transactions` (
  `CID` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `invoice_id` int(10) unsigned NOT NULL,
  `trans_customer` int(11) NOT NULL DEFAULT '0',
  `trans_household` int(11) NOT NULL,
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_description` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`trans_id`),
  KEY `foreup_member_account_transactions_ibfk_1` (`trans_customer`),
  KEY `foreup_member_account_transactions_ibfk_2` (`trans_user`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_member_account_transactions: ~0 rows (approximately)
DELETE FROM `foreup_member_account_transactions`;
/*!40000 ALTER TABLE `foreup_member_account_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_member_account_transactions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_modifiers
DROP TABLE IF EXISTS `foreup_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_modifiers` (
  `modifier_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` tinyint(4) NOT NULL DEFAULT '1',
  `required` tinyint(4) NOT NULL,
  `default_price` decimal(10,2) NOT NULL,
  `options` text COLLATE utf8_unicode_ci,
  `date_created` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`modifier_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_modifiers: ~0 rows (approximately)
DELETE FROM `foreup_modifiers`;
/*!40000 ALTER TABLE `foreup_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_modifiers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_modules
DROP TABLE IF EXISTS `foreup_modules`;
CREATE TABLE IF NOT EXISTS `foreup_modules` (
  `name_lang_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc_lang_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  UNIQUE KEY `name_lang_key` (`name_lang_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_modules: ~0 rows (approximately)
DELETE FROM `foreup_modules`;
/*!40000 ALTER TABLE `foreup_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_modules` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_notes
DROP TABLE IF EXISTS `foreup_notes`;
CREATE TABLE IF NOT EXISTS `foreup_notes` (
  `course_id` int(11) NOT NULL,
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) NOT NULL,
  `recipient` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `message` text NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_notes: ~0 rows (approximately)
DELETE FROM `foreup_notes`;
/*!40000 ALTER TABLE `foreup_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_notes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_people
DROP TABLE IF EXISTS `foreup_people`;
CREATE TABLE IF NOT EXISTS `foreup_people` (
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `address_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(10) NOT NULL AUTO_INCREMENT,
  `foreup_news_announcements_unsubscribe` tinyint(1) NOT NULL,
  PRIMARY KEY (`person_id`),
  KEY `email` (`email`),
  KEY `person_orderby` (`last_name`,`first_name`,`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_people: ~3 rows (approximately)
DELETE FROM `foreup_people`;
/*!40000 ALTER TABLE `foreup_people` DISABLE KEYS */;
INSERT INTO `foreup_people` (`first_name`, `last_name`, `phone_number`, `cell_phone_number`, `email`, `birthday`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`, `foreup_news_announcements_unsubscribe`) VALUES
	('Test1', '', '', '', 'Test1@Test1.com', '0000-00-00', '', '', '', '', '', '', '', 1, 0),
	('Test2', '', '', '', 'Test2@Test2.com', '0000-00-00', '', '', '', '', '', '', '', 2, 0),
	('Test3', '', '', '', 'Test3@Test3.com', '0000-00-00', '', '', '', '', '', '', '', 3, 0);
/*!40000 ALTER TABLE `foreup_people` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_permissions
DROP TABLE IF EXISTS `foreup_permissions`;
CREATE TABLE IF NOT EXISTS `foreup_permissions` (
  `CID` int(11) NOT NULL,
  `module_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(10) NOT NULL,
  PRIMARY KEY (`module_id`,`person_id`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `foreup_permissions_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `foreup_employees` (`person_id`),
  CONSTRAINT `foreup_permissions_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `foreup_modules` (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_permissions: ~0 rows (approximately)
DELETE FROM `foreup_permissions`;
/*!40000 ALTER TABLE `foreup_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_permissions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_pos_cart
DROP TABLE IF EXISTS `foreup_pos_cart`;
CREATE TABLE IF NOT EXISTS `foreup_pos_cart` (
  `cart_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `mode` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'sale',
  `suspended_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teetime_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_activity` datetime NOT NULL,
  `taxable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `override_authorization_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_pos_cart: ~0 rows (approximately)
DELETE FROM `foreup_pos_cart`;
/*!40000 ALTER TABLE `foreup_pos_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_pos_cart` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_pos_cart_customers
DROP TABLE IF EXISTS `foreup_pos_cart_customers`;
CREATE TABLE IF NOT EXISTS `foreup_pos_cart_customers` (
  `cart_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `selected` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cart_id`,`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_pos_cart_customers: ~0 rows (approximately)
DELETE FROM `foreup_pos_cart_customers`;
/*!40000 ALTER TABLE `foreup_pos_cart_customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_pos_cart_customers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_pos_cart_items
DROP TABLE IF EXISTS `foreup_pos_cart_items`;
CREATE TABLE IF NOT EXISTS `foreup_pos_cart_items` (
  `cart_id` int(10) unsigned NOT NULL,
  `line` mediumint(8) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `quantity` mediumint(9) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(5,2) NOT NULL,
  `item_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `item_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_class_id` int(10) unsigned DEFAULT NULL,
  `timeframe_id` int(10) unsigned DEFAULT NULL,
  `special_id` int(10) unsigned DEFAULT NULL,
  `params` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `punch_card_id` int(10) unsigned DEFAULT NULL,
  `tournament_id` int(10) unsigned NOT NULL,
  `invoice_id` int(10) unsigned DEFAULT NULL,
  `selected` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `unit_price_includes_tax` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cart_id`,`line`),
  KEY `line` (`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_pos_cart_items: ~0 rows (approximately)
DELETE FROM `foreup_pos_cart_items`;
/*!40000 ALTER TABLE `foreup_pos_cart_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_pos_cart_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_pos_cart_item_modifiers
DROP TABLE IF EXISTS `foreup_pos_cart_item_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_pos_cart_item_modifiers` (
  `cart_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cart_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_pos_cart_item_modifiers: ~0 rows (approximately)
DELETE FROM `foreup_pos_cart_item_modifiers`;
/*!40000 ALTER TABLE `foreup_pos_cart_item_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_pos_cart_item_modifiers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_pos_cart_item_sides
DROP TABLE IF EXISTS `foreup_pos_cart_item_sides`;
CREATE TABLE IF NOT EXISTS `foreup_pos_cart_item_sides` (
  `cart_id` int(10) unsigned NOT NULL,
  `cart_line` smallint(5) unsigned NOT NULL,
  `position` smallint(5) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`cart_id`,`cart_line`,`position`,`type`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_pos_cart_item_sides: ~0 rows (approximately)
DELETE FROM `foreup_pos_cart_item_sides`;
/*!40000 ALTER TABLE `foreup_pos_cart_item_sides` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_pos_cart_item_sides` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_pos_cart_item_side_modifiers
DROP TABLE IF EXISTS `foreup_pos_cart_item_side_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_pos_cart_item_side_modifiers` (
  `cart_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `side_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `side_position` smallint(5) unsigned NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cart_id`,`line`,`side_type`,`side_position`,`modifier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_pos_cart_item_side_modifiers: ~0 rows (approximately)
DELETE FROM `foreup_pos_cart_item_side_modifiers`;
/*!40000 ALTER TABLE `foreup_pos_cart_item_side_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_pos_cart_item_side_modifiers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_pos_cart_payments
DROP TABLE IF EXISTS `foreup_pos_cart_payments`;
CREATE TABLE IF NOT EXISTS `foreup_pos_cart_payments` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(10) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `type` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `params` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  UNIQUE KEY `payment_type` (`type`,`record_id`,`cart_id`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_pos_cart_payments: ~0 rows (approximately)
DELETE FROM `foreup_pos_cart_payments`;
/*!40000 ALTER TABLE `foreup_pos_cart_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_pos_cart_payments` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_price_classes
DROP TABLE IF EXISTS `foreup_price_classes`;
CREATE TABLE IF NOT EXISTS `foreup_price_classes` (
  `class_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cart` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `color` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`class_id`),
  UNIQUE KEY `course_id` (`course_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_price_classes: ~0 rows (approximately)
DELETE FROM `foreup_price_classes`;
/*!40000 ALTER TABLE `foreup_price_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_price_classes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_price_colors
DROP TABLE IF EXISTS `foreup_price_colors`;
CREATE TABLE IF NOT EXISTS `foreup_price_colors` (
  `course_id` int(10) unsigned NOT NULL,
  `price_category` smallint(5) unsigned NOT NULL,
  `color` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`course_id`,`price_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_price_colors: ~0 rows (approximately)
DELETE FROM `foreup_price_colors`;
/*!40000 ALTER TABLE `foreup_price_colors` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_price_colors` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_promotions
DROP TABLE IF EXISTS `foreup_promotions`;
CREATE TABLE IF NOT EXISTS `foreup_promotions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_definition_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `expiration_date` date DEFAULT NULL,
  `redeemed` char(1) DEFAULT 'N',
  `deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_promotions: ~0 rows (approximately)
DELETE FROM `foreup_promotions`;
/*!40000 ALTER TABLE `foreup_promotions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_promotions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_promotion_definitions
DROP TABLE IF EXISTS `foreup_promotion_definitions`;
CREATE TABLE IF NOT EXISTS `foreup_promotion_definitions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `number_used` int(255) DEFAULT '0',
  `number_issued` int(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `online_code` varchar(255) DEFAULT NULL,
  `amount_type` varchar(100) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `valid_for` varchar(255) DEFAULT NULL,
  `Sunday` tinyint(1) DEFAULT '1',
  `Monday` tinyint(1) DEFAULT '1',
  `Tuesday` tinyint(1) NOT NULL DEFAULT '1',
  `Wednesday` tinyint(1) NOT NULL DEFAULT '1',
  `Thursday` tinyint(1) NOT NULL DEFAULT '1',
  `Friday` tinyint(1) NOT NULL DEFAULT '1',
  `Saturday` tinyint(1) NOT NULL DEFAULT '1',
  `valid_between_from` varchar(255) DEFAULT NULL,
  `valid_between_to` varchar(255) DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  `buy_quantity` int(11) DEFAULT NULL,
  `get_quantity` int(11) DEFAULT NULL,
  `min_purchase` double DEFAULT NULL,
  `additional_details` text,
  `rules` text,
  `text_text` text,
  `email_text` text,
  `expiration_date` date DEFAULT '3000-01-01',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `bogo` enum('bogo','discount') NOT NULL,
  `coupon_limit` enum('one','unlimited') NOT NULL,
  `item_id` int(255) NOT NULL DEFAULT '0',
  `discount_type` tinyint(10) NOT NULL DEFAULT '0',
  `category_type` varchar(100) NOT NULL DEFAULT '',
  `subcategory_type` varchar(100) NOT NULL DEFAULT '',
  `department_type` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_promotion_definitions: ~0 rows (approximately)
DELETE FROM `foreup_promotion_definitions`;
/*!40000 ALTER TABLE `foreup_promotion_definitions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_promotion_definitions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_punch_cards
DROP TABLE IF EXISTS `foreup_punch_cards`;
CREATE TABLE IF NOT EXISTS `foreup_punch_cards` (
  `punch_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `punch_card_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiration_date` date DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`punch_card_id`),
  UNIQUE KEY `course_id` (`course_id`,`punch_card_number`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_punch_cards: ~0 rows (approximately)
DELETE FROM `foreup_punch_cards`;
/*!40000 ALTER TABLE `foreup_punch_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_punch_cards` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_punch_card_items
DROP TABLE IF EXISTS `foreup_punch_card_items`;
CREATE TABLE IF NOT EXISTS `foreup_punch_card_items` (
  `punch_card_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `punches` smallint(6) NOT NULL,
  `used` smallint(6) NOT NULL,
  PRIMARY KEY (`punch_card_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_punch_card_items: ~0 rows (approximately)
DELETE FROM `foreup_punch_card_items`;
/*!40000 ALTER TABLE `foreup_punch_card_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_punch_card_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_quickbooks_accounts
DROP TABLE IF EXISTS `foreup_quickbooks_accounts`;
CREATE TABLE IF NOT EXISTS `foreup_quickbooks_accounts` (
  `course_id` int(10) unsigned NOT NULL,
  `quickbooks_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `parent_quickbooks_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_number` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sub_level` tinyint(3) unsigned NOT NULL,
  `account_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `balance` decimal(10,2) unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `map_to` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`course_id`,`quickbooks_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_quickbooks_accounts: ~0 rows (approximately)
DELETE FROM `foreup_quickbooks_accounts`;
/*!40000 ALTER TABLE `foreup_quickbooks_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_quickbooks_accounts` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_quickbooks_sync
DROP TABLE IF EXISTS `foreup_quickbooks_sync`;
CREATE TABLE IF NOT EXISTS `foreup_quickbooks_sync` (
  `course_id` int(10) unsigned NOT NULL,
  `record_id` int(10) unsigned NOT NULL,
  `record_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `quickbooks_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_synced` datetime DEFAULT NULL,
  PRIMARY KEY (`course_id`,`record_id`,`record_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_quickbooks_sync: ~0 rows (approximately)
DELETE FROM `foreup_quickbooks_sync`;
/*!40000 ALTER TABLE `foreup_quickbooks_sync` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_quickbooks_sync` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_quickbuttons
DROP TABLE IF EXISTS `foreup_quickbuttons`;
CREATE TABLE IF NOT EXISTS `foreup_quickbuttons` (
  `quickbutton_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `tab` tinyint(4) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL,
  `color` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`quickbutton_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_quickbuttons: ~0 rows (approximately)
DELETE FROM `foreup_quickbuttons`;
/*!40000 ALTER TABLE `foreup_quickbuttons` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_quickbuttons` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_quickbutton_items
DROP TABLE IF EXISTS `foreup_quickbutton_items`;
CREATE TABLE IF NOT EXISTS `foreup_quickbutton_items` (
  `quickbutton_id` int(11) NOT NULL,
  `item_id` varchar(20) DEFAULT NULL,
  `item_kit_id` varchar(20) DEFAULT NULL,
  `order` tinyint(4) NOT NULL,
  UNIQUE KEY `item_id` (`quickbutton_id`,`item_id`),
  UNIQUE KEY `item_kit_id` (`quickbutton_id`,`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_quickbutton_items: ~0 rows (approximately)
DELETE FROM `foreup_quickbutton_items`;
/*!40000 ALTER TABLE `foreup_quickbutton_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_quickbutton_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_rainchecks
DROP TABLE IF EXISTS `foreup_rainchecks`;
CREATE TABLE IF NOT EXISTS `foreup_rainchecks` (
  `raincheck_id` int(11) NOT NULL AUTO_INCREMENT,
  `raincheck_number` int(11) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `teetime_id` varchar(40) NOT NULL,
  `teetime_date` date NOT NULL DEFAULT '0000-00-00',
  `teetime_time` smallint(5) unsigned NOT NULL DEFAULT '0',
  `date_issued` datetime NOT NULL,
  `date_redeemed` datetime NOT NULL,
  `players` tinyint(4) NOT NULL,
  `holes_completed` tinyint(4) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `green_fee` float(15,2) NOT NULL,
  `cart_fee` float(15,2) NOT NULL,
  `tax` float(15,2) NOT NULL,
  `total` float(15,2) NOT NULL,
  `green_fee_price_category` varchar(255) NOT NULL,
  `cart_price_category` varchar(255) NOT NULL,
  `expiry_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`raincheck_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_rainchecks: ~0 rows (approximately)
DELETE FROM `foreup_rainchecks`;
/*!40000 ALTER TABLE `foreup_rainchecks` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_rainchecks` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_receivings
DROP TABLE IF EXISTS `foreup_receivings`;
CREATE TABLE IF NOT EXISTS `foreup_receivings` (
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `receiving_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_cost` decimal(15,2) NOT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`receiving_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `employee_id` (`employee_id`),
  KEY `deleted` (`deleted`),
  CONSTRAINT `foreup_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `foreup_employees` (`person_id`),
  CONSTRAINT `foreup_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `foreup_suppliers` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_receivings: ~0 rows (approximately)
DELETE FROM `foreup_receivings`;
/*!40000 ALTER TABLE `foreup_receivings` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_receivings` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_receivings_items
DROP TABLE IF EXISTS `foreup_receivings_items`;
CREATE TABLE IF NOT EXISTS `foreup_receivings_items` (
  `CID` int(11) NOT NULL,
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` int(10) NOT NULL DEFAULT '0',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `foreup_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`),
  CONSTRAINT `foreup_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `foreup_receivings` (`receiving_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_receivings_items: ~0 rows (approximately)
DELETE FROM `foreup_receivings_items`;
/*!40000 ALTER TABLE `foreup_receivings_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_receivings_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_register_log
DROP TABLE IF EXISTS `foreup_register_log`;
CREATE TABLE IF NOT EXISTS `foreup_register_log` (
  `register_log_id` int(10) NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) NOT NULL,
  `course_id` smallint(6) NOT NULL,
  `terminal_id` int(11) NOT NULL,
  `shift_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shift_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `open_amount` double(15,2) NOT NULL,
  `close_amount` double(15,2) NOT NULL,
  `cash_sales_amount` double(15,2) NOT NULL,
  `persist` tinyint(4) NOT NULL,
  `closing_employee_id` int(11) NOT NULL,
  PRIMARY KEY (`register_log_id`),
  KEY `phppos_register_log_ibfk_1` (`employee_id`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `foreup_register_log_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `foreup_employees` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_register_log: ~0 rows (approximately)
DELETE FROM `foreup_register_log`;
/*!40000 ALTER TABLE `foreup_register_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_register_log` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_register_log_counts
DROP TABLE IF EXISTS `foreup_register_log_counts`;
CREATE TABLE IF NOT EXISTS `foreup_register_log_counts` (
  `register_log_id` int(11) NOT NULL,
  `pennies` smallint(6) NOT NULL,
  `nickels` smallint(6) NOT NULL,
  `dimes` smallint(6) NOT NULL,
  `quarters` smallint(6) NOT NULL,
  `ones` smallint(6) NOT NULL,
  `fives` smallint(6) NOT NULL,
  `tens` smallint(6) NOT NULL,
  `twenties` smallint(6) NOT NULL,
  `fifties` smallint(6) NOT NULL,
  `hundreds` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_register_log_counts: ~0 rows (approximately)
DELETE FROM `foreup_register_log_counts`;
/*!40000 ALTER TABLE `foreup_register_log_counts` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_register_log_counts` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_reservations
DROP TABLE IF EXISTS `foreup_reservations`;
CREATE TABLE IF NOT EXISTS `foreup_reservations` (
  `reservation_id` varchar(21) NOT NULL,
  `track_id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `start` bigint(20) NOT NULL,
  `end` bigint(20) NOT NULL,
  `allDay` varchar(5) NOT NULL,
  `player_count` smallint(6) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `carts` float NOT NULL,
  `paid_player_count` tinyint(4) NOT NULL DEFAULT '0',
  `paid_carts` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `details` text NOT NULL,
  `side` varchar(10) NOT NULL,
  `className` varchar(40) NOT NULL,
  `person_id` int(10) NOT NULL,
  `person_name` varchar(255) NOT NULL,
  `person_id_2` int(11) NOT NULL,
  `person_name_2` varchar(255) NOT NULL,
  `person_id_3` int(11) NOT NULL,
  `person_name_3` varchar(255) NOT NULL,
  `person_id_4` int(11) NOT NULL,
  `person_name_4` varchar(255) NOT NULL,
  `person_id_5` int(11) NOT NULL,
  `person_name_5` varchar(255) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_booked` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_cancelled` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `send_confirmation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `confirmation_emailed` tinyint(4) NOT NULL,
  `booking_source` varchar(255) NOT NULL,
  `booker_id` varchar(255) NOT NULL,
  `canceller_id` int(11) NOT NULL,
  `teed_off_time` datetime NOT NULL,
  `turn_time` datetime NOT NULL,
  `finish_time` datetime NOT NULL,
  PRIMARY KEY (`reservation_id`),
  KEY `track_id` (`track_id`),
  KEY `start` (`start`),
  KEY `end` (`end`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_reservations: ~0 rows (approximately)
DELETE FROM `foreup_reservations`;
/*!40000 ALTER TABLE `foreup_reservations` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_reservations` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales
DROP TABLE IF EXISTS `foreup_sales`;
CREATE TABLE IF NOT EXISTS `foreup_sales` (
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `teetime_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `override_authorization_id` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `table_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `terminal_id` int(11) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `deleted_by` int(11) NOT NULL,
  `deleted_at` datetime NOT NULL,
  `mobile_guid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `mobile_guid` (`mobile_guid`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `deleted` (`deleted`),
  KEY `course_id` (`course_id`),
  KEY `course_sale` (`course_id`,`sale_time`),
  KEY `teetime_id` (`teetime_id`),
  KEY `number` (`number`),
  CONSTRAINT `foreup_sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `foreup_customers` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales: ~0 rows (approximately)
DELETE FROM `foreup_sales`;
/*!40000 ALTER TABLE `foreup_sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_comps
DROP TABLE IF EXISTS `foreup_sales_comps`;
CREATE TABLE IF NOT EXISTS `foreup_sales_comps` (
  `sale_id` int(10) unsigned NOT NULL,
  `line` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `dollar_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `course_id` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sale_id`,`line`,`item_id`),
  KEY `course_id` (`course_id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_comps: ~0 rows (approximately)
DELETE FROM `foreup_sales_comps`;
/*!40000 ALTER TABLE `foreup_sales_comps` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_comps` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_invoices
DROP TABLE IF EXISTS `foreup_sales_invoices`;
CREATE TABLE IF NOT EXISTS `foreup_sales_invoices` (
  `sale_id` int(10) NOT NULL,
  `invoice_id` int(10) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `teesheet` varchar(255) NOT NULL,
  `price_category` tinyint(4) NOT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL,
  `invoice_cost_price` decimal(15,2) NOT NULL,
  `invoice_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL,
  `tax` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `profit` decimal(15,2) NOT NULL,
  `total_cost` decimal(15,2) NOT NULL,
  `receipt_only` tinyint(4) NOT NULL,
  UNIQUE KEY `sale_invoice_line` (`sale_id`,`invoice_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_sales_invoices: ~0 rows (approximately)
DELETE FROM `foreup_sales_invoices`;
/*!40000 ALTER TABLE `foreup_sales_invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_invoices` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_invoices_taxes
DROP TABLE IF EXISTS `foreup_sales_invoices_taxes`;
CREATE TABLE IF NOT EXISTS `foreup_sales_invoices_taxes` (
  `sale_id` int(10) NOT NULL,
  `invoice_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`invoice_id`,`line`,`name`,`percent`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_invoices_taxes: ~0 rows (approximately)
DELETE FROM `foreup_sales_invoices_taxes`;
/*!40000 ALTER TABLE `foreup_sales_invoices_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_invoices_taxes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_items
DROP TABLE IF EXISTS `foreup_sales_items`;
CREATE TABLE IF NOT EXISTS `foreup_sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `invoice_id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teesheet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_category` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timeframe_id` int(10) unsigned DEFAULT '0',
  `special_id` int(10) unsigned DEFAULT '0',
  `price_class_id` int(10) unsigned DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `num_splits` smallint(5) unsigned NOT NULL DEFAULT '1',
  `is_side` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'item',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `unit_price_includes_tax` tinyint(4) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL,
  `tax` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `profit` decimal(15,2) NOT NULL,
  `total_cost` decimal(15,2) NOT NULL,
  `receipt_only` tinyint(4) NOT NULL,
  `erange_code` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  KEY `sale_id` (`sale_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `type` (`type`),
  CONSTRAINT `foreup_sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_items: ~0 rows (approximately)
DELETE FROM `foreup_sales_items`;
/*!40000 ALTER TABLE `foreup_sales_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_items_modifiers
DROP TABLE IF EXISTS `foreup_sales_items_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_sales_items_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `selected_option` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selected_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_items_modifiers: ~0 rows (approximately)
DELETE FROM `foreup_sales_items_modifiers`;
/*!40000 ALTER TABLE `foreup_sales_items_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_items_modifiers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_items_taxes
DROP TABLE IF EXISTS `foreup_sales_items_taxes`;
CREATE TABLE IF NOT EXISTS `foreup_sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_id`),
  KEY `sale_id_line` (`sale_id`,`line`),
  CONSTRAINT `foreup_sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_items` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_items_taxes: ~0 rows (approximately)
DELETE FROM `foreup_sales_items_taxes`;
/*!40000 ALTER TABLE `foreup_sales_items_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_items_taxes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_items_temp
DROP TABLE IF EXISTS `foreup_sales_items_temp`;
CREATE TABLE IF NOT EXISTS `foreup_sales_items_temp` (
  `deleted` int(11) NOT NULL DEFAULT '0',
  `sale_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `serialnumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `teetime_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `terminal_id` int(11) NOT NULL DEFAULT '0',
  `sale_id` int(11) NOT NULL DEFAULT '0',
  `number` int(11) unsigned DEFAULT NULL,
  `line` int(11) NOT NULL DEFAULT '0',
  `timeframe_id` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) DEFAULT NULL,
  `item_kit_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_unit_price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actual_category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(511) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory` varchar(298) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gl_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total` decimal(15,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(15,2) NOT NULL DEFAULT '0.00',
  `profit` decimal(15,2) NOT NULL DEFAULT '0.00',
  `total_cost` decimal(15,2) NOT NULL DEFAULT '0.00',
  `receipt_only` tinyint(4) NOT NULL DEFAULT '0',
  KEY `sale_id` (`sale_id`),
  KEY `number` (`number`),
  KEY `deleted` (`deleted`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `sale_date` (`sale_date`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_sales_items_temp: 0 rows
DELETE FROM `foreup_sales_items_temp`;
/*!40000 ALTER TABLE `foreup_sales_items_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_items_temp` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_item_kits
DROP TABLE IF EXISTS `foreup_sales_item_kits`;
CREATE TABLE IF NOT EXISTS `foreup_sales_item_kits` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_kit_id` int(10) NOT NULL DEFAULT '0',
  `invoice_id` int(10) unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teesheet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_category` tinyint(4) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_kit_cost_price` decimal(15,2) NOT NULL,
  `item_kit_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(15,2) NOT NULL,
  `tax` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `profit` decimal(15,2) NOT NULL,
  `total_cost` decimal(15,2) NOT NULL,
  `receipt_only` tinyint(4) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`),
  KEY `item_kit_id` (`item_kit_id`),
  CONSTRAINT `foreup_sales_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`),
  CONSTRAINT `foreup_sales_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_item_kits: ~0 rows (approximately)
DELETE FROM `foreup_sales_item_kits`;
/*!40000 ALTER TABLE `foreup_sales_item_kits` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_item_kits` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_item_kits_taxes
DROP TABLE IF EXISTS `foreup_sales_item_kits_taxes`;
CREATE TABLE IF NOT EXISTS `foreup_sales_item_kits_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_kit_id`),
  CONSTRAINT `foreup_sales_item_kits_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_item_kits` (`sale_id`),
  CONSTRAINT `foreup_sales_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_item_kits_taxes: ~0 rows (approximately)
DELETE FROM `foreup_sales_item_kits_taxes`;
/*!40000 ALTER TABLE `foreup_sales_item_kits_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_item_kits_taxes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_payments
DROP TABLE IF EXISTS `foreup_sales_payments`;
CREATE TABLE IF NOT EXISTS `foreup_sales_payments` (
  `sale_id` int(10) NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reports_only` tinyint(4) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  `invoice_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tip_recipient` int(11) NOT NULL,
  `record_id` int(10) unsigned NOT NULL,
  `number` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  KEY `type` (`type`),
  CONSTRAINT `foreup_sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_payments: ~0 rows (approximately)
DELETE FROM `foreup_sales_payments`;
/*!40000 ALTER TABLE `foreup_sales_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_payments` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_payments_credit_cards
DROP TABLE IF EXISTS `foreup_sales_payments_credit_cards`;
CREATE TABLE IF NOT EXISTS `foreup_sales_payments_credit_cards` (
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `mercury_id` varchar(255) NOT NULL,
  `element_account_id` varchar(128) DEFAULT NULL,
  `ets_id` varchar(255) NOT NULL,
  `tran_type` varchar(255) NOT NULL,
  `amount` float(15,2) NOT NULL,
  `auth_amount` float(15,2) NOT NULL,
  `card_type` varchar(255) NOT NULL,
  `masked_account` varchar(255) NOT NULL,
  `cardholder_name` varchar(255) NOT NULL,
  `ref_no` varchar(255) NOT NULL,
  `invoice` int(11) NOT NULL AUTO_INCREMENT,
  `operator_id` varchar(255) NOT NULL,
  `terminal_name` varchar(255) NOT NULL,
  `trans_post_time` datetime NOT NULL,
  `auth_code` varchar(255) NOT NULL,
  `voice_auth_code` varchar(255) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `acq_ref_data` varchar(255) NOT NULL,
  `process_data` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_used` tinyint(4) NOT NULL,
  `amount_refunded` float(15,2) NOT NULL,
  `frequency` varchar(9) NOT NULL,
  `response_code` smallint(6) NOT NULL,
  `status` varchar(30) NOT NULL,
  `status_message` varchar(30) NOT NULL,
  `display_message` varchar(255) NOT NULL,
  `avs_result` varchar(20) NOT NULL,
  `cvv_result` varchar(20) NOT NULL,
  `tax_amount` float(15,2) NOT NULL,
  `avs_address` varchar(255) NOT NULL,
  `avs_zip` varchar(20) NOT NULL,
  `payment_id_expired` varchar(10) NOT NULL,
  `customer_code` varchar(255) NOT NULL,
  `memo` varchar(255) NOT NULL,
  `batch_no` varchar(6) NOT NULL,
  `gratuity_amount` float(15,2) NOT NULL,
  `voided` int(1) NOT NULL,
  PRIMARY KEY (`invoice`),
  KEY `course_id` (`course_id`),
  KEY `invoice` (`invoice`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_sales_payments_credit_cards: ~0 rows (approximately)
DELETE FROM `foreup_sales_payments_credit_cards`;
/*!40000 ALTER TABLE `foreup_sales_payments_credit_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_payments_credit_cards` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_suspended
DROP TABLE IF EXISTS `foreup_sales_suspended`;
CREATE TABLE IF NOT EXISTS `foreup_sales_suspended` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `course_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `deleted` (`deleted`),
  CONSTRAINT `foreup_sales_suspended_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `foreup_employees` (`person_id`),
  CONSTRAINT `foreup_sales_suspended_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `foreup_customers` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_suspended: ~0 rows (approximately)
DELETE FROM `foreup_sales_suspended`;
/*!40000 ALTER TABLE `foreup_sales_suspended` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_suspended` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_suspended_items
DROP TABLE IF EXISTS `foreup_sales_suspended_items`;
CREATE TABLE IF NOT EXISTS `foreup_sales_suspended_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_category` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `foreup_sales_suspended_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`),
  CONSTRAINT `foreup_sales_suspended_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_suspended` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_suspended_items: ~0 rows (approximately)
DELETE FROM `foreup_sales_suspended_items`;
/*!40000 ALTER TABLE `foreup_sales_suspended_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_suspended_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_suspended_items_modifiers
DROP TABLE IF EXISTS `foreup_sales_suspended_items_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_sales_suspended_items_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `selected_option` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selected_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_suspended_items_modifiers: ~0 rows (approximately)
DELETE FROM `foreup_sales_suspended_items_modifiers`;
/*!40000 ALTER TABLE `foreup_sales_suspended_items_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_suspended_items_modifiers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_suspended_items_taxes
DROP TABLE IF EXISTS `foreup_sales_suspended_items_taxes`;
CREATE TABLE IF NOT EXISTS `foreup_sales_suspended_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `foreup_sales_suspended_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_suspended_items` (`sale_id`),
  CONSTRAINT `foreup_sales_suspended_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_suspended_items_taxes: ~0 rows (approximately)
DELETE FROM `foreup_sales_suspended_items_taxes`;
/*!40000 ALTER TABLE `foreup_sales_suspended_items_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_suspended_items_taxes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_suspended_item_kits
DROP TABLE IF EXISTS `foreup_sales_suspended_item_kits`;
CREATE TABLE IF NOT EXISTS `foreup_sales_suspended_item_kits` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_kit_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_kit_cost_price` decimal(15,2) NOT NULL,
  `item_kit_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`),
  KEY `item_kit_id` (`item_kit_id`),
  CONSTRAINT `foreup_sales_suspended_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`),
  CONSTRAINT `foreup_sales_suspended_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_suspended` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_suspended_item_kits: ~0 rows (approximately)
DELETE FROM `foreup_sales_suspended_item_kits`;
/*!40000 ALTER TABLE `foreup_sales_suspended_item_kits` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_suspended_item_kits` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_suspended_item_kits_taxes
DROP TABLE IF EXISTS `foreup_sales_suspended_item_kits_taxes`;
CREATE TABLE IF NOT EXISTS `foreup_sales_suspended_item_kits_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_kit_id`),
  CONSTRAINT `foreup_sales_suspended_item_kits_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_suspended_item_kits` (`sale_id`),
  CONSTRAINT `foreup_sales_suspended_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_suspended_item_kits_taxes: ~0 rows (approximately)
DELETE FROM `foreup_sales_suspended_item_kits_taxes`;
/*!40000 ALTER TABLE `foreup_sales_suspended_item_kits_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_suspended_item_kits_taxes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_suspended_payments
DROP TABLE IF EXISTS `foreup_sales_suspended_payments`;
CREATE TABLE IF NOT EXISTS `foreup_sales_suspended_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  CONSTRAINT `foreup_sales_suspended_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_sales_suspended` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sales_suspended_payments: ~0 rows (approximately)
DELETE FROM `foreup_sales_suspended_payments`;
/*!40000 ALTER TABLE `foreup_sales_suspended_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_suspended_payments` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sales_tournaments
DROP TABLE IF EXISTS `foreup_sales_tournaments`;
CREATE TABLE IF NOT EXISTS `foreup_sales_tournaments` (
  `sale_id` int(10) NOT NULL,
  `tournament_id` int(10) NOT NULL,
  `teesheet` varchar(255) NOT NULL,
  `price_category` tinyint(4) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL,
  `tournament_cost_price` decimal(15,2) NOT NULL,
  `tournament_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL,
  `taxes_paid` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_sales_tournaments: ~0 rows (approximately)
DELETE FROM `foreup_sales_tournaments`;
/*!40000 ALTER TABLE `foreup_sales_tournaments` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sales_tournaments` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_schedules
DROP TABLE IF EXISTS `foreup_schedules`;
CREATE TABLE IF NOT EXISTS `foreup_schedules` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `online_open_time` varchar(10) NOT NULL DEFAULT '0900',
  `online_close_time` varchar(10) NOT NULL DEFAULT '1800',
  `online_time_limit` smallint(6) NOT NULL DEFAULT '120',
  `increment` float(4,1) NOT NULL DEFAULT '8.0',
  `frontnine` smallint(6) NOT NULL DEFAULT '200',
  `days_out` tinyint(4) NOT NULL,
  `days_in_booking_window` tinyint(4) NOT NULL DEFAULT '7',
  `minimum_players` tinyint(4) NOT NULL DEFAULT '2',
  `limit_holes` tinyint(4) NOT NULL,
  `booking_carts` tinyint(4) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `default_price_class` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_schedules: ~0 rows (approximately)
DELETE FROM `foreup_schedules`;
/*!40000 ALTER TABLE `foreup_schedules` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_schedules` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_seasonal_timeframes
DROP TABLE IF EXISTS `foreup_seasonal_timeframes`;
CREATE TABLE IF NOT EXISTS `foreup_seasonal_timeframes` (
  `timeframe_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL,
  `season_id` int(10) unsigned NOT NULL,
  `timeframe_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `monday` tinyint(1) unsigned NOT NULL,
  `tuesday` tinyint(1) unsigned NOT NULL,
  `wednesday` tinyint(1) unsigned NOT NULL,
  `thursday` tinyint(1) unsigned NOT NULL,
  `friday` tinyint(1) unsigned NOT NULL,
  `saturday` tinyint(1) unsigned NOT NULL,
  `sunday` tinyint(1) unsigned NOT NULL,
  `price1` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price2` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price3` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price4` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price5` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price6` decimal(15,2) NOT NULL DEFAULT '0.00',
  `start_time` smallint(5) unsigned NOT NULL,
  `end_time` smallint(5) unsigned NOT NULL,
  `default` tinyint(1) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`timeframe_id`),
  KEY `class_id` (`class_id`),
  KEY `season_id` (`season_id`),
  KEY `start_time` (`start_time`,`end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_seasonal_timeframes: ~0 rows (approximately)
DELETE FROM `foreup_seasonal_timeframes`;
/*!40000 ALTER TABLE `foreup_seasonal_timeframes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_seasonal_timeframes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_seasons
DROP TABLE IF EXISTS `foreup_seasons`;
CREATE TABLE IF NOT EXISTS `foreup_seasons` (
  `season_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `teesheet_id` int(10) unsigned NOT NULL,
  `season_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `holiday` tinyint(1) unsigned NOT NULL,
  `increment` decimal(15,2) NOT NULL,
  `front_nine` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `online_booking` tinyint(1) unsigned NOT NULL,
  `online_open_time` smallint(5) unsigned NOT NULL,
  `online_close_time` smallint(5) unsigned NOT NULL,
  `default` tinyint(1) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`season_id`),
  KEY `start_date` (`start_date`,`end_date`),
  KEY `holiday` (`holiday`),
  KEY `course_id` (`course_id`),
  KEY `online_open_time` (`online_open_time`,`online_close_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_seasons: ~0 rows (approximately)
DELETE FROM `foreup_seasons`;
/*!40000 ALTER TABLE `foreup_seasons` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_seasons` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_season_price_classes
DROP TABLE IF EXISTS `foreup_season_price_classes`;
CREATE TABLE IF NOT EXISTS `foreup_season_price_classes` (
  `season_id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`season_id`,`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_season_price_classes: ~0 rows (approximately)
DELETE FROM `foreup_season_price_classes`;
/*!40000 ALTER TABLE `foreup_season_price_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_season_price_classes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sendhub_accounts
DROP TABLE IF EXISTS `foreup_sendhub_accounts`;
CREATE TABLE IF NOT EXISTS `foreup_sendhub_accounts` (
  `phone_number` varchar(10) NOT NULL,
  `sendhub_id` varchar(255) NOT NULL,
  `text_reminder_unsubscribe` tinyint(1) NOT NULL,
  UNIQUE KEY `phone_number` (`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_sendhub_accounts: ~0 rows (approximately)
DELETE FROM `foreup_sendhub_accounts`;
/*!40000 ALTER TABLE `foreup_sendhub_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sendhub_accounts` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sendhub_messages
DROP TABLE IF EXISTS `foreup_sendhub_messages`;
CREATE TABLE IF NOT EXISTS `foreup_sendhub_messages` (
  `campaign_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `sent` datetime NOT NULL,
  `confirmed` tinyint(4) NOT NULL,
  KEY `campaign_id` (`campaign_id`,`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_sendhub_messages: ~0 rows (approximately)
DELETE FROM `foreup_sendhub_messages`;
/*!40000 ALTER TABLE `foreup_sendhub_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sendhub_messages` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sendhub_message_contacts
DROP TABLE IF EXISTS `foreup_sendhub_message_contacts`;
CREATE TABLE IF NOT EXISTS `foreup_sendhub_message_contacts` (
  `message_id` int(11) NOT NULL,
  `sendhub_id` int(11) NOT NULL,
  KEY `message_id` (`message_id`,`sendhub_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_sendhub_message_contacts: ~0 rows (approximately)
DELETE FROM `foreup_sendhub_message_contacts`;
/*!40000 ALTER TABLE `foreup_sendhub_message_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_sendhub_message_contacts` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_sessions
DROP TABLE IF EXISTS `foreup_sessions`;
CREATE TABLE IF NOT EXISTS `foreup_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_sessions: ~2 rows (approximately)
DELETE FROM `foreup_sessions`;
/*!40000 ALTER TABLE `foreup_sessions` DISABLE KEYS */;
INSERT INTO `foreup_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
	('06baa8d8d022f5b9a9a1127494b9c1d1', '0.0.0.0', '0', 1427752823, NULL),
	('078b3012ee565b41995df17fdfc0aca5', '0.0.0.0', '0', 1427752976, NULL),
	('1d4c0634091ce0424afb293111e7d0c8', '0.0.0.0', '0', 1427753463, NULL),
	('a10f79db5b983837c5a02fbb041b1977', '0.0.0.0', '0', 1427752995, NULL),
	('a27eb2723344676b843e5332b7f9490d', '0.0.0.0', '0', 1427752938, NULL),
	('ac99b8ec6a0dd3db4cb683d1d80e8e38', '0.0.0.0', '0', 1427752756, NULL),
	('bbe9cd97402407b3f0ca259d1e6a29c5', '0.0.0.0', '0', 1427753492, NULL),
	('cdf2b9f1a11667b28a6b91f2fe6b255a', '0.0.0.0', '0', 1427752871, NULL),
	('d3dc13131a7d3b08d4be8c320a91b35e', '0.0.0.0', '0', 1427753448, NULL),
	('f8b969fb3fed4729a74598f5e73fee06', '0.0.0.0', '0', 1427752674, NULL);
/*!40000 ALTER TABLE `foreup_sessions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_session_history
DROP TABLE IF EXISTS `foreup_session_history`;
CREATE TABLE IF NOT EXISTS `foreup_session_history` (
  `session_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `url` text NOT NULL,
  `sess_data` mediumtext NOT NULL,
  `basket_data` text NOT NULL,
  PRIMARY KEY (`session_log_id`),
  KEY `course_id` (`course_id`,`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_session_history: ~0 rows (approximately)
DELETE FROM `foreup_session_history`;
/*!40000 ALTER TABLE `foreup_session_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_session_history` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_specials
DROP TABLE IF EXISTS `foreup_specials`;
CREATE TABLE IF NOT EXISTS `foreup_specials` (
  `special_id` int(11) NOT NULL AUTO_INCREMENT,
  `teesheet_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `price_1` decimal(15,2) NOT NULL,
  `price_2` decimal(15,2) NOT NULL,
  `price_3` decimal(15,2) NOT NULL,
  `price_4` decimal(15,2) NOT NULL,
  `price_5` decimal(15,2) NOT NULL,
  `price_6` decimal(15,2) NOT NULL,
  PRIMARY KEY (`special_id`),
  KEY `teesheet_id` (`teesheet_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_specials: ~0 rows (approximately)
DELETE FROM `foreup_specials`;
/*!40000 ALTER TABLE `foreup_specials` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_specials` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_special_times
DROP TABLE IF EXISTS `foreup_special_times`;
CREATE TABLE IF NOT EXISTS `foreup_special_times` (
  `special_id` int(11) NOT NULL,
  `time` int(4) NOT NULL,
  PRIMARY KEY (`special_id`,`time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_special_times: ~0 rows (approximately)
DELETE FROM `foreup_special_times`;
/*!40000 ALTER TABLE `foreup_special_times` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_special_times` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_suppliers
DROP TABLE IF EXISTS `foreup_suppliers`;
CREATE TABLE IF NOT EXISTS `foreup_suppliers` (
  `CID` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `acc_num` (`course_id`,`account_number`),
  KEY `person_id` (`person_id`),
  KEY `deleted` (`deleted`),
  CONSTRAINT `foreup_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `foreup_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_suppliers: ~0 rows (approximately)
DELETE FROM `foreup_suppliers`;
/*!40000 ALTER TABLE `foreup_suppliers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_suppliers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_tables
DROP TABLE IF EXISTS `foreup_tables`;
CREATE TABLE IF NOT EXISTS `foreup_tables` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `manager_auth_id` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `course_id` int(11) NOT NULL,
  `table_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `table_id` (`course_id`,`table_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `deleted` (`deleted`),
  CONSTRAINT `foreup_tables_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `foreup_employees` (`person_id`),
  CONSTRAINT `foreup_tables_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `foreup_customers` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_tables: ~0 rows (approximately)
DELETE FROM `foreup_tables`;
/*!40000 ALTER TABLE `foreup_tables` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_tables` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_buttons
DROP TABLE IF EXISTS `foreup_table_buttons`;
CREATE TABLE IF NOT EXISTS `foreup_table_buttons` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sub_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`name`,`course_id`,`category`,`sub_category`),
  KEY `order` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_buttons: ~0 rows (approximately)
DELETE FROM `foreup_table_buttons`;
/*!40000 ALTER TABLE `foreup_table_buttons` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_buttons` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_comps
DROP TABLE IF EXISTS `foreup_table_comps`;
CREATE TABLE IF NOT EXISTS `foreup_table_comps` (
  `sale_id` int(10) unsigned NOT NULL,
  `line` smallint(5) unsigned NOT NULL DEFAULT '0',
  `item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `course_id` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sale_id`,`line`,`item_id`),
  KEY `course_id` (`course_id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_comps: ~0 rows (approximately)
DELETE FROM `foreup_table_comps`;
/*!40000 ALTER TABLE `foreup_table_comps` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_comps` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_customers
DROP TABLE IF EXISTS `foreup_table_customers`;
CREATE TABLE IF NOT EXISTS `foreup_table_customers` (
  `sale_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`sale_id`,`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table testTable.foreup_table_customers: ~0 rows (approximately)
DELETE FROM `foreup_table_customers`;
/*!40000 ALTER TABLE `foreup_table_customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_customers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_items
DROP TABLE IF EXISTS `foreup_table_items`;
CREATE TABLE IF NOT EXISTS `foreup_table_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `seat` smallint(3) unsigned NOT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  `is_ordered` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_paid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `total_splits` smallint(5) unsigned NOT NULL,
  `paid_splits` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `foreup_table_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `foreup_items` (`item_id`),
  CONSTRAINT `foreup_table_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_tables` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_items: ~0 rows (approximately)
DELETE FROM `foreup_table_items`;
/*!40000 ALTER TABLE `foreup_table_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_items_modifiers
DROP TABLE IF EXISTS `foreup_table_items_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_table_items_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sale_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_items_modifiers: ~0 rows (approximately)
DELETE FROM `foreup_table_items_modifiers`;
/*!40000 ALTER TABLE `foreup_table_items_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_items_modifiers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_item_kits
DROP TABLE IF EXISTS `foreup_table_item_kits`;
CREATE TABLE IF NOT EXISTS `foreup_table_item_kits` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_kit_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_kit_cost_price` decimal(15,2) NOT NULL,
  `item_kit_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`),
  KEY `item_kit_id` (`item_kit_id`),
  CONSTRAINT `foreup_table_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`),
  CONSTRAINT `foreup_table_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `foreup_tables` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_item_kits: ~0 rows (approximately)
DELETE FROM `foreup_table_item_kits`;
/*!40000 ALTER TABLE `foreup_table_item_kits` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_item_kits` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_item_kits_taxes
DROP TABLE IF EXISTS `foreup_table_item_kits_taxes`;
CREATE TABLE IF NOT EXISTS `foreup_table_item_kits_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_kit_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_kit_id`),
  CONSTRAINT `foreup_table_item_kits_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_table_item_kits` (`sale_id`),
  CONSTRAINT `foreup_table_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `foreup_item_kits` (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_item_kits_taxes: ~0 rows (approximately)
DELETE FROM `foreup_table_item_kits_taxes`;
/*!40000 ALTER TABLE `foreup_table_item_kits_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_item_kits_taxes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_item_sides
DROP TABLE IF EXISTS `foreup_table_item_sides`;
CREATE TABLE IF NOT EXISTS `foreup_table_item_sides` (
  `sale_id` int(10) unsigned NOT NULL,
  `cart_line` smallint(5) unsigned NOT NULL,
  `position` smallint(5) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`sale_id`,`cart_line`,`position`,`type`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_item_sides: ~0 rows (approximately)
DELETE FROM `foreup_table_item_sides`;
/*!40000 ALTER TABLE `foreup_table_item_sides` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_item_sides` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_item_side_modifiers
DROP TABLE IF EXISTS `foreup_table_item_side_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_table_item_side_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `side_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `side_position` smallint(5) unsigned NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sale_id`,`line`,`side_type`,`side_position`,`modifier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_item_side_modifiers: ~0 rows (approximately)
DELETE FROM `foreup_table_item_side_modifiers`;
/*!40000 ALTER TABLE `foreup_table_item_side_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_item_side_modifiers` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_layouts
DROP TABLE IF EXISTS `foreup_table_layouts`;
CREATE TABLE IF NOT EXISTS `foreup_table_layouts` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order` tinyint(3) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`layout_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_layouts: ~0 rows (approximately)
DELETE FROM `foreup_table_layouts`;
/*!40000 ALTER TABLE `foreup_table_layouts` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_layouts` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_layout_objects
DROP TABLE IF EXISTS `foreup_table_layout_objects`;
CREATE TABLE IF NOT EXISTS `foreup_table_layout_objects` (
  `object_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `layout_id` int(10) unsigned NOT NULL,
  `label` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_x` decimal(7,1) unsigned DEFAULT '0.0',
  `pos_y` decimal(7,1) unsigned DEFAULT '0.0',
  `width` smallint(4) unsigned DEFAULT NULL,
  `height` smallint(4) unsigned DEFAULT NULL,
  `rotation` smallint(3) unsigned DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`object_id`),
  UNIQUE KEY `object_label` (`layout_id`,`label`),
  KEY `layout_id` (`layout_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_layout_objects: ~0 rows (approximately)
DELETE FROM `foreup_table_layout_objects`;
/*!40000 ALTER TABLE `foreup_table_layout_objects` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_layout_objects` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_payments
DROP TABLE IF EXISTS `foreup_table_payments`;
CREATE TABLE IF NOT EXISTS `foreup_table_payments` (
  `sale_id` int(10) NOT NULL,
  `receipt_id` int(10) unsigned NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `credit_card_invoice_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sale_id`,`receipt_id`,`payment_type`),
  KEY `credit_card_invoice_id` (`credit_card_invoice_id`),
  CONSTRAINT `foreup_table_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `foreup_tables` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_payments: ~0 rows (approximately)
DELETE FROM `foreup_table_payments`;
/*!40000 ALTER TABLE `foreup_table_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_payments` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_receipts
DROP TABLE IF EXISTS `foreup_table_receipts`;
CREATE TABLE IF NOT EXISTS `foreup_table_receipts` (
  `receipt_id` int(10) unsigned NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `taxable` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `customer_id` int(10) unsigned DEFAULT NULL,
  `auto_gratuity` decimal(15,2) NOT NULL,
  `date_paid` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sale_id`,`receipt_id`),
  KEY `sale_id` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_receipts: ~0 rows (approximately)
DELETE FROM `foreup_table_receipts`;
/*!40000 ALTER TABLE `foreup_table_receipts` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_receipts` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_receipt_items
DROP TABLE IF EXISTS `foreup_table_receipt_items`;
CREATE TABLE IF NOT EXISTS `foreup_table_receipt_items` (
  `receipt_id` int(10) unsigned NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `line` smallint(4) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`receipt_id`,`sale_id`,`item_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_receipt_items: ~0 rows (approximately)
DELETE FROM `foreup_table_receipt_items`;
/*!40000 ALTER TABLE `foreup_table_receipt_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_receipt_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_receipt_item_kits
DROP TABLE IF EXISTS `foreup_table_receipt_item_kits`;
CREATE TABLE IF NOT EXISTS `foreup_table_receipt_item_kits` (
  `receipt_id` int(10) unsigned NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `item_kit_id` int(10) unsigned NOT NULL,
  `line` smallint(4) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`receipt_id`,`sale_id`,`item_kit_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_receipt_item_kits: ~0 rows (approximately)
DELETE FROM `foreup_table_receipt_item_kits`;
/*!40000 ALTER TABLE `foreup_table_receipt_item_kits` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_receipt_item_kits` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_tickets
DROP TABLE IF EXISTS `foreup_table_tickets`;
CREATE TABLE IF NOT EXISTS `foreup_table_tickets` (
  `sale_id` int(10) unsigned NOT NULL,
  `ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `terminal_id` int(10) unsigned DEFAULT NULL,
  `printed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_ordered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_completed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ticket_id`),
  KEY `sale_id` (`sale_id`),
  KEY `terminal_id` (`terminal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_tickets: ~0 rows (approximately)
DELETE FROM `foreup_table_tickets`;
/*!40000 ALTER TABLE `foreup_table_tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_tickets` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_ticket_items
DROP TABLE IF EXISTS `foreup_table_ticket_items`;
CREATE TABLE IF NOT EXISTS `foreup_table_ticket_items` (
  `ticket_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `line` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`ticket_id`,`item_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_ticket_items: ~0 rows (approximately)
DELETE FROM `foreup_table_ticket_items`;
/*!40000 ALTER TABLE `foreup_table_ticket_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_ticket_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_table_ticket_item_kits
DROP TABLE IF EXISTS `foreup_table_ticket_item_kits`;
CREATE TABLE IF NOT EXISTS `foreup_table_ticket_item_kits` (
  `ticket_id` int(10) unsigned NOT NULL,
  `item_kit_id` int(10) unsigned NOT NULL,
  `line` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`ticket_id`,`item_kit_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.foreup_table_ticket_item_kits: ~0 rows (approximately)
DELETE FROM `foreup_table_ticket_item_kits`;
/*!40000 ALTER TABLE `foreup_table_ticket_item_kits` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_table_ticket_item_kits` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_teesheet
DROP TABLE IF EXISTS `foreup_teesheet`;
CREATE TABLE IF NOT EXISTS `foreup_teesheet` (
  `teesheet_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `TSID` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `associated_courses` text NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `online_open_time` varchar(10) NOT NULL DEFAULT '0900',
  `online_close_time` varchar(10) NOT NULL DEFAULT '1800',
  `increment` float(4,1) NOT NULL DEFAULT '8.0',
  `frontnine` smallint(6) NOT NULL DEFAULT '200',
  `days_out` tinyint(4) NOT NULL DEFAULT '0',
  `days_in_booking_window` tinyint(4) NOT NULL DEFAULT '7',
  `minimum_players` tinyint(4) NOT NULL DEFAULT '2',
  `limit_holes` tinyint(4) NOT NULL DEFAULT '0',
  `booking_carts` tinyint(4) NOT NULL,
  `fntime` smallint(6) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `default_price_class` varchar(5) NOT NULL,
  `color` char(7) NOT NULL,
  `online_booking` tinyint(1) NOT NULL,
  `online_rate_setting` tinyint(4) NOT NULL,
  `require_credit_card` tinyint(1) unsigned NOT NULL,
  `send_thank_you` tinyint(4) NOT NULL,
  `thank_you_campaign_id` int(11) NOT NULL,
  `thank_you_next_send` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `specials_enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`teesheet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_teesheet: ~0 rows (approximately)
DELETE FROM `foreup_teesheet`;
/*!40000 ALTER TABLE `foreup_teesheet` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_teesheet` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_teesheet_notes
DROP TABLE IF EXISTS `foreup_teesheet_notes`;
CREATE TABLE IF NOT EXISTS `foreup_teesheet_notes` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `teesheet_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `note` text NOT NULL,
  PRIMARY KEY (`note_id`),
  UNIQUE KEY `teesheet_id_2` (`teesheet_id`,`date`),
  KEY `teesheet_id` (`teesheet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_teesheet_notes: ~0 rows (approximately)
DELETE FROM `foreup_teesheet_notes`;
/*!40000 ALTER TABLE `foreup_teesheet_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_teesheet_notes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_teesheet_restrictions
DROP TABLE IF EXISTS `foreup_teesheet_restrictions`;
CREATE TABLE IF NOT EXISTS `foreup_teesheet_restrictions` (
  `teesheet_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `limit` varchar(255) NOT NULL,
  `Mon` tinyint(4) NOT NULL,
  `Tue` tinyint(4) NOT NULL,
  `Wed` tinyint(4) NOT NULL,
  `Thu` tinyint(4) NOT NULL,
  `Fri` tinyint(4) NOT NULL,
  `Sat` tinyint(4) NOT NULL,
  `Sun` tinyint(4) NOT NULL,
  `start_time` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_teesheet_restrictions: ~0 rows (approximately)
DELETE FROM `foreup_teesheet_restrictions`;
/*!40000 ALTER TABLE `foreup_teesheet_restrictions` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_teesheet_restrictions` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_teetime
DROP TABLE IF EXISTS `foreup_teetime`;
CREATE TABLE IF NOT EXISTS `foreup_teetime` (
  `TTID` varchar(21) NOT NULL,
  `TSID` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `start` bigint(20) NOT NULL,
  `end` bigint(20) NOT NULL,
  `allDay` varchar(5) NOT NULL,
  `player_count` smallint(6) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `carts` float NOT NULL,
  `paid_player_count` tinyint(4) NOT NULL DEFAULT '0',
  `paid_carts` tinyint(4) NOT NULL DEFAULT '0',
  `clubs` tinyint(4) NOT NULL,
  `title` varchar(50) NOT NULL,
  `cplayer_count` tinyint(4) NOT NULL,
  `choles` tinyint(4) NOT NULL,
  `ccarts` float NOT NULL,
  `cpayment` varchar(10) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `details` text NOT NULL,
  `side` varchar(10) NOT NULL,
  `className` varchar(40) NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `price_class_1` mediumint(9) NOT NULL,
  `person_paid_1` tinyint(4) NOT NULL,
  `cart_paid_1` tinyint(4) NOT NULL,
  `person_name` varchar(255) NOT NULL,
  `person_id_2` int(11) NOT NULL,
  `price_class_2` mediumint(9) NOT NULL,
  `person_paid_2` tinyint(4) NOT NULL,
  `cart_paid_2` tinyint(4) NOT NULL,
  `person_name_2` varchar(255) NOT NULL,
  `person_id_3` int(11) NOT NULL,
  `price_class_3` mediumint(9) NOT NULL,
  `person_paid_3` tinyint(4) NOT NULL,
  `cart_paid_3` tinyint(4) NOT NULL,
  `person_name_3` varchar(255) NOT NULL,
  `person_id_4` int(11) NOT NULL,
  `price_class_4` mediumint(9) NOT NULL,
  `person_paid_4` tinyint(4) NOT NULL,
  `cart_paid_4` tinyint(4) NOT NULL,
  `person_name_4` varchar(255) NOT NULL,
  `person_id_5` int(11) NOT NULL,
  `price_class_5` mediumint(9) NOT NULL,
  `person_paid_5` tinyint(4) NOT NULL,
  `cart_paid_5` tinyint(4) NOT NULL,
  `person_name_5` varchar(255) NOT NULL,
  `default_cart_fee` smallint(5) unsigned NOT NULL,
  `default_price_category` smallint(5) unsigned NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_booked` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_cancelled` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `send_confirmation` tinyint(4) NOT NULL,
  `confirmation_emailed` tinyint(4) NOT NULL,
  `thank_you_emailed` tinyint(4) NOT NULL,
  `booking_source` varchar(255) NOT NULL,
  `booker_id` varchar(255) NOT NULL,
  `canceller_id` int(11) NOT NULL,
  `teetime_id` int(11) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `teed_off_time` datetime NOT NULL,
  `turn_time` datetime NOT NULL,
  `finish_time` datetime NOT NULL,
  `api_id` int(11) NOT NULL,
  `raincheck_players_issued` tinyint(4) NOT NULL,
  `front_paid` tinyint(4) NOT NULL,
  `back_paid` tinyint(4) NOT NULL,
  `front_player_count` tinyint(4) NOT NULL,
  `back_player_count` tinyint(4) NOT NULL,
  `front_carts` tinyint(4) NOT NULL,
  `back_carts` tinyint(4) NOT NULL,
  `front_paid_carts` tinyint(4) NOT NULL,
  `back_paid_carts` tinyint(4) NOT NULL,
  `cart_num_1` varchar(20) NOT NULL,
  `cart_num_2` varchar(20) NOT NULL,
  `promo_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`TTID`),
  KEY `teesheet_id` (`teesheet_id`),
  KEY `start` (`start`),
  KEY `end` (`end`),
  KEY `status` (`status`),
  KEY `player_count` (`player_count`),
  KEY `side` (`side`),
  KEY `teesheet_id_start` (`teesheet_id`,`start`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_teetime: ~0 rows (approximately)
DELETE FROM `foreup_teetime`;
/*!40000 ALTER TABLE `foreup_teetime` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_teetime` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_teetimes_bartered
DROP TABLE IF EXISTS `foreup_teetimes_bartered`;
CREATE TABLE IF NOT EXISTS `foreup_teetimes_bartered` (
  `teetime_id` varchar(21) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `start` bigint(20) NOT NULL,
  `end` bigint(20) NOT NULL,
  `player_count` tinyint(4) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `carts` float NOT NULL,
  `date_booked` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `booker_id` varchar(255) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `api_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`teetime_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_teetimes_bartered: ~0 rows (approximately)
DELETE FROM `foreup_teetimes_bartered`;
/*!40000 ALTER TABLE `foreup_teetimes_bartered` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_teetimes_bartered` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_teetime_standbys
DROP TABLE IF EXISTS `foreup_teetime_standbys`;
CREATE TABLE IF NOT EXISTS `foreup_teetime_standbys` (
  `standby_id` int(11) NOT NULL AUTO_INCREMENT,
  `teesheet_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `time` datetime NOT NULL,
  `players` tinyint(4) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `details` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  PRIMARY KEY (`standby_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_teetime_standbys: ~0 rows (approximately)
DELETE FROM `foreup_teetime_standbys`;
/*!40000 ALTER TABLE `foreup_teetime_standbys` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_teetime_standbys` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_terminals
DROP TABLE IF EXISTS `foreup_terminals`;
CREATE TABLE IF NOT EXISTS `foreup_terminals` (
  `terminal_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `quickbutton_tab` tinyint(4) NOT NULL DEFAULT '1',
  `course_id` int(11) NOT NULL,
  `mercury_id` varchar(255) NOT NULL,
  `mercury_password` varchar(255) NOT NULL,
  `element_account_id` varchar(255) DEFAULT NULL,
  `element_account_token` varchar(255) DEFAULT NULL,
  `element_application_id` varchar(255) DEFAULT NULL,
  `element_acceptor_id` varchar(255) DEFAULT NULL,
  `ets_key` varchar(255) NOT NULL,
  `e2e_account_id` varchar(255) NOT NULL,
  `e2e_account_key` varchar(255) NOT NULL,
  `auto_print_receipts` tinyint(4) DEFAULT NULL,
  `webprnt` tinyint(4) DEFAULT NULL,
  `receipt_ip` varchar(20) NOT NULL,
  `hot_webprnt_ip` varchar(20) NOT NULL,
  `cold_webprnt_ip` varchar(20) NOT NULL,
  `use_register_log` tinyint(4) DEFAULT NULL,
  `cash_register` tinyint(4) DEFAULT NULL,
  `print_tip_line` tinyint(4) DEFAULT NULL,
  `signature_slip_count` tinyint(4) DEFAULT NULL,
  `credit_card_receipt_count` tinyint(4) DEFAULT NULL,
  `non_credit_card_receipt_count` tinyint(4) DEFAULT NULL,
  `ibeacon_minor_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `persistent_logs` tinyint(4) NOT NULL,
  `after_sale_load` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`terminal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_terminals: ~0 rows (approximately)
DELETE FROM `foreup_terminals`;
/*!40000 ALTER TABLE `foreup_terminals` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_terminals` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_timeclock_entries
DROP TABLE IF EXISTS `foreup_timeclock_entries`;
CREATE TABLE IF NOT EXISTS `foreup_timeclock_entries` (
  `employee_id` int(11) NOT NULL,
  `terminal_id` int(11) NOT NULL,
  `shift_start` datetime NOT NULL,
  `shift_end` datetime NOT NULL,
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_timeclock_entries: ~0 rows (approximately)
DELETE FROM `foreup_timeclock_entries`;
/*!40000 ALTER TABLE `foreup_timeclock_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_timeclock_entries` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_tournaments
DROP TABLE IF EXISTS `foreup_tournaments`;
CREATE TABLE IF NOT EXISTS `foreup_tournaments` (
  `tournament_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `green_fee` decimal(15,2) NOT NULL,
  `carts_issued` tinyint(4) NOT NULL,
  `cart_fee` decimal(15,2) NOT NULL,
  `customer_credit_fee` decimal(15,2) NOT NULL,
  `member_credit_fee` decimal(15,2) NOT NULL,
  `pot_fee` decimal(15,2) NOT NULL,
  `tax_included` tinyint(4) NOT NULL,
  `inventory_item_taxes` decimal(15,2) NOT NULL,
  `total_cost` decimal(15,2) NOT NULL,
  `accumulated_pot` decimal(15,2) NOT NULL,
  `remaining_pot_balance` decimal(15,2) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`tournament_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_tournaments: ~0 rows (approximately)
DELETE FROM `foreup_tournaments`;
/*!40000 ALTER TABLE `foreup_tournaments` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_tournaments` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_tournament_inventory_items
DROP TABLE IF EXISTS `foreup_tournament_inventory_items`;
CREATE TABLE IF NOT EXISTS `foreup_tournament_inventory_items` (
  `tournament_inventory_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`tournament_inventory_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_tournament_inventory_items: ~0 rows (approximately)
DELETE FROM `foreup_tournament_inventory_items`;
/*!40000 ALTER TABLE `foreup_tournament_inventory_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_tournament_inventory_items` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_tournament_winners
DROP TABLE IF EXISTS `foreup_tournament_winners`;
CREATE TABLE IF NOT EXISTS `foreup_tournament_winners` (
  `tournament_winner_id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `description` varchar(180) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`tournament_winner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_tournament_winners: ~0 rows (approximately)
DELETE FROM `foreup_tournament_winners`;
/*!40000 ALTER TABLE `foreup_tournament_winners` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_tournament_winners` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_tracks
DROP TABLE IF EXISTS `foreup_tracks`;
CREATE TABLE IF NOT EXISTS `foreup_tracks` (
  `track_id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `reround_track_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `online_booking` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`track_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_tracks: ~0 rows (approximately)
DELETE FROM `foreup_tracks`;
/*!40000 ALTER TABLE `foreup_tracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_tracks` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_unsubscribes
DROP TABLE IF EXISTS `foreup_unsubscribes`;
CREATE TABLE IF NOT EXISTS `foreup_unsubscribes` (
  `email` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `unsubscribe_all` tinyint(1) NOT NULL,
  `course_news_announcements` tinyint(1) NOT NULL,
  `teetime_reminders` tinyint(1) NOT NULL,
  `foreup_news_announcements` tinyint(1) NOT NULL,
  `comments` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_unsubscribes: ~0 rows (approximately)
DELETE FROM `foreup_unsubscribes`;
/*!40000 ALTER TABLE `foreup_unsubscribes` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_unsubscribes` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_users
DROP TABLE IF EXISTS `foreup_users`;
CREATE TABLE IF NOT EXISTS `foreup_users` (
  `person_id` int(10) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `twitter_id` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_users: ~0 rows (approximately)
DELETE FROM `foreup_users`;
/*!40000 ALTER TABLE `foreup_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_users` ENABLE KEYS */;


-- Dumping structure for table testTable.foreup_zip_code
DROP TABLE IF EXISTS `foreup_zip_code`;
CREATE TABLE IF NOT EXISTS `foreup_zip_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `county` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state_prefix` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `area_code` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `time_zone` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zip_code` (`zip_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.foreup_zip_code: 0 rows
DELETE FROM `foreup_zip_code`;
/*!40000 ALTER TABLE `foreup_zip_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `foreup_zip_code` ENABLE KEYS */;


-- Dumping structure for table testTable.quickbooks_config
DROP TABLE IF EXISTS `quickbooks_config`;
CREATE TABLE IF NOT EXISTS `quickbooks_config` (
  `quickbooks_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cfgkey` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cfgval` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cfgtype` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cfgopts` text COLLATE utf8_unicode_ci NOT NULL,
  `write_datetime` datetime NOT NULL,
  `mod_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.quickbooks_config: ~0 rows (approximately)
DELETE FROM `quickbooks_config`;
/*!40000 ALTER TABLE `quickbooks_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `quickbooks_config` ENABLE KEYS */;


-- Dumping structure for table testTable.quickbooks_connection
DROP TABLE IF EXISTS `quickbooks_connection`;
CREATE TABLE IF NOT EXISTS `quickbooks_connection` (
  `quickbooks_connection_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `certificate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `application_id` int(10) unsigned NOT NULL,
  `application_login` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lasterror_num` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lasterror_msg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connection_datetime` datetime NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_connection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.quickbooks_connection: ~0 rows (approximately)
DELETE FROM `quickbooks_connection`;
/*!40000 ALTER TABLE `quickbooks_connection` DISABLE KEYS */;
/*!40000 ALTER TABLE `quickbooks_connection` ENABLE KEYS */;


-- Dumping structure for table testTable.quickbooks_ident
DROP TABLE IF EXISTS `quickbooks_ident`;
CREATE TABLE IF NOT EXISTS `quickbooks_ident` (
  `quickbooks_ident_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_object` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `unique_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_ident` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `editsequence` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `extra` text COLLATE utf8_unicode_ci,
  `map_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_ident_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.quickbooks_ident: ~0 rows (approximately)
DELETE FROM `quickbooks_ident`;
/*!40000 ALTER TABLE `quickbooks_ident` DISABLE KEYS */;
/*!40000 ALTER TABLE `quickbooks_ident` ENABLE KEYS */;


-- Dumping structure for table testTable.quickbooks_log
DROP TABLE IF EXISTS `quickbooks_log`;
CREATE TABLE IF NOT EXISTS `quickbooks_log` (
  `quickbooks_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `batch` int(10) unsigned NOT NULL,
  `msg` text NOT NULL,
  `log_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_log_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `batch` (`batch`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table testTable.quickbooks_log: ~0 rows (approximately)
DELETE FROM `quickbooks_log`;
/*!40000 ALTER TABLE `quickbooks_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `quickbooks_log` ENABLE KEYS */;


-- Dumping structure for table testTable.quickbooks_notify
DROP TABLE IF EXISTS `quickbooks_notify`;
CREATE TABLE IF NOT EXISTS `quickbooks_notify` (
  `quickbooks_notify_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_object` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `unique_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_ident` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `errnum` int(10) unsigned DEFAULT NULL,
  `errmsg` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `write_datetime` datetime NOT NULL,
  `mod_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_notify_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.quickbooks_notify: ~0 rows (approximately)
DELETE FROM `quickbooks_notify`;
/*!40000 ALTER TABLE `quickbooks_notify` DISABLE KEYS */;
/*!40000 ALTER TABLE `quickbooks_notify` ENABLE KEYS */;


-- Dumping structure for table testTable.quickbooks_queue
DROP TABLE IF EXISTS `quickbooks_queue`;
CREATE TABLE IF NOT EXISTS `quickbooks_queue` (
  `quickbooks_queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quickbooks_ticket_id` int(10) unsigned DEFAULT NULL,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ident` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `extra` text COLLATE utf8_unicode_ci,
  `qbxml` text COLLATE utf8_unicode_ci,
  `priority` int(10) unsigned DEFAULT '0',
  `qb_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `msg` text COLLATE utf8_unicode_ci,
  `enqueue_datetime` datetime NOT NULL,
  `dequeue_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`quickbooks_queue_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`),
  KEY `priority` (`priority`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`,`qb_status`),
  KEY `qb_status` (`qb_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.quickbooks_queue: ~0 rows (approximately)
DELETE FROM `quickbooks_queue`;
/*!40000 ALTER TABLE `quickbooks_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `quickbooks_queue` ENABLE KEYS */;


-- Dumping structure for table testTable.quickbooks_recur
DROP TABLE IF EXISTS `quickbooks_recur`;
CREATE TABLE IF NOT EXISTS `quickbooks_recur` (
  `quickbooks_recur_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ident` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `extra` text COLLATE utf8_unicode_ci,
  `qbxml` text COLLATE utf8_unicode_ci,
  `priority` int(10) unsigned DEFAULT '0',
  `run_every` int(10) unsigned NOT NULL,
  `recur_lasttime` int(10) unsigned NOT NULL,
  `enqueue_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_recur_id`),
  KEY `qb_username` (`qb_username`,`qb_action`,`ident`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.quickbooks_recur: ~0 rows (approximately)
DELETE FROM `quickbooks_recur`;
/*!40000 ALTER TABLE `quickbooks_recur` DISABLE KEYS */;
/*!40000 ALTER TABLE `quickbooks_recur` ENABLE KEYS */;


-- Dumping structure for table testTable.quickbooks_ticket
DROP TABLE IF EXISTS `quickbooks_ticket`;
CREATE TABLE IF NOT EXISTS `quickbooks_ticket` (
  `quickbooks_ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ticket` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `processed` int(10) unsigned DEFAULT '0',
  `lasterror_num` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lasterror_msg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipaddr` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`quickbooks_ticket_id`),
  KEY `ticket` (`ticket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.quickbooks_ticket: ~0 rows (approximately)
DELETE FROM `quickbooks_ticket`;
/*!40000 ALTER TABLE `quickbooks_ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `quickbooks_ticket` ENABLE KEYS */;


-- Dumping structure for table testTable.quickbooks_user
DROP TABLE IF EXISTS `quickbooks_user`;
CREATE TABLE IF NOT EXISTS `quickbooks_user` (
  `qb_username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qb_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qb_company_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_initialized` tinyint(1) NOT NULL DEFAULT '0',
  `qbwc_wait_before_next_update` int(10) unsigned DEFAULT '0',
  `qbwc_min_run_every_n_seconds` int(10) unsigned DEFAULT '0',
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY (`qb_username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table testTable.quickbooks_user: ~0 rows (approximately)
DELETE FROM `quickbooks_user`;
/*!40000 ALTER TABLE `quickbooks_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `quickbooks_user` ENABLE KEYS */;


-- Dumping structure for view testTable.temptable2
DROP VIEW IF EXISTS `temptable2`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `temptable2` (
	`deleted` INT(11) NOT NULL,
	`sale_date` TIMESTAMP NOT NULL,
	`serialnumber` VARCHAR(255) NULL COLLATE 'utf8_unicode_ci',
	`description` VARCHAR(255) NULL COLLATE 'utf8_unicode_ci',
	`teetime_id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`terminal_id` INT(11) NOT NULL,
	`sale_id` INT(11) NOT NULL,
	`number` INT(11) UNSIGNED NULL,
	`line` INT(11) NOT NULL,
	`timeframe_id` VARCHAR(10) NULL COLLATE 'utf8mb4_general_ci',
	`comment` TEXT NOT NULL COLLATE 'utf8_unicode_ci',
	`payment_type` VARCHAR(255) NULL COLLATE 'utf8_unicode_ci',
	`customer_id` INT(11) NULL,
	`employee_id` INT(11) NOT NULL,
	`item_id` INT(11) NULL,
	`item_kit_id` INT(11) NULL,
	`invoice_id` INT(11) NULL,
	`supplier_id` INT(11) NULL,
	`quantity_purchased` DECIMAL(15,2) NOT NULL,
	`item_cost_price` DECIMAL(15,2) NOT NULL,
	`item_unit_price` DECIMAL(15,2) NOT NULL,
	`item_number` VARCHAR(255) NULL COLLATE 'utf8_unicode_ci',
	`name` VARCHAR(255) NULL COLLATE 'utf8_unicode_ci',
	`actual_category` VARCHAR(255) NULL COLLATE 'utf8_unicode_ci',
	`department` VARCHAR(255) NULL COLLATE 'utf8_unicode_ci',
	`category` VARCHAR(511) NULL COLLATE 'utf8_unicode_ci',
	`subcategory` VARCHAR(298) NULL COLLATE 'utf8_unicode_ci',
	`gl_code` VARCHAR(20) NULL COLLATE 'utf8_unicode_ci',
	`discount_percent` DECIMAL(15,2) NOT NULL,
	`subtotal` DECIMAL(15,2) NOT NULL,
	`total` DECIMAL(15,2) NOT NULL,
	`tax` DECIMAL(15,2) NOT NULL,
	`profit` DECIMAL(15,2) NOT NULL,
	`total_cost` DECIMAL(15,2) NOT NULL,
	`receipt_only` TINYINT(4) NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view testTable.temptable2
DROP VIEW IF EXISTS `temptable2`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `temptable2`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `temptable2` AS (select `s`.`deleted` AS `deleted`,`s`.`sale_time` AS `sale_date`,`si`.`serialnumber` AS `serialnumber`,`si`.`description` AS `description`,`s`.`teetime_id` AS `teetime_id`,`s`.`terminal_id` AS `terminal_id`,`s`.`sale_id` AS `sale_id`,`s`.`number` AS `number`,`si`.`line` AS `line`,`si`.`timeframe_id` AS `timeframe_id`,`s`.`comment` AS `comment`,`s`.`payment_type` AS `payment_type`,`s`.`customer_id` AS `customer_id`,`s`.`employee_id` AS `employee_id`,`i`.`item_id` AS `item_id`,NULL AS `item_kit_id`,NULL AS `invoice_id`,`i`.`supplier_id` AS `supplier_id`,`si`.`quantity_purchased` AS `quantity_purchased`,`si`.`item_cost_price` AS `item_cost_price`,`si`.`item_unit_price` AS `item_unit_price`,`i`.`item_number` AS `item_number`,`i`.`name` AS `name`,`i`.`category` AS `actual_category`,if((`si`.`item_id` = 0),'Invoice Line Items',`i`.`department`) AS `department`,if((`si`.`item_id` = 0),'Invoice Line Items',`i`.`category`) AS `category`,if(((`si`.`price_category` <> '') and (`tf`.`timeframe_name` is not null)),concat(`si`.`price_category`,' - ',`tf`.`timeframe_name`),if((`si`.`price_category` <> ''),concat(`si`.`price_category`,' - No Timeframe'),`i`.`subcategory`)) AS `subcategory`,`i`.`gl_code` AS `gl_code`,`si`.`discount_percent` AS `discount_percent`,`si`.`subtotal` AS `subtotal`,`si`.`total` AS `total`,`si`.`tax` AS `tax`,`si`.`profit` AS `profit`,`si`.`total_cost` AS `total_cost`,`si`.`receipt_only` AS `receipt_only` from (((`foreup_sales_items` `si` join `foreup_sales` `s` on((`si`.`sale_id` = `s`.`sale_id`))) left join `foreup_items` `i` on((`si`.`item_id` = `i`.`item_id`))) left join `foreup_seasonal_timeframes` `tf` on((`si`.`timeframe_id` = `tf`.`timeframe_id`))) where ((1 = 1) and (`s`.`course_id` = 7566) and (`s`.`sale_time` between '2014-03-01' and '2015-03-30'))) union all (select `s`.`deleted` AS `deleted`,`s`.`sale_time` AS `sale_date`,'' AS `serialnumber`,`si`.`description` AS `description`,`s`.`teetime_id` AS `teetime_id`,`s`.`terminal_id` AS `terminal_id`,`s`.`sale_id` AS `sale_id`,`s`.`number` AS `number`,`si`.`line` AS `line`,'' AS `Name_exp_45`,`s`.`comment` AS `comment`,`s`.`payment_type` AS `payment_type`,`s`.`customer_id` AS `customer_id`,`s`.`employee_id` AS `employee_id`,NULL AS `item_id`,`i`.`item_kit_id` AS `item_kit_id`,NULL AS `invoice_id`,NULL AS `supplier_id`,`si`.`quantity_purchased` AS `quantity_purchased`,`si`.`item_kit_cost_price` AS `item_kit_cost_price`,`si`.`item_kit_unit_price` AS `item_kit_unit_price`,`i`.`item_kit_number` AS `item_kit_number`,`i`.`name` AS `name`,`i`.`category` AS `actual_category`,`i`.`department` AS `department`,if(((`si`.`price_category` <> '') and (`si`.`teesheet` <> '')),concat_ws(' ',`si`.`teesheet`,`i`.`category`),`i`.`category`) AS `category`,if((`si`.`price_category` <> ''),`si`.`price_category`,`i`.`subcategory`) AS `subcategory`,NULL AS `gl_code`,`si`.`discount_percent` AS `discount_percent`,`si`.`subtotal` AS `subtotal`,`si`.`total` AS `total`,`si`.`tax` AS `tax`,`si`.`profit` AS `profit`,`si`.`total_cost` AS `total_cost`,`si`.`receipt_only` AS `receipt_only` from ((`foreup_sales_item_kits` `si` join `foreup_sales` `s` on((`si`.`sale_id` = `s`.`sale_id`))) left join `foreup_item_kits` `i` on((`si`.`item_kit_id` = `i`.`item_kit_id`))) where ((1 = 1) and (`s`.`course_id` = 7566) and (`s`.`sale_time` between '2014-03-01' and '2015-03-30'))) union all (select `s`.`deleted` AS `deleted`,`s`.`sale_time` AS `sale_date`,'' AS `serialnumber`,`si`.`description` AS `description`,`s`.`teetime_id` AS `teetime_id`,`s`.`terminal_id` AS `terminal_id`,`s`.`sale_id` AS `sale_id`,`s`.`number` AS `number`,`si`.`line` AS `line`,'' AS `Name_exp_80`,`s`.`comment` AS `comment`,`s`.`payment_type` AS `payment_type`,`s`.`customer_id` AS `customer_id`,`s`.`employee_id` AS `employee_id`,NULL AS `item_id`,NULL AS `item_kit_id`,`i`.`invoice_id` AS `invoice_id`,NULL AS `supplier_id`,`si`.`quantity_purchased` AS `quantity_purchased`,`si`.`invoice_cost_price` AS `invoice_cost_price`,`si`.`invoice_unit_price` AS `invoice_unit_price`,NULL AS `item_kit_number`,`i`.`name` AS `name`,`i`.`category` AS `actual_category`,`i`.`department` AS `department`,if(((`si`.`price_category` <> '') and (`si`.`teesheet` <> '')),concat_ws(' ',`si`.`teesheet`,`i`.`category`),`i`.`category`) AS `category`,if((`si`.`price_category` <> ''),`si`.`price_category`,`i`.`subcategory`) AS `subcategory`,NULL AS `gl_code`,`si`.`discount_percent` AS `discount_percent`,`si`.`subtotal` AS `subtotal`,`si`.`total` AS `total`,`si`.`tax` AS `tax`,`si`.`profit` AS `profit`,`si`.`total_cost` AS `total_cost`,`si`.`receipt_only` AS `receipt_only` from ((`foreup_sales_invoices` `si` join `foreup_sales` `s` on((`si`.`sale_id` = `s`.`sale_id`))) left join `foreup_invoices` `i` on((`si`.`invoice_id` = `i`.`invoice_id`))) where ((1 = 1) and (`s`.`course_id` = 7566) and (`s`.`sale_time` between '2014-03-01' and '2015-03-30')));
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
