<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");

class Coupons extends Secure_area implements iData_controller
{
  function __construct()
	{
		parent::__construct('coupons');

    $this->load->model('Coupon');
	}
  
  public function index()
  {
		$config['base_url'] = site_url('coupons/index');
		$config['total_rows'] = $this->Coupon->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_coupons_manage_table($this->Coupon->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('coupons/manage',$data);

  }
	public function search()
  {
    $search=$this->input->post('search');
		$data_rows=get_coupons_manage_table_data_rows($this->Coupon->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20),$this);
		echo $data_rows;
  }
	public function suggest()
  {
    $suggestions = $this->Coupon->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
  }

	public function get_row()
	{
		$coupon_id = $this->input->post('row_id');
		$data_row=get_coupons_data_row($this->Coupon->get_info($coupon_id),$this);
		echo $data_row;
	}
  
	public function view($coupon_id=-1)
  {
    $data = array();
    
    $this->load->view("coupons/form",$data);
  }

	public function save($coupon_id=-1)
  {

  }
  
	public function delete()
  {

  }

	public function get_form_width()
  {
    return 550;
  }
}