<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Invoices extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('invoices');
		$this->load->model('Customer_billing');
		$this->load->model('Invoice');
		$this->load->model('Customer');
		$this->load->model('Customer_credit_card');
		$this->load->model('Charge_attempt');
	}

	function index()
	{
        $config['base_url'] = site_url('invoices/index');
		$config['total_rows'] = $this->Invoice->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);
		//print_r($this->Invoice->get_all(false, $config['per_page'], $this->uri->segment(3))->result_array());
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$invoices = $this->Invoice->get_all(false, $config['per_page'], $this->uri->segment(3), true);
		$billings = $this->Customer_billing->get_all(false, $config['per_page'], $this->uri->segment(3), true);
		//print_r($billings->result_array());
		$data['invoice_manage_table']=get_invoices_manage_table($invoices,$this);
		$data['billing_manage_table']=get_customer_billings_manage_table($billings,$this);
		$this->load->view('invoices/manage',$data);
	}
	function billing($customer_id=-1,$billing_id = 0, $invoice_id = 0)
	{
		$this->load->model('Customer_credit_card');
		$this->load->model('Customer_billing');
		$this->load->model('Invoice');
		$data['person_info']=$this->Customer->get_info($customer_id);
		$data['course_info']=$this->Course->get_info($this->session->userdata('course_id'));
		$data['credit_cards']=$this->Customer_credit_card->get($customer_id);
		$data['billings']=$this->Customer_billing->get_all($customer_id);
		$data['invoices']=$this->Invoice->get_all($customer_id);
		$data['billing_id'] = $billing_id;
		$data['invoice_id'] = $invoice_id;
		$this->load->view("customers/form_billing",$data);
	}
	function load_billing($billing_id, $person_id = false)
	{
		$this->load->model('Customer_credit_card');
		$this->load->model('Customer_billing');
		$this->load->library('Sale_lib');

		$data = $this->Customer_billing->get_details($billing_id);
		if($billing_id == -1){
			$data['person_info'] = $this->Customer->get_info($person_id);
		}

		$data['is_recurring_billing'] = true;
		$data['course_info'] = $this->Course->get_info($this->session->userdata('course_id'));
		$data['credit_cards'] = $this->Customer_credit_card->get($data['person_id']);
		$data['popup'] = 1;
		$data['editing'] = 1;
		$data['html'] = $this->load->view('customers/invoice', $data, true);

		echo json_encode($data);
	}
	function delete_billing($billing_id)
	{
		$this->load->model('Customer_credit_card');
		$this->load->model('Customer_billing');
		$success = $this->Customer_billing->delete($billing_id);
		echo json_encode(array('deleted_billing'=>$success));
	}
	function load_invoice($invoice_id = -1, $person_id = false, $pdf = false, $batch_type = false, $member_balance = false, $customer_credit = false, $recurring_billings = false, $start_on = false, $end_on = false, $limit = 10000, $offset = 0)
	{
		$this->load->model('Customer');
		$this->load->model('Customer_credit_card');
		$this->load->model('Course');

		$data = array();

		// If the invoice is being created
		if($invoice_id == -1){
			$data['sent'] = false;
			$course_info = $this->Course->get_info($this->session->userdata('course_id'));
			$data['course_info'] = $course_info;

			if(!empty($person_id)){
				$data['person_info'] = $this->Customer->get_info($person_id);
				$data['credit_cards']= $this->Customer_credit_card->get($person_id);
			}

		// If viewing a previously generated invoice
		}else{
			$data = $this->Invoice->get_details($invoice_id);
			$data['sent'] = true;
		}

		if ($pdf)
		{
			$data['pdf'] = true;
			$invoice_html = $this->load->view('customers/invoice', $data, true);

			$this->load->library('Html2pdf');
			$html2pdf = new Html2pdf('P','A4','en');
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->setDefaultFont('Arial');
			$html2pdf->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
			$html2pdf->Output('invoice.pdf');
		}
		else
		{
			$invoice_html = $this->load->view('customers/invoice', $data, true);
			echo json_encode(array('html'=>$invoice_html));
		}
	}
	function view_pay_options ($invoice_id, $selected_card_id = 0) {
		$invoice_data = $this->Invoice->get_info($invoice_id);
		$data = $invoice_data[0];
		
		$data['customer'] = $this->Customer->get_info($data['person_id']);
		$data['payment_attempts'] = $this->Charge_attempt->get_all($invoice_id, false, true);
        $data['selected_card_id'] = $selected_card_id;
        $data['credit_card_id'] = $selected_card_id;
		$this->load->view('invoices/pay', $data);
	}
	function view_projections() {
		$this->load->view('invoices/projected_bills');
	}
	function view_charge_attempts() {
		$this->load->view('invoices/charge_attempts');
	}
	function run_charge_attempts($date) {
		$charge_attempts = $this->Charge_attempt->get_report_data($date);
		print_r($charge_attempts);
	}
	function run_projection($date) {
		session_write_close();
		$data = array();
		// GET INVOICE DATA FOR DATE
		$invoices_to_generate = $this->Customer_billing->get_invoices_to_generate($date, $this->session->userdata('course_id'));
		//print_r($invoices_to_generate);
		$return_array = array();
		foreach ($invoices_to_generate as $invoice)
		{
			// GENERATE INVOICE W/O SAVING
			$invoice_data = $this->Invoice->generate_from_billing($invoice['billing_id'], $date, true);
			$customer_info = $this->Customer->get_info($invoice_data['person_id']);
			//print_r($invoice_data);
			//continue;
			$account_totals = 0;
			$other_totals = 0;
			foreach ($invoice_data['items'] as $item) {
				if ($item['item_type']  == 'account_balance' || $item['item_type'] == 'member_balance') {
					$account_totals += $item['price'];
				}
				else {
					$other_totals += number_format(($item['quantity'] * $item['price']) * (100 + $item['tax_percentage']) / 100, 2);
				}
			}
			$card_on_file = 'No';
			if ($invoice_data['credit_card_id'] > 0)
			{
				$card_on_file = 'Yes';
			}
			$return_array[strtolower($customer_info->last_name.$customer_info->first_name)] = "<tr>
					<td>{$customer_info->first_name} {$customer_info->last_name}</td>
					<td class='currency_alignment'>$".number_format($other_totals,2)."</td>
					<td class='currency_alignment'>$".number_format($account_totals,2)."</td>
					<td class='currency_alignment'>$".number_format($other_totals + $account_totals,2)."</td>
					<td class='center_alignment'>$card_on_file</td></tr>";
		}
		ksort($return_array);
		$data['return_html'] = implode('', $return_array);
		echo json_encode($data);
	}
	function get_item_info($type = null, $item_id = null){

		if(empty($type) || empty($item_id)){
			echo 'null';
			return false;
		}

		$data = array();
		if($type == 'item'){
			$this->load->model('Item');
			$this->load->model('Item_taxes');

			$data = (array) $this->Item->get_info($item_id);
			$data['taxes'] = $this->Item_taxes->get_info($item_id);

		}else if($type == 'item_kit'){
			$this->load->model('Item_kit');
			$this->load->model('Item_kit_taxes');

			$data = (array) $this->Item_kit->get_info($item_id);
			$data['taxes'] = $this->Item_kit_taxes->get_info($item_id);
		}

		echo json_encode($data);
		return false;
	}

	// get the calculated invoice prices without saving anything.
	function calculate_invoice(){
		$invoice_data = $this->get_invoice_data();
		$items = $invoice_data['items'];
		$invoice_id = null;
		$customer_id = $this->input->post('customer_id');
		$employee_id =$invoice_data['employee_id'];
		$course_id = $invoice_data['course_id'];
		$sale_total = 0;
		$subtotal = 0;
		$tax = 0;
		$invoice_response = $this->Invoice->process_invoice_items($items, $invoice_id, $customer_id, $employee_id, $course_id,$sale_total,$subtotal,$tax);
		echo json_encode(array('items' => $invoice_response,'total' => $sale_total,'subtotal' => $subtotal, 'tax' => $tax));
	}

	function get_invoice_data(){

		$customer_id = $this->input->post('customer_id');
		$credit_card_id = (int) $this->input->post('credit_card_id');
		$email_invoice = $this->input->post('email_invoice');
		$show_account_transactions = $this->input->post('show_account_transactions');
		$items = $this->input->post('items');
		$bill_start = date('Y-m-d H:i:s');
		$bill_end = $bill_start;
		$due_days = (int) $this->input->post('invoice_due_days');
		$due_date = date('Y-m-d', strtotime('+'.$due_days.' days'));

		$pay_member_account = 0;
		if($this->input->post('pay_member_balance') == 'on'){
			$pay_member_account = 1;
		}
		$pay_customer_account = 0;
		if($this->input->post('pay_customer_balance') == 'on'){
			$pay_customer_account = 1;
		}

		$invoice_data = array(
			'course_id' => $this->session->userdata('course_id'),
			'bill_start' => $bill_start,
			'bill_end' => $bill_end,
			'billing_id' => 0,
			'pay_member_account' => $pay_member_account,
			'pay_customer_account' => $pay_customer_account,
			'person_id' => $customer_id,
			'employee_id' => $this->session->userdata('person_id'),
			'show_account_transactions' => $show_account_transactions,
			'items' => $items,
			'email_invoice' => $email_invoice,
			'credit_card_id' => $credit_card_id,
			'due_date' => $due_date
		);
		return $invoice_data;
	}

	function save_invoice(){

		$invoice_data = $this->get_invoice_data();

		$invoice_response = $this->Invoice->save($invoice_data);
		$invoice_id = $invoice_response['invoice_id'];

		// If selected, charge credit card and update the invoice
		if((int) $invoice_data['credit_card_id'] != 0){

			$due = (float) $invoice_response['total'];

			// Charge the card, charge_info is passed by reference and
			// will be updated with credit_card_payment_id
			$charge_success = $this->Invoice->charge_invoice($invoice_id, $this->session->userdata('course_id'), $invoice_data['credit_card_id'], $due);
		}

		// If selected, email copy of invoice to customer
		if ($invoice_data['email_invoice']){
			$email_success = $this->Invoice->send_email($invoice_id);
		}

		echo json_encode(array('success' => $invoice_response['invoice_id'], 'email_status' => $email_success, 'charge_status' => $charge_success));
	}

	function save_recurring_invoice($billing_id = null){

		if($billing_id == -1){
			$billing_id = null;
		}

		$title = $this->input->post('billing_title');
		$customer_id = $this->input->post('customer_id');
		$items = $this->input->post('items');
		$never_end = $this->input->post('end_never');
		$end_date = $this->input->post('end_date');
		$due_days = $this->input->post('due_days');
		$auto_bill_delay = $this->input->post('auto_bill_delay');

		if($never_end == 1){
			$end_date = '0000-00-00';
		}

		if(empty($due_days)){
			$due_days = 30;
		}

		$billing_data = array(
			'title' => $title,
			'course_id' => $this->session->userdata('course_id'),
			'employee_id' => $this->session->userdata('person_id'),
			'show_account_transactions' => (int) $this->input->post('show_account_transactions'),
			'email_invoice' => (int) $this->input->post('email_invoice'),
			'frequency' => $this->input->post('frequency'),
			'frequency_period' => $this->input->post('frequency_period'),
			'frequency_on' => $this->input->post('frequency_on'),
			'frequency_on_date' => $this->input->post('frequency_on_date'),
			'start_date' => $this->input->post('start_date'),
			'end_date' => $end_date,
			'due_days' => (int) $due_days,
			'auto_bill_delay' => $auto_bill_delay,
			'attempt_limit' => $this->input->post('attempt_limit'),
			'credit_card_id' => (int) $this->input->post('credit_card_id'),
			'person_id' => $customer_id,
			'items' => $items
		);

		$response = $this->Customer_billing->save($billing_data, $billing_id);
		echo json_encode(array('success' => true, 'billing_id' => $response));
	}
	
	function find_item_info()
	{
		$item_number=$this->input->post('scan_item_number');
		echo json_encode($this->Billing->find_item_info($item_number));
	}

	function switch_data_type($type = 'invoices') {
		$this->session->set_userdata('invoices_data_type', $type);
		echo json_encode(array());
	}

	function search($offset = 0, $month = false, $paid_status = 'all', $person_id = '')
	{
		$data = array();
		$search=$this->input->post('search');
		$type = $this->session->userdata('invoices_data_type');
		if (!$type || $type == 'invoices') {
			$results = $this->Invoice->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $month, $paid_status, $offset, $person_id);
			$data_rows=get_invoices_manage_table_data_rows($results,$this);
			$config['total_rows'] = $this->Invoice->count_all();
		}
		else if ($type == 'billings') {
			$results = $this->Customer_billing->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset, $person_id);
			$data_rows=get_customer_billings_manage_table_data_rows($results,$this);
			$config['total_rows'] = $this->Customer_billing->count_all();
		}
		$config['base_url'] = site_url('invoices/index');
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['data_rows'] = $data_rows;

        echo json_encode($data);



		// $search=$this->input->post('search');
		// $data_rows=get_billings_manage_table_data_rows($this->Billing->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20),$this);
		// echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->get('term'),100, 'ln_and_pn');
		echo json_encode($suggestions);
	}

	function billing_search()
	{
        $suggestions = $this->Billing->get_billing_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_category()
	{
		$suggestions = $this->Billing->get_category_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}

	function recipient_search($type='')
	{
		//$everyone_count = $this->Customer->count_all(true);
	    $suggestions = array();
	    $groups = $this->Customer->get_group_info('', $this->input->get('term'));
	    //$suggestions[]=array('value'=> 0, 'label' => $this->db->last_query());
		foreach ($groups as $group)
		{
			$gr = $this->Customer->get_group_members($group['group_id']);
	    	
	    	$suggestions[]=array('value'=> $group['group_id'], 'label' => $group['label'].' ('.count($gr).')', 'is_group'=>1, 'email'=>count($gr));
		}
		
		$suggestions = array_merge($suggestions, $this->Customer->get_customer_search_suggestions($this->input->get('term'), 100, $type));
		echo json_encode($suggestions);
	}

	function get_row()
	{
		$item_id = $this->input->post('row_id');
		$data_row=get_invoice_data_row($this->Invoice->get_one($item_id),$this);
		echo $data_row;
	}

	function get_billing_row()
	{
		$item_id = $this->input->post('row_id');
		$data_row=get_customer_billing_data_row($this->Customer_billing->get_one($item_id),$this);
		echo $data_row;
	}

	function get_info($item_id=-1)
	{
		echo json_encode($this->Customer_billing->get_info($item_id));
	}

	function view($invoice_id = -1, $person_id = false, $pdf = false, $batch_type = false, $member_balance = false, $customer_credit = false, $recurring_billings = false, $start_on = false, $end_on = false, $limit = 10000, $offset = 0)
	{
		$this->load->model('Customer');
		$this->load->model('Customer_credit_card');
		$this->load->model('Course');

		$data = array();

		// If the invoice is being created
		if($invoice_id == -1){
			$data['sent'] = false;
			$course_info = $this->Course->get_info($this->session->userdata('course_id'));
			$data['course_info'] = $course_info;

			if(!empty($person_id)){
				$data['person_info'] = $this->Customer->get_info($person_id);
				$data['credit_cards']= $this->Customer_credit_card->get($person_id);
			}

		// If viewing a previously generated invoice
		}else{
			$data = $this->Invoice->get_details($invoice_id);
			$data['sent'] = true;
		}

		if ($pdf)
		{
			$data['pdf'] = true;
			$invoice_html = $this->load->view('customers/invoice', $data, true);

			$this->load->library('Html2pdf');
			$html2pdf = new Html2pdf('P','A4','en');
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->setDefaultFont('Arial');
			$html2pdf->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
			$html2pdf->Output('invoice.pdf');
		}
		else
		{
			$data['editable'] = true;
			//print_r($data);
			$this->load->view('customers/invoice', $data);
			//echo json_encode(array('html'=>$invoice_html));
		}
	}
	function edit_totals($invoice_id)
	{
		$data = array();
		$data['invoice_info'] = $this->Invoice->get_info($invoice_id);
		$this->load->view('invoices/edit', $data);
	}
	// function view($invoice_id=-1)
	// {
		// $person_id = $this->input->get('person_id');
		// $course_id = $this->session->userdata('course_id');
		// $course_info = $this->Course->get_info($course_id);
		// $invoice_data = $this->Invoice->get_info($invoice_id);
		// $data = $invoice_data[0];
		// $data['items']=$this->Invoice->get_items($invoice_id);
		// //echo '<br/>Person_id '.$person_id.'<br/>';
		// //echo '<br/>Data["person_id"] '.$data['person_id'].'<br/>';
// //print_r($data['items']);
		// $data['person_info']= $person_id ? $this->Customer->get_info($person_id, $course_id) : $this->Customer->get_info($data['person_id'], $course_id);
		// //echo '<br/>'.$this->db->last_query().'<br/>';
		// $data['course_info']=$course_info;
		// $data['credit_cards']= $person_id ? $this->Customer_credit_card->get($person_id) : $this->Customer_credit_card->get($data['person_id']);
		// $data['popup'] = 1;// = array(
		// $data['is_invoice'] = true;
		// $data['sent'] = $invoice_id != -1 ? true : false;
		// if ($pdf)
		// {
			// $data['pdf'] = true;
			// // $data['emailing_invoice'] = false;
			// $invoice_html = $this->load->view('customers/invoice', $data, true);
// 
			// $this->load->library('Html2pdf');
			// $html2pdf = new Html2pdf('P','A4','fr');
			// //$html2pdf->setModeDebug();
			// $html2pdf->pdf->SetDisplayMode('fullpage');
			// $html2pdf->setDefaultFont('Arial');
			// $html2pdf->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
			// $html2pdf->Output('invoice.pdf');
		// }
		// else
		// {
			// if ($data['person_info']->person_id)
				// $this->load->view('invoices/controls.php', $data);
			// $this->load->view('customers/invoice', $data);
// 
		// }
	// }
	function view_batch_pdfs()
	{
		$data = array();
		$course_id = $this->session->userdata('course_id');
		$url = 'archives/invoices/'.$course_id;
		$data['files'] = array_reverse(get_filenames($url));
		$data['course_id'] = $course_id;
		$data['no_batch_files_message'] = lang('invoices_no_batch_files');
		//echo 'here<br/>';
		//echo $this->session->userdata('course_id');
		//print_r($data['files']);
		$this->load->view('invoices/batch_pdfs', $data);
	}
	function view_settings()
	{
		$data = array();//$this->invoice->get_settings();
		$data['generate_on_message'] = lang('invoices_generate_on_message');
		$this->load->view('invoices/settings', $data);
	}
	function history($course_id = -1)
	{
		if ($course_id == -1)
			return;
		else
		{
			$data['course_info'] = $this->Course->get_info($course_id);
			$data['course_payments'] = $this->Billing->get_course_payments($course_id);
			//print_r($course_payments);
			$this->load->view('invoices/history', $data);
		}
	}

	function save($billing_id=-1)
	{
		$billing_data = array(
			'course_id'=>$this->input->post('course_id'),
			'contact_email'=>$this->input->post('contact_email'),
			'tax_name'=>$this->input->post('tax_name'),
			'tax_rate'=>$this->input->post('tax_rate'),
			'product'=>$this->input->post('product'),
			'start_date'=>$this->input->post('start_date'),
			'period_start'=>$this->input->post('period_start'),
			'period_end'=>$this->input->post('period_end'),
			'credit_card_id'=>$this->input->post('credit_card_id'),
			'annual'=>$this->input->post('annual'),
			'email_limit'=>$this->input->post('email_limit'),
			'text_limit'=>$this->input->post('text_limit'),
			'annual_amount'=>$this->input->post('annual_amount'),
			'annual_month'=>$this->input->post('annual_month'),
			'annual_day'=>$this->input->post('annual_day'),
			'monthly'=>$this->input->post('monthly'),
			'monthly_amount'=>$this->input->post('monthly_amount'),
			'monthly_day'=>$this->input->post('monthly_day'),
			'teetimes'=>$this->input->post('teetimes'),
			'teetimes_daily'=>$this->input->post('teetimes_daily'),
			'teetimes_weekly'=>$this->input->post('teetimes_weekly'),
			'teesheet_id'=>$this->input->post('teesheet_id'),
			'free'=>$this->input->post('free')
		);
		if($this->Billing->save($billing_data,$billing_id))
		{
			//New item
			if($billing_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_adding').' '.
				$billing_data['name'],'billing_id'=>$billing_data['billing_id']));
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_updating').' '.
				$billing_data['name'],'billing_id'=>$billing_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_error_adding_updating').$this->db->last_query().' '.
			$billing_data['name'],'billing_id'=>-1));
		}

	}


	function delete($type = 'invoices')
	{
		$this->load->model('Customer_billing');
		$items_to_delete=$this->input->post('ids');
		$type = $this->input->post('invoice_type');

		if($type == 'invoices' && $this->Invoice->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('items_successful_deleted').' '.
			count($items_to_delete).' '.lang('items_one_or_multiple')));
		}
		else if($type == 'billings' && $this->Customer_billing->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('items_successful_deleted').' '.
			count($items_to_delete).' '.lang('items_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_cannot_be_deleted')));
		}
	}
	
	function view_billing($billing_id = false) {
		if (!$billing_id)
			return;
		
		$this->load->model('Customer_credit_card');
		$this->load->model('Customer_billing');
		$this->load->library('Sale_lib');
		$data = $this->Customer_billing->get_details($billing_id);
		$data['billing'] = $data;
		$data['billing_info'] = new stdClass;
		$data['billing_info']->billing_id = $data['billing_id'];
		if($billing_id == -1){
			$data['person_info'] = $this->Customer->get_info($person_id);
		}

		$data['is_recurring_billing'] = true;
		$data['course_info'] = $this->Course->get_info($this->session->userdata('course_id'));
		$data['credit_cards'] = $this->Customer_credit_card->get($data['person_id']);
		$data['popup'] = 1;
		$data['editing'] = 1;
		
		$this->load->view('invoices/form_billing', $data);

		//echo json_encode($data);
		
	}
	function pay_off_invoice($invoice_id, $credit_card_id) 
	{
		$invoice_data = $this->Invoice->get_info($invoice_id);
		$invoice_data = $invoice_data[0];
		$credit_card_data = $this->Customer_credit_card->get_info($credit_card_id);
		$message = '';
		$success = false;
		if ($invoice_data['person_id'] != $credit_card_data->customer_id) 
		{
			$message = 'The credit card and invoice do not belong to the same customer '.$invoice['person_id'].' - '.$credit_card_data->customer_id;
		}
		else 
		{
			$due = $invoice_data['total'] - $invoice_data['paid'];
			// Run charge invoice
			$success = $this->Invoice->charge_invoice(
				$invoice_id,
				$this->session->userdata('course_id'),
				$credit_card_id,
				$due
			);
			if (!$success)
			{
				$message = 'Charge attempt failed. Please try again later or collect new credit card information';
			}
			else {
				$message = 'Charge successful';
			}
		}
		
		$data = array('success'=>$success, 'message'=>$message);
		echo json_encode($data);
	}
	

	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 650;
	}


}
?>
