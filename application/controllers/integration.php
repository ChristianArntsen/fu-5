<?php
class Integration extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function load($id)
	{
		$course_id = $this->config->item('course_id');
		$module = $this->db->from("api_user_courses")
			->where("course_id",$course_id)
			->where("id",$id)
			->get()->row();

		if(empty($module)){
			return false;
		}

		$jwt = new \fu\auth\json_web_token();
		// Set Private Claims
		$jwt->addPrivateClaim('course_id', $this->session->userdata('course_id'));
		$jwt->addPrivateClaim('employee_id', $this->session->userdata('emp_id'));
		$jwt->addPrivateClaim('employee_email', $this->session->userdata('email'));


		$jwt->createToken($module->module_url);
		$token = $jwt->getToken();

		header("Location: ".$module->module_url."?token=".$token) ;
	}

	function test()
	{

	}
}
?>