<?php
//This controller is for the purpose of running cron tasks in the system

class Daemon extends MY_Controller//REST_Controller
{
	private $cron_key = 'f0r3upcr0n';
    function __construct()
    {

        parent::__construct('cron');
		$this->load->library('Marketing_recipients_lib');
		$this->load->model('Marketing_Texting');
    	$this->load->model('Customer');
		$this->load->model('Teesheet');
		$this->load->model('Teetime');
		$this->load->model('Communication');
		$this->load->model('Sendhub_account');
		$this->load->library('sendhub');
		$this->load->helper('email_marketing');
		$this->load->helper('email_helper');
        $this->load->model('coupon');

        $this->load->model('Course');
        $this->load->model('Invoice');
        $this->load->model('Customer_billing');
        $this->load->model('Customer_credit_card');
        $this->load->model('Item_taxes');
        $this->load->model('Item_kit_taxes');
        $this->load->helper('download');
        $this->load->helper('file');
        $this->load->library('zip');
        $this->load->library('Html2pdf');
        $this->load->library('sale_lib');
        $this->load->model("exec_queue");
	    if (extension_loaded('newrelic'))
	        newrelic_set_appname("daemon");
	}

	function forwardAllVirtualNumbers(){
    	$this->load->model("course");
    	$virtual_numbers = $this->db->from("course_virtual_numbers")
		    ->get()->result_array();

		$this->nexmo_account = new \fu\nexmo\NexmoAccount("224d4353","b1518272");

    	foreach($virtual_numbers as $number)
	    {
	    	$this->course_info = $this->course->get_info($number['course_id']);

		    $country = $this->course_info->country;
		    $country_code = "US";
		    if($country == "UK"){
			    $country_code = "UK";
		    } else if($country == "CA"){
			    $country_code = "CA";
		    }



		    $msisdnMaker = new \fu\marketing\texting\MsisdnMaker();
		    $msisdnMaker->set($this->course_info->phone);
		    $to = $msisdnMaker->get();
		    echo "Forward ".$number['virtual_number']." To ".$to;
		    $this->nexmo_account->updateNumber($country_code,$number['virtual_number'],$to);
	    }
	}

	function testMemberBilling($reset = false){

	    if(ENVIRONMENT != 'development'){
	        echo 'Incorrect environment';
            return false;
        }

        if(!empty($reset)){

            require_once('application/tests/integration/member_billing/memberBillingIntegrationTest.php');
            \tests\integration\member_billing\MemberBillingIntegrationTest::setUpBeforeClass();
	        \tests\integration\member_billing\MemberBillingIntegrationTest::resetData();
	        \tests\integration\member_billing\MemberBillingIntegrationTest::clearStatements();
            echo '<h1>Data reset</h1>';
            return false;
        }

        $this->load->view('daemon/testMemberBilling');
    }

    function startMemberBillingCharges()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $queue = new \fu\statementWorkflow\classes\SqsQueue($this);
        $generateCharges = new \fu\statementWorkflow\generateCharges($queue);
        $this->runGeneratorJob($generateCharges,$queue);

    }

    function startMemberBillingStatements()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $queue = new \fu\statementWorkflow\classes\SqsQueue($this);
        $generateStatements = new \fu\statementWorkflow\generateStatementJobs($queue);
        $this->runGeneratorJob($generateStatements,$queue);

    }

    function startDelayedStatementAutoPayments()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $queue = new \fu\statementWorkflow\classes\SqsQueue($this);
        $generateStatements = new \fu\statementWorkflow\generateDelayedStatementChargeJobs($queue);
        $this->runGeneratorJob($generateStatements,$queue);

    }

    function startStatementFinanceCharges()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $queue = new \fu\statementWorkflow\classes\SqsQueue($this);
        $generateFinanceCharges = new \fu\statementWorkflow\generateStatementFinanceChargeJobs($queue);
        $this->runGeneratorJob($generateFinanceCharges,$queue);
    }

	private function runGeneratorJob($generator,$queue)
	{
	    $current_time = null;
		if(!empty($_REQUEST)&&isset($_REQUEST['current_time']))
			$current_time =$_REQUEST['current_time'];

        if(empty($current_time)){
            $current_time = date('c');
        }

        $generator->runUntilEmpty($current_time);

		echo ENVIRONMENT.'<br>';
		echo 'Run Cycles: '.$generator->run_cycles;
		echo '<br>Messages Received: '.$generator->messages_received;
		echo '<br>Messages Sent: '.$generator->messages_sent;
	}

	function processRecurringCharges()
	{
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

	    $current_time = null;
		if(!empty($_REQUEST)&&isset($_REQUEST['current_time']))
			$current_time =$_REQUEST['current_time'];

        if(empty($current_time)){
            $current_time = date('c');
        }

		$this->close_and_continue(false);

		$queue = new \fu\statementWorkflow\classes\SqsQueue();
		$processRecurringChargeJobs = new \fu\statementWorkflow\generateChargeSale($queue);

        $processRecurringChargeJobs->runUntilEmpty($current_time);

		echo ENVIRONMENT.'<br>';
		echo 'Run Cycles: '.$processRecurringChargeJobs->run_cycles;
		echo '<br>Messages Received: '.$processRecurringChargeJobs->messages_received;
		echo '<br>Messages Sent: '.$processRecurringChargeJobs->messages_sent;
	}

	function processStatementJobs()
	{
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

	    $current_time = null;
		if(!empty($_REQUEST)&&isset($_REQUEST['current_time']))
			$current_time =$_REQUEST['current_time'];

        if(empty($current_time)){
            $current_time = date('c');
        }

		$this->close_and_continue(false);

		$queue = new \fu\statementWorkflow\classes\SqsQueue();
		$processRecurringChargeJobs = new \fu\statementWorkflow\processStatementJobs($queue);

        $processRecurringChargeJobs->runUntilEmpty($current_time);

		echo ENVIRONMENT.'<br>';
		echo 'Run Cycles: '.$processRecurringChargeJobs->run_cycles;
		echo '<br>Messages Received: '.$processRecurringChargeJobs->messages_received;
		echo '<br>Messages Sent: '.$processRecurringChargeJobs->messages_sent;
		echo '<br>Errors Generated: '.$processRecurringChargeJobs->errors_sent;
	}

	function processStatementPdfJobs()
	{
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

	    $current_time = null;
		if(!empty($_REQUEST)&&isset($_REQUEST['current_time']))
			$current_time =$_REQUEST['current_time'];

        if(empty($current_time)){
            $current_time = date('c');
        }

		$this->close_and_continue(false);

		$queue = new \fu\statementWorkflow\classes\SqsQueue();
		$processRecurringChargeJobs = new \fu\statementWorkflow\processStatementPdfJobs($queue);

        $processRecurringChargeJobs->runUntilEmpty($current_time);

		echo ENVIRONMENT.'<br>';
		echo 'Run Cycles: '.$processRecurringChargeJobs->run_cycles;
		echo '<br>Messages Received: '.$processRecurringChargeJobs->messages_received;
		echo '<br>Messages Sent: '.$processRecurringChargeJobs->messages_sent;
		echo '<br>Errors Generated: '.$processRecurringChargeJobs->errors_sent;
	}

	function processStatementChargeJobs()
	{
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

	    $current_time = null;
		if(!empty($_REQUEST)&&isset($_REQUEST['current_time']))
			$current_time =$_REQUEST['current_time'];

        if(empty($current_time)){
            $current_time = date('c');
        }

		$this->close_and_continue(false);

		$queue = new \fu\statementWorkflow\classes\SqsQueue();
		$processRecurringChargeJobs = new \fu\statementWorkflow\processStatementChargeJobs($queue);

        $processRecurringChargeJobs->runUntilEmpty($current_time);

		echo ENVIRONMENT.'<br>';
		echo 'Run Cycles: '.$processRecurringChargeJobs->run_cycles;
		echo '<br>Messages Received: '.$processRecurringChargeJobs->messages_received;
		echo '<br>Messages Sent: '.$processRecurringChargeJobs->messages_sent;
	}

	function close_and_continue($test = true)
	{
		ob_end_clean();
		header("Connection: close");
		ignore_user_abort(true);
		ob_start();
		//echo 'We are going to cut you loose. This will take a while...';
		$size = ob_get_length();
		header("Content-Length: $size");
		ob_end_flush();
		flush();

		// I think that gzip causes problems with closing the connection.
		if($test){
			sleep(10);
			echo 'You should not see this...';
		}

	}

	function migrate_menu_categories(){
		session_write_close();
		$this->load->model('v2/Item_model');
		$this->load->model('v2/Menu_model');

		$courses = $this->db->select('course_id')
			->from('courses')
			->where('active_course', 1)
			->where('active', 0)
			->get()
			->result_array();

		foreach($courses as $course){
			$data = [];
			$course_id = (int) $course['course_id'];
			$category_map = [];
			$subcategory_map = [];
			$item_map = [];

			// Retrieve all categories for F&B items
			$categories = $this->db->select("i.category AS name, GROUP_CONCAT(i.item_id) AS item_ids", false)
				->from('items AS i')
				->where('i.course_id', $course_id)
				->where('i.food_and_beverage', 1)
				->group_by('i.category')
				->get()
				->result_array();

			// Migrate all F&B categories to new menu categories
			foreach($categories as $category){

				$data['name'] = trim($category['name']);
				$data['course_id'] = $course_id;

				// Make sure we don't create duplicates
				$existing_row = $this->db->select('category_id')
					->from('menu_categories')
					->where('name', $data['name'])
					->where('course_id', $data['course_id'])
					->get()->row_array();

				if(!$existing_row){
					$this->db->insert('menu_categories', $data);
					$category_id = $this->db->insert_id();
				}else{
					$category_id = $existing_row['category_id'];
				}

				// Attach newly created menu category to item
				if(!empty($category['item_ids'])){

					$item_ids = explode(',', $category['item_ids']);

					if(!empty($item_ids) && count($item_ids) > 0){
						foreach($item_ids as $item_id){
							$this->Menu_model->save_item_category($item_id, $category_id, $course_id);
						}
					}
				}

				$category_map[$category['name']] = $category_id;
			}

			// Retrieve all subcategories for F&B items
			$subcategories = $this->db->select("i.subcategory AS name, GROUP_CONCAT(i.item_id) AS item_ids", false)
				->from('items AS i')
				->where('i.course_id', $course_id)
				->where('i.food_and_beverage', 1)
				->where("i.subcategory != ''")
				->group_by('i.subcategory')
				->get()
				->result_array();

			// Migrate all F&B SUB categories to menu SUB categories
			foreach($subcategories as $subcategory){

				$data['name'] = trim($subcategory['name']);
				$data['course_id'] = $course_id;

				$existing_row = $this->db->select('subcategory_id')
					->from('menu_subcategories')
					->where('name', $data['name'])
					->where('course_id', $data['course_id'])
					->get()->row_array();

				if(!$existing_row){
					$this->db->insert('menu_subcategories', $data);
					$subcategory_id = $this->db->insert_id();
				}else{
					$subcategory_id = $existing_row['subcategory_id'];
				}

				// Attach newly created menu subcategory to item
				if(!empty($subcategory['item_ids'])){

					$item_ids = explode(',', $subcategory['item_ids']);

					if(!empty($item_ids) && count($item_ids) > 0){
						foreach($item_ids as $item_id){
							$this->Menu_model->save_item_subcategory($item_id, $subcategory_id, $course_id);
						}
					}
				}

				$subcategory_map[$subcategory['name']] = $subcategory_id;
			}

			// Update new menu buttons so we don't lose the saved order of each button
			$old_buttons = $this->db->select('name, category, sub_category, order')
				->from('table_buttons')
				->where('course_id', $course_id)
				->get()
				->result_array();

			foreach($old_buttons as $old_button){

				if($old_button['category'] == '' && $old_button['sub_category'] == ''){

					if(empty($category_map[$old_button['name']])){
						continue;
					}

					$data = [
						'category_id' => $category_map[$old_button['name']],
						'subcategory_id' => 0,
						'item_id' => 0
					];

				}else if($old_button['category'] != '' && $old_button['sub_category'] != ''){

					if(empty($category_map[$old_button['category']])){
						continue;
					}
					if(empty($subcategory_map[$old_button['sub_category']])){
						continue;
					}

					$item_row = $this->db->select('item_id')
						->from('items')
						->where('course_id', $course_id)
						->where('name', $old_button['name'])
						->get()
						->row_array();

					if(empty($item_row)){
						continue;
					}

					$data = [
						'category_id' => $category_map[$old_button['category']],
						'subcategory_id' => $subcategory_map[$old_button['sub_category']],
						'item_id' => $item_row['item_id']
					];

				}else if($old_button['category'] != ''){

					if(empty($category_map[$old_button['category']])){
						continue;
					}

					if(!empty( $subcategory_map[$old_button['name']] )){
						$subcategory_id = $subcategory_map[$old_button['name']];
					}else{
						$subcategory_id = 0;
					}

					if(empty($subcategory_id)){
						$item_row = $this->db->select('item_id')
							->from('items')
							->where('course_id', $course_id)
							->where('name', $old_button['name'])
							->get()
							->row_array();

						if(empty($item_row)){
							continue;
						}
						$item_id = (int) $item_row['item_id'];

					}else{
						$item_id = 0;
					}

					$data = [
						'category_id' => $category_map[$old_button['category']],
						'subcategory_id' => $subcategory_id,
						'item_id' => $item_id
					];
				}

				if(empty($data['category_id']) && empty($data['subcategory_id']) && empty($data['item_id'])){
					continue;
				}

				$this->db->query("INSERT IGNORE INTO foreup_menu_buttons (`category_id`, `subcategory_id`, `item_id`, `order`, `course_id`)
					VALUES ({$data['category_id']}, {$data['subcategory_id']}, {$data['item_id']}, {$old_button['order']}, {$course_id})
					ON DUPLICATE KEY UPDATE `order` = {$old_button['order']}");
			}
		}
	}
	function sync_unsubscribes()
	{
		$this->load->library("sendgrid");
		$sendgrid = new Sendgrid();
		$unsubscribes = $sendgrid->list_unsubscribe();
		$toUnsubscribe = [];
		foreach($unsubscribes as $unsubscribe){
			$toUnsubscribe[] = $unsubscribe->email;
		}
		

		$this->db->where_in("email",$toUnsubscribe);
		$this->db->update("customers c JOIN foreup_people p ON c.person_id = p.person_id",["opt_out_email"=>1]);
	}
	function test_email($cron_key)
	{
		$count = 0;
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			do
			{
				send_sendgrid(
					'jhopkins@foreup.com',
					"Testing out Daemons ".date("Y-m-d h:i:s"),
					'Here is a little bit of text to go with it. Count: '.$count,
					'no-reply@foreup.com',
				 	'ForeUP Daemon Tester'
				);
				// WAIT 4 MINUTES
				$count++;
			} while($count < 4);
		}
	}

	function cleanse_temporary_tables($cron_key)
	{
		if ($cron_key != $this->cron_key)
			return;

		$salesTables = new fu\reports\TemporaryTables\SalesTable();
		$salesTables->cleanseAllOldTables();
	}

	function gc_sessions($cron_key)
	{
		if ($cron_key != $this->cron_key)
			return;
		ini_set('session.gc_probability', 1);
		ini_set('session.gc_divisor', 1);
		session_write_close();
		session_start();
	}

	function processReminderQueue()
	{


		$this->load->model("Marketing_Texting_Queue");
		$queue = new Marketing_Texting_Queue();


		$this->db->close();
		$reminderQueue = new \fu\reminders\ReminderQueue($this);


		$this->load->model("Reminder");
		for($i=0;$i<=2;$i++) {

			$this->db->close();
			try {
				$result = $reminderQueue->getMessages();
			} catch (Exception $e) {
				//Do Nothing
				print "Error recieving messages ".$e->getMessage()." \n";
				sleep(5);
				continue;
			}


			foreach ($result->getPath('Messages') as $message) {
				$eventBody = json_decode($message['Body']);
				if(!isset($eventBody->reminderId)){
					continue;
				}

				$reminder = new Reminder();
				$reminderObj = $reminder->get($eventBody->reminderId);

				//Foreach teesheet of each course with reminders and texting
				$this->load->model("teetime");
				$this->load->model("teesheet");
				$teetimeObj = new Teetime();

				$teesheet = new Teesheet();
				$teesheets = $teesheet->get_teesheets($reminderObj->course_id);

				foreach($teesheets as $teesheet)
				{
					echo "Teesheet {$teesheet['teesheet_id']} <br/>";
					$teetimes = $teetimeObj->getTeetimesNeedingReminder(($teesheet['teesheet_id']),$reminderObj->timebefore_unit,$reminderObj->timebefore,$eventBody->timestamp);
					foreach($teetimes as $customer_id => $teetime){
						if(!$customer_id)
							continue;
						if(isset($reminderObj->campaign_id) && $reminderObj->type == 'email'){
							$recipient = new fu\marketing\recipientRetriever\single();
							$recipient->setRecipients($customer_id);
							$marketing_campaigns_model = new Marketing_campaign();
							$campaign_info = $marketing_campaigns_model->get_info($reminderObj->campaign_id,true);
							$campaign = new \fu\marketing\Campaign($recipient,$campaign_info);


							//A customer should never get a message from the same campaign on the same day
							$recipientInformation = $recipient->getRecipients();
							if(isset($recipientInformation[0])){
								$recipientInformation = $recipientInformation[0];
							} else {
								"Unknown recipient. Skipping. <br/>";
							}
							if($this->hasCustomerReceivedCampaignToday($recipientInformation['email'],$reminderObj->campaign_id)){
								print "Customer already recieved {$customer_id} \n";
							} else {
								$campaign->send();
								$campaign_info = (array) $campaign_info;
								$marketing_campaigns_model->save($campaign_info,$campaign_info['campaign_id']) ;
								print "Email {$customer_id} \n";
							}
						} else {
							$success = $queue->insert([
								"course_id"=>$reminderObj->course_id,
								"job_name"=>'teetime_reminder',
								"status"=>'queued',
								"parameters"=>json_encode(["teetime_id"=>$teetime,"customer_id"=>$customer_id,"campaign_id"=>$row['campaign_id']]),
							]);

							if(!$success){
								throw new Exception(json_encode($queue->form_validation->get_all_errors()));
							}
						}

					}


				}


				$reminderQueue->deleteMessage($message);
			}


		}

	}
	public function hasCustomerReceivedCampaignToday($email,$campaignId)
	{
		$this->db_datastore = $this->load->database('datastore', TRUE);
		$today = \Carbon\Carbon::now()->startOfDay();
		$counts = $this->db_datastore->query("
        SELECT
          COUNT(*) as count
        FROM foreup_sendgrid_events
        WHERE
        (email = '{$email}' AND marketing_campaign_id = '{$campaignId}') AND timestamp > '{$today->toDateTimeString()}'
        AND `event` = 'delivered'
        ;
        ");
		$results = $counts->row_array();
		var_dump($results);var_dump($results['count'] != "0");
		return $results['count'] != "0";

	}
	function reminders($cron_key)
	{
		if ($cron_key != $this->cron_key)
			return;
		$teetime = new Teetime();

		$currentTimestamp = \Carbon\Carbon::now()->second(0);
		$currentTimestamp->setTimezone("UTC");
		//A minute reminder should be ran once a minute, a day reminder once a day
		$results = $this->db->select(['reminders.id','reminders.timebefore_unit','reminders.timebefore','reminders.campaign_id','marketing_campaigns.type'])
			->from("reminders")
			->where(
				"(last_ran < '".$currentTimestamp->copy()->subDay()->toDateTimeString().
				"' AND
				timebefore_unit = 'day')
				OR
				(last_ran < '".$currentTimestamp->copy()->toDateTimeString().
				"' AND
				timebefore_unit = 'minute')"
			)
			->where("marketing_campaigns.status = 'reminder'")
			//->where("reminder.last_ran <","text")
			->join("marketing_campaigns","reminders.campaign_id = marketing_campaigns.campaign_id","left")
			->get();



		if($results->num_rows == 0){
			return;
		}
		$results = $results->result_array();



		$reminderQueue = new \fu\reminders\ReminderQueue($this);
		foreach($results as $row)
		{
			$reminderQueue->sendToQueue($row['id'],$currentTimestamp->toDateTimeString());


			$this->db->where('id', $row['id']);
			$this->db->update('reminders', ["last_ran"=>$currentTimestamp->toDateTimeString()]);
		}

		return;



	}

	function run_recurring_events($cron_key)
	{
		if ($cron_key != $this->cron_key)
			return;

		//Get all events with an occurence this hour
		//Mark each one as ran so they don't get duplicated
		//Then run each one
		$this->load->model("repeatable");

		date_default_timezone_set('UTC');
		$current_time = new \Carbon\Carbon();

		$repeatable_model = new repeatable();
		$events = $repeatable_model->getAllBy([
			"next_occurence >="=>$current_time->format("Y-m-d H"),
			"next_occurence <"=>$current_time->addHour()->format("Y-m-d H"),
			"last_ran <="=>$current_time->subHour()->format("Y-m-d H")
		]);

		foreach($events as $event){
			$event->markAsRan();
			$repeatable_model->saveRowModel($event);
		}
		foreach($events as $event){
			if($event->type == "marketing_campaign"){
				$campaign = $this->Marketing_campaign->get_info($event->resource_id,true );
				if($this->Marketing_campaign->get_reports_for_campaign($campaign->campaign_id)){
					$recipientRetriever = new \fu\marketing\recipientRetriever\report();
				} else {
					$recipientRetriever = new \fu\marketing\recipientRetriever\unsent();
				}
				$recipientRetriever->setPossibleRecipients(unserialize($campaign->recipients));
				$recipientRetriever->setCampaign($campaign);
				$campaignDispatcher = new \fu\marketing\Campaign($recipientRetriever,$campaign);
				$campaignDispatcher->send();

				$campaign = (array) $campaign;
				$this->Marketing_campaign->save($campaign,$campaign['campaign_id']) ;
			}
		}


	}

	function send_marketing_campaigns($cron_key)
	{
		if ($cron_key != $this->cron_key)
			return;
		date_default_timezone_set('America/Chicago');
		set_time_limit(0);
		ini_set('memory_limit', '600M');


		$unsentCampaigns = $this->Marketing_campaign->get_all_unsent_campaigns();
        if (function_exists('newrelic_custom_metric')) {
            newrelic_custom_metric('marketing_cron', 1000);
            newrelic_custom_metric('sent_marketing_campaigns', count($unsentCampaigns) * 1000);
        }

	    foreach($unsentCampaigns as $campaign)
	    {
		    if($this->Marketing_campaign->get_reports_for_campaign($campaign->campaign_id)){
			    $recipientRetriever = new \fu\marketing\recipientRetriever\report();
		    } else {
			    $recipientRetriever = new \fu\marketing\recipientRetriever\unsent();
		    }
			$recipientRetriever->setPossibleRecipients(unserialize($campaign->recipients));
			$recipientRetriever->setCampaign($campaign);
			$campaignDispatcher = new \fu\marketing\Campaign($recipientRetriever,$campaign);
			$campaignDispatcher->send();
	    }

		//Run all other texting related notifications
		//These need to be ran together with the marketing because all texting needs to be throttled on our end.
		$this->Marketing_Texting->runQueuedJobs();
	}
	function invite_new_customers($cron_key)
	{
		if ($cron_key != $this->cron_key)
			return;
		//Invite all new customers who have been added since last time
		$this->Marketing_Texting->inviteNewCustomers();
	}
	function service_start_notification($service)
	{
        return true;
		if ($service == 'send_marketing_campaigns' && (date('H') < 6 || date('H') > 7 || date('i') > 5))
			return;
		if ($service == 'generate_invoices' && (date('H') < 6 || date('H') > 7))
			return;
		if ($service == 'send_tee_time_thank_yous' && (date('H') < 7 || date('H') > 8))
			return;
		if ($service == 'quickbooks_sync' && (date('H') >= 21 || date('H') <= 3))
			return;
        if ($service == 'clean_out_sessions' && (date('H') >= 0 || date('H') <= 1))
            return;
        if ($service == 'clean_out_demo_accounts' && (date('H') >= 0 || date('H') <= 1))
            return;

        send_sendgrid(
			'jhopkins@foreup.com',
			"Restarted $service Daemon Service ".date("Y-m-d h:i:s"),
			"$service Daemon Service has just restarted",
			'daemon@foreup.com',
		 	'ForeUP Daemon'
		);
	}

	function minimum_charges($course_id = false, $date = false){
		$this->load->model('Minimum_charge');
		//$this->Minimum_charge->setDryRun(true);
		$charges_ran = $this->Minimum_charge->charge_customers($course_id, $date);

		$msg = '';
		if(!$charges_ran){
			$msg = 'No minimum charges ran';
			echo $msg;
		}
		
		$msg .= '('.$this->Minimum_charge->charge_count.') minimum charges created';
		echo $msg;
	}

	function test_minimums($course_id,$run_date,$start_date,$difference = 0)
	{
		//Run Minimums as if it were on the 30th
		$this->load->model("Minimum_charge");
		$min = new Minimum_charge();
		$min->setDryRun(true);
		$charges = $min->charge_customers($course_id,$run_date,$start_date);
		if($difference ==0){
			echo json_encode($charges);
			return;
		}

		$difference = [];
		foreach($charges as $charge){
			$row = $this->db->from("account_transactions")
				->where("course_id",$charge['customer']['course_id'])
				->where("trans_date",$charge['transaction_date'])
				->where("trans_customer",$charge['customer']['customer_id'])->get()->row_array();
			$charge['charge_should_have_been'] = $charge['difference'];
			if($charge['charge_should_have_been']>0){
				$charge['charge_should_have_been'] = 0;
			}
			unset($charge['difference']);
			if(isset($row['trans_amount']) && (double)$row['trans_amount']!=(double)$charge['charge_should_have_been']){

				$charge['actual_charge'] = (double)$row['trans_amount'];
				$charge['credit'] = -1*($charge['actual_charge'] -  $charge['charge_should_have_been']);
				if($charge['credit'] < .01)
					continue;
				$difference[] = $charge;
			}
		}

		echo json_encode($difference);
	}

	function run_invoice_queue()
	{
		// LOADING LIBRARIES AND MODELS
		$this->load->model('Queue_invoice');
		$this->load->library('zip');
		$this->load->model('Customer_credit_card');
		$this->load->model('Customer_billing');
		$this->load->model('Invoice');
		$this->load->library('Html2pdf');

		$jobs = $this->Queue_invoice->get_jobs();

		foreach ($jobs as $key => $job) {
			//include member balance or customer credit balances on invoice?
			if ($job['member_balance'] && $job['customer_credit'])
				$batch_type = 'both';
			else if ($job['member_balance'])
				$batch_type = 'member';
			else if ($job['customer_credit'])
				$batch_type = 'customer';

			//general course info
			$course_info = $this->Course->get_info($job['course_id']);

			$customers = $this->Customer->get_negative_balances($batch_type);
			foreach ($customers as $key => $customer) {
				$customers_by_id[$customer->person_id] = $customer;
			}
			$combined_doc = new Html2pdf('P','A4','en');
			$combined_doc->pdf->SetDisplayMode('fullpage');
			$combined_doc->setDefaultFont('Arial');

			$start_date = $job['start_date'];
			$end_date = $job['end_date'];
			$limit = 10000;
			$offset = 0;

			$billings = $this->Customer_billing->get_all_in_range($start_date, $end_date, $limit, $offset);

			// echo $this->db->last_query();
			foreach ($billings->result_array() as $key => $billing) {
				//check the database for an invoice related to this billing
				$existing_invoice = $this->Invoice->get_recurring_billing_invoice($billing, $start_date, $end_date);
				$customer = $this->Customer->get_info($billing['person_id']);

				if (!$existing_invoice) {
					$invoice_data = array(
						'course_id'=>$billing['course_id'],
						'credit_card_id'=>$billing['credit_card_id'],
						'billing_id'=>$billing['billing_id'],
						'person_id'=>$billing['person_id'],
						'month_billed'=>$start_date
					);

					$this->Invoice->save($invoice_data);

					$invoice_number = $existing_invoice ? $existing_invoice['invoice_number'] : $invoice_data['invoice_number'];
					$item_data = array();
					$calculated_total = 0;
					$items=$this->Customer_billing->get_items($billing['billing_id']);
					// Just saving to invoices
					foreach ($items as $index => $item)
					{
						$item_data[] = array(
							'invoice_id'=> $existing_invoice? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'],
							'line_number'=>$index,
							'description'=>$item['description'],
							'quantity'=>$item['quantity'],
							'amount'=>$item['amount'],
							'tax'=>$item['tax'],
							'pay_account_balance'=>0,
							'pay_member_balance'=>0
						);
						$calculated_total += (int)$item['quantity'] * (float)$item['amount'] * (1 + (float)$item['tax']/100);
					}
					// ADD CUSTOMER ACCOUNT BALANCE
					if (($billing['pay_account_balance'] || $job['customer_credit']) && $customer->account_balance < 0)
					{
						$index++;
						$item_data[] = array(
							'invoice_id'=> $existing_invoice? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'],
							'line_number'=>$index,
							'description'=>($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname')),
							'quantity'=>1,
							'amount'=>-$customer->account_balance,
							'tax'=>0,
							'pay_account_balance'=>1,
							'pay_member_balance'=>0
						);
						$calculated_total += -(float)$customer->account_balance;
					}
					// ADD MEMBER ACCOUNT BALANCE
					if (($billing['pay_member_balance'] || $job['member_balance']) && $customer->member_account_balance < 0)
					{
						$index++;
						$item_data[] = array(
							'invoice_id'=>$existing_invoice? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'],
							'line_number'=>$index,
							'description'=>($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')),
							'quantity'=>1,
							'amount'=>-$customer->member_account_balance,
							'tax'=>0,
							'pay_account_balance'=>0,
							'pay_member_balance'=>1
						);
						$calculated_total += -(float)$customer->member_account_balance;
					}

					$invoice_id = $existing_invoice? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'];
					$this->Invoice->save_items($item_data, $invoice_id);

					// Charge credit card and update the invoice
					$invoice_data = array(
						'total'=>$calculated_total
					);
					if (!$existing_invoice) {
						$invoice_data['paid'] = '0.00';
					}

					$this->Invoice->save($invoice_data, $invoice_id);
				} else {
					$item_data = $this->Invoice->get_items($existing_invoice['invoice_id']);
				}
				$course_info = $this->Course->get_info($billing['course_id']);

				$invoice_data = $this->Invoice->get_info($existing_invoice? $existing_invoice['invoice_id'] : $invoice_data['invoice_id']);

				$data = $invoice_data[0];
				$data['invoice_number'] = $existing_invoice ? $existing_invoice['invoice_number'] : $invoice_number;
				$data['total_due'] = $existing_invoice ? $existing_invoice['total_due'] : $calculated_total;
				$data['total_paid'] = $existing_invoice ? $existing_invoice['paid'] : 0;
				$data['invoice_id'] = $existing_invoice ? $existing_invoice['invoice_id'] : $invoice_data['invoice_id'];
				$data['items']=$item_data;
				$data['person_info']=$customer;
				$data['course_info']=$course_info;
				$data['credit_cards']='';
				$data['popup'] = 1;// = array(
				$data['is_invoice'] = true;
				$data['sent'] = true;
				$data['pdf'] = true;

				$invoice_html = $this->load->view('customers/invoice', $data, true);
				$html2pdf = new Html2pdf('P','A4','en');

				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->setDefaultFont('Arial');
				$html2pdf->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
				$combined_doc->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
				$this->zip->add_data("{$customer->last_name}_{$customer->first_name}_".date('m-Y').".pdf", $html2pdf->Output('', true));

				unset($customers_by_id[$customer->person_id]);

			}

			foreach ($customers_by_id as $customer)
			{
				// Save invoice
				$invoice_data = array(
					'course_id'=>$this->session->userdata('course_id'),
					'person_id'=>$customer->person_id
				);
				$this->Invoice->save($invoice_data);

				$invoice_number = $invoice_data['invoice_number'];

				$item_data = array();
				$calculated_total = 0;
				$index = 0;
				// Just saving to invoices
				if (($batch_type == 'member' || $batch_type == 'both') && $customer->member_account_balance < 0)
				{
					$item_data[] = array(
						'invoice_id'=>$invoice_data['invoice_id'],
						'line_number'=>$index,
						'description'=>($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')),
						'quantity'=>1,
						'amount'=>-$customer->member_account_balance,
						'tax'=>0,
						'pay_account_balance'=>0,
						'pay_member_balance'=>1

					);
					$calculated_total += -$customer->member_account_balance;
					$index ++;
				}
				if (($batch_type == 'customer' || $batch_type == 'both') && $customer->account_balance < 0)
				{
					$item_data[] = array(
						'invoice_id'=>$invoice_data['invoice_id'],
						'line_number'=>$index,
						'description'=>($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname')),
						'quantity'=>1,
						'amount'=>-$customer->account_balance,
						'tax'=>0,
						'pay_account_balance'=>1,
						'pay_member_balance'=>0
					);
					$calculated_total += -$customer->account_balance;
				}
				$this->Invoice->save_items($item_data);
				//print_r($item_data);
				// Charge credit card and update the invoice
				$invoice_id = $invoice_data['invoice_id'];
				$invoice_data = array(
					'total'=>$calculated_total
				);
				$this->Invoice->save($invoice_data, $invoice_id);

				// Email copy of invoice
				if ($email_invoice)
					$this->Invoice->email($invoice_data['invoice_id']);//$invoice_id
				// Add to zip file
				$invoice_data = $this->Invoice->get_info($invoice_id);
				$data = $invoice_data[0];
				$data['invoice_number'] = $invoice_number;
				$data['total_due'] = $calculated_total;
				$data['total_paid'] = 0;
				$data['invoice_id'] = $invoice_id;
				$data['items']=$item_data;
				$data['person_info']=$customer;
				$data['course_info']=$course_info;
				$data['credit_cards']='';
				$data['popup'] = 1;// = array(
				$data['is_invoice'] = true;
				$data['sent'] = true;
				$data['pdf'] = true;
				$invoice_html = $this->load->view('customers/invoice', $data, true);
				$html2pdf = new Html2pdf('P','A4','en');
				//$html2pdf->setModeDebug();
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->setDefaultFont('Arial');
				$html2pdf->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
				$combined_doc->writeHTML("<page style'width:600px;'>".$invoice_html.'</page>');
				$this->zip->add_data("{$customer->last_name}_{$customer->first_name}_".date('m-Y', strtotime($start_date)).".pdf", $html2pdf->Output('', true));

			}

			$this->zip->add_data(date('m-Y', strtotime($start_date))."_Combined_Invoices".".pdf", $combined_doc->Output('', true));
			if (!$this->zip->download("invoices_".date('m-Y', strtotime($start_date)).".zip")) {

				$no_invoices_data = array(
					'member_balance'=>$member_balance,
					'customer_credit'=>$customer_credit,
					'recurring_billings'=>$recurring_billings,
					'start_on'=>$start_date,
					'end_on'=>$end_date
				);

				$this->load->view('customers/no_invoices',$no_invoices_data);
			}

			return;


		}//end of foreach on jobs


	}

	function queue_start(){

		session_write_close();
		$this->load->model('exec_queue');
		$this->exec_queue->startThread();
	}

// FROM HERE ON, THESE FUNCTIONS ARE FROM THE ORIGINAL CRON SCRIPT
    function index()
    {
        //echo 'adding item';
        //log_message('error', 'We are successfully hitting the daemon controller');
		send_sendgrid(
			'signorehopkins@gmail.com',
			'Testing to see if our system works well',
			'I think it is working just fine, dont you?',
			'testing@foreup.com',//$this->session->userdata('course_email'),
			'ForeUP Test'
		);
		echo 'We are successfully hitting the daemon controller';
    }

	function auto_bill_credit_cards($cron_key)
	{
		//echo 'getting inside auto_bill_credit_cards';
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			set_time_limit(0);
			$product_names = array('1'=>'Software','2'=>'Website','3'=>'Marketing');
			$this->load->model('Billing');
			$this->load->model('Credit_card');
			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			//echo 'getting into it<br/>';
			$todays_billings = $this->Billing->get_todays_billings();
			$todays_billed = $this->Billing->get_todays_billed();
			echo '<br/>'.$this->db->last_query().'<br/><br/>';
			$todays_billings = $todays_billings->result_array();
			$billing_summary_data = array();

            if (function_exists('newrelic_custom_metric')) {
                newrelic_custom_metric('invoices_cron', 1000);
                newrelic_custom_metric('billings_run', count($todays_billings) * 1000);
            }

			print_r($todays_billings);

			//echo '<br/>'.$this->db->last_query().'<br/>';
			$total_billed = 0;
			$billed_count = 0;
			$total_failed = 0;
			$failed_count = 0;
			foreach ($todays_billings as $billing)
			{
                $this->load->database();
                $this->db->reconnect();
				if (in_array($billing['billing_id'], $todays_billed))
				{
					echo "<br/>Duplicate billing {$billing['billing_id']}<br/>";
					continue;
				}
				//Start success
				$this->Billing->mark_as_started($billing['billing_id']);

				$tax_rates = array();//explode(',', $billing['tax_rates']);
				//$tax_names = explode(',', $billing['tax_names']);
				$contact_emails = array_unique(explode(',', $billing['contact_emails']));
				$products = explode(',', $billing['products']);

				$credit_card_charges = $product_list = array();
				$subtotal = 0;
				$product_list = $this->Billing->get_billing_products($billing['course_id'], $billing['credit_card_id']);
				echo '<br/><br/>'.$this->db->last_query();
				print_r($product_list->result_array());
				foreach ($product_list->result_array() as $product)
				{
					$payment_type = ($product['free'] ? 'Free' : ($product['monthly'] ? 'Monthly Payment' : ($product['annual'] ? 'Annual Payment' : '')));
					$payment_amount = ($product['free'] ? 0 : ($product['monthly'] ? $product['monthly_amount'] : ($product['annual'] ? $product['annual_amount'] : 0)));
					$subtotal += $payment_amount;
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>"ForeUP Services - {$product_names[$product['product']]} $payment_type", 'amount'=>$payment_amount);
					$tax_rates[$product['tax_name']] = array('tax_amount'=>$payment_amount * $product['tax_rate'] / 100, 'tax_rate' => $product['tax_rate']);
				}
				echo '<br/>Parsed product list<br/>';
				print_r($credit_card_charges);
/*
				if ($billing['total_annual_amount'] != '0.00')
				{
				}
				if ($billing['total_monthly_amount'] != '0.00')
				{
					$subtotal += $billing['total_monthly_amount'];
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>'foreUP Services - Monthly Payment', 'amount'=>$billing['total_monthly_amount']);
				}
				foreach ($products as $product)
					$product_list[] = array('line_number'=>count($credit_card_charges)+1,'description'=>' - '.$product_names[$product], 'amount'=>'');
			*/
				/*if ($billing['annual'] && $billing['annual_month'] == date('n') && $billing['annual_day'] == date('j'))
				{
					$subtotal += $billing['annual_amount'];
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>'foreUP '.$products[$billing['product']].' Services - Annual Payment', 'amount'=>$billing['annual_amount']);
				}
				if ($billing['monthly'] && $billing['period_start'] <= date('n') && $billing['period_end'] >= date('n') && $billing['monthly_day'] == date('j'))
				{
					$subtotal += $billing['monthly_amount'];
					$credit_card_charges[] = array('line_number'=>count($credit_card_charges)+1,'description'=>'foreUP '.$products[$billing['product']].' Services - Monthly Payment', 'amount'=>$billing['monthly_amount']);
				}*/
				$taxes = $billing['annual_tax_amount'] + $billing['monthly_tax_amount'];
				$total = $subtotal + $taxes;
				$totals = array('subtotal'=>$subtotal,'taxes'=>$taxes,'total'=>$total);
				//print_r($billing);
				//echo '<br/>';
				$course_info = $this->Course->get_info($billing['course_id']);
				//$invoice = $this->Sale->add_credit_card_payment(array('mercury_id'=>config_item('foreup_mercury_id'),'tran_type'=>'CreditSaleToken','frequency'=>'Recurring'));
				$invoice = $this->Sale->add_credit_card_payment(array('mercury_id'=>config_item('foreup_mercury_id'),'tran_type'=>'CreditSaleToken','frequency'=>'Recurring'));
				$employee = 'Auto Billing';
				echo '<br/>Added Credit Card Payment<br/>';
				$HC->set_frequency('Recurring');
				$HC->set_token($billing['token']);
				$HC->set_cardholder_name($billing['cardholder_name']);
				$HC->set_invoice($invoice);
				$HC->set_merchant_credentials(config_item('foreup_mercury_id'),config_item('foreup_mercury_password'));//foreUP Credentials
				//$HC->set_merchant_credentials(config_item('test_mercury_id'),config_item('test_mercury_password'));//Test Credentials
				echo '<br/>Set all credentials<br/>';
				//echo config_item('foreup_mercury_id').' here are the credentials - '.config_item('foreup_mercury_password');
				//echo 'and here === '.$this->config->item('mercury_id').' --- '.$this->mercury_id;
				//return;
				$transaction_results = $HC->token_transaction('Sale',$total, '0.00', $taxes);
				echo '<br/>Ran transaction<br/>';
				$payment_data = array(
					'acq_ref_data'=>(string)$transaction_results->AcqRefData,
					'auth_code'=>(string)$transaction_results->AuthCode,
					'auth_amount'=>(string)$transaction_results->AuthorizeAmount,
					'avs_result'=>(string)$transaction_results->AVSResult,
					'batch_no'=>(string)$transaction_results->BatchNo,
					'card_type'=>(string)$transaction_results->CardType,
					'cvv_result'=>(string)$transaction_results->CVVResult,
					'gratuity_amount'=>(string)$transaction_results->GratuityAmount,
					'masked_account'=>(string)$transaction_results->Account,
					'status_message'=>(string)$transaction_results->Message,
					'amount'=>(string)$transaction_results->PurchaseAmount,
					'ref_no'=>(string)$transaction_results->RefNo,
					'status'=>(string)$transaction_results->Status,
					'token'=>(string)$transaction_results->Token,
					'process_data'=>(string)$transaction_results->ProcessData
				);
				$this->Sale->update_credit_card_payment($invoice, $payment_data);

				$credit_card_id = $billing['credit_card_id'];
				$credit_card_data = array(
					'token'=>$payment_data['token'],
					'token_expiration'=>date('Y-m-d', strtotime('+2 years'))
				);
				//echo "<br/>Status {$payment_data['status']}<br/>";
				if ($payment_data['status'] == 'Approved')
				{
					echo "<br/>Approved<br/>";
					$this->Credit_card->save($credit_card_data, $credit_card_id);
					//$success = $payment_data['status'] == 'Approved' ? 1 : 0;
					$this->Credit_card->record_charges($credit_card_id, $credit_card_charges, array('total'=>$total), $billing['billing_id'], 1);
				}
                else
                {
                    $this->Credit_card->record_charges($credit_card_id, $credit_card_charges, array('total'=>$total), $billing['billing_id'], 0);
                }
				$data = array(
					'receipt_title'=>'Auto Billing Receipt',
					'transaction_time'=>date('Y-m-d'),
					'customer'=>$course_info->name,
					'contact_email'=>$billing['contact_emails'],
					'sale_id'=>$invoice,
					'employee'=>$employee,
					'items'=>$credit_card_charges,
					'product_list'=>$product_list,
					'tax_rates'=>$tax_rates,
					'totals'=>$totals,
					'payment_data'=>$payment_data
				);
				$subject = ($payment_data['status'] == 'Approved'?'':'Declined ');
				$data['status_message'] = ($payment_data['status'] == 'Approved'?'':$payment_data['status_message']);
				$subject .=  $course_info->name.' Auto Bill';
				$recipients = array('jhopkins@foreup.com');
				if ($payment_data['status'] == 'Approved')
				{
					//Record success
					$this->Billing->mark_as_charged($billing['billing_id']);
					$total_billed += $total;
					$billed_count ++;
					if (count($contact_emails) > 0)
					{
						//Email success
						$this->Billing->mark_as_sent($billing['billing_id']);
						foreach ($contact_emails as $contact_email)
							$recipients[] = $contact_email;
					}
				}
				else {
					$total_failed += $total;
					$failed_count ++;
				}
				$billing_summary_data['summary_data'][$data['customer']] = $data;
				echo $subject;
				print_r($recipients);
				send_sendgrid($recipients, $subject, $this->load->view("billing/receipt_email",$data, true), 'billing@foreup.com', 'billing@foreup.com');
                $this->db->close();
			}
			ksort($billing_summary_data['summary_data']);
			$billing_summary_data['total_billed'] = $total_billed;
			$billing_summary_data['total_failed'] = $total_failed;
			$billing_summary_data['billed_count'] = $billed_count;
			$billing_summary_data['failed_count'] = $failed_count;

			send_sendgrid(array('billing@foreup.com','jhopkins@foreup.com', 'matson@foreup.com'), 'Billing Summary', $this->load->view("billing/billing_summary",$billing_summary_data, true), 'billing@foreup.com', 'billing@foreup.com');

		//echo json_encode(array('success'=>true, 'billing_count' => count($todays_billings)));
		//echo json_encode(array('success'=>$payment_data['status']=='Approved'?true:false, 'message'=>"A charge of {$payment_data['amount']} was {$payment_data['status']} for card {$payment_data['masked_account']}", 'item_id'=>0));
		}
	}

	function auto_bill_customers($cron_key, $days = 0, $GENERATE_INVOICES = true, $GENERATE_PDFS = false, $EMAIL_PDFS_TO_COURSE = false, $EMAIL_INVOICES = false, $CHARGE_INVOICES = false, $override = false, $course_id = false)
	{
		if ($cron_key != $this->cron_key){
			return;
		}
        echo 'Had the correct key<br/>';
		//$GENERATE_INVOICES		= true;
		//$CHARGE_INVOICES 		= true;
		//$EMAIL_INVOICES			= true;
		//$GENERATE_PDFS 			= true;
		//$EMAIL_PDFS_TO_COURSE 	= true;
		// Date to generate invoices for, default: today
		date_default_timezone_set('US/Mountain');		
		$date = date('Y-m-d', strtotime("+$days days"));
		$datetime = date('Y-m-d H:i:s', strtotime("+$days days"));
		
		set_time_limit(0);
		ini_set('memory_limit', '500M');




		// First run the F&B minimum spend function
		// (Only runs on 1st of the month)
		//$this->minimum_spend($cron_key);
		//$this->minimum_charges();
		$cron_output = '* Starting auto_bill_customers *'.PHP_EOL;
		
		$current_hour = date('H');

		$courses_only = true;
		$courses = $this->Customer_billing->get_invoices_to_generate($date, $course_id, $courses_only);
        $invoices_to_charge = $this->Invoice->get_invoices_to_charge(null, $course_id);

        foreach($invoices_to_charge as $invoice_to_charge)
        {
            $courses[$invoice_to_charge['course_id']] = 1;
        }

        // Sweep up any invoices that should have been sent that haven't been yet
//        $invoices_to_email = $this->Invoice->get_invoices_to_email();
//
//        foreach($invoices_to_email as $invoice_to_email)
//        {
//            $courses[$invoice_to_email['course_id']] = 1;
//        }

        $courses = array_keys($courses);

		// NEW CODE TO ADD exec_queue JOBS
		foreach ($courses as $course)
		{
			var_dump($course);
            // Don't add to the queue if it's already there
            if (!$this->exec_queue->alreadyInQueue([
                "command"=>"php index.php daemon generate_invoices $days {$course} $GENERATE_INVOICES $CHARGE_INVOICES $EMAIL_INVOICES $GENERATE_PDFS $EMAIL_PDFS_TO_COURSE"
            ])) {
                $this->exec_queue->insert([
                    "command" => "php index.php daemon generate_invoices $days {$course} $GENERATE_INVOICES $CHARGE_INVOICES $EMAIL_INVOICES $GENERATE_PDFS $EMAIL_PDFS_TO_COURSE"
                ]);
            }
		}
		// END NEW CODE TO ADD exec_queue JOBS
		return;

        echo 'Trying to generate invoices<br/>';
		if($GENERATE_INVOICES && count($courses) > 0){
	
			$this->benchmark->mark('generate_invoices_start');
			$this->session->unset_userdata('course_id');

			// Get recurring invoices to generate for today
			$invoices_to_generate = $this->Customer_billing->get_invoices_to_generate($date, $courses[0]);
			// Loop through recurring bills and generate necessary invoices
			$this->session->set_userdata('course_id', $courses[0]);
			// Set tax included
			$course_info = $this->Course->get_info($courses[0]);
			$this->config->set_item('unit_price_includes_tax', $course_info->unit_price_includes_tax);		
					
			$invoices_generated = 0;
			if(!empty($invoices_to_generate)){
				foreach($invoices_to_generate as $recurring_invoice){
					$response = $this->Invoice->generate_from_billing($recurring_invoice['billing_id'], $datetime);
					if (!$response || !$response['sale_id'] || $response['sale_id'] == -1)
					{
						// MARK BILLING AS HAVING AN ERROR
						$this->Customer_billing->register_error($recurring_invoice['billing_id']);
						$cron_output .= "ERROR OCCURRED: BILLING #".$recurring_invoice['billing_id']." - INVOICE #".$response['invoice_id']." HAD AN ERROR, RESETARTING".PHP_EOL;
						$cron_output .= "PEAK MEMORY USED: ".round(memory_get_peak_usage(true) / 1000000, 2).' Mbs'.PHP_EOL; 
						echo $cron_output;
						//sleep(20 * 1);
						return;
					}
					$invoices_generated++;
				}
			}
			$this->benchmark->mark('generate_invoices_end');

			$cron_output .= "Invoices Generated For Course {$courses[0]}: ".
				$invoices_generated.'/'.count($invoices_to_generate).
				' ('.$this->benchmark->elapsed_time('generate_invoices_start', 'generate_invoices_end').' seconds)'.
				PHP_EOL;
            unset($invoices_to_generate);
		}
		if ($GENERATE_INVOICES && count($courses) > 1) {
			$cron_output .= "PEAK MEMORY USED: ".round(memory_get_peak_usage(true) / 1000000, 2).' Mbs'.PHP_EOL; 
			echo $cron_output;
			if (base_url() != 'http://localhost:8888/') {
				//sleep(60 * 1);
			}
			return;
		}
        // Attempting to charge invoices
        echo 'Trying to charge invoices<br/>';
		if($CHARGE_INVOICES && ($override || ($current_hour >= 2 && $current_hour < 4))){
	
			$this->session->unset_userdata('course_id');			
			$this->benchmark->mark('charge_invoices_start');
			// Retrieve invoices that need charged on credit card
			$invoices_to_charge = $this->Invoice->get_invoices_to_charge();

			// Loop through invoices and charge cards
			$invoice_charges = 0;
			$invoice_charges_failed = 0;

			if(!empty($invoices_to_charge)){
				foreach($invoices_to_charge as $invoice_to_charge){
					
					$this->session->set_userdata('course_id', $invoice_to_charge['course_id']);
                    $success = $this->Invoice->charge_invoice(
						$invoice_to_charge['invoice_id'],
						$invoice_to_charge['course_id'],
						$invoice_to_charge['credit_card_id'],
						$invoice_to_charge['due'],
						false,
						true
					);
					if($success){
						$invoice_charges++;
					}else{
						$invoice_charges_failed++;
					}
					if ($success === 1) {
						break;
					}
				}
			}
			$this->benchmark->mark('charge_invoices_stop');

			unset($invoices_to_charge);
			$cron_output .= 'Invoice Charges - SUCCESS:'. $invoice_charges.' FAILED:'.$invoice_charges_failed.
				' ('.$this->benchmark->elapsed_time('charge_invoices_start', 'charge_invoices_stop').' seconds)'.
				PHP_EOL;
			if (isset($success) && $success === 1) {
				$cron_output .= "PEAK MEMORY USED: ".round(memory_get_peak_usage(true) / 1000000, 2).' Mbs'.PHP_EOL; 
				echo $cron_output;
				if (base_url() != 'http://localhost:8888/') {
					//sleep(60 * 1);
				}
				return;
			}
		}

        echo 'Attempting to email invoices<br/>';
		if($EMAIL_INVOICES && ($override || ($current_hour >= 3 && $current_hour < 4))){
	
			$this->session->unset_userdata('course_id');	
			$this->benchmark->mark('email_invoice_start');
			// Retrieve invoices that need emailed
			$invoices_to_email = $this->Invoice->get_invoices_to_email();
			// Loop through invoices and send emails
			$emails_sent = 0;
			if(count($invoices_to_email) > 0){
				foreach($invoices_to_email as $invoice_to_email){
					$success = $this->Invoice->send_email($invoice_to_email['invoice_id']);
					$emails_sent++;
				}
			}

			$this->benchmark->mark('email_invoice_stop');
			$cron_output .= 'Invoices Emailed: '.
				$emails_sent.
				' ('.$this->benchmark->elapsed_time('email_invoice_start', 'email_invoice_stop').' seconds)'.
				PHP_EOL;
		}

        echo 'Attempting to generate pdfs<br/>';
		if($GENERATE_PDFS && ($override || ($current_hour >= 4 && $current_hour < 6))){
			$this->load->model("exec_queue");

			$billed_courses = $this->Invoice->get_billed_courses($date, $course_id);
			$jobsAdded = 0;
			foreach($billed_courses as $course){
				$last_emailed_date = $this->Course->get_last_emailed_date($course['course_id']);
				if($last_emailed_date >= $date){
					continue;
				}
				$this->Course->date_stamp_sent_invoices($course['course_id']);
				$this->exec_queue->insert([
					"command"=>"php index.php daemon generate_pdfs $days {$course['course_id']} $EMAIL_PDFS_TO_COURSE"
				]);
				$jobsAdded ++;
			}

			if($jobsAdded > 0){
				$results = shell_exec("php index.php daemon queue_start");
			}


		}
		
		$cron_output .= "PEAK MEMORY USED: ".round(memory_get_peak_usage(true) / 1000000, 2).' Mbs'.PHP_EOL; 
		echo $cron_output;
	}

	function generate_invoices($days, $course_id, $GENERATE_INVOICES = true, $CHARGE_INVOICES = false, $EMAIL_INVOICES = false, $GENERATE_PDFS = false, $EMAIL_PDFS_TO_COURSE = false)
	{
        $this->load->model('Invoice');
        $this->load->model('Customer_billing');
        $this->load->model("exec_queue");

		if($GENERATE_INVOICES) {
            // Run minimum charges for this course
            $this->minimum_charges($course_id);

			date_default_timezone_set('US/Mountain');
			$date = date('Y-m-d', strtotime("+$days days"));
			$datetime = date('Y-m-d H:i:s', strtotime("+$days days"));
			$cron_output = '';

			set_time_limit(0);
			ini_set('memory_limit', '500M');

			$this->benchmark->mark('generate_invoices_start');
			$this->session->unset_userdata('course_id');

			// Get recurring invoices to generate for today
			$invoices_to_generate = $this->Customer_billing->get_invoices_to_generate($date, $course_id);
            // Loop through recurring bills and generate necessary invoices
			$this->session->set_userdata('course_id', $course_id);
			// Set tax included
			$course_info = $this->Course->get_info($course_id);
			$this->config->set_item('unit_price_includes_tax', $course_info->unit_price_includes_tax);
			$invoices_generated = 0;
			if (!empty($invoices_to_generate)) {
				foreach ($invoices_to_generate as $recurring_invoice) {
					$response = $this->Invoice->generate_from_billing($recurring_invoice['billing_id'], $datetime);
					if (!$response || !$response['sale_id'] || $response['sale_id'] == -1) {
						// MARK BILLING AS HAVING AN ERROR
						$this->Customer_billing->register_error($recurring_invoice['billing_id']);
						$cron_output .= "ERROR OCCURRED: BILLING #" . $recurring_invoice['billing_id'] . " - INVOICE #" . $response['invoice_id'] . " HAD AN ERROR, RESETARTING" . PHP_EOL;
						$cron_output .= "PEAK MEMORY USED: " . round(memory_get_peak_usage(true) / 1000000, 2) . ' Mbs' . PHP_EOL;
						echo $cron_output;
						//sleep(20 * 1);
						return;
					}
					$invoices_generated++;
				}
			}

			$this->benchmark->mark('generate_invoices_end');

			$cron_output .= "Invoices Generated For Course {$course_id}: " .
				$invoices_generated . '/' . count($invoices_to_generate) .
				' (' . $this->benchmark->elapsed_time('generate_invoices_start', 'generate_invoices_end') . ' seconds)' .
				PHP_EOL;

			unset($invoices_to_generate);

			echo $cron_output;
		}

		// Add charging invoices to the exec_queue
        if (!empty($course_id)) {
            if (!$this->exec_queue->alreadyInQueue([
                "command"=>"php index.php daemon charge_invoices $days $course_id $CHARGE_INVOICES $EMAIL_INVOICES $GENERATE_PDFS $EMAIL_PDFS_TO_COURSE"
            ])) {
                $this->exec_queue->insert([
                    "command" => "php index.php daemon charge_invoices $days $course_id $CHARGE_INVOICES $EMAIL_INVOICES $GENERATE_PDFS $EMAIL_PDFS_TO_COURSE"
                ]);
            }
        }

		return;
	}

	function charge_invoices($days, $course_id, $CHARGE_INVOICES = false, $EMAIL_INVOICES = false, $GENERATE_PDFS = false, $EMAIL_PDFS_TO_COURSE = false)
	{
        $this->load->model('Invoice');
        $this->load->model("exec_queue");
        $this->load->model('Customer_credit_card');

        if ($CHARGE_INVOICES) {

			$this->session->unset_userdata('course_id');
			$this->benchmark->mark('charge_invoices_start');
			// Retrieve invoices that need charged on credit card
			$invoices_to_charge = $this->Invoice->get_invoices_to_charge(null, $course_id);

			// Loop through invoices and charge cards
			$invoice_charges = 0;
			$invoice_charges_failed = 0;
			$cron_output = '';

			if (!empty($invoices_to_charge)) {
				foreach ($invoices_to_charge as $invoice_to_charge) {
					$this->session->set_userdata('course_id', $invoice_to_charge['course_id']);
					$success = $this->Invoice->charge_invoice(
						$invoice_to_charge['invoice_id'],
						$invoice_to_charge['course_id'],
						$invoice_to_charge['credit_card_id'],
						$invoice_to_charge['due'],
						false,
						true
					);
					if ($success) {
						$invoice_charges++;
					} else {
						$invoice_charges_failed++;
					}
					if ($success === 1) {
                        break;
					}
				}
			}
			$this->benchmark->mark('charge_invoices_stop');

			unset($invoices_to_charge);
			$cron_output .= 'Invoice Charges - SUCCESS:' . $invoice_charges . ' FAILED:' . $invoice_charges_failed .
				' (' . $this->benchmark->elapsed_time('charge_invoices_start', 'charge_invoices_stop') . ' seconds)' .
				PHP_EOL;

			echo $cron_output;
		}

		// Add emailing invoices to the exec_queue
        if (!empty($course_id)) {
            if (!$this->exec_queue->alreadyInQueue([
                "command"=>"php index.php daemon email_invoices $days $course_id $EMAIL_INVOICES $GENERATE_PDFS $EMAIL_PDFS_TO_COURSE"
            ])) {
                $this->exec_queue->insert([
                    "command" => "php index.php daemon email_invoices $days $course_id $EMAIL_INVOICES $GENERATE_PDFS $EMAIL_PDFS_TO_COURSE"
                ]);
            }
        }

		return;
	}

	function email_invoices($days, $course_id, $EMAIL_INVOICES = false, $GENERATE_PDFS = false, $EMAIL_PDFS_TO_COURSE = false)
	{
        $this->load->model('Invoice');
        $this->load->model("exec_queue");

		if ($EMAIL_INVOICES) {

			$this->session->unset_userdata('course_id');
			$this->benchmark->mark('email_invoice_start');
			// Retrieve invoices that need emailed
			$invoices_to_email = $this->Invoice->get_invoices_to_email(null, $course_id);
			// Loop through invoices and send emails
			$emails_sent = 0;
			$cron_output = '';

			if (count($invoices_to_email) > 0) {
				foreach ($invoices_to_email as $invoice_to_email) {
					$success = $this->Invoice->send_email($invoice_to_email['invoice_id']);
					$emails_sent++;
				}
			}

			$this->benchmark->mark('email_invoice_stop');
			$cron_output .= 'Invoices Emailed: ' .
				$emails_sent .
				' (' . $this->benchmark->elapsed_time('email_invoice_start', 'email_invoice_stop') . ' seconds)' .
				PHP_EOL;

			echo $cron_output;
		}

		// Add generate pdfs to the exec_queue
        if (!empty($course_id)) {
            if (!$this->exec_queue->alreadyInQueue([
                "command"=>"php index.php daemon queue_generate_pdfs $days $course_id $GENERATE_PDFS $EMAIL_PDFS_TO_COURSE"
            ])) {
                $this->exec_queue->insert([
                    "command" => "php index.php daemon queue_generate_pdfs $days $course_id $GENERATE_PDFS $EMAIL_PDFS_TO_COURSE"
                ]);
            }
        }

		return;
	}

	function queue_generate_pdfs($days, $course_id, $GENERATE_PDFS = false, $EMAIL_PDFS_TO_COURSE = false)
	{
        $this->load->model('Course');
        $this->load->model("exec_queue");

		if ($GENERATE_PDFS && !empty($course_id))
		{

			date_default_timezone_set('US/Mountain');
			$date = date('Y-m-d', strtotime("+$days days"));

			$last_emailed_date = $this->Course->get_last_emailed_date($course_id);
			if ($last_emailed_date >= $date) {
				return;
			}
			$this->Course->date_stamp_sent_invoices($course_id);
            if (!$this->exec_queue->alreadyInQueue([
                "command"=>"php index.php daemon generate_pdfs $days {$course_id} $EMAIL_PDFS_TO_COURSE"
            ])) {
                $this->exec_queue->insert([
                    "command" => "php index.php daemon generate_pdfs $days {$course_id} $EMAIL_PDFS_TO_COURSE"
                ]);
            }
		}
	}

	function generate_pdfs($days,$course_id, $EMAIL_PDFS_TO_COURSE = false)
	{
		$PDF_DOWNLOAD_URL		= 'http://mobile.foreupsoftware.com';
		$PDF_AWS_DOWNLOAD_URL	= 'https://s3-us-west-2.amazonaws.com/foreup.application.invoices';
		$INVOICE_ZIP_CC 		= array('jHopkins@foreup.com', 'matson@foreup.com');

		$date = date('Y-m-d', strtotime("+$days days"));
		$pdfGenerator = new fu\invoices\pdfGenerator();
		$pdfGenerator->setDate($date);
		$pdfGenerator->setCourseId($course_id);
		$pdfGenerator->setEmailPdfsToCourse($EMAIL_PDFS_TO_COURSE);
		$pdfGenerator->setInvoiceZipCc($INVOICE_ZIP_CC);
		$pdfGenerator->setPdfDownloadUrl($PDF_DOWNLOAD_URL);
		$pdfGenerator->setPdfAWSDownloadUrl($PDF_AWS_DOWNLOAD_URL);

		$pdfGenerator->generatePDFs();
	}

	function email_pdfs($days,$course_id)
	{
		$PDF_DOWNLOAD_URL		= 'http://mobile.foreupsoftware.com';
		$PDF_AWS_DOWNLOAD_URL	= 'https://s3-us-west-2.amazonaws.com/foreup.application.invoices';
		$INVOICE_ZIP_CC 		= array('jHopkins@foreup.com', 'matson@foreup.com');

		$date = date('Y-m-d', strtotime("+$days days"));
		$pdfGenerator = new fu\invoices\pdfGenerator();
		$pdfGenerator->setDate($date);
		$pdfGenerator->setCourseId($course_id);
		$pdfGenerator->setInvoiceZipCc($INVOICE_ZIP_CC);
		$pdfGenerator->setPdfDownloadUrl($PDF_DOWNLOAD_URL);
		$pdfGenerator->setPdfAWSDownloadUrl($PDF_AWS_DOWNLOAD_URL);
		$pdfGenerator->emailPdf();
	}

	function simulate_timeout() {
        sleep(30);
        echo json_encode(array('response'=>'succcessful delay'));
    }

	function send_auto_mailers($cron_key)
	{
		//echo 'getting inside auto_bill_credit_cards';
		if ($cron_key != $this->cron_key)
			return;
		else
		{
			set_time_limit(0);
			$this->load->model('Auto_mailer');
			$this->load->model('Recipient');
			$this->load->model('Auto_mailer_campaign');
			$this->load->model('Marketing_campaign');

			$auto_mailers = $this->Auto_mailer->get_all(10000, 0, true)->result_array();
			foreach ($auto_mailers as $auto_mailer)
			{
				$campaigns = $this->Auto_mailer_campaign->get_all($auto_mailer['auto_mailer_id'])->result_array();;
				$course  = $this->Course->get_info($auto_mailer['course_id']);
				foreach($campaigns as $campaign)
				{
					//print_r($campaign);
					$recipients = array();
					$date = date('Y-m-d 00:00:00', strtotime('-'.$campaign['days_from_trigger'].' days'));
					$rcpts = $this->Recipient->get_all_recipients($auto_mailer['auto_mailer_id'], $date);
					$index  = $campaign['type'] == 'email' ? 'email' : 'phone_number';
					//echo $this->db->last_query().'<br/><br/>';
					foreach ($rcpts->result_array() as $recipient)
					{
						//if(empty($recipient[$index]) || ($recipient["opt_out_{$index}"] == 'Y')) continue;
				        // making the email as a key will make sure that no email will be sent twice
				        $recipients[strtolower($recipient[$index])] = array(
				            "{$index}" => $recipient[$index],
				            'customer_id' => $recipient['person_id']
				        );
					}

					// get the template ready
					$contents = $this->get_contents($campaign['campaign_id']);
					if(!$contents)
						continue;
		        	//$this->Marketing_campaign->send_mails($campaign['campaign_id'], $recipients, $contents, $course);

					//echo '$auto_mailer '.$auto_mailer['name'].'<br/><br/>';
					//echo '$campaign_id '.$campaign['campaign_id'].'<br/>';
					//print_r($rcpts);
					//print_r($recipients);
					//print_r($contents);
					//echo '<br/><br/>';
					if ($contents != '')
					{
						if($campaign['type'] == 'email')
					    {
					      	$this->Marketing_campaign->send_mails($campaign['campaign_id'], $recipients, $contents, $course, $campaign['subject']);
						}
					    else
					    {
					        $this->Marketing_campaign->send_text($campaign['campaign_id'], $recipients, $contents);
					    }
					}
				}


			}

		}
	}

	private function get_contents($campaign_id)
	{
		$this->config->set_item('base_url', 'http://foreupsoftware.com');
		$info = $this->Marketing_campaign->get_info($campaign_id, true);
		if(isset($info->rendered_html) && $info->rendered_html != '') {
			return $info->rendered_html;
		}

		return false;
	}

	function send_tee_time_thank_yous($cron_key)
	{
		//echo 'getting inside auto_bill_credit_cards';
		if ($cron_key != $this->cron_key)
			return;
		else
		{
            if (function_exists('newrelic_custom_metric')) {
                newrelic_custom_metric('thank_you_cron', 1000);
            }

            // Returns list of teesheets with thank you email option checked (includes golf course data)
			date_default_timezone_set('America/Chicago');
			$tee_sheets = $this->Teesheet->get_thank_you_tee_sheets()->result_array();
			echo 'Tee Sheets';
			print_r($tee_sheets);
			//echo '<br/>'.$this->db->last_query().'<br/>';
			if (count($tee_sheets) > 0)
			{
				foreach ($tee_sheets as $tee_sheet)
				{
					//Mark tee sheet as run
					// I need to set everything with the tee sheet using the same timezone... keeps them all unified.
					// remember we are getting all the tee sheets at the same time, but each one needs to fetch tee times in their own timezone
					date_default_timezone_set('America/Chicago');
					$this->Teesheet->mark_next_thank_you_time($tee_sheet['teesheet_id']);

					//Set TimeZone
					date_default_timezone_set($tee_sheet['timezone']);

					//Get tee time individuals to thank
					//And mark individuals as emailed
					$course = $this->Course->get_info($tee_sheet['course_id']);
					$customers_to_thank = $this->Teetime->get_thank_you_list($tee_sheet['teesheet_id']);
					print_r($customers_to_thank);
					//echo $this->db->last_query();
					//print_r($customers_to_thank);
					//Email individuals
					//Save in communications
					$contents = $this->get_contents($tee_sheet['thank_you_campaign_id']);
					if ($contents)
					    $this->Marketing_campaign->send_mails($tee_sheet['thank_you_campaign_id'], $customers_to_thank, $contents, $course, 'Thanks for Golfing!');
				}
			}
			else
			{

					//Calculate time until next hour starts
					//Should run exactly on the hour
					$next_hour = strtotime(date('Y-m-d H:00:00', strtotime('+1 hour')));
					$now = time();
					$time_til_next_hour = $next_hour - $now;
					echo 'ttnh '.$time_til_next_hour;
					//sleep($time_til_next_hour + 120);
			}
		}
	}

	/* Scans database for changes, queues up those changes to be sent
	 * off to QuickBooks web connector for each course that has
	 * QuickBooks connected.
	 */
	function quickbooks_sync($cron_key)
	{
		if ($cron_key != $this->cron_key){
			return;

		}else{
			echo 'STARTING quickbooks_sync'.PHP_EOL;
			$now = time();
			$nine_o_clock/*PM*/ = strtotime(date('Y-m-d 21:00:00'));
			$three_o_clock/*AM*/ = strtotime(date('Y-m-d 04:00:00'));
			echo 'date '.date('Y-m-d H:i:s').' - ';
			echo 'now '.$now.' - ';
			echo '9pm '.$nine_o_clock.' - ';
			echo '3am '.$three_o_clock.' - '.PHP_EOL;
			if ($now > $nine_o_clock || $now < $three_o_clock)
			{
				echo 'Running script (within times)'.PHP_EOL;
				// Load all courses who have QuickBooks enabled
				$this->load->model('quickbooks');
				echo 'Getting users...'.PHP_EOL;
				$users = $this->quickbooks->get_users();
				echo 'Fetched users...'.PHP_EOL;

				// Loop through each course
				foreach($users as $user){
					$courseId = $user['qb_username'];
					if(empty($courseId)){
						continue;
					}

					// Make sure proper QuickBooks accounts are mapped
					if(!$this->quickbooks->check_account_map($courseId)){
						echo 'ERROR SYNCING COURSE '.$courseId.PHP_EOL.': Accounts are not mapped'.PHP_EOL;
						continue;
					}

					$this->quickbooks->init_account_map($courseId);
					$this->benchmark->mark('qb_sync_start');
					echo 'Queuing up course '.$courseId.PHP_EOL.'========================='.PHP_EOL;

					// Sync any new items
					$num_items = $this->quickbooks->queue_items($courseId, 50);
					echo 'Items: '.$num_items.PHP_EOL;

					$num_item_kits = $this->quickbooks->queue_item_kits($courseId, 50);
					echo 'Item Kits: '.$num_item_kits.PHP_EOL;

					$num_customers = $this->quickbooks->queue_customers($courseId, 50);
					echo 'Customers: '.$num_customers.PHP_EOL;

					$num_sales = $this->quickbooks->queue_sales($courseId, null, null, 25);
					echo 'Sales: '.$num_sales.PHP_EOL;

					$num_tips = $this->quickbooks->queue_tips($courseId, 25);
					echo 'Tips: '.$num_tips.PHP_EOL;

					$num_returns = $this->quickbooks->queue_returns($courseId, null, null, 25);
					echo 'Returns: '.$num_returns.PHP_EOL;
					$this->benchmark->mark('qb_sync_end');
					echo 'TOTAL TIME: '.$this->benchmark->elapsed_time('qb_sync_start','qb_sync_end').' seconds'.PHP_EOL.PHP_EOL;
				}
				//sleep(60 * 15); // 15 minutes
			}
			else
			{
				$seconds_til_nine = $nine_o_clock - $now;
				//sleep($seconds_til_nine); // Restart at 9pm
			}
		}
	}

	function clean_out_sessions($cron_key)
	{
		if ($cron_key != $this->cron_key){
			return;
		}else{
			$hour = date('H');
			if ($hour > 0 && $hour < 3)
			{
                if (function_exists('newrelic_custom_metric')) {
                    newrelic_custom_metric('sessions_cron', 1000);
                }

                $this->db->where("( last_activity < ".strtotime('-7 days')." OR user_data IS NULL )");
				$this->db->limit(2000);
				$this->db->delete('sessions');
				echo $this->db->last_query();
				//sleep(60*10);
			}
			else {
				//sleep(60*60);
			}
		}
	}

    // This erases phone numbers and resets emails to foreup@mailinator.com for all customers at demo courses except for anyone with an @foreup.com email
    function clean_out_demo_accounts($cron_key)
    {
        if ($cron_key != $this->cron_key){
            return;
        }else{
            $hour = date('H');
            if ($hour > 0 && $hour < 3)
            {
                if (function_exists('newrelic_custom_metric')) {
                    newrelic_custom_metric('demo_account_cron', 1000);
                }
                $this->db->query("UPDATE foreup_people AS p
                  LEFT JOIN foreup_customers AS cu ON p.person_id = cu.person_id
                  LEFT JOIN foreup_courses AS co ON co.course_id = cu.course_id
                  SET p.email = 'foreup@mailinator.com'
                  WHERE co.demo = 1
                    AND co.clean_out_nightly = 1 
                    AND cu.deleted = 0
                    AND p.email != ''
                    AND INSTR(p.email, 'foreup.com') = 0
                    AND p.email != 'foreup@mailinator.com'
                  LIMIT 5000");

                $this->db->query("UPDATE foreup_people AS p
                    LEFT JOIN foreup_customers AS cu ON p.person_id = cu.person_id
                    LEFT JOIN foreup_courses AS co ON co.course_id = cu.course_id
                  SET p.phone_number = '',
                    p.cell_phone_number = ''
                  WHERE co.demo = 1
                    AND co.clean_out_nightly = 1
                    AND cu.deleted = 0
                    AND INSTR(p.email, 'foreup.com') = 0
                  LIMIT 5000");

                echo $this->db->last_query();
                //sleep(60*10);
            }
            else {
               // sleep(60*60);
            }
        }
    }

    function check_db_connections($cron_key)
    {
        if ($cron_key != $this->cron_key) {
            return false;
        }

        if (function_exists('newrelic_custom_metric')) {
            newrelic_custom_metric('connections_cron', 1000);
        }

        $stati = $this->db->query("SHOW STATUS")->result_array();
        $results = array();
        $html = '<br/><br/>';
        foreach($stati as $status){
            $results[$status['Variable_name']] = $status['Value'];
            $html .= $status['Variable_name'].': '.$status['Value'].'<br/>';
        }
        $hour = date('H');
        $minute = date('i');
        $second = date('s');

        if ($hour == 1 && $minute < 1 && $second < 2) {
            $uptime = '';
            if (isset($results['Uptime'])) {
                $uptime = floor($results['Uptime'] / (24 * 60 * 60)) . ' days and ' . number_format($results['Uptime'] % (24 * 60 * 60) / (60 * 60).' hours', 2);
            }
            // Email a db connections running email
            send_sendgrid(
                'jhopkins@foreup.com',
                "DB Connections Running ".date("Y-m-d h:i:s"),
                'Current Uptime is: '.$uptime.$html,
                'no-reply@foreup.com',
                'ForeUP DB Connections'
            );

        }


        if (isset($results['Threads_connected'])) {
            if ($results['Threads_connected'] > 200) {
                //echo 'Threads connected (current connections): '.$results['Threads_connected'].'<br/>';
                send_sendgrid(
                    'jhopkins@foreup.com',
                    "Warning - DB Connections High " . date("Y-m-d h:i:s"),
                    'Connection count: ' . $results['Threads_connected'] . $html,
                    'no-reply@foreup.com',
                    'Warning - ForeUP DB'
                );
send_sendgrid(
                    'brendon@foreup.com',
                    "Warning - DB Connections High " . date("Y-m-d h:i:s"),
                    'Connection count: ' . $results['Threads_connected'] . $html,
                    'no-reply@foreup.com',
                    'Warning - ForeUP DB'
                );
send_sendgrid(
                    'jordan@foreup.com',
                    "Warning - DB Connections High " . date("Y-m-d h:i:s"),
                    'Connection count: ' . $results['Threads_connected'] . $html,
                    'no-reply@foreup.com',
                    'Warning - ForeUP DB'
                );

                //$this->load->library('sendhub');
                //$this->sendhub->send_message("Warning - DB Connections High" . date("Y-m-d h:i:s"). " Connection count: " . $results['Threads_connected'], array(1232367), false, 0);
            }

            if (function_exists('newrelic_custom_metric')) {
                echo 'adding to new relic Threads Connected';
                newrelic_custom_metric('ThreadsConnected', $results['Threads_connected'] * 1000);
                newrelic_custom_metric('ThreadsRunning', $results['Threads_running'] * 1000);
            }
        }
        if (isset($results['Max_used_connections'])) {


            if (function_exists('newrelic_custom_metric')) {
                echo 'adding to new relic Max Connections';
                newrelic_custom_metric('MaxUsedConnections', $results['Max_used_connections'] * 1000);
            }
        }
    }
    
    // Marks any cart in the database as incomplete if it has
    // payments and has not been active for 15 minutes
    function mark_incomplete_carts(){
    	
    	$_15_min_ago = gmdate('Y-m-d H:i:s', strtotime('-15 minutes'));

    	$cart_ids = $this->db->select('cart.cart_id, cart.course_id')
    		->from('pos_cart AS cart')
    		->join('pos_cart_payments AS payment', 'payment.cart_id = cart.cart_id', 'inner')
    		->where("last_ping != '0000-00-00:00:00:00' AND last_ping < '".$_15_min_ago."'")
			->where_in('status',['active','complete'])
    		->get()->result_array();

    	if(empty($cart_ids)){
    		echo "No incomplete carts found";
    		return false;
    	}

    	foreach($cart_ids as $row){
			$logger = new \fu\new_relic\insights();
			$event = new \fu\new_relic\event();
			$event->eventType = "mark_incomplete";
			$event->course_id = $row['course_id'];
			$event->cart_id = $row['cart_id'];
			$logger->send_event($event);
    		$this->db->update('pos_cart', array('status' => 'incomplete'), array('cart_id' => $row['cart_id']));
    	}

    	echo count($cart_ids)." carts marked 'incomplete'";
    }

    // Clear out abandoned carts
    function delete_abandoned_carts(){
    	
    	$_1_day_ago = gmdate('Y-m-d H:i:s', strtotime('-1 day'));
    	
    	// Determine which carts need deleted
    	$row = $this->db->select('COUNT(cart.cart_id) AS num_carts, GROUP_CONCAT(cart.cart_id) AS cart_ids', false)
    		->from('pos_cart AS cart')
    		->where("last_ping < '".$_1_day_ago."'")
			->where('status', 'active')
    		->get()->row_array();

    	if(empty($row['num_carts']) || $row['num_carts'] == 0){
    		echo "No abandoned carts found";
    		return false;
    	}

    	if(empty($row['cart_ids'])){
    		return false;
    	}

    	// Delete cart items, payments and customers
    	$this->db->query("DELETE item.*, customer.*, payment.*,
			modifier.*, side.*, side_modifier.*
			FROM foreup_pos_cart AS cart
			LEFT JOIN foreup_pos_cart_items AS item
				ON item.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_item_modifiers AS modifier
				ON modifier.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_item_sides AS side
				ON side.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_item_side_modifiers AS side_modifier
				ON side_modifier.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_customers AS customer
				ON customer.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_payments AS payment
				ON payment.cart_id = cart.cart_id
			WHERE cart.cart_id IN ({$row['cart_ids']})");

    	// Delete carts
		$this->db->query("DELETE FROM foreup_pos_cart 
			WHERE cart_id IN ({$row['cart_ids']})");
    	
    	echo $row['num_carts']." abandoned carts deleted";
    }

    /**
     * @param $campaign
     */
    private function sendOldCampaigns($campaign)
    {


        $recipients = $campaign->recipients;
        $recipients = unserialize($recipients);
        $mrl = $this->marketing_recipients_lib;
        if ($recipients['groups']) {
            $mrl->set_groups($recipients['groups']);
        }
        if ($recipients['individuals']) {
            $mrl->set_individuals($recipients['individuals']);
        }
        $recipients = $mrl->get_recipients($campaign->type, $campaign->course_id);

        if ($campaign->type == 'email') {
            $contents = $this->get_contents($campaign->campaign_id);
            if ($contents != '') {
                $this->Marketing_campaign->send_mails(
                    $campaign->campaign_id,
                    $recipients,
                    $contents,
                    $this->Course->get_info($campaign->course_id),
                    $campaign->subject
                );
            }
        } else {
//            $this->Sendhub_account->send_group_text(
//                $campaign->campaign_id,
//                $recipients,
//                $campaign->content,
//                $campaign->course_id
//            );
        }
    }

    // Check Credit Card Payments Table
    function check_cc_payments($cron_key, $date_time = false)
    {
        if ($cron_key != $this->cron_key) {
            return false;
        }

        $this->load->model('Credit_card_payments');
        $this->load->model('sale');

        // Cancel any pending mercury payment cancellations
        $cancel_payments = $this->Credit_card_payments->get_cancelled_mercury_payments();
        echo 'fetching cancel button payments<br/>';
        echo $this->db->last_query().'<br/>';
        $report_data = '';
        var_dump($cancel_payments);
        $cancel_payment_data = '';
        $this->load->library('Hosted_checkout_2');

        echo 'looping through cancel button payments<br/>';
        foreach ($cancel_payments as $payment) {
            // Verify credit card transaction...
            $cc_invoice = false;
            $cc_amount = 0;
            $cancelled_data = array();
            if (!empty($payment['payment_id'])) {

                $HC = new Hosted_checkout_2();
                echo "setting mercury credentials un: {$payment['mercury_id']}, pw: {$payment['mercury_password']}<br/>";
                $HC->set_merchant_credentials($payment['mercury_id'], $payment['mercury_password']);

                $HC->set_payment_id($payment['payment_id']);
                $verify_results = $HC->verify_payment();
                $HC->complete_payment();

                if ((string)$verify_results->Status == 'Approved') {
                    $invoice = (string)$verify_results->Invoice;

                    $payment_data = array(
                        'mercury_id' => $payment['mercury_id'],
                        'tran_type' => (string)$verify_results->TranType,
                        'amount' => (string)$verify_results->Amount,
                        'auth_amount' => (string)$verify_results->AuthAmount,
                        'card_type' => (string)$verify_results->CardType,
                        'frequency' => 'OneTime',
                        'masked_account' => (string)$verify_results->MaskedAccount,
                        'cardholder_name' => (string)$verify_results->CardholderName,
                        'ref_no' => (string)$verify_results->RefNo,
                        'operator_id' => (string)$verify_results->OperatorID,
                        'trans_post_time' => (string)$verify_results->TransPostTime,
                        'auth_code' => (string)$verify_results->AuthCode,
                        'voice_auth_code' => (string)$verify_results->VoiceAuthCode,
                        'payment_id' => $payment['payment_id'],
                        'acq_ref_data' => (string)$verify_results->AcqRefData,
                        'process_data' => (string)$verify_results->ProcessData,
                        'token' => (string)$verify_results->Token,
                        'response_code' => (int)$verify_results->ResponseCode,
                        'status' => (string)$verify_results->Status,
                        'status_message' => (string)$verify_results->StatusMessage,
                        'display_message' => (string)$verify_results->DisplayMessage,
                        'avs_result' => (string)$verify_results->AvsResult,
                        'cvv_result' => (string)$verify_results->CvvResult,
                        'tax_amount' => (string)$verify_results->TaxAmount,
                        'avs_address' => (string)$verify_results->AVSAddress,
                        'avs_zip' => (string)$verify_results->AVSZip,
                        'payment_id_expired' => (string)$verify_results->PaymendIDExpired,
                        'customer_code' => (string)$verify_results->CustomerCode,
                        'memo' => (string)$verify_results->Memo,
                        'cancel_time' => gmdate('Y-m-d H:i:s')
                    );

                    $this->sale->update_credit_card_payment($invoice, $payment_data);

                    $cc_invoice = $invoice;
                    $cc_amount = (string)$verify_results->AuthAmount;
                }

                if ($cc_invoice) {
                    // Cancel credit card transaction...
                    $HC = new Hosted_checkout_2();
                    $return_data = $payment_data;
                    $return_data['mercury_password'] = $payment['mercury_password'];
                    $return_data['payment_id'] = $payment['payment_id'];
                    $return_data['invoice'] = $cc_invoice;

					if ($payment_data['tran_type'] == 'Return') {
					 	if (!$HC->cancel_return($return_data)) {
							$cancelled_data['cancel_failed'] = 1;
							//$cancel_payment_data .= "<tr><td>{$payment['id']}</td><td>{$payment['payment_id']}</td><td>Yes</td></tr>";
							$this->db->insert('payment_irregularities', array(
								'course_id' => '',
								'type' => 'Return Cancel Button',
								'invoice_id' => NULL,
								'payment_id' => $payment['payment_id'],
								'successful' => 0,
								'time' => NULL,
								'cancel_time' => gmdate('Y-m-d H:i:s')
							));
						}
						 else {
							$returned_info = array(
								'token_used' => 1,
								'amount_refunded' => $payment_data['auth_amount']
							);
							$this->db->where('invoice', $cc_invoice);
							$this->db->update('sales_payments_credit_cards', $returned_info);
							$this->db->insert('payment_irregularities', array(
								'course_id' => '',
								'type' => 'Return Cancel Button',
								'invoice_id' => NULL,
								'payment_id' => $payment['payment_id'],
								'successful' => 1,
								'time' => NULL,
								'cancel_time' => gmdate('Y-m-d H:i:s')
							));
							//$cancel_payment_data .= "<tr><td>{$payment['id']}</td><td>{$payment['payment_id']}</td><td>Successfully returned</td></tr>";
						}
					}
					else {
						if (!$HC->cancel_payment($return_data)) {
							$cancelled_data['cancel_failed'] = 1;
							//$cancel_payment_data .= "<tr><td>{$payment['id']}</td><td>{$payment['payment_id']}</td><td>Yes</td></tr>";
							$this->db->insert('payment_irregularities', array(
								'course_id' => '',
								'type' => 'Cancel Button',
								'invoice_id' => NULL,
								'payment_id' => $payment['payment_id'],
								'successful' => 0,
								'time' => NULL,
								'cancel_time' => gmdate('Y-m-d H:i:s')
							));
						} else {
							$returned_info = array(
								'token_used' => 1,
								'amount_refunded' => $payment_data['auth_amount']
							);
							$this->db->where('invoice', $cc_invoice);
							$this->db->update('sales_payments_credit_cards', $returned_info);
							$this->db->insert('payment_irregularities', array(
								'course_id' => '',
								'type' => 'Cancel Button',
								'invoice_id' => NULL,
								'payment_id' => $payment['payment_id'],
								'successful' => 1,
								'time' => NULL,
								'cancel_time' => gmdate('Y-m-d H:i:s')
							));
							//$cancel_payment_data .= "<tr><td>{$payment['id']}</td><td>{$payment['payment_id']}</td><td>Successfully returned</td></tr>";
						}
					}

//                    $this->load->model('Payment_return');
//                    $return_log_data = array(
//                        'note' => 'daemon->check_cc_payments ',
//                        'invoice_id' => $cc_invoice,
//                        'employee_id' => $this->session->userdata('person_id')
//                    );
//                    $this->Payment_return->save($return_log_data);

                }
            }

            $cancelled_data['cancel_time'] = gmdate('Y-m-d H:i:s');
            $this->db->where('id', $payment['id']);
            $this->db->update('mercury_cancelled_payments', $cancelled_data);
        }

        if ($cancel_payment_data != '') {
            $report_data .= "<tr><td colspan='3'>Canceled Payments</td></tr>";
            $report_data .= "<tr><td>ID</td><td>Payment ID</td><td>Failed</td></tr>";
            $report_data .= $cancel_payment_data;
        }

        // Check unfinished mercury charges
        $payments = $this->Credit_card_payments->get_incomplete_payments('mercury', 3, $date_time);
echo 'fetching incomplete payments<br/>';
        echo $this->db->last_query().'<br/>';
        var_dump($payments);
        $incomplete_payments_data = '';
echo 'looping through incomplete payments<br/>';
        foreach ($payments as $payment) {
            // Check payment
            $updated = $this->Credit_card_payments->check($payment) ? 'Yes' : 'No';
            if ($updated == 'Yes') {
                //$incomplete_payments_data .= "<tr><td>{$payment['course_id']}</td><td>{$payment['invoice']}</td><td>$updated</td></tr>";
                $this->db->insert('payment_irregularities', array(
                    'course_id' => $payment['course_id'],
                    'type' => 'Unrecorded Payments',
                    'invoice_id' => $payment['invoice'],
                    'payment_id' => NULL,
                    'successful' => 1,
                    'time' => NULL,
                    'cancel_time' => NULL
                ));

            }
        }

        if ($incomplete_payments_data != '') {
            $report_data .= "<tr><td colspan='3'>Unrecorded Payments</td></tr>";
            $report_data .= "<tr><td>Payment Course</td><td>Invoice ID</td><td>Successful</td></tr>";
            $report_data .= $incomplete_payments_data;
        }


        $cancelled_payments = $this->Credit_card_payments->get_early_cancelled_payments('mercury', $date_time);
        echo 'fetching cancelled payments<br/>';
        echo $this->db->last_query().'<br/>';
        var_dump($cancelled_payments);
        $canceled_header_data = "<tr><td colspan='3'>Cancelled Payments</td></tr>";
        $canceled_header_data .= "<tr><td>Payment Course</td><td>Invoice ID</td><td>Problem</td></tr>";
        $canceled_data = '';

        echo 'looping through cancelled payments<br/>';
        foreach ($cancelled_payments as $cp) {
            $adjusted_cancel_time = date("Y-m-d H:i:s", strtotime("{$cp['cancel_time']} - 4 hours"));

            if ($cp['trans_post_time'] > $adjusted_cancel_time || // Cancelled before it processed
                $adjusted_cancel_time > date('Y-m-d H:i:s', strtotime("{$cp['trans_post_time']} + 5 minutes"))) // Cancelled more than 5 minutes after the transaction
            {
                //$canceled_data .= "<tr><td>{$cp['course_id']}</td><td>{$cp['invoice']}</td><td>Processed at {$cp['trans_post_time']} canceled at $adjusted_cancel_time</td></tr>";
                $this->db->insert('payment_irregularities', array(
                    'course_id' => $cp['course_id'],
                    'type' => 'Cancelled Early',
                    'invoice_id' => $cp['invoice'],
                    'payment_id' => NULL,
                    'successful' => NULL,
                    'time' => $cp['trans_post_time'],
                    'cancel_time' => $adjusted_cancel_time
                ));

            }
        }

        if ($canceled_data != '') {
            $report_data .= $canceled_header_data;
            $report_data .= $canceled_data;
        }

        // Check for completed payments that aren't linked to any sale or cart payment
        $no_sale_payments = $this->Credit_card_payments->get_payments_without_sale();
        echo 'fetching no sale payments<br/>';
        echo $this->db->last_query().'<br/>';
        $ns_data = '';
        echo 'looping through no sale payments<br/>';
        foreach ($no_sale_payments as $ns_payment) {
            //$ns_data .= "<tr><td>{$ns_payment['course_id']}</td><td>{$ns_payment['invoice']}</td><td>Processed at {$ns_payment['trans_post_time']}</td></tr>";
            $this->db->insert('payment_irregularities', array(
                'course_id' => $ns_payment['course_id'],
                'type' => 'Floating Payment',
                'invoice_id' => $ns_payment['invoice'],
                'payment_id' => NULL,
                'successful' => NULL,
                'time' => $ns_payment['trans_post_time'],
                'cancel_time' => NULL
            ));

        }
        if ($ns_data != '') {
            $report_data .= "<tr><td colspan='3'>No Sale Payments</td></tr>";
            $report_data .= "<tr><td>Payment Course</td><td>Invoice ID</td><td>Time</td></tr>";
            $report_data .= $ns_data;
        }

        if ($report_data != '') {
            echo 'saving report<br/>';

            $report_data = "<table><tbody>".$report_data."</tbody></table>";
            // Send report
            send_sendgrid(
                'jhopkins@foreup.com',
                "Payment Irregularity Report ".date("Y-m-d h:i:s"),
                $report_data,
                'no-reply@foreup.com',
                'ForeUP Payment Irregularity Report'
            );
        }
echo 'done<br/>';
    }


	function collect_historical_weather($cron_key) {

		if ($cron_key != $this->cron_key) {
			return false;
		}

		$this->load->model('Historical_weather');
		$this->load->model('Course');
		// Validate call

		// Fetch courses to collect for
		$courses = $this->Course->get_weather_update_list()->result_array();
		
		foreach ($courses as $course) {
			$this->Course->mark_weather_collected($course['course_id']);

			$start_date = date('Y-m-d 00:00:00', strtotime($this->Historical_weather->get_last_save_date($course['course_id']).' +1 day'));

			if ($start_date >= date('Y-m-d 00:00:00')) {
				// Only want to collect past data
				echo 'collect historical weather... continuing. Skipping course_id: '.$course['course_id'].'<br/>';
				continue;
			}

			$start_date = str_replace(' ', 'T', $start_date);
			$url = $this->config->item('forecastio_api_url').'/'.$this->config->item('forecastio_api_key').'/'.$course['latitude_centroid'].','.$course['longitude_centroid'].','.$start_date;
			// Fetch Forecast.io Information
			$historical_weather = json_decode(file_get_contents($url));

			if (empty($historical_weather->hourly)) {
				// If no hourly data is returned, then move on
				// Save in database
				$data = array(
					'course_id' => $course['course_id'],
					'date_collected' => gmdate('Y-m-d H:i:s'),
					'day' => $start_date,
					'date' => gmdate('Y-m-d H:i:s', $start_date),
					'summary' => '',
					'icon' => '',
					'precipIntensity' => 0,
					'precipProbability' => 0,
					'temperature' => 0,
					'apparentTemperature' => 0,
					'dewPoint' => 0,
					'humidity' => 0,
					'windSpeed' => 0,
					'windBearing' => 0,
					'visibility' => 0,
					'cloudCover' => 0,
					'pressure' => 0
				);

				echo 'Historical Weather response<br/>';
				var_dump($data);

				$this->Historical_weather->save($data);
			}
			else {
				foreach ($historical_weather->hourly->data as $weather) {
					// Save in database
					$data = array(
						'course_id' => $course['course_id'],
						'date_collected' => gmdate('Y-m-d H:i:s'),
						'day' => $start_date,
						'date' => gmdate('Y-m-d H:i:s', $weather->time),
						'summary' => !empty($weather->summary) ? $weather->summary : '',
						'icon' => !empty($weather->icon) ? $weather->icon : '',
						'precipIntensity' => !empty($weather->precipIntensity) ? $weather->precipIntensity : 0,
						'precipProbability' => !empty($weather->precipProbability) ? $weather->precipProbability : 0,
						'temperature' => !empty($weather->temperature) ? $weather->temperature : 0,
						'apparentTemperature' => !empty($weather->apparentTemperature) ? $weather->apparentTemperature : 0,
						'dewPoint' => !empty($weather->dewPoint) ? $weather->dewPoint : 0,
						'humidity' => !empty($weather->humidity) ? $weather->humidity : 0,
						'windSpeed' => !empty($weather->windSpeed) ? $weather->windSpeed : 0,
						'windBearing' => !empty($weather->windBearing) ? $weather->windBearing : 0,
						'visibility' => !empty($weather->visibility) ? $weather->visibility : 0,
						'cloudCover' => !empty($weather->cloudCover) ? $weather->cloudCover : 0,
						'pressure' => !empty($weather->pressure) ? $weather->pressure : 0
					);

					echo 'Historical Weather response<br/>';
					var_dump($data);

					$this->Historical_weather->save($data);
				}
			}
		}
	}



	function plist2json($child) {
		return;
		switch($child->nodeName) {
			case 'dict':
				$d = new StdClass();
				$nodes = $child->childNodes;
				for($i = 0; $i < $nodes->length; $i++){
					if ($nodes->item($i)->nodeName == 'key'){
						$key = $nodes->item($i)->textContent;
						$i++;
						while ($nodes->item($i)->nodeName == "#text") {
							$i++;
						}
						$d->$key = $this->plist2json($nodes->item($i));
					}
				}
				return $d;
				break;
			case 'array':
				$a = array();
				$nodes = $child->childNodes;
				for($i = 0; $i < $nodes->length; $i++){
					if ($nodes->item($i)->nodeName != "#text")
						$a[] = $this->plist2json($nodes->item($i));
				}
				return $a;
				break;
			case 'string':
				return $child->textContent;
				break;
			case 'data':
				return $child->textContent;
				break;
			case 'real':
				return $child->textContent;
				break;
			case 'integer':
				return $child->textContent;
				break;
			case 'true':
				return true;
				break;
			case 'false':
				return false;
				break;
		}
	}

	function floating_payments_support()
	{

		$timestamp = \Carbon\Carbon::now()->tz('UTC')->subMinutes(5);
		echo $timestamp->toDateTimeString();
		//Run every minute and create a support ticket for every floating payment
		$results = $this->db->query("
		SELECT
			foreup_courses.name,
			emv_payment,
			foreup_pos_cart.status as \"Cart Status\",
			foreup_sales_payments_credit_cards.cart_id,
			foreup_sales_payments_credit_cards.timestamp
			FROM foreup_sales_payments_credit_cards
			
			LEFT JOIN foreup_sales_payments ON `foreup_sales_payments_credit_cards`.invoice = foreup_sales_payments.invoice_id
			LEFT JOIN foreup_table_payments ON foreup_table_payments.credit_card_invoice_id = foreup_sales_payments_credit_cards.invoice
			LEFT JOIN foreup_courses ON foreup_sales_payments_credit_cards.course_id = foreup_courses.course_id
			LEFT JOIN foreup_pos_cart ON foreup_sales_payments_credit_cards.cart_id = foreup_pos_cart.cart_id
			WHERE
			(foreup_sales_payments_credit_cards.`status` = \"Approved\" OR foreup_sales_payments_credit_cards.`status` =\"success\") AND
			`amount_refunded` = 0 AND
			foreup_sales_payments.sale_id IS NULL AND
			auth_amount > 0 AND
			tran_type = \"Sale\" AND
			(foreup_sales_payments_credit_cards.status = \"Approved\" OR foreup_sales_payments_credit_cards.status = \"success\" ) AND
			cancel_time = \"0000-00-00 00:00:00\" AND
			token_used = 0 AND
			foreup_sales_payments_credit_cards.mercury_id != 88430119384 AND
			((foreup_sales_payments_credit_cards.mercury_id != \"\" AND  foreup_sales_payments_credit_cards.mercury_password != \"\")
			
			OR foreup_sales_payments_credit_cards.element_account_id IS NOT NULL OR foreup_sales_payments_credit_cards.ets_id != \"\") AND
			foreup_table_payments.sale_id IS NULL 
			AND (foreup_pos_cart.status != \"active\" OR foreup_pos_cart.status IS NULL)
			AND foreup_sales_payments_credit_cards.timestamp >= \"{$timestamp->second(0)->toDateTimeString()}\" AND foreup_sales_payments_credit_cards.timestamp < \"{$timestamp->addMinute()->second(0)->toDateTimeString()}\"
			ORDER BY invoice DESC LIMIT 5
		")->result_array();


		$sendgridConfig = $this->config->item('sendgrid');
		$sendgrid = new \SendGrid($sendgridConfig['username'], $sendgridConfig['password']);
		foreach($results as $row){
			//Course just had a floating payment and needs help asap.  []. [There payment is currently in a [status] cart].  The payment was ran at [date created].
			//  It looks like they [swiped normally, were using EMV].  Can you call them real quick and see if you can help them and gather the standard floating payment information for dev.

			$content = "{$row['name']} just had a floating payment and needs help asap. ";


			if(!isset($row['cart_id']) || $row['cart_id'] == ""){
				$content .= "I believe they were running a payment in f&b. Ask them if they had a split item in a reciept. ";
			}
			if(isset($row['cart_id']) && $row['cart_id'] != ""){
				$content .= "Their payment is currently in cart (#{$row['cart_id']}) with a status of {$row['status']}. ";
			}

			$date = \Carbon\Carbon::parse($row['timestamp']);
			$content .= "The payment was ran at {$date->setTimezone("America/Denver")->toDateTimeString()} MST. ";

			if(isset($row['emv_payment']) && $row['emv_payment'] == 1){
				$content .= "It looks like they were using EMV. ";
			} else {
				$content .= "It looks like they swiped normally. ";
			}
			$content .= "They need a call right away and please collect the standard floating payment information as well as any other details that may help replicate the problem. ";

			$email = new \SendGrid\Email();
			$email->setSubject("Help with floating payment.")
				->setFrom("no-reply@foreupsoftware.com")
				->setFromName($row['name'])
				->setReplyTo("brendon@foreup.com")
				->setHtml($content)
				->setSmtpapiTos(["brendon@foreup.com","support@foreup.com"]);


			$sendgrid->send($email);
		}
	}

	function parsegps()
	{
		return;
		$it = new RecursiveDirectoryIterator("/var/www/development.foreupsoftware.com/fu/plists/Journey's End");
		foreach(new RecursiveIteratorIterator($it) as $file)
		{
			if ($file->getFilename() == "CourseGPSData.plist")
			{
				$fileObject = $file->openFile('r');
				$contents = $fileObject->fread($fileObject->getSize());


				$doc = new DOMDocument();
				$doc->loadXML($contents);

				foreach($doc->documentElement->childNodes as $child) {
					if ($child->nodeName != "#text") {
						$output = $this->plist2json($child);
						break;
					}
				}

				//Line by line
				//
				$gps = json_decode(json_encode($output),true);
				foreach($gps['Courses'] as $teesheet){
					foreach($gps[$teesheet['name']] as $key=>$hole){
						$this->db->insert("teesheet_holes",[
							"tee_sheet_id"=>$teesheet['scheduleId'],
							"hole_number"=>$key+1,
							"par"=>$hole['par'],
							"handicap"=>"",
							"pro_tip"=>"",
							"tee_box_long"=>$hole['tee'][0]['longitude'],
							"tee_box_lat"=>$hole['tee'][0]['latitude'],
							"mid_point_long"=>$hole['middle']['longitude'],
							"mid_point_lat"=>$hole['middle']['latitude'],
							"hole_long"=>$hole['green']['center']['longitude'],
							"hole_lat"=>$hole['green']['center']['latitude'],
							"green_front_lat"=>$hole['green']['front']['latitude'],
							"green_front_long"=>$hole['green']['front']['longitude'],
							"green_back_long"=>$hole['green']['back']['longitude'],
							"green_back_lat"=>$hole['green']['back']['latitude']
						]);
					}

				}




			}
		}



	}

    function course_summary($key = '', $course_id = '', $search = 0, $case_number = '') {
        if ($key != 'a7b2ee28-ad8e-49c4-927a-fd15d7c7a83d') {
            return;
        }
        $this->load->model('Course');
        $this->load->model('Course_information_website');
        $this->load->model('Course_information');
        if ($course_id != '') {
            $course_info = $this->Course->get_info($course_id);
            $data = array('course_info' => $course_info, 'search' => $search);
            $data['course_information'] = $this->Course_information->get($course_id);
            $data['website_info'] = $this->Course_information_website->get($course_id);
            $data['billing_info'] = $this->Billing->get_summary_by_course_id($course_id);
        }
        $data['case_number'] = $case_number;
        $this->load->view('courses/summary', $data);
    }
    function course_search() {
        ini_set('memory_limit', '500M');
        $this->load->model('course');
        $term = $this->input->get('term');
        $search_suggestions = $this->course->get_course_search_suggestions($term, 10);
        echo json_encode($search_suggestions);
    }

    public function set_default_permissions()
    {
	    $personId = 0;
	    $roleId = 2;
	    $courseId = 0;
	    $acl = new \fu\acl\Acl($personId,$courseId);

	    $permission = new fu\acl\Permissions\Teetimes\Checkedin(["delete"]);
	    $acl->roleAllow($roleId,$permission);


		$permissions = [];
	    $permissions[] = new fu\acl\Permissions\Customer\AccountBalance(["update"]);
	    $permissions[] = new fu\acl\Permissions\Customer\OnlineCredentials(["update"]);
	    $permissions[] = new fu\acl\Permissions\Customer\PriceClass(["update"]);
	    $permissions[] = new fu\acl\Permissions\Inventory\Cost(["update"]);
	    $permissions[] = new fu\acl\Permissions\Inventory\Quantity(["update"]);
	    $permissions[] = new fu\acl\Permissions\Inventory\MaxDiscount(["update"]);
	    $permissions[] = new fu\acl\Permissions\Inventory(["delete"]);
	    $permissions[] = new fu\acl\Permissions\Pos\FeeDropdown(["update"]);
	    $permissions[] = new fu\acl\Permissions\Pos\Taxable(["update"]);
	    $permissions[] = new fu\acl\Permissions\Teetimes\Checkedin(["delete"]);
	    $permissions[] = new fu\acl\Permissions\Teetimes\Anonymous(["create"]);
	    foreach($permissions as $permission){
		    $acl->roleAllow($roleId,$permission);
	    }

	    $acl->rowAllow(3,new fu\acl\Permissions\Sale(["delete_wo_reason"]));

    }

    public function clean_out_pending_reservations() {
        $this->load->model('teetime');
        $this->teetime->remove_old_pending_reservations();
    }

	function process_queue($loops = 0) {
		$s3Config = $this->config->item('s3');
		$sqsConfig = $this->config->item('sqs');

		$client = new \Aws\Sqs\SqsClient(array(
			'region'  => 'us-west-2',
			'credentials' => [
				'key'    => $s3Config["access_key"],
				'secret' => $s3Config["secret"],
			],
			'version' => '2012-11-05'
		));

		for($i=0;$i<=20;$i++){

			$this->db->close();
			try {
				$result = $client->receiveMessage(array(
					'QueueUrl'    => $sqsConfig['course_events_url'],
					'WaitTimeSeconds' => 5,
					'MaxNumberOfMessages' => 5
				));
			} catch (Exception $e)
			{
				//Do Nothing
				sleep(5);
			}


			foreach ($result->getPath('Messages') as $message) {
					$eventBody = json_decode($message['Body']);
					if(!isset($eventBody->course_id)){
						continue;
					}
					$hooks = $this->db->from("api_user_courses")
						->join("api_hooks","api_user_courses.user_id = api_hooks.user_id","LEFT")
						->where("course_id",$eventBody->course_id)
						->where("event",$eventBody->event_name)
						->get()->result_array();

					foreach($hooks as $hook){
						$guzzleClient = new GuzzleHttp\Client();

						$request = $guzzleClient->createRequest('POST',$hook['hook_url']);
						$request->setHeaders(
							[]
						);
						if(isset($hook['header_key'])){
							$request->setHeader($hook['header_key'],$hook['header_value']);
						}
						$request->setHeader("Content-type","application/json");
						$bodyContent =[
							'event'=>$eventBody->event_name,
							'course_id'=>$eventBody->course_id,
							'id'=>$eventBody->id
						];
						if(!empty($eventBody->teesheet_id)){
							$bodyContent['teesheet_id'] = $eventBody->teesheet_id;
						}
						$request->setBody(\GuzzleHttp\Stream\Stream::factory(json_encode($bodyContent)));
						try {
							$guzzleClient->send($request);
						} catch (Exception $e)
						{
							error_log("Problem sending events to url: ".$hook['hook_url']);
						}
					}

					$client->deleteMessage(array(
						'QueueUrl' => $sqsConfig['course_events_url'],
						'ReceiptHandle' => $message['ReceiptHandle'],
					));

				}

		}
    }

    public function run_data_import_jobs(){

    	$this->load->model('v2/Import_job_model');

		$jobs = $this->Import_job_model->get(['status' => 'ready', 'deleted_at' => null], $all_courses = true);
		if(empty($jobs)){
			return false;
		}

		foreach($jobs as $job){
			$command = 'php index.php daemon import_data '.$job['id'];
			if(!$this->exec_queue->alreadyInQueue(["command" => $command])){
				$this->exec_queue->insert(["command" => $command]);
			}
		}
	}

	public function import_data($job_id){

		$this->load->model('v2/Import_job_model');
		$rows = $this->Import_job_model->get(['id' => $job_id, 'status' => 'ready'], $all_courses = true);

		if(empty($rows[0])){
			return false;
		}
		$row = $rows[0];
        $import_job = new \fu\data_import\DataImportJob($row);
		$import_job_model =& $this->Import_job_model;

		$import_job->set_progress_callback(function($data, $import_job) use ($job_id, $import_job_model){
			static $record_number = 0;

            // Check if the job has been cancelled
            $status = $import_job_model->get_status($job_id);
            if($status == 'cancelled'){
                $import_job->update_status('cancelled');
            }

            // Update status every 100 records processed
			if($record_number % 100 == 0 || $import_job->is_done()){
				$data['last_progress_update'] = date('Y-m-d H:i:s');
				$import_job_model->save($job_id, $data);
			}
			$record_number++;
		});

		// Start importing records
		$import_job->import();
        $import_job->save_failed_csv();

		// Get finished response data
		$response = $import_job->to_array();
		$import_job_model->save($job_id, $response);

        // Notify user by email that the import is complete
        if(!empty($row['settings']['notify_email'])){

            $subject = 'ForeUp '.ucfirst($row['type']).' import #'.$row['id'].' complete.';
            $body = $this->load->view("email_templates/data_import_complete", $response, true);

            send_sendgrid(
                $row['settings']['notify_email'],
                $subject,
                $body,
                'info@foreup.com',
                'ForeUP'
            );
        }
    }

    public function bulk_edit($jobId){

        $row = $this->db->select('*')
            ->from('foreup_bulk_edit_jobs')
            ->where('id', $jobId)
            ->where('status', 'ready')
            ->where('deleted_at', null)
            ->get()
            ->row_array();

        if(empty($row)){
            return false;
        }
        $row['record_ids'] = explode(',', $row['record_ids']);
        $row['settings'] = json_decode($row['settings'], true);
        $row['response'] = json_decode($row['response'], true);

        $bulkEditJob = new \fu\bulk_edit\BulkEditJob($row);
        $db =& $this->db;

        $bulkEditJob->set_progress_callback(function($data, $bulkEditJob) use ($jobId, $db){

            static $record_number = 0;

            // Check if the job has been cancelled
            $row = $db->select('status')->from('foreup_bulk_edit_jobs')->where('id', $jobId)->get()->row_array();
            $status = $row['status'];
            if($status == 'cancelled'){
                $bulkEditJob->update_status('cancelled');
            }

            // Update status every 100 records processed
            if($record_number % 100 == 0 || $bulkEditJob->is_done()){
                $data['last_progress_update'] = date('Y-m-d H:i:s');
                $db->where('id', $jobId)->update('foreup_bulk_edit_jobs', $data);
            }
            $record_number++;
        });

        // Start editing records
        $bulkEditJob->transform();

        // Get finished response data
        $jobData = $bulkEditJob->to_array();

        $db->where('id', $jobId)
            ->update('foreup_bulk_edit_jobs', [
                'response' => json_encode($jobData['response'])
            ]);

        // Notify user by email that the import is complete
        /*
        if(!empty($row['settings']['notify_email'])){

            $subject = 'ForeUp '.ucfirst($row['type']).' bulk edit #'.$row['id'].' complete.';
            $body = $this->load->view("email_templates/data_import_complete", $response, true);

            send_sendgrid(
                $row['settings']['notify_email'],
                $subject,
                $body,
                'info@foreup.com',
                'foreUP'
            );
        } */
    }

    public function loyalty_expiration()
    {
	    //Get all passes that expired yesterday
	    $query = $this->db->from("passes p")
		    ->select("p.customer_id")
		    ->join("pass_definitions pd","p.pass_item_id = pd.item_id")
		    ->join("customers c","p.customer_id = c.person_id")
		    ->where("DATE(p.end_date)",\Carbon\Carbon::now()->subDay()->toDateString())
		    ->where("pd.enroll_loyalty",1)
	        ->get();
	    $this->load->model("customer");
	    foreach($query->result_array() as $expired)
	    {
			$this->customer->disable_loyalty($expired['customer_id']);
	    }
	    //Update customer record to not be in loyalty
    }

    public function teesnapcheck(){
		$i = 99;

	    $courseStats = [
		    "course_not_found"=>0,
		    "course_not_of_property"=>0,
		    "other"=>0,
	    ];
		while($i < 200){

			$i++;
			$guzzleClient = new GuzzleHttp\Client();
			$request = $guzzleClient->createRequest('GET',"https://dragonfly.teesnap.net/customer-api/teetimes-day?course=$i&date=2017-01-05&players=4&holes=18&addons=off");
			try {
				$response = $guzzleClient->send($request);
			} catch(Exception $e) {
				$body = $e->getResponse()->getBody()->getContents();
				if(preg_match("/course_not_found/",$body)){
					print $i . "course_not_found<br/>";
					$courseStats['course_not_found']++;
				} else if (preg_match("/course_not_of_property/",$body)) {
					print $i . "course_not_of_property<br/>";
					$courseStats['course_not_of_property']++;
				} else {
					print $i . "other<br/>";
					$courseStats['other']++;
				}
			}

		}
		var_dump($courseStats);
    }
}
?>