<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");

class Events extends Secure_area implements iData_controller
{
  function __construct()
  {    
    parent::__construct('events');

    $this->load->Model('Event');
  }

  public function index()
  {
    $data['controller_name'] = __CLASS__;
    $this->load->view('events/manage', $data);
  }

  public function search(){}
	public function suggest(){}
	public function get_row(){}
	public function view($data_item_id=-1){}
  
	public function save($event_id=-1){

    $start = $this->input->post('event_start');
    $end = $this->input->post('event_end');
    $details = $this->input->post('event_details');

    $end = $end == 'end' ? $start : $end;

    $data = array(
        'title' => $this->input->post('event_title'),
        'start_date' => $start,
        'end_date' => $end,
        'details' => $details
    );

    $success = $this->Event->save($data, $event_id);
    
    $response = array(
        'success' => $success,
        'cmd' => ($event_id==-1) ? 'create' : 'update',
        'id' => $data['id'],
        'title' => $this->input->post('event_title'),
        'start' => $start,
        'end' => $end,
        'details' => $details,
        'allDay' => false,
        'errorMessage' => 'Invalid input, please check and try again.'
    );

    echo json_encode($response);
  }

	public function delete()
  {
    $event_id = $this->input->post('event_id');
    $success = $this->Event->delete($event_id);
    $success = $success > 0 ? true : false;
    echo json_encode(array('success'=>$success));
  }

	public function get_form_width(){}

  public function get_all()
  {
    $data = array();

    $events = $this->Event->get_all();

    if($events->num_rows() > 0)
    {
      foreach($events->result() as $event)
      {
        $data[] = array(
            'id' => $event->id,
            'title'  => $event->title,
            'start'  => $event->start_date,
            'end'  => $event->end_date,
            'details'  => $event->details
        );
      }
    }

    echo json_encode($data);
  }
}