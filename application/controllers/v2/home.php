<?php
require_once(APPPATH.'controllers/secure_area.php');
class Home extends Secure_area {

	function __construct() {
		parent::__construct();
	}

	function ping(){
		
		$data = $this->input->post();
		if(empty($data['module'])){
			return false;
		}

		if($data['module'] == 'sales' && !empty($data['cart_id'])){
			$this->load->model('v2/cart_model');
			session_write_close();
			
			$this->cart_model->ping($data['cart_id']);
		}

		echo 'OK';
	}

	function index(){

		$this->load->model('v2/cart_model');
		$this->load->model('v2/sale_model');
		$this->load->model('v2/course_model');
		$this->load->model('v2/item_model');
		$this->load->model('v2/customer_model');
		$this->load->model('price_class');

		$course_ids = array();
		$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $this->session->userdata('course_id'));

		$data['course'] = $this->course_model->get($this->session->userdata('course_id'));
		$data['course']['customer_field_settings'] = $this->customer_model->get_field_settings();

		$default_taxes = [];
		if(!empty($data['course']['default_tax_1_name'])){
			$default_taxes[] = [
				'name' => $data['course']['default_tax_1_name'],
				'percent' => (float) $data['course']['default_tax_1_rate'],
				'cumulative' => 0
			];
		}
		if(!empty($data['course']['default_tax_2_name'])){
			$default_taxes[] = [
				'name' => $data['course']['default_tax_2_name'],
				'percent' => (float) $data['course']['default_tax_2_rate'],
				'cumulative' => (int) $data['course']['default_tax_2_cumulative']
			];
		}
		$data['course']['default_taxes'] = $default_taxes;

		$data['cart'] = $this->cart_model->get(['cart_id' => $this->cart_model->cart_id]);
		$data['cart'] = $data['cart'][0];

        // This gives us fully detailed items for returns upon page reload
        // or from refunding from reports
        $data['return_sale'] = null;
        if($data['cart']['return_sale_id']){
            // perform a brain transplant to have fully detailed items in the cart
            $sale_params = array('sale_id'=>$data['cart']['return_sale_id']);
            $data['return_sale'] = $this->sale_model->get($sale_params);
            if(is_array($data['return_sale']) && count($data['return_sale']) === 1){
               
                $data['return_sale'] = $data['return_sale'][0];
                $data['cart']['items'] = $data['return_sale']['items'];
                foreach($data['cart']['items'] as &$item){
                    $item['selected'] = true;
                    $item['quantity'] = 0 - $item['quantity'];
                     unset($item['service_fees']);
                }
                $data['cart']['sale_payments'] = $data['return_sale']['payments'];

                $tmp = array();
                foreach($data['cart']['sale_payments'] as $payment){
                    if($payment['amount'] ){
                        $tmp[] = $payment;
                    }
                }
                $data['cart']['sale_payments'] = $tmp;
            }
        }
		
		$sale_params = array(
			'limit' => 25,
			'timeago' => '24h',
			'include_customer_details' => true
		);

		if($data['course']['use_terminals'] == 1){
			$sale_params['terminal_id'] = (int) $this->session->userdata('terminal_id');
		}

		$data['sales'] = $this->sale_model->get($sale_params);
		$data['suspended_sales'] = $this->cart_model->get(['status' => 'suspended']);

		$this->load->library("Permissions");
		if($this->session->userdata('user_level') >= 2){
			$data['incomplete_sales'] = $this->cart_model->get(['status' => 'incomplete']);
		} else {
			$data['incomplete_sales'] = [];
		}

		// Get list of all course price classes
		$this->load->model('Price_class');
        $data['price_classes'] = $this->Price_class->get_menu();
        $data['price_class_array'] = $this->Price_class->get_menu(null, true);

        // Get list of all customer groups
		$groups = $this->db->select('group_id, label, item_cost_plus_percent_discount')
			->from('customer_groups')
			->where_in('course_id', $course_ids)
			->order_by('label')
			->get()->result_array();
		$data['customer_groups'] = $groups;

		// Get list of green/cart fees
		$this->load->model('Pricing');
		$fees = $this->Pricing->get_fees();
		$data['fees'] = $fees;

		// Get list of seasons
		$this->load->model('Season');
		$seasons = $this->Season->get_all();
		$data['seasons'] = $seasons;

		// Get all courses tee sheets
		$this->load->model('Teesheet');
		$teesheets = $this->Teesheet->get_teesheets(
			(int) $this->session->userdata('course_id'),
			['include_shared' => true]
		);
		$data['teesheets'] = $teesheets;


		// Get list of POS quick buttons
		$this->load->model('v2/Quick_button_model');
		$quick_buttons = $this->Quick_button_model->get();
		$data['quick_buttons'] = $quick_buttons;

		$this->load->model('Item');

		$data['food_bev_items'] = array();
		$data['food_bev_sides'] = array();
		
		$data['special_items'] = array(
			'credit_card_fee' => $this->item_model->get_special_item('credit_card_fee'),
			'service_fee' => $this->item_model->get_special_item('service_fee')
		);

        $this->load->model('custom_payments');
        $data['custom_payments'] = $this->custom_payments->get_all();

        $this->load->model('v2/Register_log_model');
		$logs = $this->Register_log_model->get(array('status' => 'open'));
		$data['register_logs'] = $logs;

		$this->load->model('Employee');
		$employees = $this->Employee->get_all(1000)->result_array();
		$data['employees'] = array_map(function($employee){
			unset($employee['CID'], $employee['TSID'], $employee['password'], $employee['card'], $employee['pin']);
			return $employee;
		}, $employees);

		$this->load->model('Module');
		$data['modules'] = $this->Module->get_course_modules();

		$this->load->model('Terminal');
		$data['terminals'] = $this->Terminal->get_all()->result_array();
		foreach($data['terminals'] as &$terminal){
			$terminal['drawer_number']=$terminal['cash_drawer_number'];
		}
		$current_terminal_id = (int) $this->session->userdata('terminal_id');
		
		$this->load->model('Printer');
		$data['receipt_printers'] = $this->Printer->get_all();

		$this->load->model('Pass');
		$data['passes'] = $this->Pass->get();

		$this->load->model('Supplier');
		$data['suppliers'] = $this->Supplier->get_all()->result_array();

		$this->load->model('Minimum_charge');
		$data['minimum_charges'] = $this->Minimum_charge->get();

        $this->load->model('v2/Import_job_model');
        $data['data_import_jobs'] = $this->Import_job_model->get();

        $this->load->model('v2/Item_receipt_content');
        $data['item_receipt_content'] = $this->Item_receipt_content->get();

		$pos_totals = $this->Dash_data->fetch_pos_data();
		$this->load->model('Dash_data');
		$data['pos_stats'] = array(
			'total_sales' => (float) clean_number($pos_totals['header']['Total']),
			'average_sale' => (float) clean_number($pos_totals['header']['Average_Sale'])
		);
		
		$this->load->model('v2/Meal_course_model');
		$meal_courses = $this->Meal_course_model->get();
		$data['meal_courses'] = $meal_courses;
		
		if($data['course']['use_terminals'] == 1){
			$data['terminals'][] = array(
				'terminal_id' => 0,
				'label' => 'Other',
				'persistent_logs' => 0,
				'quickbutton_tab' => 1
			);			
		}

		foreach($data['terminals'] as &$terminal){
			if($terminal['terminal_id'] == $current_terminal_id){
				$terminal['active'] = true;
			}else{
				$terminal['active'] = false;
			}
		}


		$virtual_number = $this->db->from("course_virtual_numbers")
			->where("course_id", $this->session->userdata('course_id'))
			->get()->row();
		$data['virtual_number'] = "";
		if(!empty($virtual_number)){
			$data['virtual_number'] = $virtual_number->virtual_number;
		}



		$this->load->view('v2/sales/sales', $data);
	}

	// Load iframe credit card window
	function credit_card_window(){
        
        $cart_id = $this->input->post('cart_id');

        $this->load->model('v2/Cart_model');
		if(isset($cart_id) && $cart_id !== false && !$this->Cart_model->cart_exists($cart_id)){
			
			$timestamp = time();
			$log_text = '[TIMESTAMP '.$timestamp.'] MISSING CART - [COURSE_ID: '.$this->session->userdata('course_id').'] [CART_ID '.$cart_id.']';
			
			// Write log to PHP error log
			error_log($log_text);

			echo "<script>
				alert('Your sale has timed out. Refreshing page...');
				location.reload();
			</script>";

			return false;
		}        

        $this->session->unset_userdata('payment_id');
        $this->load->model('Credit_card_payments');
        $this->Credit_card_payments->credit_card_window();
	}

    function cancel_credit_card_payment() {

        // USING ETS FOR PAYMENT PROCESSING
        if ($this->config->item('ets_key')) {

            // TODO: Put this into a model or library

//            $this->load->library('Hosted_payments');
//            $payment = new Hosted_payments();
//            $payment->initialize($this->config->item('ets_key'));
//            $session_id = $payment->get("session_id");
//
//            $payment->set("action", "verify")
//                ->set("sessionID", $session_id)
//                ->set("transactionID", $transaction_id);

        }
        // USING MERCURY FOR PAYMENT PROCESSING
        else if($this->config->item('mercury_id')) {
			$this->load->model('Credit_card_payments');
			$payment_id = $this->session->userdata('payment_id');

			// Adding a payment void immediately
			$voided = $this->Credit_card_payments->void($payment_id);

            // Save to cancelled payments table
            $this->Credit_card_payments->set_to_cancel($payment_id, $voided);
            $this->db->query("UPDATE foreup_sales_payments_credit_cards SET cancel_time = '".gmdate('Y-m-d H:i:s')."' WHERE payment_id = '$payment_id' LIMIT 1");
            $this->session->unset_userdata('payment_id');
        }
        echo json_encode(array('success'=>true));
    }

	function mercury_payment(){
		$data = $this->input->post();
		$payment_data = $this->session->userdata('mercury_payment');
		$this->session->unset_userdata('mercury_payment');

		if(!empty($data['ReturnCode'])){
			error_log("MERCURY 409 PAYMENT ERROR - mercury_payment - ".json_encode($data).json_encode($payment_data));
			$approved = false;
		}else{
			$approved = true;
		}
		$payment_data['approved'] = $approved;
		$payment_data['merchant_data'] = $data;
        $payment_data['merchant_data']['payment_id'] = $data['PaymentID'];
        $payment_data['payment_id'] = $data['PaymentID'];
        $data['payment_id'] = $data['PaymentID'];

        $payment_data['amount'] = (float) round($payment_data['amount'], 2);
        $payment_data['record_id'] = (int) $payment_data['invoice_id'];
        $payment_data['type'] = $payment_data['type'];

        // Complete payment here
        $this->load->model('Credit_card_payments');
        $payment_data = $this->Credit_card_payments->complete_payment($payment_data, $data);
		$this->session->unset_userdata('payment_id');

        $this->load->view('v2/sales/mercury_payment', $payment_data);
	}

    function record_pax_transaction(){
        $course_id = $this->config->item('course_id');
        $AprivaTransaction = new \fu\credit_cards\Processors\Apriva\AprivaTransaction();
        $post_data = $this->input->post();
        $api_data = $post_data['api_data'];
        $api_data['course_id'] = $course_id;
        $api_data['date_time'] = gmdate('Y-m-d H:i:s');
        $api_data['response'] = json_encode($api_data['response']);

        $AprivaTransaction->record($api_data);

        $transaction_data = $post_data['transaction_data'];
        $transaction_data['course_id'] = $course_id;
        $transaction_data['terminal_id'] = $this->session->userdata('terminal_id');
        $transaction_data['operator_id'] = $this->session->userdata('person_id');
        $AprivaTransaction->update($transaction_data['invoice_id'], $transaction_data);
    }

    function blackline_payment(){
		$data = $this->input->post();
		$payment_data = $this->session->userdata('mercury_emv');
		$this->session->unset_userdata('mercury_emv');

		if(empty($data['ResultCode']) || $data['ResultCode'] != 'Approved'){
			$approved = false;
		}else{
			$approved = true;
		}

		$payment_data['approved'] = $approved;
		$payment_data['merchant_data'] = $data;
		$payment_data['merchant_data']['payment_id'] = $data['RefID'];
		$payment_data['merchant_data']['ReturnMessage'] = $data['Message'];
		$payment_data['payment_id'] = $data['RefID'];
		$payment_data['PaymentID'] = $data['RefID'];
		$data['payment_id'] = $data['RefID'];

		$payment_data['amount'] = (float) round($data['Amount'], 2);
		$payment_data['record_id'] = (int) $data['RefID'];

		// Complete payment here
		$this->load->model('Credit_card_payments');
		$payment_data = $this->Credit_card_payments->complete_payment($payment_data, $data);
//var_dump($payment_data);

		echo json_encode($payment_data);
//		$this->load->view('v2/sales/mercury_payment', $payment_data);
    }
	function element_payment(){
		
		$payment_response = $this->input->get();
		$payment_data = $this->session->userdata('element_payment');
		$this->session->unset_userdata('element_payment');
		
		if(empty($payment_response['HostedPaymentStatus']) || $payment_response['HostedPaymentStatus'] != 'Complete'){
			$payment_data['approved'] = false;
		}else{
			$payment_data['approved'] = true;
		}
		
		$payment_data['merchant_data'] = $payment_response;
		$this->load->view('v2/sales/element_payment', $payment_data);
	}
	
	function mercury_error(){
		$data = $this->input->post();
		$payment_data = $this->session->userdata('mercury_payment');
		$this->session->unset_userdata('mercury_payment');

		if(!empty($data['ReturnCode'])){
			$approved = false;
		}else{
			$approved = true;
		}
		$payment_data['approved'] = $approved;
		$payment_data['merchant_data'] = $data;
		$payment_data['merchant_data']['payment_id'] = $data['PaymentID'];

		$this->load->view('v2/sales/mercury_payment', $payment_data);
	}	

	// Shows ETS iframe to check ETS giftcard balance
	function ets_giftcard_window(){

		$ets_key = $this->config->item('ets_key');
		if(empty($ets_key)){
			return false;
		}

		$data = array();
		$this->load->library('Hosted_payments');
		$payment = new Hosted_payments();
		$payment->initialize($this->config->item('ets_key'));

		// Open ETS session
		$session = $payment->set("action", "session")
			->set("posAction", "balance")
			->send();

		// If session opened successfully
		if($session->id){
			$this->session->set_userdata('ets_session_id', (string) $session->id);
			$data = array(
				'session' => $session,
				'url' => site_url('home/view_ets_giftcard'),
				'window' => 'balance_check'
			);

			$this->load->view('v2/sales/ets_credit_card.php', $data);

		// If error opening session
		}else{
			$data = array('processor' => 'ETS');
			$this->load->view('sales/cant_load', $data);
		}
	}

	function set_terminal($terminal_id){
		
		$this->load->model('Terminal');
		$this->load->model('v2/Register_log_model');
		
		if(!empty($terminal_id)){
			$terminal_info = $this->Terminal->get_info($terminal_id);	
			$terminal_info = $terminal_info[0];
			if(!isset($terminal_info['terminal_https']) && isset($terminal_info['https'])){
				$terminal_info['terminal_https'] = $terminal_info['https'];
			}
			$this->session->set_userdata('terminal_https',$terminal_info['terminal_https']);

		}else{
			$terminal_info = array(
				'terminal_id' => 0,
				'label' => 'Other',
				'persistent_logs' => 0,
				'quickbutton_tab' => 1
			);
		}

		$this->session->set_userdata('terminal_id', $terminal_id);
		$this->session->set_userdata('quickbutton_tab', $terminal_info['quickbutton_tab']);
		$this->session->set_userdata('all_persistent_logs', $terminal_info['persistent_logs']);

		$register_log = $this->Register_log_model->get_active($terminal_id, $this->session->userdata('person_id'));

		echo json_encode(array('terminal' => $terminal_info, 'register_log' => $register_log));
	}

	function migrate_roles(){
	    $this->load->model('Acl');
        $this->Acl->activate_new_permission(true);
    }

    function element_test_hosted_payments(){

        if(ENVIRONMENT == 'production'){
            echo 'Endpoint can only be accessed in development environment';
            return false;
        }

        require_once('application/libraries/v2/Element_merchant.php');
        require_once('element_api_test.php');
    }
}
