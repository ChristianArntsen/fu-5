<?php
require_once ("secure_area.php");
class Prices extends Secure_area
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Pricing');
	}

	function get_price($class_id = false){
		
		$teesheet_id = $this->input->get('teesheet_id');
		$price_class_id = $this->input->get('price_class_id');
		$date = $this->input->get('date');
		$time = $this->input->get('time');
		$holes = $this->input->get('holes');
		$cart = $this->input->get('cart');		
		$timeframe_id = $this->input->get('timeframe_id');
		
		if($cart === 'false'){
			$cart = false;
		}else if($cart === 'true'){
			$cart = true;
		}
		
		if(empty($holes)){
			echo json_encode(array('success'=>false, 'message'=>'Number of holes is required'));
			return false;
		}			
			
		if(!empty($timeframe_id)){
			$price = $this->Pricing->get_price_by_timeframe($timeframe_id, $holes, $cart);		
		
		}else{
			if(empty($teesheet_id)){
				echo json_encode(array('success'=>false, 'message'=>'Teesheet ID is required'));
				return false;
			}
			if(empty($date)){
				echo json_encode(array('success'=>false, 'message'=>'Date is required'));
				return false;
			}
			$date = DateTime::createFromFormat('m#d#Y', $date);
			$date = $date->format('Y-m-d');
			
			if(empty($time)){
				echo json_encode(array('success'=>false, 'message'=>'Time is required'));
				return false;
			}				
		
			$price = $this->Pricing->get_price($teesheet_id, $price_class_id, $date, $time, $holes, $cart);
		}
		
		echo json_encode(array('success' => true, 'price' => $price));
	}
}
