<?php
require_once ("secure_area.php");

class Service_Fees extends Secure_area
{
    public function getHtml($service_fee_id = null) {
        if(empty($service_fee_id))return '';
        $ServiceFee = new \fu\service_fees\ServiceFee($service_fee_id);
        return $ServiceFee->getHtml();
    }

    public function search() {
        $ServiceFee = new \fu\service_fees\ServiceFee();
        $query = $this->input->get('term');
        $rows = $ServiceFee->search($query);
        echo json_encode($rows);
    }

}