<?php
require_once(APPPATH.'libraries/barcode/BCGFontFile.php');
require_once(APPPATH.'libraries/barcode/BCGColor.php');
require_once(APPPATH.'libraries/barcode/BCGDrawing.php');
require_once (APPPATH."libraries/barcode/BCGcode128.barcode.php");

class Barcode extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();	
	}
	
	function index()
	{
		$text = $this->input->get('text');
		$barcode = $this->input->get('barcode');
		$number = $this->input->get('number');
		$scale = $this->input->get('scale') ? $this->input->get('scale') : 1.2;
		$thickness = $this->input->get('thickness') ? $this->input->get('thickness') : 21;
		$font_size = $this->input->get('font_size') ? $this->input->get('font_size') : 8;//8 working size
		$font_size = (float) $font_size;

		$font = new BCGFontFile(APPPATH.'libraries/barcode/font/Arial.ttf', $font_size);
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255);
		
		// Barcode Part
		$code = new BCGcode128();
		$code->setScale($scale);
		$code->setThickness($thickness);
		$code->setForegroundColor($color_black);
		$code->setBackgroundColor($color_white);
		$code->setFont($font);
		$code->setLabel($text);
		$code->parse($barcode, $number);
		
		// Drawing Part
		$drawing = new BCGDrawing('', $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();
		
		header('Content-Type: image/png');
		$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);	
	}

    function pdf($item_ids, $type = '', $sheet_style = '', $start_index = 1, $additional_price = false, $additional_price_percent = false, $top_margin = ''){
        
        $result = array();
        $type = empty($type) ? $this->input->get('type') : $type;

        if(!$type){
            $type = 'item';
        }

        $data['items'] = $result;
        $data['sheet_style'] = $sheet_style;
        
        if ($sheet_style == 5267){
            $data['scale'] = 1.5;
            $data['thickness'] = 20;
            $v_limit = 20;
            $h_limit = 4;
            $char_limit = 18;
        }
        else if ($sheet_style == 5160)
        {
            $data['scale'] = 1.5;
            $data['thickness'] = 20;
            $v_limit = 10;
            $h_limit = 3;
            $char_limit = 32;
        }
        else {
            $data['scale'] = 2;
            $v_limit = 20;
            $h_limit = 4;
        }
        $item_ids = explode('~', $item_ids);
        foreach ($item_ids as $item_id){
            
            if($type == 'item_kit'){
                $this->load->model('v2/Item_kit_model');
                $item_kit_info = $this->Item_kit_model->get(array('item_kit_id'=>$item_id));
                $item_kit_info = $item_kit_info[0];
                $item_info = new stdClass();
                $item_info->item_kit_number = $item_kit_info['item_number'];
                $item_info->item_kit_id = $item_kit_info['item_id'];
                $item_info->unit_price = $item_kit_info['unit_price'];
                $item_info->name = $item_kit_info['name'];

                $item_number = $item_info->item_kit_number;
                $item_id = $item_info->item_kit_id;
            
            }else{
                $item_info = $this->Item->get_info($item_id);
                $item_number = $item_info->item_number;
                $item_id = $item_info->item_id;
            }
            
            $price = to_currency($item_info->unit_price);
            $additional_price_string = '';
            
            if ($additional_price_percent) {
                $additional_price_string = urldecode($additional_price).' '.to_currency($item_info->unit_price * (100 + $additional_price_percent)/100);
            }

            $result[] = array(
                'name' => character_limiter($item_info->name, $char_limit - strlen($price), '...').' '.$price,
                'unit_price' => to_currency($item_info->unit_price), 
                'id' => number_pad($item_id, 11), 
                'number' => $item_number, 
                'additional_price' => $additional_price_string
            );
        }

        $this->load->library('fpdf');
        $pdf = new FPDF();
        $pdf->Open();
        $pdf->AddPage();
        $pdf->SetFont('Helvetica', 'B', 8);
        $pdf->SetMargins(0, 0);
        $pdf->SetAutoPageBreak(false);
        $x = $y = 0;   

        foreach($result as $item) {

            if ($sheet_style == 5267)
                $this->Item->Avery5267($x, $y, $pdf, urlencode($item['name']), $item['id'], $item['number'], $top_margin);
            else if ($sheet_style == 5160)
                $this->Item->Avery5160($x, $y, $pdf, $item['name'], $item['id'], $item['number'], $top_margin, $item['additional_price']);

            $y++; // next row
            if($y == $v_limit) { // end of page wrap to next column
                $x++;
                $y = 0;
                if($x == $h_limit) { // end of page
                    $x = 0;
                    $y = 0;
                    $pdf->AddPage();
                }
            }
        }
        
        $pdf->Output();
    }		
}
?>