<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quickbooks_Export extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('qb_iif');

		$this->total_debits = 0;
		$this->total_credits = 0;
	}

	private function get_payments(){

		$this->db->select('sales_payments.sale_id, sale_time, reports_only,
			sales_payments.payment_type, SUM(payment_amount) AS payment_amount,tip_recipient,is_tip', false);
		$this->db->from('sales_payments');
		$this->db->join('sales', 'sales.sale_id = sales_payments.sale_id', 'inner');
		$this->db->where("sale_time BETWEEN '".$this->start_date."' AND '".$this->end_date."'");
		$this->db->where('sales.deleted', 0);
		$this->db->where('sales.course_id', (int) $this->session->userdata('course_id'));
		$this->db->group_by('sales_payments.payment_type');

		if ($this->terminal != 'all'){
			$this->db->where('terminal_id', $this->terminal);
		}

		$payments = $this->db->get()->result_array();
		$payments = $this->total_payments($payments);

		return $payments;
	}

	private function get_payment_account($type){

		if($type == 'cash_refund'){
			$type = 'cash';
		}
		if($type == 'credit_card_refund'){
			$type = 'credit_card';
		}
		if($type == 'customer_credit_refund'){
			$type = 'customer_credit';
		}
		if($type == 'member_account_refund'){
			$type = 'member_account';
		}

		$qb_account = $this->account_map['payments'][$type]['account'];

		if($type && !empty($qb_account)){
			return $qb_account;
		}

		return false;
	}

	private function get_payment_class($type){

		if($type == 'cash_refund'){
			$type = 'cash';
		}
		if($type == 'credit_card_refund'){
			$type = 'credit_card';
		}
		if($type == 'customer_credit_refund'){
			$type = 'customer_credit';
		}
		if($type == 'member_account_refund'){
			$type = 'member_account';
		}
		$qb_class = $this->account_map['payments'][$type]['class'];

		if($type){
			return $qb_class;
		}
		return null;
	}

	private function get_custom_payments(){

		$rows = $this->db->select('label, custom_payment_type')
			->from('foreup_custom_payment_types')
			->where('course_id', $this->session->userdata('course_id'))
			->get()->result_array();

		$custom_payments = array();
		if(!empty($rows)){
			foreach($rows as $row){
				$custom_payments[$row['label']] = $row['custom_payment_type'];
			}
		}

		return $custom_payments;	
	}

	// Merges tip sub payments (Cash, Check, etc) and adds them to regular payments
	private function total_payments($payments){

		$custom_payments = $this->get_custom_payments();

		$customer_account = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
		$member_account = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
		$totaled_payments = array();

		foreach($payments as $row){

			if($row['reports_only'] == 1){
				continue;
			}

			$type = false;
			$is_custom = false;
			if(!empty($custom_payments) && array_key_exists($row['payment_type'], $custom_payments)){
				$type = $custom_payments[$row['payment_type']];
				$memo = $row['payment_type'];
				$is_custom = true;

			}else if (stripos($row['payment_type'], 'Partial CC Refund') !== false){
				$type = 'credit_card_refund';
				$memo = 'Partial Credit Card Refund';

			}else if (stripos($row['payment_type'], 'M/C') !== false){
				$type = 'credit_card_mastercard';
				$memo = 'MasterCard';

			}else if (stripos($row['payment_type'], 'VISA') !== false){
				$type = 'credit_card_visa';
				$memo = 'VISA';

			}else if (stripos($row['payment_type'], 'AMEX') !== false){
				$type = 'credit_card_amex';
				$memo = 'AMEX';

			}else if (stripos($row['payment_type'], 'DCVR') !== false){
				$type = 'credit_card_discover';
				$memo = 'Discover';

			}else if (stripos($row['payment_type'], 'DINERS') !== false){
				$type = 'credit_card_diners';
				$memo = 'Diners';

			}else if (stripos($row['payment_type'], 'JCB') !== false){
				$type = 'credit_card_jcb';
				$memo = 'JCB';

			}else if (stripos($row['payment_type'], 'Cash Refund') !== false){
				$type = 'cash_refund';
				$memo = 'Cash Refund';

			}else if (stripos($row['payment_type'], 'Cash') !== false || stripos($row['payment_type'], 'Change issued') !== false){
				$type = 'cash';
				$memo = 'Cash';

			}else if (stripos($row['payment_type'], 'Raincheck') !== false){
				$type = 'rain_check';
				$memo = 'Raincheck';

			}else if (stripos($row['payment_type'], 'Check') !== false){
				$type = 'check';
				$memo = 'Check';

			}else if (stripos($row['payment_type'], 'Credit Card') !== false || stripos($row['payment_type'], 'CC Refund') !== false){
				$type = 'credit_card';
				$memo = 'Credit Card';

			}else if (stripos($row['payment_type'], $customer_account.' Refund') !== false){
				$type = 'customer_credit_refund';
				$memo = $customer_account. ' Refund';

			}else if (stripos($row['payment_type'], $member_account.' Refund') !== false){
				$type = 'member_account_refund';
				$memo = $member_account. ' Refund';

			}else if (stripos($row['payment_type'], $customer_account) !== false || stripos($row['payment_type'], 'Customer Credit') !== false){
				$type = 'customer_credit';
				$memo = $customer_account;

			}else if (stripos($row['payment_type'], $member_account) !== false || stripos($row['payment_type'], 'Member Balance') !== false){
				$type = 'member_account';
				$memo = $member_account;

			}else if (stripos($row['payment_type'], 'Gift Card Discount') !== false){
				$type = 'gift_card_discount';
				$memo = 'Gift Card Discounts';

			}else if (stripos($row['payment_type'], 'Gift') !== false){
				$type = 'gift_card';
				$memo = 'Gift Card';

			}else if (stripos($row['payment_type'], 'Punch Card') !== false){
				$type = 'punch_card';
				$memo = 'Punch Card';

			}else if (stripos($row['payment_type'], 'Invoice Charge') !== false || stripos($row['payment_type'], 'Billing Account Balance') !== false){
				$type = 'invoice_charge';
				$memo = 'Invoice Charge';

			}else if (stripos($row['payment_type'], 'Loyalty') !== false){
				$type = 'loyalty_points';
				$memo = 'Loyalty Points';

			}else if (stripos($row['payment_type'], 'Tournament Pot') !== false){
				$type = 'tournament_winnings';
				$memo = 'Tournament Winnings';

			}else if (stripos($row['payment_type'], 'Coupon') !== false){
				$type = 'coupons';
				$memo = 'Coupons';
			}

			if(empty($type)){
				continue;
			}

			if(isset($totaled_payments['payments'][$type])){
				$totaled_payments['payments'][$type]['amount'] += (float) $row['payment_amount'];
			}else{
				$totaled_payments['payments'][$type] = array('memo' => $memo, 'amount' => (float) $row['payment_amount']);
			}

			if(!$is_custom && $row['tip_recipient'] != 0){
				if($totaled_payments['tips'][$type]){
					$totaled_payments['tips'][$type]['amount'] += (float) $row['payment_amount'];
				}else{
					$memo .= ' Tip';
					$totaled_payments['tips'][$type] = array('memo' => $memo, 'amount' => (float) $row['payment_amount']);
				}
			}
		}

		return $totaled_payments;
	}

	private function process_payments($payments){

		foreach($payments as $transaction_type => $payment_list){

			foreach($payment_list as $type => $payment){

				if(empty($payment)){
					continue;
				}
				$memo = 'Payments - '.$payment['memo'];

				// If payment is a tip, credit the tips payable account
				if($transaction_type == 'tips'){
					$amount = -(float) $payment['amount'];

					$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $this->account_map['payments']['tips']['account'], null, $this->account_map['payments']['tips']['class'], $amount, $memo);
					$this->total_debits += $amount;

				// All other payments
				}else{
					$qb_class = $this->get_payment_class($type);
					$qb_account = $this->get_payment_account($type);

					$amount = (float) $payment['amount'];
					$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $qb_account, null, $qb_class, $amount, $memo);

					if($type == 'cash_refund' || $type == 'credit_card_refund'
						|| $type == 'member_account_refund'
						|| $type == 'customer_credit_refund'
					){
						$refund_account = $this->account_map['refunds']['account'];
						$refund_class = $this->account_map['refunds']['class'];
						$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $refund_account, null, $refund_class, -$amount, $memo);
					}

					$this->total_debits += $amount;
				}
			}
		}
	}

	private function process_sales($sales){

		foreach($sales as $category){

			$md5_category = md5($category['actual_category']);
			if($category['actual_category'] == 'Invoices' || $category['actual_category'] == 'Invoice Payments'){
				$md5_category = md5('Account Payments');
			}

			$qb_income = $this->account_map['categories'][$md5_category]['income'];
			$qb_income_class = $this->account_map['categories'][$md5_category]['income_class'];

			$qb_cogs = $this->account_map['categories'][$md5_category]['cogs'];
			$qb_cogs_class = $this->account_map['categories'][$md5_category]['cogs_class'];

			$qb_inventory = $this->account_map['categories'][$md5_category]['inventory'];
			$qb_inventory_class = $this->account_map['categories'][$md5_category]['inventory_class'];

			$revenue = -(float) $category['subtotal'];
			$cost = (float) $category['cost'];

			if(empty($category['actual_category'])){
				$category['actual_category'] = 'Uncategorized';
			}

			if($qb_income){
				$memo = 'Revenue - '.$category['actual_category'];
				$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $qb_income, null, $qb_income_class, $revenue, $memo);
			}

			if(!empty($this->export_data['inventory_cogs'])){
				if($qb_cogs){
					$memo = 'Cost - '.$category['actual_category'];
					$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $qb_cogs, null, $qb_cogs_class, $cost, $memo);
				}

				if($qb_inventory){
					$memo = 'Inventory - '.$category['actual_category'];
					$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $qb_inventory, null, $qb_inventory_class, -$cost, $memo);
				}
			}

			$this->total_credits += $category['subtotal'];
		}
	}

	private function process_receivings($receivings){

		foreach($receivings as $receiving){

			$qb_inventory = $this->account_map['categories'][ md5($receiving['category']) ]['inventory'];
			$qb_inventory_class = $this->account_map['categories'][ md5($category['actual_category']) ]['inventory_class'];

			$qb_payable = $this->account_map['accounts_payable']['account'];
			$qb_payable_class = $this->account_map['accounts_payable']['class'];
			$supplier = $receiving['supplier'];
			$invoice_number = $receiving['invoice_number'];

			$total = (float) $receiving['total'];
			$memo = 'Receiving Inventory - '.$receiving['category'];
			$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $qb_inventory, $supplier, $qb_inventory_class, $total, $memo, $invoice_number);

			$total = (float) $receiving['total'];
			$memo = 'Receiving Payment - '.$receiving['category'];
			$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $qb_payable, $supplier, $qb_payable_class, -$total, $memo, $invoice_number);
		}
	}

	private function process_taxes($taxes){

		foreach($taxes as $tax){
			$qb_account = $this->account_map['taxes'][ md5($tax['percent']) ]['account'];
			$qb_class = $this->account_map['taxes'][ md5($tax['percent']) ]['class'];
			$qb_vendor = $this->account_map['taxes'][ md5($tax['percent']) ]['vendor'];

			// Apply excess tax to 1st tax account in list
			$amount = -(float) $tax['tax'];

			$memo = $tax['percent'].' Tax';
			$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $qb_account, $qb_vendor, $qb_class, $amount, $memo);
		}
	}

	private function process_receivable_adjustments(){

		$start = date('Y-m-d', strtotime($this->start_date));
		$end = date('Y-m-d', strtotime($this->end_date));

		$this->load->model('Account_transactions');
		$amount = $this->Account_transactions->get_total(
			array('member', 'customer'),
			'all',
			array('manual_adjustments' => true, 'date_start' => $start, 'date_end' => $end)
		);

		$from = $this->account_map['receivable_adjustments']['account'];
		$from_class = $this->account_map['receivable_adjustments']['class'];

		$qb_receivable = $this->account_map['categories'][md5('Account Payments')]['income'];
		$qb_receivable_class = $this->account_map['categories'][md5('Account Payments')]['income_class'];

		$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $from, null, $from_class, $amount, 'A/R Manual Adjustments');
		$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $qb_receivable, null, $qb_receivable_class, -$amount, 'A/R Manual Adjustments');
	}

	// Totals up rainchecks issued for a particular period
	private function process_rainchecks(){

		$course_id = (int) $this->session->userdata('course_id');

		$query = $this->db->query("SELECT SUM(total) AS total
			FROM foreup_rainchecks
			LEFT JOIN foreup_teesheet
				ON foreup_teesheet.teesheet_id = foreup_rainchecks.teesheet_id
			WHERE foreup_teesheet.course_id = {$course_id}
				AND date_issued >= '{$this->start_date}'
				AND date_issued <= '{$this->end_date}'");

		if($query->num_rows() > 0){

			$row = $query->row_array();
			$amount = (float) $row['total'];

			$debit = $this->account_map['rain_checks_issued_debit']['account'];
			$debit_class = $this->account_map['rain_checks_issued_debit']['class'];

			$credit = $this->account_map['rain_checks_issued_credit']['account'];
			$credit_class = $this->account_map['rain_checks_issued_credit']['class'];

			$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $credit, null, $credit_class, -$amount, 'Rainchecks Issued');
			$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $debit, null, $debit_class, $amount, 'Rainchecks Issued');
		}
	}
	
	// Pulls any manual gift card value adjustments
	private function process_gift_card_adjustments(){

		$course_id = (int) $this->session->userdata('course_id');

		$query = $this->db->query("SELECT SUM(trans_amount) AS total
			FROM foreup_giftcard_transactions
			WHERE course_id = {$course_id}
				AND trans_date >= '{$this->start_date}'
				AND trans_date <= '{$this->end_date}'
				AND trans_comment = 'Giftcard Save'");

		if($query->num_rows() > 0){

			$row = $query->row_array();
			$amount = (float) $row['total'];

			$debit = $this->account_map['gift_card_adjustments_debit']['account'];
			$debit_class = $this->account_map['gift_card_adjustments_debit']['class'];

			$credit = $this->account_map['gift_card_adjustments_credit']['account'];
			$credit_class = $this->account_map['gift_card_adjustments_credit']['class'];

			$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $credit, null, $credit_class, -$amount, 'Manual Giftcard Adjustments (Credit)');
			$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $debit, null, $debit_class, $amount, 'Manual Giftcard Adjustments (Debit)');
		}
	}	

	function index(){

		$params = $this->input->get();

		if(empty($params['start_date']) || empty($params['end_date'])){
			echo 'Start and end date required';
		}

		if(empty($params['register_log'])){
			$params['register_log'] = 0;
		}

		$data = $params['data'];
		$this->export_data = $data;

		$start_date = date('Y-m-d', strtotime($params['start_date'])). ' 00:00:00';
		$this->date = date('m/d/Y', strtotime($params['end_date']));
		$end_date = date('Y-m-d', strtotime($params['end_date'])). ' 23:59:59';
		$this->start_date = $start_date;
		$this->end_date = $end_date;
		$register_log = $params['register_log'];
		$terminal = $params['terminal'];

		if(empty($terminal)){
			$terminal = 'all';
		}
		$this->terminal = $terminal;

		ini_set('memory_limit', '512M');
		set_time_limit(180);

		$this->load->model('reports/Summary_z_out');
		$this->load->model('reports/Summary_payments');
		$this->load->model('reports/Summary_categories');
		$this->load->model('reports/Summary_taxes');
		$this->load->model('reports/Detailed_receivings');
		$this->load->model('Sale');
		$this->load->model('Receiving');

		// Load the QuickBooks account map
		$account_map = json_decode($this->config->item('quickbooks_export_settings'), true);

		// Map all invoice payoffs to the course's accounts receivable
		$account_map['categories'][md5('Invoice')]['income'] = $account_map['categories'][md5('Account Payments')]['income'];
		$account_map['categories'][md5('Invoice')]['income_class'] = $account_map['categories'][md5('Account Payments')]['income_class'];
		$account_map['categories'][md5('Invoice Payments')]['income'] = $account_map['categories'][md5('Account Payments')]['income'];
		$account_map['categories'][md5('Invoice Payments')]['income_class'] = $account_map['categories'][md5('Account Payments')]['income_class'];
		$this->account_map = $account_map;

		$params = array('start_date' => $start_date, 'end_date' => $end_date);
		$department = 'all';
		$params['department'] = $department;
		$this->Summary_z_out->setParams($params);

		$this->Sale->create_sales_items_temp_table($params);

		$debits = '';
		$credits = '';
		$summary_data = '';
		$second_summary = '';
		$cash_drawer_variance = 0;

		if($data['sales'] == 1){

			// Get payment totals
			$payments = $this->get_payments();

			// Get revenue/costs by category
			$this->Summary_categories->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'department'=>'all', 'terminal'=>$terminal));
			$sales = $this->Summary_categories->getData();

			// Get tax totals
			$this->Summary_taxes->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'department'=>'all', 'terminal'=>$terminal));
			$taxes = $this->Summary_taxes->getData(false);

			if($register_log == 1){

				// Get register counts
				$drawer_counts = $this->Summary_z_out->getDrawerCounts();
				$total_drawer_count = 0;
				foreach ($drawer_counts['summary'] as $drawer_count) {
					$total_drawer_count += round($drawer_count['close_amount'] - $drawer_count['open_amount'], 2);
				}

				// Calculate difference from actual cash sales
				$cash_drawer_variance = round($total_drawer_count - $payments['payments']['cash']['amount'], 2);

				// Use actual cash in drawer instead of total cash sales
				$payments['payments']['cash']['amount'] = $total_drawer_count;
			}
		}

		// Begin IIF transaction
		$this->iif_transaction = new Qb_IIF_Transaction(null, null, 'GENERAL JOURNAL', $this->date, 'Undeposited Funds', null, null, 0, null, '');

		$this->process_payments($payments);
		$this->process_sales($sales);
		$this->process_rainchecks();
		$this->process_gift_card_adjustments();

		if($data['receivable_adjustments'] == 1){
			$this->process_receivable_adjustments();
		}

		// If cash drawer is over or short, add it to transactions
		if($data['sales'] == 1 && $register_log == 1 && $cash_drawer_variance != 0){
			$cash_drawer_account = $this->account_map['cash_variance']['account'];
			$cash_drawer_class = $this->account_map['cash_variance']['class'];

			if($cash_drawer_variance > 0){
				$memo = 'Cash Drawer Over';
			}else{
				$memo = 'Cash Drawer Short';
			}
			$this->total_debits -= $cash_drawer_variance;

			$this->iif_transaction->add_split(null, null, 'GENERAL JOURNAL', $this->date, $cash_drawer_account, null, $cash_drawer_class, -$cash_drawer_variance, $memo);
		}

		// Process taxes
		$this->process_taxes($taxes);

		// Process receivings
		if($data['receivings'] == 1){
			$this->Receiving->create_receivings_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => 'all'));
			$this->Detailed_receivings->setParams();
			$receivings = $this->Detailed_receivings->getDataBySupplier();
			$this->process_receivings($receivings);
		}

		// Generate IIF data
		$this->qb_iif->add($this->iif_transaction);
		$this->iif = $this->qb_iif->generate();

		$course_name = preg_replace("/[^a-zA-Z0-9]+/", "", $this->session->userdata('course_name'));
		$filename = $course_name.'_ForeUp_'.date('m-d-Y', strtotime($params['start_date'])).'_to_'.date('m-d-Y', strtotime($params['end_date'])).'.csv';

		// Output IIF file to browser for download
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		
		// Output the IIF data, replace all Unix new line characters with
		// windows new line characters
		echo preg_replace('~(*BSR_ANYCRLF)\R~', "\r\n", $this->iif);
	}
}
