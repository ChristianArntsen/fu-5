<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Items extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();

		$this->load->model('v2/item_model');
		$this->load->model('v2/Quick_button_model');
		$this->load->model('v2/Menu_model');
	}

	// Loop through item list and attach correct button order to each item
	private function apply_order_key(&$items){
		
		$this->load->model('Table');
		$order = $this->Table->get_button_order();

		foreach($items as &$item){
			$key = $item['name'].'/'.$item['category'].'/'.$item['subcategory'];
			
			if(!empty($order[$key])){
				$item['order'] = (int) $order[$key];
			}else{
				$item['order'] = 0;
			}
		}
	}

	// Get list of items filtered by a variety of parameters
	function index_get($method = null){
		
		session_write_close();

		if($method == 'categories'){
			return $this->categories_get();
			
		}else if($method == 'sub_categories'){
			return $this->sub_categories_get();
		
		}else if($method == 'departments'){
			return $this->departments_get();
		
		}else if($method == 'menu_categories'){
			return $this->menu_categories_get();
		
		}else if($method == 'menu_subcategories'){
			return $this->menu_subcategories_get();
		
		}else if($method == 'menu_buttons'){
			return $this->menu_buttons_get();

		}else if($method == 'csv'){
			return $this->csv_get();
		
		}else if($method == 'upcs'){
			return $this->upcs_get();
		}
		
		$params = $this->input->get();
		if(!empty($params['search'])){
			$params['q'] = $params['search'];
		}
		
		$item_id = $method;
		if(!empty($item_id)){
			$params['item_id'] = $item_id;
		}

		if(!empty($params['food_and_beverage'])){
			
			$this->load->model('Item');
			
			if(!empty($params['sides'])){	
				$items = $this->Item->get_all_food(1);
			}else{
				$items = $this->Item->get_all_food();
			}
			$this->apply_order_key($items);
		
		}else{
			
			if(empty($params['sort'])){
				$params['sort'] = 'name';
			}

			if(!empty($params['item_id'])){
				$params['include_inactive'] = true;
			}

			$items = $this->item_model->get($params);
			$data = [
				'total' => $this->item_model->total,
				'rows' => $items
			];
			$this->response($data, 200);
			return true;
		}

		$this->response($items, 200);
	}

	function index_post($method = null, $record_id = null){

		if($method == 'quickbuttons'){
			return $this->quickbuttons_post();
		
		}else if($method == 'menu_buttons'){
			return $this->menu_buttons_post();
		}

		$item_id = $this->item_model->save($method, $this->request->body);
		
		if(!$item_id){
			$msg = 'Error saving item';
			if(!empty($this->item_model->error)){
				$msg = $this->item_model->error;
			}
			$this->response(['msg' => $msg], 400);

		}else{
			$this->response(['item_id' => (int) $item_id], 200);
		}
	}

	function index_put($method = null, $record_id = null){
		
		session_write_close();

		if($method == 'quickbuttons'){
			return $this->quickbuttons_put($record_id);
		}
		$this->index_post($method, $record_id);
	}
	
	function index_delete($method = null, $record_id = null){
		
		if($method == 'quickbuttons'){
			return $this->quickbuttons_delete($record_id);
		}

		$this->item_model->delete($method);
		$this->response(['item_id' => (int) $method], 200);
	}	
	
	/*******************************************************************
	 * Quickbuttons
	 * ****************************************************************/
	function quickbuttons_post(){
		session_write_close();
		$response = $this->Quick_button_model->save(null, $this->request->body);

		if($response){
			$this->response(array('success' => true, 'quickbutton_id' => (int) $response), 201);
		}else{
			$this->response(array('success' => false), 400);
		}
	}

	function quickbuttons_put($param){
		session_write_close();
		if($param == 'order'){
			$this->Quick_button_model->save_order($this->request->body);
			$this->response(array('success' => true), 200);
		
		}else{
			$response = $this->Quick_button_model->save($param, $this->request->body);
			
			if($response){
				$this->response(array('success' => true, 'quickbutton_id' => (int) $response), 200);
			}else{
				$this->response(array('success' => false), 400);
			}
		}
	}

	function quickbuttons_delete($quickbutton_id){
		session_write_close();
		$response = $this->Quick_button_model->delete($quickbutton_id);
		
		if($response){
			$this->response(array('success' => true), 200);
		}else{
			$this->response(array('success' => false), 400);
		}		
	}
	
	function upcs_get($upc_id = null){
		session_write_close();
		$params = $this->input->get();
		$categories = $this->item_model->get_upcs($upc_id, $params);

		$this->response($categories, 200);
	}

	/*******************************************************************
	 * Categories
	 * ****************************************************************/
	function categories_get(){
		session_write_close();
		$params = $this->input->get();
		$categories = $this->item_model->get_categories($params);

		$this->response($categories, 200);
	}

	/*******************************************************************
	 * Departments
	 * ****************************************************************/
	function departments_get(){
		session_write_close();
		$params = $this->input->get();
		$departments = $this->item_model->get_departments($params);

		$this->response($departments, 200);
	}
	
	/*******************************************************************
	 * Sub-categories
	 * ****************************************************************/
	function sub_categories_get(){
		session_write_close();
		$params = $this->input->get();
		$sub_categories = $this->item_model->get_sub_categories($params);

		$this->response($sub_categories, 200);
	}

	/*******************************************************************
	 * Menu categories/subcategories
	 * ****************************************************************/
	function menu_categories_get(){
		session_write_close();
		$params = $this->input->get();

		if(!empty($params['term'])){
			$params['search'] = $params['term'];
		}
		$data = $this->Menu_model->get_categories($params);

		$this->response($data, 200);
	}

	function menu_subcategories_get(){
		session_write_close();
		$params = $this->input->get();

		if(!empty($params['term'])){
			$params['search'] = $params['term'];
		}
		$data = $this->Menu_model->get_subcategories($params);

		$this->response($data, 200);
	}
	
	function menu_buttons_get(){
		session_write_close();
		$params = $this->input->get();
		$data = $this->Menu_model->get_menu_buttons($params);
		$this->response($data, 200);
	}

	// Used to save sort order of buttons in F&B
	function menu_buttons_post(){
		session_write_close();
		$data = $this->input->post();
		$buttons_saved = $this->Menu_model->save_menu_buttons($data['buttons']);
		
		if($buttons_saved){
			$success = true;
		}else{
			$success = false;
		}
		$this->response(['success' => $success]);
	}

	function csv_get(){
		$csv = $this->item_model->get_csv();
		force_download('items_export' . '.csv', $csv);
		exit;
	}	
}
