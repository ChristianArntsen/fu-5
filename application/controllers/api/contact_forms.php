<?php
require_once(APPPATH . 'libraries/REST_Controller.php');

class contact_forms extends REST_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->authenticate();

		$this->course_id = (int)$this->session->userdata('course_id');
		$this->person_id = (int)$this->session->userdata('person_id');

		// Instantiate DI Models.
		$this->load->model('course');
		$this->load->model('contact_form');
		$this->load->model('contact_form_reply');
		$this->load->model('contact_form_key');
	}

	/*
	 * Returns contact forms for current course
	 */
	public function index_get($id = null)
	{
		session_write_close();

		if ($id === null) {
			$data['contact_forms'] = $this->get_course_forms($this->course_id);

			if ($data['contact_forms'] !== false) {
				$this->response($data, 200);
			} else {
				$data['contact_forms'] = array();
				$this->response($data, 200);

			}
		} else {
			$this->contact_form->id = (int)$id;
			if ($this->contact_form->retrieve() && $this->contact_form->course_id == $this->course_id) {

				$data['contact_form'] = $this->contact_form;
				$this->response($data, 200);
			} else {
				// Could not retrieve or wrong course. Most likely invalid data or service outage.
				$data = [
					'success' => false,
					'message' => 'Could not get contact form'
				];
				$this->response($data, 400);
			}
		}
	}

	/*
	 * Create new Contact Form
	 */
	public function index_post()
	{
		session_write_close();

		$this->contact_form->course_id = $this->course_id;
		$this->contact_form->created_by = $this->person_id;

		$json_request_data = $this->request->body;
		$customer_group_id = $json_request_data['customer_group_id'];
		$this->contact_form->customer_group_id = ((int)$customer_group_id == false ? null : (int)$customer_group_id);
		$this->contact_form->name = $json_request_data['name'];
		$this->contact_form->has_first_name = ($json_request_data['has_first_name'] == 'true' ? true : false);
		$this->contact_form->has_last_name = ($json_request_data['has_last_name'] == 'true' ? true : false);
		$this->contact_form->has_phone_number = ($json_request_data['has_phone_number'] == 'true' ? true : false);
		$this->contact_form->has_email = ($json_request_data['has_email'] == 'true' ? true : false);
		$this->contact_form->has_birthday = ($json_request_data['has_birthday'] == 'true' ? true : false);
		$this->contact_form->has_address = ($json_request_data['has_address'] == 'true' ? true : false);
		$this->contact_form->has_city = ($json_request_data['has_city'] == 'true' ? true : false);
		$this->contact_form->has_state = ($json_request_data['has_state'] == 'true' ? true : false);
		$this->contact_form->has_zip = ($json_request_data['has_zip'] == 'true' ? true : false);
		$this->contact_form->has_message = ($json_request_data['has_message'] == 'true' ? true : false);
		$this->contact_form->req_first_name = ($json_request_data['req_first_name'] == 'true' ? true : false);
		$this->contact_form->req_last_name = ($json_request_data['req_last_name'] == 'true' ? true : false);
		$this->contact_form->req_phone_number = ($json_request_data['req_phone_number'] == 'true' ? true : false);
		$this->contact_form->req_email = ($json_request_data['req_email'] == 'true' ? true : false);
		$this->contact_form->req_birthday = ($json_request_data['req_birthday'] == 'true' ? true : false);
		$this->contact_form->req_address = ($json_request_data['req_address'] == 'true' ? true : false);
		$this->contact_form->req_city = ($json_request_data['req_city'] == 'true' ? true : false);
		$this->contact_form->req_state = ($json_request_data['req_state'] == 'true' ? true : false);
		$this->contact_form->req_zip = ($json_request_data['req_zip'] == 'true' ? true : false);
		$this->contact_form->req_message = ($json_request_data['req_message'] == 'true' ? true : false);

		if ($this->contact_form->create() === true) {
			unset($data);
			$data['contact_form'] = $this->contact_form;
			$this->response($data, 200);
		} else {
			// Could not save. Most likely invalid data or service outage.
			$data = [
				'success' => false,
				'message' => 'Could not save contact form'
			];
			$this->response($data, 400);
		}
	}

	/*
	 * Update existing contact form
	 */
	public function index_put($id = null)
	{
		session_write_close();

		if ($id === null) {
			// Needs an ID
			$data = [
				'success' => false,
				'message' => 'Invalid ID'
			];
			$this->response($data, 400);
		}

		$id = (int)$id;
		$this->contact_form->id = $id;
		if ($this->contact_form->retrieve()) {

			if ($this->contact_form->course_id != $this->course_id) {
				// Not part of the right course!
				$data = [
					'success' => false,
					'message' => 'Not authorized'
				];
				$this->response($data, 401);
				die();
			}

			$json_request_data = $this->request->body;
			$customer_group_id = $json_request_data['customer_group_id'];
			$this->contact_form->customer_group_id = ((int)$customer_group_id == false ? null : (int)$customer_group_id);
			$this->contact_form->name = $json_request_data['name'];
			$this->contact_form->has_first_name = ($json_request_data['has_first_name'] == 'true' ? true : false);
			$this->contact_form->has_last_name = ($json_request_data['has_last_name'] == 'true' ? true : false);
			$this->contact_form->has_phone_number = ($json_request_data['has_phone_number'] == 'true' ? true : false);
			$this->contact_form->has_email = ($json_request_data['has_email'] == 'true' ? true : false);
			$this->contact_form->has_birthday = ($json_request_data['has_birthday'] == 'true' ? true : false);
			$this->contact_form->has_address = ($json_request_data['has_address'] == 'true' ? true : false);
			$this->contact_form->has_city = ($json_request_data['has_city'] == 'true' ? true : false);
			$this->contact_form->has_state = ($json_request_data['has_state'] == 'true' ? true : false);
			$this->contact_form->has_zip = ($json_request_data['has_zip'] == 'true' ? true : false);
			$this->contact_form->has_message = ($json_request_data['has_message'] == 'true' ? true : false);
			$this->contact_form->req_first_name = ($json_request_data['req_first_name'] == 'true' ? true : false);
			$this->contact_form->req_last_name = ($json_request_data['req_last_name'] == 'true' ? true : false);
			$this->contact_form->req_phone_number = ($json_request_data['req_phone_number'] == 'true' ? true : false);
			$this->contact_form->req_email = ($json_request_data['req_email'] == 'true' ? true : false);
			$this->contact_form->req_birthday = ($json_request_data['req_birthday'] == 'true' ? true : false);
			$this->contact_form->req_address = ($json_request_data['req_address'] == 'true' ? true : false);
			$this->contact_form->req_city = ($json_request_data['req_city'] == 'true' ? true : false);
			$this->contact_form->req_state = ($json_request_data['req_state'] == 'true' ? true : false);
			$this->contact_form->req_zip = ($json_request_data['req_zip'] == 'true' ? true : false);
			$this->contact_form->req_message = ($json_request_data['req_message'] == 'true' ? true : false);

			if ($this->contact_form->update() === true) {
				// Ahh yisss. Everything is going as planned.
				unset($data); // Clean out any existing data
				$data['contact_form'] = $this->contact_form;
				$this->response($data, 200);
			} else {
				// Could not update. Most likely invalid data or service outage.
				$data = [
					'success' => false,
					'message' => 'Could not update contact form'
				];
				$this->response($data, 400);
			}
		} else {
			// Invalid ID
			$data = [
				'success' => false,
				'message' => 'Invalid ID'
			];
			$this->response($data, 400);
		}
	}

	/*
	 * (Soft) deletes a contact form
	 */
	public function index_delete($id = null)
	{
		session_write_close();

		if ($id !== null) {
			$id = (int)$id;

			$this->contact_form->id = $id;
			if ($this->contact_form->retrieve()) {
				// Valid Contact Form
				$this->contact_form->deleted_by = $this->person_id;
				$this->contact_form->delete();
				$data['contact_form'] = $this->contact_form;
				$this->response($data, 200);

			} else {
				// Invalid ID / Contact form
				$data = [
					'success' => false,
					'message' => 'Invalid ID'
				];
				$this->response($data, 400);

			}
		} else {
			// Needs an ID
			$data = [
				'success' => false,
				'message' => 'Invalid ID'
			];
			$this->response($data, 400);
		}
	}

	/*
	 * Return customer groups
	 */
	public function groups_get()
	{
		session_write_close();

		$data['customer_groups'] = $this->Course->get_customer_groups($this->course_id);

		if ($data['customer_groups'] !== false) {
			$this->response($data, 200);
		} else {
			$data['customer_groups'] = array();
			$this->response($data, 200);
		}
	}

	/*
	 * Returns repl(y|ies) for the course
	 * If Form_ID argument is not provided, then load ALL replies for the current course
	 */
	public function reply_get($form_id = null)
	{
		session_write_close();

		if ($form_id === null) {
			// Get all for course
			$data['customer_replies'] = $this->get_all_course_replies($this->course_id);

			if ($data['customer_replies'] !== false) {
				$this->response($data, 200);
			} else {
				$data['customer_replies'] = array();
				$this->response($data, 200);
			}

		} else {
			// Get for specific contact form
			$form_id = (int)$form_id;

			// Verify Access to this form before retrieving replies
			$this->contact_form->id = $form_id;
			if (!$this->contact_form->retrieve() || $this->contact_form->course_id != $this->course_id) {
				// Unauthorized Course
				$data = [
					'success' => false,
					'message' => 'Not authorized for course replies'
				];
				$this->response($data, 401);
			}

			$data['customer_replies'] = $this->get_all_form_replies($form_id);

			if ($data['customer_replies'] !== false) {
				$this->response($data, 200);
			} else {
				$data['customer_replies'] = array();
				$this->response($data, 200);
			}

		}
	}

	/*
	 * Create a new customer reply
	 */
	public function reply_post()
	{
		session_write_close();

		$json_request_data = $this->request->body;

		$this->contact_form_reply->id = null;
		$this->contact_form_reply->contact_form_id = (int)$json_request_data['contact_form_id'];
		$customer_group_id = (int)$json_request_data['customer_group_id'];
		$this->contact_form_reply->customer_group_id = ($customer_group_id == false ? null : $customer_group_id);
		$this->contact_form_reply->person_id = null;
		$this->contact_form_reply->first_name = $json_request_data['first_name'];
		$this->contact_form_reply->last_name = $json_request_data['last_name'];
		$this->contact_form_reply->phone_number = $json_request_data['phone_number'];
		$this->contact_form_reply->email = $json_request_data['email'];
		$this->contact_form_reply->birthday = $json_request_data['birthday'];
		$this->contact_form_reply->address = $json_request_data['address'];
		$this->contact_form_reply->city = $json_request_data['city'];
		$this->contact_form_reply->state = $json_request_data['state'];
		$this->contact_form_reply->zip = $json_request_data['zip'];
		$this->contact_form_reply->message = $json_request_data['message'];

		if ($this->contact_form_reply->create()) {
			// Successfully Saved
			unset($data);
			$data['contact_form_reply'] = $this->contact_form_reply;
			$this->response($data, 200);
		} else {
			// Could not create new reply
			$data = [
				'success' => false,
				'message' => 'Could not save new contact form reply'
			];
			$this->response($data, 400);
		}
	}

	/*
	 * Retrieve the key for the current course
	 */
	public function key_get()
	{
		session_write_close();

		$this->contact_form_key->course_id = $this->course_id;
		if ($this->contact_form_key->retrieve()) {
			// Found the key!
			$data['contact_form_key'] = $this->contact_form_key;
			$this->response($data, 200);
		} else {
			// Could not find key for current course, generate one.

			$random_key = $this->course_id .'_'.$this->generate_random_string(10);
			$this->contact_form_key->course_id = $this->course_id;
			$this->contact_form_key->key = $random_key;

			if($this->contact_form_key->create()) {
				$data['contact_form_key'] = $this->contact_form_key;
				$this->response($data, 200);
			} else {
				// Could not create new key
				$data = [
					'success' => false,
					'message' => 'Could not create new key'
				];
				$this->response($data, 500);
			}
		}
	}

	/*
	 * Save the coarse's key (or update if exists)
	 * synonym for key_put
	 */
	private function key_post()
	{
		$this->key_put();
	}

	/*
	 * Update the coarse's key (or save new if non-existent)
	 */
	private function key_put()
	{
		session_write_close();

		$this->contact_form_key->course_id = $this->course_id;

		if ($this->contact_form_key->retrieve()) {
			// Update existing
			$json_request_data = $this->request->body;
			$this->contact_form_key->key = $json_request_data['key'];

			if ($this->contact_form_key->update()) {
				// Successful update
				$data['contact_form_key'] = $this->contact_form_key;
				$this->response($data, 200);
			} else {
				// Update failed
				$data = [
					'success' => false,
					'message' => 'Could not update contact form key'
				];
				$this->response($data, 400);
			}
		} else {
			// Create New
			$this->contact_form_key->key = $this->input->post('key');
			if ($this->contact_form_key->create()) {
				// Successful creation
				$data['contact_form_key'] = $this->contact_form_key;
				$this->response($data, 200);
			} else {
				// Creation failed
				$data = [
					'success' => false,
					'message' => 'Could not save new contact form key'
				];
				$this->response($data, 400);
			}
		}

	}

	/*
	 * Delete a coarse's key
	 */
	private function key_delete()
	{
		session_write_close();

		$this->contact_form_key->course_id = $this->course_id;

		if ($this->contact_form_key->retrieve()) {
			$this->contact_form_key->delete();
			$data['contact_form_key'] = $this->contact_form_key;
			$this->response($data, 200);
		} else {
			// Could not find an entry for the coarse
			$data = [
				'success' => false,
				'message' => 'Could not save new contact form reply'
			];
			$this->response($data, 400);
		}

	}

	/*
	 * Returns an array of contact_forms for a specific course
 	*/
	private function get_course_forms($course_id, $include_deleted = false)
	{
		$this->db->from('contact_forms');
		$this->db->where("course_id = '$course_id'");
		if ($include_deleted !== true) {
			$this->db->where("deleted_by is NULL");
		}
		$this->db->order_by("name", "asc");
		$results = $this->db->get();

		$contact_forms = array();

		// Load each result
		foreach ($results->result() as $row) {
			$contact_form = new contact_form();

			$contact_form->id = (int)$row->id;
			$contact_form->customer_group_id = ((int)$row->customer_group_id == false ? null : (int)$row->customer_group_id);
			$contact_form->course_id = (int)$row->course_id;
			$contact_form->created_by = (int)$row->created_by;
			$contact_form->created_at = $row->created_at;
			$contact_form->deleted_by = (int)$row->deleted_by;
			$contact_form->deleted_at = $row->deleted_at;
			$contact_form->name = $row->name;
			$contact_form->has_first_name = (bool)$row->has_first_name;
			$contact_form->has_last_name = (bool)$row->has_last_name;
			$contact_form->has_phone_number = (bool)$row->has_phone_number;
			$contact_form->has_email = (bool)$row->has_email;
			$contact_form->has_birthday = (bool)$row->has_birthday;
			$contact_form->has_address = (bool)$row->has_address;
			$contact_form->has_city = (bool)$row->has_city;
			$contact_form->has_state = (bool)$row->has_state;
			$contact_form->has_zip = (bool)$row->has_zip;
			$contact_form->has_message = (bool)$row->has_message;
			$contact_form->req_first_name = (bool)$row->req_first_name;
			$contact_form->req_last_name = (bool)$row->req_last_name;
			$contact_form->req_phone_number = (bool)$row->req_phone_number;
			$contact_form->req_email = (bool)$row->req_email;
			$contact_form->req_birthday = (bool)$row->req_birthday;
			$contact_form->req_address = (bool)$row->req_address;
			$contact_form->req_city = (bool)$row->req_city;
			$contact_form->req_state = (bool)$row->req_state;
			$contact_form->req_zip = (bool)$row->req_zip;
			$contact_form->req_message = (bool)$row->req_message;

			$contact_forms[] = $contact_form;
		}

		return $contact_forms;
	}

	/*
	 * Returns an array of contact_forms for a specific course
	 */
	private function get_all_form_replies($contact_form_id)
	{
		$this->db->from('contact_form_replies');
		$this->db->where("contact_form_id = $contact_form_id");
		$results = $this->db->get();
		$contact_form_replies = array();
		foreach ($results->result() as $row) {
			$contact_form_reply = new contact_form_reply();

			$contact_form_reply->id = (int)$row->id;
			$contact_form_reply->contact_form_id = (int)$row->contact_form_id;
			$contact_form_reply->customer_group_id = (int)$row->customer_group_id;
			$contact_form_reply->person_id = (int)$row->person_id;
			$contact_form_reply->completed_at = $row->completed_at;
			$contact_form_reply->ip_address = long2ip($row->ip_address);
			$contact_form_reply->first_name = $row->first_name;
			$contact_form_reply->last_name = $row->last_name;
			$contact_form_reply->phone_number = $row->phone_number;
			$contact_form_reply->email = $row->email;
			$contact_form_reply->birthday = $row->birthday;
			$contact_form_reply->address = $row->address;
			$contact_form_reply->city = $row->city;
			$contact_form_reply->state = $row->state;
			$contact_form_reply->zip = $row->zip;
			$contact_form_reply->message = $row->message;

			$contact_form_replies[] = $contact_form_reply;
		}
		return $contact_form_replies;
	}

	/*
	 * Returns an array of contact_forms for a specific course
	 */
	private function get_all_course_replies($course_id)
	{
		$contact_form_replies = array();

		$this->db->select('*');
		$this->db->from('contact_forms');
		$this->db->join('contact_form_replies', 'contact_form_replies.contact_form_id = contact_forms.id');
		$this->db->where("contact_forms.course_id = '$course_id'");
		$results = $this->db->get();

		foreach ($results->result() as $row) {
			$contact_form_reply = new contact_form_reply();

			$contact_form_reply->id = (int)$row->id;
			$contact_form_reply->contact_form_id = (int)$row->contact_form_id;
			$contact_form_reply->customer_group_id = (int)$row->customer_group_id;
			$contact_form_reply->person_id = (int)$row->person_id;
			$contact_form_reply->completed_at = $row->completed_at;
			$contact_form_reply->ip_address = long2ip($row->ip_address);
			$contact_form_reply->first_name = $row->first_name;
			$contact_form_reply->last_name = $row->last_name;
			$contact_form_reply->phone_number = $row->phone_number;
			$contact_form_reply->email = $row->email;
			$contact_form_reply->birthday = $row->birthday;
			$contact_form_reply->address = $row->address;
			$contact_form_reply->city = $row->city;
			$contact_form_reply->state = $row->state;
			$contact_form_reply->zip = $row->zip;
			$contact_form_reply->message = $row->message;

			$contact_form_replies[] = $contact_form_reply;
		}

		return $contact_form_replies;
	}

	/*
	 * Generate a random string
	 */
	private function generate_random_string($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

}
