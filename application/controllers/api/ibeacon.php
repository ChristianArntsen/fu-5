<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class ibeacon extends REST_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('ibeacon_model');
	}
	
	// Retrieve which course and terminal the beacon belongs to based 
	// on major and minor beacon ID
	private function get_course_location($major_id = null, $minor_id = null){
		/*
		if(empty($major_id)){
			$this->response(array('success' => false, 'msg' => 'Major ID is required', 'field' => 'major_id'), 400);
			die();
		}
		
		if(empty($minor_id)){
			$this->response(array('success' => false, 'msg' => 'Minor ID is required', 'field' => 'minor_id'), 400);
			die();
		}	*/	
		
		$course_id = $this->ibeacon_model->get_course_id($major_id);
		/*if(empty($course_id)){
			$this->response(array('success' => false, 'msg' => 'No location found with matching major ID', 'field' => 'major_id'), 400);
			die();
		} */
		$this->course_id = (int) $course_id;
		$this->session->set_userdata('course_id', $course_id);
		
		$terminal_id = (int) $this->ibeacon_model->get_terminal_id($course_id, $minor_id);
		/*if(empty($terminal_id)){
			$this->response(array('success' => false, 'msg' => 'No location found with matching minor ID', 'field' => 'minor_id'), 400);
			die();
		} */
		$this->terminal_id = (int) $terminal_id;
		$this->session->set_userdata('terminal_id', $terminal_id);	
		
		return true;
	}
	
	// Make sure iBeacon UUID matches ForeUp's set UUID
	private function check_uuid($uuid = null){
		
		if(empty($uuid)){
			$this->response(array('success' => false, 'msg' => 'iBeacon UUID is required', 'field' => 'ibeacon_uuid'), 400);
			die();			
		}
		/*
		if($uuid != $this->config->item('foreup_ibeacon_uuid')){
			$this->response(array('success' => false, 'msg' => 'Invalid ForeUp iBeacon UUID', 'field' => 'ibeacon_uuid'), 400);
			die();			
		}*/
		
		return true;
	}
	
	// Receives an iBeacon "ping" from a user's mobile device
    function index_post(){
		
		$this->load->model('user');
		$data = $this->request->body;

		// Validate beacon data
		$this->check_uuid($data['ibeacon_uuid']);
		$this->get_course_location($data['ibeacon_major'], $data['ibeacon_minor']);
        
        if(empty($data['user_id'])){
			$this->response(array('success' => false, 'msg' => 'User ID is required', 'field' => 'user_id'), 400);
			return false;		
		}
		
        if(!$this->ibeacon_model->user_exists($data['user_id'])){
			$this->response(array('success' => false, 'msg' => 'User ID is invalid', 'field' => 'user_id'), 400);
			return false;		
		}		
        
        // Save user's customer record to course (if it hasn't been already)
        $this->user->add_to_course($data['user_id'], $this->course_id);
        
        // Save the user's ibeacon ping
        $saved = $this->ibeacon_model->save($data['ibeacon_uuid'], $data['ibeacon_major'], 
			$data['ibeacon_minor'], $data['user_id'], $this->course_id, $this->terminal_id, '');
        
        if($saved){
			$this->response(array('success' => true), 201);
		}else{
			$this->response(array('success' => false), 500);
		}
    }
	
	// Register a new user from iBeacon enabled mobile app
	function users_post(){
		
		$this->load->model('customer');
		$this->load->model('person');
		$this->load->model('user');
		
		$first_name = $this->request->body['first_name'];	
		$last_name = $this->request->body['last_name'];	
		$phone = $this->request->body['phone'];

		$email = $this->request->body['email'];
		$password = $this->request->body['password'];
		
		if(empty($first_name)){
			$this->response(array('success' => false, 'msg' => 'First name is required', 'field' => 'first_name'), 400);
			return false;			
		}
		
		if(empty($last_name)){
			$this->response(array('success' => false, 'msg' => 'Last name is required', 'field' => 'last_name'), 400);
			return false;			
		}
		
		if(empty($phone)){
			$this->response(array('success' => false, 'msg' => 'Phone number is required', 'field' => 'phone'), 400);
			return false;			
		}
			
		if(empty($email)){
			$this->response(array('success' => false, 'msg' => 'Email is required', 'field' => 'email'), 400);
			return false;			
		}
		
		if(empty($password)){
			$this->response(array('success' => false, 'msg' => 'Password is required', 'field' => 'password'), 400);
			return false;			
		}
		
		if(strlen($password) < 6){
			$this->response(array('success' => false, 'msg' => 'Password must be at least 6 characters', 'field' => 'password'), 400);
			return false;			
		}		
		
		if($this->user->username_exists($email)){
			$this->response(array('success' => false, 'msg' => 'An account with that email already exists', 'field' => 'email'), 409);
			return false;
		}
		
		$person_data = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'phone_number' => $phone
		);
		
		$user_data = $person_data;
		$user_data['email'] = $email;
		$user_data['password'] = md5($password);
		
		// Create a new person record
		$this->person->save_person($person_data);
		$user_id = $person_data['person_id'];
		
		// Create a new user record linked to person record
		$this->user->save($user_id, $user_data);
		
		if($user_id){
			$this->response(array('success' => true, 'user_id' => (int) $user_id), 201);	
		
		}else{
			$this->response(array('success' => false, 'msg' => 'Error saving user'), 500);	
		}	
	}

	// Returns an existing user's ID for logging in to iBeacon 
	// enabled mobile app
	function authenticate_post(){
		
		$this->load->model('user');
		$data = $this->request->body;
		
		if(empty($data['email'])){
			$this->response(array('success' => false, 'msg' => 'Email is required', 'field' => 'email'), 400);
			return false;				
		}
		
		if(empty($data['password'])){
			$this->response(array('success' => false, 'msg' => 'Password is required', 'field' => 'password'), 400);
			return false;				
		}
		
		$user_id = $this->ibeacon_model->get_user_id($data['email'], $data['password']);
		
		if($user_id){
			$this->response(array('success' => true, 'user_id' => (int) $user_id), 200);
		}else{
			$this->response(array('success' => false, 'msg' => 'Email/password did not match'), 400);				
		}					
	}
}
