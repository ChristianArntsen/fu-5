<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Giftcards extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();
		
		$this->load->model('v2/Giftcard_model');
	}

	function index_get($id = null){
		
		$response = false;
		$number = $this->input->get('giftcard_number');
		$response = $this->Giftcard_model->get(array('giftcard_number' => $number));
		
		$this->response($response, 200);
	}
}
