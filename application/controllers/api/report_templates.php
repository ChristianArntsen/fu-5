<?php
class Report_Templates extends CI_Controller {

    function __construct(){

        parent::__construct();
        session_write_close();
        $this->load->model('Report');
        $this->CI = & get_instance();
        $this->sale_id = false;
    }

    public function create(){
	    $json = file_get_contents('php://input');
        $json = json_decode($json);
        if(isset($json->chart_showing))
            unset($json->chart_showing);
	    $id = $this->Report->save($json);

	    print json_encode(["id"=>$id]);
    }

    public function show($id){

        $report = $this->Report->get_info($id);
        print json_encode($report);
    }

    public function index(){
        $reports = $this->Report->get_all();

        print json_encode($reports);
    }

    public function update($id){
	    $json = file_get_contents('php://input');
	    $json = json_decode($json);

        unset($json->split_by);
        unset($json->chart_data);
	    $success = $this->Report->update($json,$id);

	    print json_encode($success);
    }

    public function delete($id){
        echo "delete";
    }

}
