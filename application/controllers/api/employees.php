<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Employees extends REST_Controller {
	
	function __construct(){
		parent::__construct();
		$this->authenticate();

		$this->load->model('v2/employee_model');
		$this->load->model('timeclock_entry');
	}
	
	// Get list of employees filtered by a variety of parameters
	function index_get($employee_id = null, $method = null){

		$params = $this->input->get();
		$employees = $this->employee_model->get($params);

		$this->response($employees, 200);
	}
	
	function index_post($employee_id = null, $method = null){
		if($method == 'clock_in'){
			return $this->clock_in_post($employee_id);
		}else if($method == 'clock_out'){
			return $this->clock_out_post($employee_id);
		}		
	}		
}
