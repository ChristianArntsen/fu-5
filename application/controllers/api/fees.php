<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Fees extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();

		$this->load->model('Pricing');
	}

	// Get list of items filtered by a variety of parameters
	function index_get($method = null){
		
		session_write_close();
		
		$teesheet_id = $this->input->get('teesheet_id');
		$price_class_id = $this->input->get('price_class_id');
		$datetime = $this->input->get('date');

		if(empty($teesheet_id)){
			$this->response(array('msg' => 'teesheet_id required'), 400);
			return false;
		}

		if(empty($price_class_id)){
			$this->response(array('msg' => 'price_class_id required'), 400);
			return false;
		}

		if(empty($datetime)){
			$datetime = date('Y-m-d H:i:s');
		}

		$date = date('Y-m-d', strtotime($datetime));
		$time = date('Hi', strtotime($datetime));

		$timeframe = $this->Pricing->get_price_timeframe(
			$teesheet_id, 
			$price_class_id, 
			$date, 
			$time
		);

		if(!empty($timeframe)){
			$this->response(['timeframe_id' => $timeframe['timeframe_id']], 200);			
		
		}else{
			$this->response([], 200);
		}

	}
}