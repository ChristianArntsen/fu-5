<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Rainchecks extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();

		$this->load->model('v2/Raincheck_model');
	}

	function index_get($id = null){
		
		$response = false;
		$number = $this->input->get('number');
		$response = $this->Raincheck_model->get(array('number' => $number));
		
		if(count($response) == 1){
			$response = $response[0];
		}
		
		if($response){
			$response['success'] = true;
			$this->response($response, 200);
		}else{
			$this->response(null, 404);
		}
	}

	function index_post(){
		return $this->index_put();
	}

	function index_put(){

		$data = $this->request->body;
		$response = $this->Raincheck_model->save(false, $data);
		
		if($response){
			$this->response(array('success' => true, 'raincheck_id' => $response['raincheck_id'], 'raincheck_number' => $response['raincheck_number']));
		}else{
			$this->response(array('success' => false), 400);
		}
	}
}
