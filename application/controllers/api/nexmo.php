<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH . 'libraries/REST_Controller.php');

class Nexmo extends REST_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Marketing_Texting');
	}

	function subscriptions_get()
	{
		$this->db->insert("nexmo_logs_incoming",[
			"dump"=>json_encode($this->_get_args)
		]);

		$msisdn = $this->get('msisdn');
		$text = $this->get('text');
		$to = $this->get('to');
		$isShortCode = true;
		if(strlen($to) >= 10){
			$isShortCode = false;
			$virtual_number = $this->db->from("course_virtual_numbers")
				->where("virtual_number",$to)
				->get()->row();

			$this->load->model("Course");
			$course_info = $this->Course->get_info($virtual_number->course_id);
			$nexmo = new \fu\nexmo\Nexmo($course_info,$this->db,$this->config->item('api_key', 'nexmo'),$this->config->item('api_secret', 'nexmo'));
			if(empty(!$isShortCode)){
				return $this->response(array('success'), 200);
			}
		}
		$keyword = strtoupper($this->get('keyword'));
		if (strcmp('STOP', strtoupper($text)) == 0) {
            $texting_status = 'unsubscribed';
			if($isShortCode){
				$this->Marketing_Texting->updateSubscriptionByMsisdn($msisdn, $keyword, $texting_status);
			} else {
				$reply = "You have been successfully unsubscribed and will receive no more messages. support@foreup.com.";
				$nexmo->send_message($msisdn,$reply);
				$this->Marketing_Texting->updateSubscriptionByMsisdn($msisdn, "GOLF", $texting_status,$virtual_number->course_id);
			}
		} else if($keyword == "GOLF") {
			$texting_status = 'subscribed';
			if($isShortCode){
				$this->Marketing_Texting->updateSubscriptionByMsisdn($msisdn, $keyword, $texting_status);
			} else {
				$reply = "Thanks for subscribing. < 10 msgs/mo. Reply HELP for info.  Reply STOP to cancel. Msg&Data rates may apply.";
				$nexmo->send_message($msisdn,$reply);
				$this->Marketing_Texting->updateSubscriptionByMsisdn($msisdn, $keyword, $texting_status,$virtual_number->course_id);
			}
        } else if($keyword == "HELP" && !$isShortCode){
			$reply = "foreUP SMS Marketing support@foreup.com < 10 msgs/mo. Reply STOP to cancel. Msg&Data Rates May Apply.";
			$nexmo->send_message($msisdn,$reply);
		} else if(!$isShortCode && !empty($text)){
        	//Forward message to the course via their virtual number
			$this->load->model("Personal_messages");
			/** Personal_messages $this->Personal_messages */
			$this->Personal_messages->set_course_id($virtual_number->course_id);
			$this->Personal_messages->save_message($text,$msisdn,1);
		}
        $this->response(array('success'), 200);
	}
}
