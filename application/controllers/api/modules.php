<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Modules extends REST_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('Module');
	}

	// Get list of employees filtered by a variety of parameters
	function index_get(){

		$modules = $this->Module->get_course_modules();

		foreach($modules as &$module)
		{
			$module['name'] = $this->Module->get_module_name($module['module_id']);
			$module['description'] = $this->Module->get_module_desc($module['module_id']);
			unset($module['name_lang_key']);
			unset($module['desc_lang_key']);
		}

		$this->response($modules, 200);
	}
}
