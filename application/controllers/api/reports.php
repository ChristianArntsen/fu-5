<?php
class Reports extends CI_Controller {

    function __construct(){
	    parent::__construct();
	    $this->load->model('Report');
	    $this->CI = & get_instance();
	    $this->sale_id = false;
    }

    public function create(){

    }

    public function show($report_name){



        try {
            $s3 = new \fu\aws\s3();
            $result = $s3->getObject("foreup.application.reports",$report_name);
            // Display the object in the browser
            //header("Content-Type: {$result['ContentType']}",false);

	        $this->load->model("report_exports");
	        $report_exports = new Report_Exports();
	        $reportExport = $report_exports->get_info($report_name);

	        $filename = !empty($reportExport->human_readable_name)?$reportExport->human_readable_name .".". pathinfo($reportExport->name, PATHINFO_EXTENSION):$report_name;

	        header("Content-type: application/octet-stream");
	        header("Content-Disposition: attachment; filename=\"".$filename."\"");
            echo $result['Body'];

	        $report_exports->mark_as_downloaded($report_name);
        } catch (S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    public function index(){
	    $json = file_get_contents('php://input');


	    $reports = $this->CI->Report->get_all();
	    print json_encode($reports);
    }

    public function update($id){
        echo "update";
    }

    public function delete($id){
        echo "delete";
    }

}
