<?php
class Columns extends MY_Controller {

    function __construct(){

        parent::__construct();
        $this->load->model('Report');
        $this->CI = & get_instance();
        $this->sale_id = false;
    }

    public function create($report_id){
        $json = file_get_contents('php://input');


        $input = json_decode($json,true);
        $id = $this->CI->Report->addReportColumn($input);
        print json_encode(["id"=>$id]);
    }

    public function show($report_id,$id){
        $column =$this->CI->Report->getReportColumn($report_id,$id);
        print json_encode($column);
    }

    public function index($report_id){
        $columns =$this->CI->Report->getReportColumns($report_id);
        print json_encode($columns);
    }

    public function update($report_id,$column_id){
        $json = file_get_contents('php://input');
        $input = json_decode($json,true);
        $result = $this->CI->Report->editReportColumn($report_id,$column_id,$input);
        print json_encode(["result"=>$result]);
    }

    public function delete($report_id,$id){
        $success =$this->CI->Report->removeReportColumn($id);

        print json_encode(["success"=>$success]);
    }

}
