<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Data_import extends REST_Controller {

    function __construct(){

        parent::__construct();
        $this->authenticate();
        $this->load->model('v2/Import_job_model');

        session_write_close();
    }

    function index_get($id = null){
        $data = $this->Import_job_model->get(['id' => $id]);
        if(!empty($id)){
            $data = $data[0];
        }
        return $this->response($data);
    }

    function index_put($job_id = null, $method = null){

        $data = $this->request->body;
        $code = 200;
        if($job_id === null){
            $code = 201;
        }

        $job_id = $this->Import_job_model->save($job_id, $data);

        if($data['status'] == 'ready'){
            $this->Import_job_model->start_job($job_id);
        }

        // Test response for finishing the front end
        return $this->response(['id' => $job_id], $code);
    }

    function index_delete($job_id = null){

        $this->Import_job_model->delete($job_id);

        // Test response for finishing the front end
        return $this->response(['id' => $job_id], 200);
    }

    function index_post($job_id = null, $method = null){

        if($job_id == 'upload' || $method == 'upload'){
            if($job_id == 'upload'){
                $job_id = null;
            }
            return $this->upload_post($job_id);
        }

        return $this->index_put($job_id, $method);
    }

    // Upload CSV to S3 and create new import jobto S3 and save the path
    function upload_post($job_id = null){

        $upload_dir = './uploads/data_import';
        @mkdir($upload_dir, 0777);

        $this->load->library('upload');
        $this->upload->initialize([
            'file_name' => 'customers_'.md5(time()),
            'upload_path' => $upload_dir,
            'allowed_types' => 'txt|csv|xls|xlsx'
        ]);

        if(!$this->upload->do_upload('file')){
            $error = $this->upload->display_errors();

            if(stristr($error, 'maximum allowed size') !== false){
                $error = 'File is too large';
            }
            return $this->response(['msg' => $error], 400);
        }

        $file_data = $this->upload->data();
        $filename = $file_data['file_name'];

        $s3_path = $this->session->userdata('course_id') .'/'. $filename;
        if(ENVIRONMENT != 'production') {
            $s3_path = 'dev_' . $s3_path;
        }
        $local_path = $upload_dir .'/'. $filename;

        $s3 = new fu\aws\s3();
        $s3->uploadToDataImportBucket($local_path, $s3_path);

        unlink($local_path);

        // Create a new import job with the uploaded file
        $s3_location = $s3->dataImportBucket.':'.$s3_path;
        $job_id = $this->Import_job_model->save(null, ['source_path' => $s3_location, 'type' => 'customers', 'status' => 'pending']);

        return $this->response(['id' => $job_id], 201);
    }
}