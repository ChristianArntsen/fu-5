<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Teesheets extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();

	}

	function index_get(){

		$data = $this->input->get();
		$this->load->model('v2/Teesheet_model');
		$teesheets = $this->Teesheet_model->get($data);

		$this->response($teesheets, 200);
	}

	function recipients_get(){
		$data = $this->input->get();
		$start = \Carbon\Carbon::parse($data['start']);
		$end = \Carbon\Carbon::parse($data['end']);
		$course_ids = array();

		$this->load->model("course");
		$this->course->get_linked_course_ids($course_ids, 'shared_customers');
		$people_ids = [];
		$courses_string = implode(",",$course_ids);

		$teesheets = $this->db->from("teesheet")
			->select("teesheet_id")
			->where("deleted",0)
			->where("course_id",$this->session->userdata('course_id'))
			->get()->result_array();
		$teesheet_ids = [];
		foreach($teesheets as $teesheet){
			$teesheet_ids[] = $teesheet['teesheet_id'];
		}
		$teesheet_ids = implode(",",$teesheet_ids);

		$result = $this->db->query("
			SELECT t.TTID,t.person_id p1,t.person_id_2 p2,t.person_id_3 p3,t.person_id_4 p4,t.person_id_5 p5,t.start_datetime FROM foreup_teetime t

			WHERE
			t.teesheet_id IN ($teesheet_ids) AND
			status != 'deleted' and
			reround != 1 AND
			t.`start_datetime` >= '{$start->toDateTimeString()}' AND t.end_datetime <= '{$end->toDateTimeString()}'
		")->result_array();
		foreach($result as $row){
			if(isset($row['p1']))
				$people_ids[$row['p1']] = $row['start_datetime'];
			if(isset($row['p2']))
				$people_ids[$row['p2']] = $row['start_datetime'];
			if(isset($row['p3']))
				$people_ids[$row['p3']] = $row['start_datetime'];
			if(isset($row['p4']))
				$people_ids[$row['p4']] = $row['start_datetime'];
			if(isset($row['p5']))
				$people_ids[$row['p5']] = $row['start_datetime'];
		}
		if(empty($people_ids)){

			$this->response([], 200);
			return [];
		}


		$result = $this->db->query("
			SELECT p.person_id p1,t.start_datetime FROM foreup_teetime  t
			RIGHT JOIN foreup_event_people ep ON t.TTID  = ep.teetime_id
			LEFT JOIN foreup_people p ON ep.person_id = p.person_id
			WHERE
			t.teesheet_id IN ($teesheet_ids) AND
			status != 'deleted' and
			reround != 1 AND
			t.`start_datetime` >= '{$start->toDateTimeString()}' AND t.end_datetime <= '{$end->toDateTimeString()}'
		")->result_array();
		foreach($result as $row){
			if(isset($row['p1']))
				$people_ids[$row['p1']] = $row['start_datetime'];
		}

		$results = $this->db->from("people")
				->select(["people.person_id","first_name","last_name","email","cell_phone_number","phone_number","opt_out_text","opt_out_email","texting_status","msisdn"])
				->join("customers","people.person_id = customers.person_id","left")
				->join("marketing_texting","people.person_id = marketing_texting.person_id","left")
				->where_in("customers.course_id",$course_ids)
				->where_in("people.person_id",array_keys($people_ids))
				->group_by("person_id")->get()->result_array();
		foreach($results as &$result){
			$result['start_datetime'] = $people_ids[$result['person_id']];
		}


		$this->response($results, 200);
	}
}