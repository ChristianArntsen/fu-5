<?php
//This controller is for the purpose of running cron tasks in the system

class Payment extends CI_Controller//REST_Controller
{

    function __construct()
    {
    parent::__construct('payment');
    }

    function success()
    {

    }

    function fail()
    {

    }
		
	function mobile_ets_pay_window()
	{
		//$session_id = $this->input->get('session_id');
		$session = new stdClass();
		$course_id = $this->input->get('course_id');
		$ets_key = $this->input->get('ets_key');
		$person_id = $this->input->get('person_id');
		$session->id = $this->input->get('id');
		$session->status = $this->input->get('status');
		$session->message = $this->input->get('message');
		$session->customers = $this->input->get('customers');
		$amount = $this->input->get('amount');
		if ($session->id)
		{
			$user_message = '';//$previous_card_declined!='false'?'Card declined, please try another.':'';
			$return_code = '';
			//$this->session->set_userdata('ets_session_id', (string)$session->id);
			//$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
			$data = array('mobile'=>true, 'user_message'=>$user_message, 'return_code'=>$return_code, 'session'=>$session, 'url'=>site_url("api/payment/ets_payment_made/1?ets_key=$ets_key&course_id=$course_id&person_id=$person_id"), 'amount'=>$amount);
			$this->load->view('sales/ets', $data);
		}
		else
		{
			$data = array('processor' => 'ETS');
			$this->load->view('sales/cant_load', $data);
		}
	}
	
	function ets_payment_made($mobile = false) {
		$ets_key = $this->input->get('ets_key');
		$course_id = $this->input->get('course_id');
		$operator_id = $this->input->get('person_id');
		
		$this->load->model('sale');
		$response = $this->input->post('response');
		$ets_response = json_decode($response);
		
		// VERIFYING A POSTed TRANSACTION
		$this->load->library('Hosted_payments');
		$payment = new Hosted_payments();
		$payment->initialize($this->config->item('ets_key'));
		$session_id = $payment->get("session_id");

		$transaction_id = $ets_response->transactions->id;

		$payment->set("action", "verify")
			->set("sessionID", $ets_response->id)
			->set("transactionID", $transaction_id);

		$account_id = $ets_response->customers->id;
		if ($account_id)
			$payment->set('accountID', $account_id);

		$verify = $payment->send();
		
		$transaction_time = date('Y-m-d H:i:s', strtotime($ets_response->created));

		// Convert card type to match mercury card types
		if ((string)$ets_response->transactions->type == 'credit card') {
			$ets_card_type = $ets_response->transactions->cardType;
			$card_type = '';
			switch($ets_card_type){
				case 'MasterCard':
					$card_type = 'M/C';
				break;
				case 'Visa':
					$card_type = 'VISA';
				break;
				case 'Discover':
					$card_type = 'DCVR';
				break;
				case 'AmericanExpress':
					$card_type = 'AMEX';
				break;
				case 'Diners':
					$card_type = 'DINERS';
				break;
				case 'JCB':
					$card_type = 'JCB';
				break;
			}
			$masked_account = (string) $ets_response->transactions->cardNumber;
			$auth_code = (string) $ets_response->transactions->approvalCode;
		}
		else //if ((string)$ets_response->transactions->type == 'bank account')
		{
			$card_type = 'Bank Acct';
			$masked_account = (string) $verify->transactions->accountNumber;
			$auth_code = '';
		}

		//Update credit card payment data
		$trans_message = (string) $ets_response->transactions->message;
		$payment_data = array (
			'course_id' 		=> $course_id,
			'masked_account' 	=> $masked_account,
			'trans_post_time' 	=> (string) $transaction_time,
			'ets_id' 			=> $ets_key,
			'tran_type' 		=> 'Sale',
			'amount' 			=> (string) $ets_response->transactions->amount,
			'auth_amount'		=> (string)	$ets_response->transactions->amount,
			'auth_code'			=> $auth_code,
			'card_type' 		=> $card_type,
			'frequency' 		=> 'OneTime',
			'payment_id' 		=> (string) $ets_response->transactions->id,
			'process_data'		=> (string) $ets_response->id,
			'status'			=> (string) $ets_response->transactions->status,
			'status_message'	=> (string) $ets_response->transactions->message,
			'display_message'	=> (string) $ets_response->message,
			'operator_id'		=> (string) $operator_id
		);

		//$invoice_id = $this->sale->add_credit_card_payment($payment_data);
		$primary_Store = (string) $ets_response->store->primary;
		$this->sale->update_credit_card_payment($primary_Store, $payment_data);
		$payment_data['payment_type'] = $payment_data['card_type'].' '.$payment_data['masked_account'];

		if($ets_response->status == "success"){
			$payment_data['mobile'] = $mobile;
			$this->load->view('sales/ets_payment_made', $payment_data);
		}else{
			$this->load->view('sales/ets_payment_made', array('status'=>'declined', 'mobile'=>$mobile));
		}
	}
		
}

?>