<?php
require_once(APPPATH.'libraries/REST_Controller.php');

class Timeclock extends REST_Controller {

	function __construct(){
		parent::__construct();
		$this->authenticate();
		
		$this->load->model('v2/Employee_model');
		$this->load->model('timeclock_entry');
	}

	function index_get($method = null){
		
		$params = $this->input->get();
		$params['limit'] = min(1000, (int) $params['limit']);

		$entries = $this->timeclock_entry->get_all($params['limit'], (int) $params['employee_id']);
		$this->response($entries->result_array(), 200);
	}

	function index_post($method = null, $record_id = null){

		if($method == 'clock_in'){
			return $this->clock_in_post();
		}else if($method == 'clock_out'){
			return $this->clock_out_post();
		}
	}

	function index_put($method = null, $record_id = null){
		if($method == 'clock_in'){
			return $this->clock_in_post();
		}else if($method == 'clock_out'){
			return $this->clock_out_post();
		}
	}
	
	function clock_in_post(){
		
		$password = $this->request->body['password'];
		$terminal_id = (int) $this->request->body['terminal_id'];
		$employee_id = $this->request->body['employee_id'];
		$authenticated = $this->Employee_model->authenticate($employee_id, 0, $password);
		
		if(!$authenticated){
			$this->response(array('success' => false, 'msg' => 'Password is incorrect'), 400);
			return false;
		}
		
		$clocked_in = $this->timeclock_entry->is_clocked_in($employee_id);
		if($clocked_in){
			$this->response(array('success' => false, 'msg' => 'Already clocked in'), 400);
			return false;			
		}

		$role_id = null;
        $hourly_rate = 0.00;

		$this->load->model('acl_roles');
        $roles = $this->acl_roles->get_acl_roles(['employee_id' => $employee_id]);
        if(!empty($roles[0])){
            $role_id = $roles[0]['role_id'];
            $hourly_rate = $roles[0]['hourly_rate'];
        }

		$this->timeclock_entry->clock_in($employee_id, $terminal_id, $role_id, $hourly_rate);
		$entry = $this->timeclock_entry->get_last_entry($employee_id);
		$entry['success'] = true;
		
		$this->response($entry, 201);
	}
	
	function clock_out_post(){
		
		$password = $this->input->post('password');
		$employee_id = $this->input->post('employee_id');
		$authenticated = $this->Employee_model->authenticate($employee_id, 0, $password);
		
		if(!$authenticated){
			$this->response(array('success' => false, 'msg' => 'Password is incorrect'), 400);
			return false;
		}
		
		$clocked_in = $this->timeclock_entry->is_clocked_in($employee_id);
		if(!$clocked_in){
			$this->response(array('success' => false, 'msg' => 'Must be clocked in first'), 400);
			return false;			
		}
		
		$this->timeclock_entry->clock_out($employee_id);
		$entry = $this->timeclock_entry->get_last_entry($employee_id);
		$entry['success'] = true;
		
		$this->response($entry, 200);		
	}	
}
