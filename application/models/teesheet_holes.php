<?php
class Teesheet_holes extends CI_Model
{
    public function get_all($tee_sheet_id) {
        $this->db->from('teesheet_holes');
        $this->db->where('tee_sheet_id', $tee_sheet_id);
        return $this->db->get()->result_array();
    }

    public function delete_all($tee_sheet_id) {
        return $this->db->delete('teesheet_holes', array('tee_sheet_id' => $tee_sheet_id));
    }

    public function save($data) {
        return $this->db->insert('teesheet_holes', $data);
    }

}