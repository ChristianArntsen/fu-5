<?php
class Refund_reason extends CI_Model
{
    function get_all() {
        $this->db->from('refund_reasons');
        $this->db->where('course_id', $this->config->item('course_id'));
        return $this->db->get()->result_array();
    }

    function save($data, $reason_id = false) {
        if (!$reason_id) {
            $data = array(
                'course_id' => $this->config->item('course_id'),
                'label' => $data['label']
            );
            $success = $this->db->insert('refund_reasons', $data);
            $reason_id = $this->db->insert_id();
        }
        else {
            $data = array(
                'label' => $data['label']
            );
            $this->db->where('reason_id', $reason_id);
            $this->db->where('course_id', $this->config->item('course_id'));
            $success = $this->db->update('refund_reasons', $data);
        }

        return array('success'=>$success, 'reason_id'=>$reason_id);
    }

    function remove($reason_id) {
        $this->db->where('course_id', $this->config->item('course_id'));
        $this->db->where('reason_id', $reason_id);
        $success = $this->db->delete('refund_reasons');

        return array('success'=>$success, 'reason_id' => $reason_id);
    }

}