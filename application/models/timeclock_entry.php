<?php
class Timeclock_entry extends CI_Model
{
	function is_clocked_in($employee_id = false) {
		$employee_id = $employee_id ? $employee_id : $this->session->userdata('person_id');
		$this->db->from('timeclock_entries');
		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_start !=','0000-00-00 00:00:00' );
		$this->db->where('shift_end =','0000-00-00 00:00:00' );
		$this->db->limit(1);
		$this->db->order_by('shift_start', 'desc');
		
		$last_login = $this->db->get()->result_array();
		if (isset($last_login[0]) && $last_login[0]['shift_end'] == '0000-00-00 00:00:00')
			return true;
		
		return false;
	}
	function get_info($employee_id, $start_time)
	{
		$this->db->from('timeclock_entries');
		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_start', $start_time);
		$this->db->limit(1);
		
		return $this->db->get()->result_array();
	}
	function clock_in($employee_id = false, $terminal_id = false, $role_id = null, $hourly_rate = 0) {
		$employee_id = $employee_id ? $employee_id : $this->session->userdata('person_id');
		$this->db->insert('timeclock_entries', array(
			'employee_id'=>$employee_id,
			'terminal_id'=>$terminal_id,
			'shift_start'=>date('Y-m-d H:i:s'),
			'role_id' => $role_id,
			'hourly_rate' => $hourly_rate
		));
	}
	
	function clock_out($employee_id = false) {
		$employee_id = $employee_id ? $employee_id : $this->session->userdata('person_id');

		$shift_end = date('Y-m-d H:i:s');

		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_end', '0000-00-00 00:00:00');
		$this->db->update('timeclock_entries', array('shift_end'=>$shift_end));

		$this->calculate_total_cost($employee_id, $shift_end);
	}

	function calculate_total_cost($employee_id, $shift_end){

		$this->db->select('hourly_rate, shift_start, shift_end');
		$this->db->from('timeclock_entries');
		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_end', $shift_end);
		$row = $this->db->get()->row_array();

		if(empty($row)){
			return false;
		}
		if(empty($row['shift_start']) || $row['shift_start'] == '0000-00-00 00:00:00'){
			return false;
		}
		if(empty($row['shift_end']) || $row['shift_end'] == '0000-00-00 00:00:00'){
			return false;
		}
		if(empty($row['hourly_rate'])){
			$row['hourly_rate'] = 0.00;
		}

		$start = \Carbon\Carbon::parse($row['shift_start']);
		$end = \Carbon\Carbon::parse($row['shift_end']);
		$minutes = $end->diffInMinutes($start);

		if(empty($minutes)){
			$hours = 0.00;
		}else{
			$hours = (float) round($minutes / 60, 4);
		}
		$total_cost = (float) round($hours * $row['hourly_rate'], 2);

		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_end', $shift_end);
		$this->db->limit(1);
		$this->db->update('timeclock_entries', [
			'total_cost' => $total_cost,
			'total_hours' => $hours
		]);

		return $this->db->affected_rows();
	}

	function save($employee_id, $start_time) {
		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_start', $start_time);

		$data = [];
		if(!empty($this->input->post('shift_start'))){
			$data['shift_start'] = $this->input->post('shift_start');
		}
		if(!empty($this->input->post('shift_end'))){
			$data['shift_end'] = $this->input->post('shift_end');
		}
		if(!empty($this->input->post('terminal_id'))){
			$data['terminal_id'] = $this->input->post('terminal_id');
		}

		$this->db->update('timeclock_entries', $data);


		$this->calculate_total_cost($employee_id,$this->input->post('shift_end'));
	}
	function delete($employee_id, $start_time) {
		$this->db->where('employee_id', $employee_id);
		$this->db->where('shift_start', $start_time);
		$this->db->delete('timeclock_entries');
	}
	function get_all($limit = 1000, $employee_id = false, $start = '1969-12-31', $end = '3000-12-31') {
		$start = date("Y-m-d", strtotime($start)).' 00:00:00';
		$end = date("Y-m-d", strtotime($end)).' 23:59:59';
		$employee_id = ($employee_id ? $employee_id : $this->session->userdata('person_id'));
		$results = $this->db->query("SELECT `shift_start` , `shift_end` , `foreup_timeclock_entries`.`terminal_id`, CASE WHEN shift_end != '0000-00-00 00:00:00' THEN TIMEDIFF( shift_end, shift_start ) ELSE 0 END AS total, label AS terminal FROM (`foreup_timeclock_entries`) LEFT JOIN foreup_terminals ON foreup_timeclock_entries.terminal_id = foreup_terminals.terminal_id WHERE employee_id = '$employee_id' AND shift_start > '$start' AND shift_start < '$end' ORDER BY  `shift_start` DESC LIMIT $limit");
		return $results;
	}
	
	function get_last_entry($employee_id){
		$this->db->select("entry.*, terminal.label AS terminal, IF(entry.shift_end != '0000-00-00 00:00:00', TIMEDIFF(entry.shift_end, entry.shift_start), 0) AS total", false);
		$this->db->from('timeclock_entries AS entry');
		$this->db->join('terminals AS terminal', 'terminal.terminal_id = entry.terminal_id', 'left');
		$this->db->where('entry.employee_id', $employee_id);
		$this->db->order_by('entry.shift_start DESC');
		$this->db->limit(1);
		
		return $this->db->get()->row_array();		
	}
}
?>
