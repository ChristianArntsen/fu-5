<?php

// BEST PRACTICES:
//
// TODO: Clean up and move DB result parsing to separate method (DRY)
// TODO: Require new objects of self to be initialized by constructor only
//       any initialized object should be valid, ideally.
// TODO: Move methods not working with single self (this) object to controller
// TODO: Move small small validation methods (and UTC Time Generator) to a common library
//

class contact_form extends CI_Model
{
	public $id = null;
	public $course_id;
	public $customer_group_id = null;

	public $created_by;
	public $created_at;
	public $deleted_at = null;
	public $deleted_by = null; // Soft delete checks are based on this column

	public $name;

	public $has_first_name;
	public $has_last_name;
	public $has_phone_number;
	public $has_email;
	public $has_birthday;
	public $has_address;
	public $has_city;
	public $has_state;
	public $has_zip;
	public $has_message;

	public $req_first_name;
	public $req_last_name;
	public $req_phone_number;
	public $req_email;
	public $req_birthday;
	public $req_address;
	public $req_city;
	public $req_state;
	public $req_zip;
	public $req_message;

	/*
	 * Loads data into this object. Returns true / false on success
	 */
	public function retrieve($include_deleted = false)
	{
		// Validate ID
		if(!$this->validate_row_id($this->id)) {
			return false;
		}

		$this->db->from('contact_forms');
		$this->db->where("id = '$this->id'");
		if($include_deleted !== true){
			$this->db->where("deleted_by is NULL");
		}

		$result = $this->db->get();
		if($result->num_rows() > 0){
			// Load in the data
			$row = $result->first_row();
			$this->id = (int)$row->id;
			$this->course_id = (int)$row->course_id;
			$this->customer_group_id = ((int)$row->customer_group_id == false ? null : (int)$row->customer_group_id);
			$this->created_by = (int)$row->created_by;
			$this->created_at = $row->created_at;
			$this->deleted_by = ((int)$row->deleted_by == false ? null : (int)$row->deleted_by);
			$this->deleted_at = $row->deleted_at;
			$this->name = $row->name;
			$this->has_first_name = (bool)$row->has_first_name;
			$this->has_last_name = (bool)$row->has_last_name;
			$this->has_phone_number = (bool)$row->has_phone_number;
			$this->has_email = (bool)$row->has_email;
			$this->has_birthday = (bool)$row->has_birthday;
			$this->has_address = (bool)$row->has_address;
			$this->has_city = (bool)$row->has_city;
			$this->has_state = (bool)$row->has_state;
			$this->has_zip = (bool)$row->has_zip;
			$this->has_message = (bool)$row->has_message;
			$this->req_first_name = (bool)$row->req_first_name;
			$this->req_last_name = (bool)$row->req_last_name;
			$this->req_phone_number = (bool)$row->req_phone_number;
			$this->req_email = (bool)$row->req_email;
			$this->req_birthday = (bool)$row->req_birthday;
			$this->req_address = (bool)$row->req_address;
			$this->req_city = (bool)$row->req_city;
			$this->req_state = (bool)$row->req_state;
			$this->req_zip = (bool)$row->req_zip;
			$this->req_message = (bool)$row->req_message;

			return true;
		} else {
			return false;
		}
	}

	/*
	 * Soft deletes an entry
	 */
	public function delete(){
		// Validate User ID
		if(!$this->validate_row_id($this->deleted_by)) {
			return false;
		}

		// Validate ID
		if(!$this->validate_row_id($this->id)) {
			return false;
		}

		$this->deleted_at = $this->get_utc_datetime();

		// Attempt soft delete
		$query = "UPDATE `foreup_contact_forms`
				  SET
				  `deleted_by`='$this->deleted_by',
				  `deleted_at`='$this->deleted_at'
				  WHERE `id`='$this->id'
				  ";

		return $this->db->simple_query($query);
	}

	/*
	 * Soft (un)deletes an entry
	 */
	public function undelete(){
		// Validate ID
		if(!$this->validate_row_id($this->id)) {
			return false;
		}

		$this->deleted_at = null;
		$this->deleted_by = null;

		// Attempt soft (un)delete
		$query = "UPDATE `foreup_contact_forms`
				  SET
				  `deleted_by`=NULL,
				  `deleted_at`=NULL
				  WHERE `id`='$this->id'
				  ";

		return $this->db->simple_query($query);
	}

	/*
	 * Check if a retrieved entry was deleted
	 */
	public function was_deleted(){
		if($this->deleted_by !== NULL && $this->deleted_by > 0){
			return true;
		} else {
			return false;
		}
	}

	/*
	 * Persists object into database storage. Returns true / false on success
 	*/
	public function create()
	{
		// Validate Self
		if(!$this->validate()){
			return false;
		}

		// Escape input
		$this->name = $this->db->escape_str($this->name);
		$this->created_at = $this->get_utc_datetime();
		$sql_customer_group_id = ($this->customer_group_id === null ? 'NULL' : '\''.$this->customer_group_id.'\'');

		$query = "INSERT INTO `foreup_contact_forms`
				  SET
				  `course_id`='$this->course_id',
				  `customer_group_id`=$sql_customer_group_id,
				  `created_by`='$this->created_by',
				  `created_at`='.$this->created_at',
				  `name`= '$this->name',
				  `has_first_name`='$this->has_first_name',
				  `has_last_name`='$this->has_last_name',
				  `has_phone_number`='$this->has_phone_number',
				  `has_email`='$this->has_email',
				  `has_birthday`='$this->has_birthday',
				  `has_address`='$this->has_address',
				  `has_city`='$this->has_city',
				  `has_state`='$this->has_state',
				  `has_zip`='$this->has_zip',
				  `has_message`='$this->has_message',
				  `req_first_name`='$this->req_first_name',
				  `req_last_name`='$this->req_last_name',
				  `req_phone_number`='$this->req_phone_number',
				  `req_email`='$this->req_email',
				  `req_birthday`='$this->req_birthday',
				  `req_address`='$this->req_address',
				  `req_city`='$this->req_city',
				  `req_state`='$this->req_state',
				  `req_zip`='$this->req_zip',
				  `req_message`='$this->req_message'";

		$result_bool = (bool)$this->db->simple_query($query);
		$this->id = $this->db->insert_id();

		return $result_bool;
	}

	/*
	 * Persists updated object into database storage. Returns true / false on success
	 */
	public function update(){
		// Validate Self
		if(!$this->validate()){
			return false;
		}
		if($this->id === null){
			return false;
		}

		// Escape input
		$this->name = $this->db->escape_str($this->name);
		$sql_customer_group_id = ($this->customer_group_id === null ? 'NULL' : '\''.$this->customer_group_id.'\'');

		$query = "UPDATE `foreup_contact_forms`
				  SET
				  `customer_group_id`=$sql_customer_group_id,
				  `name`='$this->name',
				  `has_first_name`='$this->has_first_name',
				  `has_last_name`='$this->has_last_name',
				  `has_phone_number`='$this->has_phone_number',
				  `has_email`='$this->has_email',
				  `has_birthday`='$this->has_birthday',
				  `has_address`='$this->has_address',
				  `has_city`='$this->has_city',
				  `has_state`='$this->has_state',
				  `has_zip`='$this->has_zip',
				  `has_message`='$this->has_message',
				  `req_first_name`='$this->req_first_name',
				  `req_last_name`='$this->req_last_name',
				  `req_phone_number`='$this->req_phone_number',
				  `req_email`='$this->req_email',
				  `req_birthday`='$this->req_birthday',
				  `req_address`='$this->req_address',
				  `req_city`='$this->req_city',
				  `req_state`='$this->req_state',
				  `req_zip`='$this->req_zip',
				  `req_message`='$this->req_message'
				  WHERE `id`='$this->id'
				  AND `deleted_by` IS NULL";

		return $this->db->simple_query($query);
	}

	/*
	 * Validates the data
	 */
	private function validate(){
		if($this->id !== null && (!$this->validate_row_id($this->id))){
			return false;
		}

		if($this->customer_group_id !== null && (!$this->validate_row_id($this->customer_group_id))){
			return false;
		} else {
			if($this->customer_group_id !== null){
				// Verify group exists and is valid for the current course.
				$this->db->select('course_id');
				$this->db->from('customer_groups');
				$this->db->where('course_id', $this->course_id);

				$result = $this->db->get();
				if($result->num_rows() > 0){
					// Load in the data
					$row = $result->first_row();
					if((int)$row->course_id != $this->course_id){
						return false; // Customer Group belongs to some other course! Madness!
					}
				} else {
					return false; // Customer Group doesn't exist
				}
			}
		}

		if(!$this->validate_row_id($this->course_id)){
			return false;
		}

		if(!$this->validate_row_id($this->created_by)){
			return false;
		}

		if($this->deleted_by !== null && (!$this->validate_row_id($this->deleted_by))){
			return false;
		}

		if(!is_string($this->name) || $this->name == ''){
			return false;
		}

		/*
		 * Shouldn't need to be verified. Set by the database. Never passed to the database.
		 * same for deleted_at column
		try{
			$temp = new DateTime($this->created_at);
		} catch (Exception $e) {
			return false;
		}
		*/

		$this->customer_group_id = ($this->customer_group_id == false ? null : (int)$this->customer_group_id);

		$this->has_first_name = ($this->has_first_name === true ? true : false);
		$this->has_last_name = ($this->has_last_name === true ? true : false);
		$this->has_phone_number = ($this->has_phone_number === true ? true : false);
		$this->has_email = ($this->has_email === true ? true : false);
		$this->has_birthday = ($this->has_birthday === true ? true : false);
		$this->has_address = ($this->has_address === true ? true : false);
		$this->has_city = ($this->has_city === true ? true : false);
		$this->has_state = ($this->has_state === true ? true : false);
		$this->has_zip = ($this->has_zip === true ? true : false);
		$this->has_message = ($this->has_message === true ? true : false);
		
		$this->req_first_name = ($this->req_first_name === true ? true : false);
		$this->req_last_name = ($this->req_last_name === true ? true : false);
		$this->req_phone_number = ($this->req_phone_number === true ? true : false);
		$this->req_email = ($this->req_email === true ? true : false);
		$this->req_birthday = ($this->req_birthday === true ? true : false);
		$this->req_address = ($this->req_address === true ? true : false);
		$this->req_city = ($this->req_city === true ? true : false);
		$this->req_state = ($this->req_state === true ? true : false);
		$this->req_zip = ($this->req_zip === true ? true : false);
		$this->req_message = ($this->req_message === true ? true : false);

		// No errors
		return true;
	}

	/*
	 * Validates a row id
	 */
	private function validate_row_id($id){
		if(!is_integer($id) || $id < 1) {
			return false;
		} else {
			return true;
		}
	}

	/*
	* Returns a string representation of UTC datetime in RFC3339 format
	*/
	private function get_utc_datetime(){
		$date_utc = new \DateTime(null, new \DateTimeZone("UTC"));
		return $date_utc->format(\DateTime::RFC3339);
	}

}