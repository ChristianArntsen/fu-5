<?php
namespace models\ExecQueue;
/**
 * Class Row
 * @package models\ExecQueue
 */
class Row {
    public $id,$command,$timestamp_started,$timestamp_finished;
}