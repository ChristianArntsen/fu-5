<?php

class repeatable extends CI_Model
{
	/**
	 * @param $campaign_id
	 * @return models\Repeatable\Row
	 */
	public function getRowModel(array $criteria)
	{
		$query = $this->db->get_where('repeatable', $criteria);

		$row = $query->result("models\\Repeatable\\Row");
		if(count($row) > 0){
			$row = $row[0];
		}
		return $row;

	}

	public function saveRowModel(\models\Repeatable\Row $row)
	{
		$this->db->where('id', $row->id);
		$this->db->update('repeatable', $row);
	}


	/**
	 * @param array $criteria
	 * @return models\Repeatable\Row[]
	 */
	public function getAllBy(array $criteria)
	{
		$query = $this->db->get_where('repeatable', $criteria);

		$rows = $query->result("models\\Repeatable\\Row");
		return $rows;
	}


	/**
	 * @param array $criteria
	 * @return models\Repeatable\Row[]
	 */
	public function delete_by(array $criteria)
	{
		$this->db->where($criteria);
		$this->db->delete('repeatable');

		return true;
	}
}