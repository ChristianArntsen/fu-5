<?php
/*
 * A template is just a json object that represents an email
 */
class Marketing_Images extends CI_Model
{
	public function __construct()
	{
		parent::__construct();


		$this->load->model('course');
		$this->load->model('Marketing_Campaign');
		$this->course_info = $this->course->get_info($this->session->userdata('course_id'));
	}

	public function get($id)
	{
		$this->db->select('*');
		$this->db->from('marketing_images');
		$this->db->where('id', $id);
		$this->db->where('course_id', $this->course_info-course_id);
		$result = $this->db->get();
		$result = $result->result_array()[0];
		if(!$result)
			return false;
		return $result;
	}

	public function getMyHistory()
	{
		$this->db->select('*');
		$this->db->from('marketing_images');
		$this->db->where('course_id', $this->course_info->course_id);
		$this->db->where('deleted', 0);
		$result = $this->db->get();
		$result = $result->result_array();
		if(!$result)
			return false;
		return $result;
	}

	public function getSystemDefaults()
	{
		$this->db->select('*');
		$this->db->from('marketing_images');
		$this->db->where('course_id', -1);
		$this->db->where('deleted', 0);
		$result = $this->db->get();
		$result = $result->result_array();
		if(!$result)
			return false;
		return $result;
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->where('course_id', $this->course_info-course_id);
		return $this->db->update('marketing_images', array('deleted' => 1));
	}

	public function save($image_path,$id=null)
	{
		$dataToInsert = array(
			"course_id"=>$this->course_info->course_id,
			"path"=>$image_path
		);

		if (!$id or !$this->exists($id))
		{
			if($this->db->insert('marketing_images',$dataToInsert))
			{
				$dataToInsert['id']=$this->db->insert_id();
				//An insert should return the new id if possible, is there a better way?
				return $dataToInsert;
			}
			return false;
		}

		$this->db->where('id', $id);
		$this->db->where('course_id', $this->course_info-course_id);
		return $this->db->update('marketing_images',$dataToInsert);
	}
}