<?php
class Special extends CI_Model
{
	public $aggregate = 0;
	private $cache = array();

	// ADD/UPDATE DAILY SPECIALS
	public function save($specials_data, $specialtime, $special_id = false)
	{
		if ($special_id) {
			$this->db->where('special_id', $special_id);
			$this->db->update('specials', $specials_data);
			$this->db->delete('special_times',array('special_id' => $special_id));
			foreach ($specialtime as $value) {
	            $data_specialtime = array(
	            'special_id'=>$special_id,
	            'time'=>$value
	            );
	            $this->db->insert('special_times',$data_specialtime);
	        }
		}else{
			$this->db->insert('specials',$specials_data);
	        $last_id = $this->db->insert_id();
	        foreach ($specialtime as $value) {
	            $data_specialtime = array(
	            'special_id'=>$last_id,
	            'time'=>$value
	            );
	            $this->db->insert('special_times',$data_specialtime);
	        }

	        if($this->db->affected_rows() > 0){
    			return true; 
			}
        }
	}
	// DELETE DAILY SPECIALS
	public function delete($special_id)
	{
		$arrResponse['status'] = "fail";
		$this->db->delete('specials',array('special_id' => $special_id));
		$this->db->delete('special_times',array('special_id' => $special_id));
		$response = $this->db->affected_rows();
		if($response >= 0){
			$arrResponse['status'] = "success";
		}
		return $arrResponse;
	}
	// GET DAILY SPECIALS FOR EDIT
	public function get_info($teesheet_id, $id = false)
	{
		$this->db->select("specials.*, GROUP_CONCAT(foreup_special_times.time) as special_times");
		$this->db->from('specials');
		$this->db->join('special_times', 'specials.special_id = special_times.special_id','left');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->where('specials.special_id', $id);
		$this->db->group_by('special_times.special_id'); 
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}
	// GET DAILY SPECIALS
	public function get_all($teesheet_id = false, $special_id = false, $aggregate = 0)
	{
		$teesheet_sql = '';
		if($teesheet_id){
			$teesheet_sql = 'AND specials.teesheet_id = '. (int) $teesheet_id;
		}
		
		$special_sql = '';
		if($special_id){
			$special_sql = 'AND specials.special_id = '. (int) $special_id;
		}
		
		$aggregate_sql = 'AND specials.is_aggregate = '. (int) $aggregate;
		$course_id = (int) $this->session->userdata('course_id');

		if($aggregate === false){
			$aggregate_sql = '';
		}
		
		$query = $this->db->query("SELECT specials.*,
				GROUP_CONCAT(
					DATE_FORMAT(STR_TO_DATE(LPAD(times.time, 4, '0'), '%H%i'), '%l:%i%p')
					ORDER BY times.time
					SEPARATOR ', '	
				) as special_times, specials.teesheet_id, teesheet.title AS teesheet_name, bc.name AS booking_class
			FROM foreup_specials AS specials
			LEFT JOIN foreup_special_times AS times
				ON specials.special_id = times.special_id
			INNER JOIN foreup_teesheet AS teesheet
				ON teesheet.teesheet_id = specials.teesheet_id
			LEFT JOIN foreup_booking_classes AS bc
                ON bc.booking_class_id = specials.booking_class_id
			WHERE teesheet.course_id = {$course_id}
				AND teesheet.deleted = 0
				{$teesheet_sql}
				{$special_sql}
				{$aggregate_sql}
			GROUP BY specials.special_id"); 
		
		return $query->result_array();
	}
	// GET SPECIALS TEETIMES
	function get_special_teetimes($start, $end, $increment, $teesheet_id)
	{
        $this->load->model('Increment_adjustment');
        $increments = $this->Increment_adjustment->get_increment_array(date('Y-m-d'), $teesheet_id);

		$special_teetimes_array = array();
        foreach ($increments as $index => $increment) {
            $i_z = ($index < 1000)?'0':'';
			if ($index >= $start && $index < $end)
                $special_teetimes_array[] = array('special_teetime'=>$i_z.$index, 'teesheet_id'=>$teesheet_id);
        }

		return $special_teetimes_array;
	}
	// GET SPECIALS TEETIMES ARRAY FOR DISPLAY CHECKBOX
	function get_teetime_special_array($start, $end, $increment, $teesheet_id)
	{
		$final_array = array();
		$teetimes = $this->get_special_teetimes($start, $end, $increment, $teesheet_id);
		foreach($teetimes as $teetime)
		{
			$final_array[$teetime['special_teetime']] = date('g:ia', strtotime('20120101'.$teetime['special_teetime']));
		}
		return $final_array;
	}
	// ENABLE SPECIALS ON CHECKBOX
	function enabled_specials($special_enabled,$teesheet_id){
		$this->db->set('specials_enabled',$special_enabled);
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->update('teesheet');
	}
	// GET ENABLE SPECIALS 
	function get_enabled_specials($teesheet_id){
		$this->db->select('specials_enabled'); 
    	$this->db->from('teesheet'); 
    	$this->db->where('teesheet_id', $teesheet_id);
   	 	return $this->db->get()->result_array();
	}
	
	function teesheet_specials_enabled($teesheet_id){
		$this->db->select('specials_enabled'); 
    	$this->db->from('teesheet');   
    	$this->db->where('teesheet_id', $teesheet_id);
   	 	
   	 	$row = $this->db->get()->row_array();
   	 	
   	 	if(empty($row) || $row['specials_enabled'] == 0){
			return false;
		}else{
			return true;
		}
	}	
	
	// GET SPECIALS BY DATE AND TIME
	function get_special_by_date($teesheet_id, $date, $time, $booking_class_id = false){
		
		$key = (int) $teesheet_id.'_'.$date;
		if(isset($this->cache[$key])){
			if(empty($this->cache[$key][(int) $time])){
				return false;
			}
			return $this->cache[$key][(int) $time];
		}

		$date = DateTime::createFromFormat('Y-m-d', $date);
		$day_of_week = strtolower($date->format('l'));

		$this->db->select("specials.special_id, GROUP_CONCAT(special_times.time) AS times, 
			specials.price_1, specials.price_2, specials.price_3, specials.price_4", false);
		$this->db->from('specials AS specials');
		$this->db->join('special_times AS special_times', 'specials.special_id = special_times.special_id','left');
		$this->db->where('specials.teesheet_id', (int) $teesheet_id);
		$this->db->where("((specials.date = ".$this->db->escape($date->format('Y-m-d'))." AND specials.is_recurring = 0) 
			OR (specials.{$day_of_week} = 1 AND specials.is_recurring = 1))");
		$this->db->where("specials.active", 1);
		$this->db->where("specials.is_aggregate", (int) $this->aggregate);
        $this->db->where("( specials.booking_class_id = 0 OR specials.booking_class_id = '{$booking_class_id}' )");
		$this->db->group_by('specials.special_id');

		$rows = $this->db->get()->result_array();

		if(empty($rows)){
			$this->cache[$key] = false;
			return false;
		}

		foreach($rows as $row){
			$times = explode(',', $row['times']);
			unset($row['times']);

			if(!isset($this->cache[$key])){
				$this->cache[$key] = array();
			}
			
			foreach($times as $t){
				$this->cache[$key][$t] = $row;
			}			
		}
		
		if(empty($this->cache[$key][(int) $time])){
			return false;
		}
		return $this->cache[$key][(int) $time];
	}

	// GET PRICES BY SPECIALS FOR SALE
	function get_price_by_special($special_id, $holes = '9', $cart = false){
		$this->db->select("specials.price_1, specials.price_2, specials.price_3, specials.price_4");
		$this->db->from('specials');
		$this->db->where('specials.special_id', $special_id);
		$special = $this->db->get()->row_array();

        $price = 0.00;
		if($holes == '9' && empty($cart)){
			$price = $special['price_2'];
			
		}else if($holes == '9' && !empty($cart)){
			$price = $special['price_4'];
			
		}else if($holes == '18' && empty($cart)){
			$price = $special['price_1'];
			
		}else if($holes == '18' && !empty($cart)){
			$price = $special['price_3'];		
		}
		
		return (float) $price;		
	}
	
	// GET SPECIALS PRICES OF GREEN FEE AND CART FEE
	function get_prices($teesheet_id = false, $date = false, $time = false, $holes = '9', $cart = false, $booking_class_id = false){
		$special = $this->get_special_by_date($teesheet_id, $date, $time, $booking_class_id);
		if(empty($special)){
			return false;
		}
		
		$price = 0.00;
		if($holes == '9' && empty($cart)){
			$price = $special['price_2'];
			
		}else if($holes == '9' && !empty($cart)){
			$price = $special['price_4'];
			
		}else if($holes == '18' && empty($cart)){
			$price = $special['price_1'];
			
		}else if($holes == '18' && !empty($cart)){
			$price = $special['price_3'];	
		}
		
		return (float) $price;		
	}

	function get_special($teesheet_id = false, $date = false, $time = false, $booking_class_id = false){
		$special = $this->get_special_by_date($teesheet_id, $date, $time, $booking_class_id);
		if(empty($special)){
			return false;
		}
		return $special;
	}	
}	
