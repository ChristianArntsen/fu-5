<?php
class Custom_payments extends CI_Model
{
    function get_all() {
        $this->db->from('custom_payment_types');
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('deleted', 0);

        return $this->db->get()->result_array();
    }

    function save($payment_types = array()) {
        if (empty($payment_types))
            return true;
        
        return $this->db->insert_batch('custom_payment_types', $payment_types);
    }

    function delete($type) {
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('custom_payment_type', $type);
        return $this->db->update('custom_payment_types', array('deleted'=>1));
    }

    function is_valid($type){
        $this->db->from('custom_payment_types');
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('custom_payment_type', $type);
        $this->db->limit(1);

        return ($this->db->get()->num_rows()==1);
    }

    function get_payment_type($type){
        $this->db->from('custom_payment_types');
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('custom_payment_type', $type);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows()==1) {
            return $query->row_array()['label'];
        }

        return false;
    }

    function clean($string) {
        $string = str_replace(' ', '-', strtolower($string)); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}