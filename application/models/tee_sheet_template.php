<?php
class Tee_sheet_template extends CI_Model
{
    function save($data) {
        $this->db->insert('tee_sheet_templates', $data);
        return $this->db->insert_id();
    }

    function delete($template_id) {
        $this->db->where('template_id', $template_id);
        $this->db->where('course_id', $this->config->item('course_id'));
        return $this->db->update('tee_sheet_templates', array('deleted'=>gmdate('Y-m-d')));
    }

    function get($template_id) {
        $this->db->from('tee_sheet_templates');
        $this->db->where('course_id', $this->config->item('course_id'));
        $this->db->where('template_id', $template_id);
        $this->db->limit(1);

        return $this->db->get()->row_array();
    }
    function get_all() {
        $this->db->from('tee_sheet_templates');
        $this->db->join('tee_sheet_template_events', 'tee_sheet_templates.template_id = tee_sheet_template_events.template_id', 'left');
        $this->db->where('course_id', $this->config->item('course_id'));
        $this->db->where('TTID IS NOT NULL');
        $this->db->where('deleted', null);
        $this->db->group_by('tee_sheet_templates.template_id');
        $this->db->order_by('name');

        return $this->db->get()->result_array();
    }

    function save_events($template_events) {
        return $this->db->insert_batch('tee_sheet_template_events', $template_events);
    }

    function get_events($template_id) {
        $this->db->from('tee_sheet_template_events');
        $this->db->where('template_id', $template_id);
        $this->db->order_by('start');

        return $this->db->get()->result_array();
    }

    function apply($template_id, $tee_sheet_id, $date) {
        $success = false;
        $this->load->model('Teetime');
        $template = $this->get($template_id);

        $events = $this->get_events($template['template_id']);

        foreach ($events as $event) {
            // Save each event onto the selected date/tee sheet

        }

        return $success;
    }
}
