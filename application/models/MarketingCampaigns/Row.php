<?php
namespace models\MarketingCampaigns;


class Row
{
	public $campaign_id,$course_id,$type;
	private $repeatable_occurence;




	/**
	 * @return mixed
	 */
	public function getCampaignId()
	{
		return $this->campaign_id;
	}

	/**
	 * @param mixed $campaign_id
	 */
	public function setCampaignId($campaign_id)
	{
		$this->campaign_id = $campaign_id;
	}

	/**
	 * @return mixed
	 */
	public function getCourseId()
	{
		return $this->course_id;
	}

	/**
	 * @param mixed $course_id
	 */
	public function setCourseId($course_id)
	{
		$this->course_id = $course_id;
	}

	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param mixed $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return \models\Repeatable\Row
	 */
	public function getRepeatableOccurence()
	{
		if($this->repeatable_occurence != null){
			return $this->repeatable_occurence;
		} else {
			return [];
		}
	}

	/**
	 * @param mixed $repeatable_occurence
	 */
	public function setRepeatableOccurence(\models\Repeatable\Row $repeatable_occurence)
	{
		$repeatable_occurence->setResourceId($this->getCampaignId());
		$repeatable_occurence->setType("marketing_campaign");
		$this->repeatable_occurence = $repeatable_occurence;
	}

	public function persistOccurences()
	{
		if(count($this->getRepeatableOccurence()) > 0){
			$repeatableOccurence = $this->getRepeatableOccurence();
			$CI = & get_instance();
			if(isset($repeatableOccurence->id)){
				$CI->db->where([
						"resource_id"=>$repeatableOccurence->resource_id,
						"type"=>$repeatableOccurence->type
				]);
				$repeatableOccurence->next_occurence = $repeatableOccurence->getNextCalculatedOccurrence()->format("Y-m-d H:i:s");

				$CI->db->update('repeatable',$repeatableOccurence);
			} else {
				$CI->load->model("repeatable");
				$repeatable_model = $CI->repeatable;
				$repeatable = $repeatable_model->getRowModel([
						"resource_id"=>$repeatableOccurence->resource_id,
						"type"=>$repeatableOccurence->type
				]);
				if(count($repeatable) > 0){
					$repeatableOccurence->id = $repeatable->id;
					$this->persistOccurences();
				} else {
					$repeatableOccurence->next_occurence = $repeatableOccurence->getNextCalculatedOccurrence()->format("Y-m-d H:i:s");
					$repeatableOccurence->last_ran = "0000-00-00 00:00:00";
					$CI->db->insert("repeatable",$this->getRepeatableOccurence());
				}
			}
		}
	}

}