<?php
class Person extends MY_Model
{
	/*Determines whether the given person exists*/
	function exists($person_id)
	{
		$this->db->from('people');
		$this->db->where('people.person_id',$person_id);
		$this->db->limit(1);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}

	/*Gets all people*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('people');
		$this->db->order_by("last_name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}

	function count_all()
	{
		$this->db->from('people');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a person as an array.
	*/
	function get_info($person_id)
	{
		$query = $this->db->get_where('people', array('person_id' => $person_id), 1);

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//create object with empty properties.
			$fields = $this->db->list_fields('people');
			$person_obj = new stdClass;

			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}

			return $person_obj;
		}
	}

		/*
	Gets information about a person as an array by their email address.
	*/
	function get_person_info_by_email($email)
	{
		$query = $this->db->get_where('people', array('email' => $email), 1);

		return $query->row();
	}

	/*
	Get people with specific ids
	*/
	function get_multiple_info($person_ids)
	{
		$this->db->from('people');
		$this->db->where_in('person_id',$person_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a person
	*/
	function save_person(&$person_data,$person_id=false,$log_entry = false)
	{
		// do we know a person_id?
		// if so, ensure it is consistent across the two variables that store it.
		if(!$person_id && isset($person_data['person_id'])){
			$person_id = $person_data['person_id'];
		}
		elseif($person_id && $person_id > 0 && !isset($person_data['person_id'])){
			$person_data['person_id'] = $person_id;
	    }
		elseif($person_id && $person_id > 0 && isset($person_data['person_id']) &&
			   $person_id !== $person_data['person_id']){
			// TODO: throw exception here due to ambiguous person_id
			return false;
		}

        $mass = false;
        if(!is_array($log_entry)) {
			if(isset($_SESSION['foreup']['emp_id']) && $_SESSION['foreup']['emp_id']*1>0) {
            // not mass upsert
            $act_type = 7; // upsert
            $emp_id = $_SESSION['foreup']['emp_id']*1; //$this->session->userdata('emp_id')
            $comments = 'Person saved.';
            //TODO: Need a way to resolve which tables were actually updated
            $tbls = 'foreup_people';
            $gmt = date('Y-m-d H:i:s');

            $log_entry = array(
                'editor_id'=>$emp_id,
                'gmt_logged'=>$gmt,
                'action_type' => $act_type, // mass_upsert
                'comments'=> $comments,
                'tables_updated'=> $tbls
            );
//				$this->db->query("INSERT INTO foreup_employee_audit_log (editor_id,action_type_id,comments,gmt_logged) VALUES ('$emp_id',$act_type,'$comments','$gmt')");
//				$eal_id = $this->db->query("SELECT LAST_INSERT_ID() as id")->result_array()[0]['id'];
//				$log_entry['employee_audit_log_id'] = $eal_id;
			}
        }
        else {
            // pull into their variables
            $mass = true;
            $act_type = $log_entry['action_type'];
            $emp_id = $log_entry['editor_id'];
            $comments = $log_entry['comments'];
            //TODO: Need a way to resolve which tables were actually updated
            $tbls = 'foreup_people';
            $gmt = $log_entry['gmt_logged'];
            
            // TODO: Uncomment this when problems below are fixed
            //$eal_id = $log_entry['employee_audit_log_id'];
        }

		if (isset($person_data['phone_number']))
			$person_data['phone_number'] = str_replace(array('(',')','-','_',' '), '', $person_data['phone_number']);
		if (isset($person_data['cell_phone_number']))
			$person_data['cell_phone_number'] = str_replace(array('(',')','-','_',' '), '', $person_data['cell_phone_number']);
		if (!$person_id || !$this->exists($person_id))
		{
			if ($this->db->insert('people',$person_data))
			{
				$nu_id = $person_data['person_id'] = $this->db->insert_id();
				if(isset($_SESSION['foreup']['emp_id'])) {

					// insert into person altered
					//$this->db->query("INSERT INTO foreup_person_altered (employee_audit_log_id,person_edited,tables_updated) VALUES ($eal_id,$nu_id,'$tbls')");
				}
				return true;
			}
			return false;
		}

		$this->db->where('person_id', $person_id);
		$result = $this->db->update('people',$person_data);
        if($result && isset($_SESSION['foreup']['emp_id'])){
            // successfully saved our person!
            $pers_alt = $person_data['person_id']*1;
			if($person_id){
				$pers_alt = $person_id;
			} else {
				$pers_alt = $person_data['person_id'];
			}

			/* TODO: Please fix query, causing problems with sales that have 2 payments, 1 being loyalty
            // insert into person altered
            $this->db->query("INSERT INTO foreup_person_altered (employee_audit_log_id,person_edited,tables_updated) VALUES ($eal_id,$pers_alt,'$tbls')");
            */
        }
        return $result;
	}

	/*
	Merge
	*/
	function merge($person_id_1,$person_id_2)
	{
		$original_person_info = $this->get_info($person_id_1);
		$merge_person_info = $this->get_info($person_id_2, false, true);
		$updated_info = array();

		if ((int)$this->input->post('name_box') == 2) {
			$updated_info['first_name'] = $merge_person_info->first_name;
			$updated_info['last_name'] = $merge_person_info->last_name;
			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'name',
				'original_value' => json_encode(array($original_person_info->first_name, $original_person_info->last_name)),
				'new_value' => json_encode(array($merge_person_info->first_name, $merge_person_info->last_name))
			);
			$this->Customer_merge->record_change($merge_change_data);
		}

		if ((int)$this->input->post('email_box') == 2) {
			$updated_info['email'] = $merge_person_info->email;
			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'email',
				'original_value' => $original_person_info->email,
				'new_value' => $merge_person_info->email
			);
			$this->Customer_merge->record_change($merge_change_data);
		}

		if ((int)$this->input->post('phone_box') == 2) {
			$updated_info['phone_number'] = $merge_person_info->phone_number;
			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'phone_number',
				'original_value' => $original_person_info->phone_number,
				'new_value' => $merge_person_info->phone_number
			);
			$this->Customer_merge->record_change($merge_change_data);
		}

		if ((int)$this->input->post('address_box') == 2) {
			$updated_info['address_1'] = $merge_person_info->address_1;
			$updated_info['city'] = $merge_person_info->city;
			$updated_info['state'] = $merge_person_info->state;
			$updated_info['zip'] = $merge_person_info->zip;
			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'address',
				'original_value' => json_encode(array($original_person_info->address_1, $original_person_info->city, $original_person_info->state, $original_person_info->zip)),
				'new_value' => json_encode(array($merge_person_info->address_1, $merge_person_info->city, $merge_person_info->state, $merge_person_info->zip))
			);
			$this->Customer_merge->record_change($merge_change_data);
		}

		if ((int)$this->input->post('comment_box') == 2) {
			$updated_info['comments'] = $merge_person_info->comments;
			$merge_change_data = array(
				'merge_id' => $this->merge_id,
				'type' => 'comments',
				'original_value' => $original_person_info->comments,
				'new_value' => $merge_person_info->comments
			);
			$this->Customer_merge->record_change($merge_change_data);
		}

		if (count($updated_info) > 0) {
			$this->db->update('people', $updated_info, array('person_id' => $person_id_1));
		}

		return ;
	}

	/*
	Deletes one Person (doesn't actually do anything)
	*/
	function delete($person_id)
	{
		return true;;
	}

	/*
	Deletes a list of people (doesn't actually do anything)
	*/
	function delete_list($person_ids)
	{
		return true;
 	}

	function update_mailchimp_subscriptions($email, $first_name, $last_name, $mailing_list_ids)
	{
		$this->load->library('mcapi', array('apikey' => $this->config->item('mailchimp_api_key')));
		$mailing_list_ids = $mailing_list_ids === FALSE ? array() : $mailing_list_ids;
		$current_lists = get_mailchimp_lists($email);
		foreach($current_lists as $list)
		{
			//If a list we are currently subscribed to is not in the updated list, unsubscribe
			if (!in_array($list['id'], $mailing_list_ids))
			{
				$this->mcapi->listUnsubscribe($list['id'], $email, false, false, false);
			}
		}

		foreach($mailing_list_ids as $list)
		{
			$this->mcapi->listSubscribe($list, $email, array('FNAME' => $first_name, 'LNAME' => $last_name), 'html', false, true, false, false);
		}
	}
}
?>
