<?php

class Sendgrid extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->db_datastore = $this->load->database('datastore', TRUE);
	}

	public function batchSaveEvents($events)
	{
		$toUnsubscribe = [];


		$allEventsToInsert = array();
		foreach ($events as $event) {
			if($event->event == "unsubscribe"){
				$toUnsubscribe[] = $event->email;
			}


			$allEventsToInsert[] = array(
				"event" => $event->event,
				"email" => $event->email,
				"invoice_id" => isset($event->invoice_id)?$event->invoice_id:'',
				"marketing_campaign_id" => isset($event->marketing_campaign_id)?$event->marketing_campaign_id:'',
				"customer_id" => isset($event->customer_id)?$event->customer_id:'',
				"course_id" => isset($event->course_id)?$event->course_id:'',
				"timestamp" => date("Y-m-d H:i:s", $event->timestamp)
			);
		}
		$this->db_datastore->insert_batch('sendgrid_events', $allEventsToInsert);
		if(!empty($toUnsubscribe)){
			$this->db->where_in("email",$toUnsubscribe);
			$this->db->update("customers c JOIN foreup_people p ON c.person_id = p.person_id",["opt_out_email"=>1]);
		}


		return true;
	}

    public function getCompleteMarketingSummaries($course_id)
    {
        if(!isset($course_id) || $course_id == '')
            return [];
        $counts = $this->db_datastore->query("
        SELECT
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='processed' and course_id=$course_id) as processed,
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='delivered' and course_id=$course_id) as delivered,
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='open' and course_id=$course_id) as opened,
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='click' and course_id=$course_id) as clicked,
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='bounce' and course_id=$course_id) as bounced,
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='unsubscribe' and course_id=$course_id) as unsubscribed
        FROM foreup_sendgrid_events WHERE course_id=$course_id LIMIT 1 ;
        ");
        return $counts->row_array();
    }

	public function getMarketingCampaignStatistics($campaign_id)
	{
        //TODO: nothing stopping random user from accessing results from other courses' campaigns
		$counts = $this->db_datastore->query("
        SELECT
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='processed' and marketing_campaign_id=$campaign_id) as processed,
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='delivered' and marketing_campaign_id=$campaign_id) as delivered,
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='open' and marketing_campaign_id=$campaign_id) as opened,
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='click' and marketing_campaign_id=$campaign_id) as clicked,
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='bounce' and marketing_campaign_id=$campaign_id) as bounced,
          (SELECT COUNT(DISTINCT(email)) FROM foreup_sendgrid_events WHERE event='unsubscribe' and marketing_campaign_id=$campaign_id) as unsubscribed
        FROM foreup_sendgrid_events WHERE marketing_campaign_id=$campaign_id LIMIT 1 ;
        ");
		return $counts->row_array();
	}

    public function getMarketingEventFacets($campaign_id,$grouping = ['day','hour'])
    {
        $select = [];
        $groupBy = [];
        if(isset($grouping)){
            if(in_array("day",$grouping)){
                $select[] = "DAY(timestamp) as day";
                $groupBy[] = "day";
            }
            if(in_array("hour",$grouping)){
                $select[] = "HOUR(timestamp) as hour";
                $groupBy[] = "hour";
            }
            if(in_array("dayname",$grouping)){
                $select[] = "DAYNAME(timestamp) as dayname";
                $groupBy[] = "dayname";
            }
        }
        $results = $this->db_datastore->query("
            SELECT
                DATE_FORMAT(timestamp, \"%Y-%m-%d %H:00:00\") AS timestamp,
                count(id) as count,
                ".join(',',$select).",
                event
            FROM foreup_sendgrid_events
            WHERE marketing_campaign_id=$campaign_id
            GROUP BY ".join(',',$groupBy).",event
         ");

        $groupedResults = [];
        foreach ($results->result() as $row)
        {
            if(in_array("hour",$grouping) && in_array("day",$grouping)){
                $groupingUnit = $row->timestamp;
            } else if (in_array("day",$grouping)) {
                $groupingUnit = $row->day;
            } else if(in_array("hour",$grouping)){
                $groupingUnit = $row->hour;
            } else if(in_array("dayname",$grouping)) {
                $groupingUnit = $row->dayname;
            } else {
                $groupingUnit = $row->timestamp;
            }

            if(!isset($groupedResults[$groupingUnit])){
                $groupedResults[$groupingUnit] = [];
            }
            $groupedResults[$groupingUnit][$row->event] = $row->count;


        }
        return $groupedResults;
    }


	public function getInvoiceStatistics($invoice_id)
	{
		$counts = $this->db_datastore->query("
        SELECT
          (SELECT COUNT(DISTINCT('email')) FROM datastore_sendgrid_events WHERE event='processed' and invoice_id=$invoice_id) as ProcessedCount,
          (SELECT COUNT(DISTINCT('email')) FROM datastore_sendgrid_events WHERE event='delivered' and invoice_id=$invoice_id) as DeliveredCount,
          (SELECT COUNT(DISTINCT('email')) FROM datastore_sendgrid_events WHERE event='open' and invoice_id=$invoice_id) as OpenCount,
          (SELECT COUNT(DISTINCT('email')) FROM datastore_sendgrid_events WHERE event='click' and invoice_id=$invoice_id) as ClickCount
        FROM datastore_sendgrid_events LIMIT 1 ;
        ");
		return $counts->row_array();
	}

    public function getHistoryByCampaign($campaign_id,$event = null)
    {
        $counts = $this->db_datastore
            ->select("email,timestamp")
            ->from("sendgrid_events")
            ->group_by("email")
            ->where("marketing_campaign_id",$campaign_id);
        if(isset($event)){
            $this->db_datastore
                ->where("event",$event);
        }
        return $this->db_datastore->get()->result_array();
    }



}