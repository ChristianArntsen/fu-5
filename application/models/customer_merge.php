<?php
class Customer_merge extends CI_Model
{
    function save(&$data)
    {
        $success = $this->db->insert('customer_merge', $data);
        $data['merge_id'] = $this->db->insert_id();
        return $success;
    }

    function record_change($change_data)
    {
        return $this->db->insert('customer_merge_changes', $change_data);
    }

    function get_primary($person_id = false)
    {
        if (empty($person_id)) {
            return false;
        }
        else {
            $this->db->from('customer_merge');
            $this->db->where('course_id', $this->config->item('course_id'));
            $this->db->where('duplicate_customer_id', $person_id);
            $this->db->limit(1);
            $result = $this->db->get()->row_array();

            return !empty($result['customer_id']) ? $result['customer_id'] : $person_id;
        }
    }

    function get_secondary_accounts($person_id = false)
    {
        $secondary_accounts = array();
        if (empty($person_id)) {
            return $secondary_accounts;
        }
        else {
            $this->db->from('customer_merge');
            $this->db->where('course_id', $this->config->item('course_id'));
            $this->db->where('customer_id', $person_id);
            $result = $this->db->get()->result_array();

            foreach($result as $account) {
                $secondary_accounts[] = $account['duplicate_customer_id'];
            }

            return $secondary_accounts;
        }
    }

}