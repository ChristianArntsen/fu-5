<?php
class Table_Ticket extends CI_Model
{
	function __construct(){
		parent::__construct();
		$this->load->library('sale_lib');
	}

	function mark_complete($ticket_id){
		$date = $this->db->escape(date('Y-m-d h:i:s'));
		$query = $this->db->query("UPDATE foreup_table_tickets
			SET completed = 1 AND date_completed = {$date}
			WHERE ticket_id = ".(int) $ticket_id);
		return true;
	}

	function mark_printed($ticket_id){
		$date = $this->db->escape(date('Y-m-d h:i:s'));
		$query = $this->db->query("UPDATE foreup_table_tickets
			SET printed = 1
			WHERE ticket_id = ".(int) $ticket_id);
		return true;
	}

	// Saves a new ticket
	function save($sale_id, $table_number, $employee_id, $items, $message = null, $meal_course_id = 0){

		if(empty($items)){
			return false;
		}

		$this->db->insert('table_tickets', array(
			'sale_id' => $sale_id,
			'table_number' => $table_number,
			'employee_id' => (int) $employee_id,
			'message' => $message,
			'terminal_id' => (int) $this->session->userdata('terminal_id'),
			'meal_course_id' => $meal_course_id
		));
		$ticket_id = (int) $this->db->insert_id();

		$item_batch = array();
		$item_kit_batch = array();

		// Conform the item array for DB insertion
		foreach($items as $item){
			if(!empty($item['item_id'])){
				$item_batch[] = array('ticket_id'=>$ticket_id, 'item_id'=>$item['item_id'], 'line'=>$item['line']);

			}else if(!empty($item['item_kit_id'])){
				$item_kit_batch[] = array('ticket_id'=>$ticket_id, 'item_kit_id'=>$item['item_kit_id'], 'line'=>$item['line']);
			}
		}

		if(!empty($item_batch)){
			$this->db->insert_batch('table_ticket_items', $item_batch);
		}
		if(!empty($item_kit_batch)){
			$this->db->insert_batch('table_ticket_item_kits', $item_kit_batch);
		}

		return $ticket_id;
	}

	// Retrieve tickets including associated items
	function get($sale_id, $ticket_id_filter = false)
	{
		$this->db->select('t.ticket_id, t.sale_id, t.completed, t.printed, t.message,
			t.date_created, t.date_ordered, t.date_completed, ti.line, t.deleted,
			ti.item_id, i.name', false);
		$this->db->from('table_tickets AS t');
		$this->db->join('table_ticket_items AS ti', 'ti.ticket_id = t.ticket_id', 'left');
		$this->db->join('items AS i', 'ti.item_id = i.item_id', 'left');
		$this->db->where('t.sale_id', $sale_id);
		if(!empty($ticket_id_filter)){
			$this->db->where('t.ticket_id', $ticket_id_filter);
		}
		$this->db->order_by('ti.ticket_id ASC, ti.line ASC');
		$query = $this->db->get();

		$rows = $query->result_array();
		$tickets = array();

		// Loop through ticket items, create multi-dimensional array by ticket
		foreach($rows as $item){
			$ticket_id = (int) $item['ticket_id'];
			$ticket =& $tickets[$ticket_id];

			$ticket['date_created'] = $item['date_created'];
			$ticket['date_ordered'] = $item['date_ordered'];
			$ticket['date_completed'] = $item['date_completed'];
			$ticket['sale_id'] = (int) $item['sale_id'];
			$ticket['ticket_id'] = $ticket_id;
			$ticket['completed'] = (bool) $item['completed'];
			$ticket['deleted'] = (bool) $item['deleted'];
			$ticket['printed'] = (bool) $item['printed'];
			$ticket['message'] = $item['message'];

			// If row is an item, add the item to ticket
			if(!empty($item['item_id'])){
				$ticket['items'][] = array('item_id'=>$item['item_id'],'line'=>$item['line'], 'name'=>$item['name']);
			}
		}

		if(!empty($ticket_id_filter)){
			return $tickets[$ticket_id_filter];
		}

		return array_values($tickets);
	}

	// Delete entire ticket including receipt items associated with it
	function delete($ticket_id){

		if(empty($ticket_id)){
			return false;
		}

		$result = $this->db->query("DELETE t.*, t_item.*, t_item_kit.*
			FROM foreup_table_tickets AS t
			LEFT JOIN foreup_table_ticket_items AS t_item
				ON t_item.ticket_id = t.ticket_id
			LEFT JOIN foreup_table_ticket_item_kits AS t_item_kit
				ON t_item_kit.ticket_id = t.ticket_id
			WHERE t.ticket_id = ".(int) $ticket_id);
		return $result;
	}
}
?>
