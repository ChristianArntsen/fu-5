<?php
class Report_usage extends CI_Model
{
	public function log_usage($report_id, $course_id,$name = null)
	{
		$this->db->insert("reports_usage",[
			"course_id"=>$course_id,
			"report_id"=>$report_id,
			"person_id"=>$this->session->userdata('person_id'),
			"name"=>$name
		]);
	}
}	
