<?php
class Customers_loyalty_package extends CI_Model
{
    function save(&$package_info, $id = false) {
        $package_info['last_updated'] = date("Y-m-d H:i:s");
        $package_info['last_updated_by'] = $this->session->userdata('person_id');

        if (!$id)
        {
            $package_info['created'] = date("Y-m-d H:i:s");
            $package_info['created_by'] = $this->session->userdata('person_id');
            $result = $this->db->insert('customers_loyalty_packages', $package_info);

            if ($result) {
                $package_info['id'] = $this->db->insert_id();
            }

            return $result;
        }
        else
        {
            $this->db->where('id', $id);
            return $this->db->update('customers_loyalty_packages', $package_info);
        }
    }

    function adjust_balance($customer_id, $package_id, $add_subtract) {
        $package = $this->get($customer_id, $package_id)->result_object()[0];
        $new_balance = [
            'loyalty_balance' => $package->loyalty_balance + $add_subtract
        ];
        return $this->save($new_balance, $package->id);
    }

    function get($customer_id, $package_id) {
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('customer_id', $customer_id);
        $this->db->where('loyalty_package_id', $package_id);
        return $this->db->get('customers_loyalty_packages');
    }

    function get_all($customer_id) {
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('customer_id', $customer_id);
        return $this->db->get('customers_loyalty_packages');
    }

    function has_loyalty($customer_id) {
        $customer_info = $this->Customer->get_info($customer_id);
        if ($customer_info->use_loyalty) {
            return true;
        }

        $packages = $this->get_all($customer_id)->result_object();

        return count($packages) > 0;
    }

    function delete($id) {
        $deleted = [
            'date_deleted' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->session->userdata('person_id')
        ];
        return $this->save($deleted, $id);
    }
}
