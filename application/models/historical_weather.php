<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Historical_weather extends CI_Model {

    function get_last_save_date($course_id) {
        $this->db->select('day');
        $this->db->from('weather');
        $this->db->where('course_id',$course_id);
        $this->db->order_by('day DESC');
        $this->db->limit(1);
        $result = $this->db->get()->row_array();
        if (!empty($result['day'])) {
            return date('Y-m-d 00:00:00', strtotime($result['day']));
        } else {
            // First tee time recorded
            return date('Y-m-d 00:00:00', strtotime('-1 year'));
        }
    }

    function save($data) {
        return $this->db->insert('weather', $data);
    }
}