<?php
class Module extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }
	
	function get_module_name($module_id)
	{
		$query = $this->db->get_where('modules', array('module_id' => $module_id), 1);
		
		if ($query->num_rows() ==1)
		{
			$row = $query->row();
			return lang($row->name_lang_key);
		}
		
		return lang('error_unknown');
	}
	
	function get_module_desc($module_id)
	{
		$query = $this->db->get_where('modules', array('module_id' => $module_id), 1);
		if ($query->num_rows() ==1)
		{
			$row = $query->row();
			return lang($row->desc_lang_key);
		}
	
		return lang('error_unknown');	
	}
	
	function get_all_modules()
	{
		$this->db->from('modules');
		$this->db->order_by("sort", "asc");
		return $this->db->get();		
	}
	
	function get_allowed_modules($person_id)
	{
		$this->db->from('modules');
		$this->db->join('permissions','permissions.module_id=modules.module_id');
		$this->db->where("permissions.person_id",$person_id);
		$this->db->order_by("sort", "asc");
		return $this->db->get();		
	}

	function get_3rd_party_modules()
	{
		$course_id = $this->config->item('course_id');
		return $this->db->from("api_user_courses")
			->where("course_id",$course_id)
			->where("is_module",1)
			->get();
	}

	function get_course_modules($person_id = null)
	{
		$modules = $this->get_all_modules()->result_array();
		
		foreach($modules as $key => $module){
			if($this->session->userdata($module['module_id']) != '1'){
				unset($modules[$key]);
			}
		}

		$other_modules = $this->get_3rd_party_modules()->result_array();
		foreach($other_modules as $module){
			$new_module = [
				"name_lang_key"=>$module['module_name'],
				"desc_lang_key"=>$module['module_subheading'],
				"sort"=>200,
				"module_id"=>$module['id'],
				"is_partner_app"=>true
			];
			$modules["partner".$module['id']] = $new_module;
		}

		
		return array_values($modules);	
	}	
}
?>
