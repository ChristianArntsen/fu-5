<?php
use \Carbon\Carbon;
class Personal_messages extends CI_MODEL {
	public function __construct()
	{
		parent::__construct();
		$this->readOnlyDB = $this->load->getDatabaseConnection('slave');
		$this->load->model("Course");
		$course_id = $this->session->userdata('course_id');
		$course_ids = array();
		$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);
		$this->course_ids = $course_ids;
	}

	public function set_course_id($course_id)
	{
		$course_ids = array();
		$this->Course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);
		$this->course_ids = $course_ids;
	}
	public function get_new_messages($timestamp,$person_id)
	{
		$results = $this->readOnlyDB->from("personal_messages")
			->select("personal_messages.*,people.first_name as employee_first_name,people.last_name as employee_last_name")
			->where_in("personal_messages.course_id",$this->course_ids)
			->where("personal_messages.person_id",$person_id)
			->where("personal_messages.timestamp >",$timestamp)
			
			->join("people","personal_messages.employee_id = people.person_id","LEFT")
			->limit(1)->get()->row_array();
		
		$this->mark_as_read([$results]);
		
		return $results;
	}
	public function has_new_messages($timestamp,$person_id)
    {
	    return !empty($this->get_new_messages($timestamp,$person_id));
    }
    public function mark_as_read($messages)
    {
    	if(empty($messages)){
    		return true;
	    }
	    $ids = [];
	    foreach($messages as $result){
	    	if(!empty($result)){
			    $ids[] = $result['id'];
		    }
	    }
	    if(empty($ids)){
	    	return true;
	    }
	    $timestamp = Carbon::now('UTC');

	    $this->db->where_in("id",$ids)
		    ->where("read",0)
		    ->update("personal_messages",["read"=>1,"last_updated"=>$timestamp->toDateTimeString()]);
    }

    public function get_message_history($person_id)
    {

		$results = $this->readOnlyDB->from("personal_messages")
			->select("personal_messages.*,people.first_name as employee_first_name,people.last_name as employee_last_name")
			->where_in("personal_messages.course_id",$this->course_ids)
			->where("personal_messages.person_id",$person_id)
			->order_by("timestamp ASC")
			->join("people","personal_messages.employee_id = people.person_id","LEFT")
			->limit(100)->get()->result_array();
		$this->mark_as_read($results);
		return $results;
    }

    public function has_been_messaged($person_id)
    {
	    $results = $this->readOnlyDB->from("personal_messages")
		    ->where_in("course_id",$this->course_ids)
		    ->where("person_id",$person_id)
		    ->limit(1)->get()->result_array();

	    return count($results) > 0;
    }

    public function save_message($message,$msisdn,$incoming,$employee_id = null)
    {
	    $marketing_texting_row = $this->readOnlyDB->from('marketing_texting')
	        ->where('msisdn',$msisdn)
		    ->where("customers.deleted",0)
		    ->where_in('marketing_texting.course_id',$this->course_ids);
		if(!empty($person_id)){
			$marketing_texting_row->where("person_id",$person_id);
		}
		$marketing_texting_rows = $marketing_texting_row->join("customers","marketing_texting.person_id = customers.person_id AND foreup_marketing_texting.course_id = foreup_customers.course_id")
		    ->get()->result();
	    if(empty($marketing_texting_row)){
	    	return true;
	    }


		$timestamp = Carbon::now('UTC');


    	$this->load->model("Customer");

    	foreach($marketing_texting_rows as $marketing_texting_row){
		    $customer = $this->Customer->get_info($marketing_texting_row->person_id,$marketing_texting_row->course_id);
		    if(empty($customer->person_id)){
			    throw new Exception("Can't find customer. ".$marketing_texting_row->person_id." - ".implode(",",$this->course_ids)."");
		    }
		    $this->db->insert("personal_messages",[
			    "course_id"=>$customer->course_id,
			    "person_id"=>$customer->person_id,
			    "message"=>$message,
			    "incoming"=>$incoming,
			    'timestamp'=>$timestamp->toDateTimeString(),
			    'last_updated'=>$timestamp->toDateTimeString(),
			    'employee_id'=>$employee_id
		    ]);
	    }

		return true;
    }

	public function get_threads($timestamp = null)
	{
		$course_ids = implode(",",$this->course_ids);
		
		if(isset($timestamp) && $timestamp){
			$timestamp = Carbon::parse($timestamp);
			$timestamp = "AND pm.last_updated > '".$timestamp->toDateTimeString()."'";
		} else {
			$timestamp = "";
		}
		
		$results = $this->readOnlyDB->query("
		SELECT p.first_name,p.last_name,p.cell_phone_number,p.phone_number,pm.* FROM `foreup_personal_messages` pm
			JOIN (
			    SELECT pm2.id,pm2.course_id,pm2.person_id,pm2.message,pm2.incoming, MAX(`last_updated`) AS most_recent
			      FROM `foreup_personal_messages` pm2
			     WHERE pm2.course_id  IN ($course_ids)
			     GROUP BY pm2.course_id,pm2.person_id
			) R ON R.person_id = pm.person_id AND R.course_id = pm.course_id AND pm.last_updated = R.most_recent
			LEFT JOIN foreup_people p ON pm.person_id = p.person_id
			WHERE pm.course_id  IN ($course_ids) AND pm.ignore=0 $timestamp
 			GROUP BY pm.course_id,pm.person_id
 			ORDER BY pm.last_updated DESC
		")->result_array();
		return $results;
	}
	
	public function ignore_threads($person_id,$data)
	{
			if(empty($person_id)){
			return false;
		}
		$this->db->where_in('personal_messages.course_id',$this->course_ids);
		$this->db->where("person_id",$person_id);
		
		
		$timestamp = Carbon::now('UTC');
		
		$this->db->update("personal_messages",[
			"ignore"=>1,"last_updated"=>$timestamp->toDateTimeString()
		]);
		
		return true;
	}
}
?>