<?php
class Report_Exports extends CI_Model
{



	public function __construct()
	{
		$this->load->model('Report_column');
	}

	public function get_info($name)
	{
		$this->db->from('report_exports');
		$this->db->where('name',$name);
		$row = $this->db->get()->row();

		return $row;
	}

	function exists($name)
	{
		$this->db->from('report_exports');
		$this->db->where("name", $name);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	function create($data)
	{

		$this->db->insert('report_exports',[
			"name"=>$data['name'],
			"course_id"=>$this->session->userdata('course_id'),
			"human_readable_name"=>$data['human_readable_name'],
			"last_downloaded_by"=>$this->session->userdata('person_id'),
			"created_by"=>$this->session->userdata('person_id')
		]);
		$id = $this->db->insert_id();
		return $id;
	}
	function mark_as_downloaded($name)
	{
		$this->db->where("name",$name);
		$this->db->update('report_exports',[
			"last_downloaded_by"=>$this->session->userdata('person_id')
		]);
	}
}
?>
