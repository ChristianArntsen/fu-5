<?php
class Marketing_campaign extends CI_Model
{
	/*
	Determines if a given campaign_id is an campaign
	*/
	function exists( $campaign_id )
	{
		$this->db->from('marketing_campaigns');
		$this->db->where('campaign_id',$campaign_id);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the campaigns
	*/
	function get_all($limit=10000, $offset=0)
	{
		$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->from('marketing_campaigns');
		$this->db->where("deleted = 0 $course_id");
		$this->db->where('version',1);
		$this->db->order_by("send_date", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}

	function get_by_status($status='sent',$limit=10000, $offset=0, $version=1,$course=false,$reverse = false)
	{
		if(!$course || $_SESSION['foreup']['user_level']*1!==5)
			$course = $this->session->userdata('course_id');
		$course_id = "course_id = '{$course}'";
		$this->db->from('marketing_campaigns');
		$this->db->where("$course_id");
		$this->db->where("deleted = 0");
		$this->db->where("status =  '".$status."'");
		$this->db->where("version =  '".$version."'");
		if($reverse)
			$this->db->order_by("send_date", "asc");
		else
			$this->db->order_by("send_date", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}

	function get_customer_stats($emails)
    {
        if (empty($emails)) {
            return array('recent_campaigns'=>[], 'last_open_campaigns'=>[]);
        }

        $course_id = $this->session->userdata('course_id');
        $this->db_datastore = $this->load->database('datastore', TRUE);
        // Collect campaigns processed
        $this->db_datastore->from("sendgrid_events")
            ->where("course_id",$course_id)
            ->where("marketing_campaign_id > 0")
            ->where("event","processed")
            ->where_in("email", $emails)
            ->order_by('email')
            ->order_by('timestamp', 'DESC');

        $processed_results = $this->db_datastore->get()->result_array();
        $marketing_campaign_ids = [];
        foreach($processed_results as $result)
        {
            $marketing_campaign_ids[$result['email']][] = $result['marketing_campaign_id'];
        }

        $recent_campaigns = [];
        foreach($marketing_campaign_ids as $email => $campaign_ids)
        {
            $this->db->from('marketing_campaigns');
            $this->db->where_in("campaign_id", $campaign_ids);
            $this->db->order_by("send_date", "desc");

            $recent_campaigns[$email] = $this->db->get()->result_array();
        }

        // Collect last campaign opened
        $this->db_datastore->from("sendgrid_events")
            ->where("course_id",$course_id)
            ->where("marketing_campaign_id > 0")
            ->where("event","open")
            ->where_in("email", $emails)
            ->group_by('email')
            ->order_by('timestamp', 'DESC');

        $open_results = $this->db_datastore->get()->result_array();

        $last_open_campaigns = [];
        foreach($open_results as $result)
        {
            $this->db->from('marketing_campaigns');
            $this->db->where("campaign_id", $result['marketing_campaign_id']);

            $last_open_campaigns[$result['email']] = $this->db->get()->row_array();
        }

        return array('recent_campaigns'=>$recent_campaigns, 'last_open_campaigns'=>$last_open_campaigns);
    }

	function count_all()
	{
		$this->db->from('marketing_campaigns');
		$this->db->where("deleted", 0);
		$this->db->where('version',1);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular campaign
	*/
	function get_info($campaign_id, $cron = false)
	{
		if (function_exists('newrelic_disable_autorum'))
			newrelic_disable_autorum();
	
		if (!$cron && $_SESSION['foreup']['user_level']*1!==5)
			$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->from('marketing_campaigns');
		$this->db->where('campaign_id',$campaign_id);
		$this->db->where("deleted", 0);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			$row = $query->row();
			if(!empty($row->images)){
				$row->images = json_decode($row->images, true);
			}
			return $row;
		}
		else
		{
			//Get empty base parent object, as $campaign_id is NOT an campaign
			$campaign_obj=new stdClass();

			//Get all the fields from campaigns table
			$fields = $this->db->list_fields('marketing_campaigns');

			foreach ($fields as $field)
			{
				$campaign_obj->$field='';
			}

			return $campaign_obj;
		}
	}

	function remove_report_from_campaign($campaign_id,$report_id)
	{
		$this->db->delete('marketing_recipient_reports', [
				"marketing_campaign_id"=>$campaign_id,
				"report_id"=>$report_id
		]);
	}

	function get_reports_for_campaign($campaign_id)
	{
		$this->db->select(["marketing_recipient_reports.report_id","marketing_recipient_reports.marketing_campaign_id","reports.title"]);
		$this->db->from('marketing_recipient_reports');
		$this->db->join('reports',"marketing_recipient_reports.report_id = reports.id","LEFT");
		$this->db->where('marketing_campaign_id',$campaign_id);

		$query = $this->db->get();

		return $query->row();
	}
	function add_report_to_campaign($campaign_id,$report_id)
	{
		$this->db->insert("marketing_recipient_reports",[
			"marketing_campaign_id"=>$campaign_id,
			"report_id"=>$report_id
		]);
	}

	/*
	Gets information about multiple campaigns
	*/
	function get_multiple_info($campaign_ids)
	{
		$this->db->from('marketing_campaigns');
		$this->db->where_in('campaign_id',$campaign_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("send_date", "desc");
		return $this->db->get();
	}

	/*
	 * Checks to see if the account has sufficient credits
	*/
	function has_sufficient_credits($type, $campaign_count, $campaign_id = false)
	{
		$limits = $this->Billing->get_monthly_limits($this->session->userdata('course_id'));
		$this->load->model('Communication');
		$stats = $this->Communication->get_stats($this->session->userdata('course_id'));
		$scheduled_credits = $this->get_scheduled_credits($type, $campaign_id);
		//return true;
		return ($limits[$type.'_limit'] >= ($stats[$type.'s_mk_this_month'] + $campaign_count + $scheduled_credits));
	}


	/*
	 * Gets a count for the scheduled campaigns
	*/
	function get_scheduled_credits($type, $campaign_id = false)
	{
		$this->db->select('SUM(recipient_count) AS recipient_count');
		$this->db->from('marketing_campaigns');
		$this->db->where('queued', 1);
		$this->db->where('deleted', 0);
		$this->db->where('is_sent', 0);
		$this->db->where('status', "scheduled");
		$this->db->where('type', $type);
		if ($campaign_id)
			$this->db->where('campaign_id !=', $campaign_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));

		$result = $this->db->get();
		//echo $this->db->last_query();
		//return 5;
		$result = $result->result_array();
		return $result[0]['recipient_count'];
	}
	/*
	Inserts or updates a campaign
	*/
	function save(&$campaign_data,$campaign_id=false)
	{
		if(!empty($campaign_data['images']) && is_array($campaign_data['images'])){
			$campaign_data['images'] = json_encode($campaign_data['images']);
		}else{
			$campaign_data['images'] = '';
		}

		if (!$campaign_id or !$this->exists($campaign_id))
		{
			if($this->db->insert('marketing_campaigns',$campaign_data))
			{
				$campaign_data['campaign_id']=$this->db->insert_id();
				//An insert should return the new id if possible, is there a better way?
				return $campaign_data;
			}
			return false;
		}

		$this->db->where('campaign_id', $campaign_id);
		return $this->db->update('marketing_campaigns',$campaign_data);
	}

	function save_reminder($time,$unit,$campaign_id,$type)
	{
		$this->load->model("reminder");
		//Check if reminder exists then update
		//else insert new reminder
		$existing_reminder = $this->db->from("foreup_reminders")
			->where("campaign_id",$campaign_id)
			->count_all();
		if($existing_reminder > 0){
			$this->db->where("campaign_id",$campaign_id);
			$this->db->update("reminders",[
				"timebefore"=>$time,
				"timebefore_unit"=>$unit
			]);
			return;
		}

		$reminder_model = new Reminder();
		$id = $reminder_model->insert([
			"course_id"=>$this->session->userdata('course_id'),
			"campaign_id"=>$campaign_id,
			"type"=>$type,
			"timebefore"=>$time,
			"timebefore_unit"=>$unit
		]);


		return $id;
	}
    /*
        Inserts or updates a campaign
        */
    function saveAsTemplate($campaign_id)
    {
        if(!empty($campaign_data['images']) && is_array($campaign_data['images'])){
            $campaign_data['images'] = json_encode($campaign_data['images']);
        }else{
            $campaign_data['images'] = '';
        }

        if (!$campaign_id or !$this->exists($campaign_id))
        {
            if($this->db->insert('marketing_campaigns',$campaign_data))
            {
                $campaign_data['campaign_id']=$this->db->insert_id();
                //An insert should return the new id if possible, is there a better way?
                return $campaign_data;
            }
            return false;
        }

        $this->db->where('campaign_id', $campaign_id);
        return $this->db->update('marketing_campaigns',$campaign_data);
    }


	/*
	Updates multiple campaigns at once
	*/
	function update_multiple($campaign_data,$campaign_ids)
	{
		$this->db->where_in('campaign_id',$campaign_ids);
		return $this->db->update('marketing_campaigns',$campaign_data);
	}

	/*
	Deletes one campaign
	*/
	function delete($campaign_id)
	{
		$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("campaign_id = '$campaign_id' $course_id");
		return $this->db->update('marketing_campaigns', array('deleted' => 1));
	}

	/*
	Deletes a list of campaigns
	*/
	function delete_list($campaign_ids)
	{
		$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where_in('campaign_id',$campaign_ids);
		return $this->db->update('marketing_campaigns', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find campaigns
	*/
	function get_search_suggestions($search,$limit=25)
	{
        $suggestions = array();

		$this->db->from('marketing_campaigns');
		$this->db->like('lower(name)', strtolower($search));
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where("deleted", 0);
		$this->db->order_by("campaign_id", "asc");
		$by_number = $this->db->get();

		//$suggestions[] = array('label'=>$this->db->last_query());
		foreach($by_number->result() as $row)
		{
			$suggestions[]=array('label' => $row->name, 'value'=>$row->campaign_id);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on campaigns
	*/
	function search($search, $limit=20, $offset = 0)
	{
	//	$course_id = '';
    //if (!$this->permissions->is_super_admin())
      $pr = $this->db->dbprefix('marketing_campaigns');
      $course_id = "AND {$pr}.course_id = '{$this->session->userdata('course_id')}'";
	  $this->db->from('marketing_campaigns');
	  $this->db->like('lower(name)', strtolower($search));
	  $this->db->where("deleted = 0 $course_id");
	  $this->db->order_by("campaign_id", "asc");
	  // Just return a count of all search results
      if ($limit == 0)
          return $this->db->get()->num_rows();
      // Return results
      $this->db->offset($offset);
      $this->db->limit($limit);
      return $this->db->get();
	}

	public function get_campaign_value( $campaign_number )
	{
		if ( !$this->exists( $this->get_campaign_id($campaign_number)))
			return 0;

		$this->db->from('marketing_campaigns');
		$this->db->where('campaign_id',$campaign_number);
		return $this->db->get()->row()->value;
	}

	function update_campaign_value( $campaign_number, $value )
	{
		$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("campaign_id = '$campaign_number' $course_id");
		$this->db->update('marketing_campaigns', array('value' => $value));
	}

  function get_all_unsent_campaigns()
  {
    $this->db->from('marketing_campaigns');
    // this will filter out all campaigns that are not sent on that hour or before
		$this->db->where("send_date_cst <", date("Y-m-d H:i:s"));
		$this->db->where('is_sent', 0);
		$this->db->where('status !=', "draft");
		$this->db->where('status !=', "reminder");
		$this->db->where('status !=', "recurring");
		$this->db->where('deleted', 0);
		$this->db->where("queued", 1);
		$this->db->where("attempts <", 1);
		$this->db->order_by("send_date", "desc");
		$this->db->limit(100);
		$this->db->offset(0);
		return $this->db->get()->result_object();
  }
  function mark_attempted($campaign_id)
  {
  		$this->db->query("
  			UPDATE foreup_marketing_campaigns
  			SET attempts = 1
  			WHERE campaign_id = $campaign_id");
  }

	/**
	 * @function send_mails utility function to send batch mails
	 * @param Int $campaign_id the campaign id to be updated
	 * @param array $mails list of mail recipients
	 */
	function send_mails($campaign_id, array $mails, $contents, $course, $subject = '')
	{
		log_message('error', 'starting marketing_campaign->send_mails campaign_id - ' . $campaign_id);

		$campaign_obj = $this->get_info($campaign_id);
		// send mails here
		$subject = $subject != '' ? $subject : $course->name;
		if($campaign_obj->from_email){
			$from = $campaign_obj->from_email;
		} else {
			$from = $course->email;
		}
		if($campaign_obj->from_name){
			$fromName = $campaign_obj->from_name;
		} else {
			$fromName = $course->name;
		}



		$campaign_obj = $this->get_info($campaign_id);
		$promotion_id = $campaign_obj->promotion_id;

		$discout_type = '';


		$just_emails = array();

		foreach ($mails as $mail) {
			if (filter_var($mail['email'], FILTER_VALIDATE_EMAIL)) {
				$just_emails[] = $mail['email'];
			}
		}
		$this->load->helper("email_helper");
		log_message('error', 'marketing_campaign->send_mails email campaign_id - ' . $campaign_id . ' count ' . count($just_emails));
		send_sendgrid($just_emails, $subject, $contents, $from, $fromName, $campaign_id, $course->course_id);

		$this->db->where('campaign_id', $campaign_id);
		$this->db->update('marketing_campaigns', array('is_sent' => 1, 'status' => 'sent', 'send_date' => date('Y-m-d H:i:s')));
		//log_message('error', 'FINEE');
	}


  /**
   * @function opt_out this utility function will handle opt out request
   * @param String $type
   * @param Integer $customer_id
   * @param Integer $campaign_id
   * @param Integer $course_id
   */
  function opt_out($type, $customer_id, $campaign_id, $course_id)
  {
    $type = strtolower($type);

    $this->db->where('person_id', $customer_id);
    $this->db->update('customers', array("opt_out_{$type}" => 'Y'));

    $data = array(
      'customer_id' => $customer_id,
      'campaign_id' => $campaign_id,
      'course_id' => $course_id,
      'action' => "opt out by {$type}",
      'action_date' => date('Y-m-d H:i:s')
    );

    $this->db->insert('subscription_actions', $data);
  }
  /**
   * Utility function to get status count of marketing email and texts
   * returns an array of status
   */
  function get_status_count()
  {
    $course_id = '';
    //if (!$this->permissions->is_super_admin())
    {
      $pr = $this->db->dbprefix('marketing_campaigns');
      $course_id = "AND {$pr}.course_id = '{$this->session->userdata('course_id')}'";
    }
    $this->db->select("type , is_sent, COUNT( * ) count");
    $this->db->from('marketing_campaigns');
		$this->db->where("deleted = 0 $course_id");
    $this->db->group_by(array("type", "is_sent"));
    $res = $this->db->get();

    $data = array(
        'email_sent' => 0,
        'email_remaining' => 0,
        'text_sent' => 0,
        'text_remaining' => 0
    );

    if($res->num_rows() >0)
    {
       foreach($res->result_array() as $res)
       {
         if($res['type'] == 'email')
         {
           if($res['is_sent'] == 1) $data['email_sent'] = $res['count'];
           if($res['is_sent'] == 0) $data['email_remaining'] = $res['count'];

         }
         else
         {
           if($res['is_sent'] == 1) $data['text_sent'] = $res['count'];
           if($res['is_sent'] == 0) $data['text_remaining'] = $res['count'];
         }

       }
    }

    return $data;
  }

	public function getHtmlByHash($hash)
	{
		$this->db->from('marketing_campaigns');
		$this->db->where('remote_hash',$hash);

		$this->db->select('rendered_html');
		$this->db->limit(1);
		$result = $this->db->get();
		return $result->result_array();
	}
	public function phantomJSGet($campaign_id)
	{
		//This method gives full access to every campaign so that phantomjs can view them all
		//That's why only users from 127.0.0.1 can use this method, phantomjs should be the only
		//one ever
		//if($this->session->userdata('ip_address') != "127.0.0.1")
		//	return;
		$this->db->from('marketing_campaigns');
		$this->db->where('campaign_id',$campaign_id);

		$this->db->select('json_content,logo,logo_align,remote_hash,course_id');
		$this->db->limit(1);
		$result = $this->db->get();
		return $result->result_array();
	}
	public function getTemplateInfo($campaign_id)
	{
		$this->db->select('marketing_templates.*');
		$this->db->join('marketing_templates', 'marketing_templates.id = marketing_campaigns.marketing_template_id');
		$this->db->from('marketing_campaigns');
		$this->db->where('campaign_id',$campaign_id);
		$this->db->where('marketing_campaigns.course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		$result = $this->db->get();

		if(!$result){
			return false;
		}

		$result = $result->result_array();
		return $result[0];
	}

    public function getUsageStats($course_id)
    {
		$limits = $this->Billing->get_monthly_limits($this->session->userdata('course_id'));

		$date = \Carbon\Carbon::now();

		$this->db_datastore = $this->load->database('datastore', TRUE);
        $this->db_datastore->from("sendgrid_events")
            ->where("course_id",$course_id)
            ->where("timestamp >= '{$date->startOfMonth()}'")
            ->where("timestamp <= '{$date->endOfMonth()}'")
			->where("event","processed")
            ->select("count(*) as count");

        $result = $this->db_datastore->get()->row();
		$returnObject["email"] = [
			"usage" => $result->count*1,
			"limit"=>$limits["email_limit"]*1
		];

        if(!$result)
            return false;

		$this->db_datastore->from("sms_logs")
			->where("course_id",$course_id)
			->where("timestamp >= '{$date->startOfMonth()}'")
			->where("timestamp <= '{$date->endOfMonth()}'")
			->select("count(*) as count");

		$result = $this->db_datastore->get()->row();
		$returnObject["text"] = [
			"usage" => $result->count*1,
			"limit"=>$limits["text_limit"]*1
		];


        return $returnObject;
    }
	/*
	 * The search endpoint and MRL library return completely different
	 * user objects that makes it impossible to use together.
	 * This method takes a list of ids, either user or group id's and
	 * returns the same type of object as search does
	 */
	public function convertIdsToSearchResults(array $ids,$type = "customers")
	{
		if(!isset($ids[0])){
			return array();
		}
		$niceResults = array();
		if($type == "groups"){
			$this->db->from('customer_groups');
			$this->db->where_in('group_id',$ids);
			if ($_SESSION['foreup']['user_level']*1!==5)
			  $this->db->where('course_id', $this->session->userdata('course_id'));
			$result = $this->db->get();
			if(!$result)
				return false;
			$result = $result->result_array();
			foreach($result as $row){
				$niceResults[] = array(
					"label"=>$row['label'],
					"is_group"=>true,
					"value"=>$row['group_id']
				);
			}

			//The everyone group is not a group so add it manually
			if(in_array (0,$ids)){
				$niceResults[] = array(
					"label"=>"Everyone",
					"value"=>0,
					"is_group"=>true
				);
			}
		} else {
			$this->db->select('first_name,last_name,person_id,email');
			$this->db->from('people');
			$this->db->where_in('person_id',$ids);
			$result = $this->db->get();
			if(!$result)
				return false;
			$result = $result->result_array();
			foreach($result as $row){
				$niceResults[] = array(
					"label"=>$row['first_name']." ".$row['last_name'],
					"value"=>$row['person_id'],
					"email"=>$row['email']
				);
			}
		}


		return $niceResults;
	}

	/**
	 * @param $campaign_id
	 * @return models\MarketingCampaigns\Row
	 */
	public function getRowModelById($campaign_id)
	{
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->from('marketing_campaigns');
		$this->db->where('campaign_id',$campaign_id);
		$this->db->where("deleted", 0);

		$query = $this->db->get();

		$row = $query->result("models\\MarketingCampaigns\\Row");
		if(count($row) > 0){
			$row = $row[0];
		}
		return $row;

	}
}
