<?php
class Acl_roles extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function set_role_from_userlevel($user_level,$person_id)
	{
		$names = [
			1=>"Employee",
			2=>"Manager",
			3=>"Admin",
			4=>"Super Admin",
		];
		if(!isset($names[$user_level])){
			return false;
		}

		$results = $this->db->from("acl_roles")
			->where("role_name",$names[$user_level])
			->where("is_standard_role",1)
			->get()->result_array();
		if(count($results) == 0){
			return false;
		}

		return $this->set_acl_role($person_id,$results[0]['role_id']);
	}

	function set_acl_role($person_id,$role_id)
	{
		$this->db->from("acl_roles_employees");
		$this->db->where("employee_id",$person_id);
		$this->db->delete();

		$this->db->insert("acl_roles_employees",[
			"employee_id"=>$person_id,
			"role_id"=>$role_id,
		]);

        return $role_id;
	}

    function save_employee_acl_role($employee_id, $role_id, $data = []){

        if(empty($employee_id) || empty($role_id)){
            return false;
        }
        $data = elements(['hourly_rate'], $data, null);

        if(!empty($data['hourly_rate'])){
            $data['hourly_rate'] = preg_replace('/[^.0-9]/', '', $data['hourly_rate']);
        }

        $this->db->where('employee_id', $employee_id);
        $this->db->where('role_id', $role_id);
        $this->db->update("acl_roles_employees", $data);

        return $this->db->affected_rows();
    }

	function add_acl_role($person_id,$role_id)
	{
		return $this->db->insert("acl_roles_employees",[
			"employee_id"=>$person_id,
			"role_id"=>$role_id,
		]);
	}

	function get_acl_roles($params = []){

	    $course_id = (int) $this->session->userdata('course_id');

        $this->db->from("acl_roles");
        $this->db->where("(foreup_acl_roles.course_id IS NULL OR foreup_acl_roles.course_id = {$course_id})");

        if(isset($params['employee_id'])){
            $this->db->join('acl_roles_employees', 'acl_roles_employees.role_id = acl_roles.role_id', 'inner');
            $this->db->where('acl_roles_employees.employee_id', (int) $params['employee_id']);
        }

        if(isset($params['is_standard_role'])){
            $this->db->where('is_standard_role', (int) $params['is_standard_role']);
        }

        return $this->db->get()->result_array();
    }

	function remove_acl_role($person_id,$role_id)
	{
		$this->db->from("acl_roles_employees");
		$this->db->where("employee_id",$person_id);
		$this->db->where("role_id",$role_id);
		return $this->db->delete();
	}

	function get_all_users_permissions($person_id)
    {

    }
}