<?php
class Payment extends CI_Model
{
	function exists($payment_type, $sale_id)
	{
		$this->db->from('sales_payments');
		$this->db->where("payment_type", $payment_type);
		$this->db->where('sale_id', $sale_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	function get_all($sale_id)
	{
		$this->db->from('sales_payments');
		$this->db->where('sale_id', $sale_id);
		return $this->db->get();
	}
	function get($sale_id, $type = null)
	{
		$this->db->select('payment_type AS type, payment_amount AS amount, invoice_id, tip_recipient');
		$this->db->from('sales_payments');
		$this->db->where('sale_id', $sale_id);
		$this->db->where('payment_type', $type);
		return $this->db->get()->row_array();
	}

	function record_tip_transaction($payment,$course_id = null){
		if(!isset($course_id))$course_id = $this->session->userdata('course_id');
		$transaction = new \fu\tips\TipTransaction($course_id);
		if(empty($payment['tip_recipient']))return false; // figure how to handle this. Net tips payable acct?
		$emp_id = $this->session->userdata('person_id');

		$transaction->process_single_employee_tip($payment,$emp_id);
	}

	function add($sales_payments_data)
	{
		if ($sales_payments_data['sale_id'])
		{
	//		$this->db->query("UPDATE foreup_sales SET payment_type = CONCAT(payment_type, '{$sales_payments_data['payment_type']}: \${$sales_payments_data['payment_amount']}<br/>') WHERE sale_id = {$sales_payments_data['sale_id']}");
//			if ($this->exists($sales_payments_data['payment_type'], $sales_payments_data['sale_id']))
			if (!$this->exists($sales_payments_data['payment_type'], $sales_payments_data['sale_id']))
			{
				//$this->db->ignore();
	        	if($this->db->insert('sales_payments',$sales_payments_data) && $this->update_sales_payments($sales_payments_data['sale_id']))
				{
					if(isset($sales_payments_data['is_tip'])&&$sales_payments_data['is_tip']){
						// record a tip
						$this->record_tip_transaction($sales_payments_data);
					}
					return true;
				}
				return false;
			}

			$this->db->where('sale_id', $sales_payments_data['sale_id']);
			$this->db->where('payment_type', $sales_payments_data['payment_type']);
			if($this->db->update('sales_payments', $sales_payments_data)  && $this->update_sales_payments($sales_payments_data['sale_id'])){
				if(isset($sales_payments_data['is_tip'])&&$sales_payments_data['is_tip']){
					$this->record_tip($sales_payments_data);
				}
				return true;
			};
		}
		else {
			return false;
		}
	}
	function update_sales_payments($sale_id)
	{
		$payment_info = '';
		$payments = $this->get_all($sale_id);
		foreach ($payments->result_array() as $payment)
		{
			$payment_info .= "{$payment['payment_type']}: \${$payment['payment_amount']}<br/>";
		}
		$this->db->where('sale_id',$sale_id);
		return $this->db->update('sales', array('payment_type'=>$payment_info));
	}
}
?>
