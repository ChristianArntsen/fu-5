<?php
class Report_tables extends CI_Model
{

	public function __construct()
	{
	}

	public function get_info($id)
	{
		$this->db->from('report_tables');
		$this->db->where('id',$id);
		$row = $this->db->get()->row();

		return $row;
	}
	public function get_by_tablename($tablename,$status = null)
	{
		$this->db->from('report_tables');
		$this->db->where('table_name',$tablename);
		if($status){
			$this->db->where("status =",$status);
		}
		$row = $this->db->get()->row();

		return $row;
	}

	function exists($table_name,$status = "open")
	{
		$this->db->from('report_tables');
		$this->db->where("table_name", $table_name);
		$this->db->where("status =",$status);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	function update($id,$data)
	{
		$this->db->where('id', $id);
		$this->db->update('report_tables',$data);
		return true;
	}

	function save ($data)
	{
		$this->db->insert('report_tables',$data);
		$id = $this->db->insert_id();


		return $id;
	}
	
	function delete_by_tablename($tablename)
	{
		$data = [
			"status"=>"deleted"
		];
		$this->db->where('table_name', $tablename);
		$this->db->update('report_tables',$data);
		return true;
	}

	function get_all_old()
	{
		$this->db->from('report_tables');
		$timestamp = \Carbon\Carbon::now("America/Chicago");
		$timestamp->subMinute(30);
		$this->db->where("timestamp <=",$timestamp->toDateTimeString());
		$this->db->where("status =","open");
		return $this->db->get()->result();
	}

}
?>
