<?php
class Course_information extends CI_Model {
	
	function __construct(){
		parent::__construct();	
	}

	public function get($course_id)
	{
		$this->db->select("courses_information.*,concat(sales_rep.first_name,\" \",sales_rep.last_name) as sales_rep_name,concat(mobile_rep.first_name,\" \",mobile_rep.last_name) as mobile_rep_name",false);
		$this->db->from('courses_information');
		$this->db->where("course_id = '$course_id'");
		$this->db->join("people AS sales_rep","courses_information.sales_rep = sales_rep.person_id","left");
		$this->db->join("people AS mobile_rep","courses_information.mobile_rep = mobile_rep.person_id","left");
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows()==1){
			return $query->row();
		} else {
			$this->db->insert("courses_information",["course_id"=>$course_id]);
			return $this->get($course_id);
		}
	}

	public function save($course_id,$data)
	{
		$this->db->where("course_id = '$course_id'");
		$data['course_id']=$course_id;
		$this->db->insert_on_duplicate_update_batch('courses_information',$data);
	}
}