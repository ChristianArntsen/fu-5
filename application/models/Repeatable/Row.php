<?php
namespace models\Repeatable;

use fu\rrule\RRule;

class Row
{
	public $id,$type,$resource_id,$last_ran,$next_occurence;
	public $FREQ,$DTSTART,$INTERVAL,$COUNT,$UNTIL,$BYMONTH,$BYWEEKNO,$BYYEARDAY;
	public $BYMONTHDAY,$BYDAY,$BYHOUR,$BYMINUTE,$BYSECOND,$BYSETPOS;


	/**
	 * @var \RRule\RRule
	 */
	private $rrule;

	public function createFromString($rrule)
	{
		$rule_array = RRule::parseRfcString($rrule);
		foreach ($rule_array as $key => $value)
		{
			$this->$key = $rule_array[$key];
		}
	}
	public function createFromArray($rule_array){
		foreach ($rule_array as $key => $value)
		{
			$key2 = strtoupper($key);
			$this->$key2 = $rule_array[$key];
		}
	}
	private function generateRRule()
	{
		$validParameters = [
				'FREQ','DTSTART','INTERVAL','COUNT','UNTIL','BYMONTH','BYWEEKNO','WKST',
				'BYYEARDAY','BYMONTHDAY','BYDAY','BYHOUR','BYMINUTE','BYSECOND','BYSETPOS'
		];
		$rrule_array = [];
		foreach ($validParameters as $key) {
			if(isset($this->$key)){
				$rrule_array[$key] = $this->$key;
			}
		}
		$this->rrule = new RRule($rrule_array);
		return $this->rrule;
	}

	public function getLastCalculatedOccurrence($time = null)
	{
		$this->generateRRule();
		date_default_timezone_set("UTC");
		if(isset($time)){
			$current_time = new \Carbon\Carbon($time);
		}else {
			$current_time = \Carbon\Carbon::now();
			$current_time->addHour();
			$current_time->minute = 0;
		}

		$start = isset($this->DTSTART) && $this->DTSTART ? $this->DTSTART : '2000-12-12 12:12:12';
		$occurrences = $this->rrule->getOccurrencesBetween($start, $current_time->format("Y-m-d H:i:s"));
		if(!count($occurrences))return null;
		return array_pop($occurrences);
	}

	public function getNextCalculatedOccurrence($time = null)
	{
		//If the start date is in the past, we want the next occurence relative to today
		$this->generateRRule();
		date_default_timezone_set("UTC");
		if(isset($time)){
			$current_time = new \Carbon\Carbon($time);
		}else {
			$current_time = \Carbon\Carbon::now();
			$current_time->addHour();
			$current_time->minute = 0;
		}
		$until = isset($this->UNTIL) && $this->UNTIL ? $this->UNTIL : '2118-12-12 12:12:12';
		$occurrences = $this->rrule->getOccurrencesBetween($current_time->format("Y-m-d H:i:s"),$until);
		//$next = $this->rrule->next();
		if(isset($occurrences[0]))
			return $occurrences[0];
		else
			return null;
	}
	public function markAsRan()
	{
		$this->last_ran = date("Y-m-d H:i:s");
		$this->next_occurence_date = $this->getNextCalculatedOccurrence();
		$this->next_occurence = $this->next_occurence_date->format("Y-m-d H:i:s");
		return true;
	}
	public function getNextOccurenceDate()
	{
		return $this->next_occurence_date;
	}
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param mixed $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return mixed
	 */
	public function getResourceId()
	{
		return $this->resource_id;
	}

	/**
	 * @param mixed $resource_id
	 */
	public function setResourceId($resource_id)
	{
		$this->resource_id = $resource_id;
	}

	/**
	 * @return mixed
	 */
	public function getLastRan()
	{
		return $this->last_ran;
	}

	/**
	 * @param mixed $last_ran
	 */
	public function setLastRan($last_ran)
	{
		$this->last_ran = $last_ran;
	}

	/**
	 * @return mixed
	 */
	public function getNextOccurence()
	{
		return $this->next_occurence;
	}

	/**
	 * @param mixed $next_occurence
	 */
	public function setNextOccurence($next_occurence)
	{
		$this->next_occurence = $next_occurence;
	}

	/**
	 * @return mixed
	 */
	public function getDTSTART()
	{
		return $this->DTSTART;
	}

	/**
	 * @param mixed $DTSTART
	 */
	public function setDTSTART($DTSTART)
	{
		$this->DTSTART = $DTSTART;
	}

	/**
	 * @return mixed
	 */
	public function getINTERVAL()
	{
		return $this->INTERVAL;
	}

	/**
	 * @param mixed $INTERVAL
	 */
	public function setINTERVAL($INTERVAL)
	{
		$this->INTERVAL = $INTERVAL;
	}

	/**
	 * @return mixed
	 */
	public function getCOUNT()
	{
		return $this->COUNT;
	}

	/**
	 * @param mixed $COUNT
	 */
	public function setCOUNT($COUNT)
	{
		$this->COUNT = $COUNT;
	}

	/**
	 * @return mixed
	 */
	public function getUNTIL()
	{
		return $this->UNTIL;
	}

	/**
	 * @param mixed $UNTIL
	 */
	public function setUNTIL($UNTIL)
	{
		$this->UNTIL = $UNTIL;
	}

	/**
	 * @return mixed
	 */
	public function getBYMONTH()
	{
		return $this->BYMONTH;
	}

	/**
	 * @param mixed $BYMONTH
	 */
	public function setBYMONTH($BYMONTH)
	{
		$this->BYMONTH = $BYMONTH;
	}

	/**
	 * @return mixed
	 */
	public function getBYWEEKNO()
	{
		return $this->BYWEEKNO;
	}

	/**
	 * @param mixed $BYWEEKNO
	 */
	public function setBYWEEKNO($BYWEEKNO)
	{
		$this->BYWEEKNO = $BYWEEKNO;
	}

	/**
	 * @return mixed
	 */
	public function getBYYEARDAY()
	{
		return $this->BYYEARDAY;
	}

	/**
	 * @param mixed $BYYEARDAY
	 */
	public function setBYYEARDAY($BYYEARDAY)
	{
		$this->BYYEARDAY = $BYYEARDAY;
	}

	/**
	 * @return mixed
	 */
	public function getBYMONTHDAY()
	{
		return $this->BYMONTHDAY;
	}

	/**
	 * @param mixed $BYMONTHDAY
	 */
	public function setBYMONTHDAY($BYMONTHDAY)
	{
		$this->BYMONTHDAY = $BYMONTHDAY;
	}

	/**
	 * @return mixed
	 */
	public function getBYDAY()
	{
		return $this->BYDAY;
	}

	/**
	 * @param mixed $BYDAY
	 */
	public function setBYDAY($BYDAY)
	{
		$this->BYDAY = $BYDAY;
	}

	/**
	 * @return mixed
	 */
	public function getBYHOUR()
	{
		return $this->BYHOUR;
	}

	/**
	 * @param mixed $BYHOUR
	 */
	public function setBYHOUR($BYHOUR)
	{
		$this->BYHOUR = $BYHOUR;
	}

	/**
	 * @return mixed
	 */
	public function getBYMINUTE()
	{
		return $this->BYMINUTE;
	}

	/**
	 * @param mixed $BYMINUTE
	 */
	public function setBYMINUTE($BYMINUTE)
	{
		$this->BYMINUTE = $BYMINUTE;
	}

	/**
	 * @return mixed
	 */
	public function getBYSECOND()
	{
		return $this->BYSECOND;
	}

	/**
	 * @param mixed $BYSECOND
	 */
	public function setBYSECOND($BYSECOND)
	{
		$this->BYSECOND = $BYSECOND;
	}

	/**
	 * @return mixed
	 */
	public function getBYSETPOS()
	{
		return $this->BYSETPOS;
	}

	/**
	 * @param mixed $BYSETPOS
	 */
	public function setBYSETPOS($BYSETPOS)
	{
		$this->BYSETPOS = $BYSETPOS;
	}
	/**
	 * @return string
	 */
	public function getString()
	{
		if(!isset($this->rrule))return null;
		return $this->rrule->rfcString();
	}

}