<?php

// BEST PRACTICES:
//
// TODO: Clean up and move DB result parsing to separate method (DRY)
// TODO: Require new objects of self to be initialized by constructor only
//       any initialized object should be valid, ideally.
// TODO: Move methods not working with single self (this) object to controller
// TODO: Move small small validation methods (and UTC Time Generator) to a common library
//

class contact_form_reply extends CI_Model
{
	public $id = null;
	public $contact_form_id = null;
	public $customer_group_id = null;
	public $person_id = null; // Used to identify customer from customers table.
	public $completed_at = null;
	public $ip_address = null;

	public $first_name = null;
	public $last_name = null;
	public $phone_number = null;
	public $email = null;
	public $birthday = null;
	public $address = null;
	public $city = null;
	public $state = null;
	public $zip = null;
	public $message = null;

	private $verified_course_id;

	/*
	 * Loads data into this object. Returns true / false on success
	 */
	public function retrieve()
	{
		// Validate ID
		if(!$this->validate_row_id($this->id)) {
			return false;
		}

		$this->db->from('contact_form_replies');
		$this->db->where("id = '$this->id'");
		$result = $this->db->get();

		if($result->num_rows() > 0){
			// Load in the data
			$row = $result->first_row();
			$this->id = (int)$row->id;
			$this->contact_form_id = (int)$row->contact_form_id;
			$this->customer_group_id = ($row->customer_group_id == false ? null : (int)$row->customer_group_id);
			$this->person_id = (int)$row->person_id;
			$this->completed_at = $row->completed_at;
			$this->ip_address = long2ip($row->ip_address);
			$this->first_name = $row->first_name;
			$this->last_name = $row->last_name;
			$this->phone_number = $row->phone_number;
			$this->email = $row->email;
			$this->birthday = $row->birthday;
			$this->address = $row->address;
			$this->city = $row->city;
			$this->state = $row->state;
			$this->zip = $row->zip;
			$this->message = $row->message;

			return true;
		} else {
			return false;
		}
	}

	/*
	 * Soft deletes an entry
	 */
	public function delete(){
		// Not implemented
		return false;
	}

	/*
	 * Soft (un)deletes an entry
	 */
	public function undelete(){
		// Not implemented
		return false;
	}

	/*
	 * Check if a retrieved entry was deleted
	 */
	public function was_deleted(){
		// Not implemented
		return false;
	}

	/*
	 * Persists object into database storage. Returns true / false on success
 	*/
	public function create()
	{
		// Validate Self
		if(!$this->validate_and_prepare()){
			return false;
		}

		// If an email is set and valid: Check if email exists in the 'people' table
		if(filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			$this->email = strtolower($this->email);
			$this->load->model("v2/Customer_model");

			$this->db->limit(1);
			$this->load->model("Customer_model");
			$this->Customer_model->course_id = $this->verified_course_id;
			$customer = $this->Customer_model->get([
				"q"=>$this->email,
				"limit"=>1
			]);
			if (count($customer) > 0) {
				$this->person_id = (int)$customer[0]['person_id'];
			} else {
				// If NOT exists
				// Add to people table -> get person_id back.
				$sql_first_name = ($this->first_name === null ? '' : $this->first_name);
				$sql_last_name = ($this->last_name === null ? '' : $this->last_name);
				$sql_phone_number = ($this->phone_number === null ? '' : $this->phone_number);
				$sql_email = ($this->email === null ? '' : $this->email);
				$sql_birthday = ($this->birthday === null ? '' : $this->birthday);
				$sql_address = ($this->address === null ? '' : $this->address);
				$sql_city = ($this->city === null ? '' : $this->city);
				$sql_state = ($this->state === null ? '' : $this->state);
				$sql_zip = ($this->zip === null ? '' : $this->zip);

				// Add person_id and course_id to new record on 'customer' table.
				$person_data = array(
					'first_name' => $sql_first_name,
					'last_name' => $sql_last_name,
					'phone_number' => $sql_phone_number,
					'cell_phone_number' => '',
					'email' => $sql_email,
					'birthday' => $sql_birthday,
					'address_1' => $sql_address,
					'address_2' => '',
					'city' => $sql_city,
					'state' => $sql_state,
					'zip' => $sql_zip,
					'country' => '',
					'comments' => '',
					'foreup_news_announcements_unsubscribe' => false
				);
				$customer_data = array(
					'CID' => '',
					'course_id' => $this->verified_course_id,
					'deleted' => false,
					'opt_out_email' => false,
					'opt_out_text' => false
				);
				$this->load->model('customer');
				$this->customer->save($person_data, $customer_data);

				if($person_data['person_id'] < 1) return false;
				$this->person_id = (int)$person_data['person_id'];
			}

			// Link person_id and customer_group_id in 'customer_group_members' table
			// If already exists, do nothing (or fail silently)
			if($this->customer_group_id !== null){
				$membership_data = array(
					'group_id' => $this->customer_group_id,
					'person_id' => $this->person_id
				);
				$query_string = $this->db->insert_string('customer_group_members', $membership_data);
				$query_string = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query_string);
				$this->db->simple_query($query_string);
			}
		}

		$sql_birthday = ($this->birthday === null ? 'NULL' : '"DATE(\''.$this->birthday.'\')"');
		$sql_customer_group_id = ($this->customer_group_id === null ? 'NULL' : '\''.$this->customer_group_id.'\'');
		$this->completed_at = $this->get_utc_datetime();
		$this->ip_address = $_SERVER['REMOTE_ADDR'];


		$query = "INSERT INTO `foreup_contact_form_replies`
				  SET
				  `contact_form_id`='$this->contact_form_id',
				  `customer_group_id`=$sql_customer_group_id,
				  `person_id`='$this->person_id',
				  `completed_at`='$this->completed_at',
				  `ip_address`=INET_ATON('".$this->ip_address."'),
				  `first_name`= '$this->first_name',
				  `last_name`='$this->last_name',
				  `phone_number`='$this->phone_number',
				  `email`='$this->email',
				  `birthday`=$sql_birthday,
				  `address`='$this->address',
				  `city`='$this->city',
				  `state`='$this->state',
				  `zip`='$this->zip',
				  `message`='$this->message'";

		$result_bool = (bool)$this->db->simple_query($query);
		$this->id = (int)$this->db->insert_id();

		return $result_bool;
	}

	/*
	 * Persists updated object into database storage. Returns true / false on success
	 */
	public function update(){
		// Not implemented
		return false;
	}

	/*
	 * Validates the data and prepares for persistence
	 */
	private function validate_and_prepare(){

		if(!$this->validate_row_id($this->id, true)){
			return false;
		}

		if(!$this->validate_row_id($this->contact_form_id)){
			return false;
		}

		// Load Contact Form to check required fields.
		$this->load->model('contact_form');
		$this->contact_form->id = $this->contact_form_id;
		if(!$this->contact_form->retrieve()){
			return false; // Contact form does not exist!!!
		}

		if(!$this->validate_row_id($this->customer_group_id, true)){
			return false;
		} else {
			if($this->customer_group_id !== null){
				// Verify group exists and is valid for the current course.
				$this->db->select('course_id');
				$this->db->from('customer_groups');
				$this->db->where('course_id', $this->contact_form->course_id);

				$result = $this->db->get();
				if($result->num_rows() > 0){
					// Load in the data
					$row = $result->first_row();
					if((int)$row->course_id != $this->contact_form->course_id){
						return false; // Customer Group belongs to some other course! Madness!
					}
					$this->verified_course_id = (int)$row->course_id;
				} else {
					return false; // Customer Group doesn't exist
				}
			}
		}

		if($this->contact_form->has_birthday){
			if($this->contact_form->req_birthday === true){
				try{
					new DateTime($this->birthday);
				} catch (Exception $e) {
					return false;
				}
			} else {
				try{
					new DateTime($this->birthday);
				} catch (Exception $e) {
					$this->birthday = null;
				}
			}
		} else {
			$this->birthday = null;
		}

		if($this->contact_form->has_first_name) {
			$this->first_name = $this->db->escape_str($this->first_name);
			if ($this->contact_form->req_first_name === true) {
				if (!$this->validate_string($this->first_name, false, 2, 32)) return false;
			} else {
				if (!$this->validate_string($this->first_name, false, 2, 32)) $this->first_name = null;
			}
		} else {
			$this->first_name = null;
		}

		if($this->contact_form->has_last_name) {
			$this->last_name = $this->db->escape_str($this->last_name);
			if ($this->contact_form->req_last_name === true) {
				if (!$this->validate_string($this->last_name, false, 2, 32)) return false;
			} else {
				if (!$this->validate_string($this->last_name, false, 2, 32)) $this->last_name = null;
			}
		} else {
			$this->last_name = null;
		}

		if($this->contact_form->has_address) {
			$this->address = $this->db->escape_str($this->address);
			if ($this->contact_form->req_address === true) {
				if (!$this->validate_string($this->address, false, 2, 64)) return false;
			} else {
				if (!$this->validate_string($this->address, false, 2, 64)) $this->address = null;
			}
		} else {
			$this->address = null;
		}

		if($this->contact_form->has_city) {
			$this->city = $this->db->escape_str($this->city);
			if ($this->contact_form->req_city === true) {
				if (!$this->validate_string($this->city, false, 2, 32)) return false;
			} else {
				if (!$this->validate_string($this->city, false, 2, 32)) $this->city = null;
			}
		} else {
			$this->city = null;
		}

		if($this->contact_form->has_state) {
			// TODO: Add additional State Code Validation
			$this->state = $this->strip_non_alpha($this->state);
			$this->state = $this->db->escape_str($this->state);
			if ($this->contact_form->req_state === true) {
				if (!$this->validate_string($this->state, false, 2, 2)) return false;
			} else {
				if (!$this->validate_string($this->state, false, 2, 2)) $this->state = null;
			}
		} else {
			$this->state = null;
		}

		if($this->contact_form->has_zip) {
			$this->zip = $this->strip_non_digits($this->zip);
			$this->zip = $this->db->escape_str($this->zip);
			// TODO: Add additional Zip Code Validation
			if ($this->contact_form->req_zip === true) {
				if (!$this->validate_string($this->zip, false, 5, 8)) return false;
			} else {
				if (!$this->validate_string($this->zip, false, 5, 8)) $this->zip = null;
			}
		} else {
			$this->zip = null;
		}

		if($this->contact_form->has_message) {
			$this->message = $this->db->escape_str($this->message);
			if ($this->contact_form->req_message === true) {
				if (!$this->validate_string($this->message, false, 0, 255)) return false;
			} else {
				if (!$this->validate_string($this->message, false, 0, 255)) $this->zip = null;
			}
		} else {
			$this->message = null;
		}

		if($this->contact_form->has_phone_number) {
			$this->phone_number = $this->strip_non_digits($this->phone_number);
			if ($this->contact_form->req_phone_number === true) {
				if(!$this->validate_phone_number($this->phone_number)){
					return false;
				}
			} else {
				if(!$this->validate_phone_number($this->phone_number)){
					$this->phone_number = null;
				}
			}
		} else {
			$this->phone_number = null;
		}

		if($this->contact_form->has_email) {
			if ($this->contact_form->req_email === true) {
				if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
					return false;
				}
			} else {
				if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
					$this->email = null;
				}
			}
		} else {
			$this->email = null;
		}
		if($this->email !== null) $this->email = $this->db->escape_str($this->email);

		// No errors
		return true;
	}

	/*
	 * Validates a row id
	 */
	private function validate_row_id($id, $nullable = false){
		if($id === null && $nullable === true){
			return true;
		}

		if(!is_integer($id) || $id < 1) {
			return false;
		}

		return true;
	}

	/*
	 * Validates a string
	 */
	private function validate_string($vstring, $nullable=false, $min_len=false, $max_len=false){
		if($vstring === null && $nullable === true){
			return true;
		}

		if(!is_string($vstring)) {
			return false;
		}

		if($min_len !== false && is_int($min_len)){
			if(strlen($vstring) < $min_len) return false;
		}

		if($max_len !== false && is_int($max_len)){
			if(strlen($vstring) > $max_len) return false;
		}

		return true;
	}

	/*
	 * Validates a 10 digit phone number in the format xxxxxxxxxx
	 */
	private function validate_phone_number($phone_number){
		if(preg_match("/^[0-9]{10}$/", $phone_number)){
			return true;
		} else {
			return false;
		}
	}

	/*
	 * Remove non-digits (for preparing phone number to be persisted in common format)
	 */
	private function strip_non_digits($mixed){
		return preg_replace("/[^0-9]/", '', $mixed);
	}

	/*
	 * Remove non-alpha
	 */
	private function strip_non_alpha($mixed){
		return preg_replace("/[^A-Za-z]/", '', $mixed);
	}

	/*
	 * Returns a string representation of UTC datetime in RFC3339 format
	 */
	private function get_utc_datetime(){
		$date_utc = new \DateTime(null, new \DateTimeZone("UTC"));
		return $date_utc->format(\DateTime::RFC3339);
	}

}