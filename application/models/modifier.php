<?php
class Modifier extends CI_Model {
	
	public function __construct(){
		$this->load->helper('array');
	}

	public function structure_options($row){

		$options =& $row['options'];
		$default = null;
		if(!empty($row['default'])){	
			$default = $row['default'];		
		}
		
		if($row['multi_select'] == 1 && !empty($default)){
			$default = json_decode($row['default'], true);
			if(empty($default)){
				$default = array();
			}
		}		
		
		$selected = false;
		if(!empty($row['selected_option'])){
			$selected = $row['selected_option'];
			
			if($row['multi_select'] == 1){
				$selected = json_decode($row['selected_option'], true);
			}
		}

		$structuredOptions = array();
		if(!empty($options)){
			$options = json_decode($row['options'], true);
		}

		// If no options set, just default to 'yes' or 'no'
		if(empty($options) || empty($options[0])){
			$options = array(array('label'=>'yes', 'price'=>0.00), array('label'=>'no', 'price'=>0.00));
		}

		// Set default selected option (if set)
		if(empty($selected)){
			$selected = $default;
		}
		$row['selected_option'] = $selected;
		$row['default'] = $default;
		
		$price = 0.00;
		foreach($options as $option){
			
			if($row['multi_select'] == true && is_array($selected)){
				foreach($selected as $selected_label){
					if(strtolower($selected_label) == strtolower($option['label'])){
						$price += (float) round($option['price'], 2);
					}					
				}
				
			}else{
				if(strtolower($selected) == strtolower($option['label'])){
					$price = (float) round($option['price'], 2);
				}
			}
		}

		$row['selected_price'] = $price;
		return $row;
	}

	public function search($query){

		$this->db->select('modifier_id AS value, name AS label');
		$this->db->from('modifiers');
		$this->db->where("name LIKE '%".$this->db->escape_like_str($query)."%'");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('deleted', 0);
		$query = $this->db->get();

		$rows = $query->result_array();
		return $rows;
	}

	public function get_by_item($itemId){

		if(empty($itemId)){
			return false;
		}

		$this->db->select('m.modifier_id, m.name, IF(override_price IS NULL, default_price, override_price) AS price,
			m.options, m.multi_select, im.item_id, im.default, im.order', false);
		$this->db->from('modifiers AS m');
		$this->db->join('item_modifiers AS im', 'im.modifier_id = m.modifier_id', 'inner');
		$this->db->where(array('im.item_id' => $itemId));
		$this->db->where('m.course_id', $this->session->userdata('course_id'));
		$this->db->where('m.deleted', 0);
		$this->db->order_by('im.order', 'ASC');

		$query = $this->db->get();
		$modifiers = $query->result_array();
		$rows = array();

		// Loop through rows and decode JSON options into array
		foreach($modifiers as $key => $row){
			$rows[$row['modifier_id']] = $this->structure_options($row);
			unset($rows[$row['modifier_id']]['auto_select']);
		}

		return $rows;
	}

	public function get($modifierId = null, $itemId = null){

		$this->db->select('*');
		$this->db->from('modifiers');

		if(!empty($itemId)){
			$this->db->join('item_modifiers', 'item_modifiers.modifier_id = modifiers.modifier_id', 'inner');
			$this->db->where(array('item_modifiers.item_id' => $itemId));
		}

		if(!empty($modifierId)){
			$this->db->where(array('modifiers.modifier_id' => $modifierId));
		}

		$this->db->where('modifiers.course_id', $this->session->userdata('course_id'));
		$this->db->where('modifiers.deleted', 0);

		$query = $this->db->get();
		$rows = $query->result_array();
		$data = array();

		foreach($rows as $key => $row){
			$data[] = $this->structure_options($row);
		}

		return $data;
	}

	public function save_all_item_modifiers($modifiers, $item_id){

		if(empty($item_id)){
			return false;
		}

		$this->db->delete('item_modifiers', array('item_id' => (int) $item_id));

		if(!empty($modifiers)){
			foreach($modifiers as $modifier){
				$this->save_item_modifier($modifier, $item_id);
			}			
		}
	}

	public function save_item_modifier($modifier, $item_id){

		$modifier = elements(array('modifier_id', 'default', 'order'), $modifier, null);
		$modifier['order'] = (int) $modifier['order'];
	
		if(empty($modifier['override_price'])){
			$modifier['override_price'] = null;
		}

		if(!empty($modifier['modifier_id'])){
			
			if(is_array($modifier['default'])){
				$modifier['default'] = json_encode($modifier['default']);
			}

			$modifier['item_id'] = (int) $item_id;
			$this->db->insert('item_modifiers', $modifier);
		}

		return true;
	}

	public function save($data, $modifierId = null){

		if(empty($data)){
			return false;
		}

		// Filter fields passed
		$data = elements(array('name', 'category_id', 'options', 'date_created', 'item', 'required', 'multi_select'), $data, null);
		$data['course_id'] = $this->session->userdata('course_id');
		
		if(!empty($data['options']) && is_array($data['options'])){
			$data['options'] = json_encode($data['options']);
		}else{
			$data['options'] = null;
		}

		foreach($data as $field => $val){
			if($val === null){
				unset($data[$field]);
			}
		}

		$itemId = null;
		if(!empty($itemData['item_id'])){
			$itemId = $itemData['item_id'];
		}

		if(!empty($data)){
			
			if(isset($data['multi_select'])){
				$data['multi_select'] = (int) $data['multi_select'];
			}
			
			// If creating a new record
			if(empty($modifierId)){
				$data['multi_select'] = (int) $data['multi_select'];
				$success = $this->db->insert('modifiers', $data);
				$modifierId = $this->db->insert_id();

			// If updating an existing record
			}else{
				$success = $this->db->update('modifiers', $data, array('modifier_id' => $modifierId));
			}
		}

		return $modifierId;
	}

	public function delete($modifierId){
		if(empty($modifierId)){
			return false;
		}

		return $this->db->update('modifiers', array('deleted' => 1), array('modifier_id'=>$modifierId));
	}

	public function delete_from_item($itemId, $modifierId){
		if(empty($modifierId) || empty($itemId)){
			return false;
		}

		return $this->db->delete('item_modifiers', array('modifier_id'=>$modifierId, 'item_id'=>$itemId));
	}
}
?>
