<?php
class Report extends CI_Model
{
	private $reportValidator, $reportsColumnsValidator;
	private $booleanFields = ["filterable","visible","selectable","sortable","locked"];


	public function __construct()
	{
		$this->load->model('Report_column');
	}

	public function get_info($id)
	{
		$this->db->from('reports');
		$this->db->where('id',$id);
		$row = $this->db->get()->row();
		$row = $this->deSerializeJsonFields($row);
		$baseReportId = $id;

		$this->db->select("reports_columns.label AS label_override");
		$this->db->select("reports_columns.type AS type_override");
		$this->db->select("reports_columns.* ");
		$this->db->select("report_columns.report_type,report_columns.column, report_columns.label,report_columns.description,report_columns.type,report_columns.table");
		$this->db->from("reports_columns");
		$this->db->where("report_id",$baseReportId);
		$this->db->order_by("column_order", "ASC");
		$this->db->order_by("id", "ASC");
		$this->db->join("report_columns","report_columns.id = reports_columns.column_id","left");
		$columns = $this->db->get()->result();
		foreach($columns as $column){
			foreach($this->booleanFields as $field){
				$column->$field = $column->$field == 'Y';
			}
			if(isset($column->label_override) && $column->label_override!=""){
				$column->label = $column->label_override;
			}
			if(isset($column->type_override)){
				$column->type = $column->type_override;
			}
		}
		$row->columns = $columns;



		$this->db->from("report_linkouts");
		$this->db->where("report_id",$id);
		$linkouts = $this->db->get()->result();
		foreach($linkouts as $linkout){
			$this->db->from("report_linkouts_columns");
			$this->db->join("reports_columns","reports_columns.id = report_linkouts_columns.column_id","left");
			$this->db->join("report_columns","report_columns.id = reports_columns.column_id","left");
			$this->db->where("linkout_id",$linkout->id);

			$linkout->columns = $this->db->get()->result();
		}
		$row->linkouts = $linkouts;

		$this->db->select("reports_summary.label AS label_override");
		$this->db->select("reports_summary.* ");
		$this->db->select("report_columns.report_type,report_columns.column, report_columns.label,report_columns.description,report_columns.type,report_columns.table");
		$this->db->from("reports_summary");
		$this->db->where("report_id",$baseReportId);
		$this->db->order_by("id", "ASC");
		$this->db->join("report_columns","report_columns.id = reports_summary.column_id","left");
		$columns = $this->db->get()->result();
		foreach($columns as $column){
			if(isset($column->label_override)){
				$column->label = $column->label_override;
			}
			if(isset($column->type_override)){
				$column->type = $column->type_override;
			}
		}
		$row->summary_columns = $columns;

		$default_permission = $this->get_default_permission_level();
		if($default_permission > $this->session->userdata("user_level")){
			$lock = fu\acl\Factory::createCurrentUserLock();
			if(!$lock->can("view","reports",(integer)$id)){
				return false;
			}
		}



		return $row;
	}

	private function get_default_permission_level()
	{
		$course_info = $this->db->from("courses")
			->where("course_id",$this->session->userdata('course_id'))
			->select("default_report_permissions")
			->get()
			->row();
		return $course_info->default_report_permissions;
	}

	public function get_all()
	{

		$default_reports_params = $this->create_allreport_param_string();
		if($default_reports_params == ""){
			return [];
		}

		$this->db->from('reports');
		$this->db->select('id,base,base_report,course_id,type,description,title,deleted,tags');
		$this->db->where("`user_level_required` <= {$this->session->userdata('user_level')}");
		$this->db->where("({$default_reports_params})");

		if($this->session->userdata('user_level')>5){

			$this->db->where("`user_level_required` >= 5");
		}


		$allReports = $this->db->get()->result();
		foreach($allReports as $report){
			$report->tags = $report->tags != null ? json_decode($report->tags):[];
		}
		return $allReports;
	}

	function exists($id)
	{
		$this->db->from('reports');
		$this->db->where("id", $id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	function update($data, $id)
	{

		$data = $this->serializeJsonAttributes($data);
		$data = $this->cleanseReportObject($data);
		$this->db->where('id', $id);
		$success = $this->db->update('reports',$data);

		$columnsInreport = [];

		foreach($data->columns as $column){
			$columnsInreport[] = $column->id;
			if(!isset($column->dirty) || !$column->dirty ){
				if(isset($column->id) && is_numeric($column->id)){
					continue;
				}
			}

			unset($column->dirty);
			unset($column->dirty);

			if(isset($column->id) && is_numeric($column->id)){
				$this->Report_column->update($column,$column->id);
			} else {
				$id = $this->Report_column->insert($column);
				$column->id = $id;
				$columnsInreport[] = $id;
			}
		}

		if(isset($data->id)){
			$this->db->from('reports_columns');
			$this->db->where("report_id",$data->id);
			$this->db->where_not_in("id", $columnsInreport);
			$this->db->delete();
		}

		$data = $this->deSerializeJsonFields($data);
		return true;
	}

	function create()
	{

		$this->db->insert('reports',[
			"title"=>"Report Name",
			"course_id"=>$this->session->userdata('course_id')
		]);
		$id = $this->db->insert_id();
		return $id;
	}
	function save ($data)
	{
		$data = $this->serializeJsonAttributes($data);
		$data = $this->cleanseReportObject($data);
		$data->course_id=$this->session->userdata('course_id');

		$this->db->insert('reports',$data);
		$report_id = $this->db->insert_id();


		$order = 0;
		foreach($data->columns as $column){
			unset($column->label_override);
			unset($column->type_override);
			unset($column->report_type);
			unset($column->column);
			unset($column->description);
			unset($column->table);
			unset($column->completeName);
			unset($column->id);
			$column->report_id = $report_id;
			$column->column_order = $order++;
			$id = $this->Report_column->insert($column);
			$column->id = $id;
		}


		return $report_id;
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->update('reports', array('deleted' => 1));
	}
	
	function undelete($id)
	{
		$this->db->where('id', $id);
		return $this->db->update('reports', array('deleted' => 1));
	}

	public function getReportColumns($id)
	{
		$this->db->from('reports_columns');
		$this->db->where("report_id", $id);
		return $this->db->get()->result();
	}

	public function getReportColumn($id)
	{
		$this->db->from('reports_columns');
		$this->db->where("id", $id);
		return $this->db->get()->result();
	}

	public function addReportColumn($data)
	{

		$this->db->insert('reports_columns',$data);
		$id = $this->db->insert_id();

		return $id;
	}

	public function editReportColumn($id,$column_id,$data)
	{
		$this->db->where("report_id", $id);
		$this->db->where("column_id", $column_id);

		$success = $this->db->update('reports_columns',$data);
		return $success;
	}

	public function removeReportColumn($id)
	{
		$this->db->from('reports_columns');
		$this->db->where("report_id", $id);
		return $this->db->get()->result();
	}

	/**
	 * @param $row
	 */
	private function deSerializeJsonFields($row)
	{
		$row->ordering = $row->ordering != null ? json_decode($row->ordering) : [];
		$row->grouping = $row->grouping != null ? json_decode($row->grouping) : [];
		$row->date_range = $row->date_range != null ? json_decode($row->date_range) : [];
		$row->filters = $row->filters != null ? json_decode($row->filters) : [];
		$row->tags = $row->tags != null ? json_decode($row->tags) : [];

		return $row;
	}

	/**
	 * @param $data
	 */
	private function serializeJsonAttributes($data)
	{
		$data->ordering = $data->ordering != null ? json_encode($data->ordering) : [];
		$data->grouping = $data->grouping != null ? json_encode($data->grouping) : [];
		$data->date_range = $data->date_range != null ? json_encode($data->date_range) : [];
		$data->filters = $this->cleanseFilterJson($data->filters);
		$data->filters = $data->filters != null ? json_encode($data->filters) : [];
		$data->tags = $data->tags != null ? json_encode($data->tags) : [];

		return $data;
	}

	private function cleanseReportObject($data)
	{
		$attributesToDelete = [
			"loading",
			"split_by",
			"chart_data",
			"comparative",
			"comparativeReports",
			"comparativeType",
			"columns_dirty"
		];
		foreach($attributesToDelete as $attr){
			if(isset($data->$attr)){
				unset($data->$attr);
			}
		}
		return $data;
	}

	private function cleanseFilterJson($data)
	{
		$attributesToDelete = [
			"columns",
			"default_operators",
			"default_values",
			"dropdown_values",
			"possible_filters",
			"loading"
		];
		foreach($data as &$filter){
			foreach($attributesToDelete as $attr){
				if(isset($filter->$attr)){
					unset($filter->$attr);
				}
			}

		}
		return $data;
	}

	/**
	 * @return string
	 */
	private function create_allreport_param_string()
	{
		$default_reports_params = "";
		$default_reports_permission = $this->get_default_permission_level();

		if ($default_reports_permission <= $this->session->userdata('user_level')) {
			$default_reports_params = "
			`course_id` = -1
				OR
			`course_id` = {$this->session->userdata('course_id')}
			";
		}

		//$lock = new fu\acl\Repository();
		//$ids = $lock->getAllAllowed("view", "reports");
		$ids = "";
		//$ids = implode(',', $ids);
		if ($ids != "") {
			if ($default_reports_params != "") {
				$default_reports_params .= " OR ";
			}
			$default_reports_params .= "`id` in ({$ids})";
			return $default_reports_params;
		}
		return $default_reports_params;
	}
}
?>
