<?php
class Loyalty_package extends CI_Model
{
    function save(&$package_info, $package_id = false) {
        $package_info['last_updated'] = date("Y-m-d H:i:s");
        $package_info['last_updated_by'] = $this->session->userdata('person_id');

        if (!$package_id)
        {
            $package_info['created'] = date("Y-m-d H:i:s");
            $package_info['created_by'] = $this->session->userdata('person_id');
            $result = $this->db->insert('loyalty_packages', $package_info);

            if ($result) {
                $package_info['id'] = $this->db->insert_id();
            }

            return $result;
        }
        else
        {
            $this->db->where('id', $package_id);
            return $this->db->update('loyalty_packages', $package_info);
        }
    }

    function delete($id) {
        $deleted = [
            'date_deleted' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->session->userdata('person_id')
        ];
        return $this->save($deleted, $id);
    }

    function get_defaults() {
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('is_default', true);
        return $this->db->get('loyalty_packages');
    }
}
