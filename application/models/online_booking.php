<?php
class Online_Booking extends CI_Model {

    private $time_of_day_presets = array(
        'morning' => array('start' => '0000','end' => '1100'),
        'midday' => array('start' => '1000','end' => '1500'),
        'evening' => array('start' => '1400','end' => '2400'),
        'all' => array('start' => '0000','end' => '2400')
    );

	private $filter_holes = 18;
    private $customer_taxable = 1;
    private $customer_price_class = false;
	private $filter_players = 4;
	private $filter_time_of_day = 'morning';
	private $filter_time_start = '0000';
	private $filter_time_end = '1100';
	private $filter_date = false;
	private $filter_timestamp = 0;
	private $teesheets = array();
	private $teesheet_ids = array();
	private $tax_rates = array();
	private $teetime_share = array();
	private $booking_class_id = false;
	private $booking_class = false;
	private $group_id = false;
	private $courses = array();
	private $teetime_prices = array();
	private $admin_mode = false;

	public $response = array();
	public $message = false;

	private $pay_online_types = array(
		0 => 'no',
		1 => 'optional',
		2 => 'required'
	);

	function __construct(){
		$this->load->model('teesheet');
		$this->load->model('Pricing');
		$this->load->model('Item');
		$this->load->library('v2/cart_lib');
		$this->load->model('billing');
		$this->load->model('Booking_class');
		$this->load->model('Green_fee');
		$this->load->model('course');
		$this->load->model('online_hours');
        $this->load->model('user');

        $this->set_filters();
	}

	function set_filters($holes = 18, $players = 4, $time_of_day = 'morning', $specials_only = false, $date = null){
		
		$this->filter_holes = $holes;
		$this->filter_players = $players;
		$this->filter_specials_only = (bool) $specials_only;
		$cur_date = new DateTime();

		if(empty($date)){
			$this->filter_date = $cur_date;
		}else{
			if($date instanceof DateTime){
				$this->filter_date = $date;
			}else{
				$this->filter_date = new DateTime($date);
			}
		}
		$this->filter_timestamp = $this->filter_date->format('Ymd') - 100;
		
		if(!empty($this->time_of_day_presets[$time_of_day])){
			$this->filter_time_of_day = $time_of_day;
			$this->filter_time_start = $this->time_of_day_presets[$time_of_day]['start'];
			$this->filter_time_end = $this->time_of_day_presets[$time_of_day]['end'];
		}

		$this->current_timestamp = $cur_date->format('YmdHi') - 1000000;
		
		// Golfers must book time at least 1 hour in advance
		$this->min_timestamp = $this->current_timestamp + 100;
	}

	function get_potential_teetimes($timestamp, $start, $end, $increment, $round_time, $teesheet_id, $teesheet_holes){
        $this->load->model("Increment_adjustment");
        $date = date('Y-m-d', strtotime($timestamp+100));
        $potential_teetimes_array = array();

        $this->Increment_adjustment->generate_slot_times($date, $teesheet_id);

        foreach ($this->Increment_adjustment->slot_times as $slot_time) {
            if ($slot_time >= $start && $slot_time <= $end) {
				$slot_time = str_pad($slot_time, 4, "0", STR_PAD_LEFT); 
                $slot_time = (string)$timestamp.$slot_time;
                $reround_start_time = $slot_time + $round_time;

                if($reround_start_time % 100 > 59){
                    $reround_start_time += 40;
                }
                $potential_teetimes_array[] = array(
                    'start_front' => $slot_time,
                    'start_back' => (string)$this->Increment_adjustment->get_slot_time((int)$reround_start_time + 1000000, $teesheet_id),
                    'teesheet_id' => $teesheet_id,
                    'teesheet_holes' => $teesheet_holes
                );
            }
        }

		return $potential_teetimes_array;
	}

	function get_times($params){

		$this->message = false;
		if(empty($params['teesheet_id']) && empty($params['group_id'])){
			return false;
		}

		$this->booking_class_id = false;
		if(!empty($params['booking_class_id'])){
			$this->booking_class_id = (int) $params['booking_class_id'];
		}

		$teesheet_params = array(
			'online_only' => true
		);

		$this->group_id = false;
		if(!empty($params['group_id'])){
			$teesheet_params['group_id'] = $params['group_id'];
			$teesheet_params['aggregate_only'] = true;
			
			// Booking classes are ignored when viewing aggregate times
			$this->booking_class_id = false;
			$this->group_id = (int) $params['group_id'];

            if(!empty($params['teesheet_ids'])){
                $teesheet_params['teesheet_ids'] = $params['teesheet_ids'];
            }
            else {
                $teesheet_params['teesheet_ids'] = array('');
            }
        }

		$this->customer_price_class = false;
		if(!empty($params['customer_price_class'])){
			$this->customer_price_class = $params['customer_price_class'];
		}

		$this->customer_taxable = true;
		if(isset($params['customer_taxable'])){
			$this->customer_taxable = $params['customer_taxable'];
		}

		if(!empty($params['teesheet_id'])){
			$teesheet_params['teesheet_id'] = $params['teesheet_id'];
		}

        if(!$this->load_teesheets($teesheet_params)){
			return false;
		}

		$potential_teetimes = array();
		foreach($this->teesheets as &$teesheet){

			// If online booking is closed for this teesheet, skip it
			$timezone = null;
			if(!empty($teesheet['course_timezone'])){
				$timezone = new DateTimeZone($teesheet['course_timezone']);
			}
			$cur_date = new DateTime(null, $timezone);

			if(!$this->admin_mode){
				if((int) $teesheet['block_online_booking'] == 1 && !$this->online_hours->is_available($teesheet['teesheet_id'], $cur_date)){
					continue;
				}

				// If teesheet doesn't allow number of holes being filtered, skip it
				if((int) $teesheet['limit_holes'] > 0 && (int) $teesheet['limit_holes'] != (int) $this->filter_holes){
					continue;
				}

				// If teesheet doesn't allow number of players being filtered, skip it
				if((int) $teesheet['minimum_players'] > (int) $this->filter_players && $this->filter_players > 0){
					continue;
				}

				// If the current day is a "new" day, make sure we are past the new day start time
				$new_day_start_time = $teesheet['online_booking_new_day_start_time'];
				$filter_date = \Carbon\Carbon::createFromFormat('Y-m-d', $this->filter_date->format('Y-m-d'), $teesheet['course_timezone']);
				$max_date = \Carbon\Carbon::createFromFormat('Y-m-d Hi', $teesheet['max_date'].' '.$new_day_start_time, $teesheet['course_timezone'])->subDay();

				if($filter_date->toDateString() == $max_date->toDateString()){
					if($filter_date->lt($max_date)){
						$this->message = 'Booking for '.$max_date->format('n/d/Y').' starts at '.
							$max_date->format('g:ia'). ' ('.$max_date->format('T').')';
						continue;
					}
				}
			}

			$start_time = $this->generate_open_time($teesheet, $this->filter_date);
			$end_time = $this->generate_close_time($teesheet, $this->filter_date);
			if($this->admin_mode){
				$teesheet['max_timestamp'] = 202000000000;
				$teesheet['min_timestamp'] = 0;
				if($params['start_time'])
					$start_time = $params['start_time'];
				if($params['end_time'])
					$end_time = $params['end_time'];
				$teesheet['booking_carts'] = true;
			}


			$potential_teetimes = array_merge($potential_teetimes, $this->get_potential_teetimes(
				$this->filter_timestamp,
				$start_time,
				$end_time,
				$teesheet['increment'],
				$teesheet['frontnine'],
				$teesheet['teesheet_id'],
				$teesheet['holes']
			));
        }

		// Sort all potential tee times (possibly from multiple courses) by start time
		usort($potential_teetimes, function($a, $b){
			if((int) $a['start_front'] > (int) $b['start_front']){
				return 1;
			}else if((int) $a['start_front'] < (int) $b['start_front']){
				return -1;
			}
			return 0;
		});

		$start = $this->filter_timestamp.'0000';
		$end =  $this->filter_timestamp.'2400';

		$existing_times = $this->get_existing_times($potential_teetimes, $start, $end, $this->filter_holes, $teesheet['holes']);
		$times = $this->filter_times($potential_teetimes, $existing_times);

		return $times;
	}

    function generate_open_time($teesheet, $date) {
        $this->load->model('course');
        $course_info = $this->course->get_info($teesheet['course_id']);

		if ($teesheet['book_sunrise_sunset']) {
			$suntimes = $this->course->get_suntimes($teesheet['course_id'], $date->format('Y-m-d'));
			$open_time = date('Hi', strtotime($suntimes['sunrise'] . ' + ' . $teesheet['sunrise_offset'] . ' minutes'));

			if ((int) $teesheet['online_open_time'] < (int) $open_time) {
				return (int) $open_time;
			} else {
				return $teesheet['online_open_time'];
			}
		}

        if ((int) $teesheet['online_open_time'] < (int) $course_info->open_time) {
            return (int) $course_info->open_time;
        }

        $co_minutes = (int) substr($course_info->open_time, 0, 2) * 60 +  (int) substr($course_info->open_time, 2);
        $tso_minutes = (int) substr($teesheet['online_open_time'], 0, 2) * 60 +  (int) substr($teesheet['online_open_time'], 2);
        $diff = $tso_minutes - $co_minutes;

        if (fmod($diff,$teesheet['increment']) == 0) {
            return (int) $teesheet['online_open_time'];
        }

        $open_minutes = $co_minutes + ((floor($diff/$teesheet['increment']) + 1) * $teesheet['increment']);
        $open_time = floor($open_minutes / 60) * 100 + $open_minutes % 60;

        return (int) $open_time;
    }

	function generate_close_time($teesheet, $date) {
		if ($teesheet['book_sunrise_sunset']) {
			$suntimes = $this->course->get_suntimes($teesheet['course_id'], $date->format('Y-m-d'));
			$close_time = date('Hi', strtotime($suntimes['sunset'] . ' - ' . $teesheet['sunset_offset'] . ' minutes'));

			if ((int) $teesheet['online_close_time'] > (int) $close_time) {
				return (int) $close_time;
			} else {
				return $teesheet['online_close_time'];
			}
		}

		return (int) $teesheet['online_close_time'];
	}

	function load_teesheets($teesheet_params){
		
		$this->teesheet_ids = array();
		$this->teesheets = array();
		
		$teesheets = $this->teesheet->get_teesheets(false, $teesheet_params);
		if(empty($teesheets)){
			return false;
		}		

		$is_aggregate = false;
		if(!empty($teesheet_params['aggregate_only'])){
			$is_aggregate = true;
		}

		foreach($teesheets as $teesheet){
			$teesheet_settings = $this->load_teesheet($teesheet['teesheet_id'], $this->booking_class_id, $is_aggregate, $teesheet);
			$this->teesheet_ids[] = (int) $teesheet['teesheet_id'];
			$this->teesheets[(int) $teesheet['teesheet_id']] = $teesheet_settings;		
		}	

		return true;
	}

	function load_teesheet($teesheet_id, $booking_class_id = false, $is_aggregate = false, $teesheet = false){
		
		// Load booking class details, which will override some tee sheet settings
		if($booking_class_id && empty($this->booking_class)){
			$booking_class = (array) $this->Booking_class->get_info($booking_class_id);
			if($booking_class['active'] == 1){
				$this->booking_class = $booking_class;
			}
		}

		if($booking_class_id){
			$booking_class = $this->booking_class;
		}else{
			$booking_class = false;
		}

		if(empty($teesheet)){
			$teesheet = $this->teesheet->get_teesheets(false, array('teesheet_id' => $teesheet_id, 'aggregate_only' => $is_aggregate));
			$teesheet = $teesheet[0];
		}

		// If a booking class is being used, override the teesheet settings
		if(!empty($booking_class)){
			$teesheet['price_class'] = $booking_class['price_class'];
			$teesheet['use_customer_pricing'] = $booking_class['use_customer_pricing'];
			$teesheet['hide_online_prices'] = $booking_class['hide_online_prices'];
			$teesheet['online_open_time'] = $booking_class['online_open_time'];
			$teesheet['online_close_time'] = $booking_class['online_close_time'];
			$teesheet['days_in_booking_window'] = $booking_class['days_in_booking_window'];
			$teesheet['minimum_players'] = $booking_class['minimum_players'];
			$teesheet['limit_holes'] = $booking_class['limit_holes'];
			$teesheet['booking_carts'] = $booking_class['booking_carts'];
			$teesheet['use_customer_pricing'] = $booking_class['use_customer_pricing'];
			$teesheet['show_full_details'] = $booking_class['show_full_details'];
			$teesheet['require_credit_card'] = (int) $booking_class['require_credit_card'];
			$teesheet['pay_online'] = (int) $booking_class['pay_online'];
			$teesheet['booking_fee_item_id'] = $booking_class['booking_fee_item_id'];
			$teesheet['booking_fee_enabled'] = (int) $booking_class['booking_fee_enabled'];
			$teesheet['booking_fee_terms'] = $booking_class['booking_fee_terms'];
			$teesheet['booking_fee_item'] = $booking_class['booking_fee_item'];
            $teesheet['booking_fee_per_person'] = $booking_class['booking_fee_per_person'];
		
		}else{
			$teesheet['use_customer_pricing'] = 0;
			$teesheet['show_full_details'] = 0;
			$teesheet['price_class'] = false;
			$teesheet['booking_fee_item_id'] = false;

			if(!$is_aggregate){
				$teesheet['booking_fee_enabled'] = false;
				$teesheet['booking_fee_terms'] = null;
				$teesheet['booking_fee_item'] = false;
                $teesheet['booking_fee_per_person'] = false;
			}
		}

		if($is_aggregate){
			$teesheet['show_full_details'] = 0;
		}

		// Get booking window timestamps for each teesheet
		// No times will show before min_timestamp, or after max_timestamp
		$timezone = null;
		if(!empty($teesheet['course_timezone'])){
			$timezone = new DateTimeZone($teesheet['course_timezone']);
		}
		$max_date = new DateTime(null, $timezone);
		$max_date->setTime(0, 0, 0);
		$max_date->modify('+'.$teesheet['days_in_booking_window'].' days');

		$cur_date = new DateTime(null, $timezone);
		
		$teesheet['max_date'] = $max_date->format('Y-m-d');
		$teesheet['max_timestamp'] = $max_date->format('YmdHi') - 1000000;
		$teesheet['min_timestamp'] = $cur_date->format('YmdHi') - 1000000 + 100;

		if ($teesheet['book_sunrise_sunset']) {
			$suntimes = $this->course->get_suntimes($teesheet['course_id'], $this->filter_date->format('Y-m-d'));
			// TODO ADD OFFSET
			$formated_sunrise = substr(str_replace(':', '', $suntimes['sunrise']), 0, 4);
			$formated_sunset = substr(str_replace(':', '', $suntimes['sunset']), 0, 4);
			$teesheet['course_open'] = $formated_sunrise;
			$teesheet['course_close'] = $formated_sunset;
//			$teesheet['online_open_time'] = $teesheet['course_open'] = $formated_sunrise;
//			$teesheet['online_close_time'] = $teesheet['course_close'] = $formated_sunset;
		}

		return $teesheet;
	}

	function filter_times($potential_teetimes, $existing_times, $use_time_filters = true){
		$times = array();
		foreach($potential_teetimes as $teetime){

			$existing_front = false;
            if( !empty( $existing_times[(int) $teetime['teesheet_id']]['front'][$teetime['start_front']] )){
				$existing_front = $existing_times[(int) $teetime['teesheet_id']]['front'][$teetime['start_front']];
			}

			$existing_back = false;
			if( !empty( $existing_times[(int) $teetime['teesheet_id']]['back'][$teetime['start_front']] )){
				$existing_back = $existing_times[(int) $teetime['teesheet_id']]['back'][$teetime['start_front']];
			}

			$this->apply_time_details($teetime, $existing_front, $existing_back);

            if(!$this->is_time_available($teetime, $existing_front, $existing_back, $use_time_filters)){
				continue;
			}

			if(!$this->apply_time_pricing($teetime)){
				continue;
			}

			unset($teetime['start_front'], $teetime['start_back']);
			$times[] = $teetime;	
		}

		return $times;
	}

	// Retrieves cart fee/green fee tax rate for a course, uses caching
	function get_tax_rate($course_id, $type){
		if(!isset($this->tax_rates[$course_id])){
			$greenfee_tax_rate = $this->Item->get_teetime_tax_rate($course_id);
            $this->tax_rates[$course_id]['green_fee'] = !empty($greenfee_tax_rate[0]['percent']) ? (float) $greenfee_tax_rate[0]['percent'] : 0;

			$cartfee_tax_rate = $this->Item->get_cart_tax_rate($course_id);
			$this->tax_rates[$course_id]['cart_fee'] = !empty($cartfee_tax_rate[0]['percent']) ? (float) $cartfee_tax_rate[0]['percent'] : 0;
		}
		return $this->tax_rates[$course_id][$type];
	}

	// Checks if golfer can purchase a tee time through ForeUp (tee time share), uses caching
	function can_get_foreup_discount($date, $teesheet_id){
		if(!isset($this->teetime_share[$teesheet_id])){
			$this->teetime_share[$teesheet_id] = (bool) $this->billing->have_sellable_teetimes($date, $teesheet_id);	
		}
		return $this->teetime_share[$teesheet_id];		
	}

	// Retrieves a price for a cart fee or green fee based on deprecated OLD pricing
	function get_price($teesheet_id, $price_class, $time, $holes, $day_of_week, $is_cart, $course_id){
		
		if(!isset($this->courses[$course_id])){
			$this->courses[$course_id] = $this->course->get_info($course_id);
		}
		if(!isset($this->teetime_prices[$course_id])){
			$this->teetime_prices[$course_id] = $this->Green_fee->get_info('', '', '', $course_id);			
		}
        $course_info =& $this->courses[$course_id];
		$prices =& $this->teetime_prices[$course_id];

		if ($time >= $course_info->early_bird_hours_begin && $time < $course_info->early_bird_hours_end)
	    	$col = 2;
		else if ($time >= $course_info->morning_hours_begin && $time < $course_info->morning_hours_end)
	        $col = 3;
		else if ($time >= $course_info->afternoon_hours_begin && $time < $course_info->afternoon_hours_end)
	        $col = 4;
		else if ($time >= $course_info->super_twilight_hour)
	        $col = 6;
	    else if ($time >= $course_info->twilight_hour)
	        $col = 5;
	    else
	        $col = 1;

	    if ($holes == 9){
            $row = 0;
			$cart_row = 0;
        }else{
            $row = 4;
            $cart_row = 4;
        }

        if (($day_of_week == 'Fri' && $course_info->weekend_fri) ||
                ($day_of_week == 'Sat' && $course_info->weekend_sat) ||
                ($day_of_week == 'Sun' && $course_info->weekend_sun)) {
        	$cart_row += 2;
            $row += 4;
        
        }else{
        	$cart_row += 1;
            $row += 3;
        }

		$green_fee_index = $row;
		$cart_fee_index = $cart_row;

		if($is_cart){
			if(!$price_class || empty($prices[$teesheet_id]["{$course_id}_{$cart_fee_index}"]->$price_class)){
				$price_class = 'price_category_'.$col;
			}			
			return (float) $prices[$teesheet_id]["{$course_id}_{$cart_fee_index}"]->$price_class;
		
		}else{
			if(!$price_class || empty($prices[$teesheet_id]["{$course_id}_{$green_fee_index}"]->$price_class)){
				$price_class = 'price_category_'.$col;
			}				
			return (float) $prices[$teesheet_id]["{$course_id}_{$green_fee_index}"]->$price_class;
		}
	}

	// Adds pricing to a tee time
	function apply_time_pricing(&$time){
	
		$start_time = substr($time['start_front'], 8);
		$date = $this->filter_date->format('Y-m-d');
		$teesheet_id = $time['schedule_id'];
		$teesheet = $this->teesheets[$teesheet_id];
		$price_class = $teesheet['price_class'];
		$booking_carts = (int) $teesheet['booking_carts'];
		$course_id = $teesheet['course_id'];

		if($this->customer_price_class && (($teesheet['use_customer_pricing'] == 1 && !$this->group_id) || $this->admin_mode)){
			$price_class = $this->customer_price_class;
		}

		// If course still uses old pricing
		if($teesheet['course_seasonal_pricing'] == 0){
			$this->Item->seasonal_pricing = false;
			$day_of_week = $this->filter_date->format('D');
			$cart_fee = (float) $this->get_price($teesheet_id, $price_class, $start_time, $time['holes'], $day_of_week, true, $course_id);
			$green_fee = (float) $this->get_price($teesheet_id, $price_class, $start_time, $time['holes'], $day_of_week, false, $course_id);
			
		// If course uses new seasonal pricing
		}else{
			$this->Item->seasonal_pricing = true;
			$this->Pricing->course_id = $teesheet['course_id'];
			$cart_fee = (float) $this->Pricing->get_price($teesheet_id, $price_class, $date, $start_time, $time['holes'], true);
			$green_fee = (float) $this->Pricing->get_price($teesheet_id, $price_class, $date, $start_time, $time['holes'], false);	
		}

		// Get special prices for this timeframe
		$has_special = false;
		$special_id = false;
		$special_discount_percent = 0;
			
		if(empty($this->group_id)){
			$this->Special->aggregate = false;
		}else{
			$this->Special->aggregate = true;
		}
		
		if($teesheet['specials_enabled'] == 1 && $special = $this->Special->get_special($teesheet_id, $date, $start_time, $this->booking_class_id)){

			$special_id = $special['special_id'];
			$special_cart_fee = (float) $this->Special->get_prices($teesheet_id, $date, $start_time, $time['holes'], true, $this->booking_class_id);
			$special_green_fee = (float) $this->Special->get_prices($teesheet_id, $date, $start_time, $time['holes'], false, $this->booking_class_id);
			if(($green_fee + $cart_fee) > 0 && $special_cart_fee !== false && $special_green_fee !== false){
                $has_special = true;
				// Calculate the total discount percentage offered by special
				$total = $green_fee + $cart_fee;
				$total_special = $special_green_fee + $special_cart_fee;
				$special_discount_percent = round(100 - (100 * ($total_special / $total)));
				
				// Replace standard prices with special price
				$green_fee = $special_green_fee;
				$cart_fee = $special_cart_fee;				
			}
		}

		// If tee time does not have a special, and the customer only wants to view specials, skip the tee time
		if(!$has_special && $this->filter_specials_only){
			return false;
		}

		// If course wants to display tax in the total price
        $include_tax = (bool) ($teesheet['include_tax_online_booking'] == 1 && !$teesheet['unit_price_includes_tax']);
        $green_taxes = array(
            array('percent' => $this->get_tax_rate($course_id, 'green_fee'))
        );
        $cart_taxes = array(
            array('percent' => $this->get_tax_rate($course_id, 'cart_fee'))
        );
        $green_fee_tax = $this->cart_lib->calculate_tax($green_fee, $green_taxes);
        $cart_fee_tax = $this->cart_lib->calculate_tax($cart_fee, $cart_taxes);
		if($this->customer_taxable && $include_tax){
			$time['green_fee_tax_rate'] = (float) $this->get_tax_rate($course_id, 'green_fee');
			$time['cart_fee_tax_rate'] = (float) $this->get_tax_rate($course_id, 'cart_fee');
			$time['green_fee_tax'] = $green_fee_tax;
			$time['cart_fee_tax'] = $cart_fee_tax;
		
		}else{
			$time['green_fee_tax_rate'] = false;
			$time['cart_fee_tax_rate'] = false;
			$time['green_fee_tax'] = 0;
			$time['cart_fee_tax'] = 0;
		}

		$time['has_special'] = $has_special;
		$time['special_id'] = $special_id;
		$time['special_discount_percentage'] = $special_discount_percent;
		$time['foreup_discount'] = $this->can_get_foreup_discount($time['start_front'], $teesheet_id);
		$time['pay_online'] = $this->pay_online_types[(int) $teesheet['pay_online']];
		
		// If walking only
		if($booking_carts == 0){		
			$time['green_fee'] = $green_fee;
			$time['rate_type'] = 'walking';
		
		// If both
		}else if($booking_carts == 1){
			$time['green_fee'] = $green_fee;
			$time['cart_fee'] = $cart_fee;
			$time['rate_type'] = 'both';
		
		// If Riding only
		}else{
			$time['green_fee'] = $green_fee + $cart_fee;
            $time['green_fee_tax'] = $time['green_fee_tax']  + $time['cart_fee_tax'];
			$time['cart_fee_tax']= 0;
			$time['rate_type'] = 'riding';
		}

		return true;
	}

	function apply_time_details(&$time, $front_time, $back_time){

		$back_spots = 4;
		if($back_time){
			$back_spots = 4 - (int) $back_time['player_count']; 
		}
		
		$front_spots = 4;
		if($front_time){
			$front_spots = 4 - (int) $front_time['player_count']; 
		}
		$available_spots = min($front_spots, $back_spots);
		if($available_spots < 0){
			$available_spots = 0;
		}
		$teesheet =& $this->teesheets[$time['teesheet_id']];

		$time['time'] = DateTime::createFromFormat('YmdHi', $time['start_front'] + 1000000)->format('Y-m-d H:i');
		$time['course_id'] = $teesheet['course_id'];
		$time['course_name'] = $teesheet['course_name'];
		$time['schedule_name'] = $teesheet['title'];
		$time['schedule_id'] = $time['teesheet_id'];	
		$time['available_spots'] = $available_spots;
		$time['minimum_players'] = (int) $teesheet['minimum_players'];
		$time['holes'] = (int) $this->filter_holes;
		$time['has_special'] = false;
		$time['special_discount_percentage'] = 0;
		$time['group_id'] = $this->group_id;
		$time['require_credit_card'] = $teesheet['require_credit_card'];
		$time['booking_class_id'] = $this->booking_class_id;
		$time['booking_fee_required'] = ($teesheet['booking_fee_enabled'] == 1);
		$time['booking_fee_price'] = false;
        $time['booking_fee_per_person'] = ($teesheet['booking_fee_per_person'] == 1);

		if(!empty($teesheet['booking_fee_item'])){
			if($this->customer_taxable){
				$time['booking_fee_price'] = (float) $teesheet['booking_fee_item']['total'];
			}else{
				$time['booking_fee_price'] = (float) $teesheet['booking_fee_item']['subtotal'];
			}
		}

		// If course is a private club and wants display who
		// is already playing on each time, gather up the players into a list
		$players = false;
		if($teesheet['show_full_details'] == 1){
			
			$players = array();
			for($player_num = 1; $player_num < 5; $player_num++){	
				
				// Since there could be more than one reservation per time slot,
				// the players are GROUP_CONCATed. Break apart the groups into a single
				// player list
				$parsed_player_name = explode('||', $front_time['person_name_'.$player_num]);
				$parsed_player_id = explode('||', $front_time['person_id_'.$player_num]);
				
				foreach($parsed_player_name as $key => $player_name){


					if(empty($player_name)){
						continue;
					}else{
						$players[] = array(
							'position' => $player_num,
							'person_id' => isset($parsed_player_id[$key])? (int) $parsed_player_id[$key]:null,
							'name' => $player_name
						);
					}
				}
			}

			// If number of players is greater than nameas attached, fill in
			// unknown players with primary player
			if(count($players) < (4 - $available_spots)) {
				if (!empty($players[0])) {
					$players[] = $players[0];
				}
			}

			if(!empty($players)){
				$time['players'] = $players;
			}
		}		

		return true;
	}

	function is_time_available($potential_time, $front_time = false, $back_time = false, $use_time_filters = true){
		
		$teesheet =& $this->teesheets[$potential_time['teesheet_id']];
		
		$front_players = (int) $front_time['player_count'];
		if($front_players > 4){
			$front_players = 4;
		}

		$back_players = (int) $back_time['player_count'];
		if($back_players > 4){
			$back_players = 4;
		}

		// If any conditions below are met, the tee time will be removed from the list
		if (
				(
					$back_time AND 
					(
						// If back nine time is during an event
						(int) $back_time['during_event'] != 0 OR 
						(
							// If tee time is filled up (unless course still wants to display it)
							$this->filter_players > 0 && (4 - $back_players) < (int) $this->filter_players
						) OR
						(
							(4 - $back_players) <= 0 && $teesheet['show_full_details'] == 0
						)
					)
				)
				OR
				(
					$front_time AND
					(
						// If front nine time is during an event
						(int) $front_time['during_event'] != 0 OR 
						(
							// If tee time is filled up (unless course still wants to display it)
							$this->filter_players > 0 && (4 - $front_players) < (int) $this->filter_players
						) OR
						(
							(4 - (int) $front_players) <= 0 && $teesheet['show_full_details'] == 0
						)
					)
				)
				OR
				(
				    $use_time_filters &&
                    (
                        // Outside of filtered range
                        ((int) substr($potential_time['start_front'], 8) < (int) $this->filter_time_start) OR
                        ((int) substr($potential_time['start_front'], 8) >= (int) $this->filter_time_end) OR
					
					    // Outside of teesheet allowed limits
					    ((int) $potential_time['start_front'] < (int) $teesheet['min_timestamp']) OR
					    ((int) $potential_time['start_front'] >= (int) $teesheet['max_timestamp'])
                    ) OR
					// Outside of course open/close times
					((int) substr($potential_time['start_front'], 8) < $teesheet['course_open']) OR
					((int) substr($potential_time['start_front'], 8) >= $teesheet['course_close'])
				)
		){
			return false;
		}
		
		return true;
	}

	function get_existing_times($potential_teetimes_array, $start, $end, $holes = 18, $teesheet_holes = 18){
		
		if(empty($potential_teetimes_array)){
			return array();
		}
		// Create temp table with potential teetimes
		$this->db->query('CREATE TEMPORARY TABLE foreup_potential_teetimes (
			start_front BIGINT,
			start_back BIGINT,
			teesheet_id INT,
			teesheet_holes TINYINT,
			PRIMARY KEY `teesheet_id_time` (`teesheet_id`, `start_front`, `start_back`))
			ENGINE=Memory');

		$this->db->insert_batch('potential_teetimes', $potential_teetimes_array);

		$teesheet_sql = 'AND teesheet_id IN ('.implode(',', $this->teesheet_ids).')';

		// Copy scheduled teetimes into a temporary table within date range
		$this->db->query("CREATE TEMPORARY TABLE foreup_scheduled_teetimes_temp (
			teesheet_id INT(10),
			start BIGINT(20),
			end BIGINT(20),
			player_count INT(3),
			type VARCHAR(32),
			side VARCHAR(32),
			status VARCHAR(32),
			holes INT(3),
			person_name_1 CHAR(100),
			person_name_2 CHAR(100),
			person_name_3 CHAR(100),
			person_name_4 CHAR(100),
			person_name_5 CHAR(100),
			person_id_1 CHAR(100),
			person_id_2 CHAR(100),
			person_id_3 CHAR(100),
			person_id_4 CHAR(100),
			person_id_5 CHAR(100),
			KEY `time` (`start`, `end`)
		) ENGINE=Memory AS (
			SELECT teesheet_id, 
				start, end, player_count, type, side, status, holes,
				person_name AS person_name_1,
				person_name_2,
				person_name_3,
				person_name_4,
				person_name_5,
				person_id AS person_id_1,
				person_id_2,
				person_id_3,
				person_id_4,
				person_id_5
			FROM foreup_teetime
			WHERE start >= {$start}
				AND end <= {$end}
				AND status != 'deleted'
				{$teesheet_sql}
		)");
		// If the tee sheet is only 9 holes, the re-round
		// will occur on the front nine
        $booking_side = "t.side = 'front' AND";
        $reround_side = "t.side = 'back' AND";

		$final_results = array();
		foreach($this->teesheet_ids as $teesheet_id){
			$final_results[(int) $teesheet_id] = array();
		}

		// Join potential teetimes with scheduled teetimes to return
		// which potential teetimes are NOT available for front nine
		$results = $this->db->query("SELECT p.start_front, t.teesheet_id,
				t.start, t.end, SUM(t.player_count) as player_count,
				t.holes, SUM(CASE WHEN type != 'teetime' THEN 1 ELSE 0 END) as during_event, t.side,
					CAST(GROUP_CONCAT(DISTINCT person_name_1 SEPARATOR '||') AS CHAR(100)) AS person_name_1, 
					CAST(GROUP_CONCAT(DISTINCT person_name_2 SEPARATOR '||') AS CHAR(100)) AS person_name_2, 
					CAST(GROUP_CONCAT(DISTINCT person_name_3 SEPARATOR '||') AS CHAR(100)) AS person_name_3, 
					CAST(GROUP_CONCAT(DISTINCT person_name_4 SEPARATOR '||') AS CHAR(100)) AS person_name_4, 
					CAST(GROUP_CONCAT(DISTINCT person_name_5 SEPARATOR '||') AS CHAR(100)) AS person_name_5, 
					CAST(GROUP_CONCAT(DISTINCT person_id_1 SEPARATOR '||') AS CHAR(100)) AS person_id_1,
					CAST(GROUP_CONCAT(DISTINCT person_id_2 SEPARATOR '||') AS CHAR(100)) AS person_id_2, 
					CAST(GROUP_CONCAT(DISTINCT person_id_3 SEPARATOR '||') AS CHAR(100)) AS person_id_3, 
					CAST(GROUP_CONCAT(DISTINCT person_id_4 SEPARATOR '||') AS CHAR(100)) AS person_id_4, 
					CAST(GROUP_CONCAT(DISTINCT person_id_5 SEPARATOR '||') AS CHAR(100)) AS person_id_5
			FROM foreup_potential_teetimes AS p
			INNER JOIN foreup_scheduled_teetimes_temp AS t
				ON (
					t.teesheet_id = p.teesheet_id AND
					t.start <= p.start_front AND 
					t.end > p.start_front
				)
			WHERE $booking_side status != 'deleted'
			GROUP BY t.teesheet_id, p.start_front");

		foreach($results->result_array() as $result){
			$time = $result['start_front'];
			$final_results[(int) $result['teesheet_id']]['front'][$time] = $result;
		}
		if($holes > 9){
			// Join potential teetimes with scheduled teetimes to return
			// which potential teetimes are NOT available for back nine
			$results = $this->db->query("SELECT p.start_front, t.teesheet_id,
					t.start, t.end, SUM(t.player_count) as player_count,
					t.holes, SUM(CASE WHEN type != 'teetime' THEN 1 ELSE 0 END) as during_event, t.side
				FROM foreup_potential_teetimes AS p
				INNER JOIN foreup_scheduled_teetimes_temp AS t
					ON (
						t.teesheet_id = p.teesheet_id AND
						t.start <= p.start_back AND 
						t.end > p.start_back
					)
				WHERE $reround_side status != 'deleted'
				GROUP BY t.teesheet_id, p.start_back");
			foreach($results->result_array() as $result){
				$time = $result['start_front'];
				$final_results[(int) $result['teesheet_id']]['back'][$time] = $result;
			}
		}					

		return $final_results;
	}

	// Adds fees to be purchased to cart
	function load_cart($course_id, $schedule_id, $time, $holes, $players, $carts, $customer_id, $price_class_id = false, $promo_code = false, $foreup_discount = 0, $booking_class_id = 0){
		
		$this->session->set_userdata('course_id', $course_id);
		$this->session->set_userdata('person_id', null);
		$this->session->set_userdata('employee_id', null);
		$this->session->set_userdata('terminal_id', 0);	

		$this->load->model('pricing');
		$this->load->model('special');
		$this->load->model('v2/Cart_model');
		$this->load->model('Promotion');
		
		// Create new cart
		$this->Cart_model->course_id = $course_id;
		$this->Cart_model->reset();

		$start = DateTime::createFromFormat('Y-m-d H:i', $time);
		$date = $start->format('Y-m-d');
		$time = $start->format('Hi');

		$fee_params = array(
			'holes' => $holes,
			'teesheet_id' => $schedule_id
		);
		
		// Get special (if there is one)
		$aggregate = 0;
		if($this->session->userdata('group_id')){
			$aggregate = 1;
		}

		$this->special->aggregate = $aggregate;

		$this->load->model("teesheet");
		$teesheet_info = $this->teesheet->get_info($schedule_id);
		if($teesheet_info->specials_enabled == 1){
			$special = $this->special->get_special($schedule_id, $date, $time, $booking_class_id);

		}

		// If a special is found for that time, get pricing using special ID
		if(!empty($special)){
			$fee_params['special_id'] = $special['special_id'];
			$fee_params['timeframe_id'] = false;
			$fee_params['aggregate_specials'] = $aggregate;

 		}else{
			$timeframe = $this->pricing->get_price_timeframe($schedule_id, $price_class_id, $date, $time);
			$fee_params['timeframe_id'] = $timeframe['timeframe_id'];
		}

		// If we are not booking with carts, only retrieve green fees
		if(!$carts){
			$fee_params['type'] = 'green_fee';
		}

		$this->Pricing->course_id = $course_id;
		$fees = $this->Pricing->get_fees($fee_params);
		
		// Check for promo code
		$promo_id = false;
		if(!empty($promo_code)){
			$coupon = $this->Promotion->get_coupon_by_online_code($promo_code, $start, $course_id);
			if(!empty($coupon)){
				$promo_id = $coupon['id'];
			}
		}

		// Add fees to cart
		$original_total = 0;
		$discounted_total = 0;
        $this->load->model('Course');
        $course_info = $this->Course->get_info($course_id);
        foreach($fees as $fee){
			$fee['quantity'] = (int) $players;
			//$fee['unit_price_includes_tax'] = 0; // Tax included is not properly implemented on tee time pricing yet, disabling until it is
        	$this->Cart_model->save_item(null, $fee);
		}

		if(!empty($customer_id)){
			$this->Cart_model->save_customer($customer_id, ['selected' => true]);
		}

		// Apply promo to cart as payment (if there is one)
		if(!empty($promo_id)){
			$this->Cart_model->save_payment(array(
				'amount' => 0,
				'record_id' => $promo_id,
				'type' => 'coupon'
			));				
		}

		if(!empty($foreup_discount) && empty($special) && empty($promo_id)){
			$items = $this->Cart_model->get_items(true);
			foreach($items as $item){				
				$item['discount_percent'] = $foreup_discount;
				$this->Cart_model->save_item($item['line'], $item);
			}
		}

		$totals = $this->get_cart_totals();
		$totals['promo_id'] = $promo_id;

		// Return cart total
		return $totals;
	}

	function get_cart_totals(){
		
		$cart_data = $this->Cart_model->get_totals();

		// This totals are a bit different than our standard cart totals
		// For email receipts etc, we only care about breaking out the discount
		// when the customer books a tee time
		$totals = [
			'total' => $cart_data['total_due'],
			'subtotal' => $cart_data['total_due'],
			'discount' => 0,
			'total_due' => $cart_data['total_due']
		];

		$items = $this->Cart_model->get_items(true);
		
		$discount = 0;
		foreach($items as $item){
			$discount += $item['discount_amount'];
		}

		$totals['subtotal'] += $discount;
		$totals['discount'] = $discount;

		return $totals;
	}

	function book_time($params, $paid = false, $booking_fee_paid = false){
        
        $this->load->model('Increment_adjustment');
		$this->load->model('teetime');
        $start = date('YmdHi', strtotime($params['time'])) - 1000000;
        $teesheet = (array) $this->teesheet->get_info($params['schedule_id']);

        $end = $this->Increment_adjustment->get_slot_time($start + 1000000, $params['schedule_id'],  1);

		$details = $this->get_teetime_message($params['group_id'], $paid, $booking_fee_paid);
		if(isset($params['total'])){
			$details .= " Amount due at course: ".$params['total'];
		}
		$paid_players = 0;
		$paid_carts = 0;

		if(empty($params['credit_card_id'])){
			$params['credit_card_id'] = 0;
		}

		$price_class = $params['price_class'];
		if($this->booking_class && $this->booking_class['use_customer_pricing'] == 1){
			$price_class = $this->db->select('price_class')
				->from('customers')
				->where('person_id', (int) $params['customer']['person_id'])
				->get()->row_array();
			$price_class = $price_class['price_class'];
		
		}else if($this->booking_class && !empty($this->booking_class['price_class'])){
			$price_class = $this->booking_class['price_class'];
		}

		$pending_reservation = false;
        if (!empty($params['pending_reservation'])) {
            $pending_reservation = true;
        }

		$event_data = array(
			'teesheet_id' => $params['schedule_id'],
			'start' => $start,
			'end' => $end,
			'side' => 'front',
            'status' => '',
			'allDay' => 'false',
			'holes' => $params['holes'],
			'player_count' => $params['players'],
			'paid_player_count' => $paid_players,
			'paid_carts' => $paid_carts,
			'type' => $pending_reservation ? 'pending_reservation' : 'teetime',
			'carts' => $params['carts'],
			'credit_card_id' => $params['credit_card_id'],
			'person_id' => $params['customer_id'],
			'person_name' => !empty($params['customer']['first_name'].' '.$params['customer']['last_name']) ? $params['customer']['first_name'].' '.$params['customer']['last_name'] : 'Online Reservation',
			'title' =>  $pending_reservation ? '' : (!empty($params['customer']['last_name']) ? $params['customer']['last_name'] : 'Online Booking'),
			'price_class_1' => $price_class,
			'booking_source' => 'online',
			'booker_id' => $params['customer_id'],
			'details'=> $details,
			'promo_id' => $params['promo_id'],
			'aggregate_group_id' => $params['group_id'],
            'duration' => 1,
            'pending_reservation_id' => $params['pending_reservation_id'],
            'booking_class_id' => $this->session->userdata('time_booking_class_id')
		);

		if(!empty($params['player_list']) && is_array($params['player_list'])){
			
			foreach($params['player_list'] as $person){
				if(empty($person) || empty($person['name']) || empty($person['position']) || $person['position'] > 4){
					continue;
				}
				$id_key = 'person_id_'.(int) $person['position'];
				$name_key = 'person_name_'.(int) $person['position'];
				$price_key = 'price_class_'.(int) $person['position'];

				if(!empty($person['person_id'])){
					$price_class = $this->db->select('price_class')
						->from('customers')
						->where('person_id', (int) $person['person_id'])
						->get()->row_array();
					
					$event_data[$price_key] = $price_class['price_class'];
				}

				$name = explode(' ',$person['name']);
				
				$event_data[$id_key] = (int) $person['person_id'];
				$event_data[$name_key] = $person['name'];
				$event_data['title'] .= ' - '.array_pop($name);
			}
		}

		// Book tee time
		$teetime_saved = $this->teetime->save($event_data, false, $response, true, $params['course_id']);
		// If tee time failed to book (conflict, etc)
		if($teetime_saved['success'] !== true){
			return false;
		}

		$this->eventDispatcher = \fu\Events\EventDispatcher::getDispatcher();
		$dispatchContext = new \Onoi\EventDispatcher\DispatchContext();
		$dispatchContext->set("tt_id",$teetime_saved['teetime_id']);
		$dispatchContext->set("teesheet_id",$params['schedule_id']);
		$this->eventDispatcher->dispatch("teetime.updated",$dispatchContext);

        if (!$pending_reservation) {
            // Send a confirmation email to customer
            $email_data = array(
                'TTID' => $teetime_saved['teetime_id'],
                'teetime_id' => $teetime_saved['teetime_id'],
                'new_booking' => true,
                'cancelled' => false,
                'person_id' => $params['customer_id'],
                'course_name' => $params['course']['name'],
                'course_phone' => $params['course']['phone'],
                'first_name' => $params['customer']['first_name'],
                'booked_date' => date('n/j/y', strtotime($params['time'])),
                'booked_time' => date('g:ia', strtotime($params['time'])),
                'booked_holes' => $params['holes'],
                'booked_players' => $params['players'],
                'course_id' => $params['course_id'],
                'tee_sheet' => $teesheet['title'],
                'has_online_booking' => true
            );

            $reservation_cc_email = $params['course']['reservation_email'];
            $user_data = $this->user->get_info($email_data['person_id']);

            if ($reservation_cc_email != '') {
                $emails = explode(',', $reservation_cc_email);
                $emails[] = (filter_var($user_data['username'], FILTER_VALIDATE_EMAIL) !== false) ? $user_data['username'] : $params['customer']['email'];
            } else {
                $emails[] = (filter_var($user_data['username'], FILTER_VALIDATE_EMAIL) !== false) ? $user_data['username'] : $params['customer']['email'];
            }
            $this->teetime->send_confirmation_email(
                $emails,
                'Tee Time Reservation Details',
                $email_data,
                $params['course']['email'],
                $params['course']['name'],
                $params['schedule_id']
            );


	        if($teesheet['notify_all_participants'] == 1 ){
		        $this->teetime->send_change_email($teetime_saved['teetime_id']);
	        }
        }

		$this->load->model('billing');
		$can_purchase = (bool) $this->billing->have_sellable_teetimes($start, $params['schedule_id']);		

		$reservation = array();
		$reservation['teetime_id'] = $teetime_saved['teetime_id'];
		$reservation['holes'] = $params['holes'];
		$reservation['carts'] = $params['carts'];
		$reservation['player_count'] = $params['players'];
		$reservation['reservation_time'] = date('g:ia', strtotime($params['time']));
		$reservation['time'] = date('Y-m-d H:i', strtotime($params['time']));
		$reservation['schedule_id'] = $params['schedule_id'];
		$reservation['teesheet_title'] = $teesheet['title'];
		$reservation['course_name'] = $params['course']['name'];
		$reservation['course_id'] = $params['course_id'];
		$reservation['start_timestamp'] = date('Y-m-d H:i', strtotime($params['time']));
		$reservation['foreup_discount'] = $can_purchase;
		$reservation['paid_player_count'] = 0;
		$reservation['available_spots'] = $params['available_spots'];
		$reservation['details'] = $details;

		return $reservation;
	}

    function delete_pending_reservation($reservation_id)
    {
        $this->db->where("(`TTID` = '$reservation_id' OR `TTID` = '{$reservation_id}b')");
        $this->db->where('type', 'pending_reservation');
        $this->db->limit(2);
        $this->db->update('teetime', array('status' => 'deleted', 'date_cancelled' => date('Y-m-d H:i:s')));

        $response = array();

        $response['success'] = $this->db->affected_rows() > 0;

        return $response;
    }

	function validate_reservation($teesheet_id, $time, $holes, $players, $carts, $credit_card_id = false, $booking_class_id = false, $is_aggregate = false, $person_id = false, $pending_reservation_id = false){
		
		$this->booking_class = false;
		$teesheet = $this->load_teesheet($teesheet_id, $booking_class_id, $is_aggregate);
		/** @var Course $course */
		$this->load->model("course");
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$time = DateTime::createFromFormat('Y-m-d H:i', $time);

        if(!$time){
			$this->response =array(
				'success' => false, 
				'msg' => 'Invalid reservation time, format should be YYYY-MM-DD HH:MM'
			);
			$this->response_code = 400;
			return false;
		}

		$timezone = null;
		if(!empty($teesheet['course_timezone'])){
			$timezone = new DateTimeZone($teesheet['course_timezone']);
		}
		$cur_date = new DateTime(null, $timezone);
		
		if((int) $teesheet['block_online_booking'] == 1 && !$this->online_hours->is_available($teesheet_id, $cur_date)){
			$this->response = array(
				'success' => false, 
				'msg' => 'Error, online booking is now closed for '.$teesheet['course_name'].' - '.$teesheet['title']
			);
			$this->response_code = 400;
			return false;
		}	

		// Make sure reservation is at least 1 hour into future
		if($time->getTimestamp() < (time() - 3600)){
			$this->response = array(
				'success' => false, 
				'msg' => 'Sorry, the tee time at '.
					$time->format('g:ia').
					' is no longer available',
				'time' => $time->format('Y-m-d H:i'),
				'schedule_id' => $teesheet_id
			);
			$this->response_code = 409;
			return false;
		}
		
		// Make sure time is not being booked outside of teesheet window
		if((int) $time->format('Hi') < (int) $teesheet['online_open_time'] || 
			(int) $time->format('Hi') > (int) $teesheet['online_close_time']){
			$this->response = array(
				'success' => false, 
				'msg' => 'Sorry, the tee time at '.
					$time->format('g:ia').
					' is no longer available',
				'time' => $time->format('Y-m-d H:i'),
				'schedule_id' => $teesheet_id
			);
			$this->response_code = 409;
			return false;
		}	

		if($teesheet['reservation_limit_per_day'] > 0){
			
			$reservation_count = $this->count_customer_reservations($time->format('Y-m-d'), $teesheet_id, $person_id);
			
			if($reservation_count >= $teesheet['reservation_limit_per_day']){
				
				$plural = '';
				if($teesheet['reservation_limit_per_day'] > 1){
					$plural = 's';
				}
				$this->response = array(
					'success' => false, 
					'msg' => 'You are only allowed to make '.$teesheet['reservation_limit_per_day'].' online reservation'.$plural.' per day.'
				);
				$this->response_code = 400;
				return false;
			}
		}
		if($course_info->reservation_limit_per_day > 0){
			$teesheets = $this->teesheet->get_teesheets($this->session->userdata('course_id'));
			$reservation_count = 0;
			foreach($teesheets as $teesheet_info){
				$reservation_count += $this->count_customer_reservations($time->format('Y-m-d'), $teesheet_info['teesheet_id'], $person_id);
			}

			if($reservation_count >= $course_info->reservation_limit_per_day){

				$plural = '';
				if($teesheet['reservation_limit_per_day'] > 1){
					$plural = 's';
				}
				$this->response = array(
					'success' => false,
					'msg' => 'You are only allowed to make '.$course_info->reservation_limit_per_day.' online reservation'.$plural.' per day.'
				);
				$this->response_code = 400;
				return false;
			}
		}


		if($teesheet['limit_holes'] != 0 && $holes != $teesheet['limit_holes']){
			$this->response = array(
				'success' => false, 
				'msg' => 'Invalid number of holes, only '.$teesheet['limit_holes'].' holes allowed'
			);
			$this->response_code = 400;
			return false;
		}
		
		if($teesheet['require_credit_card'] == 1 && empty($credit_card_id)){
			$this->response = array('success' => false, 'msg' => 'Error reserving teetime, a valid credit card is required');
			$this->response_code = 405;
			return false;
		}

		return true;
	}

	function count_customer_reservations($date, $teesheet_id, $person_id){

		$start_date = date('Y-m-d 00:00:00', strtotime($date));
		$end_date = date('Y-m-d 23:59:59', strtotime($date));
		$person_id = (int) $person_id;

		$this->db->select('COUNT( DISTINCT LEFT(TTID, 20) ) AS reservations', false)
			->from('teetime')
			->where("(person_id = {$person_id} OR 
				person_id_2 = {$person_id} OR 
				person_id_3 = {$person_id} OR 
				person_id_4 = {$person_id} OR 
				person_id_5 = {$person_id})")
			->where('teesheet_id', (int) $teesheet_id)
			->where("start_datetime BETWEEN '{$start_date}' AND '{$end_date}'")
			->where("status != 'deleted'")
			->where("booking_source = 'online'")
			->group_by('teesheet_id');

		$data = $this->db->get()->row_array();

		if(empty($data['reservations'])){
			return 0;
		}

		return (int) $data['reservations'];
	}

	function get_price_class($booking_class_id = false, $customer_id = false){

		if(empty($booking_class_id)){
			return false;
		}
		$booking_class = $this->Booking_class->get_info($booking_class_id);
		
		if(empty($booking_class->booking_class_id)){
			return false;
		}

		if($booking_class->use_customer_pricing == 1 && !empty($customer_id)){	
			$customer_info = $this->Customer->get_info($customer_id);
			if(!empty($customer_info->price_class)){
				return $customer_info->price_class;
			}
		}
			
		return $booking_class->price_class;
	}

	function get_teetime_message($group_id = false, $paid = false, $paid_booking_fee = false){
		
		$details = '';
		if(!empty($group_id)){
			$this->load->model('course_group');
			$group_data = $this->course_group->get($group_id);
			$details = "Reserved using online group booking ({$group_data['label']}) @ ".date('g:ia n/j T');
		
		}else{
			$details = "Reserved using online booking @ ".date('g:ia n/j T');
		}

		if($paid){
			$details .= ' - PAID ONLINE';
		
		}else if($paid_booking_fee){
			$details .= ' - PAID BOOKING FEE';
		}

		return $details;		
	}

	function has_booking_class_permission($booking_class_id, $user_id){
		
		if(empty($booking_class_id)){
			return true;
		}

		$this->load->model('Booking_class');
		$booking_class = $this->Booking_class->get_info($booking_class_id);

		if($this->session->userdata("price_class_id") != null && $this->session->userdata("price_class_id") === (int)$booking_class->price_class)
			return true;

		if(!empty($booking_class->booking_class_id) && $booking_class->online_booking_protected == 1){
			
			$customer_exist = $this->user->has_booking_class_access($user_id, $booking_class_id);
			if(!$customer_exist || empty($user_id)){
				return false;
			}
		}

		return true;		
	}

	function get_purchase_terminal($course_id = false){
		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');
		}		
		$row = $this->db->select('online_purchase_terminal_id')
			->from('courses')
			->where('course_id', $course_id)
			->get()->row_array();

		return $row['online_purchase_terminal_id'];
	}

	function get_invoice_terminal($course_id = false){
		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');
		}
		$row = $this->db->select('online_invoice_terminal_id')
			->from('courses')
			->where('course_id', $course_id)
			->get()->row_array();

		return $row['online_invoice_terminal_id'];
	}

	function get_credit_card_api_keys($course_id, $provider, $terminal_id = false){

		$api_keys = array();

		$course_row = $this->db->select('ets_key, mercury_id, mercury_password, 
			element_account_id, element_account_token, element_application_id,
			element_acceptor_id')
			->from('courses')
			->where('course_id', $course_id)
			->get()->row_array();

		if(!empty($terminal_id)){
			$terminal_row = $this->db->select('ets_key, mercury_id, mercury_password, 
				element_account_id, element_account_token, element_application_id,
				element_acceptor_id')
				->from('terminals')
				->where('terminal_id', $terminal_id)
				->get()->row_array();			
		}

		if($provider == 'ets'){
			if(!empty($terminal_row['ets_key'])){
				$row = $terminal_row;
			}else{
				$row = $course_row;
			}
			$api_keys['key'] = $row['ets_key'];
		
		}else if($provider == 'mercury'){
			if(!empty($terminal_row['mercury_id']) && !empty($terminal_row['mercury_password'])){
				$row = $terminal_row;	
			}else{
				$row = $course_row;
			}
			$api_keys['id'] = $row['mercury_id'];
			$api_keys['password'] = $row['mercury_password'];
		
		}else if($provider == 'element'){
			if(!empty($terminal_row['element_account_id']) && 
				!empty($terminal_row['element_account_token']) &&
				!empty($terminal_row['element_application_id']) &&
				!empty($terminal_row['element_acceptor_id'])
			){
				$row = $terminal_row;	
			}else{
				$row = $course_row;
			}			
			$api_keys['account_id'] = $row['element_account_id'];
			$api_keys['account_token'] = $row['element_account_token'];
			$api_keys['application_id'] = $row['element_application_id'];
			$api_keys['acceptor_id'] = $row['element_acceptor_id'];

		}else{
			return false;
		}

		return $api_keys;
	}

	function get_booking_fee($course_id, $teesheet_id, $booking_class_id = false, $taxable = true, $players = 1){
		
		$this->db->select('booking_fee_item_id, booking_fee_enabled, booking_fee_per_person')
			->from('booking_classes')
			->where('teesheet_id', $teesheet_id);

		if(!empty($booking_class_id)){
			$this->db->where('booking_class_id', $booking_class_id);
		}else{
			$this->db->where('is_aggregate', 1);
		}
		$booking_fee = $this->db->get()->row_array();

		if(empty($booking_fee['booking_fee_enabled']) || empty($booking_fee['booking_fee_item_id'])){
			return false;
		}

		$this->load->model('v2/Item_model');
		$items = $this->Item_model->get([
			'item_id' => $booking_fee['booking_fee_item_id'],
			'course_ids' => [(int) $course_id],
			'include_inactive' => true,
			'include_deleted' => true
		]);

		$item = false;
		if(!empty($items[0])){
			$item = $items[0];
			$item['quantity'] = 1;

            if($booking_fee['booking_fee_per_person'] == 1){
                $item['quantity'] = $players;
            }

			$this->Cart_model->calculate_item_totals($item, $taxable, $item['unit_price_includes_tax']);
		}

		return $item;	
	}

	function update_reservation($reservation_id, $params) {
        $update = array();
        if (!empty($params['player_count'])) {
            $update['player_count'] = $params['player_count'];
        }

        if (empty($update)){
            return false;
        }

        $this->db->where_in('TTID', array($reservation_id, $reservation_id.'b'));
        return $this->db->update('teetime', $update);
    }

	/**
	 * @return bool
	 */
	public function isAdminMode()
	{
		return $this->admin_mode;
	}

	/**
	 * @param bool $admin_mode
	 */
	public function setAdminMode($admin_mode)
	{
		$this->admin_mode = $admin_mode;
	}


}