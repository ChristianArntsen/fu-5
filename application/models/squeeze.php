<?php
class Squeeze extends CI_Model
{

    public function get_id($data) {

        $this->db->from('tee_sheet_squeezes');
        $this->db->where('tee_sheet_id', $data['tee_sheet_id']);
        $this->db->where('date', $data['date']);
        $this->db->where('start', $data['start']);
        $squeeze = $this->db->get()->row_array();

        return empty($squeeze['squeeze_id']) ? false : $squeeze['squeeze_id'];
    }

    public function get_all() {

        $this->db->from('tee_sheet_squeezes');
        $this->db->where('course_id', $this->config->item('course_id'));
        $this->db->where('date >', date('Y-m-d', strtotime('-14 days')));
        $squeezes = $this->db->get()->result_array();

        return $squeezes;
    }

    public function get_json_list($return_array = false) {
        $squeezes = $this->get_all();
        $result = array();

        foreach ($squeezes as $squeeze) {
            $result[$squeeze['tee_sheet_id']][$squeeze['date']][$squeeze['start']] = array('start'=>$squeeze['start'], 'end'=>$squeeze['end']);
        }

        if ($return_array) {
            return $result;
        }
        else {
            return json_encode($result);
        }
    }

    public function save($data) {

        $id = $this->get_id($data);
        return $this->db->insert('tee_sheet_squeezes', $data);
    }

    public function remove($data) {

        $id = $this->get_id($data);

        $this->db->where('squeeze_id', $id);
        $this->db->where('course_id', $this->config->item('course_id'));
        $this->db->limit(1);

        return $this->db->delete('tee_sheet_squeezes');
    }

}