<?php
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\ValidationExceptionInterface;

class Report_column extends CI_Model
{

	public function __construct()
	{

	}

	public function get_all($report_type)
	{
		$this->db->from('report_columns');
		$this->db->where('report_type',$report_type);
		$allColumns = $this->db->get()->result();
		return $allColumns;
	}

	public function update($data,$id)
	{
		$data->filterable = $data->filterable ? 'Y':'N';
		$data->visible = $data->visible ? 'Y':'N';
		$data->selectable = $data->selectable ? 'Y':'N';
		$data->sortable = $data->sortable ? 'Y':'N';
		$data->locked = $data->locked ? 'Y':'N';
		if(property_exists($data,"label_override")){
			unset($data->label_override);
			unset($data->report_type);
			unset($data->column);
			unset($data->table);
			unset($data->description);
			unset($data->type);
			unset($data->possible_values);
			unset($data->completeName);
			unset($data->dirty);
			unset($data->type_override);
		}

		$this->db->where('id', $id);
		$success = $this->db->update('reports_columns',$data);
	}

	public function insert($data)
	{

		if(isset($data->filter))
			unset($data->filter);
		$this->db->insert('reports_columns',$data);
		$id = $this->db->insert_id();
		return $id;
	}

}
?>
