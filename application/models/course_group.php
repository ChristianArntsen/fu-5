<?php
class Course_group extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	/*
	Gets information about a particular course group
	*/
	function get($group_id){
		$this->db->from('course_groups');
		$this->db->where('group_id', $group_id);
		$this->db->limit(1);
		$row = $this->db->get()->row_array();

		return $row;
	}
}