<?php
class Price_class extends CI_Model 
{
    function __construct(){
    	$this->default_price_class = array();
    	$this->cache = array();
    	$this->course_ids = false;
    }

    /*
	Gets information about a particular price class
	*/
	function get($class_id)
	{
		if (!$class_id || $class_id < 1) return false;
		
        if(!empty($this->cache[(int) $class_id])){
        	return $this->cache[(int) $class_id];
        }

		$this->db->select('class_id, name, default, color, cart, 
			cart_gl_code, green_gl_code, is_shared'); 
		$this->db->from('price_classes');
        $this->db->where('class_id', $class_id);

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            $row = $query->row_array();
            $this->cache[(int) $row['class_id']] = $row;
            return $row;
        }
        
        return false;
    }

    function get_id_from_name($name)
    {
        $this->db->select('class_id');
        $this->db->from('price_classes');
        $this->db->where('name', $name);
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            $result = $query->row();
            return $result->class_id;
        }
        else
        {
            // Create the price class
            $price_class_data = array(
                'name'=> $name,
                'course_id'=> $this->session->userdata('course_id'),
                'cart'=> 1,
                'default'=>0,
                'date_created'=>date('Y-m-d H:i:s')
            );
            $this->db->insert('price_classes', $price_class_data);
            return $this->db->insert_id();
        }
    }

	function get_all($params = [])
	{
        $params = elements(['person_id'], $params, null);

        $price_class_course_ids = array();
        $course_id = (int) $this->session->userdata('course_id');
        $this->load->model('course');
        
        $this->course->get_linked_course_ids($price_class_course_ids, 'shared_price_classes', $course_id);
        $price_class_course_id_list = implode(',', $price_class_course_ids);

        $this->course->get_linked_course_ids($group_course_ids, false, $course_id);
        $group_course_id_list = implode(',', $group_course_ids);

        // Select all available (inluding shared) price classes
        // Filter out default price classes from other courses
		$this->db->select('class_id, name, default, color, cart, cart_gl_code, green_gl_code, is_shared');
		$this->db->from('price_classes');
		$this->db->where("(
			course_id = {$course_id} OR 
			course_id IN ({$price_class_course_id_list}) OR (
				is_shared = 1 AND course_id IN ({$group_course_id_list})
			)
		)");
		$this->db->where("(`default` = 0 OR (`default` = 1 AND `course_id` = {$course_id}))");
        $this->db->order_by('default', 'DESC');		
        $query = $this->db->get();

        if(!empty($params['person_id'])){
        	return $this->filter_by_person($params['person_id'], $query->result_array());
        }

		return $query->result_array();
    }

	function get_customer_price_class_ids($person_id,$teesheet_id = null){

		$price_class_ids = [];
		if(empty($person_id)){
			return false;
		}

		if(isset($this->cache['person_'.$person_id])){
			return $this->cache['person_'.$person_id];
		}

		if(!$this->course_ids){
	        $course_ids = array();
	        $this->load->model('course');
	        $this->course->get_linked_course_ids($course_ids, false, $this->session->userdata('course_id'));
	        $this->course_ids = $course_ids;		
		}
		$this->readOnlyDB = $this->load->getDatabaseConnection('slave');
		$customer = $this->readOnlyDB->select('price_class')
			->from('foreup_customers')
			->where('person_id', $person_id)
			->where_in('course_id', $this->course_ids)
			->get()
			->row_array();

		$price_class_ids[(int) $customer['price_class']] = true;

		$this->load->model('Pass');
		$passes = $this->Pass->get(['customer_id' => $person_id]);

    	if(!empty($passes)){
			foreach($passes as $pass){
				
				if(!empty($pass['price_class_id'])){
					$price_class_ids[(int) $pass['price_class_id']] = true;
				}
				if(empty($pass['rules'])){
					continue;
				}

				foreach($pass['rules'] as $rule){
					if(!empty($rule['is_valid'])){
						$price_class_ids[(int) $rule['price_class_id']] = true;
					}
				}
			}
		}

		//Get all booking classes, if user has access, add price class.
		$this->load->model("Booking_class");
		$this->load->model("Online_Booking");
		$this->load->model("Teesheet");
		if(isset($teesheet_id)){
			$teesheet_ids = [$teesheet_id];
		} else {
			$teesheets = $this->Teesheet->get_teesheets($this->session->userdata('course_id'));
			foreach($teesheets as $teesheet){
				$teesheet_ids[] = $teesheet['teesheet_id'];
			}
		}
		foreach($teesheet_ids as $teesheet_id)
		{
			$booking_classes = $this->Booking_class->get_all($teesheet_id);
			foreach($booking_classes as $booking_class)
			{
				if(isset($price_class_ids[$booking_class['price_class']])){
					continue;
				}
				if ($this->Online_Booking->has_booking_class_permission($booking_class['booking_class_id'], $person_id)) {
					$price_class_ids[$booking_class['price_class']] = true;
				}
			}
		}


		$default_price_class = $this->get_default();
		$price_class_ids[(int) $default_price_class['class_id']] = true;
		$ids = array_keys($price_class_ids);

		$this->cache['person_'.$person_id] = $ids;
		return $ids;
	}
	
    function get_menu($params = null, $return_db_array = false){
		$rows = $this->get_all($params);
        if ($return_db_array) {
            return $rows;
        }
		$menu_data = array();
		
		foreach($rows as $row){
			$menu_data[$row['class_id']] = $row['name'];
		}
		
		return $menu_data;
	}
    
	function get_default($course_id = false){
		
		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');
		}
		
        if(!empty($this->default_price_class[$course_id])){
        	return $this->default_price_class[$course_id];
        }

        $this->db->from('price_classes');
        $this->db->where('default',1);
        $this->db->where('course_id', $course_id);
        $this->db->limit(1);

        $query = $this->db->get();
		$row = $query->row_array();
		$this->default_price_class[$course_id] = $row;

		return $row;
	}
	
	function get_customer_price_class($customer_id){
		
		$this->db->select('price_class');
		$this->db->from('customers');
		$this->db->where('person_id', (int) $customer_id);

		$query = $this->db->get();
		$row = $query->row_array();
		
		if(empty($row)){
			return false;
		}
		
		return (int) $row['price_class'];
	}
	
	function create_default($course_id = false){
		
		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');
		}
        $course_ids = array();
        $this->load->model('course');
        $this->course->get_linked_course_ids($course_ids, 'shared_price_classes', $course_id);

        $this->db->from('price_classes');
        $this->db->where('default',1);
        $this->db->where_in('course_id', $course_ids);
        $this->db->limit(1);

        $query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->row_array();	
			return $result['class_id'];
		}

        $data = array(
            'name' => 'Regular',
            'default' => 1,
            'cart' => 1,
            'course_id' => $course_id
        );

        $this->db->insert('price_classes', $data);
		return $this->db->insert_id();
	}
    
    /*
	Returns all the price classes for a given season
	*/
	function get_all_from_season($season_id, $limit=10000, $offset=0)
	{
		$this->db->from('price_classes')
			->join('seasonal_timeframes',"seasonal_timeframes.class_id = price_classes.class_id")
			->where("seasonal_timeframes.deleted",0)
			->where("price_classes.deleted",0)
			->where('seasonal_timeframes.season_id',$season_id)
			->group_by('price_classes.class_id')
			->order_by("default", "DESC")
			->order_by("name", "ASC")
			->limit($limit)
			->offset($offset);
		$results = $this->db->get();
		return $results->result();
	}
	
	/*
	 Returns all the unused price classes for a given season
	*/
	function get_available_from_season($season_id)
	{
		if(empty($season_id)){
			return false;
		}
        $price_classes = $this->get_all();
        $filtered = [];

        $rows = $this->db->select('class_id')
        	->from('season_price_classes')
        	->where('season_id', (int) $season_id)
        	->get()
        	->result_array();

        $price_class_used = [];
        foreach($rows as $row){
        	$price_class_used[(int) $row['class_id']] = true;
        }

        foreach($price_classes as $price_class){
        	if(empty($price_class_used[(int) $price_class['class_id']])){
        		$filtered[] = $price_class;
        	}
        }

        return $filtered;
	}
	
	/*
	 * Get a list of price classes we're already using this season
	 */
	function get_used($season_id)
	{
		//Get all the different price classes we're already using in the season
		$this->db
			->select('class_id')
			->from('seasonal_timeframes')
			->where('season_id',$season_id)
			->where('deleted',0)
			->group_by('class_id');
		$results = $this->db->get();
		return $results->result();
	}
	
	/*
	 * Get a list of price classes to suggest
	 */
	function get_suggestions($teesheet_id, $season_id, $term)
	{
		if (empty($teesheet_id)) $teesheet_id = 0;
		if (empty($season_id)) $season_id = 0;
		
		//Get all the different price classes that aren't defaults that we're using in the teesheet
		$this->db
			->select("price_category AS label, class_id AS value")
			->from('price_classes')
			->where('teesheet_id',$teesheet_id)
			->where('deleted',0)
			->where('default',0)
			->where('price_category !=','')
			->like('price_category', $term)
			->order_by('price_category','asc');
		$results = $this->db->get();
		$price_classes = $results->result();
		
		$used_classes = $this->get_used($season_id);
		foreach ($used_classes as $row)
			$used[] = $row->class_id;
		
		//Remove any used price classes from the original list
		foreach ($price_classes as $key => $price_class)
		{
			if (in_array($price_class->value, $used))
			{
				unset($price_classes[$key]);
			}
		}
		
		return $price_classes;
	}
	
	/*
	 * Deletes a price class
	 */
	function delete($class_id)
	{
		if (empty($class_id)){
			return false;
		}

		$this->db->delete('price_classes', array('class_id'=>$class_id, 'course_id'=>$this->session->userdata('course_id'), 'default'=>0));
		$price_deleted = $this->db->affected_rows();
		
		// Delete any timeframes associated with the price class
		if($price_deleted){
			$this->db->delete('seasonal_timeframes', array('class_id' => $class_id));
			return true;
		}
		
		return false;
	}
	
	/*
	Inserts or updates a price class
	*/
	function save($class_id = null, $name = null, $default = 0, $color = '', 
		$cart = 1, $green_gl_code = null, $cart_gl_code = null, $course_id = false){
		
		if(empty($name)){
			return false;
		}
		$course_id = $course_id ? $course_id : (int) $this->session->userdata('course_id');
		
		if(empty($cart_gl_code)){
			$cart_gl_code = null;
		}
		if(empty($green_gl_code)){
			$green_gl_code = null;
		}

        $course_ids = array();
        $this->load->model('course');
        $this->course->get_linked_course_ids($course_ids, false, $course_id);

		if(empty($class_id)){
            $this->db->from('price_classes');
			$this->db->where_in('course_id', $course_ids);
			$this->db->where('name', $name);
			$this->db->limit(1);
			$result = $this->db->get();
			
			if ($result->num_rows() == 1){
				$result = $result->row_array();
				return $result['class_id'];
			
			}else{
				$response = $this->db->insert('price_classes', array(
					'name' => $name, 
					'default' => (int) $default, 
					'color' => $color, 
					'cart' => $cart, 
					'green_gl_code' => $green_gl_code,
					'cart_gl_code' => $cart_gl_code,
					'course_id' => $course_id
				));
				$class_id = (int) $this->db->insert_id();
			}
		
		}else{
            $this->db->where_in('course_id', $course_ids);
			$response = $this->db->update('price_classes', array(
				'name' => $name, 
				'color' => $color, 
				'cart' => $cart,
				'green_gl_code' => $green_gl_code,
				'cart_gl_code' => $cart_gl_code
			), array('class_id' => $class_id));
		}

		return $class_id;
	}

	/*
	 Determines if a given class_id is a price class
	*/
	function exists($class_id)
	{
		$this->db->from('price_classes');
		$this->db->where('class_id',$class_id);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();
	
		return ($query->num_rows()==1);
	}
}
?>
