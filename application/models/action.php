<?php
class Action extends CI_Model
{
	function save($object_type = '', $object_id = '', $action = '', $comment = '', $employee_id = false, $course_id = false) {
		$data = array(
			'action_object_type' => $object_type,
			'action_object_id' => $object_id,
			'action' => $action,
			'comment' => $comment,
			'employee_id' => $employee_id ? $employee_id : $this->session->userdata('person_id'),
			'course_id' => $course_id ? $course_id : $this->session->userdata('course_id')
		);
		return $this->db->insert('actions', $data);
	}
	
	function get($object_type, $object_id) {
		$this->db->from('actions');
		$this->db->where('action_object_type',  $object_type);
		$this->db->where('action_object_id', $object_id);
		return $this->db->get();
	}
}
