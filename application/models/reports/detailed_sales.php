<?php
require_once("report.php");
class Detailed_sales extends Report
{
	function __construct()
	{
		parent::__construct();
	}

	public function getDataColumns()
	{
		return array(
            'summary' => array(
                array('data'=>lang('reports_sale_id'), 'align'=> 'left'),
                array('data'=>lang('reports_date'), 'align'=> 'left'),
                array('data'=>lang('reports_items'), 'align'=> 'left'),
                array('data'=>lang('reports_sold_by'), 'align'=> 'left'),
                array('data'=>lang('reports_sold_to'), 'align'=> 'left'),
                array('data'=>lang('reports_subtotal'), 'align'=> 'right'),
                array('data'=>lang('reports_total'), 'align'=> 'right'),
                array('data'=>lang('reports_tax'), 'align'=> 'right'),
                array('data'=>lang('reports_profit'), 'align'=> 'right'),
                array('data'=>lang('reports_payment_type'), 'align'=> 'right')
                ),
            'details' => array(
                array('data'=>lang('reports_item_number'), 'align'=> 'left'),
                array('data'=>lang('reports_name'), 'align'=> 'left'),
                array('data'=>lang('reports_category'), 'align'=> 'left'),
                array('data'=>lang('reports_subcategory'), 'align'=> 'left'),
                array('data'=>lang('reports_description'), 'align'=> 'left'),
                array('data'=>lang('reports_qty'), 'align'=> 'right'),
                array('data'=>lang('reports_subtotal'), 'align'=> 'right'),
                array('data'=>lang('reports_total'), 'align'=> 'right'),
                array('data'=>lang('reports_tax'), 'align'=> 'right'),
                array('data'=>lang('reports_profit'), 'align'=> 'right'),
                array('data'=>lang('reports_discount'), 'align'=> 'right')
                )
            );
	}

	public function getData()
	{
        $this->db->select('s.sale_id, s.number, sale_date, SUM(quantity_purchased) as items_purchased,
			CONCAT(employee.first_name," ",employee.last_name) as employee_name, 
			CONCAT(customer.first_name," ",customer.last_name) as customer_name, SUM(subtotal) as subtotal, 
			SUM(s.total) as total, SUM(s.tax) as tax, SUM(s.profit) as profit,
			payment_type, comment', false);
		$this->db->from('sales_items_temp AS s');
		$this->db->join('people as employee', 's.employee_id = employee.person_id', 'left');
		$this->db->join('people as customer', 's.customer_id = customer.person_id', 'left');

		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		if ($this->params['payment_type'] != 'all')
		{
			if ($this->params['payment_type'] == 'cash') {
				$this->db->where("(payment_type LIKE '%Cash%' OR payment_type LIKE '%Change%')");
			}
			else if ($this->params['payment_type'] == 'credit_card') {
				$this->db->where("(payment_type LIKE '%Credit Card%' OR payment_type LIKE '%M/C%'
					OR payment_type LIKE '%VISA%' OR payment_type LIKE '%AMEX%' OR payment_type LIKE '%DCVR%'
					OR payment_type LIKE '%DINERS%' OR payment_type LIKE '%JCB%' OR payment_type LIKE '%CC Refund%')");
			}
			else if ($this->params['payment_type'] == 'check') {
				$this->db->where("(payment_type LIKE '%Check%')");
			}
			else if ($this->params['payment_type'] == 'gift_card') {
				$this->db->where("(payment_type LIKE '%Gift Card%')");
			}
			else if ($this->params['payment_type'] == 'punch_card') {
				$this->db->where("(payment_type LIKE '%Punch Card%')");
			}
			else if ($this->params['payment_type'] == 'account') {
				$ccn = $this->config->item('customer_credit_nickname') ? $this->config->item('customer_credit_nickname') : 'Customer Credit';
				$mbn = $this->config->item('member_balance_nickname') ? $this->config->item('member_balance_nickname') : 'Member Balance';
				
				$this->db->where("(payment_type LIKE '%$ccn%' OR payment_type LIKE '%$mbn%')");
			}
		}
		if ($this->params['taxed'] != 'all')
		{
			if ($this->params['taxed'] == 'taxed')
			{
				$this->db->where('tax >', 0);
			}
			else if ($this->params['taxed'] == 'untaxed')
			{
				$this->db->where('tax', 0);
			}
					}
		if ($this->params['department'] != 'all')
			$this->db->where('s.department = "'.urldecode($this->params['department']).'"');
		if ($this->params['terminal'] != 'all')
			$this->db->where('s.terminal_id = "'.$this->params['terminal'].'"');
		if (isset($this->params['sale_id']) && $this->params['sale_id'] != '')
			$this->db->where('(s.sale_id = '.(int) $this->params['sale_id'].' OR s.number = '.(int) $this->params['sale_id'].')');
		if ($this->params['employee_id'] != '' && $this->params['employee_id'] != 0) {
			$this->db->where('s.employee_id = "'.$this->params['employee_id'].'"');
        }
        if(isset($this->params['hide_invoice_sales']) && $this->params['hide_invoice_sales'] === true){
			$this->db->join('foreup_invoices AS i', 'i.sale_id = s.sale_id', 'left');
			$this->db->where('i.invoice_id IS NULL');
		}
		$this->db->where('s.deleted', 0);
		$this->db->group_by('s.sale_id');
		$this->db->order_by('sale_date');

		$data = array();
		$summary = $this->db->get();
		//echo $this->db->last_query();
		$data['summary'] = $summary->result_array();
		$data['details'] = array();

		return $data;
	}

	public function get_quickbooks_data($courseId, $offset = 0, $numRecords = 5000){

		$data = array();

		// Select all sales from temporary table
		$this->db->select('sales_items_temp.sale_id, sales_items_temp.sale_date AS sale_time,
			CONCAT(employee.first_name," ",employee.last_name) as employee_name, employee.person_id AS employee_id,
			CONCAT(customer.first_name," ",customer.last_name) as customer_name, customer.person_id AS customer_id', false);
		$this->db->from('sales_items_temp');
		$this->db->join('people as employee', 'sales_items_temp.employee_id = employee.person_id', 'left');
		$this->db->join('customers as c_customer', 'sales_items_temp.customer_id = c_customer.person_id AND c_customer.course_id = '.(int) $courseId, 'left');
		$this->db->join('people as customer', 'c_customer.person_id = customer.person_id', 'left');

		if ($this->params['sale_type'] == 'sales'){
			$this->db->join('quickbooks_sync AS sync', "sync.record_id = sale_id AND sync.record_type = 'sale' AND sync.course_id = ".(int) $courseId, 'left');
			$this->db->where('quantity_purchased > 0');
		}elseif ($this->params['sale_type'] == 'returns'){
			$this->db->join('quickbooks_sync AS sync', "sync.record_id = sale_id AND sync.record_type = 'return' AND sync.course_id = ".(int) $courseId, 'left');
			$this->db->where('quantity_purchased < 0');
		}

		$this->db->where('sync.record_id IS NULL');
		$this->db->where('sales_items_temp.deleted', '0');
		$this->db->group_by('sales_items_temp.sale_id');
		$this->db->limit($numRecords, $offset);

		$sales = $this->db->get()->result_array();

		// Loop through each sale and retrieve the individual items in that sale
		foreach($sales as $key => $summary){

			// Retrieve items, invoices, or item kits attached to sale
			$this->db->select('items.item_id, items.item_number as item_number, items.name as item_name,
				item_kits.item_kit_id, item_kits.item_kit_number as item_kit_number, item_kits.name as item_kit_name,
				invoices.invoice_id, invoices.invoice_number, invoices.name AS invoice_name,
				sales_items_temp.category, sales_items_temp.subcategory, quantity_purchased, serialnumber,
				sales_items_temp.description, sales_items_temp.subtotal,sales_items_temp.total,
				sales_items_temp.tax, sales_items_temp.profit, sales_items_temp.discount_percent');
			$this->db->from('sales_items_temp');
			$this->db->join('items', "sales_items_temp.item_id = items.item_id AND ".$this->db->dbprefix('items').".course_id = ".(int) $courseId, 'left');
			$this->db->join('item_kits', "sales_items_temp.item_kit_id = item_kits.item_kit_id AND ".$this->db->dbprefix('item_kits').".course_id = ".(int) $courseId, 'left');
			$this->db->join('invoices', "sales_items_temp.invoice_id = invoices.invoice_id AND ".$this->db->dbprefix('invoices').".course_id = ".(int) $courseId, 'left');
			$this->db->where(array('sales_items_temp.sale_id' => $summary['sale_id']));

			$sales[$key]['total'] = 0.00;
			$items = $this->db->get()->result_array();

			foreach($items as $item){
				$sales[$key]['total'] += $item['total'];
			}

			$sales[$key]['details'] = $items;

			// Retrieve all payments on sale (exlcuding tips)
			$result = $this->db->query("SELECT sale_id, payment_amount, payment_type
				FROM ".$this->db->dbprefix('sales_payments')."
				WHERE sale_id = ".(int) $summary['sale_id']."
					AND payment_type NOT LIKE '%tip%'
				ORDER BY IF(payment_type='Change issued', 1, 0) DESC");

			$sales[$key]['payments'] = $result->result_array();
		}

		return $sales;
	}
}
?>
