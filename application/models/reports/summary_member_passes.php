<?php
require_once("report.php");
class Summary_member_passes extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns() {
		return array(
            array('data'=>lang('reports_pass'), 'align'=>'left'),
            array('data'=>lang('reports_first_name'), 'align'=> 'left'),
            array('data'=>lang('reports_last_name'), 'align'=> 'left'),
            array('data'=>lang('reports_start_date'), 'align'=> 'left'),
            array('data'=>lang('reports_expiration_date'), 'align'=> 'left'),
            array('data'=>lang('reports_loyalty_points'), 'align'=> 'left')
        );
	}
	
	public function getData() {   
        $data = array();
        $this->db->select('customer_passes.label as pass, people.first_name as first_name, people.last_name as last_name, customer_pass_members.start_date as start_date, customer_pass_members.expiration as expiration_date, loyalty_points', false);
        $this->db->from('customer_passes');
        $this->db->join('customer_pass_members', 'customer_passes.pass_id = customer_pass_members.pass_id');
        $this->db->join('people', 'customer_pass_members.person_id = people.person_id');
        $this->db->join('customers', 'customer_pass_members.person_id = customers.person_id');
        $this->db->where('customers.deleted', 0);
        $this->db->where('customers.course_id', $this->session->userdata('course_id'));
        $result = $this->db->get()->result_array();
        return $result;     
	}
	
	public function getSummaryData() {
	}
}
?>