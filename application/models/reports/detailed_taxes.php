<?php
require_once("report.php");
class Detailed_taxes extends Report
{
	function __construct()
	{
		parent::__construct();
	}

	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_sales_activity'), 'align'=>'left'), array('data'=>lang('reports_sales'), 'align'=>'right'));
	}

	public function getData(){

		$this->db->select('SUM(IF(tax != 0.00, subtotal, 0)) AS taxable,
			SUM(IF(tax = 0.00, total, 0)) AS non_taxable,
			SUM(total) AS total,
			SUM(subtotal) AS subtotal,
			SUM(tax) AS tax', false);
		$this->db->from('sales_items_temp');

		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
        if ($this->params['department'] != 'all') {
            $this->db->where('department = "'. $this->params['department'] . '"');
        }
        if ($this->params['cat'] != 'all') {
            $this->db->where('category = "'. $this->params['cat'] . '"');
        }
        if ($this->params['sub_cat'] != 'all') {
            $this->db->where('subcategory = "'. $this->params['sub_cat'] . '"');
        }
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);

		$query = $this->db->get();
		$taxes_array = $query->row_array();

		$totals_data = array(
			'Non Txbl:' => array('percent'=>'Non Taxable:','tax'=>$taxes_array['non_taxable']),
			'Txbl:' => array('percent'=>'Taxable:','tax'=>$taxes_array['taxable']),
			'Tax:' => array('percent'=>'Tax:','tax'=>$taxes_array['tax']),
			'Subtotal:' => array('percent'=>'Subtotal:','tax'=>$taxes_array['subtotal']),
			'Total:' => array('percent'=>'Total:','tax'=>$taxes_array['total'])
		);

		return $totals_data;
	}

	public function getSummaryData()
	{
		return;
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		$this->db->from('sales_items_temp');

		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
        if ($this->params['department'] != 'all') {
            $this->db->where('department = "'. $this->params['department'] . '"');
        }
        if ($this->params['cat'] != 'all') {
            $this->db->where('category = "'. $this->params['cat'] . '"');
        }
        if ($this->params['sub_cat'] != 'all') {
            $this->db->where('subcategory = "'. $this->params['sub_cat'] . '"');
        }

		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		return $this->db->get()->row_array();
	}
}
?>
