<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Model
{
	public function get_people($teetime_id){
		$this->db->select('ep.event_person_id, p.person_id, p.first_name, p.last_name, p.email,
			p.phone_number, ep.paid, p.birthday, ep.checked_in, ep.player_position, ep.hole, ep.label');
		$this->db->from('event_people AS ep');
		$this->db->join('people AS p', 'p.person_id = ep.person_id', 'left');
		$this->db->where('ep.teetime_id', $teetime_id);
		$this->db->order_by('p.last_name, p.first_name, ep.event_person_id');
		$query = $this->db->get();

		$rows = $query->result_array();
		return $rows;
	}

	public function save_people($teetime_id, $people_array){

		if(empty($teetime_id) || empty($people_array)){
			return false;
		}

		$data = array();
		foreach($people_array as $person){
			$data[] = array('person_id' => $person['person_id'], 'label' => $person['label'], 'teetime_id' => $teetime_id);
		}

		$this->db->insert_batch('event_people', $data);
		return true;
	}

	public function save_person($teetime_id, $person_id, $label){
		$success = false;
        $event_person_id = false;
		if(!empty($teetime_id) && (!empty($person_id) || !empty($label))){
			// Check if person is already part of event
            if (!empty($person_id)) {
                $query = $this->db->get_where('event_people', array('teetime_id' => $teetime_id, 'person_id' => $person_id));
                if ($query->num_rows() > 0) {
                    return false;
                }

                $success = $this->db->insert('event_people', array('teetime_id' => $teetime_id, 'person_id' => $person_id));
                $event_person_id = (int) $this->db->insert_id();
            }
            else if (!empty($label)){
                $success = $this->db->insert('event_people', array('teetime_id' => $teetime_id, 'person_id' => 0, 'label' => $label));
                $event_person_id = (int) $this->db->insert_id();
            }
		}

		return array('success' => $success, 'event_person_id' => $event_person_id);
	}

	public function save_shotgun_person($person_info){
		$success = false;
		if(!empty($person_info['teetime_id'])){
			// Check if person is already part of event ... also need to query on label and person_id = 0 
			$query = $this->db->get_where('event_people', array('teetime_id'=>$person_info['teetime_id'], 'player_position'=>$person_info['player_position'], 'hole'=>$person_info['hole']));
			if($query->num_rows() > 0){
				$this->db->where('player_position', $person_info['player_position']);
				$this->db->where('hole', $person_info['hole']);
				$this->db->where('teetime_id', $person_info['teetime_id']);
				$success = $this->db->update('event_people', $person_info);
			}
			else {
				$success = $this->db->insert('event_people', $person_info);
			}
			$this->db->query("UPDATE foreup_teetime SET player_count = player_count + 1 WHERE TTID = '{$person_info['teetime_id']}'");
		}

		return $success;
	}
	public function delete_shotgun_person($person_info){
		$success = false;
		if(!empty($person_info['teetime_id'])){
			// Check if person is already part of event ... also need to query on label and person_id = 0 
			$this->db->where('teetime_id', $person_info['teetime_id']);
			$this->db->where('player_position', $person_info['player_position']);
			$this->db->where('hole', $person_info['hole']);
			$this->db->limit(1);
			$success = $this->db->delete('event_people');
            $this->db->query("UPDATE foreup_teetime SET player_count = player_count - 1 WHERE TTID = '{$person_info['teetime_id']}'");
		}

		return $success;
	}

	public function person_checkin($teetime_id, $event_person_id, $checked_in = true, &$message = ''){
		$checked_in = (bool) $checked_in;

		if(!empty($checked_in)){
			$date = date('Y-m-d H:i:s');
		}else{
			$date = null;
		}

        if ($this->config->item('current_day_checkins_only')) {
            // Get tee time
            $tee_time_info = $this->teetime->get_info($teetime_id);
            $tt_date = date('Y-m-d', strtotime($tee_time_info->start + 1000000));
            $todays_date = date('Y-m-d');
            if ($tt_date != $todays_date) {
                // If it doesn't belong to today, then prevent action
                $message = "You can only check in players on today's date.";
                return false;
            }
        }

		$success = $this->db->update('event_people', array('checked_in' => $checked_in, 'date_checked_in'=>$date), array(
			'teetime_id'=>$teetime_id,
			'event_person_id'=>$event_person_id
		));
		
		$teetime_id = substr($teetime_id, 0, 20);
		$this->db->query("UPDATE foreup_teetime SET paid_player_count = paid_player_count + 1 WHERE TTID = '{$teetime_id}' OR TTID = '{$teetime_id}b' LIMIT 2");
		return $success;
	}

	public function person_pay($teetime_id, $event_person_id, $paid = true, $customer_id = false){
		
		$paid = (int) $paid;

		if(!empty($paid)){
			$date = date('Y-m-d H:i:s');
		}else{
			$date = null;
		}
		$where_array =  array(
            'teetime_id'=>$teetime_id
        );
		if ($customer_id) {
		    $where_array['person_id'] = $customer_id;
        }
        else {
            $where_array['event_person_id'] = $event_person_id;
        }


		$success = $this->db->update('event_people', array('paid' => $paid, 'date_paid'=>$date), $where_array);
		return $success;
	}
	public function shotgun_person_checkin($teetime_id, $hole, $player_position, $checked_in = 1){
		$checked_in = (int) $checked_in;

		if(!empty($checked_in)){
			$date = date('Y-m-d H:i:s');
		}else{
			$date = null;
		}

		$this->db->limit(1);
		$success = $this->db->update('event_people', array('checked_in' => $checked_in, 'date_checked_in'=>$date), array(
			'teetime_id'=>$teetime_id,
			'hole'=>$hole,
			'player_position'=>$player_position
		));
		
		$teetime_id = substr($teetime_id, 0, 20);
		$plus_minus = $checked_in ? 1 : -1;
		$this->db->query("UPDATE foreup_teetime SET paid_player_count = paid_player_count + $plus_minus WHERE TTID = '{$teetime_id}' OR TTID = '{$teetime_id}b' LIMIT 2");
		return $success;
	}

	public function shotgun_person_pay($teetime_id, $hole, $player_position, $paid = 1){
		
		$paid = (int) $paid;

		if(!empty($paid)){
			$date = date('Y-m-d H:i:s');
		}else{
			$date = null;
		}
		
		$this->db->limit(1);
		$success = $this->db->update('event_people', array('paid' => $paid, 'date_paid'=>$date), array(
			'teetime_id'=>$teetime_id,
			'hole'=>$hole,
			'player_position'=>$player_position
		));
		
		$teetime_id = substr($teetime_id, 0, 20);
		$plus_minus = $paid ? 1 : -1;
		$this->db->query("UPDATE foreup_teetime SET paid_player_count = paid_player_count + $plus_minus WHERE TTID = '{$teetime_id}' OR TTID = '{$teetime_id}b' LIMIT 2");
		return $success;
	}

	public function delete_person($teetime_id, $event_person_id){
		$success = $this->db->delete('event_people', array('teetime_id'=>$teetime_id, 'event_person_id'=>(int) $event_person_id));
		return $success;
	}

	public function delete_people($teetime_id){
		$success = $this->db->delete('event_people', array('teetime_id'=>$teetime_id));
		return $success;
	}
}
