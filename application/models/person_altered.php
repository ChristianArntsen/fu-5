<?php
class Person_Altered extends CI_MODEL {
    public function save($log_data)
    {
        //
        /* foreup_person_altered
         * tracks persons affected
         * editor_id INT(10) UNSIGNED NOT NULL,
         * gmt_edited datetime,
         * action_type_id INT(10) UNSIGNED,
         * person_edited INT(10),
         * tables_updated VARCHAR(256)
         * PRIMARY KEY (`editor_id`,`gmt_edited`,`person_edited`)
         * */
        if($this->db->insert('foreup_person_altered',$log_data))
        {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        return false;
    }
}
?>