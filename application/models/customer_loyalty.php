<?php
class Customer_loyalty extends CI_Model
{
	function get_all_by_sale_id($sale_id)
	{
		$result = $this->db->query("SELECT * FROM (`foreup_loyalty_transactions`) WHERE `sale_id` = '$sale_id' ORDER BY `trans_date` desc");
        return $result->result_array();
	}
	function get_rates($loyalty_package_id = null)
	{
		//$this->db->select('loyalty_rate_id, label, type, value, points_per_dollar, dollars_per_point');
		$this->db->from('loyalty_rates');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('loyalty_package_id', $loyalty_package_id);
		return $this->db->get();
	}
	function get_points_per_dollar($item, $loyalty_package_id = null)
	{
		$tee_time_index = 0;
		$price_category = 0;
		$timeframe_id = 0;
		if (!empty($item['price_category']))
		{
			$tee_time_index = $item['teetime_type'];
			$price_category = $item['price_category'];
		}
		if (!empty($item['price_class_id']))
		{
			$price_category = $item['price_class_id'];
		}
		if (!empty($item['timeframe_id']))
		{
			$timeframe_id = $item['timeframe_id'];
		}

		if (isset($item['item_id']) || isset($item['item_kit_id']) || isset($item['department']) || isset($item['category']) || isset($item['subcategory']))
		{
			$where = array();		
			$this->db->from('loyalty_rates');
			$this->db->where('course_id', $this->session->userdata('course_id'));
            $this->db->where('loyalty_package_id', $loyalty_package_id);
			$tti_sql = ($tee_time_index) ?" AND (`tee_time_index` = $tee_time_index)" : '';
			$pc_sql = ($price_category) ? " AND (`price_category` = $price_category)" : '';

			if (isset($item['item_id']))
				$where[] = "(`type` = 'item' AND `value` = ".$this->db->escape($item['item_id'])." $tti_sql $pc_sql)";
			if (isset($item['item_kit_id']))
				$where[] = "(`type` = 'item_kit' AND `value` = ".$this->db->escape($item['item_kit_id']).")";
			if (isset($item['department']))
				$where[] = "(`type` = 'department' AND `value` = ".$this->db->escape($item['department']).")";
			if (isset($item['category']))
				$where[] = "(`type` = 'category' AND `value` = ".$this->db->escape($item['category']).")";
			if (isset($item['subcategory']))
				$where[] = "(`type` = 'subcategory' AND `value` = ".$this->db->escape($item['subcategory']).")";

			if (count($where) > 0)
				$this->db->where('('.implode(' OR ', $where).')');
			if (!empty($timeframe_id)){
				$this->db->where("((limit_timeframe = 1 AND timeframe_id = $timeframe_id) OR (limit_timeframe=0))");
			}
			$this->db->order_by('points_per_dollar', "desc");
			
			$result = $this->db->get();
			if ($result->num_rows() > 0)
				return $result->row_array();
		}
	
		return array('points_per_dollar'=>0);
	}
	function get_dollars_per_point($item, $loyalty_package_id = null)
	{
		$tee_time_index = 0;
		$price_category = 0;
		if (!empty($item['price_category']))
		{
			$tee_time_index = $item['teetime_type'];
			$price_category = $item['price_category'];
		}
		if (!empty($item['price_class_id']))
		{
			$price_category = $item['price_class_id'];
		}

		if (isset($item['item_id']) || isset($item['item_kit_id']) || isset($item['department']) || isset($item['category']) || isset($item['subcategory']))
		{
			$where = array();		
			$this->db->from('loyalty_rates');
			$this->db->where('course_id', $this->session->userdata('course_id'));
            $this->db->where('loyalty_package_id', $loyalty_package_id);
			$tti_sql = ($tee_time_index) ?" AND (`tee_time_index` = $tee_time_index)" : '';
			$pc_sql = ($price_category) ? " AND (`price_category` = $price_category)" : '';
			 
			if (isset($item['item_id']))
				$where[] = "(`type` = 'item' AND `value` = ".$this->db->escape($item['item_id'])." $tti_sql $pc_sql)";
			if (isset($item['item_kit_id']))
				$where[] = "(`type` = 'item_kit' AND `value` = ".$this->db->escape($item['item_kit_id']).")";
			if (isset($item['department']))
				$where[] = "(`type` = 'department' AND `value` = ".$this->db->escape($item['department']).")";
			if (isset($item['category']))
				$where[] = "(`type` = 'category' AND `value` = ".$this->db->escape($item['category']).")";
			if (isset($item['subcategory']))
				$where[] = "(`type` = 'subcategory' AND `value` = ".$this->db->escape($item['subcategory']).")";
			if (count($where) > 0)
				$this->db->where('('.implode(' OR ', $where).')');
			$this->db->order_by('dollars_per_point', "desc");
			
			$result = $this->db->get();
			if ($result->num_rows() > 0)
				return $result->row_array();
		}

		return array('dollars_per_point'=>0);
	}
	function save(&$loyalty_info, $loyalty_id = false) {
		if (!$loyalty_id)
		{
			$result = $this->db->insert('loyalty_rates', $loyalty_info);

			if ($result) {
			    $loyalty_info['loyalty_rate_id'] = $this->db->insert_id();
            }

            return $result;
		}
		else 
		{
			$this->db->where('loyalty_rate_id', $loyalty_id);
			return $this->db->update('loyalty_rates', $loyalty_info);
		}
	}
	function delete($loyalty_id) {
		$this->db->where('loyalty_rate_id', $loyalty_id);
		return $this->db->delete('loyalty_rates');
	}
	function save_transaction($person_id = -1, $trans_description='', $add_subtract='', $trans_details='', $sale_id='', $employee_id = false, $loyalty_package_id='')
	{
		if($employee_id === false){
			$logged_in_employee = $this->Employee->get_logged_in_employee_info();
			if(!empty($logged_in_employee)){
				$employee_id = $logged_in_employee->person_id;
			}else{
				$employee_id = 0;
			}
		}
		
		$cur_customer_info = $this->Customer->get_info($person_id, $this->session->userdata('course_id'));
		if ($add_subtract == '')
			$add_subtract = $this->input->post('add_subtract');
        if ($trans_description == '')
            $trans_description = $this->input->post('trans_comment');
        if ($loyalty_package_id == '')
            $loyalty_package_id = $this->input->post('loyalty_package_id');
        if (!is_numeric($loyalty_package_id)) {
            $loyalty_package_id = null;
        }
		$trans_data = array
		(
		    'sale_id'=>$sale_id,
			'trans_date'=>date('Y-m-d H:i:s'),
			'trans_customer'=>$person_id,
			'trans_user'=>$employee_id,
			'trans_comment'=>$trans_description,
			'trans_description'=>$trans_details,
			'trans_amount'=>$add_subtract,
            'customer_package_id' => null
		);

        if ($loyalty_package_id === null) {
            //Update account balance
            $customer_data = array(
                'loyalty_points' => $cur_customer_info->loyalty_points + $add_subtract
            );
            $person_data = array(
                'address_2' => $cur_customer_info->address_2
            );

            $giftcards = array();
            $groups = array();
            $passes = array();
            if (!$this->Customer->save($person_data, $customer_data, $person_id, $giftcards, $groups, $passes)) {
                return false;
            }
        } else {
            $customers_loyalty_package = $this->Customers_loyalty_package->get($person_id, $loyalty_package_id)->result_object()[0];
            $trans_data['customer_package_id'] = $customers_loyalty_package->id;
            $this->Customers_loyalty_package->adjust_balance($person_id, $loyalty_package_id, $add_subtract);
        }

        $this->insert($trans_data);
        return true;
	}	
	
	function insert($loyalty_transaction_data)
	{
		return $this->db->insert('loyalty_transactions',$loyalty_transaction_data);
	}
	
	function get_loyalty_transaction_data_for_customer($customer_id)
	{
		$result = $this->db->query("SELECT *, DATE_FORMAT(trans_date, '%c-%e-%y %l:%i %p') AS date FROM (`foreup_loyalty_transactions`) WHERE `trans_customer` = '$customer_id' ORDER BY `trans_date` desc");
		return $result->result_array();		
	}
}	
