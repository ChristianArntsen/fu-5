<?php
namespace models\AclCallerPermissions;

class Row
{
    public $caller_type;
    public $caller_id;
    public $type;
    public $action;
    public $resource_type;
    public $resource_id;
}