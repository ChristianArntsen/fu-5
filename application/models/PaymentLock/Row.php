<?php
namespace models\PaymentLock;

class Row
{
    public $id;
    public $identifier;
    public $invoice_id;
    public $payment_id;
    public $time_open;
    public $time_close;
    public $course_id;
}