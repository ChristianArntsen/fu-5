<?php
class Blackline_devices extends CI_Model {

    function get_terminal_info($terminal_id = false, $blackline_terminal_id = false) {
        if (empty($terminal_id)) {
            $terminal_id = 0;
        }

        $this->db->from('blackline_devices');
        if ($blackline_terminal_id) {
            $this->db->where('blackline_terminal_id', $blackline_terminal_id);
        }
        else {
            $this->db->where('terminal_id', $terminal_id);
            $this->db->where('course_id', $this->config->item('course_id'));
        }
        $this->db->where('deleted', 0);
        $this->db->limit(1);

        $bl_info = $this->db->get()->row_array();

        if (!empty($bl_info['blackline_api_key'])) {
            return $bl_info;
        }

        return false;
    }

    function save($device_data, $device_id = false) {
        $response = array('success'=>false);

        if (!empty($device_data)) {
            if ($device_id) {
                $this->db->where('blackline_device_id', $device_id);
                $this->db->where('course_id', $this->config->item('course_id'));
                $response['success'] = $this->db->update('blackline_devices', $device_data);
            } else {
                $data = 'INSERT IGNORE INTO foreup_blackline_devices (course_id, blackline_terminal_id, blackline_api_key, blackline_api_password) VALUES';
                $data .= ' (' . $this->config->item('course_id') . ',';
                $data .= ' ' . $device_data['TerminalID'] . ',';
                $data .= ' "' . $device_data['ApiKey'] . '",';
                $data .= ' "' . $device_data['ApiPassword'] . '")';

                $response['success'] = $this->db->query($data);
            }
        }

        return $response;
    }

    function remove($device_id) {
        $response = array('success'=>false);

        if (!empty($device_id)) {
            $this->db->where('course_id', $this->config->item('course_id'));
            $this->db->where('blackline_device_id', $device_id);
            echo 'deleting';
            $response['success'] = $this->db->delete('blackline_devices');
            echo 'blackline deleted';
        }

        return $response;
    }

    function get_register_url() {
        $this->load->model('Employee');
        $employee_info = $this->Employee->get_info($this->session->userdata('person_id'));

        $business_name = urlencode($this->config->item('name'));
        $contact_name = urlencode($employee_info->first_name.' '.$employee_info->last_name);
        $address = urlencode($this->config->item('address'));
        $city = urlencode($this->config->item('city'));
        $state = urlencode($this->config->item('state'));
        $zip = urlencode($this->config->item('zip'));
        $email = urlencode($this->config->item('email'));
        $phone = urlencode($this->config->item('phone'));
        $reseller_name = 'ForeUP';
        $reference_id = urlencode($this->config->item('course_id'));

        return "https://econduitapp.com/setup/activateTerminal.aspx?BusinessName=$business_name&ContactName=$contact_name&Address=$address&City=$city&State=$state&ZipCode=$zip&Email=$email&Phone=$phone&ResellerName=$reseller_name&ReferenceID=$reference_id";
    }

    function get_payment_url($trans_type = 'Sale', $amount = 0, $terminal_info = array(), $ref_id = false) {
        if ($amount == 0)
        {
            return false;
        }
        else {
            $api_key = $terminal_info['blackline_api_key'];
            $api_password = $terminal_info['blackline_api_password'];
            $amount = $amount * 100;
            $terminal_id = $terminal_info['blackline_terminal_id'];
            $ref_id = $ref_id;

            return "https://econduitapp.com/terminal.aspx?Cmd=$trans_type&ApiKey=$api_key&ApiPassword=$api_password&Amount=$amount&TerminalID=$terminal_id&RefID=$ref_id";
        }
    }

    function get_all() {
        $this->db->from('blackline_devices');
        $this->db->where('course_id', $this->config->item('course_id'));
        $this->db->where('deleted', 0);

        return $this->db->get()->result_array();
    }

}