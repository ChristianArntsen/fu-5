<?php
class Quick_button_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->item_ids = array();
		$this->item_kit_ids = array();
		$this->tournament_ids = array();
	}

	public function get(){

		$this->load->model('v2/Item_model');
		$this->load->model('v2/Item_kit_model');
		$this->load->model('Pricing');
		$this->load->model('v2/Tournament_model');
		$course_id = $this->session->userdata('course_id');

		// Select all POS quick buttons from database
		$this->db->select("qb.quickbutton_id, qb.display_name, qb.tab, qb.position, qb.color,i.force_tax,
			GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				IFNULL(item.item_id, 0),
				IFNULL(item.item_kit_id, 0),
				item.`order`
			) SEPARATOR 0x1E) AS items", false);
		$this->db->from('quickbuttons AS qb');
        $this->db->join('quickbutton_items AS item', 'item.quickbutton_id = qb.quickbutton_id', 'left');
        $this->db->join('items AS i', 'item.item_id = i.item_id', 'left');
		$this->db->where('qb.course_id', $course_id);
		$this->db->group_by('qb.quickbutton_id');

		$rows = $this->db->get()->result_array();

		// Format/organize sub-items into arrays
		foreach($rows as $key => &$row){
			$row['items'] = $this->get_items_array($row['items']);
		}
		
		// Retrieve item details needed for each item
		$items = array();
		$item_kits = array();
		$tournaments = array();
		if(!empty($this->item_ids)){
			$items = $this->Item_model->get(array('item_id' => $this->item_ids));
		}
		if(!empty($this->item_kit_ids)){
			$item_kits = $this->Item_kit_model->get(array('item_id' => $this->item_kit_ids));
		}
		if(!empty($this->tournament_ids)){
			$tournaments = $this->Tournament_model->get(array('tournament_id' => $this->tournament_ids));
		}		
		
		// Key the item details by type and item_id
		$itemKitDetails = array();
		$itemDetails = array();
		$tournamentDetails = array();
		foreach($items as $item){
			$itemDetails[$item['item_id']] = $item;
		}
		foreach($item_kits as $item){
			$itemKitDetails[$item['item_id']] = $item;
		}
		foreach($tournaments as $tournament){
			$tournamentDetails[$tournament['tournament_id']] = $tournament;
		}		
		
		foreach($rows as &$row){
			
			foreach($row['items'] as &$item){
				if(!array_key_exists('item_type',$item)){
					continue;
				}

				$details = array();
				if($item['item_type'] == 'item'){
					if(!empty($itemDetails[$item['item_id']])){	
						$details = $itemDetails[$item['item_id']];
					}
					
				}else if($item['item_type'] == 'item_kit'){
					if(!empty($itemKitDetails[$item['item_id']])){
						$details = $itemKitDetails[$item['item_id']];
					}
				
				}else if($item['item_type'] == 'green_fee' || $item['item_type'] == 'cart_fee'){
					
					$fee = $this->Pricing->get_fees(array(
						'timeframe_id' => $item['timeframe_id'], 
						'special_id' => $item['special_id'],
						'holes' => $item['holes'],
						'type' => $item['item_type']
					));
					if(!empty($fee[0])){
						$details = $fee[0];
					}
					
				}else if($item['item_type'] == 'tournament'){
					if(!empty($tournamentDetails[$item['item_id']])){
						$details = $tournamentDetails[$item['item_id']];
					}
				}
				unset($item['item_type']);

				$item = array_merge($item, $details);
			}
		}

		return array_values($rows);
	}

	private function get_items_array($items){

		if(empty($items) || strlen($items) <= 1){
			return array();
		}
		$items = explode("\x1E", $items);

		// Loop through each item row, break apart fields
		foreach($items as &$item_row){

			$item = explode("\x1F", $item_row);
			$new_item = array();
			
			$item_id = $item[0];
			$item_kit_id = $item[1];
			$new_item['order'] = isset($item[2])?(int) $item[2]:"";

			// If item ID contains TID then it is a tournament
			if(!empty($item_id) && stripos($item_id, 'TID') !== false){
				$idParts = explode('_', $item_id);
				$new_item['item_id'] = (int) $idParts[1];
				$new_item['item_type'] = 'tournament';
				$this->tournament_ids[] = $new_item['item_id'];
			
			// If item ID has extra data (separated by underscores)
			// then it is a green fee or cart fee			
			}else if(!empty($item_id) && stripos($item_id, '_') !== false){
				
				$idParts = explode('_', $item_id);
				$idParts = array_pad($idParts,4,null);
				$new_item['item_id'] = (int) $idParts[0];
				$new_item['price_class_id'] = (int) $idParts[1];
				$new_item['timeframe_id'] = (int) $idParts[2];
				$new_item['item_number'] = (int) $idParts[3];
				$new_item['special_id'] = false;
				
				// If fee is a special, parse the special ID
				if(stripos($idParts[2], 'special') !== false){
					$special_id = explode(':', $idParts[2]);
					$new_item['special_id'] = (int) $special_id[1];
				}
				
				if($new_item['item_number'] == 1){
					$new_item['holes'] = 18;
					$new_item['item_type'] = 'green_fee';
				}else if($new_item['item_number'] == 2){
					$new_item['holes'] = 9;
					$new_item['item_type'] = 'green_fee';					
				}else if($new_item['item_number'] == 3){
					$new_item['holes'] = 18;
					$new_item['item_type'] = 'cart_fee';					
				}else if($new_item['item_number'] == 4){
					$new_item['holes'] = 9;
					$new_item['item_type'] = 'cart_fee';					
				}
				$this->item_ids[] = $new_item['item_id'];
			
			// If button is a regular item
			}else if(!empty($item_id)){
				$new_item['item_id'] = (int) $item_id;
				$new_item['item_type'] = 'item';
				$this->item_ids[] = $new_item['item_id'];
			
			// If button is an item kit
			}else if(!empty($item_kit_id)){
				$new_item['item_id'] = (int) $item_kit_id;
				$new_item['item_type'] = 'item_kit';
				$this->item_kit_ids[] = $new_item['item_id'];
			}

			$item_row = $new_item;
		}

		return $items;
	}
	
	function save_order($data){
		
		if(!is_array($data)){
			return false;
		}
		$course_id = (int) $this->session->userdata('course_id');
		
		foreach($data as $key => $pos){
			$this->db->update('quickbuttons', array(
				'position' => (int) $pos), 
				array('quickbutton_id' => (int) $key, 'course_id' => $course_id)
			);
		}

		return true;
	}
	
	function save($quick_button_id = null, $data){

		$response = false;
		$data = elements(array('display_name', 'items', 'tab', 'position', 'color'), $data, null);
		$items = $data['items'];
		
		if($items !== false && empty($items)){
			return false;
		}
		
		foreach($data as $key => $value){
			if($value === null){
				unset($data[$key]);
			}
		}
		unset($data['items']);
		
		// If creating a new quick button
		if(empty($quick_button_id)){
			$data['course_id'] = (int) $this->session->userdata('course_id');
			$this->db->insert('quickbuttons', $data);
			$quick_button_id = $this->db->insert_id();
			$response = $quick_button_id;

		// If updating an existing quick button
		}else{
			$this->db->update('quickbuttons', $data, array('quickbutton_id' => $quick_button_id));
			$response = $quick_button_id;
		}

		// Save quick button items
		if(!empty($items)){
			
			$this->db->delete('quickbutton_items', array('quickbutton_id' => $quick_button_id));

			$item_rows = array();
			foreach($items as $item){
				
				$item_row = array();
				$item_row['order'] = (int) $item['order'];
				$item_row['quickbutton_id'] = (int) $quick_button_id;
				
				// If green fee or cart fee
				if(in_array($item['item_type'], array('green_fee', 'cart_fee'))){
					if(!empty($item['special_id'])){
						$item_row['item_id'] = $item['item_id'].'_'.$item['price_class_id'].'_special:'.$item['special_id'].'_'.$item['item_number'];
					}else{	
						$item_row['item_id'] = $item['item_id'].'_'.$item['price_class_id'].'_'.$item['timeframe_id'].'_'.$item['item_number'];
					}
					$item_row['item_kit_id'] = null;
				
				// If just a regular item
				}else if(in_array($item['item_type'], array('service_fee', 'item', 'giftcard', 'pass'))){
					$item_row['item_id'] = (int) $item['item_id'];
					$item_row['item_kit_id'] = null;
				
				// If item kit
				}else if(in_array($item['item_type'], array('item_kit', 'punch_card'))){
					$item_row['item_kit_id'] = (int) $item['item_id'];
					$item_row['item_id'] = null;
				
				// If tournament
				}else if($item['item_type'] == 'tournament'){
					$item_row['item_kit_id'] = null;
					$item_row['item_id'] = 'TID_'.$item['tournament_id'];
				}
		
				if(empty($item_row['item_id']) && empty($item_row['item_kit_id'])){
					continue;
				}

				$item_rows[] = $item_row;
			}

			$this->db->insert_batch('quickbutton_items', $item_rows);
		}

		return $response;
	}
	
	function delete($quickbutton_id){
		
		if(empty($quickbutton_id)){
			return false;
		}
		$quickbutton_id = (int) $quickbutton_id;
		$course_id = (int) $this->session->userdata('course_id');
		
		$this->db->query("DELETE button.*, item.*
			FROM foreup_quickbuttons AS button
			LEFT JOIN foreup_quickbutton_items AS item
				ON item.quickbutton_id = button.quickbutton_id
			WHERE button.quickbutton_id = {$quickbutton_id}
				AND button.course_id = {$course_id}");
				
		return $this->db->affected_rows();			
	}
}
