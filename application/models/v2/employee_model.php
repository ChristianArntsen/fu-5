<?php
class Employee_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	// Search database and return matched employee IDs
	private function search_employees($search){
		
		$this->db->select('people.person_id');
		$this->db->from('people');
		$this->db->join('employees', 'employees.person_id = people.person_id', 'inner');
		$this->db->where("employees.deleted", 0);
		$this->db->where('employees.course_id', $this->session->userdata('course_id'));
        $this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' OR last_name LIKE '%".$this->db->escape_like_str($search)."%')");
		$this->db->order_by("last_name, first_name", "ASC");
		$this->db->limit(30);
		$rows = $this->db->get()->result_array();
		
		$ids = array();
		foreach($rows as $row){
			$ids[] = (int) $row['person_id'];
		}	
		
		return $ids;
	}
	
	public function get($params){
		
		$course_id = $this->session->userdata('course_id');

		if(!empty($params['q'])){
			$itemIds = $this->search_employees($params['q']);
			
			if(empty($itemIds)){
				return array();
			}
		}

		// Select list of employees with photos
		$this->db->select("e.person_id, p.first_name, p.last_name, p.email,
			p.phone_number, p.cell_phone_number, p.birthday, p.address_1, p.address_2,
			p.city, p.state, p.zip, p.country, p.comments, e.user_level, e.person_id as emp_id", false);
		$this->db->from('people AS p');
		$this->db->join('employees AS e', 'e.person_id = p.person_id', 'inner');
		$this->db->where('e.deleted', 0);
		$this->db->where('e.course_id', $course_id);
		$this->db->group_by('e.person_id');
		
		if(!empty($params['person_id'])){
			
			if(is_array($params['person_id'])){
				$ids = array();
				
				foreach($params['person_id'] as $person_id){
					$ids[] = (int) $person_id;
				}
				$this->db->where_in('e.person_id', $ids);
			
			}else{		
				$this->db->where('e.person_id', (int) $params['person_id']);
			}
		}
				
		if(!empty($params['q'])){
			$this->db->where_in("e.person_id", $itemIds);
		}

		$rows = $this->db->get()->result_array();

		if(count($rows) == 1){
			$this->load->model('acl');
			$rows[0]['permissions'] = $this->acl->get_user_permissions_array($rows[0]['person_id']);
		}

		return $rows;		
	}
	
	public function authenticate($person_id, $min_level, $password){
		
		$this->db->select('person_id');
		$this->db->from('employees');
		$this->db->where(array(
			'person_id' => (int) $person_id, 
			'password' => md5($password)
		));
		$this->db->where('user_level >=', (int) $min_level);
		$this->db->where('course_id', (int) $this->session->userdata('course_id')); 
		$row = $this->db->get()->row_array();
		
		if(!empty($row['person_id'])){
			return true;
		}
		
		return false;
	}
	
	public function authenticate_pin($pin){
		
		$this->db->select('person_id');
		$this->db->from('employees');
		$this->db->where('(pin = '.$this->db->escape($pin).' OR card = '.$this->db->escape($pin).')'); 
		$this->db->where('course_id', (int) $this->session->userdata('course_id')); 
		$row = $this->db->get()->row_array();
		
		if(!empty($row['person_id'])){
			return (int) $row['person_id'];
		}
		
		return false;
	}	
}
