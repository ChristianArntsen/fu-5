<?php
class Import_job_model extends CI_Model {

    function get($params = [], $all_courses = false){

        if(empty($params['course_id']) && !$all_courses){
            $params['course_id'] = $this->session->userdata('course_id');
        }

        $this->db->from('foreup_import_jobs');
        $this->db->where('deleted_at IS NULL');

        if(!empty($params['course_id'])){
            $this->db->where('course_id', (int) $params['course_id']);
        }
        if(!empty($params['status'])){
            $this->db->where('status', $params['status']);
        }
        if(!empty($params['id'])){
            $this->db->where('id', (int) $params['id']);
        }

        $this->db->order_by('created_at DESC');

        $import_jobs = $this->db->get()->result_array();

        foreach($import_jobs as &$import_job){
            $import_job['response'] = json_decode($import_job['response'], true);
            $import_job['settings'] = json_decode($import_job['settings'], true);
        }

        return $import_jobs;
    }

    function get_status($job_id){

        if(empty($job_id)){
            return false;
        }
        $row = $this->db->select('status')
            ->from('import_jobs')
            ->where('id', (int) $job_id)
            ->get()
            ->row_array();

        if(empty($row['status'])){
            return false;
        }
        return $row['status'];
    }

    function start_job($job_id){

        if(empty($job_id)){
            return false;
        }
        $job = $this->db->select('status')
            ->from('import_jobs')
            ->where('id', $job_id)
            ->get()
            ->row_array();

        if(empty($job)){
            return false;
        }
        if($job['status'] != 'ready'){
            return false;
        }

        $command = 'php index.php daemon import_data '.(int) $job_id.' > /dev/null 2>&1 &';
        shell_exec($command);

        return true;
    }

    function save($job_id = false, $data = []){

        $data = elements(['type', 'course_id', 'employee_id', 'status', 'total_records',
            'records_imported', 'records_failed', 'percent_complete', 'started_at', 'completed_at',
            'total_duration', 'response', 'source_path', 'settings', 'last_progress_update'], $data);

        if(!empty($data['response'])){
            $data['response'] = json_encode($data['response']);
        }

        if(!empty($data['settings'])){
            $data['settings'] = json_encode($data['settings']);
        }

        foreach($data as $key => $val){
            if($val === false){
                unset($data[$key]);
            }
        }

        if(empty($job_id)){
            if(empty($data['course_id'])){
                $data['course_id'] = $this->session->userdata('course_id');
            }
            $data['created_at'] = date('Y-m-d H:i:s');

            $this->db->insert('import_jobs', $data);
            $job_id = $this->db->insert_id();

        }else{
            $this->db->update('import_jobs', $data, ['id' => $job_id]);
        }

        return (int) $job_id;
    }

    function delete($job_id){

        $this->db->update('import_jobs',
            ['deleted_at' => date('Y-m-d H:i:s')],
            ['id' => (int) $job_id, 'course_id' => $this->session->userdata('course_id')]
        );
        return $this->db->affected_rows();
    }
}