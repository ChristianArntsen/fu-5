<?php
class Restaurant_reservation_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	function get($params = null){
		
		$params = elements(array('reservation_id', 'date'), $params, null);
		
		if(!empty($params['reservation_id'])){
			$this->db->where('reservation_id', (int) $params['reservation_id']);
		}
		
		if(!empty($params['date'])){
			$date = new DateTime($params['date']);
			
			if($date === false){
				return false;
			}
			$this->db->where('date', $date->format('Y-m-d'));
		}		
		
		$this->db->select("reservation.*, CONCAT(employee.first_name,' ',employee.last_name) AS employee_name", false);
		$this->db->from('restaurant_reservations AS reservation');
		$this->db->join('people AS employee', 'employee.person_id = reservation.employee_id', 'left');
		$this->db->where('reservation.course_id', (int) $this->session->userdata('course_id'));
		$this->db->where('reservation.deleted', 0);
		$this->db->order_by('reservation.date ASC, reservation.time ASC');
		$reservations = $this->db->get()->result_array();
		
		if(!empty($params['reservation_id']) && count($reservations) == 1){
			return $reservations[0];
		}
		
		return $reservations;
	}
	
	function save($reservation_id, $data){
		
		$data = elements(array('name', 'guest_count', 'date', 'time', 
			'customer_id', 'comments', 'checked_in'), $data, null);
		$data['course_id'] = (int) $this->session->userdata('course_id');
		$data['employee_id'] = (int) $this->session->userdata('person_id');
		$data['date_created'] = date('Y-m-d H:i:s');
		
		if(!empty($data['date'])){
			$date = new DateTime($data['date']);
			if($date === false){
				return false;
			}
			$data['date'] = $date->format('Y-m-d');
		}
		
		if(!empty($reservation_id)){
			unset($data['course_id'], $data['employee_id']);
			$this->db->where('reservation_id', (int) $reservation_id);
			$this->db->update('restaurant_reservations', $data);
		
		}else{
			$this->db->insert('restaurant_reservations', $data);
			$reservation_id = (int) $this->db->insert_id();
		}
		
		return (int) $reservation_id;
	}
	
	function delete($reservation_id){
		
		$reservation_id = (int) $reservation_id;
		$course_id = (int) $this->session->userdata('course_id');
		$employee_id = (int) $this->session->userdata('person_id');
		
		$this->db->update('restaurant_reservations', 
			array('deleted' => 1, 'deleted_by' => $employee_id),
			array('course_id' => $course_id, 'reservation_id' => $reservation_id)
		);
		
		return $this->db->affected_rows();
	}
}
