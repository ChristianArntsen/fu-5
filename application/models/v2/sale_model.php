<?php
class Sale_model extends CI_Model {
	
	var $credit_card_fees = 0;
	var $erange_codes = array();
	protected $last_error;

	function get_last_error(){
		return $this->last_error;
	}

	// Get list of sales
	function get($params = null){

		$course_id = $this->session->userdata('course_id');
		$this->load->library('v2/cart_lib');
		$s3 = new fu\aws\s3();

		if(!empty($params['sale_id'])){

			if(is_array($params['sale_id'])){
				$ids = array();

				foreach($params['sale_id'] as $sale_id){
					$ids[] = (int) $sale_id;
				}
				$this->db->where("(sale.sale_id IN (".implode(',',$ids)."))");

			}else{
				$this->db->where('(sale.sale_id = '.(int) $params['sale_id'].' OR sale.number = '.(int) $params['sale_id'].')');
			}
		}

		if(!isset($params['include_customer_details'])){
			$params['include_customer_details'] = false;
		}

		if(empty($params['limit']) || $params['limit'] > 500){
			$params['limit'] = 500;
		}

		if(empty($params['offset'])){
			$params['offset'] = 0;
		}else{
			$params['offset'] = (int) $params['offset'];
		}

		if(!empty($params['date'])){
            $this->db->where('(sale.sale_time > "'.date('Y-m-d 00:00:00', strtotime($params['date'])).'" AND sale.sale_time < "'.date('Y-m-d 24:00:00', strtotime($params['date'])).'")');
		}

        if(!empty($params['timeago']) && $params['timeago'] = '24h'){
            $this->db->where('(sale.sale_time >= "'.date('Y-m-d H:i:s', strtotime('-24 hours')).'" AND sale.sale_time <= "'.date('Y-m-d H:i:s').'")');
        }

		if(!empty($params['employee_id'])){
			$this->db->where('employee_id', (int) $params['employee_id']);
		}

		if(!empty($params['customer_id'])){
			$this->db->where('customer_id', (int) $params['customer_id']);
		}

		if(!empty($params['food_and_beverage'])){
			$this->db->where("table_id != '0'");
		}

		if(isset($params['terminal_id'])){
			$this->db->where("terminal_id", (int) $params['terminal_id']);
		}

		// Get sales from database
		$this->db->select("sale.sale_id, sale.course_id, sale.sale_time,
			sale.teetime_id, sale.employee_id, sale.comment, sale.refund_reason, 
			sale.refund_comment, sale.terminal_id,
			sale.table_id AS table_number, sale.customer_note,
			sale.customer_id, sale.table_id, sale.number,
			employee.first_name AS employee_first_name,
			employee.last_name AS employee_last_name", false);
		$this->db->from('sales AS sale');
		$this->db->join('foreup_people AS employee', 'employee.person_id = sale.employee_id', 'left');
		$this->db->where("sale.course_id = $course_id");
		$this->db->where("sale.deleted = 0");
		$this->db->order_by("sale.sale_time DESC");
		$this->db->group_by("sale.sale_id");
		$this->db->limit($params['limit'], $params['offset']);
		$result = $this->db->get();
        
        $rows = $result->result_array();

		if(empty($rows)){
			return array();
		}	

		$sale_ids = array();
		$customer_ids = array();
		$sales = array();
		$teetime_ids = array();

		// Loop through sales and save each sale ID, also set defaults
		foreach($rows as $row){
			$sale_ids[] = (int) $row['sale_id'];

			$sale = $row;
			$sale['payments'] = array();
			$sale['items'] = array();
			$sale['total'] = 0;
			$sale['subtotal'] = 0;
			$sale['tax'] = 0;
			$sale['taxes'] = array();
			$sale['auto_gratuity'] = 0;
			$sale['auto_gratuity_amount'] = 0;
			
			// If customer is attached to sale
			if($sale['customer_id'] > 0){
				$customer_ids[] = $sale['customer_id'];
			}

			if(!empty($sale['teetime_id'])){
				$teetime_ids[] = $sale['teetime_id'];
			}
			
			// If employee exists, create employee sub-array
			if($sale['employee_id'] > 0){
				$sale['employee'] = array(
					'person_id' => (int) $sale['employee_id'],
					'first_name' => $sale['employee_first_name'],
					'last_name' => $sale['employee_last_name']
				);
				unset($sale['employee_first_name'], $sale['employee_last_name']);
			}

			$sales[(int) $row['sale_id']] = $sale;
		}

		$teetimes = $this->get_teetime_info($teetime_ids);

		$payments = $this->get_payments($sale_ids, array('reports_only' => 0));

		// Add payments to sales
		if($payments){
			
			foreach($payments as $key => $payment){
				if($payment['is_auto_gratuity'] == 1){
					$sales[(int) $payment['sale_id']]['auto_gratuity_amount'] += $payment['amount'];
				}
				$payment['amount'] = (float) $payment['amount'];
				$payment['record_id'] = (int) $payment['record_id'];
				$payment['invoice_id'] = (int) $payment['invoice_id'];
				$payment['tip_recipient'] = (int) $payment['tip_recipient'];

				if(!empty($payment['signature'])){
					$payment['signature'] = $s3->client->getObjectUrl($s3->signatureBucket, $payment['signature'], '+1 hour');
				}

				$sales[(int) $payment['sale_id']]['payments'][] = $payment;
			}
		}

		$sale_id_list = implode(',', $sale_ids);
		$unit_price_includes_tax = (int) $this->config->item('unit_price_includes_tax');
		
		// Get all items & item kits associated with selected sales
		$query = $this->db->query("SELECT si.line, si.sale_id, i.item_id, si.is_side,
				NULL AS invoice_id, i.name, i.department, i.do_not_print_customer_receipt,
				i.category, i.category, i.subcategory, i.supplier_id, i.force_tax, si.serialnumber AS item_number,
				i.description, si.item_unit_price AS unit_price, i.unit_price AS base_price, i.max_discount,
				i.quantity AS inventory_level, i.is_unlimited AS inventory_unlimited, i.food_and_beverage,
				i.cost_price, si.quantity_purchased, si.type AS item_type, si.total, si.tax, si.subtotal,
				si.discount_percent, si.profit, si.total_cost, si.num_splits AS number_splits,
				GROUP_CONCAT(DISTINCT CONCAT(tax.name,'^:^',tax.percent,'^:^',tax.cumulative,'^:^',tax.amount)) AS taxes,
				i.unit_price_includes_tax, timeframe_id, special_id, price_class_id, si.erange_code,
				si.pass_id, si.parent_line
			FROM foreup_sales_items AS si
			LEFT JOIN foreup_items AS i
				ON si.item_id = i.item_id
			LEFT JOIN foreup_sales_items_taxes AS tax
				ON tax.line = si.line
				AND tax.sale_id = si.sale_id
			WHERE si.sale_id IN({$sale_id_list})
			GROUP BY si.sale_id, si.line
			UNION
			SELECT si.line, si.sale_id, i.item_kit_id AS item_id, 0 AS is_side,
				NULL AS invoice_id, i.name, i.department, 0 AS do_not_print_customer_receipt,
				i.category, i.category, i.subcategory, NULL AS supplier_id,null as force_tax, si.description AS item_number,
				i.description, si.item_kit_unit_price AS unit_price, i.unit_price AS base_price,
				100 AS max_discount, 0 inventory_level, 1 AS inventory_unlimited, 0 AS food_and_beverage,
				i.cost_price, si.quantity_purchased,
				IF(i.is_punch_card = 1, 'punch_card', 'item_kit') AS item_type,
				si.total, si.tax, si.subtotal,
				si.discount_percent, si.profit, si.total_cost, 1 AS number_splits,
				GROUP_CONCAT(DISTINCT CONCAT(tax.name,'^:^',tax.percent,'^:^',tax.cumulative,'^:^')) AS taxes,
				{$unit_price_includes_tax} AS unit_price_includes_tax, 0 AS timeframe_id, 0 AS special_id, 0 AS price_class_id,
				NULL AS erange_code, NULL AS pass_id, NULL AS parent_line
			FROM foreup_sales_item_kits AS si
			INNER JOIN foreup_item_kits AS i
				ON si.item_kit_id = i.item_kit_id
			LEFT JOIN foreup_sales_item_kits_taxes AS tax
				ON tax.line = si.line
				AND tax.sale_id = si.sale_id
			WHERE si.sale_id IN({$sale_id_list})
			GROUP BY si.sale_id, si.line
			UNION
			SELECT si.line, si.sale_id, 0 AS item_id, 0 AS is_side, i.invoice_id,
				CONCAT('Invoice #',i.invoice_number) AS name, i.department, 0 AS do_not_print_customer_receipt,
				i.category, i.category, i.subcategory, NULL AS supplier_id, null as force_tax, NULL AS item_number,
				NULL AS description, si.total AS unit_price, si.total AS base_price,
				0 AS max_discount, 0 inventory_level, 1 AS inventory_unlimited, 0 AS food_and_beverage,
				0 AS cost_price, 1 AS quantity_purchased,
				'invoice' AS item_type, si.total, 0 AS tax, si.total AS subtotal,
				0 AS discount_percent, si.total AS profit, 0 AS total_cost, 1 AS number_splits,
				'' AS taxes, {$unit_price_includes_tax} AS unit_price_includes_tax, 0 AS timeframe_id, 0 AS special_id, 
				0 AS price_class_id, NULL AS erange_code, NULL AS pass_id, NULL AS parent_line
			FROM foreup_sales_invoices AS si
			INNER JOIN foreup_invoices AS i
				ON si.invoice_id = i.invoice_id
			WHERE si.sale_id IN({$sale_id_list})
			GROUP BY si.sale_id, si.line
			ORDER BY sale_id, line");

		$items = $query->result_array();

		$sale_item_modifiers = $this->get_item_modifiers($sale_ids);
		$sale_item_comps = $this->get_item_comps($sale_ids);

		// Add items to sales
		if($items){

			foreach($items as $key => $item){

				// Break apart tax column into array
				$taxes = array();
				$taxesArray = explode(',', $item['taxes']);

				foreach($taxesArray as $tax){
					$taxArray = explode('^:^', $tax);
					if(empty($taxArray[0]) && empty($taxArray[1])){
						continue;
					}
					$taxes[] = array('name' => $taxArray[0], 'percent' => (float) $taxArray[1], 'cumulative' => $taxArray[2], 'amount' => (isset($taxArray[3])?$taxArray[3]:null));
				}
				uasort($taxes, array($this, 'sort_tax'));
				$item['taxes'] = array_values($taxes);

				$this->cart_lib->calculate_item_tax($item['subtotal'], $item['item_id'],'item');

				$item['non_discount_subtotal'] = (float) $this->cart_lib->calculate_subtotal($item['unit_price'], $item['quantity_purchased'], 0);
				$item['quantity'] = (float) $item['quantity_purchased'];
				$item['modifier_total'] = 0;
				$item['comp_total'] = 0;

				if(!empty($item['pass_id'])){
					$item['params']['pass_id'] = $item['pass_id'];
					unset($item['pass_id']);
				}

				if($item['item_type'] == 'member_account' || $item['item_type'] == 'customer_account' || $item['item_type'] == 'invoice_account'){
					$item['params']['customer']['person_id'] = $sales[(int) $item['sale_id']]['customer_id'];
				
				}else if($item['item_type'] == 'tournament'){
					$tournament_number = explode(' ',$item['item_number']);
					$item['tournament_id'] = $tournament_number[1];
					$item['params']['tournament']['tournament_id'] = $item['tournament_id'];
				
				}else if($item['item_type'] == 'member_account'){
					$item['name'] = 'Member Balance';
				
				}else if($item['item_type'] == 'customer_account'){
					$item['name'] = 'Customer Credit';
				
				}else if($item['item_type'] == 'invoice_account'){
					$item['name'] = 'Open Invoices';
				
				}else if($item['item_type'] == 'green_fee' || $item['item_type'] == 'cart_fee'){
					$item['teetime_id'] = $sales[(int) $item['sale_id']]['teetime_id'];
				}

				// Attach any modifiers to the item
				$item_modifiers =& $sale_item_modifiers[(int) $item['sale_id']][(int) $item['line']];
				if(!empty($item_modifiers)){
					$item['modifiers'] = $item_modifiers;
					foreach($item_modifiers as $modifier){
						$item['modifier_total'] += $modifier['selected_price'];
					}

				}else{
					$item['modifiers'] = [];
				}

				// Attach any comps to the item
				$item_comp =& $sale_item_comps[(int) $item['sale_id']][(int) $item['line']];
				if(!empty($item_comp)){
					$item['comp'] = $item_comp;
					$item['comp_total'] += $item_comp['amount'];
				}else{
					$item['comp'] = false;
				}

				$item['non_discount_subtotal'] += (float) $item['modifier_total'];
				$item['discount_amount'] = (float) $item['non_discount_subtotal'] - (float) $item['subtotal'];

				// Add item to sale
				if(
					($item['item_type'] == 'item' || $item['item_type'] == 'green_fee' || $item['item_type'] == 'cart_fee') 
					&& !empty($item['parent_line'])
				){
					$parent_item =& $sales[(int) $item['sale_id']]['items'][(int) $item['parent_line']];
					
					if(!isset($parent_item['items'])){
						$parent_item['items'] = [];
					}
					
					if(!isset($parent_item['params'])){
						$parent_item['params'] = [];
						$parent_item['params']['sub_item_overrides'] = [];
					}
					$parent_item['params']['sub_item_overrides'][] = (float) $item['unit_price'];

					$item['base_quantity'] = round($item['quantity'] / $parent_item['quantity'], 2);
					$parent_item['items'][] = $item;
					
					$parent_item['non_discount_subtotal'] += (float) $item['non_discount_subtotal'];
					$parent_item['discount_amount'] += (float) $item['discount_amount'];
					$parent_item['subtotal'] += (float) $item['subtotal'];
					$parent_item['tax'] += (float) $item['tax'];
					$parent_item['total'] += (float) $item['total'];
					$parent_item['unit_price'] += (float) $item['non_discount_subtotal'];

				}else{
					$sales[(int) $item['sale_id']]['items'][(int) $item['line']] = $item;	
				}

				// Add item totals to sale totals
				$sales[(int) $item['sale_id']]['total'] += (float) $item['total'];
				$sales[(int) $item['sale_id']]['subtotal'] += (float) $item['subtotal'];
				$sales[(int) $item['sale_id']]['tax'] += (float) $item['tax'];
			}
		}

		// If there are customers we need details on
		$sale_customers = array();
		if(!empty($customer_ids)){
			$this->load->model('v2/Customer_model');
			
			$customer_params = [
				'person_id' => $customer_ids
			];

			if(!empty($params['include_customer_details'])){
				$customer_params['include_recent_transactions'] = true;
				$customer_params['include_gift_cards'] = true;
			}
			$customers = $this->Customer_model->get($customer_params);
			
			// Organize customer records by person_id
			foreach($customers as $customer){
				$sale_customers[$customer['person_id']] = $customer;
			}
			unset($customers);
		}
		
		// Group taxes for each sale
		foreach($sales as &$sale){
			$sale['taxes'] = $this->group_taxes($sale['items']);
			if($sale['auto_gratuity_amount'] > 0){
				$sale['auto_gratuity'] = round($sale['auto_gratuity_amount'] / $sale['subtotal'], 2) * 100;
			}

			$sale['taxable'] = 1;
			if(empty($sale['taxes'])){
				$sale['taxable'] = 0;
			}
			
			if(!empty($sale['customer_id']) && !empty($sale_customers[$sale['customer_id']])){
				$sale['customer'] = $sale_customers[$sale['customer_id']];
			}

			if(!empty($sale['items'])){
				$sale['items'] = array_values($sale['items']);
			}

			$sale['teetime'] = false;
			if(!empty($sale['teetime_id']) && !empty($teetimes[$sale['teetime_id']])){
				$sale['teetime'] = $teetimes[$sale['teetime_id']];
			}
		}

		return array_values($sales);
	}

	public function get_teetime_info($teetime_ids = []){

		if(empty($teetime_ids)){
			return [];
		}

		if(!is_array($teetime_ids)){
			$teetime_ids = [$teetime_ids];
		}

		$rows = $this->db->select('time.TTID AS teetime_id, time.start_datetime AS start_date, teesheet.title AS teesheet_title')
			->from('teetime AS time')
			->join('teesheet AS teesheet', 'teesheet.teesheet_id = time.teesheet_id')
			->where_in('time.TTID', $teetime_ids)
			->get()->result_array();

		if(empty($rows)){
			return [];
		}

		$teetimes = [];
		foreach($rows as $teetime){
			$teetimes[$teetime['teetime_id']] = $teetime;
		}

		return $teetimes;
	}
	
	private function get_next_number(){
        $course_id = $this->session->userdata('course_id');

        $this->db->select('current_index');
        $this->db->from('course_increments');
        $this->db->where('course_id', $course_id);
        $this->db->where('type', 'sales');
        $this->db->limit(1);
        $row = $this->db->get()->row_array();

        if (isset($row['current_index'])) {
            // Return next_index, and save new index
            $this->db->where('course_id', $course_id);
            $this->db->where('type', 'sales');
            $this->db->update('course_increments', array('current_index' => $row['current_index'] + 1));

            return (int) $row['current_index'] + 1;
        }
        else {
            // Return next index, then save it to course_increments
            $this->db->select('number');
            $this->db->from('sales');
            $this->db->where('course_id', (int) $course_id);
            $this->db->where('number IS NOT NULL');
            $this->db->order_by('number DESC');
            $this->db->limit(1);
            $row = $this->db->get()->row_array();

            $this->db->insert('course_increments', array('course_id' => $this->session->userdata('course_id'), 'type' => 'sales', 'current_index' => $row['number'] + 1));

            return (int) $row['number'] + 1;
        }
	}

	function get_item_modifiers($sale_ids = array()){
		
		if(empty($sale_ids)){
			return false;
		}

		$rows = $this->db->select('modifier.modifier_id, modifier.name, sale_modifier.sale_id, 
			sale_modifier.line, sale_modifier.selected_option, modifier.multi_select, 
			sale_modifier.selected_price')
			->from('sales_items_modifiers AS sale_modifier')
			->join('modifiers AS modifier', 'modifier.modifier_id = sale_modifier.modifier_id', 'left')
			->where_in('sale_modifier.sale_id', $sale_ids)
			->group_by('sale_modifier.sale_id, sale_modifier.line, sale_modifier.modifier_id')
			->get()->result_array();

		$sale_modifiers = array();
		if(empty($rows)){
			return $sale_modifiers;
		}

		foreach($rows as $modifier){
			$sale_line_modifiers =& $sale_modifiers[(int) $modifier['sale_id']][(int) $modifier['line']];
			unset($modifier['line'], $modifier['sale_id']);
			$modifier['selected_price'] = (float) $modifier['selected_price'];
			$modifier['modifier_id'] = (int) $modifier['modifier_id'];
			
			if($modifier['multi_select'] == 1){
			    $temp = json_decode($modifier['selected_option'], true);
                if(is_array($temp))
				    $modifier['selected_option'] = json_decode($modifier['selected_option'], true);
			}

			$sale_line_modifiers[] = $modifier;
		}
		unset($rows);

		return $sale_modifiers;
	}

	function get_item_comps($sale_ids = array()){
		
		if(empty($sale_ids)){
			return false;
		}

		$rows = $this->db->select('comp.sale_id, comp.line, 
			comp.description, comp.dollar_amount AS amount')
			->from('sales_comps AS comp')
			->where_in('comp.sale_id', $sale_ids)
			->get()->result_array();

		$comps = array();
		if(empty($rows)){
			return $comps;
		}

		foreach($rows as $comp){
			$sale_line_comp =& $comps[(int) $comp['sale_id']][(int) $comp['line']];
			unset($comp['line'], $comp['sale_id']);
			$comp['amount'] = (float) $comp['amount'];
			$sale_line_comp = $comp;
		}
		unset($rows);

		return $comps;
	}	
	
	public function generate_payment_type($description){

		$type = '';

		$customer_account = 'Customer Credit';
		$member_account = 'Member Balance';

		if($this->config->item('customer_credit_nickname')){
			$customer_account = $this->config->item('customer_credit_nickname');
		}
		if($this->config->item('member_balance_nickname')){		
			$member_account = $this->config->item('member_balance_nickname');
		}

		if(stripos($description, 'member') !== false && stripos($description, 'tip') !== false){
			$type = 'member_account_tip';
		}else if(stripos($description, 'customer') !== false && stripos($description, 'tip') !== false){
			$type = 'customer_account_tip';
		}else if(stripos($description, $member_account) !== false){
			$type = 'member_account';
		}else if(stripos($description, $customer_account) !== false){
			$type = 'customer_account';
		}else if(stripos($description, 'check tip') !== false){
			$type = 'check_tip';
		}else if(stripos($description, 'cash tip') !== false){
			$type = 'cash_tip';	
		}else if(stripos($description, 'gift card tip') !== false || 
			(stripos($description, 'gift card') !== false && stripos($description, 'tip') !== false)){
			$type = 'gift_card_tip';
		}else if(stripos($description, 'credit card tip') !== false){
			$type = 'credit_card_tip';					
		}else if(stripos($description, 'tip') !== false &&
			(stripos($description, 'VISA') !== false 
			|| stripos($description, 'M/C') !== false 
			|| stripos($description, 'AMEX') !== false
			|| stripos($description, 'DCVR') !== false
			|| stripos($description, 'DINERS') !== false
			|| stripos($description, 'JCB') !== false)){
			$type = 'credit_card_tip';		
		}else if(stripos($description, 'Credit Card') !== false
			|| stripos($description, 'VISA') !== false 
			|| stripos($description, 'M/C') !== false 
			|| stripos($description, 'AMEX') !== false
			|| stripos($description, 'DCVR') !== false
			|| stripos($description, 'DINERS') !== false
			|| stripos($description, 'JCB') !== false){
			$type = 'credit_card';
		}else if(stripos($description, 'punch_card') !== false){
			$type = 'punch_card';
		}else if(stripos($description, 'gift card') !== false || stripos($description, 'giftcard') !== false){
			$type = 'gift_card';	
		}else if(stripos($description, 'check') !== false){
			$type = 'check';
		}else if(stripos($description, 'cash') !== false){
			$type = 'cash';
		}else if(stripos($description, 'change issued') !== false){
			$type = 'change';
		}else if(stripos($description, 'loyalty') !== false){
			$type = 'loyalty_points';
		}

		return $type;
	}

	private function generate_payment_id($payment){

		if(stripos($payment['type'], 'member_account') !== false || stripos($payment['type'], 'cutomer_account') !== false){
			$payment_id = $payment['type'].':'.(int) $payment['record_id'];

		}else if(($payment['type'] == 'credit_card' || $payment['type'] == 'credit_card_tip') && $payment['invoice_id'] != 0){
			$payment_id = $payment['type'].':'.(int) $payment['invoice_id'];

		}else if(!empty($payment['record_id'])){
			$payment_id = $payment['type'].':'.$payment['record_id'];
		
		}else if(!empty($payment['number'])){
			$payment_id = $payment['type'].':'.$payment['number'];	
				
		}else{
			$payment_id = $payment['type'];
		}

		if(!empty($payment['is_auto_gratuity']) && $payment['is_auto_gratuity'] == 1){
			$payment_id .= ':auto';
		}

		$payment['payment_id'] = $payment_id;
		return $payment;
	}

	// Retrieves all payments attached to a sale or multiple sales
	function get_payments($sale_ids = false, $filters = array()){
		
		$this->load->model('v2/Giftcard_model');
		$this->load->model('v2/Punch_card_model');
		$payments = array();
		if(empty($sale_ids)){
			return $payments;
		}

		if(!is_array($sale_ids)){
			$sale_ids = array($sale_ids);
		}				

		// Get all payments associated with selected sales
		$this->db->select("payment.sale_id, payment.payment_amount AS amount, payment.number, payment.is_auto_gratuity,
			payment.payment_type AS description, payment.type, payment.invoice_id, payment.record_id, payment.tip_recipient,
			CONCAT(employee.first_name,' ',employee.last_name) AS tip_recipient_name, payment.signature,
			CONCAT(customer.first_name,' ',customer.last_name) AS customer_name, sale.customer_id AS sale_customer_id,
			cc_info.amount AS cc_amount, cc_info.masked_account AS cc_masked_account, cc_info.card_type AS cc_card_type, 
			cc_info.amount_refunded AS cc_amount_refunded, cc_info.cardholder_name AS cc_cardholder_name,
			payment.record_id", false);
		$this->db->from('sales_payments AS payment');
		$this->db->join('sales AS sale', 'sale.sale_id = payment.sale_id', 'inner');
		$this->db->join('people AS employee', 'employee.person_id = payment.tip_recipient', 'left');
		$this->db->join('people AS customer', 'customer.person_id = payment.record_id', 'left');
		$this->db->join('sales_payments_credit_cards AS cc_info', 'cc_info.invoice = payment.invoice_id', 'left');
		$this->db->where_in('sale.sale_id', $sale_ids);
		$this->db->group_by('sale.sale_id, payment.payment_type');
		
		if(!empty($filters['record_id'])){
			$this->db->where('payment.record_id', (int) $filters['record_id']);
		}

		if(!empty($filters['type'])){
			$this->db->where('payment.type', $filters['type']);
		}

		if(!empty($filters['invoice_id'])){
			$this->db->where('payment.invoice_id', (int) $filters['invoice_id']);
		}
		
		if(isset($filters['reports_only'])){
			$this->db->where('payment.reports_only', (int) $filters['reports_only']);
		}
		
		if(isset($filters['description'])){
			$this->db->where('payment.payment_type', trim($filters['description']));
		}

		if(isset($filters['is_auto_gratuity'])){
			$this->db->where('payment.is_auto_gratuity', (int) $filters['is_auto_gratuity']);
		}			
		
		$payments = $this->db->get()->result_array();
		$payments_with_numbers = array('check', 'check_tip', 'gift_card', 'gift_card_tip', 'punch_card');
		
		// Loop through payments and create unique payment ID for each
		foreach($payments as &$payment){
			
			if(empty($payment['record_id']) && !empty($payment['invoice_id'])){
				$payment['record_id'] = (int) $payment['invoice_id'];
			}
			
			if(empty($payment['type'])){
				$payment['type'] = $this->generate_payment_type($payment['description']);
			}
			
			if(($payment['type'] == 'member_account' || $payment['type'] == 'customer_account' ||
				$payment['type'] == 'member_account_tip' || $payment['type'] == 'customer_account_tip') &&
				empty($payment['record_id'])){
				$payment['record_id'] = (int) $payment['sale_customer_id'];
			}
			
			// If the payment number is stored in the payment description (old sales)
			// Extract it and place it in the number field
			if(in_array($payment['type'], $payments_with_numbers) && empty($payment['number']) && stripos($payment['description'], ':') !== false){
				$description = str_replace('Tip', '', $payment['description']);
				$split_description = explode(':', $description);
				$payment['number'] = trim($split_description[1]);
			}

			if(!empty($payment['record_id']) && $payment['type'] == 'credit_card'){
				$payment['params'] = array(
					'amount' => (float) $payment['cc_amount'],
					'card_type' => $payment['cc_card_type'],
					'masked_account' => $payment['cc_masked_account'],
					'cardholder_name' => $payment['cc_cardholder_name'],
					'amount_refunded' => (float) $payment['cc_amount_refunded']
				);
			}

			unset($payment['cc_amount'], $payment['cc_card_type'], 
				$payment['cc_masked_account'], $payment['cc_cardholder_name'],
				$payment['cc_amount_refunded']);			
			
			$payment = $this->generate_payment_id($payment);
		}

		return $payments;
	}

	// Groups taxes from each item into a combined tax list (for entire sale)
	private function group_taxes($items = array()){

		$taxes = array();
		if(empty($items)){
			return $taxes;
		}

		// Loop through items in a sale and combine their taxes into
		// one list
		foreach($items as $item){

			if(($item['item_type'] == 'item_kit' || $item['item_type'] == 'punch_card') && !empty($item['items'])){
				$taxes = $this->group_taxes($item['items']);

			}else if(!empty($item['taxes'])){
				foreach($item['taxes'] as $tax){
					
					if(!isset($tax['amount'])){
						$tax['amount'] = 0;
					}
					$tax_key = $tax['name'].'-'.round($tax['percent'], 3);

					if(!empty($taxes[$tax_key])){
						$taxes[$tax_key]['amount'] += $tax['amount'];
					}else{
						$taxes[$tax_key] = $tax;
					}
				}			
			}
		}

		return array_values($taxes);
	}

	// Sort taxes so cumulative taxes are calculated last
	private function sort_tax($a, $b){
		if((int) $a['cumulative'] > (int) $b['cumulative']){
			return 1;
		}else{
			return 0;
		}
	}

	// Saves a sale to the database (including associated items, payments, etc)
	function save($sale_id = false, $data = array()){
		
		$this->number = null;
        $taxable = $data['taxable']; // save taxable before it is eaten up...
        $data = elements(array('items', 'payments', 'sale_time', 'customer_id',
            'teetime_id', 'employee_id', 'override_authorization_id', 'comment',
            'table_id', 'terminal_id', 'refund_reason', 'refund_comment', 
            'return_sale_id', 'customer_note'), $data, false);
		$response = false;
		$this->special_payments = array();
		$this->payments = array();
		$this->erange_codes = array();
		
		$this->db->trans_start();
		
		// If creating a new sale
		if(empty($sale_id)){
			$sale_items = $data['items'];
			$sale_payments = $data['payments'];

            if (isset($sale_payments[0]) && $sale_payments[0]['amount'] < 0) {
                $data['refunded_sale_id'] = $data['return_sale_id'];
            }
            unset($data['items'], $data['payments'], $data['return_sale_id']);

			if((int) $data['customer_id'] == 0){
				$data['customer_id'] = null;
			}
			if(empty($data['employee_id'])){
				$data['employee_id'] = (int) $this->session->userdata('person_id');
			}
			if(empty($data['terminal_id'])){
				$data['terminal_id'] = (int) $this->session->userdata('terminal_id');
			}
			if(empty($data['customer_note'])){
				$data['customer_note'] = null;
			}
			
			$this->payments = $sale_payments;
			
			$data['sale_time'] = date('Y-m-d H:i:s');
			$data['course_id'] = $this->session->userdata('course_id');
			$data['payment_type'] = $this->get_payment_list($sale_payments);
			$data['number'] = $this->get_next_number();

            $this->sale_number = $data['number'];
			$this->customer_id = $data['customer_id'];

			// Save the sale
			$this->db->insert('sales', $data);
			$sale_id = (int) $this->db->insert_id();

			$teetime_id = false;
			if(!empty($data['teetime_id'])){
				$teetime_id = $data['teetime_id'];
			}
			$this->save_items($sale_id, $sale_items, $teetime_id,$taxable);
			
			// If there are special AR or AP payments, combine those with
			// the sale payments and save them to the sale
			if(!empty($this->special_payments) && count($this->special_payments > 0)){
				$sale_payments = array_merge($sale_payments, $this->special_payments);
				$this->save_payments($sale_id, $sale_payments);
				$this->refresh_sale_payment_list($sale_id);
			
			}else{
				$this->save_payments($sale_id, $sale_payments);
			}
			
			// If sale is paying for a tee time, mark tee time as paid
			if(!empty($data['teetime_id'])){
				$this->mark_teetime_paid(
					$data['teetime_id'],
					$data['customer_id'],
					$this->paid_players,
					$this->paid_carts,
                    $sale_items,
                    $this->session->userdata('event_person_id')
				);
                $this->session->unset_userdata('event_person_id', null);
			}			
			$response = $sale_id;
			$this->number = $data['number'];

		// If updating an existing sale
		}else{
			if(empty($data['customer_id'])){
				$data['customer_id'] = null;
			}
			if(empty($data['customer_note'])){
				$data['customer_note'] = null;
			}

			// Filter out unset columns
			foreach($data as $key => $value){
				if($value === false){
					unset($data[$key]);
				}
			}

			if(empty($data)){
				return false;
			}

			if ($this->db->update('sales', $data, 'sale_id = '.(int) $sale_id))
            {
                $this->Action->save('sale', $sale_id, 'update', json_encode($data));
                // UPDATE TRANSACTIONS ASSOCIATED WITH THE SALE
                $this->Inventory->update_time($sale_id, $data['sale_time']);
                $this->Giftcard->update_time($sale_id, $data['sale_time']);
                $this->Account_transactions->update_time($sale_id, $data['sale_time']);
            }
			$response = true;
		}
		if ($this->db->trans_status() === FALSE){
			$course_id = $this->session->userdata('course_id');
			$employee_id = $this->session->userdata('person_id');

			error_log('SALE TRANSACTION FAILED - [course_id:'.$course_id.'] [employee_id:'.$employee_id.']'.
				' [MYSQL ERROR '.$this->db->_error_number().'- '.$this->db->_error_message().' - '.']');
		}
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			$this->msg = 'Error completing sale, please try again';
			return false;
		}

		// Now that the sale is committed to the database, update any statement payments we made (if any)
        // with the proper sale IDs
		if(!empty($this->statementPaymentIds)){
		    $this->db->where_in('id', $this->statementPaymentIds);
		    $this->db->update('account_statement_payments', ['sale_id' => (int) $sale_id]);
            $this->statementPaymentIds = [];
        }

		return $response;
	}

	function email_receipt($sale_id, $email){

		if(empty($sale_id) || empty($email)){
			return false;
		}
		$sale = $this->get(array('sale_id' => $sale_id));

		$receipt_html = $this->load->view("v2/sales/email_receipt", $sale[0], true);
		$email_subject = $this->session->userdata('course_name').' - Sale '.$sale[0]['number'].' Receipt';

		return send_sendgrid($email, $email_subject, $receipt_html, $this->config->item('email'), $this->config->item('name'));
	}

	// Save an array of items to a sale
	function save_items($sale_id, $items = array(), $teetime_id = false, $taxable = 1){

		$this->load->model('Invoice');
		$this->load->model('Account_transactions');
		$this->load->model('Price_class');
		$this->load->model('Pass');

		$sale_id = (int) $sale_id;
		if(empty($items)){
			return false;
		}

		$item_rows = array();
		$invoice_rows = array();
		$item_kit_rows = array();
		$item_tax_rows = array();
		$item_kit_tax_rows = array();

        $this->paid_carts = 0;
		$this->paid_players = 0;

        // Track statement payments so we can go back and update each statement payment
        // with the sale ID. We can't do this while saving the items because its all wrapped
        // in a database transaction. When communicating with doctrine it looks for a sale that doesn't
        // exist yet to attach it to the statement payment.
        $this->statementPaymentIds = [];
		
		if($this->config->item('unit_price_includes_tax') == 1){
			$include_tax = 1;
		}else{
			$include_tax = 0;
		}
		$payment_amount = 0;
		// Check if there are any credit card fees
		foreach($this->payments as $payment){
            $payment_amount += $payment['amount'];
		    if(!empty($payment['params']['fee'])){
				$this->credit_card_fees += (float) $payment['params']['fee'];
			}			
		}
		
		// Add a new item to the sale to account for credit card fees
		if($this->credit_card_fees > 0){
			$fee_item = $this->Item_model->get_special_item('credit_card_fee');
			$fee_item['item_unit_price'] = $fee_item['subtotal'] = $fee_item['total'] = $this->credit_card_fees;
			$fee_item['tax'] = 0;
			$fee_item['quantity_purchased'] = 1;
			$fee_item['line'] = count($items) + 1;
			$items[] = $fee_item;
		}
        $ItemTax = new \fu\items\ItemTax();
		foreach($items as $item){

			// Organize taxes into rows for database insertion
			if(!empty($item['taxes'])){
				foreach($item['taxes'] as $item_tax){
					
					$tax = elements(array('sale_id', 'item_id', 'line', 'name', 'percent', 'cumulative','amount'), $item_tax);
					$tax['sale_id'] = $sale_id;
					$tax['line'] = (int) $item['line'];

					if($item['item_type'] == 'item_kit' || $item['item_type'] == 'punch_card'){
						$tax['item_kit_id'] = (int) $item['item_id'];
						unset($tax['item_id']);
						$item_kit_tax_rows[] = $tax;
					}else{
						$tax['item_id'] = (int) $item['item_id'];
						$item_tax_rows[] = $tax;
					}
				}
			}

			if(!empty($item['modifiers'])){
				$this->save_item_modifiers($sale_id, $item['item_id'], $item['line'], $item['modifiers']);
			}

			if(isset($item['item_type'])){
				$item['type'] = $item['item_type'];
			}

			if($item['type'] == 'giftcard'){		
				if($this->config->item('use_ets_giftcards') == 0 && $item['quantity'] >0){
					$this->issue_giftcard($item, $sale_id);
				}
				if($item['quantity'] < 0){
					$this->load->model('v2/Giftcard_model');
					$this->Giftcard_model->delete(array('giftcard_number' => $item['item_number']));
				}
				$this->add_special_payment('GiftCardAP', 'payable', -($item['unit_price'] * $item['quantity']) - ($item['tax'] * $item['quantity']));

				// If the gift card was discounted, track the discounted amount as
				// a new payment type so the course can track it
				$giftcard_discount = (($item['unit_price'] + $item['tax']) * $item['quantity']) - $item['total'];
                $item['discount_percent'] = 0;
				$item['subtotal'] += $giftcard_discount;
				$item['total'] += $giftcard_discount;

				if($giftcard_discount != 0){
					$this->add_special_payment('Gift Card Discount', 'gift_card_discount', $giftcard_discount, 0);
				}

				$item['receipt_only'] = 1;
			
			}else if($item['type'] == 'tournament'){
				$this->process_tournament($item, $sale_id);
                $this->add_special_payment('TournamentAP', 'payable', -$item['total']);
                $item['receipt_only'] = 1;

            }else if($item['type'] == 'punch_card'){
				$this->issue_punch_card($item, $sale_id);
				$this->add_special_payment('PunchCardAP', 'payable', -$item['total']);
				$item['receipt_only'] = 1;

			}else if($item['type'] == 'invoice'){
				$this->pay_invoice($item, $sale_id);
				$this->add_special_payment('InvoiceBalanceAR', 'receivable', -$item['total']);	
				$item['receipt_only'] = 1;

            }else if($item['type'] == 'statement'){
                $this->pay_statement($item, $sale_id);
                $this->add_special_payment('InvoiceBalanceAR', 'receivable', -$item['total']);
                $item['receipt_only'] = 1;

			}else if($item['type'] == 'member_account'){
				$this->pay_member_account($item, $sale_id);
				$item['serialnumber'] = 'MemberBalance';
				$this->add_special_payment($item['serialnumber'].'AR', 'receivable', -$item['total']);
				$item['receipt_only'] = 1;
				$item['item_id'] = $this->Item_model->check_item_number('member_balance');

			}else if($item['type'] == 'customer_account'){
				$this->pay_customer_account($item, $sale_id);
				$item['serialnumber'] = 'CustomerBalance';
				$this->add_special_payment($item['serialnumber'].'AR', 'receivable', -$item['total']);
				$item['receipt_only'] = 1;
				$item['item_id'] = $this->Item_model->check_item_number('account_balance');

			}else if($item['type'] == 'invoice_account'){
                // Hacked fix to pay off invoices using old pay_invoice method
                $most_recent_invoice = $this->Invoice->get_all($item['params']['customer']['person_id'], $limit=1)->row_array();
                $item['invoice_id'] = $most_recent_invoice['invoice_id'];
                $this->pay_invoice($item, $sale_id, true);
                //$this->pay_invoice_account($item, $sale_id);
				$item['serialnumber'] = 'InvoiceBalance';
				$this->add_special_payment($item['serialnumber'].'AR', 'receivable', -$item['total']);
				$item['receipt_only'] = 1;
			
			}else if($item['type'] == 'green_fee' || $item['type'] == 'cart_fee'){
				
				// If pass was being used on fee, add it to pass history
				if($item['type'] == 'green_fee' && !empty($item['params']['pass_id'])){
					
					$item['pass_id'] = $item['params']['pass_id'];

					// If transaction was a sale, add the history
					if($item['quantity'] >= 0){

						$rule_number = isset($item['params']['pass_rule_number'])?$item['params']['pass_rule_number']:null;
						$rule_uniq_id = isset($item['params']['pass_rule_id'])?$item['params']['pass_rule_id']:null;
						$this->Pass->add_history(
							$item['params']['pass_id'],
							'sale', [
								'sale_id' => $sale_id,
								'count' => (int) $item['quantity'],
								'teetime_id' => $teetime_id,
								'rule_number' => $rule_number,
								'rule_id' => $rule_uniq_id,
								'price_class_id' => $item['price_class_id']
							]
						);
					
					// If transaction was a return, remove the pass history line
					}else{
						$this->Pass->delete_history(
							$item['params']['pass_id'], 
							'sale', 
							['sale_id' => $sale_id]
						);						
					}
				}

				// Get price class name (for reporting)
				if(!empty($item['price_class_id'])){
					$price_class = $this->db->select('name')
						->from('price_classes')
						->where('class_id', (int) $item['price_class_id'])
						->get()->row_array();
					
					if(!empty($price_class['name'])){
						$item['price_category'] = $price_class['name'];
					}
				}
				
				// Get tee sheet name (for reporting)
				if(!empty($item['special_id'])){
					$teesheet = $this->db->select('title AS name')
						->from('teesheet')
						->join('specials', 'specials.teesheet_id = teesheet.teesheet_id', 'inner')
						->where('special_id', (int) $item['special_id'])
						->get()->row_array();
					
					if(!empty($teesheet['name'])){
						$item['teesheet'] = $teesheet['name'];
					}
				
				}else if(!empty($item['timeframe_id'])){
					$teesheet = $this->db->select('title AS name')
						->from('teesheet')
						->join('seasons', 'seasons.teesheet_id = teesheet.teesheet_id', 'inner')
						->join('seasonal_timeframes', 'seasonal_timeframes.season_id = seasons.season_id', 'inner')
						->where('seasonal_timeframes.timeframe_id', (int) $item['timeframe_id'])
						->get()->row_array();
					
					if(!empty($teesheet['name'])){
						$item['teesheet'] = $teesheet['name'];
					}
				}				
				
				if($item['type'] == 'green_fee'){
					$this->paid_players += $item['quantity'];
				}else{
					$this->paid_carts += $item['quantity'];
				}
			
			}else if($item['type'] == 'pass'){
				$item['params']['pass_item_id'] = (int) $item['item_id'];
				$this->Pass->save(null, $item['params']);
			} else {

				$item['pass_id'] = $item['params']['pass_id'];
				$rule_number = isset($item['params']['rule_number'])?$item['params']['rule_number']:null;

				// If transaction was a sale, add the history
				if($item['quantity'] >= 0){

					$this->Pass->add_history(
						$item['params']['pass_id'],
						'sale', [
							'sale_id' => $sale_id,
							'count' => (int) $item['quantity'],
							'rule_number'=> $rule_number
						]
					);

					// If transaction was a return, remove the pass history line
				}else{
					$this->Pass->delete_history(
						$item['params']['pass_id'],
						'sale',
						['sale_id' => $sale_id]
					);
				}
			}

			$item_data = elements(array('sale_id', 'item_id', 'teesheet',
				'price_category', 'serialnumber', 'line', 'quantity_purchased',
				'type', 'item_cost_price', 'item_unit_price', 'unit_price_includes_tax',
				'discount_percent', 'subtotal', 'tax', 'total', 'total_cost', 'profit',
				'timeframe_id', 'special_id', 'price_class_id', 'receipt_only',
				'teesheet', 'price_category', 'pass_id', 'parent_line'), $item, '');

			$item_data['sale_id'] = $sale_id;
			$item_data['timeframe_id'] = (int) $item_data['timeframe_id'];
			$item_data['special_id'] = (int) $item_data['special_id'];
			$item_data['price_class_id'] = (int) $item_data['price_class_id'];
			$item_data['receipt_only'] = (int) $item_data['receipt_only'];
			
			if($item_data['unit_price_includes_tax'] === ''){
				$item_data['unit_price_includes_tax'] = $include_tax;
			}

			if($item_data['parent_line'] === ''){
				$item_data['parent_line'] = null;
			}

            // If the item is an erange item, get the erange number
            if (!empty($item['erange_size']) && $item['erange_size'] > 0 && $this->config->item('erange_id') && $item['quantity'] > 0){
                $item_data['erange_code'] = $this->get_erange_code($item['erange_size']);
                $this->erange_codes[(int) $item['line']] = $item_data['erange_code'];
            }else{
            	$item_data['erange_code'] = 0;
            }

			// If the item is an item kit
			if($item['item_type'] == 'item_kit' || $item['item_type'] == 'punch_card'){
				$item_data['item_kit_id'] = (int) $item['item_id'];
				$item_data['item_kit_unit_price'] = (float) $item['item_unit_price'];
				$item_data['item_kit_cost_price'] = (float) $item['item_cost_price'];
				$item_data['description'] = $item['description'];

				unset($item_data['item_id'], $item_data['item_unit_price'], $item_data['item_cost_price'],
					$item_data['price_class_id'], $item_data['special_id'], $item_data['timeframe_id'],
					$item_data['serialnumber'], $item_data['type'], $item_data['unit_price_includes_tax'],
					$item_data['erange_code'], $item_data['parent_line'], $item_data['pass_id']);

				if($item['item_type'] == 'item_kit'){
					$item_data['new'] = 1;
				} else {
					$item_data['new'] = 0;
				}

				$item_kit_rows[] = $item_data;

			// If the item is an invoice being paid off
			}else if($item['item_type'] == 'invoice'){
				$invoice = elements(array('invoice_id', 'teesheet',
					'price_category', 'line', 'subtotal', 'tax', 'total',
					'receipt_only'), $item, '');

				$invoice['sale_id'] = $sale_id;
				$invoice['invoice_unit_price'] = $invoice['total'];
				$invoice['subtotal'] = $invoice['total'];
				$invoice['tax'] = 0;
				$invoice['discount_percent'] = 0;
				$invoice['total_cost'] = 0;
				$invoice['invoice_cost_price'] = 0;
				$invoice['profit'] = $invoice['total'];
				$invoice['quantity_purchased'] = 1;

				$invoice_rows[] = $invoice;

			}else{
				$item_rows[] = $item_data;
			}

		}

		// Insert items
		$items_inserted = 0;
		if(!empty($item_rows)){
			$this->db->insert_batch('sales_items', $item_rows);
			$items_inserted = $this->db->affected_rows();
		}
		if(!empty($item_kit_rows)){
			$this->db->insert_batch('sales_item_kits', $item_kit_rows);
			$items_inserted = $this->db->affected_rows();
		}

		if(!empty($invoice_rows)){
			$this->db->insert_batch('sales_invoices', $invoice_rows);
			$invoices_inserted = $this->db->affected_rows();
		}

		if(empty($items_inserted)){
			return false;
		}

		// Insert item taxes
		if(!empty($item_tax_rows)){
			$this->db->insert_batch('sales_items_taxes', $item_tax_rows);
		}

		return $items_inserted;
	}

    function get_erange_code($erange_size = false){

        $erange_id = $this->config->item('erange_id');
        $erange_password = $this->config->item('erange_password');
        $time = date('H:i:s');
        $erange_code = file_get_contents("http://www.152iq.com/erange_pos_2014/index.php?cust_id={$erange_id}&cust_pass={$erange_password}&time={$time}&size={$erange_size}");

        return $erange_code;
    }

    function save_item_modifiers($sale_id, $item_id, $line, $modifiers = []){

    	if(empty($modifiers) || !is_array($modifiers)){
    		return false;
    	}

    	$modifier_data = [];
    	foreach($modifiers as $modifier){
		    $selected_option = $modifier['selected_option'];
		    if(!empty($selected_option) && is_array($selected_option)){
    			$selected_option = json_encode($selected_option);
    		} else if(is_array($selected_option)){
    			$selected_option = null;
		    }

    		$modifier_data[] = [
    			'sale_id' => $sale_id,
    			'line' => $line,
    			'item_id' => $item_id,
    			'modifier_id' => (int) $modifier['modifier_id'],
    			'selected_price' => (float) $modifier['selected_price'],
    			'selected_option' => $selected_option
    		];
		}
		$this->db->insert_batch('sales_items_modifiers', $modifier_data);
		return $this->db->affected_rows();
	}

	function process_tournament(&$item, $sale_id){
		$item['serialnumber'] = 'TOURNAMENT '.$item['tournament_id'];
		$item['item_id'] = $this->Tournament_model->get_tournament_item($item['serialnumber'], $item['name']);
		
		$this->Tournament_model->pot_transaction($item['tournament_id'], $item['total']);
	}
	
	function add_special_payment($name, $type, $amount, $reports_only = 1){

		if (isset($this->special_payments[$name])) {
			$this->special_payments[$name]['payment_amount'] += $amount;	
			$this->special_payments[$name]['amount'] += $amount;
		
		}else{
			$this->special_payments[$name] = array(
				'type' => $type,
				'description' => $name,
				'payment_type' => $name,
				'payment_amount' => $amount,
				'reports_only' => (int) $reports_only,
				'amount' => $amount
			);
		}		
	}
	
	function mark_teetime_paid($teetime_id, $customer_id, $players, $carts, $items = false, $event_person_id = false){
		
		$teetime_id = substr($teetime_id, 0, 20);
		$players = (int) $players;
		$carts = (int) $carts;
        $position_sql = '';
        
        if(!empty($items)){
			foreach ($items as $item) {
				if(!empty($item['params']['player_position']) && $item['params']['player_position'] > 0){
					$position_sql .= ", person_paid_{$item['params']['player_position']} = 1";
				}
			}        	
        }
        
		$this->db->query("UPDATE foreup_teetime 
			SET player_count = CASE WHEN (paid_player_count + {$players}) > player_count THEN (paid_player_count + {$players}) ELSE player_count END,
				carts = CASE WHEN (paid_carts + {$carts}) > carts THEN (paid_carts + {$carts}) ELSE carts END,
				paid_player_count = (paid_player_count + {$players}), 
				paid_carts = (paid_carts + {$carts}), 
				status = CASE WHEN (status != 'teed off') THEN 'checked in' ELSE 'teed off' END
				$position_sql
			WHERE (TTID = '{$teetime_id}' OR TTID = '{$teetime_id}b') 
				AND status != 'deleted'
			LIMIT 2");
		
		$event_paid = true;
		if($players < 0){
			$event_paid = false;
		}
		
		// If the tee time is an event, mark the correct player as "paid"
		$this->load->model('event');
        foreach($items as $item) {
            if (!empty($item['params']['customer_id']) || !empty($event_person_id)) {
                $this->event->person_pay($teetime_id, $event_person_id, $event_paid, $item['params']['customer_id']);
            }
        }

		return true;
	}
	
	function pay_invoice_account($item, $sale_id){
		
		if(empty($item['params']['customer']['person_id'])){
			$person_id = $this->customer_id;
		}else{
			$person_id = $item['params']['customer']['person_id'];
		}


		$this->Account_transactions->save(
			'invoice',
			$person_id,
			'Invoice Payment',
			$item['total'],
			 'POS '.$this->sale_number,
			 $sale_id,
			 0,
			 $this->session->userdata('person_id'),
			 $this->session->userdata('course_id')
		);
	}

	function pay_member_account($item, $sale_id){
		
		if(empty($item['params']['customer']['person_id'])){
			$person_id = $this->customer_id;
		}else{
			$person_id = $item['params']['customer']['person_id'];
		}		
		
		$this->Account_transactions->save(
			'member',
			$person_id,
			'Balance Payment',
			$item['total'],
			 'POS '.$this->sale_number,
			 $sale_id,
			 0,
			 $this->session->userdata('person_id'),
			 $this->session->userdata('course_id')
		);
	}

	function pay_customer_account($item, $sale_id){
		
		if(empty($item['params']['customer']['person_id'])){
			$person_id = $this->customer_id;
		}else{
			$person_id = $item['params']['customer']['person_id'];
		}		
		
		$this->Account_transactions->save(
			'customer',
			$person_id,
			'Balance Payment',
			$item['total'],
			 'POS '.$this->sale_number,
			 $sale_id,
			 0,
			 $this->session->userdata('person_id'),
			 $this->session->userdata('course_id')
		);
	}

	function pay_invoice($item, $sale_id, $pay_oldest_first = false){

		if(empty($item['invoice_id']) && !$pay_oldest_first){
			return false;
		}
		$employeeId = (int) $this->session->userdata('person_id');
		$success = $this->Invoice->pay_invoice($sale_id, (int) $item['invoice_id'], $employeeId, (float) $item['total'], $pay_oldest_first);

		if($success){

		    $this->pay_invoice_account($item, $sale_id);

            // Check if this invoice is part of a statement,
            // If so, we need to attach the same payment to the statement
            $statement = $this->db->select('charge.statement_id')
                ->from('invoices AS i')
                ->join('account_statement_charges AS charge', 'charge.sale_id = i.sale_id', 'inner')
                ->where('i.invoice_id', $item['invoice_id'])
                ->get()->row_array();

            if(!empty($statement['statement_id'])){
                $this->apply_statement_payment($item['total'], $statement['statement_id'], $sale_id, false);
            }
		}

		return $success;
	}

    function pay_statement($item, $saleId){

        if(empty($item['params']) || empty($item['params']['id'])){
            return false;
        }
        $statementId = $item['params']['id'];
        $amount = $item['total'];

        return $this->apply_statement_payment($amount, $statementId, $saleId);
    }

    function apply_statement_payment($amount, $statementId, $saleId, $applyToInvoices = true){

        $silex = new \fu\statementWorkflow\classes\silex_handler();
        $statements = $silex->db->getRepository('e:ForeupAccountStatements');
        $customers = $silex->db->getRepository('e:ForeupCustomers');

        $statement = $statements->find($statementId);
        if(!$statement) {
            return false;
        }

        $courseId = $statement->getOrganizationId();
        $personId = $this->customer_id;
        $customer = $customers->findOneBy(['personId' => $personId, 'courseId' => $courseId]);

        $statementPayments = new \fu\statementWorkflow\classes\AutomaticPayment(
            $silex,
            $customer,
            $statement
        );

        if($applyToInvoices){
            $statementPaymentId = $statementPayments->applyPayment($saleId, $amount, 'pos', null, '', '', null, false);
        }else{
            $statementPaymentId = $statementPayments->saveStatementPayment($saleId, $amount, 'pos', null, '', '', null, false);
        }

        if(!empty($statementPaymentId)){
            $this->statementPaymentIds[] = (int) $statementPaymentId;
        }

        return $statementPaymentId;
    }

	function issue_giftcard($item, $sale_id){

		$this->load->model('Giftcard');

		$item['params'] = elements(['customer', 'giftcard_number', 'expiration_date', 'department', 'category', 'details', 'action'], $item['params'], '');

		$giftcard_data['value'] = $item['unit_price'];
		$giftcard_data['customer_id'] = null;
		if(!empty($item['params']['customer']['person_id'])){
			$giftcard_data['customer_id'] = (int) $item['params']['customer']['person_id'];
		}
		$giftcard_data['giftcard_number'] = $item['params']['giftcard_number'];
		$giftcard_data['details'] = $item['params']['details'];
		$giftcard_data['date_issued'] = date('Y-m-d');
		$giftcard_data['expiration_date'] = $item['params']['expiration_date'];
		$giftcard_data['department'] = $item['params']['department'];
		$giftcard_data['category'] = $item['params']['category'];
		$giftcard_data['course_id'] = (int) $this->session->userdata('course_id');

		if($item['params']['action'] == 'new'){
			$response = $this->Giftcard->save($giftcard_data);

		}else{
			$cur_value = (float) $this->Giftcard->get_giftcard_value($giftcard_data['giftcard_number']);
			$new_value = (float) $giftcard_data['value'] + $cur_value;
			$response = $this->Giftcard->update_giftcard_value($giftcard_data['giftcard_number'], $new_value, $sale_id);
		}

		return $response;
	}

	function issue_punch_card($item, $sale_id){

		$this->load->model('v2/Punch_card_model');

		$punch_card = $item['params'];
		$punch_card['items'] = $item['items'];

		$response = $this->Punch_card_model->save(false, $punch_card);

		return $response;
	}

	// Builds a summary payment list in HTML to store in sale database row
	function get_payment_list($payments){
		$html = '';
		if(empty($payments)){
			return $html;
		}
		$payment_type_counts = array();

		foreach($payments as $payment){
			// Append a counter to the payment type to avoid duplicate key errors
			// when inserting into database. Only a problem when user makes 2+ payments
			// with the same credit card.
			if(!empty($payment_type_counts[ $payment['description'] ])){
				$count = ++$payment_type_counts[ $payment['description'] ];
				$payment['description'] .= ' ('.$count.')';

			}else{
				$payment_type_counts[ $payment['description'] ] = 1;
			}
			$html .= $payment['description'].': '.to_currency($payment['amount']).'<br />';
		}

		return $html;
	}

	function refresh_sale_payment_list($sale_id){
		$payments = $this->get_payments($sale_id);
		$payment_list = $this->get_payment_list($payments);
		$this->db->update('sales', array('payment_type' => $payment_list), array('sale_id' => (int) $sale_id));

		return $this->db->affected_rows();
	}

	// Saves an array of payments to a sale
	function save_payments($sale_id, $payments = array()){

		$sale_id = (int) $sale_id;
		if(empty($payments)){
			return false;
		}
		$this->load->model('v2/Raincheck_model');
		$this->load->model('v2/Punch_card_model');
		$this->load->model('Giftcard');
		$this->credit_card_fees = 0;

		$payment_type_counts = array();

		// Loop through payments and filter by valid database fields
		$rows = array();
		foreach($payments as $payment){

			$payment_data = elements(array('record_id', 'type', 'description',
				'amount', 'invoice_id', 'tip_recipient', 'number', 'reports_only'), $payment);
			$payment_data['sale_id'] = $sale_id;
			$payment_data['payment_type'] = $payment['description'];
			$payment_data['payment_amount'] = $payment['amount'];
			$payment_data['reports_only'] = (int) $payment_data['reports_only'];
			
			if(!empty($payment['params']['signature_id'])){
				$payment_data['signature'] = $payment['params']['signature_id'];
			}

			unset($payment_data['amount'], $payment_data['description']);

			// Append a counter to the payment type to avoid duplicate key errors
			// when inserting into database. Only a problem when user makes 2+ payments
			// with the same credit card.
			if(!empty($payment_type_counts[ $payment_data['payment_type'] ])){
				$count = ++$payment_type_counts[ $payment_data['payment_type'] ];
				$payment_data['payment_type'] .= ' ('.$count.')';
			
			}else{
				$payment_type_counts[ $payment_data['payment_type'] ] = 1;
			}

			$rows[] = $payment_data;

			// Mark raincheck(s) as redeemed
			if($payment['type'] == 'raincheck' && !empty($payment['record_id'])){
				$this->Raincheck_model->redeem($payment['record_id']);
			}

			// Deduct payment from gift card
			if($payment['type'] == 'gift_card' && !empty($payment['record_id'])){
				$this->deduct_gift_card($payment['number'], $payment['amount'], $sale_id);
			}

			// Deduct punches from punch card
			if($payment['type'] == 'punch_card' && !empty($payment['record_id'])){
				$this->deduct_punch_card($payment, $sale_id);
			}
		}

		foreach($rows as $row){
			$this->db->insert('sales_payments',$row);
		}
		return $this->db->affected_rows();
	}

	function deduct_gift_card($gift_card_number, $amount, $sale_id){

		$cur_giftcard_value = (float) $this->Giftcard->get_giftcard_value($gift_card_number);
        $this->Giftcard->update_giftcard_value($gift_card_number, $cur_giftcard_value - $amount, $sale_id, $this->sale_number);

		return true;
	}

	function deduct_punch_card($payment, $sale_id){

		$this->Punch_card_model->use_punches($payment['record_id'], $payment['params']['punches_used']);
		return true;
	}

	// Saves a single payment to a sale (eg. when adding tips or refunds)
	function save_payment($sale_id, $payment){

		if(empty($sale_id) || empty($payment)){
			$this->last_error = 'v2/sale_model->save_payment Error: sale_id and payment parameters required.';
			return false;
		}
		$this->load->model('v2/cart_model');

		$sale_id = (int) $sale_id;
		$payment = elements(array('record_id', 'type', 'number', 'is_auto_gratuity',
			'amount', 'invoice_id', 'tip_recipient', 'merchant_data'), $payment);

		$payment['sale_id'] = $sale_id;
		$payment['amount'] = (float) $payment['amount'];
		$payment['tip_recipient'] = (int) $payment['tip_recipient'];
		$payment['record_id'] = (int) $payment['record_id'];
		$payment['is_auto_gratuity'] = (int) $payment['is_auto_gratuity'];
		$payment['invoice_id'] = 0;
		$payment['description'] = $this->cart_model->generate_payment_description($payment);

		if(empty($payment['type'])){
			$payment['type'] = $this->generate_payment_type($payment['description']);			
		}
		
		if($payment['type'] == 'credit_card_tip' || $payment['type'] == 'credit_card'){
			$payment['invoice_id'] = $payment['record_id'];
		}
		
		$employee_id = (int) $this->session->userdata('person_id');
		$course_id = (int) $this->session->userdata('course_id');
		
		if(!empty($payment['invoice_id'])){
			$params = array(
				'invoice_id' => $payment['invoice_id'],
				'description' => $payment['description'],
				'is_auto_gratuity' => $payment['is_auto_gratuity']
			);
			
		}else if(!empty($payment['record_id'])){
			$params = array(
				'record_id' => $payment['record_id'],
				'description' => $payment['description'],
				'is_auto_gratuity' => $payment['is_auto_gratuity']
			);			
		
		}else{
			$params = array(
				'description' => $payment['description'],
				'is_auto_gratuity' => $payment['is_auto_gratuity']
			);				
		}
		
		// Check if a payment already exists of that type
		$existing_payment = $this->get_payments($sale_id, $params);
		
		// If a payment already exists of that same type, calculate
		// the difference
		$existing_amount = 0;
		if(!empty($existing_payment[0])){
			$existing_amount = $existing_payment[0]['amount'];
		}
		$difference = (float) $payment['amount'] - (float) $existing_amount;
		
		$payment_processed = true;
		if($difference != 0){

			// If payment is from a member/customer account
			if(in_array($payment['type'], array(
				'member_account', 'member_account_tip', 'member_account_refund',
				'customer_account', 'customer_account_tip', 'customer_account_refund'))){

				if(stripos($payment['type'], 'member') !== false){
					$account_type = 'member';
				}else{
					$account_type = 'customer';
				}

				$sale = $this->get(array('sale_id'=>$sale_id));
				$person_id = $sale[0]['customer']['person_id'];

				// Verify account balance
				$approved = $this->cart_model->verify_account_balance($account_type, $payment['record_id'], $difference);
				if($approved === false){
					$this->last_error = 'v2/sale_model->save_payment Error: Unable to apply to account: '.$this->cart_model->msg;
					return false;
				}
				$payment['amount'] = $approved + $existing_amount;
				$transaction_amount = $payment['amount'];

				if(stripos($payment['type'], 'tip') !== false){
					$transaction_comment = 'POS '.$sale_id.' Tip';
					$transaction_desc = 'Tip';
					$transaction_amount = -$difference;

				}else if(stripos($payment['type'], 'refund') !== false){
					$transaction_comment = 'POS '.$sale_id.' Refund';
					$transaction_desc = 'Refund';
					$transaction_amount = -$transaction_amount;

				}else{
					$transaction_comment = 'POS '.$sale_id.' Payment';
					$transaction_desc = 'Payment';
				}

				// Deduct payment from account
				$this->Account_transactions->save(
					$account_type,
					(int) $payment['record_id'],
					$transaction_comment,
					$transaction_amount,
					$transaction_desc,
					$sale_id,
					0,
					$employee_id,
					$course_id
				);

			// If payment is credit card tip
			}else if($payment['type'] == 'credit_card_tip' && $payment['record_id'] > 0){
				$payment['invoice_id'] = $payment['record_id'];
				$approved = $this->save_credit_card_tip($payment['invoice_id'], $payment['amount'], $difference);
				
				if(!$approved){
					$this->last_error = 'v2/sale_model->save_payment Error: Credit card tip was not approved.';
					return false;
				}

			// If payment is credit card refund
			}else if($payment['type'] == 'credit_card_partial_refund' && ($payment['record_id'] > 0 || !empty($payment['merchant_data']))){
				$payment['invoice_id'] = $payment['record_id'];
				$credit_card_payment = $this->save_credit_card_partial_refund($payment);
				
				if(!$credit_card_payment){
					$this->last_error = 'v2/sale_model->save_payment Error: partial refund failed.';
					return false;
				}
				$payment['invoice_id'] = !empty($credit_card_payment['invoice_id']) ? $credit_card_payment['invoice_id'] : $payment['invoice_id'];
				$payment['record_id'] = !empty($credit_card_payment['record_id']) ? $credit_card_payment['record_id'] : $payment['invoice_id'];
				$payment['description'] = $credit_card_payment['description'];
			
			}else if($payment['type'] == 'gift_card_tip'){
				//Check if there is enough for the difference
				$this->load->model('Giftcard');

				if(empty($payment['number'])){
					$this->response(array('success'=>false, 'msg'=>'Gift card number is required'), 400);
					return false;
				}
				$giftcard_id = $this->Giftcard->get_giftcard_id($payment['number']);
				// If gift card doesn't exist, or is expired, return error
				if(empty($giftcard_id) || $this->Giftcard->is_expired($giftcard_id)){
					$this->response(array('success'=> false, 'msg'=>lang('sales_giftcard_does_not_exist')), 400);
					return false;
				}

				$giftcard_info = $this->Giftcard->get_info($giftcard_id);
				$cur_giftcard_value =  $giftcard_info->value;
				if ($cur_giftcard_value < $difference || ($cur_giftcard_value <= 0 && $payment['amount'] > 0)){
					$this->last_error = lang('sales_giftcard_balance_is').' '.to_currency($giftcard_info->value);
					return false;
				}
				$this->deduct_gift_card($payment['number'], $difference, $sale_id);
			}
		}

		$payment = $this->generate_payment_id($payment);

		// If the payment not a tip, the new amount should add to the existing amount
		if(stripos($payment['type'], 'tip') === false){
			$payment['amount'] += $existing_amount;
			if(!isset($payment['is_tip'])){
				$payment['is_tip'] = 0;
			}
		} else {
			$payment['is_tip'] = 1;
		}
		
		unset($payment['merchant_data']);
		$this->db->query("INSERT INTO foreup_sales_payments
			(
			sale_id,
			type,
			payment_type,
			payment_amount,
			invoice_id,
			tip_recipient,
			record_id,
			is_tip
			)
			VALUES
			(
			{$sale_id},
			{$this->db->escape($payment['type'])},
			{$this->db->escape($payment['description'])},
			{$payment['amount']},
			{$payment['invoice_id']},
			{$payment['tip_recipient']},
			{$payment['record_id']},
			{$payment['is_tip']}
			)
			ON DUPLICATE KEY UPDATE
				payment_amount = {$payment['amount']},
				tip_recipient = {$payment['tip_recipient']},
				is_tip = {$payment['is_tip']}
				");

		$this->refresh_sale_payment_list($sale_id);


		if($payment['is_tip'] && !empty($payment['tip_recipient'])) {
			if(!isset($course_id))$course_id = $this->session->userdata('course_id');
			$transaction = new \fu\tips\TipTransaction($course_id);
			$emp_id = $this->session->userdata('person_id');
			$payment_copy = $payment;
			$payment_copy['payment_type']=$payment['description'];
			$payment_copy['payment_amount']=$payment['amount'];
			$transaction->process_single_employee_tip($payment_copy, $emp_id);
		}else{
			// figure how to handle this. Net tips payable acct?
		}
		
		unset($payment['payment_type'], $payment['payment_amount']);
		return $payment;
	}

	// Saves a new credit card "credit"
	function save_credit_card_partial_refund($payment){
		
		$merchant_data = $payment['merchant_data'];
		
		if(!empty($merchant_data) && $this->config->item('ets_key')){
			$payment = $this->cart_model->process_ets_payment($payment, $merchant_data);

		}else if(!empty($merchant_data) && $this->config->item('mercury_id')){
			$payment = $this->cart_model->process_mercury_payment($payment, $merchant_data);
		
		}else if(!empty($merchant_data) && $this->config->item('element_account_id')){
			$payment = $this->cart_model->process_element_payment($payment, $merchant_data);
		}
		
		$payment['description'] = 'CC Partial Refund';
		return $payment;
	}

	// Refunds part or all of a pre-existing credit card payment
	function refund_credit_card_payment($cc_invoice_id, $refund_amount = false){

		if(empty($cc_invoice_id) || empty($refund_amount)){
			return false;
		}

		// Get credit card payment details
		$cc_payment = $this->db->select('cardholder_name, token, payment_id,
				amount, amount_refunded, voided, frequency')
			->from('sales_payments_credit_cards')
			->where('invoice', (int) $cc_invoice_id)
			->get()->row_array();

		// If payment has already been refunded
		if($cc_payment['amount_refunded'] > 0 || $cc_payment['voided'] == 1){
			$this->msg = 'Credit card payment has already been refunded';
			return false;
		}

		if($refund_amount > $cc_payment['amount']){
			$refund_amount = $cc_payment['amount'];
		}

		$success = false;
        $apriva_username = $this->config->item('apriva_username');
        $ets_key = $this->config->item('ets_key');
        $mercury_id = $this->config->item('mercury_id');
		$mercury_password = $this->config->item('mercury_password');
		$element_account_id = $this->config->item('element_account_id');

        // Apriva
        if(!empty($apriva_username)) {

            $data = array();
            $processor = \fu\credit_cards\ProcessorFactory::create("apriva", $data);
            $terminal_id = $this->session->userdata('terminal_id');
            $response = $processor->returnPayment($cc_invoice_id, $terminal_id, number_format($refund_amount,2));
            $success = $response['success'];

            // ETS
        }else if(!empty($ets_key)){

			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($ets_key);

			$ets_response = $payment->set('action', 'refund')
				->set('transactionID', $cc_payment['payment_id'])
				->set('amount', $refund_amount)
				->send();

			if($ets_response->status == 'success'){
				$success = true;
			}

		// Mercury
		}else if(!empty($mercury_id)){

			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			$HC->set_merchant_credentials($mercury_id, $mercury_password);
			$HC->set_frequency($cc_payment['frequency']);
			$HC->set_invoice($cc_invoice_id);
			$HC->set_frequency($cc_payment['frequency']);
			$HC->set_cardholder_name($cc_payment['cardholder_name']);
			$HC->set_token($cc_payment['token']);
			$response = $HC->issue_refund($cc_invoice_id, $refund_amount);

			if (!empty($response) && (int) $response->ResponseCode === 0 && $response->Status == 'Approved'){
				$success = true;
			}
		
		// Element
		}else if(!empty($element_account_id)){

			$this->load->library('v2/Element_merchant');
			$this->element_merchant->init(
				new GuzzleHttp\Client()
			);
			
			// Apply refund
			$this->element_merchant->credit_card_return(array(
				'transaction_id' => $cc_payment['payment_id'],
				'reference_number' => $cc_invoice_id,
				'ticket_number' => $cc_invoice_id,
				'amount' => $refund_amount
			));

			if($this->element_merchant->success()){
				$success = true;
			}else{
				$this->msg = (string) $this->element_merchant->response()->xml()->Response->ExpressResponseMessage;
			}
		}

		if($success){
			$this->db->update('sales_payments_credit_cards', 
				array('amount_refunded' => $refund_amount, 'token_used' => 1), 
				array('invoice' => $cc_invoice_id)
			);
		}

		return $success;
	}

	function save_credit_card_tip($cc_invoice_id, $amount, $difference){

		if(empty($cc_invoice_id) || $amount === null){
			return false;
		}
		
		$this->load->model('Sale');
		$payment_info = $this->get_credit_card_payment($cc_invoice_id);
		
		if(empty($payment_info)){
			return false;
		}
		
		$approved = false;

		// If course is using ETS payments
		if($this->config->item('ets_key')){

			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));

			$session = $payment->set('action', 'addTip')
						->set('transactionID', $payment_info['payment_id'])
						->set('amount', $amount)
						->send();

			if($session->status == 'success'){
				$approved = true;
				
				$card_amount = round($payment_info['auth_amount'] + $difference, 2);
				$payment_data = array (
					'auth_amount' => (string) $card_amount,
					'gratuity_amount' => (string) $amount,
				);
			}

		// If course is using Mercury payments
		}else if($this->config->item('mercury_id')){

			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
			$HC->set_invoice($payment_info['invoice']);
			$HC->set_token($payment_info['token']);
			$HC->set_ref_no($payment_info['ref_no']);
			$HC->set_frequency($payment_info['frequency']);
			$HC->set_memo($payment_info['memo']);
			$HC->set_auth_code($payment_info['auth_code']);
			$HC->set_cardholder_name($payment_info['cardholder_name']);
			$HC->set_customer_code($payment_info['customer_code']);
			
			$card_amount = round($payment_info['auth_amount'] + $difference, 2);
			$response = $HC->token_transaction('Adjust', $card_amount, 0.00, 0.00);
			
			if ((string) $response->Status == 'Approved'){
				$approved = true;
			}

			$payment_data = array (
				'amount' => $card_amount,
				'auth_amount'=> $card_amount,
				'gratuity_amount' => (string) $amount,
				'process_data' => (string) $response->ProcessData,
				'token' => (string) $response->Token
			);
		
		// If course uses element payments
		}else if($this->config->item('element_account_id')){
			
			$this->load->library('v2/Element_merchant');
			
			$this->element_merchant->init(
				new GuzzleHttp\Client()
			);
			
			// If the payment doesn't have a token yet, create one
			if(empty($payment_info['token'])){
				$this->element_merchant->payment_account_create_with_trans_id(array(
					'transaction_id' => $payment_info['payment_id'],
					'payment_account_reference_number' => $cc_invoice_id
				));
			
				if(!$this->element_merchant->success()){
					return false;
				}
				$payment_account_token = (string) $this->element_merchant->response()->xml()->Response->PaymentAccount->PaymentAccountID;
			
			}else{
				$payment_account_token = $payment_info['token'];
			}
			
			// Charge credit card with original amount + new tip amount
			$card_amount = round($payment_info['auth_amount'] + $difference, 2);
			$gratuity_amount = $payment_info['gratuity_amount'] + $difference;
			
			$this->element_merchant->credit_card_sale(array(
				'amount' => $card_amount,
				'payment_account_id' => $payment_account_token,
				'reference_number' => time(),
				'ticket_number' => time()
			));
			
			if(!$this->element_merchant->success()){	
				return false;
			}
			$new_transaction_id = (string) $this->element_merchant->response()->xml()->Response->Transaction->TransactionID;			
			
			// Finally, void the original credit card payment
			$this->element_merchant->credit_card_void(array(
				'transaction_id' => $payment_info['payment_id'],
				'reference_number' => time(),
				'ticket_number' => time()
			));
			
			// If the void of the original transaction failed
			// void the NEW transaction so we don't double charge the customer
			if(!$this->element_merchant->success()){
				
				$this->element_merchant->credit_card_void(array(
					'transaction_id' => $new_transaction_id,
					'reference_number' => time(),
					'ticket_number' => time()
				));
				return false;
			}		
			
			if($this->element_merchant->success()){
				$approved = true;
				$payment_data['amount'] = $card_amount;
				$payment_data['auth_amount'] = $card_amount;
				$payment_data['gratuity_amount'] = $gratuity_amount;
				$payment_data['token'] = $payment_account_token;
				$payment_data['payment_id'] = $new_transaction_id;
			}
		}

		// If the credit card gratuity transaction was approved
		if($approved){
			
			// Update credit card payment info
			$this->Sale->update_credit_card_payment($cc_invoice_id, $payment_data);
			return true;
		}

		return false;
	}

	function get_credit_card_payment($invoice_id) {
		$this->db->from('sales_payments_credit_cards');
		$this->db->where('invoice', $invoice_id);
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}

	function add_ets_refund_payment($invoice_id, $amount){
		$this->db->where('invoice', $invoice_id);
		$this->db->update('sales_payments_credit_cards', array('token_used' => 1,'amount_refunded' => $amount));
		return $this->db->affected_rows();
	}

	function delete($sale_id){

		if(empty($sale_id)){
			return false;
		}
		$this->load->model('v2/cart_model');
		$this->load->model('Account_transactions');
		$this->load->model('Customer_loyalty');
		$this->load->model('v2/Tournament_model');
		$this->load->model('v2/Giftcard_model');
		$this->load->model('v2/Punch_card_model');
		$this->load->model('Pass');

		// Get sale data (items, payments, etc)
		$sale = $this->get(array('sale_id' => $sale_id));
		
		if(empty($sale[0])){
			return false;
		}
		$sale = $sale[0];
		$employee_id = $this->session->userdata('person_id');
		$course_id = (int) $this->session->userdata('course_id');
		
		$players = 0;
		$carts = 0;
		$teetime_id = $sale['teetime_id'];
		
		foreach($sale['items'] as $item){
			
			// Delete gift cards issued
			if($item['item_type'] == 'giftcard' && !empty($item['item_number'])){
				$this->Giftcard_model->delete(array('giftcard_number' => $item['item_number']));
			}
			
			// Delete punch cards issued
			if($item['item_type'] == 'punch_card' && !empty($item['item_number'])){
				$this->Punch_card_model->delete(array('punch_card_number' => $item['item_number']));
			}			
			
			// Reverse any tournament payments
			if($item['item_type'] == 'tournament' && !empty($item['tournament_id'])){
				$this->Tournament_model->pot_transaction($item['tournament_id'], -$item['total']);
			}
			
			if($item['item_type'] == 'green_fee'){
				$players += $item['quantity'];
			}

			if($item['item_type'] == 'cart_fee'){
				$carts += $item['quantity'];
			}

			if(($item['item_type'] == 'green_fee' || $item['item_type'] == 'cart_fee') 
				&& !empty($item['params']['pass_id'])){
				$this->Pass->delete_history(
					$item['params']['pass_id'], 
					'sale', 
					['sale_id' => $sale_id]
				);
			}

			if($item['item_type'] == 'invoice')
            {
                $this->Invoice->pay_invoice($sale_id, $item['invoice_id'], false, -$item['total']);
            }
		}

        // Revert tee time paid status (if there was a tee time sold)
		if(!empty($teetime_id)){
			$this->mark_teetime_paid($teetime_id, $sale['customer_id'], -$players, -$carts);
		}
		
		// Refund any credit card payments made
		foreach($sale['payments'] as $payment){
			
			// If payment is a credit card, refund it
			if((int) $payment['invoice_id'] > 0 && stripos($payment['description'], 'giftcard') === false && stripos($payment['description'], 'PreAuth') === false){
				$this->cart_model->refund_credit_card($payment['invoice_id']);
			}
		}

		// Reverse any account transactions made
		$transactions = $this->Account_transactions->get_all_by_sale_id($sale_id);
		if(!empty($transactions)){
			foreach($transactions as $transaction){
				$this->Account_transactions->save($transaction['account_type'], (int) $transaction['trans_customer'], 'Deleted POS '.$sale_id, -$transaction['trans_amount'], '', 0, $transaction['invoice_id'], $employee_id, false, false, false, 1);
                // Mark the original transaction to not show on invoices
                $this->Account_transactions->update($transaction['trans_id'], array('hide_on_invoices'=>1));
            }
		}

		// Adjust inventory
		$this->adjust_inventory($sale['items'], $sale_id, $employee_id, 'refund');

		// Refund loyalty rewards or loyalty payments
        if((int)$this->config->item('use_loyalty') == 1 && !empty($sale['customer']) && (int)$sale['customer']['use_loyalty'] == 1){
            $loyalty_transactions = $this->Customer_loyalty->get_all_by_sale_id($sale_id);
            if(!empty($loyalty_transactions)){
                foreach($loyalty_transactions as $lt)
                {
                    $this->Customer_loyalty->save_transaction($lt['trans_customer'], 'Deleted POS '.$sale_id, -$lt['trans_amount'], '', $sale_id, $employee_id);
                }
            }
		}

		// Mark the sale as deleted
		$this->db->update('sales', array(
			'deleted' => 1,
			'deleted_by' => $employee_id,
			'deleted_at' => date('Y-m-d H:i:s')
		), array(
			'sale_id' => (int) $sale_id,
			'course_id' => $course_id
		));

		return $this->db->affected_rows();
	}

	function adjust_inventory($items, $sale_id, $employee_id, $type = 'sale'){

		$this->load->model('Inventory');
		$this->load->model('v2/Item_model');

		if(empty($items)){
			return false;
		}

		// Loop through items and make inventory adjustments
		foreach($items as $item){

			if(($item['item_type'] != 'item' && $item['item_type'] != 'item_kit')|| ($item['quantity'] < 0 && $item['food_and_beverage'] == 1)){
				continue;
			}

			if($type == 'sale'){
				$qty = (float) -$item['quantity'];
			}else{
				$qty = (float) $item['quantity'];
			}

            if($item['item_type'] == 'item') {
                
                // Update item quantity field
                $this->Item_model->update_quantity($item['item_id'], $qty);

                // Make adjustment to inventory table
                $sale_remarks = 'POS ' . $sale_id;
                $inv_data = array(
                    'trans_date' => date('Y-m-d H:i:s'),
                    'trans_items' => $item['item_id'],
                    'trans_user' => $employee_id,
                    'trans_comment' => $sale_remarks,
                    'trans_inventory' => $qty,
                    'sale_id' => $sale_id
                );
                
                $this->Inventory->insert($inv_data);
            
            }else if ($item['item_type'] == 'item_kit'){ 
                if(empty($item['items'])){
                	return true;
                }
                $this->adjust_inventory($item['items'], $sale_id, $employee_id, $type);
            }
		}

		return true;
	}

	function adjust_loyalty($items, $payments, $sale_id, $customer_id, $employee_id, $type = 'sale', $loyalty_package_id = null){

		$this->load->model('Customer_loyalty');
		$this->load->model('v2/Item_model');

		$invalid_loyalty_payments = array(
			'loyalty_points',
			'raincheck'
		);

		if(empty($items)){
			return false;
		}
		$item_points = 0;
		$item_dollar_points = 0.00;
		$total_qualified_payments = 0.00;
		$total_payments = 0;
		
		// Loop through items and total up potential loyalty points
		foreach($items as $item){
			$item_points = $this->Customer_loyalty->get_points_per_dollar($item, $loyalty_package_id);
			$item_dollar_points += (float) $item_points['points_per_dollar'] * (float) $item['subtotal'];
		}

		// Total up all payments
		foreach($payments as $payment){
		    $total_payments += $payment['amount'];

			if(in_array($payment['type'], $invalid_loyalty_payments)){
				continue;
			}
			$total_qualified_payments += $payment['amount'];
		}

		// If non qualifying payments were used, reduce points earned accordingly
		if ($total_qualified_payments < $total_payments){
			$points = (int) floor($item_dollar_points * $total_qualified_payments / $total_payments);
		}else{
			$points = (int) floor($item_dollar_points);
		}
		
		// Save loyalty transaction
        if ($points != 0){

			$comment = 'POS '.$this->sale_number;
            if ($type == 'refund') {
                $points = -$points;
            }
			if($points < 0){
				$comment = 'Deleted POS '.$this->sale_number;
			}
			$this->Customer_loyalty->save_transaction($customer_id, $comment, $points, '', $sale_id, $employee_id, $loyalty_package_id);
		}

		return $points;
	}
	
	function save_credit_card_payment($cc_invoice_id, $data){
		
		if(!empty($cc_invoice_id)){
			$this->db->update('sales_payments_credit_cards', $data, array('invoice' => $cc_invoice_id));
			return $this->db->affected_rows();
		
		}else{
			$data['course_id'] = (int) $this->session->userdata('course_id');
			
			$this->db->insert('sales_payments_credit_cards', $data);
			return $this->db->insert_id();
		}	
	}

	function get_register_log_payment_totals($shift_start, $shift_end, $person_id = false, $terminal_id = false, $persisting_log = false){
    	
        $employee_id = $person_id ? $person_id : $this->Employee->get_logged_in_employee_info()->person_id;
		$terminal_id = $terminal_id ? $terminal_id : $this->session->userdata('terminal_id');
		$terminal_id = $terminal_id == '' ? 0 : $terminal_id;

		$shift_start = date('Y-m-d H:i:s', strtotime($shift_start));
		$shift_end = date('Y-m-d H:i:s', strtotime($shift_end));

		$this->db->select('sales_payments.sale_id, sales_payments.payment_type, payment_amount', false);
        $this->db->from('sales_payments');
        $this->db->join('sales','sales_payments.sale_id = sales.sale_id');
		$this->db->where('sale_time >=', $shift_start);
		$this->db->where('sale_time <=', $shift_end);
		
		if(!$persisting_log){
			$this->db->where('employee_id', $employee_id);
		}
		
		$this->db->where('terminal_id', $terminal_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where($this->db->dbprefix('sales').'.deleted', 0);
		
		$sales_payments = $this->db->get()->result_array();

		$payment_data = array();
		$tips = 0;

		$payment_data = array(
			'check' => array('payment_amount' => 0),
			'cash' => array('payment_amount' => 0)
		);

		foreach($sales_payments as $row){

			if(stripos($row['payment_type'], 'tip') !== false){
				$tips += (float) $row['payment_amount'];
			}

			$payment_amount = $row['payment_amount'];
			if(stripos($row['payment_type'], 'cash') !== false || 
				stripos($row['payment_type'], 'change issued') !== false){
				$row['payment_type'] = 'cash';
			}
			if(stripos($row['payment_type'], 'check') !== false && stripos($row['payment_type'], 'raincheck') === false ){
				$row['payment_type'] = 'check';
			}
			
			if(!isset($payment_data[$row['payment_type']])){
				$payment_data[$row['payment_type']] = array('payment_type' => $row['payment_type'], 'payment_amount' => 0 );
			}

			$payment_data[$row['payment_type']]['payment_amount'] += $payment_amount;
		}
		
		if($this->config->item('deduct_tips') == 1){
			$payment_data['cash']['payment_amount'] -= $tips;
		}

		return array(
			'cash' => $payment_data['cash']['payment_amount'],
			'check' => $payment_data['check']['payment_amount']
		);		
	}
    function save_refund_details($sale_id, $data) {
        $refund_details = array();

        if (!empty($data['refund_comment'])) {
            $refund_details[] = "refund_comment = CONCAT(refund_comment, \" - ".addslashes($data['refund_comment'])."\")";
        }
        if (!empty($data['refund_reason'])) {
            $refund_details[] = "refund_reason = \"".addslashes($data['refund_reason'])."\"";
        }

        $refund_details_sql = implode(', ', $refund_details);

        if (!empty($refund_details_sql)) {
            $this->db->query("
                UPDATE foreup_sales SET {$refund_details_sql} WHERE sale_id = {$sale_id} AND course_id = {$this->config->item('course_id')} LIMIT 1;
            ");
        }
    }
    function has_been_returned($sale_id){
        $this->db->from('sales');
        $this->db->where('refunded_sale_id', $sale_id);
        $this->db->limit(1);

        return $this->db->get()->num_rows();
    }
}
