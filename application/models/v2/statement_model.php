<?php
class Statement_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function get($params){

	    $course_id = (int) $this->session->userdata('course_id');

	    if(empty($params['number']) && empty($params['id'])){
	        return false;
        }

        $this->db->select('s.id, s.number, s.date_created, s.due_date, s.start_date, s.end_date, s.date_paid, s.total,
            s.total_finance_charges, s.total_paid, s.total_open, s.previous_open_balance, s.person_id, 
            p.first_name AS customer_first_name, p.last_name AS customer_last_name, 
            c.invoice_balance AS customer_invoice_balance', false);
        $this->db->from('account_statements AS s');
        $this->db->join('people AS p', 'p.person_id = s.person_id');
        $this->db->join('customers AS c', 'c.person_id = p.person_id AND c.course_id = s.organization_id');
        $this->db->where('organization_id', $course_id);
        $this->db->where('date_deleted', null);

        if(!empty($params['number'])){
            $this->db->where('number', trim($params['number']));
        }
        if(!empty($params['id'])){
            $this->db->where('number', (int) $params['id']);
        }

        $rows = $this->db->get()->result_array();

        foreach($rows as &$row){

            $customer = [
                'person_id' => $row['person_id'],
                'first_name' => $row['customer_first_name'],
                'last_name' => $row['customer_last_name'],
                'invoice_balance' => $row['customer_invoice_balance']
            ];

            unset($row['customer_first_name'], $row['customer_last_name'], $row['customer_invoice_balance']);
            $row['customer'] = $customer;
        }

        return $rows;
	}
}
