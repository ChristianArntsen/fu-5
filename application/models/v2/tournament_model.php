<?php
class Tournament_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->loyalty_rates = null;
	}

	// Search item database and return matched item IDs
	function search($search){

		$this->db->select('tournament_id');
		$this->db->from('tournaments');
		$this->db->where("deleted", 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where("name LIKE '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by("name", "ASC");
		$this->db->limit(50);
		$rows = $this->db->get()->result_array();

		$ids = array();
		foreach($rows as $row){
			$ids[] = (int) $row['tournament_id'];
		}

		return $ids;
	}

	public function get($params){
		
		$this->load->model('Item_taxes');
		$this->load->model('v2/Item_model');
		$this->load->model('v2/Cart_model');
		$this->load->model('Price_class');
		$this->load->model('Pricing');
		$course_id = $this->session->userdata('course_id');

		// Get tournament details, including any attached items
		$this->db->select("t.*, 'tournament' AS item_type, 
			total_cost AS total, total_cost AS subtotal, total_cost AS unit_price, 
			0 AS discount_percent, 1 AS quantity,
			GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				item.tournament_inventory_item_id,
				item.item_id,
				item.price,
				item.quantity,
				1
			) SEPARATOR 0x1E) AS items", false);
		$this->db->from('tournaments AS t');
		$this->db->join('tournament_inventory_items AS item', 'item.tournament_id = t.tournament_id', 'left');
		$this->db->where('t.deleted', 0);
		$this->db->where('t.course_id', $course_id);
		$this->db->group_by('t.tournament_id');

		if(!empty($params['tournament_id'])){
			if(is_array($params['tournament_id'])){
				$ids = array();
				foreach($params['tournament_id'] as $tournament_id){
					$ids[] = (int) $tournament_id;
				}
				$this->db->where_in('t.tournament_id', $ids);

			}else{
				$this->db->where('t.tournament_id', (int) $params['tournament_id']);
			}
		}
		
		$rows = $this->db->get()->result_array();		

		$default_price_class = $this->Price_class->get_default();
		$price_class_id = (int) $default_price_class['class_id'];
		
		// Loop through each tournament. Calculate prices and put together
		// list of items that are bundled with the tournament
		foreach($rows as &$row){
			
			$row['item_type'] = 'tournament';
			$row['items'] = $this->get_items_array($row['items']);
			$row['unit_price_includes_tax'] = 1;
						
			// Get details of each item
			if(!empty($row['items'])){
				
				$item_ids = array_map(function($row){ return (int) $row['item_id']; }, $row['items']);
				$items = $this->Item_model->get(array('item_id' => $item_ids));

				// Apply details to items
				foreach($items as $item){
					$row['items'][$item['item_id']] = array_merge($item, $row['items'][$item['item_id']]);
					$row['items'][$item['item_id']]['subtotal'] = $item['unit_price'];
					$row['items'][$item['item_id']]['total'] = $item['unit_price'];
				}
				
				$row['items'] = array_values($row['items']);
			}
			
			// If green fee included
			if($row['green_fee'] > 0){
				
				$green_fees = $this->Pricing->get_fees(array(
					'holes' => 9, 
					'type' => 'green_fee',
					'price_class_id' => $price_class_id,
					'date' => date('Y-m-d')
				));
				$green_fee = $green_fees[0];
				$green_fee['unit_price'] = $row['green_fee'];
				$green_fee['unit_price_includes_tax'] = 1;
				
				$row['items'][] = $green_fee;
			}
			
			// If cart fee included
			if($row['cart_fee'] > 0){
				$cart_fees = $this->Pricing->get_fees(array(
					'holes' => 9, 
					'type' => 'cart_fee',
					'price_class_id' => $price_class_id,
					'date' => date('Y-m-d')
				));
				$cart_fee = $cart_fees[0];
				$cart_fee['unit_price'] = $row['cart_fee'];
				$cart_fee['unit_price_includes_tax'] = 1;
				
				$row['items'][] = $cart_fee;
			}
			
			// If payment into pro shop account included
			if($row['customer_credit_fee'] > 0){
				$row['items'][] = array(
					'name' => "Customer Credit",
					'item_type' => 'customer_account',
					'item_id' => 0,
					'unit_price' => $row['customer_credit_fee'],
					'quantity' => 1,
					'discount_percent' => 0,
					'subtotal' => $row['customer_credit_fee'],
					'total' => $row['customer_credit_fee'],				
					'taxes' => array(),
					'unit_price_includes_tax' => 1
				);				
			}
			
			// If payment into member account included
			if($row['member_credit_fee'] > 0){
				$row['items'][] = array(
					'name' => "Member Account",
					'item_type' => 'member_account',
					'item_id' => 0,
					'unit_price' => $row['member_credit_fee'],
					'quantity' => 1,
					'discount_percent' => 0,
					'subtotal' => $row['member_credit_fee'],
					'total' => $row['member_credit_fee'],				
					'taxes' => array(),
					'unit_price_includes_tax' => 1
				);				
			}										
			
			$this->Cart_model->calculate_item_totals($row, true, false);					
		}
		
		return array_values($rows);
	}
	
	private function get_items_array($items){
		
		$items = explode("\x1E", $items);
		$item_rows = array();
		
		if(empty($items)){
			return $item_rows;
		}
		
		// Loop through each item row, break apart fields
		foreach($items as $item_row){
			$item = explode("\x1F", $item_row);
			
			if(empty($item[1])){
				continue;
			}
			
			$item = array(
				'tournament_inventory_item_id' => (int) $item[0],
				'item_id' => (int) $item[1],
				'unit_price' => (float) $item[2],
				'quantity' => (float) $item[3],
				'unit_price_includes_tax' => (int) $item[4]
			);
			
			$item_rows[$item['item_id']] = $item;
		}

		return $item_rows;
	}
	
	function get_tournament_item($tournament_item_number, $name){
		
		$this->load->model('v2/Item_model');
		$item_id = $this->Item_model->check_item_number($tournament_item_number);
		
		// If a tournament item already exists, return the ID
		if($item_id){
			return $item_id;
		}
		
		// If no tournament item exists, create it in inventory to 
		// make tracking/reporting easier
		$item = array(
			'name' => $name,
			'department' => 'Tournaments',
			'category' => 'Tournaments',
			'item_number' => $tournament_item_number,
			'cost_price' => 0,
			'unit_price' => 0,
			'max_discount' => 100,
			'quantity' => 0,
			'is_unlimited' => 1,
			'reorder_level' => 0,
			'is_serialized' => 1,
			'invisible' => 1,
			'course_id' => $this->session->userdata('course_id')
		);
		
		// Add the new tournament item to the database
		$item_id = $this->Item_model->save(null, $item);
		return $item_id;
	}
	
	function pot_transaction($tournament_id, $amount){
		
		if(empty($tournament_id)){
			return false;
		}
		$tournament_id = (int) $tournament_id;
		$amount = (float) $amount;
		
		$this->db->query("UPDATE foreup_tournaments 
			SET accumulated_pot = accumulated_pot + {$amount},
			remaining_pot_balance = remaining_pot_balance + {$amount}
			WHERE tournament_id = {$tournament_id}");
			
		return $this->db->affected_rows();	
	}
}
