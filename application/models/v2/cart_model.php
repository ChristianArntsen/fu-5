<?php
class Cart_model extends CI_Model {

	var $item_details = array();
	var $customer_details = array();
	var $valid_payment_types = array('cash', 'check', 'credit_card', 'credit_card_refund', 'member_account',
		'customer_account', 'gift_card', 'raincheck', 'raincheck_issued', 'punch_card', 'loyalty_points', 'credit_card_save',
		'coupon', 'ets_gift_card');

	var $no_discount_items = array('green_fee', 'cart_fee', 'giftcard', 
		'punch_card', 'member_account', 'customer_account', 
		'invoice_account', 'invoice', 'tournament');

	var $error = false;
	var $status = false;
	var $msg = null;
	var $credit_card_fees = 0;
	var $terminal_id = false;
	var $terminal_name = '';
	var $employee_name = '';
	var $cart_id;


	function __construct(){

		parent::__construct();
		$this->load->library('v2/cart_lib');
		$this->course_id = (int) $this->session->userdata('course_id');
		$this->load->model('v2/sale_model');

		if($this->session->userdata('pos_cart_id')){
			$this->cart_id = (int) $this->session->userdata('pos_cart_id');

			// Check that the cart row still exists
			$row = $this->db->select('*')
				->from('pos_cart')
				->where('cart_id', $this->cart_id)
				->get()->row_array();

			if(empty($row) || empty($row['cart_id']) || $row['status'] != 'active'){
				$this->cart_id = $this->initialize_cart();
				$this->session->set_userdata('pos_cart_id', $this->cart_id);
			}

		}else{
			$this->cart_id = $this->initialize_cart();
			$this->session->set_userdata('pos_cart_id', $this->cart_id);
		}
	}

	function initialize_cart(){

		if(!$this->session->userdata('course_id') || $this->session->userdata('course_id') == 0 || $this->session->userdata('course_id') == '15710'){
			return false;
		}

		$cur_date = gmdate('Y-m-d H:i:s');
		$this->db->insert('pos_cart', array(
			'date_created' => $cur_date,
			'employee_id' => $this->session->userdata('person_id'),
			'course_id' => $this->session->userdata('course_id'),
			'status' => 'active',
			'suspended_name' => null,
			'last_activity' => $cur_date,
			'last_ping' => gmdate('Y-m-d H:i:s'),
			'taxable' => 1,
			'return_sale_id' => null,
			'terminal_id' => $this->session->userdata('terminal_id')
		));

		return (int) $this->db->insert_id();
	}

	private function get_next_line(){
		$this->db->select('MAX(line) AS line');
		$this->db->from('pos_cart_items');
		$this->db->where('cart_id', $this->cart_id);
		$row = $this->db->get()->row_array();

		return (int) $row['line'] + 1;
	}

	private function update_activity(){
		$this->db->update('pos_cart', array(
			'last_activity' => gmdate('Y-m-d H:i:s'),
			'last_ping' => gmdate('Y-m-d H:i:s')
		), array(
			'cart_id' => $this->cart_id
		));
		return $this->db->affected_rows();
	}

	function cart_exists($cart_id, $status = false){
		
		$this->db->select('cart_id')
			->from('pos_cart')
			->where('cart_id', (int) $cart_id);

		if($status){
			$this->db->where('status', $status);
		}
		$row = $this->db->get()->row_array();

		if(!empty($row['cart_id'])){
			return true;
		}

		return false;
	}

	function ping($cart_id){
		
		$this->db->where('cart_id', $cart_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));

		date_default_timezone_set("UTC");
		$this->db->update('pos_cart', array(
			'last_ping' => gmdate('Y-m-d H:i:s')
		));

		return $this->db->affected_rows();		
	}

	// Marks the current cart as "suspended" and creates a new cart
	function suspend($name = false){

		if(!$name){
			return false;
		}
		$current_cart_id = $this->session->userdata('pos_cart_id');

		// Make sure a suspended cart doesn't already exist with that name
		$cart_exists = $this->db->select('cart_id')
			->from('pos_cart')
			->where('suspended_name', $name)
			->where('cart_id !=', $current_cart_id)
			->where('course_id', $this->course_id)
			->where_in('status', ['suspended', 'active', 'incomplete'])
			->get()->row_array();

		if(!empty($cart_exists['cart_id'])){
			$this->msg = 'A sale already exists with that label. Choose a different label.';
			return false;
		}

		// Mark current cart as suspended
		$this->db->update('pos_cart',
			array('suspended_name' => $name, 'status' => 'suspended'),
			array('cart_id' => $current_cart_id)
		);

		// Create a new cart
		$this->cart_id = $this->initialize_cart();
		$this->session->set_userdata('pos_cart_id', $this->cart_id);
		$this->session->sess_write();

		return $this->cart_id;
	}

	// Deletes current cart and creates a new one
	function reset(){
		$this->delete($this->session->userdata('pos_cart_id'));
		$new_cart_id = $this->initialize_cart();
		$this->session->set_userdata('pos_cart_id', $new_cart_id);
		$this->session->sess_write();

		$this->cart_id = $new_cart_id;

		return $new_cart_id;
	}

	function resume($cart_id){

		// Check that the cart is still suspended or incomplete, and is part of the course
		$cart = $this->db->select("cart.cart_id, terminal.label AS terminal_name, cart.status,
				CONCAT(employee.first_name,' ',employee.last_name) AS employee_name", false)
			->from('pos_cart AS cart')
			->join('terminals AS terminal', 'terminal.terminal_id = cart.terminal_id', 'left')
			->join('people AS employee', 'employee.person_id = cart.employee_id', 'left')
			->where('cart.course_id', $this->course_id)
			->where('cart.cart_id', $cart_id)
			->get()->row_array();

		// If the cart no longer exists
		if(empty($cart['cart_id'])){
			$this->msg = 'That cart no longer exists';
			return false;
		}

		// If the cart exists, but has already been re-activated somewhere else
		if($cart['status'] == 'active'){
			if(!empty($cart['terminal_name']) && !empty($cart['employee_name'])){
				$this->msg = 'That cart was already opened on '.$cart['terminal_name'].' by '.$cart['employee_name'];
			}else if(!empty($cart['employee_name'])){
				$this->msg = 'That cart was already opened by '.$cart['employee_name'];
			}else{
				$this->msg = 'That cart was opened on another terminal';
			}
			return false;
		}

		$current_cart_id = $this->session->userdata('pos_cart_id');

		$current_cart = $this->db->select('suspended_name')
			->from('pos_cart')
			->where('course_id', $this->course_id)
			->where('cart_id', $current_cart_id)
			->get()->row_array();
		
		if(empty($current_cart['suspended_name'])){
			$this->cart_id = $current_cart_id;
			$this->delete($current_cart_id);
			$this->cart_id = $cart_id;
		
		}else{
			$this->db->update('pos_cart',
				array('status' => 'suspended'),
				array('cart_id' => $current_cart_id)
			);			
		}
		
		$this->db->update('pos_cart',
			array(
				'status' => 'active', 
				'terminal_id' => $this->session->userdata('terminal_id'),
				'last_activity' => date('Y-m-d H:i:s'),
				'last_ping' => gmdate('Y-m-d H:i:s'),
				'employee_id' => $this->session->userdata('person_id')
			),
			array('course_id' => $this->course_id, 'cart_id' => $cart_id)
		);

		$this->cart_id = $cart_id;
		$this->session->set_userdata('pos_cart_id', $this->cart_id);
		$this->session->sess_write();

		return $this->cart_id;
	}

	// Get all currently suspended carts
	function get($params = array()){
		$this->course_id = (int) $this->session->userdata('course_id');
		$validStatus = ['suspended', 'incomplete'];
		$params = elements(['status', 'cart_id'], $params, null);

		$this->load->model('v2/Item_model');
		$this->load->model('v2/Customer_model');

		$this->item_ids = array();
		$this->item_kit_ids = array();
		$this->customer_ids = array();

		$filter_sql = '';
		if(empty($params['status']) || !in_array($params['status'], $validStatus)){
			unset($params['status']);
		}else{
			$filter_sql .= "AND cart.status = '".$params['status']."'";
		}
		
		if(!empty($params['cart_id'])){
			$filter_sql .= " AND cart.cart_id = ".(int) $params['cart_id'];
		}

		// Get all suspended carts with associated customers, items, and payments
		$query = $this->db->query("SELECT cart.*, teetime.start_datetime AS teetime_date, 
			teetime.teesheet_id AS teetime_teesheet_id, teesheet.title AS teesheet_title,
			concat(employee.first_name,' ',employee.last_name) as employee_name,
			terminal.label as 'terminal_label',
			GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				payment.payment_id,
				payment.amount,
				payment.type,
				payment.description,
				payment.number,
				payment.record_id,
				IFNULL(payment.params, '')
			) SEPARATOR 0x1E) AS payments,

			GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				customer.customer_id,
				customer.selected
			) SEPARATOR 0x1E) AS customers

			FROM foreup_pos_cart AS cart
			LEFT JOIN foreup_teetime AS teetime
				ON teetime.TTID = cart.teetime_id
			LEFT JOIN foreup_teesheet AS teesheet
				ON teesheet.teesheet_id = teetime.teesheet_id
			LEFT JOIN foreup_pos_cart_items AS item
				ON item.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_payments AS payment
				ON payment.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_customers AS customer
				ON customer.cart_id = cart.cart_id
			LEFT JOIN foreup_people AS employee
				ON cart.employee_id = employee.person_id
			LEFT JOIN foreup_terminals AS terminal
				ON cart.terminal_id = terminal.terminal_id
			WHERE 1=1 {$filter_sql}
				AND cart.course_id = {$this->course_id}
			GROUP BY cart.cart_id
			ORDER BY cart.suspended_name ASC");

		$rows = $query->result_array();

		// Get all items from all suspended carts
		$cart_items = $this->get_items(false, $params);
		$return_sale_ids = array();

		// Loop through suspended carts, turn items, payments and customers
		// into sub-arrays
		foreach($rows as &$row){
			
			if(!empty($row['return_sale_id'])){
				$return_sale_ids[] = (int) $row['return_sale_id'];
			}
			
			$cart_id = (int) $row['cart_id'];
			$row['items'] = array();
			
			if(!empty($cart_items[$cart_id])){
				$row['items'] = array_values($cart_items[$cart_id]);
			}else if(!empty($params['cart_id']) && !empty($cart_items)){
				$row['items'] = $cart_items;
			}

			$row['teetime'] = false;
			if(!empty($row['teetime_id'])){
				$row['teetime'] = [
					'teetime_id' => $row['teetime_id'],
					'start_date' => $row['teetime_date'],
					'teesheet_title' => $row['teesheet_title']
				];
			}

			$row['payments'] = $this->get_payments_array($row['payments']);
			$row['customers'] = $this->get_customers_array($row['customers']);
		}

		$sale_payments = array();
		if(!empty($return_sale_ids)){
			$sale_payments = $this->get_sale_payments($return_sale_ids);
		}

		// Retrieve details needed for each customer in each cart
		$customers = array();
		if(!empty($this->customer_ids)){

			// If there is a tee time in the cart, set the teetime
			// data in the customer model which will be used to check
			// if any customer passes are valid
			if(!empty($row['teetime_id'])){
				$this->Customer_model->teetime_data = [
					'date' => $row['teetime_date'],
					'teesheet_id' => $row['teetime_teesheet_id']
				];				
			}

			$customers = $this->Customer_model->get([
				'person_id' => $this->customer_ids,
				'include_recent_transactions' => true,
				'include_gift_cards' => true,
				'include_rainchecks' => true
			]);
		}

		// Key the customer details by person_id
		$customerDetails = array();
		foreach($customers as $customer){
			$customerDetails[$customer['person_id']] = $customer;
		}

		// Loop through each cart's items and apply details and calculate
		// totals of each item. Also calculate cart totals
		foreach($rows as &$cart){

			$cart['total'] = 0;
			$cart['total_due'] = 0;
			$cart['tax'] = 0;
			$cart['subtotal'] = 0;
			$cart['taxes'] = array();
			$cart['num_items'] = 0;
			$cart['sale_payments'] = array();

			if($cart['taxable'] == 1){
				$cart['taxable'] = true;
			}else{
				$cart['taxable'] = false;
			}

			if(!empty($cart['return_sale_id']) && !empty($sale_payments[$cart['return_sale_id']])){
				$cart['sale_payments'] = $sale_payments[$cart['return_sale_id']];
			}

			// Loop through each customer
			foreach($cart['customers'] as &$customer){
				if(isset($customerDetails[$customer['customer_id']])){
					$customer = array_merge($customerDetails[$customer['customer_id']], $customer);
				}
			}

			// Loop through each cart item and add up cart totals
			foreach($cart['items'] as &$item){

				if(empty($item)){
					continue;
				}
				
				if($cart['mode'] == 'return'){
					$item['service_fees'] = [];
				}

				$this->calculate_item_totals($item, $cart['taxable']);
				
				$cart['num_items'] += $item['quantity'];
				$cart['total'] += $item['total'];
				$cart['subtotal'] += $item['subtotal'];
				$cart['tax'] += $item['tax'];
				if(isset($item['taxes']) && is_array($item['taxes'])){
					$cart['taxes'] = array_merge($cart['taxes'], array_values($item['taxes']));
				}

				if(!empty($item['food_and_beverage']) && $item['food_and_beverage'] == 1){
					
					if(empty($item['sides_total'])){
						$item['sides_total'] = 0;
					}
					if(empty($item['sides_subtotal'])){
						$item['sides_subtotal'] = 0;
					}
					if(empty($item['sides_tax'])){
						$item['sides_tax'] = 0;
					}

					$cart['total'] += $item['sides_total'];
					$cart['subtotal'] += $item['sides_subtotal'];
					$cart['tax'] += $item['sides_tax'];
				}
				
				$sideTypes = array('sides', 'soups', 'salads');
				foreach($sideTypes as $sideType){
					if(empty($item[$sideType])){
						continue;
					}
					
					foreach($item[$sideType] as $side){
						if(empty($side['taxes'])){
							continue;
						}
						$cart['taxes'] = array_merge($cart['taxes'], array_values($side['taxes']));
					}
				}
			}

			$this->set_applied_customer_passes($cart['customers'], $cart['items']);			

			// Total payments
			$cart['total_due'] = $cart['total'];
			foreach($cart['payments'] as $payment){
				$cart['total_due'] -= $payment['amount'];
			}
			
			if($cart['taxable']){
				$cart['taxes'] = $this->group_cart_taxes($cart['taxes']);
			}else{
				$cart['taxes'] = array();
			}
		}

		return $rows;
	}

	function set_applied_customer_passes(&$customers, $items){
		
		if(empty($customers) || empty($items)){
			return false;
		}

		foreach($customers as &$customer){
			
			if(empty($customer['passes'])){
				continue;
			}

			foreach($customer['passes'] as &$pass){
				
				$pass['is_applied'] = false;

				foreach($items as $item){
					if(
						!empty($item['params']['pass_id']) && 
						$item['params']['pass_id'] == $pass['pass_id'] &&
						$item['params']['customer_id'] == $customer['person_id']
					){	
						if(empty($pass['rules'])){
							continue;
						}
						$pass['is_applied'] = true;

						foreach($pass['rules'] as &$rule){
							if(
								isset($item['params']['pass_rule_number']) && 
								$rule['rule_number'] == $item['params']['pass_rule_number']
							){
								$rule['is_applied'] = true;
							}
						}
					}
				}
			}
		}
	}

	private function get_items_array($items){

		if(strlen($items) == 1 || empty($items) || empty($items[0])){
			return array();
		}
		$items = explode("\x1E", $items);

		// Loop through each item row, break apart fields
		foreach($items as &$item_row){
			$item = explode("\x1F", $item_row);

			$selected = true;
			if($item[12] == 0){
				$selected = false;
			}

			$item = array(
				'line' => (int) $item[0],
				'item_id' => (int) $item[1],
				'quantity' => (float) $item[2],
				'unit_price' => (float) $item[3],
				'discount_percent' => (float) $item[4],
				'item_type' => $item[5],
				'item_number' => $item[6],
				'price_class_id' => $item[7],
				'timeframe_id' => $item[8],
				'special_id' => $item[9],
				'params' => $item[10],
				'punch_card_id' => $item[11],
				'selected' => $selected
			);
			$item_row = $item;

			if(!empty($item['item_id'])){
				if($item['item_type'] == 'item_kit' || $item['item_type'] == 'punch_card'){
					$this->item_kit_ids[] = (int) $item['item_id'];
				}else{
					$this->item_ids[] = (int) $item['item_id'];
				}
			}
		}

		return $items;
	}

	private function get_payments_array($payments){

		if(strlen($payments) == 1 || empty($payments) || empty($payments[0])){
			return array();
		}
		$payments = explode("\x1E", $payments);

		// Loop through each item row, break apart fields
		foreach($payments as &$payment_row){
			$payment = explode("\x1F", $payment_row);
			$payment = array(
				'payment_id' => (int) $payment[0],
				'amount' => (float) $payment[1],
				'type' => $payment[2],
				'description' => $payment[3],
				'number' => $payment[4],
				'record_id' => (int) $payment[5],
				'params' => json_decode($payment[6], true),
			);
			$payment_row = $payment;
		}

		return $payments;
	}

	private function get_customers_array($customers){

		if(strlen($customers) == 1 || empty($customers) || empty($customers[0])){
			return array();
		}
		$customers = explode("\x1E", $customers);

		// Loop through each item row, break apart fields
		foreach($customers as &$customer_row){
			$customer = explode("\x1F", $customer_row);
			$customer = array(
				'customer_id' => (int) $customer[0],
				'selected' => (bool) $customer[1]
			);
			$customer_row = $customer;
			$this->customer_ids[] = (int) $customer['customer_id'];
		}

		return $customers;
	}

	// Retrieves cart details
	function get_cart(){
		$query = $this->db->get_where('pos_cart', array('cart_id' => $this->cart_id));
		$row = $query->row_array();
		if(isset($row['taxable']))
			$row['taxable'] = (bool) $row['taxable'];
		return $row;
	}

	// Saves a few settings on the cart (taxable, mode, status)
	function save_cart($cart_data){

		$cart_data = elements(array('taxable', 'mode', 'status',
			'suspended_name', 'teetime_id', 'override_authorization_id',
			'return_sale_id'), $cart_data, "\x18");

		// Clear settings that were not passed
		foreach($cart_data as $key => $value){
			if($value === "\x18"){
				unset($cart_data[$key]);
			}
		}
		
		$current_cart = $this->get_cart();

		// If mode is changed (et sale to return), reverse the item quantities
		if(!empty($cart_data['mode']) && $current_cart['mode'] != $cart_data['mode']){
			$this->db->query("UPDATE foreup_pos_cart_items
				SET quantity = 0 - quantity
				WHERE cart_id = {$this->cart_id}");
		}
		$cart_data['last_activity'] = date('Y-m-d H:i:s');

		$this->db->update('pos_cart', $cart_data, array('cart_id' => $this->cart_id));
		return $this->db->affected_rows();
	}

	function get_taxable(){
		$cart = $this->get_cart();
		return $cart['taxable'];
	}

	// Totals up all payments in cart
	function get_payment_total(){

		$payment_total = 0.00;
		$payments = $this->get_payments();
		$this->credit_card_fees = 0;
		
		foreach($payments as $type => $payment){
			$payment_total += $payment['amount'];
			
			if(!empty($payment['params']['fee']) && $payment['type'] == 'credit_card'){
				$this->credit_card_fees += $payment['params']['fee'];	
			}
		}
		
		return (float) round($payment_total, 2);
	}

	// Totals up all items in cart
	function get_item_totals($selected_only = true,$taxable = 1){

        $ItemPrice = new \fu\items\ItemPrice();
		$total = 0.00;
		$tax = 0.00;
		$subtotal = 0.00;
		$total_loyalty_points = 0;
		$total_loyalty_dollars = 0;

		// All item totals are calculated in this method by default
		$items = $this->get_items($selected_only);
		// Loop through items and calculate tax and totals
		foreach($items as $line => $item){

            // Skip unselected items
			if($selected_only && empty($item['selected'])){
				continue;
			}

			if(!empty($item['sides_subtotal'])){
				$subtotal += $item['sides_subtotal'];
			}
			if(!empty($item['sides_tax'])){
				$tax += $item['sides_tax'];
			}
			if(!empty($item['sides_total'])){
				$total += $item['sides_total'];
			}

			// these are not set for some cases???
            $total += $item['total'] + $item['service_fees_total'];
            $subtotal += $item['subtotal'] + $item['service_fees_subtotal'];
            $tax += $item['tax'] + $item['service_fees_tax'];
            
            if(!empty($item['loyalty_cost'])){
            	$total_loyalty_points += $item['loyalty_cost'];
            	$total_loyalty_dollars += $item['total'];
            }
		}

		return array(
			'total' => (float) round($total, 2),
			'subtotal' => (float) round($subtotal, 2),
			'tax' => (float) round($tax, 2),
			'total_loyalty_points' => (float) round($total_loyalty_points, 2),
			'total_loyalty_dollars' => (float) round($total_loyalty_dollars, 2)
		);
	}

	function get_totals($selected = true,$taxable = 1){

		$item_totals = $this->get_item_totals($selected,$taxable);
		$payment_total = $this->get_payment_total();

		$item_totals['total_due'] = round($item_totals['total'] - $payment_total, 2);	
		$item_totals['total'] += $this->credit_card_fees;
		$item_totals['total_due'] += $this->credit_card_fees;
		$item_totals['total_paid'] = $payment_total;
		
		return $item_totals;
	}

	// Check if the selected items in the cart have been paid for
	function is_paid(){

		$cart = $this->get_cart();
        $totals = $this->get_totals(true,$cart['taxable']);
        if($cart['taxable']===false && $totals['total_due']===$totals['tax']){
            $totals['total_due']=0;
        }
        
		if(($cart['mode'] == 'sale' && $totals['total_due'] <= 0) ||
			($cart['mode'] == 'return' && $totals['total_due'] >= 0)){
			return true;
		}
		return false;
	}

	// Get all items, customers, and payments currently in cart
	function all(){

		$cart = $this->get_cart();
		$cart['items'] = $this->get_items();
		$cart['customers'] = $this->get_customers();
		$cart['payments'] = $this->get_payments();
		$cart['sale_payments'] = array();

		if(!empty($cart['return_sale_id'])){
			$cart['sale_payments'] = array_values($this->get_sale_payments((int) $cart['return_sale_id']));
		}

		return $cart;
	}

	function calculate_item_totals(&$item, $taxable, $include_tax = null){
		
		if(!empty($item['params']) && !is_array($item['params'])){
			$item['params'] = json_decode($item['params'], true);
		}

		if(!isset($item['quantity'])||$item['quantity']*1==0){
			$item['quantity'] = 1;
		}
		$item['quantity_purchased'] = $item['quantity'];

		if(empty($item['discount_percent'])){
			$item['discount_percent'] = 0;
		}
		if(empty($item['selected'])){
			$item['selected'] = false;
		}
		if(empty($item['params'])){
			$item['params'] = null;
		}
		if(empty($item['cost_price'])){
			$item['cost_price'] = 0.00;
		}	
		if(!isset($item['force_tax'])){
			$item['force_tax'] = null;
		}

		if($include_tax === null){
			$include_tax = (bool) $this->config->item('unit_price_includes_tax');
		}
		
		if(isset($item['unit_price_includes_tax'])){
			$include_tax = (bool) $item['unit_price_includes_tax'];
		}
		
		if(isset($item['price']) && !isset($item['unit_price'])){
			$item['unit_price'] = $item['price'];
		}

		$force_tax = null;
		if((int) $item['force_tax'] === 1){
			$force_tax = true;
		}else if($item['force_tax'] !== null && $item['force_tax'] == 0){
			$force_tax = false;
		}

		$item['modifier_total'] = 0;
		if(!empty($item['modifiers'])){
			foreach($item['modifiers'] as $modifier){
				$item['modifier_total'] += (float) $modifier['selected_price'];
			}
		}

		$item['comp_total'] = 0;
		$item['subtotal'] = $this->cart_lib->calculate_subtotal(
			$item['unit_price'] + $item['modifier_total'], $item['quantity'], 
			$item['discount_percent']
		);
		$item['non_discount_subtotal'] = $this->cart_lib->calculate_subtotal(
			$item['unit_price'] + $item['modifier_total'], 
			$item['quantity'], 0);
		$item['tax'] = 0;
		
        if(!empty($item['comp']) && !empty($item['comp']['amount'])){
            $comp_percent = $item['comp']['amount'] / 100;
            $comp_discount = round($item['subtotal'] * $comp_percent, 2);
            $item['subtotal'] -= $comp_discount;
            $item['comp_total'] = 0;
        }

		$item['service_fees_subtotal'] = 0;
		$item['service_fees_tax'] = 0;
		$item['service_fees_total'] = 0;

		// If item has sub items
		if(isset($item['item_type']) && ($item['item_type'] == 'tournament' || $item['item_type'] == 'item_kit' || $item['item_type'] == 'punch_card')){
			
			if($item['item_type'] == 'item_kit' || $item['item_type'] == 'punch_card'){
				$item['subtotal'] = 0;
				$item['non_discount_subtotal'] = 0;
				$item['unit_price'] = 0;
			}

			if($item['item_type'] == 'tournament'){
				$include_tax = true;
			}
			$item['taxes'] = array();
			
			$overrides = [];
			if(!empty($item['params']['sub_item_overrides'])){
				$overrides = $item['params']['sub_item_overrides'];
			}

			// Calculate totals of sub items
			$sub_taxes = array();
			if(!empty($item['items'])){
				
				foreach($item['items'] as $key => &$sub_item){

					if(isset($overrides[$key])){
						$sub_item['unit_price'] = (float) $overrides[$key];
					}
					$orig_unit_price = $sub_item['unit_price'];

					// Add up sub-items to get parent item totals
					if($item['item_type'] == 'item_kit' || $item['item_type'] == 'punch_card'){
						$sub_item['discount_percent'] = $item['discount_percent'];
						$sub_item['quantity'] = round($sub_item['base_quantity'] * $item['quantity'], 2);
						$this->calculate_item_totals($sub_item, $taxable, $include_tax);

						$item['subtotal'] += $sub_item['subtotal'];
						$item['non_discount_subtotal'] += $sub_item['non_discount_subtotal'];
						$item['tax'] += $sub_item['tax'];
						$item['unit_price'] += $this->cart_lib->calculate_subtotal($sub_item['unit_price'], $sub_item['base_quantity'], 0);
						$item['service_fees_subtotal'] += $sub_item['service_fees_subtotal'];
						$item['service_fees_tax'] += $sub_item['service_fees_tax'];
						$item['service_fees_total'] += $sub_item['service_fees_total'];
					// If item is a tournament
					}else{
						// We need the original tax amount would be, to subtract from tournament subtotal
						$this->calculate_item_totals($sub_item, $taxable, $include_tax);
						$item['tax'] += $this->cart_lib->calculate_item_tax($orig_unit_price, $sub_item['item_id'], 'item');
					}

					$sub_taxes = array_merge($sub_taxes, $sub_item['taxes']);
				}
			}
			// Apply combined taxes of all sub items to parent item
			$item['taxes'] = $sub_taxes;
			
		}else{
            
            $item['tax'] = $this->cart_lib->calculate_tax($item['subtotal'], $item['taxes'], $include_tax);
		}
		
		// For item kits and puch cards, the total is made up of all the sub-items 
		// so tax included is already accounted for
		if($include_tax && isset($item['item_type']) && $item['item_type'] != 'item_kit' && $item['item_type'] != 'punch_card'){
			$item['subtotal'] -= $item['tax'];
			$item['non_discount_subtotal'] -= $item['tax'];
		}
		
		if((!$taxable && $force_tax!==true)||$force_tax===false){
			$item['tax'] = 0;
		}

        if(!empty($item['service_fees'])){
            
            foreach($item['service_fees'] as &$fee){
                
                $ServiceFee = new \fu\service_fees\ServiceFee($fee['service_fee_id']);
                $ServiceFee->setParentItem($item);
                $calculated_service_fee = $ServiceFee->calculatePriceForSubtotal($item['subtotal'], $taxable,1,$item['quantity_purchased']);
                
                $item['service_fees_subtotal'] += $calculated_service_fee['subtotal'];
                $item['service_fees_tax'] += $calculated_service_fee['tax'];
                $item['service_fees_total'] += $calculated_service_fee['total'];

                $fee['unit_price'] = $fee['item_unit_price'] = $calculated_service_fee['unit_price'];
                $fee['quantity'] = $calculated_service_fee['quantity'];
                $fee['subtotal'] = $calculated_service_fee['subtotal'];
                $fee['tax'] = $calculated_service_fee['tax'];
                $fee['total'] = $calculated_service_fee['total'];
                $fee['taxes'] = $calculated_service_fee['taxes'];
                $fee['taxable'] = $calculated_service_fee['taxable'];
                $fee['profit'] = $calculated_service_fee['profit'];
            }
        }
		
		$item['discount_amount'] = (float) $item['non_discount_subtotal'] - (float) $item['subtotal'];
		$item['discount'] = (float) $item['discount_percent'];
		$item['discount_percent'] = (float) $item['discount_percent'];
		$item['total'] = (float) round($item['subtotal'] + $item['tax'], 2);
		$item['total_cost'] = $this->cart_lib->calculate_subtotal($item['cost_price'], $item['quantity'], 0);
		$item['item_cost_price'] = (float) $item['cost_price'];
		$item['item_unit_price'] = (float) $item['unit_price'];
		$item['profit'] = (float) round($item['subtotal'] - $item['total_cost'], 2);
		$item['selected'] = (bool) $item['selected'];
		$item['taxable'] = $taxable;
		
		// If item is an F&B item, calculate the prices of sides, soups and salads
		if(!empty($item['sides']) || !empty($item['soups']) || !empty($item['salads'])){

			$item['sides_subtotal'] = 0;
			$item['sides_tax'] = 0;
			$item['sides_total'] = 0;

			$side_types = array('sides', 'soups', 'salads');
			
			// Loop through each type of side
			foreach($side_types as $side_type){
				
				// If item doesn't have any sides, skip it
				if(empty($item[$side_type])){
					continue;
				}
				
				// Loop through each side, calculate price
				foreach($item[$side_type] as &$side){
					$side['quantity'] = $item['quantity'];
					$side['discount_percent'] = $item['discount_percent'];

					$this->calculate_item_totals($side, $taxable, $include_tax);
					$item['sides_subtotal'] += $side['subtotal'];
					$item['sides_tax'] += $side['tax'];
					$item['sides_total'] += $side['total'];
				}
			}
		}

		if(!empty($item['timeframe_id'])){
			$item['timeframe_id'] = (int) $item['timeframe_id'];
		}

		if($this->config->item('use_loyalty')){
			
			if(!empty($item['loyalty_dollars_per_point']) != 0){
				$item['loyalty_cost'] = (float) round($item['total'] / $item['loyalty_dollars_per_point'], 2);		
			}else{
				$item['loyalty_cost'] = 0.0;
			}
			
			if(!empty($item['loyalty_points_per_dollar'])){
				$item['loyalty_reward'] = (float) round($item['subtotal'] * $item['loyalty_points_per_dollar'], 2);
			}else{
				$item['loyalty_reward'] = 0;
			}	
		}

		$member_account_nickname = 'Member Balance';
		$customer_account_nickname = 'Customer Credit';
		if($this->config->item('member_balance_nickname')){
			$member_account_nickname = $this->config->item('member_balance_nickname');
		}
		if($this->config->item('customer_credit_nickname')){
			$customer_account_nickname = $this->config->item('customer_credit_nickname');
		}
        if(!isset($item['item_type'])){
            //pass
        }else if($item['item_type'] == 'invoice'){
			$item['name'] = 'Invoice #'.$item['params']['invoice_number'];
        }else if($item['item_type'] == 'statement'){
            $item['name'] = 'Statement #'.$item['params']['number'];
		}else if($item['item_type'] == 'member_account'){
			$item['name'] = $member_account_nickname;
		}else if($item['item_type'] == 'customer_account'){
			$item['name'] = $customer_account_nickname;
		}else if($item['item_type'] == 'invoice_account'){
			$item['name'] = 'Open Invoices';
		}

		$item['price'] = $item['unit_price'];
		return true;
	}

	// Retrieves all items in a cart (or multiple carts)
	function get_items($selected = false, $params = array()){

		$this->item_details = array();
		$sql_params = array('cart.course_id' => (int) $this->session->userdata('course_id'));

		if(isset($params['status'])){
			$sql_params['cart.status'] = $params['status'];
		}

		if(!empty($params['cart_id'])){
			$sql_params['cart.cart_id'] = $params['cart_id'];
		}

		if(empty($params)){
			$sql_params['cart.cart_id'] = $this->cart_id;
		}

		if($selected){
			$sql_params['item.selected'] = 1;
		}

		// Get all cart items, including any modifiers or sides attached
		// to each item
		$this->db->select("item.*, cart.mode AS cart_mode, GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				side.item_id,
				side.type,
				side.position,
				side_override.upgrade_price
			) SEPARATOR 0x1E) AS sides,
			GROUP_CONCAT(DISTINCT CONCAT_WS(
				0x1F,
				modifier.modifier_id,
				modifier.option
			) SEPARATOR 0x1E) AS modifiers", false);
		$this->db->from('pos_cart AS cart');
        $this->db->join('pos_cart_items AS item', 'item.cart_id = cart.cart_id', 'inner');
		$this->db->join('pos_cart_item_sides AS side', 'side.cart_line = item.line AND side.cart_id = item.cart_id', 'left');
		$this->db->join('item_sides AS side_override', 'item.item_id = side_override.item_id AND side.item_id = side_override.side_id', 'left');
		$this->db->join('pos_cart_item_modifiers AS modifier', 'modifier.line = item.line AND modifier.cart_id = item.cart_id', 'left');
		$this->db->where($sql_params);
		$this->db->group_by('cart.cart_id, item.line');

		$cart_item_rows = $this->db->get()->result_array();
        unset($sql_params['item.selected']);

		// Get all item side modifiers
		$this->db->select('side_modifier.cart_id, side_modifier.modifier_id, side_modifier.line,
			side_modifier.side_type, side_modifier.side_position,
			side_modifier.option AS selected_option');
		$this->db->from('pos_cart_item_side_modifiers AS side_modifier');
		$this->db->join('pos_cart AS cart', 'cart.cart_id = side_modifier.cart_id', 'inner');
		$this->db->where($sql_params);

		$side_modifiers = $this->db->get()->result_array();
		$side_modifier_details = array();

		// Place item side modifiers into an array for later use
		foreach($side_modifiers	as $side_modifier){
			$key = $side_modifier['cart_id'].'_'.$side_modifier['line'].'_'.$side_modifier['side_type'].'_'.$side_modifier['side_position'];
			$side_modifier_details[$key][$side_modifier['modifier_id']] = $side_modifier;
		}
		
		$item_ids = array();
		$item_kit_ids = array();
		$tournament_ids = array();
		
		// Loop through items in cart, store item and tournament IDs lists
		foreach($cart_item_rows as &$cart_item_row){

			if($cart_item_row['item_type'] == 'item_kit' || $cart_item_row['item_type'] == 'punch_card'){
				$item_kit_ids[] = (int) $cart_item_row['item_id'];
			
			}else if($cart_item_row['item_type'] == 'tournament'){
				$tournament_ids[] = (int) $cart_item_row['tournament_id'];
				$cart_item_row['item_id'] = (int) $cart_item_row['tournament_id'];
			
			}else{
				$item_ids[] = (int) $cart_item_row['item_id'];
			}

			if(!empty($cart_item_row['modifiers'])){
				$modifiers = $this->get_item_modifiers_array($cart_item_row['modifiers']);
				$cart_item_row['modifiers'] = $modifiers;
			}

			if(!empty($cart_item_row['sides'])){
				$sides = $this->get_item_sides_array($cart_item_row['sides']);
				$cart_item_row['sides'] = $sides;

				foreach($sides as $side){
					$item_ids[] = (int) $side['item_id'];
				}
			}
		}
		$item_ids = array_unique($item_ids);
		$item_kit_ids = array_unique($item_kit_ids);
		$tournament_ids = array_unique($tournament_ids);

		// If there are no items found, exit function
		if(empty($item_ids) && empty($item_kit_ids) && empty($tournament_ids)){
			return array();
		}

		$this->load->model('v2/Item_model');
		$this->load->model('v2/Item_kit_model');
		$this->load->model('v2/Tournament_model');
		$rows = array();
		$items = array();
		$item_kits = array();
		$tournaments = array();

		// Get details of items, item kits and tournaments using ID lists
		if(!empty($item_ids)){
			$items = $this->Item_model->get(array('item_id' => $item_ids, 'include_inactive' => true));
		}
		if(!empty($item_kit_ids)){
			$item_kits = $this->Item_kit_model->get(array('item_id' => $item_kit_ids));
		}
		if(!empty($tournament_ids)){
			$tournaments = $this->Tournament_model->get(array('tournament_id' => $tournament_ids));
		}
		$rows = array_merge($items, $item_kits, $tournaments);

		// Load item details into an array by item ID
		foreach($rows as $item){
			if(!empty($item['tournament_id'])){
				$item['item_id'] = $item['tournament_id'];
			}
			$this->item_details[$item['item_type'].'_'.$item['item_id']] = $item;
        }

		// Merge item details (name, category, etc) into cart items
		foreach($cart_item_rows as $cart_item){
			
			$line = (int) $cart_item['line'];
			$index = $cart_item['item_type'].'_'.$cart_item['item_id'];
            if(isset($this->item_details[$index])){
            	$details = $this->item_details[$index];
            }
			else {
				$details = array();
			}

			$item_sides = $cart_item['sides'];
			$selected_modifiers = $cart_item['modifiers'];
			unset($cart_item['sides'], $cart_item['modifiers']);

			// Merge details, cart item details take precendence (overriding price, etc)

            $item = array_merge($details, $cart_item);

			if($cart_item['cart_mode'] == 'return'){
				$item['service_fees'] = [];
			}
			unset($cart_item['cart_mode']);
			
			// Remove tournament sub-items if tournament is from a
			// previous sale (doing a return). Sub items are broken out into
			// their own cart lines, so we don't want to double charge
			if($item['item_type'] == 'tournament'){
				if($item['unit_price'] == $item['pot_fee']){
					unset($item['items']);
				}
			}

			// If F&B item, attach sides/modifiers to it
			if($item['item_type'] == 'item' && !empty($item['food_and_beverage']) && $item['food_and_beverage'] == 1){

				$item['price'] = $item['unit_price'];
				$side_details = array();
				
				if(!empty($item_sides)){
					foreach($item_sides as $side){
						$side_details = $this->item_details['item_'.$side['item_id']];
						$side_details = array_merge($side_details, $side);

						$modifier_key = $cart_item['cart_id'].'_'.$cart_item['line'].'_'.$side['type'].'_'.$side['position'];
						
						$item_side_modifiers = array();
						if(!empty($side_modifier_details[$modifier_key])){
							$item_side_modifiers = $side_modifier_details[$modifier_key];
						}
						
						$this->merge_selected_modifiers($side_details['modifiers'], $item_side_modifiers);
						if(!is_array($item[$side['type']])){
							$item[$side['type']] = [];
						}
						$item[$side['type']][] = $side_details;
					}
				}
			}

			$this->merge_selected_modifiers($item['modifiers'], $selected_modifiers);


			//If item is a green fee, we need to check if there a green fee for the specific price class
			if($item['item_type'] == "green_fee" OR $item['item_type'] == "cart_fee"){
				$this->Item_model->get_loyalty_points($item);
			}

			// Calculate item subtotal, tax and total
			$this->calculate_item_totals($item, (bool) $this->get_taxable());
			
			if($item['item_type'] == 'giftcard' && !empty($item['params']['giftcard_number'])){
				$item['serialnumber'] = $item['params']['giftcard_number'];
			
			}else if($item['item_type'] == 'punch_card' && !empty($item['params']['punch_card_number'])){
				$item['description'] = $item['params']['punch_card_number'];
			}



			// If retrieving multiple carts, key items by cart_id
			if(empty($sql_params['cart.cart_id'])){
				$item_details[(int) $cart_item['cart_id']][(int) $line] = $item;
			}else{
				$item_details[(int) $line] = $item;
			}



		}

        if(!empty($sql_params['cart.cart_id'])){
			return array_values($item_details);
		}

		return $item_details;
	}
	
	private function group_cart_taxes($taxes){
		
		if(empty($taxes)){
			return false;
		}
		
		$grouped = array();
		foreach($taxes as &$tax){
			
			$key = $tax['name'].' '.$tax['percent'];
			
			if(isset($grouped[$key]['amount'])){
				$grouped[$key]['amount'] += $tax['amount'];
			}else{
				$grouped[$key] = $tax;
			}
		}
		
		return array_values($grouped);
	}
	
	// Marks which modifiers on an item were "selected" by the user
	private function merge_selected_modifiers(&$item_modifiers, $selected_modifiers){

		if(empty($item_modifiers)){
			return array();
		}
		
		foreach($item_modifiers as &$modifier){
			
			if(!isset($selected_modifiers[$modifier['modifier_id']])){
				continue;
			}
			$modifier['selected_option'] = $selected_modifiers[$modifier['modifier_id']]['selected_option'];
			
			if($modifier['multi_select'] == 1 && !is_array($modifier['selected_option'])){
				$modifier['selected_option'] = json_decode($modifier['selected_option'], true);
			}
			
			$modifier['selected_price'] = $this->Item_model->get_modifier_price($modifier);
		}
	}

	private function get_item_sides_array($sides){
		
		$sides = explode("\x1E", $sides);
		$side_list = array();
		
		// Loop through each item row, break apart fields
		foreach($sides as $side_row){
			$side_array = explode("\x1F", $side_row);
			$side = array(
				'item_id' => (int) $side_array[0],
				'type' => $side_array[1],
				'position' => (int) $side_array[2]
			);
			
			if(isset($side_array[3])){
				$side['unit_price'] = (float) $side_array[3];
			}
			$side_list[] = $side;
		}

		return $side_list;
	}

	private function get_item_modifiers_array($modifiers){
		$modifiers = explode("\x1E", $modifiers);
		$modifier_array = array();

		// Loop through each item row, break apart fields
		foreach($modifiers as &$modifier_row){
			$modifier = explode("\x1F", $modifier_row);
			
			if(empty($modifier[0])){
				continue;
			}
			if(empty($modifier[1])){
				$modifier[1] = false;
			}

			$modifier = array(
				'modifier_id' => (int) $modifier[0],
				'selected_option' => $modifier[1],
			);
			$modifier_array[$modifier['modifier_id']] = $modifier;
		}

		return $modifier_array;
	}

	// Add/update an item in the cart
	function save_item($line = false, $item_data = array()){
		
		$item_data = elements(array(
			'item_id',
			'item_type',
			'selected',
			'unit_price',
			'discount_percent',
			'quantity',
			'timeframe_id',
			'special_id',
			'punch_card_id',
			'tournament_id',
			'price_class_id',
			'params',
			'invoice_id',
			'modifiers',
			'sides',
			'soups',
			'salads',
			'item_number',
			'unit_price_includes_tax'
		), $item_data, false);
		if(empty($item_data['item_type'])){
			$item_data['item_type'] = "item";
		}
		$item_data['item_id'] = (int) $item_data['item_id'];
		$item_data['selected'] = (bool) $item_data['selected'];
		$item_data['unit_price_includes_tax'] = (int) $item_data['unit_price_includes_tax'];
		
		$modifiers = $item_data['modifiers'];

		// Gather up all side types and place them into a single array
		// with type of side defined as a column
		$sides = array_merge(
			$this->apply_side_type($item_data['soups'], 'soups'),
			$this->apply_side_type($item_data['salads'], 'salads'),
			$this->apply_side_type($item_data['sides'], 'sides')
		);

		unset($item_data['modifiers'], $item_data['sides'], $item_data['salads'], $item_data['soups']);

		if(!empty($item_data['params'])){
			$item_data['params'] = json_encode($item_data['params']);
		}else{
			$item_data['params'] = '';
		}

		$lock = new \fu\locks\cart([
			"cart_id"=>$this->cart_id
		]);
		$lock->get_lock();
		if($line){
			$line = (int) $line;
		}else{
			$query = $this->db->query("
				SELECT IFNULL(MAX(item_row.line), 0) AS line_number 
				FROM foreup_pos_cart_items AS item_row
				WHERE item_row.cart_id = '.$this->cart_id.'
			");
			$result = $query->row_array();
			$line = $result['line_number']+1;
		}
        // Get item_id if we're paying off an account
        if ($item_data['item_type'] == 'invoice_account' || $item_data['item_type'] == 'member_account' || $item_data['item_type'] == 'customer_account') {
            $item_id = $this->Sale->get_balance_item(str_replace('account', 'balance',$item_data['item_type']));
            $item_data['item_id'] = $item_id;
        }

		$item_data['item_id'] = (int) $item_data['item_id'];
		$item_data['quantity'] = (float) $item_data['quantity'];
		$item_data['unit_price'] = (float) $item_data['unit_price'];
		$item_data['discount_percent'] = (float) $item_data['discount_percent'];
		$item_data['price_class_id'] = (int) $item_data['price_class_id'];
		$item_data['timeframe_id'] = (int) $item_data['timeframe_id'];
		$item_data['special_id'] = (int) $item_data['special_id'];
		$item_data['punch_card_id'] = (int) $item_data['punch_card_id'];
		$item_data['tournament_id'] = (int) $item_data['tournament_id'];
		$item_data['punch_card_id'] = (int) $item_data['punch_card_id'];
		$item_data['invoice_id'] = (int) $item_data['invoice_id'];
		$item_data['selected'] = (int) (bool) $item_data['selected'];
		$item_data['unit_price_includes_tax'] = (int) $item_data['unit_price_includes_tax'];

		$this->db->query("INSERT INTO foreup_pos_cart_items
			(cart_id, line, item_id, quantity, unit_price, discount_percent, item_type,
			item_number, price_class_id, timeframe_id, special_id, params,
			punch_card_id, tournament_id, invoice_id, selected, unit_price_includes_tax)
			
			VALUES
				({$this->cart_id}, {$line}, {$item_data['item_id']}, {$item_data['quantity']}, {$item_data['unit_price']}, {$item_data['discount_percent']},
				{$this->db->escape($item_data['item_type'])}, {$this->db->escape($item_data['item_number'])},
				{$item_data['price_class_id']}, {$item_data['timeframe_id']}, {$item_data['special_id']}, {$this->db->escape($item_data['params'])},
				{$item_data['punch_card_id']}, {$item_data['tournament_id']}, {$item_data['invoice_id']}, {$item_data['selected']}, 
				{$item_data['unit_price_includes_tax']})
			
			ON DUPLICATE KEY UPDATE
				item_id = VALUES(item_id),
				quantity = VALUES(quantity),
				unit_price = VALUES(unit_price),
				discount_percent = VALUES(discount_percent),
				item_type = VALUES(item_type),
				item_number = VALUES(item_number),
				price_class_id = VALUES(price_class_id),
				timeframe_id = VALUES(timeframe_id),
				special_id = VALUES(special_id),
				params = VALUES(params),
				punch_card_id = VALUES(punch_card_id),
				tournament_id = VALUES(tournament_id),
				invoice_id = VALUES(invoice_id),
				selected = VALUES(selected),
				unit_price_includes_tax = VALUES(unit_price_includes_tax)");



		$this->save_item_modifiers($line, $modifiers);
		$this->save_item_sides($line, $sides);
		
		$this->update_activity();
		$this->recalculate_payments();

		$lock->release_lock();
		return (int) $line;
	}

	function save_item_modifiers($item_line, $modifiers = array()){

		$this->db->delete('pos_cart_item_modifiers', array('cart_id' => $this->cart_id, 'line' => $item_line));

		// Insert any modifiers
		if(empty($modifiers)){
			return true;
		}

		$modifierInsertSql = "INSERT IGNORE INTO foreup_pos_cart_item_modifiers
			(`cart_id`, `line`, `modifier_id`, `option`) VALUES ";

		foreach($modifiers as $modifier){
			
			if(isset($modifier['selected_option']) && is_array($modifier['selected_option'])){
				$modifier['selected_option'] = json_encode($modifier['selected_option']);
			}
			
			$modifierData = array();
			$modifierData['cart_id'] = (int) $this->cart_id;
			$modifierData['line'] = (int) $item_line;
			$modifierData['modifier_id'] = (int) $modifier['modifier_id'];
			$modifierData['option'] = $this->db->escape($modifier['selected_option']);

			$modifierInsertSql .= "({$modifierData['cart_id']}, {$modifierData['line']}, {$modifierData['modifier_id']}, {$modifierData['option']}),";
		}
		$modifierInsertSql = trim($modifierInsertSql, ',');
		$this->db->query($modifierInsertSql);

		return true;
	}

	function save_item_sides($item_line, $sides = array()){

		$item_line = (int) $item_line;

		$this->db->query("DELETE side.*, side_modifier.*
			FROM foreup_pos_cart_item_sides AS side
			LEFT JOIN foreup_pos_cart_item_side_modifiers AS side_modifier
				ON side_modifier.line = side.cart_line
				AND side_modifier.cart_id = side.cart_id
			WHERE side.cart_id = {$this->cart_id}
				AND side.cart_line = {$item_line}");

		$side_data = array();
		$side_modifiers = array();

		if(empty($sides)){
			return true;
		}

		// Loop through each side
		foreach($sides as $side){

			if(empty($side['item_id']) || empty($side['type'])){
				continue;
			}

			// Build array of data to insert into database
			$side_data[] = array(
				'cart_id' => (int) $this->cart_id,
				'item_id' => (int) $side['item_id'],
				'cart_line' => (int) $item_line,
				'position' => (int) $side['position'],
				'type' => $side['type'],
				'date_created' => date('Y-m-d h:i:s')
			);

			// If side has modifiers, build array of modifiers to insert
			if(empty($side['modifiers'])){
				continue;
			}

			foreach($side['modifiers'] as $key => $modifier){
				
				if(!empty($modifier['selected_option']) && is_array($modifier['selected_option'])){
					$modifier['selected_option'] = json_encode($modifier['selected_option']);
				}
				$modifierData = array();
				$modifierData['cart_id'] = $this->cart_id;
				$modifierData['line'] = $item_line;
				$modifierData['modifier_id'] = $modifier['modifier_id'];
				$modifierData['option'] = $modifier['selected_option'];
				$modifierData['side_position'] = (int) $side['position'];
				$modifierData['side_type'] = $side['type'];

				$side_modifiers[] = $modifierData;
			}
		}

		if(!empty($side_data)){
			$this->db->insert_batch('pos_cart_item_sides', $side_data);
		}
		if(!empty($side_modifiers)){
			$this->db->insert_batch('pos_cart_item_side_modifiers', $side_modifiers);
		}

		return true;
	}

	function apply_side_type(&$sides, $type){
		if(!isset($sides) || empty($sides)){
			return array();
		}

		$sidesWithType = array();
		foreach($sides as $key => $side){
			$side['type'] = $type;
			$sidesWithType[] = $side;
		}

		return $sidesWithType;
	}

	// Deletes an item from the cart
	function delete_item($line){

		$line = (int) $line;
		$this->update_activity();

		$this->db->query("DELETE item.*, side.*, modifier.*, side_modifier.*
			FROM foreup_pos_cart_items AS item
			LEFT JOIN foreup_pos_cart_item_modifiers AS modifier
				ON modifier.line = item.line
				AND modifier.cart_id = item.cart_id
			LEFT JOIN foreup_pos_cart_item_sides AS side
				ON side.cart_line = item.line
				AND side.cart_id = item.cart_id
			LEFT JOIN foreup_pos_cart_item_side_modifiers AS side_modifier
				ON side_modifier.line = item.line
				AND side_modifier.cart_id = item.cart_id
			WHERE item.cart_id = {$this->cart_id}
				AND item.line = {$line}");

		$this->recalculate_payments();

		return $this->db->affected_rows();
	}

	// Get all customers attached to cart
	function get_customers($selected = false){

		$this->customer_details = array();

		$params = array('cart_id' => $this->cart_id);
		if($selected){
			$params['selected'] = 1;
		}
		$query = $this->db->get_where('pos_cart_customers', $params);
		$customers = $query->result_array();

		// Loop through items in cart, store item IDs in an array
		$customer_ids = array();
		foreach($customers as $customer){
			$customer_ids[] = (int) $customer['customer_id'];
		}
		$customer_ids = array_unique($customer_ids);

		if(empty($customer_ids)){
			return array();
		}

		// Get details of all customers attached to cart
		$this->load->model('v2/Customer_model');
		$rows = $this->Customer_model->get(array('person_id' => $customer_ids));

		// Load customer details into an array by person ID
		foreach($rows as $row){
			$this->customer_details[ (int) $row['person_id'] ] = $row;
		}

		// Merge customer details into cart customers
		$customer_details = array();
		foreach($customers as $customer){

			$details = $this->customer_details[ (int) $customer['customer_id'] ];
			if(empty($details)){
				$details = array();
			}
			$customer['selected'] = (bool) $customer['selected'];
			$customer_details[ (int) $customer['customer_id'] ] = array_merge($details, $customer);
		}

		return array_values($customer_details);
	}
	
	// Attaches a customer to the cart
	function save_customer($customer_id, $customer_data){

		if(empty($customer_id)){
			return false;
		}

		$customer_data = elements(array('selected', 'taxable', 'discount'), $customer_data, null);
		$customer_data['selected'] = (int) (bool) $customer_data['selected'];
		$customer_data['person_id'] = (int) $customer_id;

        if(!empty($customer_data['selected'])){
            $this->db->where('cart_id', $this->cart_id);
            $this->db->update('pos_cart_customers', array('selected' => 0));

            $this->db->where('cart_id', $this->cart_id);
            $this->db->where('customer_id', $customer_id);
            $this->db->update('pos_cart_customers', array('selected' => 1));
        }

		$query = $this->db->query("INSERT IGNORE INTO foreup_pos_cart_customers
			(cart_id, customer_id, selected)
			VALUES
			({$this->cart_id}, {$customer_data['person_id']}, {$customer_data['selected']})");

		$course_ids = [];
		$this->load->model('course');
		$this->course->get_linked_course_ids($course_ids);

		$customer = $this->db
			->select('taxable')
			->from('customers')
			->where_in("course_id",$course_ids)
			->where('person_id', (int) $customer_id)
			->get()->row_array();

		// Set cart to customer's taxable status
		if($customer_data['selected'] == 1){
			
			$taxable = (int) $customer['taxable'];
			
			// If a different taxable status is passed in, use it instead
			if($customer_data['taxable'] !== null){
				$taxable = (int) $customer_data['taxable'];
			}
			$this->save_cart(array('taxable' => $taxable));
		}

		$this->update_activity();

		return (int) $customer_id;
	}

	// Removes a customer from the cart
	function delete_customer($customer_id){
		$this->update_activity();
		$this->db->delete('pos_cart_customers', array(
			'customer_id' => $customer_id,
			'cart_id' => $this->cart_id
		));
		return $this->db->affected_rows();
	}

	function get_selected_customer(){

		$customers = $this->get_customers(true);

		if(!empty($customers[0])){
			return $customers[0];
		}else{
			return false;
		}
	}

	// Check if a payment type is valid
	function is_valid_type($type){
        $this->load->model('custom_payments');
		$type = strtolower($type);
		if(in_array($type, $this->valid_payment_types)){
			return $type;
		}else if(stripos($type, 'member_account') !== false){
			return 'member_account';
		}else if(stripos($type, 'customer_account') !== false){
			return 'customer_account';
		}else if(stripos($type, 'credit_card') !== false){
			return 'credit_card';
		}else if ($this->custom_payments->is_valid($type)) {
            return $type;
        }

		return false;
	}

	// If something is changed in cart, some payments may need recalculated
	// such as rainchecks or punch cards since they depend on certain items
	// in the cart to be valid
	function recalculate_payments(){

		$rows = $this->db->select('payment_id, amount, type,
			description, number, record_id')
			->from('pos_cart_payments')
			->where_in('type', array('raincheck', 'punch_card', 'coupon'))
			->where('cart_id', $this->cart_id)
			->get()->result_array();

		if(count($rows) == 0){
			return false;
		}

		foreach($rows as $row){
			$this->save_payment($row, false, true);
		}

		return true;
	}

	// When returning a previous sale, load the credit card payments from that
	// sale (if any), so the employee can return to the initial card used
	function get_sale_payments($sale_ids){
		$payments = array();
		if(empty($sale_ids)){
			return $payments;
		}
		
		$sale_payments = $this->sale_model->get_payments($sale_ids);
		if(empty($sale_payments)){
			return $payments;
		}
        $return = array();
		// We only want credit card payments
		foreach($sale_payments as $sale_payment){
			if($sale_payment['type'] != 'credit_card' || empty($sale_payment['record_id']) || $sale_payment['amount'] == 0){
				continue;
			}
			if(!isset($payments[(int) $sale_payment['sale_id']]))
                $payments[(int) $sale_payment['sale_id']]=array();
			$payments[(int) $sale_payment['sale_id']][] = $sale_payment;
		}
		
		return $payments;
	}

	function get_payments($payment_id = null){

		$this->load->model('v2/Raincheck_model');
		$this->load->model('v2/Punch_card_model');
		$this->load->model('Promotion');
		$this->load->model('Giftcard');

		if($payment_id){
			$this->db->where('payment_id', $payment_id);
		}

		$this->db->select('payment_id, amount, type, description, number, record_id, params');
		$query = $this->db->get_where('pos_cart_payments', array('cart_id' => $this->cart_id));
		$rows = $query->result_array();
		
		foreach($rows as &$row){

			$row['params'] = json_decode($row['params'], true);

			if($row['type'] == 'raincheck'){
				$raincheck = $this->Raincheck_model->get(array('raincheck_id' => (int) $row['record_id']));
				$row['raincheck'] = $raincheck[0];

			}else if($row['type'] == 'gift_card'){
				$gift_card = (array) $this->Giftcard->get_info((int) $row['record_id']);
				$row['gift_card'] = $gift_card;

			}else if($row['type'] == 'punch_card'){
				$punch_card = (array) $this->Punch_card_model->get(array('punch_card_id' => (int) $row['record_id']));
				$row['punch_card'] = $punch_card[0];

			}else if($row['type'] == 'coupon'){
				$promo = (array) $this->Promotion->get_info($row['record_id']);
				$row['promotion'] = $promo;
			}
		}

		if($payment_id && !empty($rows[0])){
			return $rows[0];
		}

		return $rows;
	}
	
	function add_credit_card_fee(&$payment){
		$fee_percentage = round($this->config->item('credit_card_fee') / 100, 5);
		$fee_amount = (float) round($payment['amount'] * $fee_percentage, 2);
		$payment['params'] = array('fee' => $fee_amount);
		$payment['amount'] += $fee_amount;		
	}

	function save_payment($payment, $merchant_data = false, $replace = false){
		
		if(empty($payment['type']) || $payment['type'] === false){
			$this->msg = "Missing payment type.";
			return false;
		}
		$this->load->model('Customer_credit_card');

		$type = $this->is_valid_type($payment['type']);
		if(!$type){
			$this->msg = "Invalid payment type.";
			return false;
		}
		$amount = (float) round($payment['amount'], 2);
		$record_id = 0;
		if(isset($payment['record_id']))
			$record_id = (int) $payment['record_id'];
		$signature_id = false;

		if(empty($payment['payment_id'])){
			$payment_id = null;
		}else{
			$payment_id = $payment['payment_id'];
		}
		
		if(empty($payment['number'])){
			$payment['number'] = '';
		}
		if(empty($payment['params'])){
			$payment['params'] = '';
		}		
		if(empty($payment['bypass'])){
			$payment['bypass'] = false;
		}
		if(empty($payment['description'])){
			$payment['description'] = '';
		}
		if(empty($payment['merchant_data'])){
			$payment['merchant_data'] = false;
		}
		if(empty($payment['tran_type'])){
			$payment['tran_type'] = '';
		}
		if(!empty($payment['merchant'])){
			$merchant = $payment['merchant'];
		}
		$payment = array(
			'payment_id' => $payment_id,
            'description' => $payment['description'],
			'amount' => $amount,
			'type' => $type,
			'record_id' => $record_id,
			'number' => $payment['number'],
			'params' => $payment['params'],
            'bypass' => $payment['bypass'],
            'merchant_data' => $payment['merchant_data'],
			'tran_type' => $payment['tran_type'],
			'approved' => !empty($payment['approved']) ? $payment['approved'] : false,
			'verified' => !empty($payment['verified']) ? 1 : 0
		);

		switch($type) {

            case 'ets_gift_card':
            case 'credit_card':
            case 'credit_card_save':
                if ($amount < 0) {
                    $this->load->model('Payment_return');
                    $return_log_data = array(
                        'cart_id' => $this->cart_id,
                        'note' => 'cart_model->save_payment',
                        'invoice_id' => $payment['record_id'],
                        'employee_id' => $this->session->userdata('person_id')
                    );
                    $this->Payment_return->save($return_log_data);
                }

                if ($this->config->item('apriva_username') && (empty($merchant) || $merchant == "apriva")) {
                    $payment = $this->process_apriva_payment($payment, $merchant_data);

                // If course uses ETS
                }else if(!empty($merchant_data) && $this->config->item('ets_key') && (empty($merchant) || $merchant == "ets")){
					$payment = $this->process_ets_payment($payment, $merchant_data);
				
				// If course uses Mercury
				}else if(!empty($merchant_data) && $this->config->item('mercury_id')&& (empty($merchant) || $merchant == "mercury")){
					$payment = $this->process_mercury_payment($payment, $merchant_data);

				// Element payments
				}else if(!empty($merchant_data) && $this->config->item('element_account_id')&& (empty($merchant) || $merchant == "element")){
					$payment = $this->process_element_payment($payment, $merchant_data);				
				
				// If course uses standard credit card payment type
				}else{
					
					// Add fee to payment amount (if course has fee implemented)
					if($this->config->item('credit_card_fee') > 0 && $payment['amount'] > 0){
						$this->add_credit_card_fee($payment);
					}
					$payment['description'] = 'Credit Card';
				}
			break;

			case 'member_account':
			case 'customer_account':
				$payment = $this->process_account_payment($payment, $record_id);

				if($payment && $type == 'member_account' && !empty($payment['params']['signature'])){
					$payment['params']['signature_id'] = $this->cart_lib->save_signature($payment['params']['signature'], $payment, $this->cart_id);
					$s3 = new fu\aws\s3();
					$payment['params']['signature_url'] = $s3->client->getObjectUrl($s3->signatureBucket, $payment['params']['signature_id'], '+1 hour');
					unset($payment['params']['signature']);
				}
			break;

			case 'loyalty_points':
				$payment = $this->process_loyalty_payment($payment);
			break;

			case 'raincheck':
				$payment = $this->process_raincheck_payment($payment);
			break;

            case 'raincheck_issued':

            break;

			case 'gift_card':
				$payment = $this->process_giftcard_payment($payment);
			break;

			case 'punch_card':
				$payment = $this->process_punch_card_payment($payment);
			break;

			case 'coupon':
				$payment = $this->process_coupon_payment($payment);
			break;

			case 'check':
				$payment['description'] = 'Check';
				if(!empty($payment['number']) || $payment['number'] != ''){
					$payment['description'] .= ':'.trim($payment['number']);
				}
			break;

			default:
				$payment['description'] = $this->generate_payment_description($payment);
			break;
		}

		// If a payment failed to process for some reason, bail out
		if(!$payment){
			if(empty($this->msg)){
				$this->msg = "Payment failed to complete.";
			}

			return false;
		}

		if(empty($payment['payment_id'])){
			$payment['payment_id'] = 'NULL';
		}

		// Combine payments if a payment of that type already exists
		if(!$replace && $payment['type'] && $payment['type'] != 'raincheck'){

			$this->db->select('payment_id, amount, number, record_id');
			$this->db->from('pos_cart_payments');
			$this->db->where(array(
				'cart_id' => $this->cart_id, 
				'type' => $type, 
				'record_id' => $payment['record_id'], 
				'number' => $payment['number']
			));
			$query = $this->db->get();
			$row = $query->row_array();
			
			// If that specific credit card payment already exists in the cart,
			// return an error
			if($row && $row['record_id'] == $payment['record_id'] && $payment['type'] == 'credit_card'){
				$this->status = 409;
				$this->msg = "Credit card payment already exists.";
				return false;
			}
			
			if($row){
				if($payment['type'] != 'punch_card' && $payment['type'] != 'member_account' && $payment['type'] != 'customer_account'){
					$payment['amount'] += (float) $row['amount'];
				}
				$payment['payment_id'] = (int) $row['payment_id'];

				if($payment['type'] == 'loyalty_points'){
					$payment['number'] += (int) $row['number'];
				}
			}
		}

		$signature = false;
		if(!empty($payment['params']['signature'])){
			$signature = $payment['params']['signature'];
			unset($payment['params']['signature']);
		}

		$params = 'NULL';
		if(!empty($payment['params'])){
			$params = $this->db->escape(json_encode($payment['params']));
		}

		if(!isset($payment['record_id'])){
			$this->status = 500;
			$this->msg = "Invalid record id for the transaction";
			return false;
		}

		$this->update_activity();
		$this->db->query("INSERT INTO foreup_pos_cart_payments
			(payment_id, cart_id, amount, type, description, number, record_id, params) VALUES
			({$payment['payment_id']}, {$this->cart_id}, {$payment['amount']},
				{$this->db->escape($type)}, {$this->db->escape($payment['description'])},
				{$this->db->escape($payment['number'])}, {$payment['record_id']},
				{$params})
			ON DUPLICATE KEY UPDATE
				amount = VALUES(amount),
				number = VALUES(number),
				params = VALUES(params)");

		if(empty($payment['payment_id']) || $payment['payment_id'] == 'NULL'){
			$payment['payment_id'] = (int) $this->db->insert_id();
		}

		return $payment;
	}
	
	function update_credit_card_payment($invoice_id, $payment_id){
		$this->db->update('sales_payments_credit_cards', array('payment_id' => $payment_id), array('invoice' => (int) $invoice_id));
		return $this->db->affected_rows();
	}
	
	function credit_card_payment_exists($invoice_id){
		
		if(empty($invoice_id)){
			return false;
		}
		
		$cc_payment = $this->db->select('status')
			->from('sales_payments_credit_cards')
			->where('invoice', $invoice_id)
			->limit(1)
			->get()
			->row_array();
		
		if(!empty($cc_payment['status'])){
			return true;
		}
		
		return false;
	}
	
	function process_raincheck_payment($payment){

        $max_amount = $payment['amount'];
		$valid_cart_fees = 0;
		$valid_green_fees = 0;
		$valid_tax = 0;

		if(!$payment['number'] && !$payment['record_id']){
			return false;
		}

		$raincheck_params = array();
		if(!empty($payment['record_id'])){
			$raincheck_params['raincheck_id'] = (int) $payment['record_id'];
		}else if(!empty($payment['number'])){
			$raincheck_params['number'] = $payment['number'];
		}

		$this->load->model('v2/Raincheck_model');
		$rainchecks = $this->Raincheck_model->get($raincheck_params);
		
		if(empty($rainchecks) || empty($rainchecks[0])){
			$this->msg = 'Raincheck not found';
			return false;
		}
		$raincheck = $rainchecks[0];

		// If raincheck is expired
		if($raincheck['expiry_date'] != '0000-00-00 00:00:00' &&
			strtotime($raincheck['expiry_date']) < time()){
			$this->msg = 'Raincheck has expired';
			return false;
		}

		// If raincheck is already used
		if($raincheck['date_redeemed'] != '0000-00-00 00:00:00'){
			$this->msg = 'Raincheck has already been redeemed';
			return false;
		}

		// Get all items selected in cart
		$items = $this->get_items(true);

		// Loop through items and check if valid green fees or cart fees
		// exist in the cart
		foreach($items as $item){

			if($item['item_type'] == 'green_fee' && $raincheck['green_fee']){
				$valid_green_fees += $item['subtotal'];
				$valid_tax += $item['tax'];
			}

			if($item['item_type'] == 'cart_fee' && $raincheck['cart_fee']){
				$valid_cart_fees += $item['subtotal'];
				$valid_tax += $item['tax'];
			}
		}

		$green_total = min($raincheck['green_fee']['unit_price'] * $raincheck['players'], $valid_green_fees);
		$cart_total = min($raincheck['cart_fee']['unit_price'] * $raincheck['players'], $valid_cart_fees);
		$tax = min($raincheck['tax'], $valid_tax);

		$payment['amount'] = (float) $green_total + $cart_total + $tax;

		//SUP-723
		//The payment amount should never be larger than the raincheck total
		//Rainchecks aren't currently being saved correctly when tax is included, this means when the amount is generated it'll sometimes be larger than the raincheck total
		//Instead of fixing the root cause this patches it.  When the raincheck is saved to the database it should save the actual greenfee price without tax.
		//We have enough bad data though for the moment that a patch is needed asap.
		$payment['amount'] = min($payment['amount'],$raincheck['total']);

        $payment['amount'] = $payment['amount'] > $max_amount ? $max_amount : $payment['amount'];
		$payment['description'] = 'Raincheck '.$raincheck['raincheck_number'];
		$payment['raincheck'] = $raincheck;
		$payment['record_id'] = (int) $raincheck['raincheck_id'];

		return $payment;
	}

	function process_coupon_payment($payment){

		if(empty($payment['number']) && empty($payment['record_id'])){
			return false;
		}
		$valid_amount = 0;

		if(empty($payment['record_id'])){
			$promo_id = (int) substr($payment['number'], 3);
			$payment['record_id'] = $promo_id;
		}

		// Check if a coupon is already applied to this cart
		// only 1 coupon allowed per sale
		if(empty($payment['payment_id'])){
			$this->db->select('payment_id');
			$this->db->from('pos_cart_payments');
			$this->db->where('cart_id', $this->cart_id);
			$this->db->where('type', 'coupon');
			$existing_coupon = $this->db->get()->row_array();

			if(!empty($existing_coupon['payment_id'])){
				$this->msg = 'Only 1 coupon per cart allowed';
				return false;
			}
		}

		// Get coupon details
		$this->load->model('Promotion');
		$promotion = (array) $this->Promotion->get_info($payment['record_id']);

		if(empty($promotion['id'])){
			$this->msg = 'Promotion not found';
			return false;
		}

		if(!empty($promotion['expiration_date']) && $promotion['expiration_date'] != '0000-00-00'
			&& (strtotime($promotion['expiration_date']) + 86400) < time()){
			$this->msg = 'Promotion expired on '.date('n/j/Y', strtotime($promotion['expiration_date']));
			return false;
		}

		// Check that today is valid for promo
		$current_day = date('l');
		if($promotion[$current_day] != 1){
			$this->msg = 'Promotion does not apply today';
			return false;
		}

		// Check that time is valid
		if($promotion['valid_between_from'] == 'anytime'){
			$start_time = 0;
		}else{
			$start_time = (int) date('H', strtotime($promotion['valid_between_from']));
		}
		if($promotion['valid_between_to'] == 'anytime'){
			$end_time = 2400;
		}else{
			$end_time = (int) date('H', strtotime($promotion['valid_between_to']));
		}

		if($start_time > (int) date('H') || $end_time <= (int) date('H')){
			$this->msg = 'Promotion only valid from '.date('g:ia', strtotime($promotion['valid_between_from'])).' to '.date('g:ia', strtotime($promotion['valid_between_to']));
			return false;
		}

		// Get all items selected in cart
		$items = $this->get_items(true);
		$valid_cart_total = 0;
		$valid_item_quantity = 0;
		$valid_single_item_amount = 0;
		$amount = 0;

		// Loop through items and add up qualifying item totals
		foreach($items as $item){

			if(
				($item['category'] == $promotion['category_type'] && !empty($promotion['category_type'])) ||
				($item['department'] == $promotion['department_type'] && !empty($promotion['department_type'])) ||
				($item['subcategory'] == $promotion['subcategory_type'] && !empty($promotion['subcategory_type'])) ||
				$item['item_id'] == $promotion['item_id']
			){
				$valid_cart_total += $item['total'];
				$valid_item_quantity += $item['quantity'];

				// Calculate the total for a single item
				$subtotal = $this->cart_lib->calculate_subtotal($item['unit_price'], 1, $item['discount_percent']);
				$tax = $this->cart_lib->calculate_item_tax($subtotal, $item['item_id'], 'item');
				$item_total = $subtotal + $tax;

				if($item_total < $valid_single_item_amount || $valid_single_item_amount == 0){
					$valid_single_item_amount = $item_total;
				}
			}
		}

		// If promotion is just a discount
		if($promotion['bogo'] == 'discount'){

			if((float) $promotion['min_purchase'] <= $valid_cart_total){

				$discount_amount = (float) $promotion['amount'];
				if($promotion['amount_type'] == '%'){
					$percentage = (float) round($promotion['amount'] / 100, 5);
					$discount_amount = (float) round($valid_cart_total * $percentage, 2);
				}
				$amount = $discount_amount;
			}

		// If promotion is BOGO (Buy One Get One)
		}else if($promotion['bogo'] == 'bogo'){

			$discount_amount = 0;
			if((int) ($promotion['buy_quantity'] + $promotion['get_quantity']) <= $valid_item_quantity){

				if($promotion['amount_type'] == 'FREE'){
					$discount_amount = (float) round($promotion['get_quantity'] * $valid_single_item_amount, 2);
				}else{
					$percentage = (float) round($promotion['amount'] / 100, 5);
					$discounted_item_amount = round($percentage * $valid_single_item_amount, 2);
					$discount_amount = (float) round($promotion['get_quantity'] * $discounted_item_amount, 2);
				}
			}
			$amount = $discount_amount;
		}

		$payment['amount'] = (float) $amount;
		$payment['description'] = 'Coupon: '.$promotion['name'];
		$payment['record_id'] = (int) $payment['record_id'];
		$payment['promotion'] = $promotion;

		return $payment;
	}

	function process_punch_card_payment($payment){

		if(empty($payment['number']) && empty($payment['record_id'])){
			return false;
		}
		$valid_amount = 0;

		// Find punch card with the number given
		$this->load->model('v2/Punch_card_model');
		$punch_card = $this->Punch_card_model->get(array('punch_card_number' => $payment['number']));

		if(empty($punch_card[0])){
			$this->msg = 'Punch card not found';
			return false;
		}
		$punch_card = $punch_card[0];

		if(empty($punch_card['items'])){
			return false;
		}

		if($punch_card['expiration_date'] != '0000-00-00' && strtotime($punch_card['expiration_date']) < strtotime('midnight')){
			$this->msg = 'Punch card expired on '. date('m/d/Y', strtotime($punch_card['expiration_date']));
			return false;
		}

		$punches = array();
		foreach($punch_card['items'] as $item){
			$punches_left = $item['punches'] - $item['used'];

			if($punches_left > 0){
                $punch_item_id = empty($item['price_class_id']) ? $item['item_id'] : $item['item_id'].'_'.$item['price_class_id'];
				$punches[$punch_item_id]['punches'] = (int) $punches_left;
				$punches[$punch_item_id]['price_class_id'] = (int) $item['price_class_id'];
			}
		}

		if(empty($punches)){
			$this->msg = 'No punches left on card';
			return false;
		}

		$punches_used = array();
        $total_used = array();
		$cart_items = $this->get_items(true);
        foreach($cart_items as $cart_item){
            $cart_item_id = empty($cart_item['price_class_id']) ? $cart_item['item_id'] : $cart_item['item_id'].'_'.$cart_item['price_class_id'];
            // If the item does not match any punches
			if(empty($punches[$cart_item_id])){
                // This is for when the punch card does not require a price class (it works for any price class)
                if (!empty($punches[$cart_item['item_id']])) {
                    $cart_item_id = $cart_item['item_id'];
                }
                else {
                    continue;
                }
			}
            $punch_item = $punches[$cart_item_id];

			// If the item is a green fee or cart fee, and the price classes do not match, skip it
            if(
				!empty($punch_item['price_class_id']) &&
				(empty($cart_item['price_class_id']) || $punch_item['price_class_id'] != $cart_item['price_class_id'])
			){
				continue;
			}

            $punches_available = (int) $punches[$cart_item_id]['punches'];
            $punch_quantity = 0;
            if(!empty($total_used[$cart_item_id])){
                $punch_quantity = $total_used[$cart_item_id];
            }

            if($punches_available <  $punch_quantity + $cart_item['quantity']){
                $this->msg = 'Only '.$punches_available.' punches left on card';
                continue;
            }

            $total_used[$cart_item_id] = min((int) $punches_available, $punch_quantity + (int) $cart_item['quantity']);
			$punches_used[$cart_item_id]['used'] = $total_used[$cart_item_id];
			$punches_used[$cart_item_id]['price_class_id'] = (int) $punch_item['price_class_id'];

			//$cart_item['quantity'] = $total_used[(int) $cart_item['item_id']];
			$this->calculate_item_totals($cart_item, $this->get_taxable());
			$valid_amount += (float) $cart_item['total'];
		}

        // Update punches used to punch card item list
        foreach($punch_card['items'] as $key => &$item){
            if(!empty($total_used[(int) $item['item_id']])){
                $item['used'] += $total_used[(int) $item['item_id']];
            }
            if(!empty($total_used[(int) $item['item_id'].'_'.$item['price_class_id']])){
                $item['used'] += $total_used[(int) $item['item_id'].'_'.$item['price_class_id']];
            }
        }
        if(empty($punches_used)){
            if ($this->msg == null) {
                $this->msg = 'No valid items in cart';
            }
			return false;
		}

		$payment['amount'] = (float) $valid_amount;
		$payment['description'] = 'Punch Card:'.trim($punch_card['punch_card_number']);
		$payment['record_id'] = (int) $punch_card['punch_card_id'];
		$payment['params'] = array('punches_used' => $punches_used);
		$payment['punch_card'] = $punch_card;

		return $payment;
	}

	function process_giftcard_payment($payment){
        if(empty($payment['bypass']) || (int)$payment['bypass'] != 1) {
            $this->load->model('Giftcard');
            if (empty($payment['number'])) {
                $this->msg = 'Gift card number is required';
                return false;
            }
            $giftcard_id = $this->Giftcard->get_giftcard_id($payment['number']);

            // If no card found, return error
            if (empty($giftcard_id)) {
                $this->msg = 'Gift card not found';
                return false;
            }

            $result = $this->db->select('amount')
                ->from('pos_cart_payments')
                ->where(array(
                    'record_id' => (int)$giftcard_id,
                    'type' => 'gift_card',
                    'cart_id' => $this->cart_id
                ))->get();
            $existing_payment = $result->row_array();
            $existing_amount = 0;

            if ($existing_payment) {
                $existing_amount = (float)$existing_payment['amount'];
            }

            // If expired, return error
            if ($this->Giftcard->is_expired($giftcard_id)) {
                $this->msg = 'Gift card expired';
                return false;
            }

            $giftcard_info = $this->Giftcard->get_info($giftcard_id);
            $cur_giftcard_value = $giftcard_info->value;
            $cur_giftcard_value -= $existing_amount;

            $totals = $this->get_totals();
            $cart = $this->get_cart();
            $valid_cart_total = (float)$totals['total'];

            // If gift card is retricted to a certain department or category
            // calculate the total cost of items with that criteria
            if ($giftcard_info->department != '' || $giftcard_info->category != '') {

                $valid_cart_total = 0;
                $items = $this->get_items(true);

                foreach ($items as $item) {
                    if (($giftcard_info->department != '' && $item['department'] == $giftcard_info->department) ||
                        ($giftcard_info->category != '' && $item['category'] == $giftcard_info->category)
                    ) {
                        $valid_cart_total += $item['total'];
                    }
                }
            }

            if ($cart['mode'] == 'sale') {
                $payment['amount'] = min($valid_cart_total, $payment['amount']);
            } else {
                $payment['amount'] = max($valid_cart_total, $payment['amount']);
            }

            if ($cur_giftcard_value <= 0 && $payment['amount'] > 0) {
                $this->msg = 'Insufficient balance on giftcard';
                return false;
            }

            if (($cur_giftcard_value - $payment['amount']) > 0) {
                $this->msg = lang('sales_giftcard_balance_is') . ' ' . to_currency($cur_giftcard_value);
            }

            $payment['amount'] = min($payment['amount'], $cur_giftcard_value);
            $payment['record_id'] = (int)$giftcard_id;
            $payment['description'] = 'Gift Card:' . trim($payment['number']);
            $payment['gift_card'] = (array)$giftcard_info;
        }
        else
        {
            $payment['amount'] = $payment['amount'];
            $payment['record_id'] = 0;
            $payment['description'] = 'Gift Card Payment';
            $payment['gift_card'] = array();
        }

		return $payment;
	}

	function process_loyalty_payment($payment){

		$cart_totals = $this->get_totals();
        if ($cart_totals['total_paid'] + $payment['amount'] > $cart_totals['total']) {
            $payment['amount'] = $cart_totals['total'] - $cart_totals['total_paid'];
        }
        
        if ($cart_totals['total_paid'] < ($cart_totals['total'] - $payment['amount'])) {
            $loyalty_due_ratio = round(1 - (($cart_totals['total'] - $payment['amount']) / $cart_totals['total']), 5);
        }
        else {
            $loyalty_due_ratio = round(1 - ($cart_totals['total_paid'] / $cart_totals['total']), 5);
        }
		$loyalty_due_to_course = round($cart_totals['total_loyalty_points'] * $loyalty_due_ratio, 0);
		$loyalty_dollars = $cart_totals['total_loyalty_dollars'];

		$cur_payment_dollars = 0;
		$cur_payment_points = 0;
		$max_dollars_allowed = 0;

		// Check if a loyalty payment already exists for this customer
		$result = $this->db->select('amount, number')
			->from('pos_cart_payments')
			->where(array(
				'record_id' => (int) $payment['record_id'],
				'type' => 'loyalty_points',
				'cart_id' => $this->cart_id
			))->get();
			
		$existing_payment = $result->row_array();

		if($existing_payment){
			return false;
		}

		$course_ids = [];
		$this->load->model('course');
		$this->course->get_linked_course_ids($course_ids);
		// Get customer's current loyalty data
		$this->db->select('loyalty_points, use_loyalty');
		$this->db->from('customers');
		$this->db->where_in("course_id",$course_ids);
		$this->db->where('person_id', (int) $payment['record_id']);
		$customer = $this->db->get()->row_array();

		if(empty($customer) || $customer['use_loyalty'] == 0){
			return false;
		}
		$customer['loyalty_points'] = (int) $customer['loyalty_points'];

		// If customer has enough points to cover entire sale
		if($customer && $customer['loyalty_points'] >= $loyalty_due_to_course){
			$max_dollars_allowed = $loyalty_dollars;

		// If customer doesn't have enough points, calculate what amount
		// of dollars the customer's points translate to
		}else{
			$ratio = round($customer['loyalty_points'] / $loyalty_due_to_course, 5);
			$max_dollars_allowed = round($ratio * $loyalty_dollars, 2);

			//The customer didn't have enough loyalty, clear out their loyalty.
            // Removing this line of code... it causes 0 points to be deducted from a customer's account
            // when they don't have enough loyalty to cover the payment
			//$customer['loyalty_points'] = 0;
		}

		$max_dollars_allowed -= $cur_payment_dollars;
		$loyalty_due_to_course -= $cur_payment_points;

		$payment['amount'] = min($payment['amount'], $max_dollars_allowed);
		$payment['number'] = min($customer['loyalty_points'], $loyalty_due_to_course);
		$payment['description'] = $this->generate_payment_description($payment);

		return $payment;
	}

	function process_apriva_payment($payment, $merchant_data){
        if ($merchant_data) {
            $payment['description'] = $merchant_data['description'];
            $payment['type'] = $merchant_data['type'];
            $payment['amount'] = $merchant_data['amount'];
            $payment['cardholder_name'] = '';
            $payment['card_number'] = '';
            $payment['auth_code'] = $merchant_data['auth_code'];
            $payment['card_type'] = '';
            $payment['number'] = '';

            $payment['params']['cardholder_name'] = $payment['cardholder_name'];
            $payment['params']['masked_account'] = $payment['card_number'];
            $payment['params']['card_type'] = $payment['card_type'];
            $payment['params']['amount_refunded'] = 0;
        } else if (empty($payment['description'])){
            $payment['description'] = $this->generate_payment_description($payment);
        }
        if($payment['tran_type'] == 'return'){
            $payment['amount'] = (float) 0 - $payment['amount'];
        }


        return $payment;
    }

	// Verify an ETS credit card/gift card payment
	function process_ets_payment($payment, $ets_data){

		$token = false;
		
		// If payment is being charged to an existing saved credit card
		if(!empty($ets_data['credit_card_id'])){
			$credit_card = $this->Customer_credit_card->get_info($ets_data['credit_card_id']);
			$token = $credit_card->token;
		
		}else if(empty($ets_data['session_id']) || empty($ets_data['transaction_id'])){
			return false;
		}

		if($this->credit_card_payment_exists($payment['record_id'])){
			$this->status = 409;
			return false;
		}
		
		if(!$token){
			$this->load->model('sale');
			$this->update_credit_card_payment($payment['record_id'], $ets_data['transaction_id']);
		}

		$this->load->library('Hosted_payments');
		$hosted_payment = new Hosted_payments();
		$hosted_payment->initialize($this->config->item('ets_key'));

		// Verify ETS transaction
		if(empty($token)){
			$session_id = $ets_data['session_id'];
			$transaction_id = $ets_data['transaction_id'];			
			
			$hosted_payment->set("action", "verify")
				->set("sessionID", $session_id)
				->set("transactionID", $transaction_id);		
		
		// If submitting payment to a saved credit card (token), charge the card
		}else{
			$hosted_payment->set("action", "payment")
				->set("amount", $payment['amount'])
				->set("accountId", $token);
		}

		$ets_response = (array) $hosted_payment->send();
		$ets_response['store'] = (array) $ets_response['store'];

		if($payment['type'] == 'credit_card_save'){
			$transaction = (array) $ets_response['customers'];
			$transaction['amount'] = 0;
			$transaction['type'] = 'credit card';
			$transaction['name'] = '';
			$transaction['approvalCode'] = '';
		}else{
			$transaction = (array) $ets_response['transactions'];
		}

		$transaction_time = date('Y-m-d H:i:s', strtotime($ets_response['created']));
		
		$approved = false;
		if($ets_response['status'] == 'success'){
			$approved = true;
		}
		
		// Convert card type to match mercury card types
		if ((string) $transaction['type'] == 'credit card') {

			$ets_card_type = $transaction['cardType'];
			$card_type = '';

			switch(strtoupper($ets_card_type)){
				case 'MASTERCARD':
					$card_type = 'M/C';
				break;
				case 'VISA':
					$card_type = 'VISA';
				break;
				case 'DISCOVER':
					$card_type = 'DCVR';
				break;
				case 'AMERICANEXPRESS':
					$card_type = 'AMEX';
				break;
				case 'DINERS':
					$card_type = 'DINERS';
				break;
				case 'JCB':
					$card_type = 'JCB';
				break;
			}

			$masked_account = (string) $transaction['cardNumber'];
			$auth_code = (string) $transaction['approvalCode'];
			$payment_type = $payment['type'];

		}else if((string) $transaction['type'] == 'gift card'){
			$card_type = 'Giftcard';
			$masked_account = (string) $transaction['cardNumber'];
			$auth_code = '';
			$payment_type = 'ets_gift_card';

		}else{
			$card_type = 'Bank Acct';
			$masked_account = (string) $transaction['accountNumber'];
			$auth_code = '';
			$payment_type = 'credit_card';
		}

		if($payment['type'] == 'credit_card_save'){
			$tran_type = 'PreAuth';
		}else{
			$tran_type = 'Sale';
		}

		$message = (string) $transaction['message'];
		if($message == '00:CREDIT' || $message == '00:FUNDS ADDED'){
			$tran_type = 'Return';
		}
		
		// Update credit card payment data
		$trans_message = (string) $transaction['message'];
		$auth_data = array(
			'course_id' 		=> $this->session->userdata('course_id'),
			'masked_account' 	=> $masked_account,
			'trans_post_time' 	=> (string) $transaction_time,
			'ets_id' 			=> $this->config->item('ets_key'),
			'tran_type' 		=> $tran_type,
			'amount' 			=> (string) $transaction['amount'],
			'auth_amount'		=> (string)	$transaction['amount'],
			'auth_code'			=> $auth_code,
			'card_type' 		=> $card_type,
			'frequency' 		=> 'OneTime',
			'payment_id' 		=> (string) $transaction['id'],
			'process_data'		=> (string) $ets_response['id'],
			'status'			=> (string) $transaction['status'],
			'status_message'	=> (string) $transaction['message'],
			'cardholder_name'	=> (string) $transaction['name'],
			'display_message'	=> (string) $ets_response['message'],
			'operator_id'		=> (string) $this->session->userdata('person_id'),
			'cart_id'			=> $this->cart_id
		);

		if(!empty($token)){
			$auth_data['token'] = $token;
		}

		if(!empty($ets_data['credit_card_id'])){
			$invoice_id = (int) $this->sale->add_credit_card_payment($auth_data);
		}else{
			$invoice_id = (int) $ets_response['store']['primary'];
			$this->sale->update_credit_card_payment($invoice_id, $auth_data);			
		}

		if(!$approved){
			$this->msg = 'Error processing card: '.(string) $transaction['message'];
			return false;			
		}
		
		$amount = (float) $transaction['amount'];
		if($tran_type == 'Return'){
			$amount = (float) 0 - $amount;
		}

		// Save credit card to database for later charges
		if($payment_type == 'credit_card_save'){
			
			$this->load->model('Customer_credit_card');
			
			$card_data = [
				'recurring' => 0,
				'customer_id' => 0,
				'course_id' => $this->session->userdata('course_id'),
				'token' => $transaction['id'],
				'card_type' => $card_type,
				'masked_account' => $masked_account
			];
			$credit_card_id = $this->Customer_credit_card->save($card_data);

			$payment['params']['credit_card_id'] = (int) $credit_card_id;
		}
		
		$payment['description'] = $card_type.' '.$transaction['cardNumber'];
		$payment['type'] = $payment_type;
		$payment['amount'] = $amount;
		$payment['record_id'] = $invoice_id;
		$payment['cardholder_name'] = '';
		$payment['card_number'] = $masked_account;
		$payment['auth_code'] = $auth_code;
		$payment['card_type'] = $card_type;
		$payment['number'] = $masked_account;

		$payment['params']['cardholder_name'] = $auth_data['cardholder_name'];
		$payment['params']['masked_account'] = $auth_data['masked_account'];
		$payment['params']['card_type'] = $auth_data['card_type'];
		$payment['params']['amount_refunded'] = 0;
		
		return $payment;
	}

	// Verify an Element credit card payment
	function process_element_payment($payment, $element_data){
		
		$this->load->model('v2/Sale_model');
		$this->load->library('v2/Element_merchant');
	
		// Initialize payment library
		$this->element_merchant->init(
			new GuzzleHttp\Client()
		);

		// If card is swiped with encrypted swiper
		if(isset($element_data['encrypted_track']) || !empty($element_data['credit_card_id'])){
			
			// Add fee to payment amount (if course has fee implemented)
			if($this->config->item('credit_card_fee') > 0 && $payment['amount'] > 0){
				$this->add_credit_card_fee($payment);
			}			
			
			$params = array(
				'amount' => abs($payment['amount'])
			);

			if(!empty($element_data['credit_card_id'])){
                $credit_card = $this->Customer_credit_card->get_info($element_data['credit_card_id']);
                $token = $credit_card->token;
                $params['payment_account_id'] = $token;

			}else{
				$params['encrypted_track'] = $element_data['encrypted_track'];
			}

			if($payment['type'] == 'credit_card_save'){
				
				$this->load->model('Customer_credit_card');
	            $card_data = [
	                'recurring' => 0,
	                'customer_id' => 0,
	                'course_id' => $this->session->userdata('course_id')
	            ];
				$credit_card_id = $this->Customer_credit_card->save($card_data);

	            $params['payment_account_reference_number'] = $credit_card_id;
				$this->element_merchant->payment_account_create($params);
				$tran_type = 'PaymentAccountCreate';

			}else if($payment['amount'] >= 0){	
				$this->element_merchant->credit_card_sale($params);
				$tran_type = 'Sale';
			
			}else{
				$this->element_merchant->credit_card_credit($params);
				$tran_type = 'Return';
			}
			
			if(!$this->element_merchant->success()){
				$this->msg = 'Error: '.$this->element_merchant->message();
				return false;
			}

			if($payment['type'] == 'credit_card'){
				$transaction_id = (string) $this->element_merchant
					->response()->xml()
					->Response->Transaction->TransactionID;

				// Get more details on the credit card just swiped
				$this->element_merchant->transaction_query(array(
					'transaction_id' => $transaction_id
				));
			
			}else{
				$payment_account_id = (string) $this->element_merchant
					->response()->xml()
					->Response->PaymentAccount->PaymentAccountID;

				// Get more details on the credit card just swiped
				$this->element_merchant->payment_account_query(array(
					'payment_account_id' => $payment_account_id
				));
			}

			// If the transaction query fails, reverse the credit card transaction
			// and return an error
			if(!$this->element_merchant->success()){
				
				$message = $this->element_merchant->message();
				
				// Log the error
				error_log('ELEMENT API ERROR [COURSE_ID '.$this->session->userdata('course_id').']: '.
					$message.'. REQUEST: '.$this->element_merchant->get_request_xml());
				
				$this->element_merchant->credit_card_reversal(array(
					'transaction_id' => $transaction_id,
					'amount' => abs($payment['amount'])
				));
				
				$this->msg = 'Error saving card details: '.$message;
				return false;
			}

			$input_method = 'Swipe';

			if($payment['type'] == 'credit_card_save'){
				
				$query_response = (array) $this->element_merchant->response()->xml()->Response;
				$transaction_details = (array) $this->element_merchant->response()->xml()
					->Response->QueryData->Items->Item;				

				if($query_response['ExpressResponseMessage'] != 'Success'){
					$approved = false;
				}else{
					$approved = true;
				}
				$card_type = $this->cart_lib->standardize_credit_card_type($transaction_details['PaymentBrand']);
				
				$cc_invoice_data = array(
					'tran_type' => $tran_type,
					'element_account_id' => $this->config->item('element_account_id'),
					'amount' => 0.00,
					'auth_amount' => 0.00,
					'payment_id' => $transaction_details['PaymentAccountID'],
					'token' => $transaction_details['PaymentAccountID'],
					'status' => $query_response['ExpressResponseMessage'],
					'status_message' => $query_response['ExpressResponseMessage'],
					'masked_account' => substr((string) $transaction_details['TruncatedCardNumber'], -4, 4),
					'card_type' => $card_type,
					'response_code' => $query_response['ExpressResponseCode'],
					'auth_code' => '',
					'cart_id' => $this->cart_id
				);

				$payment['record_id'] = $this->Sale_model->save_credit_card_payment(null, $cc_invoice_data);	
				$payment['invoice_id'] = $payment['record_id'];

			}else{
				
				$transaction_details = (array) $this->element_merchant->response()->xml()
					->Response->ReportingData->Items->Item;				

				if($transaction_details['ExpressResponseMessage'] != 'Approved'){
					$approved = false;
				}else{
					$approved = true;
				}
				$card_type = $this->cart_lib->standardize_credit_card_type($transaction_details['CardLogo']);
				
				$cc_invoice_data = array(
					'tran_type' => $tran_type,
					'element_account_id' => $this->config->item('element_account_id'),
					'amount' => $transaction_details['ApprovedAmount'],
					'auth_amount' => $transaction_details['ApprovedAmount'],
					'payment_id' => $transaction_details['TransactionID'],
					'status' => $transaction_details['TransactionStatus'],
					'status_message' => $transaction_details['ExpressResponseMessage'],
					'masked_account' => substr((string) $transaction_details['CardNumberMasked'], -4, 4),
					'card_type' => $card_type,
					'auth_code' => $transaction_details['ApprovalNumber'],
					'response_code' => $transaction_details['ExpressResponseCode'],
					'cart_id' => $this->cart_id
				);

				$payment['record_id'] = $this->Sale_model->save_credit_card_payment(null, $cc_invoice_data);	
				$payment['invoice_id'] = $payment['record_id'];
			}
		
		// If card is entered manually via iframe
		}else{	

			if($element_data['ExpressResponseCode'] == 0){
				$approved = true;
			}else{
				$approved = false;
			}
	
			$input_method = 'Manual Entry';
			
			// If saving credit card for future use
			if(!empty($element_data['PaymentAccountID'])){
				$credit_card_id = -1;
				$tran_type = 'PaymentAccountCreate';

				// Get more details on the credit card we are saving
				$this->element_merchant->payment_account_query(array(
					'payment_account_id' => $element_data['PaymentAccountID']
				));

				$query_response = (array) $this->element_merchant->response()->xml()->Response;
				$transaction_details = (array) $this->element_merchant->response()->xml()
					->Response->QueryData->Items->Item;				

				if($query_response['ExpressResponseMessage'] != 'Success'){
					$approved = false;
				}else{
					$approved = true;
				}
				$card_type = $this->cart_lib->standardize_credit_card_type($transaction_details['PaymentBrand']);
				
				$cc_invoice_data = array(
					'tran_type' => $tran_type,
					'element_account_id' => $this->config->item('element_account_id'),
					'amount' => 0.00,
					'auth_amount' => 0.00,
					'payment_id' => $transaction_details['PaymentAccountID'],
					'token' => $transaction_details['PaymentAccountID'],
					'status' => $query_response['ExpressResponseMessage'],
					'status_message' => $query_response['ExpressResponseMessage'],
					'masked_account' => substr((string) $transaction_details['TruncatedCardNumber'], -4, 4),
					'card_type' => $card_type,
					'response_code' => $query_response['ExpressResponseCode'],
					'auth_code' => '',
					'cart_id' => $this->cart_id
				);
			
			// If charging card now
			}else{
				$tran_type = 'Sale';
				$card_type = $this->cart_lib->standardize_credit_card_type($element_data['CardLogo']);

				$cc_invoice_data = array(
					'tran_type' => $tran_type,
					'element_account_id' => $this->config->item('element_account_id'),
					'amount' => $element_data['ApprovedAmount'],
					'auth_amount' => $element_data['ApprovedAmount'],
					'payment_id' => $element_data['TransactionID'],
					'status' => $element_data['HostedPaymentStatus'],
					'status_message' => $element_data['ExpressResponseMessage'],
					'masked_account' => $element_data['LastFour'],
					'card_type' => $card_type,
					'auth_code' => $element_data['ApprovalNumber'],
					'response_code' => $element_data['ExpressResponseCode'],
					'cart_id' => $this->cart_id
				);
			}

			$this->Sale_model->save_credit_card_payment($payment['record_id'], $cc_invoice_data);	
		}
		
		if(!$approved){
			if(!empty($element_data['HostedPaymentStatus']) && $element_data['HostedPaymentStatus'] == 'Cancelled'){
				$this->msg = 'Payment cancelled';
			}else{
				$this->msg = 'Error: '.$cc_invoice_data['status_message'];	
			}
			return false;
		}

		if($payment['type'] == 'credit_card_save'){  
            $card_data = [
				'recurring' => 0,
				'customer_id' => 0,
				'course_id' => $this->session->userdata('course_id'),   
				'token' => $cc_invoice_data['token'],
				'card_type' => $cc_invoice_data['card_type'],
				'masked_account' => $cc_invoice_data['masked_account']
            ];
            $credit_card_id = $this->Customer_credit_card->save($card_data, $credit_card_id);

            $payment['params']['credit_card_id'] = (int) $credit_card_id;			
		}			
		
		$payment['description'] = $card_type.' '.$cc_invoice_data['masked_account'];
		$payment['invoice_id'] = $payment['record_id'];
		$payment['cardholder_name'] = '';
		$payment['card_number'] = $cc_invoice_data['masked_account'];
		$payment['auth_code'] = $cc_invoice_data['auth_code'];
		$payment['card_type'] = $cc_invoice_data['card_type'];
		$payment['transaction_type'] = $tran_type;
		$payment['transaction_id'] = $cc_invoice_data['payment_id'];
		$payment['input_method'] = $input_method;
		$payment['number'] = $cc_invoice_data['masked_account'];

		$payment['params']['cardholder_name'] = '';
		$payment['params']['masked_account'] = $cc_invoice_data['masked_account'];
		$payment['params']['card_type'] = $cc_invoice_data['card_type'];
		$payment['params']['amount_refunded'] = 0;
		
		return $payment;
	}

    // Verify a Mercury credit card payment
	function process_mercury_payment($payment, $mercury_data){

		if(!empty($payment['verified'])){
			if (!empty($payment['merchant_data']['TransType']) && $payment['merchant_data']['TransType'] == 'refund'){
				$mercury_emv_data = $this->session->userdata('mercury_emv');
				$payment['invoice_id'] = $payment['record_id'];// = $payment['merchant_data']['RefID'] = $mercury_emv_data['invoice_id'];
			}
			if ($payment['tran_type'] == 'Return'){
				$payment['amount'] = (float)0 - (float)$payment['amount'];
			}
			return $payment;
		}

		$this->load->model('Credit_card_payments');
		$this->Credit_card_payments->cart_id = $this->cart_id;

		$payment = $this->Credit_card_payments->mercury_payment($payment, $mercury_data);

		if(!empty($this->Credit_card_payments->msg)){
			$this->msg = $this->Credit_card_payments->msg;
		} else {
			$this->msg = "No response from credit card processor.";
		}

		return $payment;
    }

	// Generates human readable payment description (to be displayed on sales report, etc)
	function generate_payment_description($payment){

		$customer = false;
		$credit_card = false;

		$account_payment_types = array(
			'member_account',
			'member_account_tip',
			'customer_account',
			'customer_account_tip',
			'member_account_refund',
			'customer_account_refund'
		);

		$member_account_name = 'Member Balance';
		if($this->config->item('member_balance_nickname')){
			$member_account_name = trim($this->config->item('member_balance_nickname'));
		}
        $customer_account_name = 'Customer Credit';
		if($this->config->item('customer_credit_nickname')){
			$customer_account_name = trim($this->config->item('customer_credit_nickname'));
		}

		// If payment was from a member account, get customer's name
		if(in_array($payment['type'], $account_payment_types)){
			$this->db->select('p.first_name, p.last_name');
			$this->db->from('people as p');
			$this->db->where('p.person_id', $payment['record_id']);
			$row = $this->db->get()->row_array();

			$customer = $row['last_name'].', '.$row['first_name'];
		}

		// If payment was a credit card, get the details
		if(($payment['type'] == 'credit_card' || $payment['type'] == 'credit_card_tip') && $payment['record_id'] > 0){
			
			// Build the credit card payment description
			if(true || empty($payment['sale_id'])){
				$this->db->select('card_type, masked_account');
				$this->db->from('sales_payments_credit_cards');
				$this->db->where('invoice', $payment['record_id']);
				$row = $this->db->get()->row_array();

				$credit_card = $row['card_type'].' '.$row['masked_account'];			
			    $credit_card = empty(trim($credit_card)) ? 'Credit Card' : $credit_card;
			// If payment is already part of a sale, just retrieve 
			// the original payment description
			}else{
				// this sometimes grabs the wrong payment
				/*
				$this->db->select('payment_type');
				$this->db->from('sales_payments');
				$this->db->where('sale_id', $payment['sale_id']);
				$this->db->where('invoice_id', $payment['record_id']);
				$row = $this->db->get()->row_array();

				$credit_card = $row['payment_type'];
				*/
			}
		}

		if($payment['type'] == 'member_account'){
			$payment_type = $member_account_name.' - '.$customer;

		}else if($payment['type'] == 'customer_account'){
			$payment_type = $customer_account_name.' - '.$customer;

		}else if($payment['type'] == 'member_account_tip'){
			$payment_type = $member_account_name.' - '.$customer.' Tip';

		}else if($payment['type'] == 'customer_account_tip'){
			$payment_type = $customer_account_name.' - '.$customer.' Tip';

		}else if($payment['type'] == 'member_account_refund'){
			$payment_type = $member_account_name.' Refund - '.$customer;

		}else if($payment['type'] == 'customer_account_refund'){
			$payment_type = $customer_account_name.' Refund - '.$customer;

		}else if($payment['type'] == 'credit_card' && $payment['record_id'] != 0){
			$payment_type = $credit_card;

		}else if($payment['type'] == 'credit_card_tip' && $payment['record_id'] != 0){
			$payment_type = $credit_card.' Tip';

		}else if($payment['type'] == 'credit_card_refund'){
			$payment_type = 'CC Refund';

		}else if($payment['type'] == 'credit_card_partial_refund'){
			$payment_type = 'CC Partial Refund';

		}else if($payment['type'] == 'credit_card'){
			$payment_type = 'Credit Card';

		}else if($payment['type'] == 'credit_card_tip'){
			$payment_type = 'Credit Card Tip';

		}else if($payment['type'] == 'cash_tip'){
			$payment_type = 'Cash Tip';

		}else if($payment['type'] == 'cash_refund'){
			$payment_type = 'Cash Refund';

		}else if($payment['type'] == 'gift_card'){
			$payment_type = 'Gift Card';
			if(!empty($payment['number'])){
				$payment_type .= ':'.$payment['number'];
			}

		}else if($payment['type'] == 'gift_card_tip'){
			$payment_type = 'Gift Card Tip';
			if(!empty($payment['number'])){
				$payment_type = 'Gift Card:'.$payment['number'].' Tip';
			}

		}else if($payment['type'] == 'check_tip'){
			$payment_type = 'Check Tip';

		}else if($payment['type'] == 'loyalty_points'){
			$payment_type = 'Loyalty';

		}else{
            $this->load->model('custom_payments');
            //Check if it's a custom payment
            $payment_type = $this->custom_payments->get_payment_type($payment['type']);
            if (!$payment_type)
			    $payment_type = ucfirst($payment['type']);
		}

		if(!empty($payment['is_auto_gratuity']) && $payment['is_auto_gratuity'] == 1){
			$payment_type .= ' (Auto Gratuity)';
		}

		return $payment_type;
	}

	// Checks a member's account to verify they have a sufficient balance.
	// Returns amount of approved balance, or false if no balance available
	function verify_account_balance($account_type, $customer_id, $amount){

		if(empty($customer_id) || empty($account_type)){
			$this->msg = 'customer_id and account_type parameters required to verify customer account';
			return false;
		}
		if($account_type != 'member' && $account_type != 'customer'){
			$this->msg = 'account_type must be customer or member: '.$account_type;
			return false;
		}
		$amount = (float) $amount;

		$course_ids = [];
		$this->load->model('course');
		$this->course->get_linked_course_ids($course_ids);


    	$this->db->select('member_account_balance_allow_negative,
			account_balance_allow_negative, member_account_balance,
			account_balance, account_limit, member_account_limit');
		$this->db->from('customers');
		$this->db->where('person_id', (int) $customer_id);
		$this->db->where_in('course_id',array_values($course_ids));
		$customer_info = $this->db->get()->row_array();

		if(empty($customer_info)){
			$this->msg = 'Failed to retrieve customer: '.$customer_id;
			return false;
		}

		// Check if account allows negative balance
		$allow_negative = false;
		$account_limit = 0;
		if($account_type == 'member' && $customer_info['member_account_balance_allow_negative'] == '1'){
			$allow_negative = true;
			$account_limit = (float) $customer_info['member_account_limit'];

		}else if($account_type == 'customer' && $customer_info['account_balance_allow_negative'] == '1'){
			$allow_negative = true;
			$account_limit = (float) $customer_info['account_limit'];
		}

		// Get current balance of account
		$cur_balance = 0;
		if($account_type == 'member'){
			$cur_balance = (float) $customer_info['member_account_balance'];
		}else{
			$cur_balance = (float) $customer_info['account_balance'];
		}
		// If there is no balance on account, return error
		if($amount > 0 &&  (($cur_balance <= 0 && !$allow_negative) || ($account_limit != 0 && $cur_balance <= $account_limit))){
			$this->msg = 'Insufficient balance on account';
			return false;
		}

		// If payment is from customer account, and course has limits 
		// on what type of items can be purchased with that acccount
		if($account_type == 'customer'){
			$this->load->model('v2/Course_model');
			$restriction = $this->Course_model->get_customer_account_spending_restriction();

            // If we're restricting customer credit to subtotal only. Calculate the subtotal
            $subtotal_only = $this->config->item('credit_subtotal_only');

            if (!$restriction && $subtotal_only) {
                // Skip this if we have restrictions because the restrictions will take care of calculating the subtotal
                $items = $this->get_items(true);
                $valid_total = 0;
                foreach ($items as $item) {
                    $valid_total += $item['subtotal'];
                }
                $amount = min($amount, $valid_total);
            } elseif ($restriction) {
				$items = $this->get_items(true);
				$total_field = ($subtotal_only) ? 'subtotal' : 'total';
				$valid_total = $this->cart_lib->get_filtered_item_total($items, $restriction['field'], $restriction['value'], $total_field);

				if($valid_total == 0){
					$this->msg = 'No valid items in cart';
					return false;
				}
				$amount = min($amount, $valid_total);
			}
		}
    	
		// If there is some balance, but not enough, set the payment to the remaining balance
		if((!$allow_negative && $amount > $cur_balance) || ($account_limit != 0 && $amount > ($cur_balance - $account_limit))){
			$this->msg = 'Insufficient account funds for entire payment. Maximum funds applied.';
			$this->status = 'warn';

			return (float) $cur_balance - $account_limit;
		}

		return (float) $amount;
	}

	function process_account_payment($payment, $customer_id = false){

		if(empty($customer_id)){
			return false;
		}

		// Retrieve any payments already made for this account currently in cart
		$existing_payment = $this->db->select('amount')
			->from('pos_cart_payments')
			->where('type', $payment['type'])
			->where('record_id', $payment['record_id'])
			->where('cart_id', $this->cart_id)
			->get()
			->row_array();
			
		$cur_payment = 0;
		if(!empty($existing_payment['amount'])){
			$cur_payment = (float) $existing_payment['amount'];
		}

		if($payment['type'] == 'member_account'){
			$type = 'member';
		}else{
			$type = 'customer';
		}

    	// Make sure customer has sufficient balance
        $this->load->model('Household');
        $household_parent = $this->Household->get_head($customer_id);
        $household_parent_id = !empty($household_parent['household_head_id']) ? $household_parent['household_head_id'] : $customer_id;
        
        $approved = $this->verify_account_balance($type, $household_parent_id, $payment['amount'] + $cur_payment);
		if($approved === false){
			return false;
		}

		$payment['amount'] = $approved;
		$payment['description'] = $this->generate_payment_description($payment);

		return $payment;
	}

	// Deletes a payment from the cart, credit cards will be refunded
	function delete_payment($payment_id){

		if(empty($payment_id)){
			return false;
		}
		$payment = $this->get_payments($payment_id);
		$success = true;
		
		if(empty($payment)){
			return false;
		}

		$this->load->model('Customer_credit_card');

		// If payment is a credit card, refund the payment
		if($payment['type'] != 'credit_card_save' && stripos($payment['type'], 'credit_card') !== false && $payment['record_id'] > 0){

            $this->load->model('Payment_return');
            $return_log_data = array(
                'cart_id' => $this->cart_id,
                'note' => 'cart_model->delete_payment',
                'invoice_id' => $payment['record_id'],
                'employee_id' => $this->session->userdata('person_id')
            );
            $this->Payment_return->save($return_log_data);

            $success = $this->refund_credit_card($payment['record_id']);
		}

		if($payment['type'] == 'credit_card_save' && !empty($payment['params']['credit_card_id'])){
			$this->Customer_credit_card->delete_card(0, $payment['params']['credit_card_id']);
		}

		if($success){
			$this->db->delete('pos_cart_payments', array('cart_id' => $this->cart_id, 'payment_id' => $payment_id));
			return true;
		}

		return false;
	}

	// Refunds a credit card via Mercury or ETS API
	function refund_credit_card($invoice_id = false){

		$refunded = false;
		if(empty($invoice_id)){
			return false;
		}
		$this->load->model('v2/Sale_model');

		$cc_payment = $this->Sale_model->get_credit_card_payment($invoice_id);
		if(empty($cc_payment)){
			return false;
		}
		
		if($cc_payment['auth_amount'] < $cc_payment['amount'] ){
			$amount= (float)  $cc_payment['auth_amount'] + (float) $cc_payment['gratuity_amount'];
		} else {
			$amount = (float) $cc_payment['amount'] + (float) $cc_payment['gratuity_amount'];
		}

		// With Mercury the gratuity is already added in to the total
		// Remove the added gratuity from above so we don't return double the gratuity
		if(!empty($cc_payment['mercury_id'])){
			$amount -= $cc_payment['gratuity_amount'];
		}
		
		// If payment has been refunded already
		if ($cc_payment['amount_refunded'] > 0){
			return true;
		}

        if($this->config->item('apriva_username')) {

            $data = array();
            $processor = \fu\credit_cards\ProcessorFactory::create("apriva", $data);
            $terminal_id = $this->session->userdata('terminal_id');
            $result = $processor->returnPayment($invoice_id, $terminal_id);
            $refunded = $result['success'];
            // If course is using ETS for payments
        } else if($this->config->item('ets_key')){

			if($cc_payment['process_data'] == ''){
				return false;
			}
			
			$this->load->library('Hosted_payments');
			$hosted_payment = new Hosted_payments();
			$hosted_payment->initialize($this->config->item('ets_key'));

			// If gift card
			if($cc_payment['card_type'] == 'Giftcard'){
				
				$ets_response = $hosted_payment->set('posAction', 'refund')
						->set('amount', $amount)
						->set('sessionID', $cc_payment['process_data'])
						->send();
				$errorMessage = 'Error refunding gift card payment';
				
			// If credit card (or bank payment)
			}else{
				$ets_response = $hosted_payment->set('action', 'void')
						->set('transactionID', $cc_payment['payment_id'])
						->set('sessionID', $cc_payment['process_data'])
						->send();
				$errorMessage = 'Error refunding credit card payment';
			}

			// If refund succeeded
			if($ets_response->status == 'success'){
				$refunded = true;
				$sql = $this->Sale_model->add_ets_refund_payment($invoice_id, $amount);

			// If refund failed
			}else{
				if(!empty($ets_response->message)){
					$messages = '';
					foreach($ets_response->message as $message){
						$messages .= implode(', ', $message);
					}
					$errorMessage .= ' - '.$messages;
					$refunded = false;
				}
			}

		// If course is using Mercury for payments
		}else if($this->config->item('mercury_id')){

			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
			$HC->set_invoice($invoice_id);
			$HC->set_cardholder_name($cc_payment['cardholder_name']);
			$HC->set_token($cc_payment['token']);

			$response = $HC->issue_refund($invoice_id, $amount);

			if (!empty($response->Status) && $response->Status == 'Approved'){
				$refunded = true;
				$sql = $this->Sale->add_refund_payment($invoice_id, $amount, $response);

			}else{
				$refunded = false;
			}
		
		// If course using Element payments
		}else if($this->config->item('element_account_id')){
			
			// Initialize Element API
			$this->load->library('v2/Element_merchant');
			$this->element_merchant->init(
				new GuzzleHttp\Client()
			);

			// Void credit card transaction
			$this->element_merchant->credit_card_void(array(
				'transaction_id' => $cc_payment['payment_id'],
				'reference_number' => $invoice_id,
				'ticket_number' => $invoice_id
			));

			if($this->element_merchant->success()){
				$refunded = true;
				
				// Update CC payment data to show that it was voided
				$this->Sale_model->save_credit_card_payment($invoice_id, array(
					'amount_refunded' => $cc_payment['amount'],
					'voided' => 1
				));
			}
		}
		
		return $refunded;
	}

	public function delete($cart_id = false, $refund_payments = true){

		if(empty($cart_id)){
			return false;
		}
		$cart_id = (int) $cart_id;

		// Refund any payments made
		if($refund_payments){
			$payments = $this->get_payments();
			
			foreach($payments as $payment){
				$success = $this->delete_payment($payment['payment_id']);
				if(!$success){
					return false;
				}
			}			
		}

		$this->db->trans_start();

		$this->db->query("DELETE item.*, customer.*, payment.*,
			modifier.*, side.*, side_modifier.*
			FROM foreup_pos_cart AS cart
			LEFT JOIN foreup_pos_cart_items AS item
				ON item.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_item_modifiers AS modifier
				ON modifier.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_item_sides AS side
				ON side.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_item_side_modifiers AS side_modifier
				ON side_modifier.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_customers AS customer
				ON customer.cart_id = cart.cart_id
			LEFT JOIN foreup_pos_cart_payments AS payment
				ON payment.cart_id = cart.cart_id
			WHERE cart.cart_id = {$cart_id}");

		$this->db->query("DELETE FROM foreup_pos_cart WHERE cart_id = ".(int) $cart_id." LIMIT 1");

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
			$course_id = $this->session->userdata('course_id');
			$employee_id = $this->session->userdata('person_id');

			error_log('CART DELETE FAILED - [cart_id:'.$cart_id.'] [course_id:'.$course_id.'] [employee_id:'.$employee_id.']'.
					' [MYSQL ERROR '.$this->db->_error_number().'- '.$this->db->_error_message().']');
			
			$this->msg = 'Error deleting cart, please try again';
			return false;
		}

		return true;
	}

	// Removes all payments, and selected items from cart
	public function clear(){

		$this->db->query("DELETE item.*, payment.*, modifier.*, side.*, side_modifier.*
			FROM foreup_pos_cart AS cart
			LEFT JOIN foreup_pos_cart_items AS item
				ON item.cart_id = cart.cart_id
				AND item.selected = 1
			LEFT JOIN foreup_pos_cart_item_modifiers AS modifier
				ON modifier.line = item.line
				AND modifier.cart_id = item.cart_id
			LEFT JOIN foreup_pos_cart_item_sides AS side
				ON side.cart_line = item.line
				AND side.cart_id = item.cart_id
			LEFT JOIN foreup_pos_cart_item_side_modifiers AS side_modifier
				ON side_modifier.line = item.line
				AND side_modifier.cart_id = item.cart_id
			LEFT JOIN foreup_pos_cart_payments AS payment
				ON payment.cart_id = cart.cart_id
			WHERE cart.cart_id = {$this->cart_id}");

		$this->db->update('pos_cart_items', array('selected' => 1), array('cart_id' => $this->cart_id));

		// If the cart is now empty, clear out any customers
		$num_items = $this->db->select('COUNT(*) AS num_items')
			->from('pos_cart_items')
			->where('cart_id', $this->cart_id)
			->get()->row_array();
		$num_items = (int) $num_items['num_items'];

		// If there are still items in the cart
		if($num_items > 0){
			$params = array(
				'override_authorization_id' => null
			);
		
		// If the cart is empty, reset it
		}else{
			
			$this->db->trans_start();
			$this->db->query("DELETE FROM foreup_pos_cart_customers WHERE cart_id = {$this->cart_id}");
			$this->db->query("DELETE FROM foreup_pos_cart WHERE cart_id = ".(int) $this->cart_id." LIMIT 1");
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE){
				$course_id = $this->session->userdata('course_id');
				$employee_id = $this->session->userdata('person_id');

				error_log('CART CLEAR FAILED - [cart_id:'.$this->cart_id.'] [course_id:'.$course_id.'] [employee_id:'.$employee_id.']'.
					' [MYSQL ERROR '.$this->db->_error_number().'- '.$this->db->_error_message().']');
			}

			$new_cart_id = $this->initialize_cart();
			$this->session->set_userdata('pos_cart_id', $new_cart_id);
			$this->cart_id = $new_cart_id;
			
			return $new_cart_id;
		}

		// Return cart to default settings
		$this->db->update('pos_cart', $params, array('cart_id' => $this->cart_id));
		return true;
	}

	public function clear_customers(){

		$this->db->query("DELETE FROM foreup_pos_cart_customers
			WHERE cart_id = {$this->cart_id}");

		return $this->db->affected_rows();
	}

	// Removes taxes from all items in cart
	private function filter_item_taxes($items){

		foreach($items as &$item){
			if((!$item['taxable'] || $item['force_tax'].''==='0') && !$item['force_tax']){
				unset($item['taxes']);
			}
		}
		return $items;
	}

	function get_item_list($items = array()){

		$html = '';
		foreach($items as $item){
			$html .= '('.$item['quantity'].') '.(!empty($item['name']) ? $item['name'] : '').'<br />';
		}
		return $html;
	}

	// If member or customer account payments exist, deduct the
	// appropriate accounts
	function deduct_accounts($payments, $sale_id, $item_list, $sale_number){

		$employee_id = (int) $this->session->userdata('person_id');
		$course_id = (int) $this->session->userdata('course_id');

		// Loop through payments and check for member or customer account
		// payments
		foreach($payments as $payment){

			if($payment['type'] == 'member_account'){
				$type = 'member';
			}else if($payment['type'] == 'customer_account'){
				$type = 'customer';
			}else{
				continue;
			}

			// Deduct amount from account
			$this->Account_transactions->save(
				$type,
				(int) $payment['record_id'],
				'POS '.$sale_number,
				-$payment['amount'],
				$item_list,
				$sale_id,
				0,
				$employee_id,
				$course_id
			);
		}

		return true;
	}

    function save_refund_details($data) {

        $refund_details = array();
        if(!empty($data['refund_comment'])) {
            $refund_details[] = "refund_comment = CONCAT(refund_comment, \" - ".addslashes($data['refund_comment'])."\")";
        }
        if(!empty($data['refund_reason'])) {
            $refund_details[] = "refund_reason = \"".addslashes($data['refund_reason'])."\"";
        }

        $refund_details_sql = implode(', ', $refund_details);
		if(!empty($refund_details_sql)) {
            
            $this->db->query("UPDATE foreup_pos_cart 
            	SET {$refund_details_sql}
            	WHERE cart_id = {$this->cart_id} 
            		AND course_id = {$this->config->item('course_id')} 
            	LIMIT 1");
        }
    }

	// If member or customer account payments exist, deduct the
	// appropriate accounts
	function deduct_loyalty($payments, $sale_id, $employee_id, $sale_number){

		$this->load->model('Customer_loyalty');
		$course_id = (int) $this->session->userdata('course_id');

		// Loop through payments deduct loyalty points if loyalty
		// payments are found
		foreach($payments as $payment){

			if($payment['type'] != 'loyalty_points'){
				continue;
			}

			$this->Customer_loyalty->save_transaction(
				(int) $payment['record_id'],
				'POS '.$sale_number,
				(int) -$payment['number'],
				'',
				$sale_id,
				$employee_id
			);
		}

		return true;
	}
	
	function is_completing(){		
		
		// Check if cart is locked, if so, do not attempt to save the sale
		// The cart is being completed by another request
		$cart_row = $this->db->select('status')
			->from('pos_cart')
			->where('cart_id', $this->cart_id)
			->get()->row_array();
		
		if(empty($cart_row['status']) || $cart_row['status'] == 'complete'){
			$this->msg = 'Cart is already being completed';
			$this->status = 409;
			return true;
		}		
		
		return false;
	}

	function is_not_active(){		
		
		// Check if cart is no longer active, meaning the cart was
		// marked as 'incomplete' by the system
		$cart_row = $this->db->select('status')
			->from('pos_cart')
			->where('cart_id', $this->cart_id)
			->get()->row_array();
		
		if(empty($cart_row['status']) || $cart_row['status'] != 'active'){
			$this->msg = 'Failed to save sale. Your cart has timed out.';
			$this->status = 408;
			
			$this->cart_id = $this->initialize_cart();
			$this->session->set_userdata('pos_cart_id', $this->cart_id);			

			return true;
		}		
		
		return false;
	}
	
	// Saves the cart data as a sale (items, payments, customer ID)
	public function save_sale($customer_note = null,$force_save = false){
		
		if($this->is_completing()){
			return false;
		}

		if($this->is_not_active()){
			return false;
		}
		
		// Lock the cart while completing the sale
		$this->db->update('pos_cart', array('status' => 'complete'), array('cart_id' => (int) $this->cart_id));
		$this->load->model('Account_transactions');
		$this->load->model('Customer_credit_card');

		// Retrieve all cart data
		$customer_id = false;
		$items = $this->get_items(true);
		$payments = array_values($this->get_payments());
		$employee_id = $this->session->userdata('person_id');
		$response = false;
		$cart_data = $this->get_cart();
		$loyalty_spent = 0;

		// If an employee pin was passed to cart, place the correct employee's
		// id on the sale based on that pin
		if(!empty($this->employee_pin)){
			$temporary_employee_id = $this->Employee_model->authenticate_pin($this->employee_pin);

			// If pin does not properly authenticate
			if(!$temporary_employee_id){
				
				// Unlock cart
				$this->db->update('pos_cart', array('status' => 'active'), array('cart_id' => (int) $this->cart_id));				
				$this->msg = 'Invalid employee pin';
				return false;
			}
			$employee_id = (int) $temporary_employee_id;
		}

		// Check if any change needs to be issued
		$total = $this->get_totals(true, $cart_data['taxable']);
		
		if($total['total_due'] != 0 && $cart_data['mode']!=='return'){
			$payments[] = array(
				'type' => 'change',
				'amount' => (float) $total['total_due'],
				'description' => 'Change issued',
				'record_id' => 0
			);
		}

		// If there is a customer attached to cart
		$customer = $this->get_selected_customer();
		if($customer){
			$customer_id = (int) $customer['person_id'];
		}

		// Remove taxes from items that are not taxable
		$items = $this->filter_item_taxes($items);
		
		if(empty($items) && !$force_save){
			// Unlock cart
			$this->db->update('pos_cart', array('status' => 'active'), array('cart_id' => (int) $this->cart_id));
			return false;
		}
		
		foreach($payments as $key => $payment){
			if($payment['type'] == 'loyalty_points'){
				$loyalty_spent += (int) $payment['number'];
			}
			if($payment['type'] == 'credit_card_save'){
				if(!empty($payment['params']['credit_card_id'])){
					$this->Customer_credit_card->delete_card(0, $payment['params']['credit_card_id']);	
				}
				unset($payments[$key]);
			}
		}

		$sub_item_types = array('sides', 'salads', 'soups', 'items');
		$sale_items = array();
        $line = 1;
		
		// Flatten out sub-items into their own lines before completing sale
		foreach($items as $key => $item){
			
			$item['quantity_purchased'] = $item['quantity'];
			$item['line'] = $line;
			$parentLine = $line;		
			
			// Actual tournament price is just the pot fee, all sub-items
			// will be broken out into their own lines
			if($item['item_type'] == 'tournament'){
				$item['item_unit_price'] = $item['unit_price'] = $item['item_cost_price'] = $item['total_cost'] = (float) $item['pot_fee'];
				$item['profit'] = 0;
				$item['subtotal'] = round((float) $item['quantity'] * (float) $item['unit_price'], 2);
				$item['total'] = (float) $item['subtotal'];
				$item['tax'] = 0;
				$item['taxes'] = array();
			}

			if($item['item_type'] == 'item_kit'){
				$item['item_unit_price'] = 
				$item['subtotal'] = 
				$item['tax'] = 
				$item['total'] = 
				$item['unit_price'] = 
				$item['item_cost_price'] = 
				$item['profit'] =
				$item['total_cost'] = 0;
			}

			$sale_items[] = $item;

			if(!empty($item['service_fees'])){
				
				$item['service_fees'] = $this->filter_item_taxes($item['service_fees']);

				foreach($item['service_fees'] as $service_fee){
					$line++;
					$service_fee['line'] = $line;
					$service_fee['quantity_purchased'] = $service_fee['quantity'];
					$service_fee['item_type'] = 'service_fee';
					
					$sale_items[] = $service_fee;
				}
			}
			
			if(
				(isset($item['food_and_beverage']) && $item['food_and_beverage'] == 1) || 
				$item['item_type'] == 'tournament' || 
				$item['item_type'] == 'item_kit'
			){
				
				foreach($sub_item_types as $sub_type){	
					if(empty($item[$sub_type])){
						continue;
					}
					$item[$sub_type] = $this->filter_item_taxes($item[$sub_type]);
								
					// Break out sub-items into their own lines
					foreach($item[$sub_type] as $sub_item){
						$line++;
						$sub_item['line'] = $line;
						$sub_item['parent_line'] = $parentLine;
						$sub_item['quantity_purchased'] = $sub_item['quantity'];				
						
						$sale_items[] = $sub_item;

                        $sub_item['service_fees'] = $this->filter_item_taxes($sub_item['service_fees']);

                        foreach($sub_item['service_fees'] as $service_fee){
                            $line++;
                            $service_fee['line'] = $line;
                            $service_fee['quantity_purchased'] = $service_fee['quantity'];
                            $service_fee['item_type'] = 'service_fee';

                            $sale_items[] = $service_fee;
                        }
					}
				}
			}

			$line++;
		}

		foreach($payments as $key => &$payment){
			if($payment['record_id'] > 0 && ($payment['type'] == 'credit_card' || $payment['type'] == 'ets_gift_card')){
				$payment['invoice_id'] = $payment['record_id'];
			}
		}

        $data = array(
            'items' => $sale_items,
            'customer_id' => $customer_id,
            'payments' => $payments,
            'comment' => '',
            'customer_note' => $customer_note,
            'employee_id' => $employee_id,
            'teetime_id' => isset($cart_data['teetime_id']) ? $cart_data['teetime_id'] : '',
            'override_authorization_id' => isset($cart_data['override_authorization_id']) ? $cart_data['override_authorization_id'] : 0,
            'refund_reason' => isset($cart_data['refund_reason'])?$cart_data['refund_reason']:"",
            'refund_comment' => isset($cart_data['refund_comment'])?$cart_data['refund_comment']:"",
            'return_sale_id' => isset($cart_data['return_sale_id'])?$cart_data['return_sale_id']:"",
            'taxable' => isset($cart_data['taxable'])?$cart_data['taxable']:1
        );

		if($this->terminal_id){
			$data['terminal_id'] = (int) $this->terminal_id;
		}

		// Save the sale to database
		$sale_saved = $this->sale_model->save(false, $data);
		$sale_id = (int) $sale_saved;
		$sale_number = $this->sale_model->number;

		if($sale_saved){
			$response = array();
			$response['sale_id'] = $sale_id;
			$response['number'] = $sale_number;
			$response['loyalty_points_spent'] = $loyalty_spent;

			$item_list = $this->get_item_list($items);
			$this->deduct_accounts($payments, $sale_id, $item_list, $sale_number);
			$this->sale_model->adjust_inventory($items, $sale_id, $employee_id);

			$this->load->model('Customers_loyalty_package');
			if((int) $this->config->item('use_loyalty') == 1 && $customer_id && $this->Customers_loyalty_package->has_loyalty($customer_id)){
				// Process any loyalty payments in cart
				$this->deduct_loyalty($payments, $sale_id, $employee_id, $sale_number);

				if ($customer['use_loyalty']) {
                    // Award any loyalty points based on items purchased
                    $response['loyalty_points_earned'] = $this->sale_model->adjust_loyalty($items, $payments, $sale_id, $customer_id, $employee_id);
                }

                $packages = $this->Customers_loyalty_package->get_all($customer_id)->result_object();
				foreach ($packages as $package) {
				    // TODO assign this to the response?
                    $this->sale_model->adjust_loyalty($items, $payments, $sale_id, $customer_id, $employee_id, 'sale', $package->loyalty_package_id);
                }
			}

			if(!empty($this->sale_model->erange_codes)){
				$response['erange_codes'] = $this->sale_model->erange_codes;
			}

			$this->clear();
			$response['cart_id'] = $this->cart_id;
		}

		// Unlock cart when done completing sale
		$this->db->update('pos_cart', array('status' => 'active'), array('cart_id' => (int) $this->cart_id));
		
		return $response;
	}
}
