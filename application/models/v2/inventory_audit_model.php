<?php
class Inventory_audit_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        parent::__construct();
        $this->loyalty_rates = null;
    }

    function get($params){
        $params = elements([], $params, null);

        return $this->db->query("SELECT `inventory_audit_id`, CONCAT(date, ' (', first_name, last_name, ')') AS label
            FROM (`foreup_inventory_audits` as ai)
            LEFT JOIN `foreup_people` as p ON `p`.`person_id` = `ai`.`employee_id`
            WHERE `course_id` = '6270'
            ORDER BY `date` DESC")->result_array();

    }

    function get_items($params){
        $params = elements(['audit_id'], $params, null);

        return $this->db->query("SELECT ai.*, item_number, name, department, category, ai.manual_count - ai.current_count as difference,
            (ai.manual_count - ai.current_count) * unit_price AS rev_diff, (ai.manual_count - ai.current_count) * cost_price AS cost_diff
            FROM foreup_inventory_audit_items AS ai
            LEFT JOIN foreup_items AS i ON i.item_id = ai.item_id
            WHERE inventory_audit_id = {$params['audit_id']}
            ORDER BY i.name")->result_array();
    }
}