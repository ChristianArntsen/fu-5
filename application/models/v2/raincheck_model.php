<?php
class Raincheck_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->loyalty_rates = null;
	}
	
	public function get($params, $course_id = false){
		
		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}

		if(!empty($params['number']) && stripos($params['number'], 'RID') !== false){
			$params['number'] = explode(' ', $params['number']);
			$params['number'] = (int) $params['number'][1];
		}
	
		$this->db->select('r.*')
			->from('rainchecks AS r')
			->join('teesheet AS t', 't.teesheet_id = r.teesheet_id', 'inner')
			->where('t.course_id', $course_id);	
		
		if(!empty($params['raincheck_id'])){
			$this->db->where('r.raincheck_id', (int) $params['raincheck_id']);
		}		
		
		if(!empty($params['number'])){
			$this->db->where('r.raincheck_number', $params['number']);
		}

        if(!empty($params['person_id'])){
        	if(is_array($params['person_id'])){
        		$this->db->where_in('r.customer_id', $params['person_id']);
        	}else{
        		$this->db->where('r.customer_id', (int) $params['person_id']);
        	}  

        	$_1_week_ago = date('Y-m-d H:i:s', strtotime('-7 days'));
        	$this->db->where('r.date_redeemed', '0000-00-00 00:00:00');
        	$this->db->where("(r.expiry_date = '0000-00-00 00:00:00' OR r.expiry_date >= '{$_1_week_ago}')");
        }
        
        $this->db->where('r.deleted', 0);
        $this->db->order_by('r.date_issued', 'DESC');
		$rainchecks = $this->db->get()->result_array();	
		
		if(empty($rainchecks)){
			return false;
		}
		
		// Get raincheck item ids
        $this->db->select("SUBSTRING_INDEX(item.item_number, '_', -1) AS item_number, item.item_id", false);
        $this->db->from('items AS item');
        $this->db->join('items_taxes AS tax', 'tax.item_id = item.item_id', 'left');
        $this->db->where('item.course_id', $course_id);
        $this->db->where('item.category', 'Green Fees');
        $this->db->or_where('item.category', 'Carts');
        $this->db->like('item.item_number', "{$course_id}_", 'after');
        $this->db->group_by('item.item_id');

        $item_rows = $this->db->get()->result_array();
        $item_ids = array();
        
        foreach($item_rows as $row){
			$item_ids[(int) $row['item_number']] = (int) $row['item_id'];
		}		
		
		foreach($rainchecks as &$raincheck){
			
			$green_fee_ids = explode('_', $raincheck['green_fee_price_category']);
			$cart_fee_ids = explode('_', $raincheck['cart_price_category']);

			if(!empty($green_fee_ids[0])){
				$raincheck['green_fee'] = array(
					'unit_price' => (float) $raincheck['green_fee'],
					'price_class_id' => (int) $green_fee_ids[0],
					'timeframe_id' => (int) $green_fee_ids[1],
					'item_number' => isset($green_fee_ids[2])?(int) $green_fee_ids[2]:null,
					'item_id' =>   isset($green_fee_ids[2])?(int)$item_ids[$green_fee_ids[2]]:null
				);
			}else{
				$raincheck['green_fee'] = false;
			}
			
			if(!empty($cart_fee_ids[0])){
				$raincheck['cart_fee'] = array(
					'unit_price' => (float) $raincheck['cart_fee'],
					'price_class_id' => (int) $cart_fee_ids[0],
					'timeframe_id' => (int) $cart_fee_ids[1],
					'item_number' => isset($cart_fee_ids[2])?(int) $cart_fee_ids[2]:null,
					'item_id' => isset($cart_fee_ids[2])?(int)$item_ids[$cart_fee_ids[2]]:null
				);
			}else{
				$raincheck['cart_fee'] = false;
			}
			
			$raincheck['tax'] = (float) $raincheck['tax'];
			$raincheck['total'] = (float) $raincheck['total'];
			$raincheck['subtotal'] = $raincheck['total'] - $raincheck['tax'];
			
			unset($raincheck['green_fee_price_category'], $raincheck['cart_price_category']);
		}
		
		return $rainchecks;
	}
	
	public function save($raincheck_id, $data){
		
		$response = false;
		$this->load->model('Pricing');
        $this->load->model('Sale');
		$this->load->library('v2/cart_lib');
        $this->load->model('v2/Cart_model');
		
		$raincheck_data = $raincheck = elements(array('teesheet_id', 'teetime_id',
			'date_redeemed','players', 'holes_completed', 'green_fee', 
			'cart_fee', 'expiry_date', 'customer_id', 'taxable'), $data, null);

		$employee_id = (int) $this->session->userdata('person_id');
		$course_id = (int) $this->session->userdata('course_id');

		$raincheck['employee_id'] = $employee_id;
		$raincheck['date_issued'] = date('Y-m-d H:i:s');
		
		if(empty($raincheck['green_fee']) && empty($raincheck['cart_fee'])){
			return false;
		}
		
		if(empty($raincheck['teesheet_id'])){
			return false;
		}

		if (!empty($raincheck['teetime_id']))
        {
            // RECORD RAINCHECK ON TEE TIME
            $this->Sale->record_teetime_raincheck($raincheck['teetime_id'], $raincheck['players']);
        }

		foreach($raincheck as $field => $val){
			if($val === null){
				unset($raincheck[$field]);
			}
			unset($raincheck['taxable']);
		}
		
		// Get number of fees have
		$green_holes = isset($raincheck['green_fee']) ? (int) $raincheck['green_fee']['holes'] : 18;
        $cart_holes = isset($raincheck['cart_fee']) ? (int) $raincheck['cart_fee']['holes'] : 18;
		
		$cart_fee = 0;
		$green_fee = 0;
		$green_fee_tax = 0;
		$cart_fee_tax = 0;
		$adj_green_fee = $green_fee;
		$adj_cart_fee = $cart_fee;		
		$tax = 0;
		
		$green_ratio = $cart_ratio = 1;
		if($raincheck['holes_completed'] > 0){
            $green_ratio = round(($green_holes - (int) $raincheck['holes_completed']) / $green_holes, 5);
            $cart_ratio = round(($cart_holes - (int) $raincheck['holes_completed']) / $cart_holes, 5);
        }
		
		if(!empty($raincheck['green_fee']['price_class_id'])){
			
			$params = array(
				'type' => 'green_fee',
				'holes' => $raincheck['green_fee']['holes']
			);
			if(!empty($raincheck['green_fee']['special_id'])){
				$params['special_id'] = (int) $raincheck['green_fee']['special_id'];
			}
			if(!empty($raincheck['green_fee']['timeframe_id'])){
				$params['timeframe_id'] = (int) $raincheck['green_fee']['timeframe_id'];
			}	
	
			$green_fee_item = $this->Pricing->get_fees($params);
			
			$green_fee = $green_fee_item[0]['unit_price'];
			$adj_green_fee = (float) round($green_fee * $green_ratio, 2);
			
			$green_fee_tax = $raincheck_data['taxable'] ? $this->cart_lib->calculate_tax($adj_green_fee,  $green_fee_item[0]['taxes']) : 0;
			$raincheck['green_fee_price_category'] = $green_fee_item[0]['price_class_id'].'_'.$green_fee_item[0]['timeframe_id'].'_'.$green_fee_item[0]['item_number'];
		}
		
		if(!empty($raincheck['cart_fee']['price_class_id'])){
			
			$params = array(
				'type' => 'cart_fee',
				'holes' => $raincheck['cart_fee']['holes']
			);
			if(!empty($raincheck['cart_fee']['special_id'])){
				$params['special_id'] = (int) $raincheck['cart_fee']['special_id'];
			}
			if(!empty($raincheck['cart_fee']['timeframe_id'])){
				$params['timeframe_id'] = (int) $raincheck['cart_fee']['timeframe_id'];
			}			
			
			$cart_fee_item = $this->Pricing->get_fees($params);

			$cart_fee = $cart_fee_item[0]['unit_price'];
			$adj_cart_fee = (float) round($cart_fee * $cart_ratio, 2);
			
			$cart_fee_tax = $raincheck_data['taxable'] ? $this->cart_lib->calculate_tax($adj_cart_fee,  $cart_fee_item[0]['taxes']) : 0;
			$raincheck['cart_price_category'] = $cart_fee_item[0]['price_class_id'].'_'.$cart_fee_item[0]['timeframe_id'].'_'.$cart_fee_item[0]['item_number'];
		}
		
		$total_fees = $adj_green_fee + $adj_cart_fee;
		$tax = $green_fee_tax + $cart_fee_tax;
		$tax = round($tax * (int) $raincheck['players'], 2);
		$subtotal = round($total_fees * (int) $raincheck['players'], 2);
        if ($raincheck_data['green_fee']['unit_price_includes_tax'] || !$raincheck_data['taxable']) {
            $total = $subtotal;
        }
        else {
            $total = $subtotal + $tax;
        }

		$raincheck['green_fee'] = $adj_green_fee;
		$raincheck['cart_fee'] = $adj_cart_fee;
		$raincheck['tax'] = $tax;
		$raincheck['total'] = $total;	
		
		if(!$raincheck_id){
			
			$raincheck['raincheck_number'] = 1;
			
			// Build insert query
			$cols = implode(',', array_keys($raincheck));
			$vals = '';
			foreach($raincheck as $col => $val){
				
				/* Use sub-query to get next raincheck number.
				 * Using a subquery makes the query atomic, so parellel
				 * queries return the correct incremented value */
				if($col == 'raincheck_number'){
					$vals .= "IFNULL((SELECT MAX(r.raincheck_number) AS raincheck_number 
						FROM foreup_rainchecks AS r 
						INNER JOIN foreup_teesheet AS t 
							ON t.teesheet_id = r.teesheet_id 
						WHERE t.course_id = {$course_id} 
						GROUP BY t.course_id), 0) + 1,";
				}else{
					$vals .= $this->db->escape($val).',';
				}
			}
			$vals = trim($vals, ',');
			
			// Insert new raincheck
			$this->db->query("INSERT INTO foreup_rainchecks
				($cols) VALUES ($vals)");
			$raincheck_id = (int) $this->db->insert_id();

			// Get the raincheck number that was assigned to row
			$row = $this->db->select('raincheck_number')
				->from('rainchecks')
				->where('raincheck_id', $raincheck_id)
				->get()->row_array();
			$raincheck_number = (int) $row['raincheck_number'];
			
			$response = array('raincheck_id' => $raincheck_id, 'raincheck_number' => $raincheck_number);
// Running sale for raincheck
            $this->Cart_model->course_id = $this->config->item('course_id');
            $this->Cart_model->reset();

            for ($i = 1; $i <= $raincheck_data['players']; $i++) {
                if(!empty($raincheck_data['green_fee']['price_class_id'])) {
                    // Add booking fee to the cart
                    $green_fee_info = [
                        'selected' => true,
                        'item_type' => 'green_fee',
                        'item_id' => $raincheck_data['green_fee']['item_id'],
                        'price_class_id' => $raincheck_data['green_fee']['price_class_id'],
                        'timefram_id' => $raincheck_data['green_fee']['timeframe_id'],
                        'unit_price' => $adj_green_fee,
                        'quantity' => -1,
                        'unit_price_includes_tax' => $raincheck_data['green_fee']['unit_price_includes_tax'],
                        'params' => [
                            'teesheet_id' => $raincheck_data['green_fee']['teesheet_id'],
                            'holes' => $raincheck_data['green_fee']['holes'],
                            'season_id' => $raincheck_data['green_fee']['season_id']
                        ]
                    ];
                    $item = $this->Cart_model->save_item(null, $green_fee_info);
                }
                if (!empty($raincheck_data['cart_fee']['price_class_id'])) {
                    $cart_info = [
                        'selected' => true,
                        'item_type' => 'cart_fee',
                        'item_id' => $raincheck_data['cart_fee']['item_id'],
                        'price_class_id' => $raincheck_data['cart_fee']['price_class_id'],
                        'timefram_id' => $raincheck_data['cart_fee']['timeframe_id'],
                        'unit_price' => $adj_cart_fee,
                        'quantity' => -1,
                        'unit_price_includes_tax' => $raincheck_data['cart_fee']['unit_price_includes_tax'],
                        'params' => [
                            'teesheet_id' => $raincheck_data['cart_fee']['teesheet_id'],
                            'holes' => $raincheck_data['cart_fee']['holes'],
                            'season_id' => $raincheck_data['cart_fee']['season_id']
                        ]
                    ];
                    $item = $this->Cart_model->save_item(null, $cart_info);
                }
            }
            $payment = $this->Cart_model->save_payment(array(
                'amount' => -$total,
                'description' => 'Raincheck Issued',
                'type' => 'raincheck_issued',
                'record_id' => ''

            ), false);
            $this->Cart_model->save_customer($raincheck_data['customer_id'], ['selected' => true]);
            $sale_ids = $this->Sale->get_sale_ids_by_teetime($raincheck_data['teetime_id'])->result_array();
            $return_sale_id = $sale_ids[0]['sale_id'] ?? null;
            $this->Cart_model->save_cart(array('taxable'=>$raincheck_data['taxable'], 'return_sale_id'=>$return_sale_id));
            $sale_response = $this->Cart_model->save_sale();
		}
		
		return $response;
	}
	
	// Marks a rain check as "redeemed"
	function redeem($raincheck_id){
		
		if(empty($raincheck_id)){
			return false;
		}

		$this->db->update('rainchecks', 
			array('date_redeemed' => date('Y-m-d H:i:s')), 
			array('raincheck_id' => $raincheck_id));
			
		return $this->db->affected_rows();
	}

    function delete($raincheck_id) {

        if(empty($raincheck_id)){
            return false;
        }

        $this->db->update('rainchecks',
            array('deleted' => 1),
            array('raincheck_id' => $raincheck_id));

        return $this->db->affected_rows();
    }
}
