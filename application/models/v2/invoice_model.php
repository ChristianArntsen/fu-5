<?php
class Invoice_model extends CI_Model {

	// Get invoices or a single invoice
	function get($params = null){

		$course_id = (int) $this->session->userdata('course_id');

		if(!empty($params['invoice_id'])){
			$this->db->where('invoice_id', (int) $params['invoice_id']);
		}

		if(!empty($params['invoice_number'])){
			$this->db->where('invoice_number', (int) $params['invoice_number']);
		}

		$this->db->select('invoice.*, (invoice.total - invoice.paid) AS due');
		$this->db->from('invoices AS invoice');
		$this->db->where('course_id', $course_id);
		$invoices = $this->db->get()->result_array();

		// Loop through invoices and get list of sales and customers
		// we need info for
		$customer_ids = array();
		$sale_ids = array();
		foreach($invoices as $invoice){
			$customer_ids[] = (int) $invoice['person_id'];
			$sale_ids[] = (int) $invoice['sale_id'];
		}

		// Get details of customers
		$customers = array();
		if(!empty($customer_ids)){
			$this->load->model('v2/Customer_model');
			$customers = $this->Customer_model->get(array('person_id' => $customer_ids));

			foreach($customers as $key => $customer){
				$customers[(int) $customer['person_id']] = $customer;
			}
		}

		// Get details of sales
		$sales = array();
		if(!empty($sale_ids)){
			$this->load->model('v2/Sale_model');
			$sales = $this->Sale_model->get(array('sale_id' => $sale_ids));

			foreach($sales as $sale){
				$sales[(int) $sale['sale_id']] = $sale;
			}
		}

		// Apply customer and sale details to each invoice
		foreach($invoices as &$invoice){

			if(!empty($customers[(int) $invoice['person_id']])){
				$invoice['customer'] = $customers[(int) $invoice['person_id']];
			}

			$invoice['item_type'] = 'invoice';
			$invoice['total'] = (float) $invoice['total'];
			$invoice['unit_price'] = (float) $invoice['total'];
			$invoice['subtotal'] = (float) $invoice['total'];
			$invoice['quantity'] = 1;
			$invoice['tax'] = 0;
			$invoice['name'] = 'Invoice #'.$invoice['invoice_number'];
			$invoice['invoice_id'] = (int) $invoice['invoice_id'];
		}

		return array_values($invoices);
	}
}
