<?php
class Register_log_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	function get($params){
		
		$params = elements(array('register_log_id', 'status', 'limit', 
			'terminal_id', 'employee_id'), $params, null);
		
		if(empty($params['limit'])){
			$params['limit'] = 100;
		}
		$params['limit'] = min(100, (int) $params['limit']);
		
		$this->db->select('register_log.*');
		$this->db->from('register_log');
		$this->db->where('register_log.course_id', $this->session->userdata('course_id'));
		$this->db->limit($params['limit']);
		$this->db->order_by('shift_start DESC');
		
		if(!empty($params['status']) && $params['status'] == 'open'){
			$this->db->where('shift_end', '0000-00-00 00:00:00');
		
		}else if(!empty($params['status']) && $params['status'] == 'closed'){
			$this->db->where('shift_end !=', '0000-00-00 00:00:00');
		}
		
		if(!empty($params['register_log_id'])){
			$this->db->where('register_log_id', (int) $params['register_log_id']);
		}
		
		if(!empty($params['terminal_id'])){
			$this->db->where('terminal_id', (int) $params['terminal_id']);
		}
		
		if(!empty($params['employee_id'])){
			$this->db->where('employee_id', (int) $params['employee_id']);
		}
		
		if(!empty($params['register_log_id'])){
			$logs = $this->db->get()->row_array();
			if($logs['shift_start'] != '0000-00-00 00:00:00'){
				$logs['shift_start'] = date('c', strtotime($logs['shift_start']));
			}
			if($logs['shift_end'] != '0000-00-00 00:00:00'){
				$logs['shift_end'] = date('c', strtotime($logs['shift_end']));
			}
		
		}else{
			$logs = $this->db->get()->result_array();

			foreach($logs as &$log){
				if($log['shift_start'] != '0000-00-00 00:00:00'){
					$log['shift_start'] = date('c', strtotime($log['shift_start']));
				}
				if($log['shift_end'] != '0000-00-00 00:00:00'){
					$log['shift_end'] = date('c', strtotime($log['shift_end']));
				}
			}
		}
			
		return $logs;
	}

	function get_with_joins($params){
		$this->db->join("people as opening_employee","opening_employee.person_id = register_log.employee_id","LEFT");
		$this->db->join("people as closing_employee","closing_employee.person_id = register_log.closing_employee_id","LEFT");
		$this->db->join("terminals","terminals.terminal_id = register_log.terminal_id","LEFT");
		$this->db->select(["opening_employee.first_name AS 'opening_employee.first_name'","opening_employee.last_name AS 'opening_employee.last_name'"]);
		$this->db->select(["closing_employee.first_name AS 'closing_employee.first_name'" ,"closing_employee.last_name AS 'closing_employee.last_name'"]);
		$this->db->select(["terminals.label AS 'terminals.label'"]);

		$this->db->order_by('shift_end DESC');
		return $this->get($params);
	}


	function get_active($terminal_id, $employee_id, $course_id = false){
		
		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}
		$this->db->select('*');
		$this->db->from('register_log');
		$this->db->where('course_id', $course_id);
		$this->db->where('terminal_id', (int) $terminal_id);
		
		if($employee_id !== false){
			$this->db->where('(employee_id = '.(int) $employee_id.' OR persist = 1)');
		}
		
		$this->db->where("shift_end = '0000-00-00 00:00:00'");
		$this->db->limit(1);
		
		return $this->db->get()->row_array();
	}
	
	function save($register_log_id, $data){
		
		$data = elements(array('terminal_id', 'shift_start', 'shift_end', 
			'open_amount', 'close_amount', 'close_check_amount', 'cash_sales_amount',
			'persist', 'check_sales_amount', 'drawer_number',
			'closing_employee_id', 'terminal_id', 'employee_id', 'counts'), $data, '');

		if(empty($data['terminal_id']) && $this->session->userdata('terminal_id')){
			$data['terminal_id'] = (int) $this->session->userdata('terminal_id');
		}
		$data['terminal_id'] = (int) $data['terminal_id'];

		$counts = false;
		if(!empty($data['counts'])){
			$counts = $data['counts'];
		}
		unset($data['counts']);
		
		$data['course_id'] = (int) $this->session->userdata('course_id');
		$data['persist'] = (int) $data['persist'];
		
		// Update existing cash log
		if(!empty($register_log_id)){
			foreach($data as $key => $val){
				if($val === ''){
					unset($data[$key]);
				}
			}

			$this->db->where('register_log_id', $register_log_id);
			$this->db->update('register_log', $data);
		
		// Create new cash log
		}else{
			
			if(empty($data['drawer_number'])){
				$data['drawer_number'] == 1;
			}

			$this->db->insert('register_log', $data);
			$register_log_id = (int) $this->db->insert_id();
		}
		
		if($counts){
			$this->save_counts($register_log_id, $counts);
		}

		return (int) $register_log_id;
	}
	
	function save_counts($register_log_id, $counts){
		
		$counts = elements(array('pennies', 'nickels', 'dimes', 
			'quarters', 'ones', 'fives', 'tens',
			'twenties', 'fifties', 'hundreds'), $counts, 0);
		$counts['register_log_id'] = (int) $register_log_id;
		
		$this->db->delete('register_log_counts', array('register_log_id' => (int) $register_log_id));
		$this->db->insert('register_log_counts', $counts);
		
		return $this->db->affected_rows();
	}
}
