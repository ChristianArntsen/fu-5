<?php
class Menu_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	function delete_item_category($item_id, $course_id = null){
		
		$item_id = (int) $item_id;
		$this->db->query("DELETE FROM foreup_menu_buttons
			WHERE item_id = {$item_id} AND category_id > 0");

		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}

		$this->db->delete('item_menu_categories', [
			'item_id' => (int) $item_id, 
			'course_id' => $course_id
		]);

		return $this->db->affected_rows();
	}

	function save_item_category($item_id, $category_id, $course_id = null){
		
		$item_id = (int) $item_id;
		$category_id = (int) $category_id;

		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}
		$course_id = (int) $course_id;

		// For now, we are only allowing 1 menu category per item
		$this->delete_item_category($item_id, $course_id);

		$this->db->query("INSERT IGNORE INTO foreup_item_menu_categories (item_id, category_id, course_id)
			VALUES ({$item_id}, {$category_id}, {$course_id})");

		$this->update_item_menu_buttons($item_id, $course_id);

		return true;
	}

	function save_category($category_id, $data){
		
		$data = elements(array('name'), $data);
		$data['course_id'] = (int) $this->session->userdata('course_id');

		if(!empty($category_id)){
			$this->db->where('category_id', (int) $category_id);
			$this->db->update('menu_categories', $data);
		
		}else{
			$existing_row = $this->db->select('category_id')
				->from('menu_categories')
				->where('name', trim($data['name']))
				->where('course_id', $data['course_id'])
				->get()->row_array();
			
			if(!$existing_row){
				$this->db->insert('menu_categories', $data);
				$category_id = $this->db->insert_id();
			}else{
				$category_id = $existing_row['category_id'];
			}
		}
		
		return (int) $category_id;
	}

	function get_categories($params = null){
		
		$params = elements(array('search'), $params);

		$this->db->select('name, name AS label, category_id, category_id AS value')
			->from('menu_categories')
			->where('course_id', (int) $this->session->userdata('course_id'));

		if(!empty($params['search'])){
			$this->db->like('name', $params['search']);
		}

		return $this->db->get()->result_array();
	}

	function get_item_category($item_id){
		
		return $this->db->select('menu_categories.name, menu_categories.category_id')
			->from('menu_categories')
			->join('item_menu_categories', 'item_menu_categories.category_id = menu_categories.category_id', 'inner')
			->where('item_id', (int) $item_id)
			->where('item_menu_categories.course_id', (int) $this->session->userdata('course_id'))
			->get()
			->row_array();
	}

	function delete_item_subcategory($item_id, $course_id = null){
		
		$item_id = (int) $item_id;
		$this->db->query("DELETE FROM foreup_menu_buttons
			WHERE item_id = {$item_id} AND subcategory_id > 0");

		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}

		$this->db->delete('item_menu_subcategories', [
			'item_id' => (int) $item_id, 
			'course_id' => $course_id
		]);
		
		return $this->db->affected_rows();
	}

	function save_item_subcategory($item_id, $subcategory_id, $course_id = null){
		
		$item_id = (int) $item_id;
		$subcategory_id = (int) $subcategory_id;
		
		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}
		$course_id = (int) $course_id;

		// For now, we are only allowing 1 menu subcategory per item
		$this->delete_item_subcategory($item_id);

		$this->db->query("INSERT IGNORE INTO foreup_item_menu_subcategories (item_id, subcategory_id, course_id)
			VALUES ({$item_id}, {$subcategory_id}, {$course_id})");

		$this->update_item_menu_buttons($item_id, $course_id);

		return true;
	}

	function save_subcategory($subcategory_id, $data){
		
		$data = elements(array('name'), $data);
		$data['course_id'] = (int) $this->session->userdata('course_id');
		
		if(!empty($subcategory_id)){
			$this->db->where('subcategory_id', (int) $subcategory_id);
			$this->db->update('menu_subcategories', $data);
		
		}else{
			$existing_row = $this->db->select('subcategory_id')
				->from('menu_subcategories')
				->where('name', trim($data['name']))
				->where('course_id', $data['course_id'])
				->get()->row_array();

			if(!$existing_row){
				$this->db->insert('menu_subcategories', $data);
				$subcategory_id = $this->db->insert_id();				
			}else{
				$subcategory_id = $existing_row['subcategory_id'];
			}
		}
		
		return (int) $subcategory_id;
	}

	function get_subcategories($params = null){
		
		$params = elements(array('search'), $params);

		$this->db->select('name, name AS label, subcategory_id, subcategory_id AS value')
			->from('menu_subcategories')
			->where('course_id', (int) $this->session->userdata('course_id'));

		if(!empty($params['search'])){
			$this->db->like('name', $params['search']);
		}

		return $this->db->get()->result_array();
	}

	function get_item_subcategory($item_id){
		
		return $this->db->select('menu_subcategories.name, menu_subcategories.subcategory_id')
			->from('menu_subcategories')
			->join('item_menu_subcategories', 'item_menu_subcategories.subcategory_id = menu_subcategories.subcategory_id', 'inner')
			->where('item_id', (int) $item_id)
			->where('item_menu_subcategories.course_id', (int) $this->session->userdata('course_id'))
			->get()
			->row_array();
	}

	function save_menu_buttons($data, $update_order = true, $course_id = false){

		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}

		$buttons = array();
		foreach($data as $btn){
			$button = array();
			$button['category_id'] = (int) $btn['category_id'];
			$button['subcategory_id'] = (int) $btn['subcategory_id'];
			$button['item_id'] = (int) $btn['item_id'];
			if(empty($btn['order'])){
				$btn['order'] = 0;
			}
			$button['order'] = (int) $btn['order'];
			$button['course_id'] = $course_id;
			
			$buttons[] = $button;	
		}

		if($update_order){
			$sql = "INSERT INTO foreup_menu_buttons
				(`category_id`, `subcategory_id`, `item_id`, `course_id`, `order`) VALUES ";		
		}else{
			$sql = "INSERT IGNORE INTO foreup_menu_buttons
				(`category_id`, `subcategory_id`, `item_id`, `course_id`) VALUES ";					
		}

		foreach($buttons as $button){
			$sql .= "(".
				$button['category_id'].",".
				$button['subcategory_id'].",".
				$button['item_id'].",".
				$button['course_id'];

			if($update_order){
				$sql .= ',' .$button['order'];
			}
			$sql .= "),";
		}
		$sql = trim($sql, ",");
		
		if($update_order){
			$sql .= " ON DUPLICATE KEY UPDATE `order` = VALUES(`order`)";
		}
			
		$query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	// Refreshes the F&B buttons after modifying 
	// an item's menu category/subcategory
	function update_item_menu_buttons($item_id, $course_id = null){

		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}

		$row = $this->db->select('category_id')
			->from('foreup_item_menu_categories')
			->where('item_id', $item_id)
			->where('course_id', $course_id)
			->get()
			->row_array();
		$category_id = (int) $row['category_id'];

		$row = $this->db->select('subcategory_id')
			->from('foreup_item_menu_subcategories')
			->where('item_id', $item_id)
			->where('course_id', $course_id)
			->get()
			->row_array();
		$subcategory_id = 0;
		
		if(!empty($row['subcategory_id'])){
			$subcategory_id = (int) $row['subcategory_id'];	
		}

		// Clear out any current buttons for this item
		$this->db->query("DELETE FROM foreup_menu_buttons WHERE item_id = {$item_id}");
		
		$buttons = [];
		
		// Create a category button
		$buttons[] = [
			'item_id' => 0,
			'category_id' => $category_id,
			'subcategory_id' => 0
		];

		// Create a subcategory button "beneath" the category button
		if(!empty($subcategory_id)){
			$buttons[] = [
				'item_id' => 0,
				'category_id' => $category_id,
				'subcategory_id' => $subcategory_id
			];			
		}

		// Create item button
		$buttons[] = [
			'item_id' => $item_id,
			'category_id' => $category_id,
			'subcategory_id' => $subcategory_id
		];

		$this->save_menu_buttons($buttons, false, $course_id);
	}

	function get_menu_buttons(){

		$course_id = (int) $this->session->userdata('course_id');

		$query = $this->db->query("SELECT button.category_id, button.subcategory_id, button.item_id, 
				category.name AS category_name, subcategory.name AS subcategory_name, item.name AS item_name,
				button.`order`
			FROM foreup_menu_buttons AS button
			LEFT JOIN foreup_menu_categories AS category
				ON category.category_id = button.category_id
			LEFT JOIN foreup_menu_subcategories AS subcategory
				ON subcategory.subcategory_id = button.subcategory_id
			LEFT JOIN foreup_items AS item
				ON item.item_id = button.item_id
			WHERE button.course_id = {$course_id}
			AND (item.deleted = 0 OR button.item_id = 0 )
			GROUP BY button.category_id, button.subcategory_id, button.item_id");

		$rows = $query->result_array();

		$buttons = [];
		foreach($rows as $key => $row){

			if($row['category_id'] && !$row['subcategory_id'] && !$row['item_id']){
				$row['name'] = $row['category_name'];
			
			}else if($row['category_id'] && $row['subcategory_id'] && !$row['item_id']){
				$row['name'] = $row['subcategory_name'];
			
			}else if(
				($row['category_id'] && $row['subcategory_id'] && $row['item_id']) ||
				($row['category_id'] && $row['item_id'])
			){
				$row['name'] = $row['item_name'];
			
			}else{
				continue;
			}
			$row['order'] = (int) $row['order'];
			$buttons[] = $row;
		}

		return $buttons;
	}
}