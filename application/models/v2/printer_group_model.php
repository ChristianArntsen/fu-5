<?php
class Printer_group_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->cache = false;
		$this->default_printer = null;
	}
	
	function get($params = null){
		
		$params = elements(array('printer_group_id', 'item_id'), $params, null);
		
		if(!empty($params['printer_group_id'])){
			$this->db->where('printer_group_id', (int) $params['printer_group_id']);
		}	
		
		$this->db->select('printer_group.printer_group_id, printer_group.default_printer_id, printer_group.label, 
			default_printer.ip_address AS default_printer_ip_address');
		$this->db->from('printer_groups AS printer_group');
		$this->db->join('printers AS default_printer', 'default_printer.printer_id = printer_group.default_printer_id', 'left');
		$this->db->where('printer_group.course_id', $this->session->userdata('course_id'));
		$this->db->order_by('printer_group.label DESC');
		
		if(!empty($params['item_id'])){
			$this->db->join('item_printer_groups AS item_printer_group', 'item_printer_group.printer_group_id = printer_group.printer_group_id', 'inner');
			$this->db->where('item_printer_group.item_id', (int) $params['item_id']);
		}		
		
		$printer_groups = $this->db->get()->result_array();
		
		return $printer_groups;
	}
	
	// Returns printer data in an array for a drop down menu
	public function get_menu(){
		$printer_groups = $this->get();
		$menu = array();
		
		if(empty($printer_groups)){
			return $menu;
		}
		
		foreach($printer_groups as $printer_group){
			$menu[$printer_group['printer_group_id']] = $printer_group['label'];
		}
		return $menu;		
	}		
	
	function save($printer_group_id, $data){
		
		$data = elements(array('label', 'default_printer_id'), $data, null);
		$data['course_id'] = (int) $this->session->userdata('course_id');
		
		if(empty($data['default_printer_id'])){
			$data['default_printer_id'] = null;
		}else{
			$data['default_printer_id'] = (int) $data['default_printer_id'];
		}
		
		if($data['label'] === null){
			unset($data['label']);
		}		
		
		if(!empty($printer_group_id)){			
			$this->db->where('printer_group_id', $printer_group_id);
			$this->db->update('printer_groups', $data);
		
		}else{	
			$this->db->insert('printer_groups', $data);
			$printer_group_id = (int) $this->db->insert_id();
		}
		
		return (int) $printer_group_id;
	}
	
	function save_default_printers($printer_group_list){
		
		if(empty($printer_group_list)){
			return false;
		}
		
		$data = array();
		foreach($printer_group_list as $printer_group_id => $printer_id){
			
			if(empty($printer_id)){
				$printer_id = null;
			}
			$this->save($printer_group_id, array('default_printer_id' => $printer_id));
		}
		
		return true;
	}
	
	function save_item_printer_groups($item_id, $printer_groups){
		
		if(empty($item_id)){
			return false;
		}
		$this->db->delete('item_printer_groups', array('item_id' => (int) $item_id));
		
		if(empty($printer_groups)){
			return false;
		}
		
		$data = array();
		foreach($printer_groups as $printer_group_id){
			
			if(empty($printer_group_id)){
				continue;
			}
			$data[] = array('printer_group_id' => (int) $printer_group_id, 'item_id' => (int) $item_id);
		}
		
		if(!empty($data)){
			$this->db->insert_batch('item_printer_groups', $data);
		}	
		
		return true;
	}
	
	function delete($printer_group_id){
		
		$printer_group_id = (int) $printer_group_id;
		$course_id = (int) $this->session->userdata('course_id');
		
		$this->db->query("DELETE printer_group.*, item_printer_group.*
			FROM foreup_printer_groups AS printer_group
			LEFT JOIN foreup_item_printer_groups AS item_printer_group
				ON item_printer_group.printer_group_id = printer_group.printer_group_id
			WHERE printer_group.printer_group_id = {$printer_group_id}
				AND printer_group.course_id = {$course_id}");
		
		return $this->db->affected_rows();
	}
	
	// Gets the correct printer IP addresses for an item based on the 
	// printer groups it is attached to
	function get_item_printers($printer_group_ids, $terminal_id = false, $do_not_print = false){
		
		if($do_not_print){
			return array();
		}
		
		if(!is_array($printer_group_ids)){
			$printer_group_ids = array((int) $printer_group_ids);
		}
		
		if($this->cache !== false){
			$printer_groups = $this->cache;
			$default_printer = $this->default_printer;
		
		}else{
			$this->load->model('v2/Terminal_printers_model');
			if(!empty($terminal_id)){
				$rows = $this->Terminal_printers_model->get(array('terminal_id' => $terminal_id));
			}else{
				$rows = $this->get();
			}
			
			$printer_groups = array();
			foreach($rows as $printer_group){
				$printer_groups[$printer_group['printer_group_id']] = $printer_group;
			}
			$this->cache = $printer_groups;
			
			// Get default printer to be used if no printers were found
			$default_printer_id = $this->config->item('default_kitchen_printer');
			$this->load->model('v2/Printer_model');
			$default_printer = $this->Printer_model->get(array('printer_id' => $default_printer_id));
			$this->default_printer = $default_printer;
		}
		
		$printers = array();
		foreach($printer_group_ids as $printer_group_id){
			
			$group =& $printer_groups[$printer_group_id];
			if(isset($group['printer_id']) && $group['printer_id'] === '0'){
				continue;
			}

			if(!empty($group['printer_ip_address'])){
				$printers[] = $group['printer_ip_address'];
			
			}else if(!empty($group['default_printer_ip_address'])){
				$printers[] = $group['default_printer_ip_address'];
			}
		}
		
		// If no printers were attached to item, add the default kitchen printer
		if(!empty($default_printer) && empty($printers)){
			$printers[] = $default_printer['ip_address'];
		}
		
		return $printers;	
	}
}
