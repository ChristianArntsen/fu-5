<?php
class Pass_item_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->model('Price_class');
		$this->teesheets = [];
	}

	function save($item_id, $pass_data = [], $is_shared = false){
		
		$this->db->delete('pass_definitions', ['item_id' => $item_id]);

		if(empty($pass_data['pass_expiration_date']) || $pass_data['pass_expiration_date'] == ''){
			$pass_data['pass_expiration_date'] = null;
		}else{
			$pass_data['pass_expiration_date'] = \Carbon\Carbon::parse($pass_data['pass_expiration_date'])->toDateString();
		}

		if(empty($pass_data['pass_days_to_expiration'])){
			$pass_data['pass_days_to_expiration'] = null;
		}else{
			$pass_data['pass_days_to_expiration']= (int) $pass_data['pass_days_to_expiration'];
		}

		$rules = $this->prepare_rules($pass_data['pass_restrictions']);
		if(empty($rules)){
			$rules = null;
		}else{
			$rules = json_encode($rules);
		}

		if($pass_data['pass_price_class_id'] == 'none'){
			$pass_data['pass_price_class_id'] = null;
		}

		$this->db->insert('pass_definitions', [
			'item_id' => $item_id,
			'price_class_id' => $pass_data['pass_price_class_id'],
			'expiration_date' => $pass_data['pass_expiration_date'],
			'days_to_expiration' => $pass_data['pass_days_to_expiration'],
			'restrictions' => $rules,
			'multi_customer' => (int) $pass_data['pass_multi_customer'],
			'enroll_loyalty' => (int) $pass_data['pass_enroll_loyalty'],
		]);

		if($is_shared){
			$this->share_price_class_dependencies($item_id);
		}

		return $this->db->affected_rows();
	}

	function share_price_class_dependencies($item_id){

		$pass_item = $this->db->select("price_class_id, restrictions")
			->from('foreup_pass_definitions')
			->where('item_id', $item_id)
			->get()
			->row_array();

		$price_class_ids = [];
		if(!empty($pass_item['price_class_id'])){
			$price_class_ids[] = (int) $pass_item['price_class_id'];
		}

		$restrictions = json_decode($pass_item['restrictions'], true);
		if(!empty($restrictions)){
			foreach($restrictions as $rule){
				if(!empty($rule['price_class_id'])){
					$price_class_ids[] = (int) $rule['price_class_id'];
				}
			}
		}
		$price_class_ids = array_unique($price_class_ids);

		if(empty($price_class_ids)){
			return true;
		}

		$this->db->where('default', 0);
		$this->db->where_in('class_id', $price_class_ids);
		$this->db->update('price_classes', ['is_shared' => 1]);

		return true;
	}

	function apply_field_labels($rules = []){

		if(empty($rules)){
			return [];
		}

		foreach($rules as &$rule){
			
			if(empty($rule['conditions'])){
				continue;
			}

			foreach($rule['conditions'] as &$condition){
				if(!empty($condition['filter']) && is_array($condition['filter'])){
					foreach($condition['filter'] as &$filter){
						$filter['label'] = $this->get_field_label($filter);
					}
				}
				if(!empty($condition['value']) && is_array($condition['value'])){
					foreach($condition['value'] as &$value){
						$value['label'] = $this->get_field_label($value);
					}
				}
			}
		}

		usort($rules, function($rule1, $rule2){
			$a = $rule1['rule_number'];
			$b = $rule2['rule_number'];
			if($a == $b) {
				return 0;
			}
			return ($a < $b) ? -1 : 1;
		});

		return $rules;
	}

	function get_field_label($parameter){

		if(empty($parameter) || empty($parameter['field'])){
			return '';
		}
		$label = '';

		switch($parameter['field']){
			
			case 'timeframe':

				if(!empty($parameter['day_of_week'])){
					
					$days = array_map(function($day_number){
						$day_map = [
							1 => 'Mon',
							2 => 'Tue',
							3 => 'Wed',
							4 => 'Thu',
							5 => 'Fri',
							6 => 'Sat',
							7 => 'Sun'
						];
						return $day_map[$day_number];
					
					}, $parameter['day_of_week']);
					
					$label .= implode(', ', $days);
				}

				if(!empty($parameter['time'][0])){
					$start = \Carbon\Carbon::createFromFormat('Hi', $parameter['time'][0]);
					$end = \Carbon\Carbon::createFromFormat('Hi', $parameter['time'][1]);

					$label .= ' ('.$start->format('h:ia') .' to '. $end->format('h:ia').')';
				}

				if(!empty($parameter['date'][0])){
					$start = \Carbon\Carbon::parse($parameter['date'][0]);
					$end = \Carbon\Carbon::parse($parameter['date'][1]);

					$label .= ' '.$start->format('n/j/y') .' to '. $end->format('n/j/y');
				}
			break;
			case 'teesheet':
				$teesheet = $this->get_teesheet($parameter['id']);
				if($teesheet){
					$label = $teesheet['course_name'].' - '.$teesheet['title'];
				}
			break;
			case 'price_class':
				$price_class = $this->Price_class->get($parameter['id']);
				if($price_class){
					$label = $price_class['name'];
				}
			break;
		}

		return $label;
	}

	function get_teesheet($teesheet_id){

		if(!empty($this->teesheets)){
			if(!empty($this->teesheets[$teesheet_id])){
				return $this->teesheets[$teesheet_id];
			}
			return false;
		}

		$this->load->model('Teesheet');
		$teesheets = (array) $this->Teesheet->get_teesheets(
			(int) $this->session->userdata('course_id'),
			['include_shared' => true]
		);
		
		$this->teesheets = [];
		foreach($teesheets as $teesheet){
			$this->teesheets[(int) $teesheet['teesheet_id']] = $teesheet;
		}		

		if(!empty($this->teesheets[$teesheet_id])){
			return $this->teesheets[$teesheet_id];
		}
		return false;
	}

	function get_price_class($price_class_id){

	}

	private function prepare_rules($pass_rules = []){
		
		$rules = [];
		if(empty($pass_rules) || empty($pass_rules[0])){
			return [];
		}

		foreach($pass_rules as $rule){
			$rules[] = $this->filter_rule($rule);
		}

		return $rules;
	}

	private function filter_rule($rule){

		$rule = elements(['name', 'price_class_id', 'conditions', 'rule_number','type','items','uniq_id'], $rule, false);
		$rule['price_class_id'] = (int) $rule['price_class_id'];
		$rule['rule_number'] = (int) $rule['rule_number'];

		if(empty($rule['conditions'])){
			$rule['conditions'] = [];
			return $rule;
		}
		
		$conditions = [];
		foreach($rule['conditions'] as $condition){
			$condition = $this->filter_condition($condition);
			if($condition === false){
				continue;
			}
			$conditions[] = $condition;
		}	

		$rule['conditions'] = $conditions;
		return $rule;
	}

	// A condition parameter is either a value or filter
	private function filter_condition_parameter($parameter){

		switch($parameter['field']){
			case 'timeframe':
				$parameter = elements(['field', 'day_of_week', 'time', 'date'], $parameter);
				if(empty($parameter['time'][0]) || empty($parameter['time'][1])){
					$parameter['time'] = false;
				}
				if(empty($parameter['date'][0]) || empty($parameter['date'][1])){
					$parameter['date'] = false;
				}
			break;
			case 'teesheet':
			case 'price_class':
				$parameter = elements(['field', 'id'], $parameter);
				$parameter['id'] = (int) $parameter['id'];
			break;
			default:
				return false;
		}

		return $parameter;
	}

	private function filter_condition($condition){

		if(empty($condition['field'])){
			return false;
		}

		switch($condition['field']){
			case 'timeframe':
			case 'teesheet':
				$condition = elements(['field', 'value'], $condition, false);
				$condition['filter'] = false;
				$condition['operator'] = '=';

				if(empty($condition['value'][0])){
					$condition['value'] = [];
				}else{
					foreach($condition['value'] as &$value){
						$v = $this->filter_condition_parameter($value);
						if($v){
							$value = $v;
						}
					}
				}
			break;
			case 'total_used':
			case 'total_used_per_day':
			case 'total_used_per_week':
			case 'total_used_per_month':
			case 'total_used_per_year':
			case 'days_since_purchase':
				$condition = elements(['field', 'filter', 'operator', 'value'], $condition, false);

				if($condition['field'] == 'days_since_purchase'){
					$condition['filter'] = false;
				}

				if(empty($condition['filter'][0])){
					$condition['filter'] = [];
				}else{
					foreach($condition['filter'] as &$filter){
						$f = $this->filter_condition_parameter($filter);
						if($f){
							$filter = $f;
						}
					}
				}
			break;
		}

		return $condition;
	}	
}