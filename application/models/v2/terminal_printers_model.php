<?php
class Terminal_printers_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	function get($params){
		
		$params = elements(array('terminal_id'), $params, null);
		
		$terminal_join = 'printer_group.printer_group_id = terminal_printer.printer_group_id';
		if(!empty($params['terminal_id'])){
			$terminal_join .= ' AND terminal_printer.terminal_id = '.(int) $params['terminal_id'];
		}
		
		$this->db->select('terminal_printer.terminal_id, terminal_printer.printer_id, printer_group.default_printer_id,
			printer.label AS printer_label, printer.ip_address AS printer_ip_address,
			printer_group.printer_group_id, printer_group.label AS printer_group_label,
			default_printer.ip_address AS default_printer_ip_address');
		$this->db->from('printer_groups AS printer_group');
		$this->db->join('terminal_printers AS terminal_printer', $terminal_join, 'left');
		$this->db->join('printers AS printer', 'printer.printer_id = terminal_printer.printer_id', 'left');
		$this->db->join('printers AS default_printer', 'default_printer.printer_id = printer_group.default_printer_id', 'left');
		$this->db->where('printer_group.course_id', $this->session->userdata('course_id'));
		$this->db->order_by('printer_group.label', 'DESC');
		$terminal_printers = $this->db->get()->result_array();
		
		return $terminal_printers;
	}
	
	function save($data){
		
		$data = elements(array('terminal_id', 'printer_group_id', 'printer_id'), $data);
		
		if(empty($data['terminal_id']) || empty($data['printer_group_id'])){
			return false;
		}
		
		// Delete previous setting, in case user is overwriting an old setting
		$this->delete($data['terminal_id'], $data['printer_group_id']);
		
		// Insert new printer association
		if(isset($data['printer_id']) && $data['printer_id'] != ''){
			$this->db->insert('terminal_printers', $data);
		}
		
		return $this->db->affected_rows();
	}
	
	function save_terminal_printers($terminal_id, $terminal_printers){
		
		if(empty($terminal_printers) || empty($terminal_id)){
			return false;
		}
		
		foreach($terminal_printers as $printer_group_id => $printer_id){
			$this->save(array(
				'terminal_id' => $terminal_id,
				 'printer_group_id' => $printer_group_id,
				 'printer_id' => $printer_id
			));
		}	
		return true;
	}
	
	function delete($terminal_id, $printer_group_id){
		
		if(empty($terminal_id) || empty($printer_group_id)){
			return false;
		}
		
		$this->db->delete('terminal_printers', array('terminal_id' => $terminal_id, 'printer_group_id' => $printer_group_id));
		return $this->db->affected_rows();
	}
}
