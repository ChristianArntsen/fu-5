<?php
class Data_Request_Model extends CI_Model
{
	/**
	 * @param $report
	 * @param $input
	 * @return \fu\reports\ReportParameters
	 * @throws \fu\Exceptions\InvalidArgument
	 */
	public function createDataReportParam($report, $input)
	{
		$reportParameters = new \fu\reports\ReportParameters();
		$reportParameters->loadFromReport($report);
		isset($input['columns']) ? $reportParameters->setColumns($input['columns']) : "";
		isset($input['group']) ? $reportParameters->setGroupBy($input['group']) : "";
		isset($input['order']) ? $reportParameters->setOrderBy($input['order']) : "";
		isset($input['filters']) ? $reportParameters->setFilters($input['filters']) : "";
		isset($input['date_range']) && !empty($input['date_range']['column'])? $reportParameters->setDate($input['date_range']) : "";
		isset($input['split_by']) ? $reportParameters->setSplitBy($input['split_by']) : "";
		return $reportParameters;
	}

	/**
	 * @param $report
	 * @param $input
	 * @return \fu\reports\ReportParameters
	 * @throws \fu\Exceptions\InvalidArgument
	 */
	public function createSummaryReportParam($report, $input)
	{
		$reportParameters = new \fu\reports\ReportParameters();
		$reportParameters->setColumns([]);
		$reportParameters->loadFromReportSummary($report);
		$reportParameters->setGroupBy([]);
		$reportParameters->setOrderBy([]);
		isset($input['summary_columns']) ? $reportParameters->setColumns($input['summary_columns']) : "";
		isset($input['filters']) ? $reportParameters->setFilters($input['filters']) : "";
		isset($input['date_range']) ? $reportParameters->setDate($input['date_range']) : "";
		return $reportParameters;
	}
}