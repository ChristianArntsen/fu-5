<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('array');

		$this->upload_path = FCPATH.'archives/images/'.$this->session->userdata('course_id').'/default';
		$this->upload_path_thumb = FCPATH.'archives/images/'.$this->session->userdata('course_id').'/thumbnails';
		$this->upload_path_preview = FCPATH.'archives/images/'.$this->session->userdata('course_id').'/previews';

		$this->upload_url =$this->upload_url($this->session->userdata('course_id'));
		$this->upload_url_thumb =$this->upload_url_thumb($this->session->userdata('course_id'));
		$this->upload_url_preview =$this->upload_url_preview($this->session->userdata('course_id'));
	}

    public function upload_url($course_id = false) {
		$baseUrl = $this->config->item("urls")["files"];
        if ($course_id) {
            return $baseUrl.'/'.$course_id.'/default';
        }
        else {
            return $baseUrl.'/'.$this->session->userdata('course_id').'/default';
        }

    }

    public function upload_url_thumb($course_id = false) {
		$baseUrl = $this->config->item("urls")["files"];
        if ($course_id) {
            return $baseUrl.'/'.$course_id.'/thumbnails';
        }
        else {
            return $baseUrl.'/'.$this->session->userdata('course_id').'/thumbnails';
        }

    }

    public function upload_url_preview($course_id = false) {
		$baseUrl = $this->config->item("urls")["files"];
        if ($course_id) {
            return $baseUrl.'/'.$course_id.'/previews';
        }
        else {
            return $baseUrl.'/'.$this->session->userdata('course_id').'/previews';
        }

    }

    public function save_file($image_id = null, $data)
	{
		$data = elements(array('course_id', 'label', 'module', 'filename', 'width', 'height', 'filesize', 'date_created', 'saved'), $data);

		if(!empty($image_id)){
			foreach($data as $key => $val){
				if($val === false){ unset($data[$key]); }
			}
			$success = $this->db->update('images', $data, 'image_id = '.(int) $image_id);
		}else{
			$success = $this->db->insert('images', $data);
			$image_id = $this->db->insert_id();
		}

		if($success){
			return $image_id;
		}else{
			return false;
		}
	}

	public function get_files($module = '')
	{
        $course_ids = array();
        $this->load->model('course');
        $this->course->get_linked_course_ids($course_ids, 'shared_customers', $this->session->userdata('course_id'));

        if ($module != ''){
			$this->db->where('module', $module);
		}

		return $this->db->select()
			->from('images')
			->where_in('course_id', $course_ids)
			->where('deleted', 0)
			->where('saved', 1)
			->get()
			->result_array();
	}

	public function search($query = '', $module = '')
	{
        $course_ids = array();
        $this->load->model('course');
        $this->course->get_linked_course_ids($course_ids, 'shared_customers', $this->session->userdata('course_id'));

        if ($module != ''){
			$this->db->where('module', $module);
		}

		$words = explode(' ',$query);
		if(!empty($query)){
			$regex = implode('|', $words);
			$this->db->where("label REGEXP", $regex);
		}

		$results = $this->db->select()
			->from('images')
			->where_in('course_id', $course_ids)
			->where('deleted', 0)
			->where('saved', 1)
			->get()
			->result_array();

		return $results;
	}

	public function delete_file($file_id)
	{
        $course_ids = array();
        $this->load->model('course');
        $this->course->get_linked_course_ids($course_ids, 'shared_customers', $this->session->userdata('course_id'));

        $file = $this->get_file($file_id);
		if (!$this->db->where('image_id', $file_id)->where_in('course_id', $course_ids)->delete('images'))
		{
			return false;
		}

		if(isset($file['filename'])){
			unlink($this->upload_path.'/'.$file['filename']);
			unlink($this->upload_path_thumb.'/'.$file['filename']);
			unlink($this->upload_path_preview.'/'.$file['filename']);
		}
		return true;
	}

	public function get_file($file_id)
	{
        $course_ids = array();
        $this->load->model('course');
        $this->course->get_linked_course_ids($course_ids, 'shared_customers', $this->session->userdata('course_id'));

        $query = $this->db->select()
			->from('images')
			->where('image_id', $file_id)
			->where_in('course_id', $course_ids)
			->where('deleted', 0)
			->get();

		if($query->num_rows() <= 0){
			$row['image_id'] = 0;
			$row['url'] = base_url().'images/no-image.png';
			$row['thumb_url'] = base_url().'images/no-image.png';
		}else{
			$row = $query->row_array();
			$row['url'] = $this->upload_url($row['course_id']) .'/'.$row['filename'].'?ts='.strtotime($row['date_updated']);
			$row['thumb_url'] = $this->upload_url_thumb($row['course_id']) .'/'.$row['filename'].'?ts='.strtotime($row['date_updated']);
		}

		return $row;
	}

	public function get_url($image_id = false){
        $course_ids = array();
        $this->load->model('course');
        $this->course->get_linked_course_ids($course_ids, 'shared_customers', $this->session->userdata('course_id'));

        if(!empty($image_id)){
			$this->db->select('filename, date_updated, course_id');
            $this->db->from('images');
            $this->db->where('image_id', $image_id);
            $this->db->where('deleted', 0);
            $this->db->where('saved', 1);
            $this->db->where_in('course_id', $course_ids);
            $query = $this->db->get();

			if($query->num_rows() <= 0){
				$url = base_url().'images/no-image.png';
			}else{
				$row = $query->row_array();
                $url = $this->upload_url($row['course_id']);
                $url .= '/'.$row['filename'].'?ts='.strtotime($row['date_updated']);
			}
		}else{
			$url = base_url().'images/no-image.png';
		}

		return $url;
	}

	public function get_thumb_url($image_id = false, $type = false){
        $course_ids = array();
        $this->load->model('course');
        $this->course->get_linked_course_ids($course_ids, 'shared_customers', $this->session->userdata('course_id'));
        if(!empty($image_id)){
            $this->db->select('filename, date_updated, course_id');
            $this->db->from('images');
            $this->db->where('image_id', $image_id);
            $this->db->where('deleted', 0);
            $this->db->where('saved', 1);
            $this->db->where_in('course_id', $course_ids);
            $query = $this->db->get();

            if($query->num_rows() <= 0){
                $url = base_url().'images/no-image.png';
            }else{
                $row = $query->row_array();
                $url = $this->upload_url_thumb($row['course_id']);
                $url .= '/'.$row['filename'].'?ts='.strtotime($row['date_updated']);
            }
        }else if ($type == 'person'){
            $url = base_url().'images/profiles/profile_picture.png';
        }else{
            $url = base_url().'images/no-image.png';
        }

        return $url;
    }

    private function make_paths($course_id = false){
   		
   		if(empty($course_id)){
   			$course_id = $this->session->userdata('course_id'); 
   		}

   		$this->images_path = 'archives/images';
		$this->course_path = $this->images_path.'/'.$course_id;
		$this->upload_path = $this->images_path.'/'.$course_id.'/default';
		$this->upload_path_thumb = $this->images_path.'/'.$course_id.'/thumbnails';
		$this->upload_path_preview = $this->images_path.'/'.$course_id.'/previews';

		if(!is_dir($this->images_path)){
			mkdir($this->images_path, 0755, true);
		}
		if(!is_dir($this->course_path)){
			mkdir($this->course_path, 0755, true);
		}	
		if(!is_dir($this->upload_path)){
			mkdir($this->upload_path, 0755, true);
		}

		if(!is_dir($this->upload_path_thumb)){
			mkdir($this->upload_path_thumb, 0755, true);
		}

		if(!is_dir($this->upload_path_preview)){
			mkdir($this->upload_path_preview, 0755, true);
		}

		$this->upload_url = $this->image->upload_url($this->session->userdata('course_id'));
		$this->upload_url_thumb = $this->image->upload_url_thumb($this->session->userdata('course_id'));
		$this->upload_url_preview = $this->image->upload_url_preview($this->session->userdata('course_id')); 	
    }

    public function upload($filename, $imageData, $course_id = false){
		$this->make_paths($course_id);
    	$this->load->library('image_lib');
		file_put_contents($this->upload_path.'/'.$filename, $imageData);

		$this->image_lib->clear();
		$config['image_library'] = 'gd2';
		$config['source_image']	= $this->upload_path.'/'.$filename;
		$config['new_image'] = $this->upload_path_thumb.'/'.$filename;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 100;
		$config['height'] = 100;
		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		$config['width'] = 640;
		$config['height'] = 640;
		$config['new_image'] = $this->upload_path_preview.'/'.$filename;
		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		$this->transfer_to_s3($filename, $course_id);
		return true;
    }

	private function transfer_to_s3($fileName, $course_id = false){
		$s3 = new fu\aws\s3();
		
		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');	
		}
		
		$s3->uploadToFileBucket($this->upload_path."/".$fileName, $course_id.'/default/'.$fileName);
		$s3->uploadToFileBucket($this->upload_path_preview."/".$fileName, $course_id.'/previews/'.$fileName);
		$s3->uploadToFileBucket($this->upload_path_thumb."/".$fileName, $course_id.'/thumbnails/'.$fileName);
	}
}

