<?php

class Course extends CI_Model
{
	/*
	Determines if a given item_id is an item
	*/
	function exists($item_id)
	{
		$this->db->from('courses');
		$this->db->where("course_id = '$item_id'");
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows() == 1);
	}

	/*
	Returns all the items
	*/
	function get_all($limit = 10000, $offset = 0)
	{
		$this->db->from('courses');
        $this->db->join('courses_information as ci', 'ci.course_id = courses.course_id', 'left');
		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}

	function get_weather_update_list()
	{

		$this->db->from('courses');
		$this->db->where('longitude_centroid != ""');
		$this->db->where('latitude_centroid != ""');

		return $this->db->get();
	}

	function mark_weather_collected($course_id)
	{
		$this->db->where('course_id', $course_id);
		$this->db->update('courses', array('weather_last_collected' => gmdate('Y-m-d H:i:s')));
	}

	function get_stats()
	{
		$query = $this->db->query("SELECT SUM(IF(active_course = 1 AND demo = 0, 1, 0)) AS active, SUM(demo) AS demo, SUM(IF(ets_key != '', 1, 0)) AS ets, SUM(IF(mercury_id != '' AND ets_key = '', 1, 0)) AS mercury FROM foreup_courses");
		return $query->result_array();
	}

	function get_group_info($course_id = '', $selected = '')
	{
		$this->db->select("foreup_course_groups.group_id AS group_id, label, type, course_id, count(course_id) AS member_count, sum(CASE WHEN course_id = '$selected' THEN 1 ELSE 0 END) AS is_member");
		$this->db->from('course_groups');
		if ($course_id == '')
			$this->db->join('course_group_members', "course_groups.group_id = course_group_members.group_id", 'LEFT');
		else
			$this->db->join('course_group_members', "course_groups.group_id = course_group_members.group_id AND foreup_course_group_members.course_id = '$course_id'", 'LEFT');
		$this->db->group_by('group_id');

		return $this->db->get()->result_array();
	}

	function get_managed_group_info($course_id = '', $selected = '')
	{
		$this->db->select("foreup_course_groups.group_id AS group_id, label, type, course_id, count(course_id) AS member_count, sum(CASE WHEN course_id = '$selected' THEN 1 ELSE 0 END) AS is_management");
		$this->db->from('course_groups');
		if ($course_id == '')
			$this->db->join('course_group_management', "course_groups.group_id = course_group_management.group_id", 'LEFT');
		else
			$this->db->join('course_group_management', "course_groups.group_id = course_group_management.group_id AND course_group_management.course_id = '$course_id'", 'LEFT');
		$this->db->group_by('group_id');

		return $this->db->get()->result_array();
	}

	// Converts a course to old single IP address printing to new 
	// many-to-many printer setup
	function convert_to_multi_printers($course_id)
	{

		$this->load->model('v2/Printer_group_model');
		$this->load->model('v2/Printer_model');
		$this->load->model('v2/Terminal_printers_model');

		$course_id = (int)$course_id;
		$ip_rows = array();
		$hot_printer_id = false;
		$cold_printer_id = false;

		// Check if course already has cold printer group
		$cold_group = $this->db->select('printer_group_id')
			->from('printer_groups')
			->where(array('course_id' => $course_id, 'label' => 'Cold'))
			->get()->row_array();

		if (!empty($cold_group['printer_group_id'])) {
			$cold_printer_group_id = $cold_group['printer_group_id'];
		} else {
			$cold_printer_group_id = $this->Printer_group_model->save(null, array('label' => 'Cold'));
		}

		// Check if course already has hot printer group
		$hot_group = $this->db->select('printer_group_id')
			->from('printer_groups')
			->where(array('course_id' => $course_id, 'label' => 'Hot'))
			->get()->row_array();

		if (!empty($hot_group['printer_group_id'])) {
			$hot_printer_group_id = $cold_group['printer_group_id'];
		} else {
			$hot_printer_group_id = $this->Printer_group_model->save(null, array('label' => 'Hot'));
		}

		// Retrieve main hot/cold receipt IP address
		$this->db->select("webprnt_cold_ip, webprnt_hot_ip, webprnt_label_ip, webprnt_ip");
		$this->db->from('courses');
		$this->db->where('course_id', $course_id);
		$course_printers = $this->db->get()->row_array();

		// Course COLD printer
		if (!empty($course_printers['webprnt_cold_ip']) && substr_count($course_printers['webprnt_cold_ip'], '.') > 2) {
			$this->db->insert('printers', array(
				'course_id' => $course_id,
				'ip_address' => trim($course_printers['webprnt_cold_ip']),
				'label' => 'Cold'
			));
			$printer_id = (int)$this->db->insert_id();
			$this->Printer_group_model->save($cold_printer_group_id, array('default_printer_id' => $printer_id));
			$cold_printer_id = $printer_id;
		}

		// Course HOT printer
		if (!empty($course_printers['webprnt_hot_ip']) && substr_count($course_printers['webprnt_hot_ip'], '.') > 2) {
			$this->db->insert('printers', array(
				'course_id' => $course_id,
				'ip_address' => trim($course_printers['webprnt_hot_ip']),
				'label' => 'Hot'
			));
			$printer_id = (int)$this->db->insert_id();
			$this->Printer_group_model->save($hot_printer_group_id, array('default_printer_id' => $printer_id));
			$hot_printer_id = $printer_id;
		}

		// Course RECEIPT printer
		if (!empty($course_printers['webprnt_ip']) && substr_count($course_printers['webprnt_ip'], '.') > 2) {
			$this->db->insert('printers', array(
				'course_id' => $course_id,
				'ip_address' => trim($course_printers['webprnt_ip']),
				'label' => 'Receipt'
			));
			$printer_id = (int)$this->db->insert_id();
			$this->db->update('courses', array('webprnt_ip' => $printer_id), array('course_id' => $course_id));
		}

		// Course LABEL printer
		if (!empty($course_printers['webprnt_label_ip']) && substr_count($course_printers['webprnt_label_ip'], '.') > 2) {
			$this->db->insert('printers', array(
				'course_id' => $course_id,
				'ip_address' => trim($course_printers['webprnt_label_ip']),
				'label' => 'Barcode Label'
			));
			$printer_id = (int)$this->db->insert_id();
			$this->db->update('courses', array('webprnt_label_ip' => $printer_id), array('course_id' => $course_id));
		}

		// Retrieve terminal IP addresses
		$this->db->select("terminal_id, label AS terminal_name, receipt_ip, 
			cold_webprnt_ip, hot_webprnt_ip", false);
		$this->db->from('terminals');
		$this->db->where('course_id', $course_id);
		$terminals = $this->db->get()->result_array();

		// Loop through each terminal and translate IP addresses to printers
		foreach ($terminals as $terminal) {

			// SALES RECEIPT printer
			if (!empty($terminal['receipt_ip']) && substr_count($terminal['receipt_ip'], '.') > 2) {
				$this->db->insert('printers', array(
					'course_id' => $course_id,
					'ip_address' => trim($terminal['receipt_ip']),
					'label' => $terminal['terminal_name'] . ' Receipt'
				));
				$printer_id = (int)$this->db->insert_id();
				$this->db->update('terminals', array('receipt_ip' => $printer_id), array('terminal_id' => $terminal['terminal_id']));
			}

			// COLD kitchen printer
			if (!empty($terminal['cold_webprnt_ip']) && substr_count($terminal['cold_webprnt_ip'], '.') > 2) {
				$this->db->insert('printers', array(
					'course_id' => $course_id,
					'ip_address' => trim($terminal['cold_webprnt_ip']),
					'label' => $terminal['terminal_name'] . ' Cold'
				));
				$printer_id = (int)$this->db->insert_id();
				$this->Terminal_printers_model->save_terminal_printers($terminal['terminal_id'], array($cold_printer_group_id => $printer_id));
			}

			// HOT kitchen printer
			if (!empty($terminal['hot_webprnt_ip']) && substr_count($terminal['hot_webprnt_ip'], '.') > 2) {
				$this->db->insert('printers', array(
					'course_id' => $course_id,
					'ip_address' => trim($terminal['hot_webprnt_ip']),
					'label' => $terminal['terminal_name'] . ' Hot'
				));
				$printer_id = (int)$this->db->insert_id();
				$this->Terminal_printers_model->save_terminal_printers($terminal['terminal_id'], array($hot_printer_group_id => $printer_id));
			}
		}

		// Get all F&B inventory items with corresponding hot/cold setting
		$this->db->select('item_id, kitchen_printer');
		$this->db->from('items');
		$this->db->where('course_id', $course_id);
		$this->db->where('kitchen_printer >', 0);
		$this->db->where('food_and_beverage', 1);
		$items = $this->db->get()->result_array();

		// Loop through items and attach new printer groups to each item
		foreach ($items as $item) {

			$item_id = (int)$item['item_id'];

			if ($item['kitchen_printer'] == 1 && !empty($hot_printer_group_id)) {
				$this->db->query("INSERT IGNORE INTO foreup_item_printer_groups
					(item_id, printer_group_id) VALUES ({$item_id}, {$hot_printer_group_id})");

			} else if ($item['kitchen_printer'] == 2 && !empty($cold_printer_group_id)) {
				$this->db->query("INSERT IGNORE INTO foreup_item_printer_groups
					(item_id, printer_group_id) VALUES ({$item_id}, {$cold_printer_group_id})");
			}
		}

		$this->db->update('courses', array('multiple_printers' => 1), array('course_id' => $course_id));
		$this->config->set_item('multiple_printers', 1);
		$this->session->set_userdata('multiple_printers', 1);
		return true;
	}

	function get_group_courses($group_id, $filter = null)
	{

		if (empty($group_id)) {
			return false;
		}

		$this->db->select('courses.course_id,
			name, 
			active,
			area_id,
			name,
			address,
			city,
			state,
			postal,
			zip,
			country,
			website,
			area_code,
			timezone,
			open_time,
			close_time,
			latitude_centroid,
			longitude_centroid,
			phone,
			online_booking,
			online_booking_protected,
			booking_rules,
			email,
			no_show_policy,
			include_tax_online_booking,
			foreup_discount_percent,
			customer_credit_nickname,
			member_balance_nickname,
			ets_key,
			mercury_id,
			element_account_id');
		$this->db->from('course_groups');
		$this->db->join('course_group_members', 'course_group_members.group_id = course_groups.group_id');
		$this->db->join('courses', 'courses.course_id = course_group_members.course_id', 'left');
		$this->db->where('course_groups.type', 'linked');
		$this->db->where('course_groups.group_id', (int)$group_id);
		if ($filter) {
			$this->db->where($filter, 1);
		}
		$this->db->group_by('courses.course_id');
		$query = $this->db->get();
		$groups = $query->result_array();

		return $groups;
	}

    function is_using_api($course_id) {
        $this->db->from('api_key_permissions');
        $this->db->where ('course_id', $course_id);
        return $this->db->count_all_results() > 0;
    }

	function is_linked(&$group_ids, $search_type = false, $course_id = false)
	{
		$course_id = $course_id ? $course_id : $this->session->userdata('course_id');
		//Gathering a list of 'linked' course_groups that this course belongs to and returning true if any are found
		$this->db->from('course_groups');
		$this->db->select('course_groups.group_id');
		$this->db->join('course_group_members', 'course_group_members.group_id = course_groups.group_id');
		$this->db->where('type', 'linked');
		$this->db->where('course_group_members.course_id', $course_id);
		if ($search_type)
			$this->db->where("$search_type", 1);
		$this->db->group_by('group_id');
		$query = $this->db->get();
		//echo $this->db->last_query();
		$ids = $query->result_array();
		foreach ($ids as $id)
			$group_ids[] = $id['group_id'];
		return ($query->num_rows() > 0);
	}

	// Checks to make sure course is managing another course
	function is_managing_course($course_id)
	{
		if ($course_id == $this->session->userdata('course_id')) {
			return true;
		}

		$course_ids = array();
		if ($this->get_managed_course_ids($course_ids)) {
			return in_array($course_id, $course_ids);
		}

		return false;
	}

	function get_last_emailed_date($course_id)
	{
		$this->db->select('last_invoices_sent');
		$this->db->from('courses');
		$this->db->where('course_id', $course_id);
		$this->db->limit(1);
		$result = $this->db->get()->row_array();
		return $result['last_invoices_sent'];
	}

	function date_stamp_sent_invoices($course_id)
	{
		$this->db->where('course_id', $course_id);
		$this->db->limit(1);
		$this->db->update('courses', array('last_invoices_sent' => date('Y-m-d')));
	}

	function is_managing(&$group_ids)
	{
		//Gathering a list of 'linked' course_groups that this course belongs to and returning true if any are found
		$this->db->from('course_groups');
		$this->db->select('course_groups.group_id');
		$this->db->join('course_group_management', 'course_group_management.group_id = course_groups.group_id');
		$this->db->where('course_group_management.course_id', $this->session->userdata('course_id'));
		$this->db->group_by('group_id');
		$query = $this->db->get();
		//echo $this->db->last_query();
		$ids = $query->result_array();
		foreach ($ids as $id)
			$group_ids[] = $id['group_id'];
		return ($query->num_rows() > 0);
	}

	function get_linked_course_ids(&$course_ids, $search_type = false, $course_id = false)
	{

		if (empty($course_id)) {
			$course_id = $this->session->userdata('course_id');
		}

		//Gathering a list of 'linked' course_groups that this course belongs to and returning true if any are found
		$group_ids = array();
		if ($this->is_linked($group_ids, $search_type, $course_id)) {
			$this->db->select('course_id');
			$this->db->from('course_group_members');
			$this->db->where_in('group_id', array_values($group_ids));
			$this->db->group_by('course_id');
			$query = $this->db->get();
			//echo $this->db->last_query();
			$ids = $query->result_array();
			foreach ($ids as $id)
				$course_ids[] = $id['course_id'];
			return ($query->num_rows() > 0);
		}
		$course_ids[] = $course_id;
		return true;
	}

	function get_managed_course_ids(&$course_ids)
	{
		//Gathering a list of 'managed' course_groups that this course belongs to and returning true if any are found
		$group_ids = array();
		if ($this->is_managing($group_ids)) {
			$this->db->select('courses.course_id, courses.name');
			$this->db->from('course_group_members');
			$this->db->join('courses', 'courses.course_id = course_group_members.course_id', 'inner');
			$this->db->where_in('course_group_members.group_id', array_values($group_ids));
			$this->db->group_by('course_group_members.course_id');
			$query = $this->db->get();
			//echo $this->db->last_query();
			$ids = $query->result_array();
			foreach ($ids as $id) {
				$course_ids[$id['name']] = $id['course_id'];
			}
			return ($query->num_rows() > 0);
		}
		return true;
	}

	function get_new_dashboard_drop_down_filter()
	{
		$courses = array();
		$this->db->select('c.name AS course, c.course_id');
		$this->db->from('courses as c');
		$this->db->join("billing as b", "c.course_id = b.course_id AND b.deleted = 0", "inner");
		$this->db->where("b.teetimes_daily > 0 OR b.teetimes_weekly > 0");
		$this->db->order_by('c.name ASC');
		$this->db->group_by('c.course_id');

		$by_courses = $this->db->get();
		foreach ($by_courses->result() as $row) {
			$courses[] = array('label' => $row->course, 'type' => 'category', 'value' => $row->course_id);
		}

		return $courses;
	}

	function save_group_memberships($groups_data, $course_id)
	{
		$this->db->delete('course_group_members', array('course_id' => $course_id));
		foreach ($groups_data as $group_id)
			$this->db->insert('course_group_members', array('course_id' => $course_id, 'group_id' => $group_id));
	}

	function save_managed_groups($groups_data, $course_id)
	{
		$this->db->delete('course_group_management', array('course_id' => $course_id));
		foreach ($groups_data as $group_id)
			$this->db->insert('course_group_management', array('course_id' => $course_id, 'group_id' => $group_id));
	}

	function add_group($group_label, $group_type)
	{
		$this->db->insert('course_groups', array('label' => $group_label, 'type' => $group_type));
		return $this->db->insert_id();
	}

	function delete_group($group_id)
	{
		$this->db->delete('course_groups', array('group_id' => $group_id));
		$this->db->delete('course_group_members', array('group_id' => $group_id));
	}

	function count_all()
	{
		$this->db->from('courses');
		$this->db->where('active', 1);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular item
	*/
	function get_info($item_id)
	{
		$this->db->from('courses');
		$this->db->where("course_id = '$item_id'");
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj = new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('courses');

			foreach ($fields as $field) {
				$item_obj->$field = '';
			}

			return $item_obj;
		}
	}


	/*
	Gets information about a particular item
	*/
	function get_info_from_teesheet_id($teesheet_id)
	{
		$this->db->from('courses');
		$this->db->join('teesheet', 'courses.course_id=teesheet.course_id');
		$this->db->where("teesheet_id", $teesheet_id);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj = new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('courses');

			foreach ($fields as $field) {
				$item_obj->$field = '';
			}

			return $item_obj;
		}
	}

	function get_info_from_track_id($track_id)
	{
		$this->db->from('courses');
		$this->db->join('schedules', 'courses.course_id=schedules.course_id');
		$this->db->join('tracks', 'schedules.schedule_id=tracks.schedule_id');
		$this->db->where("track_id", $track_id);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj = new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('courses');

			foreach ($fields as $field) {
				$item_obj->$field = '';
			}

			return $item_obj;
		}
	}

	/*
	Get an item id given an item number
	*/
	function get_item_id($item_number)
	{
		$this->db->from('courses');
		$this->db->where("course_id = '$item_number'");
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->row()->course_id;
		}

		return false;
	}

	/*
	Gets information about multiple items
	*/
	function get_multiple_info($item_ids)
	{
		$this->db->from('courses');
		$this->db->where_in('course_id', $item_ids);
		$this->db->order_by("course_id", "asc");
		return $this->db->get();
	}

	function init_seasonal_pricing($course_id)
	{
		$this->load->model('Season');
		$this->load->model('Price_class');
		$this->load->model('Item');
		$this->config->set_item('seasonal_pricing', 1);
		$sql_log = array('changes' => array(), 'reversals' => array());
		$new_price_classes = array();

		// Get new tee times
		$seasonal_teetimes = $this->Item->get_teetimes($course_id, true);

		// If new times already exist, then don't run the rest of this, it's been done before.
		if (count($seasonal_teetimes) == 0) {
			$default_price_class = $this->Price_class->create_default($course_id);
			$new_price_classes['price_category_1'] = $default_price_class;
			// Create special tee time/cart items
			$this->Item->create_teetimes($course_id);
			// Get new tee times now that they're created
			$seasonal_teetimes = $this->Item->get_teetimes($course_id, true);
			// Get old tee times
			$old_teetimes = $this->Item->get_teetimes($course_id, false);
			// Add department and taxes to new tee times
//            {
//                // Green fees department update
//                $this->db->where('course_id', $course_id);
//                $this->db->where_in('item_id', array($seasonal_teetimes[0]['item_id'], $seasonal_teetimes[1]['item_id']));
//                $this->db->update('items', array('department' => $old_teetimes[3]['department']));
//                // Cart fees department update
//                $this->db->where('course_id', $course_id);
//                $this->db->where_in('item_id', array($seasonal_teetimes[2]['item_id'], $seasonal_teetimes[3]['item_id']));
//                $this->db->update('items', array('department' => $old_teetimes[1]['department']));
//                // Get green fee taxes
//                $this->db->from('items_taxes');
//                $this->db->where('item_id', $old_teetimes[3]['item_id']);
//                $this->db->limit(1);
//                $green_fee_tax = $this->db->get()->row_array();
//                // Save new green fee taxes
//                $green_fee_tax['item_id'] = $seasonal_teetimes[0]['item_id'];
//                $this->db->insert('items_taxes', $green_fee_tax);
//                $green_fee_tax['item_id'] = $seasonal_teetimes[1]['item_id'];
//                $this->db->insert('items_taxes', $green_fee_tax);
//                // Get cart fee taxes
//                $this->db->from('items_taxes');
//                $this->db->where('item_id', $old_teetimes[1]['item_id']);
//                $this->db->limit(1);
//                $cart_fee_tax = $this->db->get()->row_array();
//                // Save new green fee taxes
//                $cart_fee_tax['item_id'] = $seasonal_teetimes[2]['item_id'];
//                $this->db->insert('items_taxes', $cart_fee_tax);
//                $green_fee_tax['item_id'] = $seasonal_teetimes[3]['item_id'];
//                $this->db->insert('items_taxes', $cart_fee_tax);
//            }
			// Get a list of existing teesheets the course has
			$this->db->select('teesheet_id, default');
			$this->db->from('teesheet');
			$this->db->where('deleted', 0);
			$this->db->where('course_id', $course_id);
			$teesheets = $this->db->get()->result_array();

			$course_info = $this->Course->get_info($course_id);

			// Get Green Fee Types
			$this->load->model('Teesheet');
			$teesheet_id = $this->Teesheet->get_default($course_id);
			//echo 'default teesheet: '.$teesheet_id.' - course: '.$course_id;
			$green_fee_types = $this->Green_fee->get_types($teesheet_id, $course_id, true);

			// REPLACE REGULAR CUSTOMER PRICE CLASS ID
			$this->db->query("UPDATE foreup_customers SET price_class = $default_price_class WHERE course_id = $course_id AND price_class = 'price_category_1'");
			$sql_log['changes'][] = "UPDATE foreup_customers SET price_class = $default_price_class WHERE course_id = $course_id AND price_class = 'price_category_1';";
			$sql_log['reversals'][] = "UPDATE foreup_customers SET price_class = 'price_category_1' WHERE course_id = $course_id AND price_class = '$default_price_class';";

			//echo 'Green Fee Types ----';
			//print_r($green_fee_types);
			//print_r($teesheets);
			// Foreach to create new price classes
			foreach ($green_fee_types as $price_class_id => $type) {
				// SAVE Price Class
				if (str_replace('price_category_', '', $price_class_id) > 7) {
					$new_price_class_id = $this->Price_class->save(null, $type, 0, '', 1, null, null, $course_id);
					$new_price_classes[$price_class_id] = $new_price_class_id;
					// REPLACE CUSTOMER PRICE CLASS ID's
					$this->db->query("UPDATE foreup_customers SET price_class = $new_price_class_id WHERE course_id = $course_id AND price_class = '$price_class_id'");
					$sql_log['changes'][] = "UPDATE foreup_customers SET price_class = $new_price_class_id WHERE course_id = $course_id AND price_class = '$price_class_id';";
					$sql_log['reversals'][] = "UPDATE foreup_customers SET price_class = '$price_class_id' WHERE course_id = $course_id AND price_class = '$new_price_class_id';";
				}
			}

			// Get all prices
			$green_fee_prices = $this->Green_fee->get_teesheet_prices('', $course_id);
			//print_r($green_fee_prices['']['6270_8'][27]['6270_8']);
			//echo 'price test ';
			//echo $green_fee_prices['']['6270_8'][27]['6270_8']->price_category_2;
			//return;
			// Create a default season for each teesheet
			foreach ($teesheets as $teesheet) {
				$weekday_timeframe_id = '';
				$weekend_timeframe_id = '';
				$season_id = $this->Season->create_default($teesheet['teesheet_id'], $course_id, false);
				// Add default price class to each season
				$this->Season->add_price_class($season_id, $default_price_class);
				$green_fee_types = $this->Green_fee->get_types($teesheet['teesheet_id'], $course_id, true);
				//print_r($green_fee_types);
				foreach ($green_fee_types as $price_class_id => $type) {
					//echo 'going through green fee types';
					// Skip any blank price classes
					if ($type == '') {
						continue;
					}
					// SAVE Price Class
					$price_cat = str_replace('price_category_', '', $price_class_id);
					//echo 'price_cat '.$price_cat;
					if ($price_cat > 7) {
						// Save price class to season
						$this->Season->add_price_class($season_id, $new_price_classes[$price_class_id]);

						// Adding (non-default) time frames with prices to price classes
						$timeframe_data = array(
							'class_id' => $new_price_classes[$price_class_id],
							'season_id' => $season_id,
							'timeframe_name' => $course_info->weekend_fri || $course_info->weekend_sat || $course_info->weekend_sun ? 'Weekday' : 'Regular',
							'monday' => 1,
							'tuesday' => 1,
							'wednesday' => 1,
							'thursday' => 1,
							'friday' => $course_info->weekend_fri ? 0 : 1,
							'saturday' => $course_info->weekend_sat ? 0 : 1,
							'sunday' => $course_info->weekend_sun ? 0 : 1,
							'start_time' => '0000',
							'end_time' => '2399',
							'active' => 1,
							'price1' => $green_fee_prices[''][$course_id . '_7'][$teesheet['teesheet_id']][$course_id . '_7']->$price_class_id,
							'price2' => $green_fee_prices[''][$course_id . '_3'][$teesheet['teesheet_id']][$course_id . '_3']->$price_class_id,
							'price3' => $green_fee_prices[''][$course_id . '_5'][$teesheet['teesheet_id']][$course_id . '_5']->$price_class_id,
							'price4' => $green_fee_prices[''][$course_id . '_1'][$teesheet['teesheet_id']][$course_id . '_1']->$price_class_id
						);
						$weekday_timeframe_id = $this->Pricing->add_timeframe($timeframe_data);
						if ($course_info->weekend_fri || $course_info->weekend_sat || $course_info->weekend_sun) {
							$timeframe_data = array(
								'class_id' => $new_price_classes[$price_class_id],
								'season_id' => $season_id,
								'timeframe_name' => 'Weekend',
								'monday' => 0,
								'tuesday' => 0,
								'wednesday' => 0,
								'thursday' => 0,
								'friday' => $course_info->weekend_fri ? 1 : 0,
								'saturday' => $course_info->weekend_sat ? 1 : 0,
								'sunday' => $course_info->weekend_sun ? 1 : 0,
								'start_time' => '0000',
								'end_time' => '2399',
								'active' => 1,
								'price1' => $green_fee_prices[''][$course_id . '_8'][$teesheet['teesheet_id']][$course_id . '_8']->$price_class_id,
								'price2' => $green_fee_prices[''][$course_id . '_4'][$teesheet['teesheet_id']][$course_id . '_4']->$price_class_id,
								'price3' => $green_fee_prices[''][$course_id . '_6'][$teesheet['teesheet_id']][$course_id . '_6']->$price_class_id,
								'price4' => $green_fee_prices[''][$course_id . '_2'][$teesheet['teesheet_id']][$course_id . '_2']->$price_class_id
							);
							$weekend_timeframe_id = $this->Pricing->add_timeframe($timeframe_data);
						}

					} else {
						$conditions = array(
							1 => array(
								'condition' => 1,
								'name' => '',
								'start_time' => 0,
								'end_time' => 2399
							),
							2 => array(
								'condition' => $course_info->early_bird_hours_begin < $course_info->early_bird_hours_end,
								'name' => 'Early Bird',
								'start_time' => $course_info->early_bird_hours_begin,
								'end_time' => $course_info->early_bird_hours_end
							),
							3 => array(
								'condition' => $course_info->morning_hours_begin < $course_info->morning_hours_end,
								'name' => 'Morning',
								'start_time' => $course_info->morning_hours_begin,
								'end_time' => $course_info->morning_hours_end
							),
							4 => array(
								'condition' => $course_info->afternoon_hours_begin < $course_info->afternoon_hours_end,
								'name' => 'Afternoon',
								'start_time' => $course_info->afternoon_hours_begin,
								'end_time' => $course_info->afternoon_hours_end
							),
							5 => array(
								'condition' => (int)$course_info->twilight_hour != 2399,
								'name' => 'Twilight',
								'start_time' => $course_info->twilight_hour,
								'end_time' => $course_info->super_twilight_hour
							),
							6 => array(
								'condition' => (int)$course_info->super_twilight_hour != 2399,
								'name' => 'Super Twilight',
								'start_time' => $course_info->super_twilight_hour,
								'end_time' => 2399
							)
						);
						// Adding (default) time frames with prices to price classes (early bird, etc for Regular/Default as well as Weekday/Weekend)
						if ($conditions[$price_cat]['condition']) {
							//CREATE EARLY BIRD TIMEFRAME
							$timeframe_data = array(
								'class_id' => $default_price_class,
								'season_id' => $season_id,
								'timeframe_name' => ($conditions[$price_cat]['name'] != '' ? $conditions[$price_cat]['name'] : ($course_info->weekend_fri || $course_info->weekend_sat || $course_info->weekend_sun ? 'Weekday' : 'Everyday')),
								'monday' => 1,
								'tuesday' => 1,
								'wednesday' => 1,
								'thursday' => 1,
								'friday' => $course_info->weekend_fri ? 0 : 1,
								'saturday' => $course_info->weekend_sat ? 0 : 1,
								'sunday' => $course_info->weekend_sun ? 0 : 1,
								'start_time' => $conditions[$price_cat]['start_time'],
								'end_time' => $conditions[$price_cat]['end_time'],
								'active' => 1,
								'price1' => $green_fee_prices[''][$course_id . '_7'][$teesheet['teesheet_id']][$course_id . '_7']->$price_class_id,
								'price2' => $green_fee_prices[''][$course_id . '_3'][$teesheet['teesheet_id']][$course_id . '_3']->$price_class_id,
								'price3' => $green_fee_prices[''][$course_id . '_5'][$teesheet['teesheet_id']][$course_id . '_5']->$price_class_id,
								'price4' => $green_fee_prices[''][$course_id . '_1'][$teesheet['teesheet_id']][$course_id . '_1']->$price_class_id
							);
							$weekday_timeframe_id = $this->Pricing->add_timeframe($timeframe_data);
							if ($course_info->weekend_fri || $course_info->weekend_sat || $course_info->weekend_sun) {
								$timeframe_data = array(
									'class_id' => $default_price_class,
									'season_id' => $season_id,
									'timeframe_name' => $conditions[$price_cat]['name'] . ' Weekend',
									'monday' => 0,
									'tuesday' => 0,
									'wednesday' => 0,
									'thursday' => 0,
									'friday' => $course_info->weekend_fri ? 1 : 0,
									'saturday' => $course_info->weekend_sat ? 1 : 0,
									'sunday' => $course_info->weekend_sun ? 1 : 0,
									'start_time' => $conditions[$price_cat]['start_time'],
									'end_time' => $conditions[$price_cat]['end_time'],
									'active' => 1,
									'price1' => $green_fee_prices[''][$course_id . '_8'][$teesheet['teesheet_id']][$course_id . '_8']->$price_class_id,
									'price2' => $green_fee_prices[''][$course_id . '_4'][$teesheet['teesheet_id']][$course_id . '_4']->$price_class_id,
									'price3' => $green_fee_prices[''][$course_id . '_6'][$teesheet['teesheet_id']][$course_id . '_6']->$price_class_id,
									'price4' => $green_fee_prices[''][$course_id . '_2'][$teesheet['teesheet_id']][$course_id . '_2']->$price_class_id
								);
								$weekend_timeframe_id = $this->Pricing->add_timeframe($timeframe_data);
							}
						}
					}


					//echo 'Seasonal tee times<br/>';
					//print_r($seasonal_teetimes);
					//echo 'Old tee times<br/>';
					//print_r($old_teetimes);

					foreach ($old_teetimes as $index => $old_teetime) {
						$new_index = 0;
						if ($index < 2) {
							$new_index = 4;
						} else if ($index < 4) {
							$new_index = 2;
						} else if ($index < 6) {
							$new_index = 3;
						} else if ($index < 8) {
							$new_index = 1;
						}

						// These need to run every time a price new price class is converted since it is dependent on pricing
						// Change over quick buttons --- requires pricing
						// Old quick button item ids for tee times {item_id}_{price_category}_{teetime_type}
						$old_qb_item = "{$old_teetime['item_id']}_" . str_replace('price_category_', '', $price_class_id) . "_" . ($index + 1);
						$price_class_index = $price_class_id;
						$st_item_index = $new_index - 1;
						if ($price_cat < 8) {
							$price_class_index = 'price_category_1';
						}
						if ($index % 2 == 0 && $weekday_timeframe_id != '') {
							$new_wd_qb_item = "{$seasonal_teetimes[$st_item_index]['item_id']}_{$new_price_classes[$price_class_index]}_{$weekday_timeframe_id}_{$new_index}";
							$this->db->query("UPDATE foreup_quickbutton_items SET item_id = '$new_wd_qb_item' WHERE item_id = '$old_qb_item'");
							//if ($this->db->affected_rows() > 0) {
							$sql_log['changes'][] = "UPDATE foreup_quickbutton_items SET item_id = '$new_wd_qb_item' WHERE item_id = '$old_qb_item';";
							$sql_log['reversals'][] = "UPDATE foreup_quickbutton_items SET item_id = '$old_qb_item' WHERE item_id = '$new_wd_qb_item';";
							//}
						} else if ($weekend_timeframe_id != '') {
							$new_we_qb_item = "{$seasonal_teetimes[$st_item_index]['item_id']}_{$new_price_classes[$price_class_index]}_{$weekend_timeframe_id}_{$new_index}";
							$this->db->query("UPDATE foreup_quickbutton_items SET item_id = '$new_we_qb_item' WHERE item_id = '$old_qb_item'");
							//if ($this->db->affected_rows() > 0) {
							$sql_log['changes'][] = "UPDATE foreup_quickbutton_items SET item_id = '$new_we_qb_item' WHERE item_id = '$old_qb_item';";
							$sql_log['reversals'][] = "UPDATE foreup_quickbutton_items SET item_id = '$old_qb_item' WHERE item_id = '$new_we_qb_item';";
							//}
						}
						// New quick button item ids for tee times {item_id}_{class_id}_{seasonal_timeframe}_{price_id}

					}
				}
			}
			foreach ($old_teetimes as $index => $old_teetime) {
				$new_index = 0;
				if ($index < 2) {
					$new_index = 3;
				} else if ($index < 4) {
					$new_index = 1;
				} else if ($index < 6) {
					$new_index = 2;
				} else if ($index < 8) {
					$new_index = 0;
				}

				// These only need to run once, so we'll run after creating default
				if (isset($teesheet['default']) && $teesheet['default']) {
					// Change over loyalty --- doesn't require pricing
					$this->db->query("UPDATE foreup_loyalty_rates SET value = {$seasonal_teetimes[$new_index]['item_id']} WHERE course_id = $course_id AND type = 'item' AND value = {$old_teetime['item_id']}");
					$sql_log['changes'][] = "UPDATE foreup_loyalty_rates SET value = {$seasonal_teetimes[$new_index]['item_id']} WHERE course_id = $course_id AND type = 'item' AND value = {$old_teetime['item_id']};";
					$sql_log['reversals'][] = "UPDATE foreup_loyalty_rates SET value = {$old_teetime['item_id']} WHERE course_id = $course_id AND type = 'item' AND value = {$seasonal_teetimes[$new_index]['item_id']};";

					// Change over item kits --- doesn't require pricing
					$this->db->query("UPDATE foreup_item_kit_items SET item_id = {$seasonal_teetimes[$new_index]['item_id']} WHERE item_id = {$old_teetime['item_id']}");
					$sql_log['changes'][] = "UPDATE foreup_item_kit_items SET item_id = {$seasonal_teetimes[$new_index]['item_id']} WHERE item_id = {$old_teetime['item_id']};";
					$sql_log['reversals'][] = "UPDATE foreup_item_kit_items SET item_id = {$old_teetime['item_id']} WHERE item_id = {$seasonal_teetimes[$new_index]['item_id']};";

					// Change over punch cards --- doesn't require pricing
					$this->db->query("UPDATE foreup_punch_card_items SET item_id = {$seasonal_teetimes[$new_index]['item_id']} WHERE item_id = {$old_teetime['item_id']}");
					$sql_log['changes'][] = "UPDATE foreup_punch_card_items SET item_id = {$seasonal_teetimes[$new_index]['item_id']} WHERE item_id = {$old_teetime['item_id']};";
					$sql_log['reversals'][] = "UPDATE foreup_punch_card_items SET item_id = {$old_teetime['item_id']} WHERE item_id = {$seasonal_teetimes[$new_index]['item_id']};";
				}
			}
		}
		// Send notification email
		if (count($sql_log['changes'])) {
			$message = "<table>";
			$message .= "<tbody>";
			$message .= "<tr><td>Course:</td><td>$course_id</td></tr>";
			$message .= "<tr><td colspan=2>CHANGES</td></tr>";
			$message .= "<tr><td>INDEX</td><td>SQL</td></tr>";
			foreach ($sql_log['changes'] as $index => $change) {
				$message .= "<tr><td>$index</td><td>$change</td></tr>";
			}
			$message .= "<tr><td colspan=2>REVERSALS</td></tr>";
			$message .= "<tr><td>INDEX</td><td>SQL</td></tr>";
			foreach ($sql_log['reversals'] as $index => $reversal) {
				$message .= "<tr><td>$index</td><td>$reversal</td></tr>";
			}
			$message .= "</tbody>";
			$message .= "</table>";

		} else {
			$message = "Course ID: $course_id - No price switch changes made";
		}
	}

	/*
	Inserts or updates an item
	*/
	function save(&$item_data, $item_id = false, &$groups_data = array(), &$manage_groups_data = array())
	{
		if (!$item_id or !$this->exists($item_id)) {
			if ($this->db->insert('courses', $item_data)) {
				$item_data['course_id'] = $this->db->insert_id();
				$this->save_group_memberships($groups_data, $item_data['course_id']);
				$this->save_managed_groups($manage_groups_data, $item_data['course_id']);

				if ((int)$item_data['seasonal_pricing'] == 1) {
					$this->init_seasonal_pricing($item_data['course_id']);
				}
				return true;
			}
			return false;
		}

		// If seasonal pricing was turned on, make sure default price 
		// classes/seasons are set up
		if (!empty($item_data['seasonal_pricing']) && (int)$item_data['seasonal_pricing'] == 1) {
			$this->init_seasonal_pricing($item_id);
		}

		$this->save_group_memberships($groups_data, $item_id);
		$this->save_managed_groups($manage_groups_data, $item_id);
		$this->db->where('course_id', $item_id);
		$this->db->limit(1);
		return $this->db->update('courses', $item_data);
	}

	function update(&$course_data,$course_id = false)
	{
		if (!$course_id) {
			return false;
		}
		$this->db->where('course_id', $course_id);
		$this->db->limit(1);

		return $this->db->update('courses',$course_data);
	}

	/*
	Updates multiple items at once
	*/
	function update_multiple($item_data, $item_ids)
	{
		$this->db->where_in('course_id', $item_ids);
		return $this->db->update('courses', $item_data);
	}

	/*
	Deletes one item
	*/
	function delete($item_id)
	{
		$this->db->where("course_id = '$item_id'");
		$this->db->limit(1);
		return $this->db->update('courses', array('deleted' => 1));
	}

	/*
	Deletes a list of items
	*/
	function delete_list($item_ids)
	{
		$this->db->where_in('course_id', $item_ids);
		return $this->db->update('courses', array('deleted' => 1));
	}

	/*
	Get search suggestions to find items
	*/
	function get_search_suggestions($search, $limit = 25)
	{
		$suggestions = array();

		$this->db->from('courses');
		$this->db->like('name', $search);
		$this->db->order_by("name", "asc");
		$by_name = $this->db->get();
		foreach ($by_name->result() as $row) {
			$suggestions[] = array('label' => $row->name, 'value' => $row->course_id);
		}

		//only return $limit suggestions
		if (count($suggestions > $limit)) {
			$suggestions = array_slice($suggestions, 0, $limit);
		}
		return $suggestions;

	}

	function get_course_search_suggestions($search, $limit = 25)
	{
		$suggestions = array();

		$this->db->from('courses');
		$this->db->like('name', $search);
		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$by_name = $this->db->get();
		//echo json_encode(array('working'=>$by_name->num_rows()));
		foreach ($by_name->result() as $row) {
			$suggestions[] = array('value' => $row->course_id, 'label' => $row->name . ' - ' . $row->city . ', ' . $row->state);
		}

		//only return $limit suggestions
		if (count($suggestions > $limit)) {
			$suggestions = array_slice($suggestions, 0, $limit);
		}
		return $suggestions;

	}

	function get_teesheet_search_suggestions($search, $limit = 25)
	{
		$suggestions = array();

		$this->db->from('teesheet');
		$this->db->join('courses', 'courses.course_id = teesheet.course_id');
		$this->db->like('courses.name', $search);
		$this->db->where('teesheet.deleted', 0);
		$this->db->order_by("courses.name", "asc");
		$this->db->limit($limit);
		$by_name = $this->db->get();
		//$suggestions[] = array('value'=>'id', 'label'=> $this->db->last_query());
		//echo json_encode(array('working'=>$by_name->num_rows()));
		foreach ($by_name->result() as $row) {
			$suggestions[] = array('value' => $row->teesheet_id, 'label' => $row->name . ' - ' . $row->title);
		}

		//only return $limit suggestions
		if (count($suggestions > $limit)) {
			$suggestions = array_slice($suggestions, 0, $limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on items
	*/
	function search($search, $limit = 20, $offset = 0)
	{
		$this->db->from('courses');
		$this->db->where("(name LIKE '%" . $this->db->escape_like_str($search) . "%' or
		city LIKE '%" . $this->db->escape_like_str($search) . "%' or
		state LIKE '%" . $this->db->escape_like_str($search) . "%')");
		$this->db->order_by("name", "asc");
		// Just return a count of all search results
		if ($limit == 0)
			return $this->db->get()->num_rows();
		// Return results
		$this->db->offset($offset);
		$this->db->limit($limit);
		return $this->db->get();
	}

	/*
	 * get all course info
	 */
	public function get_all_course_info($limit = 10000, $offset = 0)
	{

		$this->db->from('courses');
//    if (!$this->permissions->is_super_admin())
//           $this->db->where('course_id',$this->session->userdata('course_id'));

		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);

		$res = $this->db->get();

		$data = array();

		if ($res->num_rows() > 0) {
			foreach ($res->result() as $course) {
				$d = array(
					'course_name' => '',
					'last_texts' => 0,
					'current_texts' => 0,
					'last_emails' => 0,
					'current_emails' => 0,
					'using_mercury' => 'NO'
				);

				if (!empty($course->mercury_id)) $d['using_mercury'] = 'YES';

				$d['course_name'] = $course->name;

				// get all emails and text for last month
				$this->db->select('type, count(*) as count');
				$this->db->from('marketing_campaigns');
				$this->db->where('is_sent', 1);
				$this->db->where('course_id', $course->course_id);
				$this->db->where("month(send_date) = (month(now()) - 1)");
				$this->db->group_by('type');

				$last_res = $this->db->get();

				if ($last_res->num_rows() > 0) {
					foreach ($last_res->result() as $lr) {
						if ($lr->type == 'email') {
							$d['last_emails'] = $lr->count;
						} else {
							$d['last_texts'] = $lr->count;
						}
					}
				}
				// get all emails and text for this month
				$this->db->select('type, count(*) as count');
				$this->db->from('marketing_campaigns');
				$this->db->where('is_sent', 1);
				$this->db->where('course_id', $course->course_id);
				$this->db->where("month(send_date) = month(now())");
				$this->db->group_by('type');

				$this_res = $this->db->get();

				if ($this_res->num_rows() > 0) {
					foreach ($this_res->result() as $tr) {
						if ($tr->type == 'email') {
							$d['current_emails'] = $tr->count;
						} else {
							$d['current_texts'] = $tr->count;
						}
					}
				}

				$data[] = $d;
			}
		}

		return $data;
	}

	/*
	 * get all the customer groups for the course
	 */
	function get_customer_groups($course_id){
		if (empty($course_id)) {
			return false;
		}

		$this->db->select('group_id, label');
		$this->db->from('customer_groups');
		$this->db->where('course_id', (int)$course_id);
		$query = $this->db->get();
		$groups = $query->result_array();

		return $groups;
	}
	
	/**
	 * Query forecastio API
	 * https://api.darksky.net/forecast/ca9ad8c3a99f431c350450afb660e2c5/37.8267,-122.4233,2017-04-10T12:00:00?exclude=currently,flags,minutely,hourly
	 *
	 * Returns suntimes in UTC
	 */
	function query_suntimes_from_api($course_id, $day) {
		$this->db->from("courses");
		$this->db->select(["latitude_centroid", "longitude_centroid", "timezone"]);
		$this->db->where('course_id', $course_id);
		$course_info = $this->db->get()->row_array();

		// Get local midnight time to pass in UTC to forecast API
		$tz = new DateTimeZone($course_info['timezone']);
		$date = new DateTime($day . 'T00:00:00', $tz);
		$date->setTimeZone(new DateTimeZone('UTC'));

		// Generate URL for API request
		$url = $this->config->item('forecastio_api_url') . '/' .
			$this->config->item('forecastio_api_key') . '/' . 
				$course_info['latitude_centroid'] . ',' . 
				$course_info['longitude_centroid'] . ',' . 
				$date->format('Y-m-d\TH:i:s') .
				'?exclude=currently,flags,minutely,hourly';

		$page = file_get_contents($url);
		$historical_weather = json_decode($page);

		$sunrise_dt = new DateTime('now', new DateTimeZone('UTC'));
		$sunrise_dt->setTimestamp($historical_weather->daily->data[0]->sunriseTime);
		$sunrise = $sunrise_dt->format('H:i:s');

		$sunset_dt = new DateTime('now', new DateTimeZone('UTC'));
		$sunset_dt->setTimestamp($historical_weather->daily->data[0]->sunsetTime);
		$sunset = $sunset_dt->format('H:i:s');

		return [
			'sunrise' => $sunrise,
			'sunset' => $sunset
		];
	}

	/**
	 * Get sunrise and sunset time for a given course/day.
	 * Uses cache when available.
	 */
	function get_suntimes($course_id, $day) {
		// Check if we already have the time in the database
		$this->db->from('course_suntimes');
		$this->db->where('course_id', $course_id);
		$this->db->where('day', $day);
		$this->db->limit(1);
        $db_suntimes = $this->db->get();

		// If not, let's query it from the API
		if ($db_suntimes->num_rows() === 0) { 
			$suntimes = $this->query_suntimes_from_api($course_id, $day);

            $sunrise = $suntimes['sunrise'];
            $sunset = $suntimes['sunset'];
            
			if ($sunrise !== '00:00:00' && $sunset !== '00:00:00') {
				// Don't save suntimes if they didn't work.
				$this->db->insert(
					'course_suntimes',
					[
						'course_id' => $course_id,
						'day' => $day,
						'sunrise' => $sunrise,
						'sunset' => $sunset
					]
				);
			}
        } else {
            $suntimes = $db_suntimes->row();
            $sunrise = $suntimes->sunrise;
            $sunset = $suntimes->sunset;
		}

		// Convert suntimes to course local time
		$this->db->from("courses");
		$this->db->select(["timezone"]);
		$this->db->where('course_id', $course_id);
		$course_info = $this->db->get()->row_array();

		$sunrise_dt = new DateTime($day . 'T' . $sunrise, new DateTimeZone('UTC'));
		$sunrise_dt->setTimeZone(new DateTimeZone($course_info['timezone']));
		$sunrise = $sunrise_dt->format('H:i:s');

		$sunset_dt = new DateTime($day . 'T' . $sunset, new DateTimeZone('UTC'));
		$sunset_dt->setTimeZone(new DateTimeZone($course_info['timezone']));
		$sunset = $sunset_dt->format('H:i:s');

		// Return the suntimes.
		return [
            'sunrise' => $sunrise,
			'sunset' => $sunset
		];
	}

	function get_suntimes_range($course_id, $start_time, $end_time) {
		$suntimes = [];
		$seconds_in_a_day = 86400;

		for ($i = $start_time; $i <= $end_time; $i += $seconds_in_a_day) {
			$day = date('Y-m-d', $i);
			$suntimes[$day] = $this->get_suntimes($course_id, $day);
		}

		return $suntimes;
	}
}
