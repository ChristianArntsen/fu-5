<?php
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\ValidationExceptionInterface;

class Report_tag extends CI_Model
{

	public function __construct()
	{

	}

	public function get_all()
	{
		$this->db->from('report_tags');
		$allTags = $this->db->get()->result();
		return $allTags;
	}


	public function insert($data)
	{
		$this->db->insert('reports_tags',$data);
		$id = $this->db->insert_id();
		return $id;
	}

}
?>
