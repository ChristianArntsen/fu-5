<?php

// BEST PRACTICES:
//
// TODO: Clean up and move DB result parsing to separate method (DRY)
// TODO: Require new objects of self to be initialized by constructor only
//       any initialized object should be valid, ideally.
// TODO: Move methods not working with single self (this) object to controller
// TODO: Move small small validation methods (and UTC Time Generator) to a common library
//

class contact_form_key extends CI_Model
{
	public $id = null;
	public $course_id = null;
	public $key = null;

	/*
	 * Loads data into this object. Returns true / false on success
	 * retrieves via course id
	 */
	public function retrieve()
	{
		// Validate ID
		if(!$this->validate_row_id($this->course_id)) {
			return false;
		}

		$this->db->from('contact_form_keys');
		$this->db->where("course_id = '$this->course_id'");

		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$row = $result->first_row();
			$this->id = (int)$row->id;
			$this->key = $row->key;

			return true;
		} else {
			return false;
		}
	}

	/*
	 * Loads data into this object. Returns true / false on success
	 * retrieves via course id
	 */
	public function retrieve_by_key()
	{
		$this->db->from('contact_form_keys');
		$this->db->where("key = '$this->key'");

		$result = $this->db->get();
		if($result->num_rows() > 0) {
			$row = $result->first_row();
			$this->id = (int)$row->id;
			$this->course_id = $row->course_id;

			return true;
		} else {
			return false;
		}
	}

	/*
	 * Deletes an entry
	 */
	public function delete(){
		// Validate ID
		if(!$this->validate_row_id($this->id)) {
			return false;
		}

		// Attempt delete
		$this->db->where('id', $this->id);
		$this->db->delete('contact_form_keys');

		return true;
	}

	/*
	 * Persists object into database storage. Returns true / false on success
 	*/
	public function create()
	{
		// Validate Self
		if(!$this->validate()){
			return false;
		}

		// Escape input
		$this->key = $this->db->escape_str($this->key);

		$query = "INSERT INTO `foreup_contact_form_keys`
				  SET
				  `course_id`='$this->course_id',
				  `key`='$this->key'
				  ";

		$result_bool = (bool)$this->db->simple_query($query);
		$this->id = $this->db->insert_id();

		return $result_bool;
	}

	/*
	 * Persists updated object into database storage. Returns true / false on success
	 */
	public function update(){
		// Validate Self
		if(!$this->validate()){
			return false;
		}
		if($this->id === null){
			return false;
		}

		// Escape input
		$this->key = $this->db->escape_str($this->key);

		$query = "UPDATE `foreup_contact_form_keys`
				  SET
				  `course_id`='$this->course_id',
				  `key`='$this->key'
				  WHERE `id`='$this->id'
				  ";

		return $this->db->simple_query($query);
	}

	/*
	 * Validates the data
	 */
	private function validate(){
		if($this->id !== null && (!$this->validate_row_id($this->id))){
			return false;
		}

		if(!$this->validate_row_id($this->course_id)){
			return false;
		}

		if(!$this->validate_string($this->key, false, 5, 32)){
			return false;
		}

		// No errors
		return true;
	}

	/*
	 * Validates a row id
	 */
	private function validate_row_id($id){
		if(!is_integer($id) || $id < 1) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * Validates a string
	 */
	private function validate_string($vstring, $nullable=false, $min_len=false, $max_len=false){
		if($vstring === null && $nullable === true){
			return true;
		}

		if(!is_string($vstring)) {
			return false;
		}

		if($min_len !== false && is_int($min_len)){
			if(strlen($vstring) < $min_len) return false;
		}

		if($max_len !== false && is_int($max_len)){
			if(strlen($vstring) > $max_len) return false;
		}

		return true;
	}

}