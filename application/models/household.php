<?php
class Household extends CI_Model
{
	/*
	Determines if a given person_id is a household head
	*/
	function get_id($person_id)
	{
		$this->db->from('households');
		$this->db->where("(household_head_id = {$person_id})");
		$this->db->limit(1);
        $result = $this->db->get()->row_array();

        if(empty($result['household_id'])){
        	return false;
        }

		return $result['household_id'];
	}
	
	/*
	Determines if a given person_id is a household head
	*/
	function is_head($person_id)
	{
		$this->db->from('households');
		$this->db->where('household_head_id',$person_id);
		$this->db->limit(1);
        $query = $this->db->get();
	
		return ($query->num_rows()==1);
	}
	
    /*
	Determines if a given person_id is a household member
	*/
	function is_member($person_id)
	{
		$this->db->from('household_members');
		$this->db->where('household_member_id',$person_id);
		$this->db->limit(1);
        $query = $this->db->get();
	
		return ($query->num_rows()==1);
	}
	
	/*
	Gets info for the household head
	*/
	function get_head($person_id)
	{
		//return $this->db->query("SELECT * FROM foreup_people WHERE person_id = (SELECT household_head_id FROM foreup_households WHERE household_id = (SELECT household_id FROM foreup_household_members WHERE household_member_id = $person_id LIMIT 1) LIMIT 1) LIMIT 1")->row_array();
		
		
		$this->db->from('households');
		$this->db->join('household_members', 'household_members.household_id = households.household_id');
		$this->db->join('people', 'people.person_id = households.household_head_id');
		$this->db->where('household_member_id',$person_id);
		$this->db->limit(1);
        $query = $this->db->get();
	
		return $query->row_array();
	}

	/*
	Gets info for the household members
	*/
	function get_members($person_id)
	{
		//return $this->db->query("SELECT * FROM foreup_people WHERE person_id IN (SELECT household_member_id FROM foreup_household_members WHERE household_id = (SELECT household_id FROM foreup_households WHERE household_head_id = $person_id LIMIT 1))")->result_array();
		
		$this->db->from('household_members');
		$this->db->join('households', 'household_members.household_id = households.household_id');
		$this->db->join('people', 'people.person_id = household_members.household_member_id');
		$this->db->where('household_head_id',$person_id);
		//$this->db->limit(1);
        $query = $this->db->get();
	
		return $query->result_array();
	}

	/*
	Save household
	*/
	function save($members, $head_id, $override = false)
	{
        if ((empty($members) || count($members) == 0) && !$override) {
            return true;
        }
		$household_id = $this->get_id($head_id);
		$employee_id = $this->session->userdata('person_id');
		$course_id = $this->session->userdata('course_id');
		$household_head_info = $this->Customer->get_info($head_id,$course_id);
		if ($household_id)
		{
			$this->delete_members($household_id);
		}
		else 
		{
			$this->db->insert('households', array('household_head_id'=>$head_id));
			$household_id = $this->db->insert_id();
		}
		$member_array = array();
		
		foreach($members as $member)
		{
			// TRANSFER ALL MEMBER ACCOUNT BALANCES TO HOUSEHOLD HEAD
			$customer_info = $this->Customer->get_info($member);
			if ($customer_info->account_balance != 0) {
				// TRANSFER FUNDS OUT OF MEMBER ACCOUNT
				$account_transaction_data=array(
					'course_id' =>$course_id,
					'trans_customer'=>$member,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_household'=>$member,
					'trans_comment'=>'Transferring funds to Household Head',
					'trans_description'=>'Household Account Consolidation',
					'trans_amount'=>-$customer_info->account_balance,
					'running_balance'=>0
				);
				$this->Account_transactions->insert('customer', $account_transaction_data);
				// TRANSFER FUNDS TO HEAD ACCOUNT
				$household_head_info->account_balance += $customer_info->account_balance;
				$account_transaction_data=array(
					'course_id' =>$course_id,
					'trans_customer'=>$member,
					'trans_household'=>$head_id,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_comment'=>'Transferring funds to Household Head',
					'trans_description'=>'Household Account Consolidation',
					'trans_amount'=>$customer_info->account_balance,
					'running_balance'=>$household_head_info->account_balance
				);
				$this->Account_transactions->insert('customer', $account_transaction_data);
			}
			if ($customer_info->member_account_balance != 0) {
				// TRANSFER FUNDS OUT OF MEMBER ACCOUNT
				$account_transaction_data=array(
					'course_id' =>$course_id,
					'trans_customer'=>$member,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_household'=>$member,
					'trans_comment'=>'Transferring funds to Household Head',
					'trans_description'=>'Household Account Consolidation',
					'trans_amount'=>-$customer_info->member_account_balance,
					'running_balance'=>0
				);
				$this->Account_transactions->insert('member', $account_transaction_data);
				// TRANSFER FUNDS TO HEAD ACCOUNT
				$household_head_info->member_account_balance += $customer_info->member_account_balance;
				$account_transaction_data=array(
					'course_id' =>$course_id,
					'trans_customer'=>$member,
					'trans_household'=>$head_id,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_comment'=>'Transferring funds to Household Head',
					'trans_description'=>'Household Account Consolidation',
					'trans_amount'=>$customer_info->member_account_balance,
					'running_balance'=>$household_head_info->member_account_balance
				);
				$this->Account_transactions->insert('member', $account_transaction_data);
			}
			if ($customer_info->invoice_balance != 0) {
				// TRANSFER FUNDS OUT OF MEMBER ACCOUNT
				$account_transaction_data=array(
					'course_id' =>$course_id,
					'trans_customer'=>$member,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_household'=>$member,
					'trans_comment'=>'Transferring funds to Household Head',
					'trans_description'=>'Household Account Consolidation',
					'trans_amount'=>-$customer_info->invoice_balance,
					'running_balance'=>0
				);
				$this->Account_transactions->insert('invoice', $account_transaction_data);
				// TRANSFER FUNDS TO HEAD ACCOUNT
				$household_head_info->invoice_balance += $customer_info->invoice_balance;
				$account_transaction_data=array(
					'course_id' =>$course_id,
					'trans_customer'=>$member,
					'trans_household'=>$head_id,
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_user'=>$employee_id,
					'trans_comment'=>'Transferring funds to Household Head',
					'trans_description'=>'Household Account Consolidation',
					'trans_amount'=>$customer_info->invoice_balance,
					'running_balance'=>$household_head_info->invoice_balance
				);
				$this->Account_transactions->insert('invoice', $account_transaction_data);
			}
			// UPDATE MEMBER TOTALS
			$this->db->where('course_id', $course_id);
			$this->db->where('person_id', $member);
			$this->db->limit(1);
			$this->db->update('customers', array(
				'account_balance'=>0, 
				'member_account_balance'=>0, 
				'invoice_balance'=>0));
			$member_array[] = array('household_id'=>$household_id, 'household_member_id'=>$member);
		}
		$this->db->where('course_id', $course_id);
		$this->db->where('person_id', $head_id);
		$this->db->limit(1);
		$this->db->update('customers', array(
			'account_balance'=>$household_head_info->account_balance, 
			'member_account_balance'=>$household_head_info->member_account_balance, 
			'invoice_balance'=>$household_head_info->invoice_balance));

        if (count($member_array) > 0) {
            return $this->db->insert_batch('household_members', $member_array);
        }
        else {
            return true;
        }
	}
	
	/*
	Delete household members
	*/
	function delete_members($household_id)
	{
		$this->db->where('household_id', $household_id);
		return $this->db->delete('household_members');
	}
	/*
	Delete household
	*/
	function delete($household_id)
	{
		$this->db->where('household_id', $household_id);
		$this->db->limit(1);
		$this->db->delete('households');
		
		return $this->delete_members($household_id);
	}
	
}
?>
