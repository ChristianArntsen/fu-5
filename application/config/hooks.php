<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['post_controller_constructor'][] = array(
    'class'    => '',
    'function' => 'load_config',
    'filename' => 'load_config.php',
    'filepath' => 'hooks'
);

$hook['post_controller_constructor'][] = array(
    'class'    => 'RedirectSSL',
    'function' => 'redirect_ssl',
    'filename' => 'ssl_redirect.php',
    'filepath' => 'hooks'
);

$hook['display_override'] = array(
    'class' => 'DisplayHook',
    'function' => 'captureOutput',
    'filename' => 'DisplayHook.php',
    'filepath' => 'hooks'
);

$hook['post_config_loaded'] = array(
    'class' => 'SetPhpIni',
    'function' => 'setPhpSettings',
    'filename' => 'SetPhpIni.php',
    'filepath' => 'hooks'
);
