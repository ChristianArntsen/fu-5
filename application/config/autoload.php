<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Helper files
| 4. Custom config files
| 5. Language files
| 6. Models
|
*/

/*
| -------------------------------------------------------------------
|  Auto-load Packges
| -------------------------------------------------------------------
| Prototype:
|
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
|
*/

$autoload['packages'] = array();


/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| These are the classes located in the system/libraries folder
| or in your application/libraries folder.
|
| Prototype:
|
|	$autoload['libraries'] = array('database', 'session', 'xmlrpc');
*/

$autoload['libraries'] = array('database','form_validation','session','user_agent', 'pagination', 'permissions');


/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['helper'] = array('url', 'file');
*/

$autoload['helper'] = array('form','url','table','text','currency', 'html', 'download', 'base64', 'mailchimp', 'language', 'file', 'email');

/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['config'] = array('config1', 'config2');
|
| NOTE: This item is intended for use ONLY if you have created custom
| config files.  Otherwise, leave it blank.
|
*/

$autoload['config'] = array('environment', 'roost', 'twilio', 'ssl_redirect', 'authorizenet', 'json_web_token');


/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['language'] = array('lang1', 'lang2');
|
| NOTE: Do not include the "_lang" part of your file.  For example
| "codeigniter_lang.php" would be referenced as array('codeigniter');
|
*/

$autoload['language'] = array('auto_mailers','billings','common', 'config', 'customers', 'dashboard', 'employees', 'error', 'items', 'login', 'module','recipients', 'reports', 'reservations', 'sales','schedules','suppliers','receivings','giftcards', 'courses', 'item_kits', 'marketing_campaigns', 'teesheets', 'promotions', 'tournaments','invoices','food_and_beverage');


/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['model'] = array('model1', 'model2');
|
*/

$autoload['model'] = array('Account_transactions','Action','Appconfig','Auto_mailer', 'Auto_mailer_campaign','Person','Customer','Dash_data','Employee','Member_account_transactions','Module','Item', 'Item_taxes', 'Reservation', 'Sale', 'Sale_suspended', 'Table', 'Schedule', 'Supplier','Inventory','Receiving','Recipient','Giftcard', 'Punch_card', 'Course', 'Item_kit', 'Item_kit_taxes', 'Item_kit_items', 'Appfile', 'Marketing_campaign', 'Green_fee', 'Fee', 'Billing', 'Quickbutton', 'Tournament', 'Tournament_inventory_items', 'Tournament_winners','Invoice','Season');


/* End of file autoload.php */
/* Location: ./application/config/autoload.php */