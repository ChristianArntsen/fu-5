<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['force_https'] = false;
$config['load_3rd_party_tools'] = true;
