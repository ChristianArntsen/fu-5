<?php
/* 
|-------------------------------------------------------------------------- 
| REST Login 
|-------------------------------------------------------------------------- 
| 
| Is login required and if so, which type of login? 
| 
|   '' = no login required, 'basic' = relatively secure login, 'digest' = secure login 
| 
*/  
$config['rest_auth'] = 'basic';  

$config['rest_default_format'] = 'json';

$config['rest_enable_keys'] = TRUE;
$config['rest_enable_limits'] = TRUE;
$config['rest_enable_logging'] = TRUE;

$config['rest_key_name'] = 'api_key';
$config['rest_keys_table'] = 'api_keys';
$config['rest_limits_table'] = 'api_limits';
$config['rest_logs_table'] = 'api_logs';

$config['api_method_settings'] = array(
	'other_get'=>array('limit'=>3, 'level'=>3, 'log'=>'db')
	);

//limit = calls per hour
//level = api level the key has to have
//log = db to log calls to the database
?>