<?php
class Booking_lib
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
	}

	function get_cart()
	{
		if($this->CI->session->userdata('cart') === false)
			$this->set_cart(array());

		return $this->CI->session->userdata('cart');
	}

	function get_basket()
	{
		if($this->CI->session->userdata('basket') === false)
			$this->set_basket(array());

		return $this->CI->session->userdata('basket');
	}
	function get_basket_html()
	{
		$basket_items = $this->get_basket();
		$basket_html = "<tr><td><a class='delete_item' href=\"javascript:booking.empty_basket()\">x</a></td><td>Time Slot</td><td class='align_right'></td></tr>";
		foreach ($basket_items as $item)
		{
			$basket_html .= $this->get_basket_row_html($item);
		}
		return "<table><tbody>$basket_html</tbody></table>";
	}
	function get_basket_row_html($item)
	{
		$substring = substr($item['sim_number'], 4);
		$track_id = substr($substring, 0, strpos($substring,'_'));
		//echo 'trackid '.$track_id;
	//	$track = $this->Track->get_info($track_id);//->result_array();
		//print_r($track);
//		$sim = substr($val, 0, 6);
//		$time_string = substr($substring, strpos($substring,'_')+1);
		$date_string = substr($substring, strpos($substring, '_')+1) + 1000000;
		$date = date('l, F jS, Y',strtotime($date_string));
		$start_time = date('g:ia', strtotime($date_string));
		$end_date_string =$date_string + $this->CI->session->userdata('increment');
		$end_date_string = ($end_date_string % 100 > 59)?$end_date_string+40:$end_date_string;
		$end_time = date('g:ia', strtotime($end_date_string));
		
//		$start_time = substr($item['description'], -4);
	//	$start_hour = $end_hour = substr($start_time, 0, 2);
		//$start_min = substr($start_time, 2);
//		$end_min = $start_min + $this->CI->session->userdata('increment');
	//	if ($end_min % 100 > 59)
		//{
//			$end_min -= 60;
	//		$end_hour += 1;
		//}
//		$end_min = ($end_min < 10)?'0'.$end_min:$end_min;
	//	$s_ampm = ($start_hour < 12)?'a':'p';
		//$e_ampm = ($end_hour < 12)?'a':'p';
		
//		$start_time = (int)$start_hour.':'.$start_min.$s_ampm;
	//	$end_time = (int)$end_hour.':'.$end_min.$e_ampm;
		$simulator = $item['description'];//(substr($item['description'], 4, 1) == 1)?'Simulator 1':'Simimulator 2';
		
		return "<tr><td><a class='delete_item' href=\"javascript:booking.change_simulator_selection('{$item['sim_number']}')\">x</a></td><td>$simulator: $start_time - $end_time</td><td class='align_right'><script>$(document).ready(function(){booking.mark_as_selected($('#{$item['sim_number']}'))})</script></td></tr><tr><td></td><td colspan=2>$date</td></tr>";
	}
	function set_cart($cart_data)
	{
		$this->CI->session->set_userdata('cart',$cart_data);
	}

	function set_basket($basket_data)
	{
		$this->CI->session->set_userdata('basket',$basket_data);
	}
	function get_item_line($time)
	{
		$items = $this->get_basket();
		foreach ($items as $item)
		{
			if ($item['sim_number'] == $time)
				return $item['line'];
		}
		return false;
	}

	//Alain Multiple Payments
	function get_payments()
	{
		if($this->CI->session->userdata('payments') === false)
			$this->set_payments(array());

		return $this->CI->session->userdata('payments');
	}

	//Alain Multiple Payments
	function set_payments($payments_data)
	{
		$this->CI->session->set_userdata('payments',$payments_data);
	}
	
	function get_comment() 
	{
		return $this->CI->session->userdata('comment') ? $this->CI->session->userdata('comment') : '';
	}

	function set_comment($comment) 
	{
		$this->CI->session->set_userdata('comment', $comment);
	}

	function clear_comment() 	
	{
		$this->CI->session->unset_userdata('comment');
	}
	
	function get_email_receipt() 
	{
		return $this->CI->session->userdata('email_receipt');
	}

	function set_email_receipt($email_receipt) 
	{
		$this->CI->session->set_userdata('email_receipt', $email_receipt);
	}

	function clear_email_receipt() 	
	{
		$this->CI->session->unset_userdata('email_receipt');
	}

	function add_payment($payment_id,$payment_amount)
	{
		$payments=$this->get_payments();
		$customer_id = $this->get_customer();
		if ($customer_id != -1 && $payment_id == 'Account') {
			$customer_info=$this->CI->Customer->get_info($customer_id);
			$payment_id .= '- '.$customer_info->last_name.', '.$customer_info->first_name;
		}
		$payment = array($payment_id=>
		array(
			'payment_type'=>$payment_id,
			'payment_amount'=>$payment_amount,
			'invoice_id'=>$this->CI->session->userdata('invoice_id'),
			'customer_id'=>$customer_id
			)
		);
		$this->CI->session->unset_userdata('invoice_id');
		//payment_method already exists, add to payment_amount
		if(isset($payments[$payment_id]))
		{
			$payments[$payment_id]['payment_amount']+=$payment_amount;
		}
		else
		{
			//add to existing array
			$payments+=$payment;
		}

		$this->set_payments($payments);
		return true;

	}

	//Alain Multiple Payments
	function edit_payment($payment_id,$payment_amount)
	{
		$payments = $this->get_payments();
		if(isset($payments[$payment_id]))
		{
			$payments[$payment_id]['payment_type'] = $payment_id;
			$payments[$payment_id]['payment_amount'] = $payment_amount;
			$this->set_payments($payment_id);
		}

		return false;
	}

	//Alain Multiple Payments
	function delete_payment($payment_id)
	{
		$payment_id = str_replace("___", "/", $payment_id);
		$this->CI->load->library('Hosted_checkout');
		$HC = new Hosted_checkout();
		
		$payments=$this->get_payments();
		$invoice_id = $payments[$payment_id]['invoice_id'];
		if ($invoice_id != '') 
			if (!$HC->cancel_payment($invoice_id)) 
				return;

		unset($payments[$payment_id]);
		$this->set_payments($payments);
	}

	//Alain Multiple Payments
	function empty_payments()
	{
		$this->CI->session->unset_userdata('payments');
	}

	//Alain Multiple Payments
	function get_payments_total()
	{
		$subtotal = 0;
		foreach($this->get_payments() as $payments)
		{
		    $subtotal+=$payments['payment_amount'];
		}
		return to_currency_no_money($subtotal);
	}

	//Alain Multiple Payments
	function get_amount_due($sale_id = false)
	{
		$amount_due=0;
		$payment_total = $this->get_payments_total();
		$sales_total=$this->get_total($sale_id);
		$amount_due=to_currency_no_money($sales_total - $payment_total);
		return $amount_due;
	}
	function get_simulator_amount_due($item_count = false)
	{
		$basket = $this->get_basket();
		$item_count = ($item_count ? $item_count : count($basket));
		$item_number = $this->CI->session->userdata('course_id').'_'.$item_count;
		$price_category = $this->CI->Fee->get_price_category();
		if (isset($basket[1]['price_category'])) {
			$price_category = $basket[1]['price_category'];
		}
//		echo 'hey';
		$schedule_id = $this->CI->session->userdata('schedule_id') ? $this->CI->session->userdata('schedule_id') : $this->CI->session->userdata('teesheet_id');
		$price_result = $this->CI->Fee->get_info();

		return $price_result[$schedule_id][$item_number]->$price_category;
	}
    function get_basket_amount_due($sale_id = false)
	{
		$amount_due=0;
		$payment_total = $this->get_payments_total();
		$sales_total=$this->get_basket_total($sale_id);
		$amount_due=to_currency_no_money($sales_total - $payment_total);
		return $amount_due;
	}

	function get_customer()
	{
		if(!$this->CI->session->userdata('customer'))
			$this->set_customer(-1);

		return $this->CI->session->userdata('customer');
	}

	function set_customer($customer_id)
	{
		$this->CI->session->set_userdata('customer',$customer_id);
		$this->set_customer_quickbutton($customer_id, $customer_name);
	}
	function get_customer_quickbuttons()
	{
		if(!$this->CI->session->userdata('customer_quickbuttons'))
			return array();//$this->set_customer_quickbbutton(-1);

		return $this->CI->session->userdata('customer_quickbuttons');
	}

	function set_customer_quickbutton($customer_id, $customer_name='')
	{
		if ($customer_name == '') {
			$customer_info = $this->CI->Customer->get_info($customer_id);
			if ($customer_info->last_name != '' && $customer_info->first_name != '')
			$customer_name = $customer_info->last_name.', '.$customer_info->first_name;
		}
		if (trim($customer_name) == '' || $customer_id == -1 || $customer_id == 0)
			return;
		$customer_quickbutton_array = $this->get_customer_quickbuttons();
		$customer_quickbutton_array[$customer_id] = array('id'=>$customer_id, 'name'=>$customer_name);
		
		$this->CI->session->set_userdata('customer_quickbuttons',$customer_quickbutton_array);
	}
	function set_taxable($taxable)
	{		
		$this->CI->session->set_userdata('taxable',$taxable);
	}
	function get_teetime()
	{
		if(!$this->CI->session->userdata('teetime'))
			$this->set_teetime(-1);

		return $this->CI->session->userdata('teetime');
	}

	function set_teetime($teetime_id)
	{
		$this->CI->session->set_userdata('teetime',$teetime_id);
	}

	function get_mode()
	{
		if(!$this->CI->session->userdata('sale_mode'))
			$this->set_mode('sale');

		return $this->CI->session->userdata('sale_mode');
	}

	function set_mode($mode)
	{
		$this->CI->session->set_userdata('sale_mode',$mode);
	}

	function add_item($item_id,$quantity=1,$discount=0,$price=null,$description=null,$serialnumber=null,$price_category=null,$teetime_type=null, $sim_number = null)
	{
		//make sure item exists
		if(!$this->CI->Item->exists(is_numeric($item_id) ? (int)$item_id : -1))	
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);

			if(!$item_id)
				return false;
		}
		else
		{
			$item_id = (int)$item_id;
		}
		//Alain Serialization and Description

		//Get all items in the cart so far...
		$items = $this->get_cart();
                
        //We need to loop through all items in the cart.
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Key to use for new entry.
		$updatekey=0;                    //Key to use to update(quantity)

		foreach ($items as $item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.

			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}

			//if(isset($item['item_id']) && $item['item_id']==$item_id)
			//{
			//	$itemalreadyinsale=TRUE;
			//	$updatekey=$item['line'];
			//}
		}

		$insertkey=$maxkey+1;

		//array/cart records are identified by $insertkey and item_id is just another field.
		$item_info = $this->CI->Item->get_info($item_id);
		$price_category = $price_category!=null ? $price_category:1;
		$teetime_type = $teetime_type!=null ? $teetime_type:substr($item_info->item_number, strrpos($item_info->item_number, '_')+1);
		$price = $price!=null ? $price: $this->get_teetime_price($price_category, $teetime_type);
		$item = array(($insertkey)=>
		array(
			'item_id'=>$item_id,
			'line'=>$insertkey,
			'name'=>$item_info->name,
			'item_number'=>$item_info->item_number,
			'price_category'=>$price_category,
			'teetime_type'=>$teetime_type,
			'description'=>$description!=null ? $description: $item_info->description,
			'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
			'allow_alt_description'=>$item_info->allow_alt_description,
			'is_serialized'=>$item_info->is_serialized,
			'quantity'=>$quantity,
            'discount'=>$discount,
            'sim_number'=>$sim_number,
			'price'=>$price
			)
		);

		//Item already exists and is not serialized, add to quantity
		if($itemalreadyinsale && ($item_info->is_serialized ==0) )
		{
			$items[$updatekey]['quantity']+=$quantity;
		}
		else
		{
			//add to existing array
			$items+=$item;
		}

		$this->set_cart($items);
		return true;
	}
        
    function add_item_to_basket($item_id,$quantity=1,$discount=0,$price=null,$description=null,$serialnumber=null,$price_category=null,$teetime_type=null,$sim_number=null)
	{
		//make sure item exists
		if(!$this->CI->Item->exists(is_numeric($item_id) ? (int)$item_id : -1))	
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);

			if(!$item_id)
				return false;
		}
		else
		{
			$item_id = (int)$item_id;
		}
		//Alain Serialization and Description

		//Get all items in the cart so far...
		$items = $this->get_basket();

        //We need to loop through all items in the cart.
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Key to use for new entry.
		$updatekey=0;                    //Key to use to update(quantity)

		foreach ($items as $item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.

			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}

			//if(isset($item['item_id']) && $item['item_id']==$item_id)
			//{
			//	$itemalreadyinsale=TRUE;
			//	$updatekey=$item['line'];
			//}
		}

		$insertkey=$maxkey+1;

		//array/cart records are identified by $insertkey and item_id is just another field.
		$item_info = $this->CI->Item->get_info($item_id);
		$price_category = $price_category!=null ? $price_category:1;
		$teetime_type = $teetime_type!=null ? $teetime_type:substr($item_info->item_number, strrpos($item_info->item_number, '_')+1);
		//echo "Price $price, $price!=null ? $price:".$this->get_teetime_price($price_category, $teetime_type);
		$price = $price!=null ? $price: $this->get_teetime_price($price_category, $teetime_type);
		$item = array(($insertkey)=>
		array(
			'item_id'=>$item_id,
			'line'=>$insertkey,
			'name'=>$item_info->name,
			'item_number'=>$item_info->item_number,
			'price_category'=>$price_category,
			'teetime_type'=>$teetime_type,
			'description'=>$description!=null ? $description: $item_info->description,
			'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
			'allow_alt_description'=>$item_info->allow_alt_description,
			'is_serialized'=>$item_info->is_serialized,
			'quantity'=>$quantity,
            'discount'=>$discount,
            'sim_number'=>$sim_number,
			'price'=>$price
			)
		);
//echo 'sim '.$sim_number;
		//Item already exists and is not serialized, add to quantity
		if($itemalreadyinsale && ($item_info->is_serialized ==0) )
		{
			$items[$updatekey]['quantity']+=$quantity;
		}
		else
		{
			//add to existing array
			$items+=$item;
		}

		$this->set_basket($items);
		return true;
	}
	
	function add_item_kit($external_item_kit_id_or_item_number,$quantity=1,$discount=0,$price=null,$description=null)
	{
		if (strpos($external_item_kit_id_or_item_number, 'KIT') !== FALSE)
		{
			//KIT #
			$pieces = explode(' ',$external_item_kit_id_or_item_number);
			$item_kit_id = (int)$pieces[1];	
		}
		else
		{
			$item_kit_id = $this->CI->Item_kit->get_item_kit_id($external_item_kit_id_or_item_number);
		}
		
		//make sure item exists
		if(!$this->CI->Item_kit->exists($item_kit_id))	
		{
			return false;
		}
		
		if ( $this->CI->Item_kit->get_info($item_kit_id)->unit_price == null)
		{
			foreach ($this->CI->Item_kit_items->get_info($item_kit_id) as $item_kit_item)
			{
				for($k=0;$k<$item_kit_item->quantity;$k++)
				{
					$this->add_item($item_kit_item->item_id, 1);
				}
			}
			
			return true;
		}
		else
		{
			$items = $this->get_cart();

	        //We need to loop through all items in the cart.
	        //If the item is already there, get it's key($updatekey).
	        //We also need to get the next key that we are going to use in case we need to add the
	        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

	        $maxkey=0;                       //Highest key so far
	        $itemalreadyinsale=FALSE;        //We did not find the item yet.
			$insertkey=0;                    //Key to use for new entry.
			$updatekey=0;                    //Key to use to update(quantity)

			foreach ($items as $item)
			{
	            //We primed the loop so maxkey is 0 the first time.
	            //Also, we have stored the key in the element itself so we can compare.

				if($maxkey <= $item['line'])
				{
					$maxkey = $item['line'];
				}

				if(isset($item['item_kit_id']) && $item['item_kit_id']==$item_kit_id)
				{
					$itemalreadyinsale=TRUE;
					$updatekey=$item['line'];
				}
			}

			$insertkey=$maxkey+1;

			//array/cart records are identified by $insertkey and item_id is just another field.
			$item = array(($insertkey)=>
			array(
				'item_kit_id'=>$item_kit_id,
				'line'=>$insertkey,
				'item_kit_number'=>$this->CI->Item_kit->get_info($item_kit_id)->item_kit_number,
				'name'=>$this->CI->Item_kit->get_info($item_kit_id)->name,
				'description'=>$description!=null ? $description: $this->CI->Item_kit->get_info($item_kit_id)->description,
				'quantity'=>$quantity,
	            'discount'=>$discount,
				'price'=>$price!=null ? $price: $this->CI->Item_kit->get_info($item_kit_id)->unit_price
				)
			);

			//Item already exists and is not serialized, add to quantity
			if($itemalreadyinsale)
			{
				$items[$updatekey]['quantity']+=$quantity;
			}
			else
			{
				//add to existing array
				$items+=$item;
			}

			$this->set_cart($items);
			return true;
		}
	}
    function add_item_kit_to_basket($external_item_kit_id_or_item_number,$quantity=1,$discount=0,$price=null,$description=null)
	{
		if (strpos($external_item_kit_id_or_item_number, 'KIT') !== FALSE)
		{
			//KIT #
			$pieces = explode(' ',$external_item_kit_id_or_item_number);
			$item_kit_id = (int)$pieces[1];	
		}
		else
		{
			$item_kit_id = $this->CI->Item_kit->get_item_kit_id($external_item_kit_id_or_item_number);
		}
		
		//make sure item exists
		if(!$this->CI->Item_kit->exists($item_kit_id))	
		{
			return false;
		}
		
		if ( $this->CI->Item_kit->get_info($item_kit_id)->unit_price == null)
		{
			foreach ($this->CI->Item_kit_items->get_info($item_kit_id) as $item_kit_item)
			{
				for($k=0;$k<$item_kit_item->quantity;$k++)
				{
					$this->add_item($item_kit_item->item_id, 1);
				}
			}
			
			return true;
		}
		else
		{
			$items = $this->get_cart();

	        //We need to loop through all items in the cart.
	        //If the item is already there, get it's key($updatekey).
	        //We also need to get the next key that we are going to use in case we need to add the
	        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

	        $maxkey=0;                       //Highest key so far
	        $itemalreadyinsale=FALSE;        //We did not find the item yet.
			$insertkey=0;                    //Key to use for new entry.
			$updatekey=0;                    //Key to use to update(quantity)

			foreach ($items as $item)
			{
	            //We primed the loop so maxkey is 0 the first time.
	            //Also, we have stored the key in the element itself so we can compare.

				if($maxkey <= $item['line'])
				{
					$maxkey = $item['line'];
				}

				if(isset($item['item_kit_id']) && $item['item_kit_id']==$item_kit_id)
				{
					$itemalreadyinsale=TRUE;
					$updatekey=$item['line'];
				}
			}

			$insertkey=$maxkey+1;

			//array/cart records are identified by $insertkey and item_id is just another field.
			$item = array(($insertkey)=>
			array(
				'item_kit_id'=>$item_kit_id,
				'line'=>$insertkey,
				'item_kit_number'=>$this->CI->Item_kit->get_info($item_kit_id)->item_kit_number,
				'name'=>$this->CI->Item_kit->get_info($item_kit_id)->name,
				'description'=>$description!=null ? $description: $this->CI->Item_kit->get_info($item_kit_id)->description,
				'quantity'=>$quantity,
	            'discount'=>$discount,
				'price'=>$price!=null ? $price: $this->CI->Item_kit->get_info($item_kit_id)->unit_price
				)
			);

			//Item already exists and is not serialized, add to quantity
			if($itemalreadyinsale)
			{
				$items[$updatekey]['quantity']+=$quantity;
			}
			else
			{
				//add to existing array
				$items+=$item;
			}

			$this->set_cart($items);
			return true;
		}
	}
	function is_unlimited($item_id)
	{
		//make sure item exists
		if(!$this->CI->Item->exists($item_id))
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);

			if(!$item_id)
				return false;
		}
		
		$item = $this->CI->Item->get_info($item_id);
		
		return $item->is_unlimited;
	}
	
	function out_of_stock($item_id)
	{
		//make sure item exists
		if(!$this->CI->Item->exists($item_id))
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);

			if(!$item_id)
				return false;
		}
		
		$item = $this->CI->Item->get_info($item_id);
		$quanity_added = $this->get_quantity_already_added($item_id);
		
		if ($item->quantity - $quanity_added < 0)
		{
			return true;
		}
		
		return false;
	}
	
	function get_quantity_already_added($item_id)
	{
		$items = $this->get_cart();
		$quanity_already_added = 0;
		foreach ($items as $item)
		{
			if(isset($item['item_id']) && $item['item_id']==$item_id)
			{
				$quanity_already_added+=$item['quantity'];
			}
		}
		
		return $quanity_already_added;
	}
	
	function get_item_id($line_to_get)
	{
		$items = $this->get_cart();

		foreach ($items as $line=>$item)
		{
			if($line==$line_to_get)
			{
				return isset($item['item_id']) ? $item['item_id'] : -1;
			}
		}
		
		return -1;
	}

	function edit_item($line,$description,$serialnumber,$quantity,$discount,$price, $item_id = '')
	{
		$items = $this->get_cart();
		if(isset($items[$line]))
		{
            if ($item_id != '' && $this->CI->Item->exists($item_id)) {
                $item_info = $this->CI->Item->get_info($item_id);
                $items[$line]['item_id'] = $item_id;
                $items[$line]['name'] = $item_info->name;
                $items[$line]['description'] = $item_info->description;
                $items[$line]['price'] = $item_info->unit_price;
			}
	        else {
	            $items[$line]['description'] = $description;
	            $items[$line]['price'] = $price;
			}
                            
                            
                            
			$items[$line]['serialnumber'] = $serialnumber;
			$items[$line]['quantity'] = $quantity;
			$items[$line]['discount'] = $discount;
		//print_r($items[$line]);
                    	$this->set_cart($items);
		}

		return false;
	}
	function get_teetime_price($price_category_index = null, $teetime_type_index = null)
	{
		
		$teetime_type_index = $teetime_type_index != null ? $teetime_type_index : $this->get_teetime_type();
		$price_category_index = $price_category_index != null ? $price_category_index : $this->get_price_category();
				
		if ($this->CI->session->userdata('reservations'))
			$item_prices = $this->CI->Fee->get_info($this->CI->session->userdata('teesheet_id').'_'.$teetime_type_index, 'price_category_'.$price_category_index);
		else
			$item_prices = $this->CI->Green_fee->get_info($this->CI->session->userdata('teesheet_id').'_'.$teetime_type_index, 'price_category_'.$price_category_index);
        return (float)$item_prices->price;
	}
	function get_price_category() 
	{
		return substr($this->CI->input->post('item_number'), 0, strpos($this->CI->input->post('item_number'), '_'));
	}
	function get_teetime_type()
	{
		return substr($this->CI->input->post('item_number'), strpos($this->CI->input->post('item_number'), '_')+1);
	}
    function edit_item_attribute($line, $attribute = '', $value = '', $item_id = '')
	{
		$items = $this->get_cart();
		if(isset($items[$line]))
		{
            if ($this->CI->input->post('item_number')) {
                $item_info = $this->CI->Item->get_info($item_id);
			    $items[$line]['item_id'] = $item_id;
                $items[$line]['name'] = $item_info->name;
				$items[$line]['item_number'] = $this->CI->session->userdata('course_id').'_'.$this->get_teetime_type();
				$items[$line]['price_category'] = $this->get_price_category();
				$items[$line]['teetime_type'] = $this->get_teetime_type();
                $items[$line]['description'] = $item_info->description;
                $items[$line]['price'] = $this->get_teetime_price();
			}
            else 
                $items[$line][$attribute] = $value;
	            
            $this->set_cart($items);
            
            return array('price'=>$items[$line]['price'], 'quantity'=>$items[$line]['quantity'], 'discount'=>$items[$line]['discount'], 'total'=>$items[$line]['price']*$items[$line]['quantity']*(100-$items[$line]['discount']/100));
        }
		return false;
	}
    function edit_basket_item_attribute($line, $attribute = '', $value = '', $item_id = '')
	{
		$items = $this->get_basket();
		if(isset($items[$line]))
		{
            if ($this->CI->input->post('item_number')) {
                $item_info = $this->CI->Item->get_info($item_id);
			    $items[$line]['item_id'] = $item_id;
                $items[$line]['name'] = $item_info->name;
				$items[$line]['item_number'] = $this->CI->session->userdata('course_id').'_'.$this->get_teetime_type();
				$items[$line]['price_category'] = $this->get_price_category();
                $items[$line]['teetime_type'] = $this->get_teetime_type();
                $items[$line]['description'] = $item_info->description;
                $items[$line]['price'] = $this->get_teetime_price();
			}
            else 
                $items[$line][$attribute] = $value;

            $this->set_basket($items);
            return array('price'=>$items[$line]['price'], 'quantity'=>$items[$line]['quantity'], 'discount'=>$items[$line]['discount'], 'total'=>$items[$line]['price']*$items[$line]['quantity']*(100-$items[$line]['discount']/100));
        }

		return false;
	}

	function is_valid_receipt($receipt_sale_id)
	{
		//POS #
		$pieces = explode(' ',$receipt_sale_id);

		if(count($pieces)==2 && $pieces[0] == 'POS')
		{
			return $this->CI->Sale->exists($pieces[1]);
		}

		return false;
	}
	
	function is_valid_item_kit($item_kit_id)
	{
		//KIT #
		$pieces = explode(' ',$item_kit_id);

		if(count($pieces)==2 && $pieces[0] == 'KIT')
		{
			return $this->CI->Item_kit->exists($pieces[1]);
		}
		else
		{
			return $this->CI->Item_kit->get_item_kit_id($item_kit_id) !== FALSE;
		}
	}

	function return_entire_sale($receipt_sale_id)
	{
		//POS #
		$pieces = explode(' ',$receipt_sale_id);
		$sale_id = $pieces[1];

		$this->empty_cart();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();
		
		foreach($this->CI->Sale->get_sale_items($sale_id)->result() as $row)
		{
			$this->add_item($row->item_id,-$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
		}
		foreach($this->CI->Sale->get_sale_item_kits($sale_id)->result() as $row)
		{
			$this->add_item_kit('KIT '.$row->item_kit_id,-$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
		}
		$this->set_customer($this->CI->Sale->get_customer($sale_id)->person_id);
	}
	function put_cart_into_basket() {
            $this->empty_basket();
            $items = $this->get_cart();
            $this->set_basket($items);
        }
	function copy_entire_sale($sale_id)
	{
		$this->empty_cart();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();

		foreach($this->CI->Sale->get_sale_items($sale_id)->result() as $row)
		{
			$this->add_item($row->item_id,$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
		}
		foreach($this->CI->Sale->get_sale_item_kits($sale_id)->result() as $row)
		{
			$this->add_item_kit('KIT '.$row->item_kit_id,$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
		}
		foreach($this->CI->Sale->get_sale_payments($sale_id)->result() as $row)
		{
			$this->add_payment($row->payment_type,$row->payment_amount);
		}
		$this->set_customer($this->CI->Sale->get_customer($sale_id)->person_id);

	}
	
	function copy_entire_suspended_sale($sale_id)
	{
		$this->empty_cart();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();

		foreach($this->CI->Sale_suspended->get_sale_items($sale_id)->result() as $row)
		{
			$this->add_item($row->item_id,$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
		}

		foreach($this->CI->Sale_suspended->get_sale_item_kits($sale_id)->result() as $row)
		{
			$this->add_item_kit('KIT '.$row->item_kit_id,$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
		}
		
		foreach($this->CI->Sale_suspended->get_sale_payments($sale_id)->result() as $row)
		{
			$this->add_payment($row->payment_type,$row->payment_amount);
		}
		$this->set_customer($this->CI->Sale_suspended->get_customer($sale_id)->person_id);
		$this->set_comment($this->CI->Sale_suspended->get_comment($sale_id));
	}
	
	function get_suspended_sale_id()
	{
		return $this->CI->session->userdata('suspended_sale_id');
	}
	
	function set_suspended_sale_id($suspended_sale_id)
	{
		$this->CI->session->set_userdata('suspended_sale_id',$suspended_sale_id);
	}
	
	function delete_suspended_sale_id()
	{
		$this->CI->session->unset_userdata('suspended_sale_id');
	}
    function get_basket_info($price_category = 'price_category_1'){
        $items_in_basket = $this->get_items_in_basket();
        $price = $this->get_simulator_amount_due();
        $tax_amount = 0;
        if (!empty($items_in_basket[0])) {
            $tax_info = $this->CI->Item_taxes->get_info($items_in_basket[0]['item_id']);
            foreach($tax_info as $key=>$tax)
            {
                $tax_amount += $price*(($tax['percent'])/100);
            }
        }
        $subtotal = $price;//$this->get_basket_subtotal();
        $total = $price + $tax_amount;//$this->get_basket_total();
        $amount_due = $total;//$this->get_basket_amount_due();
        $taxes = 0;//$this->get_basket_taxes();
        $formatted_taxes = array();
		foreach($taxes as $name => $amount){
            $formatted_taxes[] = array();//array('name' => $name, 'amount' => $amount);
        }
        return array('items_in_basket'=>$items_in_basket, 'subtotal'=>$subtotal, 'total'=>$total, 'price'=>$this->get_simulator_amount_due(),'amount_due'=>$amount_due, 'taxes'=>$formatted_taxes);
    }
	function delete_item($line)
	{
		$items=$this->get_cart();
		unset($items[$line]);
		$this->set_cart($items);
	}
        function copy_item_into_basket($line) {
            $items = $this->get_cart();
            $basket_items = $this->get_basket();
            $basket_items[$line] = $items[$line];
            
            $this->set_basket($basket_items);
/*            $this->add_item_to_basket($items[$line]['item_id'], $items[$line]['quantity'], $items[$line]['discount'], 
                    $items[$line]['price'], $items[$line]['description'], $items[$line]['serialnumber']);
 */           
        }
        function delete_item_from_basket($line)
	{
            $items=$this->get_basket();
            unset($items[$line]);
            $this->set_basket($items);

              
        }

	function empty_cart()
	{
		$this->CI->session->unset_userdata('cart');
	}
        
    function empty_basket()
	{
            $this->CI->session->unset_userdata('basket');
    }

	function delete_customer()
	{
		$this->CI->session->unset_userdata('customer');
	}

	function delete_customer_quickbuttons()
	{
		$this->CI->session->unset_userdata('customer_quickbuttons');
	}

	function clear_mode()
	{
		$this->CI->session->unset_userdata('sale_mode');
	}

	function clear_all()
	{
		$this->clear_mode();
		$this->empty_cart();
		$this->empty_basket();
		$this->clear_comment();
		$this->clear_email_receipt();
		$this->empty_payments();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();
		$this->delete_suspended_sale_id();
	}
    function clear_all_minus_cart()
    {
        $basket_data = $this->get_basket();
        $cart_data = $this->get_cart();
        foreach($basket_data as $line => $item) {
            unset($cart_data[$line]);
        }
        $this->set_cart($cart_data);
                
		$this->clear_mode();
		$this->empty_basket();
		$this->clear_comment();
		$this->clear_email_receipt();
		$this->empty_payments();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();
		$this->delete_suspended_sale_id();
    }

	function get_taxes($sale_id = false)
	{
		$taxes = array();
		
		if ($sale_id)
		{
			$taxes_from_sale = array_merge($this->CI->Sale->get_sale_items_taxes($sale_id), $this->CI->Sale->get_sale_item_kits_taxes($sale_id));
			
			foreach($taxes_from_sale as $key=>$tax_item)
			{
				$name = $tax_item['percent'].'% ' . $tax_item['name'];
			
				if ($tax_item['cumulative'])
				{
					$prev_tax = $taxes[$taxes_from_sale[$key-1]['percent'].'% ' . $taxes_from_sale[$key-1]['name']];
					$tax_amount=(($tax_item['price']*$tax_item['quantity']-$tax_item['price']*$tax_item['quantity']*$tax_item['discount']/100) + $prev_tax)*(($tax_item['percent'])/100);					
				}
				else
				{
					$tax_amount=($tax_item['price']*$tax_item['quantity']-$tax_item['price']*$tax_item['quantity']*$tax_item['discount']/100)*(($tax_item['percent'])/100);
				}

				if (!isset($taxes[$name]))
				{
					$taxes[$name] = 0;
				}
				$taxes[$name] += $tax_amount;
			}
		}
		else
		{
			$customer_id = $this->get_customer();
			$customer = $this->CI->Customer->get_info($customer_id);

			//Do not charge sales tax if we have a customer that is not taxable
			if ($this->CI->session->userdata('taxable') == 'false' || (!$customer->taxable and $customer_id!=-1))
			{
			   return array();
			}

			foreach($this->get_cart() as $line=>$item)
			{
				$tax_info = isset($item['item_id']) ? $this->CI->Item_taxes->get_info($item['item_id']) : $this->CI->Item_kit_taxes->get_info($item['item_kit_id']);
				foreach($tax_info as $key=>$tax)
				{
					$name = $tax['percent'].'% ' . $tax['name'];
				
					if ($tax['cumulative'])
					{
						$prev_tax = $taxes[$tax_info[$key-1]['percent'].'% ' . $tax_info[$key-1]['name']];
						$tax_amount=(($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100) + $prev_tax)*(($tax['percent'])/100);					
					}
					else
					{
						$tax_amount=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100)*(($tax['percent'])/100);
					}

					if (!isset($taxes[$name]))
					{
						$taxes[$name] = 0;
					}
					$taxes[$name] += $tax_amount;
				}
			}
		}
		
		return $taxes;
	}
	
    function get_basket_taxes($sale_id = false)
	{
		$taxes = array();
		
		if ($sale_id)
		{
			$taxes_from_sale = array_merge($this->CI->Sale->get_sale_items_taxes($sale_id), $this->CI->Sale->get_sale_item_kits_taxes($sale_id));
			
			foreach($taxes_from_sale as $key=>$tax_item)
			{
				$name = $tax_item['percent'].'% ' . $tax_item['name'];
			
				if ($tax_item['cumulative'])
				{
					$prev_tax = $taxes[$taxes_from_sale[$key-1]['percent'].'% ' . $taxes_from_sale[$key-1]['name']];
					$tax_amount=(($tax_item['price']*$tax_item['quantity']-$tax_item['price']*$tax_item['quantity']*$tax_item['discount']/100) + $prev_tax)*(($tax_item['percent'])/100);					
				}
				else
				{
					$tax_amount=($tax_item['price']*$tax_item['quantity']-$tax_item['price']*$tax_item['quantity']*$tax_item['discount']/100)*(($tax_item['percent'])/100);
				}

				if (!isset($taxes[$name]))
				{
					$taxes[$name] = 0;
				}
				$taxes[$name] += $tax_amount;
			}
		}
		else
		{
			$customer_id = $this->get_customer();
			$customer = $this->CI->Customer->get_info($customer_id);

			//Do not charge sales tax if we have a customer that is not taxable
			if ($this->CI->session->userdata('taxable') == 'false' || (!$customer->taxable and $customer_id!=-1))
			{
			   return array();
			}

			foreach($this->get_basket() as $line=>$item)
			{
				$tax_info = isset($item['item_id']) ? $this->CI->Item_taxes->get_info($item['item_id']) : $this->CI->Item_kit_taxes->get_info($item['item_kit_id']);
				foreach($tax_info as $key=>$tax)
				{
					$name = $tax['percent'].'% ' . $tax['name'];
				
					if ($tax['cumulative'])
					{
						$prev_tax = $taxes[$tax_info[$key-1]['percent'].'% ' . $tax_info[$key-1]['name']];
						$tax_amount=(($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100) + $prev_tax)*(($tax['percent'])/100);					
					}
					else
					{
						$tax_amount=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100)*(($tax['percent'])/100);
					}

					if (!isset($taxes[$name]))
					{
						$taxes[$name] = 0;
					}
					$taxes[$name] += $tax_amount;
				}
			}
		}
		
		return $taxes;
	}
	
	function get_items_in_cart()
	{
		$items_in_cart = 0;
		foreach($this->get_cart() as $item)
		{
		    $items_in_cart+=$item['quantity'];
		}
		
		return $items_in_cart;
	}
	
	function get_items_in_basket()
	{
		$items_in_basket = 0;
		foreach($this->get_basket() as $item)
		{
		    $items_in_basket+=$item['quantity'];
		}
		
		return $items_in_basket;
	}
	
	function get_subtotal()
	{
		$subtotal = 0;
		foreach($this->get_cart() as $item)
		{
		    $subtotal+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}
		return to_currency_no_money($subtotal);
	}

	function get_total($sale_id = false)
	{
		$total = 0;
		foreach($this->get_cart() as $item)
		{
            $total+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}

		foreach($this->get_taxes($sale_id) as $tax)
		{
			$total+=$tax;
		}

		return to_currency_no_money($total);
	}
        /*function get_basket_total($sale_id = false)
	{
		$total = 0;
		foreach($this->get_basket() as $item)
		{
            $total+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}

		foreach($this->get_taxes($sale_id) as $tax)
		{
			$total+=$tax;
		}

		return to_currency_no_money($total);
	}*/
        function get_basket_subtotal()
	{
		$subtotal = 0;
		foreach($this->get_basket() as $item)
		{
		    $subtotal+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}
		return to_currency_no_money($subtotal);
	}

	function get_basket_total($sale_id = false)
	{
		$total = 0;
		foreach($this->get_basket() as $item)
		{
            $total+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}

		foreach($this->get_basket_taxes($sale_id) as $tax)
		{
			$total+=$tax;
		}

		return to_currency_no_money($total);
	}
}
?>