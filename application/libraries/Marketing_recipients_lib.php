<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*************************************************
 * change   author    date          remarks
 * 0.1      MBPogay   11-APR-2012   updated function get_recipients to make it more dynamic
 *                                  retrieving recepients
 **************************************************/
class Marketing_recipients_lib
{
  private $groups = array();
  private $individuals = array();
  private $CI;

  function __construct()
	{
		$this->CI =& get_instance();
	}
  
  public function set_groups($groups)
  {
    $this->groups = $groups;
  }

  public function set_individuals($individuals)
  {
    $this->individuals = $individuals;
  }

  public function get_groups()
  {
    return $this->groups;
  }

  public function get_individuals()
  {
    return $this->individuals;
  }

	public function count_recipients($type)
	{
		$type = strtolower($type);
		$type  = $type == 'text' || $type == 'phone' ? 'phone' : 'email';
		$recipient_count = 0;
		if (!empty($this->groups) && in_array(0, $this->groups))
			$recipient_count = $this->CI->Customer->count_all($type, false, $this->groups);
		else 
		{
			$recipient_count = $this->CI->Customer->count_all($type, $this->individuals, false);
			$recipient_count += $this->CI->Customer->count_all($type, false, $this->groups);
		}
				
	    return $recipient_count;
	}

  public function get_recipients($type, $course_id = false)
  {
    $type = strtolower($type);

   	$idx  = $type == 'text' || $type == 'phone' ? 'phone_number' : 'email';
	//$idx  = $type == 'email' ? 'email' : 'phone_number';
    
    $recipients = array();

    // get individual email if existing
    if(!empty($this->individuals))
    {
log_message('error', 'right inside !empty($this->individuals');
        if ($type == 'text' || $type == 'phone')
	  		$res = $this->CI->Customer->get_multiple_info_for_text($this->individuals, $type);
		else
			$res = $this->CI->Customer->get_multiple_info_for_email($this->individuals, $type);
			//echo $this->CI->db->last_query();
   	 log_message('error', 'daemon sql '.$this->CI->db->last_query());
    
      foreach($res->result_array() as $in)
      {      	
        // dont include those without email/phone_number or opted out
        	// echo "email contact";
        	// echo "<pre>";
			// print_r($in);
			// echo "</pre>";
        if(empty($in[$idx]) || ($in["opt_out_{$type}"])) {
    		// echo "Not including this customer";
			// echo "<pre>";
			// print_r($in);
			// echo "</pre>";
        	continue;
		}
       	
        // making the email as a key will make sure that no email will be sent twice
        $recipients[strtolower($in[$idx])] = array(
            "{$idx}" => $in[$idx],
            'customer_id' => $in['person_id'],
            'sendhub_id' => $in['sendhub_id'],            
            'first_name' => $in['first_name'],
            'phone_number'=>$in['phone_number']//,
            //'sendhub_phone_number'=>$in['sendhub_phone_number']
        );
      }
    }
  	// get emails from the
    if(!empty($this->groups))
    {
    log_message('error', 'right inside !empty($this->groups');
	  $is_group_message = true;
	  if ($type == 'text' || $type == 'phone')
	      $resg = $this->CI->Customer->get_multiple_group_info_for_text($this->groups, $idx, $is_group_message, $course_id);
	  else 
	      $resg = $this->CI->Customer->get_multiple_group_info_for_email($this->groups, $idx, $is_group_message, $course_id);
	  	
	 log_message('error', 'daemon sql '.$this->CI->db->last_query());
	    // echo "Group Results:";
	    // echo "<pre>";
		// print_r($resg->result_array());
		// echo "</pre>";
	  foreach($resg->result_array() as $ing)
      {      	
        //$info = $this->CI->Customer->get_info($ing['person_id']);
        //$info_arr = (array) $info;

		// echo "group email contact";
        	 //echo "<pre>";
			 //print_r($ing);
			 //echo "</pre>";
        	//echo $idx;
        	if(empty($ing[$idx]) || ($ing["opt_out_{$type}"])){
    		 //echo "Not including this group customer in email";
			 //echo "<pre>";
			 //print_r($ing);
			 //echo "</pre>";
        	continue;
        } 
		
        $recipients[strtolower($ing[$idx])] = array(
            "{$idx}" => $ing[$idx],
            'customer_id' => $ing['person_id'],
            'sendhub_id' => $ing['sendhub_id'],            
            'first_name' => $ing['first_name'],
            'phone_number'=>$ing['phone_number']//,
            //'sendhub_phone_number'=>$ing['sendhub_phone_number']
        );
      }
    }		
    return $recipients;
  }
  // reset array
  public function reset()
  {
     $this->groups = array();
     $this->individuals = array();
  }
}

/* End of file Marketing_recipients_lib.php */
/* Location: ./application/controllers/Marketing_recipients_lib.php */