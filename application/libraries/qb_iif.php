<?php
/**
 * QuickBooks IIF class
 *
 * Very simple class to build QuickBooks IIF (specialized CSV) file for 
 * import to Quick Books
 *
 * @author		Jordan Bush
 */

class Qb_iif {
	
	private $lines = array();

	public function add($line){
		$this->lines[] = $line;
		return $this;
	}
	
	// Loop through each IIF line and convert data into CSV format
	public function generate(){
		
		$contents = '';
		$delimiter = ',';
		$enclosure = '"';
		$data = array();
		
		foreach($this->lines as $line){
			$data = array_merge($data, $line->get_data());
		}
		
		// Open new memory handle
		$handle = fopen('php://temp', 'r+');
		
		// Generate each CSV line in memory
		foreach ($data as $line) {
			fputcsv($handle, $line, $delimiter, $enclosure);
		}
		
		rewind($handle);
		while (!feof($handle)) {
			$contents .= fread($handle, 8192);
		}
		
		fclose($handle);
		return $contents;
	}
}

// QuickBooks IIF transaction line
class Qb_IIF_Transaction {
	
	var $split_num = 1;
	private $trans_columns = array(
		'!TRNS',
		'TRNSID',
		'TIMESTAMP',
		'TRNSTYPE',
		'DATE',
		'ACCNT',
		'NAME',
		'CLASS',
		'AMOUNT',
		'MEMO',
		'DOCNUM'
	);
	
	private $split_columns = array(
		'!SPL',
		'SPLID',
		'TIMESTAMP',
		'TRNSTYPE',
		'DATE',
		'ACCNT',
		'NAME',
		'CLASS',
		'AMOUNT',
		'MEMO',
		'DOCNUM'
	);
	
	private $transaction = array();
	private $splits = array();
	
	public function __construct($trans_id, $timestamp, $trans_type, $date, $account, $name, $class, $amount, $memo, $docnum){
		
		// Add transaction line
		$this->transaction = array(
			'TRNS',
			$trans_id,
			$timestamp,
			$trans_type,
			$date,
			$account,
			$name,
			(string) $class,
			(float) $amount,
			(string) $memo,
			(string) $docnum
		);
	}
	
	// All splits should add up to be opposite of transaction line
	public function add_split($trans_id, $timestamp, $trans_type, $date, $account, $name, $class, $amount, $memo = '', $docnum = ''){
		
		if(empty($trans_id)){
			$trans_id = $this->split_num;
		}
		$this->split_num++;
		
		// Add split transaction line
		$this->splits[] = array(
			'SPL',
			$trans_id,
			$timestamp,
			$trans_type,
			$date,
			$account,
			$name,
			(string) $class,
			(float) $amount,
			(string) $memo,
			(string) $docnum
		);
		
		return $this;
	}
	
	public function get_data(){
		
		// Define which columns are being used
		$data[] = $this->trans_columns;
		$data[] = $this->split_columns;
		$data[] = array('!ENDTRNS');
		
		// Add data lines
		$data[] = $this->transaction;
		$data = array_merge($data, $this->splits);
		$data[] = array('ENDTRNS');
		
		return $data;
	}
}

/* End of file qb_iif.php */
