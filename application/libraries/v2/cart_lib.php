<?php
class Cart_lib	{
    var $itemTax;

    function __construct()
    {
        $this->itemTax = new fu\items\ItemTax();
    }

	// Calculate subtotal of item
	function calculate_subtotal($price, $quantity, $discount = 0){
		$discount_ratio = round((100 - $discount) / 100, 5);
		$discounted_price = round($price * $discount_ratio, 2);
		return round($discounted_price * $quantity, 2);
	}

	// Take list of taxes and item amount, returns total tax amount
	function calculate_tax($subtotal, &$taxes, $tax_included = false){
		return $this->itemTax->calculate_tax($subtotal, $taxes, $tax_included);
	}

	// Takes a item dollar amount and calculates amount of tax for that
    // amount based on item
    function calculate_item_tax($subtotal, $item_id, $item_type){
        return $this->itemTax->calculate_item_tax($subtotal, $item_id, $item_type);
    }
	
	function standardize_credit_card_type($type){
		
		switch(strtoupper($type)){
			case 'MASTERCARD':
				$card_type = 'M/C';
			break;
			case 'VISA':
				$card_type = 'VISA';
			break;
			case 'DISCOVER':
				$card_type = 'DCVR';
			break;
			case 'AMERICANEXPRESS':
				$card_type = 'AMEX';
			break;
			case 'DINERS':
				$card_type = 'DINERS';
			break;
			case 'JCB':
				$card_type = 'JCB';
			break;
			default:
				$card_type = $type;
		}
		
		return $card_type;		
	}

	function get_filtered_item_total($cart_items, $field, $filter, $total_field='total'){

		$valid_total = 0;	
		foreach($cart_items as $item){
			if($item[$field] == $filter){
				$valid_total += $item[$total_field];
			}
		}
		return $valid_total;
	}

	// Signature data is base64 encoded from browser
	// eg: data:image/png;base64,iVBORw0KGgoAAAANS
	function save_signature($signature_data, $payment, $cart_id){
		
		if(empty($signature_data) || empty($payment)){
			return false;
		}

		$image = base64_decode(
			substr(
				$signature_data, 
				strpos($signature_data, ',')
			)
		);

		$payment_str = $cart_id.':'.$payment['type'].':'.$payment['record_id'];
		$filename = md5($payment_str).'.png';

		if(ENVIRONMENT == 'development'){
			$filename = 'development/'.$filename;
		}

		$s3 = new fu\aws\s3();
		$s3->set_is_stream(true);
		$s3->uploadToSignatureBucket($image, $filename);
		
		return $filename;
	}
}