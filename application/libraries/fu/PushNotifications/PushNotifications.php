<?php
namespace fu\PushNotifications;

/*
$config['roost_url']
$config['roost_api_key']
$config['roost_api_secret']
$config['roost_api_js_src']
$config['roost_push_default_image']
$config['roost_push_default_url']
$config['roost_push_default_title']
*/

class PushNotifications {

    // Default Values
    protected $enabled = false;
    protected $url = '';
    protected $api_key = '';
    protected $api_secret = '';
    protected $js_source_url = '';
    protected $push_message = '';
    protected $push_url = '';
    protected $push_title = '';
    protected $push_image = '';
    protected $segments = array ();
    private $result = '';

    public function __construct()
    {
        $this->CI = & get_instance();
        $this->getCIConfig();
    }

    protected function getCIConfig()
    {
        // Do some basic validation. Just make sure all the config is there
        if(isset($this->CI->config->config['roost_enable']) && $this->CI->config->config['roost_enable'] === true){
            // Read in config defaults
            $this->url = $this->CI->config->config['roost_url'];
            $this->api_key = $this->CI->config->config['roost_api_key'];
            $this->api_secret = $this->CI->config->config['roost_api_secret'];
            $this->js_source_url = $this->CI->config->config['roost_api_js_src'];

            $this->push_url = $this->CI->config->config['roost_push_default_url'];
            $this->push_title = $this->CI->config->config['roost_push_default_title'];
            $this->push_image = $this->CI->config->config['roost_push_default_image'];

            if($this->url && $this->api_key && $this->api_secret && $this->js_source_url){
                $this->enabled = true;
            } else {
                $this->enabled = false;
                return;
            }

        } else {
            $this->enabled = false;
            return;
        }
    }

    public function setSegments($newSegments){
        // TODO: Update to support adding to and removing from segments
        if(!is_array($newSegments)){
            return;
        } else {
            $this->segments = $newSegments;
        }
    }

    public function getResult()
    {
        return $this->result;
    }

    public function sendMessage($message, $link='', $title='', $image='')
    {
        // Basic validation
        if(strlen($message) < 5){
            $this->result = array('success'=>false, 'message'=>'Message must be at least 5 characters.');
            return false;
        }

        // Attempt to set any missing (not required) values
        if($link == ''){
            // TODO: Validate URL with regex instead of simple length, possibly
            if(strlen($this->push_url) < 12){
                $link = $this->push_url;
            } else {
                $this->result = array('success'=>false, 'message'=>'Must include a message and a URL.');
                return false;
            }
        }
        if($title == ''){
            $title = $this->push_title;
        }
        if($image == ''){
            $image = $this->push_image;
        }

        // Load values into array for sending
        $data = array(
            'alert' => $message,
            'url' => $link
            );
        if($title != '') $data['title'] = $title;
        if($image != '') $data['imageURL'] = $image;
        if(!empty($this->segments)){
            $data['segments'] = $this->segments;
        }

        // Prepare array for transport
        $data = json_encode($data);

        // Make the CURL Request
        $ch=curl_init($this->url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->api_key . ":" . $this->api_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $this->result = (curl_exec($ch));
        curl_close($ch);

        $this->result = json_decode($this->result);

        if($this->result->{'success'} === true){
            return true;
        } else {
            return false;
        }
    }

}