<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 7/1/2016
 * Time: 4:20 PM
 */

namespace fu\locks;


class invoice_charge extends lock
{
    public function __construct($fields)
    {
        parent::__construct($fields);
        $this->required_fields = ["invoice_id"];
        $this->namespace = "invoice_charge";
    }

}