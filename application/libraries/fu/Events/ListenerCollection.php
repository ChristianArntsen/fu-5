<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 10/19/2016
 * Time: 5:00 PM
 */

namespace fu\Events;

use Aws\Sqs\SqsClient;
use Onoi\EventDispatcher\EventListenerCollection;


class ListenerCollection implements EventListenerCollection   {

	private $eventListenerCollection;

	public function __construct( EventListenerCollection $eventListenerCollection ) {
		$this->eventListenerCollection = $eventListenerCollection;
	}
	private function sendMessage($id, $event,$teesheet_id = false)
	{
		//If course event's isn't turned on then don't worry
		$this->CI = & get_instance();
		if($this->CI->config->item('emit_events')){
			$s3Config = $this->CI->config->item('s3');
			$sqsConfig = $this->CI->config->item('sqs');
			$course_id = $this->CI->session->userdata('course_id');
			$client = SqsClient::factory(array(
				'region' => 'us-west-2',
				'credentials' => [
					'key' => $s3Config["access_key"],
					'secret' => $s3Config["secret"],
				],
				'version' => '2012-11-05'
			));

			$bodyContent = [
				"id" => $id,
				"course_id" => $course_id,
				"event_name" => $event
			];
			if($teesheet_id){
				$bodyContent['teesheet_id'] = $teesheet_id;
			}

			$client->sendMessage(array(
				'QueueUrl' => $sqsConfig['course_events_url'],
				'MessageBody' => json_encode($bodyContent),
			));
		}

	}

	public function getCollection() {

		$this->eventListenerCollection->registerCallback( 'customer.updated', function($context) {
			if($context->has("person_id")){
				$this->sendMessage($context->get("person_id"),"customer.updated");
			}
		} );
		$this->eventListenerCollection->registerCallback( 'teetime.updated', function($context) {
			$this->sendMessage($context->get("tt_id"), "teetime.updated",$context->get("teesheet_id"));
		} );
		$this->eventListenerCollection->registerCallback( 'teetime.deleted', function($context) {
			$this->sendMessage($context->get("tt_id"), "teetime.deleted",$context->get("teesheet_id"));
		} );
		$this->eventListenerCollection->registerCallback( 'sale.created', function($context){
			if($context->has("sale_id")){
				$this->sendMessage($context->get("sale_id"), "sale.created");
			}
		} );
		return $this->eventListenerCollection->getCollection();
	}

}