<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 10/19/2016
 * Time: 5:04 PM
 */

namespace fu\Events;


class EventDispatcher
{
	public static function getDispatcher()
	{
		$eventDispatcherFactory = new \Onoi\EventDispatcher\EventDispatcherFactory();
		$eventDispatcher = $eventDispatcherFactory->newGenericEventDispatcher();
		$listenerCollection = new \fu\Events\ListenerCollection(
			$eventDispatcherFactory->newGenericEventListenerCollection()
		);
		$eventDispatcher->addListenerCollection($listenerCollection);
		return $eventDispatcher;
	}
}