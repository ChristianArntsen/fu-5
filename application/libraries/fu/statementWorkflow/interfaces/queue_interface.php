<?php
namespace fu\statementWorkflow\interfaces;

interface queue_interface {
	// protected function getQueueClient()

	public function getQueue($name,$production);

	public function deleteQueue($name);

	public function enqueueMessage($queue, $message);

	public function fetchMessages($queue);

	public function deleteMessage ($queue, $message);
}

?>