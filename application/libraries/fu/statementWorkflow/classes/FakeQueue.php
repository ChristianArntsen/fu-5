<?php
namespace fu\statementWorkflow\classes;

if(ENVIRONMENT !== 'testing' && ENVIRONMENT !== 'jenkins')
	require_once '/var/www/development.foreupsoftware.com/fu/application/vendor/autoload.php';

use Aws\Sqs\Exception\SqsException;
use fu\statementWorkflow\classes\fake_message;
use fu\statementWorkflow\interfaces\queue_interface;

class fakeQueue implements queue_interface {
	//
	private $queue_hash = [];
	private $id_counter = 0;

	protected function resetLastError() {
		$this->last_error = null;
	}

	protected function getLastError() {
		return $this->last_error;
	}

	public function getQueue($name,$production)
	{
		if($production){
			throw new \Exception('It appears we are using a FakeQueue in production. Is that what we want?');
		}
		if(!empty($this->queue_hash[$name])){
			return $this->queue_hash[$name];
		}

		$this->queue_hash[$name] = [];
		return $name;
	}

	public function getQueueClient(){
		return true;
	}

	public function clearQueues()
	{
		foreach($this->queue_hash as $key=>$value){
			$this->clearQueue($key);
		}
	}

	public function clearQueue($name)
	{
		$this->queue_hash[$name] = [];
	}

	public function deleteQueue($name)
	{
		unset($this->queue_hash[$name]);
	}

	public function enqueueMessages($queue, $messages, $group = 'group1')
	{
		$results = [];
		foreach ($messages as $message){
			$results[] = $this->enqueueMessage($queue,$message, $group);
		}
	}

	public function enqueueMessage($queue, $message, $group = 'group1')
	{
		$message = new fake_message($this->id_counter++,$message);
		array_push($this->queue_hash[$queue],$message);
	}

	public function fetchMessages($queue)
	{
		$count = 0;
		$ret = [];
		foreach($this->queue_hash[$queue] as $message){
			if($count >= 10) break;
			if($message->visible() && !$message->deleted()){
				$ret[] = $message;
				$count++;
			}
		}

		return $ret;
	}

	public function getAttributes(){
	    return [
	        'ApproximateNumberOfMessages' => 0,
            'ApproximateNumberOfMessagesNotVisible' => 0
        ];
    }

	public function deleteMessage($queue,$message)
	{
		foreach($this->queue_hash[$queue] as &$msg){
			if($msg->get_id() === $message->get_id()){
				$msg->delete();
			}
		}
	}
}

?>