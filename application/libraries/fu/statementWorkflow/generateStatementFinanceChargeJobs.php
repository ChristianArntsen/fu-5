<?php
namespace fu\statementWorkflow;

use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\resource_transformers\account_statements_transformer;
use fu\statementWorkflow\classes\silex_handler;
use fu\statementWorkflow\interfaces\queue_interface;

class generateStatementFinanceChargeJobs
{
	use \fu\statementWorkflow\traits\daemon_trait;

	/**
	 * @var silex_handler
	 */
	public $silex;

	private $CI;

	public $run_cycles = 0,$messages_received = 0,$messages_sent = 0;

	public function __construct(queue_interface &$queue)
	{
		$prefix = '';
		$production = true;
		if(ENVIRONMENT !== 'production'){
			$prefix = ENVIRONMENT.'_';
			$production = false;
		}

		$this->queue = &$queue;

		$this->charge_queue_name = $prefix.'processStatementFinanceChargeJobs';
		$this->queue->getQueue($this->charge_queue_name,$production);

		$this->silex = new silex_handler();
		$this->CI = \get_instance();

		$this->repo = $this->silex->db->getRepository('e:ForeupAccountStatements');

		date_default_timezone_set('UTC');
	}

	public function __destruct()
	{
		unset($this->silex);
	}

	public function _get_queue()
	{
		return $this->queue;
	}

	public function run($current_time = null, $test = false)
	{
		$this->run_cycles++;
		if(!isset($current_time))$current_time = new \DateTime();
		elseif(is_string($current_time))$current_time = new \DateTime($current_time);

		// get all the +balance statements with finance charges enabled
		$delayed = $this->getDelayedFinanceStatements($current_time);
		if(method_exists($delayed,'toArray'))$delayed = $delayed->toArray();

		// filter out any that have not been delayed enough
		$dueNow = $this->filterNotDelayedEnough($current_time,$delayed);

		$this->messages_received = count($dueNow);

		// enqueue them
		$count = 0;
		foreach($dueNow as $statement){
			$count++;
			$charge = $statement->getRecurringStatement()->getRecurringCharge();

			if(!empty($charge->getDateDeleted())){
				continue;
			}

			$result = $this->queueStatementChargeJob($statement);
			$statement->setDateFinanceChargeQueued($current_time);
			$this->messages_sent++;
		}

		return $count;

	}

	public function queueStatementChargeJob(ForeupAccountStatements $statement)
	{
		$transformer = new account_statements_transformer();
		$statement_data = $transformer->transform($statement);
		return $this->queue->enqueueMessage($this->charge_queue_name,\json_encode($statement_data));
	}

	public function filterNotDelayedEnough(\DateTime $current_time,array $statements){
		$ret = [];
		foreach($statements as &$statement)
		{
			$delay = (int) $statement->getFinanceChargeAfterDays();
			$delta = $current_time->diff($statement->getDateCreated());

			$delta_days = $delta->days*-1;
			if($delta->invert)$delta_days*=-1;

			if($delta_days > $delay)$ret[] = &$statement;
		}

		return $ret;

	}

	public function getDelayedFinanceStatements(\DateTime $current_time = null)
	{
		$oneDayAgo = new \DateTime($current_time->format(DATE_ISO8601));
		$oneDayAgo->sub(new \DateInterval('P1D'));

		$criteria = new \Doctrine\Common\Collections\Criteria();
		$criteria->where($criteria->expr()->orX(
			$criteria->expr()->lt('dueDate',$oneDayAgo)
		));
		$criteria->andWhere($criteria->expr()->isNull('datePaid'));
		$criteria->andWhere($criteria->expr()->eq('financeChargeEnabled',1));
		$criteria->andWhere($criteria->expr()->gte('financeChargeAfterDays',0));
		$criteria->andWhere($criteria->expr()->isNull('dateDeleted'));
		$criteria->andWhere($criteria->expr()->isNull('dateFinanceChargeQueued'));
		$criteria->andWhere($criteria->expr()->gt('totalOpen',0));
		//$criteria->orderBy(['dateCreated','delayDays']);

		return $this->repo->matching($criteria);

	}
}

?>
