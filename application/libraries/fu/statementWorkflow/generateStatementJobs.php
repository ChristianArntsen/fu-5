<?php
// generateStatementJobs.php
// pulls statements to be generated from the database
// submits them to the queue as jobs
namespace fu\statementWorkflow;

use Doctrine\ORM\PersistentCollection;
use foreup\rest\models\entities\ForeupRepeatable;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\resource_transformers\account_recurring_charge_customers_transformer;
use foreup\rest\resource_transformers\account_recurring_statements_transformer;
use foreup\rest\resource_transformers\account_statements_transformer;
use foreup\rest\resource_transformers\repeatable_transformer;
use fu\marketing\recipientRetriever\unsent;
use fu\statementWorkflow\interfaces\queue_interface;
use \models\Repeatable\Row as RepeatableObject;

class generateStatementJobs
{

	use \fu\statementWorkflow\traits\daemon_trait;

	public $run_cycles = 0;
	public $messages_received = 0;
	public $messages_sent = 0;
	public $silex;
	private $queue,$group_hash=[];

	/**
	 * generateStatementJobs constructor.
	 * @param queue_interface $queue
	 */
	public function __construct(queue_interface &$queue)
	{
		$prefix = '';
		$production = true;
		if(ENVIRONMENT !== 'production'){
			$prefix = ENVIRONMENT.'_';
			$production = false;
		}

		$this->queue = $queue;
		$this->queue_name = $prefix.'ForeupRepeatableAccountRecurringStatementsToCustomers';
		$this->queue->getQueue($this->queue_name,$production);

		$this->charge_queue_name = $prefix.'ForeupRepeatableAccountRecurringChargeItemsToCustomers';
		$this->queue->getQueue($this->charge_queue_name,$production);

		$this->silex = new \fu\statementWorkflow\classes\silex_handler();

		$this->repTransformer = new repeatable_transformer();
		$this->cstTransformer = new account_recurring_charge_customers_transformer();
		$this->statement_transformer = new account_recurring_statements_transformer();

		$this->groups = [];

        date_default_timezone_set('UTC');
	}

	public function __destruct()
	{

		$this->silex->db->getConnection()->close();
		unset($this->silex);
	}

	/**
	 * @return queue_interface
	 */
	public function _get_queue(){

		return $this->queue;

	}

	/**
	 * @param null $current_time
	 * @param bool $test
	 */
	public function run($current_time = null, $test = false)
	{

		$this->run_cycles++;
		if(!isset($current_time))
			$current_time = new \DateTime();
		elseif (is_string($current_time))
			$current_time = new \DateTime($current_time);

		// get repeatable statements from the database
		$repeatables = $this->fetchRepeatables($current_time);
		$count = 0;

		if(!$this->checkReadyToRun()){
			echo 'Not ready<br>';
			return 0;
		}

		$courses = $this->silex->db->getRepository('e:ForeupCourses');

		foreach($repeatables->getIterator() as $rep) {

			$array = $rep->toRuleArray();
			$array['id'] = $rep->getId();
			$statement = $rep->getStatement();

			$charge = $statement->getRecurringCharge();
			if(empty($charge) || !empty($charge->getDateDeleted())){
				continue;
			}

            $nextOccurrence = $rep->getNextOccurence();
            if(empty($nextOccurrence)){
                continue;
            }
			$plain =  $nextOccurrence->format('Y-m-d');
            $nextOccurrence = $nextOccurrence->format(DATE_ISO8601);

            if(!isset($charge) || $charge->getDeletedBy())continue;
            $count++;
            $this->messages_received++;

			$statement_array = $this->statement_transformer->transform($statement);

			// we use the course id as the group id
			//$group = $charge->getOrganizationId().'';
			$group = implode('/',[$charge->getOrganizationId(), $statement->getId(), $plain]);

			if(!in_array($group,$this->groups)){
				$this->groups[] = $group;
				$this->group_hash[$group] = ['count'=>0];
			}

			// get customers for statement
			$customers = $charge->getAccountRecurringChargeCustomers();

			if(isset($customers)) {

				$customers = $this->customersEntityToArray($customers);
				foreach($customers as $customer) {
					$this->queue->enqueueMessage($this->queue_name, \json_encode(['repeatable' => $array, 'customer' => $customer, 'statement'=>$statement_array,'current_time'=>$nextOccurrence]), $group);
					$this->messages_sent++;
					$this->group_hash[$group]['count']++;
				}
			}

			$course_id = $charge->getOrganizationId();
			$course = $courses->find($course_id);
			$time_zone = $course->getTimezone();
			if(!$test) {
				// update last ran for the repeatable object
				$last_ran = $rep->getNextOccurence();
				$last_string = $last_ran->format('Y-m-d H:i:s');
				$old = new \DateTime($last_string . 'America/Denver');
				$old->setTimezone(new \DateTimeZone($time_zone));
				// does next calc occur
				$next_time = $this->getNextOccurence($old,$rep);
				$nu = $next_time;
				if(isset($next_time)) {
					$next_string = $next_time->format('Y-m-d H:i:s');
					$nu = new \DateTime($next_string . $time_zone);
					$nu->setTimezone(new \DateTimeZone('America/Denver'));
				}
				$rep->setNextOccurence($nu);
				$rep->setLastRan($last_ran);

				// propagage change to database
				$this->silex->db->flush();
			}
		}

		// Stick these at the end of the queue to indicate the end of the groups
		foreach($this->groups as $group){
			$this->queue->enqueueMessage($this->queue_name, \json_encode(['groupEnd'=>$group, 'count'=>$this->group_hash[$group]['count']]));
		}

		return $count;

	}

	public function checkReadyToRun()
	{
		$attr = $this->queue->getAttributes($this->charge_queue_name);

        if($attr['ApproximateNumberOfMessages'] > 0 || $attr['ApproximateNumberOfMessagesNotVisible'] > 0){
            return false;
        }
        return true;
	}

	public function getNextOccurence($current_time,ForeupRepeatable $repeatable)
	{
		$array = $repeatable->toRuleArray();
		if(!isset($current_time))$current_time = new \DateTime();
		$str_time = $current_time;
		if(!is_string($current_time)){
			$str_time = $current_time->format(DATE_ISO8601);
		}
		$ro = new RepeatableObject();
		$ro->createFromArray($array);
		$next_time = $ro->getNextCalculatedOccurrence($str_time);
		if(strtotime($str_time) >= strtotime($next_time->format(DATE_ISO8601))){
			$tmp = new \DateTime($current_time->format(DATE_ISO8601));
			$tmp->add(new \DateInterval('PT1S'));
			$next_time = $ro->getNextCalculatedOccurrence($tmp->format(DATE_ISO8601));
		}
		return $next_time;
	}

	/**
	 * @param $current_time
	 * @return mixed
	 */
	public function fetchRepeatables($current_time){

		$ret = [];

		if(is_string($current_time))
			$current_time = new \DateTime($current_time);

		$repo = $this->silex->db->getRepository('e:ForeupRepeatableAccountRecurringStatements');
		$criteria = new \Doctrine\Common\Collections\Criteria();

        $criteria->andWhere($criteria->expr()->orX(
            $criteria->expr()->lt('lastRan',$current_time),
            $criteria->expr()->isNull('lastRan')
        ));
        $criteria->andWhere($criteria->expr()->orX(
            $criteria->expr()->lte('nextOccurence',$current_time),
            $criteria->expr()->isNull('nextOccurence')
        ));
        $criteria->andWhere($criteria->expr()->orX(
            $criteria->expr()->lte('dtstart',$current_time),
            $criteria->expr()->isNull('dtstart')
        ));
        $exp = $criteria->getWhereExpression();


		return $repo->matching($criteria);

	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection|PersistentCollection $entities
	 * @return array
	 */
	public function customersEntityToArray ($entities )
	{

		$array = [];
		if(method_exists($entities,'toArray')){
			$entities = $entities->toArray();
		}
		foreach($entities as $entity) {
			$array[] = $this->cstTransformer->transform($entity);
		}
		return $array;

	}
}
?>