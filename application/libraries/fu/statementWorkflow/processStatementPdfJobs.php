<?php
namespace fu\statementWorkflow;

use Doctrine\Common\Collections\Criteria;
use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\resource_transformers\course_transformer;
use foreup\rest\resource_transformers\customer_transformer;
use fu\aws\s3;
use fu\statementWorkflow\classes\silex_handler;
use fu\statementWorkflow\interfaces\queue_interface;
use fu\statementWorkflow\traits\daemon_trait;
use PHPZip\Zip\File\Zip;

require_once __DIR__.'/../../../vendor/autoload.php';

class processStatementPdfJobs
{
	use daemon_trait;

	private $queue;
	private $input_queue_name;
	private $error_queue_name;
	private $silex;

	public $run_cycles = 0;
	public $messages_received = 0;
	public $messages_sent = 0;
	public $errors_sent = 0;

	public function __construct(queue_interface &$queue)
	{
		$prefix = '';
		$production = true;
		if(ENVIRONMENT !== 'production'){
			$prefix = ENVIRONMENT.'_';
			$production = false;
		}

		$this->queue = &$queue;
		$this->input_queue_name = $prefix.'ForeupRenderStatementPdf';
		$this->queue->getQueue($this->input_queue_name,$production);

		$this->error_queue_name = $prefix.'processStatementPdfJobsErrors';
		$this->queue->getQueue($this->error_queue_name,$production);

		$this->silex = new silex_handler();
		$this->CI = \get_instance();

        date_default_timezone_set('UTC');
	}

	public function __destruct()
	{

		$this->silex->db->getConnection()->close();
		unset($this->silex);

	}

	public function _get_queue()
	{

		return $this->queue;

	}

	public function _set_input_queue(queue_interface &$queue)
	{

		$this->queue = &$queue;

	}

	private function handle_error($result)
	{
        echo '<br><br>';
        echo \json_encode($result);
		echo '<br><br>';

		$this->queue->enqueueMessage($this->error_queue_name,\json_encode($result));
		$this->errors_sent++;

	}

	public function run($current_time = null, $test = false)
	{

		$this->run_cycles++;
		// fetch messages from input queue
		$messages = $this->queue->fetchMessages($this->input_queue_name);

		// I don't think we care about current time
		// The statement knows when it was generated...
		if(!isset($current_time))
			$current_time = new \DateTime();

		$count = 0;
		foreach($messages as $message) {
			$count++;
			$this->messages_received++;
			// extract message contents
			try {
				$msg_array = $this->extractMessage($message);
			}catch(\Exception $e){

				$error = [];
				$error['message'] = $e->getMessage();
				$error['trace'] = $e->getTraceAsString();
				$result = ['error'=>$error,'message'=>$message];
				$this->handle_error($result);
				$this->queue->deleteMessage($this->input_queue_name,$message);
				continue;
			}

			if(isset($msg_array['groupEnd'])){

				// last one for the site
				// zip up all of the pdf files from s3
				try{
					$folder = explode('T',$msg_array['groupEnd']);
					$file_name = $this->zipGroup($folder[0],'/tmp/processStatementPdfJobs/');

					// sometimes the FIFO queue fouls up. Send it through again...
					if(!$file_name)continue;
				}
			    catch(\Exception $e){

					$error = [];
					$error['message'] = $e->getMessage();
					$error['trace'] = $e->getTraceAsString();
					$result = ['error'=>$error,'message'=>$msg_array];
					$this->handle_error($result);
				    $this->queue->deleteMessage($this->input_queue_name,$message);
					continue;

				}

				$url = $this->storeZipOnS3(sha1($msg_array['groupEnd'].'/archive.zip').'.zip',$file_name);

				// clean up directory

				// email the url
				$rep = $this->silex->db->getRepository('e:ForeupCourses');
				$exploded = explode('/',$msg_array['groupEnd']);
				$course = $rep->find($exploded[0]);

				$summary = $this->getSummaryData($exploded[0],$exploded[2],$exploded[2],false);

				$html = $this->composeManagerEmail($summary);

				$html .= 'Here is a link to customer statements: '.$url;

				$this->sendCourseEmail($course,$html);
				$this->messages_sent++;
				$this->queue->deleteMessage($this->input_queue_name,$message);

			}
			elseif(!isset($msg_array['statement'])){
				$this->handle_error(['type'=>'malformed message','message'=>$msg_array]);
				$this->queue->deleteMessage($this->input_queue_name,$message);
			}
			else {

				// get the statement charges
				$statement =  $msg_array['statement'];
				$customer = $msg_array['customer'];

				if($test){
					// populate statement with a known good statement
					$rep = $this->silex->db->getRepository('e:ForeupAccountStatements');
					$statement = $rep->find(1);

				}elseif(!isset($statement)){
					continue;

				}

				$statement_charges = $statement->getCharges();

				// get the other info for the pdf
				$account_transactions = $this->silex->app['statementUtilities']->getAccountTransactions($statement,$customer);

				$rep = $this->silex->db->getRepository('e:ForeupCourses');
				$course = $rep->find($msg_array['courseId']);

				$snappy = new  \Knp\Snappy\Pdf();
				if(is_file('application/bin/wkhtmltopdf'))
					$snappy->setBinary("application/bin/wkhtmltopdf");
				else {
					$snappy->setBinary("/var/www/development.foreupsoftware.com/fu/application/bin/wkhtmltopdf");
				}

				try {
					$local_file = $this->silex->app['statementUtilities']->buildPdf($statement,
						$statement_charges, $account_transactions['customer'], $account_transactions['member'], $account_transactions['invoice'], $snappy);
				}
			    catch(\Exception $e){

					$error = [];
					$error['message'] = $e->getMessage();
					$error['trace'] = $e->getTraceAsString();
					$result = ['error'=>$error,'message'=>$msg_array];
					$this->handle_error($result);
					$this->queue->deleteMessage($this->input_queue_name,$message);
					echo $e->getMessage();
					continue;

				}

				$friendly = $this->silex->app['statementUtilities']->getFriendlyName($statement);
				$aws_file = str_replace('/tmp/','',$friendly);

				// store pdf on s3
				$result = $this->storePdfOnS3($local_file,$aws_file);

				$uri = 'foreupsoftware.com';
				if(ENVIRONMENT === 'testing')$uri = 'development.foreupsoftware.com';

				// put together a link to the pdf
				$link = 'https://'.
					$uri.
					'/api_rest/index.php/courses/'.
					$course->getCourseId().
					'/accountStatements/'.
					$statement->getId().
					'/pdf';

				$html = $this->composeUserEmail(str_replace('.pdf','.html',$local_file));

				// send email to customer
				if($statement->getSendEmail())
					$this->sendEmail($course,$customer,$statement,$html,$test);

				$this->messages_sent++;

				// clean up local files
				unlink($local_file);
				unlink(str_replace('.pdf','.html',$local_file));

			}

			// if we make it, delete the message
			$this->queue->deleteMessage($this->input_queue_name,$message);

		}

		return $count;

	}

	public function composeUserEmail(string $local_file)
	{
		//$html = 'Your statement is available. Please follow the following link: <a href="'.$link.'">View Statement</a><br><br> ';

		//$html .= 'Or paste the following into your URL bar: '.$link;
		//$html.=$link;
		$html = file_get_contents($local_file);

		return $html;
	}

	public function composeManagerEmail($summary)
	{
		$html = '';
		$html .= 'Statements Generated: '.$summary['runCount'];
		$html .= '<br>';
		$html .= "Statements Charged: ".$summary['paidCount'];
		$html .= '<br>';
		$html .= "Charges Failed: ".$summary['failedCount'];
		$html .= '<br>';
		foreach($summary['failed'] as $failure){
			$test = new ForeupAccountStatements();
			$test->getCustomer();
			$test->getCharges();
			$number = $failure->getNumber();
			$person = $failure->getCustomer()->getPerson();
			$name = $person->getFirstName().' '.$person->getLastName();
			$charges = $test->getPayments();

			// TODO: Circle back and extract the payment type and info
			$html .= 'Statement Number: '.$number;
			$html .= '<br>';
			$html .= 'Customer: '.$name;
			$html .= '<br>';
			//$html .= '<br>';
		}

		return $html;
	}

	public function getSummaryData($courseId,$startDate,$endDate,$includeDeleted)
	{
		$ret = [];

		if(isset($startDate) && is_string($startDate))$startDate = new \DateTime($startDate);
		if(isset($endDate) && is_string($endDate))$endDate = new \DateTime($endDate);

		// Generate a report for a given date range
		// select for number of statements generated
		$repo = $this->silex->db->getRepository('e:ForeupAccountStatements');

		$criteria = new Criteria();
		$criteria->where($criteria->expr()->eq('organizationId', $courseId));
		if(isset($startDate))
			$criteria->andWhere($criteria->expr()->gte('startDate',$startDate));
		if(isset($endDate))
			$criteria->andWhere($criteria->expr()->lte('endDate',$endDate));
		if(!$includeDeleted)
			$criteria->andWhere($criteria->expr()->isNull('dateDeleted'));

		$result_set = $repo->matching($criteria);

		// select for number of statements paid
		$count = $result_set->count();

		// select for auto payments
		$criteria2 = new Criteria();
		$criteria2->where($criteria2->expr()->eq('autopayEnabled', 1));
		$criteria2->andWhere($criteria2->expr()->gt('total', 0));
		$criteria2->andWhere($criteria2->expr()->eq('totalOpen', 0));
		$paid_set = $result_set->matching($criteria2);
		$paidCount = $paid_set->count();

		// select for auto payment failures
		$criteria3 = new Criteria();
		$criteria3->where($criteria3->expr()->eq('autopayEnabled', 1));
		$criteria3->andWhere($criteria3->expr()->gt('autopayAttempts', 0));
		$criteria3->andWhere($criteria3->expr()->gt('totalOpen', 0));
		$failure_set = $result_set->matching($criteria3);
		$failedCount = $failure_set->count();
		// report statement number
		// report customer name
		// report payment type/number

		$ret['runCount'] = $count;
		$ret['paidCount'] = $paidCount;
		$ret['failedCount'] = $failedCount;
		$ret['failed'] = $failure_set->toArray();

		return $ret;
	}

	public function zipGroup($s3Directory,$targetDirectory)
	{

		$s3 = new s3();
		$zip = new Zip();

		$res = $s3->downloadAccountStatements('processStatementPdfJobs/'.$s3Directory,'/tmp/'/*,['debug'=>fopen('/tmp/aws_debug','a')]*/);
		echo '<br>download from: '.$s3Directory;
		echo '<br>download to: '.$targetDirectory.$s3Directory;
		echo '<br>result: '.$res;

		if(!file_exists($targetDirectory.$s3Directory)){
			mkdir($targetDirectory.$s3Directory,0766,true);
		}

		chmod($targetDirectory.$s3Directory,0766);

		if(!is_writable($targetDirectory.$s3Directory)){
			throw new \Exception('Statement staging directory not writable');
		}

		$file = $targetDirectory.$s3Directory.'/archive.zip';
		if(!file_exists($file)){
			$zip->setZipFile($file);
			chmod($file,0766);
		}


		$filelist = new \FilesystemIterator($targetDirectory.$s3Directory);

		$filelimit = 245;
		$processed = 0;

		$files = [];

		foreach($filelist as $key=>$value)
		{
			if($value->getFilename() === 'archive.zip')continue;
			elseif(strpos($value->getFilename(),'.html'))continue;
			//if ($zip->numFiles == $filelimit) {$zip->close(); $zip->open($file);}
			$files[] = $key;
			$zip->addFile(file_get_contents($key),$value->getFilename());

			$processed ++;

		}
		if(!$processed){
			echo '<br><br>no files? '.$targetDirectory.$s3Directory.'<br><br>';
		}

		$merged = $targetDirectory.$s3Directory.'/allFiles.pdf';
		$success = $this->silex->app['statementUtilities']->mergePdfFiles($merged,$files);
		if($success) {
			chmod($merged, 0766);
			$zip->addFile(file_get_contents($merged), 'allFiles.pdf');
			$zip->finalize();
		}
		return $success?$file:$success;

	}

	public function sendCourseEmail(ForeupCourses $course,$message)
	{

		$course_name = $course->getName();
		$course_email = $course->getEmail();
		if(is_string($course_email)){
			$course_email = explode(',',$course_email);
		}
		if($course->getBillingEmail())
		  $course_email =array_merge($course_email, explode(',',$course->getBillingEmail()));

		$campaign_id = 0;
		$course_id = false;
		$invoice_id = false;
		$status = send_sendgrid(
			$course_email,
			"Statement billing summary",
			$message,
			'',
			$course_name,
			$campaign_id,
			$course_id,
			$invoice_id
		);

	}

	public function sendEmail($course,$customer,ForeupAccountStatements $statement,$message,$test=false)
	{

		$email = $customer->getPerson()->getEmail();
		$course_name = $course->getName();
		$course_email = $course->getEmail();
		$campaign_id = 0;
		$course_id = false;
		$invoice_id = false;
		$status = send_sendgrid(
			$email,
			"Statement from $course_name",
			$message,
			$course_email,
			$course_name,
			$campaign_id,
			$course_id,
			$invoice_id
		);

		$statement->setDateEmailSent(new \DateTime());
		if(!$test) {
			try{
				$this->silex->db->flush();
			}catch(\Exception $e){
				echo $e->getMessage();
			}
		}

	}

	public function mergePdfFiles(string $outputFile, array $inputFiles)
	{
		// use ghostscript to merge pdfs
		$output = [];
		$return_var = null;

		$exec_string = 'gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile='.$outputFile.' '.implode(',',$inputFiles);

		exec($exec_string,$output,$return_var);

		return true;
	}

	public function storePdfOnS3($pdf_file,$name)
	{

		$s3 = new s3();
		// open
		// add file

		return $s3->uploadToAccountStatementsBucket($pdf_file,$name);

	}

	public function storeZipOnS3($name,$zip_file)
	{

		$s3 = new s3();
		// open
		// add file

		return $s3->uploadToAccountStatementsBucket($zip_file,$name,true);

	}

	/**
	 * @param $message
	 * @return array|mixed
	 */
	public function extractMessage($message)
	{

		$ret = [];
		if(method_exists($message,'get'))
			$body = $message->get('Body');
		elseif (is_array($message)){
			$body = $message['Body'];
		}
		$intermediate = \json_decode($body,true);

		if(isset($intermediate['groupEnd']))return $intermediate;
//*
		$rep = $this->silex->db->getRepository('e:ForeupAccountStatements');
		$cst = $this->silex->db->getRepository('e:ForeupCustomers');
		$ret['statement'] = $rep->find($intermediate['id']);
		if(!isset($ret['statement'])){
			throw new \Exception('Statement not found: '.$intermediate['id']);
		}
		$ret['courseId'] = $ret['statement']->getRecurringStatement()->getRecurringCharge()->getOrganizationId();
		$ret['customer'] =  $cst->findOneBy(['person'=>$intermediate['personId'],'course'=>$ret['courseId']]);
		$ret['current_time'] = $intermediate['current_time']??null;
//*/
		return $ret;

	}

	public function getAccountTransactions(ForeupAccountStatements $statement)
	{

		return $this->silex->app['statementUtilities']->getAccountTransactions($statement);

	}

	public function getSales($customer,$start,$end)
	{

		$repo = $this->silex->db->getRepository('e:ForeupSales');

		$criteria = new \Doctrine\Common\Collections\Criteria();

		$criteria->expr()->eq('customer',$customer);
		$criteria->andWhere($criteria->expr()->andX(
			$criteria->expr()->lt('saleTime',$end),
			$criteria->expr()->gt('saleTime',$start)
		));

		return $repo->matching($criteria);

	}

	public function getInvoiceTransactions($sales)
	{

		$repo = $this->silex->db->getRepository('e:ForeupSalesPayments');

		$criteria = new \Doctrine\Common\Collections\Criteria();

		$criteria->where($criteria->expr()->in('sale',$sales->toArray()));
		$criteria->andWhere($criteria->expr()->orX(
			$criteria->expr()->eq('type','invoice_account'),
			$criteria->expr()->andX(
				$criteria->expr()->eq('type','receivable'),
				$criteria->expr()->eq('paymentType','InvoiceBalanceAR')
			)
		));

		return $repo->matching($criteria);
	}

	public function getCustomerTransactions($sales)
	{

		$repo = $this->silex->db->getRepository('e:ForeupSalesPayments');

		$criteria = new \Doctrine\Common\Collections\Criteria();

		$criteria->where($criteria->expr()->in('sale',$sales->toArray()));
		$criteria->andWhere($criteria->expr()->eq('type','customer_account'));

		return $repo->matching($criteria);

	}

	public function getMemberTransactions($sales)
	{

		$repo = $this->silex->db->getRepository('e:ForeupSalesPayments');

		$criteria = new \Doctrine\Common\Collections\Criteria();

		$criteria->where($criteria->expr()->in('sale',$sales->toArray()));
		$criteria->andWhere($criteria->expr()->eq('type','member_account'));

		return $repo->matching($criteria);

	}

	public function getInvoiceCharges($sales)
	{

		$repo = $this->silex->db->getRepository('e:ForeupInvoices');

		$criteria = new \Doctrine\Common\Collections\Criteria();

		$criteria->where($criteria->expr()->in('sale',$sales->toArray()));

		return $repo->matching($criteria);

	}

	public function getCourseInfo($course_id)
	{

		$rep = $this->silex->db->getRepository('e:ForeupCourses');
		$course = $rep->find($course_id);
		$course_transformer = new course_transformer();
		return $course_transformer->transform($course);

	}

	public function getCustomerInfo(ForeupCustomers $customer)
	{

		$customer_transformer = new customer_transformer();
		$customer_info = $customer_transformer->transform($customer);

		return $customer_info;

	}
}

?>