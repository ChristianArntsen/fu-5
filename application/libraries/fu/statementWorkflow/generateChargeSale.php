<?php
namespace fu\statementWorkflow;

use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\models\entities\ForeupItems;
use foreup\rest\models\entities\ForeupRepeatable;
use fu\statementWorkflow\classes\AccountCharger;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;
use Recurr\Transformer\Constraint\BeforeConstraint;
use Recurr\Transformer\Constraint\AfterConstraint;

class generateChargeSale
{
	use \fu\statementWorkflow\traits\daemon_trait;
	private $queue;
	private $input_queue_name;

	public $run_cycles = 0;
	public $messages_received = 0;
	public $messages_sent = 0;

	public function __construct(\fu\statementWorkflow\interfaces\queue_interface &$queue)
	{
		$prefix = '';
		$production = true;
		if(ENVIRONMENT !== 'production'){
			$prefix = ENVIRONMENT.'_';
			$production = false;
		}

		$this->queue = $queue;
		$this->input_queue_name = $prefix.'ForeupRepeatableAccountRecurringChargeItemsToCustomers';
		$this->queue->getQueue($this->input_queue_name,$production);

		$this->error_queue_name = $prefix.'generateChargeSaleErrors';
		$this->queue->getQueue($this->error_queue_name,$production);

		$this->silex = new \fu\statementWorkflow\classes\silex_handler();
		$this->charger = new AccountCharger($this->silex);

		$this->repTransformer = new \foreup\rest\resource_transformers\repeatable_transformer();
		$this->cstTransformer =
			new \foreup\rest\resource_transformers\account_recurring_charge_customers_transformer();
		$this->itmTransformer = new \foreup\rest\resource_transformers\items_transformer();
		$this->CI = \get_instance();

        date_default_timezone_set('UTC');
	}

	public function _get_queue()
	{
		return $this->queue;

	}

	public function _set_input_queue(\fu\statementWorkflow\interfaces\queue_interface &$queue)
	{
		$this->queue = &$queue;

	}

	public function run($current_time = null, $test = false)
	{
		$this->run_cycles++;
		// fetch messages from input queue
		$messages = $this->queue->fetchMessages($this->input_queue_name);

		$count = 0;
		foreach($messages as $message) {
			$count++;
			$this->messages_received++;
			// extract message contents
			$msg_array = $this->extractMessage($message);

			$current_time = $msg_array['current_time'];
			$invoice = null;

			// generate invoice object
			try {
				// Prorating will never happen automatically, it only occurs when a customer is attached
                // to a recurring charge and the users specifically states they want to charge their account
                // for a prorated amount. This happens in the bulk edit process for customers.
			    $msg_array['prorate'] = false;
			    $invoice_object = $this->generateInvoiceObject($msg_array, $current_time);

			}
			catch(\Exception $e){
				$error = [];
				$error['message'] = $e->getMessage();
				$error['trace'] = $e->getTraceAsString();
				$result = ['error'=>$error,'message'=>$msg_array];
				$this->handle_error($result);
				return false;

			}

			// save invoice and sale
			try {
				$invoice = $this->charger->saveInvoice($msg_array['courseId'],$msg_array['customer']->getCustomer(),$invoice_object,$test);
				$invoice->setRecurringCharge($msg_array['charge']);
				$this->silex->db->flush($invoice);

				//$result = $this->saveInvoice($invoice_object, $current_time, $test);

			}
			catch(\Exception $e){

				$error = [];
				$error['message'] = $e->getMessage();
				$error['trace'] = $e->getTraceAsString();
				$result = ['error'=>$error,'message'=>$msg_array];
				$this->handle_error($result);
				continue;

			}

			// pass on to next queue, if needed
			if(!isset($invoice)){
				// what went wrong?
				// see if we can recover

				// if it is not recoverable, will need to add it to the dead letter queue

			}else {
				// associate invoice and sale with the item that was charged
				$this->associateInvoiceWithCharge($msg_array['charge'],$invoice->getId(),$test);
				$this->messages_sent++;
				// delete message
				$this->queue->deleteMessage($this->input_queue_name,$message);

			}

			// if we make it, delete the message
			$this->queue->deleteMessage($this->input_queue_name,$message);

		}
		return $count;

	}

	public function associateInvoiceWithCharge(ForeupAccountRecurringCharges $charge,$invoice_id,$test = false)
	{
		$inv = $this->silex->db->getRepository('e:ForeupInvoices');

		if($test) {
			// if this is a test, invoice will not be in the db
			$invoice = new ForeupInvoices();
			$invoice->setRecurringCharge($charge);

		} else {
			$invoice = $inv->find($invoice_id);
			$invoice->setRecurringCharge($charge);
			$this->silex->db->flush();
		}

		return $invoice;
	}

	private function handle_error($result)
	{
		$this->queue->enqueueMessage($this->error_queue_name,\json_encode($result));

	}

	public function extractMessage($message)
	{
		$ret = [];
		if(method_exists($message,'get'))
			$body = $message->get('Body');
		elseif (is_array($message)){
			$body = $message['Body'];
		}
		$intermediate = \json_decode($body,true);

		$rep = $this->silex->db->getRepository(
			'e:ForeupRepeatableAccountRecurringChargeItems'
		);
		$cst = $this->silex->db->getRepository(
			'e:ForeupAccountRecurringChargeCustomers'
		);
		$ret['repeatable'] = $rep->find($intermediate['repeatable']['id']);
		$ret['item'] = $ret['repeatable']->getItem();
		$ret['charge'] = $ret['item']->getRecurringCharge();
		$ret['courseId'] = $ret['charge']->getOrganizationId();
		$ret['customer'] =  $cst->find($intermediate['customer']['id']);
		$ret['current_time'] = $intermediate['current_time'];

		return $ret;
	}

	public function generateInvoiceObject($msg_array, $current_time, $test = false)
	{
		$name = $msg_array['charge']->getName() . " : " .
			$msg_array['item']->getItem()->getName();
		$item = $msg_array['item']->getItem();
		$customer =  $msg_array['customer']->getCustomer();
        $prorate = $msg_array['prorate'];
        $curDate = new \DateTime($current_time);

        $nextOccurence = $msg_array['repeatable']->getNextOccurence();
        $lastRan = $msg_array['repeatable']->getLastRan();

        $amount = $item->getUnitPrice();
        if(is_numeric($msg_array['item']->getOverridePrice())) {
            $amount = $msg_array['item']->getOverridePrice();
        }

        if($prorate){

            // Prorate the charge amount based on how many days are left till the next charge happens
            if(!empty($nextOccurence) && !empty($lastRan)){
                $amount = $this->getProratedAmount(
                    $amount,
                    $lastRan,
                    $nextOccurence,
                    $curDate
                );

            // If the charge hasn't been run yet, or shouldn't be run again
            // we do not want to generate a prorated invoice
            }else{
                return false;
            }
        }

		$item->setUnitPrice($amount);
		$item->setQuantity($msg_array['item']->getQuantity());

		return $this->charger->generateInvoiceObject($item, $customer, $name, $amount, $curDate, $test);
	}

	public function getProratedAmount($fullAmount, \DateTime $startDate, \DateTime $endDate, \DateTime $curDate){

	    if(empty($fullAmount)) {
            $fullAmount = 0;
        }
        $duration = $startDate->diff($endDate);
        $daysInPeriod = $duration->days;

        $duration = $curDate->diff($endDate);
        $daysToNextCharge = $duration->days;

        if($daysInPeriod == 0 || $daysToNextCharge == 0){
            return 0;
        }
        $modifier = (float) round($daysToNextCharge / $daysInPeriod, 3);
        $proratedAmount = round($fullAmount * $modifier, 2);

        return $proratedAmount;
	}

	public function saveInvoice($invoice_object,$current_time,$test = false)
	{
		// start transaction
		//$this->silex->db->getConnection()->beginTransaction();
		//$this->CI->db->trans_start($test);

		$this->CI->load->model('invoice');

		$invoice_object['date'] = $current_time;

		$result = $this->CI->invoice->save($invoice_object,false,$test);

		if($test) {
			//$this->CI->db->trans_rollback();
			//$this->silex->db->getConnection()->rollback();
		}
		else {
			//$this->CI->db->trans_commit();
			//$this->silex->db->getConnection()->commit();
		}
		// end transaction
		return $result;

	}
}
?>