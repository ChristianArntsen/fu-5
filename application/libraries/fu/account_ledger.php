<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 4/4/2016
 * Time: 1:58 PM
 */

namespace fu;



use foreup\rest\models\entities\ForeupAccountLedger;

class account_ledger
{

    public function __construct($account_id)
    {
        $this->account_id = $account_id;
    }

    public function create($data)
    {
        $client = new \GuzzleHttp\Client();
        $data = ["data"=>$data];
        try {
            $response = $client->post("http://development.foreupsoftware.com/api_rest/index.php/accounts/{$this->account_id}/ledger", [
                'headers' =>[
                    "Content-Type"=>"application/json",
                    "x-authorization"=>"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJkZXZlbG9wbWVudC5mb3JldXBzb2Z0d2FyZS5jb20iLCJhdWQiOiJkZXZlbG9wbWVudC5mb3JldXBzb2Z0d2FyZS5jb20iLCJpYXQiOjE0NTkzODA4NTUsImV4cCI6MTQ2MTk3Mjg1NCwidWlkIjoiMTIiLCJsZXZlbCI6IjMiLCJjaWQiOiI2MjcwIiwiZW1wbG95ZWUiOnRydWV9._ZwvNa_asI6wAQky9vUMQB2bYW2dHc-nYOh8_2T6ndyuTNBZcE5dQc1eqL3KKDWkZ8tJOgkCQilxv2hGOBVB9w"
                ],
                "body"=>json_encode($data)
            ]);
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            echo $e->getResponse()->getBody();
            die;
        }

    }
}