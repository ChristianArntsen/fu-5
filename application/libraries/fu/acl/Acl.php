<?php
namespace fu\acl;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class Acl
{

    private $disabled;
    private $lock;
    private $permissions = [
            "employees"=>[
                "view",
                "edit",
                "list"
            ]
        ];

    public function __construct($person_id = false,$course_id=false)
    {
        if(!$person_id){
            $CI = & get_instance();
            $person_id = $CI->session->userdata('person_id');
        }
        if(!$course_id){
            $CI = & get_instance();
            $course_id = $CI->session->userdata('course_id');
        }

        if($CI->session){
            $this->disabled = empty($CI->session->userdata('use_new_permissions'))?true:!$CI->session->userdata('use_new_permissions');
        }
        $this->lock = Factory::createCurrentUserLock($person_id,$course_id);
    }

    public function allow(Permissions $permission)
    {
        return $this->lock->allow($permission->getActions(),$permission->getResourceName());
    }

    public function deny(Permissions $permission)
    {
        return $this->lock->deny($permission->getActions(),$permission->getResourceName());
    }

    public function can(Permissions $permission)
    {
        if($this->disabled)
            return true;
        return $this->lock->can($permission->getActions(),$permission->getResourceName());
    }

    public function attempt($permission)
    {
        if($this->can($permission))
            return true;

        $this->notAuthorizedResponse($permission);
        die;
    }


    public function roleAllow($role_id,Permissions $permission)
    {
        $this->lock->getManager()->role($role_id)->allow($permission->getActions(),$permission->getResourceName());
    }


    public function roleDeny($role_id,Permissions $permission)
    {
        $this->lock->getManager()->role($role_id)->deny($permission->getActions(),$permission->getResourceName());
    }

    public function getAllPermissions()
    {
        return $this->permissions;
    }

    public function notAuthorizedResponse(Permissions $permission)
    {
        $content = json_encode([
            "success"=>"false",
            "title"=>"You're account doesn't have access to that resource",
            "details"=>"You don't have permission to {$permission->getActionStrings()} this {$permission->getHumanReadableName()}"
        ]);

        $response = new JsonResponse();
        $response->setStatusCode("401");
        $response->setContent($content);

        return $response->send();
    }

}