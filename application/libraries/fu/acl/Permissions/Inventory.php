<?php

namespace fu\acl\Permissions;

class Inventory extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["create","read","update","delete"];
        $this->humanReadableName = "Inventory Item";
        parent::__construct($actions);
    }

}