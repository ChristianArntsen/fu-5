<?php

namespace fu\acl\Permissions\Teetimes;

class Checkedin extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["delete"];
        $this->humanReadableName = "Teetime";
        parent::__construct($actions);
    }

}