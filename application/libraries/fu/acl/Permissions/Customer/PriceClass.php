<?php

namespace fu\acl\Permissions\Customer;

class PriceClass extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["update"];
        $this->humanReadableName = "Price Class";
        parent::__construct($actions);
    }

}