<?php

namespace fu\acl\Permissions\Customer;

class AccountBalance extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["update"];
        $this->humanReadableName = "Account Balance";
        parent::__construct($actions);
    }

}