<?php

namespace fu\acl\Permissions\Customer;

class OnlineCredentials extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["update"];
        $this->humanReadableName = "Customers Online Credentials";
        parent::__construct($actions);
    }

}