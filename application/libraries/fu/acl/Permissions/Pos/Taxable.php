<?php

namespace fu\acl\Permissions\Pos;

class Taxable extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["update"];
        $this->humanReadableName = "Taxable Sale";
        parent::__construct($actions);
    }

}