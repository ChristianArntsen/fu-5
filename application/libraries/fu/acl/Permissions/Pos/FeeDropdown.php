<?php

namespace fu\acl\Permissions\Pos;

class FeeDropdown extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["update"];
        $this->humanReadableName = "Green Fee/Cart Fee Dropdown";
        parent::__construct($actions);
    }

}