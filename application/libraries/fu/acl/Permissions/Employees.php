<?php

namespace fu\acl\Permissions;

class Employees extends \fu\acl\Permissions
{
    public function __construct(Array $actions)
    {
        $this->allowedActions = ["create","read","update","delete"];
        $this->humanReadableName = "Checked-in Tee Time";
        parent::__construct($actions);
    }

}