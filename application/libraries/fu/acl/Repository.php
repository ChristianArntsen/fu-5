<?php

namespace fu\acl;


class Repository
{
    private $user;
    public function __construct($user)
    {
        $this->user = $user;
        $this->CI = & get_instance();
    }
    public function getAllRoles($course_id)
    {
        $result = $this->CI->db->from("acl_roles")
            ->or_where("course_id",$course_id)
            ->or_where("is_standard_role",1)
            ->get();
        return $result;
    }
    public function getAllAllowed($action,$resource_type)
    {
        $allowed_ids = [];
        $query = $this->CI->db->from("acl_caller_permissions")
            ->select("resource_id")
            ->where("caller_id",$this->user->getCallerId())
            ->where("resource_type",$resource_type)
            ->where("type","privilege")
            ->where("action",$action)
            ->get();
        foreach($query->result() as $row)
        {
            $allowed_ids[] = $row->resource_id; // add each user id to the array
        }
        $roles = $this->user->getCallerRoles();
        if(count($roles)>0){

            $query = $this->CI->db->from("acl_role_permission")
                ->select("resource_id")
                ->where_in("role_id",implode(",",$roles))
                ->where("resource_type",$resource_type)
                ->where("type","privilege")
                ->where("action",$action)
                ->get();
            foreach($query->result() as $row)
            {
                $allowed_ids[] = $row->resource_id; // add each user id to the array
            }
        }
        return $allowed_ids;
    }
}