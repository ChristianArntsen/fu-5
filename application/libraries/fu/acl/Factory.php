<?php

namespace fu\acl;


class Factory
{
    public static function createCurrentUserLock($person_id = false,$course_id=false)
    {
        if(!$person_id){
            $CI = & get_instance();
            $person_id = $CI->session->userdata('person_id');
        }
        if(!$course_id){
            $CI = & get_instance();
            $course_id = $CI->session->userdata('course_id');
        }
        $manager =new \BeatSwitch\Lock\Manager(new \fu\acl\CIDriver());


        $lock = $manager->caller(new User($person_id));


        $repo = new Repository($lock->getCaller());
        $roles = $repo->getAllRoles($course_id);
        foreach($roles->result() as $role){
            if(!empty($role->inherited_role)){
                $manager->setRole($role->role_id,$role->inherited_role);
            }
        }
        return $lock;
    }
}