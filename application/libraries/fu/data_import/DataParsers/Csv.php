<?php
namespace fu\data_import\DataParsers;
use League\Csv\Reader;
use Aws\S3\S3Client;

class Csv extends DataParser {

    private $source;
    private $first_row_headers = false;
    private $csv;
    private $column_map = [];

    public function __construct($source, $settings = []){

        $this->source = $source;

        if(!empty($settings['column_map'])){
            $this->column_map = $settings['column_map'];
        }

        if(isset($settings['first_row_headers'])){
            $this->first_row_headers = $settings['first_row_headers'];
        }

        $this->CI =& get_instance();

        // If the source is an actual file
        if($source instanceof \SplFileObject){
            $this->csv = Reader::createFromFileObject($source);

        // Otherwise we assume its a path to an S3 object
        }else if(is_string($source)) {
            $s3Config = $this->CI->config->item('s3');

            // Instantiate the S3 client
            $s3 = new S3Client([
                'region' => 'us-west-2',
                'credentials' => [
                    'key' => $s3Config["access_key"],
                    'secret' => $s3Config["secret"],
                ],
	            'version' => '2006-03-01'
            ]);

            $path = explode(':', $source, 2);
            $file_data = $s3->getObject([
                'Bucket' => $path[0],
                'Key' => $path[1]
            ]);

            $csv_data = $file_data['Body'];
            $this->csv = Reader::createFromString($csv_data);
        }
    }

    public function get_total_records(){
        $total = $this->csv->each(function($row){
            return true;
        });

        if($this->first_row_headers){
            $total--;
        }

        return (int) $total;
    }

    public function each($callback){

        $self =& $this;
        $transform = function($row, $row_offset) use ($self, $callback){

            // Skip first row if they are column names
            if($self->first_row_headers && $row_offset == 0){
                return true;
            }
            $data = $self->transform_row($row);

            return call_user_func($callback, $data, $row_offset);
        };

        return $this->csv->each($transform);
    }

    // Take data from CSV row and places it in properly named
    // associative array to prepare for inserting record
    public function transform_row($row){

        if(empty($this->column_map)){
            return false;
        }

        $transformed = [];
        foreach($this->column_map as $foreup_column => $csv_col_number){

            if(!isset($row[$csv_col_number])) {
                continue;
            }
            $transformed[$foreup_column] = $row[$csv_col_number];
        }

        return $transformed;
    }
}
