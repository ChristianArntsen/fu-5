<?php
namespace fu\data_import\RecordInserters;

abstract class RecordInserter {

    public $record_hashes = [];

    public abstract function insert($record_index, $record_data = []);
    public abstract function get_current_record();

    public function is_duplicate($record_index, $record_data = []){

        $hash = md5(serialize($record_data));
        if(isset($this->record_hashes[$hash])){
            return $this->record_hashes[$hash];
        }
        $this->record_hashes[$hash] = $record_index;

        return false;
    }
}