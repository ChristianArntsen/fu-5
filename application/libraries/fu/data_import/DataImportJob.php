<?php
namespace fu\data_import;

use Aws\S3\S3Client;
use Aws\CloudFront\Exception\Exception;
use Carbon\Carbon;
use League\Csv;

class DataImportJob {

    private $id;
    private $course_id;
    private $source_path;
    private $source_type = 'csv';
    private $response = [];
    private $settings = [];
    private $started_at = false;
    private $completed_at = false;
    private $total_duration = 0; // Duration the job took in seconds
    private $status = 'pending';
    private $type;
    private $percent_complete = 0;
    private $total_records = 0;
    private $records_imported = 0;
    private $records_failed = 0;
    private $failed_csv_url = false;

    private $imported_record_keys = [];
    private $errors = [];
    private $failed_records = [];

    private $inserter;
    private $parser;
    private $progress_updater = false;

    public function __construct($import_job){

        if (!ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }

        if(!empty($import_job['id'])) $this->id = $import_job['id'];
        if(!empty($import_job['course_id'])) $this->course_id = $import_job['course_id'];
        if(!empty($import_job['source_path'])) $this->source_path = $import_job['source_path'];
        if(!empty($import_job['type'])) $this->type = $import_job['type'];
        if(!empty($import_job['percent_complete'])) $this->percent_complete = (int) $import_job['percent_complete'];
        if(!empty($import_job['total_records'])) $this->total_records = (int) $import_job['total_records'];
        if(!empty($import_job['records_imported'])) $this->total_records = (int) $import_job['records_imported'];
        if(!empty($import_job['records_failed'])) $this->records_failed = (int) $import_job['records_failed'];
        if(!empty($import_job['status'])) $this->status = $import_job['type'];
        if(!empty($import_job['settings'])) $this->settings = $import_job['settings'];

        $this->initialize();
    }

    private function initialize(){

        $inserter = false;
        switch($this->type){
            case 'customers':
                $inserter = new RecordInserters\Customer();
                break;
        }
        $this->set_inserter($inserter);

        $parser = false;
        switch($this->source_type){
            case 'csv':
                $parser = new DataParsers\Csv($this->source_path, $this->settings);
                break;
        }
        $this->set_parser($parser);
    }

    private function calculate_percent_complete(){
        if(($this->records_imported == 0 && $this->records_failed == 0) || $this->total_records == 0) return false;
        $percentage = floor((($this->records_imported + $this->records_failed) / $this->total_records) * 100);
        $this->set_percent_complete($percentage);
    }

    private function set_percent_complete($percentage = 0){
        $this->percent_complete = (int) $percentage;
    }

    public function is_importing(){
        return $this->status == 'in-progress';
    }

    public function is_completed(){
        return $this->status == 'completed';
    }

    public function is_cancelled(){
        return $this->status == 'cancelled';
    }

    public function set_inserter(RecordInserters\RecordInserter $inserter){
        $this->inserter = $inserter;
    }

    public function is_done(){
        return $this->status == 'cancelled' || $this->status == 'completed';
    }

    public function set_parser(DataParsers\DataParser $parser){
        $this->parser = $parser;
    }

    public function update_status($status = 'pending'){

        switch($status){
            case 'in-progress':
                $this->started_at = Carbon::now();
            break;
            case 'completed':
            case 'cancelled':
                $this->completed_at = Carbon::now();
                $this->total_duration = (int) $this->started_at->diffInSeconds($this->completed_at);
            break;
        }

        $this->status = $status;
        return true;
    }

    public function import(){

        $this->total_records = $this->parser->get_total_records();
        $this->records_imported = 0;
        $this->records_failed = 0;
        $import_job = $this;

        $this->update_status('in-progress');

        $callback = function($record_data, $row_num) use ($import_job){
            $import_job->insert_record($row_num, $record_data);

            // If job has been cancelled, returning FALSE stops processing records
            return !$import_job->is_cancelled();
        };
        $this->parser->each($callback);

        $status = 'completed';
        if($this->status == 'cancelled'){
            $status = 'cancelled';
        }
        $this->update_status($status);
        $this->update_progress();
    }

    public function set_progress_callback($update_callback){
        $this->progress_updater = $update_callback;
    }

    private function update_progress(){

        if(!$this->progress_updater){
            return false;
        }
        $data = [
            'records_imported' => $this->records_imported,
            'records_failed' => $this->records_failed,
            'percent_complete' => $this->percent_complete,
            'status' => $this->status,
            'total_records' => $this->total_records,
            'total_duration' => $this->total_duration,
            'started_at' => null,
            'completed_at' => null
        ];

        if($this->started_at){
            $data['started_at'] = $this->started_at->toDateTimeString();
        }
        if($this->completed_at){
            $data['completed_at'] = $this->completed_at->toDateTimeString();
        }

        call_user_func($this->progress_updater, $data, $this);

        return true;
    }

    private function insert_record($row_number, $record_data = []){

        $record_data['course_id'] = $this->course_id;
        $record_id = $this->inserter->insert($row_number, $record_data);

        // Insert SUCCESS
        if($record_id){
            $this->records_imported++;
            $this->imported_record_keys[] = (int) $record_id;

        // Insert FAILED
        }else{
            $this->records_failed++;
            unset($record_data['course_id']);
            $this->failed_records[$row_number] = $record_data;
            if($this->inserter->get_error()){
                $this->errors[(int) $row_number] = $this->inserter->get_error();
            }
        }

        $this->calculate_percent_complete();
        $this->update_progress();

        return true;
    }

    public function to_array(){
        return [
            'records_imported' => $this->records_imported,
            'records_failed' => $this->records_failed,
            'percent_complete' => $this->percent_complete,
            'status' => $this->status,
            'total_records' => $this->total_records,
            'total_duration' => $this->total_duration,
            'response' => [
                'imported_record_keys' => $this->imported_record_keys,
                'errors' => $this->errors,
                'failed_csv_url' => $this->failed_csv_url
            ]
        ];
    }

    public function build_failed_csv(){

        if(empty($this->failed_records)){
            return false;
        }
        $csv = Csv\Writer::createFromFileObject(new \SplTempFileObject());

        // Insert column names as first row
        if(!empty($this->settings['column_map'])){
            $headers = array_merge(['Original Row Number'], array_keys($this->settings['column_map']));
            $headers = array_map(function($column){
                return ucfirst(str_replace('_', ' ', $column));
            }, $headers);
            $csv->insertOne($headers);
        }

        foreach($this->failed_records as $record_number => $data){
            $csv->insertOne(['row_number' => $record_number] + $data);
        }

        return $csv;
    }

    // Creates a new downloadable CSV of the records that failed to insert into the database
    public function save_failed_csv(){

        $this->failed_csv_url = false;
        $csv = $this->build_failed_csv();
        if(!$csv){
            return false;
        }

        $this->CI =& get_instance();
        $s3Config = $this->CI->config->item('s3');

        // Instantiate the S3 client
        $s3 = new S3Client([
            'region'  => 'us-west-2',
            'credentials' => [
                'key'    => $s3Config["access_key"],
                'secret' => $s3Config["secret"],
            ],
	        'version' => '2006-03-01'
        ]);

        $path = explode(':', $this->source_path, 2);
        $path[1] = str_replace('.csv', '.failed.csv', $path[1]);

        // Upload CSV to S3
        try {
            $result = $s3->putObject(array(
                'Bucket' => $path[0],
                'Key'    => $path[1],
                'Body'   => $csv->__toString(),
                'ACL'    => 'public-read'
            ));
            $this->failed_csv_url = $result['ObjectURL'];

        }catch(Exception $e){
            print_r($e);
        }
    }
}