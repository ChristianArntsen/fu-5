<?php

namespace fu\new_relic;


class event_collection extends \ArrayObject implements \JsonSerializable
{
    public function add(Event $event)
    {
        $this->append($event);
        return $this;
    }
    public function jsonSerialize()
    {
        return $this->getArrayCopy();
    }
}