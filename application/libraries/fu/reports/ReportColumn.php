<?php
namespace fu\reports;


class ReportColumn {

    private $column;
	private $direction;

    public function __construct($column){
        $this->column = $column;
        $this->direction = "asc";
    }

	/**
	 * @return mixed
	 */
	public function getDirection()
	{
		return $this->direction;
	}

	/**
	 * @param mixed $direction
	 */
	public function setDirection($direction)
	{
		$this->direction = $direction;
	}

    /**
     * @return mixed
     */
    public function getColumn($baseTable = null)
    {
        if($alias = $this->getAlias()){
            $alias = "AS ".$alias;
        }
        if($this->getAggregate() && !$this->getRelation()){
            list($aggregate,$columnName) = $this->getAggregate();
            if(isset($baseTable)){
                $columnName = $baseTable.".".$columnName;
            }

            return $aggregate."(".$columnName.") ".$alias;
        }  else if(!$this->getRelation()){
            $columnName = $this->column;
            if(isset($baseTable)){
                $columnName = $baseTable.".".$columnName;
            }
            return trim($columnName." ".$alias);
        } else {
            return $this->column;
        }
    }
    public function getTableName($baseTable = null)
    {
        $relation = $this->getRelation();
        if(!$relation){
            return $baseTable;
        } else {
            return $relation;
        }
    }
    public function getColumnName($baseTable = null)
    {
        $columnName = $this->getColumn($baseTable);
        if(preg_match("/(\w*)\.(\w*)/",$columnName,$matches)) {
            return $matches[2];
        }
        return $columnName;
    }

    public function getAlias()
    {
        if(preg_match("/AS\s*((\w|\d)+)/",$this->column,$matches)){
            return $matches[1];
        }
        return false;
    }
    public function isAggregate()
    {
        $columnName = $this->column;
        $columnName = strtolower($columnName);
        $aggregates = new Aggregations();
        $possibleAggregates = $aggregates->getAllPossibleAggregations();
        $possibleAggregates = implode("|",$possibleAggregates);
        if(preg_match("/($possibleAggregates)\(((\w|\.)+)\)/",$columnName,$matches)) {
            return true;
        }

        return false;
    }
    public function hasRelation()
    {
        if(preg_match("/(\w*)\.(\w*)/",$this->column,$matches)) {
            return true;
        }
        return false;
    }

    public function getAggregate()
    {
        $columnName = $this->column;

        $aggregates = new Aggregations();
        $possibleAggregates = $aggregates->getAllPossibleAggregations();
        $possibleAggregates = implode("|",$possibleAggregates);
        if(preg_match("/($possibleAggregates)\(((\w|\.)+)\)/",$columnName,$matches)) {
            $aggregate = $matches[1];
            $columnName = $matches[2];
            return [$aggregate,$columnName];
        }
        return false;
    }

    public function getJoin()
    {
        return $this->getRelation();
    }

    private function getRelation()
    {
        if(preg_match("/(\w*)\.(\w*)/",$this->column,$matches)) {
            return $matches[1];
        }
        return false;
    }
	public function toString($baseTable=null)
	{
		$columnName = $this->getColumn($baseTable);
		return $columnName;
	}

	public function toStringWLabel($baseTable)
	{
		$columnName = $this->getColumn($baseTable);

        //$original = mysqli::escape_string(trim($this->getColumn()));
        $original = trim($this->getColumn());
		return $columnName . " AS \"".$original."\"";
	}


}