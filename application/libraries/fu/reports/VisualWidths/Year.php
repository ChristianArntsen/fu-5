<?php
namespace fu\reports\VisualWidths;

use Carbon\Carbon;
use fu\Exceptions\InvalidArgument;

class Year extends \fu\reports\VisualChartGrouping
{
    public function getStartOfWidth()
    {
        $start = new \Carbon\Carbon();
        return $start->startOfYear();
    }
    public function getEndOfWidth()
    {
        $start = new \Carbon\Carbon();
        return $start->endOfYear();
    }
    public function getRequiredGrouping()
    {
        if($this->grouping == "week"){
            $grouping = [$this->createGroupingColumn("week")];
        } else if($this->grouping == "quarter"){
            $grouping = [$this->createGroupingColumn("quarter")];
        } else if($this->grouping == "month"){
            $grouping = [$this->createGroupingColumn("month")];
        } else {
            throw new InvalidArgument("Invalid grouping/width pair.");
        }
        return $grouping;
    }

}