<?php

namespace fu\reports;


class MovingRangeFactory{
	public static function create($range)
	{
		if($range == "yesterday"){
			return new \fu\reports\MovingRanges\Yesterday();
		} if($range == "lastweek"){
			return new \fu\reports\MovingRanges\LastWeek();
		}  if($range == "lasttwoweeks"){
			return new \fu\reports\MovingRanges\LastTwoWeeks();
		}  if($range == "lastmonth"){
			return new \fu\reports\MovingRanges\LastMonth();
		}  if($range == "lastquarter"){
			return new \fu\reports\MovingRanges\LastQuarter();
		}  if($range == "lastyear"){
			return new \fu\reports\MovingRanges\LastYear();
		} else if($range == "today"){
            return new \fu\reports\MovingRanges\Today();
        } else if($range == "tomorrow"){
			return new \fu\reports\MovingRanges\Tomorrow();
		} else if($range == "thismonth"){
            return new \fu\reports\MovingRanges\ThisMonth();
        } else if($range == "thisweek"){
			return new \fu\reports\MovingRanges\ThisWeek();
		} else {
			throw new InvalidArgument("Unknown moving range: ".$range);
		}
	}
}