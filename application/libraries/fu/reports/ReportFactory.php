<?php

namespace fu\reports;

class ReportFactory {

    public static function create($report_type,$input)
    {
        if($report_type == "sales"){
            return new ReportTypes\SalesReport($input);
        } else if($report_type == "register_logs"){
            return new ReportTypes\RegisterLogsReports($input);
        } else if($report_type == "receivings"){
            return new ReportTypes\ReceivingsReport($input);
        } else if($report_type == "items"){
            return new ReportTypes\ItemsReport($input);
        } else if($report_type == "courses"){
            return new ReportTypes\CoursesReport($input);
        }  else if($report_type == "invoices"){
            return new ReportTypes\InvoicesReport($input);
        }  else if($report_type == "raincheck"){
            return new ReportTypes\RaincheckReport($input);
        }  else if($report_type == "customers"){
            return new ReportTypes\CustomersReport($input);
        }  else if($report_type == "teetime"){
            return new ReportTypes\TeetimeReport($input);
        }  else if($report_type == "weather"){
            return new ReportTypes\WeatherReport($input);
        }   else if($report_type == "bartered_teetime"){
            return new ReportTypes\BarteredTeetimeReport($input);
        }   else if($report_type == "reservations"){
            return new ReportTypes\ReservationReport($input);
        }   else if($report_type == "employees"){
            return new ReportTypes\EmployeesReport($input);
        } else if($report_type == "teesheet"){
            return new ReportTypes\TeesheetReport($input);
        } else if($report_type == "sale_transaction"){
	        return new ReportTypes\SaleTransactionReport($input);
        } else if($report_type == "shared_teetimes"){
	        return new ReportTypes\SharedTeetimesReport($input);
        } else if($report_type == "shared_sales"){
	        return new ReportTypes\SharedSalesReport($input);
        }  else if($report_type == "loyalty"){
	        return new ReportTypes\Loyalty($input);
        }   else if($report_type == "giftcard"){
	        return new ReportTypes\GiftcardReport($input);
        } else {
            throw new \Exception("Invalid Report Type");
        }

    }

}