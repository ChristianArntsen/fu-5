<?php

namespace fu\reports;

use fu\Exceptions\InvalidArgument;

class ReportChartsParameters extends ReportParameters {

    private $reportParameters;
    private $category, $range,$visual_width,$visual_grouping, $split_by, $filters, $date_range,$chartGrouping;

    public function __construct()
    {

    }


    /**
     * @return mixed
     */
    public function createReportParameters($report)
    {
        $this->loadFromReport($report);
        return $this->prepareRequiredChartFields();
    }


    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }
    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }
    /**
     * @return mixed
     */
    public function getRange()
    {
        return $this->range;
    }
    /**
     * @param mixed $range
     */
    public function setRange($range)
    {
        $this->range = $range;
    }
    /**
     * @return mixed
     */
    public function getSplitBy()
    {
        return $this->split_by;
    }
    /**
     * @param mixed $split_by
     */
    public function setSplitBy($split_by)
    {
        $this->split_by = $split_by;
    }

    /**
     * @return ParamDate
     */
    public function getDateRange()
    {
        return $this->date_range;
    }
    /**
     * @param mixed $date_range
     */
    public function setDateRange($date_range)
    {
        $this->date_range = new ParamDate($date_range);
    }


    /**
     * @return mixed
     */
    public function getVisualGrouping()
    {
        return $this->visual_grouping;
    }

    /**
     * @param mixed $visual_grouping
     */
    public function setVisualGrouping($visual_grouping)
    {
        $this->visual_grouping = $visual_grouping;
    }

    /**
     * @return mixed
     */
    public function getVisualWidth()
    {
        return $this->visual_width;
    }

    /**
     * @param mixed $visual_width
     */
    public function setVisualWidth($visual_width)
    {
        $this->visual_width = $visual_width;
    }

    public function getChartGrouping()
    {
        return $this->chartGrouping;
    }

    /**
     * @return $this
     * @throws InvalidArgument
     */
    public function prepareRequiredChartFields()
    {
        $select = [];
        $groupBy = [];

        if ($this->getCategory() == "date") {
            $this->chartGrouping = VisualChartGrouping::createVisualWidthObject($this->visual_width);
            $this->chartGrouping->setGrouping($this->visual_grouping);
            $this->chartGrouping->setWidth($this->visual_width);
            $this->chartGrouping->setDateColumn($this->getDateRange()->getColumn());
            if ($this->getDateRange()->getColumn()) {
                $select[] = $this->getDateRange()->getColumn();
            }
            $requiredGrouping = $this->chartGrouping->getRequiredGrouping();
            if ($requiredGrouping && !empty($requiredGrouping[0])) {
                $groupBy = $requiredGrouping;
            }
            if ($this->getDateRange()->getColumn()) {
                $this->setOrderBy([$this->getDateRange()->getColumn()]);
            }
        } else {
            $select[] = $this->getCategory();
            $groupBy[] = $this->getCategory();
        }
        if ($this->getSplitBy())
            $select[] = $this->getSplitBy();
        $select[] = $this->getRange();
        $this->setColumns($select);


        $this->setDateFromObject($this->getDateRange());

        if (isset($this->split_by)) {
            $groupBy[] = $this->split_by;
        }
        $this->setGroupBy($groupBy);
        $this->setActualGroupBy($groupBy);


        return $this;
    }
}