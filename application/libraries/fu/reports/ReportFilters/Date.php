<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 3/19/2015
 * Time: 12:25 PM
 */

namespace fu\reports\ReportFilters;


use Carbon\Carbon;

class Date extends Filter {
    protected $startValue,$endValue;
    public function __construct($filter,$custom_values = null){
        if(isset($filter['start_value'])){
            $this->startValue = Carbon::parse($filter['start_value'])->toDateTimeString();
        }
        if(isset($filter['end_value'])){
            $this->endValue = Carbon::parse($filter['end_value'])->toDateTimeString();
        }
        parent::__construct($filter,$custom_values);
    }

    public function getFilterObjects()
    {
        $startFilter = clone $this;
        $endFilter = clone $this;
        $startFilter->value = $this->startValue;
        $startFilter->operator = ">=";
        $endFilter->value =  isset($this->endValue)? $this->endValue : $this->startValue;

        $endOfDay = new \Carbon\Carbon($endFilter->value);
        $endFilter->value =  $endOfDay->endOfDay()->toDateTimeString();

        $endFilter->operator = "<";
        return [$startFilter,$endFilter];
    }

}