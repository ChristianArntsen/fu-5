<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 2/23/2016
 * Time: 3:57 PM
 */

namespace fu\reports\ReportFilters;


class CustomValues
{
    private $values;
    public function __construct($values){
        $this->values = $values;
    }

    public function getValue($key){
        if(isset($this->values[$key])){
            return $this->values[$key];
        }
        return "";
    }
}