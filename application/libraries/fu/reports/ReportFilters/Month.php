<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 3/19/2015
 * Time: 12:25 PM
 */

namespace fu\reports\ReportFilters;



class Month extends Date {


    public function __construct($filter,$custom_values = null){
        parent::__construct($filter,$custom_values);


    }

    public function getFilterObjects()
    {
        $filter = clone $this;
        $date = new \Carbon\Carbon($this->startValue);
        $filter->value = $date->month;
        $filter->operator = "=";
        return [$filter];
    }

}