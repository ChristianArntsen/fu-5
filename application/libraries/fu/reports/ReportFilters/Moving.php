<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 3/19/2015
 * Time: 12:25 PM
 */

namespace fu\reports\ReportFilters;


class Moving extends Filter {
    private $movingDate;
    public function __construct($filter,$custom_values = null){
        $this->movingDate = new \fu\reports\ParamDate($filter);
        $this->movingDate->setType("moving");
        $this->movingDate->setMovingRange($filter['movingRange']);
        $this->movingDate->setTypeOfMovingRange($filter['typeOfMovingRange']);
        $this->movingDate->setRangeFromDynamicType();

        parent::__construct($filter,$custom_values);
    }

    public function getFilterObjects()
    {
        $startFilter = clone $this;
        $endFilter = clone $this;
        $startFilter->value = $this->movingDate->getStart();
        $startFilter->operator = ">=";
        $endFilter->value = $this->movingDate->getEnd();
        $endFilter->operator = "<";
        return [$startFilter,$endFilter];
    }

}