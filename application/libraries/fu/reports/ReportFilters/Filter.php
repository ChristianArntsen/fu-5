<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 3/19/2015
 * Time: 12:25 PM
 */

namespace fu\reports\ReportFilters;


use fu\reports;
use Silex\Tests\Provider\ValidatorServiceProviderTest\Constraint\Custom;

class Filter {

    protected $column,$operator,$value,$bool,$hasSubJoin,$value_type,$custom_values;

    protected $baseTable = null;

    public function __construct($filter,$custom_values = null){
        if(isset($filter['column'])){
            $this->column = $filter['column'];
        }
        if(isset($filter['operator'])){
            $this->operator = $filter['operator'];
        }
        if(isset($filter['value'])){
            $this->value = $filter['value'];
        }
        if(isset($filter['boolean'])){
            $this->bool = $filter['boolean'];
        }
        if(isset($filter['value_type'])){
            $this->value_type = $filter['value_type'];
        }
        if(isset($custom_values)){
            $this->custom_values = new CustomValues($custom_values);
        }
    }

    /**
     * @return mixed
     */
    public function getBool()
    {
        return $this->bool;
    }

    public function setColumn($column)
    {
        $this->column = $column;
    }
    public function getOriginalColumnObject($baseTable)
    {
	    return new \fu\reports\ReportColumn($this->column);
    }

    /**
     * @return mixed
     */
    public function getColumn($baseTable = null,$includeAlias = true,$safe = false)
    {
        if($this->isAggregate()){
            $alias = $this->getSubJoin()['alias'];
            if($safe){
                $columnName = $this->getSubJoin()['newName'];
            } else {
                $columnName = $this->getSubJoin()['columnName'];
            }
            if($includeAlias)
                return $alias.".`{$columnName}`";
            else
                return $columnName;
        }  else if(!$this->getRelation()){

            $safeColumn = preg_replace("/\(/","_",$this->column);
            $safeColumn = preg_replace("/\)/","",$safeColumn);
            if(isset($baseTable)){
                $safeColumn = $baseTable.".".$safeColumn;
            }
            return $safeColumn;
        } else {
            return $this->column;
        }
    }

    public function getReportColumnObject($baseTable = null)
    {
        return new \fu\reports\ReportColumn($this->getColumn($baseTable,false));
    }



    /**
     * @return mixed
     */
    public function getOperator()
    {
        if($this->value_type == "is_null"){
            return "IS";
        } else if($this->value_type == "is_not_null"){
            return "IS NOT";
        } else if($this->value_type == "not_equal"){
            return "!=";
        }
        return $this->operator;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        if($this->value_type == "is_null"){
            return "NULL";
        } else if($this->value_type == "is_not_null"){
            return "NULL";
        } else if($this->value_type == "terminal"){
            return $this->custom_values->getValue('terminal_id');
        } else if($this->value_type == "user"){
            return $this->custom_values->getValue('person_id');
        }
        return $this->value;
    }

    private function isAggregate()
    {
        $columnName = $this->column;

        $aggregates = new reports\Aggregations();
        $possibleAggregates = $aggregates->getAllPossibleAggregations();
        $possibleAggregates = implode("|",$possibleAggregates);
        if(preg_match("/($possibleAggregates)\(((\w|\.)+)\)/",$columnName,$matches)) {
            $aggregate = $matches[1];
            $columnName = $matches[2];
            return [$aggregate,$columnName];
        }
        return false;
    }

    public function getJoin()
    {
        return $this->getRelation();
    }

    public function getSubJoin()
    {
        list($aggregate,$columnName )= $this->isAggregate();
        if($aggregate){
            $newName = preg_replace("/(\.|\(|\))/",'_',$aggregate."_".$columnName);
            if(!isset($this->alias)){
                $this->alias = substr($columnName,0,1);
            }
            $newSubJoin = [
                "statement"=>new \fu\reports\ReportSelectStatement($this->column),
                "aggregate" => $aggregate,
                "columnName" => $columnName,
                "newName"=> $newName,
                "joinTable"=>$this->getRelation($columnName),
                "alias"=>$this->alias
            ];
            return $newSubJoin;
        }
    }

    private function getRelation()
    {
        if(preg_match("/(\w*)\.(\w*)/",$this->column,$matches)) {
            return $matches[1];
        }
        return false;
    }

    public function toString()
    {
        $value = $this->getValue();
        if($value != "NULL"){
            $value = "'{$this->getValue()}'";
        }
        return $this->getColumn()." ".$this->getOperator()." ".$value;
    }
    public function whereParameters()
    {
        return [$this->getColumn($this->baseTable,true,true),$this->getOperator(),$this->getValue()];
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    public function getFilterObjects()
    {
        return [$this];
    }

	/**
	 * @param \PHP_CodeCoverage_Filter $value
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}

	public function convertFilterToWhereStatement($db)
	{
		foreach($this->getFilterObjects() as $filterObject){

			if($filterObject->getBool() == "AND"){
				if(!is_array($filterObject->getValue()) && strtolower($filterObject->getValue()) == "null"){
					$db->where($filterObject->getColumn($this->baseTable,true,true)." ".$filterObject->getOperator()." ". $filterObject->getValue());
				} else {
					if(is_array($filterObject->getValue())){
						if($filterObject->getOperator() == "!="){
							$db->where_not_in($filterObject->getColumn($this->baseTable,true,true),$filterObject->getValue());
						} else {
							$db->where_in($filterObject->getColumn($this->baseTable,true,true),$filterObject->getValue());
						}
					} else {
						$db->where($filterObject->getColumn($this->baseTable,true,true)." ".$filterObject->getOperator(),$filterObject->getValue());
					}
				}
			} else {
				$db->or_where($filterObject->getColumn($this->baseTable,true,true)." ".$filterObject->getOperator(),$filterObject->getValue());
			}
		}

		return true;
	}

}