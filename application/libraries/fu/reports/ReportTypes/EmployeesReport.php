<?php
/*
 * Not finished
 */
namespace fu\reports\ReportTypes;
class EmployeesReport extends Report{
    protected $baseTable = "foreup_employees" ;
	protected $prefix = "foreup";
    protected $customerColumn = "foreup_employees.person_id";

    public function __construct($reportParameters){
        $this->defaultFilters = [
            "deleted" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"deleted",
                    "operator"=>"=",
                    "value"=>"0",
                    "boolean"=>"AND",
                ])
        ];
        parent::__construct($reportParameters);
    }
    protected function knownJoins(){
        return [
            "people"=>[
                "table"=>"people as people",
                "where"=> $this->baseTable.".person_id = people.person_id",
                "type"=>"LEFT"
            ],
            "timeclock"=>[
                "table"=>"timeclock_entries as timeclock",
                "where"=> $this->baseTable.".person_id = timeclock.employee_id",
                "type"=>"LEFT"
            ],
	        "role"=>[
		        "table"=>"acl_roles as role",
		        "where"=> "timeclock.role_id = role.role_id",
		        "type"=>"LEFT",
		        "requires"=>"timeclock"
	        ],
	        "role_details"=>[
		        "table"=>"acl_roles_employees as role_details",
		        "where"=> $this->baseTable.".person_id = role_details.employee_id AND role.role_id = role_details.role_id ",
		        "type"=>"LEFT",
		        "requires"=>"role"
	        ],
            "courses"=>[
                "table"=>"courses as courses",
                "where"=>$this->baseTable.".course_id = courses.course_id",
                "type"=>"LEFT"
            ],
            "terminals"=>[
                "table"=>"terminals as terminals",
                "where"=>"timeclock_entries.terminal_id = terminals.terminal_id",
                "type"=>"LEFT",
                "requires"=>"timeclock"
            ],
	        "tip_transactions"=>[
		        "table"=>"tip_transactions as tip_transactions",
		        "where"=>"tip_journal.transaction_id = tip_transactions.transaction_id",
		        "type"=>"LEFT",
		        "requires"=>"tip_journal"
	        ],
	        "tip_journal"=>[
		        "table"=>"tip_journal as tip_journal",
		        "where"=>"tip_journal.employee_id = ".$this->baseTable.".person_id",
		        "type"=>"LEFT"
	        ],
        ];
    }


    protected $knownColumns = [
        "foreup_employees"=>[
	        "username",
	        "activated",
	        "deleted",
	        "last_login",
	        "position",
	        "course_id",
	        "person_id"
        ],
	        "courses"=>[
		        "name"
	        ],
	        "people"=>[
		        "first_name",
		        "last_name",
		        "phone_number",
		        "cell_phone_number",
		        "email",
		        "birthday",
		        "address_1",
		        "city",
		        "state",
		        "zip",
		        "country",
		        "comments",
		        "person_id",
	        ],
	        "terminals"=>[
		        "label"
	        ],
	        "role"=>[
		        "role_name"
	        ],
	        "role_details"=>[
		        "hourly_rate"
	        ],
	        "timeclock"=>[
	            "shift_start",
	            "shift_end",
	            "total_hours",
	            "hourly_rate",
	            "total_cost"
            ],
	        "tip_journal"=>[
	        	"transaction_id",
		        "account_id",
		        "employee_id",
		        "d_c",
		        "amount",
		        "notes",
		        "comment",
		        "deleted"
	        ],
	        "tip_transactions"=>[
	        	"reference_id",
		        "employee_id",
		        "transaction_time",
		        "notes",
		        "commet",
		        "deleted"
	        ]
    ];

    protected function addSecurity()
    {
        $course_model = new \Course();
        $course_ids = array();
        $course_model->get_linked_course_ids($course_ids, 'shared_customers');
        $this->db->where_in("foreup_employees.course_id",$course_ids);
    }

}