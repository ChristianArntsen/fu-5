<?php
namespace fu\reports\ReportTypes;

class WeatherReport extends Report{
    protected $baseTable = "foreup_weather" ;
	protected $prefix = "foreup";

    public function __construct($reportParameters){

        $this->CI = & get_instance();
        $this->defaultFilters = [
            "course_id" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"course_id",
                    "operator"=>"=",
                    "value"=>$this->CI->session->userdata('course_id'),
                    "boolean"=>"AND",
                ])
        ];
        parent::__construct($reportParameters);
    }

    protected function knownJoins(){
        return [
        ];
    }


    protected $knownColumns = [
            "foreup_weather"=>[
                "weather_entry_id",
                "course_id",
                "date_collected",
                "day",
                "date",
                "summary",
                "icon",
                "precipIntensity",
                "precipProbability",
                "temperature",
                "dewPoint",
                "humidity",
                "windSpeed",
                "windBearing",
                "pressure",
            ]
    ];


    protected function addSecurity()
    {
    }

}