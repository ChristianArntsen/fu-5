<?php
namespace fu\reports\ReportTypes;

class BarteredTeetimeReport extends Report{
    protected $baseTable = "foreup_teetimes_bartered" ;
    protected $prefix = "foreup";
    protected function knownJoins(){
        return [
            "teetime"=>[
                "table"=>"teetime as teetime",
                "where"=> "teetimes_bartered.teetime_id = teetime.TTID",
                "type"=>"LEFT"
            ],
            "payment"=>[
                "table"=>"sales_payments_credit_cards as payment",
                "where"=> "teetimes_bartered.invoice_id = payment.invoice",
                "type"=>"LEFT"
            ],
            "teesheet"=>[
                "table"=>"teesheet as teesheet",
                "where"=> "teetimes_bartered.teesheet_id = teesheet.teesheet_id",
                "type"=>"LEFT"
            ],
            "courses"=>[
                "table"=>"courses as courses",
                "where"=> "courses.course_id = teesheet.course_id",
                "type"=>"LEFT",
                "requires"=>"teesheet"
            ],
            "billing"=>[
                "table"=>"billing as billing",
                "where"=> "billing.teesheet_id = teetimes_bartered.teesheet_id AND billing.deleted = 1",
                "type"=>"LEFT",
                "requires"=>"teesheet"
            ],
            "customers"=>[
                "table"=>"people as customers",
                "where"=> "teetime.person_id = customers.person_id",
                "type"=>"LEFT",
                "requires"=>"teetime"
            ],
            "api_source"=>[
                "table"=>"api_keys as api_source",
                "where"=> "teetimes_bartered.api_id = api_source.id",
                "type"=>"LEFT"
            ]
        ];
    }


    protected $knownColumns = [
        "foreup_teetimes_bartered"=>[
            "teesheet_id",
            "start",
            "end",
            "player_count",
            "holes",
            "carts",
            "date_booked",
            "booker_id",
            "invoice_id"
        ],
        "api_source"=>[
            "name",
            "level"
        ],
        "payment"=>[
            "auth_amount",
            "amount",
            "masked_account",
            "cardholder_name",
            "trans_post_time",
        ],
        "teetime"=>[
            "type",
            "start",
            "start_datetime",
            "end",
            "end_datetime",
            "allDay",
            "title",
            "details",
            "status",
            "booking_source",
            "reround",
        ],
        "teesheet"=>[
            "title",
            "holes"
        ],
        "courses"=>[
            "course_id",
            "active_course",
            "demo",
            "maintenance_model",
            "active",
            "name",
            "address",
            "city",
            "state",
            "postal",
            "zip",
            "country",
            "foreup_discount_percent"
        ],
        "billing"=>[
            "per_day",
            "teetimes",
            "teetimes_daily"
        ],
        "customers"=>[
            "first_name",
            "last_name",
            "phone_number",
            "cell_phone_number",
            "email",
            "birthday",
            "address_1",
            "city",
            "state",
            "zip",
            "country",
            "comments",
            "person_id",
        ],
    ];



}