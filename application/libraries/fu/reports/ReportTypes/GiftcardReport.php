<?php
namespace fu\reports\ReportTypes;
class GiftcardReport extends Report{

	public function __construct($reportParameters)
	{
		parent::__construct($reportParameters);

		$course_model = new \Course();
		$course_ids = array();
		$course_model->get_linked_course_ids($course_ids, 'shared_customers');


		$this->defaultFilters = [
			"course_ids" =>
				new \fu\reports\ReportFilters\Filter([
					"column"=>"course_id",
					"operator"=>"=",
					"value"=>$course_ids,
					"boolean"=>"AND",
				])
		];
	}

	protected $baseTable = "foreup_giftcards" ;
	protected $prefix = "foreup";
    protected function knownJoins(){
        return [
	        "giftcard_transactions"=>[
		        "table"=>"giftcard_transactions as giftcard_transactions",
		        "where"=>$this->baseTable.".giftcard_id = giftcard_transactions.trans_giftcard",
		        "type"=>"LEFT"
	        ],
	        "customers"=>[
		        "table"=>"people as customers",
		        "where"=>$this->baseTable.".customer_id = customers.person_id",
		        "type"=>"LEFT"
	        ]
        ];
    }


    protected $knownColumns = [
    	"foreup_giftcards"=>[
    		"giftcard_id",
		    "course_id",
		    "giftcard_number",
		    "value",
		    "customer_id",
		    "details",
		    "date_issued",
		    "expiration_date",
		    "department",
		    "category",
		    "deleted"
	    ],
        "giftcard_transactions"=>[
            "trans_id",
            "course_id",
            "trans_giftcard",
            "trans_customer",
            "trans_user",
            "trans_date",
            "trans_comment",
            "trans_description",
            "trans_amount"
        ],
	    "customers"=>[
		    "first_name",
		    "last_name",
		    "phone_number",
		    "cell_phone_number",
		    "email",
		    "birthday",
		    "address_1",
		    "city",
		    "state",
		    "zip",
		    "country",
		    "comments",
		    "person_id",
	    ]
    ];



}