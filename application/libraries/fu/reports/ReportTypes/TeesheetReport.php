<?php
namespace fu\reports\ReportTypes;

class TeesheetReport extends Report{

    protected $baseTable = "foreup_teetime" ;
    protected $prefix = "foreup";
    public function __construct($reportParameters){

        parent::__construct($reportParameters);

        $course_model = new \Course();
        $course_ids = array();
        $course_model->get_linked_course_ids($course_ids, 'shared_tee_sheet');

        /*
         *
AND foreup_teetime.`status` != "deleted"
AND reround = 0
         */
        $this->defaultFilters = [
            "course_ids" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"teesheet.course_id",
                    "operator"=>"=",
                    "value"=>$course_ids,
                    "boolean"=>"AND",
                ]),
            "deleted" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"status",
                    "operator"=>"!=",
                    "value"=>"deleted",
                    "boolean"=>"AND",
                ]),
            "reround" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"reround",
                    "operator"=>"=",
                    "value"=>0,
                    "boolean"=>"AND",
                ])
        ];

    }

    protected function knownJoins(){
        return [
            "teesheet"=>[
                "table"=>"teesheet as teesheet",
                "where"=> $this->baseTable.".teesheet_id = teesheet.teesheet_id",
                "type"=>"LEFT JOIN"
            ]
        ];
    }

    protected $knownColumns = [
        "foreup_teetime"=>[
            "type",
            "start",
            "start_datetime",
            "end",
            "end_datetime",
            "allDay",
            "title",
            "details",
            "status",
            "booking_source",
            "reround",
            "player_count",
            "paid_player_count"
        ],
        "teesheet"=>[
            "title",
            "course_id"
        ],
    ];


}