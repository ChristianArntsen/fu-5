<?php
namespace fu\reports\ReportTypes;

use \fu\reports\TemporaryTables\SalesTable;
class CoursesReport extends Report{
    protected $baseTable = "foreup_courses" ;
	protected $prefix = "foreup";
    protected function knownJoins(){
        return [
            "information"=>[
                "table"=>"courses_information as information",
                "where"=> $this->baseTable.".course_id = information.course_id",
                "type"=>"LEFT"
            ],
            "sales_employees"=>[
                "table"=>"people as sales_employees",
                "where"=>"information.sales_rep = sales_employees.person_id",
                "type"=>"LEFT",
                "requires"=>"information"
            ],
            "information_setup"=>[
                "table"=>"courses_information_setup as information_setup",
                "where"=> $this->baseTable.".course_id = information_setup.course_id",
                "type"=>"LEFT"
            ],
            "setup_employees"=>[
                "table"=>"people as setup_employees",
                "where"=>"information_setup.setup_rep = setup_employees.person_id",
                "type"=>"LEFT",
                "requires"=>"information_setup"
            ],
            "information_website"=>[
                "table"=>"courses_information_website as information_website",
                "where"=> $this->baseTable.".course_id = information_website.course_id",
                "type"=>"LEFT"
            ],
            "website_employees"=>[
                "table"=>"people as website_employees",
                "where"=>"information_website.website_rep = website_employees.person_id",
                "type"=>"LEFT",
                "requires"=>"information_website"
            ],
            "customers"=>[
                "table"=>"customers as customers",
                "where"=>$this->baseTable.".course_id = customers.course_id",
                "type"=>"LEFT"
            ],
            "sales"=>[
                "table"=>"sales as sales",
                "where"=>$this->baseTable.".course_id= sales.course_id",
                "type"=>"LEFT"
            ],
            "teetime"=>[
                "table"=>"teetime as teetime",
                "where"=>"teetime.TTID = sales.teetime_id",
                "type"=>"left",
                "requires"=>"sales",
            ],

            "teesheet"=>[
                "table"=>"teesheet as teesheet",
                "where"=>  $this->baseTable.".course_id = teesheet.course_id",
                "type"=>"LEFT"
            ],
            "teesheet_teetime"=>[
                "table"=>"teetime as teesheet_teetime",
                "where"=>"teesheet_teetime.teesheet_id = teesheet.teesheet_id",
                "type"=>"left",
                "requires"=>"teesheet",
            ],
            "teesheet_teetime_bartered"=>[
                "table"=>"teetimes_bartered as teesheet_teetime_bartered",
                "where"=>  "teesheet_teetime.TTID = teesheet_teetime_bartered.teetime_id",
                "type"=>"LEFT",
                "requires"=>"teesheet_teetime"
            ],

            "teesheet_teetime_bartered_payments"=>[
                "table"=>"sales_payments_credit_cards as teesheet_teetime_bartered_payments",
                "where"=> "teesheet_teetime_bartered.invoice_id = teesheet_teetime_bartered_payments.invoice",
                "type"=>"LEFT",
                "requires"=>"teesheet_teetime_bartered"
            ],
            "teetimes_bartered"=>[
                "table"=>"teetimes_bartered as teetimes_bartered",
                "where"=>  "teesheet.teesheet_id = teetimes_bartered.teesheet_id",
                "type"=>"LEFT",
                "requires"=>"teesheet"
            ],
            "bartered_teetime_information"=>[
                "table"=>"teetime as bartered_teetime_information",
                "where"=> "teetimes_bartered.teetime_id = bartered_teetime_information.TTID",
                "type"=>"LEFT",
                "requires"=>"teetimes_bartered"
            ],
            "bartered_payments"=>[
                "table"=>"sales_payments_credit_cards as bartered_payments",
                "where"=> "teetimes_bartered.invoice_id = bartered_payments.invoice",
                "type"=>"LEFT",
                "requires"=>"teetimes_bartered"
            ],
            "teesheet_billing"=>[
                "table"=>"billing as teesheet_billing",
                "where"=> "teesheet_billing.teesheet_id = teesheet.teesheet_id AND teesheet_billing.deleted = 0",
                "type"=>"LEFT",
                "requires"=>"teesheet"
            ],
        ];
    }


    protected $knownColumns = [
        "foreup_courses"=>[
                "course_id",
                "active_course",
                "demo",
                "active",
                "name",
                "address",
                "city",
                "state",
                "postal",
                "zip",
	            "website",
                "country",
                "area_code",
                "phone",
                "holes",
                "type",
                "config",
                "courses",
                "customers",
                "dashboards",
                "employees",
                "events",
                "food_and_beverage",
                "giftcards",
                "invoices",
                "item_kits",
                "items",
                "marketing_campaigns",
                "promotions",
                "quickbooks",
                "receivings",
                "recipients",
                "reports",
                "reservations",
                "sales",
                "schedules",
                "suppliers",
                "teesheets",
                "tournaments",
                "food_and_beverage_v2",
                "sales_v2",
                "latitude_centroid",
                "longitude_centroid",
            ],
            "information"=>[
                "stage",
                "course_type",
                "mobile_app",
                "sales_rep",
                "course_value",
                "sphere_influence",
                "apr",
                "date_sold",
                "date_contract_ends",
                "region",
                "date_cancelled"
            ],
            "information_setup"=>[
                "setup_rep",
                "date_golive_goal",
                "date_imports_completed",
                "date_training_completed",
                "date_hardward_setup",
                "date_live",
            ],
            "information_website"=>[
                "website_rep",
                "stage",
                "date_golive_goal",
                "date_started",
                "date_sent_for_review",
                "date_live",
                "date_sold",
	            "username",
	            "password",
	            "development_url"
            ],
            "website_employees"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
            "sales_employees"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
            "setup_employees"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
            "employees"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
            "customers"=>[
                "first_name",
                "last_name",
                "phone_number",
                "cell_phone_number",
                "email",
                "birthday",
                "address_1",
                "city",
                "state",
                "zip",
                "country",
                "comments",
                "person_id",
            ],
            "sales"=>[
                "teetime_id",
                "sale_number",
                "sale_time",
                "customer_id",
                "comment",
            ],
            "teetime"=>[
                "start_datetime",
                "type",
                "status",
                "reround",
                "start",
                "end",
                "allDay",
                "title",
                "details",
                "player_count",
                "booking_source",
            ],
            "teesheet_teetime"=>[
                "type",
                "status",
                "reround",
                "start_datetime",
                "start",
                "end",
                "allDay",
                "title",
                "details",
                "player_count",
                "booking_source",
            ],
            "teetimes_bartered"=>[
                "teesheet_id",
                "start",
                "end",
                "player_count",
                "holes",
                "carts",
                "date_booked",
                "booker_id",
                "invoice_id"
            ],
            "teesheet"=>[
                "title",
                "holes",
                "teesheet_id"
            ],
            "teetimes_bartered"=>[
                "teesheet_id",
                "start",
                "end",
                "player_count",
                "holes",
                "carts",
                "date_booked",
                "booker_id",
                "invoice_id"
            ],
            "bartered_teetime_information"=>[
                "type",
                "start",
                "start_datetime",
                "end",
                "end_datetime",
                "allDay",
                "title",
                "details",
                "status",
                "booking_source",
                "reround",
            ],
            "bartered_payments"=>[
                "auth_amount",
                "amount",
                "masked_account",
                "cardholder_name",
                "trans_post_time",
            ],
            "teesheet_teetime_bartered_payments"=>[
                "auth_amount",
                "amount",
                "masked_account",
                "cardholder_name",
                "trans_post_time",
            ],
            "teesheet_billing"=>[
                "teetimes",
                "teetimes_daily"
            ]
    ];



}