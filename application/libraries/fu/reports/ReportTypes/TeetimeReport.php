<?php
namespace fu\reports\ReportTypes;

class TeetimeReport extends SalesReport{
    public function __construct($reportParameters){

        parent::__construct($reportParameters);
        $this->defaultFilters = [
            "teetime.deleted"=>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"teetime.status",
                    "operator"=>"!=",
                    "value"=>"deleted",
                    "boolean"=>"AND",
                ]),
            "teetime.reround"=>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"teetime.reround",
                    "operator"=>"=",
                    "value"=>"0",
                    "boolean"=>"AND",
                ]),
            "sales_items.type"=>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"sales_items.type",
                    "operator"=>"=",
                    "value"=>"green_fee",
                    "boolean"=>"AND",
                ])
        ];
    }

}