<?php

namespace fu\reports\ReportTypes;

use Carbon\Carbon;
use fu\Exceptions\InvalidArgument;
use fu\reports\ReportParameters;
use fu\reports\ReportSelectStatement;
use fu\reports\ReportColumn;

abstract class Report {

    /**
     * @var ReportParameters
     */
    protected $reportParameters;
    /**
     * @var
     */
    protected $baseTable;

    protected $customerColumn;

    /**
     * @var
     */
    protected $knownColumns;
    protected $joinedAlready;
    protected $defaultFilters;
    protected $sharedReporting = false;


    abstract protected function knownJoins();
    /**
     * @var \CI_Controller
     */
    protected $CI;

	protected $testRun;




    public function __construct($reportParameters)
    {

	    $this->CI = & get_instance();
	    $this->db = $this->CI->load->getDatabaseConnection('default');
	    $this->CI->load->model("Report");
	    $this->CI->load->model("Reports_columns");
	    $this->reportParameters = (object)$reportParameters;
        $this->db->reset_query_builder();
        $this->createBaseTable();
    }

    protected function createBaseTable(){}

    private function generateDBObject()
    {
        $this->joinedAlready = [];
        $this->db->from($this->baseTable);
        $this->generateJoins();
        $this->generateSubJoins();
        $this->generateSelects();
        $this->generateWhere();
        $this->generateHaving();
        $this->generateGroupBy();
        $this->generateOrder();
    }
    public function getQueryObject()
    {
        $this->generateDBObject();
        if(isset($this->testRun) && $this->testRun){
            return $this->db->compileSelect();
        }
        return $this->db->get();
    }
    public function getData()
    {
        $this->generateDBObject();
	    if(isset($this->testRun) && $this->testRun){
			return $this->db->compileSelect();
        }
        $result = $this->db->get();
        if($result->num_rows() > 0){
            $result = $result->result_array();
        } else {
            $result = [];
        }

        $this->query = $this->db->last_query();
        return $result;
    }
    public function getSummary()
    {
        $this->db->from($this->baseTable);
        $this->generateSummaryJoins();
        $this->generateSummarySelects();
        $this->generateWhere();
        $result = $this->db->get()->result_array();
        $this->query = $this->db->last_query();
        return $result;
    }
    public function getPeople(){}


    public function getPossibleColumns()
    {
        $columns = [];
        $columns[$this->baseTable] = $this->db->list_fields($this->baseTable);
        foreach($this->knownJoins() as $table => $join){
            $columns[$table] = $this->db->list_fields($table);
        }
        return $columns;
    }

    private function addDBJoin($joinName){
        $parentJoin = isset($this->knownJoins()[$joinName]['requires'])?$this->knownJoins()[$joinName]['requires']:null;
        if(isset($parentJoin)){
            $this->addDBJoin($parentJoin);
        }

        if($this->isKnownJoin($joinName)){
            if($this->knownJoins()[$joinName]['type'] == "self")
                return;
            if(isset($this->joinedAlready[$joinName])){
                return;
            }
            $this->joinedAlready[$joinName] = true;
            $this->db->join(
                $this->knownJoins()[$joinName]['table'],
                $this->knownJoins()[$joinName]['where'],
                $this->knownJoins()[$joinName]['type']);
        } else {
            throw new InvalidArgument("Unknown join in query ({$joinName}). ");
        }
    }
    private function generateJoins()
    {
        foreach($this->reportParameters->getJoins() as $joinName => $join){
            $this->addDBJoin($joinName);
        }
    }

    private function generateJoinQuery($joinName)
    {
        $joinString = "";
        //"LEFT JOIN [] ON []"
        $parentJoin = isset($this->knownJoins()[$joinName]['requires'])?$this->knownJoins()[$joinName]['requires']:null;
        if(isset($parentJoin)){
            $joinString = $this->generateJoinQuery($parentJoin);
        }

        if($this->isKnownJoin($joinName)){
            if($this->knownJoins()[$joinName]['type'] == "self")
                return;
            $joinObj = $this->knownJoins()[$joinName];
            return $joinString . " {$joinObj['type']} JOIN {$this->db->dbprefix($joinObj['table'])} ON {$joinObj['where']}";
        } else {
            throw new InvalidArgument("Unknown join in query.");
        }
    }
    private function generateSubJoins()
    {
        foreach($this->reportParameters->getSubJoins() as $subJoin){
            if(!isset($this->reportParameters->getGroupBy()[0])){
                throw new InvalidArgument("Unable to filter on an aggregate without a grouping.");
            }
            $group_column = $this->getGroupingRequiredForSubjoin();
            $group_column_alias = trim($group_column->getColumn());
            $group_column_alias = preg_replace("/[\(\)\.]/","_",$group_column_alias);

            $joinQuery = "";
            if($subJoin['joinTable'] !== false){
                $joinQuery = $this->generateJoinQuery($subJoin['joinTable']);
            }
            $subJoin['statement']->setOriginal($subJoin['newName']);
            $sql = "(SELECT {$subJoin['statement']->toString($this->baseTable)} , {$group_column->getColumn($this->baseTable)} AS '{$group_column_alias}' FROM ";
            $sql .= "{$this->baseTable} {$joinQuery} GROUP BY  {$group_column->getColumnName()} )";
            $this->db->join($sql." ".$subJoin['alias'],"{$subJoin['alias']}.{$group_column_alias} = {$group_column->getColumn($this->baseTable)}");
        }
    }
    private function getGroupingRequiredForSubjoin()
    {
        //Get first grouping, ignore visual grouping.
        return $this->reportParameters->getGroupBy()[0];
    }

    private function generateSelects()
    {
        $selects = [];
        foreach($this->reportParameters->getColumns() as $key => $statement){
            if(!$this->isKnownStatement($statement)){
                throw new InvalidArgument("Unsupported column: ".$statement->toString($this->baseTable));
            }
	        $selects[] = $statement->toString($this->baseTable);
        }
        $this->db->select($selects);
    }

    private function generateHaving()
    {
	    $filters = $this->reportParameters->getPostFilters();
	    foreach($filters as $filter){
		    $columnObj = $filter->getOriginalColumnObject($this->baseTable);
		    if(!$this->isKnownColumn($columnObj)){
			    throw new InvalidArgument("Unsupported column: ".$filter->getReportColumnObject($this->baseTable)->getColumn($this->baseTable));
		    }

		    foreach($filter->getFilterObjects() as $filterObject){
			    $column = $filterObject->getColumn($this->baseTable,true,true);
			    $column = preg_replace("/\`/",'',$column);
			    $this->db->having($column." ".$filterObject->getOperator(),$filterObject->getValue());
		    }
	    }

	    $this->addSecurity();
    }

    private function generateWhere()
    {
        $this->applyDefaultFilters();

        $filters = $this->reportParameters->getFilters();
        foreach($filters as $filter){
        	$columnObj = $filter->getOriginalColumnObject($this->baseTable);
            if(!$this->isKnownColumn($columnObj)){
                throw new InvalidArgument("Unsupported column: ".$filter->getReportColumnObject($this->baseTable)->getColumn($this->baseTable));
            }
            $this->convertFilterToWhereStatement($filter);
        }

        $this->addSecurity();
    }

    //Add default filters only if the new filters aren't overriding them
    protected function applyDefaultFilters()
    {
        if(empty($this->defaultFilters)){
            return true;
        }

        foreach($this->defaultFilters as $key=>$defaultFilter){
            if(!$this->doesFilterExist($defaultFilter->getColumn())){
                $this->reportParameters->addFilter($defaultFilter);
            }
        }

        $this->generateJoins();
    }

    private function doesFilterExist($columnName)
    {
        foreach($this->reportParameters->getFilters() as $filter){
            if($columnName == $filter->getColumn())
                return true;
        }
        return false;
    }
    private function convertFilterToWhereStatement($filter)
    {
        foreach($filter->getFilterObjects() as $filterObject){

            if($filterObject->getBool() == "AND"){
                if(!is_array($filterObject->getValue()) && strtolower($filterObject->getValue()) == "null"){
                    $this->db->where($filterObject->getColumn($this->baseTable,true,true)." ".$filterObject->getOperator()." ". $filterObject->getValue());
                } else {
                    if(is_array($filterObject->getValue())){
                        if($filterObject->getOperator() == "!="){
                            $this->db->where_not_in($filterObject->getColumn($this->baseTable,true,true),$filterObject->getValue());
                        } else {
                            $this->db->where_in($filterObject->getColumn($this->baseTable,true,true),$filterObject->getValue());
                        }
                    } else {
                        $this->db->where($filterObject->getColumn($this->baseTable,true,true)." ".$filterObject->getOperator(),$filterObject->getValue());
                    }
                }
            } else {
                $this->db->or_where($filterObject->getColumn($this->baseTable,true,true)." ".$filterObject->getOperator(),$filterObject->getValue());
            }
        }
    }

    protected function addSecurity()
    {

    }

    private function generateGroupBy(){
        if(!is_array($this->reportParameters->getGroupBy()))
            return;
        $grouping = $this->reportParameters->getGroupBy();
        if($this->reportParameters->getActualGroupBy()){
            $grouping = $this->reportParameters->getActualGroupBy();
        }
        foreach($grouping as $grouping){
            if(!$this->isKnownColumn($grouping)){
                throw new InvalidArgument("Unsupported column: ".$grouping->getColumnName());
            }
            $this->db->group_by($grouping->toString($this->baseTable));
        }
    }

    private function generateOrder(){
        if(!is_array($this->reportParameters->getOrderBy()))
            return;
        foreach($this->reportParameters->getOrderBy() as $order){
            if(!$this->isKnownColumn($order)){
                throw new InvalidArgument("Unsupported column: ".$order->getColumnName());
            }
	        $this->db->_protect_identifiers = FALSE;
	        $statement = new ReportSelectStatement($order->getColumn());
	        $columnName = $statement->getCompiledName($this->baseTable);
            $this->db->order_by($columnName,$order->getDirection());
	        $this->db->_protect_identifiers = TRUE;
        }
    }

    private function isKnownJoin($join)
    {
        return isset($this->knownJoins()[$join]);
    }

    private function isKnownColumn(ReportColumn $column)
    {
        $columnName = $column->getColumnName($this->baseTable);
        $tableName = $column->getTableName($this->baseTable);
		$columnText = $column->getColumn($this->baseTable);
        //It should only equal tablename.column or aggregate(tablename.column)
	    if(!$column->isAggregate() && $columnText != $tableName.".".$columnName ){
		    $reportColumn_model = new \Reports_columns();
		    $result = $reportColumn_model->get_by([
			    "custom_column"=>$column->getColumn()
		    ]);
		    if($result){
			    return true;
		    }

	    	return false;
	    }


		if($column->isAggregate()){
			$aggregate = $column->getAggregate();
			$expected = $aggregate[0]. "(".$tableName.".".$columnName.")";
			if($expected != trim($columnText)){
				return false;
			}
		}


        return isset($this->knownColumns[$tableName]) && array_search($columnName,$this->knownColumns[$tableName]) !== false;
    }
    private function isKnownStatement(ReportSelectStatement $statement)
    {
	    if(count($statement->getColumns()) === 1 && substr_count($statement->getStatement(),"[0]") <= 1 ){
	        if($this->isKnownColumn($statement->getColumns()[0])){
                return true;
            }
        }
        $reportColumn_model = new \Reports_columns();
	    $result = $reportColumn_model->get_by([
		    "custom_column"=>$statement->getOriginal()
	    ]);
        if($result){
            return true;
        }
        return false;

    }

    public function getAllColumns()
    {
        return $this->knownColumns;
	}

	public function setTestRun($testRun)
	{
		$this->testRun = $testRun;
	}

	public function changeDateRange($type)
	{
		$filters = $this->reportParameters->getDateFilters();
		foreach($filters as $filter){
			$value = Carbon::parse($filter->getValue());
			if($type=="last_week"){
				$value->subWeek();
			} else if($type=="last_month"){
				$value->subMonth();
			} else if($type=="last_year"){
				$value->subYear();
			}
			$filter->setValue($value->toDateTimeString());
		}

	}

	public function enableSharedReport()
	{
		$this->sharedReporting = true;
	}
}