<?php
namespace fu\reports\ReportTypes;
class ReservationReport extends Report{
    protected $baseTable = "reservations" ;
	protected $prefix = "foreup";
    public function __construct($reportParameters){

        parent::__construct($reportParameters);
        $this->defaultFilters = [
            "deleted" =>
                new \fu\reports\ReportFilters\Filter([
                    "column"=>"schedule.course_id",
                    "operator"=>"=",
                    "value"=>$this->CI->session->userdata('course_id'),
                    "boolean"=>"AND",
                ])
        ];
    }

    protected function knownJoins(){
        return [
            "tracks"=>[
                "table"=>"tracks as tracks",
                "where"=> $this->baseTable.".track_id= tracks.track_id",
                "type"=>"LEFT JOIN"
            ],
            "schedule"=>[
                "table"=>"schedules as schedule",
                "where"=> "tracks.schedule_id = schedule.schedule_id",
                "type"=>"LEFT JOIN",
                "requires"=>"tracks"
            ],
            "customers"=>[
                "table"=>"people as customers",
                "where"=> $this->baseTable.".person_id = customers.person_id",
                "type"=>"LEFT JOIN"
            ]
        ];
    }
    protected function addSecurity()
    {
        $this->db->where("schedule.course_id",$this->CI->session->userdata('course_id'),false);
    }

    protected $knownColumns = [
        "reservations"=>[
                "reservation_id",
                "track_id",
                "status",
                "start_datetime",
                "end_datetime",
                "player_count",
                "holes",
                "carts",
                "title",
                "phone",
                "email",
                "details",
                "person_name",
                "last_updated",
                "date_booked",
                "date_cancelled",
                "booking_source",
                "caceller_id",
                "teed_off_time",
                "turn_time",
                "finish_time"
            ],
        "customers"=>[
            "first_name",
            "last_name",
            "phone_number",
            "cell_phone_number",
            "email",
            "birthday",
            "address_1",
            "city",
            "state",
            "zip",
            "country",
            "comments",
            "person_id",
        ],
        "tracks"=>[
            "title",
            "trans_id",
            "course_id",
            "trans_items",
            "trans_users",
            "trans_date",
            "trans_comment",
            "trans_inventory",
            "sale_id",
            "suspended_sale_id"
        ],
        "schedule"=>[
            "title",
            "type",
            "online_open_time",
            "online_closetime",
            "course_id"
        ]
    ];



}