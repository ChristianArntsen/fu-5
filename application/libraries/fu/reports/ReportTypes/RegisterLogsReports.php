<?php
namespace fu\reports\ReportTypes;
class RegisterLogsReports extends Report{
    protected $baseTable = "foreup_register_log" ;
	protected $prefix = "foreup";
    protected $customerColumn = "employees.person_id";

    protected function knownJoins(){
        return [
            "employees"=>[
                "table"=>"people as employees",
                "where"=> $this->baseTable.".employee_id = employees.person_id",
                "type"=>"LEFT JOIN"
            ],
            "closing_employees"=>[
                "table"=>"people as closing_employees",
                "where"=> $this->baseTable.".employee_id = closing_employees.person_id",
                "type"=>"LEFT JOIN"
            ],
            "courses"=>[
                "table"=>"courses as courses",
                "where"=>$this->baseTable.".course_id = courses.course_id",
                "type"=>"LEFT"
            ],
	        "terminals"=>[
		        "table"=>"terminals as terminals",
		        "where"=>$this->baseTable.".terminal_id = terminals.terminal_id",
		        "type"=>"LEFT"
	        ]
        ];
    }


    protected $knownColumns = [
        "foreup_register_log"=>[
            "register_log_id",
            "employee_id",
            "course_id",
            "terminal_id",
            "shift_start",
            "shift_end",
            "open_amount",
            "close_amount",
            "cash_sales_amount",
            "persist",
            "close_check_amount",
            "cash_sales_amount",
            "check_sales_amount",
            "closing_employee_id"
        ],
        "employees"=>[
            "first_name",
            "last_name",
            "phone_number",
            "cell_phone_number",
            "email",
            "birthday",
            "address_1",
            "city",
            "state",
            "zip",
            "country",
            "comments",
            "person_id"
        ],
        "closing_employees"=>[
            "first_name",
            "last_name",
            "phone_number",
            "cell_phone_number",
            "email",
            "birthday",
            "address_1",
            "city",
            "state",
            "zip",
            "country",
            "comments",
            "person_id"
        ],
        "courses"=>[
            "course_id",
            "active_course",
            "demo",
            "maintenance_model",
            "active",
            "name",
            "address",
            "city",
            "state",
            "postal",
            "zip",
            "country"
        ],
        "terminals"=>[
            "label"
        ]
    ];

    protected function addSecurity()
    {
        $this->db->where("foreup_register_log.course_id",$this->CI->session->userdata('course_id'));
    }


}