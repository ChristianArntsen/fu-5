<?php

namespace fu\reports;

use Carbon\Carbon;
use fu\Exceptions\InvalidArgument;

class ReportChartsData {
    /**
     * @var ReportChartsParameters
     */
    private $data,$chartParameters;
    public function __construct($chartParameters)
    {
        $this->chartParameters = $chartParameters;
    }

    public function render()
    {
        if(empty($this->data)){
            return [];
        }
        $renderedData = [];
        if($this->chartParameters->getSplitBy()){
            $renderedData[] = array_merge(["Category"],array_keys($this->subCategories));
        } else {
            $renderedData[] = array_merge(["Category"],["Value"]);
        }
        foreach($this->data as $key=>$row){
            $renderedData[] = array_merge([$key],$row);
        }
        return $renderedData;
    }

    public function loadFromDBRows($rows)
    {
        if(empty($rows)){
            $this->data = [];
        }
        $categoryColumn = $this->chartParameters->getCategory();
        $rangeColumn = $this->chartParameters->getRange();
        $subCategoryColumn = $this->chartParameters->getSplitBy();

        $this->subCategories = $this->getUniqueSubcategories($rows);
        if($categoryColumn === "date"){
            if($this->chartParameters->getVisualWidth() == "range"){
                $this->initiateDataFromDateRange(
                    $this->chartParameters->getDateRange()->getStart(),
                    $this->chartParameters->getDateRange()->getEnd(),
                    $this->chartParameters->getVisualGrouping(),
                    count($this->subCategories)
                );
            } else {
                $this->initiateDataFromVisualWidth(
                    $this->chartParameters->getVisualGrouping(),
                    count($this->subCategories)
                );
            }


        }


        foreach($rows as $row){
            if($categoryColumn === "date"){
                $categoryValue = $this->getDateLabel($this->getCategoryValue($row));
            } else {
                $categoryValue = $this->getCategoryValue($row);
            }
            $newSubCategoryIndex = $this->getSubcategoryIndex($row, $subCategoryColumn);
            $this->prefillCategoryData($categoryValue);
            $this->data[$categoryValue][$newSubCategoryIndex] = $row[$rangeColumn];
        }

        return $this->data;
    }

    private function getUniqueSubcategories($rows)
    {
        if(!$this->chartParameters->getSplitBy()){
            return [null];
        }
        $distinctSubCategories = [];
        foreach($rows as $row){
            $subCategory = $row[$this->chartParameters->getSplitBy()];
            if(!isset($distinctSubCategories[$subCategory])){
                $distinctSubCategories[$subCategory] = count($distinctSubCategories);
            }
        }
        return $distinctSubCategories;
    }


    /**
     * @param $dateObject
     */
    private function roundDateByGrouping($dateString)
    {
        $dateObject = new \Carbon\Carbon($dateString);
        if ($this->chartParameters->getVisualGrouping() == "hour") {
            $dateObject->minute(0);
            $dateObject->second(0);
        } else if ($this->chartParameters->getVisualGrouping() == "day" || $this->chartParameters->getVisualGrouping() == "dayofweek") {
            $dateObject->startOfDay();
        } else if ($this->chartParameters->getVisualGrouping() == "week" ) {
            $dateObject->startOfWeek();
        } else if ($this->chartParameters->getVisualGrouping() == "month") {
            $dateObject->startOfDay();
            $dateObject->startOfMonth();
        } else if ($this->chartParameters->getVisualGrouping() == "quarter") {
            $dateObject->startOfDay();
            $dateObject->firstOfQuarter();
        }else if ($this->chartParameters->getVisualGrouping() == "year") {
            $dateObject->startOfDay();
            $dateObject->startOfMonth();
            $dateObject->startOfYear();
        }

        return $dateObject->toDateTimeString();
    }

    private function getDateLabel($dateString)
    {
        $dateObject = new \Carbon\Carbon($dateString);
        if($this->chartParameters->getVisualWidth() != "range"){
            if ($this->chartParameters->getVisualGrouping() == "hour") {
                if($this->chartParameters->getVisualWidth() == "day"){
                    return $dateObject->format("G:00");
                } else if($this->chartParameters->getVisualWidth() == "week"){
                    return $dateObject->format("l G:00");
                }
            } else if ($this->chartParameters->getVisualGrouping() == "day") {
                //Month-Day
                return $dateObject->format("d");
            } else if ($this->chartParameters->getVisualGrouping() == "week") {
                //Month-Day
                return $dateObject->format("W");
            } else if ($this->chartParameters->getVisualGrouping() == "dayofweek") {
                //Month-Day
                return $dateObject->format("l");
            } else if ($this->chartParameters->getVisualGrouping() == "month") {
                //Month-Year
                return $dateObject->format("M");
            } else if ($this->chartParameters->getVisualGrouping() == "quarter") {
                //Quarter-Year
                return "Quarter ". $dateObject->quarter;
            }else if ($this->chartParameters->getVisualGrouping() == "year") {
                //Year
                return $dateObject->format("Y");
            }
        } else {
            if ($this->chartParameters->getVisualGrouping() == "hour") {
                return $dateObject->format("Y M d G:00");
            } else if ($this->chartParameters->getVisualGrouping() == "day") {
                return $dateObject->format("Y M d");
            } else if ($this->chartParameters->getVisualGrouping() == "week") {
                return $dateObject->format("Y M d");
            } else {
                return $dateObject->format("Y M");
            }
        }


        return $dateObject->toDateTimeString();
    }
    /**
     * @param $row
     * @return string
     */
    private function getCategoryValue($row)
    {
        if($this->chartParameters->getCategory() === "date"){
            $categoryValue = $row[$this->chartParameters->getDateRange()->getColumn()];
            $categoryValue = $this->roundDateByGrouping($categoryValue);
        } else {
            $categoryValue = $row[$this->chartParameters->getCategory()];
        }
        return $categoryValue;
    }

    /**
     * @param $categoryValue
     */
    private function prefillCategoryData($categoryValue)
    {
        if (!isset($this->data[$categoryValue])) {
            if (isset($this->subCategories)) {
                $this->data[$categoryValue] = array_fill(0, count($this->subCategories), 0);
            } else {
                $this->data[$categoryValue] = [0];
            }
        }
    }

    /**
     * @param $row
     * @param $subCategoryColumn
     * @return int
     */
    private function getSubcategoryIndex($row, $subCategoryColumn)
    {
        if ($this->chartParameters->getSplitBy()) {
            $subCategoryValue = $row[$subCategoryColumn];
            $newSubCategoryIndex = $this->subCategories[$subCategoryValue];
            return $newSubCategoryIndex;
        } else {
            $newSubCategoryIndex = 0;
            return $newSubCategoryIndex;
        }
    }
    private function initiateDataFromVisualWidth($grouping,$numberOfValues)
    {
        $start = $this->chartParameters->getChartGrouping()->getStartOfWidth();
        $end = $this->chartParameters->getChartGrouping()->getEndOfWidth();

        $currentDate = $start;
        while($currentDate->diffInMinutes($end,false) >= 0){

            $this->data[$this->getDateLabel($currentDate->toDateTimeString())] = array_fill(0,$numberOfValues,0);

            $currentDate = $this->incrementDate($grouping, $currentDate);
        }
    }
    private function initiateDataFromDateRange($start,$end,$grouping,$numberOfValues = 1)
    {
        $start = new \Carbon\Carbon($this->roundDateByGrouping($start));
        $end = new \Carbon\Carbon($this->roundDateByGrouping($end));

        //Start at midnight of start
        //Increment by grouping till end
        $currentDate = $start;
        while($currentDate->diffInMinutes($end,false) >= 0){

            $this->data[$this->getDateLabel($currentDate->toDateTimeString())] = array_fill(0,$numberOfValues,0);

            $currentDate = $this->incrementDate($grouping, $currentDate);
        }
    }
    /**
     * @param $grouping
     * @param \Carbon\Carbon $currentDate
     */
    private function incrementDate($grouping, $currentDate)
    {
        if ($grouping == "hour") {
            $currentDate->addHour();
        } else if ($grouping == "day" || $grouping == "dayofweek") {
            $currentDate->addDay();
        } else if ($grouping == "month") {
            $currentDate->addMonth();
        } else if ($grouping == "week") {
            $currentDate->addWeek();
        } else if ($grouping == "quarter") {
            $currentDate->addMonth(3);
        } else if ($grouping == "year") {
            $currentDate->addYear();
        } else {
            throw new InvalidArgument("Invalid date grouping, $grouping.");
        }

        return $currentDate;
    }

}