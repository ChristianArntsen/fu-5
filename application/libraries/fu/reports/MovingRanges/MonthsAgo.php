<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class MonthsAgo implements MovingRange {
	private $months;
	public function __construct($months)
	{
		$this->months = $months;
	}

	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subMonth($this->months);
		return $date->startOfMonth()->startOfDay()->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subMonth($this->months);
		return $date->endOfMonth()->endOfDay()->toDateTimeString();
	}
}