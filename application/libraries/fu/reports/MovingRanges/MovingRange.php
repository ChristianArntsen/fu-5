<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


interface MovingRange{
	public function getStartTimeStamp();
	public function getEndTimeStamp();
} 