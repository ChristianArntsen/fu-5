<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


use Carbon\Carbon;

class SingleDay implements MovingRange {
	private $days;
	private $date;
	public function __construct($days,$date = null)
	{
		if(isset($date)){
			$this->date = Carbon::parse($date);
		}
		$this->days = $days;
	}

	public function getStartTimeStamp()
	{
		if(isset($this->date)){
			$date = $this->date->copy();
		} else {
			$date = \Carbon\Carbon::now();
		}
		$date->addDay($this->days);
		return $date->startOfDay()->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		if(isset($this->date)){
			$date = $this->date->copy();
		} else {
			$date = \Carbon\Carbon::now();
		}
		$date->addDay($this->days);
		return $date->endOfDay()->toDateTimeString();
	}
}