<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class ThisWeek implements MovingRange
{
	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		return $date->startOfWeek()->subDay()->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		return $date->endOfWeek()->subDay()->toDateTimeString();
	}
} 