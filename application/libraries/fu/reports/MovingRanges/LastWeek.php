<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class LastWeek implements MovingRange
{
	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subWeek(1);
		return $date->startOfWeek()->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subWeek(1);
		return $date->endOfWeek()->toDateTimeString();
	}
}