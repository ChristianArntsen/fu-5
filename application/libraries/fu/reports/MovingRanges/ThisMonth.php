<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class ThisMonth implements MovingRange
{
	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		return $date->startOfMonth()->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		return $date->endOfMonth()->toDateTimeString();
	}
} 