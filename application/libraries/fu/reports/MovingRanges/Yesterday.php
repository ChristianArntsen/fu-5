<?php
/**
 * Created by PhpStorm.
 * User: beebe
 * Date: 4/23/2015
 * Time: 11:20 PM
 */

namespace fu\reports\MovingRanges;


class Yesterday implements MovingRange {

	public function getStartTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subDay(1);
		return $date->startOfDay()->toDateTimeString();
	}

	public function getEndTimeStamp()
	{
		$date = \Carbon\Carbon::now();
		$date->subDay(1);
		return $date->endOfDay()->toDateTimeString();
	}
} 