<?php

namespace fu\reports;

class Aggregations{

    private $possibleAggregations = ['sum','max','min','count','distinct','group_concat'];
    private $possibleDateAggregations = ['hour','day','dayofweek','week','month','year','quarter','dayname','monthname','date'];

    public function getAllPossibleAggregations()
    {
        return array_merge($this->possibleAggregations,$this->possibleDateAggregations);
    }

    public function getDateAggregations()
    {
        return $this->possibleDateAggregations;
    }
}