<?php
namespace fu\reports\Printers;

use Carbon\Carbon;
use fu\aws\s3;
use fu\reports\ReportParameters;
use League\Csv\Writer;

class Csv extends Printer
{



	public function __construct($report,$data,$request_parameters)
	{
		$this->report = $report;
		$this->data = $data;
		$this->request_parameters = $request_parameters;
		$this->meta_data = [];
	}

	public function generate()
	{

	}

	function add_meta_data($data)
	{
		// TODO: Implement add_meta_data() method.
	}

	public function save()
	{

		$snappy = new  \Knp\Snappy\Pdf();
		$snappy->setBinary("application/bin/wkhtmltopdf");


		$report_name = sha1(serialize($this->report).time()).".csv";
		//Save file to s3
		if (!file_exists('reports/')) {
			mkdir('reports/', 0777, true);
		}


		$headers = [];
		$newline = "\n";
		$columns = $this->request_parameters->getColumns();

		$file = new \SplFileObject("reports/$report_name","w");
		$writer = Writer::createFromFileObject($file);

		foreach($columns as $column){
			$headers[] = $this->column_to_label($column->getOriginal());
		}
		$writer->insertOne($headers);

		while($row = $this->data->fetch_object()){
			$writer->insertOne((array)$row);
		}
		$writer = null;
		$s3 = new s3();
		$url = $s3->uploadToReportBucket("reports/$report_name",$report_name);
		return ["destination"=>$url];
	}


}