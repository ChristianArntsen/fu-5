<?php
namespace fu\reports\Printers;

use Carbon\Carbon;
use fu\aws\s3;
use fu\reports\ReportParameters;

class Pdf extends Printer
{



	public function __construct($report,$data,$request_parameters)
	{
		$this->report = $report;
		$this->data = $data;
		$this->request_parameters = $request_parameters;
		$this->meta_data = [];
		$this->data_to_render = [
				"report_title"=>"Sales Report",
				"meta_data"=>[],
				"date_range"=>"",
				"filters"=>[],
				"columns"=>[],
				"data"=>[]
		];

		parent::__constructor();
	}

	public function generate()
	{

		$date_filters = $this->request_parameters->getDateFilters();
		if(isset($date_filters) && isset($date_filters[0])){
			$start_date = Carbon::parse($date_filters[0]->getValue());
		}
		if(isset($date_filters) && isset($date_filters[1])){
			$end_date = Carbon::parse($date_filters[1]->getValue());
		}
		$this->data_to_render['date_range'] = " {$start_date} - {$end_date}";
		$this->data_to_render['report_title'] = $this->report->title;

		foreach($this->request_parameters->getOrigFilter() as $filter)
		{
			$column = $this->column_to_label($filter->getColumn());
			if(is_array($filter->getValue())){
				$values = implode(",",$filter->getValue());
			} else {
				$values = $filter->getValue();
			}
			$text = "{$column} {$filter->getOperator()} {$values}";

			$this->data_to_render["filters"][] = $text;
		}


		$date_columns = [];
		$numbers = [];
		foreach($this->report->columns as $column){
			if($column->type == "date_range"){
				$date_columns[$this->render_complete_name($column)] = "date_range";
			} else if($column->type == "number"){
				$numbers[$this->render_complete_name($column)] = "number";
			}
		}


		$columns = $this->request_parameters->getColumns();
		$headers = [];
		foreach($columns as $column){
			$this->data_to_render['columns'][] = $this->column_to_label($column->getOriginal());
		}

		while((array)$row = $this->data->fetch_object()){
			foreach($row as $key => $value){
				if(isset($date_columns[$key])){
					$row->$key = Carbon::parse($value)->format("F d, Y g:i A");
				} else if(isset($numbers[$key])){
					$row->$key = number_format($value,2);
				}

				$this->add_to_totals($key,$value);
			}

			$this->data_to_render["data"][]=["row"=>array_values((array)$row)];
		}


		$this->data_to_render['totals'] = $this->totals;
	}

	public function save()
	{
		$CI = & get_instance();
		$html = $CI->load->view('reportsv2/HtmlReport',$this->data_to_render,true);

		$snappy = new  \Knp\Snappy\Pdf();
		$snappy->setBinary("application/bin/wkhtmltopdf");


		$report_name = sha1(serialize($this->report).uniqid()).".pdf";
		//Save file to s3
		if (!file_exists('reports/')) {
			mkdir('reports/', 0777, true);
		}


		$snappy->generateFromHtml($html, "reports/$report_name");
		$s3 = new s3();
		$url = $s3->uploadToReportBucket("reports/$report_name",$report_name);
		$CI->load->model("report_exports");
		$report_exports = new \Report_Exports();
		$report_exports->create([
			"name"=>$report_name,
			"human_readable_name"=>$this->data_to_render['report_title']." - ".$this->data_to_render['date_range']
		]);
		return ["destination"=>$url];
	}

	public function add_meta_data($data)
	{
		$this->data_to_render["meta_data"][]=["data"=>$data];
	}


}