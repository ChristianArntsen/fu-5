<?php

namespace fu\reports;

use Carbon\Carbon;
use fu\Exceptions\InvalidArgument;
use fu\reports\ReportFilters\Filter;

class ReportParameters {

    private $columns,$filters,$orderBy,$groupBy,$date,$splitBy,$comparative,$comparativeType;

    private $joins = [];
    private $subJoins = [];
    private $dateFilters;
    private $origFilter = [];
    private $postFilters = [];
    private $custom_values;

    public function __construct()
    {
        $this->CI = & get_instance();
        $this->custom_values = $this->CI->session->all_userdata();

        $this->filters = [];
        $this->dateFilters = [];
        $this->orderBy = [];
        $this->groupBy= [];
        $this->columns = [];
    }

    public function setDateFromObject($date)
    {
        if($date->getColumn() == null){
            return false;
        }

        $filterObj = new ReportFilters\Filter([
            "column"=>$date->getColumn(),
            "operator"=>">=",
            "value"=>Carbon::parse($date->getStart())->toDateTimeString(),
            "boolean"=>"AND"
        ],$this->custom_values);
        $this->addFilter($filterObj);
        $this->dateFilters[] = $filterObj;
        $filterObj = new ReportFilters\Filter([
            "column"=>$date->getColumn(),
            "operator"=>"<=",
            "value"=>Carbon::parse($date->getEnd())->toDateTimeString(),
            "boolean"=>"AND"
        ],$this->custom_values);
        $this->addFilter($filterObj);
        $this->dateFilters[] = $filterObj;


        if($date->getGrouping() != null){
            $grouping = $date->getGrouping()."(".$date->getColumn().")";
            array_unshift($this->groupBy,new ReportColumn($grouping));
        }
    }

    /**
     * @param mixed $orderBy
     */
    public function setDate($date)
    {
        $this->dateFilters = [];
        $date = new ParamDate($date);
        $this->setDateFromObject($date);
        return true;
    }
    /**
     * @return ReportSelectStatement[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Columns need to have
     *
     *
     * @param mixed $columns
     */
    public function setColumns(array $columns)
    {
        $this->columns = [];
        foreach($columns as $column){
            $statement = new ReportSelectStatement($column);
	        $this->columns [] = $statement;
	        foreach($statement->getColumns() as $column){
		        if($column->getJoin() ){
			        $this->joins[$column->getJoin()] = $column->getJoin();
		        }
	        }
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function getJoins()
    {
        $joins = [];
        foreach($this->filters as $filterObj){
            if($filterObj->getJoin()){
                $joins[$filterObj->getJoin()] = 1;
            }
        }
        foreach($this->columns as $statement){
            foreach($statement->getColumns() as $column){
                if($column->getJoin() ){
                    $joins[$column->getJoin()] = $column->getJoin();
                }
            }
        }
        return $joins;
    }

    public function getSubJoins()
    {
        $subJoins = [];
        foreach($this->filters as $filterObj){
            if($filterObj->getSubJoin()){
                $newSubJoin = $filterObj->getSubJoin();
                $filterObj->setAlias($newSubJoin['alias']."".count($subJoins));
                $subJoins[] = $filterObj->getSubJoin();
            }
        }
        return $subJoins;
    }

    /**
     * @param mixed $joins
     */
    public function setJoins($joins)
    {
        $this->joins = $joins;
    }

    /**
     * @return Filter[]
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param mixed $filters
     */
    public function setFilters($filters = [])
    {
        if(!isset($filters)){
            return true;
        }
        foreach($filters as $filter){
            $filter = (array)$filter;



            if(isset($filter['movingRange'])){
                $filterObj = new ReportFilters\Moving((array)$filter,$this->custom_values);
            } else if(isset($filter['type']) && ($filter['type']=="date_range" || $filter['type']=="static")){
                if(isset($filter['aggregate']) && $filter['aggregate'] == "month"){
                    $filterObj = new ReportFilters\Month((array)$filter,$this->custom_values);
                } else {
                    $filterObj = new ReportFilters\Date((array)$filter,$this->custom_values);
                }
            } else if(isset($filter['type']) && $filter['type']=="teetime"){
                $filterObj = new ReportFilters\Teetime((array)$filter,$this->custom_values);
            } else  {

                $filterObj = new ReportFilters\Filter((array)$filter,$this->custom_values);
            }

	        //If Filter is actually a custom value, then we'll want to convert it to a having statement
	        if(isset($filter['post_filter']) && $filter['post_filter'] === true){
		        $this->postFilters[] =$filterObj;
		        return;
	        }

            $this->addFilter($filterObj);
            $this->origFilter[] =$filterObj;
        }
    }

    public function addFilter($filterObj)
    {
        $this->filters[] = $filterObj;
    }

    private function checkForUniqueAlias($newSubJoin)
    {
        foreach($this->subJoins as $subjoin){
            if($newSubJoin['alias'] == $subjoin['alias']){
                return false;
            }
        }

        return true;
    }
    /**
     * @return ReportColumn[]
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param mixed $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $this->orderBy = [];
        foreach($orderBy as $column){
        	if(is_array($column)){
        		if(!isset($column['col']))
			        throw new InvalidArgument("Invalid format for column order");
		        $columnObj = new ReportColumn($column['col']);
		        if(isset($column['dir']))
		            $columnObj->setDirection($column['dir']);
	        } else {
		        $columnObj = new ReportColumn($column);
	        }
            $this->orderBy [] = $columnObj;
        }
        return true;
    }


    /**
     * @return ReportColumn[]
     */
    public function getGroupBy()
    {
        return $this->groupBy;
    }

    /**
     * @param mixed $groupBy
     */
    public function setGroupBy(array $groupBy)
    {
        $this->orderBy = [];
        foreach($groupBy as $column){
            $columnObj = new ReportColumn($column);
            $this->groupBy [] = $columnObj;
        }

        return true;
    }

    /**
     * Sometimes the data is filtered as if it were grouped a specific way
     * but the data needs to be returned grouped another way because of charts/graphics
     *
     * @param mixed $groupBy
     */
    public function setActualGroupBy(array $groupBy)
    {
        $this->actualGroupBy = [];
        foreach($groupBy as $column){
            $columnObj = new ReportColumn($column);
            $this->actualGroupBy [] = $columnObj;
        }

        return true;
    }

    public function getActualGroupBy()
    {
        if(!isset($this->actualGroupBy)){
            return false;
        }
        return $this->actualGroupBy;
    }

    public function loadFromReportSummary($reportTemplate)
    {
        $columnNames = [];
        foreach($reportTemplate->summary_columns as $columnObj){
            if(isset($columnObj->aggregate)){
                $columnNames[] = $columnObj->aggregate."(".$columnObj->column.")";
            } else {
                $columnNames[] = $columnObj->column;

            }
        }
        if(count($columnNames) > 0)$this->setColumns($columnNames);


        if(count($this->groupBy) == 0)$this->setGroupBy($reportTemplate->grouping);
        if(count($this->orderBy) == 0)$this->setOrderBy($reportTemplate->ordering);
        if(count($this->filters) == 0)$this->setFilters($reportTemplate->filters);
        if(count($this->dateFilters) == 0 && isset($reportTemplate->date_range))
            $this->setDate($reportTemplate->date_range);
    }

    public function loadFromReport($reportTemplate)
    {
        $columnNames = [];
        foreach($reportTemplate->columns as $columnObj){
            if(isset($columnObj->aggregate) && !empty($columnObj->aggregate)){
                if(!empty($columnObj->table)){
                    $columnName = $columnObj->aggregate."(".$columnObj->table.".".$columnObj->column.")";
                } else {
                    $columnName = $columnObj->aggregate."(".$columnObj->column.")";
                }
            } else {
                if(!empty($columnObj->table)){
                    $columnName = $columnObj->table.".".$columnObj->column;
                } else {
                    $columnName = $columnObj->column;
                }
            }
            if(!empty($columnName))
                $columnNames[] = $columnName;
        }
        if(count($this->columns) == 0)$this->setColumns($columnNames);
        if(count($this->groupBy) == 0)$this->setGroupBy($reportTemplate->grouping);
        if(count($this->orderBy) == 0)$this->setOrderBy($reportTemplate->ordering);
        //if(count($this->filters) == 0)$this->setFilters($reportTemplate->filters);
        if(isset($this->dateFilters) && count($this->dateFilters) == 0 && isset($reportTemplate->date_range))
            $this->setDate($reportTemplate->date_range);
    }

    public function isValid()
    {

    }


    /**
     * @return Filter[]
     */
    public function getDateFilters()
    {
        return $this->dateFilters;
    }

    /**
     * @param array $dateFilters
     */
    public function setDateFilters($dateFilters)
    {
        $this->dateFilters = $dateFilters;
    }


    /**
     * @return ReportFilters\Filter[]
     */
    public function getOrigFilter()
    {
        return $this->origFilter;
    }

    /**
     * @param array $origFilter
     */
    public function setOrigFilter($origFilter)
    {
        $this->origFilter = $origFilter;
    }

	/**
	 * @return mixed
	 */
	public function getComparative()
	{
		return $this->comparative;
	}

	/**
	 * @param mixed $comparative
	 */
	public function setComparative($comparative)
	{
		$this->comparative = $comparative;
	}

	/**
	 * @return mixed
	 */
	public function getComparativeType()
	{
		return $this->comparativeType;
	}

	/**
	 * @param mixed $comparativeType
	 */
	public function setComparativeType($comparativeType)
	{
		$this->comparativeType = $comparativeType;
	}

	/**
	 * @return array
	 */
	public function getPostFilters()
	{
		return $this->postFilters;
	}

	/**
	 * @param array $postFilters
	 */
	public function setPostFilters($postFilters)
	{
		$this->postFilters = $postFilters;
	}



}