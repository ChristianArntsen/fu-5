<?php
namespace fu\reports\TemporaryTables;

use Carbon\Carbon;

class SalesTable {

    private $tableName;


    public function __construct()
    {
        $this->CI = & get_instance();
        $this->CI->db->reset_query_builder();
	    $this->CI->load->model('Report_tables');

	    $this->cleanseAllOldTables();

    }


    /**
     * @param \fu\reports\ReportFilters\Filter[] $filters
     */
    public function setFilters(array $filters)
    {
        $this->where = [];
        foreach($filters as $filter){
            if($filter->getColumn() == "sale_date"){
                $this->where[] = "sale_time ".$filter->getOperator()." '".$filter->getValue()."'";
            }
        }
        $this->where[] = "s.course_id = ".(int) $this->CI->session->userdata('course_id');
        $tableHash = md5(implode(",",$this->where));
        $this->tableName = "reports_".$tableHash;
        $this->tableName = $this->CI->db->dbprefix($this->tableName);

    }

    public function generateIfNeeded()
    {
        if($this->isTableOld()){
	        $this->CI->Report_tables->delete_by_tablename($this->tableName);

            $this->CI->load->dbforge();
            $this->CI->db->query("DROP TABLE IF EXISTS ".$this->tableName);
            $this->tableExists = false;
        }
        if(!$this->isTableCached()){
			if($this->isTableGenerating()){
				$this->waitForGeneration();
			} else {
				$this->generateTable();
				$this->tableExists = true;
			}
        }
    }

    public function getTableName()
    {
        return $this->tableName;
    }

	public function cleanseAllOldTables()
	{
		$old = $this->CI->Report_tables->get_all_old();
		foreach($old as $table){
			if(preg_match("/foreup_reports_/",$table->table_name)){
				$this->CI->Report_tables->delete_by_tablename($table->table_name);
				$this->CI->db->query("DROP TABLE IF EXISTS ".$table->table_name);
			}
		}
	}

    public function generateTable()
    {
	    $id = $this->CI->Report_tables->save(["table_name"=>$this->tableName,"status"=>"pending"]);
	    $this->CI->db->query($this->getNewTableQuery());
	    $this->CI->Report_tables->update($id,["status"=>"open"]);
    }

    private function getNewTableQuery()
    {
        $this->CI = & get_instance();
        $start_time = time();
        $category = ($this->CI->config->item('separate_courses'))?"IF((price_category != '' AND teesheet != ''), CONCAT_WS(' ', teesheet, IF(i.item_id = 0, 'Invoice Line Items', category)), IF(si.item_id = 0, 'Invoice Line Items', category)) AS category":"IF(si.item_id = 0, 'Invoice Line Items', category) AS category";

        $whereString = "WHERE ".implode(" AND ",$this->where);

        $query = "CREATE TABLE IF NOT EXISTS ".$this->tableName."
			(
				KEY(sale_id),
				KEY(number),
				KEY(deleted),
				KEY(customer_id),
				KEY(employee_id),
				KEY(teetime_id),
				KEY(sale_date)
			)
			COLLATE='utf8_general_ci'
			ENGINE=INNODB AS (
			SELECT
				s.deleted AS deleted,
				sale_time AS sale_date,
				si.serialnumber,
				si.description,
				s.teetime_id,
				terminal_id,
				s.sale_id,
				s.number,
				si.line,
				si.timeframe_id,payment_type,
				customer_id,
				s.employee_id,
				i.item_id,
				NULL as item_kit_id,
				NULL as invoice_id,
				supplier_id,
				si.quantity_purchased,
				item_cost_price,
				item_unit_price,
				item_number,
				i.name as name,
				category AS actual_category,
				IF(si.item_id = 0, 'Invoice Line Items', department) AS department,
				$category,
				IF(price_category != '' AND timeframe_name IS NOT NULL, CONCAT(price_category, ' - ', timeframe_name), IF(price_category != '', CONCAT(price_category, ' - No Timeframe'), subcategory)) AS subcategory,
				CASE
					WHEN category = 'Green Fees' AND si.price_class_id != 0 AND si.price_class_id IS NOT NULL THEN price_class.green_gl_code
					WHEN category = 'Carts' AND si.price_class_id != 0 AND si.price_class_id IS NOT NULL THEN price_class.cart_gl_code
					ELSE i.gl_code
				END AS gl_code,
				discount_percent,
				si.subtotal,
				si.total,
				si.tax,
				si.profit,
				si.total_cost,
				si.receipt_only
			FROM ".$this->CI->db->dbprefix('sales_items')." AS si
			INNER JOIN ".$this->CI->db->dbprefix('sales')." AS s
				ON si.sale_id = s.sale_id
			LEFT JOIN ".$this->CI->db->dbprefix('items')." AS i
				ON si.item_id = i.item_id
			LEFT JOIN ".$this->CI->db->dbprefix('seasonal_timeframes')." AS tf
				ON si.timeframe_id = tf.timeframe_id
            LEFT JOIN ".$this->CI->db->dbprefix('price_classes')." AS price_class
				ON price_class.class_id = si.price_class_id
				AND si.price_class_id != 0
			{$whereString}
			) UNION ALL (
			SELECT
				s.deleted AS deleted,
				sale_time AS sale_date,
				'' AS serialnumber,
				si.description,
				s.teetime_id,
				terminal_id,
				s.sale_id,
				s.number,
				si.line,
				'',payment_type,
				customer_id,
				s.employee_id,
				NULL AS item_id,
				i.item_kit_id,
				NULL AS invoice_id,
				NULL AS supplier_id,
				si.quantity_purchased,
				item_kit_cost_price,
				item_kit_unit_price,
				item_kit_number,
				i.name as name,
				category AS actual_category,
				department, IF((price_category != '' AND teesheet != ''),
				CONCAT_WS(' ', teesheet, category), category) AS category, IF(price_category != '',
				price_category,subcategory) AS subcategory,
				NULL AS gl_code,
				discount_percent,
				si.subtotal,
				si.total,
				si.tax,
				si.profit,
				si.total_cost,
				si.receipt_only
			FROM ".$this->CI->db->dbprefix('sales_item_kits')." AS si
			INNER JOIN ".$this->CI->db->dbprefix('sales')." AS s
				ON si.sale_id = s.sale_id
			LEFT JOIN ".$this->CI->db->dbprefix('item_kits')." AS i
				ON si.item_kit_id = i.item_kit_id
			{$whereString}
			) UNION ALL (
			SELECT
				s.deleted AS deleted,
				sale_time AS sale_date,
				'' AS serialnumber,
				si.description,
				s.teetime_id,
				terminal_id,
				s.sale_id,
				s.number,
				si.line,
				'',payment_type,
				customer_id,
				s.employee_id,
				NULL AS item_id,
				NULL AS item_kit_id,
				i.invoice_id,
				NULL AS supplier_id,
				si.quantity_purchased,
				invoice_cost_price,
				invoice_unit_price,
				NULL AS item_kit_number,
				i.name as name,
				category AS actual_category,
				department, IF((price_category != '' AND teesheet != ''),
				CONCAT_WS(' ', teesheet, category), category) AS category, IF(price_category != '',
				price_category,subcategory) AS subcategory,
				NULL AS gl_code,
				discount_percent,
				si.subtotal,
				si.total,
				si.tax,
				si.profit,
				si.total_cost,
				si.receipt_only
			FROM ".$this->CI->db->dbprefix('sales_invoices')." AS si
			INNER JOIN ".$this->CI->db->dbprefix('sales')." AS s
				ON si.sale_id = s.sale_id
			LEFT JOIN ".$this->CI->db->dbprefix('invoices')." AS i
				ON si.invoice_id = i.invoice_id
			{$whereString}
			)";

        return $query;
    }

    private function isTableOld()
    {
	    $tableIsOld = false;

		$info = $this->CI->Report_tables->get_by_tablename($this->tableName,"open");

	    if(empty($info) || count($info) == 0 ){
			return false;
	    }
		$timestamp = new \Carbon\Carbon($info->timestamp);
		$dbCreation = new \Carbon\Carbon($timestamp);
		if($dbCreation->diffInMinutes(\Carbon\Carbon::now("America/Chicago")) > 1500 ){
			$tableIsOld = true;
		}
        return $tableIsOld;
    }

    private function isTableCached()
    {
        return $this->CI->Report_tables->exists($this->tableName);
    }
	private function isTableGenerating()
	{
		return $this->CI->Report_tables->exists($this->tableName,"pending");
	}

	private function waitForGeneration()
	{
		$waitLimit = 30;
		$currentWait = 1;
		while ($this->isTableGenerating() && $currentWait < $waitLimit) {
			$currentWait++;
			sleep(1);
		}
	}
}