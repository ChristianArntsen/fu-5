<?php

namespace fu\reports\StaticReports;


use fu\reports\ReportFilters\Filter;

abstract class Report
{

    /**
     * @var Filter[]
     */
    protected $filters;

    public function __construct()
    {
        $this->filters = [];

        $this->CI = & get_instance();
    }

    public function addFilter(Filter $filter)
    {
        $this->filters[] = $filter;
    }

    public abstract function render();
}