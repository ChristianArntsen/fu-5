<?php

namespace fu\reports\StaticReports;


use Carbon\Carbon;

class MembershipSummary extends Report
{

    public function render()
    {
        $this->CI->load->model("course");
        $this->CI->load->model("appconfig");
        $this->CI->load->model("terminal");
        $this->CI->load->model("employee");
		$course_id = $this->CI->session->userdata("course_id");
        $course_info = $this->CI->Course->get_info($this->CI->session->userdata("course_id"));

        $data['course_name'] =$course_info->name;
        $data['logo'] = $this->CI->appconfig->get_logo_image("",$this->CI->session->userdata("course_id"));
        $data['date'] = Carbon::now()->format("M j Y h:i a");

        $data['customers'] = [];

	    $query = $this->CI->db->query("
	    SELECT customers.person_id,customers.last_name,customers.first_name,customers.label,frequency.groups, totals.total_invoiced,totals.total_paid,proshopspend,fbspend
		FROM (
			SELECT c.person_id, last_name, first_name, GROUP_CONCAT(label,', ') label
			FROM foreup_customers AS c
			LEFT JOIN foreup_customer_group_members AS cgm ON cgm.person_id = c.person_id
			LEFT JOIN foreup_customer_groups AS cg ON cg.group_id = cgm.group_id
			LEFT JOIN foreup_people AS fp ON fp.person_id = c.person_id
			WHERE c.deleted = 0 AND c.course_id = $course_id AND c.deleted = 0
			GROUP BY c.person_id
			ORDER BY c.person_id
		) customers
		LEFT JOIN (
			SELECT c.person_id, GROUP_CONCAT(frequency_period,', ') groups
			FROM foreup_customers AS c
			LEFT JOIN foreup_customer_billing AS cb ON c.person_id = cb.person_id AND cb.deleted = 0 AND cb.course_id = $course_id
			WHERE c.deleted = 0 AND c.course_id = $course_id AND c.deleted = 0
			GROUP BY c.person_id
			ORDER BY c.person_id
		) frequency ON customers.person_id = frequency.person_id
		
		LEFT JOIN (
		SELECT c.person_id, SUM(total) total_invoiced, SUM(paid) total_paid
		FROM foreup_customers AS c
		LEFT JOIN foreup_invoices AS i ON c.person_id = i.person_id AND i.deleted = 0
		WHERE c.deleted = 0 AND c.course_id = $course_id AND c.deleted = 0
		GROUP BY c.person_id
		ORDER BY c.person_id
		) totals ON customers.person_id = totals.person_id
		
		LEFT JOIN (
		SELECT c.person_id, SUM(CASE WHEN i.department = 'PRO SHOP' THEN si.total ELSE 0 END) AS proshopspend, SUM(CASE WHEN i.department = 'FOOD & BEVERAGE' THEN si.total ELSE 0 END) AS fbspend
		FROM foreup_customers AS c
		LEFT JOIN foreup_sales AS s ON s.customer_id = c.person_id AND s.deleted = 0
		LEFT JOIN foreup_sales_items AS si ON si.sale_id = s.sale_id
		LEFT JOIN foreup_items AS i ON i.item_id = si.item_id
		WHERE c.deleted = 0 AND c.course_id = $course_id AND c.deleted = 0
		GROUP BY c.person_id
		ORDER BY c.person_id
	
		) category ON customers.person_id = category.person_id
  
		");


        $data['customers'] =  $query->result_array();








        $rendered = $this->CI->load->view('reportsv2/MembershipSummary',$data,true);
        return $rendered;
    }

}