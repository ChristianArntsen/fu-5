<?php

namespace fu\reports\StaticReports;


use Carbon\Carbon;
use fu\reports\ParamDate;
use fu\reports\ReportFactory;
use fu\reports\ReportFilters\Filter;
use fu\reports\ReportParameters;

class SellThru extends Report
{
    public function render()
    {
	    $json = file_get_contents('php://input');
	    $input = json_decode($json,true);
	    $this->CI->load->model("appconfig");
        $this->CI->load->model("course");
        $course_id = $this->CI->session->userdata('course_id');
		$course_info = $this->CI->course->get_info($course_id);


	    $dateRange = $input['date_range'];
	    $date = new ParamDate($dateRange);
	    $start = Carbon::parse($date->getStart())->toDateTimeString();
	    $end = Carbon::parse($date->getEnd())->toDateTimeString();
	    $where = $this->generateFilters($input['filters']);

		//Get starting inventory
	    $query = $this->CI->db->query("
	        SELECT 
			items.name,items.category,items.subcategory,items.department,items.quantity,items.cost_price,items.is_unlimited,
			i.trans_items,SUM(i.trans_inventory) as starting_inventory,new_inventory.total as new_inventory,total_sold.total as total_sold
			FROM foreup_inventory i 
			LEFT JOIN foreup_items items ON i.trans_items = items.item_id
			LEFT JOIN foreup_suppliers ON items.supplier_id = foreup_suppliers.person_id
			LEFT JOIN (
			
				SELECT SUM(i2.trans_inventory) as total,trans_items FROM foreup_inventory i2 WHERE 
				i2.trans_date >= '{$start}'
				AND
				course_id = {$course_id} 
				AND 
				i2.trans_date <= '{$end}'  
				AND i2.trans_inventory > 0
				GROUP BY i2.trans_items ORDER BY NULL
				
			) new_inventory ON new_inventory.trans_items = i.trans_items
			LEFT JOIN (
			
				SELECT SUM(i2.trans_inventory) as total,trans_items FROM foreup_inventory i2 WHERE 
				i2.trans_date >= '{$start}'
				AND
				course_id = {$course_id}  
				AND 
				i2.trans_date <= '{$end}'
				AND i2.trans_inventory < 0
				GROUP BY i2.trans_items ORDER BY NULL
				
			) total_sold ON total_sold.trans_items = i.trans_items
			WHERE i.course_id = {$course_id}  
			AND i.trans_date < '{$start}'
			AND items.is_unlimited = 0
			AND items.invisible = 0
			AND items.deleted = 0
			AND items.is_fee = 0
			AND items.is_pass = 0
			{$where}
			GROUP BY i.trans_items  ORDER BY NULL
	    ");
	    $this->CI->db->reset_query_builder();

	    $table = [];
	    foreach($query->result() as $row)
	    {
			$new_row = [
				"name"=>$row->name,
				"department"=>$row->department,
				"category"=>$row->category,
				"subcategory"=>$row->subcategory,
				"starting_inventory"=>$row->starting_inventory ?? 0,
				"new_inventory"=>$row->new_inventory ?? 0,
				"total_sold"=>$row->total_sold * -1 ?? 0,
				"total_inventory"=>$row->starting_inventory + $row->new_inventory,
				"sell_thru_rate"=>($row->starting_inventory + $row->new_inventory) != 0 ? number_format((-1 *$row->total_sold) / ($row->starting_inventory + $row->new_inventory) * 100,2)."%" : "0.00%",
			];
		    $table[] = $new_row;
	    }



		
		
	    $data['start'] = $date->getStart();
	    $data['table'] = $table;
	    $data['end'] = $date->getEnd();
	    $data['course_name'] =$course_info->name;
	    $data['logo'] = $this->CI->appconfig->get_logo_image("",$this->CI->session->userdata("course_id"));
	    $data['date'] = Carbon::now()->format("M j Y h:i a");
        $rendered = $this->CI->load->view('reportsv2/SellThru',$data,true);
        return $rendered;
    }

	/**
	 * @param $input
	 * @param $matches
	 * @return string
	 */
	private function generateFilters($filter): string
	{
		$reportParameters = new \fu\reports\ReportParameters();
		isset($filter) ? $reportParameters->setFilters($filter) : "";
		foreach ($reportParameters->getFilters() as $filter) {
			$filter->convertFilterToWhereStatement($this->CI->db);
		}
		$statement = $this->CI->db->compileSelect();
		$where = "";
		if (preg_match("/WHERE (.*)/", $statement, $matches)) {
			$where = " AND " . $matches[1];
		}
		return $where;
	}
}