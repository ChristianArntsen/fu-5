<?php

namespace fu\reports\StaticReports;


use Carbon\Carbon;
use fu\reports\ParamDate;
use fu\reports\ReportFactory;
use fu\reports\ReportFilters\Filter;
use fu\reports\ReportParameters;

class PaymentsAndSales extends Report
{

    public function render()
    {
	    $this->CI->load->model("appconfig");
    	
        $this->CI->load->model("course");
	    $course_model = new \Course();
	    $course_ids = array();
	    $course_model->get_linked_course_ids($course_ids, 'shared_tee_sheet');
        /*
         * Purchases are credit
         * Payments are debit
         *
         * Possible that purchases become a debit when returned, same with credit
         */
        
        $columns = ["grouping","course","glcode","debit","credit"];
        
        $table = [];
		$input = [];
		$courses = [];
	
	    $json = file_get_contents('php://input');
	    $input = json_decode($json,true);
		
		foreach($course_ids as $course_id){
			$course_info = $course_model->get_info($course_id);
			$courses[$course_id] = [];
			$table = [];
			$dateRange = $input['date_range'];
			$date = new ParamDate($dateRange);
			$date->setColumn('sale_date');
			
			
			$debitTotal = 0;
			$creditTotal=0;
			
			
			
			/*
			 * Purchases
			 */
			$reportParams = new ReportParameters();
			//quantity_purchased
			$reportParams->setColumns([
				"gl_code",
				"SUM(IF(`quantity_purchased` > 0 ,`total`, 0))",
				"SUM(IF(`quantity_purchased` < 0 ,`total`, 0))"
			]);
			$reportParams->setDateFromObject($date);
			$reportParams->setGroupBy(["gl_code"]);
			$courseFilter = new \fu\reports\ReportFilters\Filter([
				"column"=>"sales.course_id",
				"operator"=>"=",
				"value"=>$course_id,
				"boolean"=>"AND",
			]);
			$itemsFilter = new Filter([
				"column"=>"sales_items.type",
				"operator"=>"!=",
				"value"=>["member_account","invoice_account","customer_account","giftcard","raincheck"],
				"boolean"=>"AND",
			]);
			$reportParams->addFilter($courseFilter);
			$reportParams->addFilter($itemsFilter);
			$reportObj = ReportFactory::create("shared_sales",$reportParams);
			$purchases = $reportObj->getData();
			foreach($purchases as $row){
				$newrow = array_fill(0,count($columns),"");
				$newrow[array_search("glcode",$columns)] = $row['gl_code'];
				$newrow[array_search("credit",$columns)] = $row['SUM(IF(`quantity_purchased` > 0 ,`total`, 0))'];
				$newrow[array_search("debit",$columns)] = abs($row['SUM(IF(`quantity_purchased` < 0 ,`total`, 0))']);
				$newrow[array_search("grouping",$columns)] = "Sales";
				$newrow[array_search("course",$columns)] = $course_info->name;
				$creditTotal += $row['SUM(IF(`quantity_purchased` > 0 ,`total`, 0))'];
				$debitTotal += abs($row['SUM(IF(`quantity_purchased` < 0 ,`total`, 0))']);
				$table[] = $newrow;
			}
			
			
			/**
			 * Payments
			 */
			$date->setColumn("sale_time");
			$reportParams = new ReportParameters();
			$reportParams->setColumns([
				"SUM(IF(`payments.payment_amount` > 0 ,`payments.payment_amount`, 0))",
				"SUM(IF(`payments.payment_amount` < 0 ,`payments.payment_amount`, 0))",
				"payments.type"
				]
			);
			$reportParams->setGroupBy(["payments.type"]);
			$reportParams->setDateFromObject($date);
			$courseFilter = new \fu\reports\ReportFilters\Filter([
				"column"=>"course_id",
				"operator"=>"=",
				"value"=>$course_id,
				"boolean"=>"AND",
			]);
			$reportParams->addFilter($courseFilter);
			$reportObj = ReportFactory::create("sale_transaction",$reportParams);
			$reportObj->enableSharedReport();
			$payments = $reportObj->getData();
			foreach($payments as $row){
				$newrow = array_fill(0,count($columns),"");
				$newrow[array_search("grouping",$columns)] = $row['payments.type'];
				$newrow[array_search("debit",$columns)] = $row['SUM(IF(`payments.payment_amount` > 0 ,`payments.payment_amount`, 0))'];
				$newrow[array_search("credit",$columns)] = abs($row['SUM(IF(`payments.payment_amount` < 0 ,`payments.payment_amount`, 0))']);
				$newrow[array_search("glcode",$columns)] = "-";
				$newrow[array_search("course",$columns)] = $course_info->name;
				$debitTotal += $row['SUM(IF(`payments.payment_amount` > 0 ,`payments.payment_amount`, 0))'];
				$creditTotal += abs($row['SUM(IF(`payments.payment_amount` < 0 ,`payments.payment_amount`, 0))']);
				$table[] = $newrow;
			}
			
			
			/**
			 * Totals
			 */
			$newrow = array_fill(0,count($columns),"");
			$newrow[array_search("credit",$columns)] = $creditTotal;
			$newrow[array_search("debit",$columns)] = $debitTotal;
			$table[] = $newrow;
			
			
			$courses[$course_id]['table']=$table;
			$courses[$course_id]['name']=$course_info->name;
			
		}
		
		
	    $data['start'] = $date->getStart();
	    $data['end'] = $date->getEnd();
	    $data['courses'] = $courses;
	    $data['course_name'] =$course_info->name;
	    $data['logo'] = $this->CI->appconfig->get_logo_image("",$this->CI->session->userdata("course_id"));
	    $data['date'] = Carbon::now()->format("M j Y h:i a");
        $rendered = $this->CI->load->view('reportsv2/PaymentsAndSales',$data,true);
        return $rendered;
    }
}