<?php
// Table of Accounts
// Provides accounts to do bookkeeping against

namespace fu\accounting;

class TableOfAccounts {
	private $CI;
	private $table;
	private $last_error;

	function __construct($table = 'table_of_accounts')
	{
		$this->CI = get_instance();
		$this->table = $table;
	}

	public function get_last_error() {
		return $this->last_error;
	}

	public function reset_last_error() {
		$this->last_error = null;
	}

	public function post($course_id, $name, $account_type, $description = null, $gl_code = null) {
		$id = false;

		// validation
		// required parameters
		if(!is_numeric($course_id) || !is_int($course_id*1)){
			$this->last_error = 'TableOfAccounts->post Error: course_id is required, and must be an integer.';
			return false;
		}
		if(!is_string($name)){
			$this->last_error = 'TableOfAccounts->post Error: name is required, and must be a string.';
			return false;
		}
		if(!is_string($account_type) ||
		   !in_array($account_type,array('asset', 'liability', 'revenue', 'expense'))){

			$this->last_error = 'TableOfAccounts->post Error: account_type is required, '.
				                'and must be a string with one of the following values: '.
			                    'asset liability revenue expense.';
			return false;
		}

		// populate query parameters
		$params = array('course_id'=>$course_id,'name'=>$name,'account_type'=>$account_type);

		// optional parameters
		if(is_string($description)){
			$params['description'] = $description;
		}elseif (isset($description)){
			$this->last_error = 'TableOfAccounts->post Warning: description must be a string.';
		}
		if(is_numeric($gl_code) && is_int($gl_code*1)){
			$params['gl_code'] = $gl_code;
		}elseif (isset($gl_code)){
			$this->last_error = 'TableOfAccounts->post Warning: gl_code must be an integer or integer string.';
		}
		// end validation

		try {
		    $success = $this->CI->db->insert($this->table,$params);

			return $this->CI->db->insert_id();
		}
		catch(\Exception $e){
			$this->last_error = 'TableOfAccounts->post Error: '.$e->getMessage();
		}
		return false;
	}

	public function put($course_id, $name, $account_type, $account_id = null, $description = null, $gl_code = null) {
		$id = false;
		$constraints = array();
		$update_id = null;

		// validation
		// required parameters
		if(!isset($account_id) && !isset($course_id)){
			$this->last_error = 'TableOfAccounts->put Error: not enough information to insert OR update a row. Please include minimum of account_id or combination of course_id and name.';
			return false;
		}
		if(!is_numeric($course_id) || !is_int($course_id*1)){
			$this->last_error = 'TableOfAccounts->put Error: course_id is required, and must be an integer.';
			return false;
		}else{
			// course id will not change if this is an update
		}
		if(!is_string($name)){
			$this->last_error = 'TableOfAccounts->put Error: name is required, and must be a string.';
			return false;
		}else{
			// if we made it this far, it might be an update
			$unq_query = $this->CI->db->get_where($this->table,array('course_id'=>$course_id,'name'=>$name));
			$result = $unq_query->result();

			if(count($result)===1) {
				// already exists. It is an update.
				$constraints['course_id'] = $course_id;
				$constraints['name'] = $name;
				$update_id = $result[0]->account_id;
			}
			elseif(!isset($account_id)){
				// we can safely defer to post
				return $this->post($course_id, $name, $account_type, $description, $gl_code);
			}
		}
		if(!is_string($account_type) ||
			!in_array($account_type,array('asset', 'liability', 'revenue', 'expense'))){

			$this->last_error = 'TableOfAccounts->put Error: account_type is required, '.
				'and must be a string with one of the following values: '.
				'asset liability revenue expense.';
			return false;
		}

		// populate query parameters
		$params = array('course_id'=>$course_id,'name'=>$name,'account_type'=>$account_type, 'description'=>null, 'gl_code'=>null);

		// optional parameters
		if(is_numeric($account_id) && is_int($account_id*1)){
			$constraints['account_id'] = $account_id;
			if(isset($course_id))$constraints['course_id']=$course_id;
			unset($constraints['name']);// we are perhaps updating name.

			$update_id = $account_id;
			// this is now certainly an update.
			$unq_query = $this->CI->db->get_where($this->table,$constraints);
			$result = $unq_query->result();

			if(count($result)!==1) {
				$this->last_error = 'TableOfAccounts->put Error: account_id not found.';
				return false;
			}
		}elseif (isset($account_id)){
			// keel over if we are trying to update a specific account, and don't have a valid account_id
			$this->last_error = 'TableOfAccounts->put Error: account_id must be null, integer or integer string.';
			return false;
		}
		if(!isset($description) || is_string($description)){
			$params['description'] = $description;
		}elseif (isset($description)){
			$this->last_error = 'TableOfAccounts->put Warning: description must be null or a string.';
		}
		if(!isset($gl_code) || (is_numeric($gl_code) && is_int($gl_code*1))){
			$params['gl_code'] = $gl_code;
		}elseif (isset($gl_code)){
			$this->last_error = 'TableOfAccounts->put Warning: gl_code must be null, integer or integer string.';
		}
		// end validation
	    $params['deleted'] = 0;

        $success = false;
		if(count($constraints)) {
			try {
				$success = $this->CI->db->update($this->table, $params, $constraints);
			} catch (\Exception $e) {
				$this->last_error = 'TableOfAccounts->put Error: ' . $e->getMessage();
				return false;
			}
		}else{
			try {
				$success = $this->CI->db->insert($this->table,$params);

				return $this->CI->db->insert_id();
			}
			catch(\Exception $e){
				$this->last_error = 'TableOfAccounts->put Error: '.$e->getMessage();
			}
		}


		if($success) {
			return $update_id;
		}

		return false;
	}

	public function patch($parameters, $validate_only = false) {
		$params = array();
		$constraints = array();
		$update_id = null;

		// validation
		// required parameters
		if(!is_array($parameters)){
			$this->last_error = 'TableOfAccounts->patch Error: expecting an array of parameters.';
			return false;
		}

		if(!isset($parameters['account_id']) && !(isset($parameters['course_id']) && isset($parameters['name']))){
			$this->last_error = 'TableOfAccounts->patch Error: not enough information to update a row. Please include minimum of account_id or combination of course_id and name.';
			return false;
		}

		if(isset($parameters['account_id']) && (!is_numeric($parameters['account_id']) || !is_int($parameters['account_id']*1))){
			$this->last_error = 'TableOfAccounts->patch Error: account_id must be an integer.';
			return false;
		}elseif(isset($parameters['account_id'])){
			$constraints['account_id'] = $parameters['account_id'];
			$update_id = $parameters['account_id'];
		}

		if(isset($parameters['course_id']) && (!is_numeric($parameters['course_id']) || !is_int($parameters['course_id']*1))){
			$this->last_error = 'TableOfAccounts->patch Error: course_id must be an integer.';
			return false;
		}elseif(isset($parameters['course_id'])){
			$constraints['course_id'] = $parameters['course_id'];
		}

		if(isset($parameters['name']) && !is_string($parameters['name'])){
			$this->last_error = 'TableOfAccounts->patch Error: name must be a string.';
			return false;
		}elseif(isset($parameters['name']) && isset($parameters['account_id'])){
			// we might be updating name
			$params['name'] = $parameters['name'];
		}
		elseif(isset($parameters['name'])){
			$constraints['name'] = $parameters['name'];
			$result = $this->get(array('course_id'=>$parameters['course_id'],'name'=>$parameters['name']),1);

			if($result && count($result)===1) {
				$update_id = $result[0]['account_id'];
				$constraints['account_id'] = $update_id;
			}
			else{
				$this->last_error = 'TableOfAccounts->patch Error: row does not exist.';
				return false;
			}
		}

		// optional parameters
		if(is_string($parameters['account_type']) &&
			in_array($parameters['account_type'],array('debit', 'credit', 'revenue', 'expense'))){

			$params['account_type'] = $parameters['account_type'];
		}elseif (isset($parameters['account_type'])){
			$this->last_error = 'TableOfAccounts->patch Error: account_type '.
				'must be a string with one of the following values: '.
				'asset liability revenue expense.';

			return false;
		}
		if(is_string($parameters['description'])){
			$params['description'] = $parameters['description'];
		}elseif (isset($parameters['description'])){
			$this->last_error = 'TableOfAccounts->patch Error: description must be a string.';

			return false;
		}
		if(is_numeric($parameters['gl_code']) && is_int($parameters['gl_code']*1)){
			$params['gl_code'] = $parameters['gl_code'];
		}elseif (isset($parameters['gl_code'])){
			$this->last_error = 'TableOfAccounts->patch Error: gl_code must be an integer or integer string.';

			return false;
		}
		// end validation

		if($validate_only)return true;

		try {
			$success = $this->CI->db->update($this->table, $parameters, $constraints);
		}
		catch(\Exception $e){
			$this->last_error = 'TableOfAccounts->patch Error: '.$e->getMessage();
			return false;
		}

		if($success) {
			return $update_id;
		}
		else{
			$this->last_error = 'TableOfAccounts->patch Error: '.$this->CI->db->_error_message();
		}
		return false;
	}

	public function delete ($id, $name, $hard = false, $validate_only = false) {
		// validation
		// required parameters
		if(!is_numeric($id) || !is_int($id*1)){
			$this->last_error = 'TableOfAccounts->get Error: id is required, '.
				'and must be an integer corresponding to account id, '.
				'or the course id accompanied by the name parameter.';
			return false;
		}
		if(isset($name)){
			$course_id = $id;
			if(!is_string($name)){
				$this->last_error = 'TableOfAccounts->get Error: name must be a string.';
				return false;
			}
		}else{
			$account_id = $id;
			if(isset($this->CI->session) && $this->CI->session->userdata('course_id'))
				$course_id = $this->CI->session->userdata('course_id');
		}

		if($validate_only)return true;

		// populate query parameters
		$params = array();
		if($name){$params['name']=$name;}
		if($course_id){$params['course_id']=$course_id;}
		if($account_id){$params['account_id']=$account_id;}

		try {
			if ($hard) {
				if ($course_id) {
					$this->CI->db->where('course_id', $course_id);
				}
				if ($name) {
					$this->CI->db->where('name', $name);
				} else {
					$this->CI->db->where('account_id', $account_id);
				}
				$success = $this->CI->db->delete($this->table);
			} else {
				$success = $this->CI->db->update($this->table, array('deleted' => 1), $params);
			}
		}
		catch(\Exception $e){
			$this->last_error = 'TableOfAccounts->delete Error: '.$e->getMessage();
			return false;
		}

		if($success) {
			return $this->CI->db->affected_rows();
		}
		return false;
	}

	public function get ($parameters,$inc_deleted = false,$validate_only = false) {
		$course_id = false;
		$account_id = false;
		$name = false;
		// validation
		// required parameters
		if(empty($parameters) || (!is_numeric($parameters)&&!is_array($parameters))){
			$this->last_error = 'TableOfAccounts->get Error: parameters attribute is required, '.
				'and must be an integer corresponding to account id, '.
				'or the course id accompanied by the name parameter.';
			    return false;
		}elseif(is_numeric($parameters)){
			if(!is_int($parameters*1)){
				$this->last_error = 'TableOfAccounts->get Error: account_id must be an integer.';
				return false;
			}
			$account_id = $parameters*1;
		}else{
			if(isset($parameters['course_id']) && is_numeric($parameters['course_id']) && is_int($parameters['course_id']*1)){
				$course_id = $parameters['course_id'];
			}elseif (isset($parameters['course_id'])){
				$this->last_error = 'TableOfAccounts->get Error: course_id must be an integer.';
				return false;
			}

			if(isset($parameters['account_id']) && is_numeric($parameters['account_id']) && is_int($parameters['account_id']*1)){
				$account_id = $parameters['account_id'];
			}elseif (isset($parameters['account_id'])){
				$this->last_error = 'TableOfAccounts->get Error: account_id must be an integer.';
				return false;
			}

			if(isset($parameters['name']) && is_string($parameters['name'])){
				$name = $parameters['name'];
			}elseif (isset($parameters['name'])){
				$this->last_error = 'TableOfAccounts->get Error: name must be a string.';
				return false;
			}
		}

		if(!$course_id && isset($this->CI->session) && $this->CI->session->userdata('course_id'))
			$course_id = $this->CI->session->userdata('course_id');

		if($name && !$course_id && !$account_id){
			$this->last_error = 'TableOfAccounts->get Error: name must be accompanied by either a course_id or account_id.';
			return false;
		}

		if($validate_only)return true;

		$this->CI->db->select()->from($this->table);
		if(!$inc_deleted){
			$this->CI->db->where('deleted',0);
		}
		if($course_id) {
			$this->CI->db->where('course_id', $course_id);
		}
		if($name){
			$this->CI->db->where('name',$name);
		}
		if($account_id){
			$this->CI->db->where('account_id',$account_id);
		}

		$ret = false;
		try {
			$get = $this->CI->db->get();
			$ret = $get->result_array();
		}
		catch(\Exception $e){
			$this->last_error = 'TableOfAccounts->get Error: '.$e->getMessage();
			return false;
		}

		return $ret;
	}
}

?>