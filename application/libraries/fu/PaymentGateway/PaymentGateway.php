<?php
namespace fu\PaymentGateway;

interface PaymentGateway{

	// member variable $storedPayment
	// public $storedPayment

	public function charge($amount);

	public function return($amount);

	public function void();

	public function setPaymentGatewayCredentials(PaymentGatewayCredentials $credentials);

	public function setStoredPayment(StoredPaymentInfo $storedPaymentInfo);

	public function saveCreditCardPayment(int $processor_id);

	public function getAuthorizedAmount();

	public function getLastResults();

	public function getLastError();

}
?>