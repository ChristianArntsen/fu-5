<?php
namespace fu\PaymentGateway;

use fu\PaymentGateway\PaymentGatewayException\GeneralFailureException;
use fu\PaymentGateway\PaymentGatewayException\PartialAuthorizationException;
use fu\PaymentGateway\PaymentGatewayException\PaymentDeclinedException;
use fu\PaymentGateway\PaymentGatewayException\ExpiredCreditCardException;
use GuzzleHttp\Client;
use function GuzzleHttp\Psr7\_caseless_remove;

class MercuryAchPaymentGateway implements PaymentGateway
{
	/**
	 * @var StoredPaymentInfo
	 */
	private $storedPayment;

	/**
	 * @var PaymentGatewayCredentials
	 */
	private $paymentGatewayCredentials;

	/**
	 * @var \SimpleXMLElement
	 */
	private $lastResults;

	/**
	 * @var \SimpleXMLElement | null
	 */
	private $lastError;

	private $CI;

	private $config;

	public $processor_id_key = 'mercury_id';

	public $provider = 'mercury_ach';

	public function __construct(array $config,&$CI = null)
	{
		if(isset($CI))
			$this->CI = $CI;
		else
			$this->CI = get_instance();

		$this->CI->load->library('v2/Element_merchant');

		$this->config = $config;
	}

	public function charge($amount,$test = false)
	{
		if(!is_numeric($amount)){
			throw new GeneralFailureException('Amount must be numeric');

		}

		$credentials = $this->paymentGatewayCredentials->getElementCredentials();
		// Initialize payment library
		$this->CI->element_merchant->init(
			new Client(),
			[
				'account_id'=>$credentials['element_account_id'],
				'account_token'=>$credentials['element_account_token'],
				'acceptor_id'=>$credentials['element_acceptor_id'],
				'application_id' => 8366,
				'terminal_id'=>$credentials['element_application_id'],
				'testing'=>$test
			]
		);

		$this->CI->element_merchant->check_sale([
			'payment_account_reference_number' => $this->storedPayment->getAccountNumber(),
			'payment_account_id' => $this->storedPayment->getToken(),
			'market_code' =>  $this->CI->element_merchant::MARKET_CODE_ECOMMERCE,
			'reference_number' => $this->storedPayment->getAccountNumber(),
			'transaction_amount' => $amount
		]);

		$this->lastResults = $this->CI->element_merchant->response()->xml();

		$status = $this->lastResults->Response->ExpressResponseMessage;

		$status = $status === 'Success'?'Approved':$status;

		$message = isset($this->lastResults->Response->HostResponseMessage)? ': '. $this->lastResults->HostResponseMessage : '';

		$this->updatePaymentStatus($this->storedPayment->getPaymentId(),$this->lastResults->Results,null,$credentials,$amount);
		$this->CI->Charge_attempt->save($this->storedPayment->getAccountNumber(), null,
			$amount, date('Y-m-d H:i:s'),
			"'".$status."'",
			$status.$message, $this->storedPayment->getPaymentId());

		if($status !== 'Approved'){
			$transaction_id = (string) $this->CI->element_merchant
				->response()->xml()
				->Response->Transaction->TransactionID;
			return true;
		}else {
			$this->throw_error($status,$message);
			return false;
		}
	}

	public function throw_error($status, $message = '') {

		$this->lastError = $this->lastResults;
		# Element has a rich set of errors to handle
		# this is a start that tries to focus on the check related ones
		# and those that have strange meanings beyond what is included in the message.
		# When we support cards, we will want to expand coverage
		switch ($this->lastResults->Response->ExpressResponseCode) {
			case 'Invalid Transaction Status':
				throw new GeneralFailureException('Cannot perform that action on a transaction with its current status.');
			case 'Invalid Transaction Type':
				//

			case 'Declined':
				switch($message) {
					case 'Declined':
						throw new PaymentDeclinedException('Declined: ACH transaction declined by CheckGateway');
					case 'INVLD MER ID':
						throw new GeneralFailureException('Declined: Invalid merchant id: perhaps this merchant is not set up for Discover at the autorization network level (FDC)?');
					case 'DECLINE':
						throw new PaymentDeclinedException('Declined: Card holder should contact their bank');
					case 'DECLINE-TRY LATER':
						throw new GeneralFailureException('Declined - Try Later: Try again in 3 days');
					case 'REVK PAY ORDERED':
						throw new PaymentDeclinedException('Declined: Revoke pay ordered: Stop payment on this recurring transaction');
					case 'TRAN NOT ALLOWED':
						throw new PaymentDeclinedException('Declined: Transaction not allowed');
					case 'EXPIRED CARD':
						throw new ExpiredCreditCardException('Declined: Card expired');
					default:
						throw new GeneralFailureException('Declined: Payment was declined');
				}
			case 'Duplicate':
				throw new GeneralFailureException('Duplicate: Same account, same amount, same day attempted');
			case 'Declined - Pick up Card':
				throw new PaymentDeclinedException('Declined: Pick up card');
			case 'Call issuer':
				throw new PaymentDeclinedException('Call issuer: Account holder should contact issuing bank');
			case 'Not Defined':
				throw new GeneralFailureException('CheckGateway error: Element needs to call them and complain');
			case 'ERROR NOT MAPPED':
				throw new PaymentDeclinedException('Error Not Mapped: Invalid checking account');
			case 'MagneprintData Required':
				throw new GeneralFailureException('ManageprintData Required: Check the SOAP');
			case 'Unable to process Magneprint data':
				throw new GeneralFailureException('Something is wrong with magneprint. Check for spaces in the XML SOAP, and unencrypted swipe');
			case 'Invalid Data':
				switch($message) {
					case 'Bank routing number validation negative (district).':
						throw new GeneralFailureException('Invalid Data: Bank routing number validation negative (district)');
					case 'Bank routing number validation negative (ABA).':
						throw new GeneralFailureException('Invalid Data: Bank routing number validation negative (ABA).');
					case 'Bank account number should be numeric.':
						throw new GeneralFailureException('Invalid Data: Bank account number should be numeric.');
					case 'Bank routing number must be 9 digits.':
						throw new GeneralFailureException('Invalid Data: Bank routing number must be 9 digits.');
					case 'Invalid Login':
					case 'SEC Code must be specified for Debits.':
					case 'Invalid Company Number (should be 6 digits).':
						throw new GeneralFailureException('Invalid Data: Contact Element to get them to fix things on their end');
					default:
						throw new GeneralFailureException('Invalid Data: ensure proper format');
				}
			case 'TransactionAmount invalid':
				throw new GeneralFailureException('TransactionAmount invalid: Enter two full decimal places, and no "-" sign');
			case 'TransactionAmount required':
				throw new GeneralFailureException('TransactionAmount required: can be caused by outdated web browsers');
			case 'Invalid AccountToken':
				throw new GeneralFailureException('Invalid AccountToken: Check that we are pointing to right platform, and that we pass the correct account token');
			case 'Invalid Account':
				throw new GeneralFailureException('Invalid Account: Check that AcceptorID is correct and not malformed. Otherwise, contact Element for permissions and problems on their end');
			case 'Authentication Failed':
				throw new GeneralFailureException('Authentication Failed: Check that the account token is correct, and has not been replaced by generating a new one.');
			case 'RETURN AMOUNT NO EQUAL TO ORIGINAL TRANSACTION AMOUNT':
				throw new GeneralFailureException('Refund amount must be for the original amount');
				case 'Unable to connect to Host':
				case 'No Response From Host':
					throw new GeneralFailureException('Element cannot connect to their Host');
				case 'Error':
					throw new GeneralFailureException('Something may be wrong with Element Express');
				default:
					throw new GeneralFailureException('Unexpected Status received: '.$status. ': '.$message);
			}
	}

	public function return($amount)
	{
		if(!is_numeric($amount)){
			throw new GeneralFailureException('Amount must be numeric');

		}

		// TODO: Implement refund() method.

	}

	public function void()
	{
		// TODO: Implement void() method
		throw new GeneralFailureException('"void()" not implemented');
	}

	public function saveCreditCardPayment(int $processor_id)
	{
		$invoice = $this->CI->Sale->add_credit_card_payment(
			array('mercury_id'=>$processor_id,
				'tran_type'=>'ACH',
				'frequency'=>'Recurring'
			)
		);

		return $invoice;
	}

	public function updatePaymentStatus(int $invoice,$transaction_results,$transaction_time = null,$credentials,$amount){
		if(!isset($transaction_time))
			$transaction_time = date('Y-m-d H:i:s');

		$payment_data = array(
			'element_account_id' => $credentials['element_account_id'],
			'element_account_token' => $credentials['element_account_token'],
			'element_acceptor_id' => $credentials['element_acceptor_id'],
			'element_application_id' => $credentials['element_application_id'],
			'payment_id' => $transaction_results->Transaction->TransactionID,
			'status' => $transaction_results->ExpressResponseMessage,
			'status_message' => $transaction_results->HostResponseMessage,
			'masked_account' => $this->storedPayment->getMaskedAccount(),
			'card_type' => '',
			'token' => $this->storedPayment->getToken(),
			'trans_post_time'	=> (string) $transaction_time,
			'ref_no' => $transaction_results->Transaction->ReferenceNumber,
			'amount' => $amount,
			'auth_amount' => $amount
		);

		$this->CI->Sale->update_credit_card_payment($invoice, $payment_data);
	}

	public function getLastError()
	{
		return $this->lastError;
	}

	public function getLastResults()
	{
		// TODO: provide generic PaymentGatewayResults object
		return $this->lastResults;
	}

	public function getAuthorizedAmount()
	{

		// TODO: Implement getAuthorizedAmount() method
		//return (float) $this->getLastResults()->AuthorizeAmount;
		throw new GeneralFailureException('"getAuthorizedAmount()" not implemented');
	}

	public function setStoredPayment(StoredPaymentInfo $storedPaymentInfo)
	{
		$this->storedPayment=$storedPaymentInfo;
	}

	public function setPaymentGatewayCredentials(PaymentGatewayCredentials $credentials)
	{
		$this->paymentGatewayCredentials = $credentials;
	}
}