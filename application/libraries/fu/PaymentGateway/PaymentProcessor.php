<?php
namespace fu\PaymentGateway;

use fu\PaymentGateway\PaymentGatewayException\GeneralFailureException;

class PaymentProcessor
{
	protected $chargeAttempts;
	protected $customer;
	protected $chargeTransactionLogger;
	protected $paymentGateway;


	/**
	 * @param int $amount
	 */
	public function setChargeAttempts($number)
	{
		if(!is_numeric($number)){
			throw new GeneralFailureException('Number must be numeric');

		}
		if(!is_int($number*1)){
			throw new GeneralFailureException('Number must be integer');

		}

	}


	/**
	 * @param float $amount
	 */
	public function chargeCustomer($amount)
	{
		if(!is_numeric($amount)){
			throw new GeneralFailureException('Amount must be numeric');

		}

	}


	/**
	 * @param float $amount
	 */
	public function returnToCustomer($amount)
	{
		if(!is_numeric($amount)){
			throw new GeneralFailureException('Amount must be numeric');

		}

	}


	/**
	 * @param \fu\PaymentGateway\PaymentGateway $amount
	 */
	public function setPaymentGateway(\fu\PaymentGateway\PaymentGateway $gateway)
	{
		//
	}
}

?>