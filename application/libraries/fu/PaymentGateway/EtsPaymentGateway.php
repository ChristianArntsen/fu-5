<?php
namespace fu\PaymentGateway;

use fu\PaymentGateway\PaymentGatewayException\ExpiredCreditCardException;
use fu\PaymentGateway\PaymentGatewayException\GeneralFailureException;
use fu\PaymentGateway\PaymentGatewayException\PartialAuthorizationException;
use fu\PaymentGateway\PaymentGatewayException\PaymentDeclinedException;

class EtsPaymentGateway implements PaymentGateway
{
	/**
	 * @var StoredPaymentInfo
	 */
	private $storedPayment;

	/**
	 * @var PaymentGatewayCredentials
	 */
	private $paymentGatewayCredentials;

	/**
	 * @var \SimpleXMLElement
	 */
	private $lastResults;

	/**
	 * @var \SimpleXMLElement | null
	 */
	private $lastError;

	public $CI;

	private $config;

	public $processor_id_key = 'ets_key';

	public $provider = 'ets';

	public function __construct(array $config,&$CI = null)
	{
		if(isset($CI))
			$this->CI = $CI;
		else
			$this->CI = get_instance();

		$this->config = $config;
	}

	public function charge($amount)
	{
		if(!is_numeric($amount)){
			throw new GeneralFailureException('Amount must be numeric');

		}

		// stored payment info should have been set:
		// course_id, credit_card_id, ets_key,
		// token, cardholder_name
		$config = $this->config;
		if(isset($config['id']))
			$config['course_id'] = $config['id'];

		$credentials = $this->paymentGatewayCredentials->getETSCredentials();


		$CI = get_instance();
		$CI->load->library('Hosted_payments_v3');
		$CI->load->model('Charge_attempt');
		$payment = new \Hosted_payments_v3();


		$corr_id = $payment->to_corr_id($this->storedPayment->getPaymentId());
		$session_token = $payment->sign_in($credentials['ets_key'], $corr_id);
		if(!isset($session_token)){
			throw new GeneralFailureException('Authentication failed for MerchantID/Password');
		}
		$p_resp = $payment->charge($session_token, $corr_id, $this->storedPayment->getToken(), $amount, $corr_id);
		if(isset($p_resp->error)){
			throw new GeneralFailureException('Charge failed: '.$p_resp->error->description);
		}
		$response = $payment->verify($session_token, $p_resp->chargeResponse->transactionId);
		$session = $response->properties;

		if(!isset($session->amount)) {

			return false;
		}

		$credit_card_id = $this->storedPayment->getAccountNumber();
		$this->updatePaymentStatus($this->storedPayment->getPaymentId(),$session);
		$this->CI->Charge_attempt->save($credit_card_id, null,
			$session->amount, date('Y-m-d H:i:s'),
			$session->status,
			$session->message.' '.$session->message,
			$this->storedPayment->getPaymentId());

		if((string) $session->status == 'Completed'){
			if((float) $amount - (float) $session->amount !== 0.00){
				throw new PartialAuthorizationException('Payment in full has not been received and additional tender will need to be requested');
			}

			return true;
		}else{
			$this->lastError = $session;
			$status = (string) $session->message;

			if(strpos($status,'EXPIRED CARD') !== false){
				throw new ExpiredCreditCardException('Credit card is expired');

			}elseif(strpos($status,'unsupported.method') !== false){
				throw new GeneralFailureException('The HTTP API does not support the method that was called');

			}elseif(
				// presumably this is "unsupported.method" too
				// leaving it in original form to make sure we catch
				// https://www.etsms.com/docs/Hosted-Payments/docs/plugin/04._successes_and_errors
				strpos($status,'unsupported') !== false ||
				strpos($status,'UNSUPPORTED') !== false
			){
				throw new PaymentDeclinedException('Payment was declined: '.$status);

			}elseif(strpos($status,'payment.invalid.instrument') !== false){
				throw new PaymentDeclinedException('The payment instrument was found to be invalid before it was sent to the processor.');

			}elseif(strpos($status,'payment.invalid.address') !== false){
				throw new PaymentDeclinedException('A required address was either absent or invalid.');

			}elseif(strpos($status,'payment.invalid.user') !== false){
				throw new PaymentDeclinedException('The user was missing, unknown or otherwise invalid.');

			}elseif(strpos($status,'payment.invalid.currency') !== false){
				throw new PaymentDeclinedException('The currency supplied is not supported.');

			}elseif(strpos($status,'payment.unable.auth') !== false){
				throw new PaymentDeclinedException('The attempt to authorize the payment instrument failed.');

			}elseif(strpos($status,'payment.unable.charge') !== false){
				throw new PaymentDeclinedException('The attempt to charge the payment instrument failed.');

			}elseif(strpos($status,'payment.unable.capture') !== false){
				throw new PaymentDeclinedException('The attempt to capture the payment failed.');

			}elseif(strpos($status,'payment.invalid.transactionId') !== false){
				throw new GeneralFailureException('The transaction id that was passed is no longer honored, is missing or is invalid.');

			}else{
				throw new GeneralFailureException('Something went wrong with the ETS interface');

			}

		}

	}

	private function updatePaymentStatus(int $invoice, $session,$transaction_time = null)
	{
		if(!isset($transaction_time)) {
			$transaction_time = date('Y-m-d H:i:s', strtotime($session->created));
		}

		$payment_data = array(
			'auth_amount'		=> (string) $session->amount,
			'card_type'			=> (string)	$this->storedPayment->getAccountType(),
			'masked_account'	=> (string)	$this->storedPayment->getMaskedAccount(),
			'status_message'	=> (string)	$session->message,
			'display_message' 	=> (string)	$session->message,
			'amount'			=> (string) $session->amount,
			'status'			=> (string) $session->status,
			'payment_id'		=> (string) $session->correlationId,
			'trans_post_time'	=> (string) $transaction_time
		);

		$this->CI->Sale->update_credit_card_payment($invoice, $payment_data);

	}

	public function return($amount)
	{
		if(!is_numeric($amount)){
			throw new GeneralFailureException('Amount must be numeric');

		}

		// TODO: Implement refund() method.
		throw new GeneralFailureException('"return" not implemented');

	}

	public function void()
	{
		// TODO: Implement void() method.
		throw new GeneralFailureException('"void" not implemented');
	}

	public function setStoredPayment(StoredPaymentInfo $storedPaymentInfo)
	{
		$this->storedPayment=$storedPaymentInfo;
	}

	public function setPaymentGatewayCredentials(PaymentGatewayCredentials $credentials)
	{
		$this->paymentGatewayCredentials = $credentials;
	}

	public function saveCreditCardPayment(int $processor_id)
	{
		$invoice = $this->CI->Sale->add_credit_card_payment(
			array('ets_key'=>$processor_id,'tran_type'=>'CreditSaleToken','frequency'=>'Recurring')
		);

		return $invoice;
	}

	public function getLastError()
	{
		// TODO: provide a more useful generic error object
		return $this->lastError;
	}

	public function getLastResults()
	{
		// TODO: provide generic PaymentGatewayResults object
		return $this->lastResults;
	}

	public function getAuthorizedAmount()
	{
		// TODO: implement getAuthorizedAmount
		throw new GeneralFailureException('"getAuthorizedAmount" not implemented');
	}



}

?>