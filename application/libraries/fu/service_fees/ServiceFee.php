<?php
namespace fu\service_fees;

class ServiceFee {
    // item that it is applied to
    private $parentItem;
    // item that it extends
    private $item;
    private $service_fee;

    private $CI;

    private $subtotal_hash;
    public $parentItemQuantity;

    // we may want to start with a particular fee and parent item
    public function __construct($service_fee_id = null, $parent_item_id = null)
    {
        $item_id = null;
        $this->CI = get_instance();
        $this->CI->load->model('v2/Item_model');
        $this->CI->load->model('v2/Cart_model');
        $this->CI->load->model('Item');
        $this->subtotal_hash = array();

        if(is_array($service_fee_id)){
            if(isset($service_fee_id['service_fee_id'])){
                $service_fee_id = $service_fee_id['service_fee_id'];
            }elseif(isset($service_fee_id['item_id'])){
                $item_id = $service_fee_id['item_id'];
                $service_fee_id = null;
            }
        }

        if($item_id){
            $this->service_fee = (array) $this->getServiceFeeByItem($item_id);
            $this->item = (array) $this->getItem($this->service_fee['item_id']);
        }
        elseif($service_fee_id) {
            $this->service_fee = (array) $this->getServiceFee($service_fee_id);
            $this->item = (array) $this->getItem($this->service_fee['item_id']);
        }


        if(is_array($parent_item_id)&&isset($parent_item_id['item_id'])){
            $this->parentItem = $parent_item_id;
        }
        elseif($parent_item_id)
            $this->parentItem = (array) $this->getItem($parent_item_id);
    }

    // create a new and shiny service fee
    public function save($rows)
    {
        if(!isset($rows) || !is_array($rows)) return false;
        if(isset($rows['parent_item_percent'])){
            $rows = array($rows);
        }
        $affected = 0;
        foreach($rows as $data) {
            // first we create the item that we are extending,
            // or at least ensure that it exists
            $item_id = null;
            if (isset($data['item_id']) && $data['item_id']) {
                if ($this->CI->Item->exists($data['item_id'])) {
                    $item_id = $data['item_id'];
                } else {
                    return false;
                }
            } else {
                $item_id = $this->CI->Item_model->save(null, $data);
            }

            if (!$item_id) {
                continue;
            } else {
                $data['item_id'] = $item_id;
            }

            // then we create the service fee
            $data_sf = elements(array('service_fee_id', 'item_id', 'parent_item_percent', 'whichever_is'),
                $data, null);

            if($this->exists($data_sf)&&empty($data['service_fee_id'])){
                $service_fee = $this->getServiceFeeByItem($data['item_id']);
                $data_sf['service_fee_id'] = $service_fee['service_fee_id'];
                $this->subtotal_hash = array();
            }

            if (empty($data_sf['service_fee_id'])) {
                // adding new
                $this->CI->db->insert('service_fees', $data_sf);
                $service_fee_id = (int) $this->CI->db->insert_id();
                if(isset($service_fee_id)&&$service_fee_id)
                {
                    $affected++;
                    $data_sf['service_fee_id'] = $service_fee_id;
                }
            } else {
                // updating existing
                $this->CI->db->where('service_fee_id',$data_sf['service_fee_id']);
                $result = $this->CI->db->update('service_fees', $data_sf);
                if(isset($result)&&$result)$affected++;
            }
        }
        if(count($rows)===1){
            $this->item = $data;
            $this->service_fee = $data_sf;
            $this->subtotal_hash = array();
        }
        // then we return number of rows affected
        return $affected;
    }

    public function delete($service_fee_ids = null){
        if(!$service_fee_ids)$service_fee_ids = $this->service_fee['service_fee_id'];

        if($service_fee_ids && !is_array($service_fee_ids)){
            $service_fee_ids = array($service_fee_ids);
        }
        $item_ids = array();
        foreach($service_fee_ids as $id){
            $item = $this->getItemByServiceFee($id);

            $item_ids[]=$item['item_id'];
        }
        $this->CI->Item->delete_list($item_ids);
        return $this->CI->db->affected_rows();
    }

    // adds this service fee to an item
    public function applyToItems($ids = null, $order = 0)
    {
        if(!$ids)return false;
        if(!is_array($ids)){
            $ids = array($ids);
        }
        $affected = 0;
        foreach($ids as $id) {
            if($this->isServiceFee($id))continue;
            $data = array();
            $data['item_id'] = $id;
            $data['service_fee_id'] = $this->service_fee['service_fee_id'];
            $data['order'] = $order;
            if(!$this->isApplied($id)) {
                if ($this->CI->db->insert('item_service_fees', $data))
                    $affected++;
            }else{
                $this->CI->db->update('item_service_fees',array('order'=>$order),
                    array('item_id'=>$id,'service_fee_id'=>$data['service_fee_id']));
            }
        }

        if(count($ids)===1){
            $this->setParentItem($data['item_id']);
        }
        return $affected;
    }

    public function isServiceFee($item_id){
        if(!empty($this->getServiceFeeByItem($item_id)))return true;

        return false;
    }

    private function isApplied($item_id,$service_fee_id = null)
    {
        if(empty($service_fee_id) &&
          isset($this->service_fee))$service_fee_id = $this->service_fee['service_fee_id'];
        $this->CI->db->select('sf.*,i.*');
        $this->CI->db->from('item_service_fees AS isf');
        $this->CI->db->join('service_fees as sf','isf.service_fee_id = sf.service_fee_id','left');
        $this->CI->db->join('items AS i','sf.item_id = i.item_id','left');
        $this->CI->db->where('isf.item_id',$item_id);
        $this->CI->db->where('sf.service_fee_id',$service_fee_id);
        $this->CI->db->where('i.deleted',0);

        $query = $this->CI->db->get();
        $rows = $query->result_array();
        return is_array($rows) && count($rows)?1:0;
    }

    // downgrades an item from service fee to a regular item
    public function removeFromItems($ids = null) {
        if(!$ids)return false;
        if(!is_array($ids)){
            $ids = array($ids);
        }
        // we delete from service_fee where item_id = $item_id
        $this->CI->db->where_in('item_id',$ids);
        $this->CI->db->delete('item_service_fees');
        // return number of rows affected
        $this->CI->db->affected_rows();
    }

    // we may want to apply the fee to several items
    public function setParentItem($item_id)
    {
        if(is_array($item_id)){
            $this->parentItem = $item_id;
        }else{
            $this->parentItem = (array) $this->getItem($item_id); 
        }
    }

    // assumes a fresh parent item
    public function calculatePrice($taxable=1, $divisor=1, $quantity_purchased = null) {
        return $this->calculatePriceForSubtotal($this->parentItem['unit_price'],$taxable,$divisor,$quantity_purchased);
    }

    public function calculatePriceForSubtotal($subtotal, $taxable = 1, $divisor = 1, $quantity_purchased = null){

        $percent_fee = 0;
        $flat_fee = 0;

        $ItemPrice = new \fu\items\ItemPrice();

        if(isset($this->parentItem['price'])){
            $this->parentItem['unit_price'] = $this->parentItem['price'];
        }
        $this->item['item_type'] = 'service_fee';

        if(empty($this->parentItem['comp'])){
            $this->parentItem['comp'] = false;
        }
        if(empty($this->parentItem['quantity_purchased'])){
        	if(!empty($this->parentItem['quantity']))$this->parentItem['quantity_purchased'] = $this->parentItem['quantity'];
        	else $this->parentItem['quantity_purchased'] = 1;
        }
        if($quantity_purchased){ // invoices use quantity instead of quantity purchased. Override
            $this->parentItem['quantity_purchased'] = $quantity_purchased;
            $this->parentItem['quantity'] = $quantity_purchased;
        }

        $this->parentItem['item_type']='item';


        if(!isset($this->parentItem['subtotal'])&&!isset($this->parentItem['split_subtotal'])){
            $this->CI->Cart_model->calculate_item_totals($this->parentItem, $taxable,$this->parentItem['unit_price_includes_tax']);

        }

        $parent_subtotal = $this->parentItem['subtotal'];
        if(isset($this->parentItem['split_subtotal'])){
            $parent_subtotal = $this->parentItem['split_subtotal'];
        }
        $parent_subtotal = ($parent_subtotal / $this->parentItem['quantity_purchased']);

        // calculate flat fee
        $flat_fee_item = $this->item;
        $flat_fee_item['comp'] = $this->parentItem['comp'];
        $flat_fee_item['unit_price'] = $this->item['unit_price'];
        $flat_fee_item['unit_price'] = round($flat_fee_item['unit_price'] / $divisor, 2);
	    $flat_fee_item['quantity_purchased'] = $this->parentItem['quantity_purchased'];
	    $flat_fee_item['quantity'] = $this->parentItem['quantity_purchased'];
        $flat_fee_item['cost_price'] = 0;
        
        $this->CI->Cart_model->calculate_item_totals($flat_fee_item, $taxable);
        $flat_fee = $flat_fee_item['subtotal'];

        // calculate percent of parent item
        $percent_fee_item = $this->item;
        $percent_fee_item['comp'] = $this->parentItem['comp'];
        $percentage = $this->service_fee['parent_item_percent'] / 100;
        $percent_fee_item['unit_price'] = round($parent_subtotal * $percentage, 2);
        $percent_fee_item['quantity_purchased'] = $this->parentItem['quantity_purchased'];
        $percent_fee_item['quantity'] = $this->parentItem['quantity_purchased'];
        $percent_fee_item['cost_price'] = 0;

        $this->CI->Cart_model->calculate_item_totals($percent_fee_item, $taxable);
        $percent_fee = $percent_fee_item['subtotal'];
        
        // see if we want to return the bigger or smaller of the values
        if($this->service_fee['whichever_is'] === 'more'){
            if($percent_fee >= $flat_fee){
                $ret = $percent_fee_item;
            }else{
                $ret = $flat_fee_item;
            }
        }else{
            if($percent_fee < $flat_fee){
                $ret = $percent_fee_item;
            }
            else{
                $ret = $flat_fee_item;
            }
        }

        return $ret;
    }

    public function getTotal($taxable=1, $divisor=1) {
        $result = $this->calculatePrice($taxable, $divisor);
        return round( $result['total'] ,2);
    }

    public function getSubtotal($taxable=1, $divisor=1) {
        $result = $this->calculatePrice($taxable, $divisor);
        return round( $result['subtotal'] ,2);
    }

    public function getTax($taxable=1, $divisor=1) {
        $result = $this->calculatePrice($taxable, $divisor);
        return round( $result['tax'] ,2);
    }

    public function getAsArray($attribute_array = null) {
        $ret = $this->item;
        $ret = array_merge($ret, $this->service_fee);
        return $ret;
    }

    public function getAsObject($attribute_array = null) {
        $ret = (object) $this->getAsArray($attribute_array);
        return $ret;
    }

    private function getServiceFee($service_fee_id) {
        $fee = null;
        $this->CI->db->select('sf.*');
        $this->CI->db->from('service_fees AS sf');
        $this->CI->db->join('items AS i','sf.item_id = i.item_id','left');
        $this->CI->db->where('service_fee_id',$service_fee_id);
        $this->CI->db->where('sf.item_id = i.item_id');
        $this->CI->db->where('i.deleted',0);
        $fee = $this->CI->db->get()->row_array();
        return $fee;
    }

    public function exists($item = null) {
        if(!$item){
            $item = $this->service_fee;
        }
        if(!$item){
            $this->item;
        }
        if($item && !is_array($item)){
            $item = array('service_fee_id',$item);
        }
        if($item['service_fee_id']){
            $result = $this->getServiceFee($item['service_fee_id']);
            if($result)return true;
        }
        if($item['item_id']){
            $result = $this->getServiceFeeByItem($item['item_id']);
            if($result)return true;
        }
        return false;
    }

    private function getServiceFeeByItem($item_id) {
        $fee = null;
        $this->CI->db->select('sf.*');
        $this->CI->db->from('service_fees AS sf');
        $this->CI->db->join('items AS i','sf.item_id = i.item_id','left');
        $this->CI->db->where('sf.item_id',$item_id);
        $this->CI->db->where('i.deleted',0);
        $fee = $this->CI->db->get()->row_array();
        return $fee;
    }

    private function getItemByServiceFee($service_fee_id) {
        $item = null;
        $this->CI->db->select('i.*');
        $this->CI->db->from('items AS i');
        $this->CI->db->join('service_fees as sf','sf.item_id = i.item_id','left');
        $this->CI->db->where('sf.service_fee_id',$service_fee_id);
        $this->CI->db->where('i.deleted',0);
        $item = $this->CI->db->get()->row_array();
        return $item;
    }

    private function getItem($item_id) {
        $obj = $this->CI->Item->get_info($item_id,false,true);
        $obj->base_price = $obj->unit_price;
        return $obj;
    }

    public function getServiceFeesForItem($item_id,$serialize = false)
    {
        $this->CI->db->select('sf.*,i.*, i.unit_price AS base_price');
        $this->CI->db->from('item_service_fees AS isf');
        $this->CI->db->join('service_fees as sf','isf.service_fee_id = sf.service_fee_id','left');
        $this->CI->db->join('items AS i','sf.item_id = i.item_id','left');
        $this->CI->db->where('isf.item_id',$item_id);
        $this->CI->db->where('i.deleted',0);

        $query = $this->CI->db->get();
        if(!$serialize)
        {
            $fees = array();
            foreach($query->result() as $row){
                $row = (array) $row;
                $ServiceFee = new \fu\service_fees\ServiceFee($row['service_fee_id'],$item_id);
                $fees[] = $ServiceFee;
            }
            return $fees;
        }
    }

    public function deleteServiceFeesForItem($item_id, $fee_id_list = null){
        $fees = $this->getServiceFeesForItem($item_id);
        $remove_list = array();
        if(is_array($fee_id_list) && count($fee_id_list)){
            // only remove the fees that are in the list
            foreach($fees as $fee){
                if(in_array($fee->service_fee_id,$fee_id_list)){
                    $remove_list[] = $fee;
                }

            }
        }else{
            $remove_list = $fees;
        }

        foreach($fees as $fee){
            $fee->removeFromItems($item_id);
        }
    }

    public function search($query,$course_id = 0){
        if(!$course_id){
            if($this->CI->session->userdata('course_id'))
                $course_id = $this->CI->session->userdata('course_id');
        }
        $this->CI->db->select('sf.service_fee_id AS value, i.name AS label');
        $this->CI->db->from('service_fees as sf');
        $this->CI->db->join('items AS i','sf.item_id = i.item_id','left');
        $this->CI->db->where("i.name LIKE '%".$this->CI->db->escape_like_str($query)."%'");
        $this->CI->db->where('i.course_id', $course_id);
        $this->CI->db->where('i.deleted', 0);
        $query = $this->CI->db->get();

        $rows = $query->result_array();
        return $rows;
    }

    public function getHtml($service_fee_id = null) {
        // use our own service fee, if available
        if(empty($service_fee_id) && isset($this->service_fee)){
            $ServiceFee = $this;
        }

        // override if given a service fee id
        if($service_fee_id){
            $ServiceFee = new \fu\service_fees\ServiceFee($service_fee_id);
        }

        if(empty($service_fee_id) && !isset($ServiceFee)){
            return '';
        }

        $serial = $ServiceFee->getAsArray();
        $html = $this->CI->load->view('items/item_service_fee', $serial);
        return $html;
    }

    public function saveItemServiceFeesToSale($sale_id,$item,$taxable = 1){
        $parent_id = $item['item_id'];
        if(empty($item['service_fees']))return array();
        $sf_count = 0;
        foreach($item['service_fees'] as &$fee){
            $sf_count++;
            $this->CI->load->model('teesheet');
            $this->CI->load->model('schedule');
            //$this->CI->load->model('permissions');
            $fees = array();


            $q = isset($item['quantity_purchased'])?$item['quantity_purchased']:1;
            $item['quantity_purchased'] = $q;
            $d = isset($item['discount']) ? $item['discount'] : 0;
            $itemCost = 0;

            // we do not set the parent item because it may be a different price now
            $ServiceFee = new \fu\service_fees\ServiceFee($fee['service_fee_id']);

            $calc = $ServiceFee->calculatePriceForSubtotal($item['subtotal'], $taxable);
            $p = $calc['subtotal'];
            if($fee['force_tax']==1){
                $taxable = 1;
            }

            $sales_items_data = array(
                'sale_id' => $sale_id,
                'item_id' => $fee['item_id'],
                'invoice_id' => !empty($item['invoice_id']) ? (int) $item['invoice_id'] : '',
                // if a sale has > 10,000 lines,
                // the customer can afford to pay us to deal with a rollover of lines
                'line' => $item['line']+(10000*$sf_count),
                'description' => !empty($fee['description']) ? $fee['description'] : '',
                'timeframe_id' => $item['timeframe_id'],
                'special_id' => $item['special_id'],
                'price_class_id' => '',
                'teesheet' => $this->CI->permissions->course_has_module('reservations') ?
                              $this->CI->schedule->get_info($this->CI->session->userdata('schedule_id'))->title :
                              $this->CI->teesheet->get_info($this->CI->session->userdata('teesheet_id'))->title,
                'price_category' => '',
                'serialnumber' => !empty($fee['serialnumber']) ? $fee['serialnumber'] : '',
                'quantity_purchased' => $item['quantity_purchased'],
                'item_cost_price' => $fee['cost_price'],
                'item_unit_price'=> round($calc['subtotal']/$q,2), // sets the price of the thing in stone.
                'unit_price_includes_tax' => !empty($fee['unit_price_includes_tax']) ? $fee['unit_price_includes_tax'] : 0,
                'num_splits' => isset($item['num_splits'])?$item['num_splits']:1,
                'is_side' => 0,
                'erange_code' => '',
                'subtotal' => $calc['subtotal'],
                'tax' => $taxable == true ? $calc['tax'] : 0,
                'total' => $taxable == true ? $calc['total'] : $calc['subtotal'],
                'receipt_only' =>  0
            );

            if($sales_items_data['price_class_id'] == '')
                $sales_items_data['price_class_id'] = 17;
            $sales_items_data['teesheet'] = $sales_items_data['teesheet']==NULL ? '' : $sales_items_data['teesheet'];
            $sales_items_data['profit'] = round((($p * ((100 - $d) / 100)) - $fee['cost_price']), 2);
            $sales_items_data['total_cost'] = round($q * $itemCost, 2);

            $item_success = $this->CI->db->insert('sales_items', $sales_items_data);
            if(isset($fee['taxes']) && $taxable && !(isset($fee['force_taxes']) && $fee['force_taxes'])) {
                $sales_items_data['taxes'] = $calc['taxes'];
                $item['taxes'] = $calc['taxes'];
            }
            else $sales_items_data['taxes'] = array();
            $fees[] = &$sales_items_data;
        }

        return $fees;
    }
}
?>