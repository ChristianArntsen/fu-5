<?php

namespace fu\marketing\texting;

use fu\nexmo\Nexmo;
use Nexmo\Client;
use Nexmo\Exception;

/**
 * Class Text
 * @package fu\marketing\texting
 */
class Texting
{
    /**
     * @var string
     */
    private $api_key;
    /**
     * @var string
     */
    private $api_secret;
    /**
     * @var array
     */
    private $recipients;
    /**
     * @var int         $msg_delay          Number of microseconds to wait after sending a message in order to avoid
     *                                      hitting Nexmo's throughput limits. Different API's have different limits.
     */
    private $msg_delay;

    private $from;
    private $virtualNumberClient;

    private $msg_per_sec;
    /**
     * @var string
     */
    private $shortcode;

    function __construct($course_id = null)
    {
        $this->CI =& get_instance();
        $this->api_key = $this->CI->config->item('api_key', 'nexmo');
        $this->api_secret = $this->CI->config->item('api_secret', 'nexmo');
        $this->shortcode = $this->CI->config->item('shortcode', 'nexmo');
        $this->CI->load->model('Marketing_Texting');

        $course_info = $this->CI->Course->get_info($course_id);
        $nexmo = new Nexmo($course_info,$this->CI->db,$this->CI->config->item('api_key', 'nexmo'),$this->CI->config->item('api_secret', 'nexmo'));
		if($course_info->country == "CA"){
			$this->useLongNumber = true;
			$this->virtualNumberClient = $nexmo;
			if($nexmo->has_number()){
				$this->from = $nexmo->get_number()->virtual_number;
			} else {
				$this->from = $nexmo->buy_number();
			}
		}
    }

	/**
	 * @return mixed
	 */
	public function getFrom()
	{
		return $this->from;
	}

	/**
	 * @param mixed $from
	 */
	public function setFrom($from)
	{
		$this->from = $from;
	}

    /**
     * Send an invitation to a list of numbers
     *
     * @param string    $course_name        The name of the course sending the invite.
     * @param string    $course_keyword     The course's custom keyword, if there is one.
     *
     * @return array                        Indexed array where keys are MSISDN's and values
     *                                      are error messages for each number that failed.
     *
     * @throws \ErrorException              If recipients haven't been set.
     */
    public function sendTextInvite($course_name = null, $course_keyword = null)
    {
	    if($this->useLongNumber){
		    $this->msg_per_sec = 1;
	    } else {
		    $this->msg_per_sec = 25;
	    }
        $this->msg_delay = 1000000/$this->msg_per_sec; // Throughput limit of 1 msg/second on Nexmo's standard messaging API.
        if (empty($this->recipients)) {
            throw new \ErrorException('Must set $recipients before sending invite.');
        }

        $message = $this->generateMessage($course_name, $course_keyword);
        $client = new Client($this->api_key, $this->api_secret);
        $messenger = $client->__get('Message');
         $delivery_data = array();
        foreach($this->recipients as $recipient)
        {
            if(isset($recipient['course_name'])){
                $message = $this->generateMessage($recipient['course_name'], null);
            }

            try {

            	if($this->useLongNumber){
		            $response = $this->virtualNumberClient->send_message($recipient['msisdn'],$message);
	            } else {
		            $messenger->invoke (
			            'foreUP',
			            $recipient['msisdn'],
			            'text',
			            $message
		            );
	            }


                $this->CI->Marketing_Texting->logEvent($recipient['course_id'],$recipient['msisdn'],"invite");
                if(isset($course_keyword)){
                    $this->CI->Marketing_Texting->updateSubscriptionByMsisdn (
                        $recipient['msisdn'],
                        $course_keyword,
                        $texting_status = 'invited_not_subscribed'
                    );
                } else {
                    $this->CI->Marketing_Texting->updateSubscriptionById (
                        $recipient['person_id'],
                        $recipient['course_id'],
                        $texting_status = 'invited_not_subscribed'
                    );
                }

            } catch(Exception $e) {
                $delivery_data[strval($recipient)] = $e->getMessage();
            }
            usleep($this->msg_delay);
        }
        return $delivery_data;
    }

    /**
     * Function to use for automated alerts.
     *
     * @param $message
     * @return array
     * @throws \ErrorException
     */
    public function sendAlert($message)
    {
	    if($this->useLongNumber){
		    $this->msg_per_sec = 1;
	    } else {
		    $this->msg_per_sec = 25;
	    }
        $this->msg_delay = 1000000/$this->msg_per_sec; // Throughput limit of 1 msg/second on Nexmo's standard messaging API.
        if (empty($this->recipients)) {
            throw new \ErrorException('Must set $recipients before sending invite.');
        }
        $client = new Client($this->api_key, $this->api_secret);
        $messenger = $client->__get('Message');
        $delivery_data = array();
        foreach($this->recipients as $recipient)
        {
            try {
	            if($this->useLongNumber){
	            	if($recipient['texting_status'] != "unsubscribed"){
			            echo "Attempting to send to ".$recipient['msisdn'];
			            $response = $this->virtualNumberClient->send_message($recipient['msisdn'],$message);
			            $this->CI->Marketing_Texting->logEvent($recipient['course_id'],$recipient['msisdn'],"alert");
		            }
	            } else {
		            $messenger->invoke(
			            'foreUP',
			            $recipient['msisdn'],
			            'text',
			            $message
		            );
		            $this->CI->Marketing_Texting->logEvent($recipient['course_id'],$recipient['msisdn'],"alert");
	            }
            } catch(Exception $e)
            {
                $delivery_data[strval($recipient)] = $e->getMessage();
            }
            usleep($this->msg_delay);
        }
        return $delivery_data;
    }

    /**
     * Send a marketing SMS to a list of numbers.
     *
     * @param string    $message        The contents of the SMS.
     *
     * @return array                    Indexed array where keys are msisdn's and values are error messages. It gives
     *                                  you back each number that failed and the corresponding error message and code.
     *
     * @throws \ErrorException          If the recipients array is empty.
     */
    public function sendMarketingText($message)
    {
    	if($this->from){
		    $this->msg_per_sec = 1;
	    } else {
		    $this->msg_per_sec = 25;
	    }
        $this->msg_delay = 1000000/$this->msg_per_sec; // Throughput limit of 1 msg/second on Nexmo's standard messaging API.
        if (empty($this->recipients)) {
            throw new \ErrorException('Must set $recipients before sending campaign.');
        }
        $client = new Client($this->api_key, $this->api_secret);
        $messenger = $client->__get('MarketingMessage');
        $delivery_data = array();
        foreach($this->recipients as $recipient)
        {
            if($recipient['texting_status'] == 'subscribed') {
            	if($this->from){
					$response = $this->virtualNumberClient->send_message($recipient['msisdn'],$message);
	            } else {
		            $delivery_data = $this->invokeShortCodeMessage($message, $messenger, $recipient, $delivery_data);
	            }
            }
            usleep($this->msg_delay);
        }
        return $delivery_data;
    }

    /**
     * Set the recipients array.
     *
     * @param array     $recipients         The code in this class expects that $recipients is an array of
     *                                      indexed arrays containing at least the following data: 'person_id',
     *                                      'course_id', 'msisdn', 'texting_status', and 'keyword'.
     */
    public function setRecipients(Array $recipients)
    {
        $this->recipients = $recipients;
    }

    public function loadRecipient($customer_id)
    {
        $select = $this->CI->db
            ->join("customers","people.person_id = customers.person_id")
            ->select(array("people.person_id","people.email","people.birthday",'customers.course_id'))
            ->from('people');
        $this->CI->db->join("marketing_texting","people.person_id = marketing_texting.person_id","right")
            ->select(["msisdn","texting_status","keyword"]);

        $select->where('foreup_people.person_id',$customer_id);
        $select->group_by('person_id');
        $select->order_by('person_id');

        $query = $select->get();
        if ($query->num_rows() > 0)
        {
            $row = $query->row_array();
            $this->setRecipients([$row]);
            return true;
        }

        error_log("Attempting to send to customer:{$customer_id} but he doesn't have a number.");
        return false;
    }

    /**
     * @param $course_name
     * @param $course_keyword
     * @return string
     */
    private function generateMessage($course_name, $course_keyword)
    {
    	if(empty($this->from)){
		    if (!$course_name) {
			    $message = 'Text the keyword GOLF to ' . $this->shortcode . ' to receive special offers from your golf course. Reply STOP to opt out.';
			    return $message;
		    } else if (!$course_keyword) {
			    $message = 'Text the keyword GOLF to ' . $this->shortcode . ' to receive special offers from ' . $course_name . '.  Reply STOP to opt out.';
			    return $message;
		    } else {
			    $message = 'Text the keyword ' . $course_keyword . ' to ' . $this->shortcode . ' to receive special offers from ' . $course_name . '.  Reply STOP to opt out.';
			    return $message;
		    }
	    } else {
    		return "Reply with the keyword GOLF to receive special offers.  Reply STOP to opt out.";
	    }

    }

	/**
	 * @param $message
	 * @param $messenger
	 * @param $recipient
	 * @param $delivery_data
	 * @return mixed
	 */
	private function invokeShortCodeMessage($message, $messenger, $recipient, $delivery_data)
	{
		try {
			$messenger->invoke(
				$this->shortcode,
				$recipient['keyword'],
				$recipient['msisdn'],
				$message
			);
			$this->CI->Marketing_Texting->logEvent($recipient['course_id'], $recipient['msisdn'], "marketing");
			return $delivery_data;
		} catch (Exception $e) {
			$delivery_data[strval($recipient)] = $e->getMessage();
			return $delivery_data;
		}
	}
}
