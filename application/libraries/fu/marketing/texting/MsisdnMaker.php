<?php

namespace fu\marketing\texting;

/**
 * Class MsisdnMaker
 * @package fu\marketing\texting
 */
class MsisdnMaker
{
    /**
     * @var string
     */
    private $msisdn = null;
    /**
     * @var string
     */
    private $prefix = null;
    /**
     * @var array
     */
    private $usa_prefixes = null;

    public function __construct()
    {
        $this->usa_prefixes = json_decode(file_get_contents(__DIR__ . '/prefixes/usa.json'));
        $this->CI =& get_instance();
        $this->CI->load->model('Marketing_Texting');
    }


    /**
     * @param array $customers Required. Expects an array of indexed arrays containing at least the following data:
     *                         'person_id', 'course_id', 'cell_phone_number', 'phone_number', and 'keyword'. The
     *                         keyword is either the 'GOLF' keyword or the course's custom keyword.
     *
     */
    public function makeMsisdns($customers,$status = null)
    {
        foreach ($customers as $customer) {
            // Check for a valid cell phone number.
            $this->set($customer['cell_phone_number']);
            if ($this->isValid()) {
                $valid_phone = 1;
            }
            else {
                $this->set($customer['phone_number']);
                // Check if the customer's other phone number is valid.
                if ($this->isValid()) {
                    $valid_phone = 1;
                } // The customer either has no phone numbers listed or both are invalid.
                else {
                    $valid_phone = 0;
                }
            }

            $customer_data = array (
                'person_id' => $customer['person_id'],
                'course_id' => $customer['course_id'],
                'msisdn' => $this->get(),
                'valid_phone' => $valid_phone,
                'keyword' => isset($customer['keyword'])?$customer['keyword']:"GOLF"
            );
            if($status != null){
                $customer_data['texting_status'] = $status;
            }
            if($valid_phone){
            	$this->CI->Marketing_Texting->insertCustomer($customer_data);
            }else{
                $customer_data['texting_status']="not-invited";
                $this->CI->Marketing_Texting->insertCustomer($customer_data);
            }
        }
    }

    /**
     * Set the MSISDN
     *
     * @param string $msisdn
     * @return boolean Returns whether the given MSISDN is valid or not
     */
    public function set($msisdn)
    {
        $this->msisdn = $msisdn;
        $this->clean();
        $this->prefix = null;
    }

    /**
     * Cleans the phone number.
     */
    private function clean()
    {
        // Strip all non-numeric characters
        $this->msisdn = preg_replace("/[^0-9]/", "", $this->msisdn);
        // If the number is 11 digits and has a leading 1,
        // drop the 1.
        if (strlen($this->msisdn) == 11 && substr($this->msisdn, 0, 1) == '1') {
            // Store substring of $msisdn
            $this->msisdn = substr($this->msisdn, 1);
        }
    }

    /**
     * Returns the MSISDN number. Resets the class variable $msisdn to null, so after you call get(),
     * another call to set() is required.
     *
     * @return string The MSISDN number.
     */
    public function get()
    {
        if (!isset($this->msisdn)) {
            return null;
        }
        $msisdn = $this->msisdn;
        $this->msisdn = null;
        return '1' . $msisdn;
    }

    /**
     * Determines if the number is valid or not. This function assumes that the
     * number given is already clean.
     *
     * @return boolean
     *
     * @throws \ErrorException      If MSISDN isn't set.
     */
    public function isValid()
    {
        if (!isset($this->msisdn)) {
            throw new \ErrorException('Must set MSISDN before checking validity.');
        }
        // Check for empty string.
        if (empty($this->msisdn)) {
            return false;
        }
        // Make sure the number is 10 digits and is numeric only.
        if (strlen($this->msisdn) != 10 || !is_numeric($this->msisdn)) {
            return false;
        }
        // Check for valid U.S. area code.
        if (!in_array($this->getPrefix(), $this->usa_prefixes)) {
            return false;
        }

        return true;
    }

    /**
     * Returns the prefix of the MSISDN number.
     *
     * @return string The prefix of the MSISDN number
     */
    private function getPrefix()
    {
        if (!isset($this->msisdn)) {
            return null;
        }

        return substr($this->msisdn, 0, 3);
    }
}
