<?php
namespace fu\marketing\recipientRetriever;

class report extends \fu\marketing\recipientRetriever{

    private $possibleRecipients;
    private $campaign;
    private $report_id;

    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->model('Person');
        $this->CI->load->model('Course');
        $this->CI->load->model('Marketing_campaign');
        $this->CI->load->model('Report');
    }


    public function setDelay($delay)
    {
        // TODO: Implement setDelay() method.
    }


    public function limitToEmail()
    {
        $this->texting = false;
    }

    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
        $this->course_id = $campaign->course_id;
    }

    //'person_id','course_id', 'msisdn', 'texting_status', and 'keyword'.
    public function getRecipients()
    {
        if(!isset($this->course_id)){
            throw new \fu\Exceptions\InvalidArgument("Must set campaign before retrieving recipients.");
        }
        $result = "";

        $this->report_recipients = $this->getRecipientsFromReport();
        if(empty($this->report_recipients)){
            return [];
        }

        $groups = [];
        $individuals = [];
        $exclude_groups = [];
        $exclude_individuals = [];
        if(count($this->individuals)== 0 && count($this->groups) == 0){
            $this->groups[] = 0;
        }


        if(count($this->groups) > 0) {
            $groups = $this->getRecipientsInGroup($this->groups);
        }
        if(count($this->exclude_groups) > 0) {
            $exclude_groups = $this->getRecipientsInGroup($this->exclude_groups);
        }

        //Select individuals from report
        if(count($this->individuals) > 0){
            $select = $this->CI->db
                ->join("customers","people.person_id = customers.person_id")
                ->select(array("people.person_id","people.email","people.birthday",'customers.course_id',"cell_phone_number","phone_number","first_name","last_name"))
                ->from('people');
            if($this->texting){
                $this->addTextSelectionCriteria();
            }

            $select->where_in('foreup_people.person_id',$this->individuals);
            $select->where_in('foreup_people.person_id', $this->report_recipients);
            $select->group_by('person_id');
            $select->order_by('person_id');

            $individuals = $select->get()->result_array();
        }


        $exclude_result = array_merge($exclude_individuals,$exclude_groups);
        $result = array_merge($individuals,$groups);
        //$result = array_diff($result, $exclude_result);

        return $result;
    }



    private function getRecipientsInGroup($passed_groups)
    {
        if (array_search(0, $passed_groups) !== false) {
            $course_ids = array();
            $this->CI->Course->get_linked_course_ids($course_ids, 'shared_customers',$this->course_id );
            $course_ids[] = $this->course_id;
            $select = $this->CI->db
                ->select(array("people.person_id", "people.email", "birthday","cell_phone_number","phone_number","customers.course_id AS course_id","first_name","last_name"))
                ->from('people');
            $select->join('customers','people.person_id = customers.person_id');
            $select->where_in('customers.course_id', $course_ids);
            $select->where_in('people.person_id', $this->report_recipients);
        } else if ($passed_groups) {
            $select = $this->CI->db
                ->select(array("people.person_id", "people.email", "birthday","cell_phone_number","phone_number","customer_groups.course_id AS course_id","first_name","last_name"))
                ->from('people');
            $select->join('customer_group_members', 'customer_group_members.person_id = people.person_id', 'left');
            $select->join('foreup_customer_groups', 'customer_groups.group_id = customer_group_members.group_id', 'left');

            $select->where_in('people.person_id', $this->report_recipients);
            $select->where_in('customer_group_members.group_id',$passed_groups);
        }
        if($this->texting){
            $this->addTextSelectionCriteria();
        }
        $groups = $select->get()->result_array();
        return $groups;
    }
    private function addTextSelectionCriteria()
    {
        $this->CI->db->join("marketing_texting","people.person_id = marketing_texting.person_id","right")
            ->select(["msisdn","texting_status","keyword"])
            ->where("marketing_texting.texting_status","subscribed");
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function getRecipientsFromReport()
    {
        $campaign_model = new \Marketing_campaign();
        $report = $campaign_model->get_reports_for_campaign($this->campaign->campaign_id);
        $reportParameters = new \fu\reports\ReportParameters();

        $report_model = new \Report();
        $report_template = $report_model->get_info($report->report_id);

        $reportParameters->loadFromReport($report_template);

        //Use column
        if(!isset($report_template->user_field)){
            error_log("Attempting to run a report campaign without a user field. Type: {$report_template->type} Base: {$report_template->base}");
            return [];
        }
        $reportParameters->setColumns(["distinct({$report_template->user_field})"]);

        $reportObj = \fu\reports\ReportFactory::create($report_template->base, $reportParameters);
        $data = $reportObj->getData();

        $recipients = [];
        foreach ($data as $row) {
            $recipients[] = $row['distinct(customer_id)'];
        }
        return $recipients;
    }
}