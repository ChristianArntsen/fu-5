<?php
namespace fu\marketing;

use fu\marketing\texting\MsisdnMaker;
use fu\marketing\texting\Texting;

class Campaign {

	private $recipientRetriever;

	public function __construct(RecipientRetriever $recipientRetriever, $campaign)
	{
		$this->CI = & get_instance();
		$this->course = $this->CI->Course;
		$this->marketing_campaign = $this->CI->Marketing_campaign;
		$this->recipientRetriever = $recipientRetriever;
		$this->campaign = $campaign;
	}

	public function send()
	{
        $campaignObj = $this->marketing_campaign->get_info($this->campaign->campaign_id);
		$this->marketing_campaign->mark_attempted($this->campaign->campaign_id);


		$type = $this->campaign->type;
		if(strtolower($type) == 'email') {
            $recipients = $this->recipientRetriever->getRecipients();
            $contents = $this->campaign->rendered_html;

            if ($contents != '') {
                $sendgridConfig = $this->CI->config->item('sendgrid');
                $sendgrid = new \SendGrid($sendgridConfig['username'], $sendgridConfig['password']);
                $to = [];
                $substitutions = [];
                $course_model = new \Course();
                $course_info = $course_model->get_info($this->campaign->course_id);
                foreach($recipients as $recipient){
                    $to[$recipient['email']] = $recipient;
                    $substitutions["{{customer.first_name}}"][]=$recipient['first_name'];
                    $substitutions["{{customer.last_name}}"][]=$recipient['last_name'];
                    $substitutions["{{course.name}}"][]=$course_info->name;
                }
                $chunked_to = array_chunk($to, 1000);
                //Separate each group by 100
                //log_message("debug", 'email_helper send_sendgrid campaign_id - ' . $campaign_id . ' recipient count ' . count($to));
                $totalSent = 0 ;
                foreach ($chunked_to as $chunk) {
                    $emails  = [];
                    foreach($chunk as $person){
                        $totalSent++;
                        $emails[] = $person['email'];
                    }
                    //This whole part of the system seems like a huge mess.  Should be reworked but for now this should work with the unique ids
                    $email = new \SendGrid\Email();
                    $email->setSubject($this->campaign->subject)
                        ->setFrom("no-reply@foreupsoftware.com")
                        ->setFromName($this->campaign->from_name)
                        ->setReplyTo($this->campaign->from_email)
                        ->addUniqueArg("marketing_campaign_id", $this->campaign->campaign_id)
                        ->addUniqueArg("course_id", $this->campaign->course_id)
                        ->setHtml($contents)
                        ->setSubstitutions($substitutions)
                        ->setSmtpapiTos($emails);


                    $result = json_decode($sendgrid->send($email));
                    //log_message("debug", 'email_helper send_sendgrid response - ' . json_encode($result));

                    if ($result->message == 'error') {
                        $errors = '';
                    }
                }
                $this->CI->db->where('campaign_id', $this->campaign->campaign_id);
                $this->CI->db->update('marketing_campaigns', array('recipient_count'=>$totalSent,'is_sent' => 1, 'status' => 'sent', 'send_date' => date('Y-m-d H:i:s')));

            }
        }
            else
		{
            $this->recipientRetriever->limitToTexting();
            $recipients = $this->recipientRetriever->getRecipients();

            $msisdnMaker = new MsisdnMaker();
            $msisdnMaker->makeMsisdns($recipients);

            $this->recipientRetriever->limitToTexting();
            $recipients = $this->recipientRetriever->getRecipients();
            if(!empty($recipients)){
                $texting = new Texting($this->campaign->course_id);
                $texting->setRecipients($recipients);


                $course_model = new \Course();
                $course_info = $course_model->get_info($this->campaign->course_id);
                if($course_info->send_alert_sms_marketing == 1){
                    $errors = $texting->sendAlert($this->campaign->content);
                } else {
                    $errors = $texting->sendMarketingText($this->campaign->content);
                }
            }

            $this->CI->db->where('campaign_id', $this->campaign->campaign_id);
            $this->CI->db->update('marketing_campaigns', array('recipient_count'=>count($recipients)-count($errors),'is_sent' => 1, 'status' => 'sent', 'send_date' => date('Y-m-d H:i:s')));

        }
	}

} 
