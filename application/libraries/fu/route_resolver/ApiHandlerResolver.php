<?php
namespace fu\route_resolver;

use Phroute\Phroute\HandlerResolverInterface;
use Symfony\Component\HttpFoundation\Request;

class ApiHandlerResolver implements HandlerResolverInterface{

    private $container;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function resolve($handler)
    {
        if(is_array($handler) && is_string($handler[0]))
        {
            $handler[0] = new $handler[0]($this->request);
        }

        return $handler;
    }
}