<?php
namespace fu\auth;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;

class json_web_token
{
	private $secretKey = '';

	protected $signer = '';
	protected $token;
	protected $privateClaims = [];

	protected $issuer = '';
	protected $audience = '';
	private  $expiresAt = '';

	public function __construct()
	{
		$this->CI = & get_instance();
		$this->signer = new Sha512();
	}

	public function addPrivateClaim($key, $value){
		$this->privateClaims[$key] = $value;
	}

	public function createToken($audience = null){
		$this->secretKey = $this->CI->config->config['jwt_secretKey'];
		$this->issuer = $this->CI->config->config['jwt_issuer'];


		$this->audience = $this->CI->config->config['jwt_audience'];
		if(isset($audience))
			$this->audience = $audience;
		$this->expiresAt = time() + $this->CI->config->config['jwt_expiresAfter'];

		// Set Registered Claims
		$this->token = (new Builder())
			->setIssuer($this->issuer) // set iss claim
			->setAudience($this->audience) // set aud claim
			->setIssuedAt(time()) // set the iat claim
			->setExpiration($this->expiresAt); // set the exp claim
		// Set Private Claims
		foreach($this->privateClaims as $privateClaimKey => $privateClaimValue){
			$this->token->set($privateClaimKey, $privateClaimValue);
		}

		// Sign the token
		$this->token->sign($this->signer, $this->secretKey);

		// Get the actual token object from builder
		$this->token = $this->token->getToken();

		return true;
	}

	public function setToken($tokenString){
		$this->token = (new Parser())->parse((string)$tokenString);
	}

	public function getClaims(){
		return $this->token->getClaims();
	}

	public function getClaim($claim){
		return $this->token->getClaim($claim);
	}

	public function hasClaim($claim){
		return $this->token->hasClaim($claim);
	}

	public function getToken(){
		if (!$this->token instanceof Token) return false;
		return (string)$this->token;
	}

	public function setCookie(){
		if(!headers_sent()){

			return setcookie(
				'token',
				$this->getToken(),
				time() + $this->CI->config->config['jwt_cookie_expire'],
				$this->CI->config->config['jwt_cookie_path'],
				'',
				$this->CI->config->config['jwt_cookie_secure']
			);

		} else {
			return false;
		}
	}

	public function validate(){
		if (!$this->token instanceof Token) return false;

		$data = new ValidationData();
		$data->setIssuer($this->CI->config->config['jwt_issuer']);
		$data->setAudience($this->CI->config->config['jwt_audience']);

		return ($this->token->verify($this->signer, $this->secretKey) && $this->token->validate($data));
	}

}