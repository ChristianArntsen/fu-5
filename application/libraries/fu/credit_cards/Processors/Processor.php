<?php
namespace fu\credit_cards\Processors;

abstract class Processor {

    /**
     * @var \CI_Controller
     */
    protected $CI;
    protected $CourseModel;
    protected $TerminalModel;

    public function __construct($Models = false)
    {
        $this->CI = &get_instance();

        if (!empty($Models['CourseModel'])) {
            $this->CourseModel = $Models['CourseModel'];
        }
        else {
            $this->CourseModel = $this->CI->load->model('Course');
        }

        if (!empty($Models['TerminalModel'])) {
            $this->TerminalModel = $Models['TerminalModel'];
        }
        else {
            $this->CI->load->model('Terminal');
            $this->TerminalModel = $this->CI->Terminal;
        }
    }

    abstract public function initiatePayment($payment_data, $terminal_id, $swipe);
    abstract public function processResponse($response);
    abstract public function voidPayment($credit_card_payment_id);
    abstract public function returnPayment($credit_card_payment_id, $terminal_id, $amount = false);
    abstract public function adjustTip($credit_card_payment_id, $terminal_id, $amount);
    abstract public function cardOnFileSale($params);
    abstract public function saveCardOnFile($params);
}