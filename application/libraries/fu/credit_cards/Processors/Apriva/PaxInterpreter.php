<?php
namespace fu\credit_cards\Processors\Apriva;

class PaxInterpreter {
    protected $version = '2.00';

    protected $command_string_helpers = array(
        'STX' => '02',
        'ETX' => '03',
        'LRC' => '00',
        'FS' => '28',
        'US' => '31'
    );
    protected $commands = array(
        // There are more than what is listed
        // Required elements are command, version, transaction type, amount info (for certain transaction), and trace info
        'credit' => array(
            'string' => 'T00',
            'format' => array(
                'STX',
                'command_type',
                'FS',
                'version',
                'FS',
                'transaction_type',
                'FS',
                'amount_info',
                'FS',
                'account_info',
                'FS',
                'trace_info',
                'FS',
                'avs_info',
                'FS',
                'cashier_info',
                'FS',
                'commercial_info',
                'FS',
                'moto',
                'FS',
                'additional_info',
                'ETX',
                'LRC'
            )
        ),
        'getvar' => array(
            'string' => 'A02',
            'format' => array(
                'STX',
                'command_type',
                'FS',
                'version',
                'FS',
                'edc_type',
                'FS',
                'variable_name',
                'ETX',
                'LRC'
            )
        ),
        'setvar' => array(
            'string' => 'A04',
            'format' => array(
                'STX',
                'command_type',
                'FS',
                'version',
                'FS',
                'edc_type',
                'FS',
                'variable_name',
                'FS',
                'variable_value',
                'ETX',
                'LRC'
            )
        ),
        'get_transaction' => array(
            'string' => 'R02',
            'format' => array(
                'STX',
                'command_type',
                'FS',
                'version',
                'FS',
                'edc_type',
                'FS',
                'transaction_type',
                'FS',
                'card_type',
                'FS',
                'record_no',
                'FS',
                'transaction_no',
                'FS',
                'auth_code',
                'FS',
                'ref_no',
                'ETX',
                'LRC'
            )
        ),
        'debit' => array(
            'string' => 'T02',
            'format' => array()),
        'signature' => array(
            'string' => 'A20',
            'format' => array()),
        'input_text' => array(
            'string' => 'A36',
            'format' => array())
    );
    protected $info_formats = array(
        'amount_info' => array(
            'trans_amount',
            'US',
            'tip_amount',
            'US',
            'cash_back_amount',
            'US',
            'merchant_fee',
            'US',
            'tax_amount',
            'US',
            'fuel_amount'
        ),
        'trace_info' => array(
            'ref_no',
            'US',
            'invoice_number',
            'US',
            'auth_code',
            'US',
            'transaction_number',
            'US',
            'time_stamp'
        ),
        'cashier_info' => array(
            'clerk_id',
            'US',
            'shift_id'
        ),
        'additional_info' => array(
            'table',
            'US',
            'guest',
            'US',
            'ticket'
        )
    );
    protected $edc_types = array(
        'all' => '00',
        'credit' => '01',
        'debit' => '02',
        'ebt' => '03',
        'gift' => '04',
        'loyalty' => '05',
        'cash' => '06',
        'check' => '07'
    );
    protected $transactions = array(
        // There are more than what is listed
        'menu' => '00',
        'sale' => '01',
        'return' => '02',
        'auth' => '03',
        'post_auth' => '04',
        'forced' => '05',
        'adjust' => '06',
        'void' => '17',
        'verify' => '24',
        'tokenize' => '32'
    );
    protected $errors = array(
        '000000' => 'Approved',
        '000100' => 'Declined',
        '100001' => 'Timed Out',
        '100002' => 'Aborted',
        '100003' => 'Invalid Parameters'
        // And more
    );
    public function generateCommandArray($params) {
        $command_array = array();
        $format_array = $this->getCommandFormat($params['command_type']);
        foreach($format_array as $component) {
            $this->addCharCodes($command_array, $component, $params);
        }

        return $command_array;
    }

    private function addCharCodes(&$command_array, $component, $params) {
        if (!empty($this->command_string_helpers[$component])) {
            $command_array[] = $this->command_string_helpers[$component];
        }
        else if (!empty($this->info_formats[$component])) {
            $format_array = $this->getInfoFormat($component);
            foreach($format_array as $info_component) {
                $this->addCharCodes($command_array, $info_component, $params);
            }
        }
        else if ($component == 'version') {
            foreach(str_split($this->version) as $character) {
                $command_array[] = (string) unpack("C", $character)[1];
            }
        }
        else if (!empty($params[$component])) {
            $value = $params[$component];

            if (!empty($this->commands[$value])) {
                $characters = $this->commands[$value]['string'];
            }
            else if (!empty($this->transactions[$value])) {
                $characters = $this->transactions[$value];
            }
            else if (!empty($this->edc_types[$value])) {
                $characters = $this->edc_types[$value];
            }
            else {
                $characters = $value;
            }

            foreach(str_split($characters) as $character) {
                $command_array[] = (string) unpack("C", $character)[1];
            }

        }
        else {
            //echo '(ignoring '.$component.')';
        }
    }

    private function getCommandFormat($command_type) {
        return $this->commands[$command_type]['format'];
    }

    private function getInfoFormat($info_type) {
        return $this->info_formats[$info_type];
    }
}