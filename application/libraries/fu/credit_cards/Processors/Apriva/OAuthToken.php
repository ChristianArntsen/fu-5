<?php
namespace fu\credit_cards\Processors\Apriva;

use fu\Curl\Curl;

class OAuthToken
{

    private $url = 'https://ws.accounts.apriva.com/o/1022/oauth2/token';
    private $username = 'foreup3';
    private $password = '95432f8d13724add8d8a4bf63b5107d6';
    private $product = 1022;

    private $access_uri = 'https://ws.api.apriva.com';
    private $db;

    public function setCredentials($params)
    {
        $this->username = $params['username'];
        $this->password = $params['password'];
        $this->product = $params['product'];
        $this->url = $params['url'];
        $this->access_uri = $params['access_uri'];

        $CI = get_instance();
        $this->db = $CI->db;
    }

    public function get()
    {
        $oAuthToken = $this->fromDatabase();
        if (empty($oAuthToken)) {
            $oAuthToken = $this->fromApriva();
        }

        return $oAuthToken;
    }

    private function save($oAuthToken)
    {
        $data = array(
            'oauth_token' => $oAuthToken,
            'apriva_username' => $this->username,
            'apriva_password' => $this->password,
            'apriva_product' => $this->product,
            'time_generated' => gmdate('Y-m-d H:i:s')
        );

        $this->db->insert('apriva_oauth_tokens', $data);
    }

    private function remove($oAuthTokenID)
    {
        $this->db->delete('apriva_oauth_tokens', array(
            'apriva_username' => $this->username,
            'apriva_password' => $this->password,
            'apriva_product' => $this->product
        ));
    }

    private function fromDatabase()
    {
        $this->db->from('apriva_oauth_tokens');
        $this->db->where('apriva_username', $this->username);
        $this->db->where('apriva_password', $this->password);
        $this->db->where('apriva_product', $this->product);
        $oauth_tokens = $this->db->get()->result_array();

        foreach($oauth_tokens as $token) {
            // Tokens expire after 60 minutes, so we'll expire them automatically 5 minutes early just to be safe
            if (date('Y-m-d H:i:s', strtotime($token['time_generated'] . ' + 55 minutes ')) < gmdate('Y-m-d H:i:s')) {
                $this->remove($token['oauth_token_id']);
            }
            else {
                return $token['oauth_token'];
            }
        }

        return false;
    }

    private function fromApriva()
    {
        $data = array(
            'grant_type' => 'client_credentials',
            'scope' => 'https://ws.api.apriva.com/auth/user https://ws.api.apriva.com/auth/hosted.payment',
            'timestamp' => time(),
            'access_uri' => $this->access_uri
        );

        $curl = new Curl();
        $curl->setBasicAuthentication($this->username, $this->password);
        $curl->setHeader('Content-Type', 'application/x-www-form-urlencoded');
        $curl->post($this->url, $data);

        $CI = get_instance();
        $terminal_id = $CI->session->userdata('terminal_id');

        if ($curl->error) {
            //TODO: Handle OAuth Token error
            $this->recordTransaction('OAuthToken', '', $terminal_id, $data, $curl, $this->url);
//            var_dump($curl);
//            echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage;
            return false;
        } else {
            //var_dump($curl->response);
            $this->recordTransaction('OAuthToken', '', $terminal_id, $data, $curl->response, $this->url);
            $this->save($curl->response->access_token);
            return $curl->response->access_token;
        }
    }

    public function recordTransaction($trans_type, $invoice, $terminal_id, $request, $response, $url = '') {
        $CI = get_instance();
        $course_id = $CI->config->item('course_id');
        $AprivaTransaction = new \fu\credit_cards\Processors\Apriva\AprivaTransaction();
        $api_data = array();
        $api_data['transaction_type'] = $trans_type;
        $api_data['invoice'] = $invoice;
        $api_data['response'] = json_encode($response);
        $api_data['request'] = json_encode($request);
        $api_data['terminal_id'] = $terminal_id;
        $api_data['course_id'] = $course_id;
        $api_data['date_time'] = gmdate('Y-m-d H:i:s');
        $api_data['url'] = $url;

        $AprivaTransaction->record($api_data);
    }
}