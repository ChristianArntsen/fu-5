<?php
namespace fu\credit_cards\Processors\Apriva;

use fu\Curl\Curl;
use PaxInterpreter;
use OAuthToken;
use fu\credit_cards\Processors\Processor;

class AprivaProcessor extends Processor  {
    protected $url = '';
    protected $testTransaction = false;
    protected $baseUrls = array(
        'live' => 'https://ws.api.apriva.com',
        'test' => 'https://aibapp53.aprivaeng.com:9467'
    );

    public function __construct($Models = false){
        parent::__construct($Models);
    }

    private function getPaymentCredentials($terminal_id = false) {
        if ($terminal_id) {
            $terminal_info = $this->TerminalModel->get_info($terminal_id);
        }
        if (!empty($terminal_info) && !empty($terminal_info[0]['apriva_username'])) {
            //var_dump($terminal_info);
            $paymentCredentials = array(
                'apriva_username' => $terminal_info[0]['apriva_username'],
                'apriva_password' => $terminal_info[0]['apriva_password'],
                'apriva_product' => $terminal_info[0]['apriva_product'],
                'apriva_key' => $terminal_info[0]['apriva_key']
            );
        }
        else {
            $paymentCredentials = array(
                'apriva_username' => $this->CI->config->item('apriva_username'),
                'apriva_password' => $this->CI->config->item('apriva_password'),
                'apriva_product' => $this->CI->config->item('apriva_product'),
                'apriva_key' => $this->CI->config->item('apriva_key')
            );
        }

        return $paymentCredentials;
    }

    public function initiatePayment($payment_data, $terminal_id, $swipe = true, $payment_purpose = 'sale')
    {
        $data = array();

        if ($swipe) {
            $this->CI->load->model('v2/Pax_device_model');
            $this->CI->Pax_device_model->openSwipeWindow($payment_data, $terminal_id);
        }
        else {
            //$this->testTransaction = true;
            $paymentCredentials = $this->getPaymentCredentials($terminal_id);
            $OAuthToken = $this->getOAuthToken($paymentCredentials);

            $params = array(
                //'Longitude' => '-111.926',
                //'Latitude' => '33.561',
                'InvoiceNumber' => $payment_data['ref_no'],
                'SubTotalAmount' => $payment_data['trans_amount'],
                'TaxAmount' => $payment_data['tax_amount'],
                'TipAmount' => $payment_data['tip_amount'],
                //'Emails[0]',//Not sure the name on this one
                //'Description',
                'Authorization' => 'Bearer '.$OAuthToken,
                'APK' => $paymentCredentials['apriva_key']
            );
            $data['payment_data'] = $payment_data;
            $data['url'] = $this->getUrl('manualTransaction', $params);
            $data['payment_purpose'] = $payment_purpose;
            $data['terminal_id'] = $terminal_id;

            // Initiate Manual Hosted Payment
            $this->CI->load->view('v2/credit_cards/AprivaHostedCheckout', $data);
        }
    }


    public function processResponse($response) {

    }

    public function voidPayment($credit_card_payment_id){
        // This happens in a request to the device

    }

    public function returnPayment($credit_card_payment_id, $terminal_id, $amount = false) {
        $AprivaTransaction = new AprivaTransaction();
        //$this->testTransaction = true;
        $paymentCredentials = $this->getPaymentCredentials($terminal_id);
        // Fetch credit card payment info
        $cc_info = $AprivaTransaction->get($credit_card_payment_id);

        if (empty($cc_info['token'])) {
            $token = $this->saveCardOnFile($credit_card_payment_id);
            $cc_info['token'] = $token;
        }

        if (!empty($amount)) {
            $cc_info['auth_amount'] = $amount;
        }

        $params = $this->getTransactionParams($credit_card_payment_id, 'refund', $cc_info);
        $url = $this->getUrl('refund', $params);
        $data = $this->getRequestLoad('refund', $params);
        $OAuthToken = $this->getOAuthToken($paymentCredentials);

        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('APK', $paymentCredentials['apriva_key']);
        $curl->setHeader('Authorization', 'Bearer '.$OAuthToken);

        $response = $curl->post($url, $data);
        $this->recordTransaction('return', $credit_card_payment_id, $terminal_id, $data, $response);

        if (isset($response->Result->ResponseCode) && $response->Result->ResponseCode == '0') {
            $success = true;
        }
        else {
            $success = false;
        }
        return array('success'=>$success);
    }

    public function recordTransaction($trans_type, $invoice, $terminal_id, $request, $response) {
        $course_id = $this->CI->config->item('course_id');
        $AprivaTransaction = new \fu\credit_cards\Processors\Apriva\AprivaTransaction();
        $api_data = array();
        $api_data['transaction_type'] = $trans_type;
        $api_data['invoice'] = $invoice;
        $api_data['response'] = json_encode($response);
        $api_data['request'] = json_encode($request);
        $api_data['terminal_id'] = $terminal_id;
        $api_data['course_id'] = $course_id;
        $api_data['date_time'] = gmdate('Y-m-d H:i:s');

        $AprivaTransaction->record($api_data);
    }

    public function adjustTip($credit_card_payment_id, $terminal_id, $amount) {
        // Fetch credit card payment info
        $AprivaTransaction = new AprivaTransaction();
        $cc_info = $AprivaTransaction->get($credit_card_payment_id);

        // This happens in a request to the device
        $params = array(
            'command_type' => 'credit',
            'transaction_type' => 'adjust',
            'trans_amount' => number_format($amount,2),//$total,
            'tax_amount' => '0',
            'tip_amount' => '0',
            'ref_no' => $credit_card_payment_id,
            'transaction_number' => $cc_info['acq_ref_data']
        );
        $this->CI->load->model('v2/Pax_device_model');
        $this->CI->Pax_device_model->adjustTip($params, $terminal_id);
    }

    public function saveCardOnFile($credit_card_payment_id) {
        $AprivaTransaction = new AprivaTransaction();
        $cc_info = $AprivaTransaction->get($credit_card_payment_id);
        //$this->testTransaction = true;
        $terminal_id = $this->CI->session->userdata('terminal_id');

        //https://developer.apriva.com/transactions/card-on-file-transactions/add-card-on-file-with-transaction-id
        $paymentCredentials = $this->getPaymentCredentials($terminal_id);
        $params = $this->getTransactionParams($credit_card_payment_id, 'addCardOnFile', $cc_info);
        $OAuthToken = $this->getOAuthToken($paymentCredentials);

        $url = $this->getUrl('addCardOnFile', $params);

        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('APK', $paymentCredentials['apriva_key']);
        $curl->setHeader('Authorization', 'Bearer '.$OAuthToken);

        $response = $curl->get($url);

        $this->recordTransaction('addCardOnFile', $credit_card_payment_id, $terminal_id, 'Authorization: Bearer '.$OAuthToken.' --- URL: '.$url, $response);

        if (empty($response->CardOnFileToken)) {
            return false;
        }
        else {
            $AprivaTransaction = new AprivaTransaction();
            $AprivaTransaction->update($credit_card_payment_id, array('token' => $response->CardOnFileToken));

            return $response->CardOnFileToken;
        }
    }

    public function cardOnFileSale($params) {
        //https://developer.apriva.com/transactions/card-on-file-transactions/run-card-on-file
        $terminal_id = $this->CI->session->userdata('terminal_id');
        $paymentCredentials = $this->getPaymentCredentials($terminal_id);
        $OAuthToken = $this->getOAuthToken($paymentCredentials);
        $url = $this->getUrl('cardOnFileSale', $params);
        $data = $this->getRequestLoad('cardOnFileSale', $params);


        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('APK', $paymentCredentials['apriva_key']);
        $curl->setHeader('Authorization', 'Bearer '.$OAuthToken);

        $response = $curl->post($url, $data);

    }

    private function getUrl($type, $params) {
        $baseUrl = $this->getBaseUrl($this->testTransaction);

        switch ($type) {
            case 'cardOnFileSale':
                return $baseUrl.'/pay/v1/payments/credit/sale';
                break;
            case 'addCardOnFile':
                return $baseUrl.'/cardonfile/v1/cards?TransactionID='.$params['TransactionID'];
                break;
            case 'refund':
                return $baseUrl.'/pay/v1/payments/credit/refund';
                break;
            case 'getOAuthToken':
                return $baseUrl.'/o/'.$params['productID'].'/oauth2/token';
                break;
            case 'manualTransaction':
                return $baseUrl.'/pay/v1/webpages/manualcreditsale?InvoiceNumber='.$params['InvoiceNumber'].
                    '&SubTotalAmount='.$params['SubTotalAmount'].'&TaxAmount='.$params['TaxAmount'].
                    '&TipAmount='.$params['TipAmount'].'&Authorization='.urlencode($params['Authorization']).'&APK='.$params['APK'];
                break;
            default:
                return;
        }
    }

    private function getBaseURL($test = false) {
        if ($test) {
            return $this->baseUrls['test']; }
        else {
            return $this->baseUrls['live']; }
    }

    private function getTransactionParams($credit_card_payment_id, $type, $cc_info) {
        switch ($type) {
            case 'cardOnFileSale':
                return ;
                break;
            case 'addCardOnFile':
                $params = array(
                    'TransactionID' => $cc_info['payment_id']
                );
                return $params;
                break;
            case 'manualTransaction':
                return ;
                break;
            case 'refund':
                $params = array(
                    'HostTransactionID' => $cc_info['payment_id'],
                    'UniqueIdentifier' => $credit_card_payment_id,
                    'TotalAmount' => $cc_info['auth_amount'],
                    'SubTotalAmount' => $cc_info['auth_amount'],
                    'TaxAmount' => 0,
                    'TipAmount' => 0,
                    'Token' => $cc_info['token']
                );

                return $params;
                break;
            case 'getOAuthToken':
                return ;
                break;
            default:
                return;
        }
    }

    public function getRequestLoad($type, $params) {
        if ($type == 'cardOnFileSale') {
            return  array(
                'TransactionData' => array(
                    'UniqueIdentifier' => '', //Required
                    'TimeStamp' => '', //Required
                    'TotalAmount' => '', //Required
                    'Description' => null,
                    'DeviceRegistrationIdentifier' => '',
                    'InvoiceNumber' => ''
                ),
                'AmountsReq' => array(
                    'SubTotalAmount' => '', //Required
                    'TaxAmount' => '', //Required
                    'TipAmount' => '' //Required
                ),
                'PaymentData' => array(
                    'EntryMethod' => 'CardOnFile', //Required
                    'CardData' => array(
                        'ManualCardData' => null,
                        'SensitiveCardData' => null,
                        'ProtectedCardData' => null,
                        'CardOnFileData' => array(
                            'Token' => '' //Required
                        )
                    )
                ),
                'Signature' => null,
                'AdditionalData' => null
            );
        }
        else if ($type == 'refund') {
            return array(
                'PaymentData' => array(
                    'EntryMethod' => 'Standard',
                    'CardData' => array(
                        'ManualCardData' => null,
                        'SensitiveCardData' => null,
                        'ProtectedCardData' => null,
                        'CardOnFileData' => array(
                            'Token' => $params['Token'] //Required
                        )
                    ),
                    'EmvCapable' => false
                ),
                'OriginalTransactionData' => array(
                    'HostTransactionID' => $params['HostTransactionID'] //Required
                ),
                'TransactionData' => array(
                    'UniqueIdentifier' =>  $params['UniqueIdentifier'], //Required
                    'TimeStamp' =>  date('Y-m-d\TH:i:s.u'), //Required
                    'TotalAmount' =>  $params['TotalAmount'], //Required
                    'Description' => '',
                    'InvoiceNumber' => '',
                    'DeviceRegistrationIdentifier' => ''
                ),
                'AmountsReq' => array(
                    'SubTotalAmount' =>  $params['SubTotalAmount'], //Required
                    'TaxAmount' =>  $params['TaxAmount'], //Required
                    'TipAmount' =>  $params['TipAmount'], //Required
                    'SurchargeAmount' => '0.00'
                )
            );
        }
    }
    public function getOAuthToken($paymentCredentials){
//        $data = array(
//            'url' => 'https://aibapp53.aprivaeng.com:9464/o/1022/oauth2/token', // fetch
//            'username' => 'foreup3',// From terminal
//            'password' => '95432f8d13724add8d8a4bf63b5107d6', // from terminal
//            'product' => '1022',// from terminal
//            'access_uri' => 'https://aibapp53.aprivaeng.com:9467' // fetch
//        );
        $data = array(
            'url' => 'https://ws.accounts.apriva.com/o/'.$paymentCredentials['apriva_product'].'/oauth2/token', // fetch
            'username' => $paymentCredentials['apriva_username'],
            'password' => $paymentCredentials['apriva_password'],
            'product' => $paymentCredentials['apriva_product'],
            'access_uri' => 'https://ws.api.apriva.com' // fetch
        );

        $OAuthToken = new \fu\credit_cards\Processors\Apriva\OAuthToken();
        $OAuthToken->setCredentials($data);
        $token =  $OAuthToken->get();

        return $token;
    }
}