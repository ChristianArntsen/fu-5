<?php
namespace fu\credit_cards\Processors\Apriva;

class AprivaTransaction
{
    function record($data) {
        $CI = get_instance();
        $CI->db->close();
        $CI->db->insert('apriva_transactions', $data);
    }

    function update($invoice_id, $data) {
        $CI = get_instance();
        $allowed = ['course_id','tran_type','amount','auth_amount','card_type','masked_account','cardholder_name','ref_no','operator_id',
            'terminal_name','trans_post_time','auth_code','payment_id','acq_ref_data','process_data','token','token_used','amount_refunded',
            'response_code','status','status_message','display_message','gratuity_amount','voided','cart_id','success_check_count','cancel_time',
            'emv_payment'];
        $data = array_intersect_key($data, array_flip($allowed));

        $CI->load->model('credit_card_payments');
        $CI->credit_card_payments->update_credit_card_payment($invoice_id, $data);
    }

    function get($credit_card_payment_id) {
        $CI = get_instance();
        $CI->load->model('credit_card_payments');

        return $CI->credit_card_payments->get_info($credit_card_payment_id);
    }


}