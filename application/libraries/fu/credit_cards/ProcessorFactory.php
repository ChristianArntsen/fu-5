<?php

namespace fu\credit_cards;

class ProcessorFactory {

    public static function create($processor_type,$input)
    {
        if($processor_type == "apriva"){
            return new Processors\Apriva\AprivaProcessor($input);
        } else {
            throw new \Exception("Invalid Processor Type");
        }

    }

}