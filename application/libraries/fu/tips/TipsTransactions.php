<?php
// TipsTransactions.php
// Specializing in Recording Sale transactions relating to tips
namespace fu\tips;

class TipsTransactions extends \fu\accounting\Transactions {
	private $journal_entries; // cache for journal entries array(transaction_id=>journal_entry array ...)

	function __construct($table = 'tip_transactions')
	{
		$this->journal_entries = array();
		parent::__construct($table);
	}

	public function post($sale_id,$employee_id,$transaction_time = null,$notes = null,$comment = null,$validate_only = false){
		$transaction_id = false;
		// required
		// sale_id, employee_id
		if(!is_numeric($sale_id) || !is_int($sale_id*1)){
			$this->last_error = 'TipsTransactions->post Error: sale_id is required, and must be an integer.';
			return false;
		}
		if(!is_numeric($employee_id) || !is_int($employee_id*1)){
			$this->last_error = 'TipsTransactions->post Error: employee_id is required, and must be an integer.';
			return false;
		}
		// end required
		$params = array('reference_id'=>$sale_id,'employee_id'=>$employee_id);

		// optional
		if(isset($transaction_time)){
			$parsed_time = strtotime($transaction_time);
			if(!$parsed_time){
				$this->last_error = 'TipsTransactions->post Error: invalid transaction_time provided.';
				return false;
			}
			$params['transaction_time'] = $transaction_time;
		}
		if(isset($notes)&&!is_string($notes)){
			$this->last_error = 'TipsTransactions->post Error: notes parameter must be a string.';
			return false;
		}elseif (isset($notes)){
			$params['notes'] = $notes;
		}
		if(isset($comment)&&!is_string($comment)){
			$this->last_error = 'TipsTransactions->post Error: comment parameter must be a string.';
			return false;
		}elseif (isset($comment)){
			$params['comment'] = $comment;
		}

		if($validate_only)return true;

		$success = false;
		try {
			$success = $this->CI->db->insert($this->table, $params);
		}catch(\Exception $e){
			$this->last_error = 'TableOfAccounts->post Error: '.$e->getMessage();
			return false;
		}

		if($success) {
			return $this->CI->db->insert_id();
		}
		else{
			$this->last_error = 'TableOfAccounts->post Error: '.$this->CI->db->_error_message();
		}

		return false;
	}

	public function put($sale_id,$employee_id, $transaction_id = null, $transaction_time = null,$notes = null,$comment = null,$validate_only = false){
		// required
		// sale_id, employee_id
		if(!is_numeric($sale_id) || !is_int($sale_id*1)){
			$this->last_error = 'TipsTransactions->put Error: sale_id is required, and must be an integer.';
			return false;
		}
		if(!is_numeric($employee_id) || !is_int($employee_id*1)){
			$this->last_error = 'TipsTransactions->put Error: employee_id is required, and must be an integer.';
			return false;
		}
		// end required
		$updating = false;
		$params = array('reference_id'=>$sale_id,'employee_id'=>$employee_id,'notes'=>null,'comment'=>null);

		// optional parameters
		if(isset($transaction_id)&&(!is_numeric($transaction_id)||!is_int($transaction_id*1))){
			$this->last_error = 'TipsTransactions->put Error: transaction_id must be an integer.';
			return false;
		}elseif (isset($transaction_id)){
			$constraints['transaction_id'] = $transaction_id;
			$updating = true;
		}

		if(isset($transaction_time)){
			$parsed_time = strtotime($transaction_time);
			if(!$parsed_time){
				$this->last_error = 'TipsTransactions->put Error: invalid transaction_time provided.';
				return false;
			}
		}

			$temp = $params;
			if(isset($temp['reference_id'])){
				$temp['sale_id']=$params['reference_id'];
			}
			unset($temp['notes']);
			unset($temp['comment']);

			$existing = $this->get($temp,true);
			if(is_array($existing) && count($existing)===1){
				// we have a unique key
				$updating = true;
				$constraints = $params;
				unset($constraints['notes']);
				unset($constraints['comment']);
				if(isset($transaction_id)&& $transaction_id!==$existing[0]['transaction_id']){
					$this->last_error = 'TipsTransactions->put Error: you appear to be trying to update a transaction '.
						'to be identical to an existing transaction. This will never work.';
					return false;
				}
			}elseif(!isset($transaction_id)){
				return $this->post($sale_id,$employee_id,$transaction_time,$notes,$comment,$validate_only);
			}
		if(isset($notes)&&!is_string($notes)){
			$this->last_error = 'TipsTransactions->put Error: notes parameter must be a string.';
			return false;
		}elseif (isset($notes)){
			$params['notes'] = $notes;
		}
		if(isset($comment)&&!is_string($comment)){
			$this->last_error = 'TipsTransactions->put Error: comment parameter must be a string.';
			return false;
		}elseif (isset($comment)){
			$params['comment'] = $comment;
		}
		// end simple validation

		if($validate_only)return true;

		// should un-delete
		$params['deleted'] = 0;

		// this looks to be consistent/usable for all put and patch requests
		// method for parent class?
        $update_id = $transaction_id;
		$success = false;
		if(count($constraints)) {
			try {
				$success = $this->CI->db->update($this->table, $params, $constraints);
			} catch (\Exception $e) {
				$this->last_error = 'TableOfAccounts->put Error: ' . $e->getMessage();
				return false;
			}
		}


		if($success) {
			return $update_id;
		}
		return false;
	}

	public function patch($parameters,$validate_only = false){
		$transaction_id = false;
		$constraints = array();
		// transaction id, or the combination of sale_id, employee_id and transaction time are required
		if(!isset($parameters['transaction_id']) &&
		(!isset($parameters['sale_id'])||!isset($parameters['employee_id'])||!isset($parameters['transaction_time']))){
			$this->last_error = 'TipsTransactions->patch Error: transaction_id or combination of sale_id, employee_id and transaction_time are required.';
			return false;
		}

		if(isset($parameters['transaction_id']) && (!is_numeric($parameters['transaction_id']) || !is_int($parameters['sale_id']*1))){
			$this->last_error = 'TipsTransactions->patch Error: transaction_id must be an integer.';
			return false;
		}elseif (isset($parameters['transaction_id'])){
			$transaction_id = $constraints['transaction_id'] = $parameters['transaction_id'];

		}

		if(isset($parameters['sale_id']) && (!is_numeric($parameters['sale_id']) || !is_int($parameters['sale_id']*1))){
			$this->last_error = 'TipsTransactions->patch Error: sale_id must be an integer.';
			return false;
		}

		if(isset($parameters['employee_id']) && (!is_numeric($parameters['employee_id']) || !is_int($parameters['employee_id']*1))){
			$this->last_error = 'TipsTransactions->patch Error: employee_id is required, and must be an integer.';
			return false;
		}

		if(isset($parameters['transaction_time'])){
			$parsed_time = strtotime($parameters['transaction_time']);
			if(!$parsed_time){
				$this->last_error = 'TipsTransactions->patch Error: invalid transaction_time provided.';
				return false;
			}
		}

		if(isset($parameters['sale_id'])&&isset($parameters['employee_id'])&&isset($parameters['transaction_time'])){
			$existing = $this->get(array('employee_id'=>$parameters['employee_id'],'sale_id'=>$parameters['sale_id'],'transaction_time'=>$parameters['transaction_time']),true);
			if(is_array($existing) && count($existing)===1){
				// we have a unique key
				$updating = true;
				$constraints = array('employee_id'=>$parameters['employee_id'],'reference_id'=>$parameters['sale_id'],'transaction_time'=>$parameters['transaction_time']);

				// catch unique key violation early
				if(isset($parameters['transaction_id'])&& $parameters['transaction_id']!==$existing[0]['transaction_id']){
					$this->last_error = 'TipsTransactions->patch Error: you appear to be trying to update a transaction '.
					'to be identical to an existing transaction. This will never work.';
					return false;
				}
			}elseif (!isset($parameters['transaction_id'])){
				$this->last_error = 'TipsTransactions->patch Error: transaction does not exist to update.';
				return false;
			}
		}

		if(count($constraints)<1){
			throw new \Exception('TipsTransactions->patch UserError: un-constrained update should not happen.');
		}

		if(isset($parameters['notes'])&&!is_string($parameters['notes'])){
			$this->last_error = 'TipsTransactions->patch Error: notes parameter must be a string.';
			return false;
		}
		if(isset($parameters['comment'])&&!is_string($parameters['comment'])){
			$this->last_error = 'TipsTransactions->patch Error: comment parameter must be a string.';
			return false;
		}
		if(isset($parameters['sale_id'])&&!isset($parameters['reference_id'])) {
			$parameters['reference_id'] = $parameters['sale_id'];
			unset($parameters['sale_id']);
		}

		if($validate_only)return true;
		$update_id = $transaction_id;

		try {
			$success = $this->CI->db->update($this->table, $parameters, $constraints);
		}
		catch(\Exception $e){
			$this->last_error = 'TipsTransactions->patch Error: '.$e->getMessage();
			return false;
		}

		if($success) {
			return $update_id;
		}

		return false;
	}

	public function delete($transaction_id, $hard = false, $validate_only = false){
		if(!is_numeric($transaction_id)||!is_int($transaction_id*1)){
			$this->last_error = 'TipsTransactions->delete Error: transaction_id is required, and must be an integer.';
			return false;
		}

		if($validate_only)return true;

		$success = false;
		try {
			if ($hard) {
				$this->CI->db->where('transaction_id', $transaction_id);
				$success = $this->CI->db->delete($this->table);
			} else {
				$success = $this->CI->db->update($this->table, array('deleted' => 1), array('transaction_id'=>$transaction_id));
			}
		}
		catch(\Exception $e){
			$this->last_error = 'TipsTransactions->delete Error: '.$e->getMessage();
			return false;
		}
		if($success) {
			return $this->CI->db->affected_rows();
		}
		return false;
	}

	public function get($parameters,$inc_deleted = false, $validate_only = false){
		$ret = array();
		$transaction_id = false;
		$sale_id = false;
		$employee_id = false;
		$transaction_time = false;
		$notes = false;

		if(empty($parameters) || (!$this->validate_id($parameters) && !is_array($parameters))){
			$this->last_error = 'TipsTransactions->get Error: expecting transaction_id integer or parameters array.';
			return false;
		}
		elseif (is_numeric($parameters)){
			// "parameters" is really the transaction_id
			if(!$this->validate_id($parameters)){
				$this->last_error = 'TipsTransactions->get Error: transaction_id must be an integer.';
				return false;
			}
			$transaction_id = $parameters*1;
		}
		else{
			// parameters is an array
			if(isset($parameters['transaction_id']) && !$this->validate_id($parameters['transaction_id'])){
				$this->last_error = 'TipsTransactions->get Error: transaction_id must be an integer.';
				return false;
			}elseif (isset($parameters['transaction_id'])){
				$transaction_id = $parameters['transaction_id'];
			}

			if(isset($parameters['sale_id']) && !$this->validate_id($parameters['sale_id'])){
				$this->last_error = 'TipsTransactions->get Error: sale_id must be an integer.';
				return false;
			}elseif (isset($parameters['sale_id'])){
				$sale_id = $parameters['sale_id'];
			}

			if(isset($parameters['employee_id']) && !$this->validate_id($parameters['employee_id'])){
				$this->last_error = 'TipsTransactions->get Error: employee_id must be an integer.';
				return false;
			}elseif (isset($parameters['employee_id'])){
				$employee_id = $parameters['employee_id'];
			}

			if(isset($parameters['transaction_time'])){
				$parsed_time = strtotime($parameters['transaction_time']);
				if(!$parsed_time){
					$this->last_error = 'TipsTransactions->get Error: invalid transaction_time provided.';
					return false;
				}
				$transaction_time = $parameters['transaction_time'];
			}

			if(isset($parameters['notes'])&&!is_string($parameters['notes'])){
				$this->last_error = 'TipsTransactions->get Error: notes parameter must be a string.';
				return false;
			}elseif (isset($parameters['notes'])){
				$notes = $parameters['notes'];
			}
		}

		if($validate_only)return true;

		$this->CI->db->select()->from($this->table);
		if(!$inc_deleted){
			$this->CI->db->where('deleted',0);
		}
		if($transaction_id) {
			$this->CI->db->where('transaction_id', $transaction_id);
		}
		if($sale_id){
			$this->CI->db->where('reference_id',$sale_id);
		}
		if($employee_id){
			$this->CI->db->where('employee_id',$employee_id);
		}
		if($transaction_time) {
			$this->CI->db->where('transaction_time', $transaction_time);
		}
		if($notes) {
			$this->CI->db->where('notes', $notes);
		}
		try {
			$get = $this->CI->db->get();
			$ret = $get->result_array();
			foreach($ret as &$row){
				$row['sale_id'] = $row['reference_id'];
				unset($row['reference_id']);
			}
		}
		catch (\Exception $e) {
			$this->last_error = 'TableOfAccounts->get Error: ' . $e->getMessage();
			return false;
		}
		return $ret;

	}
}
