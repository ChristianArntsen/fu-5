<?php
// class for generating report data for Tips Journal
// given a date range. Returns either JSON to feed a model or HTML
namespace fu\tips;

class TipsJournalReport extends TipsJournal {

	private $transaction_table;
	private $start_time;
	private $end_time;
	private $employee_person_id;
	private $last_data;

	function __construct($table = 'tip_journal',$transaction_table = 'tip_transactions')
	{
		parent::__construct($table);
		$this->transaction_table = $transaction_table;
	}

	public function json($start_time = null,$end_time = null,$employee_person_id = null,$tip_sharing = false){

		if(!isset($employee_person_id))$employee_person_id = $this->employee_person_id;
		$CI = get_instance();
		$CI->db->select('tr.reference_id sale_id,tr.transaction_time, tr.notes transaction_notes, jr.*');

		$this->query_body($CI->session->userdata('course_id'),$employee_person_id,$start_time,$end_time);
		if($tip_sharing) {
			// this is needed for the employee tip sharing. Will break report stuff though...
			$CI->db->where('emp.person_id', $CI->session->userdata('person_id'));
		}
		$CI->db->order_by('transaction_time,d_c ASC, amount DESC');

		try{
			$get = $this->CI->db->get();
			$ret = $get->result_array();
		}
		catch (\Exception $e) {
			$this->last_error = 'TipsJournalReport->json Error: ' . $e->getMessage();
			return false;
		}
		return $ret;
	}

	public function get_tips_due_to_employee($course_id = null, $employee_person_id = null,$start_time = null,$end_time = null){
		return $this->get_employee_credit_total() - $this->get_employee_debit_total();
	}

	public function get_employee_debit_total($course_id = null, $employee_person_id = null,$start_time = null,$end_time = null){
		$CI = get_instance();
		$CI->db->select('sum(jr.amount) as amount');
		$this->query_body($course_id,$employee_person_id,$start_time,$end_time);
		$CI->db->where('jr.d_c', 'debit');
		try{
			$get = $this->CI->db->get();
			$ret = $get->result_array();
		}
		catch (\Exception $e) {
			$this->last_error = 'TipsJournalReport->get_employee_debit_total Error: ' . $e->getMessage();
			return false;
		}
		if(!count($ret))return 0.00;
		return $ret[0]['amount'];
	}

	public function get_employee_credit_total($course_id = null, $employee_person_id = null,$start_time = null,$end_time = null){
		$CI = get_instance();
		$CI->db->select('sum(jr.amount) as amount');
		$this->query_body($course_id,$employee_person_id,$start_time,$end_time);
		$CI->db->where('jr.d_c', 'credit');

		try{
			$get = $this->CI->db->get();
			$ret = $get->result_array();
		}
		catch (\Exception $e) {
			$this->last_error = 'TipsJournalReport->get_employee_credit_total Error: ' . $e->getMessage();
			return false;
		}
		if(!count($ret))return 0.00;
		return $ret[0]['amount'];
	}

	public function query_body($course_id = null, $employee_person_id = null,$start_time = null,$end_time = null){
		$CI = get_instance();
		if(!isset($start_time))$start_time = $this->start_time;
		if(!isset($end_time))$end_time = $this->end_time;
		if(!isset($employee_person_id))$employee_person_id = $this->employee_person_id;
		if(!isset($course_id))$course_id = $CI->session->userdata('course_id');


		$CI->db->from($this->table.' as jr')
			->join($this->transaction_table.' as tr', 'tr.transaction_id = jr.transaction_id')
			->join('employees as emp', 'emp.person_id = tr.employee_id')
			->join('sales as s','s.sale_id = tr.reference_id')
			->where('emp.course_id',$course_id)
			->where('tr.transaction_time >= ',$start_time)
			->where('tr.transaction_time < ', $end_time)
			->where('tr.deleted',0)
			->where('jr.deleted',0)
			->where('s.deleted',0)
			->where('emp.deleted',0);
		if(isset($employee_person_id)) {
			$CI->db->join('employees as emp2', 'emp2.person_id = jr.employee_id')
				->where('emp2.person_id', $employee_person_id * 1)
				->where('emp2.deleted',0);
		}
	}

	// implementing old report interface
	public function setParams($start_time,$end_time = null,$employee_person_id = null){
		if(is_array($start_time)){
			$end_time = $start_time['end_date'];
			$employee_person_id = $start_time['employee_id'];
			$start_time = $start_time['start_date'];
		}
		$this->start_time = $start_time;
		$this->end_time = $end_time;
		$this->employee_person_id = isset($employee_person_id) && $employee_person_id?$employee_person_id:null;
	}

	public function getDataColumns(){
		return array(
			array('data'=>'Sale ID','align'=>'left'),
			array('data'=>'Date','align'=>'left'),
			array('data'=>'Account','align'=>'left'),
			array('data'=>'Debit','align'=>'right'),
			array('data'=>'Credit','align'=>'right'),
			array('data'=>'Comment','align'=>'left')
		);
	}

	public function getSummaryData(){
		$total = 0.00;
		$credits = 0.00;
		$debits = 0.00;
		if(!$this->last_data){
			$this->getData();
		}

		foreach($this->last_data as $row){
			if($row['d_c']==='credit'){
				$total += $row['amount'];
				$credits += $row['amount'];
			}else{
				$debits += $row['amount'];
			}
		}

		return array(
			'total' => $total,
			'credits' => $credits,
			'debits' => $debits
		);
	}

	public function getTabularData(){
		$report_data = $this->getData();
		$tabular_data = array();
		if($report_data)
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>'POS '.$row['sale_id'], 'align'=> 'left'),array('data'=>$row['transaction_time'], 'align'=> 'left'), array('data'=>$row['notes'], 'align'=> 'left'),array('data'=>($row['d_c']==='debit'?to_currency($row['amount']):''), 'align'=> 'right'),array('data'=>($row['d_c']==='credit'?to_currency($row['amount']):''), 'align'=> 'right'),array('data'=>$row['comment'],'align'=>'left'));
		}
		return $tabular_data;
	}

	public function getData($tip_sharing=false){
		$this->last_data = $this->json(null,null,null,$tip_sharing);
		return $this->last_data;
	}
	// End implementing old report interface
}
?>