<?php
/**
 * Created by PhpStorm.
 * User: Brendon
 * Date: 9/29/2015
 * Time: 4:33 PM
 */

namespace fu\invoices;


class pdfGenerator {

    protected $email_pdfs_to_course,$pdf_download_url,$pdf_aws_download_url,$invoice_zip_cc,$course_id,$date;

    public function __construct()
    {
        ini_set('max_execution_time', 1500);
        $this->CI = & get_instance();
        $this->CI->load->model("Course");
        $this->CI->load->model("Invoice");
        $this->CI->load->helper('download');
        $this->CI->load->helper('file');
        $this->CI->load->library('zip');
        $this->CI->load->library('Html2pdf');
    }

    public function generatePDFs()
    {
        $cron_output = "";

        echo "Beginning PDF Generation<br/>";
        $billed_courses = $this->CI->Invoice->get_billed_courses($this->date, $this->course_id);

        $pdf_zip_sent = 0;
        if(empty($billed_courses)){
            return "";
        }
        if(count($billed_courses) > 1){
            return "";
        }

        $course = $billed_courses[0];

        $this->CI->session->set_userdata('course_id', $course['course_id']);
        $pdf_zip_url = $this->CI->Invoice->generate_pdfs($course['course_id'], array('date'=>$this->date, 'recurring'=>1));
        $pdf_aws_zip_url = $this->pdf_aws_download_url.'/'.$pdf_zip_url;
        $pdf_mobile_zip_url = $this->pdf_download_url.'/'.$pdf_zip_url;

        if($this->email_pdfs_to_course && !empty($pdf_zip_url)){
            $this->emailPdf($course, $pdf_mobile_zip_url, $pdf_aws_zip_url);
            $pdf_zip_sent++;
        }


        $cron_output .= 'PDF zip files sent: '.
            $pdf_zip_sent.
            ' ('.$this->CI->benchmark->elapsed_time('generate_pdfs_start', 'generate_pdfs_stop').' seconds)'.
            PHP_EOL;

        return $cron_output;
    }


    /**
     * @param $course
     * @param $pdf_zip_url
     * @return mixed
     */
    public function emailPdf($course = false,$pdf_zip_url = false,$pdf_aws_zip_url = false)
    {
        if(!$course){
            $billed_courses = $this->CI->Invoice->get_billed_courses($this->date, $this->course_id);
            if(empty($billed_courses)){
                return "";
            }
            if(count($billed_courses) > 1){
                return "";
            }

            $course = $billed_courses[0];
        }
        if(!$pdf_zip_url){
            $pdf_zip_url =  $this->pdf_download_url."/archives/invoices/{$this->course_id}";
            $pdf_zip_url = $pdf_zip_url."/invoices_".date('m-d-Y', strtotime($this->date)).".zip";
        }
        if (!$pdf_aws_zip_url) {
            $pdf_aws_zip_url =  $this->pdf_aws_download_url."/archives/invoices/{$this->course_id}";
            $pdf_aws_zip_url = $pdf_aws_zip_url."/invoices_".date('m-d-Y', strtotime($this->date)).".zip";
        }

        // Build email to send to course with download link
        $course_email_content = "<p>{$course['name']}</p>
                        <p>Invoice generation has completed.</p>
                        <p>You can download your invoices at<br/><br/>
                            <a href='{$pdf_aws_zip_url}'>{$pdf_aws_zip_url}</a>
                        </p>";

        $charge_report = $this->CI->Invoice->get_charge_report($course['course_id'], $this->date);

        if ($charge_report['num_success'] > 0 || $charge_report['num_failed'] > 0) {
            $course_email_content .= "<hr />
                            <h3>Credit Card Charge Report</h3>
                            Successful Charges: {$charge_report['num_success']}
                            <br />Failed Charges: {$charge_report['num_failed']}";

            if (!empty($charge_report['failed_charges'])) {
                $course_email_content .= "<br /><br />
                                <strong>Failed Charges</strong>
                                <ul>";

                foreach ($charge_report['failed_charges'] as $charge) {
                    $course_email_content .= "<li>
                                        <strong>Invoice #{$charge['invoice_number']}</strong> {$charge['first_name']} {$charge['last_name']}
                                         - {$charge['masked_account']} ({$charge['card_type']}) for " . to_currency($charge['due']) . "
                                    </li>";
                }
                $course_email_content .= "</ul>";
            }
        }

        // Get emails to send invoice bundle to
        $emails = array();
        if (is_array($this->invoice_zip_cc)) {
            $emails = $this->invoice_zip_cc;
        }
        else {
            $emails[] = $this->invoice_zip_cc;
        }
        $emails[] = $course['email'];

        if (!empty($course['billing_email'])) {
            $billing_emails = explode(',', $course['billing_email']);
            $emails = array_merge($emails, $billing_emails);
        }
        $this->CI->Course->date_stamp_sent_invoices($course['course_id']);
        // Send email to course
        echo 'Sending Email Now<br/>';
        send_sendgrid($emails, "Invoices Generated - " . $course['name'], $course_email_content, 'no-reply@foreup.com', 'no-reply@foreup.com');
        return $course;
    }

    /**
     * @return mixed
     */
    public function getEmailPdfsToCourse()
    {
        return $this->email_pdfs_to_course;
    }

    /**
     * @param mixed $email_pdfs_to_course
     */
    public function setEmailPdfsToCourse($email_pdfs_to_course)
    {
        $this->email_pdfs_to_course = $email_pdfs_to_course;
    }

    /**
     * @return mixed
     */
    public function getPdfDownloadUrl()
    {
        return $this->pdf_download_url;
    }

    /**
     * @param mixed $pdf_download_url
     */
    public function setPdfDownloadUrl($pdf_download_url)
    {
        $this->pdf_download_url = $pdf_download_url;
    }

    /**
     * @return mixed
     */
    public function getPdfAWSDownloadUrl()
    {
        return $this->pdf_aws_download_url;
    }

    /**
     * @param mixed $pdf_aws_download_url
     */
    public function setPdfAWSDownloadUrl($pdf_aws_download_url)
    {
        $this->pdf_aws_download_url = $pdf_aws_download_url;
    }

    /**
     * @return mixed
     */
    public function getInvoiceZipCc()
    {
        return $this->invoice_zip_cc;
    }

    /**
     * @param mixed $invoice_zip_cc
     */
    public function setInvoiceZipCc($invoice_zip_cc)
    {
        $this->invoice_zip_cc = $invoice_zip_cc;
    }

    /**
     * @return mixed
     */
    public function getCourseId()
    {
        return $this->course_id;
    }

    /**
     * @param mixed $course_id
     */
    public function setCourseId($course_id)
    {
        $this->course_id = $course_id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


}