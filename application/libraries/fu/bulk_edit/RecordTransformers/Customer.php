<?php
namespace fu\bulk_edit\RecordTransformers;

use Carbon\Carbon;
use fu\statementWorkflow\generateCharges;

class Customer extends RecordTransformer {

    private $error = false;
    public $recordId = false;
    public $existingRecordData = [];

    public $uniqueKeyMap = [
        'groups' => 'group_id',
        'passes' => 'pass_id',
        'minimum_charges' => 'minimum_charge_id',
        'recurring_charges' => 'id',
        'household_members' => 'person_id'
    ];

    public function __construct($CI_Model = false){

        if(!$CI_Model){
            $CI = get_instance();
            $CI->load->model('v2/Customer_model', 'Model');
            $this->Model =& $CI->Model;
        }else{
            $this->Model = $CI_Model;
        }
    }

    public function getListUniqueKey($field){

        if($this->uniqueKeyMap[$field]){
            return $this->uniqueKeyMap[$field];
        }
        return false;
    }

    public function getNewData($settings){

        $data = [];
        foreach($settings as $fieldKey => $fieldData){

            if($fieldData['action'] == 'set'){
                $data[$fieldKey] = $fieldData['value'];

            }else if($fieldData['action'] == 'increase'){
                $data[$fieldKey] = (float) ($this->existingRecordData[$fieldKey] + $fieldData['value']);

            }else if($fieldData['action'] == 'decrease'){
                $data[$fieldKey] = (float) ($this->existingRecordData[$fieldKey] - $fieldData['value']);

            }else if($fieldData['action'] == 'add'){
                if(!isset($data[$fieldKey])){
                    $data[$fieldKey] = $this->existingRecordData[$fieldKey];
                }
                $data[$fieldKey] = Helper::listAdd($data[$fieldKey], $this->getListUniqueKey($fieldKey), $fieldData['value']);

            }else if($fieldData['action'] == 'remove'){
                if(!isset($data[$fieldKey])){
                    $data[$fieldKey] = $this->existingRecordData[$fieldKey];
                }
                $data[$fieldKey] = Helper::listRemove($data[$fieldKey], $this->getListUniqueKey($fieldKey), $fieldData['value']);

            }else if($fieldData['action'] == 'clear'){
                $data[$fieldKey] = [];
            }
        }

        // If we are setting a password, we have to pass the existing username as well
        if(isset($data['password'])){
            $data['username'] = $this->existingRecordData['username'];
            $data['confirm_password'] = $data['password'];
        }

        if(isset($data['non_taxable'])){
            $data['taxable'] = 1 - (int) $data['non_taxable'];
            unset($data['non_taxable']);
        }

        return $data;
    }

    public function getCustomerData($recordId){

        $customers = $this->Model->get(['person_id' => (int) $recordId]);
        if(!empty($customers[0])){
            return $customers[0];
        }
        return false;
    }

    public function transform($settings){

        if(empty($settings)){
            $this->error = 'No fields to edit';
            return false;
        }
        $this->Model->course_id = $this->courseId;

        if(empty($this->existingRecordData)){
            $this->existingRecordData = $this->getCustomerData($this->recordId);
        }

        $newData = $this->getNewData($settings);
        $newData['course_id'] = $this->courseId;

        $success = $this->Model->save($this->recordId, $newData);

        // If the customer failed to save
        if(!$success){
            if($this->Model->error){
                $this->setError($this->Model->error);
            }
            return false;
        }

        // Generate prorated invoice charges (if selected to do so)
        if(
            !empty($settings['recurring_charges']) &&
            !empty($settings['recurring_charges']['value']) &&
            ($settings['recurring_charges']['action'] == 'add' || $settings['recurring_charges']['action'] == 'set')
        ){
            foreach($settings['recurring_charges']['value'] as $recurringCharge){

                if(empty($recurringCharge['items'])){
                    continue;
                }
                foreach($recurringCharge['items'] as $item){
                    if(empty($item['prorate']) || empty($item['repeated']['id'])){
                        continue;
                    }
                    $this->generateProratedInvoice($recurringCharge['id'], $item['repeated']['id'], $this->recordId);
                }
            }
        }

        return (int) $this->recordId;
    }

    public function generateProratedInvoice($recurringChargeId, $repeatableId, $personId){

        include_once(__DIR__.'/../../../../vendor/autoload.php');

        $queue = new \fu\statementWorkflow\classes\fakeQueue();
        $chargeGenerator = new \fu\statementWorkflow\generateChargeSale($queue);
        $silex =& $chargeGenerator->silex;
        $CI = get_instance();
        $CI->load->model('Invoice');

        $currentTime = date('Y-m-d');

        $chargeItems = $silex->db->getRepository(
            'e:ForeupRepeatableAccountRecurringChargeItems'
        );
        $chargeCustomers = $silex->db->getRepository(
            'e:ForeupAccountRecurringChargeCustomers'
        );
        $recurringCharges = $silex->db->getRepository(
            'e:ForeupAccountRecurringCharges'
        );
        $recurringCharge = $recurringCharges->find($recurringChargeId);

        $recurringChargeItem = $CI->db->query("SELECT id 
            FROM foreup_account_recurring_charge_customers
            WHERE person_id = {$personId}")->row_array();

        $ret = [];
        $ret['repeatable'] = $chargeItems->find($repeatableId);
        $ret['item'] = $ret['repeatable']->getItem();
        $ret['charge'] = $ret['item']->getRecurringCharge();
        $ret['courseId'] = $ret['charge']->getOrganizationId();
        $ret['customer'] = $chargeCustomers->find($recurringChargeItem['id']);
        $ret['current_time'] = $currentTime;
        $ret['prorate'] = true;

        $invoiceData = $chargeGenerator->generateInvoiceObject($ret, $currentTime);
        $invoice = $CI->Invoice->save($invoiceData, false);
        $chargeGenerator->associateInvoiceWithCharge($recurringCharge, $invoice['invoice_id']);

        return $invoice;
    }

    public function setRecordId($recordId){
        $this->existingRecordData = false;
        return $this->recordId = $recordId;
    }

    public function setError($error){
        $name = '('.$this->existingRecordData['first_name'] .' '. $this->existingRecordData['last_name'].')';
        $this->error = $name.' '.$error;
        return $this;
    }

    public function getError(){
        return $this->error;
    }
}