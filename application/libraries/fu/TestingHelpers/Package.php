<?php

namespace fu\TestingHelpers;


class Package
{
    protected $package;

    // TODO this is copy/pasted in multiple TestingHelpers
    protected function random_string($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function __construct($default = false) {
        $CI = get_instance();
        $CI->load->model('Loyalty_package');

        $this->package = [
            'course_id' => $CI->session->userdata('course_id'),
            'name' => $this->random_string(),
            'is_default' => $default
        ];
        $CI->Loyalty_package->save($this->package);
    }

    public function destroy() {
        $CI = get_instance();

        $CI->db->where('id', $this->package['id']);
        $CI->db->delete('loyalty_packages');
    }

    public function get_id() {
        return $this->package['id'];
    }
}