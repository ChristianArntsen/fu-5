<?php
namespace fu\TestingHelpers\TestingObjects;

class FBItem extends TestingObject
{
	private $item = 'null';

	function __construct()
	{
		$this->item = json_encode(array(
				'course_id'=> 6270,
				'item_id'=>5228,
				'name' => "Onion Rings",
				'department' => 'Food & Beverage',
				'category' => 'Appetizers',
				'subcategory' => null,
				'item_number' => null,
				'description' => 'Deep fried onion ring perfection',
				'image_id' => null,
				'cost_price' => 1.00,
				'unit_price' => 3.00,
				'unit_price_includes_tax' => 0,
				'inventory_level' =>835,
				'inventory_unlimited' => 0,
				'erange_size' => null, // what is this?
				'quickbooks_income' => null,
				'quickbooks_cogs' => null,
				'quickbooks_assets' => null,
				'max_discount' => 0,
				'quantity' => null,
				'is_unlimited' => 0,
				'reorder_level' => 0,
				'food_and_beverage' => 1,
				'number_of_sides' => 0,
				'location' => null,
				'gl_code' => null,
				'allow_alt_description' => null,
				'is_serialized' => null,
				'invisible' => null,
				'deleted' => null,
				'is_side' => 0,
				'soup_or_salad' => null,
				'add_on_price' => null,
				'print_priority' => 1,
				'kitchen_printer' => null,
				'do_not_print' => null,
				'inactive' => null,
				'meal_course_id' => null,
				'prompt_meal_course' => null,
				'do_not_print_customer_receipt' => null,
				'is_fee' => null, // not used
				'taxes' => array(
					array('name' => 'Sales Tax', 'percent' => 7.75, 'cumulative' => 0)
				),
				'supplier_id' => null,
				'is_shared' => null, // not used?
				'force_tax' => null
			)
		);
	}

	public function getObject()
	{
		return json_decode($this->item);
	}
	public function getArray()
	{
		return json_decode($this->item,true);
	}

}