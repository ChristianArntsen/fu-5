<?php
namespace fu\TestingHelpers\TestingObjects;

class Invoice extends TestingObject
{
	private $item = '{"course_id":null,"bill_start":null,"bill_end":null,"billing_id":0,
	                  "pay_member_account":0,"pay_customer_account":0,"person_id":null,
	                  "employee_id":null,"show_account_transactions":false,"items":null,
	                  "email_invoice":false,"credit_card_id":0,"due_date":null}';
	public function getObject()
	{
		return json_decode($this->item);
	}
	public function getArray()
	{
		return json_decode($this->item,true);
	}

}