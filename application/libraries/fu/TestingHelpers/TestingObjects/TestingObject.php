<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 8/25/2016
 * Time: 2:20 PM
 */

namespace fu\TestingHelpers\TestingObjects;


abstract class TestingObject
{
	abstract public function getObject();
}