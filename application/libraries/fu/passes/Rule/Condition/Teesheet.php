<?php
namespace fu\passes\Rule\Condition;

class Teesheet extends Condition {
	
	public function isTrue(){

		if(empty($this->pass->getCurrentUse())){
			return false;
		}

		if(empty($this->value)){
			return false;
		}
		$use = $this->pass->getCurrentUse();

		foreach($this->value as $value){
			if($value->isMatch($use)){
				return true;
			}
		}

		return false;
	}
}