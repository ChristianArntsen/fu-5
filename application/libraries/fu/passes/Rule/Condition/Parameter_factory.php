<?php
namespace fu\passes\Rule\Condition;

class Parameter_factory {
	
	public static function create($field = false, $data){
		
		$parameter = false;

		switch($field){
			
			case 'teesheet':
				$parameter = new Parameter\Teesheet($data);
			break;

			case 'price_class':
				$parameter = new Parameter\Price_class($data);
			break;
			
			case 'timeframe':
				$parameter = new Parameter\Timeframe($data);
			break;
			
			case 'total_used_per_day':
			case 'total_used_per_week':
			case 'total_used_per_month':
			case 'total_used_per_year':
			case 'total_used':
			case 'days_since_purchase':
				return $parameter = $data;
			break;
			default:
				return false;
		}

		return $parameter;
	}
}