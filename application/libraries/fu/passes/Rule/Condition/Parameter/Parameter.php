<?php
namespace fu\passes\Rule\Condition\Parameter;

abstract class Parameter {
	
	public abstract function isMatch($value);
}