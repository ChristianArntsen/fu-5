<?php
namespace fu\passes\Rule\Condition\Parameter;

class Teesheet extends Parameter {
	
	const FIELD = 'teesheet_id';
	private $teesheet_id = false;

	public function __construct($data = []){
		
		if(empty($data)){
			return false;
		}
		$this->teesheet_id = (int) $data['id'];
	}

	public function isMatch($use){

		if(!isset($use['teesheet_id'])){
			return false;
		}

		return ((int) $use['teesheet_id'] == (int) $this->teesheet_id);
	}
}