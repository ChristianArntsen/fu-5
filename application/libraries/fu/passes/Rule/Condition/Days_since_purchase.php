<?php
namespace fu\passes\Rule\Condition;

class Days_since_purchase extends Condition {
	
	public function isTrue(){

		$purchaseDate = $this->pass->getPurchaseDate();
		
		if(empty($purchaseDate)){
			$days = 0;
		}else{
			$days = $purchaseDate->diffInDays();
		}
		
		return self::compare($days, $this->operator, $this->value);
	}
}