<?php
namespace fu\items;


// TODO: refactor to change method sigs to have now optional $item parameter last.
class ItemPrice
{
    private $side_types;
    private $CI;
    private $item;
    
    public function __construct(&$item = null)
    {
        $this->item = null;
        $this->CI = get_instance();
        $this->CI->load->library('v2/cart_lib');
        $this->side_types = array('soups', 'salads', 'sides');
        if($item)$this->item = &$item;
    }

    public function calculatePrice(&$item = null, $taxable=1, $divisor=1)
    {

        if(!$item && $this->item)$item = &$this->item;
        if(!$item)return false;
        if(empty($divisor) || $divisor < 1) $divisor = 1;
        
        $ret = array();

        $quantity = 1;
        if(isset($item['quantity']) && $item['quantity']>1){
            $quantity = $item['quantity'];
        }
        
        $this->calculateSplitPrices($divisor, $item, $taxable);

        // If item has sides, calculate the split prices of each side
        // Split prices will be used when receipt is inserted into sales table
        $sidesTotal = 0;
        $sidesSplitTotal = 0;
        $sidesSplitSubtotal = 0;
        $sidesSplitTax = 0;
        $sidesSplitCompTotal = 0;
        $sidesLoyaltyReward = 0;
        $sidesLoyaltyCost = 0;
        $sidesLoyaltyDollars = 0;

        $item['num_splits'] = $divisor;
        $item['split_sides_subtotal'] = 0;
        $item['split_sides_tax'] = 0;
        $item['split_sides_total'] = 0;
        $item['split_sides_comp_total'] = 0;
        $item['split_sides_loyalty_cost'] = 0;
        $item['split_sides_loyalty_reward'] = 0;
        $item['split_sides_loyalty_dollars'] = 0;

        foreach($this->side_types as $side_type){
            if(!empty($item[$side_type])){
                foreach($item[$side_type] as $side_key => &$side){

                    $this->calculateSplitPrices($divisor, $side, $taxable);
                    $sidesTotal += $side['total'];

                    $item['split_sides_subtotal'] += $side['split_subtotal'];
                    $item['split_sides_tax'] += $side['split_tax'];
                    $item['split_sides_total'] += $side['split_total'];
                    $item['split_sides_comp_total'] += $side['split_comp_total'];
                    $item['split_sides_loyalty_cost'] += $side['split_loyalty_cost'];
                    $item['split_sides_loyalty_reward'] += $side['split_loyalty_reward'];
                    $item['split_sides_loyalty_dollars'] += $side['split_loyalty_dollars'];

                    $sidesSplitSubtotal += $side['split_subtotal'];
                    $sidesSplitTax += $side['split_tax'];
                    $sidesSplitTotal += $side['split_subtotal'] + $side['split_tax'];
                    $sidesSplitCompTotal += $side['split_comp_total'];
                    $sidesLoyaltyCost += $side['split_loyalty_cost'];
                    $sidesLoyaltyReward += $side['split_loyalty_reward'];
                    $sidesLoyaltyDollars += $side['split_loyalty_dollars'];
                }
            }
        }

        $item['split_service_fees_subtotal'] = 0;
        $item['split_service_fees_tax'] = 0;
        $item['split_service_fees_total'] = 0;
        $item['split_service_fees_comp_total'] = 0;     

        if(!empty($item['service_fees'])){
            foreach($item['service_fees'] as &$fee){

                $ServiceFee = new \fu\service_fees\ServiceFee($fee['service_fee_id']);
                $ServiceFee->setParentItem($item);
                $calculated_service_fee = $ServiceFee->calculatePriceForSubtotal($item['subtotal'], $taxable, $divisor);
                
                $item['split_service_fees_subtotal'] += $calculated_service_fee['subtotal'];
                $item['split_service_fees_tax'] += $calculated_service_fee['tax'];
                $item['split_service_fees_total'] += $calculated_service_fee['total'];
                $item['split_service_fees_comp_total'] += $calculated_service_fee['comp_total'];

                $fee['split_price'] = $calculated_service_fee['unit_price'];
                $fee['split_subtotal'] = $calculated_service_fee['subtotal'];
                $fee['split_tax'] = $calculated_service_fee['tax'];
                $fee['split_total'] = $calculated_service_fee['total'];
                $fee['quantity'] = $calculated_service_fee['quantity'];
                $fee['taxes'] = $calculated_service_fee['taxes'];
                $fee['comp_total'] = $calculated_service_fee['comp_total'];
                $fee['taxable'] = $calculated_service_fee['taxable']; 
                $fee['profit'] = $calculated_service_fee['profit'];
            }
        }

        // Add item + sides total to final receipt total
        $ret['subtotal'] = $item['split_subtotal'] + $sidesSplitSubtotal + $item['split_service_fees_subtotal'];
        $ret['tax'] = $item['split_tax'] + $sidesSplitTax + $item['split_service_fees_tax'];
        $ret['comp_total'] = $sidesSplitCompTotal + $item['split_service_fees_comp_total'];
        $ret['total'] = $item['split_total'] + $sidesSplitTotal + $item['split_service_fees_total'];
        
        $ret['total_loyalty_reward'] = $item['split_loyalty_reward'] + $sidesLoyaltyReward;
        $ret['total_loyalty_cost'] = $item['split_loyalty_cost'] + $sidesLoyaltyCost;
        $ret['total_loyalty_dollars'] = $item['split_loyalty_dollars'] + $sidesLoyaltyDollars;
        
        return $ret;
    }

    private function calculateSplitPrices($divisor, &$item, $taxable = 1){

        if(!$item && $this->item)$item = &$this->item;
        if(!$item)return false;

        if(empty($item['loyalty_cost'])){
            $item['loyalty_cost'] = 0;
        }
        if(empty($item['loyalty_reward'])){
            $item['loyalty_reward'] = 0;
        }
        if(empty($item['total_loyalty_dollars'])){
            $item['total_loyalty_dollars'] = 0;
        }
        if(empty($item['comp_total'])){
            $item['comp_total'] = 0;
        }
        if(!isset($item['force_tax'])){
            $item['force_tax'] = null;
        }
        $tax_included = ($item['unit_price_includes_tax'] == 1);

        if($item['force_tax'] !== null && $item['force_tax'] == 1){
            $taxable = 1;
        }else if($item['force_tax'] !== null && $item['force_tax'] == 0){
            $taxable = 0;
        }

        $item['split_price'] = round($item['price'] / $divisor, 2);
        
        if($tax_included){
            $item['split_subtotal'] = round(($item['subtotal'] + $item['tax']) / $divisor, 2);
        }else{
            $item['split_subtotal'] = round($item['subtotal'] / $divisor, 2);
        }
        $item['split_comp_total'] = round($item['comp_total'] / $divisor, 2);
        $item['split_loyalty_cost'] = round($item['loyalty_cost'] / $divisor);
        $item['split_loyalty_reward'] = round($item['loyalty_reward'] / $divisor);
        $item['split_loyalty_dollars'] = round($item['total_loyalty_dollars'] / $divisor, 2);

        $item['split_tax'] = $this->CI->cart_lib->calculate_tax($item['split_subtotal'], $item['taxes'], $tax_included);
        if($tax_included){
            $item['split_subtotal'] -= $item['split_tax'];      
        }

        if($taxable == 0){
            $item['split_tax'] = 0;
        }
        $item['split_total'] = round($item['split_subtotal'] + $item['split_tax'], 2);
        
        $item['taxable'] = ($taxable == 1);
        $item['num_splits'] = $divisor;
        return $item;
    }
}
?>