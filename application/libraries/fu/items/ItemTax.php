<?php

namespace fu\items;

class ItemTax
{
    public function calculate_tax($subtotal, &$taxes, $tax_included) {
        $total_tax = 0;
        if (!empty($taxes)) {
        	$total_percent = 0;
	        foreach ($taxes as $key => &$tax) {

		        if (empty($tax['percent']) || $tax['percent'] <= 0) {
			        continue;
		        }

		        $total_percent += $tax['percent'];

	        }


            foreach ($taxes as $key => &$tax) {

                if (empty($tax['percent']) || $tax['percent'] <= 0) {
                    continue;
                }


                $amount = 0;
                if ($tax_included) {
                    $actual_subtotal = (float)round($subtotal / (1 + $total_percent / 100), 2);
                    $amount = (float)round(($actual_subtotal) * ($tax['percent'] / 100), 2);

                } else if (empty($tax['cumulative']) || (int)$tax['cumulative'] == 0) {
                    $amount = (float)round($subtotal * ($tax['percent'] / 100), 2);

                } else {
                    $amount = (float)round(($subtotal + $total_tax) * ($tax['percent'] / 100), 2);
                }

                $tax['amount'] = $amount;
                $total_tax += $amount;
                //$subtotal = $actual_subtotal; // try not to double tax...
            }
        }

        return (float) round($total_tax, 2);
    }

    public function calculate_item_tax($subtotal, &$item_or_id, $item_type, $course_id = false) {
        $CI =& get_instance();
        $CI->load->model('Item');

        if(!is_array($item_or_id)) {
            $item = $item_or_id;
            if (!$CI->Item->exists($item, $course_id)) {
                return 0;
                //die('No item with id: '.$item_id.' found.');
            }

            $item = (array) $CI->Item->get_info($item, $course_id);
        }else{
            $item =& $item_or_id;
        }
        if (isset($item['force_tax']) && $item['force_tax'] === 0) {
            return 0;
        }

        $tax_included = 0;
        if(!empty($item['unit_price_includes_tax'])){
            //override default
            $tax_included = $item['unit_price_includes_tax'];
        }

        $manual_taxes = $CI->session->userdata('manual_taxes');
        if($item_type == 'item'){
            $taxes = $CI->Item_taxes->get_info($item['item_id']);

        }else if($item_type == 'item_kit'){
            $taxes = $CI->Item_taxes->get_info($item['item_id']);

        }else{
            return false;
        }

        return $this->calculate_tax($subtotal, $taxes, $tax_included);
    }

    public function saveItemTaxesToSale($sale_id,$item){
        $CI =& get_instance();
        if (isset($item['item_id']))
        {
            $db_tax_lines = $CI->Item_taxes->get_info($item['item_id']);

            if(empty($item['taxes'])){
                return false;
            }

            foreach($item['taxes'] as $key=>$row)
            {
                $amount = !empty($row['amount'])?$row['amount']:0;

                if($amount!==0)
                $CI->db->insert('sales_items_taxes', array(
                    'sale_id' 	=>$sale_id,
                    'item_id' 	=>$item['item_id'],
                    'line'      =>$item['line'],
                    'name'		=>!empty($row['name']) ? $row['name'] : $db_tax_lines[$key]['name'],
                    'percent' 	=>!empty($row['percent']) ? $row['percent'] : $db_tax_lines[$key]['percent'],
                    'cumulative'=>!empty($row['cumulative']) ? $row['cumulative'] : $db_tax_lines[$key]['cumulative'],
                    'amount'	=>$amount
                ));
            }

        }
        else
        {
            foreach($CI->Item_kit_taxes->get_info($item['item_kit_id']) as $row)
            {
                $CI->db->insert('sales_item_kits_taxes', array(
                    'sale_id' 		=>$sale_id,
                    'item_kit_id'	=>$item['item_kit_id'],
                    'line'      	=>$item['line'],
                    'name'			=>$row['name'],
                    'percent' 		=>$row['percent'],
                    'cumulative'	=>$row['cumulative']
                ));
            }
        }

        //INSERT TOURNAMENT TAXES HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
}
?>