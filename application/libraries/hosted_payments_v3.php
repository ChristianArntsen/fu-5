<?php

define("ECOM_V3_ROOT_URL", "https://etsemoney.com/hp/v3/adapters");

class Hosted_payments_v3 {

    public $simulate_timeout = false;

    private function get_url() {
        if ($this->simulate_timeout) {
            return 'http://127.0.0.1:8080/index.php/daemon/simulate_timeout';
        }
        else {
            return ECOM_V3_ROOT_URL;
        }
    }

    public function sign_in($apiKey, $corrId) {
        $data = array(
            'signIn'=>array(
                'signInRequest'=>array(
                    'apiKey'=>$apiKey,
                    'correlationId'=>$corrId
                )
            ));

        // Try three times before giving up
        $response = $this->send_request($data);
        if (!isset($response->signInResponse)) {
            $response = $this->send_request($data);
            if (!isset($response->signInResponse)) {
                $response = $this->send_request($data);
            }
        }
        return $response->signInResponse->token;
    }

    public function to_corr_id($id)
    {
		$env = ENVIRONMENT;
	    if(isset($env)&&$env ==='production'){
		    $prepend = 9;
	    }
	    elseif(isset($env)&&$env ==='testing'){
		    $prepend = 8;
	    }
	    elseif(isset($env)&&$env ==='development'){
		    $prepend = 7;
	    }else{
	    	$prepend = 6;
	    }

	    $timestamp = time();
	    $len = strlen($id);
	    // prepend environment and timestamp as a namespace that is *very* unlikely to be duplicated
	    // should be very strong guarantee for production
	    $correlation_id = $prepend. '' . $timestamp . "000000000000000000000";


	    $correlation_id = substr($correlation_id, 0, 32 - $len) . $id;
	    $ret = substr($correlation_id, 0, 8) . '-' . substr($correlation_id, 8, 4) . '-' . substr($correlation_id, 12, 4) . '-' . substr($correlation_id, 16, 4) . '-' . substr($correlation_id, 20);
	    return $ret;
    }

    public function charge($token, $refCode, $payInst, $amount, $corrId) {
        $data = array(
            "charge"=> array(
                "chargeRequest" => array(
                    "token" => $token,
                    "transactionId" => $refCode,
                    "instrumentId" => $payInst,
                    "amount" => $amount,
                    "correlationId" => $corrId
                )
            )
        );

        // Try three times before giving up
        $response = $this->send_request($data);

        if (!isset($response->chargeResponse) && !isset($response->error)) {
            $response = $this->send_request($data);
            if (!isset($response->chargeResponse) && !isset($response->error)) {
                $response = $this->send_request($data);
            }
        }
        return $response;
    }

    public function verify($token, $transId)
    {
        $data = array(
            "status" => array(
                "statusRequest" => array(
                    "token" => $token,
                    "transactionId" => $transId
                )
            )
        );

        // Try three times before giving up
        $response = $this->send_request($data);
        if (!isset($response->statusResponse)) {
            $response = $this->send_request($data);
            if (!isset($response->statusResponse)) {
                $response = $this->send_request($data);
            }
        }
        return $response->statusResponse;
    }

    public function void($token, $refCode, $corrId)
    {
        $data = array(
            "voidAuthorization" => array(
                "voidAuthorizationRequest" => array(
                    "token" => $token,
                    "transactionId" => $refCode,
                    "correlationId" => $corrId
                )
            )
        );

        $response = $this->send_request($data);

        return $response->transactionId;
    }

    public function refund($token, $refCode, $payInst, $amount, $corrId)
    {
        $data = array(
            "refund" => array(
                "refundRequest" => array(
                    "token" => $token,
                    "transactionId" => $refCode,
                    "instrumentId" => $payInst,
                    "amount" => $amount,
                    "customProperties" => array(),
                    "correlationId" => $corrId
                )
            )
        );

        $response = $this->send_request($data);

        return $response->refundResponse->transactionId;
    }

    private function send_request($data) {

        $data_string = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->get_url());
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        return json_decode($result);
    }
}