<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupScores
 *
 * @ORM\Table(name="foreup_scores", indexes={@ORM\Index(name="FK__foreup_users", columns={"person_id"}), @ORM\Index(name="FK_foreup_scores_foreup_courses", columns={"course_id"})})
 * @ORM\Entity
 */
class ForeupScores
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="detailed_score_information", type="string", length=5000, nullable=true)
     */
    private $detailedScoreInformation;

    /**
     * @var integer
     *
     * @ORM\Column(name="par", type="integer", nullable=true)
     */
    private $par;

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="integer", nullable=true)
     */
    private $score;

    /**
     * @var \foreup\rest\models\entities\ForeupCourses
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $course;

    /**
     * @var \foreup\rest\models\entities\ForeupUsers
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
     * })
     */
    private $person;


}

