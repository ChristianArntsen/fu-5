<!DOCTYPE html>
<html>
	<head>
		<title>API Test</title>
		<style>
			iframe {
				width: 800px;
				height: 600px;
				border: 3px solid blue;
			}
		</style>
	</head>
<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

function show_data($element){
	
	if($element->success()){
		echo '<h2 style="color: green">Success</h2>';
	}else{
		echo '<h2 style="color: red">ERROR</h2>';
	}
	
	echo '<pre style="border: 1px solid #E0E0E0; background: #F0F0F0; padding: 10px;">';
	print_r($element->response()->xml());
	echo '</pre>';
}

function get_number(){
	return rand(1, 999999);
}

$element = new Element_merchant();
$element->set_testing(true);

$credentials = array(
	'account_id' => 1045621,
	'account_token' => '4C779213331677A5A89D09F2F71C2D7884F84A08DAB0165F29385BBF0648E02827A2C601',
	'application_id' => 8366,
	'acceptor_id' => 3928907,
	'terminal_id' => 0
);

// Initialize payment library
$element->init(
	new GuzzleHttp\Client(),
	$credentials
);

$return_url = 'http://192.168.1.150/fu/index.php/v2/home/element_test_hosted_payments';
$encrypted_track = '%B5499009000006781^TEST/ELEMENT^19120000000000000?;5499009000006781=19120000000000000000?|0600|7E113BE4CA7513A6D8F9D0D57206065BC28BA6B2CCD71A2C1817C9A8351E4F43290F041F99B79F08EB30A8F4B3D43641C3C308724DEF44FB|9FCABB29131C963987F73E02D95FB1DAF7E97E7C113D316CD9BD22D12B82B70D8D859FD2CAB4C799||||0506873|4A9445E4EBCE7E29|901050034200708000A3|6D9B||5500';

$market_codes = array(
	'Retail' => Element_merchant::MARKET_CODE_RETAIL,
	'F&B' => Element_merchant::MARKET_CODE_FOOD_RESTAURANT,
    'Ecommerce' => Element_merchant::MARKET_CODE_ECOMMERCE
);

/***********************************************************************
 * Hosted payments functions
 * ********************************************************************/
if(isset($test_type) && $test_type == 'hosted_payments'){

	echo '<hr><h1>Hosted Payments Tests</h1><hr>';

	// Ecommerce (golfer facing form saving credit card via online booking)
	echo '<h1><strong>TransactionSetup - Ecommerce</strong></h1>';

	$element->set_terminal(array(
		'CardholderPresentCode' => 		Element_merchant::CARDHOLDER_PRESENT_CODE_ECOMMERCE,
		'CardInputCode' => 				Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED,
		'CardPresentCode' => 			Element_merchant::CARD_PRESENT_CODE_NOT_PRESENT,
		'MotoECICode' => 				Element_merchant::MOTO_ECI_CODE_NON_AUTHENTICATED_SECURE_ECOMMERCE_TRANSACTION,
		'TerminalCapabilityCode' => 	Element_merchant::TERMINAL_CAPABILITY_CODE_KEY_ENTERED,
		'TerminalEnvironmentCode' => 	Element_merchant::TERMINAL_ENVIRONMENT_CODE_ECOMMERCE,
		'TerminalType' => 				Element_merchant::TERMINAL_TYPE_ECOMMERCE
	));

	$element->init_hosted_payment(array(
		'amount' => 2.06, 
		'return_url' => $return_url,
		'card_input_code' => Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED_MAGSTRIPE_FAILURE,
		'market_code' => Element_merchant::MARKET_CODE_ECOMMERCE,
		'transaction_setup_method' => Element_merchant::TRANSACTION_SETUP_CREDIT_CARD_SALE,
		'billing_address1' => '1234 1st St',
		'billing_zipcode' => '83401',
		'reference_number' => get_number(),
		'ticket_number' => get_number()
	));

	$iframe_url = $element->hosted_payment_url();
	echo "<iframe src='{$iframe_url}'></iframe>";	

	// Hosted payments for Retail and F&B
	foreach($market_codes as $label => $market_code){
	
		$element->set_terminal(array(
			'CardholderPresentCode' => 		Element_merchant::CARDHOLDER_PRESENT_CODE_PRESENT,
			'CardInputCode' => 				Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED_MAGSTRIPE_FAILURE,
			'CardPresentCode' => 			Element_merchant::CARD_PRESENT_CODE_PRESENT,
			'MotoECICode' => 				Element_merchant::MOTO_ECI_CODE_NOT_USED,
			'TerminalCapabilityCode' => 	Element_merchant::TERMINAL_CAPABILITY_CODE_MAGSTRIPE_READER,
			'TerminalEnvironmentCode' => 	Element_merchant::TERMINAL_ENVIRONMENT_CODE_LOCAL_ATTENDED,
			'TerminalType' => 				Element_merchant::TERMINAL_TYPE_POINT_OF_SALE
		));
		
		// Credit card sale
		echo '<h1><strong>'.$label.' CreditCardSale</strong></h1>';
		$element->init_hosted_payment(array(
			'amount' => 2.12, 
			'return_url' => $return_url,
			'card_input_code' => Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED_MAGSTRIPE_FAILURE,
			'market_code' => $market_code,
			'transaction_setup_method' => Element_merchant::TRANSACTION_SETUP_CREDIT_CARD_SALE,
			'billing_address1' => '1234 1st St',
			'billing_zipcode' => '83401',
			'reference_number' => get_number(),
			'ticket_number' => get_number()
		));

		$iframe_url = $element->hosted_payment_url();
		echo "<iframe src='{$iframe_url}'></iframe>";	
		
		// Credit card auth
		echo '<h1><strong>'.$label.' CreditCardAuth</strong></h1>';
		$element->init_hosted_payment(array(
			'amount' => 5.59, 
			'return_url' => $return_url.'/auth/'.$market_code,
			'card_input_code' => Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED_MAGSTRIPE_FAILURE,
			'market_code' => $market_code,
			'transaction_setup_method' => Element_merchant::TRANSACTION_SETUP_CREDIT_CARD_AUTH,
			'billing_address1' => '1234 1st St',
			'billing_zipcode' => '83401',
			'reference_number' => get_number(),
			'ticket_number' => get_number()
		));

		$iframe_url = $element->hosted_payment_url();
		echo "<iframe src='{$iframe_url}'></iframe>";	
	}
	die();
}

// Health check
echo '<h1>Health Check</h1>';
$element->health_check();
show_data($element);

/***********************************************************************
 * F&B and Retail functions
 * ********************************************************************/
foreach($market_codes as $market => $market_code){

	$element->set_terminal(array(
		'CardholderPresentCode' => 		Element_merchant::CARDHOLDER_PRESENT_CODE_PRESENT,
		'CardInputCode' => 				Element_merchant::CARD_INPUT_CODE_MAGSTRIPE_READ,
		'CardPresentCode' => 			Element_merchant::CARD_PRESENT_CODE_PRESENT,
		'MotoECICode' => 				Element_merchant::MOTO_ECI_CODE_NOT_USED,
		'TerminalCapabilityCode' => 	Element_merchant::TERMINAL_CAPABILITY_CODE_MAGSTRIPE_READER,
		'TerminalEnvironmentCode' => 	Element_merchant::TERMINAL_ENVIRONMENT_CODE_LOCAL_ATTENDED,
		'TerminalType' => 				Element_merchant::TERMINAL_TYPE_POINT_OF_SALE
	));
	
	echo '<hr><h1 style="color: blue;">'.$market.'</h1></hr>';
	
	// Credit card sale
	echo "<h1>CreditCardSale - Visa Swiped (MagneprintData)</h1>";
	$params = array(
		'amount' => 2.06,
		'encrypted_track' => $encrypted_track,
		'market_code' => $market_code,
		'reference_number' => get_number(),
		'ticket_number' => get_number()
	);
	$element->credit_card_sale($params);
	show_data($element);

	$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;

	// Void Credit card sale
	echo '<h1><strong>CreditCardVoid</strong> - Visa Sale</h1>';
	$element->credit_card_void(array(
		'transaction_id' => $transaction_id,
		'reference_number' => get_number(),
		'ticket_number' => get_number(),
		'market_code' => $market_code
	));
	show_data($element);

	// Credit card credit
	echo '<h1><strong>CreditCardCredit</strong> - Visa Swiped (MagneprintData)</h1>';
	$params = array(
		'amount' => 5.22,
		'encrypted_track' => $encrypted_track,
		'market_code' => $market_code,
		'reference_number' => get_number(),
		'ticket_number' => get_number()
	);
	$element->credit_card_credit($params);
	show_data($element);

	$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;

	// Void Credit card credit
	echo '<h1><strong>CreditCardVoid</strong> - Visa Credit</h1>';
	$element->credit_card_void(array(
		'transaction_id' => $transaction_id,
		'reference_number' => get_number(),
		'ticket_number' => get_number(),
		'market_code' => $market_code
	));
	show_data($element);

	// Transaction query
	echo '<h1>TransactionQuery</h1>';
	$params = array(
		'transaction_id' => $transaction_id
	);
	$element->transaction_query($params);
	show_data($element);

	// Credit card return
	// First make a sale
	echo '<h1><strong>CreditCardReturn</strong> - Visa Sale</h1>';
	$params = array(
		'amount' => 2.06,
		'encrypted_track' => $encrypted_track,
		'market_code' => $market_code,
		'reference_number' => get_number(),
		'ticket_number' => get_number()
	);
	$element->credit_card_sale($params);

	$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;

	// Return the previous sale made
	$element->set_terminal(array('CardInputCode' => Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED));
	$params = array(
		'amount' => 2.06,
		'transaction_id' => $transaction_id,
		'market_code' => $market_code,
		'reference_number' => get_number(),
		'ticket_number' => get_number()
	);
	$element->credit_card_return($params);
	show_data($element);
	
	$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;
	
	// Void Credit card return
	echo '<h1><strong>CreditCardVoid</strong> - Return</h1>';
	$element->credit_card_void(array(
		'transaction_id' => $transaction_id,
		'reference_number' => get_number(),
		'ticket_number' => get_number(),
		'market_code' => $market_code
	));
	show_data($element);
	
	// Credit card authorization
	echo '<h1><strong>CreditCardAuthorization</strong> - Visa Swiped (MagneprintData)</h1>';
	$element->set_terminal(array('CardInputCode' => Element_merchant::CARD_INPUT_CODE_MAGSTRIPE_READ));
	$params = array(
		'amount' => 5.53,
		'encrypted_track' => $encrypted_track,
		'market_code' => $market_code,
		'reference_number' => get_number(),
		'ticket_number' => get_number()
	);
	$element->credit_card_authorization($params);
	show_data($element);
	
	$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;
	
	// Credit card authorization completion
	echo '<h1><strong>CreditCardAuthorizationCompletion</strong> - Visa Swiped (MagneprintData)</h1>';
	$params = array(
		'amount' => 5.53,
		'transaction_id' => $transaction_id,
		'reference_number' => get_number(),
		'ticket_number' => get_number(),
		'market_code' => $market_code
	);
	$element->credit_card_authorization_completion($params);
	show_data($element);
	
	// Reverse credit card sale (SYSTEM)
	$element->set_terminal(array('CardInputCode' => Element_merchant::CARD_INPUT_CODE_MAGSTRIPE_READ));
	echo '<h1><strong>CreditCardReversal</strong> - SYSTEM CreditCardSale $2.06</h1>';
	$ref_number = get_number();
	$ticket_number = get_number();
	
	$params = array(
		'amount' => 2.06,
		'encrypted_track' => $encrypted_track,
		'market_code' => $market_code,
		'reference_number' => $ref_number,
		'ticket_number' => $ticket_number
	);
	$element->credit_card_sale($params);
	$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;
	
	$params = array(
		'amount' => 2.06,
		'encrypted_track' => $encrypted_track,
		'reversal_type' => Element_merchant::REVERSAL_TYPE_SYSTEM,
		'market_code' => $market_code,
		'reference_number' => $ref_number,
		'ticket_number' => $ticket_number
	);
	$element->credit_card_reversal($params);	
	show_data($element);
	
	// Reverse credit card sale (FULL)
	$element->set_terminal(array('CardInputCode' => Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED));
	echo '<h1><strong>CreditCardReversal</strong> - FULL CreditCardSale $2.06</h1>';
	$ref_number = get_number();
	$ticket_number = get_number();
	
	$params = array(
		'amount' => 2.06,
		'encrypted_track' => $encrypted_track,
		'market_code' => $market_code,
		'reference_number' => $ref_number,
		'ticket_number' => $ticket_number
	);
	$element->credit_card_sale($params);
	$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;
	
	$params = array(
		'amount' => 2.06,
		'transaction_id' => $transaction_id,
		'reversal_type' => Element_merchant::REVERSAL_TYPE_FULL,
		'market_code' => $market_code,
		'reference_number' => $ref_number,
		'ticket_number' => $ticket_number
	);
	$element->credit_card_reversal($params);	
	show_data($element);	
	
	// Reverse credit card authorization
		$element->set_terminal(array('CardInputCode' => Element_merchant::CARD_INPUT_CODE_MAGSTRIPE_READ));
	echo "<h1><strong>CreditCardReversal</strong> - SYSTEM CreditCardAuth $5.53</h1>";	
	$ref_number = get_number();
	$ticket_number = get_number();	
	
	$params = array(
		'amount' => 5.53,
		'encrypted_track' => $encrypted_track,
		'market_code' => $market_code,
		'reference_number' => get_number(),
		'ticket_number' => get_number()
	);
	$element->credit_card_authorization($params);
	$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;
	
	$params = array(
		'amount' => 5.53,
		'encrypted_track' => $encrypted_track,
		'reversal_type' => Element_merchant::REVERSAL_TYPE_SYSTEM,
		'market_code' => $market_code,
		'reference_number' => $ref_number,
		'ticket_number' => $ticket_number
	);
	$element->credit_card_reversal($params);	
	show_data($element);
	
	// Reverse credit card authorization (FULL)
		$element->set_terminal(array('CardInputCode' => Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED));
	echo "<h1><strong>CreditCardReversal</strong> - FULL CreditCardAuth $5.53</h1>";	
	$ref_number = get_number();
	$ticket_number = get_number();	
	
	$params = array(
		'amount' => 5.53,
		'encrypted_track' => $encrypted_track,
		'market_code' => $market_code,
		'reference_number' => get_number(),
		'ticket_number' => get_number()
	);
	$element->credit_card_authorization($params);
	$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;
	
	$params = array(
		'amount' => 5.53,
		'transaction_id' => $transaction_id,
		'reversal_type' => Element_merchant::REVERSAL_TYPE_FULL,
		'market_code' => $market_code,
		'reference_number' => $ref_number,
		'ticket_number' => $ticket_number
	);
	$element->credit_card_reversal($params);	
	show_data($element);	
}

/***********************************************************************
 * Payment account functions
 * ********************************************************************/
$element->set_terminal(array(
	'CardholderPresentCode' => 		Element_merchant::CARDHOLDER_PRESENT_CODE_PRESENT,
	'CardInputCode' => 				Element_merchant::CARD_INPUT_CODE_MANUAL_KEYED,
	'CardPresentCode' => 			Element_merchant::CARD_PRESENT_CODE_PRESENT,
	'MotoECICode' => 				Element_merchant::MOTO_ECI_CODE_SINGLE,
	'TerminalCapabilityCode' => 	Element_merchant::TERMINAL_CAPABILITY_CODE_MAGSTRIPE_READER,
	'TerminalEnvironmentCode' => 	Element_merchant::TERMINAL_ENVIRONMENT_CODE_LOCAL_ATTENDED,
	'TerminalType' => 				Element_merchant::TERMINAL_TYPE_POINT_OF_SALE
));

echo '<hr><h1 style="color: blue;">Payment Accounts</h1></hr>';
$account_ref = get_number();

// Create account
echo '<h1><strong>PaymentAccountCreate Magnetic Stripe</strong></h1>';
$element->payment_account_create(array(
	'encrypted_track' => $encrypted_track,
	'payment_account_reference_number' => $account_ref
));
show_data($element);
$payment_account_id = $element->response()->xml()->Response->PaymentAccount->PaymentAccountID;

// Update account
echo '<h1><strong>PaymentAccountUpdate</strong></h1>';
$element->payment_account_update(array(
	'encrypted_track' => $encrypted_track,
	'payment_account_reference_number' => $account_ref,
	'payment_account_id' => $payment_account_id
));
show_data($element);

// Credit card sale
echo '<h1><strong>PaymentAccount</strong> - Credit Card SALE</h1>';
$ref_number = get_number();
$ticket_number = get_number();
$params = array(
	'amount' => 1.81,
	'payment_account_id' => $payment_account_id,
	'market_code' => Element_merchant::MARKET_CODE_RETAIL,
	'reference_number' => $ref_number,
	'ticket_number' => $ticket_number
);
$element->credit_card_sale($params);
show_data($element);
$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;

// Credit card reversal
echo '<h1><strong>PaymentAccount</strong> - CreditCardSale Reversal $1.81</h1>';
$params = array(
	'amount' => 2.06,
	'payment_account_id' => $payment_account_id,
	'transaction_id' => $transaction_id,
	'reversal_type' => Element_merchant::REVERSAL_TYPE_FULL,
	'market_code' => Element_merchant::MARKET_CODE_RETAIL,
	'reference_number' => $ref_number,
	'ticket_number' => $ticket_number
);
$element->credit_card_reversal($params);	
show_data($element);

// Credit card credit
echo '<h1><strong>PaymentAccount</strong> - Credit Card CREDIT</h1>';
$params = array(
	'amount' => 1.93,
	'payment_account_id' => $payment_account_id,
	'market_code' => Element_merchant::MARKET_CODE_RETAIL,
	'reference_number' => get_number(),
	'ticket_number' => get_number()
);
$element->credit_card_credit($params);
show_data($element);

// Credit card authorization
echo '<h1><strong>PaymentAccount</strong> - Credit Card AUTHORIZATION</h1>';
$params = array(
	'amount' => 1.90,
	'payment_account_id' => $payment_account_id,
	'market_code' => Element_merchant::MARKET_CODE_RETAIL,
	'reference_number' => get_number(),
	'ticket_number' => get_number()
);
$element->credit_card_authorization($params);
show_data($element);

$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;

// Credit card authorization completion
echo '<h1><strong>PaymentAccount</strong> - Credit Card AUTHORIZATION COMPLETION</h1>';
$params = array(
	'amount' => 1.90,
	'transaction_id' => $transaction_id,
	'payment_account_id' => $payment_account_id,
	'market_code' => Element_merchant::MARKET_CODE_RETAIL,
	'reference_number' => get_number(),
	'ticket_number' => get_number()
);
$element->credit_card_authorization_completion($params);
show_data($element);

// Account query
echo '<h1><strong>PaymentAccountQuery</strong></h1>';
$element->payment_account_query(array(
	'payment_account_id' => $payment_account_id
));
show_data($element);

echo '<h1><strong>PaymentAccountDelete</strong></h1>';
$element->payment_account_delete($payment_account_id);
show_data($element);

echo '<h1><strong>Check</strong></h1>';
$account_ref = get_number();

echo '<h1><strong>PaymentAccountCreate ACH</strong></h1>';
$element->payment_account_create(array(
	'payment_account_type' => $element::PAYMENT_ACCOUNT_ACH,
	'payment_account_reference_number' => $account_ref,
    'account_number' => 000000,
    'routing_number' => 000000
));
show_data($element);
$payment_account_id = $element->response()->xml()->Response->PaymentAccount->PaymentAccountID;

echo '<h1><strong>CheckVerification</strong></h1>';
$response =
	$element->check_verification(array(
		'payment_account_reference_number' => $account_ref,
		'payment_account_id' => $payment_account_id,
		'market_code' => Element_merchant::MARKET_CODE_ECOMMERCE
	));
show_data($element);

echo '<h1><strong>CheckSale</strong></h1>';
$sale_number = get_number();
$response =
	$element->check_sale(array(
		'payment_account_reference_number' => $account_ref,
		'payment_account_id' => $payment_account_id,
		'market_code' => Element_merchant::MARKET_CODE_ECOMMERCE,
        'transaction_amount'=>33.39,
        'reference_number' => $sale_number
	));
show_data($element);
$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;

echo '<h1><strong>CheckCredit</strong></h1>';
$response =
	$element->check_credit(array(
		'payment_account_reference_number' => $account_ref,
		'payment_account_id' => $payment_account_id,
		'market_code' => Element_merchant::MARKET_CODE_ECOMMERCE,
		'transaction_amount'=>5.00,
		'reference_number' => get_number()
	));
show_data($element);

echo '<h1><strong>CheckReturn</strong></h1>';
$response =
	$element->check_return(array(
		'payment_account_reference_number' => $account_ref,
		'payment_account_id' => $payment_account_id,
		'market_code' => Element_merchant::MARKET_CODE_ECOMMERCE,
		'transaction_amount'=>33.39,
		'reference_number' => get_number(),
        'transaction_id' => $transaction_id
	));
show_data($element);

echo '<h1><strong>CheckVoid</strong></h1>';


$sale_number = get_number();
$response =
	$element->check_sale(array(
		'payment_account_reference_number' => $account_ref,
		'payment_account_id' => $payment_account_id,
		'market_code' => Element_merchant::MARKET_CODE_ECOMMERCE,
		'transaction_amount'=>33.34,
		'reference_number' => $sale_number
	));
$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;

$response =
	$element->check_void(array(
		'payment_account_reference_number' => $account_ref,
		'payment_account_id' => $payment_account_id,
		'market_code' => Element_merchant::MARKET_CODE_ECOMMERCE,
		'transaction_amount'=>33.34,
		'reference_number' =>get_number(),
		'transaction_id' => $transaction_id
	));
show_data($element);
$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;

echo '<h1><strong>CheckReversal</strong></h1>';
echo '<p>Note: This is known and expected to fail with "UNABLE TO REVERSE" on Element\'s test server as of 2017-06-26</p>';

$sale_number = get_number();
$response =
	$element->check_sale(array(
		'payment_account_reference_number' => $account_ref,
		'payment_account_id' => $payment_account_id,
		'market_code' => Element_merchant::MARKET_CODE_ECOMMERCE,
		'transaction_amount'=>33.34,
		'reference_number' => $sale_number
	));
$transaction_id = (string) $element->response()->xml()->Response->Transaction->TransactionID;

$response =
	$element->check_reversal(array(
		'payment_account_reference_number' => $account_ref,
		'payment_account_id' => $payment_account_id,
		'market_code' => Element_merchant::MARKET_CODE_ECOMMERCE,
		'transaction_amount'=>33.34,
		'reference_number' =>$sale_number,
		'transaction_id' => $transaction_id
	));
show_data($element);



?>
</body>
</html>
