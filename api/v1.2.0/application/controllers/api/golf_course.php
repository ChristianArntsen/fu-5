<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Golf_course extends REST_Controller
{
	function __construct() 
	{
		parent::__construct();	
		$this->load->model('Api_data');
	}
	
    function index_get()
    {
        $this->response(array('Possible requests',array('request'=>'areas','request'=>'list',array('required'=>array('area_id')),'info'=>array('required'=>array('course_id')),'rates')));
    }
	
	function areas_get()
	{
		$areas = $this->Api_data->get_course_areas();
		//echo $this->db->last_query();
		if ($areas->num_rows() > 0)
			$this->response($areas->result_array());
		else 
			$this->response(array('error','No results'));
	}
	
	function list_get()
	{
		$area_id = $this->input->get('area_id');
		if (!$area_id)
			$this->response(array("error", 'Missing parameter: area_id'), 400);
		else {
			$course_list = $this->Api_data->get_course_list($area_id);
			if (count($course_list) > 0){				
			
				$this->response(array('success'=>true,'golf_courses'=>$course_list));
			}
			else
				$this->response(array('error','No results'));
		}
	}
	
	function info_get()
	{
		$course_id = $this->input->get('golf_course_id');
		if (!$course_id)
			$this->response(array("error", 'Missing parameter: golf_course_id'), 400);
		else {
			$course_info = $this->Api_data->get_course_info($course_id);			
			if ($course_info->num_rows() > 0)
			{							
				$course_info = $course_info->row_array();
				$this->response(array('success'=>true,'golf_course_info'=>$course_info));
			}
			else {
				$this->response(array('success'=>true,'golf_course_info'=>array(),'error'=>'No results'));			
			}
		}
	}
	
	function app_data_get()
	{
		$course_id = $this->input->get('golf_course_id');
		if (!$course_id)
		{
			$this->response(array("error", 'Missing parameter: golf_course_id'), 400);
		}			
		else 
		{
			$course_info = $this->Api_data->get_course_app_data($course_id);
				
			if ($course_info)
			{
				$this->response(array('success'=>true,'golf_course_info'=>$course_info));
			}
			else 
			{
				$this->response(array('success'=>true,'golf_course_info'=>array(),'error'=>'No results'));			
			}
		}
			
	}
	
	function links_get()
	{
		$course_id = $this->input->get('golf_course_id');
		if (!$course_id)
			$this->response(array("error", 'Missing parameter: golf_course_id'), 400);
		else {
			$course_links_list = $this->Api_data->get_course_links_list($course_id);
			if ($course_links_list->num_rows() > 0)
				$this->response(
					array(
						'success'=>true,
						'golf_course_links'=>$course_links_list->result_array()
					)
				);
			else {
				$this->response(
					array(
						'success'=>true,
						'golf_course_links'=>array()
					)
				);			
			}
		}
		
	}
}