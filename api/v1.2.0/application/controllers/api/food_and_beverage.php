<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Food_and_beverage extends REST_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('table_receipt');
		$this->load->model('table_ticket');
		$this->load->library('sale_lib');
		$this->load->model('Employee');

		$this->suspended_sale_id = false;
		$this->table_num = false;
	}

	// Retrieves suspended sale ID for table number and checks if user
	// can access table
    private function _get_table($table_num){

		$suspended_sale_id = $this->Table->get_id_by_table_number($table_num);
		$employee_id = (int) $this->session->userdata('person_id');

		// If table service (suspended sale) doesn't exist, create a new one
		if(empty($suspended_sale_id) && !empty($table_num)){
			$suspended_sale_id = $this->Table->save(array(), 0, $employee_id, '', array(), null, $table_num);
		}

		if(!$suspended_sale_id){
			$this->response(array('success'=>false, 'msg' => 'Table does not exist'), 404);
			return false;
		}

		$table_info = $this->Table->get_info($suspended_sale_id);
		$table_info = $table_info->row_array();

		// If employee isn't allowed to access table, return error
		if($this->permissions->is_employee() && $table_info['employee_id'] != $this->session->userdata('person_id')){
			$this->response(array('success'=>false, 'msg' => 'You don\'t have permission to access that table'), 403);
			return false;
		}

		$this->suspended_sale_id = $suspended_sale_id;
		$this->table_num = $table_num;

		return true;
	}

    function index_get()
    {
        echo 'Food and beverage root';
    }

    /*******************************************************************
     * Service - Manages listing/opening/suspending sales on tables
	 ******************************************************************/
	function service_get($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		// If retrieving data for single table
		if(!empty($table_num)){

			$this->_get_table($table_num);

			// Route receipts
			if($method == 'receipts'){
				$this->receipts_get($recordId, $method2, $recordId2);
				return true;
			}

			// Route cart
			if($method == 'cart'){
				$this->cart_get($recordId);
				return true;
			}

			// Route kitchen orders
			if($method == 'orders'){
				$this->orders_get($recordId);
				return true;
			}

			// Retrieve table data along with all items, receipts and
			// payments associated with it
			$table_info = $this->Table->get_info($this->suspended_sale_id);
			$table_info = $table_info->row_array();

			$data['table_num'] = $table_num;
			$data['suspended_sale_id'] = (int) $this->suspended_sale_id;
			$data['employee_id'] = $table_info['employee_id'];
			$data['cart'] = $this->Table->get_items($this->suspended_sale_id);
			$data['receipts'] = $this->table_receipt->get($this->suspended_sale_id);
			$data['receipt_payments'] = $this->sale_lib->get_receipt_payments_total();
			$data['sale_time'] = $table_info['sale_time'];

			$this->session->set_userdata('table_number', $data['table_num']);

			$this->response($data);
			return true;
		}

		// If getting list of available tables
		$tables = $this->Table->get_all(true)->result_array();
		$this->response($tables);
	}

	function service_post($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$this->_get_table($table_num);

		// Route receipts
		if($method == 'receipts'){
			$this->receipts_post($recordId, $method2, $recordId2);
			return true;
		}

		// Route cart
		if($method == 'cart'){
			$this->cart_post($recordId);
			return true;
		}

		// Route kitchen orders
		if($method == 'orders'){
			$this->orders_post($recordId);
			return true;
		}
		$success = true;
		$employee_id = $this->post('employee_id');
		if(!empty($employee_id)){

			// Employees can only be switched by admins
			if($this->permissions->is_employee()){
				$this->response(array('success' => false,'msg' => 'You don\'t have permission to switch table employees'), 403);
				return false;
			}

			$success = $this->Table->update(array('employee_id'=>(int) $employee_id), $this->suspended_sale_id);
		}

		if($success){
			$this->response(array('success' => true,'msg' => 'Table updated'));
		}else{
			$this->response(array('success' => false,'msg' => 'Error updating table'), 500);
		}
	}

	function service_put($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$this->_get_table($table_num);

		// Route receipts
		if($method == 'receipts'){
			$this->receipts_put($recordId, $method2, $recordId2);
			return true;
		}

		// Route cart
		if($method == 'cart'){
			$this->cart_put($recordId);
			return true;
		}

		// Route kitchen orders
		if($method == 'orders'){
			$this->orders_put($recordId);
			return true;
		}

		$requestData = $this->request->Body;
		$this->response(array('success' => true,'msg' => 'TODO: Update table'));
	}

	function service_delete($table_num, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$this->_get_table($table_num);

		// Route receipts
		if($method == 'receipts'){
			$this->receipts_delete($recordId, $method2, $recordId2);
			return true;
		}

		// Route cart
		if($method == 'cart'){
			$this->cart_delete($recordId);
			return true;
		}

		// Route kitchen orders
		if($method == 'orders'){
			$this->orders_delete($recordId);
			return true;
		}

		// Get any payments made on sale
		$payments = $this->Table->get_payments($this->suspended_sale_id);

		// Check if some payments have been made, but not all receipts are paid off, return error
		if(count($payments) > 0 && !$this->table_receipt->is_paid($this->suspended_sale_id)){
			$this->response(array('success' => false, 'msg' => 'Error closing sale, finish paying receipts or refund payments to close sale'), 400);
			return false;
		}
		$success = $this->Table->delete($this->suspended_sale_id);

		if(empty($success)){
			$this->response(array('success' => $success, 'msg' => 'Error closing table service'), 400);

		}else{
			$this->session->unset_userdata('table_number');
			$this->session->unset_userdata('suspended_sale_id');
			$this->response(array('success' => $success, 'msg' => 'Table service closed'));
		}
	}

    /*******************************************************************
     * Recent Transactions
	 ******************************************************************/
	function recent_transactions_get($method = null)
	{
		$this->load->model('Sale');

		// Route sales tips
		if($method == 'tips'){
			$this->recent_transactions_tips_get($sale_id);
			return true;
		}

		// If employee ID passed as parameter, filter by it
		$employee_id = $this->get('employee_id');
		$sales = $this->Table->get_recent_transactions($employee_id, 500);

		if(is_array($sales)){
			$this->response($sales, 200);
		}

		$this->response($sales);
	}

	function recent_transactions_post($sale_id = null, $method = null)
	{
		$this->load->model('Sale');

		// Route sales tips
		if($method == 'tips'){
			$this->recent_transactions_tips_post($sale_id);
			return true;
		}

		$this->response(array('success'=>false,'msg'=>'Method not allowed'), 405);
	}

	function recent_transactions_put($sale_id, $method = null, $record_id = null)
	{
		$this->load->model('Sale');

		// Route sales tips
		if($method == 'tips'){
			$this->recent_transactions_tips_put($sale_id, $record_id);
			return true;
		}

		$this->response(array('success'=>false,'msg'=>'Method not allowed'), 405);
	}

	function recent_transactions_delete($method = null)
	{
		$this->response(array('success'=>false,'msg'=>'Method not allowed'), 405);
	}

    /*******************************************************************
     * Sales Tips
	 ******************************************************************/
	function recent_transactions_tips_get($sale_id = null, $method = null)
	{
		$this->load->model('Sale');

		// Route sales tips
		if($method == 'tips'){
			$this->sales_tips_get($sale_id);
			return true;
		}

		// If employee ID passed as parameter, filter by it
		$employee_id = (int) $this->get('employee_id');
		$sales = $this->Sale->get_recent_transactions(500, $employee_id);

		if(is_array($sales)){
			$this->response($sales, 200);
		}

		$this->response($sales);
	}

	function recent_transactions_tips_put($sale_id = null, $type = null){
		$this->request->body['type'] = urldecode($type);
		$this->recent_transactions_tips_post($sale_id);
	}

	function recent_transactions_tips_post($sale_id = null)
	{
		if(empty($sale_id)){
			$this->response(array('success'=>false, 'msg'=>'Sale ID is required'), 403);
			return false;
		}
		$this->load->model('payment');

		$tip = $this->request->body;
		$gratuity = $tip['amount'];
		$tip_type = $tip['type'];
		$tip_recipient = (int) $tip['tip_recepient'];
		$cc_invoice_id = (int) $tip['invoice_id'];

		if(stripos($tip_type, 'cash') !== false){
			$tip_type = 'Cash';
		}else if(stripos($tip_type, 'check') !== false){
			$tip_type = 'Check';
		}else{
			$tip_type = 'Credit Card';
		}

		// If user is adding tip to a credit card payment
		if (!empty($cc_invoice_id)){

			$payment_info = $this->Sale->get_sale_payment($sale_id, $cc_invoice_id)->row_array();
			$credit_card_info = $this->Sale->get_credit_card_payment($cc_invoice_id);
			$credit_card_info = $credit_card_info[0];

			if(empty($payment_info)){
				$this->response(array('success'=>false, 'msg'=>'Credit card payment does not exist'), 404);
				return false;
			}

			if(empty($tip_recipient)){
				$tip_recipient = (int) $this->session->userdata('person_id');
			}
			$approved = false;

			// If account is using ETS payments
			if ($this->config->item('ets_key')){

				$this->load->library('Hosted_payments');
				$payment = new Hosted_payments();
				$payment->initialize($this->config->item('ets_key'));

				$session = $payment->set('action', 'addTip')
							->set('transactionID', $credit_card_info['payment_id'])
							->set('amount', $gratuity)
							->send();

				if($session->status == 'success'){
					$approved = true;
				}

				$auth_amount = $session->transactions->amount - $gratuity;
				$payment_data = array (
					'auth_amount' => (string) $auth_amount,
					'gratuity_amount' => (string) $gratuity,
				);

			// If account is using Mercury payments
			}else if($this->config->item('mercury_id')){

				$this->load->library('Hosted_checkout_2');
				$HC = new Hosted_checkout_2();
				$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
				$HC->set_invoice($credit_card_info['invoice']);
				$HC->set_token($credit_card_info['token']);
				$HC->set_ref_no($credit_card_info['ref_no']);
				$HC->set_frequency($credit_card_info['frequency']);
				$HC->set_memo($credit_card_info['memo']);
				$HC->set_auth_code($credit_card_info['auth_code']);
				$HC->set_cardholder_name($credit_card_info['cardholder_name']);
				$HC->set_customer_code($credit_card_info['customer_code']);

				$response = $HC->token_transaction('Adjust', $credit_card_info['amount'], $gratuity, $credit_card_info['tax_amount']);

				if ((string) $response->Status == 'Approved'){
					$approved = true;
				}

				$payment_data = array (
					'auth_amount'=> (string) $response->AuthorizeAmount,
					'gratuity_amount' => (string) $gratuity,
					'process_data' => (string) $response->ProcessData,
					'token' => (string) $response->Token
				);
			}

			// If the credit card gratuity transaction was approved
			if($approved){

				//Update credit card payment info
				$this->Sale->update_credit_card_payment($cc_invoice_id, $payment_data);

				//Update sale payment info
				$payment_type = "{$credit_card_info['card_type']} ".substr($credit_card_info['masked_account'], -8)." Tip";
				$sales_payments_data = array(
					'sale_id' => $sale_id,
					'payment_type' => $payment_type,
					'payment_amount' => $payment_data['gratuity_amount'],
					'invoice_id' => $cc_invoice_id,
					'tip_recipient' => $tip_recipient
				);

				$this->payment->add($sales_payments_data);

				// Respond with tip data
				$this->response(array(
					'amount' => $payment_data['gratuity_amount'],
					'type' => $payment_type,
					'invoice_id' => $cc_invoice_id,
					'tip_recipient' => $tip_recipient
				));
				return true;
			}

		// If user is adding other tip type (cash or check)
		} else {

			//Update sale payment info
			$payment_type = "$tip_type ".lang('sales_tip');
			$sales_payments_data = array(
				'sale_id' => $sale_id,
				'payment_type' => $payment_type,
				'payment_amount' => $gratuity,
				'invoice_id' => 0,
				'tip_recipient' => $tip_recipient
			);

			if($this->payment->add($sales_payments_data)){
				$this->response(array(
					'amount' => $gratuity,
					'type' => $payment_type,
					'invoice_id' => 0,
					'tip_recipient' => $tip_recipient
				));
				return true;
			}
		}

		$this->response(array('success' => false, 'msg' => "Unable to add tip at this time. Please try again later. Note: Tips must be added before end of day."), 500);
		return false;
	}

    /*******************************************************************
     * Register Log
	 ******************************************************************/
	function register_log_get($sale_id = null)
	{
		$this->response($sales);
	}

	function register_log_post($register_log_id = null)
	{
		$this->load->model('Sale');
		$data = $this->request->body;

		$now = date('Y-m-d H:i:s');
		$cash_register = new stdClass();
		$cash_register->employee_id = $this->session->userdata('person_id');
		$cash_register->terminal_id = $this->session->userdata('terminal_id');
		$cash_register->open_amount = $data['open_amount'];
		$cash_register->close_amount = 0;
		$cash_register->shift_end = '0000-00-00 00:00:00';
		$cash_register->course_id = $this->session->userdata('course_id');

		$cash_register->shift_start = $now;
		$logId = $this->Sale->insertRegisterLog($cash_register);

		if($logId){
			$cash_register->register_log_id = (int) $logId;
			$this->response($cash_register);
		}else{
			$this->response(array('success'=>false, 'msg'=>'Error saving register log'), 500);
		}
	}

	function register_log_put($register_log_id = null)
	{
		$this->load->model('Sale');
		$data = elements(array('close_amount', 'change', 'ones', 'fives', 'tens', 'twenties', 'fifties', 'hundreds'), $this->request->body, 0);
		$now = date('Y-m-d H:i:s');

		if($data['close_amount'] <= 0){
			$this->response(array('success'=>false,'msg'=>'Close amount is required'), 403);
			return false;
		}
		$cash_register = $this->Sale->getUnfinishedRegisterLog()->row();

		$cash_register->shift_end = $now;
		$cash_register->close_amount = $data['close_amount'];
		$counts = array(
			'register_log_id' => $cash_register->register_log_id,
			'change' => $data['change'],
			'ones' => $data['ones'],
			'fives' => $data['fives'],
			'tens' => $data['tens'],
			'twenties' => $data['twenties'],
			'fifties' => $data['fifties'],
			'hundreds' => $data['hundreds']
		);

		$cash_register->cash_sales_amount = $this->Sale->get_cash_sales_total_for_shift($cash_register->shift_start, $cash_register->shift_end);
		$cash_register->course_id = $this->session->userdata('course_id');

		$success = $this->Sale->updateRegisterLog($cash_register);
		$this->Sale->saveRegisterLogCounts($counts);

		$this->session->unset_userdata('skipped_register_log');
		$this->session->unset_userdata('cash_register');
		$this->session->unset_userdata('unfinished_register_log');

		$this->response(array('success'=>$success, 'msg'=>'Register log updated'));
	}

	function register_log_delete($method = null)
	{
		$this->response(array('success'=>false,'msg'=>'Method not allowed'), 405);
	}

    /*******************************************************************
     * Receipts
	 ******************************************************************/
	function receipts_get($receipt_id = false, $method = null, $record_id = null)
	{
		if($method == 'items'){
			$this->receipt_items_get($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_get($receipt_id, $record_id);
			return true;
		}

		$receipts = $this->table_receipt->get($this->suspended_sale_id, $receipt_id);
		$this->response($receipts);
	}

	function receipts_post($receipt_id = false, $method = null, $record_id = null)
	{
		if($method == 'items'){
			$this->receipt_items_post($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_post($receipt_id, $record_id);
			return true;
		}

		$success = $this->table_receipt->save($this->suspended_sale_id, null);
		$this->response(array('success'=>true,'msg'=>'All receipts for table'));
	}

	function receipts_put($receipt_id = false, $method = null, $record_id = null)
	{
		if($method == 'items'){
			$this->receipt_items_put($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_put($receipt_id, $record_id);
			return true;
		}

		$receiptData = $this->request->body;

		$success = $this->table_receipt->save($this->suspended_sale_id, $receipt_id);
		$this->response(array('success'=>true,'msg'=>'Receipt updated'));
	}

	function receipts_delete($receipt_id = false, $method = null, $record_id = null)
	{
		if($method == 'items'){
			$this->receipt_items_delete($receipt_id, $record_id);
			return true;
		}

		if($method == 'payments'){
			$this->receipt_payments_delete($receipt_id, $record_id);
			return true;
		}

		if($receipt_id === false){
			$this->response(array('msg'=>'Can\'t delete all receipts at once.'), 400);
			return false;
		}

		if((int) $receipt_id == 1){
			$this->response(array('msg'=>'Can\'t delete receipt #1'), 400);
			return false;
		}

		if(!$this->table_receipt->can_delete($this->suspended_sale_id, $receipt_id)){
			$this->response(array('msg'=>'Receipt must be empty to delete'), 400);
			return false;
		}

		$success = $this->table_receipt->delete($this->suspended_sale_id, $receipt_id);
		$this->response(array('success' => $success, 'msg'=>'Receipt deleted'));
	}

    /*******************************************************************
     * Receipt items
	 ******************************************************************/
	function receipt_items_get($receiptId, $line = null)
	{
		$items = $this->table_receipt->get_items($this->suspended_sale_id, $receiptId);
		if(is_array($items)){
			$this->response($items, 200);
		}
		$this->response($items);
	}

	function receipt_items_post($receiptId, $line = null)
	{
		$itemData = $this->post();
		$success = $this->table_receipt->add_item($itemData['item_id'], $itemData['line'], $this->suspended_sale_id, $receiptId);
		$this->response(array('success'=>$success,'msg'=>'Receipt item added'));
	}

	function receipt_items_put($receiptId, $line = null)
	{
		$itemData = $this->request->body;
		$success = $this->table_receipt->add_item($itemData['item_id'], $itemData['line'], $this->suspended_sale_id, $receiptId);
		$this->response(array('success'=>$success,'msg'=>'Receipt item updated'));
	}

	function receipt_items_delete($receiptId, $line = null)
	{
		$itemData = $this->request->body;

		if(!$this->table_receipt->can_delete_item($this->suspended_sale_id, $line)){
			$this->response(array('success'=>false, 'msg'=>'Item must belong to at least 1 receipt'), 400);
			return false;
		}

		$success = $this->table_receipt->delete_item(0, $line, $this->suspended_sale_id, $receiptId);
		$this->response(array('success'=>true, 'msg'=>'Receipt item deleted'));
	}

    /*******************************************************************
     * Receipt payments
	 ******************************************************************/
	function receipt_payments_get($receiptId, $line = null)
	{
		$items = $this->Table->get_payments($this->suspended_sale_id, $receiptId);
		if(is_array($items)){
			$this->response($items, 200);
		}
		$this->response($items);
	}

	function receipt_payments_post($receiptId, $line = null)
	{
		$payment = $this->request->body;
		$msg = null;

		if(empty($payment)){
			$payment = $this->post();
		}

		// Handle Mercury credit card payments a bit differently
		if(!empty($payment['Payment_ID'])){
			$payment['type'] = 'creditcard-mercury';

		}else{
			// Make sure payment type is passed
			if(empty($payment['type'])){
				$this->response(array('success'=>false, 'msg'=>'Payment type is required'), 400);
				return false;
			}
			// Make sure payment amount is over 0
			if(empty($payment['amount']) || $payment['amount'] <= 0){
				$this->response(array('success'=>false, 'msg'=>'Amount must be greater than 0'), 400);
				return false;
			}
			$payment['amount'] = (float) round($payment['amount'], 2);
		}

		// Process payment depending on type
		switch(strtolower($payment['type'])){
			case 'cash':
			case 'check':
			case 'credit card':
			break;
			case 'gift card':
				$this->load->model('Giftcard');

				if(empty($payment['card_number'])){
					$this->response(array('success'=>false, 'msg'=>'Gift card number is required'), 400);
					return false;
				}
				$giftcard_id = $this->Giftcard->get_giftcard_id($payment['card_number']);

				// If gift card doesn't exist, or is expired, return error
				if(empty($giftcard_id) || $this->Giftcard->is_expired($giftcard_id)){
					$this->response(array('success'=> false, 'msg'=>lang('sales_giftcard_does_not_exist')), 400);
					return false;
				}

				$giftcard_info = $this->Giftcard->get_info($giftcard_id);
				$valid_amount = $this->sale_lib->get_valid_amount($giftcard_info->department, $giftcard_info->category);

				$cur_giftcard_value =  $giftcard_value - $current_payments_with_giftcard;

				if ($cur_giftcard_value <= 0 && $amount_tendered > 0){
					$this->response(array('success'=>false, 'msg'=>lang('sales_giftcard_balance_is').' '.to_currency($giftcard_value)), 400);
					return false;
				}

				if (($cur_giftcard_value - $amount_tendered) > 0){
					$msg = lang('sales_giftcard_balance_is').' '.to_currency($cur_giftcard_value);
				}
				$payment['type'] = 'Gift Card:'.$payment['card_number'];

			break;
			case 'creditcard-ets':
				if(empty($payment['id'])){
					$this->response(array('success'=>false, 'msg'=>'Credit card session ID is required'), 400);
					return false;
				}
				if(empty($payment['created'])){
					$this->response(array('success'=>false, 'msg'=>'Created date is required'), 400);
					return false;
				}
				if(empty($payment['transactions'])){
					$this->response(array('success'=>false, 'msg'=>'Credit card transaction data required'), 400);
					return false;
				}
				$this->load->model('sale');

				// Verify ETS transaction
				$this->load->library('Hosted_payments');
				$hosted_payment = new Hosted_payments();
				$hosted_payment->initialize($this->config->item('ets_key'));
				$session_id = $hosted_payment->get("session_id");

				$transaction_id = $payment['transactions']['id'];

				$hosted_payment->set("action", "verify")
					->set("sessionID", $paymment['id'])
					->set("transactionID", $transaction_id);

				$account_id = $payment['customers']['id'];
				if ($account_id){
					$hosted_payment->set('accountID', $account_id);
				}

				$verify = $hosted_payment->send();
				$transaction_time = date('Y-m-d H:i:s', strtotime($payment['created']));

				// Convert card type to match mercury card types
				if ((string) $payment['transactions']['type'] == 'credit card') {
					$ets_card_type = $payment['transactions']['cardType'];
					$card_type = '';
					switch($ets_card_type){
						case 'MasterCard':
							$card_type = 'M/C';
						break;
						case 'Visa':
							$card_type = 'VISA';
						break;
						case 'Discover':
							$card_type = 'DCVR';
						break;
						case 'AmericanExpress':
							$card_type = 'AMEX';
						break;
						case 'Diners':
							$card_type = 'DINERS';
						break;
						case 'JCB':
							$card_type = 'JCB';
						break;
					}
					$masked_account = (string) $payment['transactions']['cardNumber'];
					$auth_code = (string) $payment['transactions']['approvalCode'];
				}
				else
				{
					$card_type = 'Bank Acct';
					$masked_account = (string) $payment['transactions']['accountNumber'];
					$auth_code = '';
				}

				$credit_card_data = array(
					'course_id' => $this->session->userdata('selected_course'),
					'card_type' => $card_type,
					'masked_account' => $masked_account,
					'cardholder_name' => ''
				);

				//Update credit card payment data
				$trans_message = (string) $payment['transactions']['message'];
				$payment_data = array (
					'course_id' 		=> $this->session->userdata('course_id'),
					'masked_account' 	=> $masked_account,
					'trans_post_time' 	=> (string) $transaction_time,
					'ets_id' 			=> $this->config->item('ets_key'),
					'tran_type' 		=> 'Sale',
					'amount' 			=> (string) $payment['transactions']['amount'],
					'auth_amount'		=> (string)	$payment['transactions']['amount'],
					'auth_code'			=> $auth_code,
					'card_type' 		=> $card_type,
					'frequency' 		=> 'OneTime',
					'payment_id' 		=> (string) $payment['transactions']['id'],
					'process_data'		=> (string) $payment['id'],
					'status'			=> (string) $payment['transactions']['status'],
					'status_message'	=> (string) $payment['transactions']['message'],
					'display_message'	=> (string) $payment['message'],
					'operator_id'		=> (string) $this->session->userdata('person_id')
				);

				$primary_store = (string) $payment['store']['primary'];
				$this->sale->update_credit_card_payment($primary_store, $payment_data);
				$this->session->set_userdata('invoice_id', $primary_store);
				$payment_data['payment_type'] = $payment_data['card_type'].' '.$payment_data['masked_account'];

				// Data to return
				$payment['type'] = $payment_data['payment_type'];
				$payment['card_type'] = $card_type;
				$payment['card_number'] = $masked_account;
				$payment['credit_card_invoice_id'] = $primary_store;
			break;

			case 'creditcard-mercury':
				$payment_id = $payment['Payment_ID'];

				$this->load->library('Hosted_checkout_2');
				$HC = new Hosted_checkout_2();
				$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
				$HC->set_payment_id($payment_id);
				$verify_results = $HC->verify_payment();
				$HC->complete_payment();
				$invoice = $this->session->userdata('invoice_id');

				$credit_card_data = array(
					'course_id'=>$this->session->userdata('selected_course'),
					'token'=>(string)$verify_results->Token,
					'token_expiration'=>date('Y-m-d', strtotime('+2 years')),
					'card_type'=>(string)$verify_results->CardType,
					'masked_account'=>(string)$verify_results->MaskedAccount,
					'cardholder_name'=>(string)$verify_results->CardholderName
				);

				//Update credit card payment data
				$payment_data = array (
					'course_id'=>$this->session->userdata('course_id'),
					'mercury_id'=>$this->config->item('mercury_id'),
					'tran_type'=>(string)$verify_results->TranType,
					'amount'=>(string)$verify_results->Amount,
					'auth_amount'=>(string)$verify_results->AuthAmount,
					'card_type'=>(string)$verify_results->CardType,
					'frequency'=>'OneTime',
					'masked_account'=>(string)$verify_results->MaskedAccount,
					'cardholder_name'=>(string)$verify_results->CardholderName,
					'ref_no'=>(string)$verify_results->RefNo,
					'operator_id'=>(string)$verify_results->OperatorID,
					'terminal_name'=>(string)$verify_results->TerminalName,
					'trans_post_time'=>(string)$verify_results->TransPostTime,
					'auth_code'=>(string)$verify_results->AuthCode,
					'voice_auth_code'=>(string)$verify_results->VoiceAuthCode,
					'payment_id'=>$payment_id,
					'acq_ref_data'=>(string)$verify_results->AcqRefData,
					'process_data'=>(string)$verify_results->ProcessData,
					'token'=>(string)$verify_results->Token,
					'response_code'=>(int)$verify_results->ResponseCode,
					'status'=>(string)$verify_results->Status,
					'status_message'=>(string)$verify_results->StatusMessage,
					'display_message'=>(string)$verify_results->DisplayMessage,
					'avs_result'=>(string)$verify_results->AvsResult,
					'cvv_result'=>(string)$verify_results->CvvResult,
					'tax_amount'=>(string)$verify_results->TaxAmount,
					'avs_address'=>(string)$verify_results->AVSAddress,
					'avs_zip'=>(string)$verify_results->AVSZip,
					'payment_id_expired'=>(string)$verify_results->PaymendIDExpired,
					'customer_code'=>(string)$verify_results->CustomerCode,
					'memo'=>(string)$verify_results->Memo
				);

				$this->sale->update_credit_card_payment($invoice, $payment_data);
				$payment_data['payment_type'] = $payment_data['card_type'].' '.$payment_data['masked_account'];

				// Data to return
				$payment['type'] = $payment_data['payment_type'];
				$payment['card_type'] = $payment_data['card_type'];
				$payment['card_number'] = $payment_data['masked_account'];
				$payment['credit_card_invoice_id'] = $invoice;
				$payment['amount'] = $payment_data['auth_amount'];
			break;
			default:
				$this->response(array('success'=>false, 'msg'=>'Payment type is invalid'), 400);
				return false;
			break;
		}
		$this->session->set_userdata('taxable', true);
		$success = $this->Table->add_payment($this->suspended_sale_id, $receiptId, $payment['type'], $payment['amount'], $payment['credit_card_invoice_id']);

		// If payment was successfully added
		if($success){
			$newPayment = $this->Table->get_payments($this->suspended_sale_id, $receiptId, $payment['type']);
			$returnData = array('type' => $payment['type'], 'amount' => (float) $newPayment['payment_amount']);

			if(!empty($payment['card_type'])){
				$returnData['card_type'] = $payment['card_type'];
			}
			if(!empty($payment['card_number'])){
				$returnData['card_number'] = $payment['card_number'];
			}

			// Retrieve all data for receipt (items, payments, etc)
			$receiptData = $this->table_receipt->get($this->suspended_sale_id, $receiptId);

			// If receipt is now fully paid, complete the sale
			if($receiptData['total_due'] <= 0){

				// If receipt is overpaid, add 'Change issued' payment
				if($receiptData['total_due'] < 0){
					$changePayment= array(
						'type' => 'Change issued',
						'amount' => $receiptData['total_due'],
						'credit_card_invoice_id' => 0
					);
					$this->Table->add_payment($this->suspended_sale_id, $receiptId, $changePayment['type'], $changePayment['amount'], $changePayment['credit_card_invoice_id']);

					// Manually add payment to current receipt data
					$receiptData['payments'][] = $changePayment;
				}
				$this->load->model('Sale');
				$this->table_receipt->mark_paid($this->suspended_sale_id, $receiptId);

				// Gather up receipt data to complete the sale
				$customer_id = -1; // TODO
				$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
				$comment = '';
				$payments = array();
				$items = array();
				$sideTypes = array('soups', 'salads', 'sides');

				// Loop through items, add split prices as the default price
				// Split off side dishes as their own items
				$lineCount = 1;
				foreach($receiptData['items'] as $key => $itemData){
					$item = $itemData;

					// Overwrite regular price with split price on all items
					$item['total'] = $item['split_total'];
					$item['price'] = $item['split_price'];
					$item['subtotal'] = $item['split_subtotal'];
					$item['tax'] = $item['split_tax'];
					$item['line'] = $lineCount;

					$items[] = $item;
					$lineCount++;

					// Loop through the various side dishes associated with each item
					foreach($sideTypes as $sideType){

						if(!empty($item[$sideType])){

							foreach($item[$sideType] as $side){
								// If side is 'none' skip it
								if((int) $side['item_id'] == 0){ continue; }

								$side['line'] = $lineCount;
								$side['total'] = $side['split_total'];
								$side['price'] = $side['split_price'];
								$side['subtotal'] = $side['split_subtotal'];
								$side['tax'] = $side['split_tax'];
								$items[] = $side;

								unset($items[$lineCount - 1][$sideType]);
								$lineCount++;
							}
						}
					}
				}

				// Conform payments to play nice with sale->save method
				foreach($receiptData['payments'] as $key => $payment){
					$payments[$key]['payment_type'] = ucfirst($payment['type']);
					$payments[$key]['payment_amount'] = $payment['amount'];
					$payments[$key]['invoice_id'] = $payment['credit_card_invoice_id'];
				}

				// Complete sale (places all data in sales table)
				$sale_id = $this->Sale->save($items, $customer_id, $employee_id, $comment, $payments, false, false, false, null, false, $this->suspended_sale_id);
			}

			// Format list of items from receipt as array
			$items = array();
			foreach($receiptData['items'] as $item){
				$items[] = (int) $item['line'];
			}

			// Mark all items in cart (that are in receipt) as paid
			$this->Table->mark_items_paid($this->suspended_sale_id, $items);
			$this->response($returnData);

		}else{
			$this->response(array('success'=>$success));
		}
	}

	function receipt_payments_put($receiptId, $paymentType = null)
	{
		return $this->receipt_payments_post($receiptId);
	}

	function receipt_payments_delete($receiptId, $paymentType = null)
	{
		$itemData = $this->request->body;
		$paymentType = urldecode($paymentType);
		$paymentType = str_replace('-', '/', $paymentType);

		// Get payment data
		$paymentData = $this->Table->get_payments($this->suspended_sale_id, $receiptId, $paymentType);
		$refundSuccess = true;

		// If payment being deleted is from credit card, refund the payment back to card
		if($paymentData['credit_card_invoice_id'] != 0){

			$refundSuccess = false;
			$this->load->model('sale');
			$cc_payment = $this->sale->get_credit_card_payment($paymentData['credit_card_invoice_id']);
			$cc_payment = $cc_payment[0];
			$amount = $cc_payment['amount'] + $cc_payment['gratuity_amount'];

			// If course is using ETS for payments
			if($this->config->item('ets_key')){

				// If payment hasn't been refunded already
				if ($cc_payment['amount_refunded'] <= 0){

					if($cc_payment['process_data'] != ''){
						$this->load->library('Hosted_payments');
						$hosted_payment = new Hosted_payments();
						$hosted_payment->initialize($this->config->item('ets_key'));

						$ets_response = $hosted_payment->set('action', 'void')
									->set('transactionID', $cc_payment['payment_id'])
									->set('sessionID', $cc_payment['process_data'])
									->send();

						if($ets_response->status == 'success'){
							$refundSuccess = true;
							$sql = $this->sale->add_ets_refund_payment($paymentData['credit_card_invoice_id'], $amount);
						}
					}
				}else{
					$refundSuccess = true;
				}

			// If course is using Mercury for payments
			}else if($this->config->item('mercury_id')){

				$this->load->library('Hosted_checkout_2');
				$HC = new Hosted_checkout_2();
				$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
				$HC->set_invoice($paymentData['credit_card_invoice_id']);
				$HC->set_cardholder_name($cc_payment['cardholder_name']);
				$HC->set_token($cc_payment['token']);
				$response = $HC->issue_refund($paymentData['credit_card_invoice_id'], $amount);

				if ((int) $response->ResponseCode === 0 && $response->Status == 'Approved'){
					$refundSuccess = true;
					$sql = $this->Sale->add_refund_payment($paymentData['credit_card_invoice_id'], $amount, $response);
				}
			}
		}

		if(!$refundSuccess){
			$this->response(array('success'=>false, 'msg'=>'Error refunding credit card payment'), 500);
			return false;
		}

		$success = $this->Table->delete_payment($this->suspended_sale_id, $receiptId, $paymentType);

		if($success){
			$receiptData = $this->table_receipt->get($this->suspended_sale_id, $receiptId);

			// If all payments were deleted from receipt, mark items in cart as UNPAID (that belong to receipt)
			if($receiptData['amount_paid'] <= 0){
				$items = array();
				foreach($receiptData['items'] as $item){
					$items[] = (int) $item['line'];
				}
				// Mark all items in cart (that are in receipt) as unpaid
				$this->Table->mark_items_unpaid($this->suspended_sale_id, $items);
			}
		}

		$this->response(array('success'=>true, 'msg'=>'Payment deleted'));
	}

    /*******************************************************************
     * Cart
	 ******************************************************************/
	function cart_get($line)
	{
		// If getting all items in cart
		if(empty($line)){
			$result = $this->Table->get_sale_items($this->suspended_sale_id);
			$rows = $result->result_array();
			$items = array();

			// Loop through rows and conform them to proper item structure
			foreach($rows as $row){
				$item = array(
					'item_id' => $row['item_id'],
					'line' => $row['line'],
					'description' => $row['description'],
					'serialnumber' => $row['serialnumber'],
					'seat' => $row['seat'],
					'quantity' => $row['quantity_purchased'],
					'cost' => $row['item_cost_price'],
					'price' => $row['item_unit_price'],
					'discount' => $row['discount_percent']
				);

				$items[] = $item;
			}
			$this->response($items);

		// If getting just one item
		}else{
			$this->response(array('TODO: data here'));
		}
	}

	function cart_post($line)
	{
		$itemData = $this->post();
		$result = $this->Table->save_item($this->suspended_sale_id, $itemData);
		$this->response(array('msg'=>'Post cart'));
	}

	function cart_put($line)
	{
		$itemData = $this->request->body;

		$item = $this->Item->get_info((int) $itemData['item_id']);
		$minPrice = $this->sale_lib->calculate_subtotal($item->unit_price, 1, $item->max_discount);
		$setPrice = $this->sale_lib->calculate_subtotal($itemData['price'], 1, $itemData['discount']);

		// Validate the item price
		if($setPrice < $minPrice){
			$this->response(array('success'=>false, 'msg'=>'Error saving item. Maximum discount is ' . $maxDiscount. '% or '. '$'.$minPrice), 400);
			return false;
		}

		// Save item to table cart
		$success = $this->Table->save_item($this->suspended_sale_id, $line, $itemData);

		// If item is new and not currently on a receipt
		// Automatically place item on next available receipt
		if(!$this->table_receipt->is_on_receipt($this->suspended_sale_id, $line)){
			$receipt_id = $this->table_receipt->get_available_receipt($this->suspended_sale_id);
			$this->table_receipt->add_item($itemData['item_id'], $itemData['line'], $this->suspended_sale_id, $receipt_id);
		}

		$this->response(array('success'=>$success, 'msg'=>'Item added to cart'));
	}

	function cart_delete($line)
	{
		$success = $this->Table->delete_item($this->suspended_sale_id, $line);
		$this->response(array('success'=>$success, 'msg'=>'Item removed from cart'));
	}

    /*******************************************************************
     * Kitchen Order - Post item line numbers to have them sent to kitchen
	 ******************************************************************/
	function orders_get($order_id)
	{
		$this->response(array('msg'=>'TODO: get order data'));
	}

	function orders_post($order_id)
	{
		$orderData = $this->request->body;
		if(empty($orderData['items'])){
			$this->response(array('msg'=>'Items can not be empty'), 400);
			return false;
		}
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		$ticket_id = $this->table_ticket->save($this->suspended_sale_id, $this->table_num, $employee_id, $orderData['items'], $orderData['message']);

		if(empty($ticket_id)){
			$this->response(array('success'=>false, 'msg'=>'Error placing order'), 500);
			return false;
		}

		$lines = array();
		foreach($orderData['items'] as $item){
			$lines[] = $item['line'];
		}
		$this->Table->mark_items_ordered($this->suspended_sale_id, $lines);

		// Get newly created ticket data to respond with
		$ticket = $this->table_ticket->get($this->suspended_sale_id, $ticket_id);
		$this->response($ticket);
	}

	function orders_put($order_id)
	{
		$itemData = $this->request->body;
		$this->response(array('msg'=>'TODO: edit order'));
	}

	function orders_delete($order_id)
	{
		$success = $this->Table->delete_item($this->suspended_sale_id, $line);
		$this->response(array('success'=>$success, 'msg'=>'Item removed from cart'));
	}
}
