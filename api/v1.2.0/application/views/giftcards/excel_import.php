<?php
echo form_open_multipart('giftcards/do_excel_import/',array('id'=>'giftcard_form'));
?>
<div id="required_fields_message"><?php echo lang('giftcard_mass_import_from_excel'); ?></div>
<ul id="error_message_box"></ul>
<b><a id='template_link' href="<?php echo site_url('giftcards/excel'); ?>"><?php echo lang('giftcards_download_import_template'); ?></a></b>
<fieldset id="giftcard_basic_info">
<legend><?php echo lang('giftcards_import'); ?></legend>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_file_path').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_upload(array(
		'name'=>'file_path',
		'id'=>'file_path',
		'value'=>'')
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submitf',
	'id'=>'submitf',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{	
    var submitting = false;
    $('#giftcard_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
                        beforeSubmit:function(formData, jqForm, options){
                            var queryString = $.param(formData); 
 
                            // jqForm is a jQuery object encapsulating the form element.  To access the 
                            // DOM element for the form do this: 
                            // var formElement = jqForm[0]; 

                            //alert('About to submit: \n\n' + queryString); 
                        },
                        success:function(response)
			{
           		$.colorbox.close();
				post_giftcard_form_submit(response);
                submitting = false;
			},
			dataType:'json'
                });

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			file_path:"required"
   		},
		messages: 
		{
   			file_path:"<?php echo lang('giftcards_full_path_to_excel_file_required'); ?>"
		}
	});
});
</script>