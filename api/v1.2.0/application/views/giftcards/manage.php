<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function()
{
	$('#giftcards_view').click(function(){
    	$('#giftcards_radio').click();
		do_card_type_switch(true, '', '550', $('input[name=card_type]:checked').val());
		$('#card_view_buttons .selected').removeClass('selected');
		$(this).addClass('selected');
	});
	$('#punch_cards_view').click(function(){
		$('#punch_cards_radio').click();
		do_card_type_switch(true, '', '550', $('input[name=card_type]:checked').val());
		$('#card_view_buttons .selected').removeClass('selected');
		$(this).addClass('selected');
	});
	$('input[name=card_type]').change(function(){
        $('#search_type').val($(this).val());
        console.log('Searching: ' + $(this).val());
        $('#new_item').attr('href', '<?php echo "index.php/$controller_name/view_new_item/-1/width~1100/"?>' + $(this).val());
        do_card_type_switch(true, '', '550', $(this).val());
        enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>', '', $('#search_type').val());
        
    });	
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({'maxHeight':700, 'width':550});
    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>', '', $('#search_type').val());
    enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
	enable_cleanup('<?php echo lang("giftcards_confirm_cleanup")?>');
	
	$('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("giftcards/generate_barcodes");?>/'+selected.join('~'));
    });

	$('#generate_barcode_labels').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("giftcards/generate_barcode_labels");?>/'+selected.join('~'));
    });
});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[1,0]],
			headers:
			{
				0: { sorter: false},
				6: { sorter: false}
			}
		});
	}
}

function post_giftcard_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.giftcard_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.giftcard_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.giftcard_id);
				set_feedback(response.message,'success_message',false);
			});
		}
	}
}

</script>

<!--table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('common_list_of').' '.lang('module_'.$controller_name); ?>
		</td>
		<td id="title_search">
			<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
				<input type="text" name ='search' id='search'/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table-->


<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php echo 
					anchor("$controller_name/view/-1",
					lang($controller_name.'_new'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_new')));
				?>
				
				<?php echo 
					anchor("$controller_name/generate_barcode_labels",
					lang("common_barcode_labels"),
					array('id'=>'generate_barcode_labels', 
						'class' => 'generate_barcodes_inactive',
						'target' =>'_blank',
						'title'=>lang('common_barcode_labels'))); 
				?>
				
				<?php echo 
					anchor("$controller_name/generate_barcodes",
					lang("common_barcode_sheet"),
					array('id'=>'generate_barcodes', 
						'class' => 'generate_barcodes_inactive',
						'target' =>'_blank',
						'title'=>lang('common_barcode_sheet'))); 
				?>
					
				<?php echo anchor("$controller_name/excel_import/",
                   lang('giftcards_import'),
                   array('class'=>'colbox none import','title'=>lang('giftcards_import_giftcards_from_excel')));
                ?>
                 <?php echo anchor("$controller_name/excel_export",
					lang('giftcards_export'),
					array('class'=>'none import'));
				?>
				<?php echo 
					anchor("$controller_name/delete",
					lang("common_delete"),
					array('id'=>'delete', 
						'class'=>'delete_inactive')); 
				?>
                <!--?php echo anchor("$controller_name/cleanup",
					lang("giftcards_cleanup_old_giftcards"),
					array('id'=>'cleanup', 'class'=>'cleanup')); 
				?-->

			
			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
			<?php
			echo form_open("$controller_name/switch_card_type", array('id'=>'switch_cards'));
            ?>
                <div id='card_view_buttons'>
		       		<ul>
		       			<!--li id='month_view'>Month</li-->
		       			<li id='giftcards_view' class='selected'>Giftcards</li>
		       			<li id='punch_cards_view' class='last'>Punch Cards</li>
		       		</ul>
		       </div>  
		       <div style='display:none'>
		       <input type='radio' id='giftcards_radio' name='card_type' value='0' checked/><label for='giftcards' id='giftcards_label'>Giftcards</label>  
               <input type='radio' id='punch_cards_radio' name='card_type' value='1'/><label for='punch_cards' id='punch_cards_label'>Punch Cards</label>
        	</div>
            </form>
			<div id='table_top'>
				<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
				    <input type="hidden" name="search_type" id="search_type" value="0"/>
					<input type="text" name ='search' id='search' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form>
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>
	