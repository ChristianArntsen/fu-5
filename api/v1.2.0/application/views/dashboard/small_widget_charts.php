<?php if ($test) { ?>
<!DOCTYPE html>
<html>
  <head>
	<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery-ui.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/datepicker.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/highcharts.src.js"></script>
	<script src="<?php echo base_url();?>js/exporting.src.js"></script>
	<script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.loadmask.css?<?php echo APPLICATION_VERSION; ?>" />
	<script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script type="text/javascript" src="/js/gray.js"></script>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/redmond.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/datepicker.css?<?php echo APPLICATION_VERSION; ?>" />
<?php } ?>

    <script type="text/javascript">
var <?=$container_name?>_chart;

$(function () {
	var type = '<?=$type?>';
    var graph_type = '<?=$graph_type?>';
    var start = '<?=$start?>';
    var end = '<?=$end?>';
    $(document).ready(function() {
	    $.ajax({
	           type: "POST",
	           url: "<?php echo base_url();?>index.php/dashboard/fetch_<?=$data_source?>_data/",
	           data: 'type='+type+'&start='+start+'&end='+end,
	           success: function(response){
	           	console.log(response.data);

	           	var colors = Highcharts.getOptions().colors,
		            categories = ['Missed','Complete'],
		            name = 'Tee Times',
		            data = [{
		                    y: 22,
		                    color: colors[5],
		                }, {
		                    y: 75,
		                    color: colors[2],
		                }];

		        // Build the data arrays
		        var browserData = [];
		        var versionsData = [];
		        for (var i = 0; i < data.length; i++) {

		            // add browser data
		            browserData.push({
		                name: categories[i],
		                y: data[i].y,
		                color: data[i].color
		            });
		        }
		        // console.log(browserData);
		        // console.log(browserData);

		        // Create the chart
		        <?=$container_name?>_chart = new Highcharts.Chart({
		            chart: {
		                renderTo: <?=$container_name?>,
		                type: 'pie',
		                backgroundColor:'transparent'
		            },
		            title: {
		                text: ''
		            },
		            yAxis: {
		                title: {
		                    // text: 'Total percent market share'
		                }
		            },
		            plotOptions: {
		                pie: {
		                    shadow: false
		                }
		            },
		            tooltip: {
		                valueSuffix: '%'
		            },
		            series: [{
		                name: response.title,
		                data: response.data,
		                innerSize: '40%',
		                dataLabels: {
		                    formatter: function() {
		                        // return this.y > 5 ? this.point.name : null;
		                    },
		                    color: 'white',
		                    distance: -30
		                }
		             }]
		        });


	           },
            dataType:'json'//end of success
    	});//end of ajax

    });//end of document.ready
});//end of function

		</script>

  </head>
  <body>
  	<style>
		.dashboard_filters {
			background:#555555;
			-webkit-border-radius:14px;
			border-radius:14px;
			margin-bottom:5px;
			padding:10px 0px 3px 0px;
			height: 60px !important;
		}
		.dashboard_widget:hover .dashboard_filters {
		}
		.dashboard_filters_inner {
			padding:0px 10px 0px 10px;
			float:right;
		}
		.dashboard_filters_inner input{
			/*background:url("../images/pieces/search.png") no-repeat scroll right center transparent;
		    border: medium none;
		    font-size: 0.65em;
		    padding: 6px 10px;
		    vertical-align: top;
		    width: 205px;*/
		}
		.filter_date {
			margin:0px 5px;
		}

		.chart_label{
			position: relative;
			left: 200px;
			bottom: 100px;
		}
	</style>

	<div class='dashboard_widget' style="width: <?=$width?>px; height: <?=$height?>px;">
		<?php
			if (!$static) { ?>
				<div id="<?=$container_name?>_filters" class='dashboard_filters' style="width: <?=$width?>px; height: 30px;">
				<div class='dashboard_filters_inner'>
					<?php
					echo form_open("dashboard/fetch_{$data_source}_data",array('id'=>$container_name.'_form'));
					echo form_input(array(
						'name'=>'start',
						'id'=>$container_name.'_start',
						'class'=>'filter_date',
						'placeholder'=>'Start date',
						'value'=>$start));
					echo form_input(array(
						'name'=>'end',
						'id'=>$container_name.'_end',
						'class'=>'filter_date',
						'placeholder'=>'End date',
						'value'=>$end));
					echo form_dropdown('type',array('hour'=>'Hourly','day'=>'Daily','dayname'=>'Day of Week','week'=>'Weekly','month'=>'Monthly','year'=>'Yearly'), $type, '', $container_name.'_type');
					echo form_submit(array(
						'name'=>'submit',
						'id'=>$container_name.'_submit',
						'value'=>lang('common_submit'),
						'class'=>'submit_button float_right')
					);
					?>
					</form>
				</div>
			</div>
			<?php }
		?>
		<div id="<?=$container_name?>" style="width: <?=$width?>px;"></div>
		<label class="chart_label"><?=$label?></label>
		<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
	</div>
<?php if ($test) { ?>
  </body>
</html>
<?php } ?>