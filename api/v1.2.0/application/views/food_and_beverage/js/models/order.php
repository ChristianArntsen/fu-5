var Order = Backbone.Model.extend({
	url: function(){
		return this.instanceUrl;
	},
	idAttribute: "ticket_id",
	defaults: {
		"ticket_id": null,
		"items": null,
		"message": null,
		"printed": false,
		"deleted": false,
		"completed": false,
		"date_created": null,
		"date_ordered": null,
		"date_completed": null
	},

	initialize: function(attributes, options){

		if(options && options.url){
			this.instanceUrl = options.url;
		}

		// When order is made, mark items in cart as "ordered"
		this.listenTo(this, 'sync', this.markItems, this);

		// When order is made, print order receipt in kitchen
		this.listenTo(this, 'sync', this.printOrder, this);
		this.listenTo(this, 'invalid', displayError, this);
	},

	// Override the default backbone save function
	// We only need to send certain fields to the server when making an order
    save: function(attrs, options) {
		options || (options = {});

		// Only send line numbers to order function
		var modifiedAttrs = {'items':[], 'message':this.get('message')};
		_.each(this.get('items'), function(item){
			modifiedAttrs.items.push({'line': item.get('line')});
		});

		options.data = JSON.stringify(modifiedAttrs);
		options.contentType = 'application/json';

        // Proxy the call to the original save function
        Backbone.Model.prototype.save.call(this, attrs, options);
    },

	markItems: function(order){
		// Loop through each item in order, mark as ordered
		_.each(order.get('items'), function(orderItem){
			var cartItem = App.cart.get(orderItem.get('line'));
			cartItem.set('is_ordered', true);
		});

		App.cart.unSelectItems();
		set_feedback('Order sent', 'success_message', false, 1500);
	},

	validate: function(){
		var complete = true;

		// Loop through each item in order and check if they are complete
		_.each(this.get('items'), function(orderItem){
			var item = App.cart.get(orderItem.get('line'));
			if(!item.isComplete()){
				item.set({'incomplete':true});
				complete = false;
			}
		});

		if(!complete){
			App.cart.unSelectItems();
			return 'Order failed. Please complete marked items';
		}
	},

	// Print function called AFTER order is successfully sent to database
	// and has received a response. Each item/side has its associated
	// kitchen printer IP stored in field 'printer_ip'
	printOrder: function(){
		var builder = new StarWebPrintBuilder();
		var ticket_data = {};
		var header_data = '';

		header_data += builder.createTextElement({width:2, data:'Prep Ticket\n'});
		header_data += builder.createTextElement({data:'************************\n'});
		// Add Header... date, time, table, employee, ticket #...

		header_data += builder.createTextElement({data:'************************\n'});




		var receiptText = '';
		var EOL = '\r\n';
		var items = this.get('items');

		// Ticket ID is from server response, it is unique primary key
		// of ticket from database
		receiptText += 'Ticket #' + this.get('ticket_id') + EOL +
			'TABLE # ' + App.table_num + EOL +
			'DATE:   ' + this.get('date_created') + EOL +
			'================================' + EOL +
			'ITEMS' + EOL;

		var cart = {};
		// Loop through items on ticket
		_.each(items, function(item){
			var printer_ip = item.get('printer_ip');
			ticket_data[printer_ip] += builder.createTextElement({data:add_white_space(item.get('quantity'), ' ', 5)+ item.get('name')+'\n'});

			receiptText += item.get('name') + ' (' + item.get("printer_ip") + ')' + EOL;

			// Loop through item modifiers
			if(item.get('modifiers').length > 0){
				_.each(item.get('modifiers').models, function(modifier){

					// If modifier is required OR different from default, display it
					//if(modifier.get('required') || (modifier.get('default') && modifier.get('default').toLowerCase() != modifier.get('selected_option').toLowerCase())){
						receiptText += '-- ' + modifier.get('name') + ': ' + modifier.get('selected_option') + EOL;
						ticket_data[printer_ip] += builder.createTextElement({data:'   ('+modifier.get('name')+' - '+ modifier.get('selected_option')+')\n'});
					//}
				});
			}

			// Loop through sides, soups, and salads (if any of each)
			// If side item_id is set to 0, it means the guest did not
			// want a side and it can be ignored
			if(item.get('sides')){
				_.each(item.get('sides').models, function(side){
					receiptText += side.get('name') + ' (' + side.get("printer_ip") + ')' + EOL;
					ticket_data[side.get("printer_ip")] += builder.createTextElement({data:'   - '+side.get('name')+'\n'});
				});
			}
			if(item.get('soups')){
				_.each(item.get('soups').models, function(soup){
					receiptText += soup.get('name') + ' (' + soup.get("printer_ip") + ')' + EOL;
					ticket_data[soup.get("printer_ip")] += builder.createTextElement({data:'   - '+soup.get('name')+'\n'});
				});
			}
			if(item.get('salads')){
				_.each(item.get('salads').models, function(salad){
					receiptText += salad.get('name') + ' (' + salad.get("printer_ip") + ')' + EOL;
					ticket_data[salad.get("printer_ip")] += builder.createTextElement({data:'   - '+salad.get('name')+'\n'});
				});
			}
		});
		var footer_data = builder.createCutPaperElement({feed:true});
		for (var ip in ticket_data)
		{
			var receipt_data = ticket_data[ip];
			webprnt.print(header_data + receipt_data + footer_data, "http://"+ip+"/StarWebPrint/SendMessage");
		}
	    //alert(receiptText);
		return false;
	}
});