var Item = Backbone.Model.extend({
	idAttribute: "item_id",
	defaults: {
		"name": "",
		"category": "",
		"sub_category": "",
		"department": "",
		"description": "",
		"price": 0.00,
		"modifiers": [],
		"sides": [],
		"number_sides": 0,
		"number_soups": 0,
		"number_salads": 0,
		"printer_ip": "",
		"print_priority": 0
	}
});