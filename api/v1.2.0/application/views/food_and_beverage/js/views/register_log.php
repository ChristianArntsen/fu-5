var RegisterLogView = Backbone.View.extend({
	tagName: "div",
	attributes: {'id':"register_log"},

	events: {
		"click a.skip": "skip",
		"click a.save": "save",
		"click a.close": "close"
	},

	initialize: function(attrs, options){
		if(attrs.template && attrs.template == 'close'){
			this.template = _.template( $('#template_close_register').html() );
		}else{
			this.template = _.template( $('#template_open_register').html() );
		}
	},

	render: function() {
		var data = this.model.attributes;

		this.$el.html(this.template(data));
		this.$el.find('input[type="text"]').not('#register_close_amount, #register_open_amount').keypad({position: 'bottom', formatMoney: false});
		this.$el.find('#register_close_amount').keypad({position: 'right'});
		this.$el.find('#register_open_amount').keypad({position: 'right'});

		return this;
	},

	save: function(e){
		var amount = parseFloat($('#register_open_amount').val());

		if(isNaN(amount) || amount <= 0){
			set_feedback('Amount must be greater than $0.00', 'error_message');
			return false;
		}

		this.model.set({'open_amount': amount});
		this.model.save();

		$.colorbox.close();
		this.remove();

		reload_tables(function(response){
			fnb.show_tables();
		});

		e.preventDefault();
	},

	close: function(e){
		var data = {};
		data.close_amount = parseFloat($('#register_close_amount').val());
		data.change = $('#register_close_change').val();
		data.ones = $('#register_close_ones').val();
		data.fives = $('#register_close_fives').val();
		data.tens = $('#register_close_tens').val();
		data.twenties = $('#register_close_twenties').val();
		data.fifties = $('#register_close_fifties').val();
		data.hundreds = $('#register_close_hundreds').val();

		if(isNaN(data.close_amount) || data.close_amount <= 0){
			set_feedback('Closing amount must be greater than $0.00', 'error_message');
			return false;
		}

		this.model.set(data);
		this.model.save();
		this.model.clear({"silent":true});
		$.colorbox.close();
		fnb.logout();

		e.preventDefault();
	},

	skip: function(e){
		$.colorbox.close();
		this.remove();

		reload_tables(function(response){
			fnb.show_tables();
		});
		e.preventDefault();
	}
});