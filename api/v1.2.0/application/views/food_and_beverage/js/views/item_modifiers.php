var ItemModifiersView = Backbone.View.extend({
	tagName: "div",
	className: "modifiers",
	template: _.template( $('#template_item_modifiers').html()),

	initialize: function(attributes, options){
		this.options = {};
		if(attributes.section_id){
			this.options.section_id = attributes.section_id;
		}
		if(attributes.title){
			this.options.title = attributes.title;
		}
	},

	events: {
		"click a.option_button": "changeModifierOption",
		"click a.back": "back"
	},

	render: function() {
		data = {};
		data.modifiers = this.model.get('modifiers');
		data.section_id = this.options.section_id;
		data.title = this.options.title;

		if(data.section_id == 3){
			data.category_id = 2;

		}else if(data.section_id == 2){
			data.category_id = 1;

		}else if(data.section_id == 1){
			data.category_id = 3;

		}else{
			data.category_id = null;
		}

		this.$el.html(this.template(data));
		return this;
	},

	changeModifierOption: function(event){
		var btn = $(event.currentTarget);
		var selectedOption = btn.data('value');
		var selectedPrice = btn.data('price');
		var modifierId = btn.data('modifier-id');

		btn.addClass('selected');
		btn.siblings().removeClass('selected');
		btn.parents('span').siblings('span.price').find('span.value').text(selectedPrice);

		if(selectedOption == 'no'){
			selectedPrice = 0.00;
		}

		this.model.get('modifiers').get(modifierId).set({'selected_option': selectedOption, 'selected_price': selectedPrice});
		return false;
	},

	back: function(event){
		this.remove();
		App.Page.itemEdit.renderSection( this.options.section_id );
		event.preventDefault();
	}
});