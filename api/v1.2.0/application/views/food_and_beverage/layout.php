<style>
#edit_layout {
	color: white;
	padding: 5px;
	display: block;
	text-shadow: 1px 1px 1px #333;
}

div.object:hover div.context-menu {
	display: block;
}

div.context-menu {
	position: absolute;
	background-color: white;
	display: block;
	box-shadow: 1px 1px 5px rgba(0,0,0,0.75);
	z-index: 10000;
	padding: 5px;
	display: none;
	width: 100px;
	text-align: center;
	left: 50%;
	margin-left: -55px;
}

div.context-menu > a, div.context-menu > a:visited  {
	color: #2679B7;
}

div.context-menu > a:hover {
	color: #1C5A89
}
</style>
<script>
$(function(){
	// Switch tables
	$('body').off('click', 'div.object').on('click', 'div.object', function(e){

		$('#layout_editor').mask('Please wait...');
		var table = $(this);
		var url = 'index.php/api/food_and_beverage/service/' + table.data('table');

		// Load table data
		$.get(url, null, function(response){
			// Reset API url with new table number
			App.sale_id = response.suspended_sale_id;
			App.table_num = response.table_num;
			App.api_table = App.api_root + App.table_num + '/';

			App.cart.url = App.api_table + 'cart';
			Cart.url = App.api_table + 'cart';
			App.receipts.url = App.api_table + 'receipts';
			ReceiptCollection.url = App.api_table + 'receipts';

			// Apply new data to cart and receipts
			App.cart.reset(response.cart);
			App.receipts.reset(response.receipts);

			// Reset table number and stats
			var tableOpen = moment(response.sale_time);
			$('#menubar_stats').html('<h2 class="table_num">Table #' + response.table_num + '</h2>'+
				'<h3>Seated ' + tableOpen.fromNow() + ' (' + tableOpen.format('h:mma') + ')</h3>');

			// Close table select window
			$.colorbox.close();
			$(document).scrollTop(0);
			reload_tables();

		}, 'json').always(function(){
			$('#layout_editor').unmask();
		});

		return false;
	});

	if($('div.floor_layout > div.object').data('draggable')){
		$('div.floor_layout > div.object').draggable( "destroy" );
	}

	if($('div.floor_layout > div.object').data('resizable')){
		$('div.floor_layout > div.object').resizable( "destroy" );
	}

	$('#layout_editor ul.tabs li.tab').find('a').unbind('click').click(function(e){
		$(this).parents('ul.tabs').find('li').removeClass('active');
		$(this).parent('li').addClass('active');
		var target = $(this).attr('href');
		$(target).siblings().hide();
		$(target).show();
		return false;
	});

	$('#edit_layout').colorbox({
		onComplete: function(){
			initLayoutEditor();
		},
		onClosed: function(){
			if($('div.floor_layout > div.object').data('uiDraggable')){
				$('div.floor_layout > div.object').draggable( "destroy" );
			}

			if($('div.floor_layout > div.object').data('uiResizable')){
				$('div.floor_layout > div.object').resizable( "destroy" );
			}
		}
	});

	$('div.object').find('*').unbind('click');

	$('#layout_editor').on('click', 'a.switch-employee', function(e){
		var table_num = $(this).parents('div.object').data('table');
		var employeeHtml = $('#select-employees');

		$.each(employeeHtml.find('a.employee'), function(index){
			$(this).attr('data-table-num', table_num);
		});

		$.colorbox2({
			'html': employeeHtml.html(),
			'width': 700,
			'height': 600,
			'title': 'Switch Employee'
		});
		return false;
	});
});
</script>
<div id="layout_editor" style="position: relative;">
	<ul class="tabs">
		<?php
		$active = 'active';
		foreach($layouts as $layout){ ?>
		<li class="tab <?php echo $active; ?>">
			<a href="#layout_<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></a>
		</li>
		<?php $active = ''; } ?>
		<?php if(!$this->permissions->is_employee()){ ?>
		<li style="float: right;"><a class="colbox" href="<?php echo site_url('food_and_beverage/edit_layout'); ?>" id="edit_layout">Edit Layouts</a></li>
		<?php } ?>
	</ul>

	<div class="tab_content">
	<?php
	if(!empty($layouts)){
	$active = 'active';
	foreach($layouts as $layout){ ?>
		<div id="layout_<?php echo $layout['layout_id']; ?>" class="floor_layout <?php echo $active; ?>" data-layout-id="<?php echo $layout['layout_id']; ?>">
		<?php foreach($layout['objects'] as $object){

		$activeClass = '';
		$table_attr = 'data-table="'.$object['label'].'"';
		$employee_details = '';

		if(!empty($object['employee_id'])){
			if($object['employee_id'] == $employee_id){
				$activeClass = ' my-active';
			}else{
				$activeClass = ' other-active';
			}

			$employee_details = '<span class="employee">'.$object['employee_first_name'].' '.strtoupper($object['employee_last_name'][0]).'. </span>';
		}

		$objectSize = '';
		if(!empty($object['width'])){
			$objectSize .= 'width: '.$object['width'].'px; ';
		}
		if(!empty($object['height'])){
			$objectSize .= 'height: '.$object['height'].'px; ';
		}
		?>
			<div class="object <?php echo $activeClass; ?>" <?php echo $table_attr; ?> data-type="<?php echo $object['type']; ?>" data-object-id="<?php echo $object['object_id']; ?>" style="position: absolute; left: <?php echo $object['pos_x']; ?>px; top: <?php echo $object['pos_y']; ?>px; <?php echo $objectSize; ?>;">
				<?php if(!$this->permissions->is_employee()){ ?>
				<div class="context-menu"><a class="switch-employee" href="#">Switch Employee</a></div>
				<?php } ?>
				<?php if($object['type'] == 'box'){ ?>
				<div class="box"></div>
				<?php }else{ ?>
				<?php include('images/restaurant_layout/'.$object['type'].'.svg'); ?>
				<?php } ?>
				<span class="label"><?php echo $object['label']; ?></span>
				<?php echo $employee_details; ?>
			</div>
		<?php } ?>
		</div>
	<?php $active = ''; }
	}else{ ?>
		<h1 style="text-align: center; display: block; color: #AAA; margin-top: 75px;">No tables available</h1>
	<?php } ?>
	</div>
</div>
