<table>
	<tr>
		<th>Key combination</th>
		<th>What it does</th>
	</tr>
	<tr>
		<td>Tab `</td>
		<td>Show Quick Key Guide</td>
	</tr>
	<tr>
		<td>Navigation</td>
		<td></td>
	</tr>
	<tr>
		<td>Tab + 1</td>
		<td>Tee Sheet/Reservations</td>
	</tr>
	<tr>
		<td>Tab + 2</td>
		<td>Sales</td>
	</tr>
	<tr>
		<td>Tab + 3</td>
		<td>Inventory</td>
	</tr>
	<tr>
		<td>Tab + 4</td>
		<td>Employees</td>
	</tr>
	<tr>
		<td>Tab + 5</td>
		<td>Customers</td>
	</tr>
	<tr>
		<td>Tab + 6</td>
		<td>Gift Cards</td>
	</tr>
	<tr>
		<td>Tab + 0</td>
		<td>Settings</td>
	</tr>
	<tr>
		<td>Sales Helps</td>
		<td></td>
	</tr>
	<tr>
		<td>Tab + g</td>
		<td>Open Gift Card Balance Window</td>
	</tr>
	<tr>
		<td>Tab + p</td>
		<td>Open Payments Window (Pay Now)</td>
	</tr>

</table>



