<?php if ($pdf) { ?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $receipt_title; ?></title>
</head>
<body>
<?php } ?>
	<?php
		$bgc = '#c8c8c8';
		$bc = '#606060';
		$fc = '#606060';
		$hfs = '10px';
		$hfw = 'bold';
		$cp = '2px 5px';
	?>
<?php 
$this_months_account_balance = 0;
$this_months_member_balance = 0;
$editable_area = 'editable_area';
$first_label = true;
$cc_checkboxes = '';
$cc_dropdown = '<select id="cc_dropdown" name="cc_dropdown">';
$cc_dropdown .= '<option value="invoice">Only Generate Invoice</option>';
$selected_card = false;
foreach($credit_cards as $credit_card)
{
	$cc_checkboxes .= "<div id='checkbox_holder_".$credit_card['credit_card_id']."'class='field_row clearfix ".(($credit_card['customer_id'] != '' && $credit_card['customer_id'] == $person_info->person_id)?'':'hidden')."'>".
			"<div class='form_field'>".
				form_checkbox('groups[]', $credit_card['credit_card_id'], ($credit_card['customer_id'] != '' && $credit_card['customer_id'] == $person_info->person_id) ? true:FALSE, "id='group_checkbox_{$credit_card['credit_card_id']}'")
				.' '.$credit_card['card_type'].' '.$credit_card['masked_account'].
				' - <span class="" onclick="customer.add_billing_row(\''.$credit_card['credit_card_id'].'\')">Add Billing</span>'.
			"</div>
		</div>";
		$selected = '';
		if ($credit_card_id == $credit_card['credit_card_id'])
		{
			$selected = 'selected';
			$selected_card = "{$credit_card['card_type']} {$credit_card['masked_account']}";
		}
	$cc_dropdown .= "<option value='{$credit_card['credit_card_id']}' $selected>Charge to {$credit_card['card_type']} {$credit_card['masked_account']}</option>";

	$first_label = false;
} 
$cc_dropdown .= '</select>';
$add_cc_link = ($this->config->item('mercury_id'))?anchor(site_url('customers/open_add_credit_card_window/'.$person_info->person_id.'/'.($billing_id ? $billing_id : -1)), "<div id='add_card'>". lang('customers_add_credit_card')."</div>", array('id'=>'add_credit_card_link')):'';
if ($is_invoice)
{
	$cc_dropdown = $selected_card ? $selected_card : '';
	$add_cc_link = '';
	$editable_area = '';
}
?>
								
<div id="receipt_wrapper">
	<table cellspacing="0" cellpadding="3" style='width:<?php echo $pdf?'230':'600';?>px; font-family:arial,sans-serif; font-size:12px; border-collapse:collapse'>
		<thead>
			<tr>
				<th>
					<!-- Logo -->
					<?php 
						$logo = $this->Appconfig->get_logo_image('',$course_info->course_id);
						echo $logo ? img(array('src' => $logo,'width' =>144)) : ''; 
					?>
				</th>
				<?php
					$phone_number = $course_info->phone;
					trim($phone_number);
					$len = strlen($phone_number);					
					if($len == 7)
					$phone_number = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $phone_number);
					elseif($len == 10)
					$phone_number = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3',$phone_number);											
				?>
				<th style='padding:<?=$cp?>; text-align:left; font-size:12px; font-weight: normal;'>
					<div id="company_name"><?=$course_info->name;?></div>
					<div id="company_address"><?=$course_info->address;?></div>
					<div id="company_address"><?=$course_info->city;?>, <?=$course_info->state;?> <?=$course_info->zip;?></div>
					<div id="company_phone">p: <?=$phone_number;?></div>
					<div id="company_phone">e: <?=$course_info->email;?></div>
				</th>
				<th>
					
				</th>
				<th colspan=3 style='font-size:24px; text-align: right;'>
					Invoice
					<br />
					<br />					
					<?php echo "<img src='".site_url('barcode')."?barcode=INV%20$invoice_number&text=INV%20$invoice_number' />"; ?>					
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style='border-bottom:1px solid <?=$bc?>;'>
					<div style='height:20px;width:20px;display:block;'></div>
				</td>
				<td style='border-bottom:1px solid <?=$bc?>;'></td>
				<td></td>
				<td style='border-bottom:1px solid <?=$bc?>;'></td>
				<td style='border-bottom:1px solid <?=$bc?>;'></td>
				<td style='border-bottom:1px solid <?=$bc?>;'></td>
			</tr>
			<tr>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' colspan=2>
					BILL TO
				</td>
				<td style='border-right:1px solid <?=$bc?>;'></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>
					INVOICE NUMBER
				</td>				
				<td colspan=2 style='border:1px solid <?=$bc?>;padding-left:5px;'><?php echo ($invoice_number != '') ? $invoice_number : $billing_id;?></td>
			</tr>
			<tr>
				<?php
					$customer_phone = $person_info->phone_number;
					trim($customer_phone);
					$len = strlen($customer_phone);					
					if($len == 7)
					$customer_phone = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $customer_phone);
					elseif($len == 10)
					$customer_phone = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3',$customer_phone);
				
				?>
				<td colspan=2 rowspan=3 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'>
					<div id="company_name"><?=$person_info->last_name;?>, <?=$person_info->first_name;?></div>
					<?php if ($person_info->address_1 != '') { ?>
					<div id="company_address"><?=$person_info->address_1;?></div>
					<div id="company_address"><?=$person_info->city;?>, <?=$person_info->state;?> <?=$person_info->zip;?></div>
					<?php } ?>
					<div id="company_phone"><?php if ($customer_phone != '') { echo $customer_phone;}?></div>
					<?php
						$show_email_chckbox = !$sent && $person_info->email!='';
						$class = $show_email_chckbox ? '' : 'hidden';
					?>
					<?php if($emailing_invoice || $pdf || $sent) { ?>
						<div id="company_address"><?php if ($person_info->person_id != '') { echo "<div id='email_container'>".$person_info->email."</div>";}?></div>						
					<?php } else { ?>
						<div id="company_address"><?php if ($person_info->person_id != '') { echo "<div id='email_container'>".$person_info->email."</div>".' '."<div id='email_chckbox' class='editable_area {$class}'><input type='checkbox' id='email_invoice' name='email_invoice' ".($email_invoice?'checked':'')." /> <label for='email_invoice'>Email Invoice</label></div>";}?></div>
					<?php } ?>												
					<?php
						
						$has_email = $person_info->email ? true : false;											
					?>
					<?php if(!$pdf && !$has_email && !$sent) { ?>
						<!-- <a href="index.php/customers/add_email/<?=$person_info->person_id?>/<?=($is_invoice?'invoice':'recurring_billing'); ?>/width~500" title='Add Email' class='colbox2'><div id='add_customer_email'><?php echo lang('add_customer_email') ;?></div></a> -->
						<div data-href="index.php/customers/add_email/<?=$person_info->person_id?>/<?=($is_invoice?'invoice':'recurring_billing'); ?>/width~500" id='add_customer_email'><?php echo lang('add_customer_email') ;?></div>
						
						<!-- <script>
							$('.colbox2').colorbox2();
						</script> -->	
					<?php } ?>					
					<div>
						<?php if ($person_info->person_id != '') {							 	
							 	echo ($payment_data['masked_account'] != '') ? 'Card: '.$payment_data['card_type'].' '.$payment_data['masked_account']: "<div class='$editable_area'>".$cc_dropdown."</div>";																
								echo $add_cc_link;
							}							
						?>						
					 </div>
				</td>
				<td style='border-right:1px solid <?=$bc?>;'></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>
					INVOICE DATE
				</td>
				<?php $display_date = $month_billed != '0000-00-00' ? $month_billed : date('Y-m-d');?>
				<td colspan=2 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $display_date;?></td>
			</tr>
			<tr>
				<td style='border-right:1px solid <?=$bc?>;'></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>
					AMOUNT PAID
				</td>
				<td colspan=2 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo to_currency($paid); ?></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>
					<?=$status_message?>
				</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td style='border-bottom:1px solid <?=$bc?>;'>
					<div style='height:20px;width:20px;display:block;'></div>
				</td>
				<td style='border-bottom:1px solid <?=$bc?>;'></td>
				<td style='border-bottom:1px solid <?=$bc?>;'></td>
				<td style='border-bottom:1px solid <?=$bc?>;'></td>
				<td style='border-bottom:1px solid <?=$bc?>;'></td>
				<td style='border-bottom:1px solid <?=$bc?>;'></td>
			</tr>
			<tr>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' colspan=2>PRODUCT</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>QTY</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>PRICE</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>TAX</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>AMOUNT</td>
			</tr>
			<?php foreach($items as $line=>$item) { ?>						
			<tr class='line_item'>
				<?php $this_months_account_balance = $item['pay_account_balance'] ? $item['amount'] : 0; ?>
				<?php $this_months_member_balance = $item['pay_member_balance'] ? $item['amount'] : 0; ?>
				<?php if ($sent) { ?>
					<td colspan=2 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $item['description']; ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $item['quantity']; ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency($item['amount']); ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo ($item['tax']); ?>%</td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency($item['quantity']*$item['amount']*(1+$item['tax']/100)); ?></td>					
				<?php } else { ?>
					<td colspan=2 style='border:1px solid #606060;'><span class='delete_row'>x</span><input name='description[]' placeholder='Description' size=50 value='<?php echo $item['description']; ?>' /></td>
					<td style='border:1px solid #606060;'><input class='quantity line_value' name='quantity[]' placeholder='Qty' size=2 value='<?php echo $item['quantity']; ?>'  /></td>
					<td class="price_column" style='border:1px solid #606060;'>$<input class='price line_value' name='price[]' placeholder='0.00' size=12 value='<?php echo $item['amount']; ?>' /></td>
					<td class="tax_column" style='border:1px solid #606060;'><input class='tax line_value' name='tax[]' placeholder='Tax' size=6 value='<?php echo ($item['tax']); ?>'/>%</td>
					<td style='border:1px solid #606060; text-align:right;' class='row_total'><?php echo to_currency($item['quantity']*$item['amount']*(1+$item['tax']/100)); ?></td>
				<?php } ?>				
			</tr>
			<!-- <tr>
				<td>
					<?=$this->db->last_query();?>
				</td>
			</tr> -->
			<?php 
			
			$totals['subtotal'] += $item['quantity']*$item['amount'];
			
			$totals['total_with_tax'] += $item['quantity']*$item['amount']*(1+$item['tax']/100);
			
			} ?>
			<?php if ($pay_member_balance && $person_info->member_account_balance < 0) { 
				$totals['subtotal'] -= $person_info->member_account_balance;
				$totals['total_with_tax'] -= $person_info->member_account_balance;
				?>				
				<tr id="member_balance_row">							
					<td colspan=2 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo (($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname'))); ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'>1</td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency(-$person_info->member_account_balance) ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>0.00%</td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency(-$person_info->member_account_balance) ?></td>							
				</tr>
			<?php } ?>	
			<?php if ($pay_account_balance && $person_info->account_balance < 0) { 
				$totals['subtotal'] -= $person_info->account_balance;
				$totals['total_with_tax'] -= $person_info->account_balance;
				?>				
				<tr id="customer_credit_row">							
					<td colspan=2 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo (($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname'))); ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'>1</td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency(-$person_info->account_balance) ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>0.00%</td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency(-$person_info->account_balance) ?></td>							
				</tr>
			<?php } ?>	
			<!-- MONTH TOTALS-->
			<tr id='totals_top_row'>
				<td colspan=3 style='border-right:1px solid <?=$bc?>;'></td>
				<td style='padding:<?=$cp?>; border-left:1px solid <?=$bc?>;'>Month Subtotal</td>
				<td class="subtotal" colspan=2 style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($totals['subtotal']); ?></td>
			</tr>
			<?php foreach($tax_rates as $tax_name => $tax_info) { ?>
			<tr>
				<td colspan=3></td>
				<td style='padding:<?=$cp?>; border-left:1px solid <?=$bc?>;'><?=$tax_name?> (<?=$tax_info['tax_rate']?>%)</td>
				<td colspan=2 style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($tax_info['tax_amount']); ?></td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan=3 rowspan=3 style='border-right:1px solid <?=$bc?>;'>						
					<?php if (!$sent && $person_info->person_id != '') { ?>
					<div id='add_item'>Add Item</div>
					<div class='editable_area'>						
						<?php if ($person_info->member_account_balance < 0 OR true) { ?>
						<div><input type='checkbox' class='<?php echo ($is_invoice?'is_invoice':''); ?>' id='pay_member_balance' name='pay_member_balance' <?php echo ($pay_member_balance?'checked="checked"':'');?> /><?php echo form_label('Pay down '.(($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname'))), 'pay_member_balance'); ?><?=($is_invoice ? "$ $person_info->member_account_balance" : '');?></div>
						<?php } ?>
						<?php if ($person_info->account_balance < 0 OR true) { ?>
						<div><input type='checkbox' class='<?php echo ($is_invoice?'is_invoice':''); ?>' id='pay_account_balance' name='pay_account_balance' <?php echo ($pay_account_balance?'checked="checked"':'');?> /><?php echo form_label('Pay down '.(($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname'))), 'pay_account_balance'); ?><?=($is_invoice ? "$ $person_info->account_balance" : '');?></div>
						<?php } ?>
						<div><input type='checkbox' class='<?php echo ($is_invoice?'is_invoice':''); ?>' id='include_itemized_sales' name='include_itemized_sales' <?php echo ($include_itemized_sales?'checked="checked"':'');?> /><?php echo form_label('Include Itemized Sales', 'include_itemized_sales'); ?></div>
					</div>
					<?php } ?>
				</td>
				<td style='padding:<?=$cp?>;'>Month Total with tax</td>
				<td class="total_with_tax" colspan=2 style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($totals['total_with_tax']); ?></td>
			</tr>
			<tr>
				<td style='padding:<?=$cp?>;'>Month Total paid</td>
				<td colspan=2 style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($paid); ?></td>
			</tr>
			<tr>
				<td style='padding:<?=$cp?>; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; border-bottom:1px solid <?=$bc?>; border-top:1px solid <?=$bc?>; border-right:1px solid <?=$bc?>; padding:<?=$cp?>; background:<?=$bgc?>; color:<?=$fc?>'>AMOUNT DUE</td>
				<td class="amount_due" colspan=2 style='padding:<?=$cp?>; text-align:right; width: 215px; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>															
					<?php echo to_currency($totals['total_with_tax'] - $paid); ?>
					<!-- <div class="amount_due"><?php echo to_currency($totals['total_with_tax'] - $paid);?></div> -->
					<!-- <div style='color:<?=$fc?>; font-size:10px;'>USD - U.S. Dollars</div> -->
				</td>
			</tr>
			<?php if (count($past_due_items) > 0) { ?>
				<tr>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' colspan=6>OVERDUE ITEMS</td>
				</tr>
				<?php foreach($past_due_items as $past_due_item) { 
					if (!$past_due_item['pay_account_balance'] && !$past_due_item['pay_member_balance'])
					{
					?>
					<tr class='line_item'>
						<?php if ($sent) { ?>
							<td colspan=2 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'>(<?php echo date('Y-m-d',strtotime($past_due_item['month_billed'])); ?>) <?php echo $past_due_item['description']; ?></td>
							<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $past_due_item['quantity']; ?></td>
							<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency($past_due_item['amount']); ?></td>
							<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo ($past_due_item['tax']); ?>%</td>
							<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency($past_due_item['quantity']*$past_due_item['amount']*(1+$past_due_item['tax']/100)); ?></td>					
						<?php } else { ?>
							<td colspan=2 style='border:1px solid #606060;'><span class='delete_row'>x</span><input name='description[]' placeholder='Description' size=50 value='<?php echo $past_due_item['description']; ?>' /></td>
							<td style='border:1px solid #606060;'><input class='quantity line_value' name='quantity[]' placeholder='Qty' size=2 value='<?php echo $past_due_item['quantity']; ?>'  /></td>
							<td class="price_column" style='border:1px solid #606060;'>$<input class='price line_value' name='price[]' placeholder='0.00' size=12 value='<?php echo $past_due_item['amount']; ?>' /></td>
							<td class="tax_column" style='border:1px solid #606060;'><input class='tax line_value' name='tax[]' placeholder='Tax' size=6 value='<?php echo ($past_due_item['tax']); ?>'/>%</td>
							<td style='border:1px solid #606060; text-align:right;' class='row_total'><?php echo to_currency($past_due_item['quantity']*$past_due_item['amount']*(1+$past_due_item['tax']/100)); ?></td>
						<?php } ?>				
					</tr>
					<?php
					$totals['subtotal'] += $past_due_item['quantity']*$past_due_item['amount'];
					$totals['total_with_tax'] += $past_due_item['quantity']*$past_due_item['amount']*(1+$past_due_item['tax']/100);
					?>
				<?php }
				} ?>
			<?php } ?>
			<!-- HERE IS THE TOTAL REMAINING ACCOUNT BALANCE AND MEMBER BALANCE -->
			<?php if (($this_months_member_balance != 0 && $person_info->member_account_balance + $this_months_member_balance < 0) || ($this_months_account_balance != 0 && $person_info->account_balance + $this_months_account_balance < 0)) { ?>
				<tr>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' colspan=6>ACCOUNT BALANCES</td>
				</tr>
			<?php } ?>
			<?php if ($this_months_member_balance != 0 && $person_info->member_account_balance + $this_months_member_balance < 0) { 
				$remaining_member_account = -$person_info->member_account_balance - $this_months_member_balance;
				?>
				<tr class='line_item'>
					<td colspan=2 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'>Remaining <?php echo (($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')));?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $past_due_item['quantity']; ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency($remaining_member_account); ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>0%</td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency($remaining_member_account); ?></td>					
				</tr>
			<?php 
				$totals['subtotal'] += $remaining_member_account;
				$totals['total_with_tax'] += $remaining_member_account;
			} ?>
			<?php if ($this_months_account_balance != 0 && $person_info->account_balance + $this_months_account_balance < 0) { 
				$remaining_account = -$person_info->account_balance - $this_months_account_balance;
				?>
				<tr class='line_item'>
					<td colspan=2 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'>Remaining <?php echo (($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname'))); ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'><?php echo $past_due_item['quantity']; ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency(-$person_info->account_balance + $this_months_account_balance); ?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>0%</td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency(-$person_info->account_balance + $this_months_account_balance); ?></td>					
				</tr>
			<?php 
				$totals['subtotal'] += $remaining_account;
				$totals['total_with_tax'] += $remaining_account;
			} ?>
			<tr id='totals_top_row'>
				<td colspan=3 style='border-right:1px solid <?=$bc?>;'></td>
				<td style='padding:<?=$cp?>; border-left:1px solid <?=$bc?>;'>Subtotal</td>
				<td class="subtotal" colspan=2 style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($totals['subtotal']); ?></td>
			</tr>
			<?php foreach($tax_rates as $tax_name => $tax_info) { ?>
			<tr>
				<td colspan=3></td>
				<td style='padding:<?=$cp?>; border-left:1px solid <?=$bc?>;'><?=$tax_name?> (<?=$tax_info['tax_rate']?>%)</td>
				<td colspan=2 style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($tax_info['tax_amount']); ?></td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan=3 rowspan=3 style='border-right:1px solid <?=$bc?>;'>						
					<?php if (!$sent && $person_info->person_id != '') { ?>
					<div id='add_item'>Add Item</div>
					<div class='editable_area'>						
						<?php if ($person_info->member_account_balance < 0 OR true) { ?>
						<div><input type='checkbox' class='<?php echo ($is_invoice?'is_invoice':''); ?>' id='pay_member_balance' name='pay_member_balance' <?php echo ($pay_member_balance?'checked="checked"':'');?> /><?php echo form_label('Pay down '.(($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname'))), 'pay_member_balance'); ?><?=($is_invoice ? "$ $person_info->member_account_balance" : '');?></div>
						<?php } ?>
						<?php if ($person_info->account_balance < 0 OR true) { ?>
						<div><input type='checkbox' class='<?php echo ($is_invoice?'is_invoice':''); ?>' id='pay_account_balance' name='pay_account_balance' <?php echo ($pay_account_balance?'checked="checked"':'');?> /><?php echo form_label('Pay down '.(($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname'))), 'pay_account_balance'); ?><?=($is_invoice ? "$ $person_info->account_balance" : '');?></div>
						<?php } ?>
						<div><input type='checkbox' class='<?php echo ($is_invoice?'is_invoice':''); ?>' id='include_itemized_sales' name='include_itemized_sales' <?php echo ($include_itemized_sales?'checked="checked"':'');?> /><?php echo form_label('Include Itemized Sales', 'include_itemized_sales'); ?></div>
					</div>
					<?php } ?>
				</td>
				<td style='padding:<?=$cp?>;'>Total with tax</td>
				<td class="total_with_tax" colspan=2 style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($totals['total_with_tax']); ?></td>
			</tr>
			<tr>
				<td style='padding:<?=$cp?>;'>Total paid</td>
				<td colspan=2 style='padding:<?=$cp?>; text-align:right; border-right:1px solid <?=$bc?>;'><?php echo to_currency($paid); ?></td>
			</tr>
			<tr>
				<td style='padding:<?=$cp?>; font-size:<?=$hfs?>; font-weight:<?=$hfw?>; border-bottom:1px solid <?=$bc?>; border-top:1px solid <?=$bc?>; border-right:1px solid <?=$bc?>; padding:<?=$cp?>; background:<?=$bgc?>; color:<?=$fc?>'>TOTAL DUE</td>
				<td class="amount_due" colspan=2 style='padding:<?=$cp?>; text-align:right; width: 215px; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>															
					<?php echo to_currency($totals['total_with_tax'] - $paid); ?>
					<!-- <div class="amount_due"><?php echo to_currency($totals['total_with_tax'] - $paid);?></div> -->
					<!-- <div style='color:<?=$fc?>; font-size:10px;'>USD - U.S. Dollars</div> -->
				</td>
			</tr>
		</tbody>
	</table>
	<table cellspacing="0" cellpadding="3" style='width:<?php echo $pdf?'230':'600';?>px; font-family:arial,sans-serif; font-size:12px; border-collapse:collapse'>
		<thead>
			<tr>
				<td style='border-bottom:0px solid <?=$bc?>;'>
				</td>
				<td style='border-bottom:0px solid <?=$bc?>;'></td>
				<td style='border-bottom:0px solid <?=$bc?>;'>
					<div style='height:20px;<?php echo $pdf ? 'width:500px;' : ''; ?>display:block;'></div>
				</td>
				<td style='border-bottom:0px solid <?=$bc?>;'></td>
			</tr>
			<tr>
				<td style='border-bottom:0px solid <?=$bc?>;'>
					<div style='height:20px;width:20px;display:block;'></div>
				</td>
				<td style='border-bottom:0px solid <?=$bc?>;'></td>
				<td style='border-bottom:0px solid <?=$bc?>;'></td>
				<td style='border-bottom:0px solid <?=$bc?>;'></td>
			</tr>
		</thead>
		<?php if ($include_itemized_sales && $item['pay_member_balance']) { ?>
			<tbody>
				<tr>
					<td style='border-bottom:1px solid <?=$bc?>;'>
						<div style='height:20px;width:20px;display:block;'></div>
					</td>
					<td style='border-bottom:1px solid <?=$bc?>;'></td>
					<td style='border-bottom:1px solid <?=$bc?>;'></td>
					<td style='border-bottom:1px solid <?=$bc?>;'></td>
				</tr>
				<tr>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' colspan=4>ITEMIZED BALANCE TRANSACTIONS - <?php echo (($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname'))); ?></td>
				</tr>
				<tr>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>DATE</td>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>SALE #</td>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>SALE ITEMS</td>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>AMOUNT</td>
				</tr>
			<!-- </thead>
			<tbody> -->
			<?php
				foreach(array_reverse($this->Member_account_transactions->get_member_account_transaction_data_for_customer($person_info->person_id, $display_date)) as $row)
				{
					if (strpos($row['trans_comment'], 'POS') !== false) {
				?>
				<tr bgcolor="" align="center">
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;' align='left'><?php echo $row['date'];?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;' align='left'><?php echo $row['trans_comment'];?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;' align='left'>
						<?php $trans_desc = str_replace('<br/>', ', ', $row['trans_description']); 
							if (strlen($trans_desc) > 78)
								$trans_desc = substr($trans_desc, 0, 75).'...';
							echo $trans_desc; ?>
					</td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;' align="right" class="<?php echo ($row['trans_amount'] < 0)?'red':''; ?>"><?php echo $row['trans_amount'];?></td>
				</tr>
					
				<?php
					}
				}
				?>
			</tbody>
			<?php } ?>
			<!-- <tr>
				<td>
					<?=$this->db->last_query();?>
				</td>
			</tr> -->
			<?php if ($include_itemized_sales && $item['pay_account_balance']) { ?>
			<tbody>
				<tr>
					<td style='border-bottom:1px solid <?=$bc?>;'>
						<div style='height:20px;width:20px;display:block;'></div>
					</td>
					<td style='border-bottom:1px solid <?=$bc?>;'></td>
					<td style='border-bottom:1px solid <?=$bc?>;'></td>
					<td style='border-bottom:1px solid <?=$bc?>;'></td>
				</tr>
				<tr>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' colspan=4>ITEMIZED BALANCE TRANSACTIONS - <?php echo (($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname'))); ?></td>
				</tr>
				<tr>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>DATE</td>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>SALE #</td>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>SALE ITEMS</td>
					<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>'>AMOUNT</td>
				</tr>
			<!-- </thead>
			<tbody> -->
				<?php
				foreach(array_reverse($this->Account_transactions->get_account_transaction_data_for_customer($person_info->person_id, $display_date)) as $row)
				{
					if (strpos($row['trans_comment'], 'POS') !== false) {
				?>
					<tr bgcolor="" align="center">
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;' align='left'><?php echo $row['date'];?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;' align='left'><?php echo $row['trans_comment'];?></td>
					<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;' align='left'>
						<?php $trans_desc = str_replace('<br/>', ', ', $row['trans_description']); 
							if (strlen($trans_desc) > 78)
								$trans_desc = substr($trans_desc, 0, 75).'...';
							echo $trans_desc;
						?>
					</td>
					<td  style='padding:<?=$cp?>; border:1px solid <?=$bc?>;' align="right" class="<?php echo ($row['trans_amount'] < 0)?'red':''; ?>"><?php echo $row['trans_amount'];?></td>
					</tr>
				<?php
					}
				}
				?>
			</tbody>
			<?php } ?>	
	</table>
</div>
<?php if ($pdf) { ?>
</body>
</html>
<?php } ?>

<script>	
	window.billing_table_config = 
	{
		cp : '',
		bc : ''		
	};
	window.billing_table_config.bc = '<?php echo $bc; ?>'
	window.billing_table_config.cp = '<?php echo $cp; ?>'
</script>