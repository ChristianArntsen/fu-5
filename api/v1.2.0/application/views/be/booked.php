<?php
$this->load->view("partial/course_header", array('booked'=>$booked));
?>
<script>
	var pay_now = {
		continue_paying:function(){
			if (!$('input[name=terms_and_conditions_accept]').attr('checked'))
			{
				alert('You must accept the tems and conditions before proceeding.');
			}
			else
			{
				//Select how many
				var players = $('input:radio[name=player_button]:checked').val();
	    		var carts = $('input:radio[name=include_carts]:checked').val();

				//Open iframe to payment, and resize window.
				$.colorbox({
					href:'index.php/be/open_foreup_payment_window/<?=$teetime_info->TTID?>/'+players+'/'+carts,
					'onComplete':function() {
	               		$.colorbox.resize({width:600, height:560});
	               	}
				});
			}
		},
		update_prices:function() {
			var players = $('input:radio[name=player_button]:checked').val();
	    	var carts = $('input:radio[name=include_carts]:checked').val();
	    	console.log('pl '+players+' ca '+carts);
	    	var subtotal = discount = total = 0;
	    	var single_price = '<?=$single_price?>';
	    	var single_cart_price = '<?=$single_cart_price?>';
	    	single_price = (carts == 1)?parseFloat(single_price)+parseFloat(single_cart_price):parseFloat(single_price);
	    	subtotal = single_price * players;
	    	discount = subtotal * <?=$course_info->foreup_discount_percent/100?>;
	    	total = subtotal * <?=(100-$course_info->foreup_discount_percent)/100?>;
	    	$('.subtotal_price').html(subtotal.toFixed(2));
	    	$('.discount_price').html(discount.toFixed(2));
	    	$('.final_total_price').html(total.toFixed(2));
	    }
	}
    var booking = {
    	pay_now:function() {
    		$.colorbox({
    			width:660,
    			href:'index.php/be/pay_now/<?=$teetime_info->TTID?>',
    			title:'Save <?=$course_info->foreup_discount_percent?>% now',
    			onComplete:function(){
    				$.colorbox.resize();
    			}})
    	},
    	close_popup:function() {
    		$.colorbox.close();
    	},
		hide_pay_now_info:function (callback) {
			$('#pay_now_info').slideUp('slow', function() {
				$(this).remove();
				if (callback != undefined)
					callback();
			});
		},
		show_purchase_thank_you:function() {
			$('#purchase_thank_you').toggle('slow').animate({backgroundColor:"#e1ffdd"},"slow","linear")
				.animate({backgroundColor:"#e1ffdd"},5000)
				.animate({backgroundColor:"#ffffff"},"slow","linear");;
		},
		update_booking_details:function(players, total) {
			$('#player_count').html(players);
			$('.total_price').html(total);
		},
        teesheet_id:'<?php echo $teesheet_id ?>',
        time_range:'morning',
        holes:'18',
        available_spots:1,
        update_slider_hours:function(){
            var values = $( "#slider" ).slider( "option", "values" );
            var am_pm0 = 'a';
            var am_pm1 = 'a';
            if (values[0] > 11)
                am_pm0 = 'p';
            if (values[1] > 11)
                am_pm1 = 'p';
            if (values[0] > 12)
                values[0] = values[0]-12;
            if (values[1] > 12)
                values[1] = values[1]-12;

            $($('.ui-slider-handle')[0]).html(values[0]+am_pm0);
            $($('.ui-slider-handle')[1]).html(values[1]+am_pm1);

        },
        get_available_teetimes:function(){
            $('.tab_content').html("<div class='loading_golf_ball'><img src='<?php echo base_url(); ?>images/loading_golfball2.gif'/><div>Finding available tee times...</div></div>");
            $.ajax({
               type: "POST",
               url: "index.php/be/get_available_teetimes",
               data: "time_range="+this.time_range+"&teesheet_id="+this.teesheet_id+"&holes="+this.holes+"&available_spots="+this.available_spots,
               success: function(response){
                   booking.update_available_teetimes(response);
               }
            });
        },
        update_available_teetimes:function(ajax_response) {
            var data = eval('('+ajax_response+')');
            for (var i in data) {
                $('#tab'+(parseInt(i)+1)+'_label').html(data[i].day_label);
                var teetime_html = '';
                for (var j in data[i].teetimes) {
                    teetime_html += data[i].teetimes[j];
                }
                //console.log('#tab'+(parseInt(i)+1));
                $('#tab'+(parseInt(i)+1)).html(teetime_html);
            }
            this.add_reserve_button_styles();
            tb_init('a.thickbox');
        },
        open_reservation_popup:function() {

        },
        activate_filter_buttons:function() {
            $("#"+this.time_range+'_range').click();
            $("input:[name=time_button]").click(function(e){
                //console.dir($(e.target));
                var time_range = $(e.target)[0].id;
                time_range = time_range.slice(0, time_range.indexOf('_'));
                //console.log(time_range);
                booking.time_range = time_range;
                booking.get_available_teetimes();
            });
            $("#holes_"+this.holes).click();
            $("input:[name=holes_button]").click(function(e){
                //console.dir($(e.target));
                var holes = $(e.target)[0].id;
                holes = holes.slice(holes.indexOf('_')+1);
                booking.holes = holes;
                booking.get_available_teetimes();
            });
            $("#player_"+this.available_spots).click();
            $("input:[name=player_button]").click(function(e){
                //console.dir($(e.target));
                var players = $(e.target)[0].id;
                players = players.slice(players.indexOf('_')+1);
                //console.log(players);
                booking.available_spots = players;
                booking.get_available_teetimes();
            });
        },
        add_reserve_button_styles:function() {
            $('.reserve_button a').button();
        }
    };
    $(document).ready(function() {
    	$('#turnkey_link').colorbox({title:'Share on Facebook', iframe:true, width:900, height:650});
        booking.activate_filter_buttons();
        //When page loads...
        $('#player_1').button();
        $('#player_2').button();
        $('#player_3').button();
        $('#player_4').button();
        $('#holes_9').button();
        $('#holes_18').button();
        booking.add_reserve_button_styles();
        //$('#reserve_button').button();
        $('#time_button_set').buttonset();
        $('#player_button_set').buttonset();
		$('#hole_button_set').buttonset();
		$(".tab_content").hide(); //Hide all content
		$("ul.tabs li:first").addClass("active").show(); //Activate first tab
		$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

        //$('#slider').slider({min:5, max:19, range:true, values:[5,10], change:function(){booking.update_slider_hours()}});
        //booking.update_slider_hours();
});
function post_person_form_submit(response)
{
        if(!response.success)
        {
                set_feedback(response.message,'error_message',true);
        }
        else
        {
                //This is an update, just update one row
                if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
                {
                        //update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
                        set_feedback(response.message,'success_message',false);

                }
                else //refresh entire table
                {
                        //do_search(true,function()
                        //{
                                //highlight new row
                        //        highlight_row(response.person_id);
                                set_feedback(response.message,'success_message',false);
                        //});
                }
        }
}
</script>
<style>
    html {
		background: url(<?php echo base_url(); ?>images/backgrounds/course.png)no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		font-family: Quicksand, Helvetica, Arial, sans-serif;
	}
	#content_area_wrapper {
		min-height:0;
	}
	.ui-widget {
        font-size:.8em;
    }
    .ui-buttonset .ui-button {
        margin-right:-0.5em;
        height:28px;
        padding:0px 5px 0px;
    }
    #slider {
        margin: 10px 10px 10px 20px;
        width:250px;
    }
    .ui-slider-handle {
        font-size:.8em;
        padding:1px 0 0 2px;
    }
    .tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	clear: both;
	float: left;
        width: 959px;
	background: #fff;
        height:400px;
}
.tab_content {
	padding:10px 20px;
	font-size: 1.2em;
        height:330px;
        overflow: auto;
}
    ul.tabs {
	margin: 0;
	padding: 0;
	float: left;
	list-style: none;
	height: 46px; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 959px;
}
ul.tabs li {
	float: left;
	margin: 0;
	padding: 0;
	height: 45px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 45px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	overflow: hidden;
	position: relative;
	background: #e0e0e0;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: .9em;
	padding: 0 10px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
        width:114px;
        text-align: center;
        line-height: 31px;
}
ul.tabs li a div {
    height:12px;
}
ul.tabs li a span {
    font-size:.7em
}
ul.tabs li a:hover {
	background: #ccc;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #fff;
	border-bottom: 1px solid #fff; /*--Makes the active tab look like it's connected with its content--*/
}
#time_button_set, #hole_button_set {
    float:left;
    margin:10px 0 0 16px;
}
#player_button_set {
    float:right;
    margin:10px 21px 0 0;
}
.search_controls {
    padding:0px 5px 10px;
    border-bottom: 1px solid #ddd;
}
.teetime {
    border:1px solid #dedede;
    padding:8px 15px;
    margin-bottom:5px;
}
.teetime .left {
    float:left;
    width:10%;
}
.teetime .center {
    float:left;
    width:75%;
    text-align: center;
}
.teetime .right {
    float:right;
    width:11%;
    margin-top:5px;
}
.teetime .left .players span {
    float: left;
}
.teetime .left .players span.ui-button-text {
    font-size: .6em;
    margin: 1px 0 0 2px;
}
.loading_golf_ball {
    font-size:14px;
    padding-top:94px;
    text-align:center;
}
.be_header {
    color:#336699;
    margin:15px auto 5px;
    text-align: center;
}
.be_form {
    width:65%;
    margin:auto;

}
#employee_basic_info.login_info {
    margin-right:5px;
}
.register_form {
    width:85%;
}
.be_form td input {
    margin-top:3px;
    margin-left:5px;
}
.be_fine_print {
    color:#333333;
    font-size:11px;
    text-align:center;
    margin-top:5px;
}
.be_form td hr {
    color:#336699;
    margin:11px 0px 7px;
}
.be_reserve_buttonset {
    float:left;
    margin-right:22px;
}
.be_buttonset_label {
    line-height: 30px;
    color:#336699;
    margin-right:10px;
}
#TB_ajaxContent .teetime .course_name {
    font-size:1.7em;
    margin-top:6px;
}
#TB_ajaxContent .teetime .info {
    padding-left:10px;
    margin-top:7px;
}
#TB_ajaxContent .teetime .time, #TB_ajaxContent .teetime .holes, #TB_ajaxContent .teetime .date {
    color:#336699;
    font-size:1.3em;
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label, #content_area .teetime .spots {
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label {
    font-size:.6em;
}
#content_area .teetime .price_label {
    margin-right:2px;
    padding-right:0px;
    line-height:19px;
}
#TB_ajaxContent .teetime .info .ui-icon {
    float:left;
    margin-right:5px;
}
#content_area .teetime .ui-icon {
    float:left;
    margin-right:5px;
}
#TB_ajaxContent .teetime .reservation_info {
    float:left;
    width:70%;
}
#TB_ajaxContent .teetime .price_box {
    float:right;
    text-align: right;
    width:27%;
    font-size:3.2em;
    line-height: 50px;
}
#content_area .teetime .price_details {
    padding-left: 135px;
}
#course_name {
    font-size:2em;
    text-align:center;
}
#home_module_list {
    text-align:left;
    width:900px;
    min-height:350px;
}
#content_area {
	width:900px;
}
#menubar_background, #menubar_full{
	background:none;
}
.booking_details {
	float:left;
	width:33%;
}
.reservation_info table {
	text-align:left;
}
.reservation_info table td {
	font-size:16px;
	padding:0px 0px 18px 7px;
}
.right_column {
	float:right;
	width:445px;
	font-size:14px;
	color:#5d5d5d;
	margin-right:5px;
}
button {
	width:70px;
	cursor:pointer;
}
.right_column button, button.continue_button {
	width:140px;
		background: #349ac5; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3'); /* for IE */
	background: -webkit-linear-gradient(top, #349AC5, #4173B3);
	background: -moz-linear-gradient(top,  #349AC5,  #4173B3); /* for firefox 3.6+ */
	color: white;
	display: block;
	font-size: 14px;
	font-weight: normal;
	height: 30px;
	line-height:30px;
	padding: 0px 0 0 10px;
	width: 180px;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	margin: 10px auto -25px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5),
				0px 3px 1px -2px rgba(255, 255, 255, .2),
				0px 0px 0px 10px white;
	border: 1px solid #232323;
}
.price_tag {
	background:url('../../images/pieces/price_tag.png') no-repeat 143px 0px transparent;
	height:64px;
	display:block;
	padding-left:16px;
	line-height:67px;
	font-size:28px;
	color:white;
}
.price_tag_pretext, .price_tag_posttext {
	font-size:20px;
	color:black;
}
.price_tag_pretext {
	padding-right:22px;
}
.price_tag_posttext {
	padding-left:13px;
}
#booking_basic_info legend {
	text-align:left;
}
.no_thanks {
	font-size:14px;
}
#pay_now_info legend {
	text-align: center;
	color: #3E6E97;
	font-weight: bold;
	font-size: 18px;
}
#pay_now_info {
	color: #5D5D5D;
	font-weight: normal;
	width: 400px;
	margin: 20px auto;
	padding-bottom:10px;
	background:transparent;
}
#pay_now_info span.no_thanks_link {
	font-size: 12px;
	color: #5D5D5D;
	pointer:cursor;
}
#booking_basic_info legend {
	font-size:20px;
}
.minimum_note {
	font-size:12px;
}
#confirmation_box_header {
	color: white;
	background: url(<?php echo base_url(); ?>images/backgrounds/top_bar_bg_black.png) repeat-x;
	height: 37px;
	line-height: 37px;
	font-size: 18px;
	text-align: left;
	text-indent: 15px;
	box-shadow:0px 1px 5px -1px black;
}
#confirmation_box {
	background: white;
	box-shadow: 0px 1px 13px -3px black;
}
#booking_basic_info {
	border:none;
	background:#ddd;
	padding:10px 10px 0px;
	margin-left:10px;
	margin-top:25px;
	margin-bottom:25px;
	width:385px;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
	font-weight: normal;
	color: #333;
}
.detail_label {
	font-weight:bold;
}
.details_header {
	font-size:16px;
	font-weight:bold;
}
.pay_now_box {
	text-align:center;
}
.thank_you {
	font-size:16px;
	font-weight:bold;
	margin:12px 0px 3px;
}
.facebook_share {
	margin: 10px 0px;
}
<?php if ($this->session->userdata('iframe')) { 
	// STYLES FOR AN IFRAME VERSION OF ONLINE BOOKING
	?>
#content_area {
	padding:0px;
	width:760px;
}
#content_area_wrapper {
	min-height: 540px;
}
.column {
	float:none;
	width:auto;
	margin:0px;
}
.wrapper {
	width:758px;
	margin:0px auto;
}
ul.tabs {
	width:753px;
}
#home_module_list {
	width:758px;
	margin-left:0px;
	float:none;
	min-height:350px;
}
.tab_container {
	width:756px;
}
.tab_content {
	padding:0px 10px;
}
.teetime {
	padding:8px 10px;
}
#time_button_set, #hole_button_set {
	margin: 10px 30px 0 15px;
}
div.dropdown-menu > a, #signin {
	font-size: 14px;
}
#course_name, span.customStyleSelectBox {
	font-size:12px;
}
.menu_item img {
	height: 30px;
}
#menubar_background, #menubar_full {
	padding:0px;
}
#teesheet_name, #course_name {
	line-height: 30px;
	width:100%;
	float:none;
	margin-top:12px;
	font-size:24px;
}
#navigation_menu {
	margin: 5px 15px 5px 10px;
}
#menubar_background, #menubar_full {
	display: none;
}
div.ui-datepicker {
	width: auto;
	margin-bottom: 0px;
}
.congratulations {
	margin-top: 25px;
}
.right_column {
	width: 315px;
	text-align:center;
}
#pay_now_info {
	width: 265px;
	margin: 10px auto 30px;
}
.be_fine_print {
	padding: 0px 40px;
}
#confirmation_email {
	width: 200px;
	margin: auto;
}
#for_questions {
	width: 200px;
	margin: auto;
	margin-top: 10px;
}
<?php } ?>
</style>
<div id="course_name"><?php echo $course_info->name?></div>
<div id="thank_you_box"></div>
<div id="home_module_list">
	<div id='confirmation_box'>
		<div id='confirmation_box_header'>Confirmation</div>
	<div class='content'>
		<div class='booking_details'>
			<fieldset id="booking_basic_info" class="ui-corner-all ui-state-default login_info">
				<div class='reservation_info'>
			    	<table>
			    		<tbody>
			    			<tr>
			    				<td colspan=2 class='details_header'>Booking Details</td>
			    			</tr>
			    			<tr>
			    				<td class='detail_label'>Date:</td>
			    				<td><input type="hidden" value ="<?php echo $start?>" id="hidden_start"/>
			                <?php echo date('m/d/Y', strtotime($teetime_info->start+1000000))?></td>
			    				<td class='detail_label'>Players:</td>
			    				<td><div id="player_count" class="be_reserve_buttonset">
								    <?php echo $teetime_info->player_count?>
								</div></td>
			    			</tr>
			    			<tr>
			    				<td class='detail_label'>Time:</td>
			    				<td><?php echo date('g:i a', strtotime($teetime_info->start+1000000))?></td>
			    				<td class='detail_label'>Green Fee:</td>
			    				<td><div class="price_box">$<?=($teetime_info->carts ? $single_with_cart : $single_price) ?></div></td>
			    			</tr>
			    			<tr>
			    				<td class='detail_label'>Holes:</td>
			    				<td><input type="hidden" value ="<?php echo $teetime_info->holes?>" id="hidden_holes"/>
			                <?php echo $teetime_info->holes?></td>
			    				<td class='detail_label'>Total:</td>
			    				<td><div class="price_box">$<span class="total_price"><?php echo number_format(($teetime_info->carts ? $single_with_cart : $single_price)*$teetime_info->player_count, 2)?></span>*</div></td>
			    			</tr>
			    		</tbody>
			    	</table>
			    </div>
			</fieldset>
			<div class="clear"></div>
		</div>
	</div>
	<div class='right_column'>
		<?php if ($booked){ ?>
		<div class='thank_you'>Thank you!</div>
		<div id='confirmation_email'>A confirmation email has been sent to <?php echo $this->session->userdata('customer_email'); ?>.</div>
		<div id='for_questions'>For questions, call <?php echo $this->session->userdata('course_name'); ?> at <?php echo $this->session->userdata('course_phone'); ?>.</div>
		<?php } ?>
		<?php if ($teetime_info->player_count < 4) { ?>
		<div class='facebook_share'>Want to invite your friends on Facebook? <a id='turnkey_link' href='https://www.turnkeysocial.com/ForeUp/Share/index/teetime/<?=$teetime_info->TTID?>'>Click here</a></div>
		<?php } ?>
		<?php if ($available_for_purchase){?>
		<fieldset id="pay_now_info" class="ui-corner-all ui-state-default login_info">
			<legend>Pay Now and Save <?=$course_info->foreup_discount_percent?>%</legend>
			<div class='pay_now_box'>
				<div>
					Save $<?php echo number_format(($teetime_info->carts ? $single_with_cart : $single_price)*$course_info->foreup_discount_percent/100,2)?> per player. 2 or more players required.
				</div>
				<button onclick="booking.pay_now()">Pay Now</button>
				<!--span onclick="booking.hide_pay_now_info()" class='no_thanks_link'>No thanks</span-->
			</div>
		</fieldset>
		<?php } ?>
		<div id='purchase_thank_you' style='display:none'>
			Thank you for your purchase,<br/> your receipt has been emailed <br/>to the same email address as above.
		</div>
	</div>
	<div class='clear'></div>
</div>

	<div class="be_fine_print">
	    <?php if ($available_for_purchase){?>
	    	*<?= $teetime_info->carts ? '' : 'Price does not include cart fees. '?>You are under no obligation to purchase your tee time now.
	    	If you decide not to pay now, green fees will be collected at the course.
    	<?php } else { ?>
			*Price does not include cart fees. Green fees will be collected at the course.
		<?php } ?>
	</div>

    <!--p>An email has been sent to your email address with your teetime details.</p-->
    <br/><br/>

</div>
<?php //$this->load->view("partial/course_footer"); ?>