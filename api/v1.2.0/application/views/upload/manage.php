<?php if($width == 0) { $width = 600; } if($height == 0){ $height = 600; } ?>
<script src="<?php echo base_url()?>js/ajaxfileupload.js"></script>
<script src="<?php echo base_url()?>js/jquery.Jcrop.min.js"></script>
<link href="<?php echo base_url()?>css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
<script>
var selected_image = null;
var image_autocomplete;
$(function(){
	$('#upload_file').submit(function(e){
		var form = $(this);

		e.preventDefault();
		$.ajaxFileUpload({
			url: '<?php echo base_url()?>index.php/upload/upload_file/',
			secureuri: false,
			fileElementId: 'userfile',
			dataType: 'json',
			data: {
				crop_ratio: <?php echo round((float) $crop_ratio, 2); ?>,
				module: '<?php echo $module; ?>',
				width: <?php echo (int) $width; ?>,
				height: <?php echo (int) $height; ?>
			},
			success: function(data, status){
				if(data.status != 'error'){
					$('#title').val('');
					load_image(data.image_id, true, true);
					form[0].reset();
					form.hide();
				}else{
					set_feedback(data.msg,'error_message', false, 3000);
				}
			}
		});
		return false;
	});

	$('li.file').die('click').live('click', function(e){
		var image_id = $(this).attr('data-image-id');
		$(this).addClass('active').siblings().removeClass('active');
		load_image(image_id);
		selected_image = image_id;
		return false;
	});

	$('a.upload-images').die('click').live('click', function(e){
		$('#edit_image').hide();
		$('#upload_images').show();
		return false;
	});

	// Custom autocomplete search functionality
	$('#image_search_field').on('keyup', function(e){
		var field = $(this);

		if(image_autocomplete){
			image_autocomplete.abort();
		}

		var pos = field.position();
		pos.height = field[0].offsetHeight;

		var imageList = $('div.files');

		var query = field.val();
		var module = '<?php echo $module; ?>';

		field.addClass('loading');
		image_autocomplete = $.get("<?php echo site_url('upload/search'); ?>", {q:query, module:module}, function(response){
			imageList.html(response);
			field.removeClass('loading');
		},'html');
	});

	<?php if(!empty($image_id)){ ?>
	$('#file_<?php echo $image_id; ?>').click();
	<?php } ?>
});

function load_image(image_id, editing, uploading){

	obj = {
		crop_ratio: <?php echo round((float) $crop_ratio, 2); ?>
	};

	if(editing){
		obj.editing = true;
	}
	if(uploading){
		obj.uploading = true;
	}

	$.get('<?php echo site_url('upload/view'); ?>/' + image_id, obj, function(response){
		$('#upload_images').hide();
		$('#edit_image').html(response).show();
	},'html');
}

function refresh_files()
{
   $.get('<?php echo base_url()?>index.php/upload/view_list/<?php echo $module; ?>', null, function(response){
      $('div.files').html(response);
      $('#file_'+selected_image).addClass('active');
   });
}
</script>
<style>
#image-library {
	width: auto;
	display: block;
	padding: 0px;
	overflow: hidden;
	background-color: #F5F5F5;
}

#image-library li.empty {
	display: block;
	font-size: 18px;
	color: #AAA;
	text-align: center;
	margin-top: 50px;
}

#image-library label {
	font-size: 14px;
	float: right;
	margin-right: 10px;
}

#image-library div.top-bar {
	display: block;
	width: auto;
	padding: 10px;
	background-color: white;
	border-bottom: 1px solid #E0E0E0;
	overflow: hidden;
	z-index: 1;
	position: relative;
}

#image-library .button {
	display: block;
	float: left;
	margin: 0px 5px 0px 0px;
	background: #349AC5;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3');
	background: -webkit-linear-gradient(top, #349AC5, #4173B3);
	background: -moz-linear-gradient(top, #349AC5, #4173B3);
	color: white;
	font-size: 14px;
	font-weight: normal;
	height: 26px;
	line-height: 26px;
	padding: 0px 10px;
	width: auto;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
}

#image-library .button.green {
	background: #4dad47;
	background: -moz-linear-gradient(top,  #4dad47 0%, #398235 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#4dad47), color-stop(100%,#398235));
	background: -webkit-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: -o-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: -ms-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: linear-gradient(to bottom,  #4dad47 0%,#398235 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4dad47', endColorstr='#398235',GradientType=0 );
}

#image-library .button.red {
	background: #d14d4d; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#d14d4d', endColorstr='#c03939'); /* for IE */
	background: -webkit-linear-gradient(top, #d14d4d, #c03939);
	background: -moz-linear-gradient(top,  #d14d4d,  #c03939); /* for firefox 3.6+ */
}

#image-library a.delete {
	float: right;
}

div.files h2, div.edit h2 {
	font-size: 18px;
	font-weight: bold;
	display: block;
	padding: 5px;
	margin: 0px;
	width: auto;
}

div.files, div.edit {
	display: block;
	height: 600px;
	margin: 0px;
	padding: 0px;
	position: relative;
}

div.files {
	float: left;
	width: 300px;
	padding: 0px;
	background: white;
	z-index: 2;
	top: -1px;
	bottom: 0px;
}
#edit_image {
	height: 600px;
	position: relative;
}
div.edit {
	float: right;
	width: 772px;
	z-index: 0;
	position: relative;
}

ul.files {
	display: block;
	width: auto;
	height: 585px;
	margin: 0px;
	padding: 0px 0px 15px 0px;
	overflow-y: scroll;
	position: relative;
}

li.file {
	display: block;
	width: auto;
	margin: 0px;
	padding: 8px 8px;
	border-bottom: 1px solid #DFDFDF;
	overflow: hidden;
	background: white;
}

li.file:hover, li.file.active {
	cursor: pointer;
	background-color: #EAF0FF;
	border-bottom: 1px solid #D1DEFF;
}

li.file h4 {
	font-size: 14px;
	font-weight: bold;
	color: #222;
	display: block;
	width: auto;
}

li.file > div.details {
	padding: 6px;
	float: left;
	width: 174px;
}

div.details > span {
	color: #888;
	padding-top: 2px;
	display: block;
	width: auto;
}

li.file div.thumb {
	width: 75px;
	height: 75px;
	display: block;
	float: left;
	margin: 0px;
	padding: 0px;
	text-align: center;
	vertical-align: middle;
}

div.thumb > img {
	float: none;
	display: inline-block;
	vertical-align: middle;
	border-radius:300px;
}

#upload_images {
	padding: 10px;
}

#image_container {
	padding: 0px;
	margin: 15px 0px 15px 0px;
	display: inline-block;
	float: none;
	padding: 10px;
	overflow: hidden;
	background: white;
	border: 1px solid #E0E0E0;
}

#image_search_field {
	width: 265px;
	margin: 20px 0px 0px 0px;
	float: left;
}
#image_search_field.loading {
	background-image: url('images/spinner_small.gif');
	background-color: white;
	background-position: 95% center;
	background-repeat: no-repeat;
}
</style>
<div id="image-library">
	<div class="top-bar">
		<form name="image_search" id="image_search_form">
			<input type="text" id="image_search_field" class="text" name="q" value="" placeholder="Search images..." />
		</form>
		<form method="post" action="" id="upload_file" style="margin: 10px; float: right;">
			<input type="hidden" name="module" value="<?php echo $module; ?>" />
			<input type="submit" name="submit" id="submit" value='Upload' style="float: right;" class="button" />
			<input type="file" name="userfile" id="userfile" size="20" style="float: right;" />
			<label for="userfile">File</label>
		</form>
	</div>
	<div class="files">
		<!-- <a href="#" class="upload-images button green" style="display: block; float: none; margin-right: 0px;">+ Upload Images</a> -->
		<?php $this->load->view('upload/image_list'); ?>
	</div>
	<div class="edit">
		<div id="upload_images">
			<!-- <form method="post" action="" id="upload_file">
				<label for="title">Title</label>
				<input type="text" name="title" id="title" value="" />
				<label for="userfile">File</label>
				<input type="file" name="userfile" id="userfile" size="20" />
				<input type="submit" name="submit" id="submit" value='Upload' class="button" />
			</form> -->
		</div>
		<div id="edit_image" style="display: none;"></div>
	</div>
</div>