<?php
class Table_Layout extends CI_Model
{
	function __construct(){
		parent::__construct();
	}

	// Save a new layout
	function save($layout_id, $name, $order = 0){

		if(empty($layout_id)){
			$this->db->insert('table_layouts', array(
				'name' => $name,
				'order' => $order,
				'course_id' => $this->session->userdata('course_id')
			));
			$layout_id = (int) $this->db->insert_id();

		}else{

		}

		return $layout_id;
	}

	// Retrieve layout data
	function get($layout_id = null)
	{
		$layouts = array();

		// Select all layouts and objects belonging to those layouts
		$this->db->select('layout.layout_id, layout.name AS layout_name,
			layout.order AS layout_order, layout.course_id,

			open_table.employee_id, open_table.sale_time AS time_opened,
			employee.first_name AS employee_first_name,
			employee.last_name AS employee_last_name,

			object.label AS object_label, object.type AS object_type,
			object.pos_x AS object_pos_x, object.pos_y AS object_pos_y,
			object.width AS object_width, object.height AS object_height,
			object.rotation AS object_rotation, object.object_id', false);
		$this->db->from('table_layouts AS layout');
		$this->db->join('table_layout_objects AS object', 'object.layout_id = layout.layout_id', 'left');
		$this->db->join('tables AS open_table', 'open_table.table_id = object.label', 'left');
		$this->db->join('people AS employee', 'employee.person_id = open_table.employee_id', 'left');

		if(!empty($layout_id)){
			$this->db->where('layout.layout_id', (int) $layout_id);
		}
		$this->db->where('layout.course_id', $this->session->userdata('course_id'));
		$result = $this->db->get();
		$rows = $result->result_array();

		// Loop through layouts and objects. Organize into multi-dimensional array
		// according to layout
		foreach($rows as $key => $row){

			$layout =& $layouts[$row['layout_id']];
			$layout['layout_id'] = $row['layout_id'];
			$layout['name'] = $row['layout_name'];
			$layout['order'] = $row['layout_order'];

			if(!empty($row['object_id'])){
				$layout['objects'][] = array(
					'object_id' => $row['object_id'],
					'label' => $row['object_label'],
					'type' => $row['object_type'],
					'pos_x' => $row['object_pos_x'],
					'pos_y' => $row['object_pos_y'],
					'rotation' => $row['object_rotation'],
					'width' => $row['object_width'],
					'height' => $row['object_height'],
					'employee_id' => $row['employee_id'],
					'employee_first_name' => $row['employee_first_name'],
					'employee_last_name' => $row['employee_last_name']
				);
			}
		}

		return $layouts;
	}

	// Delete layout
	function delete($layout_id){
		if(empty($layout_id)){
			return false;
		}

		$layout_delete = $this->db->delete('table_layouts', array('layout_id'=>$layout_id, 'course_id'=>$this->session->userdata('course_id')));
		if(!$layout_delete){
			return false;
		}
		$success = $this->db->delete('table_layout_objects', array('layout_id'=>$layout_id));

		return $success;
	}


}
?>