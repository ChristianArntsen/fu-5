<?php
require_once("report.php");
class Detailed_sales extends Report
{
	function __construct()
	{
		parent::__construct();
	}

	public function getDataColumns()
	{
		return array(
            'summary' => array(
                array('data'=>lang('reports_sale_id'), 'align'=> 'left'),
                array('data'=>lang('reports_date'), 'align'=> 'left'),
                array('data'=>lang('reports_items'), 'align'=> 'left'),
                array('data'=>lang('reports_sold_by'), 'align'=> 'left'),
                array('data'=>lang('reports_sold_to'), 'align'=> 'left'),
                array('data'=>lang('reports_subtotal'), 'align'=> 'right'),
                array('data'=>lang('reports_total'), 'align'=> 'right'),
                array('data'=>lang('reports_tax'), 'align'=> 'right'),
                array('data'=>lang('reports_profit'), 'align'=> 'right'),
                array('data'=>lang('reports_payment_type'), 'align'=> 'right')
//                array('data'=>lang('reports_comments'), 'align'=> 'right')
                ),
            'details' => array(
                array('data'=>lang('reports_item_number'), 'align'=> 'left'),
                array('data'=>lang('reports_name'), 'align'=> 'left'),
                array('data'=>lang('reports_category'), 'align'=> 'left'),
                array('data'=>lang('reports_subcategory'), 'align'=> 'left'),
                array('data'=>lang('reports_description'), 'align'=> 'left'),
                array('data'=>lang('reports_qty'), 'align'=> 'right'),
                array('data'=>lang('reports_subtotal'), 'align'=> 'right'),
                array('data'=>lang('reports_total'), 'align'=> 'right'),
                array('data'=>lang('reports_tax'), 'align'=> 'right'),
                array('data'=>lang('reports_profit'), 'align'=> 'right'),
                array('data'=>lang('reports_discount'), 'align'=> 'right')
                )
            );
	}

	public function getData()
	{
		//if (!$this->permissions->is_super_admin())
                  //  $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->select('sale_id, sale_date, sum(quantity_purchased) as items_purchased, CONCAT(employee.first_name," ",employee.last_name) as employee_name, CONCAT(customer.first_name," ",customer.last_name) as customer_name, sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit, payment_type, comment', false);
		$this->db->from('sales_items_temp');
		$this->db->join('people as employee', 'sales_items_temp.employee_id = employee.person_id');
		$this->db->join('people as customer', 'sales_items_temp.customer_id = customer.person_id', 'left');
		//$this->db->select('sale_id, sale_date, sum(quantity_purchased) as items_purchased, NULL as employee_name, NULL as customer_name, sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit, payment_type, comment', false);
		//$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		if ($this->params['department'] != 'all')
			$this->db->where('department = "'.$this->params['department'].'"');
		if ($this->params['terminal'] != 'all')
			$this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		if ($this->params['sale_id'] != '')
			$this->db->where('sale_id = "'.$this->params['sale_id'].'"');
		$this->db->where('deleted', 0);
		$this->db->group_by('sale_id');
		$this->db->order_by('sale_date');

		$data = array();
		$summary = $this->db->get();
		//echo '<br/>';
		//print_r($summary);
		$data['summary'] = $summary->result_array();
		$data['details'] = array();
		//while ($value = $summary->fetch_assoc())

		/* Sale details will now be fetched via ajax
		if (strtotime($this->params['end_date']) - strtotime($this->params['start_date']) < (62 * 24 * 60 * 60)) //Only add details if the time span is less than 2 months
		{
			foreach($data['summary'] as $key=>$value)
			{
	            $this->db->select("items.item_number as item_number, item_kits.item_kit_number as item_kit_number, items.name as item_name, item_kits.name as item_kit_name, invoice_number, sales_items_temp.category, sales_items_temp.subcategory, quantity_purchased, serialnumber, sales_items_temp.description, subtotal,sales_items_temp.total, tax, profit, discount_percent");
				$this->db->from('sales_items_temp');
				$this->db->join('items', 'sales_items_temp.item_id = items.item_id', 'left');
				$this->db->join('item_kits', 'sales_items_temp.item_kit_id = item_kits.item_kit_id', 'left');
				$this->db->join('invoices', 'sales_items_temp.invoice_id = invoices.invoice_id', 'left');
				$this->db->where("sale_id = '{$value['sale_id']}'");
				$details = $this->db->get();

				$data['details'][] = $details->result_array();
				$details->free_result();
			}
		}*/

		return $data;
	}

	/*public function getSummaryData()
	{
		//if (!$this->permissions->is_super_admin())
                  //  $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		if ($this->params['department'] != 'all')
			$this->db->where('department = "'.$this->params['department'].'"');
		$this->db->where('deleted', 0);
		return $this->db->get()->row_array();
	}*/
	// public function getPaymentData()
	// {
		// $sales_totals = array();
		// // WE'RE JUST GRABBING THE SALES ITEM TOTALS. NOT PAYMENTS, JUST SALES ITEM TOTALAS
		// $this->db->select('sale_id, SUM(total) as total', false);
		// $this->db->from('sales_items_temp');
		// $this->db->group_by('sale_id');
		// if ($this->params['sale_id'] != '')
			// $this->db->where('sale_id = "'.$this->params['sale_id'].'"');
		// $sales = $this->db->get()->result_array();
//
		// foreach($sales as $sale_total_row)
		// {
			// $sales_totals[$sale_total_row['sale_id']] = $sale_total_row['total'];
		// }
//
		// // HERE WE'RE GRABBING THE PAYMENTS FOR ALL SALES. THIS INCLUDES TIPS, WHICH DON'T SHOW UP IN THE PREVIOUS LIST/QUERY
		// $this->db->select('sales_payments.sale_id, sales_payments.payment_type, payment_amount', false);
		// $this->db->from('sales_payments');
		// $this->db->join('sales', 'sales.sale_id=sales_payments.sale_id and foreup_sales.course_id = "'.$this->session->userdata('course_id').'"');
		// $this->db->where('date(sale_time) BETWEEN "'. $this->params['start_date']. '" and "'. $this->params['end_date'].'"');
		// if ($this->params['sale_id'] != '')
			// $this->db->where('foreup_sales.sale_id = "'.$this->params['sale_id'].'"');
		// if ($this->params['sale_type'] == 'sales')
		// {
			// $this->db->where('payment_amount > 0');
		// }
		// elseif ($this->params['sale_type'] == 'returns')
		// {
			// $this->db->where('payment_amount < 0');
		// }
		// //if ($this->params['department'] != 'all')
		// //	$this->db->where('department = "'.$this->params['department'].'"');
		// if ($this->params['terminal'] != 'all')
			// $this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		// if ($this->params['sale_id'] != '')
			// $this->db->where('sale_id = "'.$this->params['sale_id'].'"');
//
		// $this->db->where($this->db->dbprefix('sales').'.deleted', 0);
		// $this->db->order_by('sale_id, payment_type');
		// $sales_payments = $this->db->get()->result_array();
		// //print_r($sales_payments);
		// foreach($sales_payments as $row)
		// {
        	// $payments_by_sale[$row['sale_id']][] = $row;
		// }
//
		// $payment_data = array();
//
		// foreach($payments_by_sale as $sale_id => $payment_rows)
		// {
			// //print_r($payments_by_sale);
			// $total_sale_balance = $sales_totals[$sale_id];
			// foreach($payment_rows as $row)
			// {
				// $payment_amount = $row['payment_amount'];// <= $total_sale_balance ? $row['payment_amount'] : $total_sale_balance;
				// $cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
				// $mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
				// //echo '<br/>'.$row['payment_type'].' - '.$row['payment_amount'].'<br/>';
				// //Consolidating Credit Cards,  Account Payments, and Giftcards
				// if (strpos($row['payment_type'], 'Tip') !== false)
				// {
					// $row['payment_type'] = 'Tips';
				// }
				// else if (strpos($row['payment_type'], 'M/C') !== false || strpos($row['payment_type'], 'VISA') !== false || strpos($row['payment_type'], 'AMEX') !== false || strpos($row['payment_type'], 'DCVR') !== false)
				// {
					// $row['payment_type'] = 'Credit Card';
				// }
				// else if (strpos($row['payment_type'], $cdn) !== false)
				// {
						// $row['payment_type'] = $cdn;
						// //echo $payment_amount.'<br/>';
				// }
				// else if (strpos($row['payment_type'], $mbn) !== false)
				// {
						// $row['payment_type'] = $mbn;
						// //echo $payment_amount.'<br/>';
				// }
				// else if (strpos($row['payment_type'], 'Change') !== false)
				// {
					// $row['payment_type'] = 'Cash';
				// }
				// else if (strpos($row['payment_type'], 'Gift Card') !== false)
					// $row['payment_type'] = 'Gift Card';
				// else if (strpos($row['payment_type'], 'Raincheck') !== false)
					// $row['payment_type'] = 'Raincheck';
//
				// //if($row['payment_type'] == 'Credit Card')
					// //echo 'Sale '.$row['sale_id'].' - '.$payment_amount."<br/>";
//
				// //echo $row['payment_type'].' - '.$row['payment_amount'].'<br/>';
				// //echo 'total_sales-balance '.$total_sale_balance.'<br/>';
				// if (!isset($payment_data[$row['payment_type']]))
				// {
					// $payment_data[$row['payment_type']] = array('payment_type' => $row['payment_type'], 'payment_amount' => 0 );
				// }
				// if ($total_sale_balance != 0 || $row['payment_type'] == 'Tips')
				// {
					// $payment_data[$row['payment_type']]['payment_amount'] += $payment_amount;
				// }
				// if (!isset($payment_data['Total']))
					// $payment_data['Total'] = array('payment_type' => 'Total', 'payment_amount' => 0 );
				// if ($total_sale_balance != 0)
					// $payment_data['Total']['payment_amount'] += $payment_amount;
				// //print_r($payment_data);
				// $total_sale_balance-=$payment_amount;
			// }
// //echo $row['payment_amount'].' '.$total_sale_balance.'<br/>';
		// }
		// //echo $this->db->last_query();
		// return $payment_data;
	// }
	// public function getSummaryData()
	// {
		// //if (!$this->permissions->is_super_admin())
                  // //  $this->db->where('course_id', $this->session->userdata('course_id'));
		// /*$this->db->select('payment_type');
		// $this->db->from('sales_items_temp');
		// $this->db->group_by('sale_id');
		// $results = $this->db->get()->result_array();
		// $account_charges = 0;
		// foreach($results as $result) {
			// //TODO:Account for multiple payments on account in one transaction
			// if (preg_match('/Account- [^$]+: \$(.+)</',$result['payment_type'], $match)) {
				// $account_charges += (float)$match[1];
			// }
		// }*/
		// $totals = $this->getPaymentData();
		// //echo 'Account total: '.$account_charges;
		// //print_r($results);
		// $cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
		// $mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
		// $account_charges = (isset($totals[$cdn]['payment_amount']))?$totals[$cdn]['payment_amount']:0;
		// $member_balance = (isset($totals[$mbn]['payment_amount']))?$totals[$mbn]['payment_amount']:0;
		// $giftcards = (isset($totals['Gift Card']['payment_amount']))?$totals['Gift Card']['payment_amount']:0;
		// $rainchecks = (isset($totals['Raincheck']['payment_amount']))?$totals['Raincheck']['payment_amount']:0;
		// $tips = (isset($totals['Tips']['payment_amount']))?$totals['Tips']['payment_amount']:0;
		// $total_payments = (isset($totals['Total']['payment_amount']))?$totals['Total']['payment_amount']-$account_charges-$member_balance-$giftcards:0;		//echo 'Account total: '.$account_charges;
		// $credit_card = (isset($totals['Credit Card']['payment_amount']))?$totals['Credit Card']['payment_amount']:0;
		// $check = (isset($totals['Check']['payment_amount']))?$totals['Check']['payment_amount']:0;
		// $cash = (isset($totals['Cash']['payment_amount']))?$totals['Cash']['payment_amount']:0;
		// $total_payments = $credit_card + $check + $cash;//(isset($totals['Total']['payment_amount']))?$totals['Total']['payment_amount']-$account_charges-$member_balance-$giftcards:0;		//echo 'Account total: '.$account_charges;
//
		// //This section was implemented for East Bay, it is a temporary fix, and must be updated
		// //TODO: Remove this and fix the problem of tracking where account balances are spent.
		// $this->db->select('sum(total) as ps_total');
		// $this->db->from('sales_items_temp');
		// if ($this->params['sale_type'] == 'sales')
		// {
			// $this->db->where('quantity_purchased > 0');
		// }
		// elseif ($this->params['sale_type'] == 'returns')
		// {
			// $this->db->where('quantity_purchased < 0');
		// }
		// $this->db->where('department = "Pro Shop"');
		// if ($this->params['sale_id'] != '')
			// $this->db->where('sale_id = "'.$this->params['sale_id'].'"');
//
		// $this->db->where('deleted', 0);
		// $summary_data = array();
		// foreach ($this->db->get()->row_array() as $key => $value)
			// if($value > 0 && $this->session->userdata('course_id') == 6279)
				// $summary_data[$key] = $value - $account_charges - $member_balance;
		// //END EAST BAY CUSTOM FIX
		// //$summary_data['profit'] =
//
		// //Tax totaling fix
        // $this->db->select('sum(subtotal) as subtotal, percent');
		// $this->db->from('sales_items_temp');
		// $this->db->where("percent != ''");
		// if ($this->params['sale_type'] == 'sales')
		// {
			// $this->db->where('quantity_purchased > 0');
		// }
		// elseif ($this->params['sale_type'] == 'returns')
		// {
			// $this->db->where('quantity_purchased < 0');
		// }
		// if ($this->params['department'] != 'all')
			// $this->db->where('department = "'.$this->params['department'].'"');
		// if ($this->params['terminal'] != 'all')
			// $this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		// if ($this->params['sale_id'] != '')
			// $this->db->where('sale_id = "'.$this->params['sale_id'].'"');
//
		// $this->db->where('deleted', 0);
		// $this->db->group_by('percent');
		// foreach ($this->db->get()->result_array() as $key => $row)
		// {
			// //print_r($row);
			// $percent = ($row['percent'] != '')?$row['percent']:0;
			// //$summary_data['tax'] += $row['subtotal']*($percent/100);
		// }
		// //$summary_data['tax'] = round($summary_data['tax'], 2);
		// //End tax totaling fix
//
        // $this->db->select('sum(subtotal) as subtotal, sum(tax) as collected_tax, sum(item_cost_price) as cost, sum(total) as total, sum(profit) as profit');
		// $this->db->from('sales_items_temp');
		// if ($this->params['sale_type'] == 'sales')
		// {
			// $this->db->where('quantity_purchased > 0');
		// }
		// elseif ($this->params['sale_type'] == 'returns')
		// {
			// $this->db->where('quantity_purchased < 0');
		// }
		// if ($this->params['department'] != 'all')
			// $this->db->where('department = "'.$this->params['department'].'"');
		// if ($this->params['terminal'] != 'all')
			// $this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		// if ($this->params['sale_id'] != '')
			// $this->db->where('sale_id = "'.$this->params['sale_id'].'"');
//
		// $this->db->where('deleted', 0);
		// //$summary_data = array();
		// $totals_array = $this->db->get()->row_array();
// //		foreach ($this->db->get()->row_array() as $key => $value)
	// //		if ($key != 'tax' && $key!='subtotal')
		// //		$summary_data[$key] = $value - $account_charges;
			// //else if ($key == 'subtotal') {
				// //$summary_data[$key] = $value;
				// //$summary_data['account_charges'] = -$account_charges;
			// //}
			// //else
				// //$summary_data[$key] = $value;
		// $summary_data = array();
		// if ($this->params['department'] != 'all')
		// {
			// $summary_data['subtotal'] = $totals_array['subtotal'];
			// //$summary_data['account_charges'] = -$account_charges;
			// //$summary_data['member_balance'] = -$member_balance;
			// //$summary_data['giftcards'] = -$giftcards;
			// //$summary_data['rounding_error'] = $totals_array['collected_tax'] - $summary_data['tax'];
			// //$summary_data['profit'] = $totals_array['profit'];//$summary_data['subtotal'] - $totals_array['cost'] + $summary_data['rounding_error'] - $account_charges;
			// $summary_data['tax'] = $totals_array['collected_tax'];
			// $summary_data['total'] = $totals_array['total'];//$total_payments;
		// }
		// else
		// {
			// $summary_data['subtotal'] = $totals_array['subtotal'];
			// $summary_data['account_charges'] = -$account_charges;
			// $summary_data['member_balance'] = -$member_balance;
			// $summary_data['giftcards'] = -$giftcards;
			// $summary_data['rainchecks'] = -$rainchecks;
			// $summary_data['tips'] = $tips;
			// //$summary_data['rounding_error'] = $totals_array['collected_tax'] - $summary_data['tax'];
			// //$summary_data['profit'] = $totals_array['profit'];//$summary_data['subtotal'] - $totals_array['cost'] + $summary_data['rounding_error'] - $account_charges;
			// $summary_data['tax'] = $totals_array['collected_tax'];
			// $summary_data['total'] = $total_payments;
//
		// }
		// return $summary_data;
	// }

	public function get_quickbooks_data($courseId, $offset = 0, $numRecords = 5000){

		$data = array();

		// Select all sales from temporary table
		$this->db->select('sales_items_temp.sale_id, sales_items_temp.sale_date AS sale_time,
			CONCAT(employee.first_name," ",employee.last_name) as employee_name, employee.person_id AS employee_id,
			CONCAT(customer.first_name," ",customer.last_name) as customer_name, customer.person_id AS customer_id', false);
		$this->db->from('sales_items_temp');
		$this->db->join('people as employee', 'sales_items_temp.employee_id = employee.person_id', 'left');
		$this->db->join('customers as c_customer', 'sales_items_temp.customer_id = c_customer.person_id AND c_customer.course_id = '.(int) $courseId, 'left');
		$this->db->join('people as customer', 'c_customer.person_id = customer.person_id', 'left');

		if ($this->params['sale_type'] == 'sales'){
			$this->db->join('quickbooks_sync AS sync', "sync.record_id = sale_id AND sync.record_type = 'sale' AND sync.course_id = ".(int) $courseId, 'left');
			$this->db->where('quantity_purchased > 0');
		}elseif ($this->params['sale_type'] == 'returns'){
			$this->db->join('quickbooks_sync AS sync', "sync.record_id = sale_id AND sync.record_type = 'return' AND sync.course_id = ".(int) $courseId, 'left');
			$this->db->where('quantity_purchased < 0');
		}

		$this->db->where('sync.record_id IS NULL');
		$this->db->where('sales_items_temp.deleted', '0');
		$this->db->group_by('sales_items_temp.sale_id');
		$this->db->limit($numRecords, $offset);

		$sales = $this->db->get()->result_array();

		// Loop through each sale and retrieve the individual items in that sale
		foreach($sales as $key => $summary){

			// Retrieve items, invoices, or item kits attached to sale
			$this->db->select('items.item_id, items.item_number as item_number, items.name as item_name,
				item_kits.item_kit_id, item_kits.item_kit_number as item_kit_number, item_kits.name as item_kit_name,
				invoices.invoice_id, invoices.invoice_number, invoices.name AS invoice_name,
				sales_items_temp.category, sales_items_temp.subcategory, quantity_purchased, serialnumber,
				sales_items_temp.description, sales_items_temp.subtotal,sales_items_temp.total,
				sales_items_temp.tax, sales_items_temp.profit, sales_items_temp.discount_percent');
			$this->db->from('sales_items_temp');
			$this->db->join('items', "sales_items_temp.item_id = items.item_id AND ".$this->db->dbprefix('items').".course_id = ".(int) $courseId, 'left');
			$this->db->join('item_kits', "sales_items_temp.item_kit_id = item_kits.item_kit_id AND ".$this->db->dbprefix('item_kits').".course_id = ".(int) $courseId, 'left');
			$this->db->join('invoices', "sales_items_temp.invoice_id = invoices.invoice_id AND ".$this->db->dbprefix('invoices').".course_id = ".(int) $courseId, 'left');
			$this->db->where(array('sales_items_temp.sale_id' => $summary['sale_id']));

			$sales[$key]['total'] = 0.00;
			$items = $this->db->get()->result_array();

			foreach($items as $item){
				$sales[$key]['total'] += $item['total'];
			}

			$sales[$key]['details'] = $items;

			// Retrieve all payments on sale (exlcuding tips)
			$result = $this->db->query("SELECT sale_id, payment_amount, payment_type
				FROM ".$this->db->dbprefix('sales_payments')."
				WHERE sale_id = ".(int) $summary['sale_id']."
					AND payment_type NOT LIKE '%tip%'
				ORDER BY IF(payment_type='Change issued', 1, 0) DESC");

			$sales[$key]['payments'] = $result->result_array();
		}

		return $sales;
	}
}
?>