<?php
class Bartered_teetimes extends CI_Model
{
	/*
	Determines if a given teetime_id is an teetime
	*/
	function exists($barter_id)
	{
		$teesheet_id = "AND teesheet_id = '{$this->session->userdata('teesheet_id')}'";
		$this->db->from('teetimes_bartered');
		$this->db->where("barter_id = '$barter_id' $teesheet_id");
		$this->db->limit(1);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}

	/*
	Returns all the teetimes
	*/
	function get_all($limit=10000, $offset=0)
	{
        $this->db->from('teetimes_bartered');
		$this->db->where("deleted = 0");
        $this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('teetime');
		$this->db->where("deleted = 0 $course_id");
        return $this->db->count_all_results();
	}
	
	/*
	Gets information about a particular teetime
	*/
	function get_info($teetime_id)
	{
		$this->db->from('teetime');
		$this->db->where("TTID = '$teetime_id'");
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $teetime_id is NOT a teetime
			$teetime_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('teetime');

			foreach ($fields as $field)
			{
				$teetime_obj->$field='';
			}

			return $teetime_obj;
		}
	}

	/*
	Inserts or updates a teetime
	*/
	function save(&$teetime_data,$teetime_id=false, &$json_ready_events=array())
	{
		if ($teetime_id)
			$cur_teetime_data = $this->get_info($teetime_id);
		else 
			$teetime_id = $this->generate_id('tt');
		$second_id = $teetime_id.'b';
	    if (strlen($teetime_id) > 20) 
            $second_id = substr($teetime_id, 0, 20);
		$second_data = array();
			
		$frontnine = $this->session->userdata('frontnine');
		$dif = fmod((floor($frontnine/100)*60+$frontnine%100), $this->session->userdata('increment'));
		$frontnine += ($dif > $this->session->userdata('increment')/2)?($this->session->userdata('increment') - $dif):-$dif;
			
		if ($teetime_data['booking_source'] == 'online') //Not yet adjusting for simulators
		{
			$second_data = $teetime_data;
			$teetime_data['TTID'] = $teetime_id;
			$second_data['TTID'] = $second_id;
			$second_data['side'] = 'back';
			$second_data['start'] += $frontnine;
        	$second_data['end'] += $frontnine;
        
			if ($second_data['start']%100 >59)
	            $second_data['start'] += 40;
	        if ($second_data['end']%100 >59)
	            $second_data['end'] += 40;
				
			if ($this->session->userdata('is_simulator') || $teetime_data['holes'] == 9)
				$second_data['status'] = 'deleted';
			
		}
		else {
			// Set the id of the two tee times
	        $course_holes = $this->session->userdata('holes');
	        $start = $end = '';
			if (isset($teetime_data['start']))
				$start = $teetime_data['start'];
			else if (isset($cur_teetime_data['start']))
				$start = $cur_teetime_data['start'];
	        if (isset($teetime_data['end']))
				$end = $teetime_data['end'];
			else if (isset($cur_teetime_data['end']))
				$end = $cur_teetime_data['end'];
	        
	        $second_data = $teetime_data;
			$second_data['TTID'] = $second_id;
			if (!isset($teetime_data['status']) && isset($teetime_data['holes']) && $teetime_data['holes'] == 18)
				$second_data['status'] = $cur_teetime_data->status;
			$success = true;
			// Erasing second tee time if there is one since we're only playing 9
	        if (isset($teetime_data['holes']) && $teetime_data['holes'] == 9) {
	            if (strlen($teetime_id) > 20)
					$teetime_data['status'] = 'deleted';
				else 
					$second_data['status'] = 'deleted';
			}
	        // Updating secondary tee time
	        $second_side = 'back';
	        if ((isset($cur_teetime_data->side) && $cur_teetime_data->side =='back') || 
		        	(isset($teetime_data['side']) && $teetime_data['side'] == 'back') || 
		        	$course_holes == 9) 
	            $second_side = 'front';
	        
	        if (strlen($teetime_id) > 20) {
	            unset($second_data['start']);
			    unset($second_data['end']);
			}
			else {
				$second_data['start'] = $start + $frontnine;
	        	$second_data['end'] = $end + $frontnine;
	        
				if ($second_data['start']%100 >59)
		            $second_data['start'] += 40;
		        if ($second_data['end']%100 >59)
		            $second_data['end'] += 40;
			}
	    	$second_data['side'] = $second_side;
		}
        
		if (!$second_id or !$this->exists($second_id))
		{
        	if($this->db->insert('teetime',$second_data))
        		$success = true;
			else
	        	$success = false;
		}
		else 
		{
			$this->db->where('TTID', $second_id);
			$success = $this->db->update('teetime',$second_data);
			$json_ready_events[] = $this->add_teetime_styles(array_merge($second_data, (array)$this->get_info($second_data['TTID'])));
		}
		
        $json_ready_events[] = $this->add_teetime_styles(array_merge((array)$cur_teetime_data, $teetime_data));
        if ($success && !$this->exists($teetime_id))
		{
			if($this->db->insert('teetime',$teetime_data))
        		return true;
			return false;
		}
		//echo 'making it to the end';
		$this->db->where('TTID', $teetime_id);
		return $this->db->update('teetime',$teetime_data);
	}

	function add_teetime_styles($teetime_data) {
		//echo 'adding teetime styles';
		//return $teetime_data;
		$return_data = array();
		$return_data['id'] = $teetime_data['TTID'];
		$return_data['type'] = $teetime_data['type'];
		$return_data['name'] = $this->permissions->course_has_module('reservations') ? $this->schedule->clean_for_json($teetime_data['title']) : $this->teesheet->clean_for_json($teetime_data['title']);
		$return_data['status'] = $teetime_data['status'];
		$return_data['title'] = $this->permissions->course_has_module('reservations') ? $this->schedule->clean_for_json($teetime_data['title']) : $this->teesheet->clean_for_json($teetime_data['title']);
		$return_data['allDay'] = '';
		$return_data['stimestamp'] = $teetime_data['start'];
		$return_data['etimestamp'] = $teetime_data['end'];
        $return_data['start'] = $teetime_data['start'];
		$return_data['end'] = $teetime_data['end'];
        $return_data['side'] = $teetime_data['side'];
		$return_data['player_count'] = $teetime_data['player_count'];
		$return_data['carts'] = $teetime_data['carts'];
		$return_data['backgroundColor']= '';
		if ($teetime_data['holes'] == 18)
			$return_data['borderColor']= 'orange';
		else
			$return_data['borderColor']= '';
		     
		//Add classes for styling
		$booked_class = $cart_class = $checkedin_class = '';
		if ($teetime_data['booking_source'])
			$booked_class = 'booked_online';
        if ($teetime_data['carts'] > 0)
            $cart_class = 'carts';
        if (($teetime_data['status'] == 'checked in' || $teetime_data['status'] == 'walked in') && !$this->config->item('sales'))
            $checkedin_class = 'checked_in';
		
	    if ($teetime_data['paid_player_count'] == 0)
            $paid_class = '';
		else if ($this->config->item('simulator'))
			$paid_class =  "paid_{$this->session->userdata('holes')}_4";
        else if ($teetime_data['player_count'] == 5)
            $paid_class = "paid_{$this->session->userdata('holes')}_{$teetime_data['paid_player_count']}_5";//_{$teetime_data->player_count}";
	    else //if ($teetime_data->paid_player_count < $teetime_data->player_count)
            $paid_class = "paid_{$this->session->userdata('holes')}_{$teetime_data['paid_player_count']}";//_{$teetime_data->player_count}";
		$return_data['className'] = $teetime_data['type'].' holes_'.$teetime_data['holes'].' players_'.$teetime_data['player_count'].' '.$cart_class.' '.$checkedin_class.' '.$paid_class;

		return $return_data;
		
//                $return_data['start'] = $this->format_time_string($teetime->start);
//                $return_data['end'] = $this->format_time_string($teetime->end);
            
        /*if ($teetime_data->type == 'teetime' && ($teetime_data->status == 'checked in' || $teetime_data->status == 'walked in')) {
            $return_data['backgroundColor'] = '#5c9ccc';
            $return_data['borderColor'] = '#4297d7';
        }
        else if ($teetime_data->type == 'tournament') {
            $return_data['backgroundColor'] = '#5f9b5f';
            $return_data['borderColor'] = '#336133';
        }
        else if ($teetime_data->holes == 18) {
            $return_data['borderColor'] = '#333';
            $return_data['backgroundColor'] = '#445651';
        }*/
        
		
        
	}
	/*
	Deletes one teetime
	*/
	function delete($teetime_id)
	{
		$this->db->where("TTID", substr($teetime_id,0,20));
		$this->db->or_where("TTID", substr($teetime_id,0,20).'b');
		$this->db->limit(2);
        return $this->db->update('teetime', array('status' => 'deleted'));
		echo $this->db->last_query();
		return;
    }
	function generate_id($type = 'tt') {
        if ($type == 'tt')
            $prefix = 'TTID_';
        else if ($type == 'ts')
            $prefix = 'teesheet_id_';
        else
            $prefix = '';
       
        $length = 20 - strlen($prefix);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $string = $prefix;
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters)-1)];
        }
        return $string;
    }
	function get_stats($view, $year, $month, $day, $dow) {
        $brand = $this->session->userdata("teesheet_id");
        $statsArray = array();
        $eday = $day+1;
        if ($month < 10)
            $month = "0".$month;
        if ($day < 10)
            $day = "0".$day;
        if ($eday < 10)
            $eday = "0".$eday;
        if ($view == 'agendaDay') {
            //get one day stats
            
            $dayString = $year.$month.$day.'0000';
            $edayString = $year.$month.$eday.'0000';
			if ($this->config->item('sales'))
	            $this->db->select("choles,
	                count(*) AS teetimes,
	                sum(case WHEN `status` = 'checked in' THEN 1 ELSE 0 END) AS tcheckin,
	                sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
	                sum(`player_count`) AS players,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `paid_player_count` ELSE 0 END) AS cplayers,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `paid_carts` ELSE 0 END) AS ccarts,
	                sum(`carts`) AS carts");
			else
	            $this->db->select("choles,
	                count(*) AS teetimes,
	                sum(case WHEN `status` = 'checked in' THEN 1 ELSE 0 END) AS tcheckin,
	                sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
	                sum(`player_count`) AS players,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `player_count` ELSE 0 END) AS cplayers,
	                sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in') THEN `carts` ELSE 0 END) AS ccarts,
	                sum(`carts`) AS carts");
            $this->db->from('teetime');
            $this->db->where("teesheet_id = '$brand'
                    AND `start` > '$dayString' 
                    AND `end` < '$edayString'
                    AND `status` != 'deleted'
                    AND `TTID` NOT LIKE '____________________b'");
            $this->db->group_by('choles');
            
            $results = $this->db->get();
            foreach($results->result() as $result) {
                if ($result->choles == '9')
                    $statsArray['nine'] = $result;
                else if ($result->choles == '18')
                    $statsArray['eighteen'] = $result;
            }
        }
        else if ($view == 'agendaWeek') {
            //get full week stats
        }
        return json_encode($statsArray);
    }

    public function get_total_sold_tee_times($last_week = false)
    {
      $course_id = '';
      if (!$this->permissions->is_super_admin())
          $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";

      $this->db->select('count(*) total');
      $this->db->from('teetimes_bartered');
      $this->db->join('teesheet', 'teetimes_bartered.teesheet_id = teesheet.teesheet_id');
      $this->db->where("deleted = 0 {$course_id}");
      if($last_week === true)
        $this->db->where('YEARWEEK(date_booked) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)');
        
      $query = $this->db->get();

    return $query->row()->total;
    }
}
?>
