<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * table headers
 */
$lang['promotions_name'] = 'Name';
$lang['promotions_number_available'] = 'Used/Issued';
$lang['promotions_amount_type'] = '$ or %';
$lang['promotions_amount'] = 'Amount';
$lang['promotions_rules'] = 'Rules';
$lang['promotions_no_data_to_display'] = 'No promotions to display.';

$lang['promotions_new'] = 'New Promotion';
$lang['promotions_update'] = 'Update Promotion';
$lang['promotions_confirm_delete']='Are you sure you want to delete the selected promotions?';
$lang['promotions_none_selected']='You have not selected any promotions to edit.';
$lang['promotions_error_adding_updating']='Error adding/updating promotion.';
$lang['promotions_successful_adding']='You have successfully added a new promotion.';
$lang['promotions_successful_updating']='You have successfully updated a promotion.';
$lang['promotions_successful_deleted']='You have successfully deleted';
$lang['promotions_one_or_multiple']='promotion(s)';
$lang['promotions_cannot_be_deleted']='Could not delete selected promotion, one or more promotion has sales.';

/*
 * Form
 */
$lang['promotions_basic_information'] = 'Promotion Form';
$lang['promotions_name'] = 'Name';
$lang['promotions_amount'] = 'Amount';
$lang['promotions_expiration_date'] = 'Expiration';
$lang['promotions_valid_for'] = 'Valid For';
$lang['promotions_valid_on'] = 'Valid On';
$lang['promotions_valid_between'] = 'Valid Between';
$lang['promotions_limit'] = 'Limit';
$lang['promotions_buy'] = 'Buy';
$lang['promotions_get'] = 'Get';
$lang['promotions_min_purchase'] = 'With minimum purchase of';
$lang['promotions_details'] = 'Additional Details';
/**
 * required fields
 */
$lang['promotions_name_required'] = 'Name is a required field.';
$lang['promotions_amount_required'] = 'Amount is a required field.';
$lang['promotions_expiration_date_required'] = 'Expiration is a required field.';
$lang['promotions_valid_for_required'] = 'Valid For is a required field.s';