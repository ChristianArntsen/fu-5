var data_sync = {
	// Down Sync Data
	pull : {
		initialize : function () {
			
		},
		all : function () {
			// PULL THE DATA FROM EVERY TABLE
			this.course_info();
			this.employees();
			this.customers();
			this.tee_times();
			this.items();
			this.item_kits();
		},
		course_info : function () {
			
		},
		employees : function () {
			
		},
		customers : function () {
			
		},
		tee_times : function () {
			
		},
		items : function () {
			
		},
		item_kits : function () {
			
		}
	},
	// UP Sync Data
	push : {
		tee_times : function () {
			
		},
		sales : function () {
			
		}
	}
};
