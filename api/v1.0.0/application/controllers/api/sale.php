<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Sale extends REST_Controller
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model('Api_data');		
		// CHECK API PERMISSIONS 
		$course_id = $this->input->get('course_id');
		$schedule_id = $this->input->get('schedule_id');
		
		if ($course_id)
	    	$permissions = $this->Api_data->course_permissions($course_id, false, $this->rest->api_id);
		else if ($schedule_id)
	    	$permissions = $this->Api_data->course_permissions(false, $schedule_id, $this->rest->api_id);

		if ((!$course_id && !$schedule_id)  || !$permissions['sales']) {
			$this->response(
				array(
					'status'=>400,
					'message'=>'Restricted Access',
					'description'=>'You do not have permissions to access this function',
					'code'=> 6040
				), 
				400
			);	
		}		
	}
	
	function record_get()
    {
    	// GET PARAMETERS
    	$course_id = $this->input->get('course_id');
		$schedule_id = $this->input->get('schedule_id');
    	$reservation_id = $this->input->get('reservation_id');
		$guest_info = $this->input->get('guest_info'); // EXPECTED JSON STRING {guest_8:{paid_green_fee:true,paid_cart:true}}
		$sale_date = $this->input->get('sale_date');
		$credit_card_info = $this->input->get('credit_card_info'); // EXPECTED JSON STRING {}
    	$guid = $this->input->get('sale_guid');
		
    	// VALIDATE PARAMETERS
		$errors = array();
		if (!$course_id || $course_id == '')
			$errors[] = 'Missing parameter: course_id';
		if (!$schedule_id || $schedule_id == '')
			$errors[] = 'Missing parameter: schedule_id';
		if (!$reservation_id || $reservation_id == '')
			$errors[] = 'Missing parameter: reservation_id';		
		if (!$guest_info || $guest_info == '')
			$errors[] = 'Missing parameter: guest_info';		
		if (!$sale_date || $sale_date == '')
			$errors[] = 'Missing parameter: sale_date';		
		if (!$credit_card_info || $credit_card_info == '')
			$errors[] = 'Missing parameter: credit_card_info';		
		if (!$guid || $guid == '')
			$errors[] = 'Missing parameter: sale_guid';		
		if (count($errors)>0)
			$this->response(array('success'=>false,'error'=>implode(', ', $errors)));
		
		// CHECK GUID
		$guid_sale = $this->Sale->get_sale_by_guid($sale_guid);
        if ($guid_sale['sale_id'])
        {
        	// RETURN SUCCESS - Make sure it matches the response at the bottom...
            echo json_encode(array('sale_id'=>'POS '.$guid_sale['sale_id']));
            return;
        }
		// START TRANSACTION
		$this->db->trans_start();
		
		// SAVE SALE
		$sale_obj = array(
			'course_id'=>$course_id,
			'sale_time'=>date('Y-m-d H:i:s', strtotime($sale_date)),
			'customer_id'=>'',// MAYBE
			'teetime_id'=>$reservation_id,
			'employee_id'=>'',//REQ'D?
			'comment'=>'', // FETCH API NAME HERE
			'terminal_id'=>0,
			'payment_type'=>'',
			'mobile_guid'=>$guid
		);
		$this->db->insert('sales',$sale_obj);
		$sale_id = $this->db->insert_id();
		
		if ($sale_id > 0)
		{
			// GET RESERVATION INFORMATION
			$reservation_info = $this->Api_data->tee_time_details($reservation_id, '', true);
			//$reservation_info['player_count'] = 6;
			// GET PRICE AND TAX INFO
			$green_fee_tax_info = $this->Api_data->get_tee_time_taxes($course_id);
			$cart_tax_info = $this->Api_data->get_cart_taxes($course_id);
			
			$course_info = $this->Course->get_info($course_id);
			$index_array = $this->Api_data->get_tee_time_index($course_info, $reservation_info['holes']);
			$price_types = $this->Green_fee->get_type_info($course_id, $schedule_id);
			//(float)$item_prices[$schedule_id][$course_id.'_'.$teetime_type_index]->price;
			$guest_info = array();
			$paid_green_fees = 0;
			$paid_carts = 0;
			$paid_individual_players = '';
			$line_number = 1;
			// GET PRICE CLASS INFORMATION FOR TEE TIME CUSTOMERS
			foreach($guest_info AS $index => $guest)
			{
				$key = substr($index, 6);
				
				if ($key < 6) 
				{
					
					$person_id = $key == 1 ? 'person_id' : 'person_id_'.$key;
					$customer_info = $this->Api_data->get_customer_info($reservation_info[$person_id], '', $course_id, true, true)->row_array();
					$customer_price_class = $customer_info['price_class'];
					$customer_price = $this->Api_data->get_tee_time_prices($schedule_id, $customer_price_class);
				}
				else 
				{
					$customer_price = $this->Api_data->get_tee_time_prices($schedule_id);
					$customer_price_class = 'price_category_1';
				}
				$price_category_label = $price_types[$schedule_id]->$price_category;
				$gf_item_number = $course_id.'_'.$index_array['gf'];
				$cart_item_number = $course_id.'_'.$index_array['cart'];
				
				// PURCHASING GREEN FEE/ADDING ITEM TO SALE
				if ($guest['paid_green_fee'])
				{
					
					$item_id = $this->Item->get_item_id($item_number, $course_id);
					$sales_items_data = array(
						'sale_id'=>$sale_id,
						'item_id'=>$item_id,
						'teesheet'=>$schedule_id,
						'price_category'=>$customer_price_class,
						'line'=>$line_number,
						'quantity_purchased'=>1,
						'item_cost_price'=>0,
						'item_unit_price'=>$customer_price[$schedule_id][$gf_item_number]->price,
						'discount_percent'=>0,
						'subtotal'=>$customer_price[$schedule_id][$gf_item_number]->price,
						'tax'=>to_currency_no_money($customer_price[$schedule_id][$gf_item_number]->price * ($cart_tax_info[0]['percent']/100)),
						'total'=>to_currency_no_money($customer_price[$schedule_id][$gf_item_number]->price * (1 + $cart_tax_info[0]['percent']/100)),
						'profit'=>$customer_price[$schedule_id][$gf_item_number]->price,
						'total_cost'=>0
					);
					$this->db->insert('sales_items',$sales_items_data);
					$sales_items_tax_data = array(
						'sale_id'=>$sale_id,
						'item_id'=>$item_id,
						'line'=>$line_number,
						'name'=>'Sales Tax',
						'percent'=>$cart_tax_info[0]['percent'],
						'cumulative'=>0
					);
					$this->db->insert('sales_items_taxes',$sales_items_tax_data);
					$line_number++;
					$paid_green_fees++;
					if ($key < 6)
						$paid_individual_players .= ", person_paid_$key = 1";
				}
	    	}
			
			$diff = $reservation_info['player_count'] - count($guest_info); 
			if ($diff > 0)
			{
				$price = $this->Api_data->get_tee_time_prices($schedule_id);
				for ($i = 1; $i <= $diff; $i++)
				{
					$paid_gf = ($reservation_info['paid_player_count'] - $paid_green_fees > 0);
					$paid_c = ($reservation_info['paid_carts'] - $paid_carts > 0);
					$guest_info['guest_'.(5+$i)] = array(
						'guest_id'=>'', 
						'first_name'=>'Guest',
						'last_name'=>'',
						'paid_green_fee'=>$paid_gf,
						'paid_cart'=>$paid_c,
						'green_fee'=>to_currency_no_money($price[$schedule_id][$course_id.'_'.$index_array['gf']]->price * (1 + $green_fee_tax_info[0]['percent']/100)), 
						'cart_fee'=>to_currency_no_money($price[$schedule_id][$course_id.'_'.$index_array['cart']]->price * (1 + $cart_tax_info[0]['percent']/100))
						);
					$paid_green_fees += $paid_gf ? 1 : 0;
					$paid_carts += $paid_c ? 1 : 0;
	    		}
			}		
		
			
			// SAVE PAYMENT CREDIT CARD FIRST
			$payment_obj = array(
				'sale_id'=>$sale_id,
				'payment_type'=>'',
				'payment_amount'=>'',
				'invoice_id'=>'',
				'tip_recipient'=>0
			);
			// MARK SPECIFIC PLAYERS AS PAID AND THE TEE TIME AS A WHOLE
			$this->Api_data->update_reservation();
			$this->db->query("UPDATE foreup_teetime SET 
				paid_player_count = paid_player_count + $paid_green_fees, 
				paid_carts = paid_carts + $paid_carts 
				$paid_individual_players
				WHERE TTID LIKE '$reservation_id%'
				LIMIT 2");
		}
			// ITEMS
			
			// CREDIT CARD PAYMENT
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			
		}
		else 
		{
	        echo json_encode(array('sale_id'=>$sale_id));
		}
    }
		
		
}
	