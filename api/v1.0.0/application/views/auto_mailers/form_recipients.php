<ul id="error_message_box"></ul>
<?php
echo form_open('auto_mailers/save_recipients/'.$auto_mailer_id,array('id'=>'auto_mailer_recipients_form'));
?>
<style>
	.delete_recipient {
		width:20px;
		height:20px;
		display:block;
		background:url(../images/pieces/x_red3.png) transparent;
		margin-right:5px;
		cursor:pointer;
		float:right;
	}
	#recipient_row_container .search_row {
		
	}
	#recipient_row_container .header {
		background:#ddd;
	}
	#recipient_row_container td {
		padding:5px;
	}
</style>
<fieldset id="auto_mailer_recipient_info">
<legend><?php echo $auto_mailer->name.' '.lang("auto_mailers_recipient_information"); ?></legend>
<table>
	<tbody id='recipient_row_container'>
		<tr class='search_row'>
			<td>
				<?php echo form_label(lang('common_search').':', 'name',array('class'=>' wide')); ?>
			</td>
			<td>
				<?php echo form_input(array(
					'name'=>'name',
					'id'=>'name',
					'value'=>$auto_mailer->name,
					'size'=>35)
				);?>
			</td>
		</tr>
		<tr class='header'>
			<td>
				<?php echo form_label(lang('auto_mailers_recipients').'', 'department',array('class'=>'wide')); ?>
			</td>
			<td>
				<?php echo form_label(lang('auto_mailers_recipient').'', 'department',array('class'=>'wide', 'style'=>'width:210px;')); ?>
			</td>
			<td>
				<?php echo form_label(lang('auto_mailers_start_date').'', 'department',array('class'=>'wide', 'style'=>'width:100px;')); ?>
			</td>
			<td>
				<?php echo form_label(lang('common_email').'', 'department',array('class'=>'wide', 'style'=>'width:90px;')); ?>
			</td>
		</tr>

<?php 
$c_count = 0;
foreach ($recipients as $recipient)
{
	$c_count++;
	?>
<?php echo form_label(/*lang('auto_mailers_marketing_campaigns').*/'', 'department',array('class'=>'wide')); ?>
		<tr id='recipient_row_<?=$c_count?>' class='recipient_row'>
			<td>
				<span class='delete_recipient' onclick='remove_recipient_row(<?=$c_count?>)'></span>
			</td>
			<td>
				<span style='width:210px;'>
					<?=$recipient['last_name'].', '.$recipient['first_name']?>
					<?php echo form_hidden('person_id_'.$c_count, $recipient['person_id']); ?>
				</span>
			</td>
			<td>
				<span style='width:150px;'>
					<?php echo date('Y-m-d', strtotime($recipient['start_date']))?>
				</span>
			</td>
			<td>
				<span style='width:90px;'>
					<?=$recipient['email']?>
				</span>
			</td>
		</tr>
<?php } 

$c_count++;
?>
	</tbody>
</table>
<div>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_save'),
		'class'=>'submit_button float_right')
	);
	?>
</div>
</fieldset>

<?php
echo form_close();
?>
<script type='text/javascript'>
console.log('really really before error');
</script>
<script type='text/javascript'>
console.log('really before error');
function add_additional_recipient_row(recipient) {
	var recipient_rows = $('.recipient_row');
	console.dir(recipient_rows);
	var rr_count = recipient_rows.length;
	var nr = rr_count + 1;
	console.log('rr_count '+rr_count);
	var html = '<tr id="recipient_row_'+nr+'" class="recipient_row">'+
					'<td style="width:20px;">'+
						"<span class='delete_recipient' onclick='remove_recipient_row("+nr+")'></span>"+
					'</td>'+
					'<td style="width:210px;">'+
						"<span style='width:210px;'>"+
						recipient.name+
						"<input type='hidden' id='person_id_"+nr+"' name='person_id_"+nr+"' value='"+recipient.person_id+"'/>"+
						"</span>"+
					'</td>'+
					'<td style="width:150px;">'+
						"<span style='width:150px;'>"+
						"<input type='text' id='start_date_"+nr+"' name='start_date_"+nr+"' value='"+recipient.start_date+"'/>"+
						"</span>"+
					'</td>'+
					'<td style="width:90px;">'+
						"<span style='width:90px;'>"+
						recipient.email+
						"</span>"+
					'</td>'+
				'</tr>';
		$('#recipient_row_container').append(html);
		$("#start_date_"+nr).datepicker({
			dateFormat:'yy-mm-dd'
		});
		$('#start_date_'+nr).datepicker('setDate', new Date());
		$('#name').val('');
		$.colorbox.resize();
}
function remove_campaign_row(row_number) {
	$('#campaign_id_'+row_number).val('');
	$('#campaign_row_'+row_number).hide();
	$.colorbox.resize();
}
//validation and submit handling
$(document).ready(function()
{
	$( "#name").autocomplete({
		source: "<?php echo site_url('customers/customer_search');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function( event, ui ) 
 		{
 			event.preventDefault();
 			console.log('selecting');
 			console.dir(ui.item);
			add_additional_recipient_row({
				'name':ui.item.label,
				'person_id':ui.item.value,
				'email':ui.item.email,
				'start_date':''
			});
		}
	});
	var submitting = false;
    $('#auto_mailer_recipients_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				$.colorbox.close();
				//post_item_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
		},
		messages:
		{
		}
	});
});
</script>