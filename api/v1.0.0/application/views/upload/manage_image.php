<?php
if(!empty($editing)){
	$class="editing";
}else{
	$class="preview";
}
?>
<style>
/* While previewing */
form.preview .submit.button {
	display: block;
}

form.preview #image_title_edit {
	display: none;
}

form.preview #image_title {
	display: block;
}

/* While editing */
form.editing .submit.button {
	display: none;
}

form.editing #image_title_edit {
	display: block;
}

form.editing #image_title {
	display: none;
}

#image_title > h2 {
	float: left;
	padding: 5px 5px 5px 0px;
	margin: 0px;
}

#image_title_edit label, #image_title_edit a {
	padding-top: 5px;
	display: inline-block;
}

#image-library div.bottom-bar {
	display: block;
	width: 752px;
	overflow: hidden;
	padding: 10px;
	position: absolute;
	border-top: 1px solid #DEDEDE;
	bottom: 0px;
	float: none;
	left: 0px;
	background-color: white;
}
#crop_image {
	border-radius:300px;
}
</style>
<script>
var jcrop_api = '';
function init_crop(){
	$('#crop_image').Jcrop({
		onSelect: function(c){
			$('#crop_x').val(c.x);
			$('#crop_y').val(c.y);
			$('#crop_w').val(c.w);
			$('#crop_h').val(c.h);
		},
		setSelect: [0, 0, <?php echo $width; ?>, <?php echo $height; ?>],
		trueSize: [<?php echo $width; ?>, <?php echo $height; ?>],
		<?php if(!empty($crop_ratio)){ ?>
		aspectRatio: <?php echo $crop_ratio; ?>
		<?php } ?>
	}, function(){
		jcrop_api = this;
	});
}

$(function(){
	$.colorbox2.resize();
	$('#edit_image_form').submit(function(e){
		e.preventDefault();
		var params = $(this).serialize();
		var url = $(this).attr('action');

		$.post(url, params, function(response){
			if(response.status == 'success'){
				$('#upload_file').show();
				selected_image = <?php echo $image_id; ?>;
				load_image(<?php echo $image_id; ?>);
				refresh_files();
			}else{
				set_feedback(response.msg, 'error_message', false, 3000);
			}
		},'json');
	});

	$('#enable_edit').click(function(e){
		$('#edit_image_form').removeClass('preview').addClass('editing');
		init_crop();
		return false;
	});

	$('a.delete').click(function(e){
		var url = $(this).attr('href');

		if(confirm('Are you sure you want to delete this image?')){
			$.get(url, null, function(response){
				if(response.status == 'success'){
					$('#edit_image').html('');
					$('#file_'+response.file_id).remove();
				}else{
					alert(response.msg);
				}
			},'json');
		}
		return false;
	});

	$('#cancel_image_edit').click(function(e){
		$('#edit_image_form').removeClass('editing').addClass('preview');
		jcrop_api.release();
		jcrop_api.disable();
		$('#upload_file').show();

		<?php if(!empty($uploading)){ ?>
		$('#edit_image').html('');
		<?php } ?>
		return false;
	});

	$('#use-image').click(function(e){
		var image_id = $(this).attr('data-image-id');

		// Fire a custom event, this event will be handled
		// by what module you are editing (people, items, campaigns)
		$.event.trigger({
			type: "changeImage",
			image_id: image_id
		});
		return false;
	});

	<?php if($editing){ ?>
	init_crop();
	<?php } ?>
});
</script>
<form class="<?php echo $class; ?>" name="edit_image" id="edit_image_form" method="post" action="<?php echo site_url('upload/save_image').'/'.$image_id; ?>">
	<input type='hidden' id='crop_x' name='x' value='0' />
	<input type='hidden' id='crop_y' name='y' value='0' />
	<input type='hidden' id='crop_w' name='w' value='0' />
	<input type='hidden' id='crop_h' name='h' value='0' />
	<div style="display: block; overflow: hidden; margin: 0px; padding: 0px; width: auto; text-align: center">
		<div id="image_container">
			<div id="image_title" class="field_row clearfix">
				<h2><?php echo $label; ?></h2>
				<a href="#" style="float: right; margin: 0px 0px 5px 0px" id="enable_edit" class="button">Edit</a>
				<a href="<?php echo site_url('upload/delete_file'); ?>/<?php echo $image_id; ?>" class="button red delete">Delete</a>
			</div>
			<div id="image_title_edit" class="field_row clearfix">
				<div class='form_field' style="float: left; margin: 0px 0px 5px 0px">
					<label for="image_label" style="float: left;">Label</label>
					<?php echo form_input(array('name'=>'label', 'id'=>'image_label', 'value'=>$label, 'class'=>'text', 'style'=>'width: 280px;'));?>
				</div>

				<input type="submit" style="float: right; margin-left: 10px;" name="submit" value="Save" class="button" />
				<a href="#" id="cancel_image_edit" style="float: right;">Cancel</a>
			</div>
			<img id="crop_image" src="<?php echo $preview_url.'/'.$filename; ?>?ts=<?php echo strtotime($date_updated); ?>" />
		</div>
	</div>
</form>
<div class="bottom-bar">
	<a id="use-image" href="#" style="float: right;" data-image-id="<?php echo $image_id; ?>" class="button">Use this Image</a>
</div>