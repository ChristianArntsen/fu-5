<style>
label {
	font-size: 14px;
	margin-bottom: 2px;
	display: block;
}

div.field_row {
	margin-top: 10px;
}
</style>

<form name="dashboard" id="dashboard-form" method="post" action="<?php echo site_url('dashboards/save'); ?>" style="padding: 15px; width: 428px;">
	<div class="field_row clearfix">
		<label for="dashboard-name">Name</label>
		<input name="name" value="" id="dashboard-name" style="width: 418px;" />
	</div>
	<div class="field_row clearfix">
		<label for="dashboard-desc">Description</label>
		<textarea name="description" id="dashboard-desc" style="width: 418px;"></textarea>
	</div>
	<!-- <div class="field_row clearfix">
		<label for="dashboard-template">Template</label>
		<?php echo form_dropdown('template', array('financials'=>'Financials', 'key-metrics'=>'Key Metrics', 'blank'=>'Blank'), null, 'id="dashboard-template"'); ?>
	</div>	-->
	<div class="field_row clearfix">
		<input type="submit" name="submit" value="Save" class="submit_button" />
	</div>
</form>

<script>
$('#dashboard-form').submit(function(e){
	var form = $(this);
	var url = form.attr('action');
	var data = form.serialize();
	form.mask("<?php echo lang('common_wait'); ?>");

	$.post(url, data, function(response){
		if(response.success){
			window.location = '<?php echo site_url('dashboards'); ?>/index/' + response.dashboard_id;
			form.unmask();
		}else{
			form.unmask();
			set_feedback(response.message,'error_message', false);
		}
	},'json');

	return false;
});
</script>