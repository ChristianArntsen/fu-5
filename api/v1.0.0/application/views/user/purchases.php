<table>
	<thead>
		<th>Purchase #</th>
		<th>Date Purchased</th>
		<th>Items</th>
		<th>Subtotal</th>
		<th>Tax</th>
		<th>Total</th>
	</thead>
	<tbody>
	<?php if(!empty($purchases['summary'])){ ?>
	<?php foreach($purchases['summary'] as $purchase){ ?>
		<tr>
			<td style="width: 90px;">#<?php echo $purchase['sale_id']; ?></td>
			<td style="width: 125px;"><?php echo date('M jS, Y', strtotime($purchase['sale_date'])); ?></td>
			<td><?php echo $purchase['items_purchased']; ?></td>
			<td style="width: 75px;"><?php echo money_format('$%i', $purchase['subtotal']); ?></td>
			<td style="width: 75px;"><?php echo money_format('$%i', $purchase['tax']); ?></td>
			<td style="width: 75px;"><strong><?php echo money_format('$%i', $purchase['total']); ?></strong></td>
		</tr>
	<?php } ?>
	<?php } else { ?>
		<tr>
			<td colspan="6"><h3 class="muted">No purchases found</h3></td>
		</tr>
	<?php } ?>
	</tbody>
</table>