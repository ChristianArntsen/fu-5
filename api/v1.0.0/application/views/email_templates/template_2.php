<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ruby E-mail Template - A Touch of Css3 and Magic perhaps.</title>

<!-- Hotmail ignores some valid styling, so we have to add this -->
<style type="text/css">
.ReadMsgBody
{width: 100%; background-color: #f5f9fa;}
.ExternalClass
{width: 100%; background-color: #f5f9fa;}
body
{width: 100%; background-color: #f5f9fa;}

/* If Mac OSX or iPhone, throw in these styles */
.package { position: relative; width: 0px; padding: 0px; margin: 0px; }
#switcher { position: absolute; left: 130px; }
#switcher a, #switcher a:link, #switcher a:visited { background: transparent url(images/switch_sprite.jpg) no-repeat -64px -2px; display: block; -webkit-transition: background-position .2s linear; -moz-transition: background-position .2s linear; -o-transition: background-position .2s linear; transition: background-position .2s linear; border-radius: 50px; height: 34px; }
#switcher a:hover, #switcher a:focus { background-position: -1px -2px; outline: none; }
#switcher img { width: 0px; height: 0px; }

</style>
</head>

<body bgcolor="#f5f9fa" background="http://www.premiumlab.net/demo/ruby/ruby/images/bg.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<script language="Javascript">
</script><!-- Main wrapper -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tbody><tr>
		<td>
		
			<!-- Container -->
			<table width="620" border="0" cellpadding="0" cellspacing="0" align="center">
				<tbody><tr>
					<td>
					
						<!-- Header -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
							<tbody><tr>
								<td height="50"></td>
							</tr>
							<tr>
							
								<!-- Header Image -->
								<td width="600" height="386" bgcolor="#FFFFFF" style="font-family: Helvetica, Arial, sans-serif; line-height: 1px; text-align: center; font-size: 18px; font-weight: bold; color: #cecece;">
								
									<a href="#"><img src="http://www.premiumlab.net/demo/ruby/ruby/images/header.jpg" style="display: block;" border="0" alt="iCloud has received."></a>
									
								</td>
							</tr>
						</tbody></table>
						
						<!-- Download Bar inclusive ON/OFF Switcher -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center" style="border-left: 1px solid #e8e8e8; border-right: 1px solid #e8e8e8;">
							<tbody><tr>
								<td width="34" height="65"></td>
								<td width="307" height="65" style="font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-weight: bold; color: #4a4a4a; line-height: 24px;">
								
									<!-- Message -->
									Get Auto Updates from iCloud.
								
								</td>
								<td width="130" height="65">
									
									<!-- The Css3 Switcher is actually near the original Switchers TD. When Apple Mac strikes in, we Position: Absolute it to the right. -->
									<aside class="package">
										<aside id="switcher" style="width: 96px; margin-top: -15px;"><a href="#"></a>
									
									
								</aside></aside></td>
									
								<td width="96" height="65">
								
									<!-- The static Switcher. Just a trigger image. -->
									<table width="96" border="0" cellpadding="0" cellspacing="0">
										<tbody><tr>
											<td width="96" height="17"></td>
										</tr>
										
										<!-- On/OFF Image -->
										<tr>
											<td width="96" height="34" style="font-family: Helvetica, Arial, sans-serif; text-align: center; font-size: 18px; font-weight: bold;">												
											
													<a href="#" style="color: #cecece; text-decoration: none;"><img src="http://www.premiumlab.net/demo/ruby/ruby/images/switcher.jpg" style="display: block;" border="0" alt="OFF">
												
											</a></td>
										</tr>
										<tr>
											<td width="98" height="14"></td>
										</tr>
									</tbody></table>
								
								</td>
								<td width="34" height="65"></td>
							</tr>
						</tbody></table>
						
						<!-- Divider -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
							<tbody><tr>
								<td height="18" bgcolor="#FFFFFF" style="line-height: 1px;"><img src="http://www.premiumlab.net/demo/ruby/ruby/images/divider.jpg" style="display: block;"></td>
							</tr>
						</tbody></table>
						
						<!-- Brands -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center" style="border-left: 1px solid #e8e8e8; border-right: 1px solid #e8e8e8; border-bottom: 1px solid #efefef;">
							<tbody><tr>
								<td width="33" height="30"></td>
								<td style="line-height: 1px; font-family: Helvetica, Arial, sans-serif; text-align: center; font-size: 18px; font-weight: bold;" width="151" height="60">
								
									<!-- Brand 1 -->
									<a href="#" style="color: #cecece; text-decoration: none;"><img src="http://www.premiumlab.net/demo/ruby/ruby/images/appstorm.jpg" width="151" height="23" border="0" style="display: block;" alt="App Storm"></a>
									
								</td>
								<td width="41" height="30"></td>
								<td style="line-height: 1px; font-family: Helvetica, Arial, sans-serif; text-align: center; font-size: 18px; font-weight: bold;">
								
									<!-- Brand 2 -->
									<a href="#" style="color: #cecece; text-decoration: none;"><img src="http://www.premiumlab.net/demo/ruby/ruby/images/lifehacker.jpg" width="151" height="23" border="0" style="display: block;" alt="Lifehacker"></a>
									
								</td>
								<td width="40" height="30"></td>
								<td style="line-height: 1px; font-family: Helvetica, Arial, sans-serif; text-align: center; font-size: 18px; font-weight: bold;">
								
									<!-- Brand 3 -->
									<a href="#" style="color: #cecece; text-decoration: none;"><img src="http://www.premiumlab.net/demo/ruby/ruby/images/msnbc.jpg" width="151" height="23" border="0" style="display: block;" alt="MSNBC"></a>
									
								</td>
								<td width="33" height="30"></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
						</tbody></table>
						
						<!-- 3 Col Images -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center" style="border-left: 1px solid #e8e8e8; border-right: 1px solid #e8e8e8; border-bottom: 1px solid #efefef;">
							<tbody><tr>
								<td width="33" height="164"></td>
								<td style="line-height: 1px; font-family: Helvetica, Arial, sans-serif; text-align: center; font-size: 18px; font-weight: bold;" width="151" height="164">
								
									<!-- Image 1 -->
									<a href="#" style="color: #cecece; text-decoration: none;"><img src="http://www.premiumlab.net/demo/ruby/ruby/images/col_image_1.jpg" width="151" height="107" border="0" style="display: block;" alt="Image 1"></a>
									
								</td>
								<td width="41" height="164"></td>
								<td style="line-height: 1px; font-family: Helvetica, Arial, sans-serif; text-align: center; font-size: 18px; font-weight: bold;">
								
									<!-- image 2 -->
									<a href="#" style="color: #cecece; text-decoration: none;"><img src="http://www.premiumlab.net/demo/ruby/ruby/images/col_image_2.jpg" width="151" height="107" border="0" style="display: block;" alt="Image 2"></a>
									
								</td>
								<td width="40" height="164"></td>
								<td style="line-height: 1px; font-family: Helvetica, Arial, sans-serif; text-align: center; font-size: 18px; font-weight: bold;">
								
									<!-- Image 3 -->
									<a href="#" style="color: #cecece; text-decoration: none;"><img src="http://www.premiumlab.net/demo/ruby/ruby/images/col_image_3.jpg" width="151" height="107" border="0" style="display: block;" alt="Image 3"></a>
									
								</td>
								<td width="33" height="164"></td>
							</tr>
						</tbody></table>
						
						<!-- Col Left-->
						<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center" style="border-left: 1px solid #e8e8e8; border-right: 1px solid #e8e8e8; border-bottom: 1px solid #efefef;">
							<tbody><tr>
								<td height="30"></td>
							</tr>
							<tr>
								<td width="33" height="119"></td>
								<td width="350" height="119" valign="top" style="font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-weight: bold; color: #4a4a4a; line-height: 24px;">
								
									<!-- headline -->
									Minimalistic Design.<br>
									
									<!-- Text -->
									<p style="font-size: 14px; font-weight: normal; color: #c6c6c6; line-height: 22px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis condimentum dui. Morbi nunc leo, sollicitudin at ornare at, fermentum id velit.</p>
									
								</td>
								<td width="33" height="119"></td>
								<td style="line-height: 1px;" valign="top">
								
									<!-- Col Image -->
									<img src="http://www.premiumlab.net/demo/ruby/ruby/images/col_image_1.jpg" width="151" height="107" border="0" style="display: block;" alt="">
									
								</td>
								<td width="33" height="119"></td>
							</tr>
							<tr>
								<td height="30"></td>
							</tr>
						</tbody></table>
						
						<!-- Col Right -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center" style="border-left: 1px solid #e8e8e8; border-right: 1px solid #e8e8e8; border-bottom: 1px solid #efefef;">
							<tbody><tr>
								<td height="30"></td>
							</tr>
							<tr>
								<td width="33" height="119"></td>
								<td style="line-height: 1px;" valign="top">
								
									<!-- Col Image -->
									<img src="http://www.premiumlab.net/demo/ruby/ruby/images/col_image_2.jpg" width="151" height="107" border="0" style="display: block;" alt="">
									
								</td>
								<td width="33" height="119"></td>
								<td width="350" height="119" valign="top" style="font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-weight: bold; color: #4a4a4a; line-height: 24px;">
								
									<!-- Headline -->
									Advanced Switch Button.<br>
									
									<!-- Text -->
									<p style="font-size: 14px; font-weight: normal; color: #c6c6c6; line-height: 22px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis condimentum dui. Morbi nunc leo, sollicitudin at ornare at, fermentum id velit.</p>
									
								</td>
								<td width="33" height="119"></td>
							</tr>
							<tr>
								<td height="30"></td>
							</tr>
						</tbody></table>
						
						<!-- Right -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center" style="border-left: 1px solid #e8e8e8; border-right: 1px solid #e8e8e8; border-bottom: 1px solid #efefef;">
							<tbody><tr>
								<td height="30"></td>
							</tr>
							<tr>
								<td width="33" height="119"></td>
								<td width="350" height="119" valign="top" style="font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-weight: bold; color: #4a4a4a; line-height: 24px;">
								
									<!-- headline -->
									Valid Code.<br>
									
									<!-- Text -->
									<p style="font-size: 14px; font-weight: normal; color: #c6c6c6; line-height: 22px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis condimentum dui. Morbi nunc leo, sollicitudin at ornare at, fermentum id velit.</p>
									
								</td>
								<td width="33" height="119"></td>
								<td style="line-height: 1px;" valign="top">
								
									<!-- Col image -->
									<img src="http://www.premiumlab.net/demo/ruby/ruby/images/col_image_3.jpg" width="151" height="107" border="0" style="display: block;" alt="">
									
								</td>
								<td width="33" height="119"></td>
							</tr>
							<tr>
								<td height="30"></td>
							</tr>
						</tbody></table>
						
						<!-- Left -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center" style="border-left: 1px solid #e8e8e8; border-right: 1px solid #e8e8e8; border-bottom: 1px solid #efefef;">
							<tbody><tr>
								<td height="30"></td>
							</tr>
							<tr>
								<td width="33" height="119"></td>
								<td style="line-height: 1px;" valign="top">
								
									<!-- Col image -->
									<img src="http://www.premiumlab.net/demo/ruby/ruby/images/col_image_1.jpg" width="151" height="107" border="0" style="display: block;" alt="">
									
								</td>
								<td width="33" height="119"></td>
								<td width="350" height="119" valign="top" style="font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-weight: bold; color: #4a4a4a; line-height: 24px;">
								
									<!-- headline -->
									PSD Files.<br>
									
									<!-- Text -->
									<p style="font-size: 14px; font-weight: normal; color: #c6c6c6; line-height: 22px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis condimentum dui. Morbi nunc leo, sollicitudin at ornare at, fermentum id velit.</p>
									
								</td>
								<td width="33" height="119"></td>
							</tr>
							<tr>
								<td height="30"></td>
							</tr>
						</tbody></table>
						
						<!-- Footer -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #e8e8e8; border-right: 1px solid #e8e8e8;">
							<tbody><tr>
								<td height="140" bgcolor="#FFFFFF">
									<table width="598" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
										<tbody><tr>
											<td width="30" height="61"></td>
											<td style="line-height: 1px;">
											
												<!-- Left part of the Invitation Bar -->
												<img src="http://www.premiumlab.net/demo/ruby/ruby/images/left.jpg" width="20" height="61" style="display: block;" alt=""></td>
											<td width="374" height="59" bgcolor="eeeeee" style="border-top: 1px solid #e3e3e3; border-bottom: 1px solid #e3e3e3; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; color: #303030; line-height: 22px; text-shadow: 1px 1px 1px #FFFFFF;">You're invited to download our beta.</td>
											<td width="23" height="59" bgcolor="eeeeee" style="border-top: 1px solid #e3e3e3; border-bottom: 1px solid #e3e3e3;"></td>
											<td style="line-height: 1px; font-family: Helvetica, Arial, sans-serif; text-align: center; font-size: 18px; font-weight: bold;" bgcolor="#eeeeee">
												<a href="#" style="color: #cecece; text-decoration: none;">
													
													<!-- Download -->
													<img src="http://www.premiumlab.net/demo/ruby/ruby/images/download_btn.jpg" width="101" height="34" border="0" style="display: block;" alt="Download"></a>
													
												</td>
											<td style="line-height: 1px;">
											
												<!-- Right part of the Invitation Bar -->
												<img src="http://www.premiumlab.net/demo/ruby/ruby/images/right.jpg" width="20" height="61" style="display: block;" alt=""></td>
											<td width="30" height="61"></td>
										</tr>
									</tbody></table>
								</td>
							</tr>
						</tbody></table>
						
						<!-- Footer -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
							<tbody><tr>
								<td>
									
									<!-- Shadow Image -->
									<img src="http://www.premiumlab.net/demo/ruby/ruby/images/footer_shadow.jpg"></td>
							</tr>
						</tbody></table>
						
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
							<tbody><tr>
								<td height="50" style="font-family: Helvetica, Arial, sans-serif; font-size: 11px; font-weight: normal; color: #a0a0a0; line-height: 18px; text-align: center; text-shadow: 1px 1px 1px #FFFFFF;">
									You are receiving this because you signed up online.
									<a href="#" style="text-decoration: none; color: #808080; font-weight: bold;">Unsubscribe</a> if you do not wish to hear from us.<br> But we believe you think we are cool enough, aren't we?
								</td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
						</tbody></table>
						
					</td>
				</tr>
			</tbody></table>
			
		</td>
	</tr>
</tbody></table>


<!-- And we're done. Thank you for purchasing the iCloud E-mail Template. Feel free to contact us if you experience any problems or just to say hello at support@premiumstuff.net -->




</body></html>