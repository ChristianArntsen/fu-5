<ul id="error_message_box"></ul>
<?php
$cc_header = ($this->config->item('customer_credit_nickname') == '') ? lang('customers_account_balance') : $this->config->item('customer_credit_nickname');
echo form_close();
echo form_open('customers/save_account_transaction/'.$customer_info->person_id,array('id'=>'account_transaction_form'));
?>

<table align="center" border="0">
<div class="field_row clearfix">
<tr>
<td>
	<div class='item_name'>
		<?=$customer_info->last_name.', '.$customer_info->first_name?>
	</div>
</td>
<td>
	<div class='current_quantity'>
		<?php echo $cc_header.': $'.$customer_info->account_balance ?>
	</div>
</td>
</tr>
<tr>
	<td style='width:430px;'>
		<div class='item_number'>
			<?php echo lang('customers_account_number').': '.$customer_info->account_number?>
		</div>
		<div class='account_history' style='padding-top:10px;'>
			<a href="index.php/customers/account_details/<?=$customer_info->person_id?>/width~700" title='Account History' class='colbox'>Show Account History</a>
			<script>
				$('.colbox').colorbox();
			</script>
		</div>
	</td>
	<td>
		<div class='quantity_controls'>
			<span class='pos_neg_buttonset'>
	            <input type='radio' id='negative' name='quantity_sign' value='-1' />
	            <label for='negative' id='quantity_negative_label'>-</label>
	            <input type='radio' id='positive' name='quantity_sign' value='1'  checked/>
	            <label for='positive' id='quantity_positive_label'>+</label>
	        </span>
	 		<?php echo form_input(array(
				'name'=>'add_subtract',
				'id'=>'add_subtract')
			);?>
		</div>
	</td>
</tr>
</table>
<div class='popup_divider'></div>
	<?php echo form_input(array(
		'name'=>'trans_comment',
		'id'=>'trans_comment',
		'placeholder'=>'Reason / Comment')		
	);?>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_save'),
		'class'=>'submit_button float_right')
	);
	?>
<div class='clear'></div>
<?php 
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{		
	$('.pos_neg_buttonset').buttonset();
	$('#negative').button();
	$('#positive').button();
	var submitting = false;
    $('#account_transaction_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				$.colorbox.close();
				post_person_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			add_subtract:
			{
				required:true,
				number:true
			}
   		},
		messages: 
		{
			
			add_subtract:
			{
				required:"<?php echo lang('customers_add_subtract_required'); ?>",
				number:"<?php echo lang('customers_add_subtract_number'); ?>"
			}
		}
	});
	
	//handle making the number negative
	
	$('#add_subtract').keyup(function(event){				
		if (event.keyCode === 189 && $('#quantity_positive_label').hasClass('ui-state-active'))
		{			
			switch_sign();
		}
		
		if ($('#add_subtract').val()==='' && $('#quantity_negative_label').hasClass('ui-state-active'))
		{
			switch_sign();
		}
		
		if ($('#add_subtract').val() >= 0 && $('#quantity_negative_label').hasClass('ui-state-active'))
		{
			switch_sign();
		}
	});
	
	function switch_sign()
	{		
		if ($('#quantity_negative_label').hasClass('ui-state-active')){
			$('#quantity_negative_label').removeClass('ui-state-active');			
			$('#quantity_positive_label').addClass('ui-state-active');
			$('#add_subtract').removeClass('negative_balance');
		}
		else if ($('#quantity_positive_label').hasClass('ui-state-active')){
			$('#quantity_positive_label').removeClass('ui-state-active');
			$('#quantity_negative_label').addClass('ui-state-active');
			$('#add_subtract').addClass('negative_balance');
		}
	}
	
	$('.ui-button', '.pos_neg_buttonset').click(function(){
		if($('#quantity_negative_label').hasClass('ui-state-active'))
		{
			$('#add_subtract').addClass('negative_balance');
			
			if ($('#add_subtract').val().indexOf("-") === -1 )
			{				
				$('#add_subtract').val('-' + $('#add_subtract').val());
			}
		}
		else
		{
			$('#add_subtract').removeClass('negative_balance');
			$('#add_subtract').val($('#add_subtract').val().replace('-','')); 
		}
		$('#add_subtract').focus();		
	});
});
</script>