<div class="field_row clearfix">	
            <?php echo form_label(lang('config_company').':<span class="required">*</span>', 'company',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'name',
                            'id'=>'name',
                            'placeholder'=>lang('config_company'),
                            'value'=>$this->config->item('name')));?>
                    </div>
            </div>

            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_address').':<span class="required">*</span>', 'address',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'address',
                            'id'=>'address',
                            'placeholder'=>lang('config_address'),
                           // 'rows'=>2,
                           // 'cols'=>30,
                            'value'=>$this->config->item('address')));?>
                    </div>
            </div>
            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_address2').':<span class="required">*</span>', 'address',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php 
                        $state_list = array(''=>'State',
                        	'AL'=>"AL",
                            'AK'=>"AK", 
                            'AZ'=>"AZ", 
                            'AR'=>"AR", 
                            'CA'=>"CA", 
                            'CO'=>"CO", 
                            'CT'=>"CT", 
                            'DE'=>"DE", 
                            'DC'=>"DC", 
                            'FL'=>"FL", 
                            'GA'=>"GA", 
                            'HI'=>"HI", 
                            'ID'=>"ID", 
                            'IL'=>"IL", 
                            'IN'=>"IN", 
                            'IA'=>"IA", 
                            'KS'=>"KS", 
                            'KY'=>"KY", 
                            'LA'=>"LA", 
                            'ME'=>"ME", 
                            'MD'=>"MD", 
                            'MA'=>"MA", 
                            'MI'=>"MI", 
                            'MN'=>"MN", 
                            'MS'=>"MS", 
                            'MO'=>"MO", 
                            'MT'=>"MT",
                            'NE'=>"NE",
                            'NV'=>"NV",
                            'NH'=>"NH",
                            'NJ'=>"NJ",
                            'NM'=>"NM",
                            'NY'=>"NY",
                            'NC'=>"NC",
                            'ND'=>"ND",
                            'OH'=>"OH", 
                            'OK'=>"OK", 
                            'OR'=>"OR", 
                            'PA'=>"PA", 
                            'RI'=>"RI", 
                            'SC'=>"SC", 
                            'SD'=>"SD",
                            'TN'=>"TN", 
                            'TX'=>"TX", 
                            'UT'=>"UT", 
                            'VT'=>"VT", 
                            'VA'=>"VA", 
                            'WA'=>"WA", 
                            'WV'=>"WV", 
                            'WI'=>"WI", 
                            'WY'=>"WY");
                        echo form_input(array(
                            'name'=>'city',
                            'id'=>'city',
                            'placeholder'=>'City',
                            'value'=>$this->config->item('city')));
                          echo form_dropdown('state', $state_list,$this->config->item('state'));
                          echo form_input(array(
                            'name'=>'zip',
                            'id'=>'zip',
                            'placeholder'=>'Zip',
                            'size'=>'6',
                            'value'=>$this->config->item('zip')));

                    ?>
                    </div>
            </div>

            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_phone').':<span class="required">*</span>', 'phone',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'phone',
                            'id'=>'phone',
                            'placeholder'=>lang('config_phone'),
                            'value'=>$this->config->item('phone')));
                    ?>
                    </div>
            </div>
            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_fax').':', 'fax',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'fax',
                            'id'=>'fax',
                            'placeholder'=>lang('config_fax'),
                            'value'=>$this->config->item('fax')));
                    ?>
                    </div>
            </div>
            <div class="field_row clearfix">	
            <?php echo form_label(lang('common_email').':<span class="required">*</span>', 'email',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'email',
                            'id'=>'email',
                            'placeholder'=>lang('common_email'),
                            'value'=>$this->config->item('email')));
                    ?>
                    </div>
            </div>
            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_website').':', 'website',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'website',
                            'id'=>'website',
                            'placeholder'=>lang('config_website'),
                            'value'=>$this->config->item('website')));
                    ?>
                    </div>
            </div>
            <div class="field_row clearfix">	
                    <?php echo form_label(lang('config_company_logo').':', 'company_logo',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_upload(array(
                            'name'=>'company_logo',
                            'id'=>'company_logo',
                            'value'=>$this->config->item('company_logo')));?>		
                    </div>	
            </div>

            <div class="field_row clearfix">	
                    <?php echo form_label(lang('config_delete_logo').':', 'delete_logo',array('class'=>'wide')); ?>
                    <div class='form_field'>
                            <?php echo form_checkbox('delete_logo', '1');?>
                    </div>	
            </div>
            




<!--form action="http://foreupsoftware.com/index.php/config/save/details" method="post" accept-charset="utf-8" id="config_details_form">
	<div class="field_row clearfix">	
		<label for="company" class="wide required">Course Name:</label>                    
		<div class="form_field">
			<input type="text" name="name" value="St. Albert Trail Golf Course" id="name">                    
		</div>
	</div>
	<div class="field_row clearfix">	
		<label for="address" class="wide required">Course Address:</label>                    
		<div class="form_field">
			<input type="text" name="address" value="14110 156th St" id="address">                    
		</div>
	</div>
	<div class="field_row clearfix">	
		<label for="address" class="wide required">City, State, ZIP:</label>                    
		<div class="form_field">
			<input type="text" name="city" value="Edmonton" id="city">
			<select id="state" name="state">
				<option value="AL" selected="selected">AL</option>
				<option value="AK">AK</option>
				<option value="AZ">AZ</option>
				<option value="AR">AR</option>
				<option value="CA">CA</option>
				<option value="CO">CO</option>
				<option value="CT">CT</option>
				<option value="DE">DE</option>
				<option value="DC">DC</option>
				<option value="FL">FL</option>
				<option value="GA">GA</option>
				<option value="HI">HI</option>
				<option value="ID">ID</option>
				<option value="IL">IL</option>
				<option value="IN">IN</option>
				<option value="IA">IA</option>
				<option value="KS">KS</option>
				<option value="KY">KY</option>
				<option value="LA">LA</option>
				<option value="ME">ME</option>
				<option value="MD">MD</option>
				<option value="MA">MA</option>
				<option value="MI">MI</option>
				<option value="MN">MN</option>
				<option value="MS">MS</option>
				<option value="MO">MO</option>
				<option value="MT">MT</option>
				<option value="NE">NE</option>
				<option value="NV">NV</option>
				<option value="NH">NH</option>
				<option value="NJ">NJ</option>
				<option value="NM">NM</option>
				<option value="NY">NY</option>
				<option value="NC">NC</option>
				<option value="ND">ND</option>
				<option value="OH">OH</option>
				<option value="OK">OK</option>
				<option value="OR">OR</option>
				<option value="PA">PA</option>
				<option value="RI">RI</option>
				<option value="SC">SC</option>
				<option value="SD">SD</option>
				<option value="TN">TN</option>
				<option value="TX">TX</option>
				<option value="UT">UT</option>
				<option value="VT">VT</option>
				<option value="VA">VA</option>
				<option value="WA">WA</option>
				<option value="WV">WV</option>
				<option value="WI">WI</option>
				<option value="WY">WY</option>
			</select>
			<input type="text" name="zip" value="84604" id="zip" size="6">                    
		</div>
	</div>
	<div class="field_row clearfix">	
		<label for="phone" class="wide required">Course Phone:</label>                    
		<div class="form_field">
			<input type="text" name="phone" value="(780) 447-2934" id="phone">                    
		</div>
	</div>
	<div class="field_row clearfix">	
		<label for="fax" class="wide">Fax:</label>                    
		<div class="form_field">
			<input type="text" name="fax" value="" id="fax">                    
		</div>
	</div>
	<div class="field_row clearfix">	
		<label for="email" class="wide">E-Mail:</label>                    
		<div class="form_field">
			<input type="text" name="email" value="Contact@foreUP.com" id="email">                    
		</div>
	</div>
	<div class="field_row clearfix">	
		<label for="website" class="wide">Website:</label>                    
		<div class="form_field">
			<input type="text" name="website" value="www.foreUP.com" id="website">                    
		</div>
	</div>
	<div class="field_row clearfix">	
		<label for="company_logo" class="wide">Course Logo:</label>                    
		<div class="form_field">
			<input type="file" name="company_logo" value="" id="company_logo">	
		</div>	
	</div>
	<div class="field_row clearfix">	
		<label for="delete_logo" class="wide">Delete Logo:</label>                    
		<div class="form_field">
			<input type="checkbox" name="delete_logo" value="1">                    
		</div>	
	</div>
   	<div class="centered_submit">
		<input type="submit" name="submit_details" value="Submit" id="submit_details" class="submit_button float_right">        	
	</div>
</form-->