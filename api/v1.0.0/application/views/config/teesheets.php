<table id="contents" style='width:855px;'>
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php echo 
					anchor("teesheets/view_teesheet/false/550",
					lang('teesheets_new'),
					array('class'=>'colbox none new', 
						'title'=>lang('teesheets_new')));
				?>
					
				<?php echo 
					anchor("teesheets/delete_teesheets",
					lang("common_delete"),
					array('id'=>'delete', 
						'class'=>'delete_inactive')); 
				?>			
			</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder" style='width:675px;'>
				<?php echo $teesheet_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>