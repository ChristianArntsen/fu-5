<?php
echo form_open('config/save/sales',array('id'=>'sales_settings_form'));
//print_r($price_classes);
?>
<ul id="error_message_box"></ul>
<fieldset id="sales_settings_info">
<!-- <legend><?php echo lang("config_sales_settings"); ?></legend> -->
       <div class="field_row clearfix">        
    <?php echo form_label(lang('config_print_after_sale').':', 'print_after_sale',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'print_after_sale',
                    'id'=>'print_after_sale',
                    'value'=>'1',
                    'checked'=>$this->config->item('print_after_sale')));?>
           </div>
    </div>
    <div class="field_row clearfix">        
    <?php echo form_label(lang('config_print_two_receipts').':', 'print_two_receipts',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'print_two_receipts',
                    'id'=>'print_two_receipts',
                    'value'=>'1',
                    'checked'=>$this->config->item('print_two_receipts')));?>
           </div>
    </div>
    <div class="field_row clearfix">        
    <?php echo form_label(lang('config_print_two_receipts_other').':', 'print_two_receipts_other',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'print_two_receipts_other',
                    'id'=>'print_two_receipts_other',
                    'value'=>'1',
                    'checked'=>$this->config->item('print_two_receipts_other')));?>
           </div>
    </div>
    <div class="field_row clearfix">   
    <?php echo form_label(lang('config_after_sale_load').':', 'after_sale_load',array('class'=>'wide')); ?>
            <div class='form_field radio_button_holder'>
            <?php echo form_radio(array(
                    'name'=>'after_sale_load',
                    'id'=>'after_sale_load_0',
                    'value'=>'0',
                    'checked'=>$this->config->item('after_sale_load')==0));?>
            <?php echo ($this->permissions->course_has_module('reservations') ? 'Reservations' : 'Tee Sheet');  ?>
            <?php echo form_radio(array(
                    'name'=>'after_sale_load',
                    'id'=>'after_sale_load_1',
                    'value'=>'1',
                    'checked'=>$this->config->item('after_sale_load')==1));?>
            Sales
            </div>
    </div>
    <div class="field_row clearfix">   
        <?php echo form_label(lang('config_track_cash').':', 'track_cash',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'track_cash',
                    'id'=>'track_cash',
                    'value'=>'1',
                    'checked'=>$this->config->item('track_cash')));?>
            </div>
	</div>
    <div class="field_row clearfix">	
    <?php echo form_label(lang('common_return_policy').':', 'return_policy',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_textarea(array(
                    'name'=>'return_policy',
                    'id'=>'return_policy',
                    'rows'=>'4',
                    'cols'=>'30',
                    'value'=>$this->config->item('return_policy')));?>
            </div>
    </div>    
<?php
echo form_submit(array(
       'name'=>'submit',
       'id'=>'submit',
       'value'=>lang('common_save'),
       'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
       var submitting = false;
    $('#sales_settings_form').validate({
               submitHandler:function(form)
               {
                       if (submitting) return;
                       submitting = true;
                       $(form).mask("<?php echo lang('common_wait'); ?>");
                       $(form).ajaxSubmit({
                       success:function(response)
                       {
                               $.colorbox.close();
                               //post_person_form_submit(response);
                               location.reload(true);
                submitting = false;
                       },
                       dataType:'json'
               });

               },
               errorLabelContainer: "#error_message_box",
               wrapper: "li",
              rules: 
               {
               },
               messages: 
               {
       }
       });
});
</script>