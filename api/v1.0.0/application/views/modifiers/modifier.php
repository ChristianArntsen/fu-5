<tr data-modifier-id="<?php echo $modifier['modifier_id']; ?>">
	<td>
		<div class='form_field name'>
			<span class="data" data-name="name"><?php echo $modifier['name']; ?></span>
		</div>
	</td>
	<td>
		<div class='form_field options'>
			<span class="data" data-name="options"><?php if(!empty($modifier['options'])){ ?><?php echo implode(',', $modifier['options']); ?><?php } ?></span>
		</div>
	</td>				
	<td>
		<div class='form_field price'>
			<span class="data" data-name="default_price"><?php echo $modifier['default_price']; ?></span>
		</div>
	</td>
	<td>
		<a href="#" class="edit_modifier">Edit</a>
	</td>			
	<td>
		<a href="#" class="delete_modifier" style="color: red;">X</a>
	</td>
</tr>