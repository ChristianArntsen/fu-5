<style type="text/css">
  /* css overrides*/
  .label-override {
      width: 103px !important;
  }
  .label-override-long {
      width: 200px !important;
  }

  .label-override-dual {
      width: 52px !important;
      margin-left: 15px;
  }
  .radio-override{
    float:right;
    text-align: left;
    margin-right: 45px;
    margin-top: -5px;
  }
  ul.promotion-amount-type{
    list-style: none;    
  }
  ul.promotion-amount-type li{
    float:left;
    color:red;
    margin-left: 20px;
    margin-top: 4px;
  }
  #promotion-form-left{width:385px;float:left;}
  #promotion-form-right{width:555px;float:right;}
  .wysiwyg-title-iframe{height: 52px !important;}
  .wysiwyg-contents-iframe{min-height: 150px !important;height: 150px !important;}  
  #promotion_content{min-height:150px !important;}
  #text-counter{font-size:11px; color:#67696b;}
  .label-override-content {
      width: 290px !important;
  }

  #coupon-wrapper{
    width: 500px;
    height: auto;
    margin-top: 150px;
    min-height: 200px;
    border: 4px dashed #000;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    background: url(<?= $this->Appconfig->get_logo_image(); ?>) no-repeat top left;
    text-align:center;
  }
  
  td.spacer{width:20px;}
  td.expires{font-size:12px;}
  td.promotion-text h1{font-size: 44px;line-height: 33px;}
  td.promotion-text p{font-size: 12px;}
  td.promotion-footer{font-size: 11px;text-align:left;padding-left:10px;padding-bottom:10px;}
</style>
<?= form_open('promotions/save/'.$promotion_info->id,array('id'=>'promotion_form')); ?>

<fieldset id="promotion_basic_info">
<legend><?= lang("promotions_basic_information"); ?></legend>
<div id="promotion-form-left">  
<div class="field_row clearfix">
<?= form_label(lang('promotions_name').':<span class="required">*</span>', 'name',array('class'=>' wide label-override')); ?>
	<div class='form_field'>
	<?= form_input(array(
		'name'=>'promotion_name',
		'size'=>'25',
    'style' => 'width: 239px;',
		'id'=>'promotion_name',
		'value'=> $promotion_info->name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?= form_label(lang('promotions_amount').':<span class="required">*</span>', 'name',array('class'=>' wide label-override')); ?>
	<div class='form_field'>
	<?= form_input(array(
		'name'=>'promotion_amount',
		'size'=>'25',
    'style' => 'width: 115px;float:left;margin-right: 10px;',
		'id'=>'promotion_amount',
		'value'=> $promotion_info->amount)
	);?>
  <ul class="promotion-amount-type">
    <li>$
      <?php $data = array(
            'name'        => 'promotion_amount_type',
            'id'          => 'promotion_amount_type_dollar',
            'value'       => '$',
            'checked'     => FALSE
            );

        echo form_radio($data);
      ?>            
    </li>
    <li>%
      <?php $data = array(
            'name'        => 'promotion_amount_type',
            'id'          => 'promotion_amount_type_pct',
            'value'       => '%',
            'checked'     => TRUE
            );

        echo form_radio($data);
      ?>
    </li>
  </ul>
	</div>
</div>
  
<div class="field_row clearfix">
 <?= form_label(lang('promotions_expiration_date').':<span class="required">*</span>', 'name',array('class'=>' wide label-override')); ?>
	<div class='form_field'>
	<?= form_input(array(
		'name'=>'promotion_expiration_date',
		'size'=>'25',
    'style' => 'width: 239px;',
		'id'=>'promotion_expiration_date',
		'value'=> $promotion_info->expiration_date)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?= form_label(lang('promotions_valid_for').':<span class="required">*</span>', 'name',array('class'=>' wide label-override')); ?>
	<div class='form_field'>
	<?= form_input(array(
		'name'=>'promotion_valid_for',
		'size'=>'25',
    'style' => 'width: 239px;',
		'id'=>'promotion_valid_for',
		'value'=> $promotion_info->valid_for)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?= form_label(lang('promotions_valid_on').':', 'name',array('class'=>'wide label-override')); ?>
	<div class='form_field'>
		<?= form_dropdown('promotions_valid_on_start', $days, $promotion_info->valid_day_start, 'id="promotions_valid_on_start"  style="width:123px;"');?>
		<?= form_dropdown('promotions_valid_on_end', $days, $promotion_info->valid_day_end, 'id="promotions_valid_on_end"  style="width:123px;"');?>
	</div>
</div>

<div class="field_row clearfix">
<?= form_label(lang('promotions_valid_between').':', 'name',array('class'=>'wide label-override')); ?>
	<div class='form_field'>
		<?= form_dropdown('promotions_valid_between_start', $range, $promotion_info->valid_between_from, 'id="promotions_valid_between_start"  style="width:123px;"');?>
		<?= form_dropdown('promotions_valid_between_end', $range, $promotion_info->valid_between_to, 'id="promotions_valid_between_end"  style="width:123px;"');?>
	</div>
</div>

<div class="field_row clearfix">
<?= form_label(lang('promotions_limit').':', 'name',array('class'=>'wide label-override')); ?>
	<div class='form_field'>
	<?= form_input(array(
		'name'=>'promotion_limit',
		'size'=>'25',
    'style' => 'width: 239px;',
		'id'=>'promotion_limit',
		'value'=> $promotion_info->limit
          )
	);?>
	</div>
</div>

  <div class="field_row clearfix">
    <?= form_label(lang('promotions_buy').':', 'name',array('class'=>'wide label-override')); ?>
    <div class='form_field' style="float:left;">
    <?= form_input(array(
      'name'=>'promotion_buy',
      'size'=>'25',
      'style' => 'width: 75px;',
      'id'=>'promotion_buy',
      'value'=> $promotion_info->buy_quantity)
    );?>
    </div>
    <?= form_label(lang('promotions_get').':', 'name',array('class'=>'wide label-override-dual')); ?>
    <div class='form_field'>
    <?= form_input(array(
      'name'=>'promotion_get',
      'size'=>'25',
      'style' => 'width: 75px;',
      'id'=>'promotion_get',
      'value'=> $promotion_info->get_quantity)
    );?>
    </div>
  </div>

  <div class="field_row clearfix">
  <?= form_label(lang('promotions_min_purchase').':', 'name',array('class'=>'wide label-override-long')); ?>
    <div class='form_field'>
    <?= form_input(array(
      'name'=>'promotion_min_purchase',
      'size'=>'25',
      'style' => 'width:142px;',
      'id'=>'promotion_min_purchase',
      'value'=> $promotion_info->min_purchase)
    );?>
    </div>
  </div>

  <div class="field_row clearfix">
  <?php echo form_label(lang('promotions_details').':', 'name',array('class'=>'wide label-override-long')); ?>
    <div class='form_field'>
    <?php echo form_textarea(array(
      'name'=>'promotion_additional_details',
      'size'=>'25',
      'rows'  => 2,
      'cols'  => 25,
      'style' => 'width: 352px;',
      'id'=>'promotion_additional_details',
      'value'=>$promotion_info->additional_details)
    );?>
    </div>
  </div>

<?= form_submit(array(
	 'name'=>'submitButton',
  'id'=>'submitButton',
	'value'=> lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</div>
<div id="promotion-form-right">
  <div id="coupon-wrapper">
    <table border="0">
      <tr style="height:20px;">
        <td colspan="4">&nbsp;</td>
      </tr>
      <tr>
        <td class="spacer">&nbsp;</td>
        <td>&nbsp;</td>
        <td class="spacer">&nbsp;</td>
        <td><h4><?= $course ?></h4></td>
      </tr>
      <tr style="height:20px;">
        <td colspan="4">&nbsp;</td>
      </tr>
      <tr>
        <td class="spacer">&nbsp;</td>
        <td class="expires">Expires: <span id="expiration-date"></span></td>
        <td class="spacer">&nbsp;</td>
        <td class="promotion-buy_and_get"><h3>&nbsp;</h3></td>
      </tr>
      <tr>
        <td class="spacer">&nbsp;</td>
        <td class="expires"><?= "<img src='".site_url('barcode')."?barcode=00001445770&text=C-01293-86723-15534&scale=2' />" ?></td>
        <td class="spacer">&nbsp;</td>
        <td class="promotion-text"><h1>50% off</h1><p>of anything in the Pro Shop</p></td>
      </tr>
      <tr style="height:40px;">
        <td colspan="4">&nbsp;</td>
      </tr>
      <tr style="height:20px;">
        <td class="promotion-footer" colspan="4">
          <span>&nbsp;</span>
        </td>
      </tr>
    </table>
  </div>
</div>
<div class="clearfix"></div>
</fieldset>
<?= form_close(); ?>

<script type="text/javascript">
$(document).ready(function(){

  $('#promotion_expiration_date').datepicker({dateFormat:'yy-mm-dd', minDate: new Date()});
  $('#promotion_expiration_date').change(function(){
    $('#expiration-date').html($(this).val());
  });
  function updateH(){
    var sym = $("input[name=promotion_amount_type]:checked").val();
    var t    = (sym == '$') ? sym + $('#promotion_amount').val() : $('#promotion_amount').val() + sym;
    var text = t + ' off';
    if(t == '100%') text = 'FREE';
    $('.promotion-text h1').html(text);
  }
  $('#promotion_amount').keyup(updateH);
  $('#promotion_amount').blur(updateH);
  $("input[name=promotion_amount_type]").change(updateH);

  function pvf(){
    $('.promotion-text p').html('of anything in the ' + $(this).val());
  }
  $('#promotion_valid_for').keyup(pvf);
  $('#promotion_valid_for').blur(pvf);

  function update_buy_n_get()
  {
    var b = $('#promotion_buy').val();
    var g = $('#promotion_get').val();
    var t = '';

    if(b.length > 0) t = t + 'Buy ' + b;
    if(g.length > 0) t = t + ' Get ' + g;

    $('.promotion-buy_and_get h3').html(t);
  }

  $('#promotion_buy').keyup(update_buy_n_get);
  $('#promotion_get').keyup(update_buy_n_get);
  $('#promotion_buy').blur(update_buy_n_get);
  $('#promotion_get').blur(update_buy_n_get);

  function ucwords (str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}


  function update_coupon_footer()
  {
    var m = $('#promotion_min_purchase').val();    
    if(m=='') m = '*';
    var vos = $('select[name=promotions_valid_on_start] option:selected').val();
    var voe = $('select[name=promotions_valid_on_end] option:selected').val();
    var vbs = $('select[name=promotions_valid_between_start] option:selected').val();
    if(vbs=='anytime') vbs='close';
    var vbe = $('select[name=promotions_valid_between_end] option:selected').val();
    if(vbe=='anytime') vbe='close';
    var lim   = $('#promotion_limit').val();
    var ad  = $('#promotion_additional_details').val();
    var text = '';
    if(lim.length > 0) text = text + m + ' Limit ' + lim + '. ';
    if(vos.length > 0 && voe.length > 0) text = 'Valid '+ ucwords(vos) + ' thru ' + ucwords(voe);
    if(vbs.length > 0 && vbe.length > 0) text = text + ' ' + vbs + ' and ' + vbe + '. ';
    if(ad.length > 0) text = text + ad;
//     = m + ' Limit ' + l + '. Valid ' + ucwords(vos) + ' thru ' + ucwords(voe) + ' between ';
//    text = text + vbs + ' and ' + vbe + '. ' + ad;
    text.trim();
    $('.promotion-footer span').html(text);
  }

  
  $('#promotions_valid_on_start').change(update_coupon_footer);
  $('#promotions_valid_on_end').change(update_coupon_footer);
  $('#promotions_valid_between_start').change(update_coupon_footer);
  $('#promotions_valid_between_end').change(update_coupon_footer);
  $('#promotion_limit').keyup(update_coupon_footer);
  $('#promotion_min_purchase').keyup(update_coupon_footer);
  $('#promotion_additional_details').keyup(update_coupon_footer);


  /***********************************************************************
     * repopulate coupon image template on the right during update
     ***********************************************************************/
    var promotion_id = '<?= $promotion_info->id; ?>';

    if(promotion_id)
    {
      $('#promotion_expiration_date').trigger('change');
      $('#promotion_amount').trigger('keyup');
      $("input[name=promotion_amount_type]").trigger('change');
      $('#promotion_valid_for').trigger('keyup');
      
      $('#promotion_buy').trigger('keyup');
      $('#promotion_get').trigger('keyup');
      $('#promotions_valid_on_start').trigger('change');
      $('#promotions_valid_on_end').trigger('change');
      $('#promotions_valid_between_start').trigger('change');
      $('#promotions_valid_between_end').trigger('change');
      $('#promotion_limit').trigger('keyup');
      $('#promotion_min_purchase').trigger('keyup');
      $('#promotion_additional_details').trigger('keyup');

      $.colorbox.resize();
    }
  /***********************************************************************
     * eo populating
     ***********************************************************************/

  /***********************************************************************
     * submit handler
     ***********************************************************************/

    var submitting = false;
    $('#promotion_form').validate({
		submitHandler:function(form)
		{
      if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
        success:function(response)
        {
          tb_remove();
          $.colorbox.close();
          post_promotion_submit(response);
                  submitting = false;
        },
        dataType:'json'
      });

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			promotion_name:
			{
				required:true
			},
			promotion_amount:
			{
				required:true
			},
			promotion_expiration_date:
			{
				required:true
			},
			promotion_valid_for:
			{
				required:true
			}
    },
		messages:
		{
			promotion_name:
			{
				required:"<?php echo lang('promotions_name_required'); ?>"
			},
			promotion_amount:
			{
				required:"<?php echo lang('promotions_amount_required'); ?>"
			},
			promotion_expiration_date:
			{
				required:"<?php echo lang('promotions_expiration_date_required'); ?>"
			},
			promotion_valid_for:
			{
				required:"<?php echo lang('promotions_valid_for_required'); ?>"
			}
		}
	});
  
});
</script>