<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Model
{
	public $widget_types;

	public function __construct()
	{
		parent::__construct();

		$this->widget_types = array(
			'column',
			'bar',
			'line',
			'pie',
			'table',
			'number'
		);
		$this->load->helper('array');
	}

	public function get($dashboard_id = null)
	{
		$this->db->from('dashboards');
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		if($dashboard_id != null){
			$this->db->where('id', $dashboard_id);
		}
		$query = $this->db->get();

		if($dashboard_id == null){
			return $query->result_array();
		}else{
			return $query->row_array();
		}
	}

	public function get_widgets($dashboard_id)
	{
		// Get all widgets that belong to a dashboard
		$this->db->from('dashboard_widgets');
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('dashboard_id', $dashboard_id);
		$query = $this->db->get();
		$widget_rows = $query->result_array();

		if(empty($widget_rows)){
			return array();
		}

		// Loop through widgets, organize by widget ID
		$widget_ids = array();
		$widgets = array();
		foreach($widget_rows as $key => $widget){
			$widget_ids[] = $widget['widget_id'];
			$widgets[$widget['widget_id']] = $widget;
			$widgets[$widget['widget_id']]['metrics'] = array();
		}

		// Get all metrics that belong to the widgets
		$this->db->from('dashboard_widget_metrics');
		$this->db->where_in('widget_id', $widget_ids);
		$this->db->order_by('widget_id ASC, position ASC');
		$query = $this->db->get();
		$widget_metrics = $query->result_array();

		// Loop through each widget metric and add to widget array
		foreach($widget_metrics as $key => $metric){
			$widgets[$metric['widget_id']]['metrics'][] = $metric;
		}

		return $widgets;
	}

	public function get_widget($widget_id)
	{
		// Get all widgets that belong to a dashboard
		$this->db->from('dashboard_widgets');
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('widget_id', $widget_id);
		$query = $this->db->get();
		$widget = $query->row_array();

		if(empty($widget)){
			return null;
		}

		// Get all metrics that belong to the widgets
		$this->db->select('metric_category, metric, relative_period_num, position');
		$this->db->from('dashboard_widget_metrics');
		$this->db->where('widget_id', $widget['widget_id']);
		$this->db->order_by('position');

		$query = $this->db->get();
		$widget_metrics = $query->result_array();

		$widget['metrics'] = $widget_metrics;
		return $widget;
	}

	public function save_widget($widget_id = null, $data)
	{
		$data = elements(array('name', 'period', 'data_grouping', 'period_num', 'type', 'metrics', 'dashboard_id','dashboard_width'), $data);

		if(empty($data['name']) || empty($data['dashboard_id']) || empty($data['metrics'])){
			return false;
		}

		$metrics = $data['metrics'];
		$period_num = $data['period_num'];
		$dashboard_width = $data['dashboard_width'];

		unset($data['metrics'], $data['period_num'], $data['dashboard_width']);
		$data['course_id'] = $this->session->userdata('course_id');

		if(empty($widget_id)){
			$data['width'] = 560;
			$data['height'] = 280;

			if(!empty($dashboard_width)){
				$data['pos_x'] = (int) (($dashboard_width / 2) - ($data['width'] / 2));
			}else{
				$data['pos_x'] = 400;
			}
			$data['pos_y'] = 40;

			$this->db->insert('dashboard_widgets', $data);
			$widget_id = $this->db->insert_id();

		}else{
			$this->db->where('widget_id', (int) $widget_id);
			$this->db->update('dashboard_widgets', $data);

			// Clear metrics that belong to widget to be replaced with new ones
			$this->db->delete('dashboard_widget_metrics', array('widget_id' => (int) $widget_id));
		}

		foreach($metrics as $key => $metric){
			$metric_parts = explode('/', $metric['metric']);
			$category = $metric_parts[0];
			$metric_name = $metric_parts[1];

			$this->db->insert('dashboard_widget_metrics', array(
				'widget_id' => $widget_id,
				'metric_category' => $category,
				'metric' => $metric_name,
				'relative_period_num' => $metric['relative_period'],
				'position' => $metric['position']
			));
		}

		$this->db->select('width, height, pos_x, pos_y');
		$result = $this->db->get_where('dashboard_widgets', array('widget_id'=>$widget_id));
		$widget_details = $result->row_array();

		$widget = array(
			'widget_id' => $widget_id,
			'name' => $data['name'],
			'width' => $widget_details['width'],
			'height' => $widget_details['height'],
			'type' => $data['type'],
			'pos_x' => $widget_details['pos_x'],
			'pos_y' => $widget_details['pos_y']
		);

		return $widget;
	}

	public function save_widget_position($widget_id, $position)
	{
		$position = elements(array('pos_x', 'pos_y'), $position);

		$this->db->where('widget_id', $widget_id);
		return $this->db->update('dashboard_widgets', $position);
	}

	public function save_widget_size($widget_id, $size)
	{
		$size = elements(array('width', 'height'), $size);
		if(empty($size['width']) || empty($size['height'])){
			return false;
		}

		$this->db->where('widget_id', $widget_id);
		return $this->db->update('dashboard_widgets', $size);
	}

	function delete_widget($widget_id)
	{
		$this->db->where('widget_id', $widget_id);
		return $this->db->update('dashboard_widgets', array('deleted' => 1));
	}

	public function save($dashboard_data, $dashboard_id = null)
	{
		$dashboard_data = elements(array('name', 'description'), $dashboard_data);

		if(empty($dashboard_data['name'])){
			return false;
		}
		$dashboard_data['course_id'] = $this->session->userdata('course_id');
		$dashboard_data['date_created'] = date('Y-m-d H:i:s');

		$this->db->insert('dashboards', $dashboard_data);
		return $this->db->insert_id();
	}

	function delete($dashboard_id)
	{
		$this->db->where('dashboard_id', $dashboard_id);
		return $this->db->update('dashboards', array('deleted' => 1));
	}
}