<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Help_topic extends CI_Model
{
  private $reserved_words = array(
      'and', 'or', 'of', 'is', 'the', '"', '\'',
      '.', 'has', 'had', 'have', 'to', 'a', 'by'
  );

  public function exists($topic_id)
  {
    $this->db->from('help_topics');
    $this->db->where('id', $topic_id);
    $this->db->limit(1);
    $res = $this->db->get();

    return ($res->num_rows()==1);
  }

  public function get_all()
  {
    $this->db->from('help_topics');
    return $this->db->get();
  }

  public function save(&$topic_data,$topic_id=false)
  {
    if(!$topic_id || $this->exists($topic_id))
    {
      // then create a new data
      if($this->db->insert('help_topics',$topic_data))
			{
				return true;
			}
			return false;
    }

    $this->db->where('id', $topic_id);
		return $this->db->update('help_topics',$topic_data);
  }

  public function get_all_topics($topic_id)
  {
    $this->db->from('help_posts');
    $this->db->where('topic_id', $topic_id);
    $this->db->where('deleted', 0);

    return $this->db->get();
  }

  public function get_topic($post_id)
  {
    $this->db->from('help_posts');
    $this->db->where('id', $post_id);
    $this->db->where('deleted', 0);    
    
    $query = $this->db->get();
    $row_data = $query->row();
    
    // increment views everytime the topic is viewed
    
    $this->db->where('id', $post_id);
    $this->db->where('deleted', 0);
    $this->db->update('help_posts',array('views'=>$row_data->views+1));

    return $row_data;
  }

  public function get_info($topic_id)
  {
    $this->db->from('help_topics');
    $this->db->where('id', $topic_id);

    $query = $this->db->get();

    if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $campaign_id is NOT an campaign
			$topic_obj=new stdClass();

			//Get all the fields from campaigns table
			$fields = $this->db->list_fields('help_topics');

			foreach ($fields as $field)
			{
				$topic_obj->$field='';
			}

			return $topic_obj;
		}    
  }

  public function delete_post($post_id)
  {
    $this->db->where('id', $post_id);
    return $this->db->update('help_posts', array('deleted'=> 1));
  }

  public function save_post($post_data, $post_id=false)
  {
    if(!$post_id)
    {
      return $this->db->insert('help_posts', $post_data);
    }

    $this->db->where('id', $post_id);
    return $this->db->update('help_posts', $post_data);
  }

  public function post_has_rated($user_id, $post_id)
  {
    $this->db->from('help_post_ratings');
    $this->db->where('user_id', $user_id);
    $this->db->where('post_id', $post_id);

    $res = $this->db->get();

    return ($res->num_rows()==1);
  }

  public function rate_post($user_id, $post_id, $rating)
  {
    $post_data = array(
       'post_id' => $post_id,
       'user_id' => $user_id,
       'rating' => $rating
    );

    return $this->db->insert('help_post_ratings', $post_data);
  }

  public function search_post($terms)
  {
    $this->sanitize_terms($terms);
    /*
    if(empty($terms)) $terms[0] = '$&%$!@#KJASD!@#%&^!@%#';
    
    $this->db->from('help_posts');    
    $this->db->where('deleted', 0);
    $count = 1;
    $where = "lower(title) REGEXP  '{$terms[0]}[ $]'";

    while(isset($terms[$count]) && !empty($terms[$count]))
    {
      $where .= " OR lower(title) REGEXP  '{$terms[$count]}[ $]'";
      $count++;
    }
    
    $where .= ' OR lower(keywords) like \'%' . implode(' ', $terms) .'%\'';
    */
    $search_str = implode(' ', $terms);

    $search_str = addslashes($search_str);

    $query = $this->db->query("
      SELECT *, MATCH(title, keywords) AGAINST('{$search_str}') AS score
      FROM `foreup_help_posts`
      WHERE MATCH(title, keywords) AGAINST('{$search_str}')
      AND deleted = 0
      ORDER BY score DESC
    ");

    return $query;
  }

  public function get_common_questions($limit = false)
  {
    $this->db->from('help_posts');
    $this->db->where('deleted', 0);
    $this->db->where('type', 'question');
    $this->db->order_by('views', 'desc');
    if($limit !== false) $this->db->limit($limit);

    return $this->db->get();
  }

  public function get_tutorials($limit = false)
  {
    $this->db->from('help_posts');    
    $this->db->where('deleted', 0);
    $this->db->where('type', 'tutorial');
    $this->db->order_by('views', 'desc');
    if($limit !== false) $this->db->limit($limit);

    return $this->db->get();
  }

  public function get_related_articles($topic_id, $title)
  {
    $title = addslashes($title);
    $query = $this->db->query("
      SELECT id, title, MATCH(title, contents) AGAINST('{$title}') AS score
      FROM `foreup_help_posts`
      WHERE MATCH(title, contents) AGAINST('{$title}')
      AND id != {$topic_id}
      ORDER BY score DESC LIMIT 5
    ");

    return $query;
  }

  private function sanitize_terms(&$terms)
  {
    foreach($terms as $k=>$term)
    {
      if(in_array($term, $this->reserved_words) || strlen($term) < 3)
      {
        unset($terms[$k]);
      }
    }

    sort($terms);
  }
}
