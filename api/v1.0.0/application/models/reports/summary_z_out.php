<?php
require_once("report.php");
class Summary_z_out extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_category'), 'align'=> 'left'), array('data'=>lang('reports_subtotal'), 'align'=> 'right'), array('data'=>lang('reports_total'), 'align'=> 'right'), array('data'=>lang('reports_tax'), 'align'=> 'right'), array('data'=>lang('reports_profit'), 'align'=> 'right'));
	}
	public function getDrawerCounts()
	{
		$between = 'between "' . $this->params['start_date'] . ' 00:00:00" and "' . $this->params['end_date'] . ' 23:59:59"';
		$this->db->select("people.first_name, people.last_name, register_log.*, (register_log.close_amount - register_log.open_amount - register_log.cash_sales_amount) as difference");
		$this->db->from('register_log as register_log');
		$this->db->join('people as people', 'register_log.employee_id=people.person_id');
		$this->db->where('(register_log.shift_start ' . $between);
		$this->db->or_where('register_log.shift_end ' . $between . ')');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		
		$results = $this->db->get();
		//echo $this->db->last_query();
		$data['summary'] = $results->result_array();
		
		$data['details'] = array();
		
		return $data;
	}
	public function getRevenues()
	{
		
	}
	public function getDebits()
	{
		
	}
	public function getCredits()
	{
		
	}
	public function getSecondSummaryData()
	{
		
	}
	public function getData()
	{
		$this->db->select('category, sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		
		if ($this->params['department'] != 'all')
			$this->db->where('department = "'.$this->params['department'].'"');
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		$this->db->group_by('category');
		$this->db->order_by('category');

		return $this->db->get()->result_array();		
	}
	
	public function getSummaryData()
	{
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		
		if ($this->params['department'] != 'all')
			$this->db->where('department = "'.$this->params['department'].'"');
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);

		return $this->db->get()->row_array();
	}
}
?>