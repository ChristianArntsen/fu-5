<?php
class Teesheet_restriction extends CI_Model
{
	public function get_all($teesheet_id, $type, $day_of_week)
	{
		$this->db->select('limit, start_time');
		$this->db->from('teesheet_restrictions');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->where('type', $type);
		$this->db->where("$day_of_week", 1);
		$this->db->order_by('start_time', 'asc');
		return $this->db->get();
	}
	
}