<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Note extends CI_Model
{
	public function get_all($limit = 5)
	{
		// CURRENT LOGGED IN EMPLOYEE
		$person_id = $this->session->userdata('person_id');
		$this->db->select('first_name, last_name, date, message, recipient, author, note_id');
		$this->db->from('notes');
		$this->db->join('people', 'people.person_id = notes.author');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('deleted', 0);
		$this->db->where("(recipient IS NULL OR recipient = {$person_id} OR author = {$person_id})");
		$this->db->order_by('date desc');
		if ($limit > 0)
			$this->db->limit($limit);

		return $this->db->get();
	}
	
	public function save(&$note_data, $note_id = -1)
	{
		if($note_id != -1)
		{
			$note_data['note_id'] = $note_id;
			$this->db->where('note_id', $note_id);
			return $this->db->update('notes', $note_data);
		}
		
		if($this->db->insert('notes',$note_data))
		{
			$note_data['note_id'] = $this->db->insert_id();
			return true;
		}
		return false;
	}
	
	function delete($note_id)
	{	
		$this->db->where("note_id = '$note_id'");
		return $this->db->update('notes', array('deleted' => 1));
	}
}