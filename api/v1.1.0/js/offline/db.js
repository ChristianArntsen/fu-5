// DB is the database variable
var foreup_db2 = {
	fail : function (error, query) {
		console.log('fail - ' + error.message + ': ' + query);
	},
	pass : function (results, query) {
		console.log('pass - ' + query);
	},
	create : function (result) {
		console.log('just created the database');
        console.dir(result);
	}
}

var db = SQLite({shortName:'foreup', defaultErrorHandler:foreup_db2.fail, defaultDataHandler:foreup_db2.pass, defaultDBCreateHandler:foreup_db2.create});
var foreup_db = {
	/*
	 * TABLES IN OUR LOCAL DATABASE INCLUDE
	 * Courses, * Customers, * Employees, * Fees, * Fee Types, * ? Giftcards, * Green Fees, * Green Fee Types,
	 * Items, * Items Taxes, * Item Kits, * Item Kit Taxes, * Item Modifiers, * Modifiers, * People, * ? Permissions,
	 * Quickbuttons, * Quickbutton Items, * Reservations, * ? Sales, * Schedules, * ? Sessions, * Tables, * Table Items,
	 * Table Item Modifiers, * Table Item Taxes, * Table Item Kits, * Table Item Kit Taxes, * Table Payments, * Teesheet,
	 * Teetime, * Terminals, * Tracks
	 * 
	 */
	exists : function () {
		db.query("SELECT name FROM sqlite_master WHERE type='table' AND name='foreup_courses'", [], 
			// If the table doesn't exist, then we have to build it and populate it
			function(r,q){
				console.dir(r.rows);
				if (r.rows.length === 0)
				{
					// If this is the first time loading offline mode, then the database doesn't exist, create it
					//foreup_teesheet.rebuild_database(foreup_teesheet.load_data);
				}	
				else
				{
					// Once a day, we will rebuild the database, just to make sure we have the most up to date data.
					// Before we rebuild the database, we need to make sure we sync local data up to server before we erase our local data with a rebuild
					//foreup_teesheet.push_local_changes(function(){
						// Even if we have the database built, we'll rebuild it daily with fresh data
					//	foreup_teesheet.daily_rebuild(foreup_teesheet.load_data);
					//});
				}
			}, 
			function(r,q){console.log('error in table detect sql')}
		);
	},
	table : {
		drop : function (table_name) {
			db.dropTable(table_name);
		},
		// DATABASE definitions
		create : function (table_name) {
			if (table_name == 'foreup_courses')
			{
				db.createTable('foreup_courses',
		     		'`course_id` int(11),'+
					'`active` tinyint(4),'+
					'`CID` int(11),'+
					'`name` varchar(80),'+
					'`address` varchar(40),'+
					'`city` varchar(40),'+
					'`state` varchar(20),'+
					'`state_name` varchar(50),'+
					'`postal` varchar(10),'+
					'`zip` varchar(10),'+
					'`country` varchar(40),'+
					'`area_code` varchar(10),'+
					"`timezone` varchar(20),"+
					"`DST` varchar(20),"+
					"`latitude_centroid` varchar(30),"+
					"`latitude_poly` varchar(30),"+
					"`longitude_centroid` varchar(30),"+
					"`longitude_poly` varchar(30),"+
					"`phone` varchar(20),"+
					"`holes` varchar(10),"+
					"`type` varchar(10),"+
					"`county` varchar(40),"+
					"`open_time` varchar(10) DEFAULT '0600',"+
					"`close_time` varchar(10) DEFAULT '1900',"+
					"`increment` varchar(10) DEFAULT '8',"+
					"`frontnine` varchar(10) DEFAULT '200',"+
					"`early_bird_hours_begin` varchar(10),"+
					"`early_bird_hours_end` varchar(10),"+
					"`morning_hours_begin` varchar(10),"+
					"`morning_hours_end` varchar(10),"+
					"`afternoon_hours_begin` varchar(10),"+
					"`afternoon_hours_end` varchar(10),"+
					"`twilight_hour` varchar(10) DEFAULT '1400',"+
					"`super_twilight_hour` varchar(10) DEFAULT '2399',"+
					"`holidays` tinyint(4),"+
					"`online_booking` tinyint(1) DEFAULT '0',"+
					"`booking_rules` text,"+
					"`config` int(1) DEFAULT '1',"+
					"`courses` int(1) DEFAULT '0',"+
					"`customers` int(1) DEFAULT '0',"+
					"`employees` int(1) DEFAULT '0',"+
					"`events` tinyint(4),"+
					"`giftcards` int(1) DEFAULT '0',"+
					"`item_kits` int(1) DEFAULT '0',"+
					"`items` int(1) DEFAULT '0',"+
					"`marketing_campaigns` int(1) DEFAULT '0',"+
					"`promotions` int(1) DEFAULT '0',"+
					"`receivings` int(1) DEFAULT '0',"+
					"`reports` int(1) DEFAULT '0',"+
					"`sales` int(1) DEFAULT '0',"+
					"`suppliers` int(1) DEFAULT '0',"+
					"`teesheets` int(1) DEFAULT '1',"+
					"`currency_symbol` varchar(3) DEFAULT '$',"+
					"`company_logo` varchar(255),"+
					"`date_format` varchar(20) DEFAULT 'middle_endian',"+
					"`default_tax_1_name` varchar(255) DEFAULT 'Sales Tax',"+
					"`default_tax_1_rate` varchar(20),"+
					"`default_tax_2_cumulative` varchar(20) DEFAULT '0',"+
					"`default_tax_2_name` varchar(255) DEFAULT 'Sales Tax 2',"+
					"`default_tax_2_rate` varchar(20),"+
					"`email` varchar(255),"+
					"`fax` varchar(40),"+
					"`language` varchar(20) DEFAULT 'english',"+
					"`mailchimp_api_key` varchar(255),"+
					"`number_of_items_per_page` varchar(10) DEFAULT '20',"+
					"`print_after_sale` varchar(10) DEFAULT '0',"+
					"`updated_printing` tinyint(4),"+
					"`after_sale_load` tinyint(4),"+
					"`teesheet_updates_automatically` tinyint(4),"+
					"`receipt_printer` varchar(255),"+
					"`track_cash` varchar(10),"+
					"`separate_courses` tcint(4) DEFAULT '0',"+
					"`return_policy` text,"+
					"`time_format` varchar(20) DEFAULT '12_hour',"+
					"`website` varchar(255),"+
					"`open_sun` tinyint(1) DEFAULT '1',"+
					"`open_mon` tinyint(1) DEFAULT '1',"+
					"`open_tue` tinyint(1) DEFAULT '1',"+
					"`open_wed` tinyint(1) DEFAULT '1',"+
					"`open_thu` tinyint(1) DEFAULT '1',"+
					"`open_fri` tinyint(1) DEFAULT '1',"+
					"`open_sat` tinyint(1) DEFAULT '1',"+
					"`weekend_fri` tinyint(1) DEFAULT '1',"+
					"`weekend_sat` tinyint(1) DEFAULT '1',"+
					"`weekend_sun` tinyint(1) DEFAULT '1',"+
					"`simulator` smallint(1),"+
					"`test_course` int(1) DEFAULT '0',"+
					"`at_login` varchar(20),"+
					"`at_password` varchar(20),"+
					"`at_test` tinyint(1) DEFAULT '1',"+
					"`mercury_id` varchar(255),"+
					"`mercury_password` varchar(255)"
		     	)
		    }
		    else if (table_name == 'foreup_customers')
		    {
		    	db.createTable('foreup_customers',
					"`local_only` tinyint(1) DEFAULT 0,"+
					"`person_id` int(10) NOT NULL,"+
					"`course_id` int(11) NOT NULL,"+
					"`member` smallint(6) NOT NULL,"+
					"`price_class` varchar(255) NOT NULL,"+
					"`account_number` varchar(255) DEFAULT NULL,"+
					"`account_balance` decimal(15,2) NOT NULL,"+
					"`taxable` int(1) NOT NULL DEFAULT '1',"+
					"`first_name` varchar(255) NOT NULL,"+
					"`last_name` varchar(255) NOT NULL,"+
					"`phone_number` varchar(255) NOT NULL,"+
					"`email` varchar(255) NOT NULL,"+
					"`address_1` varchar(255) NOT NULL,"+
					"`city` varchar(255) NOT NULL,"+
					"`state` varchar(255) NOT NULL,"+
					"`zip` varchar(255) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_employees')
		    {
		    	db.createTable('foreup_employees',
					"`local_only` tinyint(1) DEFAULT 0,"+
					"`person_id` int(10) NOT NULL,"+
					"`course_id` int(11) NOT NULL,"+
					"`username` varchar(255) DEFAULT NULL,"+
					"`password` varchar(255) NOT NULL,"+
					"`pin` mediumint(9) NOT NULL,"+
					"`card` varchar(255) DEFAULT NULL,"+
					"`user_level` tinyint(4) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_fees')
		    {
		    	db.createTable('foreup_fees',
					"`schedule_id` int(11) NOT NULL,"+
					"`item_number` varchar(255) NOT NULL,"+
					"`price_category_1` decimal(15,2) NOT NULL, `price_category_2` decimal(15,2) NOT NULL, `price_category_3` decimal(15,2) NOT NULL, `price_category_4` decimal(15,2) NOT NULL, `price_category_5` decimal(15,2) NOT NULL,"+
					"`price_category_6` decimal(15,2) NOT NULL, `price_category_7` decimal(15,2) NOT NULL, `price_category_8` decimal(15,2) NOT NULL, `price_category_9` decimal(15,2) NOT NULL, `price_category_10` decimal(15,2) NOT NULL,"+
					"`price_category_11` decimal(15,2) NOT NULL, `price_category_12` decimal(15,2) NOT NULL, `price_category_13` decimal(15,2) NOT NULL, `price_category_14` decimal(15,2) NOT NULL, `price_category_15` decimal(15,2) NOT NULL,"+
					"`price_category_16` decimal(15,2) NOT NULL, `price_category_17` decimal(15,2) NOT NULL, `price_category_18` decimal(15,2) NOT NULL, `price_category_19` decimal(15,2) NOT NULL, `price_category_20` decimal(15,2) NOT NULL,"+
					"`price_category_21` decimal(15,2) NOT NULL, `price_category_22` decimal(15,2) NOT NULL, `price_category_23` decimal(15,2) NOT NULL, `price_category_24` decimal(15,2) NOT NULL, `price_category_25` decimal(15,2) NOT NULL,"+
					"`price_category_26` decimal(15,2) NOT NULL, `price_category_27` decimal(15,2) NOT NULL, `price_category_28` decimal(15,2) NOT NULL, `price_category_29` decimal(15,2) NOT NULL, `price_category_30` decimal(15,2) NOT NULL,"+
					"`price_category_31` decimal(15,2) NOT NULL, `price_category_32` decimal(15,2) NOT NULL, `price_category_33` decimal(15,2) NOT NULL, `price_category_34` decimal(15,2) NOT NULL, `price_category_35` decimal(15,2) NOT NULL,"+
					"`price_category_36` decimal(15,2) NOT NULL, `price_category_37` decimal(15,2) NOT NULL, `price_category_38` decimal(15,2) NOT NULL, `price_category_39` decimal(15,2) NOT NULL, `price_category_40` decimal(15,2) NOT NULL,"+
					"`price_category_41` decimal(15,2) NOT NULL, `price_category_42` decimal(15,2) NOT NULL, `price_category_43` decimal(15,2) NOT NULL, `price_category_44` decimal(15,2) NOT NULL, `price_category_45` decimal(15,2) NOT NULL,"+
					"`price_category_46` decimal(15,2) NOT NULL, `price_category_47` decimal(15,2) NOT NULL, `price_category_48` decimal(15,2) NOT NULL, `price_category_49` decimal(15,2) NOT NULL, `price_category_50` decimal(15,2) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_fee_types')
		    {
		    	db.createTable('foreup_fee_types',
					"`course_id` int(11) NOT NULL,"+
					"`schedule_id` int(11) NOT NULL,"+
					"`price_category_1` varchar(255) NOT NULL, `price_category_2` varchar(255) NOT NULL, `price_category_3` varchar(255) NOT NULL, `price_category_4` varchar(255) NOT NULL, `price_category_5` varchar(255) NOT NULL,"+
					"`price_category_6` varchar(255) NOT NULL, `price_category_7` varchar(255) NOT NULL, `price_category_8` varchar(255) NOT NULL, `price_category_9` varchar(255) NOT NULL, `price_category_10` varchar(255) NOT NULL,"+
					"`price_category_11` varchar(255) NOT NULL, `price_category_12` varchar(255) NOT NULL, `price_category_13` varchar(255) NOT NULL, `price_category_14` varchar(255) NOT NULL, `price_category_15` varchar(255) NOT NULL,"+
					"`price_category_16` varchar(255) NOT NULL, `price_category_17` varchar(255) NOT NULL, `price_category_18` varchar(255) NOT NULL, `price_category_19` varchar(255) NOT NULL, `price_category_20` varchar(255) NOT NULL,"+
					"`price_category_21` varchar(255) NOT NULL, `price_category_22` varchar(255) NOT NULL, `price_category_23` varchar(255) NOT NULL, `price_category_24` varchar(255) NOT NULL, `price_category_25` varchar(255) NOT NULL,"+
					"`price_category_26` varchar(255) NOT NULL, `price_category_27` varchar(255) NOT NULL, `price_category_28` varchar(255) NOT NULL, `price_category_29` varchar(255) NOT NULL, `price_category_30` varchar(255) NOT NULL,"+
					"`price_category_31` varchar(255) NOT NULL, `price_category_32` varchar(255) NOT NULL, `price_category_33` varchar(255) NOT NULL, `price_category_34` varchar(255) NOT NULL, `price_category_35` varchar(255) NOT NULL,"+
					"`price_category_36` varchar(255) NOT NULL, `price_category_37` varchar(255) NOT NULL, `price_category_38` varchar(255) NOT NULL, `price_category_39` varchar(255) NOT NULL, `price_category_40` varchar(255) NOT NULL,"+
					"`price_category_41` varchar(255) NOT NULL, `price_category_42` varchar(255) NOT NULL, `price_category_43` varchar(255) NOT NULL, `price_category_44` varchar(255) NOT NULL, `price_category_45` varchar(255) NOT NULL,"+
					"`price_category_46` varchar(255) NOT NULL, `price_category_47` varchar(255) NOT NULL, `price_category_48` varchar(255) NOT NULL, `price_category_49` varchar(255) NOT NULL, `price_category_50` varchar(255) NOT NULL"
				);
		    }
		    else if (table_name == 'giftcards')
		    {
		    	db.createTable('foreup_giftcards',
					"`course_id` int(11) NOT NULL,"+
					"`giftcard_id` int(11) NOT NULL,"+
					"`giftcard_number` varchar(255) DEFAULT NULL,"+
					"`value` decimal(15,2) NOT NULL,"+
					"`customer_id` int(11) DEFAULT NULL,"+
					"`details` text NOT NULL,"+
					"`expiration_date` date NOT NULL,"+
					"`department` varchar(40) NOT NULL,"+
					"`category` varchar(40) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_green_fees')
		    {
		    	db.createTable('foreup_green_fees',
					"`schedule_id` int(11) NOT NULL,"+
					"`item_number` varchar(255) NOT NULL,"+
					"`price_category_1` decimal(15,2) NOT NULL, `price_category_2` decimal(15,2) NOT NULL, `price_category_3` decimal(15,2) NOT NULL, `price_category_4` decimal(15,2) NOT NULL, `price_category_5` decimal(15,2) NOT NULL,"+
					"`price_category_6` decimal(15,2) NOT NULL, `price_category_7` decimal(15,2) NOT NULL, `price_category_8` decimal(15,2) NOT NULL, `price_category_9` decimal(15,2) NOT NULL, `price_category_10` decimal(15,2) NOT NULL,"+
					"`price_category_11` decimal(15,2) NOT NULL, `price_category_12` decimal(15,2) NOT NULL, `price_category_13` decimal(15,2) NOT NULL, `price_category_14` decimal(15,2) NOT NULL, `price_category_15` decimal(15,2) NOT NULL,"+
					"`price_category_16` decimal(15,2) NOT NULL, `price_category_17` decimal(15,2) NOT NULL, `price_category_18` decimal(15,2) NOT NULL, `price_category_19` decimal(15,2) NOT NULL, `price_category_20` decimal(15,2) NOT NULL,"+
					"`price_category_21` decimal(15,2) NOT NULL, `price_category_22` decimal(15,2) NOT NULL, `price_category_23` decimal(15,2) NOT NULL, `price_category_24` decimal(15,2) NOT NULL, `price_category_25` decimal(15,2) NOT NULL,"+
					"`price_category_26` decimal(15,2) NOT NULL, `price_category_27` decimal(15,2) NOT NULL, `price_category_28` decimal(15,2) NOT NULL, `price_category_29` decimal(15,2) NOT NULL, `price_category_30` decimal(15,2) NOT NULL,"+
					"`price_category_31` decimal(15,2) NOT NULL, `price_category_32` decimal(15,2) NOT NULL, `price_category_33` decimal(15,2) NOT NULL, `price_category_34` decimal(15,2) NOT NULL, `price_category_35` decimal(15,2) NOT NULL,"+
					"`price_category_36` decimal(15,2) NOT NULL, `price_category_37` decimal(15,2) NOT NULL, `price_category_38` decimal(15,2) NOT NULL, `price_category_39` decimal(15,2) NOT NULL, `price_category_40` decimal(15,2) NOT NULL,"+
					"`price_category_41` decimal(15,2) NOT NULL, `price_category_42` decimal(15,2) NOT NULL, `price_category_43` decimal(15,2) NOT NULL, `price_category_44` decimal(15,2) NOT NULL, `price_category_45` decimal(15,2) NOT NULL,"+
					"`price_category_46` decimal(15,2) NOT NULL, `price_category_47` decimal(15,2) NOT NULL, `price_category_48` decimal(15,2) NOT NULL, `price_category_49` decimal(15,2) NOT NULL, `price_category_50` decimal(15,2) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_green_fee_types')
		    {
		    	db.createTable('foreup_green_fee_types',
					"`course_id` int(11) NOT NULL,"+
					"`schedule_id` int(11) NOT NULL,"+
					"`price_category_1` varchar(255) NOT NULL, `price_category_2` varchar(255) NOT NULL, `price_category_3` varchar(255) NOT NULL, `price_category_4` varchar(255) NOT NULL, `price_category_5` varchar(255) NOT NULL,"+
					"`price_category_6` varchar(255) NOT NULL, `price_category_7` varchar(255) NOT NULL, `price_category_8` varchar(255) NOT NULL, `price_category_9` varchar(255) NOT NULL, `price_category_10` varchar(255) NOT NULL,"+
					"`price_category_11` varchar(255) NOT NULL, `price_category_12` varchar(255) NOT NULL, `price_category_13` varchar(255) NOT NULL, `price_category_14` varchar(255) NOT NULL, `price_category_15` varchar(255) NOT NULL,"+
					"`price_category_16` varchar(255) NOT NULL, `price_category_17` varchar(255) NOT NULL, `price_category_18` varchar(255) NOT NULL, `price_category_19` varchar(255) NOT NULL, `price_category_20` varchar(255) NOT NULL,"+
					"`price_category_21` varchar(255) NOT NULL, `price_category_22` varchar(255) NOT NULL, `price_category_23` varchar(255) NOT NULL, `price_category_24` varchar(255) NOT NULL, `price_category_25` varchar(255) NOT NULL,"+
					"`price_category_26` varchar(255) NOT NULL, `price_category_27` varchar(255) NOT NULL, `price_category_28` varchar(255) NOT NULL, `price_category_29` varchar(255) NOT NULL, `price_category_30` varchar(255) NOT NULL,"+
					"`price_category_31` varchar(255) NOT NULL, `price_category_32` varchar(255) NOT NULL, `price_category_33` varchar(255) NOT NULL, `price_category_34` varchar(255) NOT NULL, `price_category_35` varchar(255) NOT NULL,"+
					"`price_category_36` varchar(255) NOT NULL, `price_category_37` varchar(255) NOT NULL, `price_category_38` varchar(255) NOT NULL, `price_category_39` varchar(255) NOT NULL, `price_category_40` varchar(255) NOT NULL,"+
					"`price_category_41` varchar(255) NOT NULL, `price_category_42` varchar(255) NOT NULL, `price_category_43` varchar(255) NOT NULL, `price_category_44` varchar(255) NOT NULL, `price_category_45` varchar(255) NOT NULL,"+
					"`price_category_46` varchar(255) NOT NULL, `price_category_47` varchar(255) NOT NULL, `price_category_48` varchar(255) NOT NULL, `price_category_49` varchar(255) NOT NULL, `price_category_50` varchar(255) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_items')
		    {
		    	db.createTable('foreup_items',
					"`course_id` int(11) NOT NULL,"+
					"`name` varchar(255) DEFAULT NULL,"+
					"`department` varchar(255) NOT NULL,"+
					"`category` varchar(255) NOT NULL,"+
					"`subcategory` varchar(255) NOT NULL,"+
					"`supplier_id` int(11) DEFAULT NULL,"+
					"`item_number` varchar(255) DEFAULT NULL,"+
					"`description` varchar(255) NOT NULL,"+
					"`cost_price` decimal(15,2) NOT NULL,"+
					"`unit_price` decimal(15,2) NOT NULL,"+
					"`max_discount` decimal(15,2) NOT NULL DEFAULT '100',"+
					"`quantity` int(11) NOT NULL DEFAULT '0',"+
					"`is_unlimited` tinyint(1) NOT NULL,"+
					"`reorder_level` int(11) NOT NULL DEFAULT '0',"+
					"`food_and_beverage` tinyint(1) NOT NULL DEFAULT '0',"+
					"`location` varchar(255) NOT NULL,"+
					"`item_id` int(11) NOT NULL DEFAULT '0',"+
					"`is_serialized` tinyint(1) NOT NULL,"+
					"`is_giftcard` tinyint(1) NOT NULL,"+
					"`invisible` tinyint(1) NOT NULL"				
				);
		    }
		    else if (table_name == 'foreup_items_taxes')
		    {
		    	db.createTable('foreup_items_taxes',
					"`course_id` int(11) NOT NULL,"+
					"`item_id` int(11) NOT NULL DEFAULT '0',"+
					"`name` varchar(255) DEFAULT NULL,"+
					"`percent` decimal(15,3) NOT NULL,"+
					"`cumulative` tinyint(1) NOT NULL DEFAULT '0'"
				);
		    }
		    else if (table_name == 'foreup_item_kits')
		    {
		    	db.createTable('foreup_item_kits',
					"`course_id` int(11) NOT NULL,"+
					"`item_kit_id` int(11) NOT NULL DEFAULT '0',"+
					"`item_kit_number` varchar(255) DEFAULT NULL,"+
					"`name` varchar(255) DEFAULT NULL,"+
					"`department` varchar(255) NOT NULL,"+
					"`category` varchar(255) NOT NULL,"+
					"`subcategory` varchar(255) NOT NULL,"+
					"`description` varchar(255) NOT NULL,"+
					"`cost_price` decimal(15,2) NOT NULL,"+
					"`unit_price` decimal(15,2) NOT NULL,"+
					"`is_punch_card` tinyint(1) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_item_kits_taxes')
		    {
		    	db.createTable('foreup_item_kits_taxes',
					"`course_id` int(11) NOT NULL,"+
					"`item_kit_id` int(11) NOT NULL,"+
					"`item_id` int(11) NOT NULL,"+
					"`quantity` decimal(15,3) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_item_kit_items')
		    {
		    	db.createTable('foreup_item_kit_items',
					"`course_id` int(11) NOT NULL,"+
					"`item_kit_id` int(11) NOT NULL,"+
					"`name` varchar(255) DEFAULT NULL,"+
					"`percent` decimal(15,3) NOT NULL,"+
					"`cumulative` tinyint(1) NOT NULL DEFAULT '0'"
				);
		    }
		    else if (table_name == 'foreup_item_modifiers')
		    {
		    	db.createTable('foreup_item_modifiers',
					"`item_id` int(11) NOT NULL,"+
					"`modifier_id` int(11) NOT NULL,"+
					"`override_price` decimal(15,2) NOT NULL,"+
					"`auto_select` tinyint(1) NOT NULL DEFAULT '0'"
				);
		    }
		    else if (table_name == 'foreup_modifiers')
		    {
		    	db.createTable('foreup_modifiers',
					"`course_id` int(11) NOT NULL,"+
					"`modifier_id` int(11) NOT NULL,"+
					"`name` varchar(255) NOT NULL,"+
					"`default_price` decimal(15,2) NOT NULL,"+
					"`options` text DEFAULT NULL"
				);
		    }
		    else if (table_name == 'foreup_people')
		    {
		    	db.createTable('foreup_people',
					"`first_name` varchar(255) NOT NULL,"+
					"`last_name` varchar(255) NOT NULL,"+
					"`phone_number` varchar(255) NOT NULL,"+
					"`cell_phone_number` varchar(255) NOT NULL,"+
					"`email` varchar(255) NOT NULL,"+
					"`birthday` date NOT NULL,"+
					"`address_1` varchar(255) NOT NULL,"+
					"`address_2` varchar(255) NOT NULL,"+
					"`city` varchar(255) NOT NULL,"+
					"`state` varchar(255) NOT NULL,"+
					"`zip` varchar(255) NOT NULL,"+
					"`country` varchar(255) NOT NULL,"+
					"`comments` text NOT NULL,"+
					"`person_id` int(11) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_quickbuttons')
		    {
		    	db.createTable('foreup_quickbuttons',
					"`quickbutton_id` int(11) NOT NULL,"+
					"`course_id` int(11) NOT NULL,"+
					"`display_name` varchar(255) NOT NULL,"+
					"`tab` tinyint(1) NOT NULL DEFAULT '1',"+
					"`position` int(11) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_quickbutton_items')
		    {
		    	db.createTable('foreup_quickbutton_items',
					"`quickbutton_id` int(11) NOT NULL,"+
					"`item_id` int(11) DEFAULT NULL,"+
					"`item_kit_id` int(11) DEFAULT NULL,"+
					"`order` tinyint(1) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_rainchecks')
		    {
		    	db.createTable('foreup_rainchecks',
					"`raincheck_id` int(11) NOT NULL,"+
					"`teesheet_id` int(11) NOT NULL,"+
					"`date_issued` datettime NOT NULL,"+
					"`date_redeemed` datetime NOT NULL,"+
					"`players` tinyint(4) NOT NULL,"+
					"`holes_completed` tinyint(4) NOT NULL,"+
					"`customer_id` int(11) NOT NULL,"+
					"`employee_id` int(11) NOT NULL,"+
					"`green_fee` float(15,2) NOT NULL,"+
					"`cart_fee` float(15,2) NOT NULL,"+
					"`tax` float(15,2) NOT NULL,"+
					"`total` float(15,2) NOT NULL,"+
					"`green_fee_price_category` varchar(255) NOT NULL,"+
					"`cart_price_category` varchar(255) NOT NULL"
				);
		    }
		    else if (table_name == 'foreup_reservations')
		    {
		    	db.createTable('foreup_reservations',
	       			"`local_only` tinyint(1) DEFAULT 0,"+
			  		'`reservation_id` varchar(21) PRIMARY KEY NOT NULL,'+
					'`track_id` int(11) NOT NULL,'+
					'`type` varchar(20) NOT NULL,'+
					'`status` varchar(20) NOT NULL,'+
					'`start` bigint(20) NOT NULL,'+
					'`end` bigint(20) NOT NULL,'+
					'`allDay` varchar(5) NOT NULL,'+
					'`player_count` tinyint(4) NOT NULL,'+
					'`holes` tinyint(4) NOT NULL,'+
					'`carts` float NOT NULL,'+
					"`paid_player_count` tinyint(4) NOT NULL DEFAULT '0',"+
					"`paid_carts` tinyint(4) NOT NULL DEFAULT '0',"+
					'`title` varchar(50) NOT NULL,'+
					'`phone` varchar(50) NOT NULL,'+
					'`email` varchar(50) NOT NULL,'+
					'`details` text NOT NULL,'+
					'`side` varchar(10) NOT NULL,'+
					'`className` varchar(40) NOT NULL,'+
					'`person_id` int(10) NOT NULL,'+
					'`person_name` varchar(255) NOT NULL,'+
					'`person_id_2` int(11) NOT NULL,'+
					'`person_name_2` varchar(255) NOT NULL,'+
					'`person_id_3` int(11) NOT NULL,'+
					'`person_name_3` varchar(255) NOT NULL,'+
					'`person_id_4` int(11) NOT NULL,'+
					'`person_name_4` varchar(255) NOT NULL,'+
					'`person_id_5` int(11) NOT NULL,'+
					'`person_name_5` varchar(255) NOT NULL,'+
					'`last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,'+
					"`date_booked` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',"+
					"`date_cancelled` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',"+
					'`booking_source` varchar(255) NOT NULL,'+
					'`booker_id` varchar(255) NOT NULL,'+
					'`canceller_id` int(11) NOT NULL,'+
					'`saved_offline` tinyint(4) DEFAULT 0'
				);
		    }
		    else if (table_name == 'foreup_teetime')
		    {
		    	db.createTable('foreup_teetime',
	       			"`local_only` tinyint(1) DEFAULT 0,"+
			  		'`TTID` varchar(21) PRIMARY KEY NOT NULL,'+
					'`TSID` varchar(20) NOT NULL,'+
					'`type` varchar(20) NOT NULL,'+
					'`status` varchar(20) NOT NULL,'+
					'`start` bigint(20) NOT NULL,'+
					'`end` bigint(20) NOT NULL,'+
					'`allDay` varchar(5) NOT NULL,'+
					'`player_count` tinyint(4) NOT NULL,'+
					'`holes` tinyint(4) NOT NULL,'+
					'`carts` float NOT NULL,'+
					"`paid_player_count` tinyint(4) NOT NULL DEFAULT '0',"+
					"`paid_carts` tinyint(4) NOT NULL DEFAULT '0',"+
					'`clubs` tinyint(4) NOT NULL,'+
					'`title` varchar(50) NOT NULL,'+
					'`cplayer_count` tinyint(4) NOT NULL,'+
					'`choles` tinyint(4) NOT NULL,'+
					'`ccarts` float NOT NULL,'+
					'`cpayment` varchar(10) NOT NULL,'+
					'`phone` varchar(50) NOT NULL,'+
					'`email` varchar(50) NOT NULL,'+
					'`details` text NOT NULL,'+
					'`side` varchar(10) NOT NULL,'+
					'`className` varchar(40) NOT NULL,'+
					'`person_id` int(10) NOT NULL,'+
					'`person_name` varchar(255) NOT NULL,'+
					'`person_id_2` int(11) NOT NULL,'+
					'`person_name_2` varchar(255) NOT NULL,'+
					'`person_id_3` int(11) NOT NULL,'+
					'`person_name_3` varchar(255) NOT NULL,'+
					'`person_id_4` int(11) NOT NULL,'+
					'`person_name_4` varchar(255) NOT NULL,'+
					'`person_id_5` int(11) NOT NULL,'+
					'`person_name_5` varchar(255) NOT NULL,'+
					'`last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,'+
					"`date_booked` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',"+
					"`date_cancelled` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',"+
					'`booking_source` varchar(255) NOT NULL,'+
					'`booker_id` varchar(255) NOT NULL,'+
					'`canceller_id` int(11) NOT NULL,'+
					'`teetime_id` int(11) NOT NULL,'+
					'`teesheet_id` int(11) NOT NULL,'+
					'`saved_offline` tinyint(4) DEFAULT 0'
				);
		    }
		    else if (table_name == 'foreup_teesheet')
		    {
		    	db.createTable('foreup_teesheet',
	             	  "`teesheet_id` INTEGER PRIMARY KEY NOT NULL,"+
					  "`course_id` int(11) NOT NULL,"+
					  "`CID` int(11) NOT NULL,"+
					  "`TSID` varchar(20) NOT NULL,"+
					  "`title` varchar(255) NOT NULL,"+
					  "`associated_courses` text NOT NULL,"+
					  "`holes` tinyint(4) NOT NULL,"+
					  "`online_open_time` varchar(10) NOT NULL DEFAULT '0900',"+
					  "`online_close_time` varchar(10) NOT NULL DEFAULT '1800',"+
					  "`increment` float(4,2) NOT NULL DEFAULT '8.00',"+
					  "`frontnine` smallint(6) NOT NULL DEFAULT '200',"+
					  "`fntime` smallint(6) NOT NULL,"+
					  "`default` tinyint(1) NOT NULL,"+
					  "`online_booking` tinyint(1) NOT NULL,"+
					  "`deleted` tinyint(1) NOT NULL"
				);
		    }
		    
//		    else if (table_name = )
		},
		load_data : function (table_name, callback) {
			$.ajax({
	           type: "POST",
	           url: "index.php/teesheets/fetch_all_data/",
	           data: '',
	           success: function(response){
	           		foreup_db.table.save_data(table_name, response);
	//           		console.dir(response);
					db.query(
						"INSERT INTO foreup_daily_sync (`date`) VALUES (date('now'))",
						[],
						function (r,q) {
							console.log('successful foreup_daily_sync ');
						} ,
						function (r,q) {console.log('failed update foreup_daily_sync '+q)}
					);
	           		if ($.isFunction(callback))
						callback();
			    },
	            dataType:'json'
	         });
		},
		save_data : function (table_name, data) {
			if (table_name == 'foreup_teesheet') {
				for (i in data)
				{	
					ts = data[i];
					sql = "('"+ts.teesheet_id+"','"+ts.course_id+"','"+ts.title+"','"+ts.holes+"','"+ts.online_open_time+"','"+ts.online_close_time+"','"+ts.increment+"','"+ts.frontnine+"','"+ts["default"]+"','"+ts.online_booking+"','"+ts.deleted+"','"+ts.CID+"','"+ts.TSID+"','"+ts.associated_courses+"','"+ts.fntime+"')";
					db.query(
						'INSERT INTO foreup_teesheet (`teesheet_id`, `course_id`, `title`, `holes`, `online_open_time`, `online_close_time`, `increment`, `frontnine`, `default`, `online_booking`, `deleted`, `CID`, `TSID`, `associated_courses`, `fntime`) VALUES '+sql,
						[],
						function (r,q) {
							console.log('successful save_all_data ');
						} ,
						function (r,q) {console.log('failed save_all_date '+q)}
					);
				}
			}
			else if (table_name == 'foreup_teetime') {
				for (i in data)
				{	
					tt = data[i];
					sql = "('"+tt.TTID+"','"+tt.TSID+"','"+tt.type+"','"+tt.status+"','"+tt.start+"','"+tt.end+"','"+tt.allDay+"','"+tt.player_count+"','"+tt.holes+"','"+tt.carts+"','"+tt.paid_player_count+"','"+tt.paid_carts+"','"+tt.clubs+"','"+tt.title+"','"+tt.cplayer_count+"','"+tt.choles+"','"+tt.ccarts+"','"+tt.cpayment+"','"+tt.phone+"','"+tt.email+"','"+tt.details+"','"+tt.side+"','"+tt.className+"','"+tt.person_id+"','"+tt.person_name+"','"+tt.person_id_2+"','"+tt.person_name_2+"','"+tt.person_id_3+"','"+tt.person_name_3+"','"+tt.person_id_4+"','"+tt.person_name_4+"','"+tt.person_id_5+"','"+tt.person_name_5+"','"+tt.last_updated+"','"+tt.date_booked+"','"+tt.date_cancelled+"','"+tt.booking_source+"','"+tt.booker_id+"','"+tt.canceller_id+"','"+tt.teetime_id+"','"+tt.teesheet_id+"')";
					db.query(
						'INSERT INTO foreup_teetime (`TTID`, `TSID`, `type`, `status`, `start`, `end`, `allDay`, `player_count`, `holes`, `carts`, `paid_player_count`, `paid_carts`, `clubs`, `title`, `cplayer_count`, `choles`, `ccarts`, `cpayment`, `phone`, `email`, `details`, `side`, `className`, `person_id`, `person_name`, `person_id_2`, `person_name_2`, `person_id_3`, `person_name_3`, `person_id_4`, `person_name_4`, `person_id_5`, `person_name_5`, `last_updated`, `date_booked`, `date_cancelled`, `booking_source`, `booker_id`, `canceller_id`, `teetime_id`, `teesheet_id`) VALUES '+sql,
						[],
						function (r,q) {
							console.log('successful tt save_all_data ');
						} ,
						function (r,q) {console.log('failed save_all_date '+q)}
					);
				}
			}
			else if (table_name == 'foreup_customers') {
				for (i in data)
				{	
					cu = data[i];
					sql = "('"+cu.person_id+"','"+cu.course_id+"','"+cu.member+"','"+cu.price_class+"','"+cu.account_number+"','"+cu.account_balance+"','"+cu.taxable+"','"+cu.first_name+"','"+cu.last_name+"','"+cu.phone_number+"','"+cu.email+"','"+cu.address_1+"','"+cu.city+"','"+cu.state+"','"+cu.zip+"')";
					db.query(
						'INSERT INTO foreup_customers (`person_id`, `course_id`, `member`, `price_class`, `account_number`, `account_balance`, `taxable`, `first_name`, `last_name`, `phone_number`, `email`, `address_1`, `city`, `state`, `zip`) VALUES '+sql,
						[],
						function (r,q) {
							console.log('successful cu save_all_data ');
						} ,
						function (r,q) {console.log('failed save_all_date '+q)}
					);
				}
			}
			else if (table_name == 'foreup_courses') {
				c = data;
				sql = "('"+c.course_id+"','"+c.active+"','"+c.name+"','"+c.holes+"','"+c.open_time+"','"+c.close_time+"','"+c.early_bird_hours_begin+"','"+c.early_bird_hours_end+"','"+c.morning_hours_begin+"','"+c.morning_hours_end+"','"+c.afternoon_hours_begin+"','"+c.afternoon_hours_end+"','"+c.twilight_hour+"','"+c.super_twilight_hour+"','"+c.holidays+"','"+c.config+"','"+c.courses+"','"+c.sales+"','"+c.teesheets+"','"+c.weekend_fri+"','"+c.weekend_sat+"','"+c.weekend_sun+"','"+c.simulator+"')";
				db.query(
					'INSERT INTO foreup_courses (`course_id`, `active`, `name`, `holes`, `open_time`, `close_time`, `early_bird_hours_begin`, `early_bird_hours_end`, `morning_hours_begin`, `morning_hours_end`, `afternoon_hours_begin`, `afternoon_hours_end`, `twilight_hour`, `super_twilight_hour`, `holidays`, `config`, `courses`, `sales`, `teesheets`, `weekend_fri`, `weekend_sat`, `weekend_sun`, `simulator`) VALUES '+sql,
					[],
					function (r,q) {
						console.log('successful c save_all_data ');
					} ,
					function (r,q) {console.log('failed save_all_date '+q)}
				);
			}
		}
	}
	
};
