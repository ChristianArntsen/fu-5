<?php
class Article extends CI_Model
{
	function get_newest_article($column) {
		$this->db->from('home_articles');
		$this->db->order_by('date desc');
		$this->db->where('column', $column);
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}		
	function save($article_data)
	{
		$this->db->insert('home_articles', $article_data);
	}
		
}
	