<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once ("user.php");
class Account extends User
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('Sale');
    $this->load->model('Teetime');
  }

  public function index()
  {
    $data['show_all'] = $this->input->get('show_all', '');

    $data['allowed_modules']=$this->Module->get_allowed_modules(0);

    $ui = new stdClass();
    $ui->first_name = 'John';
    $ui->last_name  = 'Doe';
    $data['user_info'] = $ui;
    $am = array('reservations', 'directories', 'calendar', 'account',
        'newsletter', 'settings', 'information');
    $data['modules'] = $am;
    $data['controller_name'] = __CLASS__;

    // get accounts data
    $account_limit = ($data['show_all'] == 'accounts') ? 10000 : 5;

    $people = $this->Employee->get_all($account_limit);

    $accounts = array();

    foreach($people->result() as $person)
    {
      $course_info = $this->Course->get_info($person->course_id);
      $accounts[] = array(
          'last_name' => $person->last_name,
          'first_name' => $person->first_name,
          'job_title' => $person->position,
          'golf_course' => $course_info->name,
          'email' => mailto($person->email,$person->email, array('class' => 'underline')),
          'phone_number' => $person->phone_number,
          'activated' => $person->activated
      );
    }

    $data['accounts'] = $accounts;

    // get purchases data
    $purchases_limit = ($data['show_all'] == 'purchases') ? false : 5;
    $res = $this->Sale->get_purchases($purchases_limit);

    $purchases = array();

    foreach($res->result_array() as $r)
    {
      $purchases[] = array(
          'description' => $r['description'],
          'date' => date('m/d/y', strtotime($r['sale_time'])),
          'amount' => number_format($r['payment_amount'], 2, '.', ',')
      );
    }
    $data['purchases'] = $purchases;

    // get bookings data
    $bookings_limit = ($data['show_all'] == 'bookings') ? false : 5;
    $results = $this->Teetime->get_bookings($bookings_limit);
    $bookings = array();

    foreach($results as $b)
    {
      $bookings[] = array(
          'date_booked' => date('m/d/Y H:i a', strtotime($b['date_booked'])),
          'start' => date('m/d/Y  H:i a', strtotime($b['start'])),
          'end' => date('m/d/Y  H:i a', strtotime($b['end'])),
          'player_count' => $b['player_count'],
          'holes' => $b['holes']
      );
    }
    $data['bookings'] = $bookings;

    $data['contents'] = $this->load->view('user/account/manage', $data, true);
    $this->load->view("user/home", $data);
  }
}