<ul id="error_message_box"></ul>
<?php
echo form_open('config/save_terminal_details/'.$terminal_info['terminal_id'], array('id'=>'terminal_form'));
?>
<fieldset id="terminal_basic_info">
<legend><?php echo lang("config_terminal_basic_information"); ?></legend>
<div class=''>
<div class="field_row clearfix">
<?php echo form_label(lang('config_terminal_name').':', 't_label',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_label',
		'id'=>'t_label',
		'value'=>$terminal_info['label'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_quickbutton_tab').':', 't_quickbutton_tab',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_quickbutton_tab', array("1"=>'1', '2'=>'2', '3'=>'3'), $terminal_info['quickbutton_tab']);?>
	</div>
</div>
<?php if ($this->config->item('mercury_id')) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('config_mercury_id').':', 't_mercury_id',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_mercury_id',
		'id'=>'t_mercury_id',
		'value'=>$terminal_info['mercury_id'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_mercury_password').':', 't_mercury_password',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_mercury_password',
		'id'=>'t_mercury_password',
		'value'=>$terminal_info['mercury_password'])
	);?>
	</div>
</div>
<?php } ?>
<?php if ($this->config->item['ets_key']) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('config_ets_key').':', 't_ets_key',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_ets_key',
		'id'=>'t_ets_key',
		'value'=>$terminal_info['ets_key'])
	);?>
	</div>
</div>
<?php } ?>
<?php if ($this->config->item('track_cash')) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('config_track_cash').':', 't_track_cash',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'t_track_cash',
		'id'=>'t_track_cash',
		'value'=>1,
		'checked'=>($terminal_info['track_cash'])? 1 : 0)
	);?>
	</div>
</div>
<?php } ?>
<?php if ($this->config->item('print_after_sale')) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('config_auto_print_receipts').':', 't_auto_print_receipts',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'t_auto_print_receipts',
		'id'=>'t_auto_print_receipts',
		'value'=>1,
		'checked'=>($terminal_info['auto_print_receipts'])? 1 : 0)
	);?>
	</div>
</div>
<?php if ($this->config->item('webprnt')) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('config_webprnt_ip').':', 't_webprnt_ip',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_webprnt_ip',
		'id'=>'t_webprnt_ip',
		'value'=>$terminal_info['webprnt_ip'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_webprnt_hot_ip').':', 't_webprnt_hot_ip',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_webprnt_hot_ip',
		'id'=>'t_webprnt_hot_ip',
		'value'=>$terminal_info['webprnt_hot_ip'])
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_webprnt_cold_ip').':', 't_webprnt_cold_ip',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'t_webprnt_cold_ip',
		'id'=>'t_webprnt_cold_ip',
		'value'=>$terminal_info['webprnt_cold_ip'])
	);?>
	</div>
</div>
<?php } ?>
<div class="field_row clearfix">
<?php echo form_label(lang('config_cash_register').':', 't_cash_register',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'t_cash_register',
		'id'=>'t_cash_register',
		'value'=>1,
		'checked'=>($terminal_info['cash_register'])? 1 : 0)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_print_tip_line').':', 't_print_tip_line',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'t_print_tip_line',
		'id'=>'t_print_tip_line',
		'value'=>1,
		'checked'=>($terminal_info['print_tip_line'])? 1 : 0)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_signature_slip_count').':', 't_signature_slip_count',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_signature_slip_count', array("0"=>'0', '1'=>'1', '2'=>'2'), $terminal_info['signature_slip_count']);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_credit_card_receipt_count').':', 't_credit_card_receipt_count',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_credit_card_receipt_count', array("0"=>'0', '1'=>'1', '2'=>'2'), $terminal_info['credit_card_receipt_count']);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('config_non_credit_card_receipt_count').':', 't_non_credit_card_receipt_count',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('t_non_credit_card_receipt_count', array("0"=>'0', '1'=>'1', '2'=>'2'), $terminal_info['non_credit_card_receipt_count']);?>
	</div>
</div>
<?php } ?>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script>
$(document).ready(function() {	
	var submitting = false;
    $('#terminal_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$('#food_and_beverage').val($('#restaurant').attr('checked') ? 1 : 0);// IF $('#restautant') is checked, then we need to mark this item as food_and_beverage
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				post_item_form_submit(response);
                submitting = false;
				if (response.success)
					$.colorbox.close();
				else
					$(form).unmask();
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
		},
		messages:
		{
		}
	});
});
</script>