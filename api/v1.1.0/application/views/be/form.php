<style>
	.left_column, .right_column {
		float:left;
		width:290px;
	}
	.right_column {
		float:right;
		margin-right:10px;
		margin-bottom:10px;
	}
	.reservation_info table td {
		padding:7px 5px;
	}
	.be_reserve_buttonset .ui-button-text-icon-primary .ui-button-text {
		padding: .4em .5em .4em 2em;
	}
	.reservation_info table {
		font-size:14px;
	}
	.right_column .be_form {
		font-size:14px;
	}
	.reserve_button_holder {
		text-align:center;
	}
	#employee_basic_info {
		width:95%;
	}
	#cboxLoadedContent {
		background:#efefef;
	}
	#cboxLoadedContent input[type="text"], #cboxLoadedContent input[type="password"] {
		box-shadow: inset 0px 6px 24px -12px black;
		border-radius: 5px;
		border: 1px solid #B1B1B1;
		padding: 4px;
		line-height: 20px;
		color: #666464;
		font-size: 14px;
		font-weight: lighter;
		font-family: "Quicksand", Helvetica, sans-serif;
		width:200px;
	}
	#cboxLoadedContent input[type='submit'] {
		margin:10px auto 7px;
	}
	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
		font-weight: normal;
		color: #333;
	}
	.detail_label {
		font-weight:bold;
	}
	.details_header {
		font-size:16px;
		font-weight:bold;
	}
	#booking_basic_info {
		border:none;
		background:#ddd;
		padding:10px 10px 15px;
		margin-left:10px;
		margin-right:10px;
		margin-top:25px;
	}
	#cboxTitle {
		border-radius: 0px;
	}
	.be_fine_print {
		margin-left:10px;
	}
	#employee_basic_info.login_info, #register_form fieldset {
		background:transparent;
		border-radius:0px;
		margin-top:11px;
	}
	#forgot_password {
		float:right;
		font-size:12px;
	}
</style>
<div style='width:600px;'>
<div class='left_column'>
	<fieldset id="booking_basic_info" class="ui-state-default login_info">
	    <div class='reservation_info'>
	    	<table>
	    		<tbody>
	    			<tr>
	    				<td colspan=2 class='details_header'>Booking Details</td>
	    			</tr>
	    			<tr>
	    				<td class='detail_label'>Date:</td>
	    				<td><input type="hidden" value ="<?php echo $start?>" id="hidden_start"/>
	                <?php echo $date_string?></td>
	    			</tr>
	    			<tr>
	    				<td class='detail_label'>Time:</td>
	    				<td><?php echo $time?></td>
	    			</tr>
	    			<tr>
	    				<td class='detail_label'>Holes:</td>
	    				<td><input type="hidden" value ="<?php echo $holes?>" id="hidden_holes"/>
	                <?php echo $holes?></td>
	    			</tr>
	    			<tr>
	    				<td class='detail_label'>Players:</td>
	    				<td><div id="player_button_set_<?php echo $holes?>" class="be_reserve_buttonset">
	    					<?php 
			            	$minimum_players = $this->session->userdata('minimum_players');
			            	if ($minimum_players == 1) { ?>
			                <input type="radio" id="player_1_<?php echo $holes?>" value="1" name="player_button_<?php echo $holes?>" checked/><label for="player_1_<?php echo $holes?>">1</label>
						    <?php }
						    if ($spots_available > 1 && $minimum_players < 3) {?>
						    <input type="radio" id="player_2_<?php echo $holes?>" value="2" name="player_button_<?php echo $holes?>"/><label for="player_2_<?php echo $holes?>">2</label>
						    <?php } if ($spots_available > 2 && $minimum_players < 4) {?>
						    <input type="radio" id="player_3_<?php echo $holes?>" value="3" name="player_button_<?php echo $holes?>"/><label for="player_3_<?php echo $holes?>">3</label>
						    <?php } if ($spots_available > 3) {?>
						    <input type="radio" id="player_4_<?php echo $holes?>" value="4" name="player_button_<?php echo $holes?>" checked/><label for="player_4_<?php echo $holes?>">4</label>
						    <?php }?>
						</div></td>
	    			</tr>
	    			<?php if ($booking_carts) { ?>
	    			<tr>
	    				<td class='detail_label'>Carts</td>
	    				<td>
	    					<div id="cart_button_set_<?php echo $holes?>" class="be_reserve_buttonset">
							    <input type="radio" id="cart_y_<?php echo $holes?>" value="cart" name="cart_button_<?php echo $holes?>" checked/><label for="cart_y_<?php echo $holes?>">Yes</label>
							    <input type="radio" id="cart_n_<?php echo $holes?>" value="no_cart" name="cart_button_<?php echo $holes?>"/><label for="cart_n_<?php echo $holes?>">No</label>
							</div>
	    				</td>
	    			</tr>
	    			<?php } ?>
	    			<tr>
	    				<td class='detail_label'>Green Fee:</td>
	    				<td><div class="price_box">$<?= $booking_carts ? $w_cart : $wo_cart?></div></td>
	    			</tr>
	    			<tr>
	    				<td class='detail_label'>Total:</td>
	    				<td><div class="price_box">$<span class="total_price"><?=number_format(($booking_carts ? $w_cart : $wo_cart)*$spots,2)?></span>*</div></td>
	    			</tr>
	    		</tbody>
	    	</table>
	</fieldset>
	
	<div class="clear"></div>
	<div class="be_fine_print">
	    *<?= $booking_carts ? '' : 'Price does not include cart fees. ';?>Green fees will be collected at the course. <?php if (!$logged_in) { echo 'Please register/login to complete your reservation.';} ?>
	</div>	
	<div class='reserve_button_holder'>
		<?php if ($logged_in) {
	    $js = 'onClick="reservations.book_tee_time();" class="login_button"';
	    echo form_button('login_button','Reserve', $js); 
	    
	    }?>
	</div>
</div>
<div class='right_column'>
	<div id="ajax_message" style="display:none">
	</div>
		<?php if (validation_errors()) {?>
			<div id="welcome_message" class="top_message_error">
				<?php echo validation_errors(); ?>
			</div>
		<?php } ?>
	<?php if (!$logged_in) {?>
	<!--h2 class="be_header">Login</h2-->
	<?php echo form_open('be/login',array('id'=>'login_form')) ?>
	<fieldset id="employee_basic_info" class="ui-corner-all ui-state-default login_info">
	<legend><?php echo lang("login_user_login"); ?></legend>
	<div id="container">
	    <table class="be_form login_form">
	        <tr id="form_field_username">	
	            <!--td class="form_field_label"><?php echo lang('common_email'); ?>: </td-->
	            <td class="form_field">
	            <?php echo form_input(array(
	            'name'=>'username', 
	            'id'=>'username', 
	            'value'=>'',
	            'placeholder'=>lang('common_email').'/'.lang('common_username'),
	            'size'=>'20')); ?>
	            </td>
	        </tr>
		<tr id="form_field_password">	
	            <!--td class="form_field_label required"><?php echo lang('login_password'); ?>: </td-->
	            <td class="form_field">
	            <?php echo form_password(array(
	            'name'=>'password', 
	            'id'=>'login_password', 
	            'value'=>'',
	            'placeholder'=>lang('login_password'),
	            'size'=>'20')); ?>
	            </td>
	        </tr>
	        <tr id="form_field_submit">	
	            <td id="submit_button" colspan="2">
	                <?php echo form_submit('login_button',lang('login_login').' & '.lang('login_reserve')); ?>
	            </td>
	        </tr>
	    	</table>
	</div>
	</fieldset>
	<?php echo form_close(); ?>
	<?php echo anchor('customer_login/reset_password', lang('login_forgot_password'), array('id'=>'forgot_password', 'target'=>'_blank')); ?> 
	<!--h2 class="be_header">Register</h2-->
	
			
	<?php echo form_open('be/register',array('id'=>'register_form')) ?>
	<fieldset id="employee_basic_info" class="ui-corner-all ui-state-default register_info">
	<legend><?php echo lang("login_new_user"); ?></legend>
	<div id="container">
		<table class="be_form register_form">
		
			<tr id="form_field_first_name">	
				<!--td class="form_field_label required"><?php echo lang('common_first_name'); ?>: </td-->
				<td class="form_field">
				<?php echo form_input(array(
				'name'=>'first_name', 
				'id'=>'first_name', 
				'value'=>'',
				'placeholder'=>lang('common_first_name'),
				'size'=>'20')); ?>
				</td>
			</tr>
		
			<tr id="form_field_last_name">	
				<!--td class="form_field_label required"><?php echo lang('common_last_name'); ?>: </td-->
				<td class="form_field">
				<?php echo form_input(array(
				'name'=>'last_name', 
				'id'=>'last_name', 
				'value'=>'',
				'placeholder'=>lang('common_last_name'),
				'size'=>'20')); ?>
				</td>
			</tr>
		
			
			<tr id="form_field_phone_number">	
				<!--td class="form_field_label"><?php echo lang('common_phone'); ?>: </td-->
				<td class="form_field">
				<?php echo form_input(array(
				'name'=>'phone_number', 
				'id'=>'phone_number', 
				'value'=>'',
				'placeholder'=>lang('common_phone'),
				'size'=>'20')); ?>
				</td>
			</tr>
	                <tr><td colspan="2"><hr/></td></tr>
			<tr id="form_field_email">	
				<!--td class="form_field_label"><?php echo lang('common_email'); ?>: </td-->
				<td class="form_field">
				<?php echo form_input(array(
				'name'=>'email', 
				'id'=>'email', 
				'value'=>'',
				'placeholder'=>lang('common_email'),
				'size'=>'20')); ?>
				</td>
			</tr>
		
			<tr id="form_field_password">	
				<!--td class="form_field_label required"><?php echo lang('login_password'); ?>: </td-->
				<td class="form_field">
				<?php echo form_password(array(
				'name'=>'password',
                'id'=>'password',
				'value'=>'',
				'placeholder'=>lang('login_password'),
				'size'=>'20')); ?>
				</td>
			</tr>
			
			<tr id="form_field_password_confirm">	
				<!--td class="form_field_label required"><?php echo lang('login_password_confirm'); ?>: </td-->
				<td class="form_field">
				<?php echo form_password(array(
				'name'=>'password_confirm', 
				'id'=>'password_confirm', 
				'value'=>'',
				'placeholder'=>lang('login_password_confirm'),
				'size'=>'20')); ?>
				</td>
			</tr>
			
			<tr id="form_field_submit">	
				<td id="submit_button" colspan="2">
					<?php echo form_submit('login_button',lang('login_register').' & '.lang('login_reserve')); ?>
				</td>
			</tr>
		</table>
	</div>
	</fieldset>
	<?php echo form_close(); ?>
	<?php } ?>
</div>
</div>
<!--[if IE]>
	<script type="text/javascript">
		add_placeholder('username','<?=lang('common_email').'/'.lang('common_username')?>');
		add_placeholder('login_password','<?=lang('login_password')?>');
		add_placeholder('first_name','<?=lang('common_first_name')?>');
		add_placeholder('last_name','<?=lang('common_last_name')?>');
		add_placeholder('phone_number','<?=lang('common_phone')?>');
		add_placeholder('email','<?=lang('common_email')?>');
		add_placeholder('password','<?=lang('login_password')?>');
		add_placeholder('password_confirm','<?=lang('login_password_confirm')?>');
	</script>
<![endif]-->
<script type='text/javascript'>
var reservations = {
    update_total_price:function() {
        var total_price = '';
        var price_array = {'cart':'<?php echo $w_cart ?>','no_cart':'<?php echo $wo_cart?>'};
        var players = $('input:radio[name=player_button_<?php echo $holes?>]:checked').val();
        var cart = $('input:radio[name=cart_button_<?php echo $holes?>]:checked').val();
        var index = 'no_cart'
        if (!isNaN(players) && cart != undefined)
            index = cart;
        total_price = players * price_array[index];
        var price_box = $('.price_box');
        $(price_box[0]).html('$'+(price_array[index]*1).toFixed(2));
        $('.total_price').html(total_price.toFixed(2));
    },
    book_tee_time:function() {
//        var title = ''; 
        var start = '<?php echo $start?>'; 
        var holes='<?php echo $holes?>';
        var players = $('input:radio[name=player_button_<?php echo $holes?>]:checked').val();
        var carts = <?php if ($booking_carts) { ?> $('input:radio[name=cart_button_<?php echo $holes?>]:checked').val();<?php } else { ?> 0 <?php } ?>
//        var end = '';
//        var status=''; 
//        var details=''; 
//        var email = ''; 
//        var phone = '';
//        var person_id = '';
//        var booker_id = '';
        $.ajax({
           type: "POST",
           url: "<?php echo site_url('be/book_tee_time'); ?>",
           data: "start="+start+"&holes="+holes+"&players="+players+"&carts="+carts,
           success: function(response){
               if (response.success) {
                   var spots_available = $('#teetime_'+start.slice(6)+'_spots').html();
                   var spots_remaining = spots_available - players;
                    trace('#teetime_'+start.slice(6));
                    trace('sr '+spots_remaining);
                    if (spots_remaining == 0)
                        $('#teetime_'+start.slice(6)).remove();
                    else 
                        $('#teetime_'+start.slice(6)+'_spots').html(spots_remaining);
                    
                    reservations.show_booked_page(response.teetime_id);
               }
               else
               {
               		alert(response.message);
               		$('#cboxContent').unmask();
               		$('#teetime_'+start.slice(6)).remove();
               		$.colorbox.close();
               }
           },
           dataType:'json'
        });

    },
    show_thank_you_message:function(teetime_id) {
    
        window.location = '<?php echo site_url('be/finish'); ?>/'+teetime_id;
        //$('#thank_you_box').html("<div id='thank_you_message'>Your tee time has been booked. Thank you.</div>");
        //$("#thank_you_box").show();
        //this.remove_thank_you_message();
    },
    show_booked_page:function(teetime_id) {
    
        window.location = '<?php echo site_url('be/booked'); ?>/'+teetime_id;
    },
    remove_thank_you_message:function(){
        $("#thank_you_box").fadeOut(2000);
    },
    show_error:function (message) {
        trace(message);
        $('#ajax_message').html(message).show();
        
    }
}
//validation and submit handling
$(document).ready(function()
{
	<?php if ($logged_in) {?>
		$.colorbox.resize({width:315});	
	<?php } ?>
    $('#player_<?php echo $spots?>_<?php echo $holes?>').click();
    $('input:radio[name=player_button_<?php echo $holes?>]').click(function(){
    	reservations.update_total_price();
    })
    $('input:radio[name=cart_button_<?php echo $holes?>]').click(function(){
    	reservations.update_total_price();
    })
    $('#cart_y_<?php echo $holes?>').button({});
    $('#cart_n_<?php echo $holes?>').button({});
    $('#player_1_<?php echo $holes?>').button();
    $('#player_2_<?php echo $holes?>').button();
    $('#player_3_<?php echo $holes?>').button();
    $('#player_4_<?php echo $holes?>').button();
    //$('input:[name=phone_number]').mask("999-999-9999");
    $('#cart_button_set_<?php echo $holes?>').buttonset();
    $('#player_button_set_<?php echo $holes?>').buttonset();
    $('input:[name=login_button]').button();
    $('button:[name=login_button]').button();
    $('#login_form').validate({
		submitHandler:function(form)
		{
            $('#cboxContent').mask("<?php echo lang('common_wait'); ?>");
            $(form).ajaxSubmit({
	            success:function(response)
	            {
	                if(response.success) {
	                    post_person_form_submit(response);
	                    reservations.book_tee_time();
	                    $.colorbox.close();
	                }
	                else {
	                    reservations.show_error(response.message);
	                	$('#cboxContent').unmask();
	                }
	            },
	            dataType:'json'
            });
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			username:{
                required:true
            },
			password:'required'
   		},
		messages: 
		{
     		username: {
                required:"<?php echo lang('employees_email_required'); ?>"
            },
			password:"<?php echo lang('employees_password_required'); ?>"
        }
	});
    trace("assigned login_form validation");
    $('#register_form').validate({
		submitHandler:function(form)
		{
			$('#cboxContent').mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
		            if (response.success) {
		                post_person_form_submit(response);
		                reservations.book_tee_time();
		                $.colorbox.close();
		            }
		            else {
		                reservations.show_error(response.message);
		                $('#cboxContent').unmask();
		            }
	            },
				dataType:'json'
			});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			first_name: "required",
			last_name: "required",
            phone_number:"required",
			username:
			{
				required:true,
				minlength: 5
			},
			
			password:
			{
				required:true,
				minlength: 8
			},	
			password_confirm:
			{
                required:true,
 				equalTo: "#password"
			},
            email: {
                required:true,
                email:true
            }
   		},
		messages: 
		{
     		first_name: "<?php echo lang('common_first_name_required'); ?>",
     		last_name: "<?php echo lang('common_last_name_required'); ?>",
     		username:
     		{
     			required: "<?php echo lang('employees_username_required'); ?>",
     			minlength: "<?php echo lang('employees_username_minlength'); ?>"
     		},
     		
			password:
			{
				required:"<?php echo lang('employees_password_required'); ?>",
				minlength: "<?php echo lang('employees_password_minlength'); ?>"
			},
			confirm_password:
			{
				required:"<?php echo lang('employees_password_required'); ?>",
				equalTo: "<?php echo lang('employees_password_must_match'); ?>"
     		},
     		email: {
                required:"<?php echo lang('employees_email_required'); ?>",
                email: "<?php echo lang('common_email_invalid_format'); ?>"
            }
		}
	});
        
        //reservations.update_total_price();
});

</script>