var CartListView = Backbone.View.extend({
	tagName: "tbody",
	initialize: function() {
		this.listenTo(this.collection, "add", this.render);
		this.listenTo(this.collection, "remove", this.render);
		this.listenTo(this.collection, "reset", this.render);
	},

	render: function() {
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('');

		_.each(this.collection.models, this.addItem, this);
		return this;
	},

	addItem: function(item){
		this.$el.append( new CartListItemView({model:item}).render().el );
	},

	close: function() {
		this.$el.unbind();
		this.$el.empty();
	}
});