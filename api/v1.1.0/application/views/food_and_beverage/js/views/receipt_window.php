var ReceiptWindowView = Backbone.View.extend({
	tagName: "div",
	id: "split_payments",
	template: _.template( $('#template_receipts').html() ),

	initialize: function() {
		this.listenTo(this.collection, "add", this.render);
		this.listenTo(this.collection, "remove", this.render);
		this.listenTo(this.collection, "reset", this.render);
	},

	events: {
		"click a.new-receipt": "newReceipt",
		"click a.remove-items": "removeItems",
		"click div.mode a": "changeMode"
	},

	render: function() {
		App.receipts.unSelectItems();
		App.cart.unSelectItems();
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html(this.template());
		_.each(this.collection.models, this.addReceipt, this);

		return this;
	},

	addReceipt: function(receipt){
		this.$el.find('div.scroll-content').append( new ReceiptView({model: receipt}).render().el );
	},

	newReceipt: function(event){
		this.collection.create({"date_paid":null});
		return this;
	},

	removeItems: function(event){
		// Retrieve selected items across all receipts
		var selectedItems = this.collection.getSelectedItems();
		if(!selectedItems){
			return false;
		}
		var splitItems = App.receipts.getSplitItems();

		// Loop through selected items and delete them
		_.each(selectedItems, function(item){
			var line = item.get('line');

			// Only allow delete if the item exists on other receipts
			if(splitItems[line] && splitItems[line] > 1){
				item.destroy();
			}
		});

		App.receipts.unSelectItems();
		App.cart.unSelectItems();

		return true;
	},

	changeMode: function(event){
		var btn = $(event.currentTarget);
		btn.siblings().removeClass('selected');
		btn.addClass('selected');
		btn.parent().data('mode', btn.data('value'));
	},

	close: function() {
		this.$el.unbind();
		this.$el.empty();
	}
});
