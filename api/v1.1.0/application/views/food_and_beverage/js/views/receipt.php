var ReceiptView = Backbone.View.extend({
	tagName: "div",
	className: "receipt-container",
	template: _.template( $('#template_receipt').html() ),

	events: {
		"click a.pay": "payReceipt",
		"click a.print": "printReceipt",
		"click a.delete-receipt": "deleteReceipt",
		"click div.receipt": "moveItems"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model.get('items'), "add remove reset", this.render);
	},

	render: function() {
		this.$el.html( this.template(this.model.attributes) );
		this.$el.find('div.title').after( new ReceiptItemListView({collection:this.model.get('items')}).render().el );
		this.renderStatus();
		return this;
	},

	renderStatus: function(){
		if(this.model.get('items').length == 0){
			this.$el.find('a.pay, a.print').addClass('disabled');

		}else if(this.model.get('status') != 'complete'){
			this.$el.find('a.pay, a.print').removeClass('disabled');

		}else{
			this.$el.find('a.print').removeClass('disabled');
		}
	},

	payReceipt: function(event){
		if(this.model.get('items').length == 0 || this.model.get('status') == 'complete'){
			return false;
		}
		var receiptModel = this.model;

		var payments = this.model.get('payments');
		var paymentWindow = new PaymentWindowView({collection: payments, model: receiptModel});

		$.colorbox2({
			title: 'Add Payment',
			html: paymentWindow.render().el,
			width: 600
		});

		return false;
	},

	printReceipt: function(event){
		alert('TODO: Print receipt');
		return false;
	},

	deleteReceipt: function(event){
		if(this.model.get('receipt_id') == 1){
			return false;
		}

		var splitItems = this.model.collection.getSplitItems();
		var items = this.model.get('items').models;
		var deleteReceipt = true;

		// Make sure items in receipt to be deleted exist on other receipts
		_.each(items, function(item){
			var line = item.get('line');
			if(splitItems[line] <= 1){
				deleteReceipt = false;
			}
		});

		if(deleteReceipt){
			this.model.destroy();
		}
		return false;
	},

	moveItems: function(event){
		// If this receipt has already been paid, do nothing
		if(this.model.get('status') == 'complete'){
			App.receipts.unSelectItems();
			return false;
		}

		var selectedItems = App.receipts.getSelectedItems();
		var items = [];
		var mode = $('#split_payments').find('div.btn-group').data('mode');

		if(selectedItems.length == 0){
			return false;
		}
		var targetReceiptItems = this.model.get('items');

		// Add selected items to target receipt.
		_.each(selectedItems, function(selectedItem){

			// Make sure items aren't being moved back onto same receipt
			if(selectedItem.collection != targetReceiptItems){

				var itemCopy = new ReceiptItem(selectedItem.attributes);
				targetReceiptItems.create(itemCopy, {complete: function(response){
					// If moving item, delete it from other receipt. Delete
					// call is made when move call has completed to prevent errors
					if(mode == 'move'){
						selectedItem.destroy();
					}

				}, isNew: true});

				// TODO: Hide the selected item so there is no delay
				// waiting for requests to complete
				if(mode == 'move'){

				}
			}
		});

		App.receipts.unSelectItems();
	}
});
