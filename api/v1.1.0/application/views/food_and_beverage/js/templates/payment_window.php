<style>
html[xmlns] div.hidden {
	display:none;
}

ul.payments > li.payment {
	display: block;
	overflow: hidden;
	font-size: 16px;
}

ul.payments > li.payment span, ul.payments > li.payment a  {
	padding: 5px;
	float: left;
	display: block;
}

ul.payments > li.payment span.amount {
	float: right;
	width: 60px;
	text-align: right;
}

#make_payment {
	height: 300px;
}

#make_payment div.left {
	width: 290px;
	overflow: hidden;
	float: left;
	margin: 0px 0px 5px 5px;
}

#make_payment div.left.max {
	float: none;
	display: block;
	width: auto;
}

#payment_credit_card {
	padding: 10px;
}
</style>
<script type="text/html" id="template_payment_window">
<ul id="error_message_box"></ul>
<div id="make_payment" style="width: auto; display: block;">
	<div class="left">
		<!-- Payment Buttons -->
		<div id="payment_buttons" class="payment-types">
			<span id='use_credit_card' class='fnb_button'>Credit Card</span>
			<span id='use_cash' class='fnb_button'>Cash</span>
			<span id='use_check' class='fnb_button'>Check</span>
			<span id='use_gift_card' class='fnb_button'>Gift Card</span>
			<!-- <span id='use_punch_card' class='fnb_button'>Punch Card</span>
			<span id='use_account' class='fnb_button payment_button_wide disabled'>Customer Credit Credit</span>
			<span id='use_member_account' class='fnb_button payment_button_wide disabled'>Member Balance Credit</span>
			<span id='use_loyalty' class='fnb_button payment_button_wide disabled'>Loyalty Points</span> -->
		</div>

		<!-- Giftcard window -->
		<div id='payment_gift_card' style='display: none'>
			<label for="payment_giftcard_number" class="required">Giftcard #:</label>
			<div class='form_field'>
				<input type="text" name="giftcard_number" value="" id="payment_giftcard_number" size="20" autocomplete="off" maxlength="16"  />
			</div>
			<div class='clear'>
				<a class="fnb_button show_payment_buttons" href="#">Back</a>
				<a id="pay_gift_card" class="fnb_button float_right" href="#">Pay</a>
			</div>
		</div>

		<!-- Punch card window -->
		<div id='payment_punch_card' style="display: none">
			<label for="punch_card_number" class="required">Punch Card #:</label>
			<div class='form_field'>
				<input type="text" name="punch_card_number" value="" id="punch_card_number" size="20" autocomplete="off" maxlength="16"  />
			</div>
			<div class='clear'>
				<a class="fnb_button show_payment_buttons" href="#">Back</a>
				<input type="submit" name="submit" value="Pay" id="submit2" class="submit_button float_right"  />
			</div>
		</div>

		<!-- Credit card window -->
		<div id='payment_credit_card' style="display: none"></div>
	</div>
	<div class="payment-amount" style="width: 240px; float: right;">
		<div style="width: auto; display: block; overflow: hidden;">
			<label for="amount_tendered" class="required" style="float: left; width: 130px;">Amount Tendered:</label>
			<div class='form_field'>
				<input type="text" name="amount_tendered" value="<%=accounting.formatMoney(amount_due, '')%>" id="payment_amount_tendered" autocomplete="off" style="text-align: right; width: 75px; float: right;"  />
			</div>
		</div>
		<div style="display: block; overflow: hidden; width: auto;">
			<ul class="payments"></ul>
			<div class='due_amount' style="padding: 5px;" id='due_amount'><%=accounting.formatMoney(amount_due) %></div>
			<div class="float_left due_amount_label">Amount Due:</div>
		</div>
	</div>
</div>
<input type="hidden" id="payment_type" name="payment_type" value="Cash" />
<input type="hidden" id="payment_gc_number" name="payment_gc_number" value="" />
<div class='clear'></div>
</script>
