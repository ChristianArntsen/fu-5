<script type="text/html" id="template_cart_totals">
<table>
	<tbody>
		<tr>
			<td>
				<div class="left" id="items_in_basket"><%=num_items%> Items</div>
			</td>
			<td>
				<div id="taxes_holder">
					<div>
						<div class="right register_taxes"><%=accounting.formatMoney(subtotal)%></div>
						<div class="left register_taxes">Subtotal</div>
						<div class="clear"></div>
					</div>
				</div>
				<div id="taxable_box">
					<div class="right"><%=accounting.formatMoney(tax)%></div>
					<div class="left">Tax</div>
					<div class="clear"></div>
				</div>
				<div id="register_total">
					<div class="right" id="basket_final_total" style="width: 50px;">
						<h3 style="font-size: 18px; display: inline"><%=accounting.formatMoney(total)%></h3>
					</div>
					<div class="left"><h3 style="font-size: 18px;">Total</h3></div>
				</div>
			</td>
		</tr>
	</tbody>
</table>
</script>