<table style='font-family:Arial, Helvetica, sans-serif; border-collapse:collapse;' border=0 cellspacing=0>
	<tbody>
		<tr>
			<td colspan=<?php echo count($headers);?>>
				<div style='text-align:center; font-size:20px; padding:15px;'><?php echo $name.': '.date('F j, Y');?></div>
			</td>
		</tr>
		<tr>
		<?php foreach($headers as $header) { ?>
			<th style='background:rgb(28,69,135); color:white; text-align:center;'>
				<div style='padding:10px; 5px; font-size:14px;'><?=$header?></div>
			</th>
		<?php } ?>
		</tr>
		<?php foreach($billing_list as $bindex => $billing) { ?>
			<tr style='<?=(($bindex%2)==1?'background-color:#ededed;':'')?>'>
				<?php foreach($billing as $index => $field) { ?>
					<td>
						<div style='padding:5px; font-size:12px; <?=($field=='No'?'color:red;':'')?> <?=($index+1 > count($headers)/2?'text-align:center;':'')?>'><?=$field?></div>
					</td>
				<?php } ?>
			</tr>
		<?php } ?>
		<tr>
			<td colspan=<?php echo count($headers);?>>
				<div style='height:30px; width:100%; display:block'></div>
			</td>
		</tr>
		<tr>
			<td colspan=<?php echo count($headers);?> style='font-size:10px;'>
				<?=lang('billings_charge_successful_note');?>
			</td>
		</tr>
		<tr>
			<td colspan=<?php echo count($headers);?> style='font-size:10px;'>
				<?=lang('billings_email_sent_note');?>
			</td>
		</tr>
	</tbody>
</table>