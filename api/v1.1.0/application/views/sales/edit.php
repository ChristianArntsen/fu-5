<?php 
if ($type != 'popup') {
       $this->load->view("partial/header"); ?>
       <table id="title_bar">
               <tr>
                       <td id="title_icon">
                               <img src='<?php echo base_url()?>images/menubar/sales.png' alt='title icon' />
                       </td>
                       <td id="title"><?php echo lang('sales_register')." - ".lang('sales_edit_sale'); ?> POS <?php echo $sale_info['sale_id']; ?></td>
               </tr>
       </table>
       <br />
<?php } else { ?>
<style>
       #edit_sale_wrapper {
               width:100%;
       }
</style>
<?php } ?>
<div id="edit_sale_wrapper">
	<?php echo form_hidden('edit_sale_id', $sale_info['sale_id']);?>
	<fieldset>
	<?php echo form_open("sales/save/".$sale_info['sale_id'],array('id'=>'sales_edit_form')); ?>
	<ul id="error_message_box"></ul>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_receipt').':', 'sales_receipt'); ?>
		<div class='form_field'>
			<?php echo anchor('sales/receipt/'.$sale_info['sale_id'], 'POS '.$sale_info['sale_id'], array('target' => '_blank'));?>
		</div>
	</div>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_date').':', 'date'); ?>
		<div class='form_field'>
			<?php echo form_input(array('name'=>'date','value'=>date(get_date_format().' '.get_time_format(), strtotime($sale_info['sale_time'])), 'id'=>'date'));?>
		</div>
	</div>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_customer').':', 'customer'); ?>
		<div class='form_field'>
			<?php //echo form_dropdown('customer_id', $customers, $sale_info['customer_id'], 'id="customer_id"');?>
			<?php echo form_input(array('name'=>'customer_name','id'=>'customer_name','value'=>$customer_name)); ?>
			<?php echo form_hidden('person_id', $sale_info['customer_id']); ?>
			<?php if ($sale_info['customer_id']) { ?>
				<?php echo anchor('sales/email_receipt/'.$sale_info['sale_id'], lang('sales_email_receipt'), array('id' => 'email_receipt'));?>
			<?php }?>
		</div>
	</div>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_employee').':', 'employee'); ?>
		<div class='form_field'>
			<?php echo form_dropdown('sale_employee_id', $employees, $sale_info['employee_id'], 'id="sale_employee_id"');?>
		</div>
	</div>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_comment').':', 'comment'); ?>
		<div class='form_field'>
			<?php echo form_textarea(array('name'=>'comment','value'=>$sale_info['comment'],'rows'=>'4','cols'=>'23', 'id'=>'comment'));?>
		</div>
	</div>
	<?php if ($invoice_id) { ?>
	<div class='field_row clearfix'>
	<?php 
		//print_r($credit_card_payments);
		echo form_label('', 'comment');
		echo "<a href='index.php/sales/tip_window/{$sale_info['sale_id']}/{$invoice_id}/width~500' title='Add Tip - POS {$sale_info['sale_id']}'}' class='colbox2'>Add Tip</a>";
		/*echo form_label('Credit Card Payment:', 'comment');
		foreach($credit_card_payments as $cc_payment)
		{
			$cardholder_name = '';
			echo " <div class='form_field'>".$cc_payment['payment_type'].' - $'.$cc_payment['amount'].$cardholder_name." (<a title='Refund to CC' class='colbox' href='index.php/sales/confirm_refund/{$cc_payment['invoice']}'>Refund to CC</a>)</div>";
		} */
	?>
	</div>
	<?php } ?>
	<div class='field_row clearfix'>
	<?php 
		//print_r($credit_card_payments);
		echo form_label('', 'comment');
		echo "<a href='javascript:void(0)' onclick='sales.load_return(\"{$sale_info['sale_id']}\", \"{$type}\", \"{$sales_page}\")'>Issue Return</a>";
		/*echo form_label('Credit Card Payment:', 'comment');
		foreach($credit_card_payments as $cc_payment)
		{
			$cardholder_name = '';
			echo " <div class='form_field'>".$cc_payment['payment_type'].' - $'.$cc_payment['amount'].$cardholder_name." (<a title='Refund to CC' class='colbox' href='index.php/sales/confirm_refund/{$cc_payment['invoice']}'>Refund to CC</a>)</div>";
		} */
	?>
	</div>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_save'),
		'class'=>'submit_button float_left')
	);
	?>
	</form>

	<?php if ($sale_info['deleted'])
	{
	?>
	<?php echo form_open("sales/undelete/".$sale_info['sale_id'],array('id'=>'sales_undelete_form')); ?>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit',
			'value'=>lang('sales_undelete_entire_sale'),
			'class'=>'submit_button float_right')
		);
		?>
	</form>
	<?php
	}
	else
	{
	?>
	<?php echo form_open("sales/delete/".$sale_info['sale_id'].'/'.$type,array('id'=>'sales_delete_form')); ?>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit',
			'value'=>lang('sales_delete_entire_sale'),
			'class'=>'delete_button float_right')
		);
		?>
	</form>
	<?php
	}
	?>
</fieldset>

</div>
<script>
	console.log('setting up validate');
	$(document).ready(function(){
		$('#customer_name').focus().select();
		$('#customer_name' ).autocomplete({
			source: '<?php echo site_url('customers/customer_search/last_name'); ?>',
			delay: 10,
			autoFocus: false,
			minLength: 0,
			select: function(event, ui) {
				event.preventDefault();
				$('#customer_name').val(ui.item.label);
				$('#person_id').val(ui.item.value);
			},
			focus: function(event, ui) {
				event.preventDefault();
				//$('#teetime_title').val(ui.item.label);
			}
		});
		$('.colbox').colorbox({width:550});
		$('.colbox2').colorbox2();
		$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
		$("#email_receipt").click(function()
		{
			$.get($(this).attr('href'), function()
			{
				alert("<?php echo lang('sales_receipt_sent'); ?>")
			});
			
			return false;
		});
		//$('#date').datePicker({startDate: '<?php echo get_js_start_of_time_date(); ?>'});
	    $('#date').datetimepicker();        
	       
	    $("#sales_undelete_form").submit(function()
		{
			if (!confirm('<?php echo lang("sales_undelete_confirmation"); ?>'))
			{
				return false;
			}
		});
	    $('#sales_delete_form').validate({
            submitHandler:function(form)
            {
                if (confirm("<?php echo lang("sales_delete_confirmation"); ?>"))
                {
                    $(form).ajaxSubmit({
                        success:function(response)
                        {
                        	if(response.success)
                            {
                                set_feedback('Sale succesfully deleted.','success_message',false);
                                var sale_id = $('#edit_sale_id').val();
                                if (typeof delete_recent_transaction == 'function')
	                                delete_recent_transaction(sale_id);
		                        if (typeof reports != 'undefined')
	                            	reports.generate();
						        $.colorbox.close();
                            }
                            else
                            {
                                set_feedback('Unable to delete sale.','error_message',true);    
                            }
                        },
                        dataType:'json'
                    });
                }
            },
            errorLabelContainer: "#error_message_box",
            wrapper: "li",
            rules: 
            {
            },
            messages: 
            {
            }
        });
	   $('#sales_edit_form').validate({
			submitHandler:function(form)
			{
				$(form).ajaxSubmit({
				success:function(response)
				{
					if(response.success)
					{
						set_feedback(response.message,'success_message',false);
						$.colorbox.close();
	                    reports.generate();
					}
					else
					{
						set_feedback(response.message,'error_message',true);	
						
					}
				},
				dataType:'json'
			});
	
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules: 
			{
	   		},
			messages: 
			{
			}
		});
		console.log('inside doc ready');
	});
</script>
<?php 
if ($type != 'popup')
{
?>
 <div id="feedback_bar"></div>
<?php  $this->load->view("partial/footer"); 
}
?>
