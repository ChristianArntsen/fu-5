<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * table headers
 */
$lang['promotions_name']  = "lorem ipsum";
$lang['promotions_number_available']  = "lorem ipsum";
$lang['promotions_amount_type']  = "lorem ipsum";
$lang['promotions_amount']  = "lorem ipsum";
$lang['promotions_rules']  = "lorem ipsum";
$lang['promotions_no_data_to_display']  = "lorem ipsum";

$lang['promotions_new']  = "lorem ipsum";
$lang['promotions_update']  = "lorem ipsum";
$lang['promotions_confirm_delete'] = "lorem ipsum";
$lang['promotions_none_selected'] = "lorem ipsum";
$lang['promotions_error_adding_updating'] = "lorem ipsum";
$lang['promotions_successful_adding'] = "lorem ipsum";
$lang['promotions_successful_updating'] = "lorem ipsum";
$lang['promotions_successful_deleted'] = "lorem ipsum";
$lang['promotions_one_or_multiple'] = "lorem ipsum";
$lang['promotions_cannot_be_deleted'] = "lorem ipsum";

/*
 * Form
 */
$lang['promotions_basic_information']  = "lorem ipsum";
$lang['promotions_name']  = "lorem ipsum";
$lang['promotions_amount']  = "lorem ipsum";
$lang['promotions_expiration_date']  = "lorem ipsum";
$lang['promotions_valid_for']  = "lorem ipsum";
$lang['promotions_valid_on']  = "lorem ipsum";
$lang['promotions_valid_between']  = "lorem ipsum";
$lang['promotions_limit']  = "lorem ipsum";
$lang['promotions_buy']  = "lorem ipsum";
$lang['promotions_get']  = "lorem ipsum";
$lang['promotions_min_purchase']  = "lorem ipsum";
$lang['promotions_details']  = "lorem ipsum";
/**
 * required fields
 */
$lang['promotions_name_required']  = "lorem ipsum";
$lang['promotions_amount_required']  = "lorem ipsum";
$lang['promotions_expiration_date_required']  = "lorem ipsum";
$lang['promotions_valid_for_required']  = "lorem ipsum";