<?php
namespace foreup\auth;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;

class json_web_token
{
	private $secretKey = '';

	protected $signer = '';
	/** @var  Token */
	protected $token;
	protected $privateClaims = [];

	protected $duration = '';
	protected $issuer = '';
	protected $audience = '';

	protected $cookie_duration = '';
	protected $cookie_path = '';
	protected $cookie_domain = '';
	protected $cookie_secure = true;

	private  $expiresAt = '';

	public function __construct(
		$secret_key,
		$issuer,
		$audience,
		$duration,
		$cookie_duration,
		$cookie_path,
		$cookie_domain,
		$cookie_secure
	)
	{
		$this->signer = new Sha512();
		$this->secretKey = $secret_key;
		$this->issuer = $issuer;
		$this->audience = $audience;
		$this->duration = (int)$duration;
		$this->cookie_duration = (int)$cookie_duration;
		$this->cookie_path = $cookie_path;
		$this->cookie_domain = $cookie_domain;
		if($cookie_secure === false){
			$this->cookie_secure = false;
		} else {
			$this->cookie_secure = true;
		}
		
	}

	public function addPrivateClaim($key, $value){
		$this->privateClaims[$key] = $value;
	}

	public function createToken(){
		$this->expiresAt = time() + (int)$this->duration;

		$currentClaims = [];
		if(!empty($this->token)){
			$currentClaims = $this->token->getClaims();
		}
		// Set Registered Claims
		$this->token = (new Builder())
			->setIssuer($this->issuer) // set iss claim
			->setAudience($this->audience) // set aud claim
			->setIssuedAt(time()) // set the iat claim
			->setExpiration($this->expiresAt); // set the exp claim

		foreach($currentClaims as $key => $claim){
			$this->token->set($claim->getName(),$claim->getValue());
		}

		// Set Private Claims
		foreach($this->privateClaims as $privateClaimKey => $privateClaimValue){
			$this->token->set($privateClaimKey, $privateClaimValue);
		}

		// Sign the token
		$this->token->sign($this->signer, $this->secretKey);

		// Get the actual token object from builder
		$this->token = $this->token->getToken();

		return true;
	}

	public function setToken($tokenString){
		$this->token = (new Parser())->parse((string)$tokenString);
	}

	public function getClaims(){
		return $this->token->getClaims();
	}

	public function getClaim($claim){
		return $this->token->getClaim($claim);
	}

	public function getToken(){
		if (!$this->token instanceof Token) return false;
		return (string)$this->token;
	}

	public function setCookie(){
		if(!headers_sent()){

			return setcookie(
				'token',
				$this->getToken(),
				time() + (int)$this->cookie_duration,
				$this->cookie_path,
				$this->cookie_domain,
				(bool) $this->cookie_secure
			);

		} else {
			return false;
		}
	}

	/**
	 * @return string
	 */
	public function getAudience()
	{
		return $this->audience;
	}

	/**
	 * @param string $audience
	 */
	public function setAudience(string $audience)
	{
		$this->audience = $audience;
	}



	public function validate(){
		if (!$this->token instanceof Token) return false;

		$data = new ValidationData();
		$data->setIssuer($this->issuer);
		$data->setAudience($this->audience);

		return ($this->token->verify($this->signer, $this->secretKey) && $this->token->validate($data));
	}

}