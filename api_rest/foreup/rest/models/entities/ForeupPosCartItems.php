<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupPosCartItems
 *
 * @ORM\Table(name="foreup_pos_cart_items", indexes={@ORM\Index(name="line", columns={"line"}), @ORM\Index(name="FK_foreup_pos_cart_items_foreup_items", columns={"item_id"}), @ORM\Index(name="IDX_3BADBA9A1AD5CDBF", columns={"cart_id"})})
 * @ORM\Entity
 */
class ForeupPosCartItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="line", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $line;

    /**
     * @var string
     *
     * @ORM\Column(name="quantity", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_price", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $unitPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="discount_percent", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $discountPercent;

    /**
     * @var string
     *
     * @ORM\Column(name="item_type", type="string", length=16, nullable=false)
     */
    private $itemType;

    /**
     * @var string
     *
     * @ORM\Column(name="item_number", type="string", length=255, nullable=true)
     */
    private $itemNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="price_class_id", type="integer", nullable=true)
     */
    private $priceClassId;

    /**
     * @var integer
     *
     * @ORM\Column(name="timeframe_id", type="integer", nullable=true)
     */
    private $timeframeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="special_id", type="integer", nullable=true)
     */
    private $specialId;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="string", length=2048, nullable=true)
     */
    private $params;

    /**
     * @var integer
     *
     * @ORM\Column(name="punch_card_id", type="integer", nullable=true)
     */
    private $punchCardId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tournament_id", type="integer", nullable=false)
     */
    private $tournamentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_id", type="integer", nullable=true)
     */
    private $invoiceId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="selected", type="boolean", nullable=false)
     */
    private $selected = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="unit_price_includes_tax", type="boolean", nullable=false)
     */
    private $unitPriceIncludesTax = '0';

    /**
     * @var \foreup\rest\models\entities\ForeupItems
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="item_id", referencedColumnName="item_id")
     * })
     */
    private $item;

    /**
     * @var \foreup\rest\models\entities\ForeupPosCart
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupPosCart")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cart_id", referencedColumnName="cart_id")
     * })
     */
    private $cart;


}

