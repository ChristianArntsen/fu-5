<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupCustomerCreditCards
 *
 * @ORM\Table(name="foreup_customer_credit_cards")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupCustomerCreditCards
{
	use \foreup\rest\models\entities\EntityValidator;

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{
		$this->resetLastError();
		$location = 'ForeupCustomerCreditCards->validate';

		$etsKey = $this->getEtsKey();
		$v = $this->validate_string($location,'etsKey',$etsKey,true,$throw);
		if($v!==true)return $v;

		$mercuryId = $this->getMercuryId();
		$v = $this->validate_string($location,'mercuryId',$mercuryId,true,$throw);
		if($v!==true)return $v;

		$mercuryPassword = $this->getMercuryPassword();
		$v = $this->validate_string($location,'mercuryPassword',$mercuryPassword,true,$throw);
		if($v!==true)return $v;

		$recurring = $this->getRecurring();
		$v = $this->validate_boolean($location,'recurring',$recurring,true,$throw);
		if($v!==true)return $v;

		$courseId = $this->getCourseId();
		$v = $this->validate_integer($location,'courseId',$courseId,true,$throw);
		if($v!==true)return $v;

		$customer = $this->getCustomer();
		$v = $this->validate_object($location,'customer',$customer,'\foreup\rest\models\entities\ForeupCustomers',true,$throw);
		if($v!==true)return $v;

		$teeTimeId = $this->getTeeTimeId();
		$v = $this->validate_string($location,'teeTimeId',$teeTimeId,true,$throw);
		if($v!==true)return $v;

		$token = $this->getToken();
		$v = $this->validate_string($location,'token',$token,true,$throw);
		if($v!==true)return $v;

		$tokenExpiration = $this->getTokenExpiration();
		$v = $this->validate_object($location,'tokenExpiration',$tokenExpiration,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$cardType = $this->getCardType();
		$v = $this->validate_string($location,'cardType',$cardType,true,$throw);
		if($v!==true)return $v;

		$cardholderName = $this->getCardholderName();
		$v = $this->validate_string($location,'cardholderName',$cardholderName,true,$throw);
		if($v!==true)return $v;

		$expiration = $this->getExpiration();
		$v = $this->validate_object($location,'expiration',$expiration,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$maskedAccount = $this->getMaskedAccount();
		$v = $this->validate_string($location,'maskedAccount',$maskedAccount,true,$throw);
		if($v!==true)return $v;

		$deleted = $this->getDeleted();
		$v = $this->validate_boolean($location,'deleted',$deleted,true,$throw);
		if($v!==true)return $v;

		$successfulCharges = $this->getSuccessfulCharges();
		$v = $this->validate_integer($location,'successfulCharges',$successfulCharges,true,$throw);
		if($v!==true)return $v;

		$failedCharges = $this->getFailedCharges();
		$v = $this->validate_integer($location,'failedCharges',$failedCharges,true,$throw);
		if($v!==true)return $v;

		$status = $this->getStatus();
		$v = $this->validate_string($location,'status',$status,true,$throw);
		if($v!==true)return $v;

		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="credit_card_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $creditCardId;

    /**
     * @var string
     *
     * @ORM\Column(name="ets_key", type="string", length=255, nullable=false)
     */
    private $etsKey = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mercury_id", type="string", length=255, nullable=false)
     */
    private $mercuryId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mercury_password", type="string", length=255, nullable=false)
     */
    private $mercuryPassword = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="recurring", type="boolean", nullable=false)
     */
    private $recurring = 0;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="course_id", type="integer", nullable=false)
	 */
	private $courseId;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customer_id", type="integer", nullable=false)
	 */
	private $customerId;

	/**
	 * @var \foreup\rest\models\entities\ForeupCustomers
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCustomers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="customer_id", referencedColumnName="person_id"),
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
    private $customer;

    /**
     * @var string
     *
     * @ORM\Column(name="tee_time_id", type="string", length=21, nullable=false)
     */
    private $teeTimeId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=false)
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="token_expiration", type="date", nullable=false)
     */
    private $tokenExpiration;

    /**
     * @var string
     *
     * @ORM\Column(name="card_type", type="string", length=255, nullable=false)
     */
    private $cardType;

    /**
     * @var string
     *
     * @ORM\Column(name="cardholder_name", type="string", length=255, nullable=false)
     */
    private $cardholderName = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration", type="date", nullable=false)
     */
    private $expiration = '0000-00-00';

    /**
     * @var string
     *
     * @ORM\Column(name="masked_account", type="string", length=255, nullable=false)
     */
    private $maskedAccount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="successful_charges", type="smallint", nullable=false)
     */
    private $successfulCharges = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="failed_charges", type="smallint", nullable=false)
     */
    private $failedCharges = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=false)
     */
    private $status = '';



    /**
     * Get creditCardId
     *
     * @return integer
     */
    public function getCreditCardId()
    {
        return $this->creditCardId;
    }

	/**
	 * Get Id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->creditCardId;
	}

    /**
     * Set etsKey
     *
     * @param string $etsKey
     *
     * @return ForeupCustomerCreditCards
     */
    public function setEtsKey($etsKey)
    {
        $this->etsKey = $etsKey;
    
        return $this;
    }

    /**
     * Get etsKey
     *
     * @return string
     */
    public function getEtsKey()
    {
        return $this->etsKey;
    }

    /**
     * Set mercuryId
     *
     * @param string $mercuryId
     *
     * @return ForeupCustomerCreditCards
     */
    public function setMercuryId($mercuryId)
    {
        $this->mercuryId = $mercuryId;
    
        return $this;
    }

    /**
     * Get mercuryId
     *
     * @return string
     */
    public function getMercuryId()
    {
        return $this->mercuryId;
    }

    /**
     * Set mercuryPassword
     *
     * @param string $mercuryPassword
     *
     * @return ForeupCustomerCreditCards
     */
    public function setMercuryPassword($mercuryPassword)
    {
        $this->mercuryPassword = $mercuryPassword;
    
        return $this;
    }

    /**
     * Get mercuryPassword
     *
     * @return string
     */
    public function getMercuryPassword()
    {
        return $this->mercuryPassword;
    }

    /**
     * Set recurring
     *
     * @param boolean $recurring
     *
     * @return ForeupCustomerCreditCards
     */
    public function setRecurring($recurring)
    {
	    if(is_string($recurring)){
            $recurring = strtolower($recurring);
		    $ac = json_decode($recurring,true);
		    if(isset($ac))$recurring = $ac;
	    }

        $this->recurring = $recurring;
    
        return $this;
    }

    /**
     * Get recurring
     *
     * @return boolean
     */
    public function getRecurring()
    {
        return $this->recurring;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupCustomerCreditCards
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    
        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return ForeupCustomerCreditCards
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    
        return $this;
    }

    /**
     * Get customerId
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set teeTimeId
     *
     * @param string $teeTimeId
     *
     * @return ForeupCustomerCreditCards
     */
    public function setTeeTimeId($teeTimeId)
    {
        $this->teeTimeId = $teeTimeId;
    
        return $this;
    }

    /**
     * Get teeTimeId
     *
     * @return string
     */
    public function getTeeTimeId()
    {
        return $this->teeTimeId;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return ForeupCustomerCreditCards
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set tokenExpiration
     *
     * @param \DateTime $tokenExpiration
     *
     * @return ForeupCustomerCreditCards
     */
    public function setTokenExpiration($tokenExpiration)
    {
    	if(is_string($tokenExpiration) && strtotime($tokenExpiration))$tokenExpiration = new \DateTime($tokenExpiration);
        $this->tokenExpiration = $tokenExpiration;
    
        return $this;
    }

    /**
     * Get tokenExpiration
     *
     * @return \DateTime
     */
    public function getTokenExpiration()
    {
        return $this->tokenExpiration;
    }

    /**
     * Set cardType
     *
     * @param string $cardType
     *
     * @return ForeupCustomerCreditCards
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
    
        return $this;
    }

    /**
     * Get cardType
     *
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * Set cardholderName
     *
     * @param string $cardholderName
     *
     * @return ForeupCustomerCreditCards
     */
    public function setCardholderName($cardholderName)
    {
        $this->cardholderName = $cardholderName;
    
        return $this;
    }

    /**
     * Get cardholderName
     *
     * @return string
     */
    public function getCardholderName()
    {
        return $this->cardholderName;
    }

    /**
     * Set expiration
     *
     * @param \DateTime $expiration
     *
     * @return ForeupCustomerCreditCards
     */
    public function setExpiration($expiration)
    {
	    if(is_string($expiration) && strtotime($expiration))$expiration = new \DateTime($expiration);
        $this->expiration = $expiration;
    
        return $this;
    }

    /**
     * Get expiration
     *
     * @return \DateTime
     */
    public function getExpiration()
    {
    	// convert default string to DateTime
    	if(is_string($this->expiration))$this->setExpiration($this->expiration);
        return $this->expiration;
    }

    /**
     * Set maskedAccount
     *
     * @param string $maskedAccount
     *
     * @return ForeupCustomerCreditCards
     */
    public function setMaskedAccount($maskedAccount)
    {
        $this->maskedAccount = $maskedAccount;
    
        return $this;
    }

    /**
     * Get maskedAccount
     *
     * @return string
     */
    public function getMaskedAccount()
    {
        return $this->maskedAccount;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ForeupCustomerCreditCards
     */
    public function setDeleted($deleted)
    {
	    if(is_string($deleted)){
            $deleted = strtolower($deleted);
		    $ac = json_decode($deleted,true);
		    if(isset($ac))$deleted = $ac;
	    }

        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set successfulCharges
     *
     * @param integer $successfulCharges
     *
     * @return ForeupCustomerCreditCards
     */
    public function setSuccessfulCharges($successfulCharges)
    {
        $this->successfulCharges = $successfulCharges;
    
        return $this;
    }

    /**
     * Get successfulCharges
     *
     * @return integer
     */
    public function getSuccessfulCharges()
    {
        return $this->successfulCharges;
    }

    /**
     * Set failedCharges
     *
     * @param integer $failedCharges
     *
     * @return ForeupCustomerCreditCards
     */
    public function setFailedCharges($failedCharges)
    {
        $this->failedCharges = $failedCharges;
    
        return $this;
    }

    /**
     * Get failedCharges
     *
     * @return integer
     */
    public function getFailedCharges()
    {
        return $this->failedCharges;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ForeupCustomerCreditCards
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

	/**
	 * Set customer
	 *
	 * @param \foreup\rest\models\entities\ForeupCustomers $customer
	 *
	 * @return ForeupCustomerCreditCards
	 */
	public function setCustomer(\foreup\rest\models\entities\ForeupCustomers $customer = null)
	{
		$this->customer = $customer;
		$this->courseId = $customer->getCourseId();
		$this->customerId = $customer->getPerson()->getPersonId();

		return $this;
	}

	/**
	 * Get customer
	 *
	 * @return \foreup\rest\models\entities\ForeupCustomers
	 */
	public function getCustomer()
	{
		return $this->customer;
	}
}
