<?php

namespace foreup\rest\models\entities;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupInventory
 *
 * @ORM\Table(name="foreup_inventory")
 * @ORM\Entity
 */
class ForeupInventory
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="trans_id", type="integer", nullable=false)
     */
    private $transId;

	/**
	 * @var integer
	 * @ORM\Column(name="course_id", type="integer", nullable=false)
	 */
	private $courseId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="trans_comment", type="string", length=255, nullable=true)
	 */
	private $transComment;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="trans_date", type="datetime", length=255, nullable=true)
	 */
	private $transDate;

	/**
	 * @var integer
	 * @ORM\Column(name="trans_inventory", type="integer", nullable=false)
	 */
	private $transInventory;

    /**
     * @var \foreup\rest\models\entities\ForeupItems
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupItems",fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="trans_items", referencedColumnName="item_id", nullable=false)
     * })
     */
    private $item;

	/**
	 * @var integer
	 * @ORM\Column(name="sale_id", type="integer", nullable=false)
	 */
	private $saleId;

    /**
     * @var \foreup\rest\models\entities\ForeupSales
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupSales")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sale_id", referencedColumnName="sale_id")
     * })
     */
    private $sale;


	/**
	 * @var \foreup\rest\models\entities\ForeupEmployees
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="trans_user", referencedColumnName="person_id")
	 * })
	 */
	private $transUser;

	/**
	 * @return int
	 */
	public function getCourseId()
	{
		return $this->courseId;
	}

	/**
	 * @param int $courseId
	 */
	public function setCourseId($courseId)
	{
		$this->courseId = $courseId;
	}



	/**
	 * @return int
	 */
	public function getTransId()
	{
		return $this->transId;
	}

	/**
	 * @param int $transId
	 */
	public function setTransId($transId)
	{
		$this->transId = $transId;
	}

	/**
	 * @return string
	 */
	public function getTransComment()
	{
		return $this->transComment;
	}

	/**
	 * @param string $transComment
	 */
	public function setTransComment($transComment)
	{
		$this->transComment = $transComment;
	}

	/**
	 * @return int
	 */
	public function getTransInventory()
	{
		return $this->transInventory;
	}

	/**
	 * @param int $transInventory
	 */
	public function setTransInventory($transInventory)
	{
		$this->transInventory = $transInventory;
		return $this;
	}

	/**
	 * @return ForeupItems
	 */
	public function getItem()
	{
		return $this->item;
		return $this;
	}

	/**
	 * @param ForeupItems $item
	 */
	public function setItem($item)
	{
		$this->item = $item;
		return $this;
	}

	/**
	 * @return ForeupSales
	 */
	public function getSale()
	{
		if(empty($this->saleId))
			return null;
		return $this->sale;
	}

	/**
	 * @param ForeupSales $sale
	 */
	public function setSale($sale)
	{
		$this->sale = $sale;
		return $this;
	}

	/**
	 * @return ForeupEmployees
	 */
	public function getTransUser()
	{
		return $this->transUser;
	}

	/**
	 * @param ForeupEmployees $transUser
	 */
	public function setTransUser($transUser)
	{
		$this->transUser = $transUser;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getTransDate()
	{
		return Carbon::instance($this->transDate);
	}

	/**
	 * @param \DateTime $transDate
	 */
	public function setTransDate($transDate)
	{
		$this->transDate = $transDate;

		return $this;
	}



}
