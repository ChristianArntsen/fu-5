<?php
namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupEmployees
 *
 * @ORM\Table(name="foreup_permissions")
 * @ORM\Entity
 */
class ForeupPermissions
{


    /**
     * @var \foreup\rest\models\entities\ForeupPeople
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
     * })
     */
    private $person;

	/**
	 * @var \foreup\rest\models\entities\ForeupPeople
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupModules")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="module_id", referencedColumnName="module_id")
	 * })
	 */
	private $module;

	/**
	 * ForeupEmployees constructor.
	 */
	public function __construct(ForeupModules $module,ForeupPeople $person)
	{
		$this->person = $person;
		$this->module = $module;
	}


}
