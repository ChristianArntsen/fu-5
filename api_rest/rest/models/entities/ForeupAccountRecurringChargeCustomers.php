<?php
namespace foreup\rest\models\entities;



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountRecurringChargeCustomers
 *
 * @ORM\Table(name="foreup_account_recurring_charge_customers", indexes={@ORM\Index(name="person_id", columns={"person_id","course_id"}), @ORM\Index(name="fk_account_recurring_charge_customers_recurring_charge_id_idx", columns={"recurring_charge_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupAccountRecurringChargeCustomers
{
	private $last_error;

	public function getLastError(){
		return $this->last_error;
	}

	public function resetLastError(){
		$this->last_error = null;
	}

	private function invalid($throw = true){
		if($throw)throw new \InvalidArgumentException($this->last_error);
		else return $this->last_error;
	}

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true) {
		$recurringCharge = $this->recurringCharge;
		if(!is_a($recurringCharge, 'foreup\rest\models\entities\ForeupAccountRecurringCharges')){
			$this->last_error = 'ForeupAccountRecurringChargeCustomers->validate Error: invalid recurringCharge: '.$recurringCharge;
			return $this->invalid($throw);
		}
		
		$person = $this->person;
		if(!is_a($person, '\foreup\rest\models\entities\ForeupCustomers')){
			$this->last_error = 'ForeupAccountRecurringChargeCustomers->validate Error: customer not found';
			return $this->invalid($throw);
		}
		
		return true;
	}
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \foreup\rest\models\entities\ForeupAccountRecurringCharges
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountRecurringCharges")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recurring_charge_id", referencedColumnName="id")
     * })
     */
    private $recurringCharge;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="person_id", type="integer", nullable=false)
	 */
	private $personId;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="course_id", type="integer", nullable=false)
	 */
	private $courseId;

	/**
	 * @var \foreup\rest\models\entities\ForeupCustomers
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCustomers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id"),
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $person;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_added", type="datetime", nullable=true, unique=false)
	 */
	private $dateAdded;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recurringCharge
     *
     * @param \foreup\rest\models\entities\ForeupAccountRecurringCharges $recurringCharge
     *
     * @return ForeupAccountRecurringChargeCustomers
     */
    public function setRecurringCharge(\foreup\rest\models\entities\ForeupAccountRecurringCharges $recurringCharge = null)
    {
        $this->recurringCharge = $recurringCharge;

        return $this;
    }

	/**
	 * Get recurringCharge
	 *
	 * @return \foreup\rest\models\entities\ForeupAccountRecurringCharges
	 */
	public function getRecurringCharge()
	{
		return $this->recurringCharge;
	}

	/**
	 * Set person
	 *
	 * @param \foreup\rest\models\entities\ForeupCustomers $person
	 *
	 * @return ForeupAccountRecurringChargeCustomers
	 */
	public function setCustomer(\foreup\rest\models\entities\ForeupCustomers $person = null)
	{
		$this->person = $person;

		return $this;
	}

	/**
	 * Get person
	 *
	 * @return \foreup\rest\models\entities\ForeupCustomers
	 */
	public function getCustomer()
	{
		return $this->person;
	}

	/**
	 * Set dateAdded
	 *
	 * @param \DateTime $dateAdded
	 *
	 * @return ForeupAccountRecurringChargeCustomers
	 */
	public function setDateAdded($dateAdded)
	{
		$this->dateAdded = $dateAdded;

		return $this;
	}

	/**
	 * Get dateAdded
	 *
	 * @return \DateTime
	 */
	public function getDateAdded()
	{
		return $this->dateAdded;
	}
}
