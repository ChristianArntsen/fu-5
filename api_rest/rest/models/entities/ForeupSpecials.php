<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupSpecials
 *
 * @ORM\Table(name="foreup_specials", indexes={@ORM\Index(name="teesheet_id", columns={"teesheet_id", "date"})})
 * @ORM\Entity
 */
class ForeupSpecials
{
    /**
     * @var integer
     *
     * @ORM\Column(name="special_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="teesheet_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $teesheetId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", precision=0, scale=0, nullable=false, unique=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="price_1", type="decimal", precision=15, scale=2, nullable=false, unique=false)
     */
    private $price1;

    /**
     * @var string
     *
     * @ORM\Column(name="price_2", type="decimal", precision=15, scale=2, nullable=false, unique=false)
     */
    private $price2;

    /**
     * @var string
     *
     * @ORM\Column(name="price_3", type="decimal", precision=15, scale=2, nullable=false, unique=false)
     */
    private $price3;

    /**
     * @var string
     *
     * @ORM\Column(name="price_4", type="decimal", precision=15, scale=2, nullable=false, unique=false)
     */
    private $price4;

    /**
     * @var string
     *
     * @ORM\Column(name="price_5", type="decimal", precision=15, scale=2, nullable=false, unique=false)
     */
    private $price5;

    /**
     * @var string
     *
     * @ORM\Column(name="price_6", type="decimal", precision=15, scale=2, nullable=false, unique=false)
     */
    private $price6;

    /**
     * @var boolean
     *
     * @ORM\Column(name="monday", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $monday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tuesday", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $tuesday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wednesday", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $wednesday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="thursday", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $thursday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="friday", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $friday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="saturday", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $saturday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sunday", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $sunday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_recurring", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isRecurring;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_aggregate", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isAggregate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $active;

    /**
     * @var integer
     *
     * @ORM\Column(name="booking_class_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $bookingClassId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set teesheetId
     *
     * @param integer $teesheetId
     *
     * @return ForeupSpecials
     */
    public function setTeesheetId($teesheetId)
    {
        $this->teesheetId = $teesheetId;

        return $this;
    }

    /**
     * Get teesheetId
     *
     * @return integer
     */
    public function getTeesheetId()
    {
        return $this->teesheetId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupSpecials
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ForeupSpecials
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set price1
     *
     * @param string $price1
     *
     * @return ForeupSpecials
     */
    public function setPrice1($price1)
    {
        $this->price1 = $price1;

        return $this;
    }

    /**
     * Get price1
     *
     * @return string
     */
    public function getPrice1()
    {
        return $this->price1;
    }

    /**
     * Set price2
     *
     * @param string $price2
     *
     * @return ForeupSpecials
     */
    public function setPrice2($price2)
    {
        $this->price2 = $price2;

        return $this;
    }

    /**
     * Get price2
     *
     * @return string
     */
    public function getPrice2()
    {
        return $this->price2;
    }

    /**
     * Set price3
     *
     * @param string $price3
     *
     * @return ForeupSpecials
     */
    public function setPrice3($price3)
    {
        $this->price3 = $price3;

        return $this;
    }

    /**
     * Get price3
     *
     * @return string
     */
    public function getPrice3()
    {
        return $this->price3;
    }

    /**
     * Set price4
     *
     * @param string $price4
     *
     * @return ForeupSpecials
     */
    public function setPrice4($price4)
    {
        $this->price4 = $price4;

        return $this;
    }

    /**
     * Get price4
     *
     * @return string
     */
    public function getPrice4()
    {
        return $this->price4;
    }

    /**
     * Set price5
     *
     * @param string $price5
     *
     * @return ForeupSpecials
     */
    public function setPrice5($price5)
    {
        $this->price5 = $price5;

        return $this;
    }

    /**
     * Get price5
     *
     * @return string
     */
    public function getPrice5()
    {
        return $this->price5;
    }

    /**
     * Set price6
     *
     * @param string $price6
     *
     * @return ForeupSpecials
     */
    public function setPrice6($price6)
    {
        $this->price6 = $price6;

        return $this;
    }

    /**
     * Get price6
     *
     * @return string
     */
    public function getPrice6()
    {
        return $this->price6;
    }

    /**
     * Set monday
     *
     * @param boolean $monday
     *
     * @return ForeupSpecials
     */
    public function setMonday($monday)
    {
        $this->monday = $monday;

        return $this;
    }

    /**
     * Get monday
     *
     * @return boolean
     */
    public function getMonday()
    {
        return $this->monday;
    }

    /**
     * Set tuesday
     *
     * @param boolean $tuesday
     *
     * @return ForeupSpecials
     */
    public function setTuesday($tuesday)
    {
        $this->tuesday = $tuesday;

        return $this;
    }

    /**
     * Get tuesday
     *
     * @return boolean
     */
    public function getTuesday()
    {
        return $this->tuesday;
    }

    /**
     * Set wednesday
     *
     * @param boolean $wednesday
     *
     * @return ForeupSpecials
     */
    public function setWednesday($wednesday)
    {
        $this->wednesday = $wednesday;

        return $this;
    }

    /**
     * Get wednesday
     *
     * @return boolean
     */
    public function getWednesday()
    {
        return $this->wednesday;
    }

    /**
     * Set thursday
     *
     * @param boolean $thursday
     *
     * @return ForeupSpecials
     */
    public function setThursday($thursday)
    {
        $this->thursday = $thursday;

        return $this;
    }

    /**
     * Get thursday
     *
     * @return boolean
     */
    public function getThursday()
    {
        return $this->thursday;
    }

    /**
     * Set friday
     *
     * @param boolean $friday
     *
     * @return ForeupSpecials
     */
    public function setFriday($friday)
    {
        $this->friday = $friday;

        return $this;
    }

    /**
     * Get friday
     *
     * @return boolean
     */
    public function getFriday()
    {
        return $this->friday;
    }

    /**
     * Set saturday
     *
     * @param boolean $saturday
     *
     * @return ForeupSpecials
     */
    public function setSaturday($saturday)
    {
        $this->saturday = $saturday;

        return $this;
    }

    /**
     * Get saturday
     *
     * @return boolean
     */
    public function getSaturday()
    {
        return $this->saturday;
    }

    /**
     * Set sunday
     *
     * @param boolean $sunday
     *
     * @return ForeupSpecials
     */
    public function setSunday($sunday)
    {
        $this->sunday = $sunday;

        return $this;
    }

    /**
     * Get sunday
     *
     * @return boolean
     */
    public function getSunday()
    {
        return $this->sunday;
    }

    /**
     * Set isRecurring
     *
     * @param boolean $isRecurring
     *
     * @return ForeupSpecials
     */
    public function setIsRecurring($isRecurring)
    {
        $this->isRecurring = $isRecurring;

        return $this;
    }

    /**
     * Get isRecurring
     *
     * @return boolean
     */
    public function getIsRecurring()
    {
        return $this->isRecurring;
    }

    /**
     * Set isAggregate
     *
     * @param boolean $isAggregate
     *
     * @return ForeupSpecials
     */
    public function setIsAggregate($isAggregate)
    {
        $this->isAggregate = $isAggregate;

        return $this;
    }

    /**
     * Get isAggregate
     *
     * @return boolean
     */
    public function getIsAggregate()
    {
        return $this->isAggregate;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return ForeupSpecials
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set bookingClassId
     *
     * @param integer $bookingClassId
     *
     * @return ForeupSpecials
     */
    public function setBookingClassId($bookingClassId)
    {
        $this->bookingClassId = $bookingClassId;

        return $this;
    }

    /**
     * Get bookingClassId
     *
     * @return integer
     */
    public function getBookingClassId()
    {
        return $this->bookingClassId;
    }
}

