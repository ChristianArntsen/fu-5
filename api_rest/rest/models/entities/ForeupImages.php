<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupImages
 *
 * @ORM\Table(name="foreup_images")
 * @ORM\Entity
 */
class ForeupImages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="image_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $imageId;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="filename", type="string", nullable=true)
	 */
	private $filename;

	/**
	 * @return int
	 */
	public function getImageId()
	{
		return $this->imageId;
	}

	/**
	 * @param int $imageId
	 */
	public function setImageId($imageId)
	{
		$this->imageId = $imageId;
	}

	/**
	 * @return string
	 */
	public function getFilename()
	{
		return $this->filename;
	}

	/**
	 * @param string $filename
	 */
	public function setFilename($filename)
	{
		$this->filename = $filename;
	}


}

