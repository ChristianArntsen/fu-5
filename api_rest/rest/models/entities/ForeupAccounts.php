<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccounts
 *
 * @ORM\Table(name="foreup_accounts", uniqueConstraints={@ORM\UniqueConstraint(name="account_number", columns={"organization_id", "number"})}, indexes={@ORM\Index(name="organization_id", columns={"organization_id"}), @ORM\Index(name="account_type_id", columns={"account_type_id"}), @ORM\Index(name="owner_id", columns={"owner_id"}), @ORM\Index(name="balance", columns={"balance"})})
 * @ORM\Entity(repositoryClass="foreup\rest\models\repositories\AccountsRepository")
 */
class ForeupAccounts
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accountLedger = new \Doctrine\Common\Collections\ArrayCollection();
        $this->accountCustomers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $organizationId;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=64, precision=0, scale=0, nullable=true, unique=false)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="balance", type="decimal", precision=15, scale=2, nullable=false, unique=false)
     */
    private $balance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dateCreated;

    /**
     * @var \foreup\rest\models\entities\ForeupCustomers
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCustomers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="customer_id", nullable=true)
     * })
     */
    private $owner;

    /**
     * @var \foreup\rest\models\entities\ForeupAccountTypes
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_type_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $accountType;


    /**
     * @var \foreup\rest\models\entities\ForeupAccountLedger
     *
     * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupAccountLedger",mappedBy="account")
     */
    private $accountLedger;

    /**
     * @var \foreup\rest\models\entities\ForeupAccountCustomers
     *
     * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupAccountCustomers",mappedBy="account")
     */
    private $accountCustomers;

    private $actualLimit;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     *
     * @return ForeupAccounts
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return ForeupAccounts
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set balance
     *
     * @param string $balance
     *
     * @return ForeupAccounts
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    
        return $this;
    }

    /**
     * Get balance
     *
     * @return string
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupAccounts
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set $owner
     *
     * @param ForeupCustomers $owner
     *
     * @return ForeupAccounts
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    
        return $this;
    }

    /**
     * Get owner
     *
     * @return integer
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @return mixed
     */
    public function getActualLimit()
    {
        return $this->actualLimit;
    }

    /**
     * @param mixed $actualLimit
     */
    public function setActualLimit($actualLimit)
    {
        $this->actualLimit = $actualLimit;
    }

    /**
     * Set accountType
     *
     * @param \foreup\rest\models\entities\ForeupAccountTypes $accountType
     *
     * @return ForeupAccounts
     */
    public function setAccountType(\foreup\rest\models\entities\ForeupAccountTypes $accountType = null)
    {
        $this->accountType = $accountType;
    
        return $this;
    }

    /**
     * Get accountType
     *
     * @return \foreup\rest\models\entities\ForeupAccountTypes
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * @return \foreup\rest\models\entities\ForeupAccountCustomers[]
     */
    public function getAccountCustomers()
    {
        return $this->accountCustomers;
    }

    /**
     * @param ForeupAccountCustomers $accountCustomers
     */
    public function setAccountCustomers($accountCustomers)
    {
        $this->accountCustomers = $accountCustomers;
    }

    private function increaseBalance($amount)
    {
        $this->balance += $amount;
    }

    public function getAllowedAmmount($amount)
    {
        if($this->getActualLimit() === null){
            $available = $amount;
        } else
        {
            $available =  $this->getActualLimit();
        }
        if($amount > $available)
            return $available;
        else
            return $amount;
    }

    public function debitAccount(ForeupAccountLedger $item)
    {
        $newAmount = $this->getAllowedAmmount($item->getAmount());
        $item->setAmount($newAmount);

        if($newAmount > 0){
            $this->increaseBalance($newAmount);
            $item->setAccount($this);
            $this->accountLedger[] = $item;
            return $newAmount;
        }

        return false;
    }

    public function creditAccount(ForeupAccountLedger $item)
    {

        $this->increaseBalance($item->getAmount());
        $item->setAccount($this);
        $this->accountLedger[] = $item;

        return $item->getAmount();
    }

   
}

