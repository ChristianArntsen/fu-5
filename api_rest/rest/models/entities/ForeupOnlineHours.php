<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupOnlineHours
 *
 * @ORM\Table(name="foreup_online_hours", indexes={@ORM\Index(name="wed_open", columns={"wed_open", "wed_close"}), @ORM\Index(name="thu_open", columns={"thu_open", "thu_close"}), @ORM\Index(name="fri_open", columns={"fri_open", "fri_close"}), @ORM\Index(name="sat_open", columns={"sat_open", "sat_close"}), @ORM\Index(name="sun_open", columns={"sun_open", "sun_close"}), @ORM\Index(name="mon_open", columns={"mon_open", "mon_close"}), @ORM\Index(name="tue_open", columns={"tue_open", "tue_close"})})
 * @ORM\Entity
 */
class ForeupOnlineHours
{
    /**
     * @var integer
     *
     * @ORM\Column(name="teesheet_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="sun_open", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $sunOpen = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="sun_close", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $sunClose = 2359;

    /**
     * @var integer
     *
     * @ORM\Column(name="mon_open", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $monOpen = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="mon_close", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $monClose = 2359;

    /**
     * @var integer
     *
     * @ORM\Column(name="tue_open", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tueOpen = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="tue_close", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $tueClose = 2359;

    /**
     * @var integer
     *
     * @ORM\Column(name="wed_open", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $wedOpen = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="wed_close", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $wedClose = 2359;

    /**
     * @var integer
     *
     * @ORM\Column(name="thu_open", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $thuOpen = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="thu_close", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $thuClose = 2359;

    /**
     * @var integer
     *
     * @ORM\Column(name="fri_open", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $friOpen = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="fri_close", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $friClose = 2359;

    /**
     * @var integer
     *
     * @ORM\Column(name="sat_open", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $satOpen = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="sat_close", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $satClose = 2359;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return ForeupOnlineHours
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sunOpen
     *
     * @param integer $sunOpen
     *
     * @return ForeupOnlineHours
     */
    public function setSunOpen($sunOpen)
    {
        $this->sunOpen = $sunOpen;

        return $this;
    }

    /**
     * Get sunOpen
     *
     * @return integer
     */
    public function getSunOpen()
    {
        return $this->sunOpen;
    }

    /**
     * Set sunClose
     *
     * @param integer $sunClose
     *
     * @return ForeupOnlineHours
     */
    public function setSunClose($sunClose)
    {
        $this->sunClose = $sunClose;

        return $this;
    }

    /**
     * Get sunClose
     *
     * @return integer
     */
    public function getSunClose()
    {
        return $this->sunClose;
    }

    /**
     * Set monOpen
     *
     * @param integer $monOpen
     *
     * @return ForeupOnlineHours
     */
    public function setMonOpen($monOpen)
    {
        $this->monOpen = $monOpen;

        return $this;
    }

    /**
     * Get monOpen
     *
     * @return integer
     */
    public function getMonOpen()
    {
        return $this->monOpen;
    }

    /**
     * Set monClose
     *
     * @param integer $monClose
     *
     * @return ForeupOnlineHours
     */
    public function setMonClose($monClose)
    {
        $this->monClose = $monClose;

        return $this;
    }

    /**
     * Get monClose
     *
     * @return integer
     */
    public function getMonClose()
    {
        return $this->monClose;
    }

    /**
     * Set tueOpen
     *
     * @param integer $tueOpen
     *
     * @return ForeupOnlineHours
     */
    public function setTueOpen($tueOpen)
    {
        $this->tueOpen = $tueOpen;

        return $this;
    }

    /**
     * Get tueOpen
     *
     * @return integer
     */
    public function getTueOpen()
    {
        return $this->tueOpen;
    }

    /**
     * Set tueClose
     *
     * @param integer $tueClose
     *
     * @return ForeupOnlineHours
     */
    public function setTueClose($tueClose)
    {
        $this->tueClose = $tueClose;

        return $this;
    }

    /**
     * Get tueClose
     *
     * @return integer
     */
    public function getTueClose()
    {
        return $this->tueClose;
    }

    /**
     * Set wedOpen
     *
     * @param integer $wedOpen
     *
     * @return ForeupOnlineHours
     */
    public function setWedOpen($wedOpen)
    {
        $this->wedOpen = $wedOpen;

        return $this;
    }

    /**
     * Get wedOpen
     *
     * @return integer
     */
    public function getWedOpen()
    {
        return $this->wedOpen;
    }

    /**
     * Set wedClose
     *
     * @param integer $wedClose
     *
     * @return ForeupOnlineHours
     */
    public function setWedClose($wedClose)
    {
        $this->wedClose = $wedClose;

        return $this;
    }

    /**
     * Get wedClose
     *
     * @return integer
     */
    public function getWedClose()
    {
        return $this->wedClose;
    }

    /**
     * Set thuOpen
     *
     * @param integer $thuOpen
     *
     * @return ForeupOnlineHours
     */
    public function setThuOpen($thuOpen)
    {
        $this->thuOpen = $thuOpen;

        return $this;
    }

    /**
     * Get thuOpen
     *
     * @return integer
     */
    public function getThuOpen()
    {
        return $this->thuOpen;
    }

    /**
     * Set thuClose
     *
     * @param integer $thuClose
     *
     * @return ForeupOnlineHours
     */
    public function setThuClose($thuClose)
    {
        $this->thuClose = $thuClose;

        return $this;
    }

    /**
     * Get thuClose
     *
     * @return integer
     */
    public function getThuClose()
    {
        return $this->thuClose;
    }

    /**
     * Set friOpen
     *
     * @param integer $friOpen
     *
     * @return ForeupOnlineHours
     */
    public function setFriOpen($friOpen)
    {
        $this->friOpen = $friOpen;

        return $this;
    }

    /**
     * Get friOpen
     *
     * @return integer
     */
    public function getFriOpen()
    {
        return $this->friOpen;
    }

    /**
     * Set friClose
     *
     * @param integer $friClose
     *
     * @return ForeupOnlineHours
     */
    public function setFriClose($friClose)
    {
        $this->friClose = $friClose;

        return $this;
    }

    /**
     * Get friClose
     *
     * @return integer
     */
    public function getFriClose()
    {
        return $this->friClose;
    }

    /**
     * Set satOpen
     *
     * @param integer $satOpen
     *
     * @return ForeupOnlineHours
     */
    public function setSatOpen($satOpen)
    {
        $this->satOpen = $satOpen;

        return $this;
    }

    /**
     * Get satOpen
     *
     * @return integer
     */
    public function getSatOpen()
    {
        return $this->satOpen;
    }

    /**
     * Set satClose
     *
     * @param integer $satClose
     *
     * @return ForeupOnlineHours
     */
    public function setSatClose($satClose)
    {
        $this->satClose = $satClose;

        return $this;
    }

    /**
     * Get satClose
     *
     * @return integer
     */
    public function getSatClose()
    {
        return $this->satClose;
    }
}

