<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use foreup\rest\exceptions\ValidateException;

/**
 * ForeupAccountLedger
 *
 * @ORM\Table(name="foreup_account_ledger", uniqueConstraints={@ORM\UniqueConstraint(name="number", columns={"account_id", "number"})}, indexes={@ORM\Index(name="record_type_id", columns={"type"}), @ORM\Index(name="person_organization", columns={"organization_id", "person_id"}), @ORM\Index(name="statement_id", columns={"statement_id"}), @ORM\Index(name="due_date", columns={"due_date"}), @ORM\Index(name="date_created", columns={"date_created"}), @ORM\Index(name="date_voided", columns={"date_voided"}), @ORM\Index(name="fk_account_ledger_account_id_idx", columns={"account_id"}), @ORM\Index(name="amounts", columns={"amount", "amount_paid", "amount_open"})})
 * @ORM\Entity (repositoryClass="foreup\rest\models\repositories\AccountLedgerRepository")
 * @HasLifecycleCallbacks
 */
class ForeupAccountLedger
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \foreup\rest\models\entities\ForeupAccountLedgerPayments
     *
     * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupAccountLedgerPayments",mappedBy="ledger")
     */
    private $accountLedgerPayments;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=2, nullable=true, unique=false)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="amount_paid", type="decimal", precision=15, scale=2, nullable=true, unique=false)
     */
    private $amountPaid;

    /**
     * @var string
     *
     * @ORM\Column(name="amount_open", type="decimal", precision=15, scale=2, nullable=true, unique=false)
     */
    private $amountOpen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dueDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $organizationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $personId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_voided", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateVoided;

    /**
     * @var integer
     *
     * @ORM\Column(name="voided_by", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $voidedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="string", length=4096, precision=0, scale=0, nullable=true, unique=false)
     */
    private $memo;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $number;

    /**
     * @var \foreup\rest\models\entities\ForeupAccounts
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccounts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $account;

    /**
     * @var \foreup\rest\models\entities\ForeupAccountStatements
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountStatements")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statement_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $statement;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="foreup\rest\models\entities\ForeupAccountPayments", inversedBy="ledger")
     * @ORM\JoinTable(name="foreup_account_ledger_payments",
     *   joinColumns={
     *     @ORM\JoinColumn(name="ledger_id", referencedColumnName="id", nullable=true)
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="payment_id", referencedColumnName="id", nullable=true)
     *   }
     * )
     */
    private $payment;

    /**
     * Constructor
     */
    public function __construct()
    {
        //$this->payment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupAccountLedger
     */
    public function setType($type)
    {
        $validTypes = ['sale','invoice','return','void','credit','estimate','charge','payment'];
        if(!in_array($type,$validTypes)){
            return false;
        }
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return ForeupAccountLedger
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        $this->setAmountOpen($amount);
        $this->setAmountPaid(0);
        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set amountPaid
     *
     * @param string $amountPaid
     *
     * @return ForeupAccountLedger
     */
    public function setAmountPaid($amountPaid)
    {
        $this->amountPaid = $amountPaid;
    
        return $this;
    }

    /**
     * Get amountPaid
     *
     * @return string
     */
    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    /**
     * Set amountOpen
     *
     * @param string $amountOpen
     *
     * @return ForeupAccountLedger
     */
    public function setAmountOpen($amountOpen)
    {
        $this->amountOpen = $amountOpen;
    
        return $this;
    }

    /**
     * Get amountOpen
     *
     * @return string
     */
    public function getAmountOpen()
    {
        return $this->amountOpen;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupAccountLedger
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return ForeupAccountLedger
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     *
     * @return ForeupAccountLedger
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;
    
        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     *
     * @return ForeupAccountLedger
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set personId
     *
     * @param integer $personId
     *
     * @return ForeupAccountLedger
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    
        return $this;
    }

    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set dateVoided
     *
     * @param \DateTime $dateVoided
     *
     * @return ForeupAccountLedger
     */
    public function setDateVoided($dateVoided)
    {
        $this->dateVoided = $dateVoided;
    
        return $this;
    }

    /**
     * Get dateVoided
     *
     * @return \DateTime
     */
    public function getDateVoided()
    {
        return $this->dateVoided;
    }

    /**
     * Set voidedBy
     *
     * @param integer $voidedBy
     *
     * @return ForeupAccountLedger
     */
    public function setVoidedBy($voidedBy)
    {
        $this->voidedBy = $voidedBy;
    
        return $this;
    }

    /**
     * Get voidedBy
     *
     * @return integer
     */
    public function getVoidedBy()
    {
        return $this->voidedBy;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return ForeupAccountLedger
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set memo
     *
     * @param string $memo
     *
     * @return ForeupAccountLedger
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
    
        return $this;
    }

    /**
     * Get memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return ForeupAccountLedger
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set account
     *
     * @param \foreup\rest\models\entities\ForeupAccounts $account
     *
     * @return ForeupAccountLedger
     */
    public function setAccount(\foreup\rest\models\entities\ForeupAccounts $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \foreup\rest\models\entities\ForeupAccounts
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set statement
     *
     * @param \foreup\rest\models\entities\ForeupAccountStatements $statement
     *
     * @return ForeupAccountLedger
     */
    public function setStatement(\foreup\rest\models\entities\ForeupAccountStatements $statement = null)
    {
        $this->statement = $statement;
    
        return $this;
    }

    /**
     * Get statement
     *
     * @return \foreup\rest\models\entities\ForeupAccountStatements
     */
    public function getStatement()
    {
        return $this->statement;
    }

    /**
     * Add payment
     *
     * @param \foreup\rest\models\entities\ForeupAccountPayments $payment
     *
     * @return ForeupAccountLedger
     */
    public function addPayment(\foreup\rest\models\entities\ForeupAccountPayments $payment)
    {
        $this->payment[] = $payment;
    
        return $this;
    }

    /**
     * Remove payment
     *
     * @param \foreup\rest\models\entities\ForeupAccountPayments $payment
     */
    public function removePayment(\foreup\rest\models\entities\ForeupAccountPayments $payment)
    {
        $this->payment->removeElement($payment);
    }

    /**
     * Get payment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayment()
    {
        return $this->payment;
    }

    public function subAmountOpen($amount)
    {
        $this->setAmountOpen($this->getAmountOpen() - $amount );
        $this->setAmountPaid($this->getAmountPaid() + $amount);

        return $this;
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function validate()
    {
        if (!isset($this->type)) {
            throw new ValidateException("Type required.");
        }

        if (!isset($this->personId)) {
            throw new ValidateException("Person id required");
        }

        if (!isset($this->account)) {
            throw new ValidateException("Account required");
        }

        if (!isset($this->createdBy)) {
            throw new ValidateException("Created by required.");
        }

        if (!isset($this->number)) {
            throw new ValidateException("Number required.");
        }
    }
}

