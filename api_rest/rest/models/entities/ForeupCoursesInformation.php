<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupCoursesInformation
 *
 * @ORM\Table(name="foreup_courses_information", indexes={@ORM\Index(name="sales_rep", columns={"sales_rep"})})
 * @ORM\Entity
 */
class ForeupCoursesInformation
{
    /**
     * @var string
     *
     * @ORM\Column(name="stage", type="string", nullable=false)
     */
    private $stage;

    /**
     * @var string
     *
     * @ORM\Column(name="course_type", type="string", nullable=false)
     */
    private $courseType;

    /**
     * @var string
     *
     * @ORM\Column(name="course_health", type="string", nullable=false)
     */
    private $courseHealth;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_app", type="string", nullable=false)
     */
    private $mobileApp;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_value", type="integer", nullable=false)
     */
    private $courseValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="sphere_influence", type="integer", nullable=false)
     */
    private $sphereInfluence;

    /**
     * @var string
     *
     * @ORM\Column(name="apr", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $apr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_sold", type="date", nullable=false)
     */
    private $dateSold;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_contract_ends", type="date", nullable=false)
     */
    private $dateContractEnds;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_cancelled", type="date", nullable=false)
     */
    private $dateCancelled;

    /**
     * @var integer
     *
     * @ORM\Column(name="mobile_rep", type="integer", nullable=false)
     */
    private $mobileRep;

    /**
     * @var string
     *
     * @ORM\Column(name="ios", type="string", nullable=false)
     */
    private $ios = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ios_stage", type="string", nullable=false)
     */
    private $iosStage = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ios_live_date", type="date", nullable=false)
     */
    private $iosLiveDate;

    /**
     * @var string
     *
     * @ORM\Column(name="android", type="string", nullable=false)
     */
    private $android = '';

    /**
     * @var string
     *
     * @ORM\Column(name="android_stage", type="string", nullable=false)
     */
    private $androidStage = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="android_live_date", type="date", nullable=false)
     */
    private $androidLiveDate;

    /**
     * @var \foreup\rest\models\entities\ForeupCourses
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $course;

//    /**
//     * @var \foreup\rest\models\entities\ForeupEmployees
//     *
//     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
//     * @ORM\JoinColumns({
//     *   @ORM\JoinColumn(name="sales_rep", referencedColumnName="person_id")
//     * })
//     */
//    private $salesRep;

    /**
     * @return string
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * @param string $stage
     */
    public function setStage($stage)
    {
        $this->stage = $stage;
    }

    /**
     * @return string
     */
    public function getCourseType()
    {
        return $this->courseType;
    }

    /**
     * @param string $courseType
     */
    public function setCourseType($courseType)
    {
        $this->courseType = $courseType;
    }

    /**
     * @return string
     */
    public function getCourseHealth()
    {
        return $this->courseHealth;
    }

    /**
     * @param string $courseHealth
     */
    public function setCourseHealth($courseHealth)
    {
        $this->courseHealth = $courseHealth;
    }

    /**
     * @return string
     */
    public function getMobileApp()
    {
        return $this->mobileApp;
    }

    /**
     * @param string $mobileApp
     */
    public function setMobileApp($mobileApp)
    {
        $this->mobileApp = $mobileApp;
    }

    /**
     * @return int
     */
    public function getCourseValue()
    {
        return $this->courseValue;
    }

    /**
     * @param int $courseValue
     */
    public function setCourseValue($courseValue)
    {
        $this->courseValue = $courseValue;
    }

    /**
     * @return int
     */
    public function getSphereInfluence()
    {
        return $this->sphereInfluence;
    }

    /**
     * @param int $sphereInfluence
     */
    public function setSphereInfluence($sphereInfluence)
    {
        $this->sphereInfluence = $sphereInfluence;
    }

    /**
     * @return string
     */
    public function getApr()
    {
        return $this->apr;
    }

    /**
     * @param string $apr
     */
    public function setApr($apr)
    {
        $this->apr = $apr;
    }

    /**
     * @return \DateTime
     */
    public function getDateSold()
    {
        return $this->dateSold;
    }

    /**
     * @param \DateTime $dateSold
     */
    public function setDateSold($dateSold)
    {
        $this->dateSold = $dateSold;
    }

    /**
     * @return \DateTime
     */
    public function getDateContractEnds()
    {
        return $this->dateContractEnds;
    }

    /**
     * @param \DateTime $dateContractEnds
     */
    public function setDateContractEnds($dateContractEnds)
    {
        $this->dateContractEnds = $dateContractEnds;
    }

    /**
     * @return \DateTime
     */
    public function getDateCancelled()
    {
        return $this->dateCancelled;
    }

    /**
     * @param \DateTime $dateCancelled
     */
    public function setDateCancelled($dateCancelled)
    {
        $this->dateCancelled = $dateCancelled;
    }

    /**
     * @return int
     */
    public function getMobileRep()
    {
        return $this->mobileRep;
    }

    /**
     * @param int $mobileRep
     */
    public function setMobileRep($mobileRep)
    {
        $this->mobileRep = $mobileRep;
    }

    /**
     * @return string
     */
    public function getIos()
    {
        return $this->ios;
    }

    /**
     * @param string $ios
     */
    public function setIos($ios)
    {
        $this->ios = $ios;
    }

    /**
     * @return string
     */
    public function getIosStage()
    {
        return $this->iosStage;
    }

    /**
     * @param string $iosStage
     */
    public function setIosStage($iosStage)
    {
        $this->iosStage = $iosStage;
    }

    /**
     * @return \DateTime
     */
    public function getIosLiveDate()
    {
        return $this->iosLiveDate;
    }

    /**
     * @param \DateTime $iosLiveDate
     */
    public function setIosLiveDate($iosLiveDate)
    {
        $this->iosLiveDate = $iosLiveDate;
    }

    /**
     * @return string
     */
    public function getAndroid()
    {
        return $this->android;
    }

    /**
     * @param string $android
     */
    public function setAndroid($android)
    {
        $this->android = $android;
    }

    /**
     * @return string
     */
    public function getAndroidStage()
    {
        return $this->androidStage;
    }

    /**
     * @param string $androidStage
     */
    public function setAndroidStage($androidStage)
    {
        $this->androidStage = $androidStage;
    }

    /**
     * @return \DateTime
     */
    public function getAndroidLiveDate()
    {
        return $this->androidLiveDate;
    }

    /**
     * @param \DateTime $androidLiveDate
     */
    public function setAndroidLiveDate($androidLiveDate)
    {
        $this->androidLiveDate = $androidLiveDate;
    }

    /**
     * @return ForeupCourses
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param ForeupCourses $course
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }

    /**
     * @return ForeupEmployees
     */
    public function getSalesRep()
    {
        return $this->salesRep;
    }

    /**
     * @param ForeupEmployees $salesRep
     */
    public function setSalesRep($salesRep)
    {
        $this->salesRep = $salesRep;
    }


}

