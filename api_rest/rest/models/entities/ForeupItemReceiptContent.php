<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupItemReceiptContent
 *
 * @ORM\Table(name="foreup_item_receipt_content", indexes={@ORM\Index(name="course_id", columns={"course_id"})})
 * @ORM\Entity
 */
class ForeupItemReceiptContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="receipt_content_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=1024, precision=0, scale=0, nullable=true, unique=false)
     */
    private $name = "Default Name";

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, precision=0, scale=0, nullable=false, unique=false)
     */
    private $content = "";

    /**
     * @var boolean
     *
     * @ORM\Column(name="signature_line", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $signatureLine = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="separate_receipt", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $separateReceipt = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $courseId;


    /**
     * Get receiptContentId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupItemReceiptContent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return ForeupItemReceiptContent
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set signatureLine
     *
     * @param boolean $signatureLine
     *
     * @return ForeupItemReceiptContent
     */
    public function setSignatureLine($signatureLine)
    {
        $this->signatureLine = $signatureLine;

        return $this;
    }

    /**
     * Get signatureLine
     *
     * @return boolean
     */
    public function getSignatureLine()
    {
        return $this->signatureLine;
    }

    /**
     * Set separateReceipt
     *
     * @param boolean $separateReceipt
     *
     * @return ForeupItemReceiptContent
     */
    public function setSeparateReceipt($separateReceipt)
    {
        $this->separateReceipt = $separateReceipt;

        return $this;
    }

    /**
     * Get separateReceipt
     *
     * @return boolean
     */
    public function getSeparateReceipt()
    {
        return $this->separateReceipt;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupItemReceiptContent
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;

        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }
}

