<?php
namespace foreup\rest\models\entities;



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountRecurringChargeGroups
 *
 * @ORM\Table(name="foreup_account_recurring_charge_groups", indexes={@ORM\Index(name="group_id", columns={"group_id"}), @ORM\Index(name="fk_account_recurring_charge_group_ibfk_1", columns={"recurring_charge_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupAccountRecurringChargeGroups
{
	private $last_error;

	public function getLastError(){
		return $this->last_error;
	}

	public function resetLastError(){
		$this->last_error = null;
	}

	private function invalid($throw = true){
		if($throw)throw new \InvalidArgumentException($this->last_error);
		else return $this->last_error;
	}

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true) {

		$include = $this->include;
		if(is_bool($include) || !isset($include)){
			// all good
		}
		elseif (is_numeric($include)){
			if(!is_int($include * 1) || $include > 1 || $include < 0){
				$this->last_error = 'ForeupAccountRecurringChargeGroups->include Error: ambiguous use of numeric in boolean context: '.$include;
				return $this->invalid($throw);
			}
		}
		elseif (is_string($include)){
			$test = strtolower($include);
			if(!in_array($test,array('true','false'))){
				$this->last_error = 'ForeupAccountRecurringChargeGroups->include Error: ambiguous use of string in boolean context: '.$include;
				return $this->invalid($throw);
			}
		}else{
			$this->last_error = 'ForeupAccountRecurringChargeGroups->include Error: bad argument in boolean context';
			return $this->invalid($throw);
		}

		$recurringCharge = $this->recurringCharge;
		if(!is_a($recurringCharge, 'foreup\rest\models\entities\ForeupAccountRecurringCharges')){
			$this->last_error = 'ForeupAccountRecurringChargeGroups->validate Error: invalid recurringCharge: '.$recurringCharge;
			return $this->invalid($throw);
		}

		$group = $this->group;
		if(!is_a($group, '\foreup\rest\models\entities\ForeupGroups')){
			$this->last_error = 'ForeupAccountRecurringChargeGroups->validate Error: group not found';
			return $this->invalid($throw);
		}

		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="include", type="boolean", nullable=true)
     */
    private $include = '1';

    /**
     * @var \foreup\rest\models\entities\ForeupAccountRecurringCharges
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountRecurringCharges")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recurring_charge_id", referencedColumnName="id")
     * })
     */
    private $recurringCharge;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="group_id", type="integer", nullable=false)
	 */
	private $groupId;

	/**
	 * @var \foreup\rest\models\entities\ForeupGroups
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupGroups")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="group_id", referencedColumnName="group_id")
	 * })
	 */
	private $group;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set include
     *
     * @param boolean $include
     *
     * @return ForeupAccountRecurringChargeGroups
     */
    public function setInclude($include)
    {
	    if(is_string($include)){
		    $tmp = json_decode(strtolower($include));
		    if(isset($tmp))$include = $tmp;
	    }
	    $this->include = $include;

        return $this;
    }

    /**
     * Get include
     *
     * @return boolean
     */
    public function getInclude()
    {
        return $this->include;
    }

    /**
     * Set recurringCharge
     *
     * @param \foreup\rest\models\entities\ForeupAccountRecurringCharges $recurringCharge
     *
     * @return ForeupAccountRecurringChargeGroups
     */
    public function setRecurringCharge(\foreup\rest\models\entities\ForeupAccountRecurringCharges $recurringCharge = null)
    {
        $this->recurringCharge = $recurringCharge;

        return $this;
    }

    /**
     * Get recurringCharge
     *
     * @return \foreup\rest\models\entities\ForeupAccountRecurringCharges
     */
    public function getRecurringCharge()
    {
        return $this->recurringCharge;
    }

	/**
	 * Set groupId
	 *
	 * @param integer $groupId
	 *
	 * @return ForeupAccountRecurringChargeGroups
	 */
	public function setGroupId($groupId)
	{
		$this->groupId = $groupId;

		return $this;
	}

	/**
	 * Get groupId
	 *
	 * @return integer
	 */
	public function getGroupId()
	{
		return $this->groupId;
	}

	/**
	 * Set group
	 *
	 * @param \foreup\rest\models\entities\ForeupGroups $group
	 *
	 * @return ForeupAccountRecurringChargeGroups
	 *
	 */
	public function setGroup(\foreup\rest\models\entities\ForeupGroups $group = null)
	{
		$this->group = $group;

		return $this;
	}

	/**
	 * Get group
	 *
	 * @return \foreup\rest\models\entities\ForeupGroups
	 */
	public function getGroup()
	{
		return $this->group;
	}

}
