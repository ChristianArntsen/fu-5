<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupMarketingCampaigns
 *
 * @ORM\Table(name="foreup_marketing_campaigns", uniqueConstraints={@ORM\UniqueConstraint(name="remote_hash", columns={"remote_hash"})}, indexes={@ORM\Index(name="marketing_campaigns_fk1", columns={"course_id"})})
 * @ORM\Entity (repositoryClass="foreup\rest\models\repositories\MarketingCampaignsRepository")
 */
class ForeupMarketingCampaigns
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="campaign_id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255, nullable=false)
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="content_2", type="text", length=65535, nullable=false)
     */
    private $content2;

    /**
     * @var string
     *
     * @ORM\Column(name="content_3", type="text", length=65535, nullable=false)
     */
    private $content3;

    /**
     * @var string
     *
     * @ORM\Column(name="content_4", type="text", length=65535, nullable=false)
     */
    private $content4;

    /**
     * @var string
     *
     * @ORM\Column(name="content_5", type="text", length=65535, nullable=false)
     */
    private $content5;

    /**
     * @var string
     *
     * @ORM\Column(name="content_6", type="text", length=65535, nullable=false)
     */
    private $content6;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="images", type="text", length=65535, nullable=false)
     */
    private $images;

    /**
     * @var string
     *
     * @ORM\Column(name="image_2", type="string", length=255, nullable=false)
     */
    private $image2;

    /**
     * @var string
     *
     * @ORM\Column(name="image_3", type="string", length=255, nullable=false)
     */
    private $image3;

    /**
     * @var string
     *
     * @ORM\Column(name="image_4", type="string", length=255, nullable=false)
     */
    private $image4;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="send_date", type="datetime", nullable=false)
     */
    private $sendDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="send_date_cst", type="datetime", nullable=false)
     */
    private $sendDateCst;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20, nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="recipients", type="text", length=16777215, nullable=true)
     */
    private $recipients;

    /**
     * @var integer
     *
     * @ORM\Column(name="recipient_count", type="smallint", nullable=false)
     */
    private $recipientCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer", nullable=false)
     */
    private $personId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CID", type="integer", nullable=false)
     */
    private $cid;

    /**
     * @var integer
     *
     * @ORM\Column(name="d", type="integer", nullable=false)
     */
    private $d;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="template_path", type="string", length=1000, nullable=false)
     */
    private $templatePath;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_path", type="string", length=1000, nullable=true)
     */
    private $logoPath;

    /**
     * @var string
     *
     * @ORM\Column(name="header", type="string", length=1000, nullable=true)
     */
    private $header;

    /**
     * @var boolean
     *
     * @ORM\Column(name="queued", type="boolean", nullable=false)
     */
    private $queued;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_sent", type="integer", nullable=false)
     */
    private $isSent = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="reported", type="boolean", nullable=false)
     */
    private $reported;

    /**
     * @var boolean
     *
     * @ORM\Column(name="attempts", type="boolean", nullable=false)
     */
    private $attempts;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="run_time", type="datetime", nullable=false)
     */
    private $runTime;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="promotion_id", type="string", length=100, nullable=false)
     */
    private $promotionId = '-1';

    /**
     * @var integer
     *
     * @ORM\Column(name="marketing_template_id", type="integer", nullable=true)
     */
    private $marketingTemplateId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=true)
     */
    private $dateCreated = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="from_name", type="string", length=255, nullable=true)
     */
    private $fromName;

    /**
     * @var string
     *
     * @ORM\Column(name="from_email", type="string", length=255, nullable=true)
     */
    private $fromEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="reply_to", type="string", length=255, nullable=true)
     */
    private $replyTo;

    /**
     * @var string
     *
     * @ORM\Column(name="rendered_html", type="text", length=65535, nullable=true)
     */
    private $renderedHtml;

    /**
     * @var string
     *
     * @ORM\Column(name="json_content", type="text", length=65535, nullable=true)
     */
    private $jsonContent;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="logo_align", type="string", length=10, nullable=true)
     */
    private $logoAlign = 'left';

    /**
     * @var integer
     *
     * @ORM\Column(name="version", type="integer", nullable=true)
     */
    private $version = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="remote_hash", type="string", length=255, nullable=true)
     */
    private $remoteHash;

    /**
     * @var \foreup\rest\models\entities\ForeupCourses
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $course;



    /**
     * Get campaignId
     *
     * @return integer
     */
    public function getCampaignId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupMarketingCampaigns
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return ForeupMarketingCampaigns
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    
        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return ForeupMarketingCampaigns
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content2
     *
     * @param string $content2
     *
     * @return ForeupMarketingCampaigns
     */
    public function setContent2($content2)
    {
        $this->content2 = $content2;
    
        return $this;
    }

    /**
     * Get content2
     *
     * @return string
     */
    public function getContent2()
    {
        return $this->content2;
    }

    /**
     * Set content3
     *
     * @param string $content3
     *
     * @return ForeupMarketingCampaigns
     */
    public function setContent3($content3)
    {
        $this->content3 = $content3;
    
        return $this;
    }

    /**
     * Get content3
     *
     * @return string
     */
    public function getContent3()
    {
        return $this->content3;
    }

    /**
     * Set content4
     *
     * @param string $content4
     *
     * @return ForeupMarketingCampaigns
     */
    public function setContent4($content4)
    {
        $this->content4 = $content4;
    
        return $this;
    }

    /**
     * Get content4
     *
     * @return string
     */
    public function getContent4()
    {
        return $this->content4;
    }

    /**
     * Set content5
     *
     * @param string $content5
     *
     * @return ForeupMarketingCampaigns
     */
    public function setContent5($content5)
    {
        $this->content5 = $content5;
    
        return $this;
    }

    /**
     * Get content5
     *
     * @return string
     */
    public function getContent5()
    {
        return $this->content5;
    }

    /**
     * Set content6
     *
     * @param string $content6
     *
     * @return ForeupMarketingCampaigns
     */
    public function setContent6($content6)
    {
        $this->content6 = $content6;
    
        return $this;
    }

    /**
     * Get content6
     *
     * @return string
     */
    public function getContent6()
    {
        return $this->content6;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return ForeupMarketingCampaigns
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set images
     *
     * @param string $images
     *
     * @return ForeupMarketingCampaigns
     */
    public function setImages($images)
    {
        $this->images = $images;
    
        return $this;
    }

    /**
     * Get images
     *
     * @return string
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set image2
     *
     * @param string $image2
     *
     * @return ForeupMarketingCampaigns
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
    
        return $this;
    }

    /**
     * Get image2
     *
     * @return string
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * Set image3
     *
     * @param string $image3
     *
     * @return ForeupMarketingCampaigns
     */
    public function setImage3($image3)
    {
        $this->image3 = $image3;
    
        return $this;
    }

    /**
     * Get image3
     *
     * @return string
     */
    public function getImage3()
    {
        return $this->image3;
    }

    /**
     * Set image4
     *
     * @param string $image4
     *
     * @return ForeupMarketingCampaigns
     */
    public function setImage4($image4)
    {
        $this->image4 = $image4;
    
        return $this;
    }

    /**
     * Get image4
     *
     * @return string
     */
    public function getImage4()
    {
        return $this->image4;
    }

    /**
     * Set sendDate
     *
     * @param \DateTime $sendDate
     *
     * @return ForeupMarketingCampaigns
     */
    public function setSendDate($sendDate)
    {
        $this->sendDate = $sendDate;
    
        return $this;
    }

    /**
     * Get sendDate
     *
     * @return \DateTime
     */
    public function getSendDate()
    {
        return $this->sendDate;
    }

    /**
     * Set sendDateCst
     *
     * @param \DateTime $sendDateCst
     *
     * @return ForeupMarketingCampaigns
     */
    public function setSendDateCst($sendDateCst)
    {
        $this->sendDateCst = $sendDateCst;
    
        return $this;
    }

    /**
     * Get sendDateCst
     *
     * @return \DateTime
     */
    public function getSendDateCst()
    {
        return $this->sendDateCst;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ForeupMarketingCampaigns
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set recipients
     *
     * @param string $recipients
     *
     * @return ForeupMarketingCampaigns
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
    
        return $this;
    }

    /**
     * Get recipients
     *
     * @return string
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * Set recipientCount
     *
     * @param integer $recipientCount
     *
     * @return ForeupMarketingCampaigns
     */
    public function setRecipientCount($recipientCount)
    {
        $this->recipientCount = $recipientCount;
    
        return $this;
    }

    /**
     * Get recipientCount
     *
     * @return integer
     */
    public function getRecipientCount()
    {
        return $this->recipientCount;
    }

    /**
     * Set personId
     *
     * @param integer $personId
     *
     * @return ForeupMarketingCampaigns
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    
        return $this;
    }

    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return ForeupMarketingCampaigns
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    
        return $this;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set d
     *
     * @param integer $d
     *
     * @return ForeupMarketingCampaigns
     */
    public function setD($d)
    {
        $this->d = $d;
    
        return $this;
    }

    /**
     * Get d
     *
     * @return integer
     */
    public function getD()
    {
        return $this->d;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ForeupMarketingCampaigns
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ForeupMarketingCampaigns
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return ForeupMarketingCampaigns
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set templatePath
     *
     * @param string $templatePath
     *
     * @return ForeupMarketingCampaigns
     */
    public function setTemplatePath($templatePath)
    {
        $this->templatePath = $templatePath;
    
        return $this;
    }

    /**
     * Get templatePath
     *
     * @return string
     */
    public function getTemplatePath()
    {
        return $this->templatePath;
    }

    /**
     * Set logoPath
     *
     * @param string $logoPath
     *
     * @return ForeupMarketingCampaigns
     */
    public function setLogoPath($logoPath)
    {
        $this->logoPath = $logoPath;
    
        return $this;
    }

    /**
     * Get logoPath
     *
     * @return string
     */
    public function getLogoPath()
    {
        return $this->logoPath;
    }

    /**
     * Set header
     *
     * @param string $header
     *
     * @return ForeupMarketingCampaigns
     */
    public function setHeader($header)
    {
        $this->header = $header;
    
        return $this;
    }

    /**
     * Get header
     *
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set queued
     *
     * @param boolean $queued
     *
     * @return ForeupMarketingCampaigns
     */
    public function setQueued($queued)
    {
        $this->queued = $queued;
    
        return $this;
    }

    /**
     * Get queued
     *
     * @return boolean
     */
    public function getQueued()
    {
        return $this->queued;
    }

    /**
     * Set isSent
     *
     * @param integer $isSent
     *
     * @return ForeupMarketingCampaigns
     */
    public function setIsSent($isSent)
    {
        $this->isSent = $isSent;
    
        return $this;
    }

    /**
     * Get isSent
     *
     * @return integer
     */
    public function getIsSent()
    {
        return $this->isSent;
    }

    /**
     * Set reported
     *
     * @param boolean $reported
     *
     * @return ForeupMarketingCampaigns
     */
    public function setReported($reported)
    {
        $this->reported = $reported;
    
        return $this;
    }

    /**
     * Get reported
     *
     * @return boolean
     */
    public function getReported()
    {
        return $this->reported;
    }

    /**
     * Set attempts
     *
     * @param boolean $attempts
     *
     * @return ForeupMarketingCampaigns
     */
    public function setAttempts($attempts)
    {
        $this->attempts = $attempts;
    
        return $this;
    }

    /**
     * Get attempts
     *
     * @return boolean
     */
    public function getAttempts()
    {
        return $this->attempts;
    }

    /**
     * Set runTime
     *
     * @param \DateTime $runTime
     *
     * @return ForeupMarketingCampaigns
     */
    public function setRunTime($runTime)
    {
        $this->runTime = $runTime;
    
        return $this;
    }

    /**
     * Get runTime
     *
     * @return \DateTime
     */
    public function getRunTime()
    {
        return $this->runTime;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupMarketingCampaigns
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set promotionId
     *
     * @param string $promotionId
     *
     * @return ForeupMarketingCampaigns
     */
    public function setPromotionId($promotionId)
    {
        $this->promotionId = $promotionId;
    
        return $this;
    }

    /**
     * Get promotionId
     *
     * @return string
     */
    public function getPromotionId()
    {
        return $this->promotionId;
    }

    /**
     * Set marketingTemplateId
     *
     * @param integer $marketingTemplateId
     *
     * @return ForeupMarketingCampaigns
     */
    public function setMarketingTemplateId($marketingTemplateId)
    {
        $this->marketingTemplateId = $marketingTemplateId;
    
        return $this;
    }

    /**
     * Get marketingTemplateId
     *
     * @return integer
     */
    public function getMarketingTemplateId()
    {
        return $this->marketingTemplateId;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupMarketingCampaigns
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set fromName
     *
     * @param string $fromName
     *
     * @return ForeupMarketingCampaigns
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
    
        return $this;
    }

    /**
     * Get fromName
     *
     * @return string
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * Set fromEmail
     *
     * @param string $fromEmail
     *
     * @return ForeupMarketingCampaigns
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;
    
        return $this;
    }

    /**
     * Get fromEmail
     *
     * @return string
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * Set replyTo
     *
     * @param string $replyTo
     *
     * @return ForeupMarketingCampaigns
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;
    
        return $this;
    }

    /**
     * Get replyTo
     *
     * @return string
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * Set renderedHtml
     *
     * @param string $renderedHtml
     *
     * @return ForeupMarketingCampaigns
     */
    public function setRenderedHtml($renderedHtml)
    {
        $this->renderedHtml = $renderedHtml;
    
        return $this;
    }

    /**
     * Get renderedHtml
     *
     * @return string
     */
    public function getRenderedHtml()
    {
        return $this->renderedHtml;
    }

    /**
     * Set jsonContent
     *
     * @param string $jsonContent
     *
     * @return ForeupMarketingCampaigns
     */
    public function setJsonContent($jsonContent)
    {
        $this->jsonContent = $jsonContent;
    
        return $this;
    }

    /**
     * Get jsonContent
     *
     * @return string
     */
    public function getJsonContent()
    {
        return $this->jsonContent;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return ForeupMarketingCampaigns
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    
        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set logoAlign
     *
     * @param string $logoAlign
     *
     * @return ForeupMarketingCampaigns
     */
    public function setLogoAlign($logoAlign)
    {
        $this->logoAlign = $logoAlign;
    
        return $this;
    }

    /**
     * Get logoAlign
     *
     * @return string
     */
    public function getLogoAlign()
    {
        return $this->logoAlign;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return ForeupMarketingCampaigns
     */
    public function setVersion($version)
    {
        $this->version = $version;
    
        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set remoteHash
     *
     * @param string $remoteHash
     *
     * @return ForeupMarketingCampaigns
     */
    public function setRemoteHash($remoteHash)
    {
        $this->remoteHash = $remoteHash;
    
        return $this;
    }

    /**
     * Get remoteHash
     *
     * @return string
     */
    public function getRemoteHash()
    {
        return $this->remoteHash;
    }

    /**
     * Set course
     *
     * @param \foreup\rest\models\entities\ForeupCourses $course
     *
     * @return ForeupMarketingCampaigns
     */
    public function setCourse(\foreup\rest\models\entities\ForeupCourses $course = null)
    {
        $this->course = $course;
    
        return $this;
    }

    /**
     * Get course
     *
     * @return \foreup\rest\models\entities\ForeupCourses
     */
    public function getCourse()
    {
        return $this->course;
    }
}
