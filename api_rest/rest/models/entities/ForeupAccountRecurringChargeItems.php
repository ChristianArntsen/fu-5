<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountRecurringChargeItems
 *
 * @ORM\Table(name="foreup_account_recurring_charge_items", indexes={@ORM\Index(name="item_id", columns={"item_id"}), @ORM\Index(name="invoice_id", columns={"recurring_charge_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupAccountRecurringChargeItems
{
	private $last_error;

	public function getLastError(){
		return $this->last_error;
	}

	public function resetLastError(){
		$this->last_error = null;
	}

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true) {
		//
		$lineNumber = $this->getLineNumber();
		if(!isset($lineNumber)){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: lineNumber cannot be NULL';
			return $this->invalid($throw);
		}
		if(!is_numeric($lineNumber)||!is_int($lineNumber*1)){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: lineNumber must be an integer: '.$lineNumber;
			return $this->invalid($throw);
		}

		$repeated = $this->getRepeated();
		if(isset($repeated) && !is_a($repeated,'foreup\rest\models\entities\ForeupRepeatable')){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: repeated must be ForeupRepeatable entity';
			return $this->invalid($throw);
		}

		$itemId = $this->getItemId();
		if(!isset($itemId)){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: itemId cannot be NULL';
			return $this->invalid($throw);
		}
		if(!is_numeric($itemId)||!is_int($itemId*1)){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: itemId must be an integer: '.$itemId;
			return $this->invalid($throw);
		}

		$itemType = $this->getItemType();
		$itemTypeEnum = array('item');
		if(!in_array($itemType,$itemTypeEnum)){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: itemType "'.$itemType.'" not found in enumerated values: '.json_encode($itemTypeEnum);
			return $this->invalid($throw);
		}

		$overridePrice = $this->getOverridePrice();
		if(isset($overridePrice) && !is_numeric($overridePrice)){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: overridePrice must be numeric: '.$overridePrice;
			return $this->invalid($throw);
		}

		$discountPercent = $this->getDiscountPercent();
		if(!isset($discountPercent)){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: discountPercent cannot be NULL';
			return $this->invalid($throw);
		}
		if(!is_numeric($discountPercent)){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: discountPercent must be numeric: '.$discountPercent;
			return $this->invalid($throw);
		}

		$quantity = $this->getQuantity();
		if(!isset($quantity)){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: quantity cannot be NULL';
			return $this->invalid($throw);
		}
		if(!is_numeric($quantity)){
			$this->last_error = 'ForeupaccountRecurringChargeItems->validate Error: quantity must be numeric: '.$quantity;
			return $this->invalid($throw);
		}

		return true;
	}

	private function invalid($throw = true){
		if($throw)throw new \InvalidArgumentException($this->last_error);
		else return $this->last_error;
	}


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="line_number", type="integer", nullable=false)
     */
    private $lineNumber = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false)
     */
    private $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="item_type", type="string", nullable=false)
     */
    private $itemType = 'item';

    /**
     * @var string
     *
     * @ORM\Column(name="override_price", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $overridePrice;

    /**
     * @var string
     *
     * @ORM\Column(name="discount_percent", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $discountPercent = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="quantity", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $quantity = '1.00';

    /**
     * @var \foreup\rest\models\entities\ForeupAccountRecurringCharges
     *
     * @ORM\OneToOne(targetEntity="ForeupAccountRecurringCharges", mappedBy="id")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recurring_charge_id", referencedColumnName="id")
     * })
     */
    private $recurringCharge;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="recurring_charge_id", type="integer", nullable=false)
	 */
	private $recurringChargeId;

	/**
	 * @var \foreup\rest\models\entities\ForeupRepeatable
	 *
	 * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupRepeatable",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="repeated", referencedColumnName="id")
	 * })
	 */
	private $repeated;

	/**
	 * @var \foreup\rest\models\entities\ForeupItems
	 *
	 * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupItems",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="item_id", referencedColumnName="item_id")
	 * })
	 */
	private $item;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


	/**
	 * @return \foreup\rest\models\entities\ForeupRepeatable
	 */
	public function getRepeated()
	{
		return $this->repeated;
	}

	/**
	 * @param \foreup\rest\models\entities\ForeupRepeatable $repeated
	 *
	 * @return ForeupAccountRecurringChargeItems
	 */
	public function setRepeated(\foreup\rest\models\entities\ForeupRepeatable $repeated = null)
	{
		$this->repeated = $repeated;
		return $this;
	}

    /**
     * Set lineNumber
     *
     * @param integer $lineNumber
     *
     * @return ForeupAccountRecurringChargeItems
     */
    public function setLineNumber($lineNumber)
    {
        $this->lineNumber = $lineNumber;

        return $this;
    }

    /**
     * Get lineNumber
     *
     * @return integer
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

    /**
     * Get itemId
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->itemId;
    }


	/**
	 * @return \foreup\rest\models\entities\ForeupItems
	 */
	public function getItem()
	{
		return $this->item;
	}

	/**
	 * @param \foreup\rest\models\entities\ForeupItems $item
	 *
	 * @return ForeupAccountRecurringChargeItems
	 */
	public function setItem(\foreup\rest\models\entities\ForeupItems $item)
	{
		$this->itemId = $item->getItemId();
		$this->item = $item;
		return $this;
	}

    /**
     * Set itemType
     *
     * @param string $itemType
     *
     * @return ForeupAccountRecurringChargeItems
     */
    public function setItemType($itemType)
    {
        $this->itemType = $itemType;

        return $this;
    }

    /**
     * Get itemType
     *
     * @return string
     */
    public function getItemType()
    {
        return $this->itemType;
    }

    /**
     * Set overridePrice
     *
     * @param string $overridePrice
     *
     * @return ForeupAccountRecurringChargeItems
     */
    public function setOverridePrice($overridePrice)
    {
        $this->overridePrice = $overridePrice;

        return $this;
    }

    /**
     * Get overridePrice
     *
     * @return string
     */
    public function getOverridePrice()
    {
        return $this->overridePrice;
    }

    /**
     * Set discountPercent
     *
     * @param string $discountPercent
     *
     * @return ForeupAccountRecurringChargeItems
     */
    public function setDiscountPercent($discountPercent)
    {
        $this->discountPercent = $discountPercent;

        return $this;
    }

    /**
     * Get discountPercent
     *
     * @return string
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return ForeupAccountRecurringChargeItems
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set recurringCharge
     *
     * @param \foreup\rest\models\entities\ForeupAccountRecurringCharges $recurringCharge
     *
     * @return ForeupAccountRecurringChargeItems
     */
    public function setRecurringCharge(\foreup\rest\models\entities\ForeupAccountRecurringCharges $recurringCharge = null)
    {
        $this->recurringCharge = $recurringCharge;
        $this->recurringChargeId = $recurringCharge->getId();

        return $this;
    }

    /**
     * Get recurringCharge
     *
     * @return \foreup\rest\models\entities\ForeupAccountRecurringCharges
     */
    public function getRecurringCharge()
    {
        return $this->recurringCharge;
    }
}
