<?php
namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountStatementPayments
 *
 * @ORM\Table(name="foreup_account_statement_payments")
 * @ORM\Entity
 */
class ForeupAccountStatementPayments
{
    use \foreup\rest\models\entities\EntityValidator;

    /**
     * @ORM\PrePersist @ORM\PreUpdate
     */
    public function validate($throw = true)
    {

        $this->resetLastError();
        $location = 'ForeupAccountStatementPayments->validate';

        $value = $this->getAmount();
        $v = $this->validate_numeric($location,'amount',$value,true,$throw);
        if($v !== true) return $v;

        $value = $this->getType();
        $valid = ['credit_card','ach','pos'];
        $v = $this->validate_enum($location,'type',$value,$valid,true,$throw);
        if($v!==true)return $v;

        $value = $this->getSubType();
        $valid = ['visa','mastercard','discover','jcb','diners','amex'];
        $v = $this->validate_enum($location,'subType',$value,$valid,false,$throw);
        if($v!==true)return $v;

        $value = $this->getAccountNumber();
        $v = $this->validate_string($location,'accountNumber',$value,false,$throw);
        if($v !== true) return $v;

        $value = $this->getName();
        $v = $this->validate_string($location,'name',$value,false,$throw);
        if($v !== true) return $v;

        $value = $this->getIsAutopayment();
        $v = $this->validate_boolean($location,'isAutopayment',$value,true,$throw);
        if($v !== true) return $v;

        $value = $this->getDateCreated();
        $v = $this->validate_object($location,'dateCreated',$value,'\DateTime',true,$throw);
        if($v !== true) return $v;

        return true;

    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var \foreup\rest\models\entities\ForeupCourses
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses",fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $course;

    /**
     * @var \foreup\rest\models\entities\ForeupAccountStatements
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountStatements")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statement_id", referencedColumnName="id")
     * })
     */
    private $statement;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $amount = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    private $type = 'credit_card';

    /**
     * @var string
     *
     * @ORM\Column(name="sub_type", type="string", nullable=true)
     */
    private $subType;

    /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=64, nullable=true)
     */
    private $accountNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=4096, nullable=true)
     */
    private $name;

    /**
     * @var \foreup\rest\models\entities\ForeupSales
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupSales", inversedBy="items")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sale_id", referencedColumnName="sale_id")
     * })
     */
    private $sale;

    /**
     * @var \foreup\rest\models\entities\ForeupSalesPaymentsCreditCards
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupSalesPaymentsCreditCards")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="charge_invoice_id", referencedColumnName="invoice")
     * })
     */
    private $chargeInvoice;

    /**
     * @var \foreup\rest\models\entities\ForeupCustomers
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCustomers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="person_id"),
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $customer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_autopayment", type="boolean", nullable=false)
     */
    private $isAutopayment = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupAccountStatementPayments
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set statement
     *
     * @param \foreup\rest\models\entities\ForeupAccountStatements $statement
     *
     * @return ForeupAccountStatementPayments
     */
    public function setStatement(\foreup\rest\models\entities\ForeupAccountStatements $statement = null)
    {
        $this->statement = $statement;

        return $this;
    }

    /**
     * Get statement
     *
     * @return \foreup\rest\models\entities\ForeupAccountStatements
     */
    public function getStatement()
    {
        return $this->statement;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return ForeupAccountStatementPayments
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupAccountStatementPayments
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set subType
     *
     * @param string|null $type
     *
     * @return ForeupAccountStatementPayments
     */
    public function setSubType($type)
    {
        $this->subType = $type;

        return $this;
    }

    /**
     * Get subType
     *
     * @return string
     */
    public function getSubType()
    {
        return $this->subType;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return ForeupAccountStatementPayments
     */
    public function setAccountNumber($accountNumber = null)
    {
        // last 4 of account number
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupAccountStatementPayments
     */
    public function setName($name = null)
    {
        // name on account, like customer name on credit card
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sale
     *
     * @param \foreup\rest\models\entities\ForeupSales $sale
     *
     * @return ForeupAccountStatementPayments
     */
    public function setSale(\foreup\rest\models\entities\ForeupSales $sale = null)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * Get sale
     *
     * @return \foreup\rest\models\entities\ForeupSales
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * Set chargeInvoice
     *
     * @param \foreup\rest\models\entities\ForeupSalesPaymentsCreditCards $charge
     *
     * @return ForeupAccountStatementPayments
     */
    public function setChargeInvoice(\foreup\rest\models\entities\ForeupSalesPaymentsCreditCards $charge = null)
    {
        $this->chargeInvoice = $charge;

        return $this;
    }

    /**
     * Get chargeinvoice
     *
     * @return \foreup\rest\models\entities\ForeupSalesPaymentsCreditCards
     */
    public function getChargeInvoice()
    {
        return $this->chargeInvoice;
    }



    /**
     * Set customer
     *
     * @param \foreup\rest\models\entities\ForeupCustomers $person
     *
     * @return ForeupAccountStatementPayments
     */
    public function setCustomer(\foreup\rest\models\entities\ForeupCustomers $person = null)
    {
        $this->customer = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \foreup\rest\models\entities\ForeupCustomers
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set isAutopayment
     *
     * @param boolean $isAutopayment
     *
     * @return ForeupAccountStatementPayments
     */
    public function setIsAutopayment($isAutopayment)
    {
        if(is_string($isAutopayment)){
            $financeChargeEnabled = strtolower($isAutopayment);
            $is=json_decode(strtolower($isAutopayment),true);
            if(isset($is))$isAutopayment=$is;
        }
        $this->isAutopayment = $isAutopayment;

        return $this;
    }

    /**
     * Get isAutopayment
     *
     * @return boolean
     */
    public function getIsAutopayment()
    {
        return $this->isAutopayment;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupAccountStatementPayments
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * getReadableType
     *
     * @return string
     */
    public function getReadableType(){

        $string = '';
        switch($this->getType()){
            case 'pos':
                $string = 'Sale #'.$this->getSale()->getSaleId();
                break;
            case 'ach':
                $string = 'ACH ******'.$this->getAccountNumber();
                break;
            case 'credit_card':

                if($this->getSubType() == 'visa'){
                    $string = 'VISA';
                }else if($this->getSubType() == 'mastercard'){
                    $string = 'MasterCard';
                }else if($this->getSubType() == 'discover'){
                    $string = 'Discover';
                }else if($this->getSubType() == 'jcb'){
                    $string = 'JCB';
                }else if($this->getSubType() == 'diners'){
                    $string = 'Diners';
                }else if($this->getSubType() == 'amex'){
                    $string = 'AmericanExpress';
                }

                $string .= ' '.$this->getAccountNumber();
        }

        if($this->getIsAutopayment()){
            $string = '(Autopayment) '.$string;
        }

        return $string;
    }

    /**
     * @return ForeupCourses
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param ForeupCourses $course
     */
    public function setCourse(ForeupCourses $course)
    {
        $this->course = $course;
        $this->courseId = $course->getCourseId();
        return $this;
    }
}