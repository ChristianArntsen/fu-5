<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupSalesPayments
 *
 * @ORM\Table(name="foreup_sales_payments", indexes={@ORM\Index(name="sale_id", columns={"sale_id"}), @ORM\Index(name="invoice_id", columns={"invoice_id"})})
 * @ORM\Entity
 */
class ForeupSalesPayments
{
	public function __construct()
	{
	}

    /**
     * @var string
     *
     * @ORM\Column(name="payment_type", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $paymentType;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32, nullable=true)
     */
    private $type;


    /**
     * @var float
     *
     * @ORM\Column(name="payment_amount", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $paymentAmount;


	/**
	 * @var \foreup\rest\models\entities\ForeupPeople
	 *
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPeople",inversedBy="personId")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="tip_recipient", referencedColumnName="person_id")
	 * })
	 */
    private $tipRecipient;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="invoice_id", type="integer", nullable=false)
	 */
	private $invoiceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="record_id", type="integer", nullable=false)
     */
    private $recordId;


    /**
     * @var boolean
     *
     * @ORM\Column(name="is_tip", type="boolean", nullable=true)
     */
    private $isTip;

    /**
     * @var \foreup\rest\models\entities\ForeupSales
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupSales")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sale_id", referencedColumnName="sale_id")
     * })
     */
    private $sale;

	private $validationMessage;

	/**
	 * @return mixed
	 */
	public function getValidationMessage()
	{
		return $this->validationMessage;
	}

	/**
	 * @param mixed $validationMessage
	 */
	public function setValidationMessage($validationMessage)
	{
		$this->validationMessage = $validationMessage;
	}



    /**
     * Set paymentType
     *
     * @param string $paymentType
     *
     * @return ForeupSalesPayments
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupSalesPayments
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set paymentAmount
     *
     * @param string $paymentAmount
     *
     * @return ForeupSalesPayments
     */
    public function setPaymentAmount($paymentAmount)
    {
        $this->paymentAmount = $paymentAmount;

        return $this;
    }

    /**
     * Get paymentAmount
     *
     * @return float
     */
    public function getPaymentAmount()
    {
        return $this->paymentAmount;
    }

    /**
     * Set invoiceId
     *
     * @param integer $invoiceId
     *
     * @return ForeupSalesPayments
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
	    $this->recordId = $invoiceId;

        return $this;
    }

    /**
     * Get invoiceId
     *
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * Set tipRecipient
     *
     * @param integer $tipRecipient
     *
     * @return ForeupSalesPayments
     */
    public function setTipRecipient($tipRecipient)
    {
        $this->tipRecipient = $tipRecipient;

        return $this;
    }

    /**
     * Get tipRecipient
     *
     * @return ForeupPeople
     */
    public function getTipRecipient()
    {
        return $this->tipRecipient;
    }


    /**
     * Set isTip
     *
     * @param boolean $isTip
     *
     * @return ForeupSalesPayments
     */
    public function setIsTip($isTip)
    {
        $this->isTip = $isTip;

        return $this;
    }

    /**
     * Get isTip
     *
     * @return boolean
     */
    public function getIsTip()
    {
        return $this->isTip;
    }

    /**
     * Set sale
     *
     * @param \foreup\rest\models\entities\ForeupSales $sale
     *
     * @return ForeupSalesPayments
     */
    public function setSale(\foreup\rest\models\entities\ForeupSales $sale)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * Get sale
     *
     * @return \foreup\rest\models\entities\ForeupSales
     */
    public function getSale()
    {
        return $this->sale;
    }


	public function isValid()
	{
		$validTypes = [
			"credit_card","cash","change","member_account","gift_card","punch_card"
		];
		if(!in_array($this->getType(),$validTypes)){
			$this->validationMessage = "Invalid payment type, valid payment types include: ".implode(",",$validTypes);
			return false;
		}
		return true;
	}
}
