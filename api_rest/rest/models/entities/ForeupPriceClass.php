<?php

namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupTerminals
 *
 * @ORM\Table(name="foreup_price_classes")
 * @ORM\Entity
 */
class ForeupPriceClass
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="class_id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=true)
	 */
	private $name = "";

    /**
     * @var boolean
     *
     * @ORM\Column(name="`default`", type="boolean", nullable=false)
     */
    private $default = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="`cart`", type="boolean", nullable=false)
     */
    private $cart = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="`is_shared`", type="boolean", nullable=false)
     */
    private $is_shared = false;

    /**
	 * @var string
	 *
	 * @ORM\Column(name="color", type="string", length=255, nullable=true)
	 */
	private $color;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="date_created", type="datetime", nullable=false)
	 */
	private $date_created ;

	/**
	 * @var \foreup\rest\models\entities\ForeupCourses
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $course;

    /**
     * @var string
     *
     * @ORM\Column(name="cart_gl_code", type="string", length=255, nullable=true)
     */
    private $cart_gl_code = "";

    /**
     * @var string
     *
     * @ORM\Column(name="green_gl_code", type="string", length=255, nullable=true)
     */
    private $green_gl_code = "";

    public function __construct(ForeupCourses $course)
	{
		$this->date_created = new \DateTime();
		$this->course = $course;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

    /**
     * @return boolean
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param boolean $default
     */
    public function setDefault($default)
    {
        $this->default = $default;
    }

    /**
     * @return boolean
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param boolean $cart
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return boolean
     */
    public function getIsShared()
    {
        return $this->is_shared;
    }

    /**
     * @param boolean $is_shared
     */
    public function setIsShared($is_shared)
    {
        $this->is_shared = $is_shared;
    }

    /**
	 * @return string
	 */
	public function getColor()
	{
		return $this->color;
	}

	/**
	 * @param string $color
	 */
	public function setColor($color)
	{
		if(!preg_match("/#([a-fA-F0-9]{3}){1,2}\b/",$color) && !empty($color))
			throw new \Exception("Invalid hexidecimal color. Must be either 3 or 6 characters.");
		$this->color = $color;
	}

	/**
	 * @return string
	 */
	public function getDateCreated()
	{
		return $this->date_created;
	}

	/**
	 * @param string $date_created
	 */
	public function setDateCreated($date_created)
	{
		$this->date_created = $date_created;
	}

	/**
	 * @return ForeupCourses
	 */
	public function getCourse()
	{
		return $this->course;
	}

	/**
	 * @param ForeupCourses $course
	 */
	public function setCourse($course)
	{
		$this->course = $course;
	}

    /**
     * @return string
     */
    public function getCartGlCode()
    {
        return $this->cart_gl_code;
    }

    /**
     * @param string $cart_gl_code
     */
    public function setCartGlCode($cart_gl_code)
    {
        $this->cart_gl_code = $cart_gl_code;
    }

    /**
     * @return string
     */
    public function getGreenGlCode()
    {
        return $this->green_gl_code;
    }

    /**
     * @param string $green_gl_code
     */
    public function setGreenGlCode($green_gl_code)
    {
        $this->green_gl_code = $green_gl_code;
    }


}
