<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupCourseGroups
 *
 * @ORM\Table(name="foreup_course_groups")
 * @ORM\Entity
 */
class ForeupCourseGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $groupId;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var boolean
     *
     * @ORM\Column(name="shared_tee_sheet", type="boolean", nullable=false)
     */
    private $sharedTeeSheet = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="shared_customers", type="boolean", nullable=false)
     */
    private $sharedCustomers = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="shared_giftcards", type="boolean", nullable=false)
     */
    private $sharedGiftcards = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="shared_price_classes", type="boolean", nullable=false)
     */
    private $sharedPriceClasses = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="aggregate_booking", type="boolean", nullable=false)
     */
    private $aggregateBooking = '0';

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="foreup\rest\models\entities\ForeupCourses", inversedBy="group")
     * @ORM\JoinTable(name="foreup_course_group_members",
     *   joinColumns={
     *     @ORM\JoinColumn(name="group_id", referencedColumnName="group_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     *   }
     * )
     */
    private $course;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->course = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function hasPermission($permission)
    {
    	if($permission == "customers"){
    		return $this->getSharedCustomers();
	    } else if($permission == "giftcards"){
		    return $this->getSharedGiftcards();
	    } else if($permission == "priceclasses"){
		    return $this->getSharedPriceClasses();
	    } else if($permission == "teesheet"){
		    return $this->getSharedTeeSheet();
	    }

	    return false;
    }

    /**
     * Get groupId
     *
     * @return integer
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return ForeupCourseGroups
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupCourseGroups
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set sharedTeeSheet
     *
     * @param boolean $sharedTeeSheet
     *
     * @return ForeupCourseGroups
     */
    public function setSharedTeeSheet($sharedTeeSheet)
    {
        $this->sharedTeeSheet = $sharedTeeSheet;

        return $this;
    }

    /**
     * Get sharedTeeSheet
     *
     * @return boolean
     */
    public function getSharedTeeSheet()
    {
        return $this->sharedTeeSheet;
    }

    /**
     * Set sharedCustomers
     *
     * @param boolean $sharedCustomers
     *
     * @return ForeupCourseGroups
     */
    public function setSharedCustomers($sharedCustomers)
    {
        $this->sharedCustomers = $sharedCustomers;

        return $this;
    }

    /**
     * Get sharedCustomers
     *
     * @return boolean
     */
    public function getSharedCustomers()
    {
        return $this->sharedCustomers;
    }

    /**
     * Set sharedGiftcards
     *
     * @param boolean $sharedGiftcards
     *
     * @return ForeupCourseGroups
     */
    public function setSharedGiftcards($sharedGiftcards)
    {
        $this->sharedGiftcards = $sharedGiftcards;

        return $this;
    }

    /**
     * Get sharedGiftcards
     *
     * @return boolean
     */
    public function getSharedGiftcards()
    {
        return $this->sharedGiftcards;
    }

    /**
     * Set sharedPriceClasses
     *
     * @param boolean $sharedPriceClasses
     *
     * @return ForeupCourseGroups
     */
    public function setSharedPriceClasses($sharedPriceClasses)
    {
        $this->sharedPriceClasses = $sharedPriceClasses;

        return $this;
    }

    /**
     * Get sharedPriceClasses
     *
     * @return boolean
     */
    public function getSharedPriceClasses()
    {
        return $this->sharedPriceClasses;
    }

    /**
     * Set aggregateBooking
     *
     * @param boolean $aggregateBooking
     *
     * @return ForeupCourseGroups
     */
    public function setAggregateBooking($aggregateBooking)
    {
        $this->aggregateBooking = $aggregateBooking;

        return $this;
    }

    /**
     * Get aggregateBooking
     *
     * @return boolean
     */
    public function getAggregateBooking()
    {
        return $this->aggregateBooking;
    }

    /**
     * Add course
     *
     * @param \foreup\rest\models\entities\ForeupCourses $course
     *
     * @return ForeupCourseGroups
     */
    public function addCourse(\foreup\rest\models\entities\ForeupCourses $course)
    {
        $this->course[] = $course;

        return $this;
    }

    /**
     * Remove course
     *
     * @param \foreup\rest\models\entities\ForeupCourses $course
     */
    public function removeCourse(\foreup\rest\models\entities\ForeupCourses $course)
    {
        $this->course->removeElement($course);
    }

    /**
     * Get course
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourses()
    {
        return $this->course;
    }
}
