<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupMobileAppUsers
 *
 * @ORM\Table(name="foreup_api_hooks")
 * @ORM\Entity
 */
class ForeupApiHooks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hook_url", type="string", length=255, nullable=true)
     */
    private $hookUrl;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="header_key", type="string", length=255, nullable=true)
	 */
	private $headerKey;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="header_value", type="string", length=255, nullable=true)
	 */
	private $headerValue;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="event", type="string", length=255, nullable=true)
	 */
	private $event;

    /**
     * @var \foreup\rest\models\entities\ForeupPeople
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="person_id")
     *
     */
    private $person;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getHookUrl()
	{
		return $this->hookUrl;
	}

	/**
	 * @param string $hookUrl
	 */
	public function setHookUrl($hookUrl)
	{
		$this->hookUrl = $hookUrl;
	}

	/**
	 * @return string
	 */
	public function getEvent()
	{
		return $this->event;
	}

	/**
	 * @param string $event
	 */
	public function setEvent($event)
	{
		$this->event = $event;
	}

	/**
	 * @return ForeupPeople
	 */
	public function getPerson()
	{
		return $this->person;
	}

	/**
	 * @param ForeupPeople $person
	 */
	public function setPerson($person)
	{
		$this->person = $person;
	}

	/**
	 * @return string
	 */
	public function getHeaderKey()
	{
		return $this->headerKey;
	}

	/**
	 * @param string $headerKey
	 */
	public function setHeaderKey($headerKey)
	{
		$this->headerKey = $headerKey;
	}

	/**
	 * @return string
	 */
	public function getHeaderValue()
	{
		return $this->headerValue;
	}

	/**
	 * @param string $headerValue
	 */
	public function setHeaderValue($headerValue)
	{
		$this->headerValue = $headerValue;
	}



}

