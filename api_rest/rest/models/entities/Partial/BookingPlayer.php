<?php
namespace foreup\rest\models\entities\Partial;
use Doctrine\ORM\EntityRepository;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupPriceClass;
use foreup\rest\models\entities\ForeupTeetime;

class BookingPlayer
{
	private $position = 1;
	private $teetime = null;
	public function __construct($position,ForeupTeetime $teetime)
	{
		$this->position = $position;
		$this->teetime = $teetime;
	}


	public function getId()
	{
		$id = "";
		$id .= $this->teetime->getTtid();
		$id .= "-";
		$id .= $this->position;
		return $id;
	}

	/**
	 * @return mixed
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param mixed $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}


	/**
	 * @return mixed
	 */
	public function getName()
	{
		$position = $this->position == 1 ? "" : $this->position;
		return $this->teetime->{"getPersonName".$position}();
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$position = $this->position == 1 ? "" : $this->position;
		return $this->teetime->{"setPersonName".$position}($name);
	}

	/**
	 * @return ForeupPeople
	 */
	public function getPerson()
	{
		return $this->teetime->{"getPerson".$this->position}();
	}

	/**
	 * @param ForeupPeople $person
	 */
	public function setPerson($person)
	{
		if(empty($person)){
			return $this->teetime->{"setPerson".$this->position}($person);
		}
		$this->setName($person->getLastName().", ".$person->getFirstName());
		return $this->teetime->{"setPerson".$this->position}($person);
	}

	/**
	 * @return mixed
	 */
	public function getCartPaid()
	{
		return $this->teetime->{"getCartPaid".$this->position}();
	}

	/**
	 * @param mixed $cartPaid
	 */
	public function setCartPaid($cartPaid)
	{
		return $this->teetime->{"setCartPaid".$this->position}($cartPaid);
	}

	/**
	 * @return mixed
	 */
	public function getNoShow()
	{
		return $this->teetime->{"getPersonNoShow".$this->position}();
	}

	/**
	 * @param mixed $noShow
	 */
	public function setNoShow($noShow)
	{
		return $this->teetime->{"setPersonNoShow".$this->position}($noShow);
	}

	/**
	 * @return mixed
	 */
	public function getPaid()
	{
		return $this->teetime->{"getPersonPaid".$this->position}();
	}

	/**
	 * @param mixed $paid
	 */
	public function setPaid($paid)
	{
		return $this->teetime->{"setPersonPaid".$this->position}($paid);
	}

	/**
	 * @return ForeupPriceClass
	 */
	public function getPriceClass()
	{
		return $this->teetime->{"getPriceClass".$this->position}();
	}

	/**
	 * @return integer
	 */
	public function getPriceClassId()
	{
		$id = $this->teetime->{"getPriceClassId".$this->position}();
		if(empty($id))
			return null;
		return $id;
	}

	/**
	 * @return integer
	 */
	public function getPersonId()
	{
		$position = $this->position == 1 ? "" : $this->position;
		$id =  $this->teetime->{"getPersonId".$position}();
		if(empty($id))
			return null;
		return $id;
	}

	public function setPersonId($id)
	{
		$position = $this->position == 1 ? "" : $this->position;
		return $this->teetime->{"setPersonId".$position}($id);
	}

	/**
	 * @param mixed $priceClass
	 */
	public function setPriceClass($priceClass)
	{
		return $this->teetime->{"setPriceClass".$this->position}($priceClass);
	}


}