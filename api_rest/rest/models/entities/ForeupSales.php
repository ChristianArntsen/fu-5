<?php

namespace foreup\rest\models\entities;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupSales
 *
 * @ORM\Table(name="foreup_sales", uniqueConstraints={@ORM\UniqueConstraint(name="mobile_guid", columns={"mobile_guid"})}, indexes={@ORM\Index(name="employee_id", columns={"employee_id"}), @ORM\Index(name="deleted", columns={"deleted"}), @ORM\Index(name="course_id", columns={"course_id"}), @ORM\Index(name="course_id_sale_time", columns={"course_id", "sale_time"}), @ORM\Index(name="course_sale", columns={"course_id", "sale_time"}), @ORM\Index(name="refunded_sale_id", columns={"refunded_sale_id"})})
 * @ORM\Entity
 */
class ForeupSales
{
	use EntityValidator;

    public function __construct()
    {
    	$this->cid = 0;
    	$this->overrideAuthorizationId = 0;
    	$this->comment = "";
    	$this->tableId = "";
    	$this->deletedBy = "";
    	$this->refundedSaleId = "";
    	$this->refundReason = "";
    	$this->refundComment = "";
    	$this->deletedAt = Carbon::create();
	    $this->teetimeId = 0;
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
        $this->payments = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * @var integer
     *
     * @ORM\Column(name="sale_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $saleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CID", type="integer", nullable=false)
     */
    private $cid;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;


    /**
     * @var \foreup\rest\models\entities\ForeupSalesItems $items
     *
     * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupSalesItems", mappedBy="sale",fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sale_id", referencedColumnName="sale_id")
     * })
     */
    private $items;

	/**
	 * @var \foreup\rest\models\entities\ForeupSalesPayments $payments
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupSalesPayments", mappedBy="sale")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="sale_id", referencedColumnName="sale_id")
	 * })
	 */
	private $payments;

	/**
	 * @var \foreup\rest\models\entities\ForeupAccountTransactions $accountTransactions
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupAccountTransactions", mappedBy="sale")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="sale_id", referencedColumnName="sale_id")
	 * })
	 */
	private $accountTransactions;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="customer_id", type="integer", nullable=true)
	 */
	private $customerId;
	/**
	 * @var \foreup\rest\models\entities\ForeupCustomers
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCustomers", inversedBy="sales",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="customer_id", referencedColumnName="person_id",nullable=true),
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $customer;


	/**
	 * @var \foreup\rest\models\entities\ForeupPeople
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="customer_id", referencedColumnName="person_id", nullable=true)
	 * })
	 */
	private $person;

	/**
	 * @var \foreup\rest\models\entities\ForeupEmployees
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="employee_id", referencedColumnName="person_id")
	 * })
	 */
	private $employee;

	/**
	 * @var \foreup\rest\models\entities\ForeupTerminals
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupTerminals")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="terminal_id", referencedColumnName="terminal_id")
	 * })
	 */
	private $terminal;

	/**
     * @var \DateTime
     *
     * @ORM\Column(name="sale_time", type="datetime", nullable=false)
     */
    private $saleTime = 'CURRENT_TIMESTAMP';

	/**
	 * @var \foreup\rest\models\entities\ForeupTeetime
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupTeetime")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="teetime_id", referencedColumnName="TTID")
	 * })
	 */
    private $teetime;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="teetime_id", type="string", length=255, nullable=true)
	 */
	private $teetimeId;

    /**
     * @var string
     *
     * @ORM\Column(name="employee_id", type="string", length=255, nullable=true)
     */
    private $employeeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="override_authorization_id", type="integer", nullable=false)
     */
    private $overrideAuthorizationId;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=false)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="table_id", type="integer", nullable=false)
     */
    private $tableId;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_type", type="string", length=255, nullable=true)
     */
    private $paymentType;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="integer", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted_by", type="integer", nullable=false)
     */
    private $deletedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=false)
     */
    private $deletedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_guid", type="string", length=255, nullable=true)
     */
    private $mobileGuid;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="guest_count", type="smallint", nullable=true)
     */
    private $guestCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="refunded_sale_id", type="integer", nullable=false)
     */
    private $refundedSaleId;

    /**
     * @var string
     *
     * @ORM\Column(name="refund_reason", type="string", length=255, nullable=false)
     */
    private $refundReason;

    /**
     * @var string
     *
     * @ORM\Column(name="refund_comment", type="string", length=255, nullable=false)
     */
    private $refundComment;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_note", type="string", length=1024, nullable=true)
     */
    private $customerNote;

	private $validationMessage;

	/**
	 * @return mixed
	 */
	public function getValidationMessage()
	{
		return $this->validationMessage;
	}

	/**
	 * @param mixed $validationMessage
	 */
	public function setValidationMessage($validationMessage)
	{
		$this->validationMessage = $validationMessage;
	}

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|ForeupSalesItems[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param ForeupSalesItems $item
     */
    public function addItems(ForeupSalesItems $item)
    {
        $item->setSale($this);
        $this->items[] = $item;

        return $this;
    }

	/**
	 * @return ForeupSalesPayments
	 */
	public function getPayments()
	{
		return $this->payments;
	}

	/**
	 * @param ForeupSalesPayments $payments
	 * @return ForeupSales $this
	 */
	public function addPayments($payments)
	{
		$payments->setSale($this);
		$this->payments [] = $payments;
		return $this;
	}

	/**
	 * @return ForeupAccountTransactions
	 */
	public function getAccountTransactions()
	{
		return $this->accountTransactions;
	}

	/**
	 * @param ForeupAccountTransactions $entity
	 * @return ForeupSales $this
	 */
	public function addAccountTransactions(ForeupAccountTransactions $entity)
	{
		$this->accountTransactions[] = $entity;
		return $this;
	}



    /**
     * Get saleId
     *
     * @return integer
     */
    public function getSaleId()
    {
        return $this->saleId;
    }

    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return ForeupSales
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    
        return $this;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupSales
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    
        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set saleNumber
     *
     * @param integer $saleNumber
     *
     * @return ForeupSales
     */
    public function setSaleNumber($saleNumber)
    {
        $this->saleNumber = $saleNumber;
    
        return $this;
    }

    /**
     * Get saleNumber
     *
     * @return integer
     */
    public function getSaleNumber()
    {
        return $this->saleNumber;
    }

    /**
     * Set saleTime
     *
     * @param \DateTime $saleTime
     *
     * @return ForeupSales
     */
    public function setSaleTime($saleTime)
    {
        $this->saleTime = $saleTime;
    
        return $this;
    }

    /**
     * Get saleTime
     *
     * @return \DateTime
     */
    public function getSaleTime()
    {
        return $this->saleTime;
    }


    /**
     * Set employeeId
     *
     * @param string $employeeId
     *
     * @return ForeupSales
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;
    
        return $this;
    }

    /**
     * Get employeeId
     *
     * @return string
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * Set overrideAuthorizationId
     *
     * @param integer $overrideAuthorizationId
     *
     * @return ForeupSales
     */
    public function setOverrideAuthorizationId($overrideAuthorizationId)
    {
        $this->overrideAuthorizationId = $overrideAuthorizationId;
    
        return $this;
    }

    /**
     * Get overrideAuthorizationId
     *
     * @return integer
     */
    public function getOverrideAuthorizationId()
    {
        return $this->overrideAuthorizationId;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ForeupSales
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set tableId
     *
     * @param integer $tableId
     *
     * @return ForeupSales
     */
    public function setTableId($tableId)
    {
        $this->tableId = $tableId;
    
        return $this;
    }

    /**
     * Get tableId
     *
     * @return integer
     */
    public function getTableId()
    {
        return $this->tableId;
    }

    /**
     * Set paymentType
     *
     * @param string $paymentType
     *
     * @return ForeupSales
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    
        return $this;
    }

    /**
     * Get paymentType
     *
     * @return string
     */
    public function getPaymentType()
    {
	    $toReturn = str_replace("<br />"," - ",$this->paymentType);
        return $toReturn;
    }

    /**
     * Set autoGratuity
     *
     * @param string $autoGratuity
     *
     * @return ForeupSales
     */
    public function setAutoGratuity($autoGratuity)
    {
        $this->autoGratuity = $autoGratuity;
    
        return $this;
    }

    /**
     * Get autoGratuity
     *
     * @return string
     */
    public function getAutoGratuity()
    {
        return $this->autoGratuity;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     *
     * @return ForeupSales
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set deletedBy
     *
     * @param integer $deletedBy
     *
     * @return ForeupSales
     */
    public function setDeletedBy($deletedBy)
    {
        $this->deletedBy = $deletedBy;
    
        return $this;
    }

    /**
     * Get deletedBy
     *
     * @return integer
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ForeupSales
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set mobileGuid
     *
     * @param string $mobileGuid
     *
     * @return ForeupSales
     */
    public function setMobileGuid($mobileGuid)
    {
        $this->mobileGuid = $mobileGuid;
    
        return $this;
    }

    /**
     * Get mobileGuid
     *
     * @return string
     */
    public function getMobileGuid()
    {
        return $this->mobileGuid;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return ForeupSales
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set guestCount
     *
     * @param integer $guestCount
     *
     * @return ForeupSales
     */
    public function setGuestCount($guestCount)
    {
        $this->guestCount = $guestCount;
    
        return $this;
    }

    /**
     * Get guestCount
     *
     * @return integer
     */
    public function getGuestCount()
    {
        return $this->guestCount;
    }

    /**
     * Set refundedSaleId
     *
     * @param integer $refundedSaleId
     *
     * @return ForeupSales
     */
    public function setRefundedSaleId($refundedSaleId)
    {
        $this->refundedSaleId = $refundedSaleId;
    
        return $this;
    }

    /**
     * Get refundedSaleId
     *
     * @return integer
     */
    public function getRefundedSaleId()
    {
        return $this->refundedSaleId;
    }

    /**
     * Set refundReason
     *
     * @param string $refundReason
     *
     * @return ForeupSales
     */
    public function setRefundReason($refundReason)
    {
        $this->refundReason = $refundReason;
    
        return $this;
    }

    /**
     * Get refundReason
     *
     * @return string
     */
    public function getRefundReason()
    {
        return $this->refundReason;
    }

    /**
     * Set refundComment
     *
     * @param string $refundComment
     *
     * @return ForeupSales
     */
    public function setRefundComment($refundComment)
    {
        $this->refundComment = $refundComment;
    
        return $this;
    }

    /**
     * Get refundComment
     *
     * @return string
     */
    public function getRefundComment()
    {
        return $this->refundComment;
    }

	/**
	 * @return ForeupEmployees
	 */
	public function getEmployee()
	{
		return $this->employee;
	}

	/**
	 * @param ForeupEmployees $employee
	 */
	public function setEmployee($employee)
	{
		$this->employee = $employee;
	}

	/**
	 * @return ForeupTerminals
	 */
	public function getTerminal()
	{
		return $this->terminal;
	}

	/**
	 * @param ForeupTerminals $terminal
	 */
	public function setTerminal($terminal)
	{
		$this->terminal = $terminal;
	}



    /**
     * Set customerNote
     *
     * @param string $customerNote
     *
     * @return ForeupSales
     */
    public function setCustomerNote($customerNote)
    {
        $this->customerNote = $customerNote;
    
        return $this;
    }

    /**
     * Get customerNote
     *
     * @return string
     */
    public function getCustomerNote()
    {
        return $this->customerNote;
    }

    /**
     * Set customer
     *
     * @param \foreup\rest\models\entities\ForeupCustomers $customer
     *
     * @return ForeupSales
     */
    public function setCustomer(\foreup\rest\models\entities\ForeupCustomers $customer = null)
    {
        $this->customer = $customer;
    
        return $this;
    }

    /**
     * Get customer
     *
     * @return \foreup\rest\models\entities\ForeupCustomers
     */
    public function getCustomer()
    {
    	if(!empty($this->customerId))
            return $this->customer;
    	return null;
    }

    public function getTotal()
    {
        $total = 0;
        foreach($this->getItems() as $item)
        {
            $total += $item->getTotal();
        }
        return $total;
    }
    public function getSubTotal()
    {
        $total = 0;
        foreach($this->getItems() as $item)
        {
            $total += $item->getSubtotal();
        }
        return $total;
    }
    public function getTax()
    {
        $total = 0;
        foreach($this->getItems() as $item)
        {
            $total += $item->getTax();
        }
        return $total;
    }

	public function isValid()
	{
		if(count($this->payments) == 0 ){
			return false;
		}
		if(count($this->items) == 0 ){
			return false;
		}
		$itemTotals = 0;
		/** @var ForeupSalesItems $item */
		foreach($this->items as $item){
			$itemTotals += $item->getTotal();
			if(!$item->isValid()){
				$this->validationMessage = $item->getValidationMessage();
				return false;
			}
			//Qty * Price * tax rate

		}

		$totalPayments = 0;
		/** @var ForeupSalesPayments $payment */
		foreach($this->payments as $payment){
			$totalPayments += $payment->getPaymentAmount();
			if(!$payment->isValid()){
				$this->validationMessage = $payment->getValidationMessage();
				return false;
			}
		}

		if($itemTotals != $totalPayments){
			$this->validationMessage = "The payments don't equal the sale total.  We can't save this sale. Item Totals: $itemTotals Payment Totals: $totalPayments";
			return false;
		}

		return true;
	}

	/**
	 * @return ForeupTeetime
	 */
	public function getTeetime()
	{
		if(empty($this->teetimeId))
			return null;
		return $this->teetime;
	}

	/**
	 * @param ForeupTeetime $teetime
	 */
	public function setTeetime($teetime)
	{
		$this->teetime = $teetime;
	}

}
