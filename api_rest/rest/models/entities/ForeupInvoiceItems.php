<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupStatementItems
 *
 * @ORM\Table(name="foreup_invoice_items", uniqueConstraints={@ORM\UniqueConstraint(name="invoice_id", columns={"invoice_id", "line_number"})}, indexes={@ORM\Index(name="item_id", columns={"item_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupInvoiceItems
{
	use \foreup\rest\models\entities\EntityValidator;

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{
		$location ='ForeupStatementItems->validate';

		$invoice = $this->getStatement();
		$v = $this->validate_object($location,'invoice',$invoice, 'foreup\rest\models\entities\ForeupInvoices',false,$throw);
		if($v !== true) return $v;

		$lineNumber = $this->getLineNumber();
		$v = $this->validate_integer($location,'lineNumber',$lineNumber,true,$throw);
		if($v !== true) return $v;

		$item = $this->getitem();
		$v = $this->validate_object($location,'item',$item,'foreup\rest\models\entities\ForeupItems',false,$throw);
		if($v !== true) return $v;

		$description = $this->getDescription();
		$v = $this->validate_string($location,'description',$description,true,$throw);
		if($v !== true) return $v;

		$quantity = $this->getQuantity();
		$v = $this->validate_integer($location,'quantity',$quantity,true,$throw);
		if($v !== true) return $v;

		$amount = $this->getAmount();
		$v = $this->validate_numeric($location,'amount',$amount,true,$throw);
		if($v !== true) return $v;

		$paidAmount = $this->getPaidAmount();
		$v = $this->validate_numeric($location,'paidAmount',$paidAmount,true,$throw);
		if($v !== true) return $v;

		$paidOff = $this->getPaidOff();
		$v = $this->validate_object($location,'paidOff',$paidOff,'\DateTime',false,$throw);
		if($v !== true) return $v;

		$tax = $this->getTax();
		$v = $this->validate_numeric($location,'tax',$tax,true,$throw);
		if($v !== true) return $v;

		$payAccountBalance = $this->getPayAccountBalance();
		$v = $this->validate_boolean($location,'payAccountBalance',$payAccountBalance,true,$throw);
		if($v !== true) return $v;

		$payMemberBalance = $this->getPayMemberBalance();
		$v = $this->validate_boolean($location,'payMemberBalance',$payMemberBalance,true,$throw);
		if($v !== true) return $v;

		return true;
	}

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="invoice_id", type="integer", nullable=false)
     */
    private $invoiceId = 0;

	/**
	 * @var \foreup\rest\models\entities\ForeupInvoices
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupInvoices")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="invoice_id")
	 * })
	 */
	private $invoice;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="line_number", type="integer", nullable=false)
     */
    private $lineNumber = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false)
     */
    private $itemId = 0;

	/**
	 * @var \foreup\rest\models\entities\ForeupItems
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupItems")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="item_id", referencedColumnName="item_id")
	 * })
	 */
	private $item;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="smallint", nullable=false)
     */
    private $quantity = 1;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", precision=15, scale=2, nullable=false)
     */
    private $amount = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="paid_amount", type="float", precision=15, scale=2, nullable=false)
     */
    private $paidAmount = 0.00;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="paid_off", type="datetime", nullable=false)
     */
    private $paidOff;

    /**
     * @var float
     *
     * @ORM\Column(name="tax", type="float", precision=15, scale=2, nullable=false)
     */
    private $tax = 0.00;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pay_account_balance", type="boolean", nullable=false)
     */
    private $payAccountBalance = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pay_member_balance", type="boolean", nullable=false)
     */
    private $payMemberBalance = 0;

    /**
     * Get invoiceId
     *
     * @return integer
     */
    public function getStatementId()
    {
        return $this->invoiceId;
    }

	/**
	 * Set invoice
	 *
	 * @param \foreup\rest\models\entities\ForeupInvoices|null $invoice
	 *
	 * @return ForeupInvoiceItems
	 */
	public function setStatement(\foreup\rest\models\entities\ForeupInvoices $invoice = null)
	{
		$this->invoice = $invoice;
		if(method_exists($invoice,'getStatementId')){
			$invoiceId = $invoice->getStatementId();
			$this->invoiceId = isset($invoiceId)?$invoiceId:0;
		}

		return $this;
	}

	/**
	 * Get invoice
	 *
	 * @return \foreup\rest\models\entities\ForeupInvoices
	 */
	public function getStatement()
	{
		return $this->invoice;
	}

    /**
     * Set lineNumber
     *
     * @param boolean $lineNumber
     *
     * @return ForeupInvoiceItems
     */
    public function setLineNumber($lineNumber)
    {
        $this->lineNumber = $lineNumber;
    
        return $this;
    }

    /**
     * Get lineNumber
     *
     * @return boolean
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

	/**
	 * Get itemId
	 *
	 * @return integer
	 */
	public function getItemId()
	{
		return $this->itemId;
	}

	/**
	 * Set item
	 *
	 * @param \foreup\rest\models\entities\ForeupItems|null $item
	 *
	 * @return ForeupInvoiceItems
	 */
	public function setItem(\foreup\rest\models\entities\ForeupItems $item = null)
	{
		$this->item = $item;
		if(method_exists($item,'getItemId')){
			$itemId = $item->getItemId();
			$this->itemId = isset($itemId)?$itemId:0;
		}

		return $this;
	}

	/**
	 * Get item
	 *
	 * @return \foreup\rest\models\entities\ForeupItems
	 */
	public function getItem()
	{
		return $this->item;
	}

	/**
	 * Get invoiceId
	 *
	 * @return integer
	 */
	public function getInvoiceId()
	{
		return $this->invoiceId;
	}

	/**
	 * Set invoice
	 *
	 * @param \foreup\rest\models\entities\ForeupInvoices|null $invoice
	 *
	 * @return ForeupInvoiceItems
	 */
	public function setInvoice(\foreup\rest\models\entities\ForeupInvoices $invoice = null)
	{
		$this->invoice = $invoice;
		if(method_exists($invoice,'getItemId')){
			$invoiceId = $invoice->getId();
			$this->invoiceId = isset($invoiceId)?$invoiceId:0;
		}

		return $this;
	}

	/**
	 * Get invoice
	 *
	 * @return \foreup\rest\models\entities\ForeupInvoices
	 */
	public function getInvoice()
	{
		return $this->invoice;
	}

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ForeupInvoiceItems
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ForeupInvoiceItems
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return ForeupInvoiceItems
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set paidAmount
     *
     * @param float $paidAmount
     *
     * @return ForeupInvoiceItems
     */
    public function setPaidAmount($paidAmount)
    {
        $this->paidAmount = $paidAmount;
    
        return $this;
    }

    /**
     * Get paidAmount
     *
     * @return float
     */
    public function getPaidAmount()
    {
        return $this->paidAmount;
    }

    /**
     * Set paidOff
     *
     * @param \DateTime|null $paidOff
     *
     * @return ForeupInvoiceItems
     */
    public function setPaidOff($paidOff = null)
    {
    	if(!isset($paidOff))$this->paidOff = '0000-00-00 00:00:00';
    	elseif(strtotime($paidOff)){
		    $paidOff = $paidOff->format(DATE_ISO8601);
	    }
	    $this->paidOff = $paidOff;
        return $this;
    }

    /**
     * Get paidOff
     *
     * @return \DateTime | null
     */
    public function getPaidOff()
    {
    	if($this->paidOff === '0000-00-00 00:00:00') return null;
	    elseif(strtotime($this->paidOff)){
		    return new \DateTime($this->paidOff);
	    }
        return $this->paidOff;
    }

    /**
     * Set tax
     *
     * @param float $tax
     *
     * @return ForeupInvoiceItems
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    
        return $this;
    }

    /**
     * Get tax
     *
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set payAccountBalance
     *
     * @param boolean $payAccountBalance
     *
     * @return ForeupInvoiceItems
     */
    public function setPayAccountBalance($payAccountBalance)
    {
	    if(is_string($payAccountBalance)){
			$payAccountBalance = strtolower($payAccountBalance);
		    $ac = json_decode($payAccountBalance,true);
		    if(isset($ac))$payAccountBalance = $ac;
	    }

	    $this->payAccountBalance = $payAccountBalance;
    
        return $this;
    }

    /**
     * Get payAccountBalance
     *
     * @return boolean
     */
    public function getPayAccountBalance()
    {
        return $this->payAccountBalance;
    }

    /**
     * Set payMemberBalance
     *
     * @param boolean $payMemberBalance
     *
     * @return ForeupInvoiceItems
     */
    public function setPayMemberBalance($payMemberBalance)
    {
	    if(is_string($payMemberBalance)){
			$payMemberBalance = strtolower($payMemberBalance);
		    $ac = json_decode($payMemberBalance,true);
		    if(isset($ac))$payMemberBalance = $ac;
	    }

	    $this->payMemberBalance = $payMemberBalance;
    
        return $this;
    }

    /**
     * Get payMemberBalance
     *
     * @return boolean
     */
    public function getPayMemberBalance()
    {
        return $this->payMemberBalance;
    }
}
