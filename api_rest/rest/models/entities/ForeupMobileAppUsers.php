<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupMobileAppUsers
 *
 * @ORM\Table(name="foreup_mobile_app_users", indexes={@ORM\Index(name="FK_foreup_mobile_app_users_foreup_users", columns={"person_id"})})
 * @ORM\Entity
 */
class ForeupMobileAppUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="app_name", type="string", length=50, nullable=true)
     */
    private $appName;

    /**
     * @var string
     *
     * @ORM\Column(name="platform", type="string", nullable=true)
     */
    private $platform;

    /**
     * @var \foreup\rest\models\entities\ForeupUsers
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupUsers", inversedBy="mobileAppUsers")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
     *
     */
    private $person;

    /**
     * @return ForeupUsers
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param ForeupUsers $person
     */
    public function setPerson($person)
    {
        $this->person = $person;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAppName()
    {
        return $this->appName;
    }

    /**
     * @param string $appName
     */
    public function setAppName($appName)
    {
        $this->appName = $appName;
    }

    /**
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
    }


}

