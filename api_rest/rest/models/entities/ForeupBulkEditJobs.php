<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupBulkEditJobs
 *
 * @ORM\Table(name="foreup_bulk_edit_jobs", indexes={@ORM\Index(name="course_id", columns={"course_id"}), @ORM\Index(name="type", columns={"type"}), @ORM\Index(name="status", columns={"status"}), @ORM\Index(name="created_at", columns={"created_at"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupBulkEditJobs
{
	use \foreup\rest\models\entities\EntityValidator;

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{
		$this->resetLastError();

		$location = 'ForeupBulkEditJobs->validate';

		$type = $this->getType();
		$type_enum = array('customers');
		$v = $this->validate_enum($location,'type',$type,$type_enum,true,$throw);
		if($v!==true)return $v;

		$courseId = $this->getCourseId();
		$v = $this->validate_integer($location,'courseId',$courseId,false,$throw);
		if($v!==true)return $v;

		$employeeId = $this->getEmployeeId();
		$v = $this->validate_integer($location,'employeeId',$employeeId,false,$throw);
		if($v!==true)return $v;

		$status = $this->getStatus();
		$status_enum = array('pending','ready','in-progress','completed','cancelled');
		$v = $this->validate_enum($location,'status',$status,$status_enum,true,$throw);
		if($v!==true)return $v;

		$totalRecords = $this->getTotalRecords();
		$v = $this->validate_integer($location,'totalRecords',$totalRecords,true,$throw);
		if($v!==true)return $v;

		$recordsCompleted = $this->getRecordsCompleted();
		$v = $this->validate_integer($location,'recordsCompleted',$recordsCompleted,true,$throw);
		if($v!==true)return $v;

		$recordsFailed = $this->getRecordsFailed();
		$v = $this->validate_integer($location,'recordsFailed',$recordsFailed,true,$throw);
		if($v!==true)return $v;

		$percentComplete = $this->getPercentComplete();
		$v = $this->validate_integer($location,'percentComplete',$percentComplete,true,$throw);
		if($v!==true)return $v;

		$createdAt = $this->getCreatedAt();
		$v = $this->validate_object($location,'createdAt',$createdAt,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$startedAt = $this->getStartedAt();
		$v = $this->validate_object($location,'startedAt',$startedAt,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$completedAt = $this->getCompletedAt();
		$v = $this->validate_object($location,'completedAt',$completedAt,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$totalDuration = $this->getTotalDuration();
		$v = $this->validate_integer($location,'totalDuration',$totalDuration,false,$throw);
		if($v!==true)return $v;

		$response = $this->getResponse();
		$v = $this->validate_array($location,'response',$response,false,$throw);
		if($v!==true)return $v;

		$deletedAt = $this->getDeletedAt();
		$v = $this->validate_object($location,'deletedAt',$deletedAt,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$settings = $this->getSettings();
		$v = $this->validate_array($location,'settings',$settings,false,$throw);
		if($v!==true)return $v;

        $recordIds = $this->getRecordIds();
        $v = $this->validate_array($location,'recordIds',$recordIds,false,$throw);
        if($v!==true)return $v;

		$lastProgressUpdate = $this->getLastProgressUpdate();
		$v = $this->validate_object($location,'lastProgressUpdate',$lastProgressUpdate,'\DateTime',false,$throw);
		if($v!==true)return $v;

		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=true)
     */
    private $courseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="employee_id", type="integer", nullable=true)
     */
    private $employeeId;

	/**
	 * @var \foreup\rest\models\entities\ForeupEmployees
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="employee_id", referencedColumnName="person_id")
	 * })
	 */
	private $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = 'pending';

    /**
     * @var integer
     *
     * @ORM\Column(name="total_records", type="integer", nullable=false)
     */
    private $totalRecords = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="records_completed", type="integer", nullable=false)
     */
    private $recordsCompleted = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="records_failed", type="integer", nullable=false)
     */
    private $recordsFailed = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="percent_complete", type="integer", nullable=false)
     */
    private $percentComplete = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="started_at", type="datetime", nullable=true)
     */
    private $startedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="completed_at", type="datetime", nullable=true)
     */
    private $completedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_duration", type="integer", nullable=true)
     */
    private $totalDuration = 0;

    /**
     * @var array
     *
     * @ORM\Column(name="response", type="json_array", nullable=true)
     */
    private $response;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var array
     *
     * @ORM\Column(name="settings", type="json_array", nullable=true)
     */
    private $settings;

    /**
     * @var array
     *
     * @ORM\Column(name="record_ids", type="simple_array", nullable=false)
     */
    private $recordIds;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_progress_update", type="datetime", nullable=true)
     */
    private $lastProgressUpdate;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ForeupBulkEditJobs
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupBulkEditJobs
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    
        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set employeeId
     *
     * @return ForeupBulkEditJobs
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;
        return $this;
    }

    /**
     * Get employeeId
     *
     * @return integer
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

	/**
	 * Set employee
	 *
	 * @param \foreup\rest\models\entities\ForeupEmployees|null $employee
	 *
	 * @return ForeupBulkEditJobs
	 */
	public function setEmployee(\foreup\rest\models\entities\ForeupEmployees $employee = null)
	{
		if(!isset($employee))$this->employeeId = null;

		if(!method_exists($employee,'getPerson')){
			throw \Exception('ForeupInvoiceItems->setEmployee: Invalid ForeupEmployees');
		}
		$this->employeeId = $employee->getPerson()->getPersonId();

		$this->employee = $employee;
		return $this;
	}

	/**
	 * Get employee
	 *
	 * @return \foreup\rest\models\entities\ForeupEmployees|null
	 */
	public function getEmployee()
	{
		if($this->employeeId == 0) return null;
		return $this->employee;
	}

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ForeupBulkEditJobs
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set totalRecords
     *
     * @param integer $totalRecords
     *
     * @return ForeupBulkEditJobs
     */
    public function setTotalRecords($totalRecords)
    {
        $this->totalRecords = $totalRecords;
    
        return $this;
    }

    /**
     * Get totalRecords
     *
     * @return integer
     */
    public function getTotalRecords()
    {
        return $this->totalRecords;
    }

    /**
     * Set recordsCompleted
     *
     * @param integer $recordsCompleted
     *
     * @return ForeupBulkEditJobs
     */
    public function setRecordsCompleted($recordsCompleted)
    {
        $this->recordsCompleted = $recordsCompleted;
    
        return $this;
    }

    /**
     * Get recordsCompleted
     *
     * @return integer
     */
    public function getRecordsCompleted()
    {
        return $this->recordsCompleted;
    }

    /**
     * Set recordsFailed
     *
     * @param integer $recordsFailed
     *
     * @return ForeupBulkEditJobs
     */
    public function setRecordsFailed($recordsFailed)
    {
        $this->recordsFailed = $recordsFailed;
    
        return $this;
    }

    /**
     * Get recordsFailed
     *
     * @return integer
     */
    public function getRecordsFailed()
    {
        return $this->recordsFailed;
    }

    /**
     * Set percentComplete
     *
     * @param boolean $percentComplete
     *
     * @return ForeupBulkEditJobs
     */
    public function setPercentComplete($percentComplete)
    {
        $this->percentComplete = $percentComplete;
    
        return $this;
    }

    /**
     * Get percentComplete
     *
     * @return boolean
     */
    public function getPercentComplete()
    {
        return $this->percentComplete;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ForeupBulkEditJobs
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set startedAt
     *
     * @param \DateTime $startedAt
     *
     * @return ForeupBulkEditJobs
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;
    
        return $this;
    }

    /**
     * Get startedAt
     *
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * Set completedAt
     *
     * @param \DateTime $completedAt
     *
     * @return ForeupBulkEditJobs
     */
    public function setCompletedAt($completedAt)
    {
        $this->completedAt = $completedAt;
    
        return $this;
    }

    /**
     * Get completedAt
     *
     * @return \DateTime
     */
    public function getCompletedAt()
    {
        return $this->completedAt;
    }

    /**
     * Set totalDuration
     *
     * @param integer $totalDuration
     *
     * @return ForeupBulkEditJobs
     */
    public function setTotalDuration($totalDuration)
    {
        $this->totalDuration = $totalDuration;
    
        return $this;
    }

    /**
     * Get totalDuration
     *
     * @return integer
     */
    public function getTotalDuration()
    {
        return $this->totalDuration;
    }

    /**
     * Set response
     *
     * @param array $response
     *
     * @return ForeupBulkEditJobs
     */
    public function setResponse($response)
    {
        $this->response = $response;
    
        return $this;
    }

    /**
     * Get response
     *
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ForeupBulkEditJobs
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set settings
     *
     * @param array $settings
     *
     * @return ForeupBulkEditJobs
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;
    
        return $this;
    }

    /**
     * Get settings
     *
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Set recordIds
     *
     * @param array $recordIds
     *
     * @return ForeupBulkEditJobs
     */
    public function setRecordIds($recordIds)
    {
        $this->recordIds = $recordIds;

        return $this;
    }

    /**
     * Get recordIds
     *
     * @return array
     */
    public function getRecordIds()
    {
        return $this->recordIds;
    }

    /**
     * Set lastProgressUpdate
     *
     * @param \DateTime $lastProgressUpdate
     *
     * @return ForeupBulkEditJobs
     */
    public function setLastProgressUpdate($lastProgressUpdate)
    {
        $this->lastProgressUpdate = $lastProgressUpdate;
    
        return $this;
    }

    /**
     * Get lastProgressUpdate
     *
     * @return \DateTime
     */
    public function getLastProgressUpdate()
    {
        return $this->lastProgressUpdate;
    }
}
