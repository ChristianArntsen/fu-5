<?php

namespace foreup\rest\models\entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupPosCart
 *
 * @ORM\Table(name="foreup_pos_cart", indexes={@ORM\Index(name="course_id", columns={"course_id"}), @ORM\Index(name="last_ping", columns={"last_ping"}), @ORM\Index(name="terminal_id", columns={"terminal_id"})})
 * @ORM\Entity
 */
class ForeupPosCart
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cart_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cartId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated;

    /**
     * @var integer
     *
     * @ORM\Column(name="employee_id", type="integer", nullable=false)
     */
    private $employeeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16, nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="mode", type="string", length=8, nullable=false)
     */
    private $mode = 'sale';

    /**
     * @var string
     *
     * @ORM\Column(name="suspended_name", type="string", length=64, nullable=true)
     */
    private $suspendedName;

    /**
     * @var string
     *
     * @ORM\Column(name="teetime_id", type="string", length=64, nullable=true)
     */
    private $teetimeId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_activity", type="datetime", nullable=false)
     */
    private $lastActivity;

    /**
     * @var boolean
     *
     * @ORM\Column(name="taxable", type="boolean", nullable=false)
     */
    private $taxable = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="override_authorization_id", type="integer", nullable=true)
     */
    private $overrideAuthorizationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="return_sale_id", type="integer", nullable=true)
     */
    private $returnSaleId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_ping", type="datetime", nullable=false)
     */
    private $lastPing;

    /**
     * @var integer
     *
     * @ORM\Column(name="terminal_id", type="integer", nullable=false)
     */
    private $terminalId;

    /**
     * @var string
     *
     * @ORM\Column(name="refund_reason", type="string", length=255, nullable=false)
     */
    private $refundReason;

    /**
     * @var string
     *
     * @ORM\Column(name="refund_comment", type="string", length=255, nullable=false)
     */
    private $refundComment;

	/**
	 * @var \foreup\rest\models\entities\ForeupPosCartItems $items
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupPosCartItems", mappedBy="cart",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="cart_id", referencedColumnName="cart_id")
	 * })
	 */
	private $items;

    private $total;
    private $totalDue;
    private $tax;
    private $subtotal;

	/**
	 * ForeupPosCart constructor.
	 */
	public function __construct()
	{
		$this->items = new ArrayCollection();
	}

	/**
	 * @return mixed
	 */
	public function getTotal()
	{
		return $this->total;
	}

	/**
	 * @param mixed $total
	 */
	public function setTotal($total)
	{
		$this->total = $total;
	}

	/**
	 * @return mixed
	 */
	public function getTotalDue()
	{
		return $this->totalDue;
	}

	/**
	 * @param mixed $totalDue
	 */
	public function setTotalDue($totalDue)
	{
		$this->totalDue = $totalDue;
	}

	/**
	 * @return mixed
	 */
	public function getTax()
	{
		return $this->tax;
	}

	/**
	 * @param mixed $tax
	 */
	public function setTax($tax)
	{
		$this->tax = $tax;
	}

	/**
	 * @return mixed
	 */
	public function getSubtotal()
	{
		return $this->subtotal;
	}

	/**
	 * @param mixed $subtotal
	 */
	public function setSubtotal($subtotal)
	{
		$this->subtotal = $subtotal;
	}


	/**
	 * @return int
	 */
	public function getCartId()
	{
		return $this->cartId;
	}

	/**
	 * @param int $cartId
	 */
	public function setCartId($cartId)
	{
		$this->cartId = $cartId;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateCreated()
	{
		return $this->dateCreated;
	}

	/**
	 * @param \DateTime $dateCreated
	 */
	public function setDateCreated($dateCreated)
	{
		$this->dateCreated = $dateCreated;
	}

	/**
	 * @return int
	 */
	public function getEmployeeId()
	{
		return $this->employeeId;
	}

	/**
	 * @param int $employeeId
	 */
	public function setEmployeeId($employeeId)
	{
		$this->employeeId = $employeeId;
	}

	/**
	 * @return int
	 */
	public function getCourseId()
	{
		return $this->courseId;
	}

	/**
	 * @param int $courseId
	 */
	public function setCourseId($courseId)
	{
		$this->courseId = $courseId;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function getMode()
	{
		return $this->mode;
	}

	/**
	 * @param string $mode
	 */
	public function setMode($mode)
	{
		$this->mode = $mode;
	}

	/**
	 * @return string
	 */
	public function getSuspendedName()
	{
		return $this->suspendedName;
	}

	/**
	 * @param string $suspendedName
	 */
	public function setSuspendedName($suspendedName)
	{
		$this->suspendedName = $suspendedName;
	}

	/**
	 * @return string
	 */
	public function getTeetimeId()
	{
		return $this->teetimeId;
	}

	/**
	 * @param string $teetimeId
	 */
	public function setTeetimeId($teetimeId)
	{
		$this->teetimeId = $teetimeId;
	}

	/**
	 * @return \DateTime
	 */
	public function getLastActivity()
	{
		return $this->lastActivity;
	}

	/**
	 * @param \DateTime $lastActivity
	 */
	public function setLastActivity($lastActivity)
	{
		$this->lastActivity = $lastActivity;
	}

	/**
	 * @return bool
	 */
	public function isTaxable()
	{
		return $this->taxable;
	}

	/**
	 * @param bool $taxable
	 */
	public function setTaxable($taxable)
	{
		$this->taxable = $taxable;
	}

	/**
	 * @return int
	 */
	public function getOverrideAuthorizationId()
	{
		return $this->overrideAuthorizationId;
	}

	/**
	 * @param int $overrideAuthorizationId
	 */
	public function setOverrideAuthorizationId($overrideAuthorizationId)
	{
		$this->overrideAuthorizationId = $overrideAuthorizationId;
	}

	/**
	 * @return int
	 */
	public function getReturnSaleId()
	{
		return $this->returnSaleId;
	}

	/**
	 * @param int $returnSaleId
	 */
	public function setReturnSaleId($returnSaleId)
	{
		$this->returnSaleId = $returnSaleId;
	}

	/**
	 * @return \DateTime
	 */
	public function getLastPing()
	{
		return $this->lastPing;
	}

	/**
	 * @param \DateTime $lastPing
	 */
	public function setLastPing($lastPing)
	{
		$this->lastPing = $lastPing;
	}

	/**
	 * @return int
	 */
	public function getTerminalId()
	{
		return $this->terminalId;
	}

	/**
	 * @param int $terminalId
	 */
	public function setTerminalId($terminalId)
	{
		$this->terminalId = $terminalId;
	}

	/**
	 * @return string
	 */
	public function getRefundReason()
	{
		return $this->refundReason;
	}

	/**
	 * @param string $refundReason
	 */
	public function setRefundReason($refundReason)
	{
		$this->refundReason = $refundReason;
	}

	/**
	 * @return string
	 */
	public function getRefundComment()
	{
		return $this->refundComment;
	}

	/**
	 * @param string $refundComment
	 */
	public function setRefundComment($refundComment)
	{
		$this->refundComment = $refundComment;
	}

	public function addItem(ForeupPosCartItems $item)
	{
		$this->items[] = $item;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getItems()
	{
		return $this->items;
	}
}

