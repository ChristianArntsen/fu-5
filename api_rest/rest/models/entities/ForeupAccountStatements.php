<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupAccountStatements
 *
 * @ORM\Table(name="foreup_account_statements", uniqueConstraints={@ORM\UniqueConstraint(name="number_organization_id", columns={"number", "organization_id"})}, indexes={@ORM\Index(name="date_created", columns={"date_created"}), @ORM\Index(name="due_date", columns={"due_date"}), @ORM\Index(name="organization_id", columns={"organization_id"}), @ORM\Index(name="person_id", columns={"person_id"}), @ORM\Index(name="start_date_end_date", columns={"start_date", "end_date"}), @ORM\Index(name="recurring_statement_id", columns={"recurring_statement_id"}), @ORM\Index(name="number", columns={"number"}), @ORM\Index(name="date_paid", columns={"date_paid"}), @ORM\Index(name="total", columns={"total"}), @ORM\Index(name="send_email", columns={"send_email"}), @ORM\Index(name="send_mail", columns={"send_mail"})})
 * @ORM\Entity (repositoryClass="foreup\rest\models\repositories\AccountStatementsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ForeupAccountStatements
{
	use \foreup\rest\models\entities\EntityValidator;

	public function __construct()
	{
		$this->charges = new \Doctrine\Common\Collections\ArrayCollection();
		$this->payments = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true) {

		$this->resetLastError();
		$location = 'ForeupAccountStatements->validate';

		$dateCreated = $this->getDateCreated();
		$v = $this->validate_object($location,'dateCreated',$dateCreated,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$dueDate = $this->dueDate;
		$v = $this->validate_object($location,'dueDate',$dueDate,'\DateTime',false,$throw);
		if($v!==true)return $v;

		$startDate = $this->startDate;
		$v = $this->validate_object($location,'startDate',$startDate,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$endDate = $this->endDate;
		$v = $this->validate_object($location,'endDate',$endDate,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$createdBy = $this->createdBy;
		$v = $this->validate_integer($location,'createdBy',$createdBy,true,$throw);
		if($v!==true)return $v;

		$organizationId = $this->organizationId;
		$v = $this->validate_integer($location,'organizationId',$organizationId,true,$throw);
		if($v!==true)return $v;

		$personId = $this->personId;
		$v = $this->validate_integer($location,'personId',$personId,true,$throw);
		if($v!==true)return $v;

		/*
		$recurringStatement = $this->getRecurringStatement();
		$v = $this->validate_object($location,'recurringStatement',$recurringStatement,'\foreup\rest\models\entities\ForeupAccountRecurringStatements',false,$throw);
		if($v!==true)return $v;
*/
		$total = $this->total;
		$v = $this->validate_numeric($location,'total',$total,true,$throw);
		if($v!==true)return $v;

		$totalPaid = $this->totalPaid;
		$v = $this->validate_numeric($location,'totalPaid',$totalPaid,true,$throw);
		if($v!==true)return $v;

		$totalOpen = $this->totalOpen;
		$v = $this->validate_numeric($location,'totalOpen',$totalOpen,true,$throw);
		if($v!==true)return $v;

		$value = $this->previousOpenBalance;
		$v = $this->validate_numeric($location,'previousOpenBalance',$value,true,$throw);
		if($v!==true)return $v;

		$memo = $this->memo;
		$v = $this->validate_string($location,'memo',$memo,false,$throw);
		if($v!==true)return $v;

		$number = $this->number;
		$v = $this->validate_integer($location,'number',$number,true,$throw);
		if($v!==true)return $v;

		$value = $this->sendZeroChargeStatement;
		$v = $this->validate_boolean($location,'sendZeroChargeStatement',$value,true,$throw);
		if($v!==true)return $v;

		$value = $this->footerText;
		$v = $this->validate_string($location,'footerText',$value,false,$throw);
		if($v!==true)return $v;

		$value = $this->messageText;
		$v = $this->validate_string($location,'messageText',$value,false,$throw);
		if($v!==true)return $v;

		$value = $this->termsAndConditionsText;
		$v = $this->validate_string($location,'termsAndConditionsText',$value,false,$throw);
		if($v!==true)return $v;

		$value = $this->totalFinanceCharges;
		$v = $this->validate_numeric($location,'totalFinanceCharges',$value,true,$throw);
		if($v!==true)return $v;

		$value = $this->financeChargeEnabled;
		$v = $this->validate_boolean($location,'financeChargeEnabled',$value,true,$throw);
		if($v!==true)return $v;

		$value = $this->financeChargeAmount;
		$v = $this->validate_numeric($location,'financeChargeAmount',$value,true,$throw);
		if($v!==true)return $v;

		$value = $this->financeChargeType;
		$v = $this->validate_enum($location,'financeChargeType',$value,['percent','fixed'],true,$throw);
		if($v!==true)return $v;

		$value = $this->financeChargeAfterDays;
		$v = $this->validate_integer($location,'financeChargeAfterDays',$value,true,$throw);
		if($v!==true)return $v;

		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", nullable=false)
     */
    private $number = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="organization_id", type="integer", nullable=false)
     */
    private $organizationId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetime", nullable=false)
     */
    private $dueDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true)
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer", nullable=false)
     */
    private $personId;

	/**
	 * @var \foreup\rest\models\entities\ForeupCustomers
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCustomers")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id"),
	 *   @ORM\JoinColumn(name="organization_id", referencedColumnName="course_id")
	 * })
	 */
	private $customer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=false)
     */
    private $endDate;

	/**
	 * @var \foreup\rest\models\entities\ForeupAccountRecurringStatements
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupAccountRecurringStatements")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="recurring_statement_id", referencedColumnName="id", nullable=true)
	 * })
	 */
	private $recurringStatement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_paid", type="datetime", nullable=true)
     */
    private $datePaid;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $total = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="total_paid", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $totalPaid = 0.00;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_open", type="decimal", precision=15, scale=2, nullable=false)
	 */
	private $totalOpen = 0.00;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="previous_open_balance", type="decimal", precision=15, scale=2, nullable=false)
	 */
	private $previousOpenBalance = 0.00;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="total_finance_charges", type="decimal", precision=15, scale=2, nullable=false)
	 */
	private $totalFinanceCharges = 0.00;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="finance_charge_enabled", type="boolean", precision=0, scale=0, nullable=false, unique=false)
	 */
	private $financeChargeEnabled = 0;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="finance_charge_amount", type="decimal", precision=15, scale=2, nullable=false)
	 */
	private $financeChargeAmount = '0.00';

	/**
	 * @var string
	 *
	 * @ORM\Column(name="finance_charge_type", type="text", length=65535, nullable=false)
	 */
	private $financeChargeType = 'percent';

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="finance_charge_after_days", type="integer", nullable=false)
	 */
	private $financeChargeAfterDays = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="text", length=65535, nullable=true)
     */
    private $memo;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="footer_text", type="text", length=65535, nullable=true)
	 */
	private $footerText;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="message_text", type="text", length=65535, nullable=true)
	 */
	private $messageText;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="terms_and_conditions_text", type="text", length=65535, nullable=true)
	 */
	private $termsAndConditionsText;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="include_member_transactions", type="boolean", nullable=false)
	 */
	private $includeMemberTransactions = '0';

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="send_zero_charge_statement", type="boolean", nullable=false)
	 */
	private $sendZeroChargeStatement = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="include_customer_transactions", type="boolean", nullable=false)
     */
    private $includeCustomerTransactions = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="autopay_enabled", type="boolean", nullable=false)
     */
    private $autopayEnabled = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="autopay_delay_days", type="boolean", nullable=false)
     */
    private $autopayDelayDays = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="autopay_attempts", type="boolean", nullable=false)
     */
    private $autopayAttempts = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="send_email", type="boolean", nullable=false)
     */
    private $sendEmail = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="send_mail", type="boolean", nullable=false)
     */
    private $sendMail = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted_by", type="integer", nullable=true)
     */
    private $deletedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_deleted", type="datetime", nullable=true)
     */
    private $dateDeleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_email_sent", type="datetime", nullable=true)
     */
    private $dateEmailSent;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_mail_sent", type="datetime", nullable=true)
	 */
	private $dateMailSent;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_autopay_queued", type="datetime", nullable=true)
	 */
	private $dateAutopayQueued = null;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_finance_charge_queued", type="datetime", nullable=true)
	 */
	private $dateFinanceChargeQueued = null;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pay_member_balance", type="boolean", nullable=true)
     */
    private $payMemberBalance = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="pay_customer_balance", type="boolean", nullable=true)
     */
    private $payCustomerBalance = '0';

	/**
	 * @var \foreup\rest\models\entities\ForeupAccountStatementCharges
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupAccountStatementCharges", mappedBy="statement",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="id", referencedColumnName="statement_id")
	 * })
	 */
	private $charges;

	/**
	 * @var \foreup\rest\models\entities\ForeupAccountStatementPayments
	 *
	 * @ORM\OneToMany(targetEntity="foreup\rest\models\entities\ForeupAccountStatementPayments", mappedBy="statement",fetch="EAGER")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="id", referencedColumnName="statement_id")
	 * })
	 */
	private $payments;

    private $memberBalanceForward = 0.00;
    private $customerBalanceForward = 0;

    /**
     * @return float
     */
    public function getMemberBalanceForward()
    {
        return $this->memberBalanceForward;
    }

    /**
     * @param float $memberBalanceForward
     */
    public function setMemberBalanceForward($memberBalanceForward)
    {
        $this->memberBalanceForward = (float) $memberBalanceForward;
    }

    /**
     * @return float
     */
    public function getCustomerBalanceForward()
    {
        return $this->customerBalanceForward;
    }

    /**
     * @param float $customerBalanceForward
     */
    public function setCustomerBalanceForward($customerBalanceForward)
    {
        $this->customerBalanceForward = (float) $customerBalanceForward;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
 * Set number
 *
 * @param integer $number
 *
 * @return ForeupAccountStatements
 */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     *
     * @return ForeupAccountStatements
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return ForeupAccountStatements
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     *
     * @return ForeupAccountStatements
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;
    
        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return ForeupAccountStatements
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return ForeupAccountStatements
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return ForeupAccountStatements
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

	/**
	 * Set recurringStatement
	 *
	 * @param \foreup\rest\models\entities\ForeupAccountRecurringStatements $recurringStatement
	 *
	 * @return ForeupAccountStatements
	 */
	public function setRecurringStatement(ForeupAccountRecurringStatements $recurringStatement = null)
	{
		$this->recurringStatement = $recurringStatement;

		return $this;
	}

	/**
	 * Get recurringStatement
	 *
	 * @return \foreup\rest\models\entities\ForeupAccountRecurringStatements
	 */
	public function getRecurringStatement()
	{
		return $this->recurringStatement;
	}

    /**
     * Set datePaid
     *
     * @param \DateTime $datePaid
     *
     * @return ForeupAccountStatements
     */
    public function setDatePaid($datePaid)
    {
        $this->datePaid = $datePaid;
    
        return $this;
    }

    /**
     * Get datePaid
     *
     * @return \DateTime
     */
    public function getDatePaid()
    {
        return $this->datePaid;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ForeupAccountStatements
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set totalPaid
     *
     * @param string $totalPaid
     *
     * @return ForeupAccountStatements
     */
    public function setTotalPaid($totalPaid)
    {
        $this->totalPaid = $totalPaid;
    
        return $this;
    }

    /**
     * Get totalPaid
     *
     * @return string
     */
    public function getTotalPaid()
    {
        return $this->totalPaid;
    }

    /**
     * Set totalOpen
     *
     * @param string $totalOpen
     *
     * @return ForeupAccountStatements
     */
    public function setTotalOpen($totalOpen)
    {
        $this->totalOpen = $totalOpen;
    
        return $this;
    }

    /**
     * Get totalOpen
     *
     * @return string
     */
    public function getTotalOpen()
    {
        return $this->totalOpen;
    }

	/**
	 * Set previousOpenBalance
	 *
	 * @param string $total
	 *
	 * @return ForeupAccountStatements
	 */
	public function setPreviousOpenBalance($total)
	{
		$this->previousOpenBalance = $total;

		return $this;
	}

	/**
	 * Get previousOpenBalance
	 *
	 * @return string
	 */
	public function getPreviousOpenBalance()
	{
		return $this->previousOpenBalance;
	}

    /**
     * Set memo
     *
     * @param string $memo
     *
     * @return ForeupAccountStatements
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
    
        return $this;
    }

    /**
     * Get memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

	/**
	 * Set footerText
	 *
	 * @param string $text
	 *
	 * @return ForeupAccountStatements
	 */
	public function setFooterText($text)
	{
		$this->footerText = $text;

		return $this;
	}

	/**
	 * Get footerText
	 *
	 * @return string
	 */
	public function getFooterText()
	{
		return $this->footerText;
	}

	/**
	 * Set messageText
	 *
	 * @param string $text
	 *
	 * @return ForeupAccountStatements
	 */
	public function setMessageText($text)
	{
		$this->messageText = $text;

		return $this;
	}

	/**
	 * Get messageText
	 *
	 * @return string
	 */
	public function getMessageText()
	{
		return $this->messageText;
	}

	/**
	 * Set termsAndConditionsText
	 *
	 * @param string $text
	 *
	 * @return ForeupAccountStatements
	 */
	public function setTermsAndConditionsText($text)
	{
		$this->termsAndConditionsText = $text;

		return $this;
	}

	/**
	 * Get termsAndConditionsText
	 *
	 * @return string
	 */
	public function getTermsAndConditionsText()
	{
		return $this->termsAndConditionsText;
	}

    /**
     * Set includeMemberTransactions
     *
     * @param boolean $includeMemberTransactions
     *
     * @return ForeupAccountStatements
     */
    public function setIncludeMemberTransactions($includeMemberTransactions)
    {
        $this->includeMemberTransactions = $includeMemberTransactions;
    
        return $this;
    }

    /**
     * Get includeMemberTransactions
     *
     * @return boolean
     */
    public function getIncludeMemberTransactions()
    {
        return $this->includeMemberTransactions;
    }

    /**
     * Set includeCustomerTransactions
     *
     * @param boolean $includeCustomerTransactions
     *
     * @return ForeupAccountStatements
     */
    public function setIncludeCustomerTransactions($includeCustomerTransactions)
    {
        $this->includeCustomerTransactions = $includeCustomerTransactions;
    
        return $this;
    }

    /**
     * Get includeCustomerTransactions
     *
     * @return boolean
     */
    public function getIncludeCustomerTransactions()
    {
        return $this->includeCustomerTransactions;
    }

    /**
     * Set autopayEnabled
     *
     * @param boolean $autopayEnabled
     *
     * @return ForeupAccountStatements
     */
    public function setAutopayEnabled($autopayEnabled)
    {
        $this->autopayEnabled = $autopayEnabled;
    
        return $this;
    }

    /**
     * Get autopayEnabled
     *
     * @return boolean
     */
    public function getAutopayEnabled()
    {
        return $this->autopayEnabled;
    }

    /**
     * Set autopayDelayDays
     *
     * @param boolean $autopayDelayDays
     *
     * @return ForeupAccountStatements
     */
    public function setAutopayDelayDays($autopayDelayDays)
    {
        $this->autopayDelayDays = $autopayDelayDays;
    
        return $this;
    }

    /**
     * Get autopayDelayDays
     *
     * @return boolean
     */
    public function getAutopayDelayDays()
    {
        return $this->autopayDelayDays;
    }

    /**
     * Set autopayAttempts
     *
     * @param boolean $autopayAttempts
     *
     * @return ForeupAccountStatements
     */
    public function setAutopayAttempts($autopayAttempts)
    {
        $this->autopayAttempts = $autopayAttempts;
    
        return $this;
    }

    /**
     * Get autopayAttempts
     *
     * @return boolean
     */
    public function getAutopayAttempts()
    {
        return $this->autopayAttempts;
    }

    /**
     * Set sendEmail
     *
     * @param boolean $sendEmail
     *
     * @return ForeupAccountStatements
     */
    public function setSendEmail($sendEmail)
    {
        $this->sendEmail = $sendEmail;
    
        return $this;
    }

    /**
     * Get sendEmail
     *
     * @return boolean
     */
    public function getSendEmail()
    {
        return $this->sendEmail;
    }

    /**
     * Set sendMail
     *
     * @param boolean $sendMail
     *
     * @return ForeupAccountStatements
     */
    public function setSendMail($sendMail)
    {
        $this->sendMail = $sendMail;
    
        return $this;
    }

    /**
     * Get sendMail
     *
     * @return boolean
     */
    public function getSendMail()
    {
        return $this->sendMail;
    }

    /**
     * Set deletedBy
     *
     * @param integer $deletedBy
     *
     * @return ForeupAccountStatements
     */
    public function setDeletedBy($deletedBy)
    {
        $this->deletedBy = $deletedBy;
    
        return $this;
    }

    /**
     * Get deletedBy
     *
     * @return integer
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

    /**
     * Set dateDeleted
     *
     * @param \DateTime $dateDeleted
     *
     * @return ForeupAccountStatements
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->dateDeleted = $dateDeleted;
    
        return $this;
    }

    /**
     * Get dateDeleted
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->dateDeleted;
    }

    /**
     * Set dateEmailSent
     *
     * @param \DateTime $dateEmailSent
     *
     * @return ForeupAccountStatements
     */
    public function setDateEmailSent($dateEmailSent)
    {
        $this->dateEmailSent = $dateEmailSent;
    
        return $this;
    }

    /**
     * Get dateEmailSent
     *
     * @return \DateTime
     */
    public function getDateEmailSent()
    {
        return $this->dateEmailSent;
    }

	/**
	 * Set dateMailSent
	 *
	 * @param \DateTime $dateMailSent
	 *
	 * @return ForeupAccountStatements
	 */
	public function setDateMailSent($dateMailSent)
	{
		$this->dateMailSent = $dateMailSent;

		return $this;
	}

	/**
	 * Get dateMailSent
	 *
	 * @return \DateTime
	 */
	public function getDateMailSent()
	{
		return $this->dateMailSent;
	}

	/**
	 * Set dateAutopayQueued
	 *
	 * @param \DateTime $dateAutopayQueued
	 *
	 * @return ForeupAccountStatements
	 */
	public function setDateAutopayQueued(\DateTime $dateAutopayQueued = null)
	{
		$this->dateAutopayQueued = $dateAutopayQueued;

		return $this;
	}

	/**
	 * Get dateAutopayQueued
	 *
	 * @return \DateTime
	 */
	public function getDateAutopayQueued()
	{
		return $this->dateAutopayQueued;
	}


	/**
	 * Set dateFinanceChargeQueued
	 *
	 * @param \DateTime $dateFinanceChargeQueued
	 *
	 * @return ForeupAccountStatements
	 */
	public function setDateFinanceChargeQueued(\DateTime $dateFinanceChargeQueued = null)
	{
		$this->dateFinanceChargeQueued = $dateFinanceChargeQueued;

		return $this;
	}

	/**
	 * Get dateFinanceChargeQueued
	 *
	 * @return \DateTime
	 */
	public function getDateFinanceChargeQueued()
	{
		return $this->dateFinanceChargeQueued;
	}

	/**
     * Set payMemberBalance
     *
     * @param boolean $payMemberBalance
     *
     * @return ForeupAccountStatements
     */
    public function setPayMemberBalance($payMemberBalance)
    {
        $this->payMemberBalance = $payMemberBalance;
    
        return $this;
    }

    /**
     * Get payMemberBalance
     *
     * @return boolean
     */
    public function getPayMemberBalance()
    {
        return $this->payMemberBalance;
    }

    /**
     * Set payCustomerBalance
     *
     * @param boolean $payCustomerBalance
     *
     * @return ForeupAccountStatements
     */
    public function setPayCustomerBalance($payCustomerBalance)
    {
        $this->payCustomerBalance = $payCustomerBalance;
    
        return $this;
    }

    /**
     * Get payCustomerBalance
     *
     * @return boolean
     */
    public function getPayCustomerBalance()
    {
        return $this->payCustomerBalance;
    }

	/**
	 * Set customer
	 *
	 * @param \foreup\rest\models\entities\ForeupCustomers $customer
	 *
	 * @return ForeupAccountStatements
	 */
	public function setCustomer(ForeupCustomers $customer)
	{
		$this->customer = $customer;
		if(isset($customer)){
			$this->personId = $customer->getPerson()->getPersonId();
		}

		return $this;
	}

	/**
	 * Get customer
	 *
	 * @return \foreup\rest\models\entities\ForeupCustomers
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection|ForeupAccountStatementCharges[]
	 */
	public function getCharges()
	{
		return $this->charges;
	}

	/**
	 * @param ForeupAccountStatementCharges $item
	 *
	 * @return ForeupAccountStatements
	 */
	public function addCharge($item)
	{
		$item->setStatement($this);
		$this->charges[] = $item;

		return $this;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection|ForeupAccountStatementPayments[]
	 */
	public function getPayments()
	{
		return $this->payments;
	}

	/**
	 * @param ForeupAccountStatementPayments $item
	 *
	 * @return ForeupAccountStatements
	 */
	public function addPayment($item)
	{
		$item->setStatement($this);
		$this->payments[] = $item;

		return $this;
	}

	/**
	 * Set sendZeroChargeStatement
	 *
	 * @param boolean $send
	 *
	 * @return ForeupAccountStatements
	 */
	public function setSendZeroChargeStatement($send)
	{
		if(is_string($send)){
			$send = strtolower($send);
			$ac = json_decode($send,true);
			if(isset($ac))$send = $ac;
		}

		$this->sendZeroChargeStatement = $send;

		return $this;
	}

	/**
	 * Get sendZeroChargeStatement
	 *
	 * @return boolean
	 */
	public function getSendZeroChargeStatement()
	{
		return $this->sendZeroChargeStatement;
	}

	/**
	 * Set totalFinanceCharges
	 *
	 * @param string $amount
	 *
	 * @return ForeupAccountStatements
	 */
	public function setTotalFinanceCharges($amount)
	{
		$this->totalFinanceCharges = $amount;

		return $this;
	}

	/**
	 * Get totalFinanceCharges
	 *
	 * @return string
	 */
	public function getTotalFinanceCharges()
	{
		return $this->totalFinanceCharges;
	}

	/**
	 * Set financeChargeEnabled
	 *
	 * @param boolean $financeChargeEnabled
	 *
	 * @return ForeupAccountStatements
	 */
	public function setFinanceChargeEnabled($financeChargeEnabled)
	{
		if(is_string($financeChargeEnabled)){
			$financeChargeEnabled = strtolower($financeChargeEnabled);
			$is=json_decode($financeChargeEnabled,true);
			if(isset($is))$financeChargeEnabled=$is;
		}
		$this->financeChargeEnabled = $financeChargeEnabled;

		return $this;
	}

	/**
	 * Get financeChargeEnabled
	 *
	 * @return boolean
	 */
	public function getFinanceChargeEnabled()
	{
		return $this->financeChargeEnabled;
	}

	/**
	 * Set financeChargeAmount
	 *
	 * @param string $amount
	 *
	 * @return ForeupAccountStatements
	 */
	public function setFinanceChargeAmount($amount)
	{
		$this->financeChargeAmount = $amount;

		return $this;
	}

	/**
	 * Get financeChargeAmount
	 *
	 * @return string
	 */
	public function getFinanceChargeAmount()
	{
		return $this->financeChargeAmount;
	}

	/**
	 * Set financeChargeType
	 *
	 * @param string $text
	 *
	 * @return ForeupAccountStatements
	 */
	public function setFinanceChargeType($text)
	{

		$this->financeChargeType = $text;

		return $this;
	}

	/**
	 * Get financechargeType
	 *
	 * @return string
	 */
	public function getFinanceChargeType()
	{
		return $this->financeChargeType;
	}


	/**
	 * Set financeChargeAfterDays
	 *
	 * @param integer $days
	 *
	 * @return ForeupAccountStatements
	 */
	public function setFinanceChargeAfterDays($days)
	{
		$this->financeChargeAfterDays = $days;

		return $this;
	}

	/**
	 * Get financeChargeAfterDays
	 *
	 * @return integer
	 */
	public function getFinanceChargeAfterDays()
	{
		return $this->financeChargeAfterDays;
	}
}
