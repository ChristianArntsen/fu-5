<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupApiApplications
 *
 * @ORM\Table(name="foreup_api_applications", indexes={@ORM\Index(name="FK_foreup_api_applications_foreup_users", columns={"user_id"})})
 * @ORM\Entity
 */
class ForeupApiApplications
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="application_name", type="string", length=50, nullable=false)
     */
    private $applicationName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="teesheet", type="boolean", nullable=false)
     */
    private $teesheet = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="sales", type="boolean", nullable=false)
     */
    private $sales = '0';

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="emails", type="boolean", nullable=false)
	 */
	private $emails = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="customers", type="boolean", nullable=false)
     */
    private $customers = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="inventory", type="boolean", nullable=false)
     */
    private $inventory = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="employees", type="boolean", nullable=false)
     */
    private $employees = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="accepted_tos", type="datetime", nullable=false)
     */
    private $acceptedTos = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="accepted_pp", type="datetime", nullable=false)
     */
    private $acceptedPp = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="accepted_parter_agreement", type="datetime", nullable=false)
     */
    private $acceptedParterAgreement = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="sso_url", type="string", length=500, nullable=true)
     */
    private $ssoUrl;

    /**
     * @var \foreup\rest\models\entities\ForeupUsers
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    public function getLimitationArray()
    {
    	$limitations = [];
    	$limitations["customers"] = $this->isCustomers();
    	$limitations["employees"] = $this->isEmployees();
    	$limitations["inventory"] = $this->isInventory();
    	$limitations["emails"] = $this->isEmails();
    	$limitations["sales"] = $this->isSales();
    	$limitations["teesheet"] = $this->isTeesheet();
    	return $limitations;
    }
	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getApplicationName()
	{
		return $this->applicationName;
	}

	/**
	 * @param string $applicationName
	 */
	public function setApplicationName($applicationName)
	{
		$this->applicationName = $applicationName;
	}

	/**
	 * @return bool
	 */
	public function isTeesheet()
	{
		return $this->teesheet;
	}

	/**
	 * @param bool $teesheet
	 */
	public function setTeesheet($teesheet)
	{
		$this->teesheet = $teesheet;
	}

	/**
	 * @return bool
	 */
	public function isSales()
	{
		return $this->sales;
	}

	/**
	 * @param bool $sales
	 */
	public function setSales($sales)
	{
		$this->sales = $sales;
	}

	/**
	 * @return bool
	 */
	public function isEmails(): bool
	{
		return $this->emails;
	}

	/**
	 * @param bool $emails
	 */
	public function setEmails(bool $emails)
	{
		$this->emails = $emails;
	}



	/**
	 * @return bool
	 */
	public function isCustomers()
	{
		return $this->customers;
	}

	/**
	 * @param bool $customers
	 */
	public function setCustomers($customers)
	{
		$this->customers = $customers;
	}

	/**
	 * @return bool
	 */
	public function isInventory()
	{
		return $this->inventory;
	}

	/**
	 * @param bool $inventory
	 */
	public function setInventory($inventory)
	{
		$this->inventory = $inventory;
	}

	/**
	 * @return bool
	 */
	public function isEmployees()
	{
		return $this->employees;
	}

	/**
	 * @param bool $employees
	 */
	public function setEmployees($employees)
	{
		$this->employees = $employees;
	}

	/**
	 * @return \DateTime
	 */
	public function getAcceptedTos()
	{
		return $this->acceptedTos;
	}

	/**
	 * @param \DateTime $acceptedTos
	 */
	public function setAcceptedTos($acceptedTos)
	{
		$this->acceptedTos = $acceptedTos;
	}

	/**
	 * @return \DateTime
	 */
	public function getAcceptedPp()
	{
		return $this->acceptedPp;
	}

	/**
	 * @param \DateTime $acceptedPp
	 */
	public function setAcceptedPp($acceptedPp)
	{
		$this->acceptedPp = $acceptedPp;
	}

	/**
	 * @return \DateTime
	 */
	public function getAcceptedParterAgreement()
	{
		return $this->acceptedParterAgreement;
	}

	/**
	 * @param \DateTime $acceptedParterAgreement
	 */
	public function setAcceptedParterAgreement($acceptedParterAgreement)
	{
		$this->acceptedParterAgreement = $acceptedParterAgreement;
	}

	/**
	 * @return string
	 */
	public function getSsoUrl()
	{
		return $this->ssoUrl;
	}

	/**
	 * @param string $ssoUrl
	 */
	public function setSsoUrl($ssoUrl)
	{
		$this->ssoUrl = $ssoUrl;
	}

	/**
	 * @return ForeupUsers
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param ForeupUsers $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}


}

