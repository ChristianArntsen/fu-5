<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupBookingClasses
 *
 * @ORM\Table(name="foreup_booking_classes", indexes={@ORM\Index(name="FK_foreup_booking_classes_foreup_teesheet", columns={"teesheet_id"})})
 * @ORM\Entity
 */
class ForeupBookingClasses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="booking_class_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bookingClassId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price_class", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $priceClass;

    /**
     * @var boolean
     *
     * @ORM\Column(name="online_booking_protected", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $onlineBookingProtected;

    /**
     * @var boolean
     *
     * @ORM\Column(name="require_credit_card", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $requireCreditCard;

    /**
     * @var string
     *
     * @ORM\Column(name="online_open_time", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $onlineOpenTime;

    /**
     * @var string
     *
     * @ORM\Column(name="online_close_time", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $onlineCloseTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="days_in_booking_window", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $daysInBookingWindow;

    /**
     * @var boolean
     *
     * @ORM\Column(name="minimum_players", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $minimumPlayers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="limit_holes", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $limitHoles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="booking_carts", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $bookingCarts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $deleted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_customer_pricing", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $useCustomerPricing;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_aggregate", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isAggregate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pay_online", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $payOnline;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hide_online_prices", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $hideOnlinePrices;

    /**
     * @var boolean
     *
     * @ORM\Column(name="online_rate_setting", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $onlineRateSetting;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_full_details", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $showFullDetails;

    /**
     * @var string
     *
     * @ORM\Column(name="pass_item_ids", type="text", length=65535, precision=0, scale=0, nullable=true, unique=false)
     */
    private $passItemIds;

    /**
     * @var integer
     *
     * @ORM\Column(name="booking_fee_item_id", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $bookingFeeItemId;

    /**
     * @var string
     *
     * @ORM\Column(name="booking_fee_terms", type="string", length=5000, precision=0, scale=0, nullable=false, unique=false)
     */
    private $bookingFeeTerms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="booking_fee_enabled", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $bookingFeeEnabled;

    /**
     * @var boolean
     *
     * @ORM\Column(name="booking_fee_per_person", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $bookingFeePerPerson;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_name_entry", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $allowNameEntry;

    /**
     * @var \foreup\rest\models\entities\ForeupTeesheet
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupTeesheet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="teesheet_id", referencedColumnName="teesheet_id", nullable=true)
     * })
     */
    private $teesheet;


    /**
     * Get bookingClassId
     *
     * @return integer
     */
    public function getBookingClassId()
    {
        return $this->bookingClassId;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return ForeupBookingClasses
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupBookingClasses
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set priceClass
     *
     * @param string $priceClass
     *
     * @return ForeupBookingClasses
     */
    public function setPriceClass($priceClass)
    {
        $this->priceClass = $priceClass;

        return $this;
    }

    /**
     * Get priceClass
     *
     * @return string
     */
    public function getPriceClass()
    {
        return $this->priceClass;
    }

    /**
     * Set onlineBookingProtected
     *
     * @param boolean $onlineBookingProtected
     *
     * @return ForeupBookingClasses
     */
    public function setOnlineBookingProtected($onlineBookingProtected)
    {
        $this->onlineBookingProtected = $onlineBookingProtected;

        return $this;
    }

    /**
     * Get onlineBookingProtected
     *
     * @return boolean
     */
    public function getOnlineBookingProtected()
    {
        return $this->onlineBookingProtected;
    }

    /**
     * Set requireCreditCard
     *
     * @param boolean $requireCreditCard
     *
     * @return ForeupBookingClasses
     */
    public function setRequireCreditCard($requireCreditCard)
    {
        $this->requireCreditCard = $requireCreditCard;

        return $this;
    }

    /**
     * Get requireCreditCard
     *
     * @return boolean
     */
    public function getRequireCreditCard()
    {
        return $this->requireCreditCard;
    }

    /**
     * Set onlineOpenTime
     *
     * @param string $onlineOpenTime
     *
     * @return ForeupBookingClasses
     */
    public function setOnlineOpenTime($onlineOpenTime)
    {
        $this->onlineOpenTime = $onlineOpenTime;

        return $this;
    }

    /**
     * Get onlineOpenTime
     *
     * @return string
     */
    public function getOnlineOpenTime()
    {
        return $this->onlineOpenTime;
    }

    /**
     * Set onlineCloseTime
     *
     * @param string $onlineCloseTime
     *
     * @return ForeupBookingClasses
     */
    public function setOnlineCloseTime($onlineCloseTime)
    {
        $this->onlineCloseTime = $onlineCloseTime;

        return $this;
    }

    /**
     * Get onlineCloseTime
     *
     * @return string
     */
    public function getOnlineCloseTime()
    {
        return $this->onlineCloseTime;
    }

    /**
     * Set daysInBookingWindow
     *
     * @param integer $daysInBookingWindow
     *
     * @return ForeupBookingClasses
     */
    public function setDaysInBookingWindow($daysInBookingWindow)
    {
        $this->daysInBookingWindow = $daysInBookingWindow;

        return $this;
    }

    /**
     * Get daysInBookingWindow
     *
     * @return integer
     */
    public function getDaysInBookingWindow()
    {
        return $this->daysInBookingWindow;
    }

    /**
     * Set minimumPlayers
     *
     * @param boolean $minimumPlayers
     *
     * @return ForeupBookingClasses
     */
    public function setMinimumPlayers($minimumPlayers)
    {
        $this->minimumPlayers = $minimumPlayers;

        return $this;
    }

    /**
     * Get minimumPlayers
     *
     * @return boolean
     */
    public function getMinimumPlayers()
    {
        return $this->minimumPlayers;
    }

    /**
     * Set limitHoles
     *
     * @param boolean $limitHoles
     *
     * @return ForeupBookingClasses
     */
    public function setLimitHoles($limitHoles)
    {
        $this->limitHoles = $limitHoles;

        return $this;
    }

    /**
     * Get limitHoles
     *
     * @return boolean
     */
    public function getLimitHoles()
    {
        return $this->limitHoles;
    }

    /**
     * Set bookingCarts
     *
     * @param boolean $bookingCarts
     *
     * @return ForeupBookingClasses
     */
    public function setBookingCarts($bookingCarts)
    {
        $this->bookingCarts = $bookingCarts;

        return $this;
    }

    /**
     * Get bookingCarts
     *
     * @return boolean
     */
    public function getBookingCarts()
    {
        return $this->bookingCarts;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return ForeupBookingClasses
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set useCustomerPricing
     *
     * @param boolean $useCustomerPricing
     *
     * @return ForeupBookingClasses
     */
    public function setUseCustomerPricing($useCustomerPricing)
    {
        $this->useCustomerPricing = $useCustomerPricing;

        return $this;
    }

    /**
     * Get useCustomerPricing
     *
     * @return boolean
     */
    public function getUseCustomerPricing()
    {
        return $this->useCustomerPricing;
    }

    /**
     * Set isAggregate
     *
     * @param boolean $isAggregate
     *
     * @return ForeupBookingClasses
     */
    public function setIsAggregate($isAggregate)
    {
        $this->isAggregate = $isAggregate;

        return $this;
    }

    /**
     * Get isAggregate
     *
     * @return boolean
     */
    public function getIsAggregate()
    {
        return $this->isAggregate;
    }

    /**
     * Set payOnline
     *
     * @param boolean $payOnline
     *
     * @return ForeupBookingClasses
     */
    public function setPayOnline($payOnline)
    {
        $this->payOnline = $payOnline;

        return $this;
    }

    /**
     * Get payOnline
     *
     * @return boolean
     */
    public function getPayOnline()
    {
        return $this->payOnline;
    }

    /**
     * Set hideOnlinePrices
     *
     * @param boolean $hideOnlinePrices
     *
     * @return ForeupBookingClasses
     */
    public function setHideOnlinePrices($hideOnlinePrices)
    {
        $this->hideOnlinePrices = $hideOnlinePrices;

        return $this;
    }

    /**
     * Get hideOnlinePrices
     *
     * @return boolean
     */
    public function getHideOnlinePrices()
    {
        return $this->hideOnlinePrices;
    }

    /**
     * Set onlineRateSetting
     *
     * @param boolean $onlineRateSetting
     *
     * @return ForeupBookingClasses
     */
    public function setOnlineRateSetting($onlineRateSetting)
    {
        $this->onlineRateSetting = $onlineRateSetting;

        return $this;
    }

    /**
     * Get onlineRateSetting
     *
     * @return boolean
     */
    public function getOnlineRateSetting()
    {
        return $this->onlineRateSetting;
    }

    /**
     * Set showFullDetails
     *
     * @param boolean $showFullDetails
     *
     * @return ForeupBookingClasses
     */
    public function setShowFullDetails($showFullDetails)
    {
        $this->showFullDetails = $showFullDetails;

        return $this;
    }

    /**
     * Get showFullDetails
     *
     * @return boolean
     */
    public function getShowFullDetails()
    {
        return $this->showFullDetails;
    }

    /**
     * Set passItemIds
     *
     * @param string $passItemIds
     *
     * @return ForeupBookingClasses
     */
    public function setPassItemIds($passItemIds)
    {
        $this->passItemIds = $passItemIds;

        return $this;
    }

    /**
     * Get passItemIds
     *
     * @return string
     */
    public function getPassItemIds()
    {
        return $this->passItemIds;
    }

    /**
     * Set bookingFeeItemId
     *
     * @param integer $bookingFeeItemId
     *
     * @return ForeupBookingClasses
     */
    public function setBookingFeeItemId($bookingFeeItemId)
    {
        $this->bookingFeeItemId = $bookingFeeItemId;

        return $this;
    }

    /**
     * Get bookingFeeItemId
     *
     * @return integer
     */
    public function getBookingFeeItemId()
    {
        return $this->bookingFeeItemId;
    }

    /**
     * Set bookingFeeTerms
     *
     * @param string $bookingFeeTerms
     *
     * @return ForeupBookingClasses
     */
    public function setBookingFeeTerms($bookingFeeTerms)
    {
        $this->bookingFeeTerms = $bookingFeeTerms;

        return $this;
    }

    /**
     * Get bookingFeeTerms
     *
     * @return string
     */
    public function getBookingFeeTerms()
    {
        return $this->bookingFeeTerms;
    }

    /**
     * Set bookingFeeEnabled
     *
     * @param boolean $bookingFeeEnabled
     *
     * @return ForeupBookingClasses
     */
    public function setBookingFeeEnabled($bookingFeeEnabled)
    {
        $this->bookingFeeEnabled = $bookingFeeEnabled;

        return $this;
    }

    /**
     * Get bookingFeeEnabled
     *
     * @return boolean
     */
    public function getBookingFeeEnabled()
    {
        return $this->bookingFeeEnabled;
    }

    /**
     * Set bookingFeePerPerson
     *
     * @param boolean $bookingFeePerPerson
     *
     * @return ForeupBookingClasses
     */
    public function setBookingFeePerPerson($bookingFeePerPerson)
    {
        $this->bookingFeePerPerson = $bookingFeePerPerson;

        return $this;
    }

    /**
     * Get bookingFeePerPerson
     *
     * @return boolean
     */
    public function getBookingFeePerPerson()
    {
        return $this->bookingFeePerPerson;
    }

    /**
     * Set allowNameEntry
     *
     * @param boolean $allowNameEntry
     *
     * @return ForeupBookingClasses
     */
    public function setAllowNameEntry($allowNameEntry)
    {
        $this->allowNameEntry = $allowNameEntry;

        return $this;
    }

    /**
     * Get allowNameEntry
     *
     * @return boolean
     */
    public function getAllowNameEntry()
    {
        return $this->allowNameEntry;
    }

    /**
     * Set teesheet
     *
     * @param \foreup\rest\models\entities\ForeupTeesheet $teesheet
     *
     * @return ForeupBookingClasses
     */
    public function setTeesheet(\foreup\rest\models\entities\ForeupTeesheet $teesheet = null)
    {
        $this->teesheet = $teesheet;

        return $this;
    }

    /**
     * Get teesheet
     *
     * @return \foreup\rest\models\entities\ForeupTeesheet
     */
    public function getTeesheet()
    {
        return $this->teesheet;
    }
}

