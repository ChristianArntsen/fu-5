<?php
namespace foreup\rest\models\entities;
use \foreup\rest\models\entities\ForeupMarketingCampaigns;
use \foreup\rest\models\entities\ForeupRepeatable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ForeupRepeatable
 * @package foreup\rest\models\entities
 * @ORM\Entity
 */
class ForeupRepeatableMarketingCampaigns extends ForeupRepeatable
{
	/**
	 * @var ForeupMarketingCampaigns
	 *
	 * @ORM\ManyToOne(targetEntity="ForeupMarketingCampaigns")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="resource_id", referencedColumnName="campaign_id")
	 * })
	 */
	private $marketing_campaign;

	/**
	 * @return ForeupMarketingCampaigns
	 */
	public function getMarketingCampaign()
	{
		return $this->marketing_campaign;
	}

	/**
	 * @return ForeupMarketingCampaigns
	 */
	public function getResource()
	{
		return $this->getMarketingCampaign();
	}

	/**
	 * @param ForeupMarketingCampaigns $marketing_campaign
	 * @return ForeupRepeatableMarketingCampaigns
	 */
	public function setMarketingCampaign($marketing_campaign)
	{
		$this->marketing_campaign = $marketing_campaign;
		$type = 'marketing_campaign';
		$resource_id = $marketing_campaign->getCampaignId();
		$this->setType($type);
		$this->setResourceId($resource_id);
		return $this;
	}

	/**
	 * @param ForeupMarketingCampaigns $marketing_campaign
	 * @return ForeupRepeatableMarketingCampaigns
	 */
	public function setResource($item){
		return $this->setMarketingCampaign($item);
	}
}
?>