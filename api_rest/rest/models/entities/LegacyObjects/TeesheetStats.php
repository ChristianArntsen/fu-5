<?php
namespace foreup\rest\models\entities\LegacyObjects;
class TeesheetStats
{
	private $bookings,$playersCheckedIn,$playerNoShows,$occupancy,$revenue,$date,$potentialSlots,$slotsAvailable;

	/**
	 * @return mixed
	 */
	public function getPotentialSlots()
	{
		return $this->potentialSlots;
	}

	/**
	 * @param mixed $potentialSlots
	 */
	public function setPotentialSlots($potentialSlots)
	{
		$this->potentialSlots = $potentialSlots;
	}

	/**
	 * @return mixed
	 */
	public function getSlotsAvailable()
	{
		return $this->slotsAvailable;
	}

	/**
	 * @param mixed $slotsAvailable
	 */
	public function setSlotsAvailable($slotsAvailable)
	{
		$this->slotsAvailable = $slotsAvailable;
	}

	/**
	 * @return mixed
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param mixed $date
	 */
	public function setDate($date)
	{
		$this->date = $date;
	}


	/**
	 * TeesheetStats constructor.
	 */
	public function __construct()
	{
	}

	/**
	 * @return mixed
	 */
	public function getOccupancy()
	{
		return $this->occupancy;
	}

	/**
	 * @param mixed $occupancy
	 */
	public function setOccupancy($occupancy)
	{
		$this->occupancy = $occupancy;
	}

	/**
	 * @return mixed
	 */
	public function getRevenue()
	{
		return $this->revenue;
	}

	/**
	 * @param mixed $revenue
	 */
	public function setRevenue($revenue)
	{
		$this->revenue = $revenue;
	}

	/**
	 * @return mixed
	 */
	public function getBookings()
	{
		return $this->bookings;
	}

	/**
	 * @param mixed $bookings
	 */
	public function setBookings($bookings)
	{
		$this->bookings = $bookings;
	}

	/**
	 * @return mixed
	 */
	public function getPlayersCheckedIn()
	{
		return $this->playersCheckedIn;
	}

	/**
	 * @param mixed $playersCheckedIn
	 */
	public function setPlayersCheckedIn($playersCheckedIn)
	{
		$this->playersCheckedIn = $playersCheckedIn;
	}

	/**
	 * @return mixed
	 */
	public function getPlayerNoShows()
	{
		return $this->playerNoShows;
	}

	/**
	 * @param mixed $playerNoShows
	 */
	public function setPlayerNoShows($playerNoShows)
	{
		$this->playerNoShows = $playerNoShows;
	}



}