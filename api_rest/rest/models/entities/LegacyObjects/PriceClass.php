<?php
namespace foreup\rest\models\entities\LegacyObjects;
use Carbon\Carbon;

class PriceClass
{
	private $classId;
	private $name;
	private $default;
	private $color;
	private $cart;
	private $cartGlCode;
	private $greenGlCode;
	private $isShared;
	private $pricing;


	public function __construct($apiResponse)
	{
		$this->setClassId($apiResponse->class_id);
		$this->setName($apiResponse->name);
		$this->setDefault($apiResponse->default);
		$this->setColor($apiResponse->color);
		$this->setCart($apiResponse->cart);
		$this->setCartGlCode($apiResponse->cart_gl_code);
		$this->setGreenGlCode($apiResponse->green_gl_code);
		$this->setIsShared($apiResponse->is_shared);
		$this->setPricing($apiResponse->pricing);
	}

	/**
	 * @return Pricing
	 */
	public function getPricing()
	{
		return $this->pricing;
	}

	/**
	 * @param mixed $pricing
	 */
	public function setPricing($pricing)
	{
		$this->pricing = new Pricing($pricing);
	}

	/**
	 * @return mixed
	 */
	public function getClassId()
	{
		return $this->classId;
	}

	/**
	 * @param mixed $classId
	 */
	public function setClassId($classId)
	{
		$this->classId = $classId;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getDefault()
	{
		return (boolean)$this->default;
	}

	/**
	 * @param mixed $default
	 */
	public function setDefault($default)
	{
		$this->default = $default;
	}

	/**
	 * @return mixed
	 */
	public function getColor()
	{
		return $this->color;
	}

	/**
	 * @param mixed $color
	 */
	public function setColor($color)
	{
		$this->color = $color;
	}

	/**
	 * @return mixed
	 */
	public function getCart()
	{
		return $this->cart;
	}

	/**
	 * @param mixed $cart
	 */
	public function setCart($cart)
	{
		$this->cart = $cart;
	}

	/**
	 * @return mixed
	 */
	public function getCartGlCode()
	{
		return $this->cartGlCode;
	}

	/**
	 * @param mixed $cartGlCode
	 */
	public function setCartGlCode($cartGlCode)
	{
		$this->cartGlCode = $cartGlCode;
	}

	/**
	 * @return mixed
	 */
	public function getGreenGlCode()
	{
		return $this->greenGlCode;
	}

	/**
	 * @param mixed $greenGlCode
	 */
	public function setGreenGlCode($greenGlCode)
	{
		$this->greenGlCode = $greenGlCode;
	}

	/**
	 * @return mixed
	 */
	public function getIsShared()
	{
		return $this->isShared;
	}

	/**
	 * @param mixed $isShared
	 */
	public function setIsShared($isShared)
	{
		$this->isShared = $isShared;
	}


}