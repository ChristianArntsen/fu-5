<?php
namespace foreup\rest\models\entities\LegacyObjects;
use Carbon\Carbon;

class TeetimeSlot
{
	private $availableSpots;
	private $bookingClassId;
	private $bookingFeePrice;
	private $bookingFeeRequired;
	private $cartFeeTax;
	private $cartFeeTaxRate;
	private $courseId;
	private $courseName;
	private $foreupDiscount;
	private $greenFee;
	private $cartFee = 0;
	private $greenFeeTax;
	private $greenFeeTaxRate;
	private $groupId;
	private $hasSpecial;
	private $holes;
	private $minimumPlayers;
	private $payOnline;
	private $rateType;
	private $requireCreditCard;
	private $scheduleId;
	private $scheduleName;
	private $specialDiscountPercentage;
	private $specialId;
	private $teesheetHoles;
	private $teesheetId;
	private $time;
	private $timezone;

	public function __construct($apiResponse)
	{
		$this->setTeesheetId($apiResponse->teesheet_id);
		$this->setTeesheetHoles($apiResponse->teesheet_holes);
		$this->setTimezone($apiResponse->timezone);
		$this->setTime($apiResponse->time);
		$this->setCourseId($apiResponse->course_id);
		$this->setCourseName($apiResponse->course_name);
		$this->setScheduleName($apiResponse->schedule_name);
		$this->setScheduleId($apiResponse->schedule_id);
		$this->setAvailableSpots($apiResponse->available_spots);
		$this->setMinimumPlayers($apiResponse->minimum_players);
		$this->setHoles($apiResponse->holes);
		$this->setHasSpecial($apiResponse->has_special);
		$this->setSpecialDiscountPercentage($apiResponse->special_discount_percentage);
		$this->setGroupId($apiResponse->group_id);
		$this->setRequireCreditCard($apiResponse->require_credit_card);
		$this->setBookingClassId($apiResponse->booking_class_id);
		$this->setBookingFeeRequired($apiResponse->booking_fee_required);
		$this->setBookingFeePrice($apiResponse->booking_fee_price);
		$this->setGreenFeeTaxRate($apiResponse->green_fee_tax_rate);
		$this->setCartFeeTax($apiResponse->cart_fee_tax);
		$this->setSpecialId($apiResponse->special_id);
		$this->setForeupDiscount($apiResponse->foreup_discount);
		$this->setPayOnline($apiResponse->pay_online);
		isset($apiResponse->green_fee)?$this->setGreenFee($apiResponse->green_fee):'';
		isset($apiResponse->green_fee_tax)?$this->setGreenFeeTax($apiResponse->green_fee_tax):'';
		isset($apiResponse->cart_fee)? $this->setCartFee($apiResponse->cart_fee):'';
		$this->setRateType($apiResponse->rate_type);
	}

	/**
	 * @return mixed
	 */
	public function getCartFee()
	{
		return $this->cartFee;
	}

	/**
	 * @param mixed $cartFee
	 */
	public function setCartFee($cartFee)
	{
		$this->cartFee = $cartFee;
	}

	/**
	 * @return mixed
	 */
	public function getTimezone()
	{
		return $this->timezone;
	}

	/**
	 * @param mixed $timezone
	 */
	public function setTimezone($timezone)
	{
		if($this->time instanceof Carbon)
			$this->time->timezone = $timezone;
		$this->timezone = $timezone;
	}

	/**
	 * @return mixed
	 */
	public function getAvailableSpots()
	{
		return $this->availableSpots;
	}

	/**
	 * @param mixed $availableSpots
	 */
	public function setAvailableSpots($availableSpots)
	{
		$this->availableSpots = $availableSpots;
	}

	/**
	 * @return mixed
	 */
	public function getBookingClassId()
	{
		return $this->bookingClassId;
	}

	/**
	 * @param mixed $bookingClassId
	 */
	public function setBookingClassId($bookingClassId)
	{
		$this->bookingClassId = $bookingClassId;
	}

	/**
	 * @return mixed
	 */
	public function getBookingFeePrice()
	{
		return $this->bookingFeePrice;
	}

	/**
	 * @param mixed $bookingFeePrice
	 */
	public function setBookingFeePrice($bookingFeePrice)
	{
		$this->bookingFeePrice = $bookingFeePrice;
	}

	/**
	 * @return mixed
	 */
	public function getBookingFeeRequired()
	{
		return $this->bookingFeeRequired;
	}

	/**
	 * @param mixed $bookingFeeRequired
	 */
	public function setBookingFeeRequired($bookingFeeRequired)
	{
		$this->bookingFeeRequired = $bookingFeeRequired;
	}

	/**
	 * @return mixed
	 */
	public function getCartFeeTax()
	{
		return $this->cartFeeTax;
	}

	/**
	 * @param mixed $cartFeeTax
	 */
	public function setCartFeeTax($cartFeeTax)
	{
		$this->cartFeeTax = $cartFeeTax;
	}

	/**
	 * @return mixed
	 */
	public function getCartFeeTaxRate()
	{
		return $this->cartFeeTaxRate;
	}

	/**
	 * @param mixed $cartFeeTaxRate
	 */
	public function setCartFeeTaxRate($cartFeeTaxRate)
	{
		$this->cartFeeTaxRate = $cartFeeTaxRate;
	}

	/**
	 * @return mixed
	 */
	public function getCourseId()
	{
		return $this->courseId;
	}

	/**
	 * @param mixed $courseId
	 */
	public function setCourseId($courseId)
	{
		$this->courseId = $courseId;
	}

	/**
	 * @return mixed
	 */
	public function getCourseName()
	{
		return $this->courseName;
	}

	/**
	 * @param mixed $courseName
	 */
	public function setCourseName($courseName)
	{
		$this->courseName = $courseName;
	}

	/**
	 * @return mixed
	 */
	public function getForeupDiscount()
	{
		return $this->foreupDiscount;
	}

	/**
	 * @param mixed $foreupDiscount
	 */
	public function setForeupDiscount($foreupDiscount)
	{
		$this->foreupDiscount = $foreupDiscount;
	}

	/**
	 * @return mixed
	 */
	public function getGreenFee()
	{
		return $this->greenFee;
	}

	/**
	 * @param mixed $greenFee
	 */
	public function setGreenFee($greenFee)
	{
		$this->greenFee = $greenFee;
	}

	/**
	 * @return mixed
	 */
	public function getGreenFeeTax()
	{
		return $this->greenFeeTax;
	}

	/**
	 * @param mixed $greenFeeTax
	 */
	public function setGreenFeeTax($greenFeeTax)
	{
		$this->greenFeeTax = $greenFeeTax;
	}

	/**
	 * @return mixed
	 */
	public function getGreenFeeTaxRate()
	{
		return $this->greenFeeTaxRate;
	}

	/**
	 * @param mixed $greenFeeTaxRate
	 */
	public function setGreenFeeTaxRate($greenFeeTaxRate)
	{
		$this->greenFeeTaxRate = $greenFeeTaxRate;
	}

	/**
	 * @return mixed
	 */
	public function getGroupId()
	{
		return $this->groupId;
	}

	/**
	 * @param mixed $groupId
	 */
	public function setGroupId($groupId)
	{
		$this->groupId = $groupId;
	}

	/**
	 * @return mixed
	 */
	public function getHasSpecial()
	{
		return $this->hasSpecial;
	}

	/**
	 * @param mixed $hasSpecial
	 */
	public function setHasSpecial($hasSpecial)
	{
		$this->hasSpecial = $hasSpecial;
	}

	/**
	 * @return mixed
	 */
	public function getHoles()
	{
		return $this->holes;
	}

	/**
	 * @param mixed $holes
	 */
	public function setHoles($holes)
	{
		$this->holes = $holes;
	}

	/**
	 * @return mixed
	 */
	public function getMinimumPlayers()
	{
		return $this->minimumPlayers;
	}

	/**
	 * @param mixed $minimumPlayers
	 */
	public function setMinimumPlayers($minimumPlayers)
	{
		$this->minimumPlayers = $minimumPlayers;
	}

	/**
	 * @return mixed
	 */
	public function getPayOnline()
	{
		return $this->payOnline;
	}

	/**
	 * @param mixed $payOnline
	 */
	public function setPayOnline($payOnline)
	{
		$this->payOnline = $payOnline;
	}

	/**
	 * @return mixed
	 */
	public function getRateType()
	{
		return $this->rateType;
	}

	/**
	 * @param mixed $rateType
	 */
	public function setRateType($rateType)
	{
		$this->rateType = $rateType;
	}

	/**
	 * @return mixed
	 */
	public function getRequireCreditCard()
	{
		return $this->requireCreditCard;
	}

	/**
	 * @param mixed $requireCreditCard
	 */
	public function setRequireCreditCard($requireCreditCard)
	{
		$this->requireCreditCard = $requireCreditCard;
	}

	/**
	 * @return mixed
	 */
	public function getScheduleId()
	{
		return $this->scheduleId;
	}

	/**
	 * @param mixed $scheduleId
	 */
	public function setScheduleId($scheduleId)
	{
		$this->scheduleId = $scheduleId;
	}

	/**
	 * @return mixed
	 */
	public function getScheduleName()
	{
		return $this->scheduleName;
	}

	/**
	 * @param mixed $scheduleName
	 */
	public function setScheduleName($scheduleName)
	{
		$this->scheduleName = $scheduleName;
	}

	/**
	 * @return mixed
	 */
	public function getSpecialDiscountPercentage()
	{
		return $this->specialDiscountPercentage;
	}

	/**
	 * @param mixed $specialDiscountPercentage
	 */
	public function setSpecialDiscountPercentage($specialDiscountPercentage)
	{
		$this->specialDiscountPercentage = $specialDiscountPercentage;
	}

	/**
	 * @return mixed
	 */
	public function getSpecialId()
	{
		return $this->specialId;
	}

	/**
	 * @param mixed $specialId
	 */
	public function setSpecialId($specialId)
	{
		$this->specialId = $specialId;
	}

	/**
	 * @return mixed
	 */
	public function getTeesheetHoles()
	{
		return $this->teesheetHoles;
	}

	/**
	 * @param mixed $teesheetHoles
	 */
	public function setTeesheetHoles($teesheetHoles)
	{
		$this->teesheetHoles = $teesheetHoles;
	}

	/**
	 * @return mixed
	 */
	public function getTeesheetId()
	{
		return $this->teesheetId;
	}

	/**
	 * @param mixed $teesheetId
	 */
	public function setTeesheetId($teesheetId)
	{
		$this->teesheetId = $teesheetId;
	}

	/**
	 * @return mixed
	 */
	public function getTime()
	{
		return $this->time;
	}

	/**
	 * @param mixed $time
	 */
	public function setTime($time)
	{
		$this->time = Carbon::parse($time,$this->timezone);

	}


}