<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupSalesPaymentsCreditCards
 *
 * @ORM\Table(name="foreup_sales_payments_credit_cards")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForeupSalesPaymentsCreditCards
{
	use \foreup\rest\models\entities\EntityValidator;

	/**
	 * @ORM\PrePersist @ORM\PreUpdate
	 */
	public function validate($throw = true)
	{

		$this->resetLastError();
		$location = 'ForeupSalesPaymentsCreditCards->validate';

		$cid = $this->getCid();
		$v = $this->validate_integer($location,'cid',$cid,true,$throw);
		if($v!==true)return $v;

		$courseId = $this->getCourseId();
		$v = $this->validate_integer($location,'courseId',$courseId,true,$throw);
		if($v!==true)return $v;

		$mercuryId = $this->getMercuryId();
		$v = $this->validate_string($location,'mercuryId',$mercuryId,true,$throw);
		if($v!==true)return $v;

		$mercuryPassword = $this->getMercuryPassword();
		$v = $this->validate_string($location,'mercuryPassword',$mercuryPassword,true,$throw);
		if($v!==true)return $v;

		$etsId = $this->getEtsId();
		$v = $this->validate_string($location,'etsId',$etsId,true,$throw);
		if($v!==true)return $v;

		$tranType = $this->getTranType();
		$v = $this->validate_string($location,'tranType',$tranType,true,$throw);
		if($v!==true)return $v;

		$amount = $this->getAmount();
		$v = $this->validate_numeric($location,'amount',$amount,true,$throw);
		if($v!==true)return $v;

		$authAmount = $this->getAuthAmount();
		$v = $this->validate_numeric($location,'authAmount',$authAmount,true,$throw);
		if($v!==true)return $v;

		$cardType = $this->getCardType();
		$v = $this->validate_string($location,'cardType',$cardType,true,$throw);
		if($v!==true)return $v;

		$maskedAccount = $this->getMaskedAccount();
		$v = $this->validate_string($location,'maskedAccount',$maskedAccount,true,$throw);
		if($v!==true)return $v;

		$cardholderName = $this->getCardholderName();
		$v = $this->validate_string($location,'cardholderName',$cardholderName,true,$throw);
		if($v!==true)return $v;

		$refNo = $this->getRefNo();
		$v = $this->validate_string($location,'refNo',$refNo,true,$throw);
		if($v!==true)return $v;

		$operatorId = $this->getOperatorId();
		$v = $this->validate_string($location,'operatorId',$operatorId,true,$throw);
		if($v!==true)return $v;

		$terminalName = $this->getTerminalName();
		$v = $this->validate_string($location,'terminalName',$terminalName,true,$throw);
		if($v!==true)return $v;

		$transPostTime = $this->getTransPostTime();
		$v = $this->validate_object($location,'transPostTime',$transPostTime,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$authCode = $this->getAuthCode();
		$v = $this->validate_string($location,'authCode',$authCode,true,$throw);
		if($v!==true)return $v;

		$voiceAuthCode = $this->getVoiceAuthCode();
		$v = $this->validate_string($location,'voiceAuthCode',$voiceAuthCode,true,$throw);
		if($v!==true)return $v;

		$paymentId = $this->getPaymentId();
		$v = $this->validate_string($location,'paymentId',$paymentId,true,$throw);
		if($v!==true)return $v;

		$acqRefData = $this->getAcqRefData();
		$v = $this->validate_string($location,'acqRefData',$acqRefData,true,$throw);
		if($v!==true)return $v;

		$processData = $this->getProcessData();
		$v = $this->validate_string($location,'processData',$processData,true,$throw);
		if($v!==true)return $v;

		$token = $this->getToken();
		$v = $this->validate_string($location,'token',$token,true,$throw);
		if($v!==true)return $v;

		$tokenUsed = $this->getTokenUsed();
		$v = $this->validate_boolean($location,'tokenUsed',$tokenUsed,true,$throw);
		if($v!==true)return $v;

		$amountRefunded = $this->getAmountRefunded();
		$v = $this->validate_numeric($location,'amountRefunded',$amountRefunded,true,$throw);
		if($v!==true)return $v;

		$frequency = $this->getFrequency();
		$v = $this->validate_string($location,'frequency',$frequency,true,$throw);
		if($v!==true)return $v;

		$responseCode = $this->getResponseCode();
		$v = $this->validate_integer($location,'responseCode',$responseCode,true,$throw);
		if($v!==true)return $v;

		$status = $this->getStatus();
		$v = $this->validate_string($location,'status',$status,true,$throw);
		if($v!==true)return $v;

		$statusMessage = $this->getStatusMessage();
		$v = $this->validate_string($location,'statusMessage',$statusMessage,true,$throw);
		if($v!==true)return $v;

		$displayMessage = $this->getDisplayMessage();
		$v = $this->validate_string($location,'displayMessage',$displayMessage,true,$throw);
		if($v!==true)return $v;

		$avsResult = $this->getAvsResult();
		$v = $this->validate_string($location,'avsResult',$avsResult,true,$throw);
		if($v!==true)return $v;

		$cvvResult = $this->getCvvResult();
		$v = $this->validate_string($location,'cvvResult',$cvvResult,true,$throw);
		if($v!==true)return $v;

		$taxAmount = $this->getTaxAmount();
		$v = $this->validate_numeric($location,'taxAmount',$taxAmount,true,$throw);
		if($v!==true)return $v;

		$avsAddress = $this->getAvsAddress();
		$v = $this->validate_string($location,'avsAddress',$avsAddress,true,$throw);
		if($v!==true)return $v;

		$avsZip = $this->getAvsZip();
		$v = $this->validate_string($location,'avsZip',$avsZip,true,$throw);
		if($v!==true)return $v;

		$paymentIdExpired = $this->getPaymentIdExpired();
		$v = $this->validate_string($location,'paymentIdExpired',$paymentIdExpired,true,$throw);
		if($v!==true)return $v;

		$customerCode = $this->getCustomerCode();
		$v = $this->validate_string($location,'customerCode',$customerCode,true,$throw);
		if($v!==true)return $v;

		$memo = $this->getMemo();
		$v = $this->validate_string($location,'memo',$memo,true,$throw);
		if($v!==true)return $v;

		$batchNo = $this->getBatchNo();
		$v = $this->validate_string($location,'batchNo',$batchNo,true,$throw);
		if($v!==true)return $v;

		$gratuityAmount = $this->getGratuityAmount();
		$v = $this->validate_numeric($location,'gratuityAmount',$gratuityAmount,true,$throw);
		if($v!==true)return $v;

		$voided = $this->getVoided();
		$v = $this->validate_boolean($location,'voided',$voided,true,$throw);
		if($v!==true)return $v;

		$initiationTime = $this->getInitiationTime();
		$v = $this->validate_object($location,'initiationTime',$initiationTime,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$primaryInvoice = $this->getPrimaryInvoice();
		$v = $this->validate_integer($location,'primaryInvoice',$primaryInvoice,true,$throw);
		if($v!==true)return $v;

		$cartId = $this->getCartId();
		$v = $this->validate_integer($location,'cartId',$cartId,true,$throw);
		if($v!==true)return $v;

		$mobileGuid = $this->getMobileGuid();
		$v = $this->validate_string($location,'mobileGuid',$mobileGuid,true,$throw);
		if($v!==true)return $v;

		$successCheckCount = $this->getSuccessCheckCount();
		$v = $this->validate_boolean($location,'successCheckCount',$successCheckCount,true,$throw);
		if($v!==true)return $v;

		$cancelTime = $this->getCancelTime();
		$v = $this->validate_object($location,'cancelTime',$cancelTime,'\DateTime',true,$throw);
		if($v!==true)return $v;

		$signatureData = $this->getSignatureData();
		$v = $this->validate_string($location,'signatureData',$signatureData,true,$throw);
		if($v!==true)return $v;

		$emvPayment = $this->getEmvPayment();
		$v = $this->validate_boolean($location,'emvPayment',$emvPayment,false,$throw);
		if($v!==true)return $v;

		$toBeVoided = $this->getToBeVoided();
		$v = $this->validate_boolean($location,'toBeVoided',$toBeVoided,true,$throw);
		if($v!==true)return $v;



		return true;
	}

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $invoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="CID", type="integer", nullable=false)
     */
    private $cid = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var string
     *
     * @ORM\Column(name="mercury_id", type="string", length=255, nullable=false)
     */
    private $mercuryId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mercury_password", type="string", length=255, nullable=false)
     */
    private $mercuryPassword = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ets_id", type="string", length=255, nullable=false)
     */
    private $etsId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="tran_type", type="string", length=255, nullable=false)
     */
    private $tranType;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", precision=15, scale=2, nullable=false)
     */
    private $amount;

    /**
     * @var float
     *
     * @ORM\Column(name="auth_amount", type="float", precision=15, scale=2, nullable=false)
     */
    private $authAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="card_type", type="string", length=255, nullable=false)
     */
    private $cardType = '';

    /**
     * @var string
     *
     * @ORM\Column(name="masked_account", type="string", length=255, nullable=false)
     */
    private $maskedAccount = '';

    /**
     * @var string
     *
     * @ORM\Column(name="cardholder_name", type="string", length=255, nullable=false)
     */
    private $cardholderName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ref_no", type="string", length=255, nullable=false)
     */
    private $refNo = '';

    /**
     * @var string
     *
     * @ORM\Column(name="operator_id", type="string", length=255, nullable=false)
     */
    private $operatorId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="terminal_name", type="string", length=255, nullable=false)
     */
    private $terminalName = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="trans_post_time", type="datetime", nullable=false)
     */
    private $transPostTime = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="auth_code", type="string", length=255, nullable=false)
     */
    private $authCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="voice_auth_code", type="string", length=255, nullable=false)
     */
    private $voiceAuthCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="payment_id", type="string", length=255, nullable=false)
     */
    private $paymentId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="acq_ref_data", type="string", length=255, nullable=false)
     */
    private $acqRefData = '';

    /**
     * @var string
     *
     * @ORM\Column(name="process_data", type="string", length=255, nullable=false)
     */
    private $processData = '';

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=false)
     */
    private $token = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="token_used", type="boolean", nullable=false)
     */
    private $tokenUsed = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="amount_refunded", type="float", precision=15, scale=2, nullable=false)
     */
    private $amountRefunded = 0.00;

    /**
     * @var string
     *
     * @ORM\Column(name="frequency", type="string", length=9, nullable=false)
     */
    private $frequency = 'OneTime';

    /**
     * @var integer
     *
     * @ORM\Column(name="response_code", type="smallint", nullable=false)
     */
    private $responseCode = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=30, nullable=false)
     */
    private $status = '';

    /**
     * @var string
     *
     * @ORM\Column(name="status_message", type="string", length=255, nullable=false)
     */
    private $statusMessage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="display_message", type="string", length=255, nullable=false)
     */
    private $displayMessage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="avs_result", type="string", length=20, nullable=false)
     */
    private $avsResult = '';

    /**
     * @var string
     *
     * @ORM\Column(name="cvv_result", type="string", length=20, nullable=false)
     */
    private $cvvResult = '';

    /**
     * @var float
     *
     * @ORM\Column(name="tax_amount", type="float", precision=15, scale=2, nullable=false)
     */
    private $taxAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="avs_address", type="string", length=255, nullable=false)
     */
    private $avsAddress = '';

    /**
     * @var string
     *
     * @ORM\Column(name="avs_zip", type="string", length=20, nullable=false)
     */
    private $avsZip = '';

    /**
     * @var string
     *
     * @ORM\Column(name="payment_id_expired", type="string", length=10, nullable=false)
     */
    private $paymentIdExpired = '';

    /**
     * @var string
     *
     * @ORM\Column(name="customer_code", type="string", length=255, nullable=false)
     */
    private $customerCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="string", length=255, nullable=false)
     */
    private $memo = '';

    /**
     * @var string
     *
     * @ORM\Column(name="batch_no", type="string", length=6, nullable=false)
     */
    private $batchNo = '';

    /**
     * @var float
     *
     * @ORM\Column(name="gratuity_amount", type="float", precision=15, scale=2, nullable=false)
     */
    private $gratuityAmount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="voided", type="integer", nullable=false)
     */
    private $voided = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="initiation_time", type="datetime", nullable=false)
     */
    private $initiationTime = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="primary_invoice", type="integer", nullable=false)
     */
    private $primaryInvoice = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="cart_id", type="integer", nullable=false)
     */
    private $cartId = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_guid", type="string", length=255, nullable=false)
     */
    private $mobileGuid = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="success_check_count", type="boolean", nullable=false)
     */
    private $successCheckCount = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cancel_time", type="datetime", nullable=false)
     */
    private $cancelTime = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="signature_data", type="text", length=65535, nullable=false)
     */
    private $signatureData = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="emv_payment", type="boolean", nullable=true)
     */
    private $emvPayment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="to_be_voided", type="boolean", nullable=false)
     */
    private $toBeVoided = '0';



	/**
	 * Get invoice
	 *
	 * @return integer
	 */
	public function getInvoice()
	{
		return $this->invoice;
	}

	/**
	 * Get invoice
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->invoice;
	}

    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    
        return $this;
    }

    /**
     * Get cid
     *
     * @return integer
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    
        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set mercuryId
     *
     * @param string $mercuryId
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setMercuryId($mercuryId)
    {
        $this->mercuryId = $mercuryId;
    
        return $this;
    }

    /**
     * Get mercuryId
     *
     * @return string
     */
    public function getMercuryId()
    {
        return $this->mercuryId;
    }

    /**
     * Set mercuryPassword
     *
     * @param string $mercuryPassword
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setMercuryPassword($mercuryPassword)
    {
        $this->mercuryPassword = $mercuryPassword;
    
        return $this;
    }

    /**
     * Get mercuryPassword
     *
     * @return string
     */
    public function getMercuryPassword()
    {
        return $this->mercuryPassword;
    }

    /**
     * Set etsId
     *
     * @param string $etsId
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setEtsId($etsId)
    {
        $this->etsId = $etsId;
    
        return $this;
    }

    /**
     * Get etsId
     *
     * @return string
     */
    public function getEtsId()
    {
        return $this->etsId;
    }

    /**
     * Set tranType
     *
     * @param string $tranType
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setTranType($tranType)
    {
        $this->tranType = $tranType;
    
        return $this;
    }

    /**
     * Get tranType
     *
     * @return string
     */
    public function getTranType()
    {
        return $this->tranType;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set authAmount
     *
     * @param float $authAmount
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setAuthAmount($authAmount)
    {
        $this->authAmount = $authAmount;
    
        return $this;
    }

    /**
     * Get authAmount
     *
     * @return float
     */
    public function getAuthAmount()
    {
        return $this->authAmount;
    }

    /**
     * Set cardType
     *
     * @param string $cardType
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
    
        return $this;
    }

    /**
     * Get cardType
     *
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * Set maskedAccount
     *
     * @param string $maskedAccount
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setMaskedAccount($maskedAccount)
    {
        $this->maskedAccount = $maskedAccount;
    
        return $this;
    }

    /**
     * Get maskedAccount
     *
     * @return string
     */
    public function getMaskedAccount()
    {
        return $this->maskedAccount;
    }

    /**
     * Set cardholderName
     *
     * @param string $cardholderName
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setCardholderName($cardholderName)
    {
        $this->cardholderName = $cardholderName;
    
        return $this;
    }

    /**
     * Get cardholderName
     *
     * @return string
     */
    public function getCardholderName()
    {
        return $this->cardholderName;
    }

    /**
     * Set refNo
     *
     * @param string $refNo
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setRefNo($refNo)
    {
        $this->refNo = $refNo;
    
        return $this;
    }

    /**
     * Get refNo
     *
     * @return string
     */
    public function getRefNo()
    {
        return $this->refNo;
    }

    /**
     * Set operatorId
     *
     * @param string $operatorId
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setOperatorId($operatorId)
    {
        $this->operatorId = $operatorId;
    
        return $this;
    }

    /**
     * Get operatorId
     *
     * @return string
     */
    public function getOperatorId()
    {
        return $this->operatorId;
    }

    /**
     * Set terminalName
     *
     * @param string $terminalName
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setTerminalName($terminalName)
    {
        $this->terminalName = $terminalName;
    
        return $this;
    }

    /**
     * Get terminalName
     *
     * @return string
     */
    public function getTerminalName()
    {
        return $this->terminalName;
    }

    /**
     * Set transPostTime
     *
     * @param \DateTime $transPostTime
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setTransPostTime($transPostTime)
    {
	    if(is_string($transPostTime) && strtotime($transPostTime))$transPostTime = new \DateTime($transPostTime);
        $this->transPostTime = $transPostTime;
    
        return $this;
    }

    /**
     * Get transPostTime
     *
     * @return \DateTime
     */
    public function getTransPostTime()
    {
    	// convert default to DateTime
    	if(is_string($this->transPostTime)){
    		$this->setTransPostTime($this->transPostTime);
	    }
        return $this->transPostTime;
    }

    /**
     * Set authCode
     *
     * @param string $authCode
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setAuthCode($authCode)
    {
        $this->authCode = $authCode;
    
        return $this;
    }

    /**
     * Get authCode
     *
     * @return string
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * Set voiceAuthCode
     *
     * @param string $voiceAuthCode
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setVoiceAuthCode($voiceAuthCode)
    {
        $this->voiceAuthCode = $voiceAuthCode;
    
        return $this;
    }

    /**
     * Get voiceAuthCode
     *
     * @return string
     */
    public function getVoiceAuthCode()
    {
        return $this->voiceAuthCode;
    }

    /**
     * Set paymentId
     *
     * @param string $paymentId
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;
    
        return $this;
    }

    /**
     * Get paymentId
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Set acqRefData
     *
     * @param string $acqRefData
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setAcqRefData($acqRefData)
    {
        $this->acqRefData = $acqRefData;
    
        return $this;
    }

    /**
     * Get acqRefData
     *
     * @return string
     */
    public function getAcqRefData()
    {
        return $this->acqRefData;
    }

    /**
     * Set processData
     *
     * @param string $processData
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setProcessData($processData)
    {
        $this->processData = $processData;
    
        return $this;
    }

    /**
     * Get processData
     *
     * @return string
     */
    public function getProcessData()
    {
        return $this->processData;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set tokenUsed
     *
     * @param boolean $tokenUsed
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setTokenUsed($tokenUsed)
    {
	    if(is_string($tokenUsed)){
			$tokenUsed = strtolower($tokenUsed);
		    $ac = json_decode($tokenUsed,true);
		    if(isset($ac))$tokenUsed = $ac;
	    }

	    $this->tokenUsed = $tokenUsed;
    
        return $this;
    }

    /**
     * Get tokenUsed
     *
     * @return boolean
     */
    public function getTokenUsed()
    {
        return $this->tokenUsed;
    }

    /**
     * Set amountRefunded
     *
     * @param float $amountRefunded
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setAmountRefunded($amountRefunded)
    {
        $this->amountRefunded = $amountRefunded;
    
        return $this;
    }

    /**
     * Get amountRefunded
     *
     * @return float
     */
    public function getAmountRefunded()
    {
        return $this->amountRefunded;
    }

    /**
     * Set frequency
     *
     * @param string $frequency
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    
        return $this;
    }

    /**
     * Get frequency
     *
     * @return string
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set responseCode
     *
     * @param integer $responseCode
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;
    
        return $this;
    }

    /**
     * Get responseCode
     *
     * @return integer
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set statusMessage
     *
     * @param string $statusMessage
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setStatusMessage($statusMessage)
    {
        $this->statusMessage = $statusMessage;
    
        return $this;
    }

    /**
     * Get statusMessage
     *
     * @return string
     */
    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    /**
     * Set displayMessage
     *
     * @param string $displayMessage
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setDisplayMessage($displayMessage)
    {
        $this->displayMessage = $displayMessage;
    
        return $this;
    }

    /**
     * Get displayMessage
     *
     * @return string
     */
    public function getDisplayMessage()
    {
        return $this->displayMessage;
    }

    /**
     * Set avsResult
     *
     * @param string $avsResult
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setAvsResult($avsResult)
    {
        $this->avsResult = $avsResult;
    
        return $this;
    }

    /**
     * Get avsResult
     *
     * @return string
     */
    public function getAvsResult()
    {
        return $this->avsResult;
    }

    /**
     * Set cvvResult
     *
     * @param string $cvvResult
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setCvvResult($cvvResult)
    {
        $this->cvvResult = $cvvResult;
    
        return $this;
    }

    /**
     * Get cvvResult
     *
     * @return string
     */
    public function getCvvResult()
    {
        return $this->cvvResult;
    }

    /**
     * Set taxAmount
     *
     * @param float $taxAmount
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setTaxAmount($taxAmount)
    {
        $this->taxAmount = $taxAmount;
    
        return $this;
    }

    /**
     * Get taxAmount
     *
     * @return float
     */
    public function getTaxAmount()
    {
        return $this->taxAmount;
    }

    /**
     * Set avsAddress
     *
     * @param string $avsAddress
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setAvsAddress($avsAddress)
    {
        $this->avsAddress = $avsAddress;
    
        return $this;
    }

    /**
     * Get avsAddress
     *
     * @return string
     */
    public function getAvsAddress()
    {
        return $this->avsAddress;
    }

    /**
     * Set avsZip
     *
     * @param string $avsZip
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setAvsZip($avsZip)
    {
        $this->avsZip = $avsZip;
    
        return $this;
    }

    /**
     * Get avsZip
     *
     * @return string
     */
    public function getAvsZip()
    {
        return $this->avsZip;
    }

    /**
     * Set paymentIdExpired
     *
     * @param string $paymentIdExpired
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setPaymentIdExpired($paymentIdExpired)
    {
        $this->paymentIdExpired = $paymentIdExpired;
    
        return $this;
    }

    /**
     * Get paymentIdExpired
     *
     * @return string
     */
    public function getPaymentIdExpired()
    {
        return $this->paymentIdExpired;
    }

    /**
     * Set customerCode
     *
     * @param string $customerCode
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setCustomerCode($customerCode)
    {
        $this->customerCode = $customerCode;
    
        return $this;
    }

    /**
     * Get customerCode
     *
     * @return string
     */
    public function getCustomerCode()
    {
        return $this->customerCode;
    }

    /**
     * Set memo
     *
     * @param string $memo
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
    
        return $this;
    }

    /**
     * Get memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set batchNo
     *
     * @param string $batchNo
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setBatchNo($batchNo)
    {
        $this->batchNo = $batchNo;
    
        return $this;
    }

    /**
     * Get batchNo
     *
     * @return string
     */
    public function getBatchNo()
    {
        return $this->batchNo;
    }

    /**
     * Set gratuityAmount
     *
     * @param float $gratuityAmount
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setGratuityAmount($gratuityAmount)
    {
        $this->gratuityAmount = $gratuityAmount;
    
        return $this;
    }

    /**
     * Get gratuityAmount
     *
     * @return float
     */
    public function getGratuityAmount()
    {
        return $this->gratuityAmount;
    }

    /**
     * Set voided
     *
     * @param boolean $voided
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setVoided($voided)
    {
	    if(is_string($voided)){
			$voided = strtolower($voided);
		    $ac = json_decode($voided,true);
		    if(isset($ac))$voided = $ac;
	    }

	    $this->voided = $voided;
    
        return $this;
    }

    /**
     * Get voided
     *
     * @return boolean
     */
    public function getVoided()
    {
        return $this->voided;
    }

    /**
     * Set initiationTime
     *
     * @param \DateTime $initiationTime
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setInitiationTime($initiationTime)
    {
	    if(is_string($initiationTime) && strtotime($initiationTime))$initiationTime = new \DateTime($initiationTime);
	    $this->initiationTime = $initiationTime;
    
        return $this;
    }

    /**
     * Get initiationTime
     *
     * @return \DateTime
     */
    public function getInitiationTime()
    {
	    // convert default to DateTime
	    if(is_string($this->initiationTime)){
		    $this->setInitiationTime($this->initiationTime);
	    }
        return $this->initiationTime;
    }

    /**
     * Set primaryInvoice
     *
     * @param integer $primaryInvoice
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setPrimaryInvoice($primaryInvoice)
    {
        $this->primaryInvoice = $primaryInvoice;
    
        return $this;
    }

    /**
     * Get primaryInvoice
     *
     * @return integer
     */
    public function getPrimaryInvoice()
    {
        return $this->primaryInvoice;
    }

    /**
     * Set cartId
     *
     * @param integer $cartId
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setCartId($cartId)
    {
        $this->cartId = $cartId;
    
        return $this;
    }

    /**
     * Get cartId
     *
     * @return integer
     */
    public function getCartId()
    {
        return $this->cartId;
    }

    /**
     * Set mobileGuid
     *
     * @param string $mobileGuid
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setMobileGuid($mobileGuid)
    {
        $this->mobileGuid = $mobileGuid;
    
        return $this;
    }

    /**
     * Get mobileGuid
     *
     * @return string
     */
    public function getMobileGuid()
    {
        return $this->mobileGuid;
    }

    /**
     * Set successCheckCount
     *
     * @param boolean $successCheckCount
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setSuccessCheckCount($successCheckCount)
    {
	    if(is_string($successCheckCount)){
			$successCheckCount = strtolower($successCheckCount);
		    $ac = json_decode($successCheckCount,true);
		    if(isset($ac))$successCheckCount = $ac;
	    }

        $this->successCheckCount = $successCheckCount;
    
        return $this;
    }

    /**
     * Get successCheckCount
     *
     * @return boolean
     */
    public function getSuccessCheckCount()
    {
        return $this->successCheckCount;
    }

    /**
     * Set cancelTime
     *
     * @param \DateTime $cancelTime
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setCancelTime($cancelTime)
    {
	    if(is_string($cancelTime) && strtotime($cancelTime))$cancelTime = new \DateTime($cancelTime);
	    $this->cancelTime = $cancelTime;
    
        return $this;
    }

    /**
     * Get cancelTime
     *
     * @return \DateTime
     */
    public function getCancelTime()
    {
	    // convert default to DateTime
	    if(is_string($this->cancelTime)){
		    $this->setCancelTime($this->cancelTime);
	    }
        return $this->cancelTime;
    }

    /**
     * Set signatureData
     *
     * @param string $signatureData
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setSignatureData($signatureData)
    {
        $this->signatureData = $signatureData;
    
        return $this;
    }

    /**
     * Get signatureData
     *
     * @return string
     */
    public function getSignatureData()
    {
        return $this->signatureData;
    }

    /**
     * Set emvPayment
     *
     * @param boolean $emvPayment
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setEmvPayment($emvPayment)
    {
	    if(is_string($emvPayment)){
			$emvPayment = strtolower($emvPayment);
		    $ac = json_decode($emvPayment,true);
		    if(isset($ac))$emvPayment = $ac;
	    }

        $this->emvPayment = $emvPayment;
    
        return $this;
    }

    /**
     * Get emvPayment
     *
     * @return boolean
     */
    public function getEmvPayment()
    {
        return $this->emvPayment;
    }

    /**
     * Set toBeVoided
     *
     * @param boolean $toBeVoided
     *
     * @return ForeupSalesPaymentsCreditCards
     */
    public function setToBeVoided($toBeVoided)
    {
	    if(is_string($toBeVoided)){
			$toBeVoided = strtolower($toBeVoided);
		    $ac = json_decode($toBeVoided,true);
		    if(isset($ac))$toBeVoided = $ac;
	    }

        $this->toBeVoided = $toBeVoided;
    
        return $this;
    }

    /**
     * Get toBeVoided
     *
     * @return boolean
     */
    public function getToBeVoided()
    {
        return $this->toBeVoided;
    }
}
