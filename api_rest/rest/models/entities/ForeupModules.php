<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupModules
 *
 * @ORM\Table(name="foreup_modules", uniqueConstraints={@ORM\UniqueConstraint(name="desc_lang_key", columns={"desc_lang_key"}), @ORM\UniqueConstraint(name="name_lang_key", columns={"name_lang_key"})})
 * @ORM\Entity
 */
class ForeupModules
{
    /**
     * @var string
     *
     * @ORM\Column(name="module_id", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $moduleId;

    /**
     * @var string
     *
     * @ORM\Column(name="name_lang_key", type="string", length=255, nullable=false)
     */
    private $nameLangKey;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_lang_key", type="string", length=255, nullable=false)
     */
    private $descLangKey;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort", type="integer", nullable=false)
     */
    private $sort;

	/**
	 * @return string
	 */
	public function getModuleId(): string
	{
		return $this->moduleId;
	}

	/**
	 * @param string $moduleId
	 */
	public function setModuleId(string $moduleId)
	{
		$this->moduleId = $moduleId;
	}

	/**
	 * @return string
	 */
	public function getNameLangKey(): string
	{
		return $this->nameLangKey;
	}

	/**
	 * @param string $nameLangKey
	 */
	public function setNameLangKey(string $nameLangKey)
	{
		$this->nameLangKey = $nameLangKey;
	}

	/**
	 * @return string
	 */
	public function getDescLangKey(): string
	{
		return $this->descLangKey;
	}

	/**
	 * @param string $descLangKey
	 */
	public function setDescLangKey(string $descLangKey)
	{
		$this->descLangKey = $descLangKey;
	}

	/**
	 * @return int
	 */
	public function getSort(): int
	{
		return $this->sort;
	}

	/**
	 * @param int $sort
	 */
	public function setSort(int $sort)
	{
		$this->sort = $sort;
	}


}

