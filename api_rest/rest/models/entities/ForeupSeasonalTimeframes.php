<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupSeasonalTimeframes
 *
 * @ORM\Table(name="foreup_seasonal_timeframes", indexes={@ORM\Index(name="class_id", columns={"class_id"}), @ORM\Index(name="season_id", columns={"season_id"}), @ORM\Index(name="start_time", columns={"start_time", "end_time"})})
 * @ORM\Entity
 */
class ForeupSeasonalTimeframes
{

    public function __construct(ForeupPriceClass $class, ForeupSeasons $season)
    {
        $this->timeframeName = "Default";
        $this->monday = true;
        $this->tuesday = true;
        $this->wednesday = true;
        $this->thursday = true;
        $this->friday = true;
        $this->saturday = true;
        $this->sunday = true;
        $this->price1 = 0;
        $this->price2 = 0;
        $this->price3 = 0;
        $this->price4 = 0;
        $this->startTime = 0;
        $this->endTime = 2400;
        $this->default = false;
        $this->setClass($class);
        $this->setSeason($season);
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="timeframe_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $timeframeId;

    /**
     * @var string
     *
     * @ORM\Column(name="timeframe_name", type="string", length=255, nullable=false)
     */
    private $timeframeName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="monday", type="boolean", nullable=false)
     */
    private $monday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tuesday", type="boolean", nullable=false)
     */
    private $tuesday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wednesday", type="boolean", nullable=false)
     */
    private $wednesday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="thursday", type="boolean", nullable=false)
     */
    private $thursday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="friday", type="boolean", nullable=false)
     */
    private $friday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="saturday", type="boolean", nullable=false)
     */
    private $saturday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sunday", type="boolean", nullable=false)
     */
    private $sunday;

    /**
     * @var string
     *
     * @ORM\Column(name="price1", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $price1 = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="price2", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $price2 = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="price3", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $price3 = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="price4", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $price4 = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="price5", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $price5 = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="price6", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $price6 = '0.00';

    /**
     * @var integer
     *
     * @ORM\Column(name="start_time", type="smallint", nullable=false)
     */
    private $startTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="end_time", type="smallint", nullable=false)
     */
    private $endTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="`default`", type="boolean", nullable=false)
     */
    private $default = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \foreup\rest\models\entities\ForeupPriceClass
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupPriceClass")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="class_id", referencedColumnName="class_id")
     * })
     */
    private $class;

    /**
     * @var \foreup\rest\models\entities\ForeupSeasons
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupSeasons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="season_id", referencedColumnName="season_id")
     * })
     */
    private $season;



    /**
     * Get timeframeId
     *
     * @return integer
     */
    public function getTimeframeId()
    {
        return $this->timeframeId;
    }

    /**
     * Set timeframeName
     *
     * @param string $timeframeName
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setTimeframeName($timeframeName)
    {
        $this->timeframeName = $timeframeName;
    
        return $this;
    }

    /**
     * Get timeframeName
     *
     * @return string
     */
    public function getTimeframeName()
    {
        return $this->timeframeName;
    }

    /**
     * Set monday
     *
     * @param boolean $monday
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setMonday($monday)
    {
        $this->monday = $monday;
    
        return $this;
    }

    /**
     * Get monday
     *
     * @return boolean
     */
    public function getMonday()
    {
        return $this->monday;
    }

    /**
     * Set tuesday
     *
     * @param boolean $tuesday
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setTuesday($tuesday)
    {
        $this->tuesday = $tuesday;
    
        return $this;
    }

    /**
     * Get tuesday
     *
     * @return boolean
     */
    public function getTuesday()
    {
        return $this->tuesday;
    }

    /**
     * Set wednesday
     *
     * @param boolean $wednesday
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setWednesday($wednesday)
    {
        $this->wednesday = $wednesday;
    
        return $this;
    }

    /**
     * Get wednesday
     *
     * @return boolean
     */
    public function getWednesday()
    {
        return $this->wednesday;
    }

    /**
     * Set thursday
     *
     * @param boolean $thursday
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setThursday($thursday)
    {
        $this->thursday = $thursday;
    
        return $this;
    }

    /**
     * Get thursday
     *
     * @return boolean
     */
    public function getThursday()
    {
        return $this->thursday;
    }

    /**
     * Set friday
     *
     * @param boolean $friday
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setFriday($friday)
    {
        $this->friday = $friday;
    
        return $this;
    }

    /**
     * Get friday
     *
     * @return boolean
     */
    public function getFriday()
    {
        return $this->friday;
    }

    /**
     * Set saturday
     *
     * @param boolean $saturday
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setSaturday($saturday)
    {
        $this->saturday = $saturday;
    
        return $this;
    }

    /**
     * Get saturday
     *
     * @return boolean
     */
    public function getSaturday()
    {
        return $this->saturday;
    }

    /**
     * Set sunday
     *
     * @param boolean $sunday
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setSunday($sunday)
    {
        $this->sunday = $sunday;
    
        return $this;
    }

    /**
     * Get sunday
     *
     * @return boolean
     */
    public function getSunday()
    {
        return $this->sunday;
    }

    /**
     * Set price1
     *
     * @param string $price1
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setPrice1($price1)
    {
        $this->price1 = $price1;
    
        return $this;
    }


    public function set9holePrice($price)
    {
	    return $this->setPrice2($price);
    }
	public function set9cartPrice($price)
	{
		return $this->setPrice4($price);
	}
	public function set18holePrice($price)
	{
		return $this->setPrice1($price);
	}
	public function set18cartPrice($price)
	{
		return $this->setPrice3($price);
	}
	public function get9holePrice()
	{
		return $this->getPrice2();
	}
	public function get9cartPrice()
	{
		return $this->getPrice4();
	}
	public function get18holePrice()
	{
		return $this->getPrice1();
	}
	public function get18cartPrice()
	{
		return $this->getPrice3();
	}
    /**
     * Get price1
     *
     * @return string
     */
    public function getPrice1()
    {
        return $this->price1;
    }

    /**
     * Set price2
     *
     * @param string $price2
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setPrice2($price2)
    {
        $this->price2 = $price2;
    
        return $this;
    }

    /**
     * Get price2
     *
     * @return string
     */
    public function getPrice2()
    {
        return $this->price2;
    }

    /**
     * Set price3
     *
     * @param string $price3
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setPrice3($price3)
    {
        $this->price3 = $price3;
    
        return $this;
    }

    /**
     * Get price3
     *
     * @return string
     */
    public function getPrice3()
    {
        return $this->price3;
    }

    /**
     * Set price4
     *
     * @param string $price4
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setPrice4($price4)
    {
        $this->price4 = $price4;
    
        return $this;
    }

    /**
     * Get price4
     *
     * @return string
     */
    public function getPrice4()
    {
        return $this->price4;
    }

    /**
     * Set price5
     *
     * @param string $price5
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setPrice5($price5)
    {
        $this->price5 = $price5;
    
        return $this;
    }

    /**
     * Get price5
     *
     * @return string
     */
    public function getPrice5()
    {
        return $this->price5;
    }

    /**
     * Set price6
     *
     * @param string $price6
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setPrice6($price6)
    {
        $this->price6 = $price6;
    
        return $this;
    }

    /**
     * Get price6
     *
     * @return string
     */
    public function getPrice6()
    {
        return $this->price6;
    }

    /**
     * Set startTime
     *
     * @param integer $startTime
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    
        return $this;
    }

    /**
     * Get startTime
     *
     * @return integer
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param integer $endTime
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    
        return $this;
    }

    /**
     * Get endTime
     *
     * @return integer
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set default
     *
     * @param boolean $default
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setDefault($default)
    {
        $this->default = $default;
    
        return $this;
    }

    /**
     * Get default
     *
     * @return boolean
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set class
     *
     * @param \foreup\rest\models\entities\ForeupPriceClass $class
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setClass(\foreup\rest\models\entities\ForeupPriceClass $class = null)
    {
        $this->class = $class;
    
        return $this;
    }

    /**
     * Get class
     *
     * @return \foreup\rest\models\entities\ForeupPriceClass
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set season
     *
     * @param \foreup\rest\models\entities\ForeupSeasons $season
     *
     * @return ForeupSeasonalTimeframes
     */
    public function setSeason(\foreup\rest\models\entities\ForeupSeasons $season = null)
    {
        $this->season = $season;
    
        return $this;
    }

    /**
     * Get season
     *
     * @return \foreup\rest\models\entities\ForeupSeasons
     */
    public function getSeason()
    {
        return $this->season;
    }

	public function isValid()
	{
        if(empty($this->class)){
            return false;
        }
		return true;
	}
}
