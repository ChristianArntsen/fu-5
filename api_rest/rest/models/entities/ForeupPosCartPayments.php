<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupPosCartPayments
 *
 * @ORM\Table(name="foreup_pos_cart_payments", uniqueConstraints={@ORM\UniqueConstraint(name="payment_type", columns={"type", "record_id", "cart_id", "number"})})
 * @ORM\Entity
 */
class ForeupPosCartPayments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="payment_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $paymentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="cart_id", type="integer", nullable=false)
     */
    private $cartId;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=15, scale=2, nullable=false)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=24, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=32, nullable=true)
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="record_id", type="integer", nullable=true)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="string", length=2048, nullable=true)
     */
    private $params;

	/**
	 * @return int
	 */
	public function getPaymentId()
	{
		return $this->paymentId;
	}

	/**
	 * @param int $paymentId
	 */
	public function setPaymentId($paymentId)
	{
		$this->paymentId = $paymentId;
	}

	/**
	 * @return int
	 */
	public function getCartId()
	{
		return $this->cartId;
	}

	/**
	 * @param int $cartId
	 */
	public function setCartId($cartId)
	{
		$this->cartId = $cartId;
	}

	/**
	 * @return string
	 */
	public function getAmount()
	{
		return $this->amount;
	}

	/**
	 * @param string $amount
	 */
	public function setAmount($amount)
	{
		$this->amount = $amount;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 * @param string $number
	 */
	public function setNumber($number)
	{
		$this->number = $number;
	}

	/**
	 * @return int
	 */
	public function getRecordId()
	{
		return $this->recordId;
	}

	/**
	 * @param int $recordId
	 */
	public function setRecordId($recordId)
	{
		$this->recordId = $recordId;
	}

	/**
	 * @return string
	 */
	public function getParams()
	{
		return $this->params;
	}

	/**
	 * @param string $params
	 */
	public function setParams($params)
	{
		$this->params = $params;
	}


}

