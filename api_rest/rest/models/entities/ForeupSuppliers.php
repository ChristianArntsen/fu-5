<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupSuppliers
 *
 * @ORM\Table(name="foreup_suppliers", uniqueConstraints={@ORM\UniqueConstraint(name="account_number", columns={"course_id", "account_number"})}, indexes={@ORM\Index(name="person_id", columns={"person_id"}), @ORM\Index(name="deleted", columns={"deleted"})})
 * @ORM\Entity
 */
class ForeupSuppliers
{
	/**
	 * @var \foreup\rest\models\entities\ForeupCourses
	 *
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $course;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="fax_number", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $faxNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $accountNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $deleted;

    /**
     * @var \foreup\rest\models\entities\ForeupPeople
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupPeople")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id", nullable=true)
     * })
     */
    private $person;

	/**
	 * @return ForeupCourses
	 */
	public function getCourse()
	{
		return $this->course;
	}

	/**
	 * @param ForeupCourses $course
	 */
	public function setCourse(ForeupCourses $course)
	{
		$this->course = $course;
	}




    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return ForeupSuppliers
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set faxNumber
     *
     * @param string $faxNumber
     *
     * @return ForeupSuppliers
     */
    public function setFaxNumber($faxNumber)
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    /**
     * Get faxNumber
     *
     * @return string
     */
    public function getFaxNumber()
    {
        return $this->faxNumber;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return ForeupSuppliers
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     *
     * @return ForeupSuppliers
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set person
     *
     * @param \foreup\rest\models\entities\ForeupPeople $person
     *
     * @return ForeupSuppliers
     */
    public function setPerson(\foreup\rest\models\entities\ForeupPeople $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \foreup\rest\models\entities\ForeupPeople
     */
    public function getPerson()
    {
        return $this->person;
    }
}

