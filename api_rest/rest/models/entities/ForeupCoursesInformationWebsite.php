<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupCoursesInformationWebsite
 *
 * @ORM\Table(name="foreup_courses_information_website", indexes={@ORM\Index(name="sales_rep", columns={"website_rep"}), @ORM\Index(name="FK_foreup_steps", columns={"stage"})})
 * @ORM\Entity
 */
class ForeupCoursesInformationWebsite
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_sold", type="date", nullable=false)
     */
    private $dateSold;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_golive_goal", type="date", nullable=false)
     */
    private $dateGoliveGoal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_started", type="date", nullable=false)
     */
    private $dateStarted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_sent_for_review", type="date", nullable=false)
     */
    private $dateSentForReview;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_live", type="date", nullable=false)
     */
    private $dateLive;

    /**
     * @var \ForeupCoursesInformationWebsiteSteps
     *
     * @ORM\ManyToOne(targetEntity="ForeupCoursesInformationWebsiteSteps")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stage", referencedColumnName="step_number")
     * })
     */
    private $stage;

    /**
     * @var \ForeupCourses
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $course;

    /**
     * @var \ForeupEmployees
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="website_rep", referencedColumnName="person_id")
     * })
     */
    private $websiteRep;


    /**
     * Set dateSold
     *
     * @param \DateTime $dateSold
     *
     * @return ForeupCoursesInformationWebsite
     */
    public function setDateSold($dateSold)
    {
        $this->dateSold = $dateSold;

        return $this;
    }

    /**
     * Get dateSold
     *
     * @return \DateTime
     */
    public function getDateSold()
    {
        return $this->dateSold;
    }

    /**
     * Set dateGoliveGoal
     *
     * @param \DateTime $dateGoliveGoal
     *
     * @return ForeupCoursesInformationWebsite
     */
    public function setDateGoliveGoal($dateGoliveGoal)
    {
        $this->dateGoliveGoal = $dateGoliveGoal;

        return $this;
    }

    /**
     * Get dateGoliveGoal
     *
     * @return \DateTime
     */
    public function getDateGoliveGoal()
    {
        return $this->dateGoliveGoal;
    }

    /**
     * Set dateStarted
     *
     * @param \DateTime $dateStarted
     *
     * @return ForeupCoursesInformationWebsite
     */
    public function setDateStarted($dateStarted)
    {
        $this->dateStarted = $dateStarted;

        return $this;
    }

    /**
     * Get dateStarted
     *
     * @return \DateTime
     */
    public function getDateStarted()
    {
        return $this->dateStarted;
    }

    /**
     * Set dateSentForReview
     *
     * @param \DateTime $dateSentForReview
     *
     * @return ForeupCoursesInformationWebsite
     */
    public function setDateSentForReview($dateSentForReview)
    {
        $this->dateSentForReview = $dateSentForReview;

        return $this;
    }

    /**
     * Get dateSentForReview
     *
     * @return \DateTime
     */
    public function getDateSentForReview()
    {
        return $this->dateSentForReview;
    }

    /**
     * Set dateLive
     *
     * @param \DateTime $dateLive
     *
     * @return ForeupCoursesInformationWebsite
     */
    public function setDateLive($dateLive)
    {
        $this->dateLive = $dateLive;

        return $this;
    }

    /**
     * Get dateLive
     *
     * @return \DateTime
     */
    public function getDateLive()
    {
        return $this->dateLive;
    }

    /**
     * Set stage
     *
     * @param \ForeupCoursesInformationWebsiteSteps $stage
     *
     * @return ForeupCoursesInformationWebsite
     */
    public function setStage(\ForeupCoursesInformationWebsiteSteps $stage = null)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return \ForeupCoursesInformationWebsiteSteps
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * Set course
     *
     * @param \ForeupCourses $course
     *
     * @return ForeupCoursesInformationWebsite
     */
    public function setCourse(\foreup\rest\models\entities\ForeupCourses $course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \ForeupCourses
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set websiteRep
     *
     * @param \ForeupEmployees $websiteRep
     *
     * @return ForeupCoursesInformationWebsite
     */
    public function setWebsiteRep(\foreup\rest\models\entities\ForeupEmployees $websiteRep = null)
    {
        $this->websiteRep = $websiteRep;

        return $this;
    }

    /**
     * Get websiteRep
     *
     * @return \ForeupEmployees
     */
    public function getWebsiteRep()
    {
        return $this->websiteRep;
    }
}

