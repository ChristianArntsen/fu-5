<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupLoyaltyPackages
 *
 * @ORM\Table(name="foreup_loyalty_packages", indexes={@ORM\Index(name="course_id_fk", columns={"course_id"}), @ORM\Index(name="deleted_by_fk", columns={"deleted_by"}), @ORM\Index(name="created_by_id_fk", columns={"created_by"}), @ORM\Index(name="last_updated_by_id_fk", columns={"last_updated_by"})})
 * @ORM\Entity
 */
class ForeupLoyaltyPackages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $isDefault;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disabled", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $disabled;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_deleted", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateDeleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_updated", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $lastUpdated;

    /**
     * @var \foreup\rest\models\entities\ForeupCourses
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id", nullable=true)
     * })
     */
    private $course;

    /**
     * @var \foreup\rest\models\entities\ForeupEmployees
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deleted_by", referencedColumnName="person_id", nullable=true)
     * })
     */
    private $deletedBy;

    /**
     * @var \foreup\rest\models\entities\ForeupEmployees
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="person_id", nullable=true)
     * })
     */
    private $createdBy;

    /**
     * @var \foreup\rest\models\entities\ForeupEmployees
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="last_updated_by", referencedColumnName="person_id", nullable=true)
     * })
     */
    private $lastUpdatedBy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ForeupLoyaltyPackages
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     *
     * @return ForeupLoyaltyPackages
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     *
     * @return ForeupLoyaltyPackages
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set dateDeleted
     *
     * @param \DateTime $dateDeleted
     *
     * @return ForeupLoyaltyPackages
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->dateDeleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->dateDeleted;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ForeupLoyaltyPackages
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set lastUpdated
     *
     * @param \DateTime $lastUpdated
     *
     * @return ForeupLoyaltyPackages
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * Get lastUpdated
     *
     * @return \DateTime
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * Set course
     *
     * @param \foreup\rest\models\entities\ForeupCourses $course
     *
     * @return ForeupLoyaltyPackages
     */
    public function setCourse(\foreup\rest\models\entities\ForeupCourses $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \foreup\rest\models\entities\ForeupCourses
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set deletedBy
     *
     * @param \foreup\rest\models\entities\ForeupEmployees $deletedBy
     *
     * @return ForeupLoyaltyPackages
     */
    public function setDeletedBy(\foreup\rest\models\entities\ForeupEmployees $deletedBy = null)
    {
        $this->deletedBy = $deletedBy;

        return $this;
    }

    /**
     * Get deletedBy
     *
     * @return \foreup\rest\models\entities\ForeupEmployees
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

    /**
     * Set createdBy
     *
     * @param \foreup\rest\models\entities\ForeupEmployees $createdBy
     *
     * @return ForeupLoyaltyPackages
     */
    public function setCreatedBy(\foreup\rest\models\entities\ForeupEmployees $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \foreup\rest\models\entities\ForeupEmployees
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set lastUpdatedBy
     *
     * @param \foreup\rest\models\entities\ForeupEmployees $lastUpdatedBy
     *
     * @return ForeupLoyaltyPackages
     */
    public function setLastUpdatedBy(\foreup\rest\models\entities\ForeupEmployees $lastUpdatedBy = null)
    {
        $this->lastUpdatedBy = $lastUpdatedBy;

        return $this;
    }

    /**
     * Get lastUpdatedBy
     *
     * @return \foreup\rest\models\entities\ForeupEmployees
     */
    public function getLastUpdatedBy()
    {
        return $this->lastUpdatedBy;
    }
}

