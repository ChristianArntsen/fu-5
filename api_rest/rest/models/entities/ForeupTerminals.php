<?php

namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupTerminals
 *
 * @ORM\Table(name="foreup_terminals")
 * @ORM\Entity
 */
class ForeupTerminals
{
    /**
     * @var integer
     *
     * @ORM\Column(name="terminal_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var integer
     *
     * @ORM\Column(name="quickbutton_tab", type="integer", nullable=false)
     */
    private $quickbuttonTab = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="course_id", type="integer", nullable=false)
     */
    private $courseId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_print_receipts", type="boolean", nullable=true)
     */
    private $autoPrintReceipts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="webprnt", type="boolean", nullable=true)
     */
    private $webprnt;

    /**
     * @var string
     *
     * @ORM\Column(name="receipt_ip", type="string", length=20, nullable=false)
     */
    private $receiptIp = "";

    /**
     * @var boolean
     *
     * @ORM\Column(name="use_register_log", type="boolean", nullable=true)
     */
    private $useRegisterLog;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cash_register", type="boolean", nullable=true)
     */
    private $cashRegister;

    /**
     * @var boolean
     *
     * @ORM\Column(name="print_tip_line", type="boolean", nullable=true)
     */
    private $printTipLine;

    /**
     * @var integer
     *
     * @ORM\Column(name="signature_slip_count", type="integer", nullable=true)
     */
    private $signatureSlipCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="credit_card_receipt_count", type="integer", nullable=true)
     */
    private $creditCardReceiptCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="non_credit_card_receipt_count", type="integer", nullable=true)
     */
    private $nonCreditCardReceiptCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="ibeacon_minor_id", type="integer", nullable=false)
     */
    private $ibeaconMinorId = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="persistent_logs", type="boolean", nullable=false)
     */
    private $persistentLogs;

    /**
     * @var boolean
     *
     * @ORM\Column(name="after_sale_load", type="boolean", nullable=true)
     */
    private $afterSaleLoad;

    /**
     * @var integer
     *
     * @ORM\Column(name="default_kitchen_printer", type="integer", nullable=false)
     */
    private $defaultKitchenPrinter = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="return_policy", type="string", length=4096, nullable=true)
     */
    private $returnPolicy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_email_receipt", type="boolean", nullable=true)
     */
    private $autoEmailReceipt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="auto_email_no_print", type="boolean", nullable=true)
     */
    private $autoEmailNoPrint;

    /**
     * @var boolean
     *
     * @ORM\Column(name="require_signature_member_payments", type="boolean", nullable=true)
     */
    private $requireSignatureMemberPayments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="multi_cash_drawers", type="boolean", nullable=true)
     */
    private $multiCashDrawers;

    /**
     * @var integer
     *
     * @ORM\Column(name="cash_drawer_number", type="integer", nullable=false)
     */
    private $cashDrawerNumber = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="https", type="boolean", nullable=true)
     */
    private $https = '0';

	/**
	 * @return int
	 */
	public function getQuickbuttonTab()
	{
		return $this->quickbuttonTab;
	}

	/**
	 * @param int $quickbuttonTab
	 */
	public function setQuickbuttonTab(int $quickbuttonTab)
	{
		$this->quickbuttonTab = $quickbuttonTab;
	}



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return ForeupTerminals
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return ForeupTerminals
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }


    /**
     * Set courseId
     *
     * @param integer $courseId
     *
     * @return ForeupTerminals
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;

        return $this;
    }

    /**
     * Get courseId
     *
     * @return integer
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

	/**
	 * @return bool
	 */
	public function isAutoPrintReceipts()
	{
		return $this->autoPrintReceipts;
	}

	/**
	 * @param bool $autoPrintReceipts
	 */
	public function setAutoPrintReceipts(bool $autoPrintReceipts)
	{
		$this->autoPrintReceipts = $autoPrintReceipts;
	}

	/**
	 * @return bool
	 */
	public function isWebprnt()
	{
		return $this->webprnt;
	}

	/**
	 * @param bool $webprnt
	 */
	public function setWebprnt(bool $webprnt)
	{
		$this->webprnt = $webprnt;
	}


    /**
     * Set receiptIp
     *
     * @param string $receiptIp
     *
     * @return ForeupTerminals
     */
    public function setReceiptIp($receiptIp)
    {
        $this->receiptIp = $receiptIp;

        return $this;
    }

    /**
     * Get receiptIp
     *
     * @return string
     */
    public function getReceiptIp()
    {
        return $this->receiptIp;
    }

	/**
	 * @return bool
	 */
	public function isUseRegisterLog()
	{
		return $this->useRegisterLog;
	}

	/**
	 * @param bool $useRegisterLog
	 */
	public function setUseRegisterLog(bool $useRegisterLog)
	{
		$this->useRegisterLog = $useRegisterLog;
	}

	/**
	 * @return bool
	 */
	public function isCashRegister()
	{
		return $this->cashRegister;
	}

	/**
	 * @param bool $cashRegister
	 */
	public function setCashRegister(bool $cashRegister)
	{
		$this->cashRegister = $cashRegister;
	}

	/**
	 * @return bool
	 */
	public function isPrintTipLine()
	{
		return $this->printTipLine;
	}

	/**
	 * @param bool $printTipLine
	 */
	public function setPrintTipLine(bool $printTipLine)
	{
		$this->printTipLine = $printTipLine;
	}

	/**
	 * @return int
	 */
	public function getSignatureSlipCount()
	{
		return $this->signatureSlipCount;
	}

	/**
	 * @param int $signatureSlipCount
	 */
	public function setSignatureSlipCount(int $signatureSlipCount)
	{
		$this->signatureSlipCount = $signatureSlipCount;
	}

	/**
	 * @return int
	 */
	public function getCreditCardReceiptCount()
	{
		return $this->creditCardReceiptCount;
	}

	/**
	 * @param int $creditCardReceiptCount
	 */
	public function setCreditCardReceiptCount(int $creditCardReceiptCount)
	{
		$this->creditCardReceiptCount = $creditCardReceiptCount;
	}



	/**
	 * @return int
	 */
	public function getNonCreditCardReceiptCount()
	{
		return $this->nonCreditCardReceiptCount;
	}

	/**
	 * @param int $nonCreditCardReceiptCount
	 */
	public function setNonCreditCardReceiptCount(int $nonCreditCardReceiptCount)
	{
		$this->nonCreditCardReceiptCount = $nonCreditCardReceiptCount;
	}

	/**
	 * @return int
	 */
	public function getIbeaconMinorId()
	{
		return $this->ibeaconMinorId;
	}

	/**
	 * @param int $ibeaconMinorId
	 */
	public function setIbeaconMinorId(int $ibeaconMinorId)
	{
		$this->ibeaconMinorId = $ibeaconMinorId;
	}

	/**
	 * @return bool
	 */
	public function isPersistentLogs()
	{
		return $this->persistentLogs;
	}

	/**
	 * @param bool $persistentLogs
	 */
	public function setPersistentLogs(bool $persistentLogs)
	{
		$this->persistentLogs = $persistentLogs;
	}

	/**
	 * @return bool
	 */
	public function isAfterSaleLoad()
	{
		return $this->afterSaleLoad;
	}

	/**
	 * @param bool $afterSaleLoad
	 */
	public function setAfterSaleLoad(bool $afterSaleLoad)
	{
		$this->afterSaleLoad = $afterSaleLoad;
	}

	/**
	 * @return int
	 */
	public function getDefaultKitchenPrinter()
	{
		return $this->defaultKitchenPrinter;
	}

	/**
	 * @param int $defaultKitchenPrinter
	 */
	public function setDefaultKitchenPrinter(int $defaultKitchenPrinter)
	{
		$this->defaultKitchenPrinter = $defaultKitchenPrinter;
	}

	/**
	 * @return string
	 */
	public function getReturnPolicy(): string
	{
		return $this->returnPolicy;
	}

	/**
	 * @param string $returnPolicy
	 */
	public function setReturnPolicy(string $returnPolicy)
	{
		$this->returnPolicy = $returnPolicy;
	}

	/**
	 * @return bool
	 */
	public function isAutoEmailReceipt()
	{
		return $this->autoEmailReceipt;
	}

	/**
	 * @param bool $autoEmailReceipt
	 */
	public function setAutoEmailReceipt(bool $autoEmailReceipt)
	{
		$this->autoEmailReceipt = $autoEmailReceipt;
	}

	/**
	 * @return bool
	 */
	public function isAutoEmailNoPrint()
	{
		return $this->autoEmailNoPrint;
	}

	/**
	 * @param bool $autoEmailNoPrint
	 */
	public function setAutoEmailNoPrint(bool $autoEmailNoPrint)
	{
		$this->autoEmailNoPrint = $autoEmailNoPrint;
	}

	/**
	 * @return bool
	 */
	public function isRequireSignatureMemberPayments()
	{
		return $this->requireSignatureMemberPayments;
	}

	/**
	 * @param bool $requireSignatureMemberPayments
	 */
	public function setRequireSignatureMemberPayments(bool $requireSignatureMemberPayments)
	{
		$this->requireSignatureMemberPayments = $requireSignatureMemberPayments;
	}

	/**
	 * @return bool
	 */
	public function isMultiCashDrawers()
	{
		return $this->multiCashDrawers;
	}

	/**
	 * @param bool $multiCashDrawers
	 */
	public function setMultiCashDrawers(bool $multiCashDrawers)
	{
		$this->multiCashDrawers = $multiCashDrawers;
	}

	/**
	 * @return int
	 */
	public function getCashDrawerNumber()
	{
		return $this->cashDrawerNumber;
	}

	/**
	 * @param int $cashDrawerNumber
	 */
	public function setCashDrawerNumber(int $cashDrawerNumber)
	{
		$this->cashDrawerNumber = $cashDrawerNumber;
	}


	/**
	 * @return bool
	 */
	public function isHttps()
	{
		return $this->https;
	}

	/**
	 * @param bool $https
	 */
	public function setHttps(bool $https)
	{
		$this->https = $https;
	}


}
