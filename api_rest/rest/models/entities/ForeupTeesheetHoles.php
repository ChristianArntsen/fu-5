<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupTeesheetHoles
 *
 * @ORM\Table(name="foreup_teesheet_holes", indexes={@ORM\Index(name="tee_sheet_id", columns={"tee_sheet_id"})})
 * @ORM\Entity
 */
class ForeupTeesheetHoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tee_sheet_hole_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $teeSheetHoleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tee_sheet_id", type="integer", nullable=false)
     */
    private $teeSheetId;

    /**
     * @var integer
     *
     * @ORM\Column(name="hole_number", type="integer", nullable=false)
     */
    private $holeNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="par", type="integer", nullable=false)
     */
    private $par;

    /**
     * @var integer
     *
     * @ORM\Column(name="handicap", type="integer", nullable=false)
     */
    private $handicap;


    /**
     * @var string
     *
     * @ORM\Column(name="pro_tip", type="string", length=255, nullable=false)
     */
    private $proTip;

    /**
     * @var string
     *
     * @ORM\Column(name="tee_box_long", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $teeBoxLong;

    /**
     * @var string
     *
     * @ORM\Column(name="tee_box_lat", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $teeBoxLat;

    /**
     * @var string
     *
     * @ORM\Column(name="mid_point_long", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $midPointLong;

    /**
     * @var string
     *
     * @ORM\Column(name="mid_point_lat", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $midPointLat;

    /**
     * @var string
     *
     * @ORM\Column(name="hole_long", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $holeLong;

    /**
     * @var string
     *
     * @ORM\Column(name="hole_lat", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $holeLat;

    /**
     * @var string
     *
     * @ORM\Column(name="green_front_long", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $greenFrontLong;

    /**
     * @var string
     *
     * @ORM\Column(name="green_front_lat", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $greenFrontLat;

    /**
     * @var string
     *
     * @ORM\Column(name="green_back_long", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $greenBackLong;

    /**
     * @var string
     *
     * @ORM\Column(name="green_back_lat", type="decimal", precision=10, scale=7, nullable=true)
     */
    private $greenBackLat;


    /**
     * @ORM\ManyToOne(targetEntity="ForeupTeesheet", inversedBy="teeSheetHoles")
     * @ORM\JoinColumn(name="tee_sheet_id", referencedColumnName="teesheet_id")
     */
    private $teeSheet;


    /**
     * Get teeSheetHoleId
     *
     * @return integer
     */
    public function getTeeSheetHoleId()
    {
        return $this->teeSheetHoleId;
    }

    /**
     * Set teeSheetId
     *
     * @param integer $teeSheetId
     *
     * @return ForeupTeesheetHoles
     */
    public function setTeeSheetId($teeSheetId)
    {
        $this->teeSheetId = $teeSheetId;

        return $this;
    }

    /**
     * Get teeSheetId
     *
     * @return integer
     */
    public function getTeeSheetId()
    {
        return $this->teeSheetId;
    }

    /**
     * Set holeNumber
     *
     * @param boolean $holeNumber
     *
     * @return ForeupTeesheetHoles
     */
    public function setHoleNumber($holeNumber)
    {
        $this->holeNumber = $holeNumber;

        return $this;
    }

    /**
     * Get holeNumber
     *
     * @return boolean
     */
    public function getHoleNumber()
    {
        return $this->holeNumber;
    }

    /**
     * Set par
     *
     * @param boolean $par
     *
     * @return ForeupTeesheetHoles
     */
    public function setPar($par)
    {
        $this->par = $par;

        return $this;
    }

    /**
     * Set handicap
     *
     * @param boolean $handicap
     *
     * @return ForeupTeesheetHoles
     */
    public function setHandicap($handicap)
    {
        $this->handicap = $handicap;

        return $this;
    }

    /**
     * Get par
     *
     * @return boolean
     */
    public function getPar()
    {
        return $this->par;
    }


    /**
     * Get par
     *
     * @return integer
     */
    public function getHandicap()
    {
        return $this->handicap;
    }

    /**
     * Set proTip
     *
     * @param string $proTip
     *
     * @return ForeupTeesheetHoles
     */
    public function setProTip($proTip)
    {
        $this->proTip = $proTip;

        return $this;
    }

    /**
     * Get proTip
     *
     * @return string
     */
    public function getProTip()
    {
        return $this->proTip;
    }

    /**
     * Set teeBoxLong
     *
     * @param string $teeBoxLong
     *
     * @return ForeupTeesheetHoles
     */
    public function setTeeBoxLong($teeBoxLong)
    {
        $this->teeBoxLong = $teeBoxLong;

        return $this;
    }

    /**
     * Get teeBoxLong
     *
     * @return string
     */
    public function getTeeBoxLong()
    {
        return $this->teeBoxLong;
    }

    /**
     * Set teeBoxLat
     *
     * @param string $teeBoxLat
     *
     * @return ForeupTeesheetHoles
     */
    public function setTeeBoxLat($teeBoxLat)
    {
        $this->teeBoxLat = $teeBoxLat;

        return $this;
    }

    /**
     * Get teeBoxLat
     *
     * @return string
     */
    public function getTeeBoxLat()
    {
        return $this->teeBoxLat;
    }

    /**
     * Set midPointLong
     *
     * @param string $midPointLong
     *
     * @return ForeupTeesheetHoles
     */
    public function setMidPointLong($midPointLong)
    {
        $this->midPointLong = $midPointLong;

        return $this;
    }

    /**
     * Get midPointLong
     *
     * @return string
     */
    public function getMidPointLong()
    {
        return $this->midPointLong;
    }

    /**
     * Set midPointLat
     *
     * @param string $midPointLat
     *
     * @return ForeupTeesheetHoles
     */
    public function setMidPointLat($midPointLat)
    {
        $this->midPointLat = $midPointLat;

        return $this;
    }

    /**
     * Get midPointLat
     *
     * @return string
     */
    public function getMidPointLat()
    {
        return $this->midPointLat;
    }

    /**
     * Set holeLong
     *
     * @param string $holeLong
     *
     * @return ForeupTeesheetHoles
     */
    public function setHoleLong($holeLong)
    {
        $this->holeLong = $holeLong;

        return $this;
    }

    /**
     * Get holeLong
     *
     * @return string
     */
    public function getHoleLong()
    {
        return $this->holeLong;
    }

    /**
     * Set holeLat
     *
     * @param string $holeLat
     *
     * @return ForeupTeesheetHoles
     */
    public function setHoleLat($holeLat)
    {
        $this->holeLat = $holeLat;

        return $this;
    }

    /**
     * Get holeLat
     *
     * @return string
     */
    public function getHoleLat()
    {
        return $this->holeLat;
    }

    /**
     * Set greenFrontLong
     *
     * @param string $greenFrontLong
     *
     * @return ForeupTeesheetHoles
     */
    public function setGreenFrontLong($greenFrontLong)
    {
        $this->greenFrontLong = $greenFrontLong;

        return $this;
    }

    /**
     * Get greenFrontLong
     *
     * @return string
     */
    public function getGreenFrontLong()
    {
        return $this->greenFrontLong;
    }

    /**
     * Set greenFrontLat
     *
     * @param string $greenFrontLat
     *
     * @return ForeupTeesheetHoles
     */
    public function setGreenFrontLat($greenFrontLat)
    {
        $this->greenFrontLat = $greenFrontLat;

        return $this;
    }

    /**
     * Get greenFrontLat
     *
     * @return string
     */
    public function getGreenFrontLat()
    {
        return $this->greenFrontLat;
    }

    /**
     * Set greenBackLong
     *
     * @param string $greenBackLong
     *
     * @return ForeupTeesheetHoles
     */
    public function setGreenBackLong($greenBackLong)
    {
        $this->greenBackLong = $greenBackLong;

        return $this;
    }

    /**
     * Get greenBackLong
     *
     * @return string
     */
    public function getGreenBackLong()
    {
        return $this->greenBackLong;
    }

    /**
     * Set greenBackLat
     *
     * @param string $greenBackLat
     *
     * @return ForeupTeesheetHoles
     */
    public function setGreenBackLat($greenBackLat)
    {
        $this->greenBackLat = $greenBackLat;

        return $this;
    }

    /**
     * Get greenBackLat
     *
     * @return string
     */
    public function getGreenBackLat()
    {
        return $this->greenBackLat;
    }
}
