<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupMobileTokens
 *
 * @ORM\Table(name="foreup_mobile_tokens", indexes={@ORM\Index(name="course_id", columns={"course_id"}), @ORM\Index(name="token", columns={"token"})})
 * @ORM\Entity
 */
class ForeupMobileTokens
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="token", type="text", nullable=true)
     */
    private $token;

    /**
     * @var integer
     *
     * @ORM\Column(name="platform", type="text", nullable=true)
     */
    private $platform;

    /**
     * @var \foreup\rest\models\entities\ForeupCourses
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     * })
     */
    private $course;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param integer $token
     *
     * @return ForeupMobileTokens
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return integer
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return int
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param int $platform
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * Set course
     *
     * @param \foreup\rest\models\entities\ForeupCourses $course
     *
     * @return ForeupMobileTokens
     */
    public function setCourse(\foreup\rest\models\entities\ForeupCourses $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \foreup\rest\models\entities\ForeupCourses
     */
    public function getCourse()
    {
        return $this->course;
    }
}
