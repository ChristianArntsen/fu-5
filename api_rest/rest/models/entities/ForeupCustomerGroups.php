<?php
namespace foreup\rest\models\entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupCustomerGroups
 *
 * @ORM\Table(name="foreup_customer_groups")
 * @ORM\Entity
 */
class ForeupCustomerGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $groupId;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="item_cost_plus_percent_discount", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $itemCostPlusPercentDiscount;

	/**
	 * @var \foreup\rest\models\entities\ForeupCourses
	 *
	 * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupCourses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
	 * })
	 */
	private $course;

	/**
	 * @return ForeupCourses
	 */
	public function getCourse()
	{
		return $this->course;
	}

	/**
	 * @param ForeupCourses $course
	 */
	public function setCourse(ForeupCourses $course)
	{
		$this->course = $course;
	}



    /**
     * Get groupId
     *
     * @return integer
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return ForeupCustomerGroups
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set itemCostPlusPercentDiscount
     *
     * @param string $itemCostPlusPercentDiscount
     *
     * @return ForeupCustomerGroups
     */
    public function setItemCostPlusPercentDiscount($itemCostPlusPercentDiscount)
    {
        $this->itemCostPlusPercentDiscount = $itemCostPlusPercentDiscount;

        return $this;
    }

    /**
     * Get itemCostPlusPercentDiscount
     *
     * @return string
     */
    public function getItemCostPlusPercentDiscount()
    {
        return $this->itemCostPlusPercentDiscount;
    }
}
