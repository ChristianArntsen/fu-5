<?php

namespace foreup\rest\models\entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForeupEmployeeAuditLog
 *
 * @ORM\Table(name="foreup_employee_audit_log", uniqueConstraints={@ORM\UniqueConstraint(name="UID", columns={"editor_id", "gmt_logged", "action_type_id"})}, indexes={@ORM\Index(name="foreup_employee_audit_log_ibfk_2", columns={"action_type_id"}), @ORM\Index(name="IDX_B3083E0D6995AC4C", columns={"editor_id"})})
 * @ORM\Entity (repositoryClass="foreup\rest\models\repositories\EmployeeAuditLogRepository")
 */
class ForeupEmployeeAuditLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="gmt_logged", type="datetime", nullable=true)
     */
    private $gmtLogged = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=256, nullable=true)
     */
    private $comments;

    /**
     * @var \foreup\rest\models\entities\ForeupEmployees
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupEmployees")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="editor_id", referencedColumnName="id")
     * })
     */
    private $editor;

    /**
     * @var \foreup\rest\models\entities\ForeupActionType
     *
     * @ORM\ManyToOne(targetEntity="foreup\rest\models\entities\ForeupActionType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="action_type_id", referencedColumnName="id")
     * })
     */
    private $actionType;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return ForeupPersonAltered
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getGmtLogged()
    {
        return $this->gmtLogged;
    }

    /**
     * @param DateTime $gmtLogged
     *
     * @return ForeupPersonAltered
     */
    public function setGmtLogged($gmtLogged)
    {
        $this->gmtLogged = $gmtLogged;
        return $this;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     *
     * @return ForeupPersonAltered
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @return \foreup\rest\models\entities\ForeupEmployees
     */
    public function getEditor()
    {
        return $this->editor;
    }

    /**
     * @param \foreup\rest\models\entities\ForeupEmployees $editor
     *
     * @return ForeupPersonAltered
     */
    public function setEditor($editor)
    {
        $this->editor = $editor;
        return $this;
    }

    /**
     * @return \foreup\rest\models\entities\ForeupActionType
     */
    public function getActionType()
    {
        return $this->actionType;
    }

    /**
     * @param \foreup\rest\models\entities\ForeupActionType $actionType
     *
     * @return ForeupPersonAltered
     */
    public function setActionType($actionType)
    {
        $this->actionType = $actionType;
        return $this;
    }


}

