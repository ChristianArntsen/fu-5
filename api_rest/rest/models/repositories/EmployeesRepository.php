<?php

namespace foreup\rest\models\repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

class EmployeesRepository extends EntityRepository
{

    public function get_all()
    {
        $em =  $this->getEntityManager();

        $q = $em->createQuery('SELECT e.id, e.courseId, IDENTITY(e.person) as personId, e.teesheetId, e.cid, e.tsid, e.userLevel, e.position, e.username, e.imageId, e.deleted, e.activated, e.lastLogin from foreup\rest\models\entities\ForeupEmployees e');
        $res = $q->getResult(); // gets an array...
        return $res;
    }

    public function get_suggestions($search, $limit = 25)
    {
        $em =  $this->getEntityManager();
        $limit *= 1;
        $q = $em->createQuery('SELECT e.id, e.courseId, IDENTITY(e.person) as personId, e.teesheetId, e.cid, e.tsid, e.userLevel, e.position, e.username, e.imageId, e.deleted, e.activated, e.lastLogin from foreup\rest\models\entities\ForeupEmployees e where e.id = :eq or e.username like :lk');
        $q->setMaxResults($limit);
        $q->setParameter('lk',"%$search%");
        $q->setParameter('eq',$search);
        $res = $q->getResult(); // gets an array...
        return $res;
    }

    public function getSuggestions(Request $request)
    {
        $uzr = $this->auth_user;
        if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin ".$uzr['level'],400);
        $search = $request->query->get('search');
        $limit = $request->query->get('limit');
        $limit = $limit ? $limit*1 : 25;
        $search = $search?$search:'';
        $employees = $this->repository->get_suggestions($search, $limit);

        $content = $this->serializeResource($employees);
        $response = new JsonResponse();
        $response->setContent(json_encode($content));
        return $response;
    }

    /**
     * @param $email
     * @param $password
     * @return \foreup\rest\models\entities\ForeupUsers
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get_employee_from_login($username, $password)
    {
        $qb = $this->createQueryBuilder('u');
        $dql = $qb
            ->select('u')
            ->andWhere('u.username = :username')
            ->andWhere('u.password = :password')
            ->getDQL();
        $em =  $this->getEntityManager();
        $result = $em->createQuery($dql)
            ->setParameter("username", $username)
            ->setParameter("password", md5($password))
            ->getOneOrNullResult();
        return $result;
    }
}
?>