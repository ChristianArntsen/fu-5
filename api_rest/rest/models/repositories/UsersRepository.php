<?php

namespace foreup\rest\models\repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;


class UsersRepository extends EntityRepository
{


	/**
	 * @param $email
	 * @param $password
	 * @return \foreup\rest\models\entities\ForeupUsers
	 * @throws \Doctrine\ORM\NoResultException
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function get_user_from_login($email, $password)
	{
		$qb = $this->createQueryBuilder('u');
		$dql = $qb
				->select('u')
				->andWhere('u.email = :email')
				->andWhere('u.password = :password')
				->getDQL();
		$em =  $this->getEntityManager();
		$result = $em->createQuery($dql)
				->setParameter("email", $email)
				->setParameter("password", md5($password))
				->getOneOrNullResult();
		return $result;
	}
}