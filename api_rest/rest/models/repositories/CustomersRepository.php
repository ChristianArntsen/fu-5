<?php

namespace foreup\rest\models\repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;


class CustomersRepository extends EntityRepository
{
	private $idOnly = false;

	private $searchableFields = [
		"group"=>["entityField"=>"groups.label","title"=>"group","label"=>"Customer Groups","type"=>"dropdown","possibleValues"=>[]],
		"course"=>["entityField"=>"course.name","title"=>"course","label"=>"Course","type"=>"text"],
		"first_name"=>["entityField"=>"person.firstName","title"=>"first_name","label"=>"First Name","type"=>"text"],
		"last_name"=>["entityField"=>"person.lastName","title"=>"last_name","label"=>"Last Name","type"=>"text"],
		"company_name"=>["entityField"=>"customers.companyName","title"=>"company_name","label"=>"Company","type"=>"text"],
		"email"=>["entityField"=>"person.email","title"=>"email","label"=>"Email","type"=>"text"],
		"handicap_account_number"=>["entityField"=>"person.handicapAccountNumber","title"=>"handicap_account_number","label"=>"Handicap Account Number","type"=>"text"],
		"handicap_score"=>["entityField"=>"person.handicapScore","title"=>"handicap_score","label"=>"Handicap Score","type"=>"number"],
		"phone_number"=>["entityField"=>"person.phoneNumber","title"=>"phone_number","label"=>"Phone","type"=>"text"],
		"account_number"=>["entityField"=>"customers.accountNumber","title"=>"account_number","label"=>"Account Number","type"=>"text"],
		"account_balance"=>["entityField"=>"customers.accountBalance","title"=>"account_balance","label"=>"Account Balance","type"=>"number"],
		"member_account_balance"=>["entityField"=>"customers.memberAccountBalance","title"=>"member_account_balance","label"=>"Member Account Balance","type"=>"number"],
		"taxable"=>["entityField"=>"customers.taxable","title"=>"taxable","label"=>"Taxable","type"=>"bool"],
		"discount"=>["entityField"=>"customers.discount","title"=>"discount","label"=>"Discount","type"=>"number"],
		"opt_out_email"=>["entityField"=>"customers.optOutEmail","title"=>"opt_out_email","label"=>"Opt Out Email","type"=>"bool"],
		"opt_out_text"=>["entityField"=>"customers.optOutText","title"=>"opt_out_text","label"=>"Opt Out Text","type"=>"bool"],
		"email_subscribed"=>["entityField"=>"customers.optOutEmail","title"=>"email_subscribed","label"=>"Email Subscribed","type"=>"bool"],
		"date_created"=>["entityField"=>"customers.dateCreated","title"=>"date_created","label"=>"Date Created","type"=>"date"],
		"birthday"=>["entityField"=>"person.birthday","title"=>"birthday","label"=>"Birthday","type"=>"date"],
		"city"=>["entityField"=>"person.city","title"=>"city","label"=>"City","type"=>"text"],
		"state"=>["entityField"=>"person.state","title"=>"state","label"=>"State","type"=>"dropdown","possibleValues"=>[]],
		"zip"=>["entityField"=>"person.zip","title"=>"zip","label"=>"Zip","type"=>"text"],
		"deleted"=>["entityField"=>"customers.deleted","title"=>"deleted","label"=>"Deleted","type"=>"bool"],
		"loyalty"=>["entityField"=>"customers.useLoyalty","title"=>"loyalty","label"=>"Enrolled In Loyalty","type"=>"bool"]
	];


	public function getAllSearchableFields()
	{
		return $this->searchableFields;
	}
	public function isSearchableField($field)
	{
		return isset($this->searchableFields[$field]);
	}
	public function getSearchableAlias($field)
	{
		if($this->isSearchableField($field))
			return $this->searchableFields[$field]['entityField'];

		return false;
	}
	public function isFieldDate($field)
	{
		return $this->searchableFields[$field]['type'] == "date";
	}

	/**
	 * @param Filter[] $filters
	 * @return array
	 */
	public function getFilteredResults($filters = [],$courses = [],$start =0,$count=100,Expr\Base $extraQuery = null,$orderBy=null,$sort=null){
		$query = $this->createQuery($filters, $courses, $start, $count, $extraQuery, $orderBy, $sort);

		return $query->getResult();
	}

	public function getFilteredCount($filters = [],$courses = [],$start =0,$count=100,Expr\Base $extraQuery = null,$orderBy=null,$sort=null){
		$this->countOnly = true;
		$query = $this->createQuery($filters, $courses, $start, $count, $extraQuery, $orderBy, $sort);


		return $query->getSingleScalarResult();
	}

	public function getArrayResults($filters = [],$courses = [],$start =0,$count=100,Expr\Base $extraQuery = null,$orderBy=null,$sort=null){
		$query = $this->createQuery($filters, $courses, $start, $count, $extraQuery, $orderBy, $sort);

		return $query->getArrayResult();
	}

	/**
	 * @param \foreup\rest\models\repositories\Filter[] $filters
	 * @param $courseId
	 * @param $start
	 * @param $count
	 * @param Expr\Base $extraQuery
	 * @param $orderBy
	 * @param $sort
	 * @return Query
	 */
	public function createQuery($filters, $courses = [], $start, $count,$extraQuery, $orderBy, $sort)
	{
		$qb = $this->createQueryBuilder('customers');
		$qb->join('customers.person', 'person');
		$qb->join('customers.course', 'course');
		$qb->leftJoin('customers.groups', 'groups');

		$andX = $qb->expr()->andX();
		$filterOutDeleted = true;
		foreach ($filters as $filter) {
			if ($filter->getExpression()->getLeftExpr() == "customers.deleted") {
				$filterOutDeleted = false;
			}
			$andX->add($filter->getExpression());
		}

		if ($filterOutDeleted) {
			$andX->add($qb->expr()->eq('customers.deleted', 0));
		}
		$andX->add($qb->expr()->eq('customers.hideFromSearch', 0));

		if ($extraQuery) {
			$andX->add($extraQuery);
		}


		$qb->where($qb->expr()->andX(
			$andX,
			$qb->expr()->in("customers.course", $courses)
		));

		if(isset($this->countOnly)){
			$qb->select("count(DISTINCT(customers.person))");
		} else {
			if($this->idOnly){
				$qb->select("person.personId");
			} else {
				$qb->select("customers","person");
			}
			$qb->setFirstResult($start);
			$qb->setMaxResults($count);

			$qb->groupBy("customers.person");
		}

		if (isset($orderBy) && isset($this->searchableFields[$orderBy]['entityField'])) {
			$qb->orderBy($this->searchableFields[$orderBy]['entityField'], $sort);
		}
		$query = $qb->getQuery();

		foreach($filters as $filter){
			if(is_object($filter->getExpression()))
				$query->setParameter($filter->getExpression()->getRightExpr(),$filter->getValue());
		}
		return $query;
	}

	/**
	 * @return bool
	 */
	public function isIdOnly()
	{
		return $this->idOnly;
	}

	/**
	 * @param bool $idOnly
	 */
	public function setIdOnly($idOnly)
	{
		$this->idOnly = $idOnly;
	}


}