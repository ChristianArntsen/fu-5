<?php

namespace foreup\rest\models\repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use foreup\rest\models\entities\ForeupEmployeeAuditLog;


class EmployeeAuditLogRepository extends EntityRepository
{

    /**
     * @return \foreup\rest\models\repositories\EmployeeAuditLogRepository
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get_all_log_entries()
    {
        $qb = $this->createQueryBuilder('log_entry');
        $dql = $qb
            ->select('log_entry')
            ->getDQL();
        $em =  $this->getEntityManager();
        $q = $em->createQuery($dql);
        $this->eager_load_logs($q);
        return $q->getResult();
    }

    public function get_log_entry($id)
    {
        $qb = $this->createQueryBuilder('log_entry');
        $dql = $qb
            ->select('log_entry')
            ->where('log_entry.id = :id')
            ->getDQL();
        $em =  $this->getEntityManager();
        $q = $em->createQuery($dql);
        $q->setParameter('id',$id);
        $this->eager_load_logs($q);
        return $q->getResult();
    }
    
    public function get($id) {
        $em = $this->getEntityManager();
        return $em->find('foreup\rest\models\entities\ForeupEmployeeAuditLog',$id);
    }

    public function redo($target)
    {
        //

        // dispatch to the appropriate undo function
        $action = $target->getActionType();
        if($action->getId()*1===11){
            // bulk upsert
            return array('data' => $this->undo_bulk_delete($target));
        }
        elseif($action->getId()*1===8){
            // bulk delete
            return array('data' => $this->undo_bulk_upsert($target));
        }
        else if(is_int($action)) {
            return $this->respondWithError("Unrecognized action_type", 400);
        }
    }

    public function undo($target)
    {
        // attempts to undo an audit log action

        // dispatch to the appropriate undo function
        $action = $target->getActionType();
        if($action->getId()*1===11){
            // bulk upsert
            return array('data' => $this->undo_bulk_upsert($target));
        }
        elseif($action->getId()*1===8){
            // bulk delete
            return array('data' => $this->undo_bulk_delete($target));
        }
        else if(is_int($action)) {
            return $this->respondWithError("Unrecognized action_type", 400);
        }
    }

    private function undo_bulk_upsert($target)
    {
        // attempts to undo an upsert action
        $delete = 1;
        // dispatch to each paradigm
        return $this->undo_bulk_person_altered($target,$delete);
    }

    private function undo_bulk_delete($target)
    {
        // attempts to undo an upsert action
        $delete = 0;
        // dispatch to each paradigm
        return $this->undo_bulk_person_altered($target,$delete);
    }

    private function undo_bulk_person_altered($target,$delete)
    {
        // interact with person altered table to undo a bulk upsert

        //*
        $tables = array(array('foreup\rest\models\entities\ForeupEmployees','foreup_employees'),array('foreup\rest\models\entities\ForeupCustomers','foreup_customers')/*no user until we have a fkey*/);
        $results = array('type'=>'employee_audit_log','id'=> (string) $target->getId(),
                         'meta'=>array('tables'=>array()),
                         'relationshps'=>array(
                             'editor'=>array('type'=>'employee','id'=> (string) $target->getEditor()->getId())),
                             'action_type'=>array('type'=>'action_type','id'=> (string) $target->getActionType()->getId())
                         );
        foreach($tables as $table){
            $qb = $this->createQueryBuilder('log_entry');
            $dql = $qb
                ->update($table[0], 'emp')
                ->set('emp.deleted', $delete)
                // doctrine hates joins in update query, so we do sub-query here
                ->where('emp.person IN (SELECT p FROM foreup\rest\models\entities\ForeupPeople p,
                                                      foreup\rest\models\entities\ForeupEmployeeAuditLog eal,
                                                      foreup\rest\models\entities\ForeupPersonAltered pa
                                                 WHERE eal.id = :id
                                                 AND eal = pa.employeeAuditLog
                                                 AND pa.tablesUpdated = :table
                                                 AND pa.personEdited = p)')
                // //TODO: this might not work in all conceivable cases. Need a separate linking table for each table (e.g. employee_altered)
                // For now we can use implicit behaviours, because to break this, other bits need to be broken too
                //->andWhere('emp.courseId = :course_id')
                ->getDQL();
            $em = $this->getEntityManager();
            $q = $em->createQuery($dql);

            $q->setParameter('id', $target->getId());
            $q->setParameter('table', $table[1]);
            $this->eager_load_logs($q);
            //array('affected_rows'=>$q->getResult(),'table'=>$table[1])
            $results['meta']['tables'][]=array('table'=>$table[1],'affected_rows'=>$q->getResult());
        }
        //*/

        // then response
        return $results;
    }

    public function get_course_log_entries($course_id)
    {
        //*
        $qb = $this->createQueryBuilder('log_entry');
        $dql = $qb
            ->select('eal')
            ->from('foreup\rest\models\entities\ForeupEmployeeAuditLog','eal')
            ->from('foreup\rest\models\entities\ForeupEmployees','emp')
            ->andWhere('eal.editor = emp')
            ->andWhere('emp.courseId = :course_id')
            ->orderBy('eal.gmtLogged','DESC')
            ->getDQL();
        $em =  $this->getEntityManager();
        $q = $em->createQuery($dql);

        $q->setParameter('course_id',$course_id);
        $this->eager_load_logs($q);
        return $q->getResult();
        //*/
    }

    public function get_employee_log_entries($id)
    {
        //*
        $qb = $this->createQueryBuilder('log_entry');
        $dql = $qb
            ->select('eal')
            ->from('foreup\rest\models\entities\ForeupEmployeeAuditLog','eal')
            ->from('foreup\rest\models\entities\ForeupEmployees','emp')
            ->andWhere('eal.editor = emp')
            ->andWhere('emp.id = :id')
            ->getDQL();
        $em =  $this->getEntityManager();
        $q = $em->createQuery($dql);

        $q->setParameter('id',$id);
        $this->eager_load_logs($q);
        return $q->getResult();
        //*/

        /* This syntax works as well. Included for educational purposes.
        $em =  $this->getEntityManager();
        $q = $em->createQuery('SELECT pa from foreup\rest\models\entities\ForeupPersonAltered pa, foreup\rest\models\entities\ForeupEmployeeAuditLog eal, foreup\rest\models\entities\ForeupEmployees emp
                               where pa.employeeAuditLog = eal and eal.editor = emp and emp.id = :id');
        $q->setParameter('id',$id);
        $this->eager_load_logs($q);
        $res = $q->getResult(); // gets an array...
        return $res;
        //*/
    }

    private function eager_load_logs(&$q)
    {
        // can we set fetch mode for more than 1?
        $q->setFetchMode("foreup\rest\models\entities\ForeupEmployeeAuditLog", "editor_id", \Doctrine\ORM\Mapping\ClassMetadata::FETCH_EAGER);
        $q->setFetchMode("foreup\rest\models\entities\ForeupEmployeeAuditLog", "action_type_id", \Doctrine\ORM\Mapping\ClassMetadata::FETCH_EAGER);
        $q->setFetchMode("foreup\rest\models\entities\ForeupEmployees", "person_id", \Doctrine\ORM\Mapping\ClassMetadata::FETCH_EAGER);
        $q->setFetchMode("foreup\rest\models\entities\ForeupUsers", "person_id", \Doctrine\ORM\Mapping\ClassMetadata::FETCH_EAGER);
    }
}