<?php

namespace foreup\rest\models\repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use foreup\rest\models\entities\ForeupAccountPayments;


class PaymentsRepository extends EntityRepository
{
    public function create($payment)
    {
        $max_number = $this->getEntityManager()->createQueryBuilder('al')
            ->select('MAX(al.number)')
            ->from('e:ForeupAccountPayments', 'al')
            ->where("al.account = :account_id")
            ->setParameter("account_id",(int)$payment->getAccount()->getId())
            ->getQuery()
            ->getSingleScalarResult();
        if($max_number === null){
            $max_number = 0;
        }
        $max_number++;
        $payment->setNumber($max_number);
        $this->getEntityManager()->persist($payment);
    }


    /**
     * @param $userAccount
     * @return ForeupAccountPayments[]
     */
    public function getOpenPayments($userAccount)
    {
        $pendingCharges = $this->getEntityManager()->createQueryBuilder('ap')
            ->select('ap')
            ->from('e:ForeupAccountPayments', 'ap')
            ->andWhere("ap.account = :account")
            ->andWhere("ap.amountOpen > 0")
            ->setParameter("account",$userAccount)
            ->getQuery()
            ->getResult();
        return $pendingCharges;
    }
}