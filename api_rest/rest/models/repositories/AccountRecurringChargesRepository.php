<?php

namespace foreup\rest\models\repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;


class AccountRecurringChargesRepository extends EntityRepository
{
	private $searchableFields = [

	];

	public function getAllSearchableFields()
	{
		return $this->searchableFields;
	}

	public function isSearchableField($field)
	{
		return isset($this->searchableFields[$field]);
	}

	public function getSearchableAlias($field)
	{
		if($this->isSearchableField($field))
			return $this->searchableFields[$field]['entityField'];

		return false;
	}

	public function isFieldDate($field)
	{
		return $this->searchableFields[$field]['type'] == "date";
	}

	/**
	 * @param Filter[] $filters
	 * @return array
	 */
	public function getFilteredResults($filters = [],$courses = [],$start =0,$count=100,Expr\Base $extraQuery = null,$orderBy=null,$sort=null){
		$query = $this->createQuery($filters, $courses, $start, $count, $extraQuery, $orderBy, $sort);

		return $query->getResult();
	}

	public function getFilteredCount($filters = [],$courses = [],$start =0,$count=100,Expr\Base $extraQuery = null,$orderBy=null,$sort=null){
		$this->countOnly = true;
		$query = $this->createQuery($filters, $courses, $start, $count, $extraQuery, $orderBy, $sort);


		return $query->getSingleScalarResult();
	}

	/**
	 * @param $filters
	 * @param $courseId
	 * @param $start
	 * @param $count
	 * @param Expr\Base $extraQuery
	 * @param $orderBy
	 * @param $sort
	 * @return array
	 */
	public function createQuery($filters, $courses = [], $start, $count,$extraQuery, $orderBy, $sort)
	{
		$qb = $this->createQueryBuilder('recurringCharges');
		//$qb->distinct()->leftJoin('customers.person', 'person');
		//$qb->distinct()->leftJoin('customers.groups', 'groups');

		$andX = $qb->expr()->andX();
		$filterOutDeleted = true;

        /*
        foreach ($filters as $filter) {
			if ($filter->getExpression()->getLeftExpr() == "statements.deleted") {
				$filterOutDeleted = false;
			}
			$andX->add($filter->getExpression());
		} */

		if ($filterOutDeleted) {
			$andX->add($qb->expr()->isNull('recurringCharges.dateDeleted'));
		}

		if ($extraQuery) {
			$andX->add($extraQuery);
		}

		$qb->where($qb->expr()->andX(
			$andX,
			$qb->expr()->in("recurringCharges.organizationId", $courses)
		));

		if(isset($this->countOnly)){
			$qb->select("count(DISTINCT(recurringCharges))");
		} else {
			//$qb->select("customers","person","groups");
			$qb->setFirstResult($start);
			$qb->setMaxResults($count);
		}

		if (isset($orderBy) && isset($this->searchableFields[$orderBy]['entityField'])) {
			$qb->orderBy($this->searchableFields[$orderBy]['entityField'], $sort);
		}
		$query = $qb->getQuery();

		foreach($filters as $filter){
			if(is_object($filter->getExpression()))
				$query->setParameter($filter->getExpression()->getRightExpr(),$filter->getValue());
		}
		return $query;
	}
}