<?php

namespace foreup\rest\models\repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;


class PersonAlteredRepository extends EntityRepository
{

    /**
     * @param $email
     * @param $password
     * @return \foreup\rest\models\entities\ForeupPersonAltered
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get_all_log_entries()
    {
        //*
        $qb = $this->createQueryBuilder('log_entry');
        $dql = $qb
            ->select('log_entry')
            ->getDQL();
        $em =  $this->getEntityManager();
        $q = $em->createQuery($dql);
        $this->eager_load_logs($q);
        return $q->getResult();
        //*/
    }

    public function get_course_log_entries($course_id)
    {
        //*
        $qb = $this->createQueryBuilder('log_entry');
        $dql = $qb
            ->select('log_entry')
            ->from('foreup\rest\models\entities\ForeupEmployeeAuditLog','eal')
            ->from('foreup\rest\models\entities\ForeupEmployees','emp')
            ->where('log_entry.employeeAuditLog = eal')
            ->andWhere('eal.editor = emp')
            ->andWhere('emp.courseId = :course_id')
            ->getDQL();
        $em =  $this->getEntityManager();
        $q = $em->createQuery($dql);

        $q->setParameter('course_id',$course_id);
        $this->eager_load_logs($q);
        return $q->getResult();
        //*/
    }

    public function get_employee_log_entries($id)
    {
        //*
        $qb = $this->createQueryBuilder('log_entry');
        $dql = $qb
            ->select('log_entry')
            ->from('foreup\rest\models\entities\ForeupEmployeeAuditLog','eal')
            ->from('foreup\rest\models\entities\ForeupEmployees','emp')
            ->where('log_entry.employeeAuditLog = eal')
            ->andWhere('eal.editor = emp')
            ->andWhere('emp.id = :id')
            ->getDQL();
        $em =  $this->getEntityManager();
        $q = $em->createQuery($dql);

        $q->setParameter('id',$id);
        $this->eager_load_logs($q);
        return $q->getResult();
        //*/

        /* This syntax works as well. Included for educational purposes.
        $em =  $this->getEntityManager();
        $q = $em->createQuery('SELECT pa from foreup\rest\models\entities\ForeupPersonAltered pa, foreup\rest\models\entities\ForeupEmployeeAuditLog eal, foreup\rest\models\entities\ForeupEmployees emp
                               where pa.employeeAuditLog = eal and eal.editor = emp and emp.id = :id');
        $q->setParameter('id',$id);
        $this->eager_load_logs($q);
        $res = $q->getResult(); // gets an array...
        return $res;
        //*/
    }

    public function get_course_employee_log_entries($course_id,$employee_id)
    {
        //*
        $qb = $this->createQueryBuilder('log_entry');
        $dql = $qb
            ->select('log_entry')
            ->from('foreup\rest\models\entities\ForeupEmployeeAuditLog','eal')
            ->from('foreup\rest\models\entities\ForeupEmployees','emp')
            ->where('log_entry.employeeAuditLog = eal')
            ->andWhere('eal.editor = emp')
            ->andWhere('emp.courseId = :course_id')
            ->andWhere('emp.id = :id')
            ->getDQL();
        $em =  $this->getEntityManager();
        $q = $em->createQuery($dql);

        $q->setParameter('course_id',$course_id);
        $q->setParameter('id',$employee_id);
        $this->eager_load_logs($q);
        return $q->getResult();
        //*/
    }

    private function eager_load_logs(&$q)
    {
        $q->setFetchMode("foreup\rest\models\entities\ForeupPersonAltered", "employee_audit_log_id", \Doctrine\ORM\Mapping\ClassMetadata::FETCH_EAGER);
        $q->setFetchMode("foreup\rest\models\entities\ForeupPersonAltered", "person_edited", \Doctrine\ORM\Mapping\ClassMetadata::FETCH_EAGER);
        $q->setFetchMode("foreup\rest\models\entities\ForeupEmployeeAuditLog", "editor_id", \Doctrine\ORM\Mapping\ClassMetadata::FETCH_EAGER);
        $q->setFetchMode("foreup\rest\models\entities\ForeupEmployeeAuditLog", "action_type_id", \Doctrine\ORM\Mapping\ClassMetadata::FETCH_EAGER);
    }
}