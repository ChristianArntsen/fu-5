<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 9/23/2016
 * Time: 3:36 PM
 */

namespace foreup\rest\models\services;


use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupItems;
use foreup\rest\models\entities\ForeupPosCart;
use foreup\rest\models\entities\ForeupPosCartItems;
use foreup\rest\models\entities\ForeupSales;
use foreup\rest\models\entities\ForeupSalesItems;
use foreup\rest\models\entities\ForeupTeetime;
use foreup\rest\models\entities\LegacyObjects\Payment;
use foreup\rest\models\entities\LegacyObjects\PriceClass;
use foreup\rest\models\entities\LegacyObjects\TeetimeSlot;
use Guzzle\Common\Exception\ExceptionCollection;
use GuzzleHttp\Client;
use JsonSchema\Exception\InvalidArgumentException;

class LegacyCartService
{

	private $cartId;
	private $db;

	public function __construct($url,EntityManager $db)
	{
		$this->db = $db;
		$this->client = new Client(['base_uri' => $url.'/index.php/']);
		$this->headers = [
			'x-authorization' => 'Bearer ',
			"api_key"=>"no_limits",
			"Content-Type"=>"application/json"
		];
		$this->date = Carbon::now()->toDateString();
		$this->startTime = 0000;
		$this->endTime = 2359;
		$this->holes = 18;
	}

	public function setToken($token)
	{
		$this->headers['x-authorization'] = "Bearer ".$token;
	}

	public function addPayment(Payment $payment)
	{

		try {
			$response = $this->client->request('POST', '/index.php/api/cart/'.$this->cartId.'/payments', [
				"headers"=>$this->headers,
				"body"=> json_encode([
					"type"=>$payment->getType(),
					"amount"=>$payment->getAmount()
				])
			]);
		} catch (\Exception $e)
		{
			$response = $e->getResponse()->getBody()->getContents();
			$response = json_decode($response);
			if($response){
				if(!empty($response->error) && $response->error == "missing_cart"){
					throw new \Exception("The cart id is missing");
				} else if($response->success == false){
					throw new \Exception($response->msg);
				} else {
					throw new \Exception("Unknown error processing the request.");
				}

			}
			throw new \Exception("Invalid response from booking server.");
		}


		$responseBody = json_decode($response->getBody()->getContents());

		if(empty($responseBody->cart_id)){
			return false;
		}


		return $responseBody->cart_id;
	}


	public function addItem(ForeupPosCartItems $item)
	{
		try {
			$response = $this->client->request('PUT', '/index.php/api/cart/'.$this->cartId.'/items', [
				"headers"=>$this->headers,
				"body"=> json_encode([
					"item_id"=>$item->getItem()->getItemId(),
					"item_type"=>$item->getItemType(),
					"quantity"=>$item->getQuantity(),
					"unit_price"=>$item->getUnitPrice(),
					"discount_percent"=>$item->getDiscountPercent()
				])
			]);
		} catch (\Exception $e)
		{
			$response = $e->getResponse()->getBody()->getContents();
			$response = json_decode($response);
			if($response){
				if(!empty($response->error) && $response->error == "missing_cart"){
					throw new \Exception("The cart id is missing");
				} else if($response->success == false){
					throw new \Exception($response->msg);
				} else {
					throw new \Exception("Unknown error processing the request.");
				}

			}
			throw new \Exception("Invalid response from booking server.");
		}


		$responseBody = json_decode($response->getBody()->getContents());

		if(empty($responseBody->success)){
			return false;
		}


		return $responseBody->success;
	}

	/**
	 * @return ForeupPosCart
	 * @throws \Exception
	 */
	public function getCart()
	{
		$response = $this->client->request('GET', '/index.php/api/cart/'.$this->cartId, [
			"headers"=>$this->headers
		]);



		$responseBody = json_decode($response->getBody()->getContents());
		$cart = new ForeupPosCart();
		if(empty($responseBody)){
			throw new \Exception("Invalid response from server.");
		}
		$cart->setTotal($responseBody->total);
		$cart->setTotalDue($responseBody->total_due);
		$cart->setTax($responseBody->tax);
		$cart->setSubtotal($responseBody->subtotal);
		$cart->setCartId($responseBody->cart_id);

		/** @var EntityRepository $itemRepo */
		$itemRepo = $this->db->getRepository('e:ForeupItems');
		foreach($responseBody->items as $data){
			$item = $itemRepo->find($data->item_id);
			if(empty($item)){
				throw new \Exception("Invalid item in cart.");
			}
			$cartItem = new ForeupPosCartItems($cart,$data);
			$cartItem->setItem($item);
			$cart->addItem($cartItem);
		}

		return $cart;
	}

	public function completeCart()
	{

		try {
			$response = $this->client->request('POST', '/index.php/api/cart/'.$this->cartId, [
				"headers"=>$this->headers,
				"body"=> json_encode([
					"status"=>"complete"
				])
			]);
		} catch (\Exception $e)
		{
			$response = $e->getResponse()->getBody()->getContents();
			$response = json_decode($response);
			if($response){
				if(!empty($response->error) && $response->error == "missing_cart"){
					throw new \Exception("The cart id is missing");
				} else if($response->success == false){
					throw new \Exception($response->msg);
				} else {
					throw new \Exception("Unknown error processing the request.");
				}

			}
			throw new \Exception("Invalid response from booking server.");
		}





		$responseBody = json_decode($response->getBody()->getContents());

		if(empty($responseBody->sale_id)){
			return false;
		}
		/**
		 * "msg": "Sale completion failed, total due has not been met"
		 *
		 *   "msg": "Your sale has timed out. Refreshing page...",
		"error": "missing_cart"
		 *
		 *
		 *   "sale_id": 5049,
		"number": 343,
		"loyalty_points_spent": 0,
		"cart_id": 506,
		"success": true
		 */

		return $responseBody->sale_id;
	}

	/**
	 * @return mixed
	 */
	public function getCartId()
	{
		return $this->cartId;
	}

	/**
	 * @param mixed $cartId
	 */
	public function setCartId($cartId)
	{
		$this->cartId = $cartId;
	}


}