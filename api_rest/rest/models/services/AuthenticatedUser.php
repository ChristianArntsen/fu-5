<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 9/23/2016
 * Time: 3:36 PM
 */

namespace foreup\rest\models\services;


use foreup\auth\json_web_token;

class AuthenticatedUser
{
	private $loggedIn,$validated,$authorized,$uid,$cid,$level,$empId,$isEmployee,$token,$appId,$apiVersion,$limitations;

	/** @var  json_web_token $token */
	private $tokenObject;
	public function __construct()
	{
		$this->loggedIn = true;
		$this->validated= true;
		$this->authorized= true;
	}

	public function hasLimitation($limitation)
	{
		if(!isset($this->limitations)){
			return true;
		}
		return isset($this->limitations->{$limitation}) && $this->limitations->{$limitation};
	}
	/**
	 * @return mixed
	 */
	public function getLimitations()
	{
		return $this->limitations;
	}

	/**
	 * @param mixed $limitations
	 */
	public function setLimitations($limitations)
	{
		$this->limitations = $limitations;
	}

	/**
	 * @return mixed
	 */
	public function getApiVersion()
	{
		return $this->apiVersion;
	}

	/**
	 * @param mixed $apiVersion
	 */
	public function setApiVersion($apiVersion)
	{
		$this->apiVersion = $apiVersion;
	}



	/**
	 * @return mixed
	 */
	public function getAppId()
	{
		if(empty($this->appId))
			return $this->uid;
		return $this->appId;
	}

	/**
	 * @param mixed $appId
	 */
	public function setAppId($appId)
	{
		$this->appId = $appId;
		$this->tokenObject->addPrivateClaim("appId",$appId);
	}

	/**
	 * @return mixed
	 */
	public function getToken()
	{
		return $this->token;
	}

	/**
	 * @param mixed $token
	 */
	public function setToken($token)
	{
		$this->token = $token;
	}

	/**
	 * @return json_web_token
	 */
	public function getTokenObject(): json_web_token
	{
		return $this->tokenObject;
	}

	/**
	 * @param json_web_token $tokenObject
	 */
	public function setTokenObject(json_web_token $tokenObject)
	{
		$this->tokenObject = $tokenObject;
	}


	/**
	 * @return boolean
	 */
	public function isLoggedIn()
	{
		return $this->loggedIn;
	}

	/**
	 * @param boolean $loggedIn
	 */
	public function setLoggedIn($loggedIn)
	{
		$this->loggedIn = $loggedIn;
	}

	/**
	 * @return boolean
	 */
	public function isValidated()
	{
		return $this->validated;
	}

	/**
	 * @param boolean $validated
	 */
	public function setValidated($validated)
	{
		$this->validated = $validated;
	}

	/**
	 * @return boolean
	 */
	public function isAuthorized()
	{
		return $this->authorized;
	}

	/**
	 * @param boolean $authorized
	 */
	public function setAuthorized($authorized)
	{
		$this->authorized = $authorized;
	}

	/**
	 * @return mixed
	 */
	public function getUid()
	{
		return $this->uid;
	}

	/**
	 * @param mixed $uid
	 */
	public function setUid($uid)
	{
		$this->uid = $uid;
		$this->tokenObject->addPrivateClaim("uid",$uid);
	}

	/**
	 * @return mixed
	 */
	public function getCid()
	{
		return $this->cid;
	}

	/**
	 * @param mixed $cid
	 */
	public function setCid($cid)
	{
		$this->cid = $cid;
		$this->tokenObject->addPrivateClaim("cid",$cid);
	}

	/**
	 * @return mixed
	 */
	public function getLevel()
	{
		return $this->level;
	}

	/**
	 * @param mixed $level
	 */
	public function setLevel($level)
	{
		$this->level = $level;
	}

	/**
	 * @return mixed
	 */
	public function getEmpId()
	{
		return $this->empId;
	}

	/**
	 * @param mixed $empId
	 */
	public function setEmpId($empId)
	{
		$this->empId = $empId;
	}

	/**
	 * @return mixed
	 */
	public function getIsEmployee()
	{
		return $this->isEmployee;
	}

	/**
	 * @param mixed $isEmployee
	 */
	public function setIsEmployee($isEmployee)
	{
		$this->isEmployee = $isEmployee;
	}


}