<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 9/23/2016
 * Time: 3:36 PM
 */

namespace foreup\rest\models\services;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class TeesheetStatsService
{

	private $teesheetId,$token;

	public function __construct($url)
	{
		$this->client = new Client(['base_uri' => $url.'/index.php/']);
	}


	/**
	 * @param $teesheetId
	 * @param $token
	 * @param Carbon $start
	 * @param Carbon $end
	 * @return \foreup\rest\models\entities\LegacyObjects\TeesheetStats
	 */
	public function get($teesheetId, $token, $start,$end = null)
	{
		$this->teesheetId = $teesheetId;
		$this->token = $token;


		$headers = ['x-authorization' => 'Bearer '.$this->token];

		$request = new \GuzzleHttp\Psr7\Request('GET', '/teesheets/generate_stats?',$headers);
		if(!isset($end)){
			$paramters = [
				"query"=>[
					"teesheet_id"=>$this->teesheetId,
					"date"=>$start->toIso8601String()
				]];
		} else {
			if($start->toDateString() != $end->toDateString()){
				throw new \Exception("Unable to process stats across more than one day.");
			}
			if($end < $start){
				throw new \Exception("The end time must be after the start time.");
			}
			$paramters = [
				"query"=>[
					"teesheet_id"=>$this->teesheetId,
					"start"=>$start->toIso8601String(),
					"end"=>$end->toIso8601String()
				]];
		}

		$response = $this->client->send($request, $paramters);
		$responseBody = $response->getBody()->getContents();
		$stats = json_decode($responseBody);
		if(empty($stats->header)){
			return false;
		}

		$statsObject = new \foreup\rest\models\entities\LegacyObjects\TeesheetStats();
		$statsObject->setDate($start);
		$statsObject ->setBookings($stats->header->Bookings);
		$statsObject ->setPlayerNoShows($stats->header->Player_No_Shows);
		$statsObject ->setPlayersCheckedIn($stats->header->Players_Checked_in);
		$statsObject->setOccupancy($stats->body->occupancy);
		$statsObject->setRevenue(isset($stats->body->revenue)?$stats->body->revenue:null);
		$statsObject->setPotentialSlots($stats->body->potential_slots);
		$statsObject->setSlotsAvailable($stats->body->slots_available);



		return $statsObject;
	}
}