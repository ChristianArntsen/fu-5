<?php
namespace foreup\rest\models\services;


use foreup\rest\models\entities\ForeupAccountLedger;
use Doctrine\ORM\EntityRepository;
use foreup\rest\models\entities\ForeupAccountLedgerPayments;
use foreup\rest\models\entities\ForeupAccountPayments;
use foreup\rest\models\entities\ForeupPaymentTypes;
use foreup\rest\models\repositories\PaymentsRepository;

class AccountLedgerService
{
	private $mySpending;

	public function __construct($user,$account,$db)
	{

		/** @var \foreup\rest\models\entities\ForeupEmployees $user*/
		/** @var \foreup\rest\models\entities\ForeupAccounts $account*/
		$this->user = $user;
		$this->account = $account;
		$this->mySpending = 0;
		$this->db = $db;
	}


	/**
	 * @param \foreup\rest\models\entities\ForeupAccountLedger $accountLedgerItem
	 * @return bool|mixed
	 */
	public function credit($accountLedgerItem)
	{
		$accountLedgerItem->setPersonId($this->user->getPerson()->getPersonId());
		$amount = $this->account->creditAccount($accountLedgerItem);

		/** @var \foreup\rest\models\entities\ForeupPaymentTypes $account*/
		/** @var \foreup\rest\models\repositories\AccountPaymentsRepository $paymentsRepo*/
		$paymentTypeRepo = $this->db->getRepository('e:ForeupPaymentTypes');
		$paymentsRepo = $this->db->getRepository('e:ForeupAccountPayments');
		$paymentType = $paymentTypeRepo->findOneBy([
			"type"=>"adjustment"
		]);

		$payment = new ForeupAccountPayments($accountLedgerItem);
		$payment->setAmount($amount*-1);
		$payment->setAmountOpen($amount*-1);
		$payment->setPaymentType($paymentType);
		//TODO: Give the correct id
		$payment->setOrganizationId($this->user->getCourseId());
		$payment->setAccount($accountLedgerItem->getAccount());
		$payment->setPersonId($this->user->getPerson()->getPersonId());
		$paymentsRepo->create($payment);

		$this->distributePayment($payment);
		$this->distributeAllRemaningPayments();
		$this->db->persist($payment);

		return $amount;
	}

	/**
	 * @param \foreup\rest\models\entities\ForeupAccountPayments $payment
	 * @return bool|mixed
	 */
	public function distributePayment($payment)
	{
		/** @var \foreup\rest\models\repositories\AccountLedgerRepository $accountLedgerRepo */
		$accountLedgerRepo = $this->db->getRepository('e:ForeupAccountLedger');
		$openItems = $accountLedgerRepo->getAllOpenItems($this->account);
		foreach($openItems as $item){
			$amountToDistribute = $payment->getAmountOpen() > $item->getAmountOpen()? $item->getAmountOpen() : $payment->getAmountOpen();

			$ledgerPayment = new ForeupAccountLedgerPayments();
			$ledgerPayment->setAmount($amountToDistribute);
			$ledgerPayment->setLedger($item);
			$ledgerPayment->setPayment($payment);
			$this->db->persist($ledgerPayment);


			$item->subAmountOpen($amountToDistribute);
			$payment->subAmountOpen($amountToDistribute);

			if(!$payment->hasBalance()){
				break;
			}
		}
	}

	public function distributeAllRemaningPayments()
	{
		/** @var PaymentsRepository $accountPaymentsRepo */
		$accountPaymentsRepo = $this->db->getRepository('foreup\rest\models\entities\ForeupAccountPayments');
		$payments = $accountPaymentsRepo->getOpenPayments($this->account);

		foreach($payments as $payment){
			$original_open = $payment->getAmountOpen();
			$this->distributePayment($payment);
			if($payment->getAmountOpen() == $original_open){
				break;
			}
		}

		return true;
	}


	/**
	 * @param \foreup\rest\models\entities\ForeupAccountLedger $accountLedgerItem
	 * @return bool|mixed
	 */
	public function debit($accountLedgerItem)
	{
		$accountLedgerItem->setPersonId($this->user->getPerson()->getPersonId());

		$this->account->setActualLimit($this->calculateSpendLimit());
		$amount = $this->account->debitAccount($accountLedgerItem);
		return $amount;
	}
	/**
	 * @param \foreup\rest\models\entities\ForeupAccountPayments $payment
	 * @return bool
	 */
	public function payOff($payment)
	{
		$ledgerItem = new ForeupAccountLedger();
		$ledgerItem->setAmount($payment->getAmount());


		$this->credit($ledgerItem);




	}

	/**
	 * @return \foreup\rest\models\entities\ForeupAccountTypes
	 */
	private function calculateSpendLimit()
	{

		/** @var \foreup\rest\models\repositories\AccountLedgerRepository $accountLedgerRepo */
		$accountLedgerRepo = $this->db->getRepository('foreup\rest\models\entities\ForeupAccountLedger');
		/** @var \foreup\rest\models\entities\ForeupAccountCustomers $accountCustomers */
		$accountCustomers = $this->db->getRepository('foreup\rest\models\entities\ForeupAccountCustomers');
		$customerAccount = $accountCustomers->findOneBy([
				"personId" => $this->user->getPerson()->getPersonId(),
				"account" => $this->account
		]);
		if($customerAccount === null){
			return 0;
		}



		/*
		 *
		 * My customer account limit is
		 * mylimit - myspending  OR totalLimit - totalBalance whichever is smaller
		 */
		$this->mySpending  = $accountLedgerRepo->getUsersPendingCharges($customerAccount);
		$customerAccountLimit = $customerAccount->getLimit() != null ? $customerAccount->getLimit() : $this->account->getAccountType();
		$accountTypeLimit = $this->account->getAccountType()->getLimit();

		if ($customerAccountLimit - $this->mySpending  < $accountTypeLimit - $this->account->getBalance()) {
			$limit = $customerAccountLimit - $this->mySpending ;
			return $limit;
		} else {
			$limit = $accountTypeLimit - $this->account->getBalance();
			return $limit;
		}
	}




	/**
	 * @return int
	 */
	public function getMySpending()
	{
		return $this->mySpending;
	}

	/**
	 * @param int $mySpending
	 */
	public function setMySpending($mySpending)
	{
		$this->mySpending = $mySpending;
	}


}