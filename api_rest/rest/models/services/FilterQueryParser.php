<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 1/20/2017
 * Time: 3:33 PM
 */

namespace foreup\rest\models\services;


use Carbon\Carbon;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use foreup\rest\models\repositories\Filter;
use Symfony\Component\HttpFoundation\Request;

class FilterQueryParser
{

	/**
	 * @param Request $request
	 * @param EntityRepository $repository
	 * @return Filter[]
	 */
	public function generateFilters(Request $request, EntityRepository $repository)
	{
		$expGen = new Expr();
		$filters = [];
		foreach($request->query->all() as $field => $parameter)
		{
			if($repository->isSearchableField($field)){
				$allValues = preg_split("/,/",$parameter);
				foreach($allValues as $key => $parameter){
					$matches =[];
					preg_match("/^((\w+):)?(.+)?$/",$parameter,$matches);
					if(count($matches) == 4){
						$operator = $matches[2];
						$value = $matches[3];
					}else if(count($matches) == 2){
						$operator = "eq";
						$value = $matches[1];
					} else {
						//ERROR
						return false;
					}

					if($value === "true"){
						$value = true;
					} else if($value === "false"){
						$value = false;
					}

					$expression = null;
					$boundParameter = ":".$field.$key;
					switch ($operator){
						case "eq":
							$expression = $expGen->eq($repository->getSearchableAlias($field),$boundParameter);
							break;
						case "neq":
							$expression = $expGen->neq($repository->getSearchableAlias($field),$boundParameter);
							break;
						case "lt":
							$expression = $expGen->lt($repository->getSearchableAlias($field),$boundParameter);
							break;
						case "lte":
							$expression = $expGen->lte($repository->getSearchableAlias($field),$boundParameter);
							break;
						case "gt":
							$expression = $expGen->gt($repository->getSearchableAlias($field),$boundParameter);
							break;
						case "gte":
							$expression = $expGen->gte($repository->getSearchableAlias($field),$boundParameter);
							break;
						case "like":
							$expression = $expGen->like($repository->getSearchableAlias($field),$boundParameter);
							$value = "%".$value."%";
							break;
						case "starts":
							$expression = $expGen->like($repository->getSearchableAlias($field),$boundParameter);
							$value = $value."%";
							break;
						case "ends":
							$expression = $expGen->like($repository->getSearchableAlias($field),$boundParameter);
							$value = "%".$value;
							break;

						case "":
							if($value=="isNull"){
								$boundParameter = null;
								$expression = $expGen->isNull($repository->getSearchableAlias($field));
								break;
							} else if ($value=="isNotNull"){
								$boundParameter = null;
								$expression = $expGen->isNotNull($repository->getSearchableAlias($field));
								break;
							} else {
								$expression = $expGen->eq($repository->getSearchableAlias($field),$boundParameter);
								break;
							}
						default:
							return false;
						//ERROR
					}

					if($repository->isFieldDate($field)){
						$value = Carbon::parse($value)->toDateTimeString();
					}
					$filter = new Filter($expression, $value,$boundParameter);
					$filters[] = $filter;
				}

			}
		}
		return $filters;
	}
}