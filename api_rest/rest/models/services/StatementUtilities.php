<?php
namespace foreup\rest\models\services;

use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupAccountTransactions;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupCustomers;

class StatementUtilities
{
    private $app, $db;
	private static $dateFormat = 'n/j/Y';

    public function __construct($app)
    {
        $this->app = $app;
        $this->db = $app['orm.em'];
    }

    public function getFriendlyName(ForeupAccountStatements $statement) {
	    $customer = $statement->getCustomer();
	    $course = $customer->getCourse();
	    $unique_array = [$course->getCourseId(),$statement->getRecurringStatement()->getId(),$statement->getDateCreated()->format('Y-m-d'),$customer->getPerson()->getLastName().'_'.$customer->getPerson()->getFirstName().'_'.$statement->getNumber()];
	    $file_unique_string = '/processStatementPdfJobs';//implode('/',$unique_array);
	    $path = '';
	    $file = '';
	    foreach ($unique_array as $subdir){
		    echo '/tmp'.$file_unique_string;
		    if(!file_exists('/tmp'.$file_unique_string))mkdir('/tmp'.$file_unique_string,0766);
		    // brute force it, since setting it in mkdir didn't seem to do the trick
		    chmod('/tmp'.$file_unique_string,0766);
		    $file_unique_string.='/'.$subdir;

	    }

	    return '/tmp'.$file_unique_string.'.pdf';

    }

	public function buildPdf(ForeupAccountStatements $statement,
	                         $charges,$customerTransactions = [],
	                         $memberTransactions = [],
	                         $invoiceTransactions = [],
                             $snappy = null
    )
	{
	    if(!isset($snappy)) {
		    $snappy = new  \Knp\Snappy\Pdf();
		    $snappy->setBinary("application/bin/wkhtmltopdf");
	    }
		// get the data nice and neat
		$customer = $statement->getCustomer();
		$course = $customer->getCourse();

		// build the html
		$html = $this->html($course,$customer,$statement,$charges,$customerTransactions,$memberTransactions,$invoiceTransactions);

		$unique_array = [$course->getCourseId(),$statement->getRecurringStatement()->getId(),$statement->getDateCreated()->format('Y-m-d'),$customer->getPerson()->getPersonId()];

		$file_unique_string = '/processStatementPdfJobs';//implode('/',$unique_array);
        $path = '';
        $file = '';
		foreach ($unique_array as $subdir){
		    echo '/tmp'.$file_unique_string;
			if(!file_exists('/tmp'.$file_unique_string))mkdir('/tmp'.$file_unique_string,0766);
            // brute force it, since setting it in mkdir didn't seem to do the trick
			chmod('/tmp'.$file_unique_string,0766);
			$path = $file_unique_string;
			$file = $subdir;
			$file_unique_string.='/'.$subdir;

		}

		$cwd = realpath('./');
        //chdir('/tmp'.$path);
		chmod('/tmp'.$path,0766);

		$hash = sha1($file_unique_string);

		if(\is_file('/tmp'.$path.'/'.$file.'.html')){
		    unlink('/tmp'.$path.'/'.$file.'.html');
        }

		touch('/tmp'.$path.'/'.$file.'.html');

		chmod('/tmp'.$path.'/'.$file.'.html',0766);

		if(!is_writable('/tmp'.$path.'/'.$file.'.html')){
			throw new \Exception('Cannot write the statement utility email to disk');
		}


		if(\is_file('/tmp'.$path.'/'.$file.'.pdf')){
			chmod('/tmp'.$path.'/'.$file.'.pdf',0766);
			unlink('/tmp'.$path.'/'.$file.'.pdf');
		}

		//touch('/tmp'.$path.'/'.$file.'.pdf');

		//chmod('/tmp'.$path.'/'.$file.'.pdf',0766);

		if(!is_writable('/tmp'.$path.'/')){
			throw new \Exception('Cannot write the statement utility pdf file to disk');
		}

		// save the template
		file_put_contents('/tmp'.$path.'/'.$file.'.html',$html);

		if(!filesize('/tmp'.$path.'/'.$file.'.html')){
			throw new \Exception('Failed to write HTML file to disk');
        }

		// convert to pdf
		$this->convertHtmlToPdf('/tmp'.$path.'/'.$file.'.html','/tmp'.$path.'/'.$file.'.pdf',$snappy);


		if(!filesize('/tmp'.$path.'/'.$file.'.pdf')){
			throw new \Exception('Failed to write PDF file to disk');
		}

		chdir($cwd);

		// return link to file
		return '/tmp'.$file_unique_string.'.pdf';
	}

	public function convertHtmlToPdf($html_file,$pdf_file,\Knp\Snappy\Pdf $snappy)
	{

		$output = [];
		$return_var = null;

		$test_file = '/var/www/development.foreupsoftware.com/fu/reports/tryit.pdf';
		$exec_string = sprintf('wkhtmltopdf %s %s',$html_file,$pdf_file);


		if(file_exists($pdf_file)) {
			unlink($pdf_file);
		}

		try {
		    /*
			echo '<br><br>';
            echo realpath('./');
			echo '<br>';
            echo $html_file;
			echo '<br>';
            echo $pdf_file;
			echo '<br><br>';
		    //*/
			$snappy->generate($html_file, $pdf_file, [], true);
		}catch(\Exception $e){
				$html = \file_get_contents($html_file);
				$snappy->generateFromHtml($html, $pdf_file,[],true);
		}
		chmod($pdf_file,0766);

		return true;

	}

	public function mergePdfFiles(string $outputFile, array $inputFiles)
	{
		// use ghostscript to merge pdfs
		$output = [];
		$return_var = null;

		if(!count($inputFiles)){
		    if(!is_file('/tmp/'.sha1($outputFile).'.tried'))
            {
	            touch('/tmp/'.sha1($outputFile).'.tried');
                return false;
            }
		    throw new \Exception('No input files for merge. Is this really a FIFO queue?');
        }

		$exec_string = 'gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile='.$outputFile.' '.implode(' ',$inputFiles);

		$res = exec($exec_string,$output,$return_var);

		return true;
	}

	/**
	 * @param ForeupAccountStatements $statement
	 * @return array
	 */
	public function getAccountTransactions(ForeupAccountStatements $statement)
    {
	    $customer = $statement->getCustomer();
	    $repo = $this->db->getRepository('e:ForeupAccountTransactions');
	    $start = $statement->getStartDate();
	    $end = $statement->getEndDate();

	    if(is_string($start))$start=new \DateTime($start);
	    if(is_string($end))$end=new \DateTime($end);

	    $criteria = new \Doctrine\Common\Collections\Criteria();

	    $course = $customer->getCourse();
	    $course_id = $customer->getCourseId();
	    $customer->getPersonId();
	    $criteria->where($criteria->expr()->eq('customer',$customer));
	    $criteria->andWhere($criteria->expr()->andX(
		    $criteria->expr()->gt('transDate',$start),
		    $criteria->expr()->lt('transDate',$end)
	    ));
	    $criteria->orderBy(['transDate'=>'ASC']);

	    $transactions = $repo->matching($criteria);

	    $criteria2 = new \Doctrine\Common\Collections\Criteria();
	    $criteria2->where($criteria2->expr()->eq('accountType','customer'));
	    //$criteria2->orderBy(['transDate'=>'ASC']);
	    $cust_trans = $transactions->matching($criteria2);

	    $criteria3 = new \Doctrine\Common\Collections\Criteria();
	    $criteria3->where($criteria3->expr()->eq('accountType','member'));
	    //$criteria3->orderBy(['transDate'=>'ASC']);
	    $memb_trans = $transactions->matching($criteria3);

	    $criteria4 = new \Doctrine\Common\Collections\Criteria();
	    $criteria4->where($criteria4->expr()->eq('accountType','invoice'));
	    //$criteria4->orderBy(['transDate'=>'ASC']);
	    $invc_trans = $transactions->matching($criteria4);

	    return ['customer'=>$cust_trans,'member'=>$memb_trans,'invoice'=>$invc_trans];
    }

    public function getBalanceForward($statement, $type){

		$customer = $statement->getCustomer();
		$repo = $this->db->getRepository('e:ForeupAccountTransactions');
		$start = $statement->getStartDate();
		if(is_string($start)) $start = new \DateTime($start);

		$criteria = new \Doctrine\Common\Collections\Criteria();

		$criteria->andWhere($criteria->expr()->eq('transCustomer', $customer->getPersonId()));
		$criteria->andWhere($criteria->expr()->eq('courseId', $customer->getCourseId()));
		$criteria->andWhere($criteria->expr()->eq('accountType', $type));
		$criteria->andWhere($criteria->expr()->lt('transDate', $start));
		$criteria->orderBy(['transDate' => 'DESC']);
		$criteria->setMaxResults(1);

		$transactions = $repo->matching($criteria)->toArray();

		$amount = 0;
		if(!empty($transactions[0])){
			$amount = (float) $transactions[0]->getRunningBalance();
		}

		return $amount;
	}

    public function getBarcode($barcodeStr){

		define('APPPATH', '/var/www/development.foreupsoftware.com/fu/application/');

		require_once(APPPATH.'libraries/barcode/BCGFontFile.php');
		require_once(APPPATH.'libraries/barcode/BCGColor.php');
		require_once(APPPATH.'libraries/barcode/BCGDrawing.php');
		require_once(APPPATH.'libraries/barcode/BCGcode128.barcode.php');

    	$font = new \BCGFontFile(APPPATH.'libraries/barcode/font/Arial.ttf', 9);
		$color_black = new \BCGColor(0, 0, 0);
		$color_white = new \BCGColor(255, 255, 255);

		// Barcode Part
		$code = new \BCGcode128();

		$code->setScale(1);
		$code->setThickness(20);
		$code->setForegroundColor($color_black);
		$code->setBackgroundColor($color_white);
		$code->setFont($font);
		//$code->setLabel($barcodeStr);
		$code->parse($barcodeStr);

		// Drawing Part
		$drawing = new \BCGDrawing('', $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();

		ob_start();
		$drawing->finish(\BCGDrawing::IMG_FORMAT_PNG);
		$imageData = ob_get_contents();
		ob_end_clean();

		return 'data:image/png;base64,'.base64_encode($imageData);
	}

	public function html(ForeupCourses $course, ForeupCustomers $customer,
	                     ForeupAccountStatements $statement,
	                     $charges,$customerTransactions,$memberTransactions,$invoiceTransactions)
	{
		ob_start();

		$memberAccountLabel = 'Member Account';
		$customerAccountLabel = 'Pro Shop Account';

		if(!empty($course->getCustomerCreditNickname())){
			$customerAccountLabel = $course->getCustomerCreditNickname();
		}
		if(!empty($course->getMemberBalanceNickname())){
			$memberAccountLabel = $course->getMemberBalanceNickname();
		}

		$memberBalanceForward = $this->getBalanceForward($statement, 'member');
		$customerBalanceForward = $this->getBalanceForward($statement, 'customer');
		?>

		<!DOCTYPE html>
		<html>
		<head>
			<meta charset="utf-8">
			<style>
				@media print
				{
					.section {
						page-break-inside:avoid;
					}
					div.footer {page-break-after: always;}
					table {page-break-inside:avoid;}
					h3 .section-title {page-break-after: avoid;}
				}

				body {
					font-family: Helvetica, Arial, sans-serif;
					font-size: 12px;
					line-height: 14px;
				}

				table {
					width: 100%;
					margin: 0px;
				}

				table th {
					font-weight: 500;
					text-align: left;
					background-color: #59718B;
					color: white;
				}

				table th,
				table td {
					padding: 4px 8px;
					margin: 0px;
					border: none;
					vertical-align: top;
				}

				div.section {
					overflow: hidden;
					display: block;
					margin-bottom: 10px;
				}

				#course-logo {
					display: block;
					float: left;
					width: 125px;
					height: 85px;
					border: none;
					margin-right: 10px;
				}

				.course-name {
					margin-bottom: 5px;
					margin-top: 0px;
					font-size: 18px;
					line-height: 18px;
				}

				#course-info {
					box-sizing: border-box;
					width: 65%;
					float: left;
					font-size: 12px;
					padding-right: 25px;
				}

				div.address {
					text-align: left;
					float: left;
				}

				div.contact-info {
					text-align: right;
					float: right;
				}

				#customer-info {
					float: left;
					width: 400px;
					margin-left: 30px;
					margin-top: 50px;
				}

				#customer-info .customer-name {
					font-size: 16px;
					margin: 0px;
					display: block;
					float: none;
					margin-bottom: 2px;
					margin-top: 5px;
				}

				#customer-info .account-number {
					margin-bottom: 5px;
				}

				#statement-summary {
					width: 35%;
					float: right;
					display: block;
					overflow: hidden;
				}

				#statement-summary td {
					text-align: right;
				}

				table.summary {
					border: 1px solid #59718B;
				}

				table.summary td,
				table.summary th {
					border-bottom: 1px solid #59718B;
					border-top: 1px solid white;
				}

				table.summary tr:first-child th {
					border-top: none;
				}

				table.summary tr:last-child td,
				table.summary tr:last-child td {
					border-bottom: none;
				}

				table.summary tr {
					margin-bottom: 1px;
				}

				div.terms {
					font-size: 12px;
					margin-top: 10px;
				}

				div.terms h4 {
					margin-top: 0px;
					margin-bottom: 5px;
				}

				div.terms ol {
					padding: 0px 0px 0px 15px;
					margin: 0px;
				}

				h3.section-title {
					display: block;
					overflow: hidden;
					padding: 10px 0px;
					margin-bottom: 0px;
					margin-top: 0px;
					color: rgba(72, 133, 177, 0.75);
				}

				h3.section-title .left {
					float: left;
					display: block;
				}

				h3.section-title .right {
					float: right;
					display: block;
				}

				table.items {
					border-bottom: 1px solid #888888;
					border-right: 1px solid #888888;
					border-collapse: collapse;
				}

				table.items th {
					padding: 8px 8px;
				}

				table.items td {
					border-left: 1px solid #888888;
					border-right: 1px solid #888888;
					background-color: transparent;
					padding: 6px 8px;
				}

				table.items tr {
					border: none;
					padding: 0px;
					margin: 0px;
				}

				table.items tr:nth-child(even) {
					background: #DDDDDD;
				}

				.total {
					font-size: 20px;
					line-height: 20px;
					margin: 0px 0px 0px;
					text-align: left;
					padding: 0px !important;
				}

				div.footer {
					display: block;
					overflow: hidden;
					text-align: center;
					border-top: 2px dashed #E0E0E0;
					padding: 25px;
					font-size: 12px;
					margin-top: 50px;
				}

				#customer-message {
					min-height: 150px;
					max-width: 500px;
					min-width: 400px;
					display: block;
					float: left;
					margin-top: 0px;
				}

				#document-label {
					display: block;
					margin: 0px 0px 5px 0px;
					color: #888;
					text-transform: uppercase;
					font-size: 30px;
					line-height: 30px;
					color: rgba(72, 133, 177, 0.5);
					text-align: right;
				}

				img.barcode {
					display: block;
					float: right;
					margin-bottom: 25px
				}

				.tax-rate {
					width: 60px;
					text-align: right;
				}

				.amount {
					width: 85px;
					text-align: right;
				}

				.quantity {
					width: 40px;
					text-align: left;
				}

				.date {
					width: 65px;
				}

				#totals {
					width: 35%;
					float: right;
					display: block;
				}

				table.totals {
					border-collapse: collapse;
					border: 1px solid #59718B;
				}

				table.totals td,
				table.totals th {
					border-bottom: 1px solid #59718B;
				}

				table.totals .value {
					text-align: right;
				}

				.statement-message {
					border: 1px solid #59718B;
					background: #F0F0F0;
					padding: 8px;
				}
			</style>
		</head>
		<body>
		<div class="section" style="margin-top: 35px">
			<div id="statement-summary">
				<h1 id="document-label">Statement</h1>
				<img class="barcode" src="<?php echo $this->getBarcode('STA '.$statement->getNumber()); ?>">
				<table cellspacing="0" class="summary">
					<tbody>
					<tr>
						<th>Bill Date</th>
						<td style="width: 125px"><?php echo $statement->getDateCreated()->format(self::$dateFormat); ?></td>
					</tr>
					<!-- <tr>
							<th>Start Date</th>
							<td><?php echo $statement->getStartDate()->format(self::$dateFormat); ?></td>
						</tr>
						<tr>
							<th>End Date</th>
							<td><?php echo $statement->getEndDate()->format(self::$dateFormat); ?></td>
						</tr> -->
					<tr>
						<th>Statement #</th>
						<td><?php echo $statement->getNumber(); ?></td>
					</tr>
					<tr>
						<td>Prev. Balance</td>
						<td><?php echo money_format('%.2n', (0 - $customer->getInvoiceBalance())); ?></td>
					</tr>
					<tr>
						<td>Payments</td>
						<td><?php echo money_format('%.2n', $statement->getTotalPaid()); ?></td>
					</tr>
					<tr>
						<td>New Charges</td>
						<td><?php echo money_format('%.2n', $statement->getTotal()); ?></td>
					</tr>
					<tr>
						<th style="border-top: none;"><h2 class="total">Total Due</h2></th>
						<td style="border-top: none;">
							<h2 class="total" style="float: right">
								<?php
								$totalDue = $statement->getTotalOpen() + (0 - $customer->getInvoiceBalance());
								echo money_format('%.2n', $totalDue); ?>
							</h2>
						</td>
					</tr>
					<tr>
						<th><em>Due On</em></th>
						<td><?php echo $statement->getDueDate()->format(self::$dateFormat); ?></td>
					</tr>
					</tbody>
				</table>
				<div class="terms">
					<?php if(!empty($statement->getTermsAndConditionsText())){ ?>
					<div class="statement-message">
						<h4>Terms & Conditions</h4>
						<?php echo $statement->getTermsAndConditionsText(); ?>
					</div>
					<?php } ?>
				</div>
			</div>
			<div id="course-info">
				<img id="course-logo" src="<?php echo $this->app['config.legacy_url'];?>/app_files/view/<?php echo $course->getCompanyLogo(); ?>">
				<h3 class="course-name"><?php echo $course->getName(); ?></h3>
				<div class="address">
					<?php echo $course->getAddress(); ?><br>
					<?php echo $course->getCity(); ?>,
					<?php echo $course->getState(); ?><br>
					<?php echo $course->getZip(); ?>
				</div>
				<div class="contact-info">
					<?php echo empty($course->getBillingEmail()) ? $course->getBillingEmail() : $course->getEmail(); ?>
					<br>
					<?php echo $course->getWebsite() ?><br>
					<?php echo $course->getPhone(); ?>
				</div>
			</div>
			<div id="customer-info">
				<table>
					<thead>
					<tr>
						<th>Bill To</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>
							<h2 class="customer-name"><?php echo $customer->getPerson()->getFirstName() . ' ' . $customer->getPerson()->getLastName() ?></h2>
							<div class="account-number">
								<strong>Acct: </strong>
								<?php if(empty($customer->getAccountNumber())){ ?>
									<span style="color: #aaa">N/A</span>
								<?php }else{ ?>
									<?php echo $customer->getAccountNumber(); ?>
								<?php } ?>
							</div>
							<div class="address">
								<?php echo $customer->getPerson()->getAddress1(); ?><br>
								<?php echo $customer->getPerson()->getAddress2() ? $customer->getPerson()->getAddress2 . '<br>' : ''; ?>
								<?php echo $customer->getPerson()->getCity(); ?><?php if(!empty($customer->getPerson()->getCity()) && !empty($customer->getPerson()->getState())){ ?>, <?php } ?>
								<?php echo $customer->getPerson()->getState(); ?>
								<?php echo $customer->getPerson()->getZip(); ?>
							</div>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="section">
			<h3 class="section-title">
				<span class="left">
					New Charges (<?php echo $statement->getStartDate()->format(self::$dateFormat); ?> to
					<?php echo $statement->getEndDate()->format(self::$dateFormat); ?>)
				</span>
			</h3>
			<table class="items" cellspacing="0">
				<thead>
				<tr>
					<th style="width: 25px;">&nbsp;</th>
					<th class="date">Date</th>
					<th>Item</th>
					<th class="quantity">Qty</th>
					<th class="amount">Price</th>
					<th class="tax-rate">Tax Rate</th>
					<th class="amount">Total</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$taxes = 0.00;
				$subtotal = 0.00;

				if(!$charges->isEmpty()){
					foreach ($charges as $charge) {
						$subtotal += $charge->getSubtotal();
						$taxes += $charge->getTax();
						?>
						<tr>
							<td><?php echo $charge->getLine(); ?></td>
							<td class="date"><?php echo $charge->getDateCharged()->format(self::$dateFormat); ?></td>
							<td><?php echo $charge->getName(); ?></td>
							<td class="quantity"><?php echo $charge->getQty(); ?></td>
							<td class="amount"><?php echo money_format('%.2n', $charge->getUnitPrice()); ?></td>
							<td class="tax-rate"><?php echo $charge->getTaxPercentage(); ?>%</td>
							<td class="amount"><?php echo money_format('%.2n', $charge->getTotal()); ?></td>
						</tr>
					<?php } }else{ ?>
					<tr>
						<td colspan="7" style="text-align: center; color: #666;"><em>No New Charges</em></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>

		<?php if(!empty($statement->getPayments())){ ?>
			<div class="section">
				<h3 class="section-title">
				<span class="left">
					Payments
				</span>
				</h3>
				<table class="items" cellspacing="0">
					<thead>
					<tr>
						<th style="width: 25px;">&nbsp;</th>
						<th class="date">Date</th>
						<th>Payment Method</th>
						<th class="amount">Amount</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($statement->getPayments() as $line => $payment){ ?>
						<tr>
							<td style="width: 25px"><?php echo $line + 1; ?></td>
							<td><?php echo $payment->getDateCreated()->format(self::$dateFormat); ?></td>
							<td><?php echo $payment->getReadableType(); ?></td>
							<td class="amount"><?php echo money_format('%.2n', $payment->getAmount()); ?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		<?php } ?>

		<div class="section">
			<div id="customer-message">
				<?php if(!empty($statement->getMessageText())){ ?>
				<div class="statement-message">
					<?php echo $statement->getMessageText(); ?>
				</div>
				<?php } ?>
			</div>
			<div id="totals">
				<table cellspacing="0" class="totals">
					<tr>
						<td class="label">Subtotal</td>
						<td class="value" style="width: 125px"><?php echo money_format('%.2n', $subtotal); ?></td>
					</tr>
					<tr>
						<td class="label">Taxes</td>
						<td class="value"><?php echo money_format('%.2n', $taxes); ?></td>
					</tr>
					<tr>
						<td class="label">Payments</td>
						<td class="value"><?php echo money_format('%.2n', $statement->getTotalPaid()); ?></td>
					</tr>
					<tr>
						<th class="label"><strong>Total</strong></th>
						<td class="value"><strong><?php echo money_format('%.2n', $statement->getTotalOpen()); ?></strong></td>
					</tr>
					<tr>
						<td class="label"><em>Due On</em></td>
						<td class="value"><?php echo $statement->getDueDate()->format(self::$dateFormat); ?></td>
					</tr>
				</table>
			</div>
		</div>

		<?php if($statement->getIncludeMemberTransactions()){
			$currentRunningBalance = $memberBalanceForward;
			?>
			<div class="section">
				<h3 class="section-title">
					<?php echo $memberAccountLabel; ?> (<?php echo $statement->getStartDate()->format(self::$dateFormat); ?> to
					<?php echo $statement->getEndDate()->format(self::$dateFormat); ?>)
				</h3>
				<table cellspacing="0" class="items">
					<thead>
					<tr>
						<th>Date</th>
						<th>Transaction</th>
						<th>Details</th>
						<th>Debit</th>
						<th>Credit</th>
						<th>Balance</th>
					</tr>
					</thead>
					<tbody>
					<?php if(!$memberTransactions->isEmpty()){ ?>
						<tr>
							<td colspan="5" style="text-align: center"><em>Balance Forward</em></td>
							<td class="amount"><?php echo money_format('%.2n', $memberBalanceForward); ?></td>
						</tr>
						<?php foreach($memberTransactions as $transaction){
							$currentRunningBalance = $transaction->getRunningBalance();
							?>
							<tr>
								<td class="date"><?php echo $transaction->getTransDate()->format(self::$dateFormat); ?></td>
								<td>
									<?php echo $transaction->getTransComment(); ?>
								</td>
								<td>
									<?php if(!empty($transaction->getSaleId()) && $transaction->getSale()->getItems()){
										foreach($transaction->getSale()->getItems() as $item){ ?>
											(<?php echo $item->getQuantityPurchased(); ?>) <?php echo $item->getItem()->getName(); ?> <?php echo money_format('%.2n', $item->getTotal()); ?><br/>
										<?php } ?>
									<?php }else{ ?>
										<?php echo $transaction->getTransDescription(); ?>
									<?php } ?>
								</td>
								<td class="amount">
									<?php
									if($transaction->getTransAmount() < 0){
										echo money_format('%.2n', abs($transaction->getTransAmount()));
									} ?>
								</td>
								<td class="amount">
									<?php
									if($transaction->getTransAmount() >= 0){
										echo money_format('%.2n', abs($transaction->getTransAmount()));
									} ?>
								</td>
								<td class="amount">
									<?php echo money_format('%.2n', $transaction->getRunningBalance()); ?>
								</td>
							</tr>
						<?php } }else{ ?>
						<tr>
							<td colspan="6" style="text-align: center; color: #666;">
								<em>
									No Transactions (<?php echo $statement->getStartDate()->format(self::$dateFormat); ?> to
									<?php echo $statement->getEndDate()->format('n/j/Y'); ?>)
								</em>
							</td>
						</tr>
					<?php } ?>
					<tr>
						<th style="text-align: right;" colspan="5"><em>Balance as of <?php echo $statement->getEndDate()->format(self::$dateFormat); ?></em></th>
						<th class="amount"><strong><?php echo money_format('%.2n', $currentRunningBalance); ?></strong></th>
					</tr>
					</tbody>
				</table>
			</div>
		<?php } ?>

		<?php if($statement->getIncludeCustomerTransactions()){
			$currentRunningBalance = $customerBalanceForward;
			?>
			<div class="section">
				<h3 class="section-title">
					<?php echo $customerAccountLabel; ?> (<?php echo $statement->getStartDate()->format(self::$dateFormat); ?> to
					<?php echo $statement->getEndDate()->format('n/j/Y'); ?>)
				</h3>
				<table cellspacing="0" class="items">
					<thead>
					<tr>
						<th>Date</th>
						<th>Transaction</th>
						<th>Details</th>
						<th>Debit</th>
						<th>Credit</th>
						<th>Balance</th>
					</tr>
					</thead>
					<tbody>
					<?php if(!$customerTransactions->isEmpty()){ ?>
						<tr>
							<td colspan="5" style="text-align: center"><em>Balance Forward</em></td>
							<td class="amount"><?php echo money_format('%.2n', $customerBalanceForward); ?></td>
						</tr>
					<?php foreach($customerTransactions as $transaction){
						$currentRunningBalance = $transaction->getRunningBalance();
						?>
						<tr>
							<td class="date"><?php echo $transaction->getTransDate()->format(self::$dateFormat); ?></td>
							<td>
								<?php echo $transaction->getTransComment(); ?>
							</td>
							<td>
								<?php if(!empty($transaction->getSaleId()) && $transaction->getSale()->getItems()){
									foreach($transaction->getSale()->getItems() as $item){ ?>
										(<?php echo $item->getQuantityPurchased(); ?>) <?php echo $item->getItem()->getName(); ?> <?php echo money_format('%.2n', $item->getTotal()); ?><br/>
									<?php } ?>
								<?php }else{ ?>
									<?php echo $transaction->getTransDescription(); ?>
								<?php } ?>
							</td>
							<td class="amount">
								<?php
								if($transaction->getTransAmount() < 0){
									echo money_format('%.2n', abs($transaction->getTransAmount()));
								} ?>
							</td>
							<td class="amount">
								<?php
								if($transaction->getTransAmount() >= 0){
									echo money_format('%.2n', abs($transaction->getTransAmount()));
								} ?>
							</td>
							<td class="amount">
								<?php echo money_format('%.2n', $transaction->getRunningBalance()); ?>
							</td>
						</tr>
						<?php } }else{ ?>
						<tr>
							<td colspan="6" style="text-align: center; color: #666;">
								<em>
									No Transactions (<?php echo $statement->getStartDate()->format(self::$dateFormat); ?> to
									<?php echo $statement->getEndDate()->format('n/j/Y'); ?>)
								</em>
							</td>
						</tr>
					<?php } ?>
					<tr>
						<th style="text-align: right;" colspan="5"><em>Balance as of <?php echo $statement->getEndDate()->format(self::$dateFormat); ?></em></th>
						<th class="amount"><strong><?php echo money_format('%.2n', $currentRunningBalance); ?></strong></th>
					</tr>
					</tbody>
				</table>
			</div>
		<?php } ?>

		<div class="footer">
			<?php if(!empty($statement->getFooterText())){ ?>
				<?php echo $statement->getFooterText(); ?>
			<?php } ?>
		</div>
		</body>
		</html>

		<?php
		$html = ob_get_clean();
		return $html;
	}
}
?>