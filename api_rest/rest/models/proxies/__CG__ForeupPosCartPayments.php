<?php

namespace DoctrineProxies\__CG__;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ForeupPosCartPayments extends \ForeupPosCartPayments implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'paymentId', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'cartId', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'amount', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'type', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'description', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'number', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'recordId', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'params'];
        }

        return ['__isInitialized__', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'paymentId', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'cartId', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'amount', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'type', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'description', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'number', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'recordId', '' . "\0" . 'ForeupPosCartPayments' . "\0" . 'params'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ForeupPosCartPayments $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getPaymentId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getPaymentId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPaymentId', []);

        return parent::getPaymentId();
    }

    /**
     * {@inheritDoc}
     */
    public function setPaymentId($paymentId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPaymentId', [$paymentId]);

        return parent::setPaymentId($paymentId);
    }

    /**
     * {@inheritDoc}
     */
    public function getCartId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCartId', []);

        return parent::getCartId();
    }

    /**
     * {@inheritDoc}
     */
    public function setCartId($cartId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCartId', [$cartId]);

        return parent::setCartId($cartId);
    }

    /**
     * {@inheritDoc}
     */
    public function getAmount()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAmount', []);

        return parent::getAmount();
    }

    /**
     * {@inheritDoc}
     */
    public function setAmount($amount)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAmount', [$amount]);

        return parent::setAmount($amount);
    }

    /**
     * {@inheritDoc}
     */
    public function getType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getType', []);

        return parent::getType();
    }

    /**
     * {@inheritDoc}
     */
    public function setType($type)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setType', [$type]);

        return parent::setType($type);
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDescription', []);

        return parent::getDescription();
    }

    /**
     * {@inheritDoc}
     */
    public function setDescription($description)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDescription', [$description]);

        return parent::setDescription($description);
    }

    /**
     * {@inheritDoc}
     */
    public function getNumber()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNumber', []);

        return parent::getNumber();
    }

    /**
     * {@inheritDoc}
     */
    public function setNumber($number)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNumber', [$number]);

        return parent::setNumber($number);
    }

    /**
     * {@inheritDoc}
     */
    public function getRecordId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRecordId', []);

        return parent::getRecordId();
    }

    /**
     * {@inheritDoc}
     */
    public function setRecordId($recordId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRecordId', [$recordId]);

        return parent::setRecordId($recordId);
    }

    /**
     * {@inheritDoc}
     */
    public function getParams()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getParams', []);

        return parent::getParams();
    }

    /**
     * {@inheritDoc}
     */
    public function setParams($params)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setParams', [$params]);

        return parent::setParams($params);
    }

}
