<?php
namespace foreup\rest\resource_transformers;

use League\Fractal;
use foreup\rest\models\entities\ForeupTeesheetHoles;

class teesheet_hole_transformer extends Fractal\TransformerAbstract
{
	public function transform(ForeupTeesheetHoles $hole)
	{
		return [
			'id' => $hole->getTeeSheetHoleId(),
			'number' => (int)$hole->getHoleNumber(),
			'par' => (int)$hole->getPar(),
			'handicap' => (int)$hole->getHandicap(),
			'pro_tip' => $hole->getProTip(),
			'gps' => [
				'tee_box' => [
					'longitude' => $hole->getTeeBoxLong(),
					'latitude' => $hole->getTeeBoxLat()
				],
				'mid_point' => [
					'longitude' => $hole->getMidPointLong(),
					'latitude' => $hole->getMidPointLat()
				],
				'hole' => [
					'longitude' => $hole->getHoleLong(),
					'latitude' => $hole->getHoleLat()
				],
				'green_front' => [
					'longitude' => $hole->getGreenFrontLong(),
					'latitude' => $hole->getGreenFrontLat()
				],
				'green_back' => [
					'longitude' => $hole->getGreenBackLong(),
					'latitude' => $hole->getGreenBackLat()
				]
			]
		];
	}
}