<?php
namespace foreup\rest\resource_transformers;

use Carbon\Carbon;
use foreup\rest\models\entities\ForeupPriceClass;
use League\Fractal;

class price_class_transformer extends Fractal\TransformerAbstract
{

	public function transform(ForeupPriceClass $entry)
	{
		return [
			'id' => $entry->getId(),
			'name' => $entry->getName(),
			'color' => $entry->getColor(),
			'dateCreated' => !empty($entry->getDateCreated())?$entry->getDateCreated()->format(\DateTime::ISO8601):null,
            'default' => $entry->getDefault(),
            'cart' => $entry->getCart(),
            'cartGlCode' => $entry->getCartGlCode(),
            'greenGlCode' => $entry->getGreenGlCode(),
            'isShared' => $entry->getIsShared()
		];
	}


}