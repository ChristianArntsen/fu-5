<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupTeetime;
use League\Fractal;
use foreup\rest\models\entities\ForeupTeesheet;

class teetime_transformer extends versioned_transformer
{
	protected $availableIncludes = [
		'players','sales','bookedPlayers'
	];
	public function transform(ForeupTeetime $teetime)
	{
		if($this->version == 1){
			return $this->transformv1($teetime);
		} else {
			return $this->transformv2($teetime);
		}
	}
	public function transformv2(ForeupTeetime $teetime)
	{
		$toReturn = $this->transformv1($teetime);
		$toReturn['lastUpdated'] = isset($toReturn['lastUpdated'])?$toReturn['lastUpdated']->toIso8601String():null;
		$toReturn['dateBooked'] = isset($toReturn['dateBooked'])?$toReturn['dateBooked']->toIso8601String():null;
		$toReturn['dateCancelled'] = isset($toReturn['dateCancelled'])?$toReturn['dateCancelled']->toIso8601String():null;
		$toReturn['teedOffTime'] = isset($toReturn['teedOffTime'])?$toReturn['teedOffTime']->toIso8601String():null;
		$toReturn['turnTime'] = isset($toReturn['turnTime'])?$toReturn['turnTime']->toIso8601String():null;
		$toReturn['finishTime'] = isset($toReturn['finishTime'])?$toReturn['finishTime']->toIso8601String():null;
		$toReturn['start'] = isset($toReturn['start'])?$toReturn['start']->toIso8601String():null;
		$toReturn['end'] = isset($toReturn['end'])?$toReturn['end']->toIso8601String():null;

		return $toReturn;
	}
	public function transformv1(ForeupTeetime $teetime)
	{
		return [
			'id' => $teetime->getTtid(),
			'isReround' => (bool)$teetime->getReround(),
			'type' => $teetime->getType(),
			'status' => $teetime->getStatus(),
			'start' => $teetime->getStartDatetime(),
			'end' => $teetime->getEndDatetime(),
			'duration' => (integer)$teetime->getDuration(),
			'playerCount' => (integer)$teetime->getPlayerCount(),
			'holes' => (integer)$teetime->getHoles(),
			'carts' => (integer)$teetime->getCarts(),
			'cart1' => (integer)$teetime->getCartNum1(),
			'cart2' => (integer)$teetime->getCartNum2(),
			'paidPlayerCount' => (integer)$teetime->getPaidPlayerCount(),
			'noShowCount' => (integer)$teetime->getNoShowCount(),
			'title' => $teetime->getTitle(),
			'details' => $teetime->getDetails(),
			'side' => $teetime->getSide(),
			'bookingSource' => $teetime->getBookingSource(),
			'lastUpdated' => $teetime->getLastUpdated(),
			'dateBooked' => $teetime->getDateBooked(),
			'dateCancelled' => $teetime->getDateCancelled(),
			'teedOffTime' => $teetime->getTeedOffTime(),
			'turnTime' => $teetime->getTurnTime(),
			'finishTime' => $teetime->getFinishTime()
		];
	}
	public function includeBookedPlayers(ForeupTeetime $teetime)
	{
		return $this->collection($teetime->getBookedPlayers(), new booked_player_transformer(), 'bookedPlayer');
	}

	public function includePlayers(ForeupTeetime $teetime)
	{
		$players = $teetime->getPlayers();

		return $this->collection($players, new person_transformer(), 'people');
	}
	public function includeSales(ForeupTeetime $teetime)
	{
		$players = $teetime->getSales();

		return $this->collection($players, new sales_transformer(), 'sales');
	}
}