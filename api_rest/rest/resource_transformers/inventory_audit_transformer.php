<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupInventoryAudits;
use foreup\rest\models\entities\ForeupInventoryAuditItems;
use League\Fractal;
use foreup\rest\models\entities\ForeupCourses;


class inventory_audit_transformer extends Fractal\TransformerAbstract
{
    /**
     * @var array
     */
    protected $availableIncludes = [

    ];

    /**
     * @var array
     */
    protected $defaultIncludes = [
        'items'
    ];

    public function transform(ForeupInventoryAudits $inventoryAudit)
    {
        return [
            'id'=>(float)$inventoryAudit->getInventoryAuditId(),
            'courseId'=>(int)$inventoryAudit->getCourseId(),
            'employeeId'=>(int)$inventoryAudit->getEmployeeId(),
            'date'=>$inventoryAudit->getDate()
        ];
    }

    /**
     * @param ForeupInventoryAudits|null $audit
     * @return Fractal\Resource\Collection
     */
    public function includeItems(ForeupInventoryAudits $audit = null)
    {
        if(!isset($audit)) return null;
        $items = $audit->getInventoryAuditItems();
        if(!isset($items))return null;

        return $this->collection($items, new inventory_audit_items_transformer(), 'inventoryAuditItem');
    }

}