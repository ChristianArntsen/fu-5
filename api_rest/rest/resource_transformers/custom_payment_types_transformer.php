<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupCustomPaymentTypes;
use League\Fractal;

class custom_payment_types_transformer extends Fractal\TransformerAbstract
{

	public function transform(ForeupCustomPaymentTypes $type)
    {
	    return [
		    'id'=>$type->getId(),
		    'label'=>$type->getLabel(),
		    'customPaymentType'=>$type->getCustomPaymentType()
	    ];
    }

}