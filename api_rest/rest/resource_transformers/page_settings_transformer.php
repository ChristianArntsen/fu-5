<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupPageSettings;
use League\Fractal;
class page_settings_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		"courses"
	];

	public function transform(ForeupPageSettings $settings)
	{
		return [
			'id' => $settings->getPage(),
			'page_size' => $settings->getPageSize(),
			'columns' => $settings->getColumns(),
		];
	}

}