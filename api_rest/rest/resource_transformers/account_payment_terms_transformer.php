<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupAccountPaymentTerms;
use League\Fractal;
class account_payment_terms_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
	];

	/**
	 * @param ForeupAccountPaymentTerms|null $terms
	 * @return array
	 */
	public function transform(ForeupAccountPaymentTerms $terms = null)
	{
		if(!isset($terms))return [];
		return [
			'id' => (int)$terms->getId(),
			'name' => $terms->getName(),
			'date_created' => !empty($terms->getDateCreated())?$terms->getDateCreated()->format(\DateTime::ISO8601):null,
			'past_due_days' => $terms->getPastDueDays(),
			'past_due_fee_percentage' => $terms->getPastDueFeePercentage(),
			'memo' => $terms->getMemo(),
			'organization_id' => $terms->getOrganizationId()
		];
	}
}
?>