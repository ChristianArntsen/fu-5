<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupSeasonalTimeframes;
use foreup\rest\models\entities\ForeupSeasons;
use League\Fractal;
use foreup\rest\models\entities\ForeupTeesheet;

class teesheet_seasonal_timeframes_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'classes','seasons'
	];

	public function transform(ForeupSeasonalTimeframes $timeframe)
	{
		return [
			'id' => (int)$timeframe->getTimeframeId(),
			'name' => (string)$timeframe->getTimeframeName(),
			'monday' => (boolean)$timeframe->getMonday(),
			'tuesday' => (boolean)$timeframe->getTuesday(),
			'wednesday' => (boolean)$timeframe->getWednesday(),
			'thursday' => (boolean)$timeframe->getThursday(),
			'friday' =>(boolean) $timeframe->getFriday(),
			'saturday' => (boolean)$timeframe->getSaturday(),
			'sunday' => (boolean)$timeframe->getSunday(),
			'9holePrice' => (float)$timeframe->get9holePrice(),
			'9cartPrice' => (float)$timeframe->get9cartPrice(),
			'18holePrice' => (float)$timeframe->get18holePrice(),
			'18cartPrice' => (float)$timeframe->get18cartPrice(),
			'startTime' => (int)$timeframe->getStartTime(),
			'endTime' => (int)$timeframe->getEndTime(),
			'default' => (boolean)$timeframe->getDefault(),
		];
	}

	public function includeClasses(ForeupSeasonalTimeframes $timeframe)
	{
		return $this->item($timeframe->getClass(), new price_class_transformer(), 'classes');
	}
	public function includeSeasons(ForeupSeasonalTimeframes $timeframe)
	{
		return $this->item($timeframe->getSeason(), new teesheet_seasons_transformer(), 'seasons');
	}
}