<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupPosCart;
use foreup\rest\models\entities\ForeupPosCartItems;
use foreup\rest\models\entities\ForeupSales;
use foreup\rest\models\entities\ForeupTerminals;
use League\Fractal;
use foreup\rest\models\entities\ForeupCourses;

class cart_item_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'items'
	];

    public function transform(ForeupPosCartItems $item)
    {
	    return [
		    'id' => $item->getLine(),
		    'discountPercent'=>$item->getDiscountPercent(),
		    'quantity'=>$item->getQuantity(),
		    'line'=>$item->getLine(),
		    'unitPrice'=>$item->getUnitPrice(),
		    'priceIncludesTax'=>$item->isUnitPriceIncludesTax(),
		    'itemType'=>$item->getItemType(),
		    'itemId'=>$item->getItem()->getItemId(),
	    ];
    }

	public function includeItems(ForeupPosCartItems $cartItem)
	{
		return $this->item($cartItem->getItem(), new items_transformer(),'items');
	}
}