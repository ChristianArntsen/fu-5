<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupPosCart;
use League\Fractal;

class cart_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'cartItems'
	];

    public function transform(ForeupPosCart $cart)
    {
    	$formattedObject = [];
		$formattedObject['total']=$cart->getTotal();
	    $formattedObject['totalDue']=$cart->getTotalDue();
	    $formattedObject['tax']=$cart->getTax();
	    $formattedObject['subTotal']=$cart->getSubtotal();
	    $formattedObject["id"] = $cart->getCartId();
	    return $formattedObject;
    }

	public function includeCartItems(ForeupPosCart $cart)
	{

		return $this->collection($cart->getItems(), new cart_item_transformer(),'cart_items');
	}
}