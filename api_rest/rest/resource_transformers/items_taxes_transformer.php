<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupAccountRecurringChargeItems;
use foreup\rest\models\entities\ForeupItemsTaxes;
use foreup\rest\models\entities\ForeupRepeatable;
use League\Fractal;
class items_taxes_transformer extends Fractal\TransformerAbstract
{


	public function transform(ForeupItemsTaxes $tax)
	{
		return [
			'id' => $tax->getCourseId() . '_' . $tax->getItem()->getItemId() . '_' . $tax->getName() . '_' . $tax->getPercent(),
			'courseId' => $tax->getCourseId(),
			'cid' => $tax->getCid(),
			'itemId' => $tax->getItem()->getItemId(),
			'name' => $tax->getName(),
			'percent' => $tax->getPercent(),
			'cumulative' => $tax->getCumulative()
		];
	}
}