<?php
namespace foreup\rest\resource_transformers;

use League\Fractal;
use foreup\rest\models\entities\ForeupMobileAppModules;

class module_transformer extends Fractal\TransformerAbstract
{
	public function transform(ForeupMobileAppModules $module)
	{
		return [
			'id' => (int)$module->getMobileAppModuleId(),
			'label' => $module->getLabel(),
			'url' => $module->getRenderedUrl(),
			'position' => $module->getPosition(),
			'icon_url' => $module->getIconUrl()
		];
	}
}