<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupCustomers;
use League\Fractal;

class field_transformer extends Fractal\TransformerAbstract {

    public function transform($entry)
    {
    	if(is_object($entry)){
    		throw new InvalidArgumentException("Field must be a string type");
	    }
	    return [
            'id' => $entry['title'],
		    'name'=>$entry['title'],
		    'type'=>$entry['type'],
		    'label'=>$entry['label'],
			'possibleValues'=>isset($entry['possibleValues'])?$entry['possibleValues']:null
        ];
    }

}