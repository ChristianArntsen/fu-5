<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupApiHooks;
use League\Fractal;

class api_hooks extends Fractal\TransformerAbstract
{
	public function transform(ForeupApiHooks $hook)
	{
		return [
			'id' => (int)$hook->getId(),
			'url' => $hook->getHookUrl(),
			'event' => $hook->getEvent()
		];
	}
}