<?php
namespace foreup\rest\resource_transformers;

use League\Fractal;
use foreup\rest\models\entities\ForeupPersonAltered;

class person_altered_transformer extends Fractal\TransformerAbstract
{


    public function transform($course)
    {
        if(is_array($course)){
            return $this->transform_array($course);
        }elseif($course instanceof ForeupPersonAltered){
            return $this->transform_ForeupPersonAltered($course);
        }elseif(is_object($course)){
            return $this->transform_object($course);
        }
    }

    public function transform_ForeupPersonAltered(ForeupPersonAltered $entry)
    {
        return [
            'id' => (int)$entry->getId(),
            'employee_audit_log_id'=>$entry->getEmployeeAuditLog()->getId(),
            'editor_id'=>(int)$entry->getEmployeeAuditLog()->getEditor()->getId(),
            'gmt_logged'=>$entry->getEmployeeAuditLog()->getGmtLogged(),
            'person_id'=>$entry->getEmployeeAuditLog()->getEditor()->getPerson()->getPersonId(),
            'course_id'=>$entry->getEmployeeAuditLog()->getEditor()->getCourseId(),
            'username'=>$entry->getEmployeeAuditLog()->getEditor()->getUsername(),
            'action_type_id'=>$entry->getEmployeeAuditLog()->getActionType()->getId(),
            'action_type_name'=>$entry->getEmployeeAuditLog()->getActionType()->getName(),
            'action_type_desc'=>$entry->getEmployeeAuditLog()->getActionType()->getLongDescription(),
            'person_edited'=>$entry->getPersonEdited()->getPersonId(),
            'tables_updated'=>$entry->getTablesUpdated()
        ];
    }

    public function transform_array(array $entry)
    {
        return [
            'id' => (int)$entry['id'],
            'employee_audit_log_id'=>$entry['employee_audit_log'],
            'person_edited'=>$entry['person_edited'],
            'tables_updated'=>$entry['tables_edited']
        ];
    }
/*
    public function transform_object($entry)
    {
        return [

        ];
    }
*/
}

?>