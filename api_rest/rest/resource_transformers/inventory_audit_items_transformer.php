<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupInventoryAuditItems;
use foreup\rest\models\entities\ForeupInventoryAudits;
use League\Fractal;
class inventory_audit_items_transformer extends Fractal\TransformerAbstract
{


    public function transform(ForeupInventoryAuditItems $auditItems)
    {
        return [
            'id' => $auditItems->getId(),//. '_' . $auditItems->getItem()->getItemId(),
            'name' => $auditItems->getItem()->getName(),
            'currentCount' => $auditItems->getCurrentCount(),
            'manualCount' => $auditItems->getManualCount(),
            'item_number' => $auditItems->getItem()->getItemNumber(),
            'cost_price' => $auditItems->getItem()->getCostPrice(),
            'unit_price' => $auditItems->getItem()->getUnitPrice(),
            'department' => $auditItems->getItem()->getDepartment(),
            'category' => $auditItems->getItem()->getCategory()
        ];
    }
}