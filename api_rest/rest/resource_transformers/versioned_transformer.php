<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupTeetime;
use League\Fractal;
use foreup\rest\models\entities\ForeupTeesheet;

class versioned_transformer extends Fractal\TransformerAbstract
{
	protected $version = 0;

	/**
	 * @return int
	 */
	public function getVersion()
	{
		return $this->version;
	}

	/**
	 * @param int $version
	 */
	public function setVersion($version)
	{
		$this->version = $version;
	}


}