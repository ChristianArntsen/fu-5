<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupAccountRecurringChargeCustomers;
use League\Fractal;

class account_recurring_charge_customers_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		'customer', 'recurring_charge'

	];

	public function transform(ForeupAccountRecurringChargeCustomers $customer)
	{
		return [
			'id' => (int)$customer->getId(),
			'date_added' => !empty($customer->getDateAdded()) ? $customer->getDateAdded()->format(\DateTime::ISO8601) : null,
			'person_id' => $customer->getCustomer()->getPerson()->getPersonId(),
			'recurring_charge_id' => $customer->getRecurringCharge()->getId(),
			'course_id' => $customer->getRecurringCharge()->getOrganizationId(),
			'organization_id' => $customer->getRecurringCharge()->getOrganizationId()

		];
	}

	/**
	 * includeRecurringCharge
	 *
	 * @param ForeupAccountRecurringChargeCustomers|null $customer
	 * @return Fractal\Resource\Item|null
	 */
	public function includeRecurringCharge(ForeupAccountRecurringChargeCustomers $customer = null)
	{
		if(!isset($customer))return null;
		$rep = $customer->getRecurringCharge();

		return $this->item($rep, new account_recurring_charges_transformer(), 'accountRecurringCharges');

	}

	/**
	 * includeCustomer
	 *
	 * @param ForeupAccountRecurringChargeCustomers|null $customer
	 * @return Fractal\Resource\Item|null
	 */
	public function includeCustomer(ForeupAccountRecurringChargeCustomers $customer = null)
	{
		if(!isset($customer))return null;
		$rep = $customer->getCustomer();

		return $this->item($rep, new customer_transformer(), 'customers');

	}
}

?>