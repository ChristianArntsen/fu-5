<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupLoyaltyRates;
use foreup\rest\models\entities\ForeupLoyaltyPackages;
use League\Fractal;

class loyalty_rates_transformer extends Fractal\TransformerAbstract
{
    public function transform(ForeupLoyaltyRates $loyaltyRate)
    {
        return [
            'id'=>$loyaltyRate->getId(),
            'label'=>$loyaltyRate->getLabel(),
            'type'=>$loyaltyRate->getType(),
            'value'=>$loyaltyRate->getValue(),
            'valueLabel'=>$loyaltyRate->getValueLabel(),
            'pointsPerDollar'=>$loyaltyRate->getPointsPerDollar(),
            'dollarsPerPoint'=>$loyaltyRate->getDollarsPerPoint(),
            'priceCategory'=>$loyaltyRate->getPriceCategory(),
            'teeTimeIndex'=>$loyaltyRate->getTeeTimeIndex()
        ];
    }
}