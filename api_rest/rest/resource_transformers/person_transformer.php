<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use League\Fractal;

class person_transformer extends Fractal\TransformerAbstract {



    public function transform(ForeupPeople $entry)
    {
        return [
            'id' => (string) $entry->getPersonId(),
            'first_name' => $entry->getFirstName(),
            'last_name' => $entry->getLastName(),
            'phone_number' => $entry->getPhoneNumber(),
            'cell_phone_number' => $entry->getCellPhoneNumber(),
            'email' => $entry->getEmail(),
            'birthday' => !empty($entry->getBirthday())?$entry->getBirthday()->format(\DateTime::ISO8601):null,
            'address_1' => $entry->getAddress1(),
            'address_2' => $entry->getAddress2(),
            'city' => $entry->getCity(),
            'state' => $entry->getState(),
            'zip' => $entry->getZip(),
            'country' => $entry->getCountry(),
            'handicap_account_number' => $entry->getHandicapAccountNumber(),
            'handicap_score' => $entry->getHandicapScore(),
            'comments' => $entry->getComments()
        ];
    }

}