<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupAccountLedger;
use foreup\rest\models\entities\ForeupAccounts;
use League\Fractal;
class account_transformer extends Fractal\TransformerAbstract
{
	protected $availableIncludes = [
		"owner","account_type"
	];

	public function transform(ForeupAccounts $account)
	{
		//\Doctrine\Common\Util\Debug::dump($course->getMyTeeSheets());
		return [
			'id' => (int)$account->getId(),
			'number' => $account->getNumber(),
			'balance' => (float)$account->getBalance(),
			'date_created' => $account->getDateCreated()->toDateTimeString(),
		];
	}

}