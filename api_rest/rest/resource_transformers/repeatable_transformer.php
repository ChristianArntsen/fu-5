<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupRepeatable;
use League\Fractal;
use \foreup\rest\models\entities\ForeupRepeatableMarketingCampaigns;
use \foreup\rest\models\entities\ForeupRepeatableAccountRecurringChargeItems;

class repeatable_transformer extends Fractal\TransformerAbstract
{
	private $db;
	public function __construct($em=null)
	{
		$this->db = $em;
	}
	protected $availableIncludes = [
		'resource','item','marketing_campaign'
	];

	public function transform(ForeupRepeatable $rep)
	{
		return [
			'id' => (int)$rep->getId(),
			'type' => $rep->getType(),
			'resource_id' => $rep->getResourceId(),
			'last_ran' => $rep->getLastRan(),
			'next_occurence' => !empty($rep->getNextOccurence())?$rep->getNextOccurence()->format(\DateTime::ISO8601):null,
			'freq' => $rep->getFreq(),
			'dtstart' => !empty($rep->getDtstart())?$rep->getDtstart()->format(\DateTime::ISO8601):null,
			'until' => !empty($rep->getUntil())?$rep->getUntil()->format(\DateTime::ISO8601):null,
			'interval' => $rep->getInterval(),
			'count' => $rep->getCount(),
			'bymonth' => $rep->getBymonth(),
			'byweekno' => $rep->getByweekno(),
			'byyearday' => $rep->getByyearday(),
			'bymonthday' => $rep->getBymonthday(),
			'byday' => $rep->getByday(),
			'wkst' => $rep->getWkst(),
			'byhour' => $rep->getByhour(),
			'byminute' => $rep->getByminute(),
			'bysecond' => $rep->getBysecond(),
			'bysetpos' => $rep->getBysetpos()
		];
	}

	public function string_transform(ForeupRepeatable $rep){
		$ret = 'FREQ='.$rep->getFreq();

		if(!empty($rep->getDtstart()))$ret.=';DTSTART='.$rep->getDtstart()->format(\DateTime::ISO8601);
		if(!empty($rep->getUntil()))$ret.=';UNTIL='.$rep->getUntil()->format(\DateTime::ISO8601);
		if(!empty($rep->getInterval()))$ret.=';INTERVAL='.$rep->getInterval();
		if(!empty($rep->getCount()))$ret.=';COUNT='.$rep->getCount();
		if(!empty($rep->getBymonth()))$ret.=';BYMONTH='.$rep->getBymonth();
		if(!empty($rep->getByweekno()))$ret.=';BYWEEKNO='.$rep->getByweekno();
		if(!empty($rep->getByyearday()))$ret.=';BYYEARDAY='.$rep->getByyearday();
		if(!empty($rep->getBymonthday()))$ret.=';BYMONTHDAY='.$rep->getBymonthday();
		if(!empty($rep->getByday()))$ret.=';BYDAY='.$rep->getByday();
		if(!empty($rep->getWkst()))$ret.=';WKST='.$rep->getWkst();
		if(!empty($rep->getByhour()))$ret.=';BYHOUR='.$rep->getByhour();
		if(!empty($rep->getByminute()))$ret.=';BYMINUTE='.$rep->getByminute();
		if(!empty($rep->getBysecond()))$ret.=';BYSECOND='.$rep->getBysecond();
		if(!empty($rep->getBysetpos()))$ret.=';BYSETPOS='.$rep->getBysetpos();

		return $ret;
	}


	/**
	 * includeResource
	 *
	 * @param ForeupRepeatable|null $rep
	 * @return Fractal\Resource\Item
	 */
	public function includeResource(ForeupRepeatable $rep=null)
	{
		if(!isset($rep) || !isset($this->db)){
			return null;
		}
		$resource_name = $rep->getType();
		$resource = $rep->getResource();
		if($resource_name==='recurring_charge_item')
			return $this->item($resource, new account_recurring_charge_items_transformer(), 'accountRecurringChargeItems');
		else
			if($resource_name==='recurring_statement')
				return $this->item($resource, new account_recurring_charge_items_transformer(), 'accountRecurringStatements');
		elseif ($resource_name==='marketing_campaign')
			return $this->item($resource, new marketing_campaign_transformer(), $resource_name);
	}

	public function includeItem(ForeupRepeatableAccountRecurringChargeItems $rep=null)
	{
		return $this->includeResource($rep);
	}

	public function includeMarketingCampaign(ForeupRepeatableMarketingCampaigns $rep=null)
	{
		return $this->includeResource($rep);
	}
}
?>