<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupPrinters;
use League\Fractal;

class printers_transformer extends Fractal\TransformerAbstract
{
    public function transform(ForeupPrinters $printers)
    {
	    return [
		    'id'=>$printers->getPrinterId(),
		    'label'=>$printers->getLabel(),
		    'ipAddress'=>$printers->getIpAddress()
	    ];
    }

}