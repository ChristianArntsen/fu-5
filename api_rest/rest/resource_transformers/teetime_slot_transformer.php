<?php
namespace foreup\rest\resource_transformers;

use Carbon\Carbon;
use foreup\rest\models\entities\ForeupTeetime;
use foreup\rest\models\entities\LegacyObjects\TeetimeSlot;
use League\Fractal;
use foreup\rest\models\entities\ForeupTeesheet;

class teetime_slot_transformer extends Fractal\TransformerAbstract
{

	public function transform(TeetimeSlot $teetime)
	{
		$time =  $teetime->getTime();
		return [
			'id' => $time->timestamp,
			'holes' => $teetime->getHoles(),
			'time' =>$time->toIso8601String(),
			'scheduleName' => $teetime->getScheduleName(),
			'scheduleId' => $teetime->getScheduleId(),
			'availableSpots' => $teetime->getAvailableSpots(),
			'greenFee'=>$teetime->getGreenFee(),
			'cartFee'=>$teetime->getCartFee(),
			'rateType'=>$teetime->getRateType(),
			'greeFeeTax'=>$teetime->getGreenFeeTax(),
			'cartFeeTax'=>$teetime->getCartFeeTax(),
		];
	}
}