<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupPrinterGroups;
use foreup\rest\models\entities\ForeupPrinters;
use League\Fractal;

class printer_groups_transformer extends Fractal\TransformerAbstract
{
	public function transform(ForeupPrinterGroups $group)
    {
	    return [
		    'id'=>$group->getId(),
		    'label'=>$group->getLabel(),
            'defaultPrinterId'=>$group->getDefaultPrinterId()
	    ];
    }
}