<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupCustomers;
use League\Fractal;

class customer_transformer extends Fractal\TransformerAbstract {

	private $fields = [];

	protected $availableIncludes = [
		'sales','customerGroups','passes','invoices'
	];

	private $hideEmails = false;

	public function hideEmails()
	{
		$this->hideEmails = true;
	}

    public function transform(ForeupCustomers $entry)
    {
    	$personTransformer = new person_transformer();

    	$customerFormat = [
		    'id' => $entry->getPerson()->getPersonId(),
		    'username' => $entry->getUsername(),
		    'account_number' => $entry->getAccountNumber(),
		    'company_name' => $entry->getCompanyName(),
		    'taxable' => (boolean)$entry->getTaxable(),
		    'discount' => (float)$entry->getDiscount(),
		    'opt_out_email' => (boolean)$entry->getOptOutEmail(),
		    'opt_out_text' => (boolean)$entry->getOptOutText(),
		    'date_created' => !empty($entry->getDateCreated())?$entry->getDateCreated()->format(\DateTime::ISO8601):null,
		    'account_balance'=>$entry->getAccountBalance(),
		    'photo'=>$entry->getPhoto(),
		    'deleted'=>$entry->getDeleted(),
		    'member_account_balance'=>$entry->getMemberAccountBalance(),
		    'email_subscribed'=>!$entry->getOptOutEmail(),
		    'course'=>$entry->getCourse()->getName(),
		    'groups'=>$entry->getGroups(),
		    'contact_info'=>$personTransformer->transform($entry->getPerson())
	    ];

    	$customerFormat['price_class'] = $entry->getPriceClass();

		if(!empty($this->fields)){
			$toReturn = [];
			foreach($this->fields as $field){
				if(isset($customerFormat[$field])){
					$toReturn[$field] = $customerFormat[$field];
				}
			}
		} else {
			$toReturn = $customerFormat;
		}

		if($this->hideEmails && !empty($toReturn['contact_info']['email'])){
			$toReturn['contact_info']['email'] = $entry->getPerson()->getPersonId()."@proxyemail.foreupsoftware.com";
		}

	    return $toReturn;
    }

    public function includeInvoices(ForeupCustomers $customers)
    {
    	$invoices = $customers->getInvoices();

	    return $this->collection($invoices, new invoices_transformer(), 'invoices');
    }

    public function includeCustomerGroups(ForeupCustomers $customer)
    {
    	$groups = $customer->getGroups();

	    return $this->collection($groups, new customer_group_transformer(), 'customerGroups');
    }

	public function includeSales(ForeupCustomers $customer)
	{
		$sales = $customer->getSales();

		return $this->collection($sales, new sales_transformer(), 'sales');
	}

	/**
	 * @return array
	 */
	public function getFields()
	{
		return $this->fields;
	}

	/**
	 * @param array $fields
	 */
	public function setFields($fields)
	{
		$this->fields = $fields;
	}


}