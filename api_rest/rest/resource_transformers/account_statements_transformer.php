<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupAccountStatements;
use League\Fractal;

class account_statements_transformer extends Fractal\TransformerAbstract
{
	/**
	 * @var array
	 */
	protected $availableIncludes = [
		'payment_terms',
		'accountRecurringStatement',
		'customerTransactions',
		'memberTransactions',
        'customer',
        'charges',
        'payments'
	];

	public function transform(ForeupAccountStatements $statement) {

	    return [
			'id' => (int)$statement->getId(),
			'dateCreated' => !empty($statement->getDateCreated())?$statement->getDateCreated()->format(\DateTime::ISO8601):null,
			'dueDate' => !empty($statement->getDueDate()) ? $statement->getDueDate()->format(\DateTime::ISO8601) : null,
			'createdBy' => $statement->getCreatedBy(),
			'organizationId' => (int) $statement->getOrganizationId(),
			'personId' => (int) $statement->getPersonId(),
			'startDate' => !empty($statement->getStartDate())?$statement->getStartDate()->format(\DateTime::ISO8601):null,
			'endDate' => !empty($statement->getEndDate())?$statement->getEndDate()->format(\DateTime::ISO8601):null,
			'total' => $statement->getTotal(),
			'totalPaid' => $statement->getTotalPaid(),
			'totalOpen' => $statement->getTotalOpen(),
			'memo' => $statement->getMemo(),
			'number' => $statement->getNumber(),
			'deletedBy' => $statement->getDeletedBy(),
			'dateDeleted' => $statement->getDateDeleted(),
			'datePaid' => !empty($statement->getDatePaid())?$statement->getDatePaid()->format(\DateTime::ISO8601):null,
			'includeMemberTransactions' => $statement->getIncludeMemberTransactions(),
			'includeCustomerTransactions' => $statement->getIncludeCustomerTransactions(),
			'autopayEnabled' => $statement->getAutopayEnabled(),
			'autopayDelayDays' => $statement->getAutopayDelayDays(),
			'autopayAttempts' => $statement->getAutopayAttempts(),
			'sendEmail' => $statement->getSendEmail(),
			'sendMail' => $statement->getSendMail(),
			'dateEmailSent' => !empty($statement->getDateEmailSent())?$statement->getDateEmailSent()->format(\DateTime::ISO8601):null,
			'dateMailSent' => !empty($statement->getDateMailSent())?$statement->getDateMailSent()->format(\DateTime::ISO8601):null,
			'payMemberBalance' => $statement->getPayMemberBalance(),
			'payCustomerBalance' => $statement->getPayCustomerBalance(),
			'sendZeroChargeStatement' => $statement->getSendZeroChargeStatement(),
			'footerText' => $statement->getFooterText(),
			'messageText' => $statement->getMessageText(),
			'termsAndConditionsText' => $statement->getTermsAndConditionsText(),
			'previousOpenBalance' => $statement->getPreviousOpenBalance(),
			'totalFinanceCharges' => $statement->getTotalFinanceCharges(),
			'financeChargeEnabled' => $statement->getFinanceChargeEnabled(),
			'financeChargeAmount' => $statement->getFinanceChargeAmount(),
			'financeChargeType' => $statement->getFinanceChargeType(),
			'financeChargeAfterDays' => $statement->getFinanceChargeAfterDays(),
            'memberBalanceForward' => (float) $statement->getMemberBalanceForward(),
            'customerBalanceForward' => (float) $statement->getCustomerBalanceForward()
		];
	}

    /**
     * includeCustomer
     *
     * @param ForeupAccountStatements $statement
     * @return Fractal\Resource\Item
     */
    public function includeCustomer(ForeupAccountStatements $statement)
    {
        $customer = $statement->getCustomer();
        return $this->item($customer, new customer_transformer(), 'customers');
    }

    /**
     * includeCharges
     *
     * @param ForeupAccountStatements $statement
     * @return Fractal\Resource\Collection
     */
    public function includeCharges(ForeupAccountStatements $statement = null)
    {
        $charges = $statement->getCharges();
        return $this->collection($charges, new account_statement_charges_transformer(), 'accountStatementCharges');
    }

    /**
     * includePayments
     *
     * @param ForeupAccountStatements $statement
     * @return Fractal\Resource\Collection
     */
    public function includePayments(ForeupAccountStatements $statement = null)
    {
        $payments = $statement->getPayments();
        return $this->collection($payments, new account_statement_payments_transformer(), 'accountStatementPayments');
    }

	/**
	 * @param ForeupAccountStatements|null $statement
	 * @return Fractal\Resource\Item
	 */
	public function includePaymentTerms(ForeupAccountStatements $statement = null)
	{
		$terms = $statement->getPaymentTerms();
		if(!isset($terms)) return null;

		return $this->item($terms, new account_payment_terms_transformer(), 'paymentTerms');
	}

	/**
	 * @param ForeupAccountStatements|null $statement
	 * @return Fractal\Resource\Item
	 */
	public function includeAccountRecurringStatement(ForeupAccountStatements $statement = null)
	{
		$rec_statement = $statement->getRecurringStatement();
		if(!isset($rec_statement)) return null;

		return $this->item($rec_statement, new account_recurring_statements_transformer(), 'accountRecurringStatements');
	}

    /**
     * @param ForeupAccountStatements|null $statement
     * @return Fractal\Resource\Item
     */
    public function includeCustomerTransactions(ForeupAccountStatements $statement = null)
    {
        $repo = $statement->getCustomer()->getAccountTransactions();
        $start = $statement->getStartDate();
        $end = $statement->getEndDate();

        $criteria = new \Doctrine\Common\Collections\Criteria();

        $criteria->andWhere($criteria->expr()->andX(
            $criteria->expr()->gte('transDate', $start),
            $criteria->expr()->lte('transDate', $end),
            $criteria->expr()->eq('accountType', 'customer')
        ));
        $criteria->orderBy(['transDate' => 'ASC']);

        $transactions = $repo->matching($criteria);

        return $this->collection($transactions, new account_transactions_transformer(), 'customerTransactions');
    }

	/**
	 * @param ForeupAccountStatements|null $statement
	 * @return Fractal\Resource\Item
	 */
	public function includeMemberTransactions(ForeupAccountStatements $statement = null)
	{
        $repo = $statement->getCustomer()->getAccountTransactions();
        $start = $statement->getStartDate();
        $end = $statement->getEndDate();

        $criteria = new \Doctrine\Common\Collections\Criteria();

        $criteria->andWhere($criteria->expr()->andX(
            $criteria->expr()->gte('transDate', $start),
            $criteria->expr()->lte('transDate', $end),
            $criteria->expr()->eq('accountType', 'member')
        ));
        $criteria->orderBy(['transDate' => 'ASC']);

        $transactions = $repo->matching($criteria);

		return $this->collection($transactions, new account_transactions_transformer(), 'memberTransactions');
	}
}
?>