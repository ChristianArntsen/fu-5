<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use League\Fractal;
use foreup\rest\models\entities\ForeupRepeatable;
class account_recurring_statements_transformer extends Fractal\TransformerAbstract
{
	/**
	 * @var array
	 */
	protected $availableIncludes = [
		'payment_terms'
	];

	/**
	 * @var array
	 */
	protected $defaultIncludes = [
		'repeated'
	];

	public function transform(ForeupAccountRecurringStatements $statement) {
		return [
			'id' => (int)$statement->getId(),
			'name' => $statement->getName(),
			'customerMessage'=>$statement->getCustomerMessage(),
			'dateCreated' => !empty($statement->getDateCreated())?$statement->getDateCreated()->format(\DateTime::ISO8601):null,
			'createdBy' => $statement->getCreatedBy()->getPerson()->getPersonId(),
			'organizationId' => (int) $statement->getOrganizationId(),
			'isActive' => $statement->getIsActive(),
			'dateDeleted' => !empty($statement->getDateDeleted())?$statement->getDateDeleted()->format(\DateTime::ISO8601):null,
			'deletedBy' => $statement->getDeletedBy(),
			'sendEmptyStatements' => $statement->getSendEmptyStatements(),
			'includeMemberTransactions'=>$statement->getIncludeMemberTransactions(),
			'includeCustomerTransactions'=>$statement->getIncludeCustomerTransactions(),
			'sendEmail'=>$statement->getSendEmail(),
			'sendMail'=>$statement->getSendMail(),
			'includePastDue'=>$statement->getIncludePastDue(),
			'dueAfterDays'=>$statement->getDueAfterDays(),
			'autopayEnabled'=>$statement->getAutopayEnabled(),
			'autopayAfterDays'=>$statement->getAutopayAfterDays(),
			'autopayAttempts'=>$statement->getAutopayAttempts(),
			'autopayOverdueBalance'=>$statement->getAutopayOverdueBalance(),
			'payMemberBalance' => $statement->getPayMemberBalance(),
			'payCustomerBalance' => $statement->getPayCustomerBalance(),
			'sendZeroChargeStatement' => $statement->getSendZeroChargeStatement(),
			'footerText' => $statement->getFooterText(),
			'messageText' => $statement->getMessageText(),
			'termsAndConditionsText' => $statement->getTermsAndConditionsText(),
			'isDefault' => $statement->getIsDefault(),
			'financeChargeEnabled' => $statement->getFinanceChargeEnabled(),
			'financeChargeAmount' => $statement->getFinanceChargeAmount(),
			'financeChargeType' => $statement->getFinanceChargeType(),
			'financeChargeAfterDays' => $statement->getFinanceChargeAfterDays()
		];
	}


	/**
	 * includeRepeated
	 *
	 * @param ForeupAccountRecurringStatements|null $item
	 * @return Fractal\Resource\Item
	 */
	public function includeRepeated(ForeupAccountRecurringStatements $item = null)
	{
		if(!isset($item))return null;
		$rep = $item->getRepeated();
		if(!isset($rep))return null;

		return $this->item($rep, new repeatable_transformer(), 'repeated');
	}

	/**
	 * @param ForeupAccountRecurringStatements|null $charge
	 * @return Fractal\Resource\Item
	 */
	public function includePaymentTerms(ForeupAccountRecurringStatements $charge = null)
	{
		$terms = $charge->getPaymentTerms();
		if(!isset($terms)) return null;

		return $this->item($terms, new account_payment_terms_transformer(), 'paymentTerms');
	}

}

?>