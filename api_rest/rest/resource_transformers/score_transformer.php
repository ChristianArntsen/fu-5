<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupMobileCourseImages;
use foreup\rest\models\entities\ForeupScores;
use League\Fractal;

class score_transformer extends Fractal\TransformerAbstract
{
	public function transform(ForeupScores $image)
	{
		return [
			'id' => (int)$image->getId(),
			'course_id' => $image->getCourse()->getCourseId(),
			'detailed_score_information' => $image->getDetailedScoreInformation(),
			'par' => $image->getPar(),
			'score' => $image->getScore()
		];
	}
}