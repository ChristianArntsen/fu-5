<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\LegacyObjects\TeesheetStats;
use League\Fractal;

class teesheet_stats_transformer extends Fractal\TransformerAbstract
{
	public function transform(TeesheetStats $stats)
	{
		return [
			'id'=>$stats->getDate()->toDateString(),
			'bookings' => (integer)$stats->getBookings(),
			'occupancy' => $stats->getOccupancy(),
			'playerNoShows' => (integer)$stats->getPlayerNoShows(),
			'playersCheckedIn' => (integer)$stats->getPlayersCheckedIn(),
			'revenue' => $stats->getRevenue(),
			'potential_slots' => $stats->getPotentialSlots(),
			'slots_available' => $stats->getSlotsAvailable(),
		];
	}
}