<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupCustomerCreditCards;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\models\entities\ForeupSalesPaymentsCreditCards;
use League\Fractal;
class customer_credit_card_transformer extends Fractal\TransformerAbstract
{
	public function transform(ForeupCustomerCreditCards $card)
	{
		return [
			'id' => (int)$card->getId(),
			// will not package any keys or passwords for mercury or ets, lest it be sent to the client...
			'recurring' =>$card->getRecurring(),
			'courseId' => $card->getCourseId(),
			'teeTime_id' => $card->getTeeTimeId(),
			'cardType' => $card->getCardType(),
			'cardholdersName' => $card->getCardholderName(),
			'expiration' => $card->getExpiration()->format(DATE_ISO8601),
			'maskedAccount' => $card->getMaskedAccount(),
			'deleted' => $card->getDeleted(),
			'successfulCharges' => $card->getSuccessfulCharges(),
			'failedCharges' => $card->getFailedCharges(),
			'status' => $card->getStatus()
		];
	}
}

?>