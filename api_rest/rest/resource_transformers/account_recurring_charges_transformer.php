<?php
namespace foreup\rest\resource_transformers;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use League\Fractal;
class account_recurring_charges_transformer extends Fractal\TransformerAbstract
{
	/**
	 * @var array
	 */
	protected $availableIncludes = [
		'items','payment_terms','recurringStatement','customers'
	];

	/**
	 * @param ForeupAccountRecurringCharges $charge
	 * @return array
	 */
	public function transform(ForeupAccountRecurringCharges $charge)
	{
		return [
			'id' => (int)$charge->getId(),
			'name' => $charge->getName(),
			'description' => $charge->getDescription(),
			'date_created' => !empty($charge->getDateCreated())?$charge->getDateCreated()->format(\DateTime::ISO8601):null,
			'created_by' => $charge->getCreatedBy(),
			'organization_id' => (int) $charge->getOrganizationId(),
			'is_active' => $charge->getIsActive(),
			'date_deleted' => !empty($charge->getDateDeleted()) ? $charge->getDateDeleted()->format(\DateTime::ISO8601) : null,
			'deleted_by' => $charge->getDeletedBy(),
			'prorate_charges' => $charge->getProrateCharges()
		];
	}

	/**
	 * @param ForeupAccountRecurringCharges|null $charge
	 * @return Fractal\Resource\Item
	 */
	public function includePaymentTerms(ForeupAccountRecurringCharges $charge = null)
	{
		$terms = $charge->getPaymentTerms();
		if(!isset($terms)) return null;

		return $this->item($terms, new account_payment_terms_transformer(), 'paymentTerms');
	}


	/**
	 * includeItems
	 *
	 * @param ForeupAccountRecurringCharges $charge
	 * @return Fractal\Resource\Collection
	 */
	public function includeItems(ForeupAccountRecurringCharges $charge)
	{
		$items = $charge->getItems();

		return $this->collection($items, new account_recurring_charge_items_transformer(), 'accountRecurringChargeItems');
	}


	/**
	 * includeCustomers
	 *
	 * @param ForeupAccountRecurringCharges $charge
	 * @return Fractal\Resource\Collection
	 */
	public function includeCustomers(ForeupAccountRecurringCharges $charge)
	{
		$customers = $charge->getAccountRecurringChargeCustomers();

		return $this->collection($customers, new account_recurring_charge_customers_transformer(), 'accountRecurringChargeCustomers');
	}


	/**
	 * includeRecurringStatements
	 *
	 * @param ForeupAccountRecurringCharges|null $charge
	 * @return Fractal\Resource\Item
	 */
	public function includeRecurringStatement(ForeupAccountRecurringCharges $charge)
	{
		$items = $charge->getRecurringStatement();

		return $this->item($items, new account_recurring_statements_transformer(), 'accountRecurringStatements');
	}
}
?>