<?php
namespace foreup\rest\resource_transformers;

use League\Fractal;
use foreup\rest\models\entities\ForeupCourses;

class settings_transformer extends Fractal\TransformerAbstract
{

    public function transform(ForeupCourses $course)
    {
        $details = [
            'businessName'=>(string) $course->getName(),
            'businessAddress'=>(string) $course->getAddress(),
            'country'=>(string) $course->getCountry(),
            'businessCity'=>(string) $course->getCity(),
            'businessState'=>(string) $course->getState(),
            'postalCode'=>(string) $course->getPostal(),
            'timezone'=>(string) $course->getTimezone(),
            'dateFormat'=>(string) $course->getDateFormat(),
            'timeFormat'=>(string) $course->getTimeFormat(),
            'phoneNumber'=>(string) $course->getPhone(),
            'faxNumber'=>(string) $course->getFax(),
            'emailAddress'=>(string) $course->getEmail(),
            'invoiceEmailAddress'=>(string) $course->getBillingEmail(),
            'websiteUrl'=>(string) $course->getWebsite()
        ];
    	$pos = [
		    'defaultTax1Rate'=>$course->getDefaultTax1Rate(),
			'defaultTax1Name'=>$course->getDefaultTax1Name(),
			'defaultTax2Rate'=>$course->getDefaultTax2Rate(),
			'defaultTax2Name'=>$course->getDefaultTax2Name(),
			'defaultTax2Cumulative'=>$course->getDefaultTax2Cumulative(),
			'autoGratuity'=>$course->getAutoGratuity(),
			'autoGratuityThreshold'=>$course->getAutoGratuityThreshold(),
			'serviceFeeActive'=>$course->isServiceFeeActive(),
			'serviceFeeTax1Name'=>$course->getServiceFeeTax1Name(),
			'serviceFeeTax1Rate'=>$course->getServiceFeeTax1Rate(),
			'serviceFeeTax2Name'=>$course->getServiceFeeTax2Name(),
			'serviceFeeTax2Rate'=>$course->getServiceFeeTax2Rate(),
			'creditCardFee'=>$course->getCreditCardFee(),
			'unitPriceIncludesTax'=>$course->isUnitPriceIncludesTax(),
			'customerCreditNickname'=>$course->getCustomerCreditNickname(),
			'memberBalanceNickname'=>$course->getMemberBalanceNickname(),
			'creditDepartment'=>$course->getCreditDepartment(),
			'creditDepartmentName'=>$course->getCreditDepartmentName(),
			'creditCategory'=>$course->getCreditCategory(),
			'creditCategoryName'=>$course->getCreditCategoryName(),
			'returnPolicy'=>$course->getReturnPolicy(),
			'printAfterSale'=>$course->isPrintAfterSale(),
			'webprnt'=>$course->isWebprnt(),
			'webprntIp'=>$course->getWebprntIp(),
			'webprntHotIp'=>$course->getWebprntHotIp(),
			'webprntColdIp'=>$course->getWebprntColdIp(),
			'webprntLabelIp'=>$course->getWebprntLabelIp(),
			'usePrinterExtension'=>$course->getUsePrinterExtension(),
			'useKitchenBuzzers'=>$course->isUseKitchenBuzzers(),
			'printCreditCardReceipt'=>$course->isPrintCreditCardReceipt(),
			'printSalesReceipt'=>$course->isPrintSalesReceipt(),
			'printTwoReceipts'=>$course->isPrintTwoReceipts(),
			'printTwoSignatureSlips'=>$course->isPrintTwoSignatureSlips(),
			'printTwoReceiptsOther'=>$course->isPrintTwoReceiptsOther(),
			'printTipLine'=>$course->isPrintTipLine(),
			'cashDrawerOnCash'=>$course->isCashDrawerOnCash(),
			'cashDrawerOnSale'=>$course->isCashDrawerOnSale(),
			'updatedPrinting'=>$course->isUpdatedPrinting(),
			'afterSaleLoad'=>$course->isAfterSaleLoad(),
			'teesheetUpdatesAutomatically'=>$course->isTeesheetUpdatesAutomatically(),
			'sendReservationConfirmations'=>$course->isSendReservationConfirmations(),
            'autoSplitTeetimes'=>$course->isAutoSplitTeetimes(),
			'fnbLogin'=>$course->isFnbLogin(),
			'receiptPrinter'=> $course->getReceiptPrinter(),
			'trackCash'=>$course->isTrackCash(),
			'blindClose'=>$course->isBlindClose(),
			'requireEmployeePin'=>$course->isRequireEmployeePin(),
			'minimumFoodSpend'=>$course->getMinimumFoodSpend(),
			'separateCourses'=>$course->isSeparateCourses(),
			'defaultKitchenPrinter'=>$course->getDefaultKitchenPrinter(),
			'hideEmployeeLastNameReceipt'=>$course->isHideEmployeeLastNameReceipt(),
			'requireGuestCount'=>$course->getRequireGuestCount(),
			'useCourseFiring'=>$course->isUseCourseFiring(),
			'courseFiringIncludeItems'=>$course->isCourseFiringIncludeItems(),
			'hideModifierNamesKitchenReceipts'=>$course->isHideModifierNamesKitchenReceipts(),
			'foodBevSortBySeat'=>$course->isFoodBevSortBySeat(),
			'defaultRegisterLogOpen'=>$course->getDefaultRegisterLogOpen(),
			'printSuggestedTip'=>$course->isPrintSuggestedTip(),
			'foodBevTipLineFirstReceipt'=>$course->isFoodBevTipLineFirstReceipt(),
			'autoEmailReceipt'=>$course->isAutoEmailReceipt(),
			'autoEmailNoPrint'=>$course->isAutoEmailNoPrint(),
			'requireSignatureMemberPayments'=>$course->isRequireSignatureMemberPayments(),
			'hideTaxable'=>$course->isHideTaxable(),
			'allowEmployeeRegisterLogBypass'=>$course->isAllowEmployeeRegisterLogBypass(),
			'teetimeDefaultToPlayer1'=>$course->isTeetimeDefaultToPlayer1(),
			'printTeeTimeDetails'=>$course->isPrintTeeTimeDetails(),
			'receiptPrintAccountBalance'=>$course->isReceiptPrintAccountBalance(),
			'printMinimumsOnReceipt'=>$course->isPrintMinimumsOnReceipt(),
			'limitFeeDropdownByCustomer'=>$course->isLimitFeeDropdownByCustomer(),
			'requireCustomerOnSale'=>$course->isRequireCustomerOnSale()
	    ];
    	$pricing = [
		    'id' => $course->getCourseId(),
		    'openTime'=>$course->getOpenTime(),
		    'closeTime'=>$course->getCloseTime(),
		    'earlyBirdHoursBegin'=>$course->getEarlyBirdHoursBegin(),
		    'earlyBirdHoursEnd'=>$course->getEarlyBirdHoursEnd(),
		    'morningHoursBegin'=>$course->getMorningHoursBegin(),
		    'morningHoursEnd'=>$course->getMorningHoursEnd(),
		    'afternoonHoursBegin'=>$course->getAfternoonHoursBegin(),
		    'afternoonHoursEnd'=>$course->getAfternoonHoursEnd(),
		    'twilightHour'=>$course->getTwilightHour(),
		    'superTwilightHour'=>$course->getSuperTwilightHour(),
		    'holidays'=>$course->getHolidays(),
		    'holes'=>$course->getHoles(),
		    'onlineBooking'=>$course->getOnlineBooking(),
		    'onlineBookingProtected'=>$course->getOnlineBookingProtected(),
		    'includeTaxOnlineBooking'=>$course->isIncludeTaxOnlineBooking(),
		    'bookingRules'=>utf8_encode($course->getBookingRules()),
		    'noShowPolicy'=>utf8_encode($course->getNoShowPolicy()),
		    'termsAndConditions'=>utf8_encode($course->getTermsAndConditions()),
		    'onlineBookingWelcomeMessage'=>utf8_encode($course->getOnlineBookingWelcomeMessage()),
            'reservationEmailText'=>utf8_encode($course->getReservationEmailText()),
		    'openSun'=>$course->isOpenSun(),
		    'openMon'=>$course->isOpenMon(),
		    'openTue'=>$course->isOpenTue(),
		    'openWed'=>$course->isOpenWed(),
		    'openThu'=>$course->isOpenThu(),
		    'openFri'=>$course->isOpenFri(),
		    'openSat'=>$course->isOpenSat(),
		    'weekendFri'=>$course->isWeekendFri(),
		    'weekendSat'=>$course->isWeekendSat(),
		    'weekendSun'=>$course->isWeekendSun()
	    ];
    	$misc = [
		    'useNewPermissions'=>$course->isUseNewPermissions(),
            'mailchimpApiKey'=>$course->getMailchimpApiKey(),
		    'billingEmail'=>$course->getBillingEmail(),
		    'reservationEmail'=>$course->getReservationEmail(),
		    'numberOfItemsPerPage'=>$course->getNumberOfItemsPerPage(),
		    'hideBackNine'=>$course->isHideBackNine(),
		    'stackTeeSheets'=>$course->isStackTeeSheets(),
		    'sideBySideTeeSheets'=>$course->isSideBySideTeeSheets(),
		    'alwaysListAll'=>$course->isAlwaysListAll(),
		    'currentDayCheckinsOnly'=>$course->isCurrentDayCheckinsOnly(),
		    'useEtsGiftcards'=>$course->getUseEtsGiftcards(),
		    'deductTips'=>$course->isDeductTips(),
		    'onlineGiftcardPurchases'=>$course->isOnlineGiftcardPurchases(),
		    'showInvoiceSaleItems'=>$course->isShowInvoiceSaleItems(),
            'teedOffColor'=>empty($course->getTeedOffColor())?null:$course->getTeedOffColor(),
            'teeTimeCompletedColor'=>empty($course->getTeeTimeCompletedColor())?null:$course->getTeeTimeCompletedColor()
	    ];
    	$sales = [
		    'printAfterSale'=>$course->isPrintAfterSale(),
            'useTerminals'=>$course->isUseTerminals(),
		    'printTwoReceipts'=>$course->isPrintTwoReceipts(),
		    'printTwoReceiptsOther'=>$course->isPrintTwoReceiptsOther(),
		    'afterSaleLoad'=>$course->isAfterSaleLoad(),
		    'returnPolicy'=>$course->getReturnPolicy(),
		    //'receipt_printer'=>$course->get_receipt_printer
		    'trackCash' => (bool) $course->isTrackCash(),
            'creditLimitToSubtotal' => $course->getCreditLimitToSubtotal(),
            'onlinePurchaseTerminalId' => $course->getOnlinePurchaseTerminalId(),
            'onlineInvoiceTerminalId' => $course->getOnlineInvoiceTerminalId()
	    ];
    	$teesheet = [
		    'teesheetUpdatesAutomatically'=>$course->isTeesheetUpdatesAutomatically()
	    ];
        $loyalty = [
            'useLoyalty' =>$course->isUseLoyalty(),
            'loyaltyAutoEnroll' =>$course->isLoyaltyAutoEnroll()
        ];

    	$configurations = array_merge($pos,$pricing);
        $configurations = array_merge($configurations,$details);
        $configurations = array_merge($configurations,$misc);
        $configurations = array_merge($configurations,$teesheet);
        $configurations = array_merge($configurations,$sales);
        $configurations = array_merge($configurations,$loyalty);
	    ksort($configurations);
	    return  $configurations;
    }


}