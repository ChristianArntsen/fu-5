<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupCustomerGroups;
use League\Fractal;

class customer_group_transformer extends Fractal\TransformerAbstract {

	public function transform(ForeupCustomerGroups $entry)
    {
	    return [
	        'id' => $entry->getGroupId(),
            'group_id' => $entry->getGroupId(),
	        'label' => $entry->getLabel(),
	        'itemDiscount' => $entry->getItemCostPlusPercentDiscount()

        ];
    }
}