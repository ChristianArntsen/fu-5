<?php
namespace foreup\rest\resource_transformers;

use League\Fractal;
use foreup\rest\models\entities\ForeupMarketingCampaigns;

class marketing_campaign_transformer extends Fractal\TransformerAbstract
{
    public function transform(ForeupMarketingCampaigns $campaign)
    {
        $test1 = $campaign->getRecipients();
        $test2 = unserialize($campaign->getRecipients());
        $test3 = json_encode(unserialize($campaign->getRecipients()));
        return [
            'id'            => $campaign->getCampaignId(),
            'name'          => $campaign->getName(),
            'content'       => $campaign->getContent(),
            'deleted'       => $campaign->getDeleted(),
            'recipients'    => $campaign->getRecipients()?json_encode(unserialize($campaign->getRecipients())):'',
            'queued'        => $campaign->getQueued(),
            'is_sent'       => $campaign->getIsSent(),
            'attempts'      => $campaign->getAttempts(),
            'date_created'  => $campaign->getDateCreated(),
            'send_date_cst' => $campaign->getSendDateCst(),
            'send_date' => $campaign->getSendDate(),
            'status'        => $campaign->getStatus(),
            'course_name'   => $campaign->getCourse()->getName(),
            'course_city'   => $campaign->getCourse()->getCity(),
            'course_state'   => $campaign->getCourse()->getState(),
            'course_id'     => $campaign->getCourse()->getCourseId(),
            'preview'       => $campaign->getRemoteHash()
        ];
    }
}