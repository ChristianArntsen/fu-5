<?php
namespace foreup\rest\resource_transformers;

use foreup\rest\models\entities\ForeupInventory;
use League\Fractal;

class inventory_transformer extends versioned_transformer
{
	protected $availableIncludes = [
		'items','employees','sales'
	];

    public function transform(ForeupInventory $item)
    {

	    return [
		    'id'=>$item->getTransId(),
		    'comment'=>$item->getTransComment(),
		    'adjustmentAmount'=>(float)$item->getTransInventory(),
		    'courseId'=>$item->getCourseId(),
		    'itemId'=>$item->getItem()->getItemId(),
		    'itemNumber'=>(integer)$item->getItem()->getItemNumber(),
		    'transDate'=>$item->getTransDate()->toIso8601String()
	    ];
    }

	public function includeItems(ForeupInventory $item)
	{
		$items = $item->getItem();

		return $this->item($items, new items_transformer(), 'items');
	}
	public function includeEmployees(ForeupInventory $item)
	{
		$employees = $item->getTransUser();

		return $this->item($employees, new employees_transformer(), 'employees');
	}
	public function includeSales(ForeupInventory $item)
	{
		$sale = $item->getSale();
		if(empty($sale))
			return  null;

		return $this->item($sale, new sales_transformer(), 'sales');
	}
}