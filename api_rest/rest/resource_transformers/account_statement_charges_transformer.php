<?php
namespace foreup\rest\resource_transformers;

use League\Fractal;
class account_statement_charges_transformer extends Fractal\TransformerAbstract
{
	/**
	 * @var array
	 */
	protected $availableIncludes = [];

	public function transform($charge) {

	    return [
			'id' => (int) $charge->getId(),
            'line' => (int) $charge->getLine(),
            'name' => $charge->getName(),
            'itemId' => (int) $charge->getItemId(),
            'total' => (float) $charge->getTotal(),
            'subtotal' => (float) $charge->getSubtotal(),
            'tax' => (float) $charge->getTax(),
            'taxPercentage' => (float) $charge->getTaxPercentage(),
            'unitPrice' => (float) $charge->getUnitPrice(),
            'qty' => (float) $charge->getQty(),
            'dateCharged' => !empty($charge->getDateCharged())?$charge->getDateCharged()->format(\DateTime::ISO8601):null
		];
	}
}

?>