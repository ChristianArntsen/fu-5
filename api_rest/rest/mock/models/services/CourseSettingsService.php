<?php
/**
 * Created by PhpStorm.
 * User: brend
 * Date: 9/23/2016
 * Time: 3:36 PM
 */

namespace foreup\rest\mock\models\services;


use foreup\rest\models\services\CourseSettingsService;

class MockCourseSettingsService extends CourseSettingsService
{

	private $coursesRepo,$authUser;

	/**
	 * CourseSettingsService constructor.
	 */
	public function __construct($em, $authUser)
	{
		$this->coursesRepo = $em->getRepository("e:ForeupCourses");
		$this->authUser = $authUser;
		$this->loadSettings();
	}


	/**
	 * @return null|ForeupCourses
	 */
	public function loadSettings($courseId = null)
	{
		if(isset($courseId)){
			$course = $this->coursesRepo->find($courseId);
		} else {
			$course = $this->coursesRepo->find($this->authUser->getCid());
		}
		return $course;
	}

}