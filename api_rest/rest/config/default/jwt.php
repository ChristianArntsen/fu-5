<?php
/*
 Configuration for the  foreUP JWT library

 JWT RFC @ https://tools.ietf.org/html/rfc7519
 Implements: https://github.com/lcobucci/jwt

 Online Base64 En/Decoder Tools: https://www.base64decode.org/
*/
unset($config);

$config['jwt_secretKey']     = 'OOVlddU82kBDgVvnCrlf8LJNlY2BY16I'; // Key used to encrypt / verify signature of issued tokens

$config['jwt_issuer']        = 'foreupsoftware.com'; // Issuer (iss claim)
$config['jwt_audience']      = 'foreupsoftware.com'; // Audience (iat claim)
$config['jwt_expiresAfter']  = 86400 * 30 * 12; // Seconds until the token expires (added to current time to  create exp claim)

$config['jwt_cookie_expire'] = (86400 * 30); // Time (seconds) until cookie expires. 86400 = 1 Day
$config['jwt_cookie_path']   = '/'; // Valid path for the cookie
$config['jwt_cookie_domain'] = 'foreupsoftware.com'; // Valid domain(s) for the cookie
$config['jwt_cookie_secure'] = false; // Set to true to only allow over HTTPS. Help mitigate XSS and CSRF attacks.
