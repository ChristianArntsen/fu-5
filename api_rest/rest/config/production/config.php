<?php
$config['orm.auto_generate_proxies'] = false;
$config['legacy_url'] = "http://mobile.foreupsoftware.com";
$config['cache'] = [
	"driver"=>"filesystem",
	"path"=>sys_get_temp_dir()."/cache",
];