<?php
unset($config);
/*
 * This configuration file is used to firewall requests
 * based on authenticated user_level and requested controller.
 *
 * Rules are executed in order.
 * Default Allow -> All users with level 5 to all controllers.
 * Default Deny -> All unmatched requests.
 *
 * Rules can be of the following types:
 *    exact
 *    pcre
 *
 * Minimum Levels of -1 is a special case, which will allow non-authenticated requests
 *
 * Rules are added to $config['permissions'] array
 * ex. $config['permissions'][] = array( "type"=>"pcre", "min_level" => 3, "endpoint" => "/^(login\.controller:).*$");
 *     ^ will match all **routed** login endpoints where user is equal to or greater than 3
 *
 * ex. $config['permissions'][] = array( "type"=>"exact", "min_level" => 3, "endpoint" => "login.controller:get"); //will match all **routed** login endpoints
 *     ^ will match the **routed** endpoint login.controller:get where user is equal to or greater than 3
 *
 */

//$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(courses\.controller:).*$/');
//$config['permissions'][] = array( 'type'=>'pcre', 'min_level' => 0, 'endpoint' => '/^(marketing_campaigns\.controller:).*$/');

$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(courses\.controller:).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(employee_audit_log\.controller:).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(marketing_campaigns\.controller:).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(courses\.controller:).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(marketing_campaigns\.controller:).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(scores\.controller:).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(accounts\.controller).*$/');

$config['permissions'][] = array('limitation'=>'customers', 'min_level' => 0, 'endpoint' => '/^(customers\.controller).*$/');
$config['permissions'][] = array('limitation'=>'customers', 'min_level' => 0, 'endpoint' => '/^(customerSales\.controller).*$/');
$config['permissions'][] = array('limitation'=>'customers', 'min_level' => 0, 'endpoint' => '/^(customerGroups\.controller).*$/');
$config['permissions'][] = array('limitation'=>'customers', 'min_level' => 0, 'endpoint' => '/^(customerGroupMembers\.controller).*$/');

$config['permissions'][] = array('limitation'=>'sales', 'min_level' => 0, 'endpoint' => '/^(carts\.controller).*$/');
$config['permissions'][] = array('limitation'=>'sales', 'min_level' => 0, 'endpoint' => '/^(cartsItems\.controller).*$/');
$config['permissions'][] = array('limitation'=>'sales', 'min_level' => 0, 'endpoint' => '/^(sales\.controller).*$/');

$config['permissions'][] = array('limitation'=>'inventory', 'min_level' => 0, 'endpoint' => '/^(inventory\.controller).*$/');
$config['permissions'][] = array('limitation'=>'inventory', 'min_level' => 0, 'endpoint' => '/^(items\.controller).*$/');
$config['permissions'][] = array('limitation'=>'inventory', 'min_level' => 0, 'endpoint' => '/^(inventoryAudits\.controller).*$/');

$config['permissions'][] = array('limitation'=>'employees', 'min_level' => 0, 'endpoint' => '/^(terminals\.controller).*$/');
$config['permissions'][] = array('limitation'=>'employees', 'min_level' => 0, 'endpoint' => '/^(printers\.controller).*$/');
$config['permissions'][] = array('limitation'=>'employees', 'min_level' => 0, 'endpoint' => '/^(employees\.controller).*$/');



$config['permissions'][] = array('limitation'=>'sales', 'min_level' => 0, 'endpoint' => '/^(printerGroups\.controller).*$/');
$config['permissions'][] = array('limitation'=>'sales', 'min_level' => 0, 'endpoint' => '/^(refundReasons\.controller).*$/');
$config['permissions'][] = array('limitation'=>'sales', 'min_level' => 0, 'endpoint' => '/^(customPaymentTypes\.controller).*$/');

$config['permissions'][] = array('limitation'=>'sales', 'min_level' => 0, 'endpoint' => '/^(receiptAgreements\.controller).*$/');
$config['permissions'][] = array('limitation'=>'sales', 'min_level' => 0, 'endpoint' => '/^(loyaltyRates\.controller).*$/');

$config['permissions'][] = array('limitation'=>'teesheet', 'min_level' => 0, 'endpoint' => '/^(teesheetStats\.controller).*$/');
$config['permissions'][] = array('limitation'=>'teesheet', 'min_level' => 0, 'endpoint' => '/^(courseTeesheets\.controller).*$/');
$config['permissions'][] = array('limitation'=>'teesheet', 'min_level' => 0, 'endpoint' => '/^(teesheetTeetimes\.controller).*$/');
$config['permissions'][] = array('limitation'=>'teesheet', 'min_level' => 0, 'endpoint' => '/^(teesheetBookings\.controller).*$/');
$config['permissions'][] = array('limitation'=>'teesheet', 'min_level' => 0, 'endpoint' => '/^(teesheetSeasons\.controller).*$/');
$config['permissions'][] = array('limitation'=>'teesheet', 'min_level' => 0, 'endpoint' => '/^(seasonTimeframes\.controller).*$/');
$config['permissions'][] = array('limitation'=>'teesheet', 'min_level' => 0, 'endpoint' => '/^(bookedPlayer\.controller).*$/');
$config['permissions'][] = array('limitation'=>'teesheet', 'min_level' => 0, 'endpoint' => '/^(priceClasses\.controller).*$/');


$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(hooks\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(coursePageSettings\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(tokensValidation\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(accountRecurringCharges\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(accountRecurringChargeItems\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(accountRecurringChargeCustomers\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(accountRecurringChargeGroups\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(accountRecurringStatements\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(accountStatements\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(statements\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(bulkEditJobs\.controller).*$/');
$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(repeatable\.controller).*$/');

$config['permissions'][] = array('min_level' => -1, 'endpoint' => '/^(customer_images\.controller).*$/');
$config['permissions'][] = array('min_level' => -1, 'endpoint' => '/^(tokens\.controller:).*$/'); // Allow all requests to login endpoint(s)

$config['permissions'][] = array('min_level' => 0, 'endpoint' => '/^(courseSettings\.controller).*$/');
