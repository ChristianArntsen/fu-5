<?php
namespace foreup\rest\application;

use foreup\auth\json_web_token;
use foreup\rest\models\services\CourseSettingsService;
use foreup\rest\models\services\LegacyCartService;
use foreup\rest\models\services\LegacySaleService;
use foreup\rest\models\services\LegacyTeetimeService;
use foreup\rest\models\services\StatementUtilities;
use foreup\rest\models\services\TeesheetStats;
use foreup\rest\models\services\TeesheetStatsService;
use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Sorien\Provider\PimpleDumpProvider;

class services_loader
{
	protected $app;

	public function __construct(Application &$app)
	{
		$this->app = $app;
	}

	public function register_services()
	{

		$this->app->register(new ServiceControllerServiceProvider());

//		$this->app->register(new MonologServiceProvider(), array(
//			'monolog.logfile' => __DIR__.'/development.log'
//		));


		$this->app->register(new DoctrineServiceProvider(), array(
			"db.options" => $this->app["config.db.options"]
		));

		$this->app->register(new DoctrineOrmServiceProvider, array(
			'orm.proxies_dir' => __DIR__ . '/../models/proxies',
			'orm.proxies_namespace'=>'DoctrineProxies',
			'orm.default_cache' =>$this->app['config.cache'],
			'orm.em.options' => array(
				'mappings' => array(
					// Using actual filesystem paths
					array(
						'type' => 'annotation',
						'namespace' => 'foreup\rest\models\entities',
						'path' => __DIR__ . '/models/entities',
						'use_simple_annotation_reader' => false
					)
				),
			),
			'orm.auto_generate_proxies'=>$this->app['config.orm.auto_generate_proxies'],
		));

		$this->app['jwt_token'] = $this->app->share(function () {
			return new json_web_token(
					$this->app['config.jwt_secretKey'],
					$this->app['config.jwt_issuer'],
					$this->app['config.jwt_audience'],
					$this->app['config.jwt_expiresAfter'],
					$this->app['config.jwt_cookie_expire'],
					$this->app['config.jwt_cookie_path'],
					$this->app['config.jwt_cookie_domain'],
					false
			);
		});

		$this->app['courseSettings'] = $this->app->share(function(){
			return new CourseSettingsService($this->app['orm.em'],$this->app['auth.userObj']);
		});

		$this->app['teesheetStats'] = $this->app->share(function () {
			return new TeesheetStatsService($this->app['config.legacy_url']);
		});

		$this->app['legacySaleService'] = $this->app->share(function () {
			return new LegacySaleService($this->app['config.legacy_url']);
		});
		$this->app['legacyTeetimeService'] = $this->app->share(function () {
			return new LegacyTeetimeService($this->app['config.legacy_url']);
		});
		$this->app['legacyCartService'] = $this->app->share(function () {
			return new LegacyCartService($this->app['config.legacy_url'],$this->app['orm.em']);
		});
		$this->app['statementUtilities'] = $this->app->share(function() {
			return new StatementUtilities($this->app);
		});
		$conn = $this->app['orm.em']->getConnection();
		$conn->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

		$config = $this->app['orm.em']->getConfiguration();
		$config->addEntityNamespace('e', 'foreup\\rest\\models\\entities');


		$this->app->register(new PimpleDumpProvider());
	}

}
