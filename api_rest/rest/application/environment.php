<?php
namespace foreup\rest\application;

if(!defined('ENVIRONMENT'))
{
	switch(getenv('ENVIRONMENT'))
	{
		case 'production':
		case 'local':
			define('ENVIRONMENT', 'production');
			error_reporting(E_ALL);
			ini_set('display_errors', 0);
			break;

		case 'testing':
			define('ENVIRONMENT', 'testing');
			error_reporting(E_ERROR | E_USER_ERROR | E_RECOVERABLE_ERROR);
			ini_set('display_errors', 0);
			break;

		case 'acceptance':
			define('ENVIRONMENT', 'acceptance');
			error_reporting(E_ERROR | E_USER_ERROR | E_RECOVERABLE_ERROR);
			ini_set('display_errors', 0);
			break;

		case 'development':
			define('ENVIRONMENT', 'development');
			error_reporting(E_ALL);
			ini_set('display_errors', 1);
			break;

		default:
			define('ENVIRONMENT', 'production');
			error_reporting(E_ALL);
			ini_set('display_errors', 0);
			break;
	}
}
