<?php
namespace foreup\rest\application;

use Silex\Application;

class config
{
	protected $app;

	public function __construct(Application &$app)
	{
		// Verify ENVIRONMENT has been set
		if(!defined('ENVIRONMENT')){
			trigger_error('Invalid ENVIRONMENT. Please check your web server configuration.', E_USER_ERROR);
			exit (1);
		}

		$this->app = $app;
	}

	public function loadConfig()
	{
		switch(ENVIRONMENT)
		{
			case 'production':
			case 'testing':
			case 'acceptance':
			case 'development':
				$this->loadConfigFiles(__DIR__.'/../config/'.ENVIRONMENT);
				break;

			default:
					$this->loadConfigFiles(__DIR__.'/../config/default');
				break;
		}
	}

	// Used to load an external config directory that follows the same structure
	// ex. $configDir/production
	public function loadExternalConfigDir($configDir)
	{
		$this->loadConfigFiles($configDir.'/'.ENVIRONMENT);
	}

	private function loadConfigFiles($configDir)
	{
		if(!is_dir($configDir))
		{
			// Directory does not exist
			$this->configDirectoryNotFound();

		} else {
			// Load each .json file in the config directory
			foreach(scandir($configDir) as $file)
			{
				if(!is_dir($file) && strtolower(pathinfo($file)['extension'])==='php')
				{
					// Load each of the files configurations
					if(isset($config)) unset($config); // Clear any existing config values

					include $configDir.'/'.$file;

					// Parse out the data
					foreach ($config as $key => $val) {
						if(!empty($this->app['config'.$key])){
							trigger_error('Configuration key already defined. Overwriting: '.$key, E_USER_WARNING);
						}
						$this->app['config.'.$key] = $val;
					}
				}
			}

			if(isset($config)) unset($config); // Clear any hanging config values

		}
		$this->processBuiltInSettings();
	}

	private function processBuiltInSettings(){
		// Configure debug mode
		if($this->app['config.debug'] === true){
			define('DEBUG', true);
			$this->app['debug'] = true;
		} else {
			define('DEBUG', false);
			$this->app['debug'] = false;
		}

	}

	private function configDirectoryNotFound()
	{
		trigger_error('Config directory does not exist for environment: '.ENVIRONMENT, E_USER_ERROR);
		exit (1);
	}

}