<?php
namespace foreup\rest\middleware;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class cors implements MiddlewareInterface
{
	public static function register(Application &$application)
	{

		//handling CORS preflight request
		$application->before(function (Request $request) {
			if ($request->getMethod() === "OPTIONS") {
				$response = new Response();
				//$response->headers->set("Access-Control-Allow-Origin","*");
				$response->headers->set("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
				$response->headers->set("Access-Control-Allow-Headers", "Content-Type");
				$response->setStatusCode(200);
				return $response;
			} else {
				return null;
			}
		});

		//handling CORS respons with right headers
		$application->after(function (Request $request, Response $response) {
			//$response->headers->set("Access-Control-Allow-Origin","*");
			$response->headers->set("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
			$response->headers->set('Access-Control-Allow-Headers', 'accept, Authorization');
			return null;
		});

	}
}