<?php
namespace foreup\rest\middleware;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use foreup\auth\json_web_token;

class json_only implements MiddlewareInterface
{
	public static function register(Application &$application)
	{
		$application->before(function (Request $request) {
			if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
				$data = json_decode($request->getContent(), true);
				if($data === false){
					$json_response = new JsonResponse();
					$json_response->setData(array(
							'success'=>false,
							'msg'=>'Invalid json sent in body.'
					));
					$json_response->setStatusCode(400); // 401 Unauthorized -> Unauthenticated

					return $json_response;
				}
				$request->request->replace(is_array($data) ? $data : array());
			}
		});
	}

}
