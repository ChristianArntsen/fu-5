<?php
namespace foreup\rest\middleware;

use foreup\rest\models\services\AuthenticatedUser;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use foreup\auth\json_web_token;

class authentication implements MiddlewareInterface
{
	public static function register(Application &$application)
	{
		// Authorization & Authentication
		$application->before(static function (Request $request, Application $application)
		{
			$endpointRequested = $request->attributes->get('_controller');

			$cookies = $request->cookies;
			if( $cookies->has('token') || $request->headers->get("x-authorization"))
			{
				// Initialize the token
				$use_secure_cookies = false;

				$token = new json_web_token(
					$application['config.jwt_secretKey'],
					$application['config.jwt_issuer'],
					$application['config.jwt_audience'],
					$application['config.jwt_expiresAfter'],
					$application['config.jwt_cookie_expire'],
					$application['config.jwt_cookie_path'],
					$application['config.jwt_cookie_domain'],
					$use_secure_cookies
				);

				if ($request->headers->get("x-authorization")){
					$passed_token = explode(" ",$request->headers->get("x-authorization"));
					$passed_token = $passed_token[1];
				} else if($cookies->has('token')){
					$passed_token = $cookies->get('token');
				}
				$token->setToken($passed_token);

				// Verify token (and all claims) are valid
				if(!($token->validate())){
					// Token is invalid / tampered with
					$json_response = new JsonResponse();
					$json_response->setData(array(
						'success'=>false,
						'msg'=>'The token is invalid.'
					));
					$json_response->setStatusCode(401); // 401 Unauthorized -> Unauthenticated

					return $json_response;
				}

				// Validate Claims
				$claims = $token->getClaims();
				if(!isset($claims['level'])){
					return self::notAuthorized();
				} else {
					$user_level = $claims['level']->getValue();
				}
				if(!isset($claims['uid'])){
					return self::notAuthorized();
				} else {
					$user_id = $claims['uid']->getValue();
				}
				if(!isset($claims['cid'])){
					return self::notAuthorized();
				} else {
					$course_id = $claims['cid']->getValue();
				}
				if(!isset($claims['employee'])){
					return self::notAuthorized();
				} else {
					$is_employee = ($claims['employee']->getValue() === true) ? true : false;
				}
                $emp_id = null;
                if(isset($claims['emp_id'])){
                    $emp_id = $claims['emp_id']->getValue();
                }
				$app_id = null;
				if(isset($claims['appId'])){
					$app_id = $claims['appId']->getValue();
				}
				$api_version = null;
				if(isset($claims['apiVersion'])){
					$api_version = $claims['apiVersion']->getValue();
				}
				$limitations = null;
				if(isset($claims['limitations'])){
					$limitations = $claims['limitations']->getValue();
				}

				// Verify Authorization
				if(self::isAuthorized($user_level, $endpointRequested, $application['config.permissions'],$limitations)){

					// Attach current user to the Application
					$application['auth.user'] = array(
						'loggedIn'=>true,
						'validated'=>true,
						'authorized'=>true,
						'uid'=>$user_id,
						'cid'=>$course_id,
						'level'=>(int)$user_level,
                        'emp_id'=>$emp_id,
						'isEmployee'=>$is_employee
					);

					//TODO: The above should be refactored, it was an anon array before, which is a pain to use.  I didn't have time to refactor it now though.
					$authenticatedUser = new AuthenticatedUser();
					$authenticatedUser->setTokenObject($token);
					$authenticatedUser->setUid($user_id);
					$authenticatedUser->setCid($course_id);
					$authenticatedUser->setLevel($user_level);
					$authenticatedUser->setLimitations($limitations);
					if(empty($emp_id) && $is_employee){
						$authenticatedUser->setEmpId($user_id);
					} else {
						$authenticatedUser->setEmpId($emp_id);
					}
					if(!empty($api_version)){
						$authenticatedUser->setApiVersion($api_version);
					}
					$authenticatedUser->setAppId($app_id);
					$authenticatedUser->setIsEmployee($is_employee);
					$authenticatedUser->setToken($passed_token);

					$application['auth.userObj'] = $authenticatedUser;

					$application['user'] = function() use ($application,$user_id){
						$em = $application['orm.em']->getRepository('e:ForeupUsers');
						$user = $em->findOneBy(["person"=>$user_id]);
						return $user;
					};
					$application['employee'] = function() use ($application,$user_id,$course_id){
						$em = $application['orm.em']->getRepository('e:ForeupEmployees');
						$result = $em->findBy(["person"=>$user_id,"courseId"=>$course_id]);

						return isset($result[0])?$result[0]:false;
					};

					// User is authorized for this controller
					return null;
				} else {
					return self::notAuthorized();
				}

			} else {
				// Lot logged in. Verify Authorization for non-logged-in user
				if(self::isAuthorized(-1, $endpointRequested, $application['config.permissions'])){
					return null; // User is authorized for this controller
				} else {
					return self::notLoggedIn();
				}
			}
		});
	}


	// Check if user is authorized to use endpoint
	private static function isAuthorized($user_level, $endpoint, $permissions,$limitations = null){
		// Level of 5 will always return true (super user)
		if($user_level == 5) return true;

		foreach($permissions as $permission){
			if(preg_match($permission['endpoint'], $endpoint) == 1){
				if(isset($permission['limitation']) && isset($limitations)){
					$requiredLimitation = $permission['limitation'];
					if(!isset($limitations->{$requiredLimitation}) || !$limitations->{$requiredLimitation}){
						return false;
					}
				}


				if($permission['min_level'] == -1 || $user_level >= $permission['min_level']){
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}

	// User is authenticated, but does not have permission
	public static function notAuthorized(){
		$json_response = new JsonResponse();
		$json_response->setData(array(
			'success'=>false,
			'msg'=>'You do not have permissions to access this resource.'
		));
		$json_response->setStatusCode(403); // 403 Forbidden

		return $json_response;
	}

	// User is not authenticated
	private static function notLoggedIn(){
		$json_response = new JsonResponse();
		$json_response->setData(array(
			'success'=>false,
			'msg'=>'You must be logged in to access this resource.'
		));
		$json_response->setStatusCode(401); // 401 Unauthorized -> Unauthenticated

		return $json_response;
	}

}
