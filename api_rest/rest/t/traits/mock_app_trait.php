<?php

namespace foreup\rest\t\traits;

use foreup\rest\t\Helpers\MockDefaultAppFactory;

/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 6/12/2017
 * Time: 9:59 AM
 */
trait mock_app_trait
{
    private $app, $controller, $transactionsOn;

    public function setupController($controllerName, $useTransactions = true){
        $this->transactionsOn = $useTransactions;

        $factory = new MockDefaultAppFactory();
        $this->app = $factory->createMockDefaultApp();

        $this->controller = $this->app[$controllerName];

        if($this->transactionsOn) {
            $this->controller->startTransaction();
        }
    }

    public function tearDownController(){
        if($this->transactionsOn) {
            $this->controller->rollback();
        }

        $conn = $this->app['orm.em']->getConnection();
        if($conn->isConnected()) {
            $conn->close();
        }
    }
}