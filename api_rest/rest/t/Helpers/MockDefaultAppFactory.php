<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 4/26/2017
 * Time: 11:08 AM
 */

namespace foreup\rest\t\Helpers;


use foreup\rest\mock\models\entities\ForeupCourses;
use Silex\Application;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupUsers;
use Symfony\Component\HttpFoundation\Request;

use Dflydev\Pimple\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use foreup\rest\t\Helpers\testing_middleware_loader;
use foreup\rest\t\Helpers\testing_config;
use foreup\rest\t\Helpers\testing_services_loader;
use foreup\rest\t\Helpers\testing_routes_loader;

class MockDefaultAppFactory extends \Codeception\TestCase\Test
{
    private $middleware_loader, $configuration, $routes_loader, $services_loader, $app;

    protected function createMockDefaultAppAndDeps()
    {
        $this->app = new Application();

        $this->setupUser();

        $this->setupEmployee();

        $this->setupDoctrineORM();

        $this->setupAppConfig();

        $this->setupMiddleWare();

        $this->setupServices();

        $this->setupControllers();

        $this->generateAPIToken();

        return $this->app;
    }

    private function setupUser(){
        $this->app['auth.userObj'] = new testing_uzr(5);
        $this->app['user'] = $this->app['auth.userObj']->getUID();
        $this->app['auth.user'] = array(
            'loggedIn' => $this->app['auth.userObj']->getIsLoggedIn(),
            'validated' => $this->app['auth.userObj']->getIsValidated(),
            'authorized' => $this->app['auth.userObj']->getIsAuthorized(),
            'uid' => $this->app['auth.userObj']->getUID(),
            'cid' => $this->app['auth.userObj']->getCid(),
            'level' => (int)$this->app['auth.userObj']->getLevel(),
            'emp_id' => $this->app['auth.userObj']->getUID(),
            'isEmployee' => $this->app['auth.userObj']->getIsEmployee()
        );
    }

    private function setupEmployee(){
        $pzn = new ForeupPeople();
        $pzn->setPersonId($this->app['auth.userObj']->getUID());

        $uzr = new ForeupUsers();
        $uzr->setPerson($pzn);

        $course = new ForeupCourses();
        $course->setCourseId($this->app['auth.userObj']->getCid());
        $this->app['employee'] = new ForeupEmployees();
        $this->app['employee']->setCourse($course);
        $this->app['employee']->setUserLevel($this->app['auth.userObj']->getLevel());
        $this->app['employee']->setPerson($pzn);
    }

    private function setupDoctrineORM(){
        $doctrineOrmServiceProvider = new DoctrineOrmServiceProvider();
        $doctrineOrmServiceProvider->register($this->app);
    }

    private function setupAppConfig(){
        $this->configuration = new testing_config($this->app);
        $this->configuration->loadConfig();
    }

    private function setupMiddleWare(){
        $this->middleware_loader = new testing_middleware_loader($this->app);
        $this->middleware_loader->load_middleware();
    }

    private function setupServices(){
        $this->services_loader = new testing_services_loader($this->app);
        $this->services_loader->register_services();
    }

    private function setupControllers(){
        $this->routes_loader = new testing_routes_loader($this->app);
        $this->routes_loader->bindRoutesToControllers();
    }

    private function generateAPIToken(){
        //generate authorization token for api
        $tokenController = $this->app['tokens.controller'];
        $request = new Request();

        $request->request->set('email', 'jackbarnes');
        $request->request->set('password', 'jackbarnes');
        $response = $tokenController->save($request);
        $token = $response->getContent();
        $token = json_decode($token, true);
        $token = $token['data']['id'];
        $this->app['auth.userObj']->setToken($token);
    }

    public function createMockDefaultApp()
    {
        return $this->createMockDefaultAppAndDeps();
    }

}