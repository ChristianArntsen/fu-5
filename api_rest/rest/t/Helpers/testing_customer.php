<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 5/17/2017
 * Time: 4:45 PM
 */

namespace foreup\rest\t\Helpers;


class testing_customer
{
    private $customerAttributes = [
        "7857" => [
            "username" => "",
            "account_number" => "4012000010000151",
            "company_name" => "0",
            "taxable" => true,
            "discount" => 5.6,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => "2017-05-19T10:40:42-0600",
            "account_balance" => "10.78",
            "photo" => "https://s3-us-west-2.amazonaws.com/foreup.application.files/6270/thumbnails/DSCF0045.JPG",
            "deleted" => 0,
            "member_account_balance" => "-711.87",
            "email_subscribed" => true,
            "groups" => [
                0 => [],
                1 => []
            ],
            "contact_info" => [
                "id" => "7857",
                "first_name" => "Jimmy \"The Rocket\"",
                "last_name" => "Aarons",
                "phone_number" => "8013693765",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => "2000-05-23T00:00:00-0600",
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "Z13-E04",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
	        'course' => 'Cascade Golf Course'
        ],
        "2073" => [
            "username" => "",
            "account_number" => "AAAA",
            "company_name" => "0",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "-1032.10",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/AAAA_AAAA.png",
            "deleted" => 0,
            "member_account_balance" => "-100.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "2073",
                "first_name" => "AAAA",
                "last_name" => "AAAA",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "AL",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => "Best customer ever!!"
            ],
	        'course' => 'Cascade Golf Course'
        ],
        "849" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => false,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Shepherd.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [
                0 => []
            ],
            "contact_info" => [
                "id" => "849",
                "first_name" => "Fred",
                "last_name" => "Shepherd",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "2365" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "23.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Bray.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "2365",
                "first_name" => "Fred",
                "last_name" => "Bray",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "7606" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Bray.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "7606",
                "first_name" => "Fred",
                "last_name" => "Bray",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "2436" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Brown.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "2436",
                "first_name" => "Fred",
                "last_name" => "Brown",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "2580" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred J_Campbell.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "2580",
                "first_name" => "Fred J",
                "last_name" => "Campbell",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "7791" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred J_Campbell.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "7791",
                "first_name" => "Fred J",
                "last_name" => "Campbell",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "2886" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Craner.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "2886",
                "first_name" => "Fred",
                "last_name" => "Craner",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "2921" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Culverhouse.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "2921",
                "first_name" => "Fred",
                "last_name" => "Culverhouse",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "1197" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => false,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/S_Fredrickson.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [
                0 => []
            ],
            "contact_info" => [
                "id" => "1197",
                "first_name" => "S",
                "last_name" => "Fredrickson",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "3787" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Haase.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "3787",
                "first_name" => "Fred",
                "last_name" => "Haase",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "5804" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Peterson.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "5804",
                "first_name" => "Fred",
                "last_name" => "Peterson",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "5817" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Phillips.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "5817",
                "first_name" => "Fred",
                "last_name" => "Phillips",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "6318" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/72154021badef53f8cc0374e55b45345?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Schultz.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "6318",
                "first_name" => "Fred",
                "last_name" => "Schultz",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "6318brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "7664" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Schultz.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "7664",
                "first_name" => "Fred",
                "last_name" => "Schultz",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "7156" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Wilde.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "7156",
                "first_name" => "Fred",
                "last_name" => "Wilde",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "7250" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Wright.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "7250",
                "first_name" => "Fred",
                "last_name" => "Wright",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "7263" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Fred_Wudell.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "7263",
                "first_name" => "Fred",
                "last_name" => "Wudell",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "850" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => false,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Karen_Shepherd.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [
                0 => []
            ],
            "contact_info" => [
                "id" => "850",
                "first_name" => "Karen",
                "last_name" => "Shepherd",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "6387" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Dale_Shepherd.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "6387",
                "first_name" => "Dale",
                "last_name" => "Shepherd",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ],
        "6388" => [
            "username" => "",
            "account_number" => null,
            "company_name" => "",
            "taxable" => true,
            "discount" => 0,
            "opt_out_email" => false,
            "opt_out_text" => false,
            "date_created" => null,
            "account_balance" => "0.00",
            "photo" => "https://www.gravatar.com/avatar/f307078decdb779a1a508b3885f194c2?r=pg&d=https://mobile.foreupsoftware.com/api_rest/index.php/customerImages/Dale_Shepherd.png",
            "deleted" => 0,
            "member_account_balance" => "0.00",
            "email_subscribed" => true,
            "groups" => [],
            "contact_info" => [
                "id" => "6388",
                "first_name" => "Dale",
                "last_name" => "Shepherd",
                "phone_number" => "",
                "cell_phone_number" => "",
                "email" => "brendon.beebe@gmail.com",
                "birthday" => null,
                "address_1" => "",
                "address_2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => "",
                "handicap_account_number" => "",
                "handicap_score" => 0,
                "comments" => ""
            ],
            'course' => 'Cascade Golf Course'
        ]
    ];

    private $customerGroups = [
        "7857" => [
            [
                "type" => "customerGroups",
                "id" => "27"
            ],
            [
                "type" => "customerGroups",
                "id" => "37"
            ]
        ],
        "849" => [
            [
                "type" => "customerGroups",
                "id" => "42"
            ]

        ],
        "850" => [
            [
                "type" => "customerGroups",
                "id" => "42"
            ]

        ],
        "1197" => [
            [
                "type" => "customerGroups",
                "id" => "46"
            ]
        ]
    ];

    private $groups = [
        "46" => [
            "type" => "customerGroups",
            "id" => "46",
            "attributes" => [
                "group_id" => 46,
                "label" => "Out of Area",
                "itemDiscount" => null
            ]
        ],
        "42" => [
            "type" => "customerGroups",
            "id" => "42",
            "attributes" => [
                "group_id" => 42,
                "label" => "Area",
                "itemDiscount" => null
            ]
        ],
        "27" => [
            "type" => "customerGroups",
            "id" => "27",
            "attributes" => [
                "group_id" => 27,
                "label" => "Men's League",
                "itemDiscount" => null
            ]
        ],
        "37" => [
            "type" => "customerGroups",
            "id" => "37",
            "attributes" => [
                "group_id" => 37,
                "label" => "Corporate Group",
                "itemDiscount" => null
            ]
        ]
    ];

    public function getCustomer($id){
        $relationship = [
            "customerGroups" => [
                "data" => []
            ]
        ];

        if(isset($this->customerGroups[$id])){
            $relationship['customerGroups']['data'] = $this->customerGroups[$id];
        }

        $response = [
            "type" => "customers",
            "id" => $id,
            "attributes" => $this->customerAttributes[$id],
            "relationships" => $relationship
        ];

        return $response;
    }

    public function getCustomersResponse(array $ids){
        $response = [
            "data" => [

            ],
            "included" => [

            ],
            "meta" => [
                "total" => 0
            ]
        ];

        $addedGroups = [];
        foreach($ids as $id){
            $customer = $this->getCustomer($id);
            array_push($response['data'], $customer);

            $groups = $customer['relationships']['customerGroups']['data'];

            if(isset($groups) && !empty($groups)){
                foreach($groups as $group){
                    if(!isset($addedGroups[$group['id']])) {
                        array_push($response['included'], $this->groups[$group['id']]);
                        $addedGroups[$group['id']] = "set";
                    }
                }
            }
        }

        $response['meta']['total'] = count($response['data']);

        if(empty($response['included'])){
            unset($response['included']);
        }

        return $response;
    }

    public function getCustomerGroups($person_id){
        return $this->customerGroups[$person_id];
    }
}