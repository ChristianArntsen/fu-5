<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 6/5/2017
 * Time: 2:13 PM
 */

namespace foreup\rest\t\Helpers;

class testing_app_setup_teardown
{

    private $app, $controller, $controllerName;

    function __construct($controllerName)
    {
        $this->controllerName = $controllerName;
    }

    public function getApp(){
        return $this->app;
    }

    public function getController(){
        return $this->controller;
    }

    public function setup(&$app, &$controller){
        $factory = new MockDefaultAppFactory();
        $app = $factory->createMockDefaultApp();
        $this->app = $app;

        $controller = $this->app[$this->controllerName];
        $controller->startTransaction();
        $this->controller = $controller;
    }

    public function tearDown(){
        $conn = $this->app['orm.em']->getConnection();
        $conn->close();
        $this->controller->rollback();
    }
}