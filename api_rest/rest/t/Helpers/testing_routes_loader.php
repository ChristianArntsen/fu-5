<?php

namespace foreup\rest\t\Helpers;

use foreup\rest\application\routes_loader;

class testing_routes_loader extends routes_loader {
    // allow non Application to be passed in for testing
    public function __construct(&$app)
    {
        $this->app = $app;

        $this->instantiateControllers();
    }
}