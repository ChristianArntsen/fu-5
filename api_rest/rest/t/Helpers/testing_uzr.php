<?php

namespace foreup\rest\t\Helpers;


class testing_uzr {
    private $level;
    private $UID = 12;
    private $appId = 134476472;
    private $CID = 6270;
    private $employee = true;
    private $loggedIn = true;
    private $validated = true;
    private $authorized = true;
    private $app = null;
    private $token = null;

    public function __construct($lvl=5)
    {
        $tokenController = null;
        $this->setLevel($lvl);
    }

    public function setToken($token){
        $this->token = $token;
    }

    public function getToken(){
        return $this->token;
    }

    public function setLevel(int $lvl){
        $this->level = $lvl;
    }

    public function getLevel() {
        return $this->level;
    }

    public function getUID(){
        return $this->UID;
    }

    public function setUID(int $uid){
        $this->UID = $uid;
    }

    public function getEmpId(){
        return $this->UID;
    }

    public function getAppId(){
        return $this->appId;
    }

    public function setAppId(int $appID){
        $this->appId = $appID;
    }

    public function getIsEmployee(){
        return $this->employee;
    }

    public function setIsEmployee(bool $isEmp){
        $this->employee = $isEmp;
    }

    public function getCid() {
        return $this->CID;
    }

    public function setCid(int $cid) {
        $this->CID = $cid;
    }

    public function getIsLoggedIn(){
        return $this->loggedIn;
    }

    public function setIsLoggedIn(bool $loggedIn){
        $this->loggedIn = $loggedIn;
    }

    public function getIsValidated(){
        return $this->validated;
    }

    public function setIsValidated(bool $valid){
        $this->validated = $valid;
    }

    public function getIsAuthorized(){
        return $this->authorized;
    }

    public function setIsAuthorized(bool $auth){
        $this->authorized = $auth;
    }

    public function hasLimitation($limitation){
        return true;
    }
}
