<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupCustomerCreditCards;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupPeople;

class testCustomerCreditCardsEntity  extends EntityTestingUtility
{
	private $card;

	protected function _before()
	{
		$this->card = new ForeupCustomerCreditCards();
		$this->card->setCourseId(1234);
		$person = new ForeupPeople();
		$person->testing_override('personId',1234);
		$course = new ForeupCourses();
		$course->testing_override('courseId',1234);
		$customer = new ForeupCustomers();
		$customer->setPerson($person);
		$customer->setCourse($course);
		$this->card->setCustomer($customer);
		$this->card->setToken('erw34yu89gsrhuiohgsioph3');
		$this->card->setTokenExpiration(new \DateTime('1234-05-06T07:08:09Z'));
		$this->card->setCardType('VISA');
		$this->card->setMaskedAccount('xxxxxxxxxxxxxxxxx23425');
	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$card = $this->card;
		$this->assertTrue(method_exists($card,'validate'));
		$this->assertTrue(method_exists($card,'getCreditCardId'));
		$this->assertTrue(method_exists($card,'getId'));
		$this->assertTrue(method_exists($card,'getEtsKey'));
		$this->assertTrue(method_exists($card,'setEtsKey'));
		$this->assertTrue(method_exists($card,'getMercuryId'));
		$this->assertTrue(method_exists($card,'setMercuryId'));
		$this->assertTrue(method_exists($card,'getMercuryPassword'));
		$this->assertTrue(method_exists($card,'setMercuryPassword'));
		$this->assertTrue(method_exists($card,'getRecurring'));
		$this->assertTrue(method_exists($card,'setRecurring'));
		$this->assertTrue(method_exists($card,'getCourseId'));
		$this->assertTrue(method_exists($card,'setCourseId'));
		$this->assertTrue(method_exists($card,'getCustomer'));
		$this->assertTrue(method_exists($card,'setCustomer'));
		$this->assertTrue(method_exists($card,'getTeeTimeId'));
		$this->assertTrue(method_exists($card,'setTeeTimeId'));
		$this->assertTrue(method_exists($card,'getToken'));
		$this->assertTrue(method_exists($card,'setToken'));
		$this->assertTrue(method_exists($card,'getTokenExpiration'));
		$this->assertTrue(method_exists($card,'setTokenExpiration'));
		$this->assertTrue(method_exists($card,'getCardType'));
		$this->assertTrue(method_exists($card,'setCardType'));
		$this->assertTrue(method_exists($card,'getCardholderName'));
		$this->assertTrue(method_exists($card,'setCardholderName'));
		$this->assertTrue(method_exists($card,'getExpiration'));
		$this->assertTrue(method_exists($card,'setExpiration'));
		$this->assertTrue(method_exists($card,'getMaskedAccount'));
		$this->assertTrue(method_exists($card,'setMaskedAccount'));
		$this->assertTrue(method_exists($card,'getDeleted'));
		$this->assertTrue(method_exists($card,'setDeleted'));
		$this->assertTrue(method_exists($card,'getSuccessfulCharges'));
		$this->assertTrue(method_exists($card,'setSuccessfulCharges'));
		$this->assertTrue(method_exists($card,'getFailedCharges'));
		$this->assertTrue(method_exists($card,'setFailedCharges'));
		$this->assertTrue(method_exists($card,'getStatus'));
		$this->assertTrue(method_exists($card,'setStatus'));
	}

	public function testGetSet()
	{
		$complete = true;
		$card = $this->card;
		$location = 'ForeupCustomerCreditCards->validate';
		$this->assertTrue($card->validate());

		$this->string_tester($card,$location,'etsKey','getEtsKey','setEtsKey',true);
		$this->string_tester($card,$location,'mercuryId','getMercuryId','setMercuryId',true);
		$this->string_tester($card,$location,'mercuryPassword','getMercuryPassword','setMercuryPassword',true);
		$this->boolean_tester($card,$location,'recurring','getRecurring','setRecurring',true);
		$this->integer_tester($card,$location,'courseId','getCourseId','setCourseId',true);
		$this->string_tester($card,$location,'teeTimeId','getTeeTimeId','setTeeTimeId',true);
		$this->string_tester($card,$location,'token','getToken','setToken',true);
		$this->datetime_tester($card,$location,'tokenExpiration','getTokenExpiration','setTokenExpiration',true);
		$this->string_tester($card,$location,'cardType','getCardType','setCardType',true);
		$this->string_tester($card,$location,'cardholderName','getCardholderName','setCardholderName',true);
		$this->datetime_tester($card,$location,'expiration','getExpiration','setExpiration',true);
		$this->string_tester($card,$location,'maskedAccount','getMaskedAccount','setMaskedAccount',true);
		$this->boolean_tester($card,$location,'deleted','getDeleted','setDeleted',true);
		$this->integer_tester($card,$location,'successfulCharges','getSuccessfulCharges','setSuccessfulCharges',true);
		$this->integer_tester($card,$location,'failedCharges','getFailedCharges','setFailedCharges',true);
		$this->string_tester($card,$location,'status','getStatus','setStatus',true);

		$this->assertTrue($complete);
	}
}