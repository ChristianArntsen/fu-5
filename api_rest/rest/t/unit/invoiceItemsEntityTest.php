<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupInvoiceItems;
use foreup\rest\models\entities\ForeupInvoices;

require_once ('EntityTestingUtility.php');

class testStatementItemsEntity  extends EntityTestingUtility
{
	private $item;

	protected function _before()
	{
		$this->item = new ForeupInvoiceItems();
		$this->item->setStatement(new ForeupInvoices());


	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$item = $this->item;
		$verbs = ['get','set'];
		$nouns = ['Statement','LineNumber','Item','Description','Quantity','Amount','PaidAmount',
		          'PaidOff','Tax','PayAccountBalance','PayMemberBalance'];
		$this->interface_tester($item,$verbs,$nouns);
	}

	public function testGetSet()
	{
		$location ='ForeupStatementItems->validate';
		$item = $this->item;

		$this->assertTrue($item->validate());
		$fields = [['integer','lineNumber',true],['string','description',true],['integer','quantity',true],
		           ['numeric','amount',true],['numeric','paidAmount',true],['datetime','paidOff',false],
				   ['numeric','tax',true],['boolean','payAccountBalance',true],['boolean','payMemberBalance',true]];
		$this->get_set_tester($item,$location,$fields);
	}
}