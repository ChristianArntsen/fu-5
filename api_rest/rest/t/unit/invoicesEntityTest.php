<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupInvoices;

class testInvoicesEntity  extends EntityTestingUtility
{
	private $invoice;

	protected function _before()
	{
		$this->invoice = new ForeupInvoices();
		$this->invoice->setInvoiceNumber(12345);
		$this->invoice->setCourseId(12345);
		$this->invoice->setBillStart(new \DateTime());
		$this->invoice->setBillEnd(new \DateTime());
		$this->invoice->setTotal(1.23);
		$this->invoice->setLastBillingAttempt(new \DateTime());
		$this->invoice->setSendDate(new \DateTime());
		$this->invoice->setDueDate(new \DateTime());
		$this->invoice->setAutoBillDate(new \DateTime());

	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$invoice = $this->invoice;

		$this->assertTrue(method_exists($invoice,'validate'));
		$this->assertTrue(method_exists($invoice,'getStatementId'));
		$this->assertTrue(method_exists($invoice,'getCreditCardPaymentId'));

		$verbs = ['get','set'];
		$nouns = ['InvoiceNumber','Name','Department','Category',
			'Subcategory','Day','CourseId','CreditCard','BillingId',
			'Person','Employee','Date','BillStart','BillEnd','Total',
			'OverdueTotal','PreviousPayments','Paid','Overdue','Deleted',
			'CreditCardPayment','EmailInvoice','LastBillingAttempt',
			'Started','ChargedNoSale','Charged','Emailed','SendDate',
			'DueDate','AutoBillDate','PayCustomerAccount','PayMemberAccount',
			'ShowAccountTransactions','Sale','Notes','AttemptLimit'];

		$this->interface_tester($invoice,$verbs,$nouns);

	}

	public function testGetSet(){
		$statement = $this->invoice;
		$location = 'ForeupInvoices->validate';
		$this->assertTrue($statement->validate());

		$fields = [['integer','invoiceNumber',true],['string','name',true],['string','department',true],
		['string','category',true],['string','subcategory',true],['boolean','day',true],
		['integer','billingId',true],['datetime','date',false],['datetime','billStart',false],
		['datetime','billEnd',false],['numeric','total',true],['numeric','overdueTotal',true],
		['numeric','previousPayments',true],['numeric','paid',true],['numeric','overdue',true],
		['boolean','deleted',true],['boolean','emailInvoice',true],['datetime','lastBillingAttempt',false],
		['boolean','started',true],['boolean','chargedNoSale',true],['boolean','charged',true],
		['boolean','emailed',true],['datetime','dueDate',false],['datetime','sendDate',false],
		['datetime','autoBillDate',false],['boolean','payCustomerAccount',true],
		['boolean','payMemberAccount',true],['boolean','showAccountTransactions',true],
		['string','notes',true],['integer','attemptLimit',true]];
		$this->assertEquals(30,count($fields));
		$this->get_set_tester($statement,$location,$fields);

	}
}
?>
