<?php
namespace foreup\rest\unit;

use foreup\rest\models\entities\ForeupAccountTransactions;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\entities\ForeupSales;
use foreup\rest\resource_transformers\account_transactions_transformer;

class testAccountTransactionsTransformer  extends \Codeception\TestCase\Test
{
	public function testInterface()
	{
		$transformer = new account_transactions_transformer();
		$this->assertTrue(method_exists($transformer, 'transform'));
		$this->assertTrue(method_exists($transformer, 'includeSale'));
		$this->assertTrue(method_exists($transformer, 'includeCustomer'));
	}

	public function testTransform()
	{
		$transformer = new account_transactions_transformer();

		$transaction = new ForeupAccountTransactions();
		$transaction->testing_override('id',1);
		$transaction->setCourseId(6270);
		$transaction->setAccountType('customer');
		$transaction->setTransDate(new \DateTime());
		$transaction->setTransComment('valid comment');
		$transaction->setTransDescription('valid description');
		$transaction->setTransAmount(11.12);
		//$transaction->setBalanceTransfer(true);
		$transaction->setRunningBalance(15.16);
		$transaction->setHideOnInvoices(1);

		$sale = new ForeupSales();
		$sale->testing_override('saleId',123);

		$transaction->setSale($sale);

		$transaction->setTransHousehold(3399);

		$person = new ForeupPeople();
		$person->testing_override('personId',3399);
		$customer = new ForeupCustomers();
		$customer->setPerson($person);
		$customer->setCourseId(6270);

		$transaction->setCustomer($customer);

		$transaction->validate();

		$result = $transformer->transform($transaction);

		$this->assertEquals($transaction->getTransId(),$result['id']);
		$this->assertEquals($transaction->getCourseId(),$result['courseId']);
		$this->assertEquals($transaction->getAccountType(),$result['accountType']);
		$this->assertEquals($transaction->getTransDate()->format(\DateTime::ISO8601),$result['transDate']);
		$this->assertEquals($transaction->getTransComment(),$result['transComment']);
		$this->assertEquals($transaction->getTransDescription(),$result['transDescription']);
		$this->assertEquals($transaction->getTransAmount(),$result['transAmount']);
		//$this->assertEquals($transaction->getBalanceTransfer(),$result['balanceTransfer']);
		$this->assertEquals($transaction->getRunningBalance(),$result['runningBalance']);
		$this->assertEquals($transaction->getHideOnInvoices(),$result['hideOnInvoices']);
	}
}

?>
