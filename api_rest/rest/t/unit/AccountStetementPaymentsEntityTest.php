<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupAccountStatementPayments;
use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupPeople;

include_once('EntityTestingUtility.php');

class testAccountStatementPaymentsEntity  extends EntityTestingUtility
{
	/**
	 * @var ForeupAccountStatements
	 */
	protected $statement;

	/**
	 * @var ForeupAccountStatementPayments
	 */
	protected $payment;

	protected function _before()
	{
		$this->statement = new ForeupAccountStatements();
		$this->statement->setDateCreated(new \DateTime('2016-12-25T12:12:12Z'));
		$this->statement->setStartDate(new \DateTime('2016-12-25T12:12:12Z'));
		$this->statement->setEndDate(new \DateTime('2016-12-25T12:12:12Z'));
		$this->statement->setCreatedBy(1);
		$this->statement->setOrganizationId(1);
		$customer = new ForeupCustomers();
		$person = new ForeupPeople();
		$person->testing_override('personId',1);
		$customer->setPerson($person);
		$this->statement->setCustomer($customer);

		$this->payment = new ForeupAccountStatementPayments();
		$this->payment->setStatement($this->statement);
		$this->payment->setDateCreated(new \DateTime());

	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$payment = $this->payment;

		$this->assertTrue(method_exists($payment,'validate'));
		$this->assertTrue(method_exists($payment,'getId'));

		$verbs = ['get','set'];
		$nouns = ['Statement','Amount','Type','SubType',
			'AccountNumber','Name','Sale','ChargeInvoice',
			'Customer','IsAutopayment','DateCreated'];

		$this->interface_tester($payment,$verbs,$nouns);
	}

	public function testGetSet()
	{

		$payment = $this->payment;
		$location = 'ForeupAccountStatementPayments->validate';
		$this->assertTrue($payment->validate());

		$fields = [
			['numeric','amount',true],['string','accountNumber',false],['string','name',false],
			['boolean','isAutopayment',true],['datetime','dateCreated',true]
		];
		//$this->assertEquals(17,count($fields));
		$this->get_set_tester($payment,$location,$fields);

		$valid = ['credit_card','ach'];
		$this->enum_tester($payment,$location,'type',$valid,'getType','setType',true);
		$valid = ['visa','mastercard','discover','jcb','diners','amex'];
		$this->enum_tester($payment,$location,'subType',$valid,'getSubType','setSubType',true);
	}

}


?>