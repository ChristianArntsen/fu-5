<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupAccountStatementCharges;
use foreup\rest\models\entities\ForeupAccountStatements;

include_once('EntityTestingUtility.php');

class testAccountStatementChargessEntity  extends EntityTestingUtility
{
	use \Codeception\Specify;

	protected $statement;

	protected function _before()
	{
		$this->statement = new ForeupAccountStatementCharges();
		$this->statement->setDateCharged(new \DateTime('2016-12-25T12:12:12Z'));
		$this->statement->setLine(1);

	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$statement = $this->statement;

		$this->assertTrue(method_exists($statement, 'validate'));
		$this->assertTrue(method_exists($statement, 'invalid'));
		$this->assertTrue(method_exists($statement, 'getId'));
		$this->assertTrue(method_exists($statement, 'getStatement'));
		$this->assertTrue(method_exists($statement, 'setStatement'));
		$this->assertTrue(method_exists($statement, 'getDateCharged'));
		$this->assertTrue(method_exists($statement, 'setDateCharged'));
		$this->assertTrue(method_exists($statement, 'getLine'));
		$this->assertTrue(method_exists($statement, 'setLine'));
		$this->assertTrue(method_exists($statement, 'getName'));
		$this->assertTrue(method_exists($statement, 'setName'));
		$this->assertTrue(method_exists($statement, 'getItemId'));
		$this->assertTrue(method_exists($statement, 'getItem'));
		$this->assertTrue(method_exists($statement, 'setItem'));
		$this->assertTrue(method_exists($statement, 'getSaleId'));
		$this->assertTrue(method_exists($statement, 'setSale'));
		$this->assertTrue(method_exists($statement, 'getTotal'));
		$this->assertTrue(method_exists($statement, 'setTotal'));
		$this->assertTrue(method_exists($statement, 'getSubtotal'));
		$this->assertTrue(method_exists($statement, 'setSubtotal'));
		$this->assertTrue(method_exists($statement, 'getTax'));
		$this->assertTrue(method_exists($statement, 'setTax'));
		$this->assertTrue(method_exists($statement, 'getQty'));
		$this->assertTrue(method_exists($statement, 'setQty'));
		$this->assertTrue(method_exists($statement, 'getUnitPrice'));
		$this->assertTrue(method_exists($statement, 'setUnitPrice'));
		$this->assertTrue(method_exists($statement, 'getTaxPercentage'));
		$this->assertTrue(method_exists($statement, 'setTaxPercentage'));
	}

	public function testGetSet()
	{
		$location = 'ForeupAccountStatementCharges->validate';
		$entity = $this->statement;
		$this->assertTrue($entity->validate());

		$this->DateTime_tester($entity,$location,'dateCharged','getDateCharged','setDateCharged',true);
		$this->integer_tester($entity,$location,'line','getLine','setLine',true);
		$this->string_tester($entity,$location,'name','getName','setName',false);
		$this->numeric_tester($entity,$location,'total','getTotal','setTotal',true);
		$this->numeric_tester($entity,$location,'subtotal','getsubTotal','setSubtotal',true);
		$this->numeric_tester($entity,$location,'tax','getTax','setTax',true);
		$this->numeric_tester($entity,$location,'qty','getQty','setQty',true);
		$this->numeric_tester($entity,$location,'unitPrice','getUnitPrice','setUnitPrice',true);
		$this->numeric_tester($entity,$location,'taxPercentage','getTaxPercentage','setTaxPercentage',true);





	}

}

?>

