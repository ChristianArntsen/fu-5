<?php
namespace foreup\rest\unit;

use Dflydev\Pimple\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Application;
use foreup\rest\t\Helpers\testing_uzr;
use foreup\rest\t\Helpers\MockDefaultAppFactory;
use foreup\rest\t\Helpers\testing_middleware_loader;
use foreup\rest\t\Helpers\testing_config;
use foreup\rest\t\Helpers\testing_services_loader;
use foreup\rest\t\Helpers\testing_routes_loader;


class testControllerInterface  extends \Codeception\TestCase\Test
{
	private $app, $middleware_loader, $configuration, $routes_loader, $services_loader;

	protected function _before()
	{
		$factory = new MockDefaultAppFactory();
		$this->app = $factory->createMockDefaultApp();
		$doctrineOrmServiceProvider = new DoctrineOrmServiceProvider();
		$doctrineOrmServiceProvider->register($this->app);
		$this->configuration = new testing_config($this->app);
		$this->configuration->loadConfig();
		$this->middleware_loader = new testing_middleware_loader($this->app);
		$this->middleware_loader->load_middleware();
		$this->services_loader = new testing_services_loader($this->app);
		$this->services_loader->register_services();
		$this->routes_loader = new testing_routes_loader($this->app);
		$this->routes_loader->bindRoutesToControllers();
	}

	protected function _after()
	{
		$conn = $this->app['orm.em']->getConnection();
		$conn->close();
	}

	public function testCoursesController(){
		$controller = $this->app['courses.controller'];
		$this->assertTrue(method_exists($controller,'__construct'));
		$this->assertTrue(method_exists($controller,'get'));
		$this->assertTrue(method_exists($controller,'getAll'));
		$this->assertTrue(method_exists($controller,'reallyGetAll'));
		$this->assertTrue(method_exists($controller,'getSuggestions'));
		$this->assertTrue(method_exists($controller,'patch'));
		$this->assertTrue(method_exists($controller,'mobileToken'));
	}
/*
 * // Dead controller
	public function testOrganizationsController(){
		$controller = $this->app['organizations.controller'];
		$this->assertTrue(method_exists($controller,'__construct'));
		$this->assertTrue(method_exists($controller,'get'));
		$this->assertTrue(method_exists($controller,'getAll'));
	}
*/
	public function testSalesController(){
		$controller = $this->app['sales.controller'];
		$this->assertTrue(method_exists($controller,'__construct'));
		$this->assertTrue(method_exists($controller,'getAll'));
		$this->assertTrue(method_exists($controller,'getMissingDataPoints'));
		$this->assertTrue(method_exists($controller,'create'));
	}

	public function testTeesheetsController(){
		$controller = $this->app['teesheets.controller'];
		$this->assertTrue(method_exists($controller,'__construct'));
		$this->assertTrue(method_exists($controller,'get'));
	}

	public function testModulesController(){
		$controller = $this->app['modules.controller'];
		$this->assertTrue(method_exists($controller,'__construct'));
		$this->assertTrue(method_exists($controller,'get'));
	}

	public function testMarketingCampaignsController(){
		$controller = $this->app['marketing_campaigns.controller'];
		$this->assertTrue(method_exists($controller,'__construct'));
		$this->assertTrue(method_exists($controller,'get'));
		$this->assertTrue(method_exists($controller,'getAll'));
		$this->assertTrue(method_exists($controller,'promoteToSent'));
		$this->assertTrue(method_exists($controller,'demoteToDraft'));
		$this->assertTrue(method_exists($controller,'delete'));
		$this->assertTrue(method_exists($controller,'save'));
		$this->assertTrue(method_exists($controller,'update'));
	}

	public function testTokensController(){
		$controller = $this->app['tokens.controller'];
		$this->assertTrue(method_exists($controller,'__construct'));
		$this->assertTrue(method_exists($controller,'save'));
	}

	public function testScoresController(){
		$controller = $this->app['scores.controller'];
		$this->assertTrue(method_exists($controller,'__construct'));
		$this->assertTrue(method_exists($controller,'save'));
	}

	public function testEmployeeAuditLogController(){
		$controller = $this->app['employee_audit_log.controller'];
		$this->assertTrue(method_exists($controller,'__construct'));
		$this->assertTrue(method_exists($controller,'save'));
		$this->assertTrue(method_exists($controller,'getAll'));
		$this->assertTrue(method_exists($controller,'get_course_log_entries'));
		$this->assertTrue(method_exists($controller,'get_employee_log_entries'));
		$this->assertTrue(method_exists($controller,'redo_logged_action'));
		$this->assertTrue(method_exists($controller,'undo_logged_action'));
	}

	public function testPersonAlteredController(){
		$controller = $this->app['person_altered.controller'];
		$this->assertTrue(method_exists($controller,'__construct'));
		$this->assertTrue(method_exists($controller,'save'));
		$this->assertTrue(method_exists($controller,'get_course_log_entries'));
		$this->assertTrue(method_exists($controller,'get_employee_log_entries'));
	}

}