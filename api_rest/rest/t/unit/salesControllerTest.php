<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 5/8/2017
 * Time: 12:09 PM
 */

namespace foreup\rest\unit;

use foreup\rest\t\traits\mock_app_trait;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;

class salesControllerTest extends \Codeception\TestCase\Test
{
    use mock_app_trait;

    private $basic_data, $request;

    protected function _before()
    {
        $this->setupController('sales.controller', false);
        $this->basic_data = json_decode('{
            "attributes":{
                "saleTime":"",
                "comment":"",
                "total":100.00,
                "subtotal":93.46,
                "tax":6.54,
                "employeeId":12,
                "terminalId":1,
                "items":{
                    "1":{
                        "type":"sales_items",
                        "attributes":{
                            "itemId":"5177",
                            "total":100.00,
                            "subTotal":93.46,
                            "tax":6.54,
                            "quantity":1,
                            "unitPrice":100.00,
                            "discountPercent":0,
                            "description":""
                        }
                    }
                },
                "payments":{
                    "1":{
                        "type":"sales_payments",
                        "attributes":{
                            "paymentAmount":100.00,
                            "invoiceId":"",
                            "type":"cash"
                        }
                    }
                }
            }
        }', true);
        $this->basic_data->attributes->saleTime = Carbon::now()->toTimeString();
        $this->request = new Request();
        $this->request->request->set('data',$this->basic_data);
    }

    protected function _after()
    {
        $this->tearDownController();
    }

    protected function validateError(JsonResponse $response, string $error, int $errorNumber = 403){
        $this->assertEquals($errorNumber, $response->getStatusCode());

        $response = $response->getContent();
        $response = json_decode($response,true);
        $this->assertNotNull($response);
        $this->assertEquals($response['success'], false);
        $this->assertNotEmpty($response['title']);
        $this->assertEquals($response['title'],$error);
    }

    public function testCreateWithTaxIncludedItem(){

        $response = $this->controller->create($this->request, 6270);
        $this->assertEquals($response->getStatusCode(),200);

        $response = $response->getContent();
        $response = json_decode($response,true)['data'];
        $this->assertNotNull($response);
        $this->assertEquals($response['type'], "sales");
        $this->assertNotEmpty($response['attributes']);
        $this->assertEquals("Cash: $100.00 - ", $response['attributes']['paymentType']);
        $this->assertEquals($response['attributes']['total'],100);
        $this->assertEquals($response['attributes']['subtotal'],93.46);
        $this->assertEquals($response['attributes']['tax'],6.54);
        $this->assertEquals($response['attributes']['deleted'],false);
        $this->assertNull($response['attributes']['deletedAt']);
    }

//    public function testCreateWithRegularItem(){
//        $total = 105.00;
//        $subtotal = 100.00;
//        $tax = 5.00;
//        $this->basic_data['attributes']['items']['1']['attributes']['itemId'] = "5179";
//        $this->basic_data['attributes']['items']['1']['attributes']['total'] = $total;
//        $this->basic_data['attributes']['total'] = $total;
//        $this->basic_data['attributes']['items']['1']['attributes']['subTotal'] = $subtotal;
//        $this->basic_data['attributes']['subtotal'] = $subtotal;
//        $this->basic_data['attributes']['items']['1']['attributes']['tax'] = $tax;
//        $this->basic_data['attributes']['tax'] = $tax;
//        $this->basic_data['attributes']['payments']['1']['attributes']['paymentAmount'] = $total;
//        $this->request->request->set('data',$this->basic_data);
//
//        $response = $this->controller->create($this->request, 6270);
//        $this->assertEquals($response->getStatusCode(),200);
//
//        $response = $response->getContent();
//        $response = json_decode($response,true)['data'];
//        $this->assertNotNull($response);
//        $this->assertEquals($response['type'], "sales");
//        $this->assertNotEmpty($response['attributes']);
//        $this->assertEquals("Cash: $105.00 - ", $response['attributes']['paymentType']);
//        $this->assertEquals($total, $response['attributes']['total']);
//        $this->assertEquals($subtotal, $response['attributes']['subtotal']);
//        $this->assertEquals($tax, $response['attributes']['tax']);
//        $this->assertEquals(false, $response['attributes']['deleted']);
//        $this->assertNull($response['attributes']['deletedAt']);
//    }

    public function testCreateWithoutItems(){
        $this->basic_data['attributes']['items']= [];
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->assertEquals($response->getStatusCode(),403);

        $response = $response->getContent();
        $response = json_decode($response,true);
        $this->assertNotNull($response);
        $this->assertEquals($response['success'], false);
        $this->assertNull($response['title']);
    }

    public function testCreateWithMissingItemType(){
        $this->basic_data['attributes']['items']['1']['type'] = "";
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->validateError($response, "Invalid item object, expected 'sales_items' but received ");
    }

    public function testCreateWithMissingItemId(){
        $this->basic_data['attributes']['items']['1']['attributes']['itemId'] = "";
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->validateError($response, "Missing required field: itemId");
    }

    public function testCreateWithInvalidItemId(){
        $this->basic_data['attributes']['items']['1']['attributes']['itemId'] = "231468764646";
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->validateError($response, "We can't find the itemId: 231468764646");
    }

    public function testCreateWithMissingPaymentType(){
        $this->basic_data['attributes']['payments']['1']['type'] = "";
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->validateError($response, "Invalid payment object, expected 'sales_payments' but received ``");
    }

    public function testCreateWithInvalidPaymentMethod(){
        $this->basic_data['attributes']['payments']['1']['attributes']['type'] = "";
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->validateError($response, "Invalid payment type, valid payment types include: credit_card,cash,change,member_account,gift_card,punch_card");
    }

    public function testCreateWithPartialPayment(){
        $this->basic_data['attributes']['items']['1']['attributes']['quantity'] = 2;
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->validateError($response, "Sale completion failed, total due has not been met");
    }

    public function testCreateWithMissingSalesAttributes(){
        unset($this->basic_data['attributes']['employeeId']);
        unset($this->basic_data['attributes']['terminalId']);
        unset($this->basic_data['attributes']['items']);
        unset($this->basic_data['attributes']['payments']);
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->validateError($response, "The request is missing the following fields: employeeId,terminalId,items,payments");
    }

    public function testCreateWithMissingAttribute(){
        unset($this->basic_data['attributes']);
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->validateError($response, "Missing attribute field", 400);
    }

    public function testCreateWithInvalidEmpID(){
        $this->basic_data['attributes']['employeeId'] = 123434648;
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->validateError($response, "Unable to find the specified employee.");
    }

    public function testCreateWithInvalidTerminal(){
        $this->basic_data['attributes']['terminalId'] = 123434648;
        $this->request->request->set('data',$this->basic_data);

        $response = $this->controller->create($this->request, 6270);
        $this->validateError($response, "Unable to find the specified terminal.");
    }
}