<?php

namespace foreup\rest\unit\controllers\courses;

use foreup\rest\t\traits\mock_app_trait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 5/11/2017
 * Time: 9:47 AM
 */
class settingsControllerTest  extends \Codeception\TestCase\Test
{
    use mock_app_trait;

    private $basic_data, $request;

    protected function _before()
    {
        $this->setupController('courseSettings.controller');

        $this->basic_data = json_decode('{
            "type":"settings",
            "id":"6270",
            "attributes":{
                "afterSaleLoad":false,
                "afternoonHoursBegin":"2399",
                "afternoonHoursEnd":"2399",
                "allowEmployeeRegisterLogBypass":false,
                "alwaysListAll":false,
                "autoEmailNoPrint":false,
                "autoEmailReceipt":false,
                "autoGratuity":"18.00",
                "autoGratuityThreshold":0,
                "autoSplitTeetimes":false,
                "billingEmail":"jhopkins@foreup.com, signorehopkins@gmail.com",
                "blindClose":true,
                "bookingRules":"",
                "businessAddress":"The place",
                "businessCity":"Cityville",
                "businessName":"Cascade Golf Course",
                "businessState":"UT",
                "cashDrawerOnCash":true,
                "cashDrawerOnSale":true,
                "closeTime":"1800",
                "country":"USA",
                "courseFiringIncludeItems":false,
                "creditCardFee":"0.00",
                "creditCategory":true,
                "creditCategoryName":"all",
                "creditDepartment":false,
                "creditDepartmentName":"all",
                "creditLimitToSubtotal":false,
                "currentDayCheckinsOnly":false,
                "customerCreditNickname":"Pro Shop",
                "dateFormat":"big_endian",
                "deductTips":true,
                "defaultKitchenPrinter":0,
                "defaultRegisterLogOpen":"100.00",
                "defaultTax1Name":"Sales Tax",
                "defaultTax1Rate":"6.75",
                "defaultTax2Cumulative":"0",
                "defaultTax2Name":"Sales Tax 2",
                "defaultTax2Rate":"1.05",
                "earlyBirdHoursBegin":"2399",
                "earlyBirdHoursEnd":"2399",
                "emailAddress":"123foreuptest@mailinator.com",
                "faxNumber":"(801) 555-5231",
                "fnbLogin":false,
                "foodBevSortBySeat":false,
                "foodBevTipLineFirstReceipt":false,
                "hideBackNine":false,
                "hideEmployeeLastNameReceipt":true,
                "hideModifierNamesKitchenReceipts":false,
                "hideTaxable":false,
                "holes":"18",
                "holidays":false,
                "includeTaxOnlineBooking":true,
                "invoiceEmailAddress":"jhopkins@foreup.com, signorehopkins@gmail.com",
                "limitFeeDropdownByCustomer":false,
                "mailchimpApiKey":"0",
                "memberBalanceNickname":"Making a nickname",
                "minimumFoodSpend":50,
                "morningHoursBegin":"2399",
                "morningHoursEnd":"2399",
                "noShowPolicy":"",
                "numberOfItemsPerPage":"500",
                "onlineBooking":false,
                "onlineBookingProtected":false,
                "onlineBookingWelcomeMessage":"",
                "onlineGiftcardPurchases":false,
                "onlineInvoiceTerminalId":0,
                "onlinePurchaseTerminalId":0,
                "openFri":true,
                "openMon":true,
                "openSat":true,
                "openSun":true,
                "openThu":true,
                "openTime":"0630",
                "openTue":true,
                "openWed":true,
                "phoneNumber":"5555555555",
                "postalCode":"84097",
                "printAfterSale":"1",
                "printCreditCardReceipt":true,
                "printMinimumsOnReceipt":false,
                "printSalesReceipt":true,
                "printSuggestedTip":false,
                "printTeeTimeDetails":false,
                "printTipLine":true,
                "printTwoReceipts":false,
                "printTwoReceiptsOther":true,
                "printTwoSignatureSlips":false,
                "receiptPrintAccountBalance":false,
                "receiptPrinter":"0",
                "requireCustomerOnSale":false,
                "requireEmployeePin":false,
                "requireGuestCount":false,
                "requireSignatureMemberPayments":false,
                "reservationEmail":"",
                "returnPolicy":"config_lorem ipsum",
                "sendReservationConfirmations":false,
                "separateCourses":false,
                "serviceFeeActive":false,
                "serviceFeeTax1Name":"Sales Tax",
                "serviceFeeTax1Rate":"0.00",
                "serviceFeeTax2Name":"Sales Tax 6",
                "serviceFeeTax2Rate":"0.00",
                "showInvoiceSaleItems":false,
                "sideBySideTeeSheets":false,
                "stackTeeSheets":false,
                "superTwilightHour":"2399",
                "teedOffColor":null,
                "teesheetUpdatesAutomatically":true,
                "teetimeDefaultToPlayer1":true,
                "termsAndConditions":"",
                "timeFormat":"24_hour",
                "timezone":"America\/Denver",
                "trackCash":true,
                "twilightHour":"2399",
                "unitPriceIncludesTax":false,
                "updatedPrinting":false,
                "useCourseFiring":false,
                "useEtsGiftcards":false,
                "useKitchenBuzzers":false,
                "useNewPermissions":false,
                "useTerminals":true,
                "webprnt":true,
                "webprntColdIp":"192.168.1.103",
                "webprntHotIp":"192.168.1.103",
                "webprntIp":"192.168.1.140",
                "webprntLabelIp":"",
                "websiteUrl":"foreup.com",
                "weekendFri":false,
                "weekendSat":true,
                "weekendSun":true,
                "loyaltyAutoEnroll":true,
                "reservationEmailText":"",
                "teeTimeCompletedColor":null,
                "useLoyalty":true
            }
        }', true);
        $this->basic_data['attributes']['reservationEmailText'] = '<p>Initial content</p>';

        $this->request = new Request();
    }

    protected function _after()
    {
        $this->tearDownController();
    }

    protected function validateError(JsonResponse $response, string $error, int $errorNumber = 403){

	    $this->request->request->set('data',$this->basic_data);
    	$this->assertEquals($errorNumber, $response->getStatusCode());

        $response = $response->getContent();
        $response = json_decode($response,true);
        $this->assertNotNull($response);
        $this->assertEquals($response['success'], false);
        $this->assertNotEmpty($response['title']);
        $this->assertEquals($response['title'],$error);
    }

    public function testGetAll(){
	    $this->request->request->set('data',$this->basic_data);
        $settings = $this->controller->getAll($this->request, 6270);
        $settings = $settings->getContent();
        $settings = json_decode($settings,true)['data'];

        // FIXME: Matching of arrays depends on local database settings
        $this->assertEquals($this->basic_data,$settings);
    }

    public function testGetAllInvalidEmployee(){
        $this->app['auth.userObj']->setIsEmployee(false);
        $settings = $this->controller->getAll($this->request, 1);

        $this->validateError($settings, "Permission Denied", 400);
    }

    public function testUpdateWithValidData(){
        $this->basic_data['attributes']["afterSaleLoad"] = true;
        $this->basic_data['attributes']["afternoonHoursBegin"] = "2299";
        $this->basic_data['attributes']["afternoonHoursEnd"] = "2299";
        $this->basic_data['attributes']["allowEmployeeRegisterLogBypass"] = true;
        $this->basic_data['attributes']["alwaysListAll"] = true;
        $this->basic_data['attributes']["autoEmailNoPrint"] = true;
        $this->basic_data['attributes']["autoEmailReceipt"] = true;
        $this->basic_data['attributes']["autoGratuity"] = "20.00";
        $this->basic_data['attributes']["autoGratuityThreshold"] = 1;
        $this->basic_data['attributes']["autoSplitTeetimes"] = true;
        $this->basic_data['attributes']["billingEmail"] = "jhopkins@mailinator.com";
        $this->basic_data['attributes']["blindClose"] = false;
        $this->basic_data['attributes']["bookingRules"] = "NO RULES!";
        $this->basic_data['attributes']["businessAddress"] = "845 N. The place";
        $this->basic_data['attributes']["businessCity"] = "foreUPVille";
        $this->basic_data['attributes']["businessName"] = "Cascade Golf Course 2";
        $this->basic_data['attributes']["businessState"] = "AZ";
        $this->basic_data['attributes']["cashDrawerOnCash"] = false;
        $this->basic_data['attributes']["cashDrawerOnSale"] = false;
        $this->basic_data['attributes']["closeTime"] = "1900";
        $this->basic_data['attributes']["country"] = "Canada";
        $this->basic_data['attributes']["courseFiringIncludeItems"] = true;
        $this->basic_data['attributes']["creditCardFee"] = "10.00";
        $this->basic_data['attributes']["creditCategory"] = false;
        $this->basic_data['attributes']["creditCategoryName"] = "credit charge";
        $this->basic_data['attributes']["creditDepartment"] = false;
        $this->basic_data['attributes']["creditDepartmentName"] = "Member Credit";
        $this->basic_data['attributes']["creditLimitToSubtotal"] = 1;
        $this->basic_data['attributes']["currentDayCheckinsOnly"] = true;
        $this->basic_data['attributes']["customerCreditNickname"] = "Customer Credit";
        $this->basic_data['attributes']["dateFormat"] = "little_endian";
        $this->basic_data['attributes']["deductTips"] = false;
        $this->basic_data['attributes']["defaultKitchenPrinter"] = 1;
        $this->basic_data['attributes']["defaultRegisterLogOpen"] = "150.00";
        $this->basic_data['attributes']["defaultTax1Name"] = "Arm";
        $this->basic_data['attributes']["defaultTax1Rate"] = "50.00";
        $this->basic_data['attributes']["defaultTax2Cumulative"] = "1";
        $this->basic_data['attributes']["defaultTax2Name"] = "Leg";
        $this->basic_data['attributes']["defaultTax2Rate"] = "50.00";
        $this->basic_data['attributes']["earlyBirdHoursBegin"] = "0500";
        $this->basic_data['attributes']["earlyBirdHoursEnd"] = "0700";
        $this->basic_data['attributes']["emailAddress"] = "jhopkins@mailinator.com";
        $this->basic_data['attributes']["faxNumber"] = "(801) 556-5231";
        $this->basic_data['attributes']["fnbLogin"] = true;
        $this->basic_data['attributes']["foodBevSortBySeat"] = true;
        $this->basic_data['attributes']["foodBevTipLineFirstReceipt"] = true;
        $this->basic_data['attributes']["hideBackNine"] = true;
        $this->basic_data['attributes']["hideEmployeeLastNameReceipt"] = true;
        $this->basic_data['attributes']["hideModifierNamesKitchenReceipts"] = true;
        $this->basic_data['attributes']["hideTaxable"] = true;
        $this->basic_data['attributes']["holes"] = "9";
        $this->basic_data['attributes']["holidays"] = true;
        $this->basic_data['attributes']["includeTaxOnlineBooking"] = false;
        $this->basic_data['attributes']["invoiceEmailAddress"] = "jhopkins@mailinator.com";
        $this->basic_data['attributes']["limitFeeDropdownByCustomer"] = true;
        $this->basic_data['attributes']["mailchimpApiKey"] = "1";
        $this->basic_data['attributes']["memberBalanceNickname"] = "Making a nickname 2";
        $this->basic_data['attributes']["minimumFoodSpend"] = 25;
        $this->basic_data['attributes']["morningHoursBegin"] = "0700";
        $this->basic_data['attributes']["morningHoursEnd"] = "0900";
        $this->basic_data['attributes']["noShowPolicy"] = "";
        $this->basic_data['attributes']["numberOfItemsPerPage"] = "750";
        $this->basic_data['attributes']["onlineBooking"] = true;
        $this->basic_data['attributes']["onlineBookingProtected"] = true;
        $this->basic_data['attributes']["onlineBookingWelcomeMessage"] = "YOLO!";
        $this->basic_data['attributes']["onlineGiftcardPurchases"] = true;
        $this->basic_data['attributes']["onlineInvoiceTerminalId"] = 12;
        $this->basic_data['attributes']["onlinePurchaseTerminalId"] = 12;
        $this->basic_data['attributes']["openFri"] = false;
        $this->basic_data['attributes']["openMon"] = false;
        $this->basic_data['attributes']["openSat"] = false;
        $this->basic_data['attributes']["openSun"] = false;
        $this->basic_data['attributes']["openThu"] = false;
        $this->basic_data['attributes']["openTime"] = "0430";
        $this->basic_data['attributes']["openTue"] = false;
        $this->basic_data['attributes']["openWed"] = false;
        $this->basic_data['attributes']["phoneNumber"] = "789-456-1245";
        $this->basic_data['attributes']["postalCode"] = "84057";
        $this->basic_data['attributes']["printAfterSale"] = "0";
        $this->basic_data['attributes']["printCreditCardReceipt"] = false;
        $this->basic_data['attributes']["printMinimumsOnReceipt"] = true;
        $this->basic_data['attributes']["printSalesReceipt"] = false;
        $this->basic_data['attributes']["printSuggestedTip"] = true;
        $this->basic_data['attributes']["printTeeTimeDetails"] = true;
        $this->basic_data['attributes']["printTipLine"] = false;
        $this->basic_data['attributes']["printTwoReceipts"] = true;
        $this->basic_data['attributes']["printTwoReceiptsOther"] = false;
        $this->basic_data['attributes']["printTwoSignatureSlips"] = true;
        $this->basic_data['attributes']["receiptPrintAccountBalance"] = true;
        $this->basic_data['attributes']["receiptPrinter"] = '3';
        $this->basic_data['attributes']["requireCustomerOnSale"] = true;
        $this->basic_data['attributes']["requireEmployeePin"] = true;
        $this->basic_data['attributes']["requireGuestCount"] = true;
        $this->basic_data['attributes']["requireSignatureMemberPayments"] = true;
        $this->basic_data['attributes']["reservationEmail"] = "joeblack@mailinator.com";
        $this->basic_data['attributes']["returnPolicy"] = "config_lorem ipsum. Read it and weep.";
        $this->basic_data['attributes']["sendReservationConfirmations"] = true;
        $this->basic_data['attributes']["separateCourses"] = true;
        $this->basic_data['attributes']["serviceFeeActive"] = true;
        $this->basic_data['attributes']["serviceFeeTax1Name"] = "Sales Tax 2";
        $this->basic_data['attributes']["serviceFeeTax1Rate"] = "5.00";
        $this->basic_data['attributes']["serviceFeeTax2Name"] = "Sales Tax 5";
        $this->basic_data['attributes']["serviceFeeTax2Rate"] = "6.00";
        $this->basic_data['attributes']["showInvoiceSaleItems"] = true;
        $this->basic_data['attributes']["sideBySideTeeSheets"] = true;
        $this->basic_data['attributes']["stackTeeSheets"] = true;
        $this->basic_data['attributes']["superTwilightHour"] = "1900";
        $this->basic_data['attributes']["teedOffColor"] = "#FF5733";
        $this->basic_data['attributes']["teesheetUpdatesAutomatically"] = false;
        $this->basic_data['attributes']["teetimeDefaultToPlayer1"] = false;
        $this->basic_data['attributes']["termsAndConditions"] = "FREEDOM!";
        $this->basic_data['attributes']["timeFormat"] = "12_hour";
        $this->basic_data['attributes']["timezone"] = "America/Los_Angeles";
        $this->basic_data['attributes']["trackCash"] = false;
        $this->basic_data['attributes']["twilightHour"] = "1830";
        $this->basic_data['attributes']["unitPriceIncludesTax"] = true;
        $this->basic_data['attributes']["updatedPrinting"] = true;
        $this->basic_data['attributes']["useCourseFiring"] = true;
        $this->basic_data['attributes']["useEtsGiftcards"] = true;
        $this->basic_data['attributes']["useKitchenBuzzers"] = true;
        $this->basic_data['attributes']["useNewPermissions"] = true;
        $this->basic_data['attributes']["useTerminals"] = false;
        $this->basic_data['attributes']["webprnt"] = false;
        $this->basic_data['attributes']["webprntColdIp"] = "192.168.1.104";
        $this->basic_data['attributes']["webprntHotIp"] = "192.168.1.104";
        $this->basic_data['attributes']["webprntIp"] = "192.168.1.104";
        $this->basic_data['attributes']["webprntLabelIp"] = "192.168.1.104";
        $this->basic_data['attributes']["websiteUrl"] = "foreuprocks.com";
        $this->basic_data['attributes']["weekendFri"] = true;
        $this->basic_data['attributes']["weekendSat"] = false;
        $this->basic_data['attributes']["weekendSun"] = false;

        $this->request->request->set('data',$this->basic_data);
        $response = $this->controller->update($this->request, 6270);

        $this->assertEquals(200, $response->getStatusCode());

        $response = $response->getContent();
        $response = json_decode($response,true)['data'];

        $this->assertEquals($response, $this->basic_data);
    }

    public function testUpdateWithMalformedData(){
        unset($this->basic_data['attributes']);
        $this->request->request->set('data',$this->basic_data);
        $response = $this->controller->update($this->request, 6270);

        $this->validateError($response, "Attributes is required. ");
    }

    public function testUpdateWithNonExistentSetting(){
        $this->basic_data['attributes']['garbage'] = true;
        $this->request->request->set('data',$this->basic_data);
        $response = $this->controller->update($this->request, 6270);

        $this->assertEquals(200, $response->getStatusCode());

        $response = $response->getContent();
        $response = json_decode($response,true)['data'];

        unset($this->basic_data['attributes']['garbage']);
        $this->assertEquals($response, $this->basic_data);
    }
}