<?php

namespace foreup\rest\unit\controllers\courses;

use foreup\rest\t\traits\mock_app_trait;
use foreup\rest\t\Helpers\testing_customer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 5/12/2017
 * Time: 2:38 PM
 */
class customersControllerTest  extends \Codeception\TestCase\Test
{
    use mock_app_trait;
    
    private $app, $basic_data, $query, $controller, $request, $customer;
    private $expectedResponse;

    protected function _before()
    {
        $this->setupController('customers.controller');

        $this->basic_data = json_decode('{
            "q" : "",
            "first_name" : "eq:Fred",
            "include" : "customerGroups",
            "limit" : "25",
            "total_pages" : "1",
            "total_entries" : "14",
            "sort_by" : "last_name",
            "order" : "asc",
            "start" : "0"
        }',true);

        $this->request = new Request($this->basic_data);

        $this->customer = new testing_customer();
        $this->expectedResponse = $this->customer->getCustomersResponse(["7857"]);
    }

    protected function _after()
    {
        $this->tearDownController();
    }

    protected function validateError(JsonResponse $response, string $error, int $errorNumber = 403){
        $response = $this->getResponseArray($response, $errorNumber);

        $this->assertNotNull($response);
        $this->assertEquals($response['success'], false);
        $this->assertNotEmpty($response['title']);
        $this->assertEquals($response['title'],$error);
    }

    protected function getResponseArray(JsonResponse $response, int $retVal = 200){
        $this->assertEquals($retVal, $response->getStatusCode());

        $response = $response->getContent();
        $response = json_decode($response,true);

        return $response;
    }

    protected function validateResponseSizes($response, $totalCustomers, $returnedCustomers = 25, $numObjects = 3){
        $this->assertEquals($numObjects, sizeof($response));
        $this->assertEquals($returnedCustomers, sizeof($response['data']));
        $this->assertGreaterThanOrEqual($totalCustomers, $response['meta']['total']);
    }

    protected function getValidGroupsFromIncluded($response){
        $validGroupIDs = [];
        foreach($response['included'] as $key => $value){
            array_push($validGroupIDs, $value['id']);
        }
        return $validGroupIDs;
    }

    protected function getValidGroupsFromCustomers($response){
        $validGroupIDs = [];
        foreach ($response['data'] as $customer) {
            foreach ($customer['relationships']['customerGroups']['data'] as $id) {
                if (!empty($id)) {
                    array_push($validGroupIDs, $id['id']);
                }
            }
        }
        return $validGroupIDs;
    }

    protected function validateGroups($response){
        //get group id's from relationships object
        $validGroupIDs = $this->getValidGroupsFromIncluded($response);

        //Make sure the customers attached groups exist
        foreach($response['data'] as $key => $customer) {
            if(!empty($customer['relationships']['customerGroups']['data'])){
                foreach($customer['relationships']['customerGroups']['data'] as $objID => $relationship) {
                    $this->assertContains($relationship['id'], $validGroupIDs);
                }
            }
        }

        //get group id's from customers
        $customerGroupIDs = $this->getValidGroupsFromCustomers($response);

        //make sure only groups attached to the returned customers were included in the relationship object
        foreach($validGroupIDs as $key => $id){
            $this->assertContains($id, $customerGroupIDs);
        }
    }

    public function testGetAllWithFirstNameQuery(){
        $this->basic_data = json_decode('{
            "q":"Fred",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $expectedResponse = $this->customer->getCustomersResponse(["2365","7606","2436","2580","7791","2886","2921","1197","3787","5804","5817","6318","7664","849","7156","7250","7263"]);

        $this->assertEquals($expectedResponse, $response);
    }

    public function testGetAllWithLastNameQuery(){
        $this->basic_data = json_decode('{
            "q":"Shepherd",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);
        $expectedResponse = $this->customer->getCustomersResponse(["849","850","6387","6388"]);

        $this->assertEquals($expectedResponse, $response);
    }

    public function testGetAllWithPhoneNumberQuery(){
        $this->basic_data = json_decode('{
            "q":"8013693765",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->assertEquals($this->expectedResponse, $response);
    }

    public function testGetAllWithSpecialCharsQuery(){
        $this->basic_data = json_decode('{
            "q":"801-369-3765",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->assertEquals($this->expectedResponse, $response);
    }

    public function testGetAllWithEmailQuery(){
        $this->basic_data = json_decode('{
            "q":"brendon.beebe@gmail.com",
            "include":"customerGroups",
            "limit":"10",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36647, 10, 3);

        foreach($response['data'] as $customer) {
            $this->assertEquals("brendon.beebe@gmail.com", $customer['attributes']['contact_info']['email']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithComplexNameQuery(){
        $this->basic_data = json_decode('{
            "q":"Jimmy \"The Rocket\" Aarons",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);

        $customer = new testing_customer();

        $this->assertEquals($this->expectedResponse, $response);

        $this->validateGroups($response);
    }

    public function testGetAllWithAccountNumberQuery()
    {
        $this->basic_data = json_decode('{
            "q":"4012000010000151",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }', true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);

        $customer = new testing_customer();

        $this->assertEquals($this->expectedResponse, $response);

        $this->validateGroups($response);
    }

    public function testGetAll(){
        $this->basic_data = json_decode('{
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36831, 25, 3);

        $this->validateGroups($response);
    }

    public function testGetAllWithoutOrder(){
        $this->basic_data = json_decode('{
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36831, 25, 3);

        $this->validateGroups($response);
    }

    public function testGetAllWithoutStart(){
        $this->basic_data = json_decode('{
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36831, 25, 3);

        $this->validateGroups($response);
    }

    public function testGetAllWithoutSortBy(){
        $this->basic_data = json_decode('{
                "include":"customerGroups",
                "limit":"5"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36831, 5, 3);

        $this->validateGroups($response);
    }

    public function testGetAllWithoutLimit(){
        $this->basic_data = json_decode('{
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36831, 10, 2);
    }

    public function testGetAllWithoutInclude(){
        unset($this->basic_data['include']);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);
        $expectedResponse = $this->customer->getCustomersResponse(["2365","7606","2436","2886","2921","3787","5804","5817","6318","7664","849","7156","7250","7263"]);
        unset($expectedResponse['included']);
        foreach ($expectedResponse['data'] as $key => $customer){
            unset($customer['relationships']);
            $expectedResponse['data'][$key] = $customer;
        }

        $this->assertEquals($expectedResponse, $response);

        foreach($response['data'] as $customer) {
            $this->assertEquals("Fred", $customer['attributes']['contact_info']['first_name']);
        }

        $this->validateResponseSizes($response, 14, 14, 2);
    }

    public function testGetAllSortByFirstName(){
        $this->basic_data['sort_by'] = "first_name";
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 14, 14, 3);

        foreach($response['data'] as $customer) {
            $this->assertEquals("Fred", $customer['attributes']['contact_info']['first_name']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithStartOffset(){
        //each request requires a fresh version of the application or it will crash


        $response = $this->controller->getAll($this->request, 6270);
        $response = $this->getResponseArray($response);
        $this->tearDownController();

        $this->setupController('customers.controller');
        $this->basic_data['start'] = "5";
        $this->request = new Request($this->basic_data);
        $offsetResponse = $this->controller->getAll($this->request, 6270);
        $offsetResponse = $this->getResponseArray($offsetResponse);

        $this->validateResponseSizes($offsetResponse, 14, 9, 3);
        foreach($offsetResponse['data'] as $customer) {
            $this->assertEquals("Fred", $customer['attributes']['contact_info']['first_name']);
        }
        $this->validateGroups($offsetResponse);
        foreach($offsetResponse['data'] as $key => $customer) {
            $this->assertEquals($response['data'][$key + 5], $customer);
        }
    }

    public function testGetAllOrderDescending(){
        //each request requires a fresh version of the application or it will crash



        $response = $this->controller->getAll($this->request, 6270);
        $response = $this->getResponseArray($response);
        $this->tearDownController();

        $this->setupController('customers.controller');
        $this->basic_data['order'] = "desc";
        $this->request = new Request($this->basic_data);

        $orderedResponse = $this->controller->getAll($this->request, 6270);

        $orderedResponse = $this->getResponseArray($orderedResponse);

        $this->validateResponseSizes($orderedResponse, 14, 14, 3);
        foreach($orderedResponse['data'] as $customer) {
            $this->assertEquals("Fred", $customer['attributes']['contact_info']['first_name']);
        }
        $this->validateGroups($orderedResponse);
        //check that customers are reverse ordered
        foreach($orderedResponse['data'] as $key => $customer) {
            $this->assertEquals($response['data'][13 - $key]['attributes']['contact_info']['first_name'], $customer['attributes']['contact_info']['first_name']);
            $this->assertEquals($response['data'][13 - $key]['attributes']['contact_info']['last_name'], $customer['attributes']['contact_info']['last_name']);
        }
    }

    public function testGetAllWithAdvancedSearchMultiField(){
        $this->basic_data = json_decode('{
            "first_name":"eq:Fred",
            "last_name":"like:i",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 3, 3, 2);
        foreach($response['data'] as $customer) {
            $this->assertEquals("Fred", $customer['attributes']['contact_info']['first_name']);
            $this->assertContains("i", $customer['attributes']['contact_info']['last_name']);
        }
        $this->validateGroups($response);
    }

    public function testGetAllWithAdvancedSearchFirstNameEquals(){
        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 14, 14, 3);

        foreach($response['data'] as $customer) {
            $this->assertEquals("Fred", $customer['attributes']['contact_info']['first_name']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithAdvancedSearchFirstNameNotEqual(){
        $this->basic_data['first_name'] = "neq:Fred";
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36817, 25, 3);

        foreach($response['data'] as $customer) {
            $this->assertNotEquals("Fred", $customer['attributes']['contact_info']['first_name']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithAdvancedSearchFirstNameContains(){
        $this->basic_data['first_name'] = "like:Fr";
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 59, 25, 3);
        foreach($response['data'] as $customer) {
            $this->assertContains("Fr", $customer['attributes']['contact_info']['first_name'], '', true);
        }
        $this->validateGroups($response);
    }

    public function testGetAllWithAdvancedSearchFirstNameStartsWith(){
        $this->basic_data['first_name'] = "starts:F";
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 48, 25, 3);
        foreach($response['data'] as $customer) {
            $this->assertStringStartsWith("F", strtoupper($customer['attributes']['contact_info']['first_name']));
        }
        $this->validateGroups($response);
    }

    public function testGetAllWithAdvancedSearchFirstNameEndsWith(){
        $this->basic_data['first_name'] = "ends:b";
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 131, 25, 3);
        foreach($response['data'] as $customer) {
            $this->assertStringEndsWith("b", strtolower($customer['attributes']['contact_info']['first_name']));
        }
        $this->validateGroups($response);
    }

    public function testGetAllWithAdvancedSearchLastName(){
        $this->basic_data = json_decode('{
            "last_name":"eq:Haase",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 2);
        $this->assertEquals("Haase",$response['data']['0']['attributes']['contact_info']['last_name']);
        $this->validateGroups($response);
    }

    public function testGetAllWithAdvancedSearchGroups(){
        $this->basic_data = json_decode('{
            "group":"eq:Rename Again",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);
        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 2, 2, 3);
        $this->assertEquals("Rename Again", $response['included']['0']['attributes']['label']);
        $this->validateGroups($response);
    }

    public function testGetAllWithAdvancedSearchEmail(){
        $this->basic_data = json_decode('{
            "email":"eq:brendon.beebe@gmail.com",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);
        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36647, 25, 3);

        foreach($response['data'] as $customer) {
            $this->assertEquals("brendon.beebe@gmail.com", $customer['attributes']['contact_info']['email']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithAdvancedSearchHandicapAccountNum(){
        $this->basic_data = json_decode('{
            "handicap_account_number":"eq:4564163",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);
        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);
        $this->assertEquals("22", $response['data']['0']['id']);
        $this->assertEquals("4564163", $response['data']['0']['attributes']['contact_info']['handicap_account_number']);
        $this->validateGroups($response);
    }

    public function testGetAllWithAdvancedSearchHandicapAccountScore(){
        $this->basic_data = json_decode('{
            "handicap_score":"eq:10",
            "include":"customerGroups",
            "limit":"25",
            "sort_by":"last_name",
            "order":"asc",
            "start":"0"
        }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);
        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);

        foreach($response['data'] as $customer) {
            $this->assertEquals("10", $customer['attributes']['contact_info']['handicap_score']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithPhoneNumber(){
        $this->basic_data = json_decode('{
                "phone_number":"eq:8013693765",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->assertEquals($this->expectedResponse, $response);
    }

    public function testGetAllWithPhoneNumberSpecialChars(){
        $this->basic_data = json_decode('{
                "phone_number":"eq:801-369-3765",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->assertEquals($this->expectedResponse, $response);
    }

    public function testGetAllWithAccountNumber(){
        $this->basic_data = json_decode('{
                "account_number":"eq:4012000010000151",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->assertEquals($this->expectedResponse, $response);
    }

    public function testGetAllWithAccountAccountBalanceEquals(){
        $this->basic_data = json_decode('{
                "account_balance":"eq:10.78",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->assertEquals($this->expectedResponse, $response);
    }

    public function testGetAllWithAccountAccountBalanceNotEqual(){
        $this->basic_data = json_decode('{
                "account_balance":"neq:10.78",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36830, 25, 3);

        $this->assertNotContains($this->expectedResponse['data']['0'],$response['data']);

        foreach($response['data'] as $customer) {
            $this->assertNotEquals("10.78", $customer['attributes']['account_balance']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithAccountAccountBalanceLessThan(){
        $this->basic_data = json_decode('{
                "account_balance":"lt:10.78",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36811, 25, 3);

        $this->assertNotContains($this->expectedResponse['data']['0'],$response['data']);

        foreach($response['data'] as $customer) {
            $this->assertLessThan("10.78", $customer['attributes']['account_balance']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithAccountAccountBalanceLessThanOrEqual(){
        $this->basic_data = json_decode('{
                "account_balance":"lte:10.78",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36812, 25, 3);

        foreach($response['data'] as $customer) {
            $this->assertLessThanOrEqual("10.78", $customer['attributes']['account_balance']);
            $this->assertEquals("Cascade Golf Course", $customer['attributes']['course']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithAccountAccountBalanceGreaterThan(){
        $this->basic_data = json_decode('{
                "account_balance":"gt:10.78",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 19, 19, 3);

        $this->assertNotContains($this->expectedResponse['data']['0'],$response['data']);

        foreach($response['data'] as $customer) {
            $this->assertGreaterThan("10.78", $customer['attributes']['account_balance']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithAccountAccountBalanceGreaterThanOrEqual(){
        $this->basic_data = json_decode('{
                "account_balance":"gte:10.78",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 20, 20, 3);

        $this->assertContains($this->expectedResponse['data']['0'],$response['data']);

        foreach($response['data'] as $customer) {
            $this->assertGreaterThanOrEqual("10.78", $customer['attributes']['account_balance']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithMemberAccountBalance(){
        $this->basic_data = json_decode('{
                "member_account_balance":"eq:-711.87",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);

        $this->assertEquals($this->expectedResponse,$response);

        $this->validateGroups($response);
    }

    public function testGetAllTaxable(){
        $this->basic_data = json_decode('{
                "taxable":"eq:1",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 35967, 25, 3);

        foreach($response['data'] as $customer) {
            $this->assertEquals(true, $customer['attributes']['taxable']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithDiscount(){
        $this->basic_data = json_decode('{
                "discount":"gt:0",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 3, 3, 3);

        $this->assertContains($this->expectedResponse['data']['0'],$response['data']);

        foreach($response['data'] as $customer) {
            $this->assertGreaterThan(0, $customer['attributes']['discount']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllOptOutEmail(){
        $this->basic_data = json_decode('{
                "opt_out_email":"eq:1",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 29962, 25, 3);

        foreach($response['data'] as $customer) {
            $this->assertEquals(true, $customer['attributes']['opt_out_email']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllOptOutText(){
        $this->basic_data = json_decode('{
                "opt_out_text":"eq:0",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 36831, 25, 3);

        foreach($response['data'] as $customer) {
            $this->assertEquals(false, $customer['attributes']['opt_out_text']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllEmailSubscribed(){
        $this->basic_data = json_decode('{
                "email_subscribed":"eq:1",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 29962, 25, 3);

        foreach($response['data'] as $customer) {
            $this->assertEquals(true, $customer['attributes']['opt_out_email']);
        }

        $this->validateGroups($response);
    }

    public function testGetAllWithDateCreatedEqual(){
        $this->basic_data = json_decode('{
                "date_created":"eq:2017-05-19",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);

        $this->assertEquals($this->expectedResponse, $response);

        $this->validateGroups($response);
    }

    public function testGetAllWithDateCreatedNotEqual(){
        $this->basic_data = json_decode('{
                "date_created":"neq:2017-05-19",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 0, 0, 2);

        $this->validateGroups($response);
    }

    public function testGetAllWithDateCreatedBefore(){
        $this->basic_data = json_decode('{
                "date_created":"lt:2017-05-20",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);

        $this->assertEquals($this->expectedResponse, $response);

        $this->validateGroups($response);
    }

    public function testGetAllWithDateCreatedOnORBefore(){
        $this->basic_data = json_decode('{
                "date_created":"lte:2017-05-19",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);

        $this->assertEquals($this->expectedResponse, $response);

        $this->validateGroups($response);
    }

    public function testGetAllWithDateCreatedAfter(){
        $this->basic_data = json_decode('{
                "date_created":"gt:2017-05-18",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);

        $this->assertEquals($this->expectedResponse, $response);

        $this->validateGroups($response);
    }

    public function testGetAllWithDateCreatedOnORAfter(){
        $this->basic_data = json_decode('{
                "date_created":"gte:2017-05-19",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);

        $this->assertEquals($this->expectedResponse, $response);

        $this->validateGroups($response);
    }

    public function testGetAllWithBirthday(){
        $this->basic_data = json_decode('{
                "birthday":"eq:2000-05-23",
                "include":"customerGroups",
                "limit":"25",
                "sort_by":"last_name",
                "order":"asc",
                "start":"0"
            }',true);
        $this->request = new Request($this->basic_data);

        $response = $this->controller->getAll($this->request, 6270);

        $response = $this->getResponseArray($response);

        $this->validateResponseSizes($response, 1, 1, 3);

        $this->assertEquals($this->expectedResponse, $response);

        $this->validateGroups($response);
    }

    //TODO: write tests for every searchable field in advanced search

    //TODO: write tests for csv and  simple format

    //TODO: write test to see what the limit is on customers returned in a response
}