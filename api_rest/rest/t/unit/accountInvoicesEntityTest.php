<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupAccountInvoices;

class testAccountInvoicesEntity  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;
	protected $invoice;

	protected function _before()
	{
		$this->invoice = new ForeupAccountInvoices();
		$this->invoice->setDateCreated(new \DateTime('2016-12-25T12:12:12Z'));
	}

	protected function _after()
	{
	}
	
	public function testInterface()
	{
		$invoice = $this->invoice;

		$this->assertTrue(method_exists($invoice,'validate'));
		$this->assertTrue(method_exists($invoice,'invalid'));
		$this->assertTrue(method_exists($invoice,'getId'));
		$this->assertTrue(method_exists($invoice,'getDateCreated'));
		$this->assertTrue(method_exists($invoice,'getDueDate'));
		$this->assertTrue(method_exists($invoice,'getCreatedBy'));
		$this->assertTrue(method_exists($invoice,'getOrganizationId'));
		$this->assertTrue(method_exists($invoice,'getPersonId'));
		$this->assertTrue(method_exists($invoice,'getRecurringChargeId'));
		$this->assertTrue(method_exists($invoice,'getSubtotal'));
		$this->assertTrue(method_exists($invoice,'getTax'));
		$this->assertTrue(method_exists($invoice,'getTotal'));
		$this->assertTrue(method_exists($invoice,'getTotalPaid'));
		$this->assertTrue(method_exists($invoice,'getTotalOpen'));
		$this->assertTrue(method_exists($invoice,'getType'));
		$this->assertTrue(method_exists($invoice,'getMemo'));
		$this->assertTrue(method_exists($invoice,'getNumber'));
		$this->assertTrue(method_exists($invoice,'getLedger'));
		$this->assertTrue(method_exists($invoice,'setDateCreated'));
		$this->assertTrue(method_exists($invoice,'setDueDate'));
		$this->assertTrue(method_exists($invoice,'setCreatedBy'));
		$this->assertTrue(method_exists($invoice,'setOrganizationId'));
		$this->assertTrue(method_exists($invoice,'setPersonId'));
		$this->assertTrue(method_exists($invoice,'setRecurringChargeId'));
		$this->assertTrue(method_exists($invoice,'setSubtotal'));
		$this->assertTrue(method_exists($invoice,'setTax'));
		$this->assertTrue(method_exists($invoice,'setTotal'));
		$this->assertTrue(method_exists($invoice,'setTotalPaid'));
		$this->assertTrue(method_exists($invoice,'setTotalOpen'));
		$this->assertTrue(method_exists($invoice,'setType'));
		$this->assertTrue(method_exists($invoice,'setMemo'));
		$this->assertTrue(method_exists($invoice,'setNumber'));
		$this->assertTrue(method_exists($invoice,'setLedger'));
	}

}

?>
