<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupInvoiceChanges;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\models\entities\ForeupPeople;

require_once ('EntityTestingUtility.php');

class testStatementChangesEntity  extends EntityTestingUtility
{
	private $item;

	protected function _before()
	{
		$this->item = new ForeupInvoiceChanges();
		$this->item->setStatement(new ForeupInvoices());
		$this->item->setCourseId(123);
		$cst = new ForeupCustomers();
		$pzn = new ForeupPeople();
		$crs = new ForeupCourses();
		$pzn->setPersonId(456);
		$crs->testing_override('courseId',789);
		$cst->setPerson($pzn);
		$cst->setCourse($crs);
		$this->item->setPerson($cst);
		$ivc = new ForeupInvoices();
		$ivc->testing_override('invoiceId', 789);
		$this->item->setStatement($ivc);


	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$item = $this->item;
		$verbs = ['get', 'set'];
		$nouns = ['Statement', 'CourseId', 'Person', 'Date', 'PreviousPaid', 'Paid', 'PreviousOverdue',
			'Overdue', 'Notes', 'PreviousDate', 'NewDate'];
		$this->interface_tester($item, $verbs, $nouns);
	}

	public function testGetSet()
	{
		$location ='ForeupStatementChanges->validate';
		$item = $this->item;

		$this->assertTrue($item->validate());
		$fields = [['integer','courseId',true],['datetime','date',true],['numeric','previousPaid',true],
		['numeric','paid',true],['numeric','previousOverdue',true],['numeric','overdue',true],
		['string','notes',true],['datetime','previousDate',false],['datetime','newDate',false]];
		$this->get_set_tester($item,$location,$fields);
	}
}