<?php
namespace foreup\rest\unit;
use foreup\rest\controllers\account_recurring\accountRecurringCharges;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupAccountPaymentTerms;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\resource_transformers\account_recurring_statements_transformer;
use foreup\rest\resource_transformers\invoices_transformer;

//use Silex\Application;

class testStatementsTransformer  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	public function testInterface()
	{
		$transformer = new invoices_transformer();
		$this->assertTrue(method_exists($transformer, 'transform'));
		$this->assertTrue(method_exists($transformer, 'includeSale'));
		$this->assertTrue(method_exists($transformer, 'includeCreditCard'));
		$this->assertTrue(method_exists($transformer, 'includeCustomer'));
		$this->assertTrue(method_exists($transformer, 'includeEmployee'));
		$this->assertTrue(method_exists($transformer, 'includeCreditCardPayment'));

	}
}