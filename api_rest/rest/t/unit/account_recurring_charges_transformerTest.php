<?php
namespace foreup\rest\unit;
use foreup\rest\controllers\account_recurring\accountRecurringCharges;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupAccountPaymentTerms;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\resource_transformers\account_recurring_charges_transformer;
//use Silex\Application;

class testAccountRecurringChargesTransformer  extends \Codeception\TestCase\Test
{
	use \Codeception\Specify;

	public function testInterface()
	{
		$transformer = new account_recurring_charges_transformer();
		$this->assertTrue(method_exists($transformer,'transform'));
		$this->assertTrue(method_exists($transformer,'includePaymentTerms'));
		$this->assertTrue(method_exists($transformer,'includeItems'));
	}

	public Function testTransform()
	{
		//$controller = new accountRecurringCharges(null,null);
		$transformer = new account_recurring_charges_transformer();
		$charge = new ForeupAccountRecurringCharges();
		$charge->setName('valid name');
		$charge->setDescription('valid description');
		$charge->setDateCreated(new \DateTime('2016-03-03T12:12:12Z'));
		$charge->setCreatedBy(12);
		$charge->setOrganizationId(123);
		$charge->setIsActive(1);
		$charge->setProrateCharges(1);

		$terms = new ForeupAccountPaymentTerms();
		$terms->setName('valid name');
		$terms->setDateCreated(new \DateTime('2016-03-03T12:12:12Z'));
		$terms->setMemo('valid memo');
		$terms->setOrganizationId(123);

		$returned = $transformer->transform($charge);

		$this->assertEquals($charge->getName(),$returned['name']);
		$this->assertEquals($charge->getDescription(),$returned['description']);
		$this->assertEquals($charge->getCreatedBy(),$returned['created_by']);
		$this->assertEquals($charge->getOrganizationId(),$returned['organization_id']);
		$this->assertEquals($charge->getDateCreated(),new \DateTime($returned['date_created']));
		$this->assertEquals($charge->getIsActive(),$returned['is_active']);
		$this->assertEquals($charge->getProrateCharges(),$returned['prorate_charges']);

		$returned = $transformer->includePaymentTerms($charge);
		$this->assertEmpty($returned);

		$charge->setPaymentTerms($terms);
		$returned = $transformer->includePaymentTerms($charge);
		//$returned = $controller->serializeResource($charge);
		//$this->assertNotEmpty($returned->getData());
		$this->assertEquals($terms,$returned->getData());
	}
}
?>