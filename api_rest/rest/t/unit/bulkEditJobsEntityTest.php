<?php

namespace foreup\rest\unit;
use foreup\rest\models\entities\ForeupBulkEditJobs;

class testBulkEditJobsEntity  extends EntityTestingUtility
{
	private $job;

	protected function _before()
	{
		$this->job = new ForeupBulkEditJobs();
		$this->job->setType('customers');
        $this->job->setRecordIds([1,2]);
		$this->job->setCreatedAt(new \DateTime());

	}

	protected function _after()
	{
	}

	public function testInterface()
	{
		$job = $this->job;

		$this->assertTrue(method_exists($job, 'validate'));
		$this->assertTrue(method_exists($job, 'getId'));
		$this->assertTrue(method_exists($job, 'getEmployeeId'));

		$verbs = ['get', 'set'];
		$nouns = ['Type','CourseId','Status','TotalRecords','RecordsCompleted','RecordsFailed',
			'PercentComplete','CreatedAt','TotalDuration','Response','DeletedAt','Settings',
			'LastProgressUpdate', 'RecordIds'];

		$this->interface_tester($job, $verbs, $nouns);

	}

	public function testGetSet()
	{
		$job = $this->job;
		$location = 'ForeupBulkEditJobs->validate';
		$this->assertTrue($job->validate());

		$fields = [
			['integer','courseId',false], ['integer','employeeId',false], ['enum','type',true,['customers']],
			['enum','status',true,['pending','ready','in-progress','completed','cancelled']],
			['integer','totalRecords',true],['integer','recordsCompleted',true],['integer','recordsFailed',true],
			['integer','percentComplete',true],['datetime','createdAt',true],['datetime','startedAt',false],
			['datetime','completedAt',false],['integer','totalDuration',false],['array','response',false],
			['datetime','deletedAt',false],['array','settings',false],['datetime','lastProgressUpdate',false],
            ['array','recordIds',false]
		];
		$this->assertEquals(17,count($fields));
		$this->get_set_tester($job,$location,$fields);
	}

}
?>