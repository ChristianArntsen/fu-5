<?php
namespace foreup\rest\controllers;

use Carbon\Carbon;
use foreup\rest\models\entities\ForeupEmployeeAuditLog;
use foreup\rest\resource_transformers\employee_audit_log_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class employee_audit_log extends api_controller
{
    /** @var \foreup\rest\models\repositories\EmployeeAuditLogRepository $repository */
    private $repository;


    public function __construct($db,$current_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->current_user = $current_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupEmployeeAuditLog');
        $this->transformer = new employee_audit_log_transformer();
        $this->resource_type = "employee_audit_log";
    }

    public function get_course_log_entries(Request $request)
    {
        //* Returns log entries by foreup_employee course_id field
        $uzr = $this->current_user;
        if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin",400);

        $entries = $this->repository->get_course_log_entries($request->attributes->get('id'));
        $content = $this->serializeResource($entries);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
        //*/
    }

    public function get_employee_log_entries(Request $request)
    {
        //* Returns log entries by foreup_employee id field
        $uzr = $this->current_user;
        if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin",400);
        $id = $request->attributes->get('id');
        $entries = $this->repository->get_employee_log_entries($id);
        $content = $this->serializeResource($entries);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
        //*/
    }

    public function getAll(Request $request)
    {
        $uzr = $this->current_user;
        if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin",400);

        $response = new JsonResponse();
        $response->setContent([]);
        return $response;
    }

    public function redo_logged_action(Request $request)
    {
        // This function is intended to undo an action recorded by an particular employee audit log entry

        $uzr = $this->current_user;
        if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin",400);

        // Retrieve id
        $id = $request->attributes->get('id');

        // request the entry from the database by id
        $target = $this->repository->get_log_entry($id)[0];

        // dispatch to undo
        $res = $this->repository->redo($target);

        // return success/fail
        $content = json_encode($res);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function undo_logged_action(Request $request)
    {
        // This function is intended to undo an action recorded by an particular employee audit log entry

        $uzr = $this->current_user;
        if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin",400);

        // Retrieve id
        $id = $request->attributes->get('id');

        // request the entry from the database by id
        $target = $this->repository->get_log_entry($id)[0];

        // dispatch to undo
        $res = $this->repository->undo($target);

        // return success/fail
        $content = json_encode($res);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function save(Request $request)
    {
        $uzr = $this->current_user;
        if($uzr['level']*1!==5)return $this->respondWithError("Must be super admin",400);
        $data = $request->request->get('data');
        if (isset($data['attributes'])) {
            $data = $data['attributes'];
        } else {
            return $this->respondWithError("Missing attribute field", 400);
        }

        $entry = new ForeupEmployeeAuditLog();

        if(isset($data['editor_id'])){
            $entry->setEditor($data['editor_id']);
        }
        elseif(isset($data['employee_id'])){
            // we are given the employee id.
            $employee = $this->db->find('e:ForeupEmployees',(int) $data['employee_id']);
            $entry->setEditor($employee);
        }elseif(isset($data['person_id'])&&isset($data['course_id'])){
            // we are given the unique identifier for an employee

            $person = $this->db->find('e:ForeupPeople',(int) $data['person_id']);
            $course = $this->db->find('e:ForeupCourses',(int) $data['course_id']);
            $employee = $this->db->getRepository('foreup\rest\models\entities\ForeupEmployees')->findBy(array('person'=>$person,'courseId'=>(int) $data['course_id']));
            //$employee->setCourseId($course);
           // $employee->setPerson($person);
            $entry->setEditor($employee[0]);
            //$course = $this->db->getReference('e:ForeupEmployee',);
        }elseif(isset($uzr['emp_id'])){
            // try to get employee id from current user
            $employee = $this->db->find('e:ForeupEmployees',(int) $uzr['emp_id']);
            $entry->setEditor($employee);
        }elseif(isset($uzr['cid'])&&isset($uzr['uid'])){
            // try to get employee id from current user
            $person = $this->db->find('e:ForeupPeople',(int) $uzr['pid']);
            $course = $this->db->find('e:ForeupCourses',(int) $uzr['cid']);
            $employee = $this->db->getRepository('foreup\rest\models\entities\ForeupEmployees')->findBy(array('person'=>$person,'courseId'=>(int) $uzr['cid']));
            //$employee->setCourseId($course);
           // $employee->setPerson($person);
            $entry->setEditor($employee[0]);
        }

        if(isset($data['gmt_logged'])){
            $entry->setGmtLogged(date_create($data['gmt_logged']));
        }else{
            $entry->setGmtLogged(date_create(date("Y-m-d H:i:s")));
        }

        if(isset($data['action_type_id'])){
            $action_type =$person = $this->db->find('e:ForeupActionType',(int) $data['action_type_id']);
            $entry->setActionType($action_type);
        }else {
            $action_type =$person = $this->db->find('e:ForeupActionType',(int) 1);
            $entry->setActionType($action_type);
        }

        if(isset($data['comments'])) {
            $entry->setComments($data['comments']);
        }

        $this->db->persist($entry);
        $this->db->flush();

        $content = $this->serializeResource($entry);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }
}

?>