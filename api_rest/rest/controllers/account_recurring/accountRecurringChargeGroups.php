<?php
namespace foreup\rest\controllers\account_recurring;

use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupAccountRecurringChargeGroups;
use foreup\rest\resource_transformers\account_recurring_charge_groups_transformer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class accountRecurringChargeGroups extends api_controller
{
	public function __construct($em,$app,$current_user, $auth_user)
	{
		parent::__construct();

		$this->db = $em;
		$this->repository = $this->db->getRepository('e:ForeupAccountRecurringChargeGroups');
		$this->current_user = $current_user;
		$this->employee = $app['employee'];
		$this->auth_user = $auth_user;
		$this->transformer = new account_recurring_charge_groups_transformer();
		$this->resource_type = "accountRecurringChargeGroups";
	}

	public function get($courseId,$id,Request $request){
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		//$charge = $this->db->getRepository('e:ForeupAccountRecurringCharges')->findBy(array('id'=>$rc_id,'organizationId'=>$courseId));
		$customer = $this->repository->findBy(array('id'=>$id));
		$charge = $customer[0]->getRecurringCharge();
		$oid = $charge->getOrganizationId();
		if((int) $oid !== (int) $courseId ){
			return $this->respondWithError("Organization id mismatch. Cannot access that recurring charge",400);
		}
		$resource = $this->serializeResource($customer);

		$response = new JsonResponse();
		$response->setContent($resource);

		return $response;
	}

	public function getAll($courseId,$rc_id,Request $request){
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		$charge = $this->db->getRepository('e:ForeupAccountRecurringCharges')->findBy(array('id'=>$rc_id,'organizationId'=>$courseId));
		$customer = $this->repository->findBy(array('recurringCharge'=>$charge));
		$resource = $this->serializeResource($customer);

		$response = new JsonResponse();
		$response->setContent($resource);

		return $response;
	}

	public function create($courseId,Request $request){
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		if(isset($data['attributes'])){
			$id = isset($data['id'])?$data['id']:null;
			$data = $data['attributes'];
			if(isset($id))$data['id']=$id;
			elseif(isset($data['id']))$id = $data['id'];
		} else {
			return $this->respondWithError("Missing attribute field",400);
		}


		$link = $this->save($courseId, $data, $meta);
		$response = new JsonResponse();
		$resource = $this->serializeResource($link);

		$response->setContent($resource);
		return $response;
	}

	public function update($courseId,$id,Request $request){
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		if(isset($data['attributes'])){
			$data = $data['attributes'];
			if(isset($id))$data['id']=$id;
			elseif(isset($data['id']))$id = $data['id'];
			if(!isset($id)){
				return $this->respondWithError("Missing id parameter",400);
			}
		} else {
			return $this->respondWithError("Missing attribute field",400);
		}

		$link = $this->save($courseId, $data, $meta);
		$response = new JsonResponse();
		$resource = $this->serializeResource($link);

		$response->setContent($resource);
		return $response;
	}

	public function delete($courseId, $id, Request $request){
		$courseId = (int) $courseId;
		$this->db->getConnection()->beginTransaction();
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		$validate_only = false;
		$test = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;

		$link = $this->db->getRepository('e:ForeupAccountRecurringChargeGroups')->find($id);

		$group_course = (int) $link->getGroup()->getCourse()->getCourseId();
		$charge_course = (int) $link->getRecurringCharge()->getOrganizationId();
		if($group_course !== $courseId ||
		   $charge_course !== $courseId){
			return $this->respondWithError("Course mismatch",501);
		}

		$this->db->remove($link);
		if(!$validate_only)$this->db->flush();
		if(!$test)$this->db->getConnection()->commit();

		$response = new JsonResponse();

		$response->setContent('{"data":{"success":true,"content":"accountRecurringChargeGroups instance deleted"}}');
		return $response;
	}

	private function save($courseId, $data, $meta){
		$validate_only = false;
		$test = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;

		$this->db->getConnection()->beginTransaction();

		if(!isset($data['id'])) {
			$link = new ForeupAccountRecurringChargeGroups();
		}else{
			$link = $this->db->getRepository('e:ForeupAccountRecurringChargeGroups')->find($data['id']);
			if(is_array($link))$link = $link[0];
			if(!$link)return $link;
			if($link->getRecurringCharge()->getOrganizationId()*1!==$courseId*1){
				return null;
			}
		}
		$charge = $this->db->getRepository('e:ForeupAccountRecurringCharges')->findBy(array('id'=>$data['recurring_charge_id'],'organizationId'=>$courseId));
		$course = $this->db->getRepository('e:ForeupCourses')->findBy(array('courseId'=>$courseId));
		$group = $this->db->getRepository('e:ForeupGroups')->findBy(array('groupId'=>$data['group_id'],'course'=>$course));
		$include = isset($data['include'])?$include=$data['include']:null;

		$link->setGroup($group[0]);
		$link->setRecurringCharge($charge[0]);
		$link->setInclude($include);


		$link_id = $link->getId();
		if(!isset($link_id)){
			$this->db->persist($link);
		}
		if(!$validate_only)$this->db->flush();
		if(!$test)$this->db->getConnection()->commit();

		return $link;
	}
}
?>