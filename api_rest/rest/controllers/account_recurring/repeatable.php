<?php
namespace foreup\rest\controllers\account_recurring;

use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupRepeatable;

class repeatable extends api_controller
{
	public function __construct($em,$app,$current_user, $auth_user)
	{
		parent::__construct();

		$this->db = $em;
		$this->repository = $this->db->getRepository('e:ForeupRepeatable');
		$this->current_user = $current_user;
		$this->employee = $app['employee'];
		$this->auth_user = $auth_user;
		//$this->transformer = new repeatable_transformer();
		$this->resource_type = "repeatable";
	}

	public function get($courseId,$id){
		return $this->respondWithError("Not Implemented",501);
	}

	public function getAll($courseId){
		return $this->respondWithError("Not Implemented",501);
	}

	public function create($courseId,$id){
		return $this->respondWithError("Not Implemented",501);
	}

	public function update($courseId,$id){
		return $this->respondWithError("Not Implemented",501);
	}

	public function delete($courseId,$id){
		return $this->respondWithError("Not Implemented",501);
	}
}
?>