<?php
namespace foreup\rest\controllers\account_recurring;

use Doctrine\Common\Collections\Criteria;
use foreup\rest\models\entities\ForeupAccountRecurringChargeItems;
use foreup\rest\models\entities\ForeupAccountRecurringCharges;
use foreup\rest\models\entities\ForeupRepeatableAccountRecurringChargeItems;
use foreup\rest\resource_transformers\account_recurring_charges_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\services\FilterQueryParser;
use \Carbon\Carbon;

class accountRecurringCharges extends api_controller
{
	/** @var \Doctrine\ORM\EntityRepository $repository */
	protected $repository;


	/** @var \Doctrine\ORM\EntityManager $db */
	protected $db;

	protected $current_user;

	protected $auth_user;

	/** @var  \foreup\rest\models\entities\ForeupEmployees */
	protected $employee;

	protected $recurringStatementController;

	public function __construct($em,$app,$current_user, $auth_user)
	{
		parent::__construct();

		$this->db = $em;
		$this->repository = $this->db->getRepository('e:ForeupAccountRecurringCharges');
		$this->current_user = $current_user;
		$this->employee = $app['employee'];
		$this->auth_user = $auth_user;
		$this->transformer = new account_recurring_charges_transformer();
		$this->resource_type = "accountRecurringCharges";
		$this->recurringStatementController = $app['accountRecurringStatements.controller'];

	}

	public function getTransformer(){
		return $this->transformer;
	}

	public function startTransaction(){
		$this->db->getConnection()->beginTransaction();
	}

	public function rollback(){
		$this->db->getConnection()->rollBack();
	}

	/**
	 * @param int $courseId
	 * @param int $id
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function get($courseId, $id, Request $request)
	{
		$this->saveParametersAndDefaults($request);
		$includeDeleted = false;
		$inc = $request->query->all();
		if(isset($inc['includeDeleted'])&&$inc['includeDeleted'])$includeDeleted = true;
		if(!$this->checkAccess($courseId))
			return $this->response;
		$org_id = $this->employee->getCourseId();

		$criteria = new Criteria();
		$criteria->andWhere($criteria->expr()->eq('organizationId',$courseId));
		$criteria->andWhere($criteria->expr()->eq('id',$id));
		if(!$includeDeleted){
			$criteria->andWhere($criteria->expr()->eq('deletedBy', null));
			$criteria->andWhere($criteria->expr()->eq('dateDeleted', null));
		}

		//$charge = $this->db->find('e:ForeupAccountRecurringCharges', $id);
		$charge = $this->db->getRepository('e:ForeupAccountRecurringCharges')->matching($criteria);
		if(count($charge)===1){
			$charge = $charge[0];
		}
		$resource = $this->serializeResource($charge);
		$response = new JsonResponse();
		$response->setContent($resource);

		return $response;
	}

	/**
	 * @param int $courseId
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function getAll($courseId, Request $request){

	    $this->saveParametersAndDefaults($request);

		if(!$this->checkAccess($courseId)) {
            return $this->response;
        }

        $queryParser = new FilterQueryParser();

        $filters = $queryParser->generateFilters($request, $this->repository);
        $wildCardQuery = null;
        if($filters === false){
            return $this->respondWithError("Problem generating filters for the request",500);
        }

        $charges = $this->repository->getFilteredResults(
            $filters,
            $courseId,
            $this->start,
            $this->limit,
            $wildCardQuery,
            $this->sortBy,
            $this->order
        );

        $total = $this->repository->getFilteredCount(
            $filters,
            $courseId,
            $this->start,
            $this->limit,
            $wildCardQuery,
            $this->sortBy,
            $this->order
        );

        $content = $this->serializeResource($charges);
        $content = json_decode($content);
        $content->meta = [];
        $content->meta['total'] = $total;
        $content = json_encode($content);
        $response = new JsonResponse();
        $response->setContent($content);

        return $response;
	}

	/**
	 * @param int $courseId
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function update($courseId, Request $request)
	{

		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;
		$data = $request->request->get('data');
		$meta = $request->request->get('meta');
		if(!isset($data['id'])&&!isset($data['attributes']['id'])){
			return $this->respondWithError("id required when updating",400);
		}

		if(isset($data['attributes'])){
			$id = isset($data['id'])?$data['id']:null;
			$data = $data['attributes'];
			if(isset($id))$data['id']=$id;
			elseif(isset($data['id']))$id = $data['id'];
		} else {
			return $this->respondWithError("accountRecurringCharges->update->Missing attribute field",400);
		}

		return $this->save($courseId,$data, $meta);
	}

	/**
	 * @param int $courseId
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function create($courseId, Request $request)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;
		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		if(isset($data['id'])||isset($data['attributes']['id'])){
			return $this->respondWithError("accountRecurringCharges->create->Please use PATCH request to update existing recurring charge",400);
		}

		if(isset($data['attributes'])){
			$id = isset($data['id'])?$data['id']:null;
			$data = $data['attributes'];
			if(isset($id))$data['id']=$id;
			elseif(isset($data['id']))$id = $data['id'];
		} else {
			return $this->respondWithError("accountRecurringCharges->create->Missing attributes field",400);
		}
		$person = $this->db->getRepository('e:ForeupPeople')->find($this->employee->getPerson()->getPersonId());
		$criteria = new Criteria();
		$criteria->andWhere($criteria->expr()->eq('person',$person));
		//$criteria->andWhere($criteria->expr()->eq('courseId',$courseId));

		$data['created_by'] = $this->db->getRepository('e:ForeupEmployees')->matching($criteria)->toArray();

		if(is_array($data['created_by']) && count($data['created_by'])>1){
			foreach($data['created_by'] as $employee){
				if($employee->getUserLevel*1 === 5 || $employee->getCid() == $courseId) {
					$data['created_by'] = [$employee];
					break;
				}
			}
		}

		if(is_array($data['created_by']) && count($data['created_by'])===1){
			$data['created_by'] = $data['created_by'][0];
		}
		$data['created_by']->setPerson($person);
		$data['date_created'] = new \DateTime('now');

		return $this->save($courseId,$data, $meta);
	}

	/**
	 * @param int $courseId
	 * @param int|null $id
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function put($courseId, $id = null, Request $request)
	{
		if(!$this->checkAccess($courseId))
			return $this->response;
		$content = json_decode($request->getContent(),true);
		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		if(isset($data['id'])){
			$id = $data['id'];
		}
		if(!isset($id)){
			return $this->create($courseId,$request);
		}
		$courseId = (int) $courseId;
		$this->db->getConnection()->beginTransaction();
		$this->saveParametersAndDefaults($request);

		$validate_only = false;
		$test = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;

		if(isset($data['attributes'])){
			$data = $data['attributes'];
		}

		$link = $this->db->getRepository('e:ForeupAccountRecurringCharges')->find($id);
		if(!isset($link)){
			return $this->respondWithError('No charge with that ID found',404);
		}
		$items = $link->getItems();
		$keep_items = array();
		if(is_array($data['items']) && count($data['items'])>0){
			foreach($data['items'] as $item){
				if(isset($item['id'])){
					$keep_items[] = $item['id'];
				}
			}
		}

		// reset (most) defaults
		$link->setPaymentTerms(null);
		$link->setDeletedBy(null);
		$link->setDateDeleted(null);
		$link->setIsActive(1);
		$link->setDescription(null);
		$link->setName(null);
		$link->setProrateCharges(0);
		// remove items that are not being updated
		foreach($items as $item){
			if(in_array($item->getId(),$keep_items))continue;
			$repeated = $item->getRepeated();
			$item->setRepeated(null);
			if(!$validate_only)$this->db->flush();
			$this->db->remove($repeated);
			$this->db->remove($item);
		}
		if(!$validate_only)$this->db->flush();

        $data['id'] = $id;
        $ret = $this->save($courseId,$data, $meta);

		if(!$test && $ret->getStatusCode() === 200)$this->db->getConnection()->commit();
		return $ret;
	}

	/**
	 * @param int $courseId
	 * @param int $id
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function delete($courseId, $id, Request $request)
	{
		$this->db->getConnection()->beginTransaction();
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		$validate_only = false;
		$test = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;

		$charge = $this->getCharge(array('id'=>$id),$courseId,$meta);
		/* // hard delete
		$items = $charge->getItems();
		foreach($items as $item){
			$repeated = $item->getRepeated();
			if(isset($repeated))
			  $this->db->remove($repeated);
			$this->db->remove($item);
		}
		$this->db->remove($charge);
		*/

		// soft delete
		$charge->setDeletedBy($this->employee->getPerson()->getPersonId());
		$charge->setDateDeleted(new \DateTime());

		if(!$validate_only)$this->db->flush();
		if(!$test)$this->db->getConnection()->commit();

		$response = new JsonResponse();

		$response->setContent('{"data":{"success":true,"content":"recurring_charge deleted"}}');
		return $response;
	}

	/**
	 * @param int $courseId
	 * @param $data
	 * @param $meta
	 * @return ForeupAccountRecurringCharges|null|object|JsonResponse
	 */
	public function save($courseId, $data, $meta)
	{
		$validate_only = false;
		$test = false;
		$return_entity = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;
		if(isset($meta) && isset($meta['return_entity']) && (bool)$meta['return_entity'])
			$return_entity = true;
		$this->db->getConnection()->beginTransaction();

		// get and save the recurring charge

		$charge = $this->getCharge($data,$courseId,$meta);

		if(!method_exists($charge,'validate')){
			return $charge;
		}

		$validation = $charge->validate(false);

		if($validation !== true){
			$this->db->getConnection()->rollBack();
			return $this->respondWithError($validation,500);
		}else{
			//persist the entity
			if(!isset($data['id'])) {
				$this->db->persist($charge);
			}
		}

		foreach($data['items'] as $item){
			$rc_item = $this->getItem($charge,$item);

			if(!method_exists($rc_item,'validate'))return $rc_item;
			$validation = $rc_item->validate(false);

			if($validation !== true){
				$this->db->getConnection()->rollBack();
				return $this->respondWithError($validation,500);
			}else{
				// persist the entity
				if(!isset($item['id']))
				    $this->db->persist($rc_item);
				try {
					if(!$validate_only)$this->db->flush();
				}catch (\Exception $e){
					$this->db->getConnection()->rollBack();
					return $this->respondWithError($e->getMessage(),500);
				}
			}
			if(isset($item['attributes'])){
				$item = $item['attributes'];
			}
			if(isset($item['repeated'])){
				$repeated = $this->getRepeated($rc_item,$item['repeated']);

				if(!method_exists($repeated,'validate'))return $repeated;
				$validation = $repeated->validate(false);

				if($validation !== true){
					$this->db->getConnection()->rollBack();
					return $this->respondWithError($validation,500);
				}else{
					$has_id = $repeated->getId();
                    $today = Carbon::now();
                    $next = $repeated->calculateNextOccurence($today);
                    $repeated->setNextOccurence($next);
                    if(is_null($has_id)) {
						$this->db->persist($repeated);
					}
					$rc_item->setRepeated($repeated);
				}

			}
			$charge->addItems($rc_item);
			if(!$validate_only)$this->db->flush();
		}
		// get and save the repeatable
		if(!$validate_only)$this->db->flush();
		if($return_entity)return $charge;

		if(!$test)$this->db->getConnection()->commit();
		$response = new JsonResponse();
		$resource = $this->serializeResource($charge);

		$response->setContent($resource);
		return $response;
	}

	private function getPaymentTerms($terms){}

	private function getCharge($data,$courseId,$meta)
	{
		if(isset($data['id'])){
			$charge = $this->repository->find($data['id']);
			if(!isset($charge)) {
				return $this->respondWithError('accountRecurringCharge entity not found with id: ' . $data['id'], 404);
			}
		}else {
			$charge = new ForeupAccountRecurringCharges();
		}
		if(isset($data['attributes'])){
			$data = $data['attributes'];
		}
		$terms_id = null;
		if(isset($data['payment_terms'])){
			if(is_numeric($data['payment_terms']) && is_int($data['payment_terms']*1)){
				$terms_id = $data['payment_terms'];
			}elseif(isset($data['payment_terms']['id'])){
				$terms_id = $data['payment_terms']['id'];
			}else{
				$this->db->getConnection()->rollBack();
				return $this->respondWithError('payment_terms must be integer id or payment_terms object',500);
			}

			try {
				$terms = $this->db->find('e:ForeupAccountPaymentTerms', $terms_id);
			}catch (\Exception $e){
				$this->db->getConnection()->rollBack();
				return $this->respondWithError($e->getMessage(),500);
			}
		}
		$statement = null; // statement is nullable
		if(isset($data['recurringStatement'])){
			$persist_me = false;
			if(is_numeric($data['recurringStatement']) && is_int($data['recurringStatement']*1)){
				$terms_id = $data['recurringStatement'];
			}elseif(isset($data['recurringStatement']['id'])){
				$terms_id = $data['recurringStatement']['id'];
			}else{
				$persist_me = true;
			}
			if($persist_me){

				if(isset($data['recurringStatement']['attributes'])){
					$data['recurringStatement']['attributes']['created_by'] = $data['created_by'];
					$data['recurringStatement']['attributes']['date_created'] = $data['date_created'];
				}
				else {
					$data['recurringStatement']['created_by'] = $data['created_by'];
					$data['recurringStatement']['date_created'] = $data['date_created'];
				}
			}
			$statement = $this->getRecurringStatement($charge,$courseId,$data['recurringStatement'],$meta);
			if($statement&&$persist_me){

				if(!method_exists($statement,'validate')){
					return $statement;
				}

				$this->db->persist($statement);
			}
		}
		if(isset($statement)) {
			if(!method_exists($statement,'validate')){
				return $statement;
			}
			$charge->setRecurringStatement($statement);
		}

		isset($data['name'])?$charge->setName($data['name']):null;
		isset($data['description'])?$charge->setDescription($data['description']):null;
		//isset($data['date_created'])?$charge->setDateCreated(new \DateTime($data['date_created'])):null;

		isset($data['prorate_charges'])?$charge->setProrateCharges($data['prorate_charges']):null;
		$charge->setOrganizationId($courseId);
		if(!isset($data['id'])) {
			$charge->setCreatedBy($this->employee->getPerson()->getPersonId());
			$charge->setDateCreated(new \DateTime());
		}
		isset($data['is_active'])?$charge->setIsActive($data['is_active']):null;
		//isset($data['date_deleted'])?$charge->setDateDeleted(\DateTime($data['date_deleted'])):null;
		//isset($data['deleted_by'])?$charge->setDeletedBy($data['deleted_by']):null;

		return $charge;

	}

	private function getItem(&$charge,$item)
	{
		$charge_id = $charge->getId();
		if(isset($charge_id)&&isset($item['id'])){
			// this is really an update...
			$rc_item = $this->db->getRepository('e:ForeupAccountRecurringChargeItems')->find($item['id']);
			if(!isset($rc_item)) {
				return $this->respondWithError('accountRecurringChargeItem entity not found with id: ' . $item['id'], 400);
			}
		}
		elseif(isset($item['id']) || (isset($item['attributes'])&& isset($item['attributes']['id']))){
			return $this->respondWithError('Re-assigning existing recurringChargeItem to new recurringCharge not supported', 500);
		}
		else {
			$rc_item = new ForeupAccountRecurringChargeItems();
		}
		if(isset($item['attributes'])){
			$item = $item['attributes'];
		}

		$itm = $this->db->getRepository('e:ForeupItems')->find($item['item_id']);
		$rc_item->setItem($itm);

		isset($item['item_type'])?$rc_item->setItemType($item['item_type']):null;
		isset($item['line_number'])?$rc_item->setLineNumber($item['line_number']):null;
        array_key_exists('override_price', $item)?$rc_item->setOverridePrice($item['override_price']):null;
		isset($item['discount_percent'])?$rc_item->setDiscountPercent($item['discount_percent']):null;
		isset($item['quantity'])?$rc_item->setQuantity($item['quantity']):null;
		$rc_item->setRecurringCharge($charge);
		return $rc_item;
	}

	private function getRepeated(&$rc_item,$rep)
	{
		if(isset($rep['id'])){
			$repeated = $this->db->getRepository('e:ForeupRepeatableAccountRecurringChargeItems')->find($rep['id']);
			if(!isset($repeated)) {
				return $this->respondWithError('Repeatable entity not found with id: ' . $rep['id'], 404);
			}
			if(empty($rep['dtstart'])){
				$rep['dtstart'] = new \DateTime();
			}
		}elseif($rc_item->getRepeated()){
			$repeated = $rc_item->getRepeated();
		}else {
			$repeated = new ForeupRepeatableAccountRecurringChargeItems();
		}
		$repeated->setItem($rc_item);

		if(isset($rep['attributes'])){
			$rep = $rep['attributes'];
		}
        isset($rep['freq'])?$repeated->setFreq($rep['freq']):null;
        array_key_exists('until', $rep)?$repeated->setUntil($rep['until']):null;
        array_key_exists('interval', $rep)?$repeated->setInterval($rep['interval']):null;
        array_key_exists('count', $rep)?$repeated->setCount($rep['count']):null;
        array_key_exists('dtstart', $rep)?$repeated->setDtstart($rep['dtstart']):null;
        array_key_exists('bymonth', $rep)?$repeated->setBymonth($rep['bymonth']):null;
        array_key_exists('byweekno', $rep)?$repeated->setByweekno($rep['byweekno']):null;
        array_key_exists('byyearday', $rep)?$repeated->setByyearday($rep['byyearday']):null;
        array_key_exists('bymonthday', $rep)?$repeated->setBymonthday($rep['bymonthday']):null;
        array_key_exists('byday', $rep)?$repeated->setByday($rep['byday']):null;
        array_key_exists('wkst', $rep)?$repeated->setWkst($rep['wkst']):null;
        array_key_exists('byhour', $rep)?$repeated->setByhour($rep['byhour']):null;
        array_key_exists('byminute', $rep)?$repeated->setByminute($rep['byminute']):null;
        array_key_exists('bysecond', $rep)?$repeated->setBysecond($rep['bysecond']):null;
        array_key_exists('bysetpos', $rep)?$repeated->setBysetpos($rep['bysetpos']):null;

		return $repeated;
	}

	public function getRecurringStatement(ForeupAccountRecurringCharges &$charge,$courseId,$data,$meta)
	{
		if(!empty($charge->getRecurringStatement())&&!isset($data['id'])){
			$chg = $charge->getRecurringStatement();
			if(!empty($chg->getId())){
				$data['id'] = $chg->getId();
			}
		}
		return $this->recurringStatementController->save($courseId,$data,$meta);
	}

}