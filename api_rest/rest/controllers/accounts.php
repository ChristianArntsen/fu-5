<?php
namespace foreup\rest\controllers;

use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use foreup\rest\models\entities\ForeupAccountLedger;
use foreup\rest\models\entities\ForeupAccountPayments;
use foreup\rest\models\entities\ForeupAccounts;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\services\AccountLedgerService;
use foreup\rest\resource_transformers\account_ledger_transformer;
use foreup\rest\resource_transformers\account_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use League\Fractal\Resource\Item;

class accounts extends api_controller
{
	private $repository;

	/** @var \Doctrine\ORM\EntityManager $db */
	private $db;


	/** @var  \foreup\rest\models\entities\ForeupEmployees  $current_employee*/
	protected $current_employee;

	/**
	 * accounts constructor.
	 * @param \Doctrine\ORM\EntityManager $em
	 * @param $current_user
     */
	public function __construct($em, $current_employee)
	{
		parent::__construct();
		$this->current_employee = $current_employee;
		$this->db = $em;
		$this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupAccounts');
		$this->transformer = new account_transformer();
		$this->resource_type = "accounts";
	}


	public function create(Request $request)
	{
		$input = $request->request->get("data");
		if(empty($input)){
			return $this->respondWithError("Data empty",400);
		}

		$customersRepository = $this->db->getRepository('foreup\rest\models\entities\ForeupCustomers');
		$customer = $customersRepository->find($input['relationships']['owner']['id']);
		if(empty($customer)){
			return $this->respondWithError("Unknown owner id.",400);
		}


		$accountTypeRepo = $this->db->getRepository('foreup\rest\models\entities\ForeupAccountTypes');
		$accountType = $accountTypeRepo->find($input['relationships']['account_type']['id']);
		if(empty($accountType)){
			return $this->respondWithError("Unknown account type.",400);
		}

		$account = new ForeupAccounts();
		$account->setBalance($input['attributes']['balance']);
		$account->setNumber(uniqid());
		$account->setAccountType($accountType);
		$account->setOwner($customer);

		$account->setDateCreated(Carbon::now());
		$account->setOrganizationId($this->current_employee->getCourseId());
		$this->db->persist($account);
		$this->db->flush();

		$content = $this->serializeResource($account);
		$response = new JsonResponse();
		$response->setContent($content);

		return $response;
	}


	public function debit($account_id,Request $request)
	{
		$input = $request->request->get("data");
		/** @var \foreup\rest\models\entities\ForeupAccounts $account*/
		$account = $this->repository->find($account_id);


		if(!isset($input['attributes']) || !isset($input['attributes']['amount']) || $input['attributes']['amount'] <= 0){
			$json_response = new JsonResponse();
			$json_response->setData(array(
					'title'=>"Invalid Amount",
					'detail'=>'Amount should be a positive number.'
			));
			$json_response->setStatusCode(400);
			return $json_response;
		}


		$accountLedgerItem = new ForeupAccountLedger();
		$accountLedgerItem->setAmount($input['attributes']['amount']);
		$accountLedgerItem->setType("sale");
		$accountLedgerItem->setDateCreated(Carbon::now());
		$accountLedgerItem->setCreatedBy($this->current_employee->getPerson()->getPersonId());

		$accountLedger = new AccountLedgerService($this->current_employee,$account,$this->db);
		$amount  = $accountLedger->debit($accountLedgerItem);

		if(!$amount){
			$json_response = new JsonResponse();
			$json_response->setData(array(
					'title'=>"Fund's not available.",
					'detail'=>"The account's limit has been passed. Customer has a balance of \${$accountLedger->getMySpending()}"
			));
			$json_response->setStatusCode(400);
			return $json_response;
		}


		/** @var \foreup\rest\models\repositories\AccountLedgerRepository $accountLedgerRepo*/
		$accountLedgerRepo = $this->db->getRepository('foreup\rest\models\entities\ForeupAccountLedger');
		$accountLedgerRepo->create($accountLedgerItem);
		$this->db->flush();
		$resource = new Item($accountLedgerItem,new account_ledger_transformer(), "account_ledger");
		$serialized =  $this->serializeResponse($resource);
		$response = new JsonResponse();
		$response->setContent($serialized);
		return $response;
	}


	public function credit($account_id,Request $request)
	{
		$input = $request->request->get("data");
		/** @var \foreup\rest\models\entities\ForeupAccounts $account*/
		$account = $this->repository->find($account_id);


		if(!isset($input['attributes']) || !isset($input['attributes']['amount']) || $input['attributes']['amount'] <= 0){
			$json_response = new JsonResponse();
			$json_response->setData(array(
					'title'=>"Invalid Amount",
					'detail'=>'Amount should be a positive number.'
			));
			$json_response->setStatusCode(400);
			return $json_response;
		}


		$accountLedgerItem = new ForeupAccountLedger();
		$accountLedgerItem->setAmount(-1*$input['attributes']['amount']);
		$accountLedgerItem->setDateCreated(Carbon::now());
		$accountLedgerItem->setCreatedBy($this->current_employee->getPerson()->getPersonId());

		if(isset($input['attributes']['type'])){
			$accountLedgerItem->setType($input['attributes']['type']);
		} else {
			$accountLedgerItem->setType("return");
		}


		$accountLedgerService = new AccountLedgerService($this->current_employee,$account,$this->db);
		$accountLedgerService->credit($accountLedgerItem);

		/** @var \foreup\rest\models\repositories\AccountLedgerRepository $accountLedgerRepo*/
		$accountLedgerRepo = $this->db->getRepository('foreup\rest\models\entities\ForeupAccountLedger');
		$accountLedgerRepo->create($accountLedgerItem);
		$this->db->flush();
		$resource = new Item($accountLedgerItem,new account_ledger_transformer(), "account_ledger");
		$serialized =  $this->serializeResponse($resource);
		$response = new JsonResponse();
		$response->setContent($serialized);
		return $response;
	}

	public function payment($account_id,Request $request)
	{

		$accountsPaymentRepo = $this->db->getRepository('e:ForeupAccountPayments');
		$input = $request->request->get("data");
		/** @var \foreup\rest\models\entities\ForeupAccounts $account*/
		$account = $this->repository->find($account_id);
		/** @var \foreup\rest\models\entities\ForeupPaymentTypes $account*/
		$paymentTypeRepo = $this->db->getRepository('e:ForeupPaymentTypes');
		$paymentType = $paymentTypeRepo->findOneBy([
			"type"=>"credit_card",
			"subType"=>"visa"
		]);
		if($paymentType === null){
			$json_response = new JsonResponse();
			$json_response->setData(array(
				'title'=>"Invalid Payment Type",
				'detail'=>"Invalid Payment Type"
			));
			$json_response->setStatusCode(400);
			return $json_response;
		}
		$amount = $input['attributes']['amount'];

		//Create the payment
		$accountLedgerItem = $this->createLedgerItemForPayment($amount,$account);
		$account->creditAccount($accountLedgerItem);

		$payment = new ForeupAccountPayments($accountLedgerItem);
		$payment->setAmount($amount);
		$payment->setAmountOpen($amount);
		$payment->setAccount($account);
		$payment->setOrganizationId($this->current_employee->getCourseId());
		$payment->setPaymentType($paymentType);
		$payment->setPersonId($this->current_employee->getPerson()->getPersonId());
		isset($input['attributes']['memo'])? $payment->setMemo($input['attributes']['memo']) :"";
		isset($input['attributes']['meta'])? $payment->setMeta($input['attributes']['meta']) :"";
		isset($input['attributes']['record_id'])? $payment->setRecordId($input['attributes']['record_id']):"";


		$accountsPaymentRepo->create($payment);


		$accountLedgerService = new AccountLedgerService($this->current_employee,$account,$this->db);
		$accountLedgerService->distributePayment($payment);




		$accountLedgerService->distributeAllRemaningPayments();

		$this->db->flush();
		$resource = new Item($accountLedgerItem,new account_ledger_transformer(), "account_ledger");
		$serialized =  $this->serializeResponse($resource);
		$response = new JsonResponse();
		$response->setContent($serialized);
		return $response;
	}

	/**
	 * @param $payment
	 * @param $account
	 * @return ForeupAccountLedger
	 */
	private function createLedgerItemForPayment($amount, $account)
	{
		/** @var \foreup\rest\models\repositories\AccountLedgerRepository $accountLedgerRepo */
		$accountLedgerRepo = $this->db->getRepository('e:ForeupAccountLedger');
		$accountLedgerItem = new ForeupAccountLedger();
		$accountLedgerItem->setAmount($amount * -1);
		$accountLedgerItem->setPersonId($this->current_employee->getPerson()->getPersonId());
		$accountLedgerItem->setAccount($account);
		$accountLedgerItem->setAmountOpen($amount * -1);
		$accountLedgerItem->setDateCreated(Carbon::now());
		$accountLedgerItem->setCreatedBy($this->current_employee->getPerson()->getPersonId());
		$accountLedgerItem->setType("payment");
		$accountLedgerRepo->create($accountLedgerItem);
		return $accountLedgerItem;
	}

}