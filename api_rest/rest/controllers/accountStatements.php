<?php
namespace foreup\rest\controllers;


use Doctrine\Common\Collections\Criteria;
use foreup\rest\models\entities\ForeupAccountStatements;
use foreup\rest\resource_transformers\field_transformer;
use foreup\rest\models\services\FilterQueryParser;
use foreup\rest\models\services\StatementUtilities;
use foreup\rest\resource_transformers\account_statements_transformer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;

class accountStatements extends api_controller
{
	protected $repository;

	/**
	 * @var StatementUtilities
	 */
	protected $statementUtilities;

	public function __construct($em,$app,$current_user, $auth_user)
	{
		parent::__construct();
		$this->statementUtilities = $app['statementUtilities'];
		$this->app = $app;
		$this->db = $em;
		$this->repository = $this->db->getRepository('e:ForeupAccountStatements');
		$this->current_user = $current_user;
		$this->employee = $app['employee'];
		$this->auth_user = $auth_user;
		$this->transformer = new account_statements_transformer($em);
		$this->resource_type = "accountStatements";

	}

	public function startTransaction(){
		$this->db->getConnection()->beginTransaction();
	}

	public function rollback(){
		$this->db->getConnection()->rollBack();
	}

	public function getAll($courseId, Request $request){

		$this->saveParametersAndDefaults($request);

		if(!$this->checkAccess($courseId))
			return $this->response;

		$queryParser = new FilterQueryParser();
		$filters = $queryParser->generateFilters($request, $this->repository);
		$wildCardQuery = null;

		if($filters === false){
			return $this->respondWithError("Problem generating filters for the request",500);
		}

		if(empty($this->limit)){
            $this->limit = 25;
        }
        if(empty($this->start)){
            $this->start = 0;
        }

		$wildCardQuery = null;
		if($request->get("q")){
			$expGen = $this->db->getExpressionBuilder();
			$orX = $expGen->orX();
			$orX->add($expGen->eq("statements.number", $request->get('q')));
			$wildCardQuery = $orX;
		}

		$statements = $this->repository->getFilteredResults(
			$filters,
			$courseId,
			$this->start,
			$this->limit,
			$wildCardQuery,
			$this->sortBy,
			$this->order
		);

		$total =  $this->repository->getFilteredCount(
			$filters,
			$courseId,
			$this->start,
			$this->limit,
			$wildCardQuery,
			$this->sortBy,
			$this->order
		);

		$content = $this->serializeResource($statements);
		$content = json_decode($content);
		$content->meta = [];
		$content->meta['total'] = $total;
		$content = json_encode($content);
		$response = new JsonResponse();
		$response->setContent($content);

		return $response;
	}

	public function get($courseId, $id, Request $request)
	{
		$this->saveParametersAndDefaults($request);
		$includeDeleted = false;
		$inc = $request->query->all();
		if(isset($inc['includeDeleted'])&&$inc['includeDeleted'])$includeDeleted = true;
		if(!$this->checkAccess($courseId))
			return $this->response;

		$criteria = new Criteria();
		$criteria->andWhere($criteria->expr()->eq('organizationId',$courseId));
		$criteria->andWhere($criteria->expr()->eq('id',$id));

		if(!$includeDeleted){
			$criteria->andWhere($criteria->expr()->eq('deletedBy', null));
			$criteria->andWhere($criteria->expr()->eq('dateDeleted', null));

		}

        $statement = $this->db->getRepository('e:ForeupAccountStatements')->matching($criteria);

		if(method_exists($statement,'toArray')){
			$statement = $statement->toArray();
		}
		if(count($statement)===1){
			$statement = $statement[0];
		}
		elseif (count($statement) > 1){
			throw new \Exception('Too many account statements found!');
		}

		if(!method_exists($statement,'getId')){
			return $this->respondWithError('accountStatement not found',404);

		}

        $statement->setMemberBalanceForward($this->statementUtilities->getBalanceForward($statement, 'member'));
        $statement->setCustomerBalanceForward($this->statementUtilities->getBalanceForward($statement, 'customer'));

		$resource = $this->serializeResource($statement);

		$response = new JsonResponse();
		$response->setContent($resource);

		return $response;

	}

	public function fields(Request $request, $courseId)
	{
		$this->transformer = new field_transformer();
		$this->resource_type = "field";
		$fields = $this->repository->getAllSearchableFields();

		$content = $this->serializeResource($fields);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	public function delete($courseId, $id, Request $request)
	{
		$this->db->getConnection()->beginTransaction();
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		$validate_only = false;
		$test = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;

		$statement = $this->getStatement(array('id'=>$id),$courseId,$meta);

		// soft delete
		$statement->setDeletedBy($this->employee->getPerson()->getPersonId());
		$statement->setDateDeleted(new \DateTime());

		if(!$validate_only)$this->db->flush();
		if(!$test)$this->db->getConnection()->commit();

		$response = new JsonResponse();

		$response->setContent('{"data":{"success":true,"content":"recurring_charge deleted"}}');

		return $response;

	}

	public function getStatement($data,$courseId,$meta)
	{
		if(isset($data['id'])){
			$statement = $this->repository->find($data['id']);
			if(!isset($statement)) {
				return $this->respondWithError('accountStatement entity not found with id: ' . $data['id'], 404);
			}
		}else {
			return $this->respondWithError('Put/create not implemented yet.');
			//$statement = new ForeupAccountStatements();
		}

		return $statement;

	}

	public function getStatementPdf($courseId,$id)
	{
		// generate fresh pdf

		$repo = $this->db->getRepository('e:ForeupAccountStatements');

		$statement = $repo->find($id);

		$statement_charges = $statement->getCharges();
		$account_transactions = $this->statementUtilities->getAccountTransactions($statement);

		$local_file = $this->statementUtilities->buildPdf($statement,
			$statement_charges,$account_transactions['customer'],$account_transactions['member']);


		$response = new BinaryFileResponse($local_file);

		$response->deleteFileAfterSend(true);
		//$response->setContentDisposition('inline; filename="somefilename.pdf"');

		return $response;

	}

	public function getStatementHtml($courseId, $id){

		$repo = $this->db->getRepository('e:ForeupAccountStatements');
		$statement = $repo->find($id);
		$customer = $statement->getCustomer();
		$course = $customer->getCourse();
		$statement_charges = $statement->getCharges();
		$account_transactions = $this->statementUtilities->getAccountTransactions($statement);

		$html = $this->statementUtilities->html($course, $customer, $statement, $statement_charges, $account_transactions['customer'], $account_transactions['member'], $account_transactions['invoice']);

		$response = new Response();
		$response->setContent($html);

		return $response;
	}
}

?>