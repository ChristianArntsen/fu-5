<?php
namespace foreup\rest\controllers;

use Doctrine\ORM\Tools\DisconnectedClassMetadataFactory;
use foreup\rest\resource_transformers\teesheet_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class teesheets extends api_controller
{
	/** @var \foreup\rest\models\repositories\teesheet_repository $repository */
	private $repository;

	public function __construct($db)
	{
		parent::__construct();

		$this->db = $db;
		$this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupTeesheet');
		$this->transformer = new teesheet_transformer();
		$this->resource_type = "teesheet";
	}

	public function get($id, Request $request)
	{
		$teesheet = $this->repository->find($id);
		$content = $this->serializeResource($teesheet);
		return $content;
	}

}