<?php
namespace foreup\rest\controllers;

use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use foreup\rest\models\entities\ForeupAccountLedger;
use foreup\rest\models\entities\ForeupAccountPayments;
use foreup\rest\models\entities\ForeupAccountRecurringStatements;
use foreup\rest\models\entities\ForeupAccounts;
use foreup\rest\models\entities\ForeupAccountTypes;
use foreup\rest\models\entities\ForeupEmployees;
use foreup\rest\models\services\AccountLedgerService;
use foreup\rest\resource_transformers\account_ledger_transformer;
use foreup\rest\resource_transformers\account_transformer;
use foreup\rest\resource_transformers\account_type_statement_transformer;
use foreup\rest\resource_transformers\account_types_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use League\Fractal\Resource\Item;

class account_type_statements extends api_controller
{
	private $repository;

	/** @var \Doctrine\ORM\EntityManager $db */
	private $db;


	/** @var  \foreup\rest\models\entities\ForeupEmployees  $current_employee*/
	protected $current_employee;

	/**
	 * accounts constructor.
	 * @param \Doctrine\ORM\EntityManager $em
	 * @param $current_user
     */
	public function __construct($em, $current_employee)
	{
		parent::__construct();
		$this->current_employee = $current_employee;
		$this->db = $em;
		$this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupAccountRecurringStatements');
		$this->transformer = new account_type_statement_transformer();
		$this->resource_type = "account_type_statement";
	}


	public function create(Request $request,$account_type_id)
	{
		$this->request = $request;
		$input = $request->request->get("data");
		if(empty($input) || empty($input['attributes'])){
			return $this->respondWithError("Data empty",400);
		}


		$statement = new ForeupAccountRecurringStatements();

		$statement->setName(isset($input['attributes']['name'])?$input['attributes']['name']:'');
		$statement->setIsActive(isset($input['attributes']['is_active'])?$input['attributes']['is_active']:'');
		$statement->setIncludeBalanceForward(isset($input['attributes']['include_balance_forward'])?$input['attributes']['include_balance_forward']:'');
		$statement->setSendEmptyStatements(isset($input['attributes']['send_empty_statements'])?$input['attributes']['send_empty_statements']:'');
		$statement->setItemizeSales(isset($input['attributes']['itemize_sales'])?$input['attributes']['itemize_sales']:'');
		$statement->setItemizeInvoices(isset($input['attributes']['itemize_invoices'])?$input['attributes']['itemize_invoices']:'');
		$statement->setItemizeCharges(isset($input['attributes']['itemize_charges'])?$input['attributes']['itemize_charges']:'');
		$statement->setIncludeSales(isset($input['attributes']['include_sales'])?$input['attributes']['include_sales']:'');
		$statement->setIncludeInvoices(isset($input['attributes']['include_invoices'])?$input['attributes']['include_invoices']:'');
		$statement->setIncludeCharges(isset($input['attributes']['include_charges'])?$input['attributes']['include_charges']:'');

		$statement->setOrganizationId($this->current_employee->getCourseId());

		$statement->setCreatedBy($this->current_employee);
		$statement->setDateCreated(Carbon::now());


		$this->db->persist($statement);
		$this->db->flush();

		$content = $this->serializeResource($statement);
		$response = new JsonResponse();
		$response->setContent($content);

		return $response;
	}

}