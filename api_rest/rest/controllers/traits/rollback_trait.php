<?php
namespace foreup\rest\controllers\traits;
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 5/11/2017
 * Time: 1:18 PM
 */

trait rollback_trait
{
    public function startTransaction(){
        $this->db->getConnection()->beginTransaction();
    }

    public function rollback(){
        $this->db->getConnection()->rollBack();
    }
}