<?php
namespace foreup\rest\controllers;

use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\models\entities\ForeupScores;
use foreup\rest\resource_transformers\score_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class scores extends api_controller
{
	private $repository;


	public function __construct($db,$current_user)
	{
		parent::__construct();

		$this->db = $db;
		$this->current_user = $current_user;
		$this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupScores');
		$this->transformer = new score_transformer();
		$this->resource_type = "scores";
	}

	public function save(Request $request)
	{
		$data = $request->request->get('data');
		if(isset($data['attributes'])){
			$data = $data['attributes'];
		} else {
			return $this->respondWithError("Missing attribute field",400);
		}

		$scores = new ForeupScores();
		$course = $this->db->getReference('e:ForeupCourses',$data['course_id']);
		$user = $this->db->getReference('e:ForeupUsers',$this->current_user['uid']); 

		$scores->setPerson($user);
		$scores->setCourse($course);

		isset($data['score'])?$scores->setScore($data['score']):'';
		isset($data['par'])?$scores->setPar($data['par']):'';
		isset($data['detailed_score_information'])?$scores->setDetailedScoreInformation($data['detailed_score_information']):'';

		$this->db->persist($scores);
		$this->db->flush();

		$content = $this->serializeResource($scores);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}
}