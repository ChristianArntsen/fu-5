<?php
namespace foreup\rest\controllers;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use foreup\rest\models\entities\ForeupApiHooks;
use foreup\rest\models\services\AuthenticatedUser;
use foreup\rest\resource_transformers\api_hooks;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class hooks
 * @package foreup\rest\controllers
 */
class hooks extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;

	/** @var  AuthenticatedUser $auth_user */
	protected $auth_user;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupApiHooks');
        $this->transformer = new api_hooks();
        $this->resource_type = "api_hooks";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request)
    {
	    $this->saveParametersAndDefaults($request);



	    $criteria = new Criteria();
	    $criteria->andWhere($criteria->expr()->eq('person', $this->db->getReference('e:ForeupPeople',$this->auth_user->getAppId())));
	    $results = $this->repository->matching($criteria);
	    $content = $this->serializeResource($results->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

	public function delete(Request $request,$id)
	{
		$this->saveParametersAndDefaults($request);



		/**@var \foreup\rest\models\entities\ForeupApiHooks $hook */
		$hook = $this->repository->find($id);
		if(empty($hook) || $hook->getPerson()->getPersonId() != $this->auth_user->getAppId()){
			return $this->respondWithError("Unable to find the specified hook",401);
		}

		$this->db->remove($hook);
		$this->db->flush();
		$content = json_encode([
			"success"=>true
		]);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	public function create(Request $request)
	{
		$this->saveParametersAndDefaults($request);
		$data = $request->request->get('data');
		if(isset($data['attributes'])){
			$data = $data['attributes'];
		} else {
			return $this->respondWithError("Missing attribute field",400);
		}

		$validEvents = ["teetime.updated","teetime.deleted","customer.updated","sale.created"];
		if(empty($data['event']) || !in_array($data['event'],$validEvents)){
			return $this->respondWithError("Invalid event, must specify one of the following: ".implode(",",$validEvents));
		}
		if(!isset($data['url'])){
			return $this->respondWithError("Url is required: ".implode(",",$validEvents));
		}
		$newHook = new ForeupApiHooks();
        $newHook->setEvent($data['event']);
        $newHook->setHookUrl($data['url']);
		isset($data['headerKey']) ? $newHook->setHeaderKey($data['headerKey']) : "";
		isset($data['headerValue']) ? $newHook->setHeaderValue($data['headerValue']) : "";

		$newHook->setPerson($this->db->getReference('e:ForeupPeople',$this->auth_user->getAppId()));

		$this->db->persist($newHook);
		$this->db->flush();

		$content = $this->serializeResource($newHook);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}
}
?>