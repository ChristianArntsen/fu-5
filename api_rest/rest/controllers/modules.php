<?php
namespace foreup\rest\controllers;

use Doctrine\ORM\Tools\DisconnectedClassMetadataFactory;
use foreup\rest\resource_transformers\module_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class modules extends api_controller
{
	/** @var \foreup\rest\models\repositories\module_repository $repository */
	private $repository;

	public function __construct($db)
	{
		parent::__construct();

		$this->db = $db;
		$this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupMobileAppModules');
		$this->transformer = new module_transformer();
		$this->resource_type = "module";
	}

	public function get($id, Request $request)
	{
		$module = $this->repository->find($id);
		$content = $this->serializeResource($module);
		return $content;
	}

}