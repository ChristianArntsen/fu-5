<?php
namespace foreup\rest\controllers;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use foreup\rest\models\entities\ForeupBulkEditJobs;
use foreup\rest\models\entities\ForeupInvoices;
use foreup\rest\resource_transformers\bulk_edit_jobs_transformer;
use foreup\rest\resource_transformers\invoices_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use foreup\rest\controllers\api_controller;

class bulk_edit_jobs extends api_controller
{
	/** @var \Doctrine\ORM\EntityRepository $repository */
	protected $repository;

	/** @var \Doctrine\ORM\EntityManager $db */
	protected $db;

	protected $current_user;

	protected $auth_user;

	/** @var  \foreup\rest\models\entities\ForeupEmployees */
	protected $employee;

	public function __construct($em,$app,$current_user, $auth_user)
	{
		parent::__construct();

		$this->db = $em;
		$this->repository = $this->db->getRepository('e:ForeupBulkEditJobs');
		$this->current_user = $current_user;
		$this->employee = $app['employee'];
		$this->auth_user = $auth_user;
		$this->transformer = new bulk_edit_jobs_transformer();
		$this->resource_type = "bulkEditJobs";
	}

	public function startTransaction(){
		$this->db->getConnection()->beginTransaction();
	}

	public function rollback(){
		$this->db->getConnection()->rollBack();
	}

	public function get ($courseId, $id, Request $request)
	{
		$this->saveParametersAndDefaults($request);
		$includeDeleted = false;
		$inc = $request->query->all();
		if(isset($inc['includeDeleted'])&&$inc['includeDeleted'])$includeDeleted = true;

        if(!$this->checkAccess($courseId))
			return $this->response;

		$criteria = new Criteria();
		$criteria->andWhere($criteria->expr()->eq('courseId',$courseId));
		$criteria->andWhere($criteria->expr()->eq('id',$id));
		if(!$includeDeleted){
			$criteria->andWhere($criteria->expr()->eq('deletedAt', null));
		}

		if(isset($inc['status'])){
		    $inc['status'] = explode('|', $inc['status']);
            $criteria->andWhere( $criteria::expr()->orX($inc['status']) );
        }

		$job = $this->repository->matching($criteria)->toArray();

		if(is_array($job)&&count($job)===1){
			$job = $job[0];
		}

		$resource = $this->serializeResource($job);

		$response = new JsonResponse();
		$response->setContent($resource);

		return $response;
	}

	public function getAll ($courseId,Request $request)
	{

		$this->saveParametersAndDefaults($request);
		$inc = $request->query->all();
		$includeDeleted = false;
		$offset=0;
		$limit=null;

		if(isset($inc['includeDeleted'])&&$inc['includeDeleted'])$includeDeleted = true;
		if(isset($inc['limit'])&&is_numeric($inc['limit'])){
			$limit = $inc['limit'];
		}
		if(isset($inc['offset'])&&is_numeric($inc['offset'])){
			$offset = $inc['offset'];
		}

		if(!$this->checkAccess($courseId))
			return $this->response;

		$criteria = new Criteria();
		$criteria->andWhere($criteria->expr()->eq('courseId',$courseId));
		if(isset($limit)){
			$criteria->setMaxResults($limit);
		}
		if($offset){
			$criteria->setFirstResult($offset);
		}
		if(!$includeDeleted){
			$criteria->andWhere($criteria->expr()->eq('deletedAt', null));
		}
		$job = $this->repository->matching($criteria);
		$resource = $this->serializeResource($job->toArray());

		$response = new JsonResponse();
		$response->setContent($resource);

		return $response;
	}

	public function update($courseId, $id, Request $request){
	    return $this->save($courseId, $id, $request);
    }

    public function create($courseId, Request $request){
        return $this->save($courseId, null, $request);
    }

	public function save($courseId, $bulkEditJobId, Request $request)
	{
	    $this->saveParametersAndDefaults($request);

        $data = $request->request->get('data');
        if(empty($data['attributes'])){
            return $this->respondWithError('attributes not found' .$bulkEditJobId, 400);
        }
        $data = $data['attributes'];

        if(!empty($bulkEditJobId)){
            $job = $this->db->getRepository('e:ForeupBulkEditJobs')->find($bulkEditJobId);
            if(empty($job)){
                return $this->respondWithError('bulkEditJbos entity not found with id: ' .$bulkEditJobId, 404);
            }

        }else{
            $job = new ForeupBulkEditJobs();
            $job->setEmployee($this->employee);
            $job->setCreatedAt(new \DateTime());
            $job->setCourseId($courseId);
        }

        if(isset($data['type'])) $job->setType($data['type']);
        if(isset($data['status'])) $job->setStatus($data['status']);
        if(isset($data['totalRecords'])) $job->setTotalRecords($data['totalRecords']);
        if(isset($data['settings'])) $job->setSettings($data['settings']);
        if(isset($data['recordIds'])) $job->setRecordIds($data['recordIds']);

        $this->db->persist($job);
        $this->db->flush();
        $bulkEditJobId = $job->getId();

        if($job->getStatus() == 'ready'){
            $command = 'php ../index.php daemon bulk_edit '.(int) $bulkEditJobId. ' > /dev/null 2>&1 &';
            shell_exec($command);
        }

		return $this->get($courseId, $bulkEditJobId, $request);
	}

	public function delete($courseId,$id,Request $request)
	{

		$this->db->getConnection()->beginTransaction();
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		$data = $request->request->get('data');
		$meta = $request->request->get('meta');

		$validate_only = false;
		$test = false;
		if(isset($meta) && isset($meta['validate_only']) && (bool) $meta['validate_only'])
			$validate_only=true;
		if(isset($meta) && isset($meta['test']) && (bool) $meta['test'])
			$test=true;

		$job = $this->getJob(array('id'=>$id),$courseId);

		// soft delete
		$job->setDeletedAt(new \DateTime());

		if(!$validate_only)$this->db->flush();
		if(!$test)$this->db->getConnection()->commit();

		$response = new JsonResponse();

		$response->setContent('{"data":{"success":true,"content":"bulkEditJobs instance deleted"}}');
		return $response;
	}

	public function getJob($data,$courseId){
		if(is_numeric($data)&&is_int($data*1)){
			return $this->repository->find($data);
		}
		elseif(isset($data['id'])){
			$job = $this->repository->find($data['id']);
		}else {
			$job = new ForeupBulkEditJobs();
		}

		return $job;
	}
}
