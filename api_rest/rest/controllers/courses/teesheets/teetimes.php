<?php
namespace foreup\rest\controllers\courses\teesheets;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\repositories\CustomersRepository;
use foreup\rest\models\services\AuthenticatedUser;
use foreup\rest\models\services\CourseSettingsService;
use foreup\rest\models\services\LegacyCartService;
use foreup\rest\models\services\LegacyTeetimeService;
use foreup\rest\resource_transformers\cart_transformer;
use foreup\rest\resource_transformers\teetime_pricing_transformer;
use foreup\rest\resource_transformers\teetime_slot_transformer;
use foreup\rest\resource_transformers\teetime_transformer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class teetimes extends api_controller
{

	/**
	 * @var LegacyTeetimeService
	 */
	private $legacyTeetimeService;
	/** @var  LegacyCartService $legacyCartService */
	private $legacyCartService;
	private $courseSettingsService;
    public function __construct($db,AuthenticatedUser $auth_user,LegacyTeetimeService $legacyTeetimeService,CourseSettingsService $courseSettingsService,LegacyCartService $legacyCartService)
    {
        parent::__construct();

	    $this->courseSettingsService = $courseSettingsService;
	    $this->db = $db;
	    $this->auth_user = $auth_user;
	    $this->transformer = new teetime_slot_transformer();
	    $this->resource_type = "teetimeSlot";
	    $this->loadEmployeeInformation();

	    $this->legacyCartService = $legacyCartService;
	    $this->legacyCartService->setToken($auth_user->getToken());
	    $this->legacyTeetimeService = $legacyTeetimeService;
    }

    public function getAll(Request $request,$courseId,$teesheetId,$customerId = null)
    {
	    $this->checkCoursePermission($request, $courseId);
	    $settings = $this->courseSettingsService->loadSettings($courseId);
	    if(!isset($settings)){
	    	return $this->respondWithError("Unable to locate course settings.",404);
	    }

	    !empty($request->get("date")) ? $this->legacyTeetimeService->setDate($request->get("date")):'';
	    !empty($request->get("startTime")) ? $this->legacyTeetimeService->setStartTime($request->get("startTime")):'';
	    !empty($request->get("endTime")) ? $this->legacyTeetimeService->setEndTime($request->get("endTime")):'';
	    !empty($request->get("holes")) ? $this->legacyTeetimeService->setHoles($request->get("holes")):'';
	    if(!empty($request->get("customerId"))){
		    $this->legacyTeetimeService->setCustomer($request->get("customerId"));
	    }
	    if(!empty($request->get("priceClassId"))){
		    $this->legacyTeetimeService->setPriceClassId($request->get("priceClassId"));
	    }
	    $this->legacyTeetimeService->setTimezone($settings->getTimezone());



	    $token = $this->auth_user->getTokenObject();
	    $token->createToken();
	    $str = $token->getToken();


	    try {
		    $slots = $this->legacyTeetimeService->getTimes($str,$teesheetId);
	    } catch(\Exception $e){
		    error_log($e->getMessage());
	    	return $this->respondWithError("Error contacting tee time database. ");
	    }

	    $content = $this->serializeResource($slots);
	    $response = new JsonResponse();
	    $response->setContent($content);
	    return $response;
    }

    public function getPricing(Request $request,$courseId,$teetimeId)
    {
	    $this->checkCoursePermission($request, $courseId);

	    $pricing = $this->legacyTeetimeService->getPriceClasses($this->auth_user->getToken(),$teetimeId);

	    $this->transformer = new teetime_pricing_transformer();
	    $this->resource_type = "bookingPricing";
	    $content = $this->serializeResource($pricing);
	    $response = new JsonResponse();
	    $response->setContent($content);
	    return $response;
    }

    public function checkIn(Request $request,$courseId,$teesheetId,$teetimeId)
    {
	    $this->checkCoursePermission($request, $courseId);

	    $data = $request->request->get("data");


	    if(!isset($data['attributes']) || !isset($data['attributes']['positions'])){
		    return $this->respondWithError("Attributes is required. ");
	    }

	    $positions = $data['attributes']['positions'];
	    if(!is_array($positions)){
	    	return $this->respondWithError(("Positions must be an array of integers"));
	    }

	    $this->legacyTeetimeService->setToken($this->auth_user->getToken());
	    $cartId = $this->legacyTeetimeService->createCart($teetimeId,$positions);

	    /** @var EntityRepository $cartRepo */
	    $this->legacyCartService->setCartId($cartId);
	    $cart = $this->legacyCartService->getCart();


	    $this->resource_type = "carts";
	    $this->transformer = new cart_transformer();


	    return $this->generateJsonResponse($cart);
    }
}
?>