<?php
namespace foreup\rest\controllers\courses\teesheets\seasons;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupPriceClass;
use foreup\rest\models\entities\ForeupSeasonalTimeframes;
use foreup\rest\models\entities\ForeupSeasons;
use foreup\rest\models\entities\ForeupTeesheet;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\teesheet_seasonal_timeframes_transformer;
use foreup\rest\resource_transformers\teesheet_seasons_transformer;
use foreup\rest\resource_transformers\teetime_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class timeframes extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupSeasonalTimeframes');
        $this->transformer = new teesheet_seasonal_timeframes_transformer();
        $this->resource_type = "timeframes";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId,$seasonId)
    {
	    $this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
		    return $this->response;


	    $criteria = new Criteria();
	    $criteria->andWhere($criteria->expr()->eq('active',1));
	    $criteria->andWhere($criteria->expr()->eq('season', $this->db->getReference('e:ForeupSeasons',$seasonId)));

	    $results = $this->repository->matching($criteria);



	    $content = $this->serializeResource($results->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

	public function get(Request $request,$courseId,$seasonId,$timeframeId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;


		$timeframe = $this->repository->find($timeframeId);
		if(!$timeframe){
			return $this->respondWithError("Can't find the timeframe specified ",404);
		}
		if($timeframe->getSeason()->getCourse()->getCourseId() != $courseId){
			return $this->respondWithError("No permission. ",401);
		}



		$content = $this->serializeResource($timeframe);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	public function delete(Request $request,$courseId,$seasonId,$timeframeId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;


		/** @var ForeupSeasonalTimeframes $timeframe */
		$timeframe = $this->repository->find($timeframeId);
		$timeframe->setActive(0);

		if($timeframe->getSeason()->getCourse()->getCourseId() != $courseId){
			return $this->respondWithError("No permission. ",401);
		}


		$this->db->flush();


		$content = $this->serializeResource($timeframe);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}


	public function create(Request $request,$courseId,$teesheetId,$seasonId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		/** @var ForeupTeesheet $teesheet */
		$teesheetRepo = $this->db->getRepository('e:ForeupTeesheet');
		$teesheet = $teesheetRepo->find($teesheetId);
		if($teesheet->getId() != $teesheetId){
			return $this->respondWithError("No permission. ",401);
		}

		$data = $request->request->get("data");
		if(!isset($data['attributes'])){
			return $this->respondWithError("Attributes is required. ");
		}
		if(!isset($data['relationships']['classes']['data']['id']) ){
			return $this->respondWithError("Relationship required for all timeframes.");
		}


		/** @var ForeupSeasons $season */
		$seasonRepo = $this->db->getRepository('e:ForeupSeasons');
		$season = $seasonRepo->find($seasonId);

		/** @var ForeupPriceClass $priceClass */
		$classRepo = $this->db->getRepository('e:ForeupPriceClass');
		$priceClass = $classRepo->find($data['relationships']['classes']['data']['id']);





		if($priceClass->getCourse()->getCourseId() != $courseId){
			return $this->respondWithError("No permission.",401);
		}
		if(empty($season)){
			return $this->respondWithError("Season does not exist.",404);
		}

		/*
		 * Create a default timeframe if this price class hasn't been added yet.
		 */
		if(!$season->hasClass($priceClass)){
			$defaultTimeframe = new ForeupSeasonalTimeframes($priceClass,$season);
			$defaultTimeframe->setDefault(true);
			$this->db->persist($defaultTimeframe);
		}
		$season->addClass($priceClass);




		$timeframe = new ForeupSeasonalTimeframes($priceClass,$season);
		$possibleSets = [
				"name"=>"setTimeframeName",
				"monday"=>"setMonday",
				"tuesday"=>"setTuesday",
				"wednesday"=>"setWednesday",
				"thursday"=>"setThursday",
				"friday"=>"setFriday",
				"saturday"=>"setSaturday",
				"sunday"=>"setSunday",
				"9holeprice"=>"set9holePrice",
				"9cartprice"=>"set9cartPrice",
				"18holeprice"=>"set18holePrice",
				"18cartprice"=>"set18cartPrice",
				"starttime"=>"setStartTime",
				"endtime"=>"setEndTime"
		];
		$data['attributes'] = array_change_key_case($data['attributes']);
		foreach($possibleSets as $attribute=>$functionName){
			if(isset($data['attributes'][$attribute])){
				$timeframe->{$functionName}($data['attributes'][$attribute]);
			}
		}








		if($timeframe->isValid()){
			$this->db->persist($timeframe);
			$this->db->flush();
		} else {
			return $this->respondWithError("The season is not valid. ");
		}




		$content = $this->serializeResource($timeframe);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}


	public function update(Request $request,$courseId,$teesheetId,$seasonId,$timeframeId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		/** @var ForeupSeasonalTimeframes $timeframe */
		$timeframe = $this->repository->find($timeframeId);
		if(empty($timeframe) || $timeframe->getSeason()->getCourse()->getCourseId() != $courseId){
			return $this->respondWithError("No permission. ",401);
		}


		$data = $request->request->get("data");


		if(!isset($data['attributes'])){
			return $this->respondWithError("Attributes is required. ");
		}

		if($timeframe->getDefault()){
			$possibleSets = [
				"9holeprice"=>"set9holePrice",
				"9cartprice"=>"set9cartPrice",
				"18holeprice"=>"set18holePrice",
				"18cartprice"=>"set18cartPrice"
			];
		} else {
			$possibleSets = [
				"name"=>"setTimeframeName",
				"monday"=>"setMonday",
				"tuesday"=>"setTuesday",
				"wednesday"=>"setWednesday",
				"thursday"=>"setThursday",
				"friday"=>"setFriday",
				"saturday"=>"setSaturday",
				"sunday"=>"setSunday",
				"9holeprice"=>"set9holePrice",
				"9cartprice"=>"set9cartPrice",
				"18holeprice"=>"set18holePrice",
				"18cartprice"=>"set18cartPrice",
				"starttime"=>"setStartTime",
				"endtime"=>"setEndTime"
			];
		}

		$data['attributes'] = array_change_key_case($data['attributes']);
		foreach($possibleSets as $attribute=>$functionName){
			if(isset($data['attributes'][$attribute])){
				$timeframe->{$functionName}($data['attributes'][$attribute]);
			}
		}

		$seasonRepo = $this->db->getRepository('e:ForeupSeasons');
		/** @var ForeupSeasons $season */
		$season = $seasonRepo->find($seasonId);

		if(empty($season)){
			return $this->respondWithError("Season does not exist.",404);
		}
		if(isset($data['relationships']['classes']['data']['id']) && !   $timeframe->getDefault()){
			/** @var ForeupPriceClass $priceClass */
			$class = $this->db->getRepository('e:ForeupPriceClass');
			$priceClass = $class->find($data['relationships']['classes']['data']['id']);
			if($priceClass->getCourse()->getCourseId() != $courseId){
				return $this->respondWithError("No permission. ",401);
			}

			$season->addClass($priceClass);
			$timeframe->setClass($priceClass);
		}


		if($timeframe->isValid()){
			$this->db->flush();
		} else {
			$this->respondWithError("The season is not valid. ");
		}
		$content = $this->serializeResource($timeframe);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}
}
?>