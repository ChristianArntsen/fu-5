<?php
namespace foreup\rest\controllers\courses\teesheets\seasons;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Logging\EchoSQLLogger;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupPriceClass;
use foreup\rest\models\entities\ForeupSeasonPriceClasses;
use foreup\rest\models\entities\ForeupSeasons;
use foreup\rest\models\entities\ForeupTeesheet;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\teesheet_seasons_classes_transformer;
use foreup\rest\resource_transformers\teetime_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class priceClasses extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;
    private $seasonRepository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupSeasonPriceClasses');
        $this->seasonRepository = $this->db->getRepository('foreup\rest\models\entities\ForeupSeasons');
        $this->transformer = new teesheet_seasons_classes_transformer();
        $this->resource_type = "seasons";
        $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId,$seasonId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;


        $criteria = new Criteria();
        $criteria->andWhere($criteria->expr()->eq('deleted',0));
        $criteria->andWhere($criteria->expr()->eq('seasonId', $seasonId));

        $results = $this->repository->matching($criteria);



        $content = $this->serializeResource($results->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function get(Request $request,$courseId,$teesheetId,$seasonId,$seasonPriceClassId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;


        $season = $this->seasonRepository->find($seasonId);
        if($season->getTeesheet()->getId() != $teesheetId){
            return $this->respondWithError("No permission. ",401);
        }

        $seasonPriceClass = $this->repository->find($seasonPriceClassId);

        $content = $this->serializeResource($seasonPriceClass);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function delete(Request $request,$courseId,$teesheetId,$seasonId, $seasonPriceClassId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupSeasons $season */
        $season = $this->seasonRepository->find($seasonId);
        if($season->getTeesheet()->getId() != $teesheetId){
            return $this->respondWithError("No permission. ",401);
        }

        $seasonPriceClass = $this->repository->find($seasonPriceClassId);

        $this->db->remove($seasonPriceClass);
        $this->db->flush();


        $content = $this->serializeResource($season);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function create(Request $request,$courseId,$teesheetId, $seasonId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        $data = $request->request->get("data");

        $seasonPriceClass = new ForeupSeasonPriceClasses();
        if(!isset($data['attributes'])){
            return $this->respondWithError("Attributes is required. ");
        }

        $seasonPriceClass->setSeasonId($seasonId);

        if($seasonPriceClass->isValid()){
            $this->db->persist($seasonPriceClass);
            $this->db->flush();
        } else {
            return $this->respondWithError("The seasonPriceClass is not valid. ");
        }

        $content = $this->serializeResource($seasonPriceClass);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function update(Request $request,$courseId,$teesheetId,$seasonId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupSeasons $season */
        $season = $this->repository->find($seasonId);
        if($season->getTeesheet()->getId() != $teesheetId){
            $this->respondWithError("No permission. ",401);
        }


        $data = $request->request->get("data");


        if(isset($data['attributes'])){
            if(isset($data['attributes']['name'])){
                $season->setSeasonName($data['attributes']['name']);
            }
            if(isset($data['attributes']['startDate'])){
                $date = Carbon::parse($data['attributes']['startDate']);
                $season->setStartDate($date);
            }
            if(isset($data['attributes']['endDate'])){
                $date = Carbon::parse($data['attributes']['endDate']);
                $season->setEndDate($date);
            }

            if($season->isValid()){
                $this->db->flush();
            } else {
                $this->respondWithError("The season is not valid. ");
            }

        }


        $content = $this->serializeResource($season);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }
}
?>