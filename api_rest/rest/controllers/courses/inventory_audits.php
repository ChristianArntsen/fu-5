<?php
namespace foreup\rest\controllers\courses;

use Doctrine\Common\Collections\Criteria;
use foreup\rest\controllers\api_controller;
use foreup\rest\resource_transformers\inventory_audit_transformer;
//use foreup\rest\resource_transformers\inventory_audit_items_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class inventory_audits extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();
        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupInventoryAudits');
        $this->transformer = new inventory_audit_transformer();
        $this->resource_type = "inventory_audits";
        $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;
        $criteria = new Criteria();
        $criteria->andWhere($criteria->expr()->eq('courseId', $courseId));
        $criteria->setMaxResults($this->limit);
        $criteria->setFirstResult($this->start);

        $results = $this->repository->matching($criteria);
        $content = $this->serializeResource($results->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function get(Request $request,$courseId,$inventoryAuditId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        $audit = $this->repository->find($inventoryAuditId);

        $content = $this->serializeResource($audit);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }
}
?>