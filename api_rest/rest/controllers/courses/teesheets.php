<?php
namespace foreup\rest\controllers\courses;

use Doctrine\Common\Collections\Criteria;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupTeesheet;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\teesheet_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class teesheets extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('e:ForeupTeesheet');
        $this->transformer = new teesheet_transformer();
        $this->resource_type = "teesheets";
	    $this->loadEmployeeInformation();
    }


	public function getAll(Request $request,$courseId)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;


		$criteria = new Criteria();
		$criteria->andWhere($criteria->expr()->eq('courseId', $courseId));
		$criteria->andWhere($criteria->expr()->eq('deleted', 0));
		$results = $this->repository->matching($criteria);
		$content = $this->serializeResource($results->toArray());
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

    public function get(Request $request,$courseId,$teesheetId)
    {
        $this->checkCoursePermission($request, $courseId);

        $teesheet = $this->repository->findOneBy([
            "course"=>$courseId,
            "id"=>$teesheetId
        ]);

        return $this->generateJsonResponse($teesheet);
    }

    public function update(Request $request,$courseId,$teesheetId)
    {
        $this->checkCoursePermission($request, $courseId);

        $teesheet = $this->repository->findOneBy([
            "course"=>$courseId,
            "id"=>$teesheetId
        ]);

        if(!empty($teesheet)){
            $this->updateResource($request, $teesheet);
        }

        return $this->generateJsonResponse($teesheet);
    }

    public function create(Request $request,$courseId)
    {
        $this->checkCoursePermission($request, $courseId);

        /** @var ForeupTeesheet $teesheet */
        $teesheet = new ForeupTeesheet($this->course);

        $this->db->persist($teesheet);
        $this->updateResource($request, $teesheet);

        return $this->generateJsonResponse($teesheet);
    }

    public function delete(Request $request,$courseId,$teesheetId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupTeesheet $teesheet */
        $teesheet = $this->repository->find($teesheetId);

        $this->db->remove($teesheet);

        $this->db->flush();

        $content = $this->serializeResource($teesheet);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }
}
?>