<?php
namespace foreup\rest\controllers\courses;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupPageSettings;
use foreup\rest\models\repositories\Filter;
use foreup\rest\models\services\FilterQueryParser;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\field_transformer;
use foreup\rest\resource_transformers\page_settings_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class page_settings extends api_controller
{
    /** @var \foreup\rest\models\repositories\CustomersRepository $repository */
    private $repository;


	/**
	 * @var EntityManager
	 */
	protected $db;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupPageSettings');
        $this->transformer = new page_settings_transformer();
        $this->resource_type = "page_settings";
	    $this->loadEmployeeInformation();
    }

	public function get(Request $request,$courseId,$page)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		/** @var ForeupPageSettings $pageSettings */
		$pageSettings = $this->repository->findBy([
			"page"=>$page,
			"course"=>$courseId
		]);

		if(count($pageSettings)>1){
			return $this->respondWithError("There appears to be multiple page settings saved, unable to decide which one to use",500);
		} else if(count($pageSettings)==1){
			$pageSettings = $pageSettings[0];
		} else {
			if($page == "customers"){
				$pageSettings = new ForeupPageSettings();
				$pageSettings->setColumns(["select","first_name","last_name","phone_number","email","actions"]);
				$pageSettings->setPageSize(25);
				$pageSettings->setPage("customers");
			} else {
				return $this->respondWithError("The settings do not exist",404);
			}
		}


		$content = $this->serializeResource($pageSettings);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}


	public function update(Request $request,$courseId,$page)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		$data = $request->request->get("data");


		if(!isset($data['attributes'])){
			return $this->respondWithError("Attributes is required. ");
		}

		/** @var ForeupPageSettings $pageSettings */
		$pageSettings = $this->repository->findOneBy([
			"page"=>$page,
			"course"=>$courseId
		]);
		if(count($pageSettings) == 0){
			//If page exists already
			//Update it, otherwise create
			$pageSettings = new ForeupPageSettings();
			$pageSettings->setColumns($data['attributes']['columns']);
			$pageSettings->setPageSize($data['attributes']['page_size']);
			$pageSettings->setCourse($this->db->getReference('e:ForeupCourses',$courseId));
			$pageSettings->setPage($page);
			$this->db->persist($pageSettings);
		} else {
			$pageSettings->setColumns($data['attributes']['columns']);
			$pageSettings->setPageSize($data['attributes']['page_size']);
		}

		$this->db->flush();







		$content = $this->serializeResource($pageSettings);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

}
?>