<?php
namespace foreup\rest\controllers\courses;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupPrinters;
use foreup\rest\models\entities\ForeupRefundReasons;
use foreup\rest\models\entities\ForeupTerminals;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\inventory_transformer;
use foreup\rest\resource_transformers\items_transformer;
use foreup\rest\resource_transformers\printers_transformer;
use foreup\rest\resource_transformers\return_reasons_transformer;
use foreup\rest\resource_transformers\terminals_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class refundReasons extends api_controller
{
    /** @var EntityRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupRefundReasons');
        $this->transformer = new return_reasons_transformer();
        $this->resource_type = "refundReasons";
	    $this->loadEmployeeInformation();
    }

	public function getAll(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);
		$results = $this->repository->findBy([
			"course"=>$courseId
		]);
		return $this->generateJsonResponse($results);
	}


	public function get(Request $request,$courseId,$reasonId)
	{
		$this->checkCoursePermission($request, $courseId);

		$reason = $this->repository->findOneBy([
			"course"=>$courseId,
			"id"=>$reasonId
		]);

		return $this->generateJsonResponse($reason);
	}

	public function update(Request $request,$courseId,$reasonId)
	{
		$this->checkCoursePermission($request, $courseId);

		$reason = $this->repository->findOneBy([
			"course"=>$courseId,
			"id"=>$reasonId
		]);

		if(!empty($reason)){
			$this->updateResource($request, $reason);
		}

		return $this->generateJsonResponse($reason);
	}

	public function create(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupPrinters $printer */
		$reason = new ForeupRefundReasons($this->course);

		$this->db->persist($reason);
		$this->updateResource($request, $reason);

		return $this->generateJsonResponse($reason);
	}

    public function delete(Request $request,$courseId,$reasonId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupRefundReasons $refundReason */
        $reason = $this->repository->find($reasonId);

        $this->db->remove($reason);

        $this->db->flush();

        $content = $this->serializeResource($reason);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

}
?>