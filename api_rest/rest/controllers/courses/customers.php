<?php
namespace foreup\rest\controllers\courses;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;
use foreup\rest\controllers\api_controller;
use foreup\rest\controllers\traits\rollback_trait;
use foreup\rest\models\entities\ForeupCustomers;
use foreup\rest\models\entities\ForeupPeople;
use foreup\rest\models\services\FilterQueryParser;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\field_transformer;
use foreup\rest\resource_transformers\person_transformer;
use League\Csv\Writer;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class customers extends api_controller
{

	use rollback_trait;

    /** @var \foreup\rest\models\repositories\CustomersRepository $repository */
    private $repository;


	/**
	 * @var EntityManager
	 */
	protected $db;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('e:ForeupCustomers');
        $this->transformer = new customer_transformer();
        $this->resource_type = "customers";
	    $this->loadEmployeeInformation();


	    if(!$this->auth_user->hasLimitation("emails")){
			$this->transformer->hideEmails();
	    }
    }

	//Takes an advanced search query comparison like "eq:2017-05-20"
	//and breaks it up into its constituent pieces
	private function getComparisons($query){
		$comparisons = [];

		$allValues = preg_split("/,/", $query);
		foreach ($allValues as $key => $parameter) {
			$matches = [];
			$operator = "";
			$value = "";
			preg_match("/^((\w+):)?(.+)?$/", $parameter, $matches);
			if(count($matches) == 4){
				$operator = $matches[2];
				$value = $matches[3];
			}else if(count($matches) == 2){
				$operator = "eq";
				$value = $matches[1];
			}
			array_push($comparisons, array($operator,$value));
		}

		return $comparisons;
	}

	private function isDateField($field){
		if($field === "date_created" || $field === "birthday") {
			return true;
		}

		return false;
	}

	private function transformDateComparison($request){
		$queries = [];

		foreach($request->query->all() as $field => $value) {

			if ($this->isDateField($field)) {
				$comparisons = $this->getComparisons($value);
				$date_query = "";
				foreach ($comparisons as $comparison) {
					$operator = $comparison[0];
					$val = $comparison[1];

					switch ($operator) {
						case "eq":
							$date_query .= "gte:" . $val . "T00:00:00,lte:" . $val . "T23:59:59,";
							break;
						case "neq":
							$date_query .= "lt:" . $val . "T00:00:00,gt:" . $val . "T23:59:59,";
							break;
						case "lte":
							$date_query .= $operator . ":" . $val . "T23:59:59,";
							break;
						case "gte":
							$date_query .= $operator . ":" . $val . "T00:00:00,";
							break;
						default:
							$date_query .= $operator . ":" . $val . ",";
							break;
					}
				}

				$date_query = trim($date_query, ",");
				$request->query->set($field, $date_query);
			}
		}

		return $request;
	}

	private function transformPhoneNumbers($request){
		$phone_number = "";
		$comparisons = $request->query->get("phone_number");

		if($comparisons !== null) {
			$comparisons = $this->getComparisons($comparisons);
			foreach ($comparisons as $comparison) {
				$operator = $comparison[0];
				$original_search = $comparison[1];
				$search = str_replace(array('(', ')', '-', '_',','), '',$original_search);
				$phone_number .= $operator . ":" . $search . ",";
			}

			$phone_number = trim($phone_number, ",");
			$request->query->set("phone_number", $phone_number);
		}

		return $request;
	}

    public function getAll(Request $request,$courseId)
    {
	    $this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
		    return $this->response;

	    $courseIds = $this->course->getGroupMemberIds("customers");

	    $queryParser = new FilterQueryParser();

	    $wildCardQuery = null;
	    if($request->get("q")){
	    	$expGen = $this->db->getExpressionBuilder();
		    $original_search = $request->get("q");
		    $search = str_replace(array('(', ')', '-', '_',','), '',$original_search);
		    $names = preg_split("/\s/",$original_search);

		    $orX = $expGen->orX();
		    $orX->add($expGen->like("person.firstName",$expGen->literal("%$original_search%")));
		    $orX->add($expGen->like("person.lastName",$expGen->literal("%$original_search%")));
		    $orX->add($expGen->like("person.cellPhoneNumber",$expGen->literal($search."%")));
		    $orX->add($expGen->like("person.phoneNumber",$expGen->literal($search."%")));
		    $orX->add($expGen->like("person.email",$expGen->literal($original_search."%")));
		    $orX->add($expGen->like("customers.accountNumber",$expGen->literal($original_search."%")));
		    $orX->add($expGen->like("customers.accountNumber",$expGen->literal($search."%")));

		    if(count($names) >= 2){
		    	$andX = $expGen->andX();
			    $andX->add($expGen->like("person.firstName",$expGen->literal("%$names[0]%")));
			    $andX->add($expGen->like("person.lastName",$expGen->literal("%".($names[count($names)-1])."%")));

			    $andX2 = $expGen->andX();
			    $andX2->add($expGen->like("person.firstName",$expGen->literal("%".($names[count($names)-1])."%")));
			    $andX2->add($expGen->like("person.lastName",$expGen->literal("%$names[0]%")));

			    $orX->add($andX);
			    $orX->add($andX2);
		    }

		    $wildCardQuery = $orX;
	    }

		$request = $this->transformDateComparison($request);

		$request = $this->transformPhoneNumbers($request);

	    $filters = $queryParser->generateFilters($request,$this->repository);
	    if($filters === false){
		    return $this->respondWithError("Problem generating filters for the request",500);
	    }

	    if($request->get("format") == "csv"){
		    return $this->exportToCsv($filters, $courseIds, $queryParser, $wildCardQuery);
	    }
	    if($request->get("format") == "simple"){
	    	$this->repository->setIdOnly(true);
		    $custs = $this->repository->getArrayResults(
			    $filters,
			    $courseIds,
			    $this->start,
			    $this->limit,
			    $wildCardQuery,
			    $this->sortBy,
			    $this->order
		    );
		    $returnObject = [];
		    foreach($custs as $customer){
				$returnObject[] = $customer['personId'];
		    }
		    $response = new JsonResponse();
		    $response->setContent(json_encode($returnObject));
		    return $response;
	    }

		$custs = $this->repository->getFilteredResults(
			$filters,
			$courseIds,
			$this->start,
			$this->limit,
			$wildCardQuery,
			$this->sortBy,
			$this->order
		);
	    $total =  $this->repository->getFilteredCount(
		    $filters,
		    $courseIds,
		    $this->start,
		    $this->limit,
		    $wildCardQuery,
		    $this->sortBy,
		    $this->order
	    );



	    $content = $this->serializeResource($custs);
	    $content = json_decode($content);
	    $content->meta = [];
	    $content->meta['total'] = $total;
	    $content = json_encode($content);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

	public function delete(Request $request,$courseId,$id)
	{
		$this->saveParametersAndDefaults($request);
		if(!$this->checkAccess($courseId))
			return $this->response;

		$courseIds = $this->course->getGroupMemberIds("customers");
		/** @var ForeupCustomers $customers */
		$customers = $this->repository->findOneBy([
				"person"=>$id,
				"courseId"=>$courseIds,
		]);

		$customers->deleteCustomer($this->db);
		$this->db->flush();

		$content = $this->serializeResource($customers);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	public function get(Request $request,$courseId,$id)
	{
		$this->checkCoursePermission($request, $courseId);
		$courseIds = $this->course->getGroupMemberIds("customers");


		$customer = $this->repository->findOneBy([
			"person"=>$id,
			"course"=>$courseIds
		]);
		if(empty($customer)){
			return $this->respondWithError("Customer not found.",404);
		}

		return $this->generateJsonResponse($customer);
	}

	public function fields(Request $request,$courseId)
	{
		$this->transformer = new field_transformer();
		$this->resource_type = "field";
		$fields = $this->repository->getAllSearchableFields();
		if(isset($fields['group'])){

			$groupRepo = $this->db->getRepository('e:ForeupCustomerGroups');
			$groups = $groupRepo->findBy([
				"course"=>$this->db->getReference('e:ForeupCourses',$courseId),
			]);
			foreach($groups as $group){
				$fields['group']['possibleValues'][] = $group->getLabel();
			}
		}
		if(isset($fields['state'])){
			$fields['state']['possibleValues'] = ["AK","AZ","AR","CA","CO","CT","DE","DC","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"];
		}
		$content = $this->serializeResource($fields);
		$response = new JsonResponse();
		$response->setContent($content);
		return $response;
	}

	/**
	 * @param Request $request
	 * @param $courseId
	 * @param $queryParser
	 * @param $wildCardQuery
	 * @return StreamedResponse
	 */
	private function exportToCsv($filters,$courseIds, $queryParser, $wildCardQuery)
	{
		$pageSize = 10000;
		$query = $this->repository->createQuery(
			$filters,
			$courseIds,
			0,
			$pageSize,
			$wildCardQuery,
			$this->sortBy,
			$this->order
		);


		$paginator = new Paginator($query,false);
		$paginator->setUseOutputWalkers(false);

		//$query->setHint('knp_paginator.count',$paginator->count());
		//$totalResults = count($paginator);


		$report_name = sha1(time()).".csv";
		//Save file to s3
		if (!file_exists('reports/')) {
			mkdir('reports/', 0777, true);
		}
		$file = new \SplFileObject("reports/$report_name","w");
		$writer = Writer::createFromFileObject($file);

		$counter = 0;
		$manager = new Manager();

		$currentStart = 0;
		$resultsRemain = true;
		while($resultsRemain){
			$resultsRemain = false;
			foreach ($paginator as $customer) {
				$resultsRemain = true;
				$counter++;
				$resource = new Item($customer, $this->transformer, $this->resource_type);
				$row = $manager->createData($resource)->toArray();
				unset($resource);
				$row = array_merge($row['data'], $row['data']['contact_info']);
				unset($row['contact_info']);
				unset($row['groups']);
				$writer->insertOne((array)$row);
				$this->db->detach($customer->getPerson());
				$this->db->detach($customer);
			}
			$currentStart += $pageSize;
			$query->setFirstResult($currentStart);
			$query->setMaxResults($pageSize);


		}



		clearstatcache(false, "reports/$report_name");
		$response = new BinaryFileResponse("reports/$report_name");
		$response->headers->set('Content-Type', 'text/csv; charset=utf-8');
		$disposition = $response->headers->makeDisposition(
			ResponseHeaderBag::DISPOSITION_ATTACHMENT,
			$report_name
		);
		$response->headers->set('Content-Disposition',$disposition);
		return $response;
	}



	public function update(Request $request,$courseId,$id)
	{
		$this->checkCoursePermission($request, $courseId);

		$data = $request->request->get("data");


		$courseIds = $this->course->getGroupMemberIds("customers");
		/** @var ForeupCustomers $customer */
		$customer = $this->repository->findOneBy([
				"person"=>$id,
				"course"=>$courseId,
		]);
		if(empty($customer)){
			return $this->respondWithError("Customer not found.",404);
		}

		if($customer->getDeleted() == true && $data['attributes']['deleted'] == false){
			$customer->undeleteCustomer();
		}

		$this->fieldsToSkip = ["deleted","course","groups","contact_info","photo","","date_created","username","opt_out_email","email_subscribed","account_balance","member_account_balance","opt_out_text"];
		$this->createPersonFromRequest($data,$customer->getPerson());
		$this->updateResource($request, $customer);
		return $this->generateJsonResponse($customer);
	}

	public function create(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupCustomers $customer*/
		$customer = new ForeupCustomers();

		$data = $request->request->get("data");

		$person = new ForeupPeople();
		$person = $this->createPersonFromRequest($data,$person);


		$customer->setPerson($person);
		$customer->setCourse($this->course);
		$this->fieldsToSkip = ["groups","contact_info","photo","","priceClassId","priceClass"];
		$this->updateResource($request, $customer);

		if (!empty($data['attributes']['priceClassId'])) {
				/** @var EntityRepository $classRepo */
				$classRepo = $this->db->getRepository('e:ForeupPriceClass');
				$priceClass = $classRepo->findOneBy([
					"course"=>$this->course->getGroupMemberIds(),
					"id"=>$data['attributes']['priceClassId']
				]);
				if(empty($priceClass)){
					return $this->respondWithError("There doesn't exist a price class for the id: ".$data['attributes']['priceClassId']);
				}
			$customer->setPriceClass($data['attributes']['priceClassId']);

		} else {
			$customer->setPriceClass("");
		}



		$this->db->persist($person);
		$this->db->flush();
		$customer->setPerson($person);
		$customer->setPhoto($this->db->getReference('e:ForeupImages',0));

		if($person->getFirstName() == "" || $person->getLastName() == "" || $person->getEmail() == "")
		{
			return $this->respondWithError("Creating a customer requires a first name, last name, and an email.",403);
		}

		$this->db->persist($customer);
		try{
			$this->db->flush();
		} catch (UniqueConstraintViolationException $e){
			return $this->respondWithError("Validation error, account number not unique.",401);
		}
		return $this->generateJsonResponse($customer);
	}

	/**
	 * @param $data
	 * @return ForeupPeople
	 * @throws \Exception
	 */
	private function createPersonFromRequest($data,$person): ForeupPeople
	{
		$personTransformer = new person_transformer();
		$transformedObject = $personTransformer->transform($person);
		$personFieldsToSkip = ["id"];
		foreach ($transformedObject as $field => $value) {
			if (array_search($field, $personFieldsToSkip) !== false)
				continue;

			if (isset($data['attributes']['contact_info'][$field])) {
				$functionName = "set" . $field;
				$functionName = preg_replace("/\_/", "", $functionName);
				if (!method_exists($person, $functionName)) {
					throw new \Exception("Something seems to have been set up wrong, the following method doesn't exist on our object: " . $functionName);
				}
				$person->{$functionName}($data['attributes']['contact_info'][$field]);
			}
		}
		return $person;
	}
}
?>