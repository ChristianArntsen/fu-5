<?php
namespace foreup\rest\controllers\courses;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use foreup\rest\controllers\api_controller;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\inventory_transformer;
use foreup\rest\resource_transformers\items_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class inventory extends api_controller
{
    /** @var \foreup\rest\models\repositories\CoursesRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupInventory');
        $this->transformer = new inventory_transformer();
        $this->resource_type = "inventory";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId)
    {
	    $this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
		    return $this->response;


	    $startDate = !empty($request->get("startDate"))?Carbon::parse($request->get("startDate")):Carbon::now()->startOfDay();
	    $endDate = !empty($request->get("endDate"))?Carbon::parse($request->get("endDate")):Carbon::now()->endOfDay();

	    $criteria = new Criteria();
	    $criteria->andWhere($criteria->expr()->gte('transDate', $startDate));
	    $criteria->andWhere($criteria->expr()->lte('transDate', $endDate));
	    $criteria->andWhere($criteria->expr()->eq('courseId', $courseId));
	    $criteria->setMaxResults($this->limit);
	    $criteria->setFirstResult($this->start);


	    $results = $this->repository->matching($criteria);
	    $content = $this->serializeResource($results->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }


}
?>