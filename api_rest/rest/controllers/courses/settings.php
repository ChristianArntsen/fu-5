<?php
namespace foreup\rest\controllers\courses;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\controllers\traits\rollback_trait;
use foreup\rest\models\entities\ForeupCourses;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\items_transformer;
use foreup\rest\resource_transformers\settings_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class settings extends api_controller
{
	use rollback_trait;
    /** @var EntityRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupCourses');
        $this->transformer = new settings_transformer();
        $this->resource_type = "settings";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId)
    {
	    $this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
		    return $this->response;

	    /** @var ForeupCourses $course */
	    $course = $this->repository->find($courseId);

	    $content = $this->serializeResource($course);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function update(Request $request,$courseId)
    {
	    $this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
		    return $this->response;

	    /** @var ForeupCourses $course */
	    $course = $this->repository->find($courseId);

	    $data = $request->request->get("data");
	    if(!isset($data['attributes'])){
		    return $this->respondWithError("Attributes is required. ");
	    }


	    $transformedObject = $this->transformer->transform($course);
        foreach($transformedObject as $field=>$value)
	    {
			if(isset($data['attributes'][$field])){
                $setFunction = "set".$this->getFieldName($field);
                if(!method_exists($course,$setFunction)){
					throw new \Exception("Something seems to have been set up wrong, the following method doesn't exist on our object: ".$functionName);
				}
				$course->{$setFunction}($data['attributes'][$field]);
			}
	    }

	    $this->db->flush();

	    $content = $this->serializeResource($course);
	    $response = new JsonResponse();
	    $response->setContent($content);
	    return $response;
    }

    public function getFieldName($field) {
        $fieldNames = array(
            'businessName'=>'Name',
            'businessAddress'=>'Address',
            'country'=>'Country',
            'businessCity'=>'City',
            'businessState'=>'State',
            'postalCode'=>'Postal',
            'timezone'=>'Timezone',
            'dateFormat'=>'DateFormat',
            'timeFormat'=>'TimeFormat',
            'phoneNumber'=>'Phone',
            'faxNumber'=>'Fax',
            'emailAddress'=>'Email',
            'invoiceEmailAddress'=>'BillingEmail',
            'websiteUrl'=>'Website',
            'defaultTax1Rate'=>'DefaultTax1Rate',
			'defaultTax1Name'=>'DefaultTax1Name',
			'defaultTax2Rate'=>'DefaultTax2Rate',
			'defaultTax2Name'=>'DefaultTax2Name',
			'defaultTax2Cumulative'=>'DefaultTax2Cumulative',
			'autoGratuity'=>'AutoGratuity',
			'autoGratuityThreshold'=>'AutoGratuityThreshold',
			'serviceFeeActive'=>'ServiceFeeActive',
			'serviceFeeTax1Name'=>'ServiceFeeTax1Name',
			'serviceFeeTax1Rate'=>'ServiceFeeTax1Rate',
			'serviceFeeTax2Name'=>'ServiceFeeTax2Name',
			'serviceFeeTax2Rate'=>'ServiceFeeTax2Rate',
			'creditCardFee'=>'CreditCardFee',
			'unitPriceIncludesTax'=>'UnitPriceIncludesTax',
			'customerCreditNickname'=>'CustomerCreditNickname',
			'memberBalanceNickname'=>'MemberBalanceNickname',
			'creditDepartment'=>'CreditDepartment',
			'creditDepartmentName'=>'CreditDepartmentName',
			'creditCategory'=>'CreditCategory',
			'creditCategoryName'=>'CreditCategoryName',
			'returnPolicy'=>'ReturnPolicy',
			'printAfterSale'=>'PrintAfterSale',
			'webprnt'=>'Webprnt',
			'webprntIp'=>'WebprntIp',
			'webprntHotIp'=>'WebprntHotIp',
			'webprntColdIp'=>'WebprntColdIp',
			'webprntLabelIp'=>'WebprntLabelIp',
			'useKitchenBuzzers'=>'UseKitchenBuzzers',
			'printCreditCardReceipt'=>'PrintCreditCardReceipt',
			'printSalesReceipt'=>'PrintSalesReceipt',
			'printTwoReceipts'=>'PrintTwoReceipts',
			'printTwoSignatureSlips'=>'PrintTwoSignatureSlips',
			'printTwoReceiptsOther'=>'PrintTwoReceiptsOther',
			'printTipLine'=>'PrintTipLine',
			'cashDrawerOnCash'=>'CashDrawerOnCash',
			'cashDrawerOnSale'=>'CashDrawerOnSale',
			'updatedPrinting'=>'UpdatedPrinting',
			'afterSaleLoad'=>'AfterSaleLoad',
			'teesheetUpdatesAutomatically'=>'TeesheetUpdatesAutomatically',
			'sendReservationConfirmations'=>'SendReservationConfirmations',
			'autoSplitTeetimes'=>'AutoSplitTeetimes',
			'fnbLogin'=>'FnbLogin',
			'receiptPrinter'=>'ReceiptPrinter',
			'trackCash'=>'TrackCash',
			'blindClose'=>'BlindClose',
			'requireEmployeePin'=>'RequireEmployeePin',
			'minimumFoodSpend'=>'MinimumFoodSpend',
			'separateCourses'=>'SeparateCourses',
			'defaultKitchenPrinter'=>'DefaultKitchenPrinter',
			'hideEmployeeLastNameReceipt'=>'HideEmployeeLastNameReceipt',
			'requireGuestCount'=>'RequireGuestCount',
			'useCourseFiring'=>'UseCourseFiring',
			'courseFiringIncludeItems'=>'CourseFiringIncludeItems',
			'hideModifierNamesKitchenReceipts'=>'HideModifierNamesKitchenReceipts',
			'foodBevSortBySeat'=>'FoodBevSortBySeat',
			'defaultRegisterLogOpen'=>'DefaultRegisterLogOpen',
			'printSuggestedTip'=>'PrintSuggestedTip',
			'foodBevTipLineFirstReceipt'=>'FoodBevTipLineFirstReceipt',
			'autoEmailReceipt'=>'AutoEmailReceipt',
			'autoEmailNoPrint'=>'AutoEmailNoPrint',
			'requireSignatureMemberPayments'=>'RequireSignatureMemberPayments',
			'hideTaxable'=>'HideTaxable',
			'allowEmployeeRegisterLogBypass'=>'AllowEmployeeRegisterLogBypass',
			'teetimeDefaultToPlayer1'=>'TeetimeDefaultToPlayer1',
			'printTeeTimeDetails'=>'PrintTeeTimeDetails',
			'receiptPrintAccountBalance'=>'ReceiptPrintAccountBalance',
			'printMinimumsOnReceipt'=>'PrintMinimumsOnReceipt',
			'limitFeeDropdownByCustomer'=>'LimitFeeDropdownByCustomer',
			'requireCustomerOnSale'=>'RequireCustomerOnSale',
	        'id' => 'CourseId',
            'openTime'=>'OpenTime',
            'closeTime'=>'CloseTime',
            'earlyBirdHoursBegin'=>'EarlyBirdHoursBegin',
            'earlyBirdHoursEnd'=>'EarlyBirdHoursEnd',
            'morningHoursBegin'=>'MorningHoursBegin',
            'morningHoursEnd'=>'MorningHoursEnd',
            'afternoonHoursBegin'=>'AfternoonHoursBegin',
            'afternoonHoursEnd'=>'AfternoonHoursEnd',
            'twilightHour'=>'TwilightHour',
            'superTwilightHour'=>'SuperTwilightHour',
            'holidays'=>'Holidays',
            'holes'=>'Holes',
            'onlineBooking'=>'OnlineBooking',
            'onlineBookingProtected'=>'OnlineBookingProtected',
            'includeTaxOnlineBooking'=>'IncludeTaxOnlineBooking',
            'bookingRules'=>'BookingRules',
            'noShowPolicy'=>'NoShowPolicy',
            'termsAndConditions'=>'TermsAndConditions',
            'onlineBookingWelcomeMessage'=>'OnlineBookingWelcomeMessage',
            'openSun'=>'OpenSun',
            'openMon'=>'OpenMon',
            'openTue'=>'OpenTue',
            'openWed'=>'OpenWed',
            'openThu'=>'OpenThu',
            'openFri'=>'OpenFri',
            'openSat'=>'OpenSat',
            'weekendFri'=>'WeekendFri',
            'weekendSat'=>'WeekendSat',
            'weekendSun'=>'WeekendSun',
            'useNewPermissions'=>'UseNewPermissions',
            'mailchimpApiKey'=>'MailchimpApiKey',
            'billingEmail'=>'BillingEmail',
            'reservationEmail'=>'ReservationEmail',
            'numberOfItemsPerPage'=>'NumberOfItemsPerPage',
            'hideBackNine'=>'HideBackNine',
            'stackTeeSheets'=>'StackTeeSheets',
            'sideBySideTeeSheets'=>'SideBySideTeeSheets',
            'alwaysListAll'=>'AlwaysListAll',
            'currentDayCheckinsOnly'=>'CurrentDayCheckinsOnly',
            'useEtsGiftcards'=>'UseEtsGiftcards',
            'deductTips'=>'DeductTips',
            'onlineGiftcardPurchases'=>'OnlineGiftcardPurchases',
            'showInvoiceSaleItems'=>'ShowInvoiceSaleItems',
            'teedOffColor'=>'TeedOffColor',
            'creditLimitToSubtotal' => 'CreditLimitToSubtotal',
            'onlinePurchaseTerminalId' => 'OnlinePurchaseTerminalId',
            'onlineInvoiceTerminalId' => 'OnlineInvoiceTerminalId',
            'useTerminals' => 'UseTerminals',
            'loyaltyAutoEnroll' => 'LoyaltyAutoEnroll',
            'useLoyalty' => 'UseLoyalty',
            'reservationEmailText'=>'ReservationEmailText'
        );


        return $fieldNames[$field];
    }
}
?>