<?php
namespace foreup\rest\controllers\courses\customers;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\services\AuthenticatedUser;
use foreup\rest\resource_transformers\customer_group_transformer;
use foreup\rest\models\entities\ForeupCustomerGroups;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class groups extends api_controller
{
    /** @var EntityR $repository */
    private $repository;


	/**
	 * sales constructor.
	 * @param $db
	 * @param AuthenticatedUser $auth_user
	 */
	public function __construct($db, $auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupCustomerGroups');
        $this->transformer = new customer_group_transformer();
        $this->resource_type = "customer_groups";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId)
    {
		$this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
	    	return $this->response;

        $courseIds = $this->course->getGroupMemberIds("customers");

	    $criteria = new Criteria();
	    $criteria->andWhere($criteria->expr()->in('course', $courseIds));
	    $groups = $this->repository->matching($criteria);


        $content = $this->serializeResource($groups->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

    public function create(Request $request,$courseId){
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        $data = $request->request->get('data');
        if(isset($data['attributes'])){
            $data = $data['attributes'];
        } else {
            return $this->respondWithError("Missing attribute field",400);
        }
        if(empty($this->auth_user->getEmpId())){
            return $this->respondWithError("Your token doesn't have access to this endpoint, contact foreUp if this is a mistake.");
        }

        $customerGroup = $this->save($courseId, $data);

        $content = $this->serializeResource($customerGroup);
        $response = new JsonResponse();
        $response->setContent($content);

        return $response;
    }

    public function delete(Request $request, $courseId, $id) {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        $link = $this->db->getRepository('e:ForeupCustomerGroups')->find($id);

        $this->db->remove($link);
        $this->db->flush();

        $response = new JsonResponse();

        $response->setContent('{"data":{"success":true,"content":"customerGroup instance deleted"}}');
        return $response;
    }

    public function update(Request $request, $courseId, $id) {
        $this->saveParametersAndDefaults($request);

        if(!$this->checkAccess($courseId))
            return $this->response;

        $data = $request->request->get('data');

        if(isset($data['attributes'])){
            $data = $data['attributes'];
            if(isset($id))$data['id']=$id;
            elseif(isset($data['id']))$id = $data['id'];
            if(!isset($id)){
                return $this->respondWithError("Missing id parameter",400);
            }
        } else {
            return $this->respondWithError("Missing attribute field",400);
        }

        $link = $this->save($courseId, $data);
        $response = new JsonResponse();
        $resource = $this->serializeResource($link);

        $response->setContent($resource);

        return $response;
    }

    private function save($courseId, $data){

        if(!isset($data['id'])) {
            $customerGroup = new ForeupCustomerGroups();
        }else{
            $customerGroup = $this->db->getRepository('e:ForeupCustomerGroups')->find($data['id']);
            if(is_array($customerGroup))$customerGroup = $customerGroup[0];
            if(!$customerGroup)return $customerGroup;
        }
        isset($data['label']) ? $customerGroup->setLabel($data['label']) : "";
        isset($data['itemDiscount']) ? $customerGroup->setItemCostPlusPercentDiscount($data['itemDiscount']) : "";
        $customerGroup->setCourse($this->course);

        $this->db->persist($customerGroup);
        $this->db->flush();

        return $customerGroup;
    }
}
?>