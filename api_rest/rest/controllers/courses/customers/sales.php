<?php
namespace foreup\rest\controllers\courses\customers;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\services\AuthenticatedUser;
use foreup\rest\resource_transformers\sales_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class sales extends api_controller
{
    /** @var EntityManager $repository */
    private $repository;


	/**
	 * sales constructor.
	 * @param $db
	 * @param AuthenticatedUser $auth_user
	 */
	public function __construct($db, $auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupSales');
        $this->transformer = new sales_transformer();
        $this->resource_type = "sales";
	    $this->loadEmployeeInformation();
    }

    public function getAll(Request $request,$courseId,$personId)
    {
		$this->saveParametersAndDefaults($request);
	    if(!$this->checkAccess($courseId))
	    	return $this->response;

	    $criteria = new Criteria();
	    $criteria->andWhere($criteria->expr()->eq('person', $this->db->getReference('e:ForeupPeople',$personId)));
	    $criteria->andWhere($criteria->expr()->eq('courseId', $courseId));
	    $criteria->andWhere($criteria->expr()->eq('deleted', false));
	    $criteria->setMaxResults($this->limit);
	    $criteria->setFirstResult($this->start);
	    $criteria->orderBy(["saleId"=>Criteria::DESC]);
	    $sales = $this->repository->matching($criteria);




        $content = $this->serializeResource($sales->toArray());
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }
}
?>