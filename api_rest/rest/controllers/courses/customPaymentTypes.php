<?php
namespace foreup\rest\controllers\courses;

use Carbon\Carbon;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use foreup\rest\controllers\api_controller;
use foreup\rest\models\entities\ForeupCustomPaymentTypes;
use foreup\rest\models\entities\ForeupPrinters;
use foreup\rest\models\entities\ForeupTerminals;
use foreup\rest\resource_transformers\custom_payment_types_transformer;
use foreup\rest\resource_transformers\customer_transformer;
use foreup\rest\resource_transformers\inventory_transformer;
use foreup\rest\resource_transformers\items_transformer;
use foreup\rest\resource_transformers\printers_transformer;
use foreup\rest\resource_transformers\terminals_transformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class customPaymentTypes extends api_controller
{
    /** @var EntityRepository $repository */
    private $repository;

    public function __construct($db,$auth_user)
    {
        parent::__construct();

        $this->db = $db;
        $this->auth_user = $auth_user;
        $this->repository = $this->db->getRepository('foreup\rest\models\entities\ForeupCustomPaymentTypes');
        $this->transformer = new custom_payment_types_transformer();
        $this->resource_type = "customPaymentTypes";
	    $this->loadEmployeeInformation();
    }

	public function getAll(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);
		$results = $this->repository->findBy([
			"course"=>$courseId,
            "deleted"=>0
		]);
		return $this->generateJsonResponse($results);
	}


	public function get(Request $request,$courseId,$paymentId)
	{
		$this->checkCoursePermission($request, $courseId);

		$payment = $this->repository->findOneBy([
			"course"=>$courseId,
			"id"=>$paymentId
		]);

		return $this->generateJsonResponse($payment);
	}

	public function update(Request $request,$courseId,$paymentId)
	{
		$this->checkCoursePermission($request, $courseId);

		$payment = $this->repository->findOneBy([
			"course"=>$courseId,
			"id"=>$paymentId
		]);

		if(!empty($payment)){
			$this->fieldsToSkip = ["customPaymentType"];
			$this->updateResource($request, $payment);
		}

		return $this->generateJsonResponse($payment);
	}

	public function create(Request $request,$courseId)
	{
		$this->checkCoursePermission($request, $courseId);

		/** @var ForeupPrinters $printer */
		$reason = new ForeupCustomPaymentTypes($this->course);

		$this->db->persist($reason);
		$this->fieldsToSkip = ["customPaymentType"];
		$this->updateResource($request, $reason);

		return $this->generateJsonResponse($reason);
	}

    public function delete(Request $request,$courseId,$paymentId)
    {
        $this->saveParametersAndDefaults($request);
        if(!$this->checkAccess($courseId))
            return $this->response;

        /** @var ForeupCustomPaymentTypes $paymentType */
        $paymentType = $this->repository->find($paymentId);

        $paymentType->setDeleted(1);

        $this->db->flush();

        $content = $this->serializeResource($paymentType);
        $response = new JsonResponse();
        $response->setContent($content);
        return $response;
    }

}
?>