<?php
namespace foreup\rest\application;
if(function_exists("newrelic_disable_autorum")){
	newrelic_disable_autorum();
}
require_once './vendor/autoload.php';
require_once 'rest/application/environment.php';
date_default_timezone_set('America/Denver');



if (extension_loaded('newrelic'))
	newrelic_set_appname("api");

setlocale(LC_MONETARY, 'en_US.UTF-8');


use Silex\Application;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use foreup\rest\middleware;
use foreup\rest\application\CustomException;


// Verify environment has been correctly set up ====================================================
if(!defined('ENVIRONMENT'))
{
	trigger_error('Invalid ENVIRONMENT. Please check your web server configuration.', E_USER_ERROR);
	exit (1);
}

// CREATE THE SILEX APPLICATION ====================================================
$app = new Application();

// 1) Load the working configuration
$configLoader = new config($app);
$configLoader->loadConfig();

// 2) Register Middleware ====================================================
$middleware_loader = new middleware_loader($app);
$middleware_loader->load_middleware();

// 3) Register Services ====================================================
$servicesLoader = new services_loader($app);
$servicesLoader->register_services();

// 4) Register Custom Error Handler ====================================================
$app->error(function (\Exception $e, $code) use ($app) {
	$json_response = new JsonResponse();

	if($app['debug'] === true){
		return ;
	} else {

		if(extension_loaded('newrelic')){
			newrelic_notice_error($e->getMessage(),$e);
		} else {
			error_log("New relic isn't hooked up correctly");
		}
		error_log( $e->getMessage());
		$json_response->setData(array(
			'success'=>false,
			"statusCode" => $e->getCode()
		));

	}
	return $json_response;
});
ErrorHandler::register();


// 5) Register Routes ====================================================
$routesLoader = new routes_loader($app);
$routesLoader->bindRoutesToControllers();

// Run the application ====================================================
$app->run();
