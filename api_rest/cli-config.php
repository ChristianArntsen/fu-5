<?php
namespace foreup\rest;

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// replace with file to your own project bootstrap
require_once "vendor/autoload.php";

$paths = array("rest/models/entities");
$proxyDir = 'rest/models/proxies';
$isDevMode = true; // If true -> Caching is done in memory, and proxy objects are created on each request

$dbParams = array(
    "driver" => "mysqli",
    "user" => "admin",
    "password" => "password",
    "dbname" => "foreup_dev",
    "host" => "localhost",
);

/*
Commands to run to create database annotations/model entities:

Run from api_rest

php vendor/doctrine/orm/bin/doctrine orm:convert-mapping annotation ./ --from-database --namespace="\\foreup\\rest\\models\\entities\\" --filter ForeupPosCart

after the entity is generated in to foreup/rest/models/entities/foreup folder, move it to the rest/models/entities folder, then run the following

 * To generate getters and setters for a single entity run this command:
 * php vendor/doctrine/orm/bin/doctrine orm:generate-entities ./rest/models/entities --generate-annotations=true --filter ForeupCustomerGroupMembers
 *
 * To generate getters and setters for all entities run this command:
 * php vendor/doctrine/orm/bin/doctrine orm:generate-entities ./rest/models/entities --generate-annotations=true

the file will be regenerated in the foreup/rest/models/entities/foreup folder. now drag that one down to replace the one already in the rest/models/entities folder
*/

//TODO: after running the above commands run rm -rf /tmp/cache in your dev environment

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, $proxyDir,null,false);
$entityManager = EntityManager::create($dbParams, $config);


/*
 * to generate getters and setters for multiple entities use a .* instead of the $
 */
$entityManager->getConnection()->getConfiguration()->setFilterSchemaAssetsExpression("/^(foreup_customer_group_members)$/");


$connection = $entityManager->getConnection();
$connection->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

return ConsoleRunner::createHelperSet($entityManager);
